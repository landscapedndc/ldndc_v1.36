/*!
 * @brief
 *    ldndc process monitor library
 *
 * @author
 *    steffen klatt (created on: mar 23, 2015)
 */

#ifndef  LDNDC_PROCMONITOR_LIB_H_
#define  LDNDC_PROCMONITOR_LIB_H_

#include  "ld_init.h"
#include  "global/libconfig.h"

#define  simmonitor_addr "tcp://*:8001"

struct LDNDC_API ldndc_simulation_monitor_t
{
    ldndc_simulation_monitor_t( void *);
    ~ldndc_simulation_monitor_t();

    int  bind();

    int  serve();
    int  process_message( char *);

    private:
        void *  mq_context;
        void *  mq_socket;

        void *  data;
};


#endif  /*  LDNDC_PROCMONITOR_LIB_H_  */

