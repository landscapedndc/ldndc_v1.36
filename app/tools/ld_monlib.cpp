/*!
 * @brief
 *    ldndc process monitor library
 *    (implementation)
 *
 * @author
 *    steffen klatt (created on: mar 23, 2015)
 */

#include  "app/tools/ld_monlib.h"
#include  <zmq.h>


ldndc_simulation_monitor_t::ldndc_simulation_monitor_t(
		void *  _data) : data( _data)
{
    this->mq_context = zmq_ctx_new();
    this->mq_socket = zmq_socket( this->mq_context, ZMQ_REP);
    if ( !this->mq_context || !this->mq_socket)
    {
	    fprintf( stderr, "not ");
    }
    fprintf( stderr, "connected..\n");
}

ldndc_simulation_monitor_t::~ldndc_simulation_monitor_t()
{
    if ( this->mq_socket)
    {
	zmq_close( this->mq_socket);
    }
    if ( this->mq_context)
    {
	zmq_ctx_destroy( this->mq_context);
    }
}

int
ldndc_simulation_monitor_t::bind()
{
    int  rc = -1;
    if ( this->mq_context && this->mq_socket)
    {
	fprintf( stderr, "binding to address..\n");
        rc = zmq_bind( this->mq_socket, simmonitor_addr);
        if ( rc)
        {
            zmq_close( this->mq_socket);
            this->mq_socket = NULL;
            zmq_ctx_destroy( this->mq_context);
            this->mq_context = NULL;
        }
    }

    return  rc == 0 ? 0 : -1;
}

#include  "string/lstring-basic.h"
#include  "string/lstring-compare.h"
static char *
zmq_receive_request( void *  _socket)
{
#define  MSGSIZE  511
    char  buffer[MSGSIZE+1];
    int  size = zmq_recv( _socket, buffer, MSGSIZE, 0);
    if ( size == -1)
    {
        return  NULL;
    }
    if ( size > MSGSIZE)
    {
        size = MSGSIZE;
    }
    buffer[size] = 0;
    return  cbm::strdup( buffer);
}
int
zmq_send_reply( void *  _socket, char const *  _reply)
{
    int  size = zmq_send(
            _socket, _reply, cbm::strlen( _reply), 0);
    return  size;
}


int
ldndc_simulation_monitor_t::serve()
{
    fprintf( stderr, "waiting for requests..\n");
    int  rc = 0;
    while ( rc == 0)
    {
        char *  msg =
            zmq_receive_request( this->mq_socket);
        if ( msg)
        {
            rc = this->process_message( msg);
	    free( msg);
        }
        else
        {
            rc = -1;
        }
    }

    return  ( rc == 0 || rc == 100 /*terminate*/) ? 0 : -1;
}

int
ldndc_simulation_monitor_t::process_message(
        char *  _msg)
{
    fprintf( stderr, "received request: %s\n", _msg);
    char  reply_buf[8096];
    reply_buf[0] = '.';
    reply_buf[1] = '\0';

    if ( cbm::is_equal( _msg, "bye"))
    {
	zmq_send_reply( this->mq_socket, "ok");
        return  100;
    }
    else if ( cbm::is_equal( _msg, "print_x"))
    {
	fprintf( stdout, "x=%f\n", *(double*)this->data);
	cbm::snprintf( reply_buf, 8095, "%.12f\n", *(double*)this->data);
    }
    zmq_send_reply( this->mq_socket, reply_buf);
    return  0;
}

