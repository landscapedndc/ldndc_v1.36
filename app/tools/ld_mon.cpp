/*!
 * @brief
 *    ldndc process monitor test front-end
 *
 * @author
 *    steffen klatt (created on: mar 23, 2015)
 */

#include  "app/tools/ld_monlib.h"

#include  <math/lmath-basic.h>

#include  <pthread.h>
#include  <unistd.h>

double  x;


int
lmon_simulation_monitor_test()
{
    ldndc_simulation_monitor_t  smon( &x);
    int  rc_bind = smon.bind();
    if ( rc_bind)
    {
        return  -1;
    }

    int  rc_serve = smon.serve();
    return  rc_serve ? -2 : 0;
}

void *
lmon_simulation_monitor( void *)
{
	lmon_simulation_monitor_test();
	return  NULL;
}
void *
lmon_dummy_model( void *  _x0)
{
	x = *(int*)_x0;
	int  c = 0;
	while ( c++ < 37)
	{
		x = 100.0 * sin( x);
		printf( "-- x=%f\n", x);
		sleep( 1);
	}
	return  NULL;
}

int
main( int  argc, char **  argv)
{
	int  rc = 0;

	pthread_t  model_thread;
	pthread_t  monitor_thread;

	double  x0 = ( 3.141/2.0) * 3.0;
	pthread_create( &model_thread, NULL, lmon_dummy_model, &x0);
	pthread_create( &monitor_thread, NULL, lmon_simulation_monitor, NULL);

	pthread_join( model_thread, NULL);
	pthread_join( monitor_thread, NULL);

//    int  rc = ldndc_simulation_monitor_test();

    return  ( rc == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}

