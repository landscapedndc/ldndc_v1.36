/*!
 * @brief
 *    write project meta information
 *
 * @author
 *    steffen klatt (created on: apr 27, 2015)
 */

#include  "ldndc.h"
#include  "ldndc-version.h"

#include  <prjfile/cbm_prjfile.h>
#include  <string/cbm_string.h>
#include  <time/cbm_time.h>
#include  <utils/cbm_utils.h>

#include  <stdlib.h>
#include  <stdio.h>

lerr_t
ldndc_projectinfo_read(
        char const *  _project_fname, ldndc::project_file_t *  _project_file)
{
    if ( !_project_file || !_project_fname
            || cbm::is_empty( _project_fname))
    {
        return  LDNDC_ERR_OK;
    }

    std::string  project_fname =
            cbm::format_expand( _project_fname);
    if ( project_fname.empty())
    {
        return  LDNDC_ERR_INVALID_ARGUMENT;
    }

    lerr_t  rc_pfread =
            _project_file->read( project_fname.c_str());
    RETURN_IF_NOT_OK(rc_pfread);

    return  LDNDC_ERR_OK;
}

struct  ldndc_projectinfo_args_t
{
    ldndc_projectinfo_args_t() { flags.all = 0; }
    union  projectinfo_flags
    {
        struct  flags_t
        {
            bool  write_help:1;

            bool  write_info_creator:1;
            bool  write_info_name:1;

            bool  write_schedule:1;
            bool  write_schedule_from:1;
            bool  write_schedule_to:1;
            bool  write_sources:1;
            bool  write_source_prefix:1;
            bool  write_sinks:1;
            bool  write_sink_prefix:1;
        }  flag;
        int  all;
    };
    projectinfo_flags  flags;
};

int
ldndc_projectinfo_parseargs(
        ldndc_projectinfo_args_t *  _args,
        int  _n_clargs, char **  _clargs)
{
    _args->flags.all = 0;
    int  c_pfname = -1;
    int  c = 1;
    while ( c < _n_clargs)
    {
        if ( cbm::is_equal( _clargs[c], "--all"))
        {
            _args->flags.all = ~0;
        }
        else if ( cbm::is_equal( _clargs[c], "--schedule"))
        {
            _args->flags.flag.write_schedule = 1;
        }
        else if ( cbm::is_equal( _clargs[c], "--schedule-from"))
        {
            _args->flags.flag.write_schedule_from = 1;
        }
        else if ( cbm::is_equal( _clargs[c], "--schedule-to"))
        {
            _args->flags.flag.write_schedule_to = 1;
        }
// sk:todo        else if ( cbm::is_equal( _clargs[c], "--sources"))
// sk:todo        {
// sk:todo            _args->flags.flag.write_sources = 1;
// sk:todo        }
// sk:todo        else if ( cbm::has_prefix( _clargs[c], "--source="))
// sk:todo        {
// sk:todo            /* TODO  enqueue source identifier */
// sk:todo        }
// sk:todo        else if ( cbm::is_equal( _clargs[c], "--source-prefix"))
// sk:todo        {
// sk:todo            _args->flags.flag.write_source_prefix = 1;
// sk:todo        }
// sk:todo        else if ( cbm::is_equal( _clargs[c], "--sinks"))
// sk:todo        {
// sk:todo            _args->flags.flag.write_sinks = 1;
// sk:todo        }
// sk:todo        else if ( cbm::has_prefix( _clargs[c], "--sink="))
// sk:todo        {
// sk:todo            /* TODO  enqueue sink identifier */
// sk:todo        }
// sk:todo        else if ( cbm::is_equal( _clargs[c], "--sink-prefix"))
// sk:todo        {
// sk:todo            _args->flags.flag.write_sink_prefix = 1;
// sk:todo        }
        else if ( cbm::is_equal( _clargs[c], "--meta"))
        {
            _args->flags.flag.write_info_creator = 1;
            _args->flags.flag.write_info_name = 1;
        }
        else if ( cbm::is_equal( _clargs[c], "--creator"))
        {
            _args->flags.flag.write_info_creator = 1;
        }
        else if ( cbm::is_equal( _clargs[c], "--name"))
        {
            _args->flags.flag.write_info_name = 1;
        }
        else if ( cbm::is_equal( _clargs[c], "--help")
                || cbm::is_equal( _clargs[c], "-h"))
        {
            _args->flags.flag.write_help = 1;
        }
        else
        {
            if ( c_pfname == -1)
            {
                c_pfname = c;
            }
            else
            {
                fprintf( stderr, "dropped input file  [%s]\n", _clargs[c]);
            }
        }
        ++c;
    }
    if ( _args->flags.all == 0)
    {
        /* if nothing was selected, select all */
        _args->flags.all = ~0;
    }
    return  c_pfname;
}

int
ldndc_projectinfo_write(
        ldndc_projectinfo_args_t const *  _args,
        ldndc::project_file_t const *  _project_file)
{
    if ( _args->flags.flag.write_schedule)
    {
        ltime_t  schedule;
        if ( cbm::is_empty( _project_file->schedule()))
        {
            printf( "schedule:\n");
        }
        else if ( schedule.from_string(
                _project_file->schedule()) != LDNDC_ERR_OK)
        {
            printf( "schedule: INVALID\n");
        }
        else
        {
            printf( "schedule: %s\n", schedule.to_string().c_str());
        }
    }
    if ( _args->flags.flag.write_schedule_from)
    {
        ltime_t  schedule;
        if ( cbm::is_empty( _project_file->schedule()))
        {
            printf( "schedule-from:\n");
        }
        else if ( schedule.from_string(
                _project_file->schedule()) != LDNDC_ERR_OK)
        {
            printf( "schedule-from: INVALID");
        }
        else
        {
            printf( "schedule-from: %s\n", schedule.from().to_string().c_str());
        }
    }
    if ( _args->flags.flag.write_schedule_to)
    {
        ltime_t  schedule;
        if ( cbm::is_empty( _project_file->schedule()))
        {
            printf( "schedule-to:\n");
        }
        else if ( schedule.from_string(
                _project_file->schedule()) != LDNDC_ERR_OK)
        {
            printf( "schedule-to: INVALID");
        }
        else
        {
            printf( "schedule-to: %s\n", schedule.to().to_string().c_str());
        }
    }
    if ( _args->flags.flag.write_info_creator)
    {
        ldndc::project_info_t  project_info;
        _project_file->project_info( &project_info);
        printf( "creator: %s\n", project_info.project_creator.c_str());
    }
    if ( _args->flags.flag.write_info_name)
    {
        ldndc::project_info_t  project_info;
        _project_file->project_info( &project_info);
        printf( "name: %s\n", project_info.project_name.c_str());
    }

    return  0;
}

int
ldndc_projectinfo_usage( int  _status)
{
    fprintf( stderr, "NAME\n  ldndc-project-info -\n    "
                "extract information from %s project file\n", ldndc_package_name());
    fprintf( stderr, "SYNOPSIS\n  ldndc-project-info [-h|--help] [--all] [--schedule | --schedule-from --schedule-to]\n    "
                "[--meta | --creator --name] PROJECTFILE\n");
    return  _status;
}

int
main( int  argc, char *  argv[])
{
    LD_DefaultLogger->set_silent();

    int  rc = -1;
    if ( argc > 1)
    {
        ldndc::project_file_t  project_file;

        ldndc_projectinfo_args_t  args;
        int  project_fname_at =
                ldndc_projectinfo_parseargs( &args, argc, argv);
        if ( project_fname_at != -1)
        {
            rc = ldndc_projectinfo_read(
                        argv[project_fname_at], &project_file);
            if ( !rc)
            {
                if ( args.flags.flag.write_help)
                {
                    rc = ldndc_projectinfo_usage( 0);
                }
                else
                {
                    rc = ldndc_projectinfo_write(
                        &args, &project_file);
                }
            }
            else
            {
                fprintf( stderr, "failed to read project file\n");
                rc = ldndc_projectinfo_usage( -1);
            }
        }
        else
        {
            rc = 0;
            if ( !args.flags.flag.write_help)
            {
                fprintf( stderr, "no input file\n");
                rc = -1;
            }
            rc = ldndc_projectinfo_usage( rc);
        }
    }
    else
    {
        rc = ldndc_projectinfo_usage( -1);
    }

    return  rc ? EXIT_FAILURE : EXIT_SUCCESS;
}

