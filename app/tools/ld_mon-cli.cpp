/*!
 * @brief
 *    ldndc process monitor client test front-end
 *
 * @author
 *    steffen klatt (created on: mar 23, 2015)
 */

#include  <string/cbm_string.h>

#include  <zmq.h>
#define  simmonitor_addr "tcp://localhost:8001"

static int
zmq_send_request( void *  _socket, char const *  _request)
{
    int  size = zmq_send(
            _socket, _request, cbm::strlen( _request), 0);
    return  size;
}
static char *
zmq_receive_reply( void *  _socket)
{
#define  MSGSIZE  8095
    char  buffer[MSGSIZE+1];
    int  size = zmq_recv( _socket, buffer, MSGSIZE, 0);
    if ( size == -1)
    {
        return  NULL;
    }
    if ( size > MSGSIZE)
    {
        size = MSGSIZE;
    }
    buffer[size] = 0;
    return  cbm::strdup( buffer);
}


int 
lmoncli_sendrequest( char const *  _request)
{
    void *  mq_context = zmq_ctx_new();
    void *  mq_socket = zmq_socket( mq_context, ZMQ_REQ);

    int  rc = -1;
    if ( mq_context && mq_socket)
    {
        fprintf( stderr, "connecting..\n");
        int  rc_conn = zmq_connect( mq_socket, simmonitor_addr);
        if ( rc_conn == 0)
        {
            rc = zmq_send_request( mq_socket, _request);
            fprintf( stderr, "send request..\n");
            char *  msg = zmq_receive_reply( mq_socket);
            if ( msg)
            {
                fprintf( stdout, "server said: %s\n", msg);
                free( msg);
            }
        }
    }

    zmq_close( mq_socket);
    zmq_ctx_destroy( mq_context);

    return  rc;
}

int
main(
        int  argc,
        char **  argv)
{
    int  rc = 0;
    if ( argc > 1)
    {
        rc = lmoncli_sendrequest( argv[1]);
    }
    else
    {
        fprintf( stderr, "require command string\n");
        rc = -1;
    }
    return  ( rc == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}

