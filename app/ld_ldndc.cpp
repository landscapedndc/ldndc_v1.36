/*!
 * @author
 *    Steffen Klatt (created on: nov 17, 2016)
 */

//#define  LDNDC_MPI_ATTACHDEBUGGER

#include  <stdio.h>

#include  "ldndc-version.h"
#include  "app/ld_opt.h"
#include  "app/ld_init.h"
#include  "app/ld_sim.h"

#include  "kernels/ld_kernels.h"

#include  <crabmeat-version.h>
#include  <cbm_rtcfg.h>
#include  <utils/cbm_utils.h>
#include  <mpi/cbm_mpi.h>

#ifdef  LDNDC_MPI
#  define  LDNDC_EXIT_SUCCESS EXIT_SUCCESS
#  define  LDNDC_EXIT_FAILURE EXIT_SUCCESS
#else
#  define  LDNDC_EXIT_SUCCESS EXIT_SUCCESS
#  define  LDNDC_EXIT_FAILURE EXIT_FAILURE
#endif

#ifdef  LDNDC_MPI
#  include  <dispatcher/cbm_dispatcher.h>
#  include  <dispatcher/cbm_dispatchers.h>
#else
namespace cbm 
{
    class  work_dispatcher_t;
}
#endif

extern void ldndc_log_status( lerr_t /*return code*/);
extern void ldndc_dump_resources();

ldndc::llandscapedndc_t *  ldndc_sim = 0;
char const *  ldndc_cfg_fname = 0; /*configuration filename*/
char const *  ldndc_lpf_fname = 0; /*project filename*/
char const *  ldndc_schedule = 0;  /*simulation time*/
char const *  ldndc_dispatch = 0;  /*kernel dispatch strategy*/
cbm::work_dispatcher_t *  ldndc_dispatcher = 0; /*kernel dispatcher*/
int  ldndc_show_stats = 0;

cbm::td_scalar_t  ldndc_sse_launch_end = 0;

#ifdef  LDNDC_MPI
char const * const  ldndc_default_dispatch = "roundrobin";
#else
char const * const  ldndc_default_dispatch = 0;
#endif

ldndc_args_info_t  ldndc_cargs; /*command line arguments*/
struct ldndc_cmdline_parser_params *  ldndc_parser_params = 0;

#ifdef  LDNDC_PTHREADS
#  include  <pthread.h>
#  ifdef  _LDNDC_HAVE_ONLINECONTROL
#    include  <comm/lmessage.h>
#    include  <comm/lcomponent.h>
#  endif
int  rc_monitor;
void *
ldndc_run_monitor( void *)
{
#ifdef  _LDNDC_HAVE_ONLINECONTROL
    fprintf( stderr, "monitor thread :-)\n");
    rc_monitor = LDNDC_ERR_OK;
#else
    fprintf( stderr, "monitor thread :-(\n");
#endif
    return  &rc_monitor;
}
int  rc_simulator;
void *
ldndc_run_simulator( void *  _ldndc)
{
    fprintf( stderr, "simulator thread :-)\n");
    rc_simulator = ((ldndc::llandscapedndc_t *)_ldndc)->launch();
    return  &rc_simulator;
}
#endif /* LDNDC_PTHREADS */


lerr_t
ldndc_launch( ldndc::llandscapedndc_t * _ldndc)
{
#ifdef  LDNDC_PTHREADS
	pthread_t  monitor_thread;
	int  rc_tmon = pthread_create(
            &monitor_thread, NULL, ldndc_run_monitor, _ldndc);

	pthread_t  simulator_thread;
	int  rc_tsim = pthread_create(
            &simulator_thread, NULL, ldndc_run_simulator, _ldndc);

	lerr_t *  rc_mon = NULL;
    if ( rc_tmon == 0)
    {
        pthread_join( monitor_thread, (void**)&rc_mon);
    }
	lerr_t *  rc_sim = NULL;
    if ( rc_tsim == 0)
    {
        pthread_join( simulator_thread, (void**)&rc_sim);
    }

    return  rc_sim ? *rc_sim : LDNDC_ERR_FAIL;
#else
    return  _ldndc->launch( ldndc_sse_launch_end);
#endif  /* LDNDC_PTHREADS */
}


#if  defined(LDNDC_MPI) && defined(_DEBUG)
void  ldndc_attach_debugger()
{
    char  hostname[256];
    gethostname( hostname, sizeof(hostname));
#ifdef  LDNDC_MPI_ATTACHDEBUGGER
    fprintf( stderr, "PID %d on %s ready for debugger attach\n", getpid(), hostname);
    fflush( stderr);
    //int  i = 0;
    //while ( 0 == i)
        { sleep( 10); }
#else
    fprintf( stderr, "PID %d on %s debugger attach inactive\n", getpid(), hostname);
    fflush( stderr);
#endif
}
#endif


void
ldndc_log_status( lerr_t  _rc)
{
    std::string  messages_text;
    int  n_criticalmessages = 0;
    int  n_errors = LD_DefaultLogger->messages_count( LOGLEVEL_ERROR);
    if (( n_errors > 0) &&
        LD_DefaultLogger->log_level() >= LOGLEVEL_ERROR)
    {
        messages_text += messages_text.empty() ? "" : " and ";
        messages_text += cbm::n2s( n_errors) + " error";
    }
    else
    {
        n_errors = 0;
    }
    n_criticalmessages += n_errors;

    int  n_warnings = LD_DefaultLogger->messages_count( LOGLEVEL_WARN);
    if (( n_warnings > 0) &&
        LD_DefaultLogger->log_level() >= LOGLEVEL_WARN)
    {
        messages_text += messages_text.empty() ? "" : " and ";
        messages_text += cbm::n2s( n_warnings) + " warning";
    }
    else
    {
        n_warnings = 0;
    }
    n_criticalmessages += n_warnings;

    if ( n_criticalmessages > 0)
    {
        std::string  waswere = "was";
        messages_text += " message";
        if ( n_criticalmessages != 1)
        {
            waswere = "were";
            messages_text += 's';
        }

        int const  n_messages =
            LD_DefaultLogger->messages_count();
        messages_text += " (";
        messages_text += cbm::n2s( n_messages) + " total";
        messages_text += ")";

        LOGWRITE_( "there ", waswere, " ", messages_text);
        fprintf( stderr, "node %4d: there %s %s\n",
            cbm::mpi::comm_rank( cbm::mpi::comm_ldndc),
            waswere.c_str(), messages_text.c_str());
    }

    LOGWRITE_( "node ", cbm::mpi::comm_rank( cbm::mpi::comm_ldndc),
        (( _rc) ? ": premature termination or incomplete run" : ": normal termination"), ".");
}


#include  <kernel/kstats.h>
void ldndc_dump_resources()
{
    cbm::cputime_summary_t cs;
    int c_nb_kernels = ldndc_sim->cputime_summary( &cs);
    if ( c_nb_kernels == 0)
        { return; }

    cbm::walltime_summary_t ws;
    int w_nb_kernels = ldndc_sim->walltime_summary( &ws);
    if ( w_nb_kernels != c_nb_kernels)
        { return; }

    cbm::kernels_stats_t kstats;
    int r_nb_kernels = ldndc_sim->kernel_resource_stats( kstats);
    if ( r_nb_kernels == w_nb_kernels && cs.nb > 0)
    {
        for ( int k = 0; k < r_nb_kernels; ++k)
        {
            cbm::kernel_stats_t const & Ks = kstats[k];
            fprintf( stderr, "kernel %7d:\tid=" "%d"
                     "\tmin=" "%.6f" "s\tmax=" "%.6f" "s\tavg=" "%.6f" "s\ttotal=" "%.2fs (%0.2f%%)"
                     " %s (%d)"
                     "\n",
                     k, Ks.id,
                     Ks.cpumin, Ks.cpumax, Ks.cpuavg, Ks.cputotal,
                     100.0*Ks.cputotal/cs.total,
                     Ks.modelname.c_str(), Ks.rank);
        }
    }

    if ( cs.nb > 0)
    {
        fprintf( stderr, "====================\n");
        fprintf( stderr, "landscape:"  "\tmin=" "%.6fs[%u]" "\tmax=" "%.6fs[%u]" "\tavg=" "%.6f" "s"
                      "  (total=%.2fs/%.2fs[%u/%u])\n",
            cs.min, (unsigned int)cs.kmin, cs.max, (unsigned int)cs.kmax, cs.total/cs.nb,
            cs.total, ws.total, (unsigned int)ws.kmin, (unsigned int)ws.kmax);
    }

    cbm::memory_summary_t  ms;
    ldndc_sim->memory_summary( &ms);

    fprintf( stderr, "====================\n");
    fprintf( stderr,
            "%20s  %.4fKB\n%20s  %.4f\n%20s  %.4f\n",
            "total program size", ms.prog_sz,
            "library", ms.lib_sz,
            "data/stack", ms.data_sz);
}


int
ldndc_query_features( ldndc_args_info_t const *  _args_info)
{
    int  q = 0;
    if ( _args_info->list_build_options_flag)
    {
        fprintf( stderr, "### ldndc {revision %s}\n%s\n\n",
                 ldndc_package_revision(),
                 ldndc_package_build_configuration());
        fprintf( stderr, "### crabmeat {revision %s}\n%s\n",
                 crabmeat_package_revision(),
                 crabmeat_package_build_configuration());
        ++q;
    }
    if ( _args_info->list_mobile_modules_flag)
    {
        int  rc_query = ldndc_query_kernel( NULL, _LD_MoBiLE, "show-available-models");
        if ( rc_query)
        {
            fprintf( stderr, "Error occured when querying"
                     " available MoBiLE modules\n");
        }
        ++q;
    }

    if ( _args_info->list_kernels_flag)
    {
#ifdef  LDNDC_KERNELS
        ldndc_list_available_kernels( _args_info->verbose_given);
#else
        fprintf( stderr, "no kernels available"
                " due to kernels being disabled.\n");
#endif  /*  LDNDC_KERNELS  */
        ++q;
    }

    if ( _args_info->list_dispatch_flag)
    {
#ifdef  LDNDC_MPI
        cbm::work_dispatcher_factory_t::list_available();
#else
        fprintf( stderr, "work dispatchers not enabled in serial build.\n");
#endif
        ++q;
    }

    if ( _args_info->show_stats_flag)
        { ldndc_show_stats = 1; }

    return q;
}


lerr_t
ldndc_checkpoint( ldndc_args_info_t const *  _args_info)
{
    if ( _args_info->create_checkpoint_given)
    {
#ifdef  _HAVE_SERIALIZE
        ltime_t  cptime;
        if ( !cbm::is_equal( _args_info->create_checkpoint_arg, ""))
        {
            lerr_t  rc_cptime = cptime.from_string(
                    _args_info->create_checkpoint_arg);
            if ( rc_cptime)
                { return  LDNDC_ERR_FAIL; }
            fprintf( stderr, "checkpoints are currently only created at"
                " the end of the simulation. time will be ignored for now.\n");
        }
        LD_RtCfg.io.create_checkpoint = true;
        LD_RtCfg.io.create_date = cptime.from().to_string();
#else
        fprintf( stderr, "checkpointing support disabled; option '--create-checkpoint' ignored\n");
#endif  /*  _HAVE_SERIALIZE  */
    }
    if ( _args_info->restore_checkpoint_given)
    {
#ifdef  _HAVE_SERIALIZE
        ltime_t  cptime;
        lerr_t  rc_cptime = cptime.from_string(
                _args_info->restore_checkpoint_arg);
        if ( rc_cptime)
                { return  LDNDC_ERR_FAIL; }
        LD_RtCfg.io.restore_checkpoint = true;
        LD_RtCfg.io.restore_date = cptime.from().to_string();
#else
        fprintf( stderr, "checkpointing support disabled; option '--restore-checkpoint' ignored\n");
#endif  /*  _HAVE_SERIALIZE  */
    }
    return  LDNDC_ERR_OK;
}


#include  <utils/signal/cbm_signal.h>
#include  <utils/ieee/cbm_ieee.h>

/*!
 * @brief
 * - Read command line arguments
 * - Initialize logger
 * - Initialize MPI
 * - ...
 */
lerr_t
ldndc_sim_initialize( int  _argc, char **  _argv)
{
    (void)ldndc_ieee_set_mode( LDNDC_FE_DIVBYZERO|LDNDC_FE_OVERFLOW|/*LDNDC_FE_UNDERFLOW|*/LDNDC_FE_INVALID);

    (void)ldndc_signal_traps_init();
    (void)ldndc_have_signal_trap( LDNDC_SIGFPE);
    (void)ldndc_have_signal_trap( LDNDC_SIGINT);
    (void)ldndc_have_signal_trap( LDNDC_SIGSEGV);
    (void)ldndc_have_signal_trap( LDNDC_SIGUSR1);

    ldndc_parser_params = ldndc_cmdline_parser_params_create();

    ldndc_cmdline_parser_params_init( ldndc_parser_params);
    ldndc_cmdline_parser_init( &ldndc_cargs);

    if ( ldndc_cmdline_parser_ext( _argc, _argv, &ldndc_cargs, ldndc_parser_params) != 0)
    {
        fprintf( stderr, "error parsing command line options\n");
        return LDNDC_ERR_FAIL;
    }

    int  can_exit = ldndc_query_features( &ldndc_cargs);
    if ( can_exit)
        { return LDNDC_ERR_NONE; }

    if ( ldndc_cargs.define_given)
    {
        lerr_t  rc_define = ldndc_define_variables( ldndc_cargs.define_arg, ldndc_cargs.define_given);
        if ( rc_define)
            { return LDNDC_ERR_FAIL; }
    }

    /* initialize crabmeat library (requires dotpath) */
    int  rc_cbm = ldndc_initialize( _argc, _argv);
    if ( rc_cbm)
    {
        fprintf( stderr, "CrabMEAT initialization failed\n");
        exit( 23);
    }

    if ( ldndc_cargs.log_file_given)
    {
        cbm::setenv( LDNDC_ENV_DEFAULT_LOGFILE, ldndc_cargs.log_file_arg, 1);
    }
    if ( ldndc_cargs.input_path_given)
    {
        cbm::setenv( LDNDC_ENV_INPUT_PATH, ldndc_cargs.input_path_arg, 1);
    }
    if ( ldndc_cargs.output_path_given)
    {
        cbm::setenv( LDNDC_ENV_OUTPUT_PATH, ldndc_cargs.output_path_arg, 1);
    }
    if ( ldndc_cargs.resources_path_given)
    {
        cbm::setenv( LDNDC_ENV_RESOURCES_PATH, ldndc_cargs.resources_path_arg, 1);
    }
    if ( ldndc_cargs.scenario_given)
    {
        LD_RtCfg.pc.scenario_name = ldndc_cargs.scenario_arg;
    }

    lerr_t  rc_checkpoint = ldndc_checkpoint( &ldndc_cargs);
    if ( rc_checkpoint)
        { return LDNDC_ERR_FAIL; }

    ldndc_cfg_fname = NULL;
    if ( ldndc_cargs.config_given)
        { ldndc_cfg_fname = ldndc_cargs.config_arg; }
    ldndc_lpf_fname = NULL;
    if ( ldndc_cargs.inputs && ldndc_cargs.inputs[0])
        { ldndc_lpf_fname = ldndc_cargs.inputs[0]; }
    ldndc_schedule = NULL;
    if ( ldndc_cargs.schedule_given)
        { ldndc_schedule = ldndc_cargs.schedule_arg; }
    ldndc_dispatch = ldndc_default_dispatch;
    if ( ldndc_cargs.dispatch_given) 
        { ldndc_dispatch = ldndc_cargs.dispatch_arg; }

    return LDNDC_ERR_OK;
}


/*!
 * @brief
 * - Initialize crabmeat domain library
 * - ...
 */
lerr_t
ldndc_sim_create()
{
#if  defined(LDNDC_MPI) && defined(_DEBUG)
    ldndc_attach_debugger();
#endif

    ldndc_sim = new ldndc::llandscapedndc_t();
    if ( !ldndc_sim)
        { return  LDNDC_ERR_NOMEM; }

#ifdef  LDNDC_MPI
    ldndc_dispatcher = cbm::work_dispatcher_factory_t::new_dispatcher( ldndc_dispatch,
                                                                       cbm::mpi::comm_ldndc,
                                                                       0);
    if ( !ldndc_dispatcher)
    {
        fprintf( stderr, "node %4d: Failed to construct dispatcher '%s'\n",
                 (int)cbm::mpi::comm_rank( cbm::mpi::comm_ldndc), ldndc_dispatch);
        return  LDNDC_ERR_FAIL;
    }
#else
    ldndc_dispatcher = NULL;
#endif

    int const  ldndc_status = ldndc_sim->initialize( ldndc_lpf_fname, ldndc_schedule,
                                                     &ldndc_cargs, ldndc_cfg_fname, ldndc_dispatcher);
    int const  global_status = cbm::mpi::status( ldndc_status, cbm::mpi::comm_ldndc);
    if ( ldndc_status || global_status)
    {
        if ( ldndc_status)
        {
            fprintf( stderr, "node %4d: Setting up ldndc instance failed with status %d%cError: \"%s\"\n",
                     (int)cbm::mpi::comm_rank( cbm::mpi::comm_ldndc),
                     (int)ldndc_status,
                     cbm::mpi::have_mpi() ? '\t' : '\n', ldndc_sim->get_status_message());

#if !defined(_DEBUG) && !defined(LDNDC_MPI)
            ldndc::explain_error( ldndc_status);
#endif
        }
        else if ( global_status && !ldndc_status)
        {
            fprintf( stderr, "node %4d: Setting up ldndc instance failed on other nodes\n",
                     cbm::mpi::comm_rank( cbm::mpi::comm_ldndc));
        }
        return  LDNDC_ERR_FAIL;
    }
    LOGWRITE( "clk=",LD_RtCfg.clk->to_string(), "  clock=",LD_RtCfg.clk->sse(), "  dt=",LD_RtCfg.clk->dt() );
    return  LDNDC_ERR_OK;
}


lerr_t
ldndc_sim_launch( cbm::td_scalar_t _sse_end)
{
    CBM_Assert( ldndc_sim);
    cbm::mpi::barrier( cbm::mpi::comm_ldndc);

    ldndc_sse_launch_end = _sse_end;

    lerr_t  rc_launch = ldndc_launch( ldndc_sim);
    return  rc_launch;
}


void
ldndc_sim_destroy( lerr_t  _rc)
{
    if ( !ldndc_sim)
        { return; }

    ldndc_sim->finalize( &ldndc_cargs);

#ifdef  LDNDC_MPI
    /* free dispatcher */
    if ( ldndc_dispatcher)
        { cbm::work_dispatcher_factory_t::free_dispatcher( ldndc_dispatcher); }
#endif

    ldndc_log_status( _rc);

    if ( _rc == LDNDC_ERR_OK && ldndc_show_stats
            && ldndc_sim->have_kernel_resource_statistics())
        { ldndc_dump_resources(); }

    ldndc_sim->shutdown_log();
    /* at this point the logging system has been deinitialized: do not use it */

    delete  ldndc_sim;
    ldndc_sim = 0;
}


void
ldndc_sim_finalize()
{
    ldndc_finalize();

    ldndc_cmdline_parser_free( &ldndc_cargs);
    free( ldndc_parser_params);
}


int
ldndc_kickstart( int  _argc, char **  _argv)
{
    lerr_t rc = ldndc_sim_initialize( _argc, _argv);
    if ( rc == LDNDC_ERR_OK)
    {
        rc = ldndc_sim_create();
        if ( rc == LDNDC_ERR_OK)
        {
            rc = ldndc_sim_launch( 0 );
        }
        ldndc_sim_destroy( rc);
    } 
    else if ( rc == LDNDC_ERR_NONE)
    {
        return LDNDC_EXIT_SUCCESS;
    }
    ldndc_sim_finalize();

    fprintf( stderr, "node %4d: %s.\n",
             cbm::mpi::comm_rank( cbm::mpi::comm_ldndc),
             ( rc) ? "premature termination or incomplete run" : "normal termination");

    if ( rc)
    { return  LDNDC_EXIT_FAILURE; }

    return  LDNDC_EXIT_SUCCESS;
}

