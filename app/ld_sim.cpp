/*!
 * @brief
 *
 * @author
 *    steffen klatt (created on: apr 02, 2014),
 *    edwin haas
 */

#include  "app/ld_sim.h"
#include  "app/ld_init.h"
#include  "app/ld_opt.h"

#include  <cbm_rtcfg.h>
#include  <math/cbm_math.h>
#include  <resources/Lresources.h>
#include  <cfgfile/cbm_cfgfile.h>
#include  <prjfile/cbm_prjfile.h>

#include  <io-dcomm.h>
#include  <crabmeat-version.h>

char const * const  ldndc::llandscapedndc_status_msg[11] =
{
    "Ok",
    "Failed to read configuration",
    "Failed to initialize logging stream",
    "Failed to read project file",
    "Failed to read Lresources file",
    "Failed to read udunits",
    "Failed to open project",
    "Failed to initialize project",
    "Failed to restore checkpoint",
    "Failed to set schedule",
    "Not running required Package Version"
};

#include  "ldndc-version.h"
void
ldndc::explain_error( int  _status)
{
    static char const *  boxline =
        "==== HELP =============================================";
    fprintf( stderr, "\n%s\n", boxline);
    fprintf( stderr, "Rats! What happened now? See below for possible\nexplainations "
            "(You'll also find more detailed inform-\nation "
            "in the %s Users Guide v%s) \n\n",
            ldndc_package_name(), ldndc_package_version());
    if ( _status == ldndc::LLDNDC_STATUS_READCONFIGFAIL)
    {
        fprintf( stderr, "The attempt to find and read the configuration file\n"
                "failed." " This can have several reasons:"
                "\n    a) The configuration file does not exist"
                "\n    b) The configuration file is spelled incorrectly"
                "\n       (check command line option -c and `--config')"
                "\n    c) The configuration file contains syntax errors"
                "\n    d) You have insufficient read permissions"
                "\nPlease, check your configuration file.\n");
    }
    else if ( _status == ldndc::LLDNDC_STATUS_READPROJECTFAIL)
    {
        fprintf( stderr, "The attempt to find and read the project file\n"
                "failed." " This can have several reasons:"
                "\n    a) The project file does not exist"
                "\n    b) The project file is spelled incorrectly"
                "\n    c) The project file contains syntax errors"
                "\n    d) You have insufficient read permissions"
                "\nPlease, check your project file."
                " Consider using\nan XML editor with syntax highlighting\n"
                "and an XML validator (e.g., xmllint).\n");
    }
    else if ( _status == ldndc::LLDNDC_STATUS_READLRESOURCESFAIL)
    {
        fprintf( stderr, "The attempt to find and read the Lresources\n"
                "database failed." " This can have several reasons:"
                "\n    a) The Lresources database does not exist"
                "\n    b) The Lresources database is out-of-date"
                "\n    c) The Lresources database name is spelled"
                "\n       incorrectly   (e.g., check command line"
                "\n       option -R and `--resources-path')"
                "\n    d) Your configuration contains an incorrect"
                "\n       `resources_path' entry"
                "\n    e) The value of the environment variable"
                "\n       `" LDNDC_ENV_RESOURCES_PATH "' is incorrect"
                "\n    f) The Lresources database contains syntax errors"
                "\n    g) You have insufficient read permissions"
                "\nPlease, check those items.\n");
    }
    else if ( _status == ldndc::LLDNDC_STATUS_READUNITSFAIL)
    {
        fprintf( stderr, "The attempt to find and read the udunits\n"
                "database failed." " This can have several reasons:"
                "\n    a) The udunits database does not exist"
                "\n    b) Your configuration contains an incorrect"
                "\n       `udunits_path' entry"
                "\n    c) You have insufficient read permissions"
                "\nPlease, check those items.\n");
    }
    else if ( _status == ldndc::LLDNDC_STATUS_INITLOGFAIL)
    {
        fprintf( stderr, "The attempt to open and initialize the default\n"
                "logging sink failed." " This can have several reasons:"
                "\n    a) The logging file could not be created"
                "\n       (e.g., invalid filename)"
                "\n    b) The logging file is spelled incorrectly"
                "\n       (e.g., check command line option `--log-file')"
                "\n    c) Your configuration contains an incorrect"
                "\n       `log_file' entry"
                "\n    d) The value of the environment variable"
                "\n       `" LDNDC_ENV_DEFAULT_LOGFILE "' is incorrect"
                "\n    e) You ran out of disk storage"
                "\n    f) You have insufficient write permissions"
                "\nPlease, check your project file.\n");
    }
    else if ( _status == ldndc::LLDNDC_STATUS_SETSCHEDULEFAIL)
    {
        fprintf( stderr, "The simulation schedule could not be initialized."
                " " "This\ncan have several reasons:"
                "\n    a) The schedule was not found (e.g., it is missing"
                "\n       or incorrect spelled in project file)"
                "\n    b) The date contains invalid numbers (e.g., month>12)"
                "\n    c) The date is formed incorrectly"
                "\n    d) The date is outside supported boundaries (i.e.,"
                "\n       dates cannot be prior 1st of January, %d)"
                "\n\nPlease, make sure it has one of"
                " the forms:\n(most generic form, some items are optional)\n"
                "\n    YYYY-MM-DD-SS/R -> YYYY-MM-DD-SS (start -> end)"
                "\nor"
                "\n    YYYY-MM-DD-SS/R -> +YYYY-MM-DD-SS (start -> period)\n"
                "\nand contains valid values. For further information"
                "\nabout time specifications, "
                "see the %s Users\nGuide\n",

                1, ldndc_package_name());
    }
    else if ( _status == ldndc::LLDNDC_STATUS_OPENPROJECTFAIL)
    {
        fprintf( stderr, "The attempt to open and create"
                " the simulation\nproject failed."
                " This can have numerous reasons that\nneed to be"
                " investigated by consulting your\nproject's logging"
                " file. Some common mistakes are:"
                "\n    a) 1) One or more input sources do not exist"
                "\n       2) One or more output sinks cannot be created"
                "\n    b) One or more input sources or output sinks"
                "\n       are spelled incorrectly"
                "\n    c) One or more inputs contain syntax errors"
                "\n    d) Your project file contains incorrect"
                "\n       `source_prefix' or `sink_prefix' entries"
                "\n    e) Your configuration contains incorrect"
                "\n       `input_path' or `output_path' entries"
                "\n    f) The values of the environment variables"
                "\n       `LDNDC_ENV_INPUT_PATH' or `LDNDC_ENV_OUTPUT_PATH'"
                "\n       are incorrect"
                "\n    g) You requested an input reader or output writer"
                "\n       that was not included in your version of %s"
                "\n    h) You have insufficient read/write permissions"
                "\nPlease, consult your project's logging file!\n",

                ldndc_package_name());
    }
    else if ( _status == ldndc::LLDNDC_STATUS_INITPROJECTFAIL)
    {
        fprintf( stderr, "The attempt to initialize"
                " the simulation\nproject failed."
                " This can have numerous reasons that\nneed to be"
                " investigated by consulting your\nproject's logging"
                " file. Some common mistakes are:"
                "\n    a) 1) One or more inputs contain invalid data (e.g.,"
                "\n          misspelled parameter, landuse or module)"
                "\n       2) One or more inputs contain syntax errors"
                "\n    b) The Lresources database is out-of-date"
                "\n    c) You misspelled a source identifier (e.g., climate)"
                "\n       (check the sources block in the project file)"
                "\n    d) Using wrong IDs in `use' clauses"
                "\n    e) You requested a kernel or MoBiLE module that was"
                "\n       not included in your version of %s"
                "\n       (check by running `ldndc -k' and `ldndc -m')"
                "\nPlease, consult your project's logging file!\n",
                
                ldndc_package_name());
    }
// sk:todo    else if ( _status == ldndc::LLDNDC_STATUS_RESTORECHECKPOINTFAIL)
// sk:todo    {
// sk:todo    }
    else
    {
        fprintf( stderr, "No explaination available, yet;"
                " Please, consult the\n%s developers.\n", ldndc_package_name());
    }
    fprintf( stderr, "%s\n", boxline);
}


#include  <logging/cbm_logging.h>
void
ldndc::llandscapedndc_t::shutdown_log()
{
    CBM_RegisteredLoggers.shutdown();
}

ldndc::llandscapedndc_t::llandscapedndc_t()
        : m_status( LLDNDC_STATUS_OK)
    { }
ldndc::llandscapedndc_t::~llandscapedndc_t()
    { }

int
ldndc::llandscapedndc_t::initialize(
                                    char const *  _project_fname,
                                    char const *  _schedule,
                                    ldndc_args_info_t const * /*args*/,
                                    char const *  _config_fname,
                                    cbm::work_dispatcher_t *  _wd)
{
    if ( this->m_status == LLDNDC_STATUS_OK)
    {
        lerr_t const  rc_conf = this->read_configuration( _config_fname);
        if ( rc_conf)
            { this->set_status( LLDNDC_STATUS_READCONFIGFAIL); }
    }

    if ( this->m_status == LLDNDC_STATUS_OK)
    {
        lerr_t const  rc_proj = this->read_projectfile( _project_fname ? _project_fname : LD_RtCfg.cfg->project_file());
        if ( rc_proj)
            { this->set_status( LLDNDC_STATUS_READPROJECTFAIL); }
    }

    if ( this->m_status == LLDNDC_STATUS_OK)
    {
        lerr_t const  rc_log = this->initialize_log( LD_RtCfg.cfg);
        if ( rc_log)
            { this->set_status( LLDNDC_STATUS_INITLOGFAIL); }
    }

    if ( this->m_status == LLDNDC_STATUS_OK)
    {
        lerr_t const  rc_log = this->check_version_required();
        if ( rc_log)
            { this->set_status( LLDNDC_STATUS_VERSIONREQUIREDFAIL); }
    }

    if ( this->m_status == LLDNDC_STATUS_OK)
    {
        LOGWRITE( "==== project \"",LD_RtCfg.prj->name(),"\" ====");
        LOGWRITE( "running crabmeat library version ", crabmeat_package_version(),
                  " (", crabmeat_package_revision(),")");
    }
    cbm::random::seed( LD_RtCfg.cc.random_seed);

    if ( this->m_status == LLDNDC_STATUS_OK)
    {
        lerr_t const  rc_schedule = this->set_schedule( _schedule ? _schedule : LD_RtCfg.prj->schedule());
        if ( rc_schedule)
            { this->set_status( LLDNDC_STATUS_SETSCHEDULEFAIL); }
    }

    if ( this->m_status == LLDNDC_STATUS_OK)
    {
        lerr_t const  rc_lresources = this->read_Lresources((LD_RtCfg.io.resources_path+LDNDC_RESOURCES_FILENAME).c_str());
        if ( rc_lresources)
            { this->set_status( LLDNDC_STATUS_READLRESOURCESFAIL); }
    }

#ifdef  LDNDC_UNITS
    if ( this->m_status == LLDNDC_STATUS_OK)
    {
        cbm::string_t const  udunits_db = LD_RtCfg.io.udunits_path + "udunits2/udunits2.xml";
        int  rc_units = cbm_units_t::initialize( udunits_db.c_str());
        if ( rc_units)
            { this->set_status( LLDNDC_STATUS_READUNITSFAIL); }
    }
#endif

    cbm::io_dcomm_t *  io_dcomm = NULL;
    lid_t  ld_pid = invalid_lid;
    if ( this->m_status == LLDNDC_STATUS_OK)
    {
        io_dcomm = cbm::io_dcomm_t::new_instance();
        if ( !io_dcomm)
            { this->set_status( LLDNDC_STATUS_OPENPROJECTFAIL); }
        else
        {
            ld_pid = io_dcomm->open_project();
            if ( cbm::is_invalid( ld_pid))
                { this->set_status( LLDNDC_STATUS_OPENPROJECTFAIL); }
        }
        if ( io_dcomm && this->m_status != LLDNDC_STATUS_OK)
        {
            io_dcomm->delete_instance();
            io_dcomm = NULL;
        }
    }
    if ( this->m_status == LLDNDC_STATUS_OK)
    {
        ldndc_assert( io_dcomm);

        lerr_t  rc_projectinit = this->initialize_region( ld_pid, io_dcomm, _wd);
        if ( rc_projectinit)
            { this->set_status( LLDNDC_STATUS_INITPROJECTFAIL); }

#ifdef  _HAVE_SERIALIZE
        if ( LD_RtCfg.io.restore_checkpoint)
        {
            ltime_t  restore_date;
            restore_date.from_string( LD_RtCfg.io.restore_date);
            int  n_fails = this->restore_checkpoint( restore_date.from());
            LOGVERBOSE( "restoring checkpoint from ", restore_date, "  [#fails=",n_fails,"]");
            if ( n_fails)
            {
                this->set_status( LLDNDC_STATUS_RESTORECHECKPOINTFAIL);
            }
        }
#endif  /*  _HAVE_SERIALIZE  */
    }
    return  this->m_status;
}


void
ldndc::llandscapedndc_t::finalize( ldndc_args_info_t const *)
{
#ifdef  _HAVE_SERIALIZE
    if ( LD_RtCfg.io.create_checkpoint
        && this->m_status == LLDNDC_STATUS_OK)
    {
        this->create_checkpoint();
    }
#endif  /*  _HAVE_SERIALIZE  */
    this->m_domain.finalize();
}

size_t
ldndc::llandscapedndc_t::size() const
    { return  this->number_of_kernels(); }
size_t
ldndc::llandscapedndc_t::number_of_kernels() const
    { return  this->m_domain.number_of_kernels(); }
size_t
ldndc::llandscapedndc_t::number_of_valid_kernels() const
    { return  this->m_domain.number_of_valid_kernels(); }
size_t
ldndc::llandscapedndc_t::number_of_active_kernels() const
    { return  this->m_domain.number_of_alive_kernels(); }
cbm::sclock_t const *
ldndc::llandscapedndc_t::lclock() const
    { return LD_RtCfg.clk; }

lerr_t
ldndc::llandscapedndc_t::read_configuration( char const *  _config_fname)
{
    char const *  config_fname =
        ( _config_fname && !cbm::is_empty( _config_fname)) ? _config_fname : NULL;
    lerr_t  rc_conf = ldndc_open_configuration( config_fname);
    if ( rc_conf)
        { return  LDNDC_ERR_RUNTIME_ERROR; }

    lerr_t  rc_lib = ldndc_set_libconf_from_configurationfile();
    if ( rc_lib)
        { return  LDNDC_ERR_RUNTIME_ERROR; }

    return  LDNDC_ERR_OK;
}
#include  <utils/lutils-env.h>
lerr_t
ldndc::llandscapedndc_t::initialize_log( config_file_t const *  _cfg)
{
    std::string  log_fname =
        cbm::format_expand( _cfg->log_file());
    if ( cbm::getenv( LDNDC_ENV_DEFAULT_LOGFILE))
    {
        log_fname = cbm::format_expand(
                cbm::getenv( LDNDC_ENV_DEFAULT_LOGFILE));
    }

    if ( log_fname.empty())
        { return  LDNDC_ERR_FAIL; }
    std::string const  logline_prefix = ldndc_logline_prefix();
    lerr_t const  rc_log = LD_DefaultLogger->initialize(
        log_fname.c_str(), _cfg->log_level(), logline_prefix.c_str());
    if ( rc_log)
        { return  LDNDC_ERR_FAIL; }
#ifndef  _DEBUG
    LD_DefaultLogger->terminate_on_fatal_error = false;
#endif
    lflags_t const  logmask = _cfg->log_targets_mask();
    LD_DefaultLogger->log_targets_mask( &logmask);

    return  LDNDC_ERR_OK;
}

lerr_t
ldndc::llandscapedndc_t::read_Lresources(
               char const *  _lresources_fname)
{
    return  ldndc_read_Lresources( _lresources_fname);
}

lerr_t
ldndc::llandscapedndc_t::read_projectfile(
                char const *  _project_fname)
{
    lerr_t  rc_prj = ldndc_set_libconf_from_projectfile( _project_fname);
    if ( rc_prj)
        { return  LDNDC_ERR_RUNTIME_ERROR; }
    return  LDNDC_ERR_OK;
}

lerr_t
ldndc::llandscapedndc_t::check_version_required()
{
    return  ldndc_check_version_required();
}

lerr_t
ldndc::llandscapedndc_t::set_schedule( char const *  _schedule_str)
{
    lerr_t  rc_schedule = ldndc_set_schedule( _schedule_str);
    if ( rc_schedule)
        { return LDNDC_ERR_RUNTIME_ERROR; }
    return  LDNDC_ERR_OK;
}

#include  "kernels/ld_kernels.h"
#include  "kernels/ld_defaults.h"
lerr_t
ldndc::llandscapedndc_t::initialize_region( lid_t const &  _pid,
                                            cbm::io_dcomm_t *  _io_dcomm,
                                            cbm::work_dispatcher_t *  _wd)
{
    this->m_domain.set_object_id( _pid);
    ldndc_set_kernel_factories( ldndc_registered_kernels());

    char const *  kspatial = cbm::getenv( "LD_KSpatial");
    if ( !kspatial)
        { kspatial = _LD_KSpatial; }
    this->m_domain.select_systemcomponent( "DomainRoot", kspatial);
    char const *  ktemporal = cbm::getenv( "LD_KTemporal");
    if ( !ktemporal)
        { ktemporal = _LD_KTemporal; }
    this->m_domain.select_systemcomponent( "KTemporal", ktemporal);
    char const *  kmodelcompositor = cbm::getenv( "LD_ModelCompositor");
    if ( !kmodelcompositor)
        { kmodelcompositor = _LD_ModelCompositor; }
    this->m_domain.select_systemcomponent( "ModelCompositor", kmodelcompositor);

    lerr_t  rc_reginit = this->m_domain.initialize( _io_dcomm, _wd);
    return  rc_reginit;
}


lerr_t
ldndc::llandscapedndc_t::launch( cbm::td_scalar_t  _sse_stop)
{
    if ( _sse_stop == 0)
        { return  this->m_domain.launch(); }
    return  this->m_domain.launch( _sse_stop);
}

void
ldndc::llandscapedndc_t::set_status(
        int  _status)
{
    if ( this->m_status == LLDNDC_STATUS_OK)
        { this->m_status = _status; }
}
char const *
ldndc::llandscapedndc_t::get_status_message()
const
{
    return  llandscapedndc_status_msg[-this->m_status];
}


#ifdef  _HAVE_SERIALIZE
int
ldndc::llandscapedndc_t::create_checkpoint()
{
    if ( this->size() > 0)
        { return  this->m_domain.create_checkpoint(); }
    return  0;
}
int
ldndc::llandscapedndc_t::restore_checkpoint( ldate_t  _ldate)
{
    if ( this->size() > 0)
        { return  this->m_domain.restore_checkpoint( _ldate); }
    return  0;
}
#endif

