/*!
 * @file
 *    main program file of (regional) simulation system LandscapeDNDC
 *
 * @author
 *    Edwin Haas,
 *    Steffen Klatt,
 *    David Kraus
 */

extern int  ldndc_kickstart( int, char **);

int
main( int  argc, char **  argv)
{
    int  rc_ldndc = ldndc_kickstart( argc, argv);
    return  rc_ldndc;
}

