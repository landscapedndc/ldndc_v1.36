/*!
 * @brief
 *    common functions shared by ldndc applications
 *    (implementation)
 *
 * @author
 *    steffen klatt (created on: 20 sep., 2013),
 *    david kraus,
 *    edwin haas
 */

#include  "app/ld_init.h"

#include  <cbm_rtcfg.h>
#include  <math/cbm_math.h>
#include  <string/cbm_string.h>
#include  <utils/cbm_utils.h>
#include  <mpi/cbm_mpi.h>

#include  <cfgfile/cbm_cfgfile.h>
static  cbm::config_file_t *  ld_cfg = NULL;
#include  <prjfile/cbm_prjfile.h>
static  cbm::project_file_t *  ld_prj = NULL;
#include  <time/cbm_time.h>
static  cbm::sclock_t *  ld_clk = NULL;
#include  <kernel/kfactory.h>
#include  <kernel/kfactorystore.h>
static  cbm::kernelfactorystore_t *  ld_kfactory = NULL;
static  cbm::factorystore_t< cbm::kernelfactory_base_t > *  ld_kfactorystore = NULL;
#include  <cpl/cbm_cpl.h>
static  CBM_Coupler  ld_cpl;
static  CBM_CouplerStats  ld_cplstats;
static  int  ld_cplok = -1;

#include  "ldndc-version.h"
#ifdef  LDNDC_PYTHON
#  include  <scripting/cbm_python.h>
#endif
#ifdef  LDNDC_UNITS
#  include  <units/cbm_units.h>
#endif
static int  cbm_loglevel( char const *  _env_var)
{
    return CBM_LogLevelFromName(
        cbm::getenv( _env_var), LOGLEVEL_ERROR);
}
static int  cbm_logcolor( char const *  _env_var)
{
    char const *  CBM_LOGCOLOR = cbm::getenv( _env_var);
    if ( !CBM_LOGCOLOR)
        { return 0; }
    return cbm::is_bool_true( CBM_LOGCOLOR) ? 1 : 0;
}
int  ldndc_initialize( int _argc, char **  _argv)
{
    LDNDC_FIX_UNUSED(_argc);
    LDNDC_FIX_UNUSED(_argv);

    package_user.set_name( ldndc_package_name());
    package_user.set_version( ldndc_package_version());
    package_user.set_version_long( ldndc_package_version_long());

    /* by default CrabMEAT only emits error messages, unless
     * environment variable specifies differently */
    cbm::baselogger_t::log_level = cbm_loglevel( "CBM_LOGLEVEL");
    /* by default CrabMEAT uses no colored message type
     * prefixes, unless environment variable is set */
    cbm::baselogger_t::set_color( cbm_logcolor( "CBM_LOGCOLOR"));

    int  rc = 0; /*ok*/

    /* set ldndc dotpath, e.g., ~/.ldndc */
    lerr_t  rc_dotpath = ldndc_set_ldndc_dot_path();
    if ( rc_dotpath)
    {
        CBM_LogError( "Failed to set ",ldndc_package_name(), " dot path");
        rc = -1;
    }

#ifdef  LDNDC_PYTHON
    int  rc_python = cbm_python_t::initialize( _argv[0]);
    if ( rc_python)
    {
        CBM_LogError( "Failed to initialize Python environment");
        rc = -1;
    }
#endif
    
    int  rc_mpiinit = cbm::mpi::init( &_argc, &_argv);
    if ( rc_mpiinit == 0)
    {
        cbm::mpi::comm_dup(
            MPI_COMM_WORLD, &cbm::mpi::comm_ldndc);
        LD_RtCfg.rc.rank =
            cbm::mpi::comm_rank( cbm::mpi::comm_ldndc);
    }
    else
        { rc = -1; }

    /* coupler */
	/* TODO set global switch to turn on/off coupler summary.
	 *  - a more powerful way of coupler configuration is
	 *    desirable
	 */
	if ( cbm::getenv( "CBM_ShowCouplerStats"))
	{
		LD_RtCfg.rc.coupler_statistics =
			cbm::is_bool_true( cbm::getenv( "CBM_ShowCouplerStats"))
				? true : false;
	}

    CBM_CouplerEnvironment  cpl_environment;
    CBM_CouplerEnvironmentInitialize( &cpl_environment);
    cpl_environment.collect_stats = LD_RtCfg.rc.coupler_statistics;
    CBM_CouplerStats * cplstats = NULL;
    if ( cpl_environment.collect_stats)
        { cplstats = &ld_cplstats; }

    ld_cpl = CBM_CouplerCreate( "LD_Coupler",
            &cpl_environment, cplstats, &ld_cplok);
    if ( ld_cplok == 0)
        { LD_RtCfg.cpl = ld_cpl; }
    else 
    {
        CBM_LogError( "Failed to create coupler");
        rc = -1;
    }

    return rc;
}
int  ldndc_finalize()
{
    if ( ld_cplok == 0)
    {
        if ( LD_RtCfg.rc.coupler_statistics)
            { CBM_CouplerWriteSummaryTable( &ld_cplstats, 0); }
        CBM_CouplerDestroy( ld_cpl);
    }

#ifdef  LDNDC_PYTHON
    cbm_python_t::finalize();
#endif
#ifdef  LDNDC_UNITS
    cbm_units_t::finalize();
#endif

    cbm::mpi::finalize();
    CBM_RegisteredLoggers.shutdown(); 

    if ( ld_clk)
        { delete ld_clk; ld_clk = NULL; }
    if ( ld_cfg)
        { delete ld_cfg; ld_cfg = NULL; }
    if ( ld_prj)
        { delete ld_prj; ld_prj = NULL; }
    if ( ld_kfactory)
        { delete ld_kfactory; ld_kfactory = NULL; }
    if ( ld_kfactorystore)
        { delete ld_kfactorystore; ld_kfactorystore = NULL; }

    return 0;
}

lerr_t
ldndc_define_variables( char **  _envvars, int  _envvars_sz)
{
    if (( ! _envvars) || ( _envvars_sz == 0))
        { return  LDNDC_ERR_OK; }

    for ( int  k = 0;  k < _envvars_sz;  ++k)
    {
        if ( ! _envvars[k])
            { continue; }

        cbm::key_value_t  envnv =
            cbm::as_key_value( _envvars[k]);

        if ( envnv.key.length() > 0)
        {
            cbm::setenv(
                envnv.key.c_str(), envnv.value.c_str(), 1);
        }
    }

    return  LDNDC_ERR_OK;
}


static
lerr_t
ldndc_set_path(
        std::string *  _target, char const *  _system_default, char const *  _envvar)
{
    ldndc_assert(_target);
    if ( _envvar && cbm::getenv( _envvar))
    {
        *_target = cbm::getenv( _envvar);
    }
    else
    {
        *_target = _system_default;
    }
    if ( cbm::is_empty( *_target))
    {
        *_target = _system_default;
    }

    lerr_t  rc_fdot = cbm::format_expand( _target);
    RETURN_IF_NOT_OK(rc_fdot);
    if ( cbm::is_empty( *_target))
    {
        *_target = _system_default;
    }
    *_target += '/';
    if ( !cbm::is_absolute_path( _target->c_str()))
    {
        *_target =
            LD_RtCfg.io.current_working_path+'/'+*_target;
    }
    return  LDNDC_ERR_OK;
}

lerr_t
ldndc_set_ldndc_dot_path()
{
    return  ldndc_set_path(
        &LD_RtCfg.io.dot_path, cbm::DOT_PATH, LDNDC_ENV_DOT_PATH);
}

lerr_t
ldndc_open_configuration( char const *  _cfg_fname)
{
    if ( ld_cfg)
        { return  LDNDC_ERR_OK; }
    ld_cfg = new cbm::config_file_t;
    if ( !ld_cfg)
        { return  LDNDC_ERR_NOMEM; }

    if ( _cfg_fname)
    {
        ld_cfg->set_filename( _cfg_fname);
    }
    else
    {
        std::string const  cfg_filename = 
            ldndc::config_file_t::default_filename( &LD_RtCfg.io.dot_path);
        ld_cfg->set_filename( cfg_filename.c_str());
    }
    if ( ld_cfg->read() != LDNDC_ERR_OK)
    {
        fprintf( stderr, "[EE] failed to load configuration file  [file=%s]\n", ld_cfg->filename());
        return  LDNDC_ERR_FAIL;
    }

    LD_RtCfg.cfg = ld_cfg;
    return  LDNDC_ERR_OK;
}

lerr_t
ldndc_set_environment_from_configurationfile()
{
    ldndc_assert( ld_cfg);
    ldndc::config_file_t::iterator  eit = ld_cfg->begin_env();
    for ( ; eit != ld_cfg->end_env();  ++eit)
    {
        if ( eit.key() && !cbm::is_empty( eit.key()))
        {
            cbm::setenv( eit.key(), eit.value(), 0);
        }
    }
    return  LDNDC_ERR_OK;
}

lerr_t
ldndc_set_libconf_from_configurationfile()
{
    if ( !ld_cfg)
        { return  LDNDC_ERR_RUNTIME_ERROR; }

    lerr_t  rc_env = ldndc_set_environment_from_configurationfile();
    RETURN_IF_NOT_OK(rc_env);

    LD_RtCfg.cc.random_seed =
        ld_cfg->random_seed_given() ? ld_cfg->random_seed() : cbm::random::invalid_random_seed;

    LD_RtCfg.lc.log_append = ld_cfg->log_append();

    LD_RtCfg.kc.balance_check = ld_cfg->have_balance_check();
    LD_RtCfg.kc.balance_tolerance = ld_cfg->balance_tolerance();

    lerr_t  rc_ipath = ldndc_set_path(
        &LD_RtCfg.io.input_path, ld_cfg->input_path(), LDNDC_ENV_INPUT_PATH);
    RETURN_IF_NOT_OK(rc_ipath);
    
    lerr_t  rc_opath = ldndc_set_path(
        &LD_RtCfg.io.output_path, ld_cfg->output_path(), LDNDC_ENV_OUTPUT_PATH);
    RETURN_IF_NOT_OK(rc_opath);
    
    lerr_t  rc_rpath = ldndc_set_path(
        &LD_RtCfg.io.resources_path, ld_cfg->resources_path(), LDNDC_ENV_RESOURCES_PATH);
    RETURN_IF_NOT_OK(rc_rpath);

    lerr_t  rc_upath = ldndc_set_path(
        &LD_RtCfg.io.udunits_path, ld_cfg->udunits_path(), LDNDC_ENV_RESOURCES_PATH);
    RETURN_IF_NOT_OK(rc_upath);

    lerr_t  rc_spath = ldndc_set_path(
        &LD_RtCfg.io.service_registry_url, ld_cfg->service_registry_url(), LDNDC_ENV_RESOURCES_PATH);
    RETURN_IF_NOT_OK(rc_spath);
    
	if ( ld_cfg->sink_format_given())
	{
        LD_RtCfg.io.default_sink_format = ld_cfg->sink_format();
	}

    return  LDNDC_ERR_OK;
}

#include  "ldndc-version.h"
#include  <resources/Lresources.h>
lerr_t
ldndc_read_Lresources(
        char const *  _res_filename)
{
    if ( _res_filename)
    {
        return  LD_Resources.read( ldndc_package_version(), _res_filename);
    }
    return  LD_Resources.read( ldndc_package_version());
}

lerr_t
ldndc_set_libconf_from_projectfile(
                char const *  _project_fname)
{
    if ( ld_prj)
        { return  LDNDC_ERR_OK; }

    if ( !_project_fname || cbm::is_empty( _project_fname))
        { return  LDNDC_ERR_OK; }

    std::string  project_fname = cbm::format_expand( _project_fname);
    if ( project_fname.empty())
        { return  LDNDC_ERR_INVALID_ARGUMENT; }

    ld_prj = new cbm::project_file_t;
    if ( !ld_prj)
        { return  LDNDC_ERR_NOMEM; }

    lerr_t  rc_pfread = ld_prj->read( project_fname.c_str());
    if ( rc_pfread)
        { return  LDNDC_ERR_FAIL; }
    ld_prj->set_object_id( 0);

    LD_RtCfg.pc.project_filename = _project_fname;

    ldndc::project_info_t  project_meta;
    lerr_t  rc_pm = ld_prj->project_info( &project_meta);
    if ( rc_pm)
        { return  LDNDC_ERR_FAIL; }
    LD_RtCfg.pc.project_name = project_meta.project_name;


    LD_RtCfg.pc.global_source_prefix =
        cbm::format_expand( ld_prj->global_source_prefix());
    LD_RtCfg.pc.global_sink_prefix =
        cbm::format_expand( ld_prj->global_sink_prefix());

    if ( cbm::getenv( LDNDC_ENV_SCENARIO))
    {
        LD_RtCfg.pc.scenario_name =
            cbm::getenv( LDNDC_ENV_SCENARIO);
    }

    LD_RtCfg.prj = ld_prj;
    return  LDNDC_ERR_OK;
}

lerr_t
ldndc_set_schedule( char const *  _schedule_str)
{
    if ( ld_clk)
        { return  LDNDC_ERR_OK; }
    if ( !_schedule_str || cbm::is_empty( _schedule_str))
    {
        fprintf( stderr, "no schedule.\n");
        return  LDNDC_ERR_RUNTIME_ERROR;
    }

    ltime_t  schedule;
    if ( schedule.from_string( _schedule_str) != LDNDC_ERR_OK)
    {
        fprintf( stderr, "invalid schedule specification"
                        "  [schedule=%s]\n", _schedule_str);
        return  LDNDC_ERR_RUNTIME_ERROR;
    }

    ld_clk = new cbm::sclock_t( schedule);
    if ( !ld_clk)
        { return  LDNDC_ERR_NOMEM; }

    LD_RtCfg.clk = ld_clk;
    return LDNDC_ERR_OK;
}

lerr_t
ldndc_set_kernel_factories( void const *  _kfactories)
{
    if ( ld_kfactory)
        { return  LDNDC_ERR_OK; }

    ld_kfactorystore = new cbm::factorystore_t< cbm::kernelfactory_base_t >(
        static_cast< cbm::factorystore_t< cbm::kernelfactory_base_t >::registry_t const * >( _kfactories));
    if ( !ld_kfactorystore)
        { return  LDNDC_ERR_NOMEM; }
    ld_kfactory = new cbm::kernelfactorystore_t( ld_kfactorystore);
    if ( !ld_kfactory)
        { return  LDNDC_ERR_NOMEM; }

    LD_RtCfg.kfactory = ld_kfactory;
    return  LDNDC_ERR_OK;
}

#ifdef  LDNDC_OPENMP
#  include  <openmp/cbm_omp.h>
#endif
#ifdef  LDNDC_MPI
#  include  <mpi/cbm_mpi.h>
#endif
std::string
ldndc_logline_prefix()
{
    std::string  logline_prefix = "";
#if  defined(LDNDC_OPENMP)
    logline_prefix += cbm::n2s( cbm::omp::get_thread_num());
#endif
#if  defined(LDNDC_MPI)
#  if  defined(LDNDC_OPENMP)
    logline_prefix += '|';
#  endif
    logline_prefix += cbm::n2s(
        cbm::mpi::comm_rank( cbm::mpi::comm_ldndc));
#endif
    return  logline_prefix;
}

#include  "ldndc-version.h"
lerr_t
ldndc_check_version_required()
{
    /* package requirement settings */
    ldndc::package_require_t  pkg_require;
    lerr_t  rc_pkgrequire =
        ld_prj->package_require( &pkg_require);
    if ( rc_pkgrequire)
        { return  LDNDC_ERR_RUNTIME_ERROR; }
    if ( ! cbm::is_empty( pkg_require.version_required))
    {
        ldndc_package_version_t  target_version;
        int  rc_targetversion =
            target_version.from_string( pkg_require.version_required.c_str());
        if ( rc_targetversion)
        {
            CBM_LogError( "invalid target version  [version=",pkg_require.version_required,"]");
            return  LDNDC_ERR_FAIL;
        }
        if ( target_version != ldndc_package_versions())
        {
            CBM_LogError( ldndc_package_name(), " package version does not match target version  ",
                "[package version=",ldndc_package_version(),
                ",target version=",pkg_require.version_required,"]");
            return  LDNDC_ERR_FAIL;
        }
    }
    else if ( ! cbm::is_empty( pkg_require.minimum_version_required))
    {
        ldndc_package_version_t  target_version;
        int  rc_targetversion =
            target_version.from_string( pkg_require.minimum_version_required.c_str());
        if ( rc_targetversion)
        {
            CBM_LogError( "invalid target version  [version=",pkg_require.minimum_version_required,"]");
            return  LDNDC_ERR_FAIL;
        }
        if ( target_version > ldndc_package_versions())
        {
            CBM_LogError( ldndc_package_name(), " package version not sufficient for target version  ",
                "[package version=",ldndc_package_version(),
                ",target version>=",pkg_require.minimum_version_required,"]");
            return  LDNDC_ERR_FAIL;
        }
    }
    else
    {
        /* no target version */
        LOGWARN( "Please provide ", ldndc_package_name()," minimum version:",
            " 'PackageMinimumVersionRequired'",
            " (This warning may turn into a fatal error in the future)");
    }
    return  LDNDC_ERR_OK;
}

