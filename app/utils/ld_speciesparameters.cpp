/*!
 * @brief
 *    dump content of speciesparameters input stream
 *
 * @author
 *    steffen klatt (created on: apr 30, 2012)
 */

#include  "app/ld_init.h"

#include  <input/speciesparameters/speciesparameters.h>
#include  <string/cbm_string.h>

#include  <stdlib.h>
#include  <stdio.h>

using  namespace  ldndc::speciesparameters;

void
ldndc_cat_speciesparameters_get_sources(
        char *  _sourcenames,
        char const **  _sourcename, char const **  _lresourcesname)
{
    *_sourcename = _sourcenames;
    while ( *_sourcenames != '\0')
    {
        if ( *_sourcenames == ':')
        {
            *_lresourcesname = _sourcenames+1;
            *_sourcenames = '\0';
            break;
        }

        ++_sourcenames;
    }
}

lerr_t
ldndc_cat_speciesparameters(
        FILE *  _o_file, char const *  _sourcenames, lid_t  _id,
        char **  _species_in, size_t  _species_in_cnt)
{
    char *  sourcenames = cbm::strdup( _sourcenames);
    char const *  sourcename = NULL;
    char const *  lresourcesname = NULL;
    ldndc_cat_speciesparameters_get_sources( sourcenames, &sourcename, &lresourcesname);
    if ( lresourcesname)
    {
        ldndc_read_Lresources( lresourcesname);
    }

    cbm::project_t *  lproj =
        cbm::project_t::new_instance( 0);
    cbm::io_dcomm_t  io_dcomm;
    input_class_speciesparameters_t  in_speciesparameters;
    lerr_t  rc_open = ldndc_open_source< input_class_speciesparameters_t >(
            &in_speciesparameters, _id, lproj, &io_dcomm, sourcename, NULL);
    if ( rc_open)
    {
        cbm::strfree( sourcenames);
        return  LDNDC_ERR_FAIL;
    }

        char **  s_list = NULL;
        size_t  s_list_size( 0);
        if ( _species_in)
        {
            s_list_size = _species_in_cnt;
            s_list = _species_in;
        }
        else
        {
        LOGERROR( "dumping all species types not yet supported");
        cbm::strfree( sourcenames);
        return  LDNDC_ERR_FAIL;
        }

        for ( size_t  k = 0;  k < s_list_size;  ++k)
        {
            speciesparameters_set_t  params( in_speciesparameters[s_list[k]]);

            fprintf( _o_file, "species %s (group=%s,parent=%s):\n",
                    s_list[k],
                    params.GROUPNAME(),
                    params.PARENT());
            if ( ! params.is_valid())
            {
                fprintf( stderr, "[EE]  failed to acquire parameter set for species\n");
                continue;
            }
            if ( params.GROUP() == ldndc::species::SPECIES_GROUP_NONE)
            {
                fprintf( stderr, "[WW]  parameter set has group \"none\"\n");
                continue;
            }

            for ( size_t  j = 0;  j < SPECIESPARAMETERS_CNT;  ++j)
            {
                char const *  p_name( SPECIESPARAMETERS_NAMES[j]);
                fprintf( _o_file, "%-20s\t%s\n", p_name, params.parametervalue_as_string( p_name).c_str());
            }
        }


        cbm::strfree( sourcenames);
        return  LDNDC_ERR_OK;
}


    int
main( int  argc, char **  argv)
{
    if ( argc < 3)
    {
        fprintf( stderr, "not enough arguments\n");
        fprintf( stderr, "%s <data file>[:Lresources] <id> [<species_name_1> [<species_name_2>] ...]]\n", argv[0]);
        return  EXIT_FAILURE;
    }

    char *  i_file( argv[1]);
    lid_t  bid( atoi( argv[2]));
    int  species_in_cnt( argc-3);
    char **  species_in = NULL;
    if ( species_in_cnt > 0)
    {
        /* species names given */
        species_in = argv+3;
    }

    FILE *  o_file = stdout;

    lerr_t  rc = ldndc_cat_speciesparameters(
        o_file, i_file, bid, species_in, species_in_cnt);

    return  ( rc == LDNDC_ERR_OK) ? EXIT_SUCCESS : EXIT_FAILURE;
}

