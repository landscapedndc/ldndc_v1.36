/*!
 * @brief
 *    testing LDNDC configuration file parser
 *
 * @author
 *    steffen klatt (created on: dec 28, 2011),
 *    edwin haas,
 */

#include  "app/ld_init.h"
#include  <cfgfile/cbm_cfgfile.h>

#include  <stdlib.h>
#include  <stdio.h>

int
main( int  argc, char *  argv[])
{
    lerr_t  rc( LDNDC_ERR_FAIL);

    if ( argc > 1)
    {
        /* configuration file object */
        ldndc::config_file_t  cf( argv[1]);
        /* parse given configuration file */
        rc = cf.read();
        if ( rc == LDNDC_ERR_OK)
        {
            cf.dump();

// sk:off            printf( "\n-----------------------------------------------\naccess examples:");
// sk:off            printf( "\n\tbalance tolerance = %.16f", cf.balance_tolerance());
// sk:off            printf( "\n\nquery examples:");
// sk:off            double  tnorm( invalid_dbl);
// sk:off            lerr_t  tnorm_err = cf.query( "soilchemistry:dndc", "t_norm", &tnorm);
// sk:off            double  rw( invalid_dbl);
// sk:off            lerr_t  rw_err = cf.query( "physiology:dndc", "root_width", &rw);
// sk:off            printf( "\n\tt-norm = %f\n\troot width = %f\n", tnorm, rw);
// sk:off            int  i_do_not_exist_b( invalid_int);
// sk:off            lerr_t  b_err = cf.query( "foo:bar", "some_attribute", &i_do_not_exist_b);
// sk:off            float  i_do_not_exist_a( invalid_flt);
// sk:off            lerr_t  a_err = cf.query( "soilchemistry:dndc", "some_attribute", &i_do_not_exist_a);
// sk:off            printf( "\n\tt-norm %s= %f\n\troot width %s= %f\n\ti do not exist (int) %s= %d\n\ti do not exist (float) %s= %f\n", 
// sk:off                    ( tnorm_err == LDNDC_ERR_ATTRIBUTE_NOT_FOUND) ? "?" : "", tnorm,
// sk:off                    ( rw_err == LDNDC_ERR_ATTRIBUTE_NOT_FOUND) ? "?" : "", rw, 
// sk:off                    ( b_err == LDNDC_ERR_ATTRIBUTE_NOT_FOUND) ? "?" : "", i_do_not_exist_b, 
// sk:off                    ( a_err == LDNDC_ERR_ATTRIBUTE_NOT_FOUND) ? "?" : "", i_do_not_exist_a);
        }
    }
    else
    {
        /* abort if no configuration file was given as parameter */
        fprintf( stdout, "no ldndc configuration file specified. using default [%s].\n", ldndc::config_file_t::default_filename().c_str());

        /* configuration file object */
        ldndc::config_file_t  cf( ldndc::config_file_t::default_filename().c_str());
        cf.read();
        if ( cf.status())
        {
            fprintf( stdout, "failed to read default configuration file, falling back to internal defaults.\n\n");
        }
        else
        {
            fprintf( stdout, "\n");
        }
        cf.dump();
    }

    return  ( rc == LDNDC_ERR_OK) ? EXIT_SUCCESS : EXIT_FAILURE;
} // end main

