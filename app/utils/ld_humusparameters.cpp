/*!
 * @brief
 *    dump content of humusparameters input stream
 *
 * @author
 *    steffen klatt (created on: mar 19, 2014)
 */

#include  "app/ld_init.h"

#include  <input/soilparameters/soilparameters.h>

#include  <stdlib.h>
#include  <stdio.h>

using  namespace  ldndc::soilparameters;
using  namespace  ldndc::humusparameters;

lerr_t
ldndc_cat_humusparameters(
        FILE *  _o_file, char const *  _sourcename, lid_t  _id,
        char **  _humus_in, size_t  _humus_in_cnt)
{
    cbm::project_t *  lproj =
        cbm::project_t::new_instance( 0);
    cbm::io_dcomm_t  io_dcomm;
    input_class_soilparameters_t  in_humusparameters;
    lerr_t  rc_open = ldndc_open_source< input_class_soilparameters_t >(
            &in_humusparameters, _id, lproj, &io_dcomm, _sourcename, NULL);
    if ( rc_open)
    {
        return  LDNDC_ERR_FAIL;
    }

        char **  s_list = NULL;
        size_t  s_list_size( 0);
        if ( _humus_in)
        {
                s_list_size = _humus_in_cnt;
                s_list = _humus_in;
        }
        else
        {
        LOGERROR( "dumping all humus types not yet supported");
        return  LDNDC_ERR_FAIL;
        }

        for ( size_t  k = 0;  k < s_list_size;  ++k)
        {
        fprintf( _o_file, "%s\n", s_list[k]);
        for ( size_t  j = 0;  j < HUMUSPARAMETERS_CNT;  ++j)
        {
                        char const *  parname( HUMUSPARAMETERS_NAMES[j]);
            std::string const  parvalue( in_humusparameters.humusparametervalue_as_string( parname, s_list[k]));
                        fprintf( _o_file, "%-20s\t%s\n", parname, parvalue.c_str());
        }
        }

    return  LDNDC_ERR_OK;
}


int
main( int  argc, char **  argv)
{
    if ( argc < 3)
    {
        fprintf( stderr, "not enough arguments\n");
        fprintf( stderr, "%s <source> <id> [<humus_name_1> [<humus_name_2>] ...]]\n", argv[0]);
        return  EXIT_FAILURE;
    }

    char *  i_file( argv[1]);
    lid_t  bid( atoi( argv[2]));
    int  humus_in_cnt( argc-3);
    char **  humus_in = NULL;
    if ( humus_in_cnt > 0)
    {
        /* humus names given */
        humus_in = argv+3;
    }

    FILE *  o_file = stdout;

    lerr_t  rc = ldndc_cat_humusparameters(
        o_file, i_file, bid, humus_in, humus_in_cnt);

    return  ( rc == LDNDC_ERR_OK) ? EXIT_SUCCESS : EXIT_FAILURE;
}

