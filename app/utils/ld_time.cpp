/*!
 * @brief
 *    LDNDC date calculator
 *
 * @author
 *    steffen klatt (created on: jan 05, 2012),
 *    edwin haas,
 */

#include  "app/ld_init.h"

#include  <time/cbm_time.h>
#include  <string/cbm_string.h>

#include  <stdlib.h>
#include  <stdio.h>


#define  REF_LDATE "1953-02-13-01/48 -> +0"

void
date_plus_timespan( ldate_t const *  /*_d*/,
                lperiod_t const *  /*_p*/, char  /*_dir*/)
{
// sk:off    unsigned long  t_d = _d->sse();
// sk:off
// sk:off    ldate_t  d_inv = ldate_t::from_subdays( t_d, _d->time_resolution());
// sk:off    printf( "\tdate=%s  date-inverse=%s  [p=%ld,t=%lu]\n", _d->to_string().c_str(), d_inv.to_string().c_str(), t_p, t_d);
// sk:off
// sk:off    t_d += ( _dir == '+') ? t_p : -t_p;
// sk:off    d_inv = ldate_t::from_subdays( t_d, _d->time_resolution());
// sk:off    printf( "\tdate=%s  date-shift=%s\n", _d->to_string().c_str(), d_inv.to_string().c_str());
}

int
main( int  argc, char *  argv[])
{
    lerr_t  rc( LDNDC_ERR_OK);

    if ( argc > 1)
    {
        /* ldndc date object */
        ltime_t  ltime;
        /* set date from string object */
        rc = ltime.from_string( argv[1]);

        if ( rc == LDNDC_ERR_OK)
        {
            printf( "ltime=%s;\n\tjulian day=%u/%u\n\tdiff=%u\n\tperiod=%u\n\tsse=%u\n",
                    ltime.to_string().c_str(),
                    (unsigned int)ltime.from().yearday(),
                    (unsigned int)ltime.to().yearday(),
                    (unsigned int)ltime.diff( 1),
                    (unsigned int)ltime.period( 1),
                    (unsigned int)ltime.from().sse());

            if ( argc > 2)
            {
                if ( argv[2][0] == '-' || argv[2][0] == '+')
                {
                    char *  timespan = cbm::strdup( argv[2]);
                    char const  dir = timespan[0];
                    timespan[0] = '+';
                    lperiod_t  lperiod( timespan);
                    date_plus_timespan( &ltime.from(), &lperiod, dir);
                }
                else
                {
                    /* ldndc date object */
                    ltime_t  ldate_ref;
                    /* set date from string object */
                    rc = ldate_ref.from_string( argv[2]);
                    if ( rc)
                    {
                        fprintf( stderr, "date parsing failed,  date=%s\n", argv[2]);
                    }
                    else
                    {
                        printf( "merging: %s && %s ",
                                ltime.to_string().c_str(),
                                ldate_ref.to_string().c_str());

                        ltime.merge( ldate_ref);
                        printf( "= %s\n",
                                ltime.to_string().c_str());
                    }

                    LOGWRITE( "t=",ldate_ref);
                }
            }
        }
        else
        {
            fprintf( stderr, "date parsing failed,  date=%s\n", argv[1]);
        }
    }
    else
    {
        fprintf( stderr, "no argument\n");
    }

    return  ( rc == LDNDC_ERR_OK) ? EXIT_SUCCESS : EXIT_FAILURE;
}

