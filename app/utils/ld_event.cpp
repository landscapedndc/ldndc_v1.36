/*!
 * @brief
 *    dump content of site input stream
 *
 * @author
 *    steffen klatt (created on: mar 19, 2014)
 */

#include  "app/ld_init.h"

#include  <cbm_rtcfg.h>
#include  <input/event/event.h>
#include  <tinyxml2.h>

#include  <stdlib.h>
#include  <stdio.h>

using  namespace  ldndc::event;


void
ldndc_event_timeshift_apply(
        tinyxml2::XMLElement *  _ev_node,
        lperiod_t const *  _lperiod, char  _dir)
{
    if ( !_ev_node->Attribute( "type") || !_ev_node->Attribute( "time"))
    {
        return;
    }

    ltime_t  t;
    t.from_string( _ev_node->Attribute( "time"));

    ldate_t  t0 = t.from();
    ldate_t  t1 = t.to();

    if ( _dir == '-')
    {
        t0 += *_lperiod * -1;
        t1 += *_lperiod * -1;
    }
    else if ( _dir == '+')
    {
        t0 += *_lperiod;
        t1 += *_lperiod;
    }
    else
    {
        /* FIXME  error! */
    }

    t = ltime_t( t0, t1);

    std::string  new_time = t.to_string( ldate_t::LDATE_FLAG_SHORT|ldate_t::LDATE_FLAG_OMIT_TRES);
    _ev_node->SetAttribute( "time", new_time.c_str());
}

void
ldndc_event_timeshift_block(
        tinyxml2::XMLElement *  _ev_node,
        lperiod_t const *  _lperiod, char  _dir)
{
    while ( _ev_node)
    {
        if ( _ev_node->Attribute( "type") && _ev_node->Attribute( "time"))
        {
            ldndc_event_timeshift_apply( _ev_node, _lperiod, _dir);
        }

        _ev_node = _ev_node->NextSiblingElement( "event");
    }
}

lerr_t
ldndc_event_timeshift(
        lperiod_t const *  _lperiod, char  _dir,
        FILE *  _o_file, char const *  _sourcename, lid_t  _id)
{
    tinyxml2::XMLDocument  event_file;
    event_file.LoadFile( _sourcename);

    tinyxml2::XMLElement *  ev_root = event_file.RootElement();
    tinyxml2::XMLElement *  ev_blk = NULL;
    tinyxml2::XMLElement *  ev_node = NULL;
    if ( ev_root && ev_root->FirstChildElement( "event"))
    {
        ev_node = ev_root->FirstChildElement( "event");
        if ( ev_node->Attribute( "id"))
        {
            ev_blk = ev_node;
        }
    }

    if ( !ev_blk)
    {
        if ( _id == 0)
        {
            ldndc_event_timeshift_block( ev_node, _lperiod, _dir);
        }
    }
    else
    {
        while ( ev_blk)
        {
            ev_node = ev_blk->FirstChildElement( "event");
            if ( ev_node)
            {
                lid_t  b_id = 0;
                if ( ev_node->Attribute( "id"))
                {
                    b_id = ev_node->IntAttribute( "id");
                }
                if ( b_id == _id)
                {
                    ldndc_event_timeshift_block( ev_node, _lperiod, _dir);
                }
            }

            ev_blk = ev_blk->NextSiblingElement( "event");
        }
    }

    event_file.SaveFile( _o_file);
    return  LDNDC_ERR_OK;
}

lerr_t
ldndc_cat_event(
        ltime_t const *  _ltime,
        FILE *  _o_file, char const *  _sourcename, lid_t  _id)
{
    cbm::project_t *  lproj =
        cbm::project_t::new_instance( 0);
    cbm::io_dcomm_t  io_dcomm;
    input_class_event_t  in_event;
    cbm::sclock_t  clk( *_ltime);
    LD_RtCfg.clk = &clk;

    lerr_t  rc_open = ldndc_open_source< input_class_event_t >(
            &in_event, _id, lproj, &io_dcomm, _sourcename, NULL);
    if ( rc_open)
    {
        return  LDNDC_ERR_FAIL;
    }

    size_t  n_event_handles = in_event.get_event_handles_cnt();
    if ( n_event_handles == invalid_size)
    {
        fprintf( stderr, "failed to receive event handles\n");
        return  LDNDC_ERR_FAIL;
    }
    if ( n_event_handles == 0)
    {
        fprintf( stderr, "no events\n");
        return  LDNDC_ERR_OK;
    }

    event_handle_t *  event_handles =
        LD_Allocator->construct< event_handle_t >( n_event_handles);
    if ( !event_handles)
    {
        return  LDNDC_ERR_FAIL;
    }

    in_event.get_event_handles( event_handles, n_event_handles);

    for ( size_t  e = 0;  e < n_event_handles;  ++e)
    {
        Event const *  ev = event_handles[e].event;
        std::string  event_str = ev->to_string();
        std::string  event_time_str = event_handles[e].tspan_exec.to_string();
        fprintf( _o_file, "%s@%s\t[%s]\n", ev->name(), event_time_str.c_str(), event_str.c_str());
    }

    LD_Allocator->destroy( event_handles, n_event_handles);

    return  LDNDC_ERR_OK;
}


int
main( int  argc, char *  argv[])
{
    if ( argc < 4)
    {
        fprintf( stderr, "not enough arguments\n");
        fprintf( stderr, "%s <timespan> <data file> <id>\n", argv[0]);
        return  EXIT_FAILURE;
    }

    char *  i_file( argv[2]);
    lid_t  bid( atoi( argv[3]));

    FILE *  o_file = stdout;

    lerr_t  rc = LDNDC_ERR_FAIL;

    /* get relevant timespan */
    if ( cbm::strlen( argv[1]) > 1)
    {
        char *  timespan = cbm::strdup( argv[1]);

        if ( timespan[0] == '-' || timespan[0] == '+')
        {
            char  dir = timespan[0];
            timespan[0] = lperiod_t::PERIOD_TAG;

            lperiod_t  ev_period( timespan);
            rc = ldndc_event_timeshift( &ev_period, dir, o_file, i_file, bid);
        }
        else
        {
            ltime_t  ev_time;
            lerr_t  rc_time = ev_time.from_string( timespan);
            if ( rc_time)
            {
                return  EXIT_FAILURE;
            }

            rc = ldndc_cat_event( &ev_time, o_file, i_file, bid);
        }
    }
    else
    {
        fprintf( stderr, "not a valid time specification\n");
    }

    return  ( rc == LDNDC_ERR_OK) ? EXIT_SUCCESS : EXIT_FAILURE;
}

