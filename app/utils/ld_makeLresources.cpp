/*!
 * @brief
 *    writes Lresources from global resources database
 *
 * @author
 *    steffen klatt (created on: may 04, 2012)
 */

#include  "ldndc-version.h"
#include  "app/ld_init.h"

#include  <resources/Lresources.h>

#include  <string/cbm_string.h>
#include  <time/cbm_time.h>

#include  <stdlib.h>
#include  <stdio.h>

#include  <input/siteparameters/siteparameters.h>
lerr_t write_siteparameters( FILE *  _o_file, char const *  _sourcename)
{
    cbm::project_t *  lproj =
        cbm::project_t::new_instance( 0);
    cbm::io_dcomm_t  io_dcomm;
    ldndc::siteparameters::input_class_siteparameters_t  in_siteparameters;

    lerr_t  rc_open = ldndc_open_source< ldndc::siteparameters::input_class_siteparameters_t >(
            &in_siteparameters, 0, lproj, &io_dcomm, _sourcename, NULL);
    if ( rc_open)
    {
        return  LDNDC_ERR_FAIL;
    }


    fprintf( _o_file, "\n\n## %s\n", in_siteparameters.input_class_type());
    for ( size_t  j = 0;  j < ldndc::siteparameters::SITEPARAMETERS_CNT;  ++j)
    {
        char const *  p_name = ldndc::siteparameters::SITEPARAMETERS_NAMES[j];

        fprintf( _o_file, "site.parameter.%s.value = \"%s\"\n",
                        p_name, in_siteparameters.parametervalue_as_string( p_name).c_str());
    }

    return  LDNDC_ERR_OK;
}

#include  <input/speciesparameters/speciesparameters.h>
lerr_t write_speciesparameters( FILE *  _o_file, char const *  _sourcename)
{
    cbm::project_t *  lproj =
        cbm::project_t::new_instance( 0);
    cbm::io_dcomm_t  io_dcomm;
    ldndc::speciesparameters::input_class_speciesparameters_t  in_speciesparameters;

    lerr_t  rc_open = ldndc_open_source< ldndc::speciesparameters::input_class_speciesparameters_t >(
            &in_speciesparameters, 0, lproj, &io_dcomm, _sourcename, NULL);
    if ( rc_open)
    {
        return  LDNDC_ERR_FAIL;
    }

    fprintf( _o_file, "\n\n## %s\n", in_speciesparameters.input_class_type());
    fprintf( _o_file, "species.count = \"%d\"\n", static_cast< int >( in_speciesparameters.parameterized_species_cnt()));
    for ( size_t  k = 0;  k < in_speciesparameters.parameterized_species_cnt();  ++k)
    {
        ldndc::speciesparameters::speciesparameters_set_t  p = in_speciesparameters.at( k);

        char const *  p_type = p.TYPE();
        fprintf( _o_file, "species.%s.mnemonic = \"%s\"\n", p_type, p_type);
        char const *  p_group = p.GROUPNAME();
        fprintf( _o_file, "species.%s.group = \"%s\"\n", p_type, p_group);
        char const *  p_parent = p.PARENT();
        fprintf( _o_file, "species.%s.parent = \"%s\"\n", p_type, p_parent);
        if ( !p.is_valid())
        {
            fprintf( stderr, "[EE]  failed to acquire parameter set for species\n");
            continue;
        }
        for ( size_t  j = 0;  j < ldndc::speciesparameters::SPECIESPARAMETERS_CNT;  ++j)
        {
            char const *  p_name = ldndc::speciesparameters::SPECIESPARAMETERS_NAMES[j];

            fprintf( _o_file, "species.%s.parameter.%s.value = \"%s\"\n",
                            p_type, p_name, p.parametervalue_as_string( p_name).c_str());
        }
    }

    return  LDNDC_ERR_OK;
}

#include  <input/soilparameters/soilparameters.h>
lerr_t write_soilparameters( FILE *  _o_file, char const *  _sourcename)
{
    cbm::project_t *  lproj =
        cbm::project_t::new_instance( 0);
    cbm::io_dcomm_t  io_dcomm;
    ldndc::soilparameters::input_class_soilparameters_t  in_soilparameters;

    lerr_t  rc_open = ldndc_open_source< ldndc::soilparameters::input_class_soilparameters_t >(
            &in_soilparameters, 0, lproj, &io_dcomm, _sourcename, NULL);
    if ( rc_open)
    {
        return  LDNDC_ERR_FAIL;
    }

    fprintf( _o_file, "\n\n## %s\n", in_soilparameters.input_class_type());
    fprintf( _o_file, "soil.count = \"%d\"\n", static_cast< int >( in_soilparameters.parameterized_soil_cnt()));
    for ( size_t  k = 0;  k < in_soilparameters.parameterized_soil_cnt();  ++k)
    {
        char const *  mnemonic = ldndc::soillayers::SOIL_MNEMONIC[k];
        fprintf( _o_file, "soil.%s.mnemonic = \"%s\"\n", mnemonic, mnemonic);
        for ( size_t  j = 0;  j < ldndc::soilparameters::SOILPARAMETERS_CNT;  ++j)
        {
            char const *  parname = ldndc::soilparameters::SOILPARAMETERS_NAMES[j];
            std::string const  parvalue =
                in_soilparameters.soilparametervalue_as_string( parname, ldndc::soillayers::SOIL_MNEMONIC[k]);
            fprintf( _o_file, "soil.%s.parameter.%s.value = \"%s\"\n",
                    mnemonic, parname, parvalue.c_str());
        }
    }
    char const *  meta_mnemonic = "METASOIL"; /*defined in Lresources reader*/
    fprintf( _o_file, "soil.%s.mnemonic = \"%s\"\n", meta_mnemonic, meta_mnemonic);
    for ( size_t  j = 0;  j < ldndc::soilparameters::SOILPARAMETERS_CNT;  ++j)
    {
        char const *  parname = ldndc::soilparameters::SOILPARAMETERS_NAMES[j];
        std::string const  parvalue =
            in_soilparameters.soilparametervalue_as_string( parname, ldndc::soillayers::SOIL_MNEMONIC[ldndc::soillayers::SOIL_NONE]);
        fprintf( _o_file, "soil.%s.parameter.%s.value = \"%s\"\n",
                meta_mnemonic, parname, parvalue.c_str());
    }

    return  LDNDC_ERR_OK;
}
lerr_t write_humusparameters( FILE *  _o_file, char const *  _sourcename)
{
    cbm::project_t *  lproj =
        cbm::project_t::new_instance( 0);
    cbm::io_dcomm_t  io_dcomm;
    ldndc::soilparameters::input_class_soilparameters_t  in_humusparameters;

    lerr_t  rc_open = ldndc_open_source< ldndc::soilparameters::input_class_soilparameters_t >(
            &in_humusparameters, 0, lproj, &io_dcomm, _sourcename, NULL);
    if ( rc_open)
    {
        return  LDNDC_ERR_FAIL;
    }

    fprintf( _o_file, "\n\n## %s\n", "humusparameters");
    fprintf( _o_file, "humus.count = \"%d\"\n", static_cast< int >( in_humusparameters.parameterized_humus_cnt()));
    for ( size_t  k = 0;  k < in_humusparameters.parameterized_humus_cnt();  ++k)
    {
        char const *  mnemonic = ldndc::soillayers::HUMUS_MNEMONIC[k];
        fprintf( _o_file, "humus.%s.mnemonic = \"%s\"\n", mnemonic, mnemonic);
        for ( size_t  j = 0;  j < ldndc::humusparameters::HUMUSPARAMETERS_CNT;  ++j)
        {
            char const *  parname = ldndc::humusparameters::HUMUSPARAMETERS_NAMES[j];
            std::string const  parvalue =
                in_humusparameters.humusparametervalue_as_string( parname, mnemonic);
            fprintf( _o_file, "humus.%s.parameter.%s.value = \"%s\"\n",
                    mnemonic, parname, parvalue.c_str());
        }
    }
    char const *  meta_mnemonic = "METAHUMUS"; /*defined in Lresources reader*/
    fprintf( _o_file, "humus.%s.mnemonic = \"%s\"\n", meta_mnemonic, meta_mnemonic);
    for ( size_t  j = 0;  j < ldndc::humusparameters::HUMUSPARAMETERS_CNT;  ++j)
    {
        char const *  parname = ldndc::humusparameters::HUMUSPARAMETERS_NAMES[j];
        std::string const  parvalue =
            in_humusparameters.humusparametervalue_as_string( parname, ldndc::soillayers::HUMUS_MNEMONIC[ldndc::soillayers::HUMUS_NONE]);
        fprintf( _o_file, "humus.%s.parameter.%s.value = \"%s\"\n",
                meta_mnemonic, parname, parvalue.c_str());
    }

    return  LDNDC_ERR_OK;
}


lerr_t write_libcntl( FILE *  _o_file)
{
    fprintf( _o_file, "\ninclude optional \"%%R/Lresources-local\"\n");

    return  LDNDC_ERR_OK;
}

#define  MAKELRESOURCES_MAXINFILE  1024
int main( int  argc, char *  argv[])
{
    if ( argc < 3)
    {
        fprintf( stderr, "not enough arguments\n");
        fprintf( stderr, "make-Lresources <output-file> <input-base-dir>\n");
        return  EXIT_FAILURE;
    }

    char const *  o_filename = argv[1];
    char const *  s_basedir = argv[2];

    FILE *  o_file = stdout;
    if ( !cbm::is_equal( o_filename, "-"))
    {
        o_file = fopen( o_filename, "w");
        if ( o_file == NULL)
        {
            fprintf( stderr, "failed to open output file  [filename=%s]\n", o_filename);
            return  EXIT_FAILURE;
        }
    }
    char  s_filename[MAKELRESOURCES_MAXINFILE];

    std::string  t;
    cbm::current_time_as_string( &t);
    fprintf( o_file, "## %s v%s Resources (created %s)\n\n",
        ldndc_package_name(), ldndc_package_version(),  t.c_str());
    fprintf( o_file, LRESOURCES_TARGETVERSION_KEY " = \"%s\"\n",
        ldndc_package_version());

    lerr_t  rc = LDNDC_ERR_OK;

    char const *  filename_siteparameters = "parameters-site.xml";
    cbm::snprintf( s_filename, MAKELRESOURCES_MAXINFILE, "%s/%s", s_basedir, filename_siteparameters);
    lerr_t  rc_siteparameters = write_siteparameters( o_file, s_filename);
    if ( rc_siteparameters)
        { rc = LDNDC_ERR_FAIL; }

    char const *  filename_speciesparameters = "parameters-species.xml";
    cbm::snprintf( s_filename, MAKELRESOURCES_MAXINFILE, "%s/%s", s_basedir, filename_speciesparameters);
    lerr_t  rc_speciesparameters = write_speciesparameters( o_file, s_filename);
    if ( rc_speciesparameters)
        { rc = LDNDC_ERR_FAIL; }

    char const *  filename_soilparameters = "parameters-soil.xml";
    cbm::snprintf( s_filename, MAKELRESOURCES_MAXINFILE, "%s/%s", s_basedir, filename_soilparameters);
    lerr_t  rc_soilparameters = write_soilparameters( o_file, s_filename);
    if ( rc_soilparameters)
        { rc = LDNDC_ERR_FAIL; }

    char const *  filename_humusparameters = "parameters-soil.xml";
    cbm::snprintf( s_filename, MAKELRESOURCES_MAXINFILE, "%s/%s", s_basedir, filename_humusparameters);
    lerr_t  rc_humusparameters = write_humusparameters( o_file, s_filename);
    if ( rc_humusparameters)
        { rc = LDNDC_ERR_FAIL; }

    lerr_t  rc_libcntl = write_libcntl( o_file);
    if ( rc_libcntl)
        { rc = LDNDC_ERR_FAIL; }

    fprintf( o_file, "\n\n## %s\n", "END");
    fclose( o_file);

    return  ( rc == LDNDC_ERR_OK) ? EXIT_SUCCESS : EXIT_FAILURE;
}

