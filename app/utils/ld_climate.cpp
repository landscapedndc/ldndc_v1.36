/*!
 * @brief
 *    dump content of climate input stream
 *
 * @author
 *    steffen klatt (created on: jun 18, 2015)
 */

#include  "ldndc-version.h"
#include  "app/ld_init.h"

#include  <cbm_rtcfg.h>
#include  <input/climate/climate.h>
#include  <string/cbm_string.h>

#include  <stdlib.h>
#include  <stdio.h>

using  namespace  ldndc::climate;


void
ldndc_cat_climate_write_header( FILE *  _o_file)
{
    fprintf( _o_file, "%s\t%s\t%s\t", "id", "year", "julianday");
    for ( size_t  k = 0;  k < record::RECORD_SIZE;  ++k)
    {
        fprintf( _o_file, "%s%c",
                RECORD_ITEM_NAMES[k], (( k == (record::RECORD_SIZE-1)) ? '\n' : '\t'));
    }
}

lerr_t
ldndc_cat_climate( ltime_t const *  _ltime, FILE *  _o_file,
        cbm::io_dcomm_t *  _io_dcomm, input_class_climate_t const *  _in_climate)
{
    ldate_t const  climate_timefrom( _in_climate->get_time().from());
    cbm::sclock_t  clk( *_ltime);
    LD_RtCfg.clk = &clk;

    record::item_type  climaterecord[record::RECORD_SIZE];

    while ( clk.run())
    {
        ldate_t const  clk_now = clk.now();
        if ( clk_now < climate_timefrom)
        {
            /* no op */
        }
        else
        {
            _io_dcomm->update_io();
            _in_climate->get_record( climaterecord, clk);
            fprintf( _o_file, "%u\t%u\t%u\t", (unsigned int)_in_climate->object_id(),
                    (unsigned int)clk_now.year(), (unsigned int)clk_now.julian_day());
            for ( size_t  k = 0;  k < record::RECORD_SIZE;  ++k)
            {
                fprintf( _o_file, "%.2f%c",
                        climaterecord[k], (( k == (record::RECORD_SIZE-1)) ? '\n' : '\t'));
            }
        }
        clk.advance();
    }

    return  LDNDC_ERR_OK;
}

cbm::string_t::uint64_array_t
ldndc_cat_climate_parseids( char const *  _ids)
{
    cbm::string_t::uint64_array_t  dumpids;
    cbm::string_t  ids = _ids;

    if ( ids == "all")
    {
        /* TODO */
    }
    else
    {
        /* attempt to parse ID list */
        dumpids = ids.as_uint64_array( ',');
        if ( ids.size() == 0u)
        {
            LOGERROR( "error parsing ID list");
        }
    }
    return  dumpids;
}
 

lerr_t
ldndc_cat_climates( ltime_t const *  _ltime,
        FILE *  _o_file, char const *  _sourcename, char const *  _ids)
{
    lerr_t  rc = LDNDC_ERR_OK;

    cbm::string_t::uint64_array_t  dumpids =
        ldndc_cat_climate_parseids( _ids);
    if ( dumpids.size() == 0u)
    {
        rc = LDNDC_ERR_FAIL;
    }
    else
    {
        cbm::project_t *  lproj =
            cbm::project_t::new_instance( 0);
// sk:obs        lproj->set_reference_time( *_ltime);

        cbm::io_dcomm_t  io_dcomm;

        lerr_t  rc_open = ldndc_open_source< input_class_climate_t >(
                NULL, invalid_lid, lproj, &io_dcomm, _sourcename, NULL);
        if ( rc_open)
        {
            rc = LDNDC_ERR_FAIL;
        }
        else
        {
            input_class_climate_t  in_climate;
            for ( size_t  k = 0;  k < dumpids.size();  ++k)
            {
                lid_t const  id = dumpids[k];

                lerr_t  rc_get =
                    ldndc_get_input< input_class_climate_t >( &in_climate, id, &io_dcomm);
                if ( rc_get == LDNDC_ERR_OK)
                {
                    rc = ldndc_cat_climate( _ltime, _o_file, &io_dcomm, &in_climate);
                    if ( rc)
                    {
                        break;
                    }
                }
                else
                {
                    LOGERROR( "no input with ID ", id);
                }
                io_dcomm.reset_io();
            }
        }
    }
    return  rc;
}


int
main( int  argc, char *  argv[])
{
    if ( argc < 4)
    {
        fprintf( stderr, "not enough arguments\n");
        fprintf( stderr, "%s <timespan> <data file> <id1[,id2[,..]]> [<outputfile>]\n", argv[0]);
        return  EXIT_FAILURE;
    }

    char const *  i_file = argv[2];
    char const *  c_ids = argv[3];

    FILE *  o_file = stdout;

    lerr_t  rc = LDNDC_ERR_FAIL;

    /* get relevant timespan */
    if ( cbm::strlen( argv[1]) > 1)
    {
        char *  timespan = argv[1];
        ltime_t  req_timespan;
        lerr_t  rc_time = req_timespan.from_string( timespan);
        if ( rc_time)
        {
            fprintf( stderr, "failed to parse time  [time=%s]\n", timespan);
            return  EXIT_FAILURE;
        }
        if (( argc > 4) && ( cbm::strlen( argv[4]) > 0))
        {
            char const *  o_filename = argv[4];
            o_file = fopen( o_filename, "w");
            if ( !o_file)
            {
                fprintf( stderr, "failed to open output file  [file=%s]\n", o_filename);
                return EXIT_FAILURE;
            }
        }
        ldndc_cat_climate_write_header( o_file);
        rc = ldndc_cat_climates( &req_timespan, o_file, i_file, c_ids);
        fclose( o_file);
    }
    else
    {
        fprintf( stderr, "not a valid time specification\n");
    }

    return  ( rc == LDNDC_ERR_OK) ? EXIT_SUCCESS : EXIT_FAILURE;
}

