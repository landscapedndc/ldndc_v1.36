/*!
 * @brief
 *    visualize domain as described in LandscapeDNDC setup input
 *
 * @author
 *    steffen klatt (created on: mar 19, 2014)
 */

#include  "app/ld_init.h"

#include  <input/setup/setup.h>
#include  <string/cbm_string.h>

#include  <stdlib.h>
#include  <stdio.h>

#include  <map>

#define  VIZSETUP_USE_XYZ
#ifdef  VIZSETUP_USE_XYZ
#  define  POS_X  x
#  define  POS_Y  y
#  define  POS_Z  z
#else /* longitude, latitude, elevation */
#  define  POS_X  latitude
#  define  POS_Y  longitude
#  define  POS_Z  elevation
#endif

using  namespace  ldndc::setup;

static char const * const  label_fmt =
    "set label %d%06d \"%d\" at %f,%f,%f center font \"Helvetica,5\" front textcolor %s\n";
static char const * const  arrow_fmt =
    "set arrow from %f,%f,%f to %f,%f,%f filled front linestyle %d\n";
static char const * const  point_fmt =
    "set label %d%06d at %f,%f,%f point lt %d pt 7 ps 0.4\n";

struct  lohi_t
{
    lohi_t() : lo(1.0e12), hi(-1.0e12) {}
    double  lo, hi;
};
typedef  std::map< std::string, int >  arrow_colors_map_t;
struct  domain_info_t
{
    domain_info_t() :
        outlet_owners( NULL), infiles( NULL), n_infiles( 0),
        cmd_filename( NULL), cmd_file( NULL),
        show_ids( 0), show_grid( 1), save_pdf( 0),
        raise_id( 2.0)
    {}

    char const *  outlet_owners;

    char **  infiles;
    size_t  n_infiles;

    char const *  cmd_filename;
    FILE *  cmd_file;

    int  show_ids;
    int  show_grid;
    int  save_pdf;

    double  raise_id;

    cbm::string_t  key_fakeplots;

    struct lohi_t  x, y, z;
    arrow_colors_map_t  arrow_colors;
};

lerr_t
ldndc_vizsetup_attach_sources(
        cbm::project_t *  _project, cbm::io_dcomm_t *  _io_dcomm,
        char **  _sourcenames, size_t  _n_sourcenames)
{
    cbm::io_dcomm_t *  io_dcomm = _io_dcomm;

    for ( size_t  s = 0;  s < _n_sourcenames;  ++s)
    {
        cbm::string_t  in_source = _sourcenames[s];
        cbm::string_t::string_array_t  in_source_info =
            in_source.as_string_array( ':');
        char const *  source_name =
            ( in_source_info.size() > 0) ? in_source_info[0].c_str() : NULL;
        char const *  source_id =
            ( in_source_info.size() > 1) ? in_source_info[1].c_str() : NULL;
        if ( in_source_info.size() > 2)
        {
            LOGERROR( "invalid source specification",
                    "  [source=",in_source,"]");
            return  LDNDC_ERR_FAIL;
        }

        lerr_t  rc_Open = ldndc_open_source< input_class_setup_t >(
            NULL, 0, _project, io_dcomm, source_name, NULL, source_id);
        if ( rc_Open)
        {
            return  LDNDC_ERR_FAIL;
        }
        io_dcomm = NULL;
    }

return  LDNDC_ERR_OK;
}

lerr_t
ldndc_vizsetup_boundaries(
        struct domain_info_t *  _di, input_class_setup_t const *  _in_setup)
{
    if ( _in_setup->get_mooreboundaries() & cbm::ORIENT_ANY)
    {
        fprintf( _di->cmd_file, label_fmt, 1, (int)_in_setup->object_id()+1, (int)_in_setup->object_id(),
            _in_setup->POS_X(), _in_setup->POS_Y(), _in_setup->POS_Z()+_di->raise_id, "rgb 'red'");
    }
    return  LDNDC_ERR_OK;
}

bool
ldndc_vizsetup_check_outlet_ownership(
        struct domain_info_t *  _di, input_class_setup_t const *  _in_setup)
{
    bool  i_have_an_outlet = false;

    cbm::string_t  outlet_owners = _di->outlet_owners;
    if ( outlet_owners == "none"
            || outlet_owners == invalid_lstring || outlet_owners.is_empty())
    {
        /* no op */
    }
    else if ( outlet_owners == "all")
    {
        i_have_an_outlet = true;
    }
    else
    {
        lerr_t  rc_moore =
            cbm::geo_orientation_t::mooreboundaries_from_string( outlet_owners, NULL);
        if ( rc_moore == LDNDC_ERR_OK)
        {
            cbm::geo_orientations_t  orientations( cbm::ORIENT_NONE);
            cbm::geo_orientation_t::mooreboundaries_from_string( outlet_owners, &orientations);
            if ( orientations & _in_setup->get_mooreboundaries())
            {
                i_have_an_outlet = true;
            }
        }
        else
        {
            /* attempt to parse ID list */
            cbm::string_t::uint32_array_t  outlet_owner_ids =
                outlet_owners.as_uint32_array( ',');
            if ( outlet_owner_ids.size() == 0u)
            {
                LOGERROR( "error parsing outlet ID list in configuration");
            }
            for ( size_t  k = 0;  k < outlet_owner_ids.size();  ++k)
            {
                LOGDEBUG( "seeing outlet owner ",outlet_owner_ids[k]);
                if ( static_cast< lid_t >( outlet_owner_ids[k]) == _in_setup->object_id())
                {
                    LOGDEBUG( "seeing myself in outlet owner list :-)");
                    i_have_an_outlet = true;
                    break;
                }
            }
        }
    }

    return  i_have_an_outlet;
}

lerr_t
ldndc_vizsetup_discharge(
        struct domain_info_t *  _di, input_class_setup_t const *  _in_setup)
{
    if ( !ldndc_vizsetup_check_outlet_ownership( _di, _in_setup))
    {
        return  LDNDC_ERR_OK;
    }

    double  x = _in_setup->POS_X(), y = _in_setup->POS_Y(), z = _in_setup->POS_Z();
    unsigned int const  bounds = _in_setup->get_mooreboundaries();
    double  X=2.0, Y=2.0;
    double  P=1.20;
    if ( bounds & cbm::ORIENT_NORTH)
    {
                y += Y;
    }
    if ( bounds & cbm::ORIENT_NORTHEAST)
    {
        x += X; y += Y;
    }
    if ( bounds & cbm::ORIENT_EAST)
    {
        x += X;
    }
    if ( bounds & cbm::ORIENT_SOUTHEAST)
    {
        x += X; y -= Y;
    }
    if ( bounds & cbm::ORIENT_SOUTH)
    {
                y -= Y;
    }
    if ( bounds & cbm::ORIENT_SOUTHWEST)
    {
        x -= X; y -= Y;
    }
    if ( bounds & cbm::ORIENT_WEST)
    {
        x -= X;
    }
    if ( bounds & cbm::ORIENT_NORTHWEST)
    {
        x -= X; y += Y;
    }
    fprintf( _di->cmd_file, arrow_fmt,
        _in_setup->POS_X(), _in_setup->POS_Y(), _in_setup->POS_Z(), x, y, z-P, 1);
    return  LDNDC_ERR_OK;
}


void
ldndc_vizsetup_update_lohi( 
        struct domain_info_t *  _di,
        double  _x, double  _y, double  _z)
{
    if ( _x > _di->x.hi) _di->x.hi = _x;
    if ( _x < _di->x.lo) _di->x.lo = _x;
    if ( _y > _di->y.hi) _di->y.hi = _y;
    if ( _y < _di->y.lo) _di->y.lo = _y;
    if ( _z > _di->z.hi) _di->z.hi = _z;
    if ( _z < _di->z.lo) _di->z.lo = _z;
}

lerr_t
ldndc_vizsetup_setup_to_gnuplot(
        struct domain_info_t *  _di)
{
    cbm::project_t *  lproj =
        cbm::project_t::new_instance( 0);
    cbm::io_dcomm_t  io_dcomm;

    lerr_t const  rc_sources =
        ldndc_vizsetup_attach_sources( lproj, &io_dcomm, _di->infiles, _di->n_infiles);
    if ( rc_sources)
    {
        return  LDNDC_ERR_FAIL;
    }

    std::vector< cbm::source_descriptor_t >  domain( io_dcomm.request_region());
    int  n_neighbor_connections = 0, n_cells = domain.size();

    for ( size_t  k = 0;  k < domain.size();  ++k)
    {
        input_class_setup_t  in_setup;
        lerr_t  rc_open = ldndc_get_input< input_class_setup_t >(
                &in_setup, &domain[k], &io_dcomm);
        if ( rc_open)
        {
            return  LDNDC_ERR_FAIL;
        }
        lid_t const  k_id = domain[k].block_id;

        std::string const  s_id = domain[k].source_identifier;
        if ( _di->arrow_colors.find( s_id)
                == _di->arrow_colors.end())
        {
            _di->arrow_colors[s_id] = _di->arrow_colors.size()+1;
            fprintf( _di->cmd_file, "set style line %d linecolor %d\n",
                    _di->arrow_colors[s_id], _di->arrow_colors[s_id]);
        }

        cbm::neighbor_t const *  neighbors = in_setup.get_neighbors();
        if ( neighbors) for ( size_t  n = 0;  n < in_setup.get_number_of_neighbors();  ++n)
        {
            input_class_setup_t  in_setup_neighbor;
            lerr_t  rc_open_neighbor =
                ldndc_get_input< input_class_setup_t >(
                    &in_setup_neighbor, &neighbors[n].desc, &io_dcomm);
            if ( rc_open_neighbor)
            {
                return  LDNDC_ERR_FAIL;
            }
            n_neighbor_connections += 1;

            if ( _di->show_grid)
            {
                fprintf( _di->cmd_file, arrow_fmt,
                    in_setup.POS_X(), in_setup.POS_Y(), in_setup.POS_Z(),
                    in_setup_neighbor.POS_X(), in_setup_neighbor.POS_Y(), in_setup_neighbor.POS_Z(),
                    _di->arrow_colors[s_id]);
            }
        }
        /* set point to represent cell, if no neigbors exist */
        else
        {
            fprintf( _di->cmd_file, point_fmt, 2, (int)k_id+1,
                in_setup.POS_X(), in_setup.POS_Y(), in_setup.POS_Z(),
                _di->arrow_colors[s_id]);
        }
        if ( _di->show_ids)
        {
            fprintf( _di->cmd_file, label_fmt, 1, (int)k_id+1, (int)k_id,
                in_setup.POS_X(), in_setup.POS_Y(), in_setup.POS_Z()+_di->raise_id, "default");
            ldndc_vizsetup_boundaries( _di, &in_setup);
        }

        if ( _di->outlet_owners)
        {
            ldndc_vizsetup_discharge( _di, &in_setup);
// sk:noise            if ( ldndc_vizsetup_check_outlet_ownership( _di, &in_setup))
// sk:noise            {
// sk:noise                fprintf( stderr, "%d %f %f %f\n",
// sk:noise                    k_id, in_setup.POS_X(), in_setup.POS_Y(), in_setup.POS_Z());
// sk:noise            }
        }

        ldndc_vizsetup_update_lohi( _di, in_setup.POS_X(), in_setup.POS_Y(), in_setup.POS_Z());
    }
    fprintf( _di->cmd_file, "set view equal xyz\n");
    fprintf( _di->cmd_file, "set ticslevel 0\n");
    if ( _di->save_pdf)
    {
        fprintf( _di->cmd_file, "set terminal pdfcairo enhanced color size %fin,%fin\n",
               8.0, 8.0*(_di->y.hi-_di->y.lo)/(_di->x.hi-_di->x.lo));
        fprintf( _di->cmd_file, "set output \"viz_setup.pdf\"\n");
    }
    for ( arrow_colors_map_t::const_iterator  arrow_color = _di->arrow_colors.begin();
            arrow_color != _di->arrow_colors.end();  ++arrow_color)
    {
        _di->key_fakeplots += cbm::string_t::format_t< 256 >::make( 
            ", %f lc %d title \"%s\"",
            _di->y.hi+1.0e10, arrow_color->second, arrow_color->first.c_str());
    }
    fprintf( _di->cmd_file, "set xrange [%f:%f]\n", _di->x.lo, _di->x.hi);
    fprintf( _di->cmd_file, "set yrange [%f:%f]\n", _di->y.lo, _di->y.hi);
    fprintf( _di->cmd_file, "set zrange [%f:%f]\n", _di->z.lo, _di->z.hi+_di->raise_id);
    fprintf( _di->cmd_file, "set title \"domain [#cells=%d,#connections=%d]\"\nshow title\n",
            n_cells, n_neighbor_connections);
    fprintf( _di->cmd_file, "set key on\n");
    fprintf( _di->cmd_file, "splot NaN notitle %s\n\n", _di->key_fakeplots.c_str());

    return  LDNDC_ERR_OK;
}

int
ldndc_vizsetup_usage(
        int  _rc)
{
    fprintf( stderr, "%s [OPTIONS] <data file>[:<identifier] [<data file>[:<identifier] ...]\n", "viz_setup");
    fprintf( stderr, "OPTIONS are:\n");
    fprintf( stderr, "--show-ids:\t\t\tdisplay IDs on cell position\n");
    fprintf( stderr, "--no-show-grid:\t\t\thide connection arrows\n");
    fprintf( stderr, "--save-pdf:\t\t\tset terminal to PDF in gnuplot\n");
    fprintf( stderr, "--outlet-owners <OWNERS>:\tspecify cells having outlet\n\t allowed arguments {none,all,<comma separated ID list>,<comma separated orientations}\n");

    return  _rc;
}

int
main( int  argc, char **  argv)
{
    if ( argc < 2)
    {
        fprintf( stderr, "not enough arguments\n");
        return  ldndc_vizsetup_usage( EXIT_FAILURE);
    }

    lerr_t  rc = LDNDC_ERR_OK;

    struct domain_info_t  domain_info;
    int  infiles_offs = 1;
    for ( int  c = 1;  c < argc;  ++c)
    {
        if ( cbm::is_equal( argv[c], "--help") || cbm::is_equal( argv[c], "-h"))
        {
            return  ldndc_vizsetup_usage( EXIT_SUCCESS);
        }
        else if ( cbm::is_equal( argv[c], "--no-show-grid"))
        {
            domain_info.show_grid = 0;
            domain_info.raise_id = 0.0;
            infiles_offs += 1;
        }
        else if ( cbm::is_equal( argv[c], "--show-ids"))
        {
            domain_info.show_ids = 1;
            infiles_offs += 1;
        }
        else if ( cbm::is_equal( argv[c], "--save-pdf"))
        {
            domain_info.save_pdf = 1;
            infiles_offs += 1;
        }
        else if ( cbm::is_equal( argv[c], "--outlet-owners"))
        {
            if ( argc > (c+1))
            {
                domain_info.outlet_owners = argv[c+1];
                c += 1;
                infiles_offs += 2;
            }
            else
            {
                LOGERROR( "missing argument for option '--outlet-owners'");
                rc = LDNDC_ERR_FAIL;
            }
        }
    }

    if ( rc == LDNDC_ERR_OK && ( infiles_offs < argc))
    {
        domain_info.infiles = argv+infiles_offs;
        domain_info.n_infiles = argc-infiles_offs;
        domain_info.cmd_file = stdout;

        rc = ldndc_vizsetup_setup_to_gnuplot( &domain_info);
    }

    return  ( rc == LDNDC_ERR_OK) ? EXIT_SUCCESS : EXIT_FAILURE;
}

