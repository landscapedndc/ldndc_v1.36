/*!
 * @brief
 *    dump content of setup input stream
 *
 * @author
 *    steffen klatt (created on: mar 19, 2014)
 */

#include  "app/ld_init.h"

#include  <input/setup/setup.h>

#include  <stdlib.h>
#include  <stdio.h>

using  namespace  ldndc::setup;

#define  DEFAULT_USE_ID  0

lerr_t
ldndc_cat_setup_table(
        FILE *  _o_file, char const *  _sourcename, lid_t  _id)
{
    cbm::project_t *  lproj =
        cbm::project_t::new_instance( 0);
    cbm::io_dcomm_t  io_dcomm;
    input_class_setup_t  in_setup;
    lerr_t  rc_open = ldndc_open_source< input_class_setup_t >(
            &in_setup, _id, lproj, &io_dcomm, _sourcename, NULL);
    if ( rc_open)
    {
        return  LDNDC_ERR_FAIL;
    }

    fprintf( _o_file, "id: %d\nlongitude: %.4f\nlatitude: %.4f\nelevation: %.4f\n",
            (int)_id, in_setup.longitude(), in_setup.latitude(), in_setup.elevation());
    fprintf( _o_file, "x: %.4f\ny: %.4f\nz: %.4f\narea: %.4f\n",
            in_setup.x(), in_setup.y(), in_setup.z(), in_setup.area());
    fprintf( _o_file, "dx: %.4f\ndy: %.4f\ndz: %.4f\nvolume: %.4f\n",
            in_setup.dx(), in_setup.dy(), in_setup.dz(), in_setup.volume());
    fprintf( _o_file, "consume-id:");
// sk:todo    int  given = 0;
// sk:todo    int  i = 0;
// sk:todo    for ( int  c = 0;  c < INPUT_CNT;  ++c)
// sk:todo    {
// sk:todo        cbm::source_descriptor_t  source_info;
// sk:todo        in_setup.block_consume(
// sk:todo                &source_info, static_cast< input_class_type_e >( c), &given);
// sk:todo
// sk:todo        int const  c_id = static_cast< int >( source_info.block_id);
// sk:todo        if ( given && ( c != INPUT_SETUP))
// sk:todo        {
// sk:todo            fprintf( _o_file, "%s%s=%d", (i==0?" ":";"), INPUT_NAMES[c], c_id);
// sk:todo            i += 1;
// sk:todo        }
// sk:todo    }

    size_t const  n_neighbors =
        in_setup.get_number_of_neighbors();
    if ( n_neighbors > 0)
    {
        cbm::neighbor_t const *  cell_neighbors = in_setup.get_neighbors();

        fprintf( _o_file, "\nneighbors-id: ");
        fprintf( _o_file, "%d", cell_neighbors[0].desc.block_id);
        for ( size_t  n = 1;  n < n_neighbors;  ++n)
        {
            fprintf( _o_file, ";%d", cell_neighbors[n].desc.block_id);
        }
        fprintf( _o_file, "\nneighbors-sourceidentifier: ");
        fprintf( _o_file, "%s", cell_neighbors[0].desc.source_identifier);
        for ( size_t  n = 1;  n < n_neighbors;  ++n)
        {
            fprintf( _o_file, ";%s", cell_neighbors[n].desc.source_identifier);
        }
        fprintf( _o_file, "\nneighbors-intersect: ");
        fprintf( _o_file, "%.4f", cell_neighbors[0].intersect);
        for ( size_t  n = 1;  n < n_neighbors;  ++n)
        {
            fprintf( _o_file, ";%.4f", cell_neighbors[n].intersect);
        }
    }

    fprintf( _o_file, "\n");


    return  LDNDC_ERR_OK;
}


int
main( int  argc, char *  argv[])
{
    if ( argc < 3)
    {
        fprintf( stderr, "not enough arguments\n");
        fprintf( stderr, "%s <data file> <id>\n", argv[0]);
        return  EXIT_FAILURE;
    }

    char *  i_file( argv[1]);
    lid_t  bid( atoi( argv[2]));

    FILE *  o_file = stdout;

    lerr_t  rc =
        ldndc_cat_setup_table( o_file, i_file, bid);

    return  ( rc == LDNDC_ERR_OK) ? EXIT_SUCCESS : EXIT_FAILURE;
}

