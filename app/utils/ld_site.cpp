/*!
 * @brief
 *    dump content of site input stream
 *
 * @author
 *    steffen klatt (created on: mar 19, 2014)
 */

#include  "app/ld_init.h"

#include  <input/site/site.h>

#include  <stdlib.h>
#include  <stdio.h>

using  namespace  ldndc::site;

lerr_t
ldndc_cat_site(
        FILE *  _o_file, char const *  _sourcename, lid_t  _id)
{
    cbm::project_t *  lproj =
        cbm::project_t::new_instance( 0);
    cbm::io_dcomm_t  io_dcomm;
    input_class_site_t  in_site;
    lerr_t  rc_open = ldndc_open_source< input_class_site_t >(
            &in_site, _id, lproj, &io_dcomm, _sourcename, NULL);
    if ( rc_open)
    {
        return  LDNDC_ERR_FAIL;
    }

    fprintf( _o_file, "%s=%.2fm\n", "watertable", in_site.watertable());
    fprintf( _o_file, "%s=\"%s\"\n", "usehistory", ECOSYSTEM_NAMES[in_site.soil_use_history()]);
    fprintf( _o_file, "%s=%.3fmm\n%s=%.3f\n%s=%.3f\n%s=\"%s\"\n%s=\"%s\"\n",
            "litterheight", in_site.litterheight(),
            "carbon content (50mm)", in_site.c_org05(), "carbon content (300mm)", in_site.c_org30(),
            "soil", ldndc::soillayers::SOIL_NAMES[in_site.soil_type()],
            "humus", ldndc::soillayers::HUMUS_NAMES[in_site.humus_type()]);

    int  strata_cnt = static_cast< int >( in_site.strata_cnt());
    int  layer_cnt = 0;
    for ( int  l = 0;  l < strata_cnt;  ++l)
    {
        iclass_site_stratum_t  stratum;
        in_site.stratum( l, &stratum);
        layer_cnt += stratum.split;
    }
    fprintf( _o_file, "soil strata/layer [%d/%d]:\n", strata_cnt, layer_cnt);
    for ( int  l = 0;  l < strata_cnt;  ++l)
    {
        iclass_site_stratum_t  stratum;
        in_site.stratum( l, &stratum);

        fprintf( _o_file,
                        "%3d: "
                        "%s=%.2f; %s=%d;%s"
                        " %s=%.3f; %s=%.3f; %s=%.3f;"
                        " %s=%.3f; %s=%.3f;"
                        " %s=%.3f;"
                        " %s=%.3f; %s=%.3f; %s=%.3f;"
                        " %s=%.3f; %s=%.3f; %s=%.3f; %s=%.3f;"
                        " %s=%.3f; %s=%.3f"
                        "\n",
                    l+1,
                    "height",stratum.stratum_height, "split",stratum.split, "   ",
                    "bulk density", stratum.bulk_density,
                    "carbon content",stratum.c_org, "nitrogen content", stratum.n_org,
                    "ph", stratum.ph,
                    "skeleton", stratum.stone_fraction,
                    "iron content", stratum.iron,
                    "clay", stratum.clay, "sand", stratum.sand,
                    "hydraulic conductivity", stratum.sks,
                    "wilting point", stratum.wcmin, "field capacity", stratum.wcmax,
                    "maximum water filled pore space", stratum.wfps_max,
                    "minimum water filled pore space", stratum.wfps_min,
                    "vangenuchten n", stratum.vangenuchten_n, "vangenuchten alpha", stratum.vangenuchten_alpha);
    }


    return  LDNDC_ERR_OK;
}


int
main( int  argc, char *  argv[])
{
    if ( argc < 3)
    {
        fprintf( stderr, "not enough arguments\n");
        fprintf( stderr, "%s <data file> <id>\n", argv[0]);
        return  EXIT_FAILURE;
    }

    char *  i_file( argv[1]);
    lid_t  bid( atoi( argv[2]));

    FILE *  o_file = stdout;

    lerr_t  rc =
        ldndc_cat_site( o_file, i_file, bid);

    return  ( rc == LDNDC_ERR_OK) ? EXIT_SUCCESS : EXIT_FAILURE;
}

