/*!
 * @brief
 *    ldndc utility to create an ldndc geometry input file
 *    from an esri shapefile
 *
 * @author
 *    steffen klatt (created on: feb 03, 2014),
 *    edwin haas
 */

#include  "app/ld_init.h"

#include  <io/io-comm.h>
#include  <geom/geom.h>

#include  <stdlib.h>
#include  <stdio.h>

#include  <shapefil.h>

lerr_t
usage(
        lerr_t  _rc)
{
    fprintf( stderr, "this tool converts the files <shapefile>.shp and <shapefile>.shx to an LDNDC geometry file.\n\n");
    fprintf( stderr, "the output is written to stdout\n");
    fprintf( stderr, "synopsis:\n");
    fprintf( stderr, "\tgeom-from-shapefile  <shapefile> <dim-x> <dim-y>\n");

    return  _rc;
}

struct  ldndc_geom_wsg84_t
{
    double  minbound_lat;
    double  maxbound_lat;
    double  minbound_lon;
    double  maxbound_lon;

    double  false_easting;
    double  false_northing;
};

#define  LDNDC_GEOM_DROP_PARTS  1
//#define  LDNDC_GEOM_NOMETA  1
lerr_t
read_shapefile(
        SHPHandle  _shp_hdl,
        ldndc::geom_dim_t *  _dim,
        ldndc_geom_wsg84_t *  _remap_info)
{
    int  ent_cnt = 0;
    double  minbound[4], maxbound[4];
    SHPGetInfo( _shp_hdl, &ent_cnt, NULL, minbound, maxbound);

    fprintf( stderr, "bb:   X=(%f,%f),   Y=(%f,%f)\n", minbound[0], maxbound[0], minbound[1], maxbound[1]);
    if ( _remap_info)
    {
        fprintf( stderr, "bb: LON=(%f,%f), LAT=(%f,%f)\n", _remap_info->minbound_lon, _remap_info->maxbound_lon, _remap_info->minbound_lat, _remap_info->maxbound_lat);
    }

#ifndef  LDNDC_GEOM_NOMETA
    fprintf( stdout, "%%global\n");
    fprintf( stdout, "\tentities = %d\n", ent_cnt);

//    fprintf( stdout, "\tupperleftx = %.3f\n", minbound[0]);
//    fprintf( stdout, "\tupperlefty = %.3f\n", minbound[1]);
//    fprintf( stdout, "\tupperleftz = %.3f\n", minbound[2]);
//
//    fprintf( stdout, "\tlowerrightx = %.3f\n", maxbound[0]);
//    fprintf( stdout, "\tlowerrighty = %.3f\n", maxbound[1]);
//    fprintf( stdout, "\tlowerrightz = %.3f\n", maxbound[2]);

//    fprintf( stdout, "\textentx = %.3f\n", ( maxbound[0]-minbound[0]) / _dim->n_x);
//    fprintf( stdout, "\textenty = %.3f\n", ( maxbound[1]-minbound[1]) / _dim->n_y);

    fprintf( stdout, "\tdimx = %d\n", _dim->n_x);
    fprintf( stdout, "\tdimy = %d\n", _dim->n_y);

    fprintf( stdout, "\n\n");
#else
    LDNDC_FIX_UNUSED(_dim);
#endif

    if ( ent_cnt == 0)
    {
        return  LDNDC_ERR_OK;
    }

#ifdef  LDNDC_GEOM_DROP_PARTS
    int  dropped_parts = 0;
#endif
    int  dumped_parts = 0;
    for ( int  ent_id = 0;  ent_id < ent_cnt; ++ent_id)
    {
        SHPObject *  shp = SHPReadObject( _shp_hdl, ent_id);

        if ( !shp)
        {
            fprintf( stderr, "geom-from-shapefile: failed to read shape object %d\n", ent_id);
            return  LDNDC_ERR_FAIL;
        }

        if (( !shp->padfX && !shp->padfY && !shp->padfZ) || ( shp->nParts == 0))
        {
            fprintf( stderr, "geom-from-shapefile: shape without vertices");
            continue;
        }

        int  n_parts_h = shp->nParts - 1;
        for ( int  ent_part = 0;  ent_part < shp->nParts;  ++ent_part)
        {
#ifdef  LDNDC_GEOM_DROP_PARTS
            if ( ent_part > 0)
            {
                dropped_parts += n_parts_h;
                break;
            }
#endif
            dumped_parts += 1;

            int  n_vertices = ( ent_part == n_parts_h) ? 
                ( shp->nVertices - shp->panPartStart[ent_part]) :
                       ( shp->panPartStart[ent_part+1] - shp->panPartStart[ent_part]);

#ifndef  LDNDC_GEOM_NOMETA
            fprintf( stdout, "%%geometry\n");
            fprintf( stdout, "\tid = %d #part %d\n", ent_id, ent_part);
            fprintf( stdout, "\tvertices = %d\n", n_vertices);
            fprintf( stdout, "\tvertexstart = %d\n", shp->panPartStart[ent_part]);
            fprintf( stdout, "\n");

            fprintf( stdout, "%%data\n");
// sk:off [z]            fprintf( stdout, "%s%s%s\n", ((shp->padfX)?"x":""), ((shp->padfY)?"\ty":""), ((shp->padfZ)?"\tz":""));
            fprintf( stdout, "%s%s\n", ((shp->padfX)?"x":""), ((shp->padfY)?"\ty":""));
#else
            fprintf( stdout, "\n\n");
#endif
            for ( int  n_vert = 0;  n_vert < n_vertices;  ++n_vert)
            {
                if ( shp->padfX)
                {
                    if ( _remap_info)
                    {
                        fprintf( stdout, "%.12f", _remap_info->minbound_lon+( shp->padfX[n_vert]-minbound[0]) / std::abs( maxbound[0]-minbound[0]) * std::abs( _remap_info->maxbound_lon-_remap_info->minbound_lon));
                    }
                    else
                    {
                        fprintf( stdout, "%.12f", shp->padfX[n_vert]);
                    }
                }
                if ( shp->padfY)
                {
                    if ( shp->padfY)
                    {
                        fprintf( stdout, "\t");
                        if ( _remap_info)
                        {
                            fprintf( stdout, "%.12f", _remap_info->minbound_lat+( shp->padfY[n_vert]-minbound[1]) / std::abs( maxbound[1]-minbound[1]) * std::abs( _remap_info->maxbound_lat-_remap_info->minbound_lat));
                        }
                        else
                        {
                            fprintf( stdout, "%.12f", shp->padfY[n_vert]);
                        }
                    }
                }
// sk:off                if ( shp->padfZ)
// sk:off                {
// sk:off                    fprintf( stdout, "\t%.12f", shp->padfZ[n_vert]);
// sk:off                }

                fprintf( stdout, "\n");
            }

            fprintf( stdout, "\n");
        }
        SHPDestroyObject( shp);
    }

#ifdef  LDNDC_GEOM_DROP_PARTS
    if ( dropped_parts > 0)
    {
        fprintf( stderr, "geom-from-shapefile: dropped %d polygons that were part of a single shape\n", dropped_parts);
    }
#endif
    fprintf( stderr, "geom-from-shapefile: dumped a total of %d polygons\n", dumped_parts);

    return  LDNDC_ERR_OK;
}

lerr_t
geom_from_shapefile(
        char const *  _shapefile_name /*path plus the basename of the pair*/,
        ldndc::geom_dim_t *  _dim)
{
    lerr_t  rc = LDNDC_ERR_OK;

    SHPHandle  shp_hdl = SHPOpen( _shapefile_name, "rb");

    int  shape_type = SHPT_NULL;
    SHPGetInfo( shp_hdl, NULL, &shape_type, NULL, NULL);

    if ( shape_type == SHPT_POLYGON)
    {
//        ldndc_geom_wsg84_t  remap_info;
//        remap_info.minbound_lat = 29.0;
//        remap_info.maxbound_lat = 32.0;
//        remap_info.minbound_lon = 105.0;
//        remap_info.maxbound_lon = 106.0;
//        remap_info.false_easting = 500000.0;
//        remap_info.false_northing = 0.0;
        rc = read_shapefile( shp_hdl, _dim, NULL);//&remap_info);
    }
    else
    {
        fprintf( stderr, "geom-from-shapefile: currently only shape type \"POLYGON\" is supported\n");
        rc = LDNDC_ERR_INVALID_ARGUMENT;
    }

    SHPClose( shp_hdl);

    return  rc;
}

int
main( int  argc, char **  argv)
{
    if ( argc < 4)
    {
        return  usage( LDNDC_ERR_FAIL);
    }

    ldndc::geom_dim_t  dim;
    dim.n_x = atoi( argv[2]);
    dim.n_y = atoi( argv[3]);

    if ( dim.n_x == 0 || dim.n_y == 0)
    {
        fprintf( stderr, "geom-from-shapefile: dimensions may not be null\n");
        return  usage( LDNDC_ERR_FAIL);
    }

    lerr_t  rc = geom_from_shapefile( argv[1], &dim);

    return  ( rc == LDNDC_ERR_OK) ? EXIT_SUCCESS : EXIT_FAILURE;
}

