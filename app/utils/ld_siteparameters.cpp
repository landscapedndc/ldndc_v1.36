/*!
 * @brief
 *    dump content of siteparameters input stream
 *
 * @author
 *    steffen klatt (created on: may 04, 2012)
 */

#include  "app/ld_init.h"

#include  <input/siteparameters/siteparameters.h>

#include  <stdlib.h>
#include  <stdio.h>

using  namespace  ldndc::siteparameters;

lerr_t
ldndc_cat_siteparameters(
        FILE *  _o_file, char const *  _sourcename, lid_t  _id)
{
    cbm::project_t *  lproj =
        cbm::project_t::new_instance( 0);
    cbm::io_dcomm_t  io_dcomm;
    input_class_siteparameters_t  in_siteparameters;
    lerr_t  rc_open = ldndc_open_source< input_class_siteparameters_t >(
            &in_siteparameters, _id, lproj, &io_dcomm, _sourcename, NULL);
    if ( rc_open)
    {
        return  LDNDC_ERR_FAIL;
    }

    for ( size_t  j = 0;  j < SITEPARAMETERS_CNT;  ++j)
    {
        char const *  p_name( SITEPARAMETERS_NAMES[j]);

        fprintf( _o_file, "%-20s\t%s\n",
            p_name, in_siteparameters.parametervalue_as_string( p_name).c_str());
    }

    return  LDNDC_ERR_OK;
}


int
main( int  argc, char *  argv[])
{
    if ( argc < 3)
    {
        fprintf( stderr, "not enough arguments\n");
        fprintf( stderr, "%s <data file> <id>\n", argv[0]);
        return  EXIT_FAILURE;
    }

    char *  i_file( argv[1]);
    lid_t  bid( atoi( argv[2]));

    FILE *  o_file = stdout;

    lerr_t  rc =
        ldndc_cat_siteparameters( o_file, i_file, bid);

    return  ( rc == LDNDC_ERR_OK) ? EXIT_SUCCESS : EXIT_FAILURE;
}

