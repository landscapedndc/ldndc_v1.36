# -*- coding: utf-8 -*-
## launching ldndc simulation from python.
## invoke like
## $> python ld_demo.py /path/to/ldndc/project.ldndc

import ldndc as ld
import sys

## === only plotting stuff =============================== 8< =
from collections import deque
from matplotlib import pyplot as plt

OnlinePlotting = False

class LD_PlotData( object ) :
    def __init__( self, max_length ) :
        self.ax = deque( [ 0.0 ] * max_length )
        self.max_length = max_length

    def AddToBuffer( self, buffer, value ) :
        if len( buffer ) < self.max_length :
            buffer.append( value )
        else :
            buffer.pop()
            buffer.appendleft( value )

    ## add data
    def Add( self, data ) :
        self.AddToBuffer( self.ax, data[ 0 ] )

class LD_Plotter( object ) :
    def  __init__( self, data, ylimit ) :
        ## set plot to animated
        plt.ion() 
        self.axlines = [ plt.plot( d.ax )[ 0 ] for d in data ]
        plt.ylim( [ ylimit[ 0 ], ylimit[ 1 ] ] )
        plt.gcf().set_size_inches( 13.5, 6.5, forward=True )

    def UpdatePlot( self, data ):
        for j, d in enumerate( data ) :
            self.axlines[ j ].set_ydata( d.ax )
        plt.draw()

class LD_PlotOptions( object ) :
    def __init__( self, **args ) :
        self.scale = args.get( 'scale', 1.0 )
        self.ylim_lo, self.ylim_hi = args.get( 'ylimits', ( -1.0, 1.0 ) )
        self.stride = args.get( 'stride', 1 )
        self.datapoints = args.get( 'datapoints', 1000 )

class LD_Plot( object ) :
    def __init__( self, field, plotoptions ) :

        self.plotoptions = plotoptions

        self.field = field
        self.plotdata = None
        self.plotter = None

        self.m_Initialize()

    def m_Initialize( self ) :
        if self.plotoptions is None :
            return  0

        count = 1
        if self.field.size > 1 :
            count = self.field.size / self.plotoptions.stride + 1

        self.plotdata = [ LD_PlotData( self.plotoptions.datapoints ) for k in range( count ) ]
        self.plotter = LD_Plotter( self.plotdata, \
			[ self.plotoptions.ylim_lo, self.plotoptions.ylim_hi ] )
        return 0

    def Execute( self ) :
        value  = self.field.Receive()
        stride, scale = self.plotoptions.stride, self.plotoptions.scale
        for j, v in enumerate( value[ : : stride ] ) :
            self.plotdata[ j ].Add( [ scale * v ] )
            self.plotter.UpdatePlot( self.plotdata )

        return 0

## === only plotting stuff =============================== >8 =


## stringify list
def L( array, fmt='%f', delim=';' ) :
    if array is None :
        return ''
    return delim.join( [ fmt%x for x in array ] )


## event handler
class EventCutSumHandler( object ) :
    def __init__( self ) :
        self.cut_sum = 0.0

    def __call__( self, data ) :
        print 'cut:',data
        self.add( data )
        print 'accumulated cut biomass:', self.get()
        return 0

    def get( self ) :
        return self.cut_sum
    def add( self, data ) :
        self.cut_sum += float( data[ 'remains' ] )
        return self.cut_sum

def EventFertilizeLogger( data ) :
    print 'fertilize:',data
    return 0

## initialize simulation (uses command line arguments)
ldndc = ld.LD_Ldndc( argv=sys.argv )
## initialize data coupler
shmem = ld.LD_Shared( ldndc )

## subscribe a vector (unknown size assumes that this field is already published)
VolumetricWaterSaturation_Consumer = shmem.Subscribe( \
        'VolumetricWaterSaturation', unit='m3/m3', size=ld.LD_Ask, datatype='double' )
## subscribe a scalar
StubbleCarbon = shmem.Subscribe( 'StubbleCarbon', unit='kg/m2', transform='sum' )
## subscribe another scalar
MaximumAirTemperature = shmem.Subscribe( 'MaximumAirTemperature', unit='°C' )

## subscribe to 'fertilize' event
EventFertilize_Consumer = shmem.SubscribeEvent( 'fertilize', EventFertilizeLogger )
## subscribe to 'cut' events
EventCutSum = EventCutSumHandler()
EventCut_Consumer = shmem.SubscribeEvent( 'cut', EventCutSum )
## publish 'cut' events
EventCut_Producer = shmem.PublishEvent( 'cut' )


## visualize 
if OnlinePlotting :
    plot = LD_Plot( VolumetricWaterSaturation_Consumer, \
        LD_PlotOptions( scale=1.0, ylimits=( 0.0, 1.0 ), stride=2, datapoints=50 ) )


print 'sse[begin]=',ldndc.GetSimulationTime()
t = ldndc.GetSimulationTime()
dt, dt_unit, dt_count = 24 * 60 * 60, 'day', 366
for j in range( dt_count ) :
    t += dt
    rc = ldndc.Launch( t )
    if j>1:
        pass
        #print '%s %4d\t\t[ %d ]' % ( dt_unit, j, t )
        #print ' :: <- stubble-carbon=%f;  max-temp=%f' % ( StubbleCarbon.Receive(), MaximumAirTemperature.Receive() )
        print ' :: <- volumetric-water-saturation[%d]=%s' % ( VolumetricWaterSaturation_Consumer.Size(), L( VolumetricWaterSaturation_Consumer.Receive() ) )
    if OnlinePlotting :
        plot.Execute()
    if j in [ 10, 20, 30, 40 ] :
        EventCut_Producer.Send( '{"remains": %f}'%( j + 123.456 ) )
print 'sse[end]=',ldndc.GetSimulationTime(), '  dt=',ldndc.GetSimulationTimeDelta()

## release all resources (coupler fields, events )
shmem.ReleaseAll()

## ldndc resources are automatically released by destructor

