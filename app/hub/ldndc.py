# -*- coding: utf-8 -*-
## python binding for LandscapeDNDC simulation launcher

import ctypes as cxpy
import os
import sys

## searches ldndc configuration file for
## option "libpath" in section "c-api".
## e.g.:
## ...
## [c-api]
## libpath = "~/lib/libldhub.so"
## ...
def find_library( cfg=None ) :
    import ConfigParser
    if cfg is None :
        cfg = '%s/.ldndc/ldndc.conf' % ( os.environ['HOME'] )
    cfgreader = ConfigParser.ConfigParser()
    is_read = cfgreader.read( cfg )
    if is_read and cfgreader.has_option( 'c-api', 'libpath' ) :
        libpath = cfgreader.get( 'c-api', 'libpath' )
        return libpath.strip( '"' ) 
    return None

## wrapper for single ldndc simulation project
class LD_Ldndc( object ) :
    def __init__( self, libpath=None, argv=[] ) :
        if not libpath :
            loadlib = find_library()
        else :
            loadlib = libpath
        if not loadlib :
            sys.stderr.write( 'Error: Failed to get shared object name; ' )
            sys.stderr.write( 'Please add the following section to the LandscapeDNDC configuration file:\n\n' );
            sys.stderr.write( '\t[c-api]\n\tlibpath = "/path/to/libldhub.{so,shlib,dll}"\n\n' );
            raise Exception
        try :
            self.shlib = cxpy.cdll.LoadLibrary( loadlib )
        except :
            sys.stderr.write( 'Failed to load shared object %s\n' % ( loadlib ) )
            raise Exception

        argc, argv = Py2C_StringArray( argv )
        self.rc = self.shlib.LD_SimCreate( argc, argv )
        if self.rc :
            sys.stderr.write( 'Failed to initialize simulation %s\n' )
            raise Exception
        ## force initial kernels to be mounted
## ??        self.Launch( self.GetSimulationTime() )

    def __del__( self ) :
        if getattr( self, 'shlib', None ) :
            self.shlib.LD_SimDestroy( self.rc )

    @property
    def Handle( self ) :
        return self.shlib

    def Launch( self, sse_end=0 ) :
        sse_end = max( 0, sse_end )
        fn = self.shlib.LD_SimLaunch
        fn.argtypes = [ cxpy.c_ulonglong ]
        self.rc = fn( sse_end )
        return self.rc

    def GetSimulationTime( self ) :
        fn = self.shlib.LD_CurrentTime
        fn.restype = cxpy.c_ulonglong
        return fn()
    def GetSimulationTimeDelta( self ) :
        fn = self.shlib.LD_CurrentTimestepDelta
        fn.restype = cxpy.c_ulonglong
        return fn()


class LD_StructHandle( cxpy.Structure ) :
    _fields_ = [ ( 'ID', cxpy.c_ulonglong ) ]
class LD_StructCallback( cxpy.Structure ) :
    CallbackFn = cxpy.CFUNCTYPE( cxpy.c_int, cxpy.c_char_p, cxpy.c_ulong, cxpy.c_void_p )
    _fields_ = [ ( 'fn', CallbackFn ), ( 'data', cxpy.c_void_p ) ]


import json
def LD_EventCallbackHook( message, message_size, data ) :
    dataproxy = cxpy.cast( data, cxpy.POINTER( cxpy.py_object )).contents.value
    if dataproxy :
        if not message :
            message = '{}'
        jsonobj = json.loads( message )
        return dataproxy.callback( jsonobj )
    return -1
class LD_EventCallback( object ) :
    def __init__( self, callback ) :
        self.callback = callback

        self.struct = LD_StructCallback()
        self.struct.fn = LD_StructCallback.CallbackFn( LD_EventCallbackHook )
        self.struct.data = cxpy.cast( cxpy.pointer( cxpy.py_object( self ) ), cxpy.c_void_p )

    @property
    def hook( self ) :
        return self.struct


class LD_Coupler( object ) :
    def __init__( self, shlib ) :
        self.fn_publish = shlib.Handle.LD_PublishField
        self.fn_publish.argtypes = as_c_types( [ 'char*', 'ulong', 'char*' ] )
        self.fn_publish.restype = as_c_type( 'ld_handle' )

        self.fn_unpublish = shlib.Handle.LD_UnpublishField
        self.fn_unpublish.argtypes = as_c_types( [ 'ld_handle'] )
        self.fn_unpublish.restype = as_c_type( 'int' )

        self.fn_send = shlib.Handle.LD_SendField
        self.fn_send.argtypes = as_c_types( [ 'ld_handle', 'void*', 'ulong', 'char*' ] )
        self.fn_send.restype = as_c_type( 'int' )

        self.fn_subscribe = shlib.Handle.LD_SubscribeField
        self.fn_subscribe.argtypes = as_c_types( [ 'char*', 'char*' ] )
        self.fn_subscribe.restype = as_c_type( 'ld_handle' )

        self.fn_unsubscribe = shlib.Handle.LD_UnsubscribeField
        self.fn_unsubscribe.argtypes = as_c_types( [ 'ld_handle' ] )
        self.fn_unsubscribe.restype = as_c_type( 'int' )

        self.fn_receive = shlib.Handle.LD_ReceiveField
        self.fn_receive.argtypes = as_c_types( [ 'ld_handle', 'void*', 'ulong', 'char*'  ] )
        self.fn_receive.restype = as_c_type( 'int' )

        self.fn_fieldsize = shlib.Handle.LD_FieldSize
        self.fn_fieldsize.argtypes = as_c_types( [ 'ld_handle' ] )
        self.fn_fieldsize.restype = as_c_type( 'int' )

        self.fn_publishevent = shlib.Handle.LD_PublishEvent
        self.fn_publishevent.argtypes = as_c_types( [ 'char*' ] )
        self.fn_publishevent.restype = as_c_type( 'ld_handle' )

        self.fn_unpublishevent = shlib.Handle.LD_UnpublishEvent
        self.fn_unpublishevent.argtypes = as_c_types( [ 'ld_handle' ] )
        self.fn_unpublishevent.restype = as_c_type( 'int' )
        
        self.fn_subscribeevent = shlib.Handle.LD_SubscribeEvent
        self.fn_subscribeevent.argtypes = as_c_types( [ 'char*', 'ld_callback' ] )
        self.fn_subscribeevent.restype = as_c_type( 'ld_handle' )

        self.fn_unsubscribeevent = shlib.Handle.LD_UnsubscribeEvent
        self.fn_unsubscribeevent.argtypes = as_c_types( [ 'ld_handle' ] )
        self.fn_unsubscribeevent.restype = as_c_type( 'int' )

    def Publish( self, uri, size, datatype ) :
        return self.fn_publish( uri, size, datatype )
    def Unpublish( self, field ) :
        self.fn_unpublish( field.handle )
    def Send( self, field, senddata ) :
        data = ( as_c_type( field.datatype ) * field.size )()
        data[:] = senddata
        self.fn_send( field.handle, data, field.size, field.datatype )

    def Subscribe( self, uri, transform ) :
        return self.fn_subscribe( uri, transform )
    def Unsubscribe( self, field ) :
        return self.fn_unsubscribe( field.handle )
    def Receive( self, field ) :
        data = ( as_c_type( field.datatype ) * field.size )()
        data[:] = [ 0.0 ] * field.size
        self.fn_receive( field.handle, data, field.size, field.datatype )
        return data

    def FieldSize( self, field ) :
        return self.fn_fieldsize( field.handle )


    def PublishEvent( self, eventname ) :
        return self.fn_publishevent( eventname )
    def UnpublishEvent( self, event ) :
        return self.fn_unpublishevent( event.handle )

    def SubscribeEvent( self, eventname, callback ) :
        return self.fn_subscribeevent( eventname, callback.hook )
    def UnsubscribeEvent( self, event ) :
        return self.fn_unsubscribeevent( event.handle )

    def SendEvent( self, event, data ) :
        return self.Send( event, data )


class LD_Field( object ) :
    def __init__( self, size, datatype ) :
        self.size = size ## >1 means vector field
        self.datatype = as_c_typename( datatype )
        self.handle = None
    def Size( self ) :
        return self.size
    def DataType( self ) :
        return self.datatype

class LD_PublishedField( LD_Field ) :
    def __init__( self, cpl, uri, size, datatype ) :
        super( LD_PublishedField, self ).__init__( size, datatype )
        self.cpl = cpl
        self.handle = self.cpl.Publish( uri, size, datatype )
        self.value = None
    def __del__( self ) :
        self.Release()
    def Release( self ) :
        if self.handle :
            self.cpl.Unpublish( self )
            self.handle = None

    @property
    def Value( self ) :
        return self.value
    def Send( self, data ) :
        self.value = data if isinstance( data, list ) else [ data ]
        self.cpl.Send( self, self.value )
        return data

LD_Ask = -1
class LD_SubscribedField( LD_Field ) :
    def __init__( self, cpl, uri, size, datatype, transform=None ) :
        super( LD_SubscribedField, self ).__init__( size, datatype )
        self.cpl = cpl
        self.handle = self.cpl.Subscribe( uri, transform )
        self.value = None
    def __del__( self ) :
        self.Release()
    def Release( self ) :
        if self.handle :
            self.cpl.Unsubscribe( self )
            self.handle = None

    @property
    def Value( self ) :
        self.value = self.Receive()
        return self.value
    def Receive( self ) :
        if self.size == LD_Ask :
            size = self.cpl.FieldSize( self )
            if size <= 0 :
                return None
            self.size = size
        data = self.cpl.Receive( self )
        if self.size == 1 :
            return data[0]
        return list( data )

class LD_PublishedEvent( LD_Field ) :
    def __init__( self, cpl, eventname ) :
        super( LD_PublishedEvent, self ).__init__( 0, 'char' )
        self.cpl = cpl
        self.handle = self.cpl.PublishEvent( eventname )
    def __del__( self ) :
        self.Release()
    def Release( self ) :
        if self.handle :
            self.cpl.UnpublishEvent( self )
            self.handle = None

    def Send( self, data ) :
        eventdata = [ c for c in data ] + [ '\0' ]
        self.size = len( eventdata )
        self.cpl.SendEvent( self, eventdata )
        return data

class LD_SubscribedEvent( LD_Field ) :
    def __init__( self, cpl, name, callback ) :
        super( LD_SubscribedEvent, self ).__init__( 0, 'char' )
        self.cpl = cpl
        self.callback = LD_EventCallback( callback )
        self.handle = self.cpl.SubscribeEvent( name, self.callback )
    def __del__( self ) :
        self.Release()
    def Release( self ) :
        if self.handle :
            self.cpl.UnsubscribeEvent( self )
            self.handle = None

class LD_Shared( object ) :
    def __init__( self, shlib ) :
        self.cpl = LD_Coupler( shlib )
        self.fields = list()

    @staticmethod
    def MakeUri( uri, spatial, temporal, unit ) :
        return '/%s?dS=%d&dT=%d&unit=%s' % ( uri, spatial, temporal, unit )

    def Publish( self, uri, size=1, unit='-', datatype='double', spatial=0, temporal=1 ) :
        fielduri = LD_Shared.MakeUri( uri, spatial, temporal, unit )
        field = LD_PublishedField( self.cpl, fielduri, size, datatype )
        self.fields.append( field )
        return field

    def Subscribe( self, uri, size=1, unit='-', datatype='double', spatial=0, temporal=1, transform='' ) :
        fielduri = LD_Shared.MakeUri( uri, spatial, temporal, unit )
        field = LD_SubscribedField( self.cpl, fielduri, size, datatype, transform )
        self.fields.append( field )
        return field

    @staticmethod
    def MakeEventUri( eventname, spatial ) :
        return '/%s?dS=%d' % ( eventname, spatial )

    def PublishEvent( self, eventname, spatial=0 ) :
        eventuri = LD_Shared.MakeEventUri( eventname, spatial )
        event = LD_PublishedEvent( self.cpl, eventuri )
        self.fields.append( event )
        return event

    def SubscribeEvent( self, eventname, callback, spatial=0 ) :
        eventuri = LD_Shared.MakeEventUri( eventname, spatial )
        event = LD_SubscribedEvent( self.cpl, eventuri, callback )
        self.fields.append( event )
        return event


    def ReleaseAll( self ) :
        while self.fields :
            field = self.fields.pop()
            field.Release()            

## convert python string array to c char pointer array
C2PyTypes = { 'bool':( cxpy.c_bool, bool, 'bool' ),
    'char':( cxpy.c_char, str, 'char' ), 'wchar':( cxpy.c_wchar, unicode, 'wchar' ),
    'byte':( cxpy.c_byte, int, 'char' ), 'ubyte':( cxpy.c_ubyte, int, 'uchar' ),
    'short':( cxpy.c_short, int, 'short' ), 'ushort':( cxpy.c_ushort, int, 'ushort' ),
    'int':( cxpy.c_int, int, 'int' ), 'uint':( cxpy.c_uint, int, 'uint' ),
    'long':( cxpy.c_long, long, 'long' ), 'ulong':( cxpy.c_ulong, long, 'ulong' ),
    'float':( cxpy.c_float, float, 'float' ), 'double':( cxpy.c_double, float, 'double' ),
    'char*':( cxpy.c_char_p, str, 'char*' ), 'wchar*':( cxpy.c_wchar_p, unicode, 'wchar*' ),
    'void*':( cxpy.c_void_p, None, 'void*' ),

	'ld_handle': ( LD_StructHandle, LD_StructHandle, '' ),
	'ld_callback': ( LD_StructCallback, LD_StructCallback, '' )
}

def as_c_typename( typename ) :
    if typename.endswith( '->' ) :
        return '%s*'% ( C2PyTypes[typename[:-2]][2] )
    return C2PyTypes[typename][2]
def as_c_type( typename ) :
    if typename.endswith( '->' ) :
        return cxpy.POINTER( C2PyTypes[typename[:-2]][0] )
    return C2PyTypes[typename][0]
def as_c_types( typenames ) :
    return [ as_c_type( typename ) for typename in typenames ]

## return tuple ( array_length, array )
def Py2C_StringArray( array ) :
    carray = ( cxpy.c_char_p * len( array ) )()
    carray[:] = array
    return len( array ), carray

