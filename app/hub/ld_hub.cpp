/*!
 * @author
 *    steffen klatt (created on: aug 19, 2018)
 */

#include  "ldndc.h"

#define LD_APIVERSION "0.1"

extern lerr_t ldndc_sim_initialize( int /*argc*/, char ** /*argv*/ );
extern lerr_t ldndc_sim_create();
extern lerr_t ldndc_sim_launch( cbm::td_scalar_t /*sse_end*/ );
extern void ldndc_sim_destroy( lerr_t );
extern void ldndc_sim_finalize();

extern int ldndc_kickstart( int /*argc*/, char ** /*argv*/);

#if defined(ldhub_EXPORTS)
#  define LD_API LDNDC_HELPER_DLL_EXPORT
#else
#  define LD_API LDNDC_HELPER_DLL_IMPORT
#endif

extern "C" {

/* creates and initializes simulation */
LD_API int LD_SimCreate( int _argc, char ** _argv)
{
    lerr_t rc = ldndc_sim_initialize( _argc, _argv );
    if ( rc==LDNDC_ERR_OK )
        { rc = ldndc_sim_create(); }
    return rc==LDNDC_ERR_OK ? 0 : -1;
}
/* launches simulation and returns at time _sse_end */
LD_API int LD_SimLaunch( uint64_t _sse_end )
{
    cbm::td_scalar_t sse_end =
        static_cast< cbm::td_scalar_t >( _sse_end );
    lerr_t rc = ldndc_sim_launch( sse_end );
    return rc==LDNDC_ERR_OK ? 0 : -1;
}
/* finalizes and destroys simulation */
LD_API void LD_SimDestroy( lerr_t  _rc )
{
    ldndc_sim_destroy( _rc );
    ldndc_sim_finalize();
}


/* initializes, launches and destroys simulation */
LD_API int LD_KickStart( int _argc, char ** _argv)
{
    return ldndc_kickstart( _argc, _argv);
}


/** helpers **/

/* return current in time in SSE */
LD_API uint64_t LD_CurrentTime()
{
    return LD_RtCfg.clk->sse();
}
/* return current dT in seconds */
LD_API uint64_t LD_CurrentTimestepDelta()
{
    return LD_RtCfg.clk->dt();
}

/* data exchange */

static CBM_Datatype LD_ResolveDatatype( char const * /*name*/ );

LD_API CBM_Handle LD_PublishField(
		char const * _uri, size_t _size, char const * _datatypename )
{
    CBM_Datatype const datatype = LD_ResolveDatatype( _datatypename );
    return CBM_CouplerPublishField( LD_RtCfg.cpl, _uri,
			NULL, _size, datatype, 0, NULL );
}
LD_API int LD_UnpublishField( CBM_Handle _handle )
{
	int rc = 0;
	CBM_CouplerUnpublishField( LD_RtCfg.cpl, _handle, &rc );
	return rc;
}
LD_API int LD_SendField( CBM_Handle _handle,
	   void * _data, size_t _size, char const * _datatypename )
{
	CBM_Datatype const datatype = LD_ResolveDatatype( _datatypename );
	if ( datatype.ID != CBM_VOID.ID )
	{
		int rc = 0;
		CBM_CouplerSendField( LD_RtCfg.cpl, _handle,
				_data, _size, datatype, 0, &rc );
		return rc;
	}
	return -1;
}

static CBM_Handle s_LD_SubscribeField( char const * _uri,
			char const * _transformname, CBM_Callback _callback )
{
    CBM_Transform * transform = CBM_TransformNoop;
    if ( _transformname==NULL || cbm::is_empty( _transformname ) )
            { /* none */ }
    else if ( cbm::is_equal( _transformname, "sum" ) )
        { transform = CBM_TransformSum; }
    else
        { return CBM_None; }

    return CBM_CouplerSubscribeField( LD_RtCfg.cpl, _uri,
			transform, _callback, 0, NULL );
}
LD_API CBM_Handle LD_SubscribeField(
        char const * _uri, char const * _transformname )
{
	return s_LD_SubscribeField( _uri, _transformname, CBM_CallbackNone );
}
LD_API int LD_UnsubscribeField( CBM_Handle _handle )
{
    int rc = 0;
    CBM_CouplerUnsubscribeField( LD_RtCfg.cpl, _handle, &rc );
    return rc;
}
LD_API int LD_ReceiveField( CBM_Handle _handle,
        void * _data, size_t _size, char const * _datatypename )
{
    CBM_Datatype const datatype = LD_ResolveDatatype( _datatypename );
    if ( datatype.ID != CBM_VOID.ID )
    {
        int rc = 0;
        CBM_CouplerReceiveField( LD_RtCfg.cpl, _handle,
                    _data, _size, datatype, 0, &rc );
        return rc;
    }
    return -1;
}

static int LD_PingPort( CBM_Handle _handle, CBM_FieldPingInfo * _buf )
{
    int const msgsize = CBM_CouplerPingField(
            LD_RtCfg.cpl, _handle, _buf, NULL );
    return msgsize;
}
LD_API int LD_FieldSize( CBM_Handle _handle )
{
    return LD_PingPort( _handle, NULL );
}

LD_API CBM_Handle LD_PublishEvent( char const * _name )
{
	return LD_PublishField( _name, CBM_DynamicSize, "char" );
}
LD_API int LD_UnpublishEvent( CBM_Handle _handle )
{
	return LD_UnpublishField( _handle );
}
LD_API CBM_Handle LD_SubscribeEvent(
		char const * _name, CBM_Callback _callback )
{
	return s_LD_SubscribeField( _name, NULL, _callback );
}
LD_API int LD_UnsubscribeEvent( CBM_Handle _handle )
{
	return LD_UnsubscribeField( _handle );
}

static CBM_Datatype LD_ResolveDatatype( char const * _name )
{
    if ( cbm::is_equal( _name, "float" ) )
        { return CBM_FLOAT; }
    else if ( cbm::is_equal( _name, "double" ) )
        { return CBM_DOUBLE; }
    else if ( cbm::is_equal( _name, "char" ) )
        { return CBM_CHAR; }
    else if ( cbm::is_equal( _name, "uchar" ) )
        { return CBM_UCHAR; }
    else if ( cbm::is_equal( _name, "short" ) )
        { return CBM_SHORT; }
    else if ( cbm::is_equal( _name, "ushort" ) )
        { return CBM_USHORT; }
    else if ( cbm::is_equal( _name, "int" ) )
        { return CBM_INT; }
    else if ( cbm::is_equal( _name, "uint" ) )
        { return CBM_UINT; }
    else if ( cbm::is_equal( _name, "long" ) )
        { return CBM_LONG; }
    else if ( cbm::is_equal( _name, "ulong" ) )
        { return CBM_ULONG; }
    else
        { return CBM_VOID; }
}

} /* extern "C" */

