/*!
 * @brief
 *    common functions shared by ldndc applications
 *
 * @author
 *    steffen klatt (created on: 20 sep., 2013),
 *    david kraus,
 *    edwin haas
 */

#ifndef  LDNDC_INIT_H_
#define  LDNDC_INIT_H_


#include  "kernels/ld_kernel.h"

#include  <prjfile/cbm_prjfile.h>

extern int  ldndc_initialize( int, char **);
extern int  ldndc_finalize();

extern lerr_t  ldndc_define_variables(
    char ** /*variables [name=value]*/, int /*number of variables*/);

extern std::string  ldndc_logline_prefix();

extern lerr_t  ldndc_set_ldndc_dot_path();

extern lerr_t  ldndc_open_configuration( char const * /*conf. filename*/);

extern lerr_t  ldndc_set_environment_from_configurationfile();

extern lerr_t  ldndc_set_libconf_from_configurationfile();

extern lerr_t  ldndc_set_libconf_from_projectfile(
    char const * /*project filename*/);

extern lerr_t  ldndc_set_schedule( char const * /*schedule*/);
extern lerr_t  ldndc_set_kernel_factories( void const * /*registered factories*/);

extern lerr_t ldndc_read_Lresources( char const * = NULL);

extern lerr_t ldndc_check_version_required();

#include  <time/cbm_time.h>
#include  <cbm_project.h>
#include  <io-dcomm.h>
#include  <io/source-info.h>
template < typename  _IC >
lerr_t
ldndc_get_input(
        _IC *  _ic, cbm::source_descriptor_t const *  _sd,
        cbm::io_dcomm_t *  _io_dcomm)
{
    _IC *  ic = static_cast< _IC * >(
            _io_dcomm->new_input_class( _IC::iclassname(), _sd));
    if ( _ic && ic && ic->is_initialized())
        { *_ic = *ic; }
    else
    {
        LOGERROR( "failed to read source with  [id=",*_sd,"]");
        return  LDNDC_ERR_FAIL;
    }
    if ( ic)
        { _io_dcomm->delete_input_class( ic); }
    return  LDNDC_ERR_OK;
}
template < typename  _IC >
lerr_t
ldndc_get_input(
        _IC *  _ic, lid_t  _id, cbm::io_dcomm_t *  _io_dcomm)
{
    cbm::source_descriptor_t  ssi;
    cbm::set_source_descriptor_defaults( &ssi, _IC::iclassname(), _id);
    _IC *  ic = static_cast< _IC * >(
            _io_dcomm->new_input_class( _IC::iclassname(), &ssi));
    if ( _ic && ic && ic->is_initialized())
        { *_ic = *ic; }
    else
    {
        LOGERROR( "failed to read source with  [id=",_id,"]");
        return  LDNDC_ERR_FAIL;
    }
    if ( ic)
        { _io_dcomm->delete_input_class( ic); }
    return  LDNDC_ERR_OK;
}
template < typename  _IC >
lerr_t
ldndc_open_source(
        _IC *  _ic, lid_t  _id, cbm::project_t *  _project, cbm::io_dcomm_t *  _io_dcomm,
        char const *  _sourcename, char const *  _ioformat, char const *  _source_identifier = NULL)
{
    ldndc::source_info_t  source_info( _IC::iclassname());
    source_info.name = _sourcename;
    if ( _ioformat)
        { source_info.format = _ioformat; }
    else
        { source_info.format = cbm::string_t( strrchr( _sourcename, '.')
                ? strrchr( _sourcename, '.') + 1 : ""); /*try suffix*/}
    if ( _source_identifier)
    {
        source_info.identifier = _source_identifier;
    }

    lid_t  s_read = _project->attach_source( &source_info);
    if ( s_read == invalid_lid)
    {
        LOGERROR( "failed opening source  [source=",source_info.name,"]");
        return  LDNDC_ERR_FAIL;
    }

    if ( _io_dcomm)
    {
        if ( _io_dcomm->set_project( _project) != LDNDC_ERR_OK)
            { return  LDNDC_ERR_FAIL; }
        _io_dcomm->update_io();

        /* retrieve input class */
        if ( _ic)
        {
            lerr_t  rc_get = ldndc_get_input< _IC >( _ic, _id, _io_dcomm);
            if ( rc_get)
                { return  LDNDC_ERR_FAIL; }
        }
    }

    return  LDNDC_ERR_OK;
}
template < typename  _IC >
void
ldndc_close_source( cbm::io_dcomm_t *)
{ }
 
#endif  /*  LDNDC_INIT_H_  */

