/*!
 * @brief
 *
 * @author
 *    steffen klatt (created on: apr 02, 2014),
 *    edwin haas
 */

#ifndef  LDNDC_LANDSCAPEDNDC_H_
#define  LDNDC_LANDSCAPEDNDC_H_

#include  "ldndc.h"

#include  <domain.h>
#include  <kernel/kstats.h>
#include  <time/cbm_time.h>

struct  ldndc_args_info_t;

namespace cbm {
    class  work_dispatcher_t;
}
namespace ldndc {
    class  project_file_t;
    class  config_file_t;
}

namespace ldndc {

enum  llandscapedndc_status_e
{
    LLDNDC_STATUS_READCONFIGFAIL = -1,
    LLDNDC_STATUS_INITLOGFAIL = -2,
    LLDNDC_STATUS_READPROJECTFAIL = -3,
    LLDNDC_STATUS_READLRESOURCESFAIL = -4,
    LLDNDC_STATUS_READUNITSFAIL = -5,

    LLDNDC_STATUS_OPENPROJECTFAIL = -6,
    LLDNDC_STATUS_INITPROJECTFAIL = -7,
    LLDNDC_STATUS_RESTORECHECKPOINTFAIL = -8,

    LLDNDC_STATUS_SETSCHEDULEFAIL = -0,
    LLDNDC_STATUS_VERSIONREQUIREDFAIL = -10,

    LLDNDC_STATUS_OK = 0
};
extern char const * const  llandscapedndc_status_msg[11];
extern void explain_error( int /*status*/);

struct llandscapedndc_t
{
public:
    static void  shutdown_log();

    llandscapedndc_t();
    ~llandscapedndc_t();

    int  initialize(
        char const * /*project filename*/, char const * /*schedule*/,
        ldndc_args_info_t const * /*command line arguments*/,
        char const * = NULL /*configuration filename*/,
        cbm::work_dispatcher_t * = NULL /*work dispatcher*/);

// sk:todo  implement
    lerr_t  reset()
        { return  LDNDC_ERR_FAIL; }

    lerr_t  launch( cbm::td_scalar_t = 0 /*sse when to stop*/);

    void  finalize( ldndc_args_info_t const * /*command line arguments*/);

    char const *  get_status_message() const;

    size_t  size() const;
    size_t  number_of_kernels() const;
    size_t  number_of_valid_kernels() const;
    size_t  number_of_active_kernels() const;

    cbm::sclock_t const *  lclock() const;

    bool  have_kernel_resource_statistics() const
        { return this->m_domain.have_kernel_resource_statistics(); }
    int  cputime_summary( cbm::cputime_summary_t * _cs) const
        { return this->m_domain.cputime_summary( _cs); }
    int  walltime_summary( cbm::walltime_summary_t * _ws) const
        { return this->m_domain.walltime_summary( _ws); }
    void  memory_summary( cbm::memory_summary_t * _ms) const
        { this->m_domain.memory_summary( _ms); }

    int kernel_resource_stats( cbm::kernels_stats_t & _kstats) const
        { return this->m_domain.kernel_resource_stats( _kstats); }

#ifdef  _HAVE_SERIALIZE
    int  create_checkpoint();
    int  restore_checkpoint( ldate_t);
#endif

#ifdef  _LDNDC_HAVE_ONLINECONTROL
    int  process_request(
            lreply_t *  _reply,  lrequest_t const *  _request)
        { return  this->m_domain.process_request( _reply, _request); }
#endif

private:
    cbm::domain_t  m_domain;
    int  m_status;

    lerr_t  read_configuration( char const * /*configuration filename*/);
    lerr_t  initialize_log( config_file_t const * /*cfg*/);
    lerr_t  read_Lresources(
            char const * = NULL /*Lresources filename*/);
    lerr_t  read_projectfile(
            char const * /*project filename*/);
    lerr_t  check_version_required();
    lerr_t  set_schedule( char const * /*schedule*/);
    lerr_t  initialize_region( lid_t const & /*pid*/,
                    cbm::io_dcomm_t *, cbm::work_dispatcher_t *);

    void  set_status(
            int /*new status only set if current status is 0*/);
};

}

#endif  /*  !LDNDC_LANDSCAPEDNDC_H_  */

