
#ifndef  LDNDC_VERSION_H_
#define  LDNDC_VERSION_H_

#include  "ldndc-config.h.inc"
#include  "ldndc-dllexport.h"

extern LDNDC_API char const *  ldndc_package_name();
extern LDNDC_API char const *  ldndc_package_name_short();

/* LandscapeDNDC version:
 *
 * Format: MAJOR.MINOR.PATCHLEVEL
 *
 * MAJOR:  development stage: concept changes,
 *         large code changes
 * MINOR:  extensions in inputs/outputs, accepts 
 *         older inputs and contains previous outputs
 *         changes in model results
 * PATCHLEVEL:
 *         indicate bug fixes, required back-ports
 */

struct LDNDC_API ldndc_package_version_t
{
    ldndc_package_version_t();
    int  from_string(
        char const *  /*pkg version string*/);
    bool  operator<( ldndc_package_version_t const &);
    bool  operator<=( ldndc_package_version_t const &);
    bool  operator==( ldndc_package_version_t const &);
    bool  operator!=( ldndc_package_version_t const &);
    bool  operator>=( ldndc_package_version_t const &);
    bool  operator>( ldndc_package_version_t const &);

    int  versionmajor, versionminor, patchlevel;
};
extern LDNDC_API char const *  ldndc_package_version();
extern LDNDC_API char const *  ldndc_package_version_long();
extern LDNDC_API int  ldndc_package_version_major();
extern LDNDC_API int  ldndc_package_version_minor();
extern LDNDC_API int  ldndc_package_version_patchlevel();
extern LDNDC_API ldndc_package_version_t  ldndc_package_versions();

extern LDNDC_API char const *  ldndc_package_revision();
extern LDNDC_API int  ldndc_package_revision_number();

extern LDNDC_API char const *  ldndc_package_build_timestamp();
extern LDNDC_API char const *  ldndc_package_build_configuration();

#endif  /*  !LDNDC_VERSION_H_  */


