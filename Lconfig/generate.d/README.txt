
all files having a filename according to format given below are run
by passing them to CMake's execute_process() command which calls
CMake in "script mode" (-P). the order of execution is determined
by the three digit (zero padded) prefix, i.e. generators are sorted
in ascending order.

filename format ( <D> is a digit (0,...,9)):

        "DDD_<somename>.cmake-inc"

