# vim: ft=cmake
# ====================================================================================
#
#  compiler settings for CLang (C++ compiler front-end for LLVM)
#
# =====================================================================================

# set release or debug build type and respective compiler flags
if( NOT LDNDC_RELEASE)
    set( CMAKE_COLOR_MAKEFILE ON)
    ## TODO  add -Weverything
    set( CMAKE_C_FLAGS_DEBUG "${CMAKE_C_FLAGS_DEBUG} -Werror -pedantic-errors")
    set( CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -Werror -pedantic-errors")
    set( CMAKE_C_FLAGS_DEBUG "${CMAKE_C_FLAGS_DEBUG} -O0 -g -g3 -ggdb")
    set( CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -O0 -g -g3 -ggdb")
    if( LDNDC_USE_PROFILER)
        set( CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -p -pg")
        set( CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -ftime-report -fmem-report -rdynamic")
    endif()
else()
    set( CMAKE_C_FLAGS_RELEASE "${CMAKE_C_FLAGS_RELEASE} -Ofast -ffast-math")
    set( CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -Ofast -ffast-math")
endif( NOT LDNDC_RELEASE)

set( CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Wno-unused-command-line-argument")
set( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wno-unused-command-line-argument")

if( LDNDC_NATIVE_ARCH)
    set( CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -march=native" )
    set( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -march=native" )
endif()

## exporting symbols
set( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fvisibility=hidden" ) # -fvisibility-inlines-hidden

## compiler flags we generally want 
set( CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -std=c99")
if( LDNDC_CXX_STD98)
    set( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++98")
elseif( LDNDC_CXX_STD11)
    set( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")
elseif( LDNDC_CXX_STD14)
    set( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++14")
else()
    set( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")
endif()

if( MINGW OR LDNDC_STATIC)
    set( CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -static-libgcc -static")
    set( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -static-libgcc -static")
    if( MINGW)
        set( CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Wno-pedantic-ms-format")
        set( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wno-pedantic-ms-format")
    endif()
endif()

if( NOT MINGW)
    set( CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -fPIC")
    set( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fPIC")
endif( NOT MINGW)

## warnings
set( CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Wno-system-headers")
set( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wno-system-headers")

set( CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Wall -Wextra -pedantic -Wfatal-errors")
set( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra -pedantic -Wfatal-errors")

set( CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Wmissing-include-dirs")
set( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wmissing-include-dirs")

set( CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Wuninitialized -Wshadow -Wcast-align -Wchar-subscripts -Wfloat-equal")
set( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wnon-virtual-dtor -Wuninitialized -Wshadow -Wcast-align -Wchar-subscripts -Wfloat-equal")

set( CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Wno-variadic-macros -Wno-long-long")
set( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wno-variadic-macros -Wno-long-long")

set( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wdisabled-optimization -Wno-error=disabled-optimization")

set( CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Wunused")
set( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wunused")
set( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wunused-private-field")
set( CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Wunused-function")
set( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wunused-function")
set( CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Wunused-parameter")
set( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wunused-parameter")
set( CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Wunused-variable")
set( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wunused-variable")
#   set( CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Qunused-arguments")
#   set( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Qunused-arguments")

#set( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wconversion -Wno-error=conversion -Wbool-conversions -Wno-error=bool-conversions")
#set( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Winline -Wno-error=inline")
#set( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wpadded -Wno-error=padded")

## sk:?? set( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fbounds-check")
set( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fdiagnostics-show-option") # -fno-diagnostics-show-name")

set( CMAKE_INCLUDE_SYSTEM_FLAG_CXX "-isystem ")
set( CMAKE_INCLUDE_SYSTEM_FLAG_C "-isystem ")

