#!/bin/bash

## TODO
##      add support for configuration file (see __default_* settings)
##      add support for core/column selection per file

HELPLINE0=$(( $LINENO))
## synopsis
##      ./<plot.bin>  [<cores>@]<column[s]>  <output format[=filename]>  <file1[=title]> [<file2[=title]> [...]]

## column (C) and cores (S) formats
##      C               plot column (core) C
##      C0,C1,...       plot columns (cores) C0, C1
##      C0:C1           plot columns (cores) C0 through C1
##      C0:-            plot columns (core) C0 through last available column (core)
##
## combinations are possible:  e.g. "C0,C1:C2,C3:C4,C6:-,C5"
## note that for core ranges (i.e. S1:S2) only those existing are considered)

## output formats
##      x11     to screen (file name is ignored)
##      eps     create eps
##      pdf     create pdf
HELPLINE1=$(( $LINENO - 1))


if [ $# -lt 3 ]
then
        echo -e "not enouch argument\n\n" 1>&2
        head -n$HELPLINE1 $0 | tail -n$((HELPLINE1 - HELPLINE0))  1>&2
        exit 1
fi


## set defaults
__default_plot_eps_outfile="./plot-output"
__default_plot_pdf_outfile="./plot-output"
__default_plot_x11_size="3600,1160"
__default_range_cid_column="1"
__default_range_cid_header_idx="0"

## first file is used to extract column headers
LF_="`echo ${3} | sed 's/=.*$//'`"
if [ ! -r "$LF_" ]
then
        echo -e "cannot read file  [file=$LF_]" 1>&2
        exit 2
fi
H_LIST=(`head -n1 "$LF_"`)
H_CNT=${#H_LIST[@]}

## split core and column list
P_RANGE=(`echo "$1" | tr '@' ' '`)
C_LIST_=""
S_LIST_=""
if [ ${#P_RANGE[@]} -gt 1 ]
then
        S_LIST_=(`echo "${P_RANGE[0]}" | tr ',' ' '`)
        C_LIST_=(`echo "${P_RANGE[1]}" | tr ',' ' '`)

        S_MAX=`cut -f${__default_range_cid_column} "$LF_" | sort -n | uniq | tail -n1`

        ## get core list
        S_LIST=()
        cid_list=(`cut -f${__default_range_cid_column} "$LF_" | sort -n | uniq`)
        for s in ${S_LIST_[@]}
        do
                s_seq=($(echo "$s:$s" | tr ':' ' '))
                if [ "${s_seq[1]}X" = "-X" ]
                then
                        s_seq=( ${s_seq[0]} ${S_MAX})
                fi

                for cid in ${cid_list[@]}
                do
                        if [ $cid = ${H_LIST[$__default_range_cid_header_idx]} ]
                        then
                                continue
                        fi

                        if [ $cid -ge ${s_seq[0]} ]
                        then
                                if [ $cid -le ${s_seq[1]} ]
                                then
                                        S_LIST+=($cid)
                                else
                                        break
                                fi
                        fi
                done
        done
else
        C_LIST_=(`echo "${P_RANGE[0]}" | tr ',' ' '`)
        S_LIST=( -1)
fi
echo "core-list: ${S_LIST[@]}" 1>&2
S_CNT=${#S_LIST[@]}


## get column list
C_LIST=()
for col in ${C_LIST_[@]}
do
        col_seq=($(echo "$col:$col" | tr ':' ' '))
        if [ "${col_seq[1]}X" = "-X" ]
        then
                col_seq[1]=$H_CNT
        fi
        C_LIST+=(`seq ${col_seq[0]} ${col_seq[1]}`)
done
echo "column-list: ${C_LIST[@]}" 1>&2
C_CNT=${#C_LIST[@]}


## get output format
O_FMT="$2"
## TODO  check for valid format

## remaining arguments are input files
shift 2
F_LIST=($@)

## find "optimal" grid
if [ $(( ( $C_CNT / 2 ) * 2)) -eq $C_CNT ]
then
        :
        #echo  "$C_CNT even" 1>&2
else
        #echo  "$C_CNT odd" 1>&2
        if [ $C_CNT -gt 7 ]
        then
                LI2=$(( $C_CNT / 2))
                is_prime=1
                for li in `seq $LI2 -1 2`
                do
                        if [ $(( ( $C_CNT / $li ) * $li)) -eq $C_CNT ]
                        then
                                is_prime=0
                                break
                        fi
                done

                if [ $is_prime -eq 1 ]
                then
                        echo "graph count is prime .. using next integer" 1>&2
                        C_CNT=$(( $C_CNT + 1))
                fi
        fi
fi

SI_X=0
SI_Y=0

ERR=$C_CNT
for m in `seq 1 $C_CNT`
do
        for n in `seq 1 $m`
        do
                N=$(( $m * $n))
                ERR_N=$(( $N - $C_CNT))
                [ $ERR_N -lt 0 ] && continue

                if [ $ERR_N -eq $ERR ]
                then
                        if [ $(( $SI_Y * $n)) -gt $(( $SI_X * $m)) ]
                        then
                                SI_X=$n
                                SI_Y=$m
                                echo "m=$m, n=$n  (err=$ERR,better ratio)" 1>&2
                        fi
                fi

                if [ $ERR_N -lt $ERR ]
                then
                        ERR=$ERR_N
                        SI_X=$n
                        SI_Y=$m
                        echo "m=$m, n=$n  (err=$ERR)" 1>&2
                fi
        done
done
SI_GRID=$(( $SI_X * $SI_Y))

case "$O_FMT" in
        x11*)
                if [ $SI_GRID -gt 1 ]
                then
                        echo "set terminal x11 reset"
                        echo "set terminal x11 noenhanced size ${__default_plot_x11_size}"
                fi
                ;;
        eps*)
                opts=(`echo $O_FMT | sed 's/^.*=//' | tr ',' ' '`)
                echo "set terminal postscript default"
                echo "set terminal postscript eps noenhanced color colortext size $(($SI_X * 12))cm,$(($SI_Y * 6))cm"
                opt_filename="${opts[0]}"
                if [ "$O_FMT" = "eps" -o -z "$opt_filename" ]
                then
                        opt_filename="${__default_plot_eps_outfile}.eps"
                fi
                echo -e "set output \"$opt_filename\""

                ;;
        pdf*)
                opts=(`echo $O_FMT | sed 's/^.*=//' | tr ',' ' '`)
                echo "set terminal pdfcairo noenhanced color size $(($SI_X * 9))in,$(($SI_Y * 4))in"
                opt_filename="${opts[0]}"
                if [ "$O_FMT" = "pdf" -o -z "$opt_filename" ]
                then
                        opt_filename="${__default_plot_pdf_outfile}.pdf"
                fi
                echo -e "set output \"$opt_filename\""

                ;;
        *)
                echo "output terminal not understood  [terminal=$O_FMT]" 1>&2
                exit 3
                ;;
esac


if [ $SI_GRID -gt 1 ]
then
        echo "set multiplot layout $SI_Y,$SI_X scale 1,1 title \"\" "
        #echo "set format y \"%g\""
        echo "set lmargin 14"
        echo "set bmargin 4"
        echo "set tmargin 0"
        echo "set rmargin 6"
fi

echo "set grid"
echo "set tics out nomirror scale 0.25"
echo "set border 3"

COMMA=""
C_CNT__=${C_CNT}

for k in ${C_LIST[@]}
do
        K=$(( k - 1))

        if [ $k -gt $H_CNT ]
        then
                echo  "column $k not available in input file" 1>&2
                C_CNT__=$(( $C_CNT__ - 1 ))
                continue
        fi

        echo  "adding plot ... $k (${H_LIST[$K]})" 1>&2

        #echo -en "set title \"${H_LIST[$K]}\" offset graph 0,1\n"
        echo -en "set ylabel \"${H_LIST[$K]}  <$k>\" noenhanced\n"
        echo -en "set key top right horizontal box Left samplen 0.5 reverse\n"
        echo -en "plot "

        p=0
        for f in ${F_LIST[@]}
        do
                ## let gnuplot do the file read check ... ;-)

                ## extract title if available
                f_=(`echo "$f" | tr '=' ' '`)
                f_data="${f_[0]}"
                f_title="${f_[1]:-$f_data}"

                if [ ! -r "$f_data" ]
                then
                        echo  "file not readable  [file=$f_data]" 1>&2
                        exit  -2
                fi

                for s in ${S_LIST[@]}
                do
                        if [ $s -ne -1 ]
                        then
                                f_title_s="$f_title ($s)"
                                f_data_s="< egrep '^$s[[:space:]]' $f_data"
                        else
                                f_title_s="$f_title"
                                f_data_s="$f_data"
                        fi

                        ## special formating for first and second plot
                        if [ $p -eq 0 ]
                        then
                                echo -en "\"$f_data_s\" u $k with lines linecolor 1 linewidth 1.5 title \"$f_title_s \""
                                COMMA=','
                        
                        elif [ $p -eq 1 ]
                        then
                                echo -en "$COMMA \"$f_data_s\" u $k with points pointsize 0.3 linecolor 3 title \"$f_title_s \""
                        else
                                echo -en "$COMMA \"$f_data_s\" u $k with points pointsize 0.3 linecolor $((p + 3)) title \"$f_title_s \""
                        fi

                        p=$(( p + 1 ))
                done
        done

        echo ""
done


if [ $SI_GRID -gt 1 ]
then
        echo -e "unset multiplot"
fi
echo  "unset output"


echo "#graphs=${C_CNT__}, rows=$SI_X, columns=$SI_Y", size=$SI_GRID 1>&2

