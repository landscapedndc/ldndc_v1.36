#!/bin/bash

infile="$1"
if [ -z "$infile" ]
then
        echo "specify text table input file"
        exit 1
fi

for h in `head -n1 "$infile"`
do
        echo $h
done | cat -n

