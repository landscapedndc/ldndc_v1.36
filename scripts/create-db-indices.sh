
db_soil="$1"
db_phys="$2"

sqlite3 "$db_soil" 'create index soilchemistrysubdaily_timestep_index ON soilchemistrysubdaily (timestep);'
sqlite3 "$db_soil" 'create index soilchemistrydaily_timestep_index ON soilchemistrydaily (timestep);'

sqlite3 "$db_soil" 'create index microclimatedaily_timestep_index ON microclimatedaily (timestep);'

sqlite3 "$db_soil" 'create index watercycledaily_timestep_index ON watercycledaily (timestep);'

sqlite3 "$db_soil" 'create index soilchemistrylayersubdaily_timestep_layer_index ON soilchemistrylayersubdaily (timestep,layer);'
sqlite3 "$db_soil" 'create index soilchemistrylayerdaily_timestep_layer_index ON soilchemistrylayerdaily (timestep,layer);'
sqlite3 "$db_soil" 'create index watercyclelayerdaily_timestep_layer_index ON watercyclelayerdaily (timestep,layer);'
sqlite3 "$db_soil" 'create index watercyclelayersubdaily_timestep_layer_index ON watercyclelayersubdaily (timestep,layer);'
sqlite3 "$db_soil" 'create index physiologylayerdaily_timestep_index ON physiologylayerdaily (timestep,layer);'



sqlite3 "$db_phys" 'create index physiologydaily_timestep_index ON physiologydaily (timestep);'
sqlite3 "$db_phys" 'create index vegstructuredaily_timestep_index ON vegstructuredaily (timestep);'

