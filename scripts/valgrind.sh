#!/bin/bash

ldndcbin="${1:-"NONE"}"
if [ "$ldndcbin" = "NONE" ]
then
    printf "usage: valgrind.sh <ldndc command line>\n" >&2
    exit 1
fi

valgrind --leak-check=full --show-reachable=yes --error-limit=no --track-origins=yes --gen-suppressions=all \
    --suppressions=share/valgrind-suppressions.supp $@

