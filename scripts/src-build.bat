
@echo OFF

if "%1"=="release" goto Release
if "%1"=="Release" goto Release
if "%1"=="" goto Release
goto Debug


:Release
cmake --build . --config Release
goto END

:Debug
cmake --build . --config Debug
goto END


:END
