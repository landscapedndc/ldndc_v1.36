/*!
 * @brief
 *    set our own macros for compilers
 *
 * @author
 *    steffen klatt (created on: sep 02, 2012)
 */

#ifndef  LDNDC_COMPILERS_H_
#define  LDNDC_COMPILERS_H_

/** compiler detection **/
#include  "crabmeat-compiler.h"

/* clang (llvm), note: have before gcc because clang also identifies as gcc.. */
#if  defined(CRABMEAT_COMPILER_CLANG)
#  define  LDNDC_COMPILER_CLANG
/* pgi */
#elif defined(CRABMEAT_COMPILER_PGI)
#  define  LDNDC_COMPILER_PGI
/* gnu c/c++ */
#elif defined(CRABMEAT_COMPILER_GCC)
#  define  LDNDC_COMPILER_GCC
/* intel */
#elif defined(CRABMEAT_COMPILER_INTEL)
#  define  LDNDC_COMPILER_INTEL
/* ms c */
#elif defined(CRABMEAT_COMPILER_MSC)
#  define  LDNDC_COMPILER_MSC
/* ? */
#else
        /* we wish you the best of luck */
#endif

/* mingw */
#ifdef  CRABMEAT_MINGW
#  define  LDNDC_MINGW
#endif

#endif /* !LDNDC_COMPILERS_H_ */

