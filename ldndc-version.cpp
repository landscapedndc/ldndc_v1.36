
#include  "ldndc-version.h"

#define  numeric_to_string(__numeric__)  #__numeric__
#define  as_string(__numeric__)  numeric_to_string(__numeric__)

#define  LDNDC_PACKAGE_VERSION_LONG  LDNDC_PACKAGE_VERSION " (revision " as_string(LDNDC_PACKAGE_REVISION) ")  " LDNDC_PACKAGE_BUILD_TIMESTAMP

ldndc_package_version_t::ldndc_package_version_t()
        : versionmajor(-1), versionminor(-1), patchlevel(-1)
{ }
#include  "string/cbm_string.h"
int  ldndc_package_version_t::from_string(
                char const *  _pkg_version_string)
{
    if ( !_pkg_version_string ||
            cbm::is_empty( _pkg_version_string))
        { return  -2; }
    cbm::string_t  pkg_version_str( _pkg_version_string);
    cbm::string_t::int32_array_t  pkg_v =
        pkg_version_str.as_int32_array( '.');
    if (( pkg_v.size() > 0) && ( pkg_v.size() < 4))
    {
        this->versionmajor = pkg_v[0];
        if ( pkg_v.size() > 1)
        {
            this->versionminor = pkg_v[1];
            if ( pkg_v.size() > 2)
            {
                this->patchlevel = pkg_v[2];
            }
            else
            {
                this->patchlevel = 0;
            }
        }
        else
        {
            this->versionminor = 0;
            this->patchlevel = 0;
        }

        return  0;
    }
    else
    {
        /* error */
        return  -1;
    }
}
bool ldndc_package_version_t::operator<(
        ldndc_package_version_t const & _pkg_version)
{
    if ( this->versionmajor < _pkg_version.versionmajor)
        { return true; }
    else
    {
        if ( this->versionmajor == _pkg_version.versionmajor)
        {
            if ( this->versionminor < _pkg_version.versionminor)
                { return true; }
            else
            {
                if ( this->versionminor == _pkg_version.versionminor)
                {
                    if ( this->patchlevel < _pkg_version.patchlevel)
                        { return true; }
                }
            }
        }
    }
    return false;
}
bool ldndc_package_version_t::operator<=(
        ldndc_package_version_t const &  _pkg_version)
{
    return this->operator<( _pkg_version) || this->operator==( _pkg_version);
}
bool ldndc_package_version_t::operator==(
        ldndc_package_version_t const &  _pkg_version)
{
    return ( this->versionmajor == _pkg_version.versionmajor) &&
        ( this->versionminor == _pkg_version.versionminor) && ( this->patchlevel == _pkg_version.patchlevel);
}
bool ldndc_package_version_t::operator!=(
        ldndc_package_version_t const &  _pkg_version)
{
    return  ! this->operator==( _pkg_version);
}
bool ldndc_package_version_t::operator>=(
        ldndc_package_version_t const &  _pkg_version)
{
    return  ! this->operator>( _pkg_version);
}
bool ldndc_package_version_t::operator>(
        ldndc_package_version_t const &  _pkg_version)
{
    return  ! this->operator<=( _pkg_version);
}

#include  "ldndc-version.h.inc"
char const *  ldndc_package_name()
    { return LDNDC_PACKAGE_NAME; }
char const *  ldndc_package_name_short()
    { return LDNDC_PACKAGE_NAME_SHORT; }

char const *  ldndc_package_version()
    { return LDNDC_PACKAGE_VERSION; }
int  ldndc_package_version_major()
    { return LDNDC_PACKAGE_MAJOR_VERSION; }
int  ldndc_package_version_minor()
    { return LDNDC_PACKAGE_MINOR_VERSION; }
int  ldndc_package_version_patchlevel()
    { return LDNDC_PACKAGE_PATCHLEVEL; }
ldndc_package_version_t  ldndc_package_versions()
{
    ldndc_package_version_t  pkg_version;
    pkg_version.versionmajor = ldndc_package_version_major();
    pkg_version.versionminor = ldndc_package_version_minor();
    pkg_version.patchlevel = ldndc_package_version_patchlevel();
    return  pkg_version;
}

char const *  ldndc_package_build_timestamp()
    { return LDNDC_PACKAGE_BUILD_TIMESTAMP; }

#include  "ldndc-revision.h.inc"
char const *  ldndc_package_revision()
    { return as_string(LDNDC_PACKAGE_REVISION); }
int  ldndc_package_revision_number()
    { return LDNDC_PACKAGE_REVISION; }
char const *  ldndc_package_version_long()
    { return LDNDC_PACKAGE_VERSION_LONG; }

#include  "ldndc-build-configuration.h.inc"
char const *
ldndc_package_build_configuration()
    { return LDNDC_PACKAGE_BUILD_CONFIGURATION; }

#undef  numeric_to_string
#undef  as_string

