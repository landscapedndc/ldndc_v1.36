/*!
 * @brief
 *    Draws a simple progressbar to stdout
 *
 * @author
 *    steffen klatt (created on: aug 01, 2017)
 */

#ifndef  LDNDC_KERNEL_LDPROGRESSBAR_H_
#define  LDNDC_KERNEL_LDPROGRESSBAR_H_

/* select progress bar style:
 *  1 := 0% .... 5% ..          (.:=default,.:=growing season)
 *  2 := <date> [===OO=  ] x%   (=:=default,O:=growing season)
 */
#define  LD_ProgressBarStyle 2

#include  "ld_kernel.h"
#include  "ld_shared.h"

namespace ldndc {

class LDNDC_API LK_ProgressBar : public cbm::kernel_t
{
    LDNDC_KERNEL_OBJECT(LK_ProgressBar,progressbar)
    public:
        LK_ProgressBar();
        ~LK_ProgressBar();

    public:
        lerr_t  configure( cbm::RunLevelArgs *);
        lerr_t  initialize( cbm::RunLevelArgs *);
        lerr_t  write( cbm::RunLevelArgs *);
        lerr_t  finalize( cbm::RunLevelArgs *);

        lerr_t  register_ports( cbm::RunLevelArgs *);
        lerr_t  unregister_ports( cbm::RunLevelArgs *);

    private:
        /*interval counter*/
        cbm::td_scalar_t  m_Calls;

    private:
        int  m_NbSpecies;
        SubscribedEvent<LD_EventHandlerCall<LK_ProgressBar> >  m_PlantEvent;
        SubscribedEvent<LD_EventHandlerCall<LK_ProgressBar> >  m_HarvestEvent;
    public:
        int  onPlant( ldndc::EventAttributes const *);
        int  onHarvest( ldndc::EventAttributes const *);

    private:
        /* hide these buggers for now */
        LK_ProgressBar( LK_ProgressBar const &);
        LK_ProgressBar &  operator=( LK_ProgressBar const &);
};
} /* namespace ldndc */

#endif /* !LDNDC_KERNEL_LDPROGRESSBAR_H_ */

