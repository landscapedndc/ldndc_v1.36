/*!
 * @author
 *    steffen klatt (created on: aug 01, 2017)
 */

#include  "progressbar/ld_progressbar.h"

#include  <time/cbm_time.h>
#include  <string/cbm_string.h>
#include  <logging/cbm_logging.h>
#include  <utils/cbm_utils.h>

#define  PB_FINAL  0x0001
#define  PB_GROW   0x0002
#define  PB_INTR   0x0004
#define  PB_ERROR  0x0008

LDNDC_KERNEL_OBJECT_DEFN(LK_ProgressBar,progressbar,"_LD_ProgressBar","ProgressBar")
ldndc::LK_ProgressBar::LK_ProgressBar()
        : cbm::kernel_t(), m_Calls( 0), m_NbSpecies( 0)
    { }

ldndc::LK_ProgressBar::~LK_ProgressBar()
    { }

#if LD_ProgressBarStyle == 1
static int  s_Printf_1( char const *  _text, int  _progress=-1)
{
    if ( _progress == -1 )
        { fprintf( stdout, "%s", _text); }
    else
        { fprintf( stdout, _text, _progress); }
    fflush( stdout);
    return 0;
}
#endif /* LD_ProgressBarStyle == 1 */
#if LD_ProgressBarStyle == 2
#define  ProgressBarMemSize 256
#define  ProgressBarDefaultSize 60
#define  ProgressBarMaxSize ProgressBarMemSize-1
static int  s_ProgressBarSize;
static int  s_ProgressBarFillSize;
static char s_ProgressBar[ProgressBarMemSize];
static char s_ProgressBarUndisplay[ProgressBarMemSize];
static char s_C = '-';
static int  s_L = 0;
static int  s_InitializeBuffers()
{
    int const  StatusInfoSize = cbm::strlen( "YYYYY MMM DD hh:mm") + 2/*' ['*/+2/*'] '*/+4/*'nnn%'*/;
    s_ProgressBarSize = ProgressBarDefaultSize;
    int  _ProgressBarSize = 0;
    int rc = cbm::terminal_extents( NULL, &_ProgressBarSize);
    if ( rc == 0)
        { s_ProgressBarSize = std::min( _ProgressBarSize-1, ProgressBarMaxSize); }
    s_ProgressBarSize = std::max( s_ProgressBarSize, StatusInfoSize+10);
    s_ProgressBarFillSize = s_ProgressBarSize - StatusInfoSize;

    s_C = '-';
    s_L = 0;
    for ( int j = 0; j < ProgressBarMemSize; ++j)
    {
        s_ProgressBar[j] = ' ';
        s_ProgressBarUndisplay[j] = '\r';
    }
    s_ProgressBar[s_ProgressBarSize] = '\0';
    s_ProgressBarUndisplay[s_ProgressBarSize] = '\0';
    return 0;
}
static char const *  monthabbr[] =
    { "-", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
static int  s_Printf_2( int _progress,
    cbm::sclock_t const * _clk, int _flag=0)
{
    fprintf( stdout, "%s", s_ProgressBarUndisplay);
    int const  L = (_progress*s_ProgressBarFillSize)/100;
    char *  P = s_ProgressBar;
    int  y = _clk->year(), m = _clk->month(), d = _clk->day();
    int  H = _clk->hour(), M = _clk->minute();
    int  date_len = cbm::snprintf( P, 24, "%5d %s %02d %02d:%02d ", y,monthabbr[m],d, H,M);
    P += date_len;
    *P = '[';
    P += s_L+1;
    char const  C = _flag&PB_GROW ? 'O' : '=';
    for ( int l = s_L; l < L; ++l)
        { *P++ = C; }
    s_L = L;

    if ( s_C == '-')
        { s_C = '/'; }
    else if ( s_C == '/')
        { s_C = '\\'; }
    else if ( s_C == '\\')
        { s_C = '-'; }

    if ( _flag&PB_FINAL)
        { /*no op*/ }
    else if ( _flag&PB_INTR)
        { *P++ = 'I'; }
    else if ( _flag&PB_ERROR)
        { *P++ = 'E'; }
    else
        { *P++ = s_C; }

    if ( _flag&(PB_ERROR|PB_INTR))
        { /*no op*/ }
    else
    {
        P = s_ProgressBar + 20 + s_ProgressBarFillSize;
        cbm::snprintf( P, 7, "] %3d%%", _progress);
    }
    if ( _flag&(PB_FINAL|PB_INTR|PB_ERROR))
        { s_ProgressBar[s_ProgressBarSize-1] = '\n'; }

    fprintf( stdout, "%s", s_ProgressBar);
    fflush( stdout);
    return 0;
}
#endif /* LD_ProgressBarStyle == 2 */

lerr_t
ldndc::LK_ProgressBar::configure( cbm::RunLevelArgs *  _args)
{
    cbm::td_scalar_t  sse_dt = 3600;

#if LD_ProgressBarStyle == 1
    cbm::td_scalar_t  sse_start = _args->clk->sse();
    cbm::td_scalar_t  sse_stop = _args->clk->sse_final();
    sse_dt = (sse_stop - sse_start) / 100; /* every 1% */
#elif LD_ProgressBarStyle == 2
    sse_dt = 2*_args->clk->dt();           /* every second initial timestep delta */
    s_InitializeBuffers();
#else
#  error unknown progress bar style
#endif

    _args->nextcall_dT.unit = 's';
    _args->nextcall_dT.q = sse_dt;

    return LDNDC_ERR_OK;
}

lerr_t
ldndc::LK_ProgressBar::initialize( cbm::RunLevelArgs *)
    { return LDNDC_ERR_OK; }

lerr_t
ldndc::LK_ProgressBar::write( cbm::RunLevelArgs *  _args)
{
    (void)_args;
#if LD_ProgressBarStyle == 0
        /* no progress bar */
#elif LD_ProgressBarStyle == 1
        if ( this->m_Calls%10==0)
            { s_Printf_1( " %d%% ", this->m_Calls); }
        else
        {
            if ( this->m_NbSpecies == 0)
                { s_Printf_1( "."); }
            else
                { s_Printf_1( "."); } /* NOTE  change here to indicate growing season */
        }
#elif LD_ProgressBarStyle == 2
        int flags = 0;
        if ( this->m_NbSpecies > 0)
            { flags |= PB_GROW; }
        cbm::td_scalar_t  sse_remaining = _args->clk->sse_final() - _args->clk->sse();
        cbm::td_scalar_t  sse_completed = this->m_Calls * _args->dT.q;
        s_Printf_2( 100*sse_completed/(sse_completed+sse_remaining), _args->clk, flags);
#endif
    this->m_Calls += 1;

    return LDNDC_ERR_OK;
}

lerr_t
ldndc::LK_ProgressBar::finalize( cbm::RunLevelArgs *  _args)
{
    if ( CBM_LibRuntimeConfig.sc.request_term == 1)
#if LD_ProgressBarStyle == 1
        { s_Printf_1( " %d%%  INTERRUPTED!\n", this->m_Calls); }
#elif LD_ProgressBarStyle == 2
        { s_Printf_2( 0, _args->clk, PB_INTR); }
#endif
    else if ( _args->clk->sse_final() <= _args->clk->sse())
#if LD_ProgressBarStyle == 1
        { s_Printf_1( "\n"); }
#elif LD_ProgressBarStyle == 2
        { s_Printf_2( 100, _args->clk, PB_FINAL); }
#endif
    else
#if LD_ProgressBarStyle == 1
        { s_Printf_1( " %d%%  ERROR!\n", this->m_Calls); }
#elif LD_ProgressBarStyle == 2
    {
        s_Printf_2( 0, _args->clk, PB_ERROR);
    }
#else
#  error select progress bar style
#endif
    return LDNDC_ERR_OK;
}

int  ldndc::LK_ProgressBar::onPlant( ldndc::EventAttributes const *)
{
    this->m_NbSpecies += 1;
    return 0;
}
int  ldndc::LK_ProgressBar::onHarvest( ldndc::EventAttributes const *)
{
    if ( this->m_NbSpecies == 0)
        { return -1; }
    this->m_NbSpecies -= 1;
    return 0;
}

lerr_t
ldndc::LK_ProgressBar::register_ports( cbm::RunLevelArgs *  _args)
{
    int rc_plant = this->m_PlantEvent.subscribe( "plant", _args->iokcomm);
    if ( rc_plant != LD_PortOk)
        { return LDNDC_ERR_FAIL; }
    this->m_PlantEvent.handler( &LK_ProgressBar::onPlant, this);

    int rc_harvest = this->m_HarvestEvent.subscribe( "harvest", _args->iokcomm);
    if ( rc_harvest != LD_PortOk)
        { return LDNDC_ERR_FAIL; }
    this->m_HarvestEvent.handler( &LK_ProgressBar::onHarvest, this);

    return LDNDC_ERR_OK;
}

lerr_t
ldndc::LK_ProgressBar::unregister_ports( cbm::RunLevelArgs *)
{
    this->m_HarvestEvent.unsubscribe();
    this->m_PlantEvent.unsubscribe();

    return LDNDC_ERR_OK;
}

