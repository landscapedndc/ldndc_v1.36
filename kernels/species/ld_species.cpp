/*!
 * @author
 *    steffen klatt (created on: jul 24, 2017)
 */

#include  "species/ld_species.h"
#include  <input/species/species.h>

LDNDC_KERNEL_OBJECT_DEFN(LK_Species,species,_LD_Species,"LandscapeDNDC Species")
ldndc::LK_Species::LK_Species()
        : cbm::kernel_t(), m_input( NULL)
    { }

ldndc::LK_Species::~LK_Species()
    { }

lerr_t
ldndc::LK_Species::register_ports( cbm::RunLevelArgs *)
    { return  LDNDC_ERR_OK; }
lerr_t
ldndc::LK_Species::unregister_ports( cbm::RunLevelArgs *)
    { return  LDNDC_ERR_OK; }


static lerr_t  s_FetchInput( ldndc::input_class_srv_base_t ** _input,
            char const * _itype, cbm::RunLevelArgs *  _args)
{
    *_input = NULL;

    cbm::source_descriptor_t  objId;
    lid_t const  inputId =
        _args->iokcomm->resolve_source( _itype, &objId);
    if ( inputId == invalid_lid)
        { return LDNDC_ERR_RUNTIME_ERROR; }
    else
    {
        *_input = _args->iokcomm->fetch_input( _itype, &objId);
        if ( *_input == NULL)
            { return LDNDC_ERR_FAIL; }
    }
    return LDNDC_ERR_OK;
}
static lerr_t  s_UpdateInput( ldndc::input_class_srv_base_t * _input,
                cbm::RunLevelArgs *  _args)
{
    lerr_t rc_update = LDNDC_ERR_OK;
    if ( _input)
        { rc_update = _input->update_internal_state( *_args->clk); }
    return rc_update;
}

lerr_t
ldndc::LK_Species::configure( cbm::RunLevelArgs *  _args)
{
    if ( s_FetchInput( &this->m_input, "species", _args) != LDNDC_ERR_OK)
        { return LDNDC_ERR_FAIL; }
    return LDNDC_ERR_OK;
}

lerr_t
ldndc::LK_Species::initialize( cbm::RunLevelArgs *)
    { return LDNDC_ERR_OK; }

lerr_t
ldndc::LK_Species::read( cbm::RunLevelArgs *  _args)
{
    species::input_class_species_t const *  species_in =
            _args->iokcomm->get_input_class< species::input_class_species_t >();
    if ( !species_in)
        { return LDNDC_ERR_OK; }

    if ( s_UpdateInput( this->m_input, _args) != LDNDC_ERR_OK)
        { return LDNDC_ERR_FAIL; }
    return LDNDC_ERR_OK;
}

