/*!
 * @author
 *    steffen klatt (created on: jul 24, 2017)
 */

#ifndef  LDNDC_KERNEL_LDSPECIES_H_
#define  LDNDC_KERNEL_LDSPECIES_H_

#include  "ld_kernel.h"

namespace ldndc {

class LDNDC_API LK_Species : public cbm::kernel_t
{
    LDNDC_KERNEL_OBJECT(LK_Species,species)
    public:
        LK_Species();
        ~LK_Species();

    public:
        lerr_t  configure( cbm::RunLevelArgs *);
        lerr_t  register_ports( cbm::RunLevelArgs *);
        lerr_t  initialize( cbm::RunLevelArgs *);
        lerr_t  read( cbm::RunLevelArgs *);
        lerr_t  unregister_ports( cbm::RunLevelArgs *);

    protected:
        input_class_srv_base_t *  m_input;
    private:
        /* hide these buggers for now */
        LK_Species( LK_Species const &);
        LK_Species &  operator=( LK_Species const &);
};
} /* namespace ldndc */

#endif /* !LDNDC_KERNEL_LDSPECIES_H_ */

