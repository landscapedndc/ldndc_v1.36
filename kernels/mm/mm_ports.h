/*!
 * @brief
 *    Share fields from the global state of MoBiLE modules
 *
 * @author
 *    steffen klatt (created on: apr 12, 2017)
 */

#ifndef  LDNDC_KERNEL_MM_PORTS_H_
#define  LDNDC_KERNEL_MM_PORTS_H_

#include  "legacy/state/mbe_state.h"
#include  "ld_kernel.h"
#include  "ld_shared.h"

namespace ldndc {

class LDNDC_API LD_MMPorts
{
public:
    LD_MMPorts();
    
    size_t nd_soil_layers;
    
public:
    lerr_t  register_ports( cbm::io_kcomm_t *);
    lerr_t  unregister_ports( cbm::io_kcomm_t *);

    lerr_t  receive( MoBiLE_State &);
    lerr_t  send( MoBiLE_State &);

private:

    PublishedField<double>  WaterContent20cm;
    PublishedField<double>  FieldCapacity20cm;

    PublishedAndSubscribedField<double>  SurfaceWater;
    PublishedAndSubscribedField<double>  SurfaceSnow;
    
    PublishedField<double>  AccumulatedPrecipitation;
    PublishedAndSubscribedField<double>  AccumulatedThroughfall;
    PublishedAndSubscribedVectorField<double>  AccumulatedTranspiration;
    PublishedAndSubscribedField<double>  AccumulatedWaterdeficit;
    PublishedAndSubscribedField<double>  AccumulatedInterceptionevaporation;
    PublishedAndSubscribedField<double>  AccumulatedSoilevaporation;
    PublishedAndSubscribedField<double>  AccumulatedSurfacewaterevaporation;
    PublishedAndSubscribedField<double>  AccumulatedRunoff;
    PublishedAndSubscribedField<double>  AccumulatedPercolation;
    PublishedAndSubscribedField<double>  AccumulatedInfiltration;
    
    PublishedAndSubscribedField<double>  AccumulatedGroundwaterAccess;
    PublishedAndSubscribedField<double>  AccumulatedGroundwaterLoss;
    PublishedAndSubscribedVectorField<double>  SoilPorosity;
    PublishedAndSubscribedVectorField<double>  SoilDepth;
    PublishedAndSubscribedVectorField<double>  VolumetricWaterContent;
    PublishedAndSubscribedVectorField<double>  VolumetricFieldCapacity;
    PublishedAndSubscribedVectorField<double>  MaximumWaterFilledPoreSpace;
    PublishedAndSubscribedVectorField<double>  SaturatedHydraulicConductivity;
    PublishedAndSubscribedVectorField<double>  IceContent;
    
    PublishedAndSubscribedVectorField<double>  SoilSO4;
    PublishedAndSubscribedVectorField<double>  SoilNO3;
    PublishedAndSubscribedVectorField<double>  SoilNH4;
    PublishedAndSubscribedVectorField<double>  SoilDON;
    
    PublishedAndSubscribedVectorField<double>  SoilDOC;
    
    PublishedAndSubscribedField<double>  AccumulatedLeachingNO3;
    PublishedAndSubscribedField<double>  AccumulatedLeachingNH4;
    PublishedAndSubscribedField<double>  AccumulatedLeachingDON;
    
    PublishedAndSubscribedField<double>  AccumulatedLeachingDOC;
};

} /* namespace ldndc */

#endif  /*  !LDNDC_KERNEL_MM_PORTS_H_  */

