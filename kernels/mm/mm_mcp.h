/*!
 * @brief
 *    simulation control logic
 *
 * @author
 *    Steffen Klatt (created on: apr 06, 2017)
 *    David Kraus
 */

#ifndef  MM_MOBILE_MODULE_MCP_H_
#define  MM_MOBILE_MODULE_MCP_H_

#include  "legacy/mbe_legacymodel.h"
#include  "legacy/state/mbe_state.h"
#include  "legacy/mbe_setup.h"
#include  "mm/mm_ports.h"
#include  "ld_shared.h"

namespace ldndc {

struct  LD_MMMasterControlProgramModuleOrder
{
    /* module classes are added to the list in the order:
     *
     *    [ [M] | I_1 .. I_T | U_1 | F_1 | O_1 | ... | U_T | F_T | O_T ]
     *
     * where
     *    T := number of different time modes
     *
     *    M := mcp (this control module (implicitly added)),
     *    I := initializers,
     *    U := user modules,
     *    F := finalizers,
     *    O := output modules
     */
    unsigned int  get( lflags_t const &, timemode_e) const;
};

class  LDNDC_API  LD_MMMasterControlProgram  :  public  MBE_LegacyModel
{
    LMOD_EXPORT_MODULE_INFO(LD_MMMasterControlProgram,"_MoBiLE::MasterControlProgram","MoBiLE Legacy MasterControlProgram");
    public:
        void *  owner_kernel;
    public:
        LD_MMMasterControlProgram( MoBiLE_State *,
                                   cbm::io_kcomm_t *,
                                   timemode_e);
        ~LD_MMMasterControlProgram();

        lerr_t  configure( ldndc::config_file_t const *);
        lerr_t  register_ports( cbm::io_kcomm_t *);
        lerr_t  initialize();

        lerr_t  read();
        lerr_t  solve();
        lerr_t  integrate();
        lerr_t  write();

        lerr_t  finalize() { return LDNDC_ERR_OK; }
        lerr_t  unregister_ports( cbm::io_kcomm_t *);

        lerr_t  sleep() { return LDNDC_ERR_OK; }
        lerr_t  wake() { return LDNDC_ERR_OK; }

    private:
        /* global state */
        MoBiLE_State  m_state;
        /* shared ports */
        LD_MMPorts  m_ports;

        SubscribedEvent<LD_EventHandlerQueue>  m_HarvestEvents;
        SubscribedEvent<LD_EventHandlerQueue>  m_PlantEvents;
        SubscribedEvent<LD_EventHandlerQueue>  m_ReparameterizeEvents;

        cbm::io_kcomm_t *  m_iokcomm;
        /* reference to configuration file */
        config_file_t const *  m_config;
        /* reference to module selection and configuration */
        mobile_modules_info_t  m_modulessetup;
        /* reference to module container */
        module_container_t  m_modules;

        lerr_t  m_readmobile( cbm::io_kcomm_t *);
        lerr_t  m_configure();

        /*!
         * @brief
         *    insert module into module list
         */
        lerr_t  m_createmodules();

        /*!
         * @brief
         *    creates module with ID if requested time mode
         *    can be fulfilled
         *
         *    creating module with specific flags can be 
         *    controlled by means of "require" and "deny"
         *    flags
         */
        lerr_t  m_modulecreate( ldndc::MoBiLE_ModuleEnvelope *,
                char const *, timemode_e,
                lflags_t = LMOD_FLAG_NONE /*required*/,
                lflags_t = LMOD_FLAG_NONE /*denied*/);
        /*!
         * @brief
         *    creates module given its factory if requested
         *    time mode can be fulfilled and module flags
         *    are satisfied.
         */
        lerr_t  m_modulecreate( ldndc::MoBiLE_ModuleEnvelope *,
                MoBiLE_ModuleFactoryEnvelope const &,
                timemode_e,
                lflags_t = LMOD_FLAG_NONE /*required*/,
                lflags_t = LMOD_FLAG_NONE /*denied*/);

        /*!
         * @brief
         *    searches module in module container by ID. if
         *    it was found the pointer (mod) is set if given.
         */
        lerr_t  m_modulefind( char const * /*id*/,
                    ldndc::MBE_LegacyModel ** = NULL /*mod*/);
        /*!
         * @brief
         *    insert module into module list according to
         *    mount order strategy
         */
        lerr_t  m_modulemount( ldndc::MoBiLE_ModuleEnvelope *);
        lerr_t  m_modulesenqueue();

        /*!
         * @brief
         *    add module to module list by id
         */
        lerr_t  m_moduleadd( char const *, timemode_e,
            lflags_t /*require-flag*/, lflags_t /*deny-flags*/);

        /*!
         * @brief
         *    mounts maintenance system modules (LMOD_FLAG_MAINTENANCE)
         *    into the module chain. whether a maintenance module is
         *    added before or after the user modules is determined by the
         *    flag marking it as an initializer or finalizer. the
         *    order within the set of maintenance modules that run before
         *    (after) the user modules is undefined (technically the
         *    order in the factory array will determine the order, however
         *    the factory is automatically generated at build time
         *    reading the module build info files, here order is
         *    undefined.
         *
         *    there is no guard for multiple calls to this method. each
         *    time a new set is added, which is in general an error.
         *
         * @param
         *    the given flag indicated if set to be added is the
         *    initializer or finalizer set.
         */
        lerr_t  m_addmaintenancemodules(
                lmodule_flag_e /* initializer vs finalizer */);

        /*! adds initial set of modules as given in setup */
        lerr_t  m_addusermodules(
                lflags_t = LMOD_FLAG_NONE /*required*/,
                lflags_t = LMOD_FLAG_NONE /*denied*/);

        /*!
         * @brief
         *    if a species was added (plant event in event input)
         *    this triggers the species specific initialization routines
         */
        lerr_t  m_mountspecies();
    
        /*!
         * @brief
         *    if parameters for species were changed (reparameterizespecies in event input)
         *    this triggers the species specific parameter update
         */
        lerr_t
        m_switchparameters();
};

} /* namespace ldndc */

#endif  /*  !MM_MOBILE_MODULE_MCP_H_  */

