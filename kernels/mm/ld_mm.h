/*!
 * @brief
 *    Wraps a MoBiLE Module (Grote et al, 2009) to be usable as
 *    a LandscapeDNDC kernel.
 *
 * @author
 *    steffen klatt (created on: jul 01, 2016)
 */

#ifndef  LDNDC_KERNEL_MM_H_
#define  LDNDC_KERNEL_MM_H_

#include  "ld_kernel.h"
#include  "legacy/mbe_legacymodel.h"

namespace ldndc {

class LDNDC_API LK_LegacyModel : public cbm::kernel_t
{
    LDNDC_KERNEL_OBJECT(LK_LegacyModel,mm)
    public:
        LK_LegacyModel();
        ~LK_LegacyModel();

        char const *  model_name() const
            { return this->m_mmodule ? this->m_mmodule->name() : this->name(); }

    public:
        lerr_t  configure( cbm::RunLevelArgs *);
        lerr_t  register_ports( cbm::RunLevelArgs *);
        lerr_t  initialize( cbm::RunLevelArgs *);

        lerr_t  read( cbm::RunLevelArgs *);
        lerr_t  solve( cbm::RunLevelArgs *);
        lerr_t  integrate( cbm::RunLevelArgs *);
        lerr_t  write( cbm::RunLevelArgs *);

        lerr_t  unregister_ports( cbm::RunLevelArgs *);
        lerr_t  finalize( cbm::RunLevelArgs *);

#ifdef  _HAVE_SERIALIZE
        int  create_checkpoint()
            { return  -1; /*this->m_mmodule->create_checkpoint( this->m_iokcomm);*/ }
        int  restore_checkpoint( ldate_t)
            { return  -1; /*this->m_mmodule->restore_checkpoint( this->m_iokcomm, &_date);*/ }
#endif
        int  query_feature( char ** /*query result*/,
                char const *  _query);

    /* module specific extensions */
    public:
        virtual lerr_t  sleep()
            { return  this->m_mmodule->sleep(); }
        virtual lerr_t  wake()
            { return  this->m_mmodule->wake(); }


    protected:
        /* the wrapped module */
        MBE_LegacyModel *  m_mmodule;

        MBE_LegacyModel *  m_createlegacymodel( cbm::RunLevelArgs *);

    private:
        /* hide these buggers for now */
        LK_LegacyModel( LK_LegacyModel const &);
        LK_LegacyModel &  operator=( LK_LegacyModel const &);
};

} /* namespace ldndc */

#endif /* !LDNDC_KERNEL_MM_H_ */

