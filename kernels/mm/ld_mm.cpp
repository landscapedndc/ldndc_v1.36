/*!
 * @author
 *    Steffen Klatt (created on: jul 01, 2016)
 */

#include  "mm/ld_mm.h"

#include  "legacy/mbe_legacymodel.h"
#include  "legacy/state/mbe_state.h"

#include  <logging/cbm_logging.h>

LDNDC_KERNEL_OBJECT_DEFN(LK_LegacyModel,mm,_LD_MoBiLE,"Legacy Models")
ldndc::LK_LegacyModel::LK_LegacyModel(): cbm::kernel_t(),
                                         m_mmodule( NULL)
{}

ldndc::LK_LegacyModel::~LK_LegacyModel()
{
    if ( this->m_mmodule)
    {
        this->m_mmodule->delete_instance();
        this->m_mmodule = NULL;
    }
}

#include  "mm/mm_mcp.h"
lerr_t
ldndc::LK_LegacyModel::configure( cbm::RunLevelArgs *  _args)
{
    //master control program instance
    if ( _args->ptr == NULL)
    {
        this->m_mmodule = LD_MMMasterControlProgram::new_instance( NULL, _args->iokcomm, _args->clk, TMODE_SUBDAILY);
        if ( !this->m_mmodule)
            { return LDNDC_ERR_FAIL; }
        this->m_mmodule->set_object_id( this->object_id());

        LD_MMMasterControlProgram *  mcp = static_cast< LD_MMMasterControlProgram *> ( this->m_mmodule);
        mcp->owner_kernel = this;
        CBM_LogDebug( "MCP successfully created at ",(void*)mcp);
    }
    //all other mobile modules
    else
    {
        this->m_mmodule = this->m_createlegacymodel( _args);
        if ( !this->m_mmodule)
            { return LDNDC_ERR_FAIL; }

        /* first execution is at (t+dT)-R  */
        if (   this->m_mmodule->timemode()==TMODE_POST_DAILY
            || this->m_mmodule->timemode()==TMODE_POST_MONTHLY
            || this->m_mmodule->timemode()==TMODE_POST_YEARLY)
        {
            _args->nextcall_sse = cbm::add_dt( _args->sse, &_args->dT) - _args->clk->dt();
            if ( this->m_mmodule->timemode()==TMODE_POST_DAILY)
                { _args->flags.prio = -1; }
            else if ( this->m_mmodule->timemode()==TMODE_POST_MONTHLY)
                { _args->flags.prio = -2; }
            else if ( this->m_mmodule->timemode()==TMODE_POST_YEARLY)
                { _args->flags.prio = -3; }
        }
    }
    return  this->m_mmodule->configure( _args->cfg);
}

ldndc::MBE_LegacyModel *
ldndc::LK_LegacyModel::m_createlegacymodel( cbm::RunLevelArgs * _args)
{
    MoBiLE_ModuleEnvelope *  modenv = static_cast< MoBiLE_ModuleEnvelope * >( _args->ptr);

    ldndc_kassert( modenv->module==NULL);
    ldndc_kassert( modenv->factory!=NULL);
    modenv->module = modenv->factory->construct( modenv->mbe_state, modenv->iokcomm,
                                                 _args->clk, modenv->mbe_tmmode);
    if ( !modenv->module)
    {
        KLOGERROR( "failed to instantiate module (out of memory?)");
        return NULL;
    }

    /* inject reference to kernel communicator .. we are a friend ;-) */
    modenv->module->__m_iokcomm = modenv->iokcomm;

    /* inject reference to modules info .. we are a friend ;-) */
    modenv->module->__m_moduleinfos = modenv->mbe_info;
    mobile_module_info_t const * module_info = modenv->mbe_info->get_mobile_module_info_by_id( modenv->module->id());
    if ( module_info)
    {
        /* inject module options if applicable */
        lerr_t  rc_merge = modenv->module->merge_options( module_info->options);
        if ( rc_merge)
        {
            modenv->module->delete_instance();
            modenv->module = NULL;
        }
    }

    if ( modenv->module)
        { CBM_LogDebug( "Enqueue module  id=", modenv->module->id(),
                        ", timemode=", ldndc::time::timemode_name( modenv->module->timemode())); }

    return modenv->module;
}

lerr_t
ldndc::LK_LegacyModel::register_ports( cbm::RunLevelArgs *  _args)
{
    if ( this->m_mmodule)
        { return this->m_mmodule->register_ports( _args->iokcomm); }
    return LDNDC_ERR_OK;
}

lerr_t
ldndc::LK_LegacyModel::unregister_ports( cbm::RunLevelArgs *  _args)
{
    if ( this->m_mmodule)
        { return this->m_mmodule->unregister_ports( _args->iokcomm); }
    return LDNDC_ERR_OK;
}

lerr_t
ldndc::LK_LegacyModel::initialize( cbm::RunLevelArgs *)
{
    if ( this->m_mmodule)
        { return this->m_mmodule->initialize(); }
    return LDNDC_ERR_OK;
}

static int s_ExecuteRunlevel( char const * _runlevel_name,
                              cbm::RunLevelArgs *,
                              ldndc::MBE_LegacyModel *  _mmodule,
                              lerr_t (ldndc::MBE_LegacyModel::*fn)( void))
{
    int rc_exec = 0;

    /* check module's run conditions */
    if ( _mmodule && _mmodule->active())
    {
        lerr_t rc_runlevel = (_mmodule->*fn)();
        if ( rc_runlevel)
        {
            LOGERROR( "errors while executing runlevel",
                      " \"", _runlevel_name,"\"", "  [module=", _mmodule->name(),"]");
            rc_exec = -1;
        }
    }
    return rc_exec;
}

lerr_t
ldndc::LK_LegacyModel::read( cbm::RunLevelArgs *  _args)
{
    int rc_exec = s_ExecuteRunlevel( "read", _args, this->m_mmodule, &MBE_LegacyModel::read);
    if ( rc_exec)
        { return LDNDC_ERR_RUNTIME_ERROR; }
    return LDNDC_ERR_OK;
}

lerr_t
ldndc::LK_LegacyModel::solve( cbm::RunLevelArgs *  _args)
{
    int rc_exec = s_ExecuteRunlevel( "solve", _args, this->m_mmodule, &MBE_LegacyModel::solve);
    if ( rc_exec)
        { return LDNDC_ERR_RUNTIME_ERROR; }
    return LDNDC_ERR_OK;
}

lerr_t
ldndc::LK_LegacyModel::integrate( cbm::RunLevelArgs *  _args)
{
    int rc_exec = s_ExecuteRunlevel( "integrate", _args, this->m_mmodule, &MBE_LegacyModel::integrate);
    if ( rc_exec)
        { return LDNDC_ERR_RUNTIME_ERROR; }
    return LDNDC_ERR_OK;
}

lerr_t
ldndc::LK_LegacyModel::write( cbm::RunLevelArgs *  _args)
{
    int rc_exec = s_ExecuteRunlevel( "write", _args, this->m_mmodule, &MBE_LegacyModel::write);
    if ( rc_exec)
        { return LDNDC_ERR_RUNTIME_ERROR; }
    return LDNDC_ERR_OK;
}

lerr_t
ldndc::LK_LegacyModel::finalize( cbm::RunLevelArgs *)
{
    if ( this->m_mmodule)
    {
        lerr_t  rc_fin = this->m_mmodule->finalize();
        if ( rc_fin != LDNDC_ERR_OK)
        {
            KLOGERROR( "errors while finalizing kernel",
                       "  [kernel=", this->name(),
                       " with module=", this->m_mmodule->name(),"]");
        }
    }
    return  LDNDC_ERR_OK;
}

#include  "legacy/mbe_legacymodels.h"
int
ldndc::LK_LegacyModel::query_feature( char ** /*query result*/,
                char const *  _query)
{
    int  rc = 0;
 
    if ( cbm::is_equal( _query, "show-available-models"))
    {
        ldndc_list_available_legacymodels( 1);
    }
    else
    {
        fprintf( stderr, "Did not understand query: \"%s\"\n", _query);
        rc = -1;
    }
 
    return rc;
}

