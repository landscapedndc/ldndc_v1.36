/*!
 * @brief
 *    simulation control logic (implementation)
 *
 * @author
 *    Steffen Klatt (created on: apr 06, 2017)
 *    David Kraus
 */

#include  <list>

#include  <kernel/io-kcomm.h>

#include  <string/cbm_string.h>
#include  <logging/cbm_logging.h>
#include  <utils/cbm_utils.h>

#include  <containers/cbm_bitset.h>
#include  <containers/lbstack.h>

#include  <cfgfile/cbm_cfgfile.h>

#include  "mm/mm_mcp.h"


/* if output modules should be treated special and
 * mounted after finalizers uncomment this macro
 */
#define  _HAVE_OUTPUT_MODULES_AFTER_FINALIZERS


unsigned int
ldndc::LD_MMMasterControlProgramModuleOrder::get( lflags_t const &  _m_flags,
                                                  timemode_e  _m_timemode)
const
{
    static unsigned int const  MOD_CLASS_CNT = 4u;

    static unsigned int const  MOD_CLASS_INI_OFFS = 1u;
    static unsigned int const  MOD_CLASS_USR_OFFS = 2u;
    static unsigned int const  MOD_CLASS_FIN_OFFS = 3u;
    static unsigned int const  MOD_CLASS_OUT_OFFS = MOD_CLASS_CNT;

    unsigned int const  tm_shift( MOD_CLASS_CNT * cbm::lsb_pos( static_cast< unsigned int >( _m_timemode)));

    if ( _m_flags & LMOD_FLAG_SYSTEM)
    {
        if ( _m_flags & LMOD_FLAG_CONTROL)
        {
            return  0;
        }

        ldndc_assert( _m_flags & LMOD_FLAG_MAINTENANCE);

        if ( _m_flags & LMOD_FLAG_INITIALIZER)
        {
            return  MOD_CLASS_INI_OFFS + cbm::lsb_pos( static_cast< unsigned int >( _m_timemode));
        }
        if ( _m_flags & LMOD_FLAG_FINALIZER)
        {
            return  MOD_CLASS_FIN_OFFS + tm_shift;
        }
    }
    else if ( _m_flags & LMOD_FLAG_USER)
    {
#ifdef  _HAVE_OUTPUT_MODULES_AFTER_FINALIZERS
        if ( _m_flags & LMOD_FLAG_OUTPUT)
        {
            return  MOD_CLASS_OUT_OFFS + tm_shift;
        }
#endif  /* _HAVE_OUTPUT_MODULES_AFTER_FINALIZERS */
        return  MOD_CLASS_USR_OFFS + tm_shift;
    }

    /* else .. no good */
    LOGFATAL( "BUG");
    return  ldndc::invalid_t< unsigned int >::value;
}


LMOD_MODULE_INFO(LD_MMMasterControlProgram,TMODE_SMALLEST,LMOD_FLAG_SYSTEM|LMOD_FLAG_CONTROL);
ldndc::LD_MMMasterControlProgram::LD_MMMasterControlProgram(
        ldndc::MoBiLE_State *  _state, cbm::io_kcomm_t *  _io_kcomm,
        timemode_e  _timemode)
        : ldndc::MBE_LegacyModel( _state, _timemode), owner_kernel( NULL),
          m_iokcomm( _io_kcomm), m_config( NULL)
{ }


ldndc::LD_MMMasterControlProgram::~LD_MMMasterControlProgram()
{ }

lerr_t
ldndc::LD_MMMasterControlProgram::configure( ldndc::config_file_t const *  _config)
{
    this->m_config = _config;

    if ( !this->m_iokcomm->state.ok)
    {
        KLOGERROR( "master control: i/o handler is bad.");
        return  LDNDC_ERR_FAIL;
    }
    ldndc_assert( this->m_config);

    lerr_t  rc_cfg = this->m_readmobile( this->m_iokcomm);
    if ( rc_cfg)
        { return  LDNDC_ERR_FAIL; }

    lerr_t  rc_ini = this->m_configure();
    if ( rc_ini)
        { return LDNDC_ERR_FAIL; }

    lerr_t  rc_modules = this->m_createmodules();
    if ( rc_modules)
        { return LDNDC_ERR_FAIL; }

    return  LDNDC_ERR_OK;
}

#include  <input/setup/setup.h>
lerr_t
ldndc::LD_MMMasterControlProgram::m_readmobile( cbm::io_kcomm_t * _iokcomm)
{
    setup::input_class_setup_t const *  setup_in = _iokcomm->get_input_class< setup::input_class_setup_t >();
    if ( !setup_in)
    {
        KLOGERROR( "Failed to get setup input interface");
        return  LDNDC_ERR_FAIL;
    }

    static char const * const  MoBiLE_SectionNames[] = { "/MoBiLE", "/mobile", NULL /*sentinel*/ };
    int  s = 0;
    char *  mobile_cfg = NULL;
    while ( MoBiLE_SectionNames[s]) /*try alternative paths*/
    {
        setup_in->section_data( &mobile_cfg, MoBiLE_SectionNames[s]+1);
        if ( mobile_cfg)
        { break; }
        ++s;
    }

    if ( mobile_cfg)
    {
        KLOGVERBOSE( "Using MoBiLE section \"", MoBiLE_SectionNames[s],"\"");
        cbm::ksetup_t  ksetup = cbm::ksetup_t( MoBiLE_SectionNames[s], mobile_cfg);
        CBM_FreeSectionData( mobile_cfg);
        if ( !ksetup.parse_ok())
        {
            KLOGERROR( "Failed to parse MoBiLE input");
            return  LDNDC_ERR_FAIL;
        }
        
        {
            cbm::string_t  query_key;
            query_key << "/substates/options/";
            cbm::string_t substates_options = ksetup.query_any( query_key, "");
            if ( !substates_options.is_empty())
            {
                cbm::string_t::string_array_t  keyvalues = substates_options.as_string_array( ';');
                for ( size_t  v = 0;  v < keyvalues.size();  ++v)
                {
                    cbm::key_value_t const  keyvalue = cbm::as_key_value( keyvalues[v].c_str());
                    if ( cbm::is_equal( keyvalue.key.str(), "waterretention"))
                    {
                        m_state.wr_type = keyvalue.value;
                    }
                }
            }
        }

        int  nb_modules = ksetup.query_size( "/modulelist/module");
        KLOGVERBOSE( "seeing ", nb_modules, " MoBiLE module(s)");
        if ( nb_modules == 0)
        {
            KLOGVERBOSE( "No module selection found. Doing without..");
            return  LDNDC_ERR_OK;
        }

        for ( int  m = 0;  m < nb_modules;  ++m)
        {
            mobile_module_info_t  mminfo;
            cbm::string_t  module_id;
            {
                cbm::string_t  query_key;
                query_key << "/modulelist/module/" << m << "/id";
                module_id = ksetup.query_string( query_key, "");
            }

            if ( module_id.is_empty())
            {
                KLOGERROR( "empty or missing MoBiLE module ID is not valid");
                return LDNDC_ERR_FAIL;
            }
            mminfo.id = module_id;

            cbm::string_t  module_timemode;
            timemode_e  module_tm;
            {
                cbm::string_t  query_key;
                query_key << "/modulelist/module/" << m << "/timemode";
                module_timemode = ksetup.query_string( query_key, time::timemode_name(TMODE_NONE));
            }

            /* translate timemode string */
            unsigned int  tm = invalid_t< unsigned int >::value;
            cbm::find_index( module_timemode.c_str(), time::TMODE_NAMES, invalid_size, &tm);
            if ( cbm::is_invalid( tm))
            {
                LOGERROR( "time mode not understood [mode=",module_timemode,"]");
                return  LDNDC_ERR_FAIL;
            }
            else
            {
                module_tm = static_cast< timemode_e >(( 1u << tm) >> 1);
            }
            mminfo.timemode = module_tm;

            cbm::string_t  module_options;
            {
                cbm::string_t  query_key;
                query_key << "/modulelist/module/" << m << "/options";
                module_options = ksetup.query_any( query_key, "");
            }

            if ( !module_options.is_empty())
            {
                cbm::string_t::string_array_t  keyvalues = module_options.as_string_array( ';');
                for ( size_t  v = 0;  v < keyvalues.size();  ++v)
                {
                    cbm::key_value_t const  keyvalue = cbm::as_key_value( keyvalues[v].c_str());
                    lerr_t  rc_addoption = mminfo.options.insert( keyvalue.key.str(), keyvalue.value);
                    KLOGVERBOSE( "setting option:",keyvalue.key," -> ",keyvalue.value);
                    if ( rc_addoption)
                    {
                        LOGERROR( "adding option '",keyvalue.key,"'",
                                  " for module '",mminfo.id, "' failed",
                                  "  [id=",this->object_id(),"]");
                        return  LDNDC_ERR_FAIL;
                    }
                }
            }

            this->m_modulessetup.add_module_setup( mminfo);
        }
    }

    return  LDNDC_ERR_OK;
}

lerr_t
ldndc::LD_MMMasterControlProgram::m_configure()
{
    input_class_soillayers_t const *  soillayers = m_iokcomm->get_input_class< input_class_soillayers_t >();
    if ( !soillayers)
	{
		KLOGERROR( "failed to retrieve soil layers input");
		return  LDNDC_ERR_FAIL;
	}
	else if ( soillayers && soillayers->soil_layer_cnt() < 3)
    {
        KLOGERROR( "MoBiLE requires at least 3 soillayers",
                   " [#layers=",soillayers->soil_layer_cnt(),"]");
        return  LDNDC_ERR_FAIL;
    }

    /* initializing substates */
    lerr_t  rc_state_init = m_state.initialize( this->m_iokcomm);
    if ( rc_state_init || !m_iokcomm->state.ok)
    {
        KLOGERROR( "creating and initializing state failed for kernel");
        return  LDNDC_ERR_FAIL;
    }
    
    //set soil layer count to input/output ports
    m_ports.nd_soil_layers = soillayers->soil_layer_cnt();

    return  LDNDC_ERR_OK;
}

lerr_t
ldndc::LD_MMMasterControlProgram::initialize()
{
    lerr_t  rc_send = this->m_ports.send( m_state);
    if ( rc_send)
        { return  LDNDC_ERR_FAIL; }
    
    return  LDNDC_ERR_OK;
}

lerr_t
ldndc::LD_MMMasterControlProgram::m_createmodules()
{
    /* clear container (for reinitialization) */
    this->m_modules.clear();

    /* maintenance modules running before user modules (initializers) */
    lerr_t  rc_ini = this->m_addmaintenancemodules( LMOD_FLAG_INITIALIZER);
    if ( rc_ini)
        { return LDNDC_ERR_FAIL; }
    /* maintenance modules running after user modules (finalizers) */
    lerr_t  rc_fin = this->m_addmaintenancemodules( LMOD_FLAG_FINALIZER);
    if ( rc_fin)
        { return LDNDC_ERR_FAIL; }

    /* create all user modules (including output modules) */
    lerr_t  rc_usr = this->m_addusermodules( LMOD_FLAG_USER, 0);
    if ( rc_usr)
        { return LDNDC_ERR_FAIL; }

    lerr_t  rc_enq = this->m_modulesenqueue();
    if ( rc_enq)
        { return LDNDC_ERR_FAIL; }

    return  LDNDC_ERR_OK;
}


lerr_t
ldndc::LD_MMMasterControlProgram::m_modulecreate(
            MoBiLE_ModuleEnvelope * _me,
        MoBiLE_ModuleFactoryEnvelope const &  _m_factory,
        timemode_e  _timemode,
        lflags_t  _mflags_req, lflags_t  _mflags_den)
{
    /* deny construction if not all of the required-flags were set */
    if ( _mflags_req != LMOD_FLAG_NONE)
    {
        ldndc::bitset  req( LMOD_FLAG_CNT, static_cast< ldndc::bitset::bitset_base_t>( _mflags_req));
        ldndc::bitset  fmod( LMOD_FLAG_CNT, static_cast< ldndc::bitset::bitset_base_t >( _m_factory.info().mflags));
        if ( !fmod.all( req))
        {
// sk:??            if ( _mcp_flags & MCP_FLAG_MFLAG_MISMATCH_FATAL)
            {
                KLOGWARN( "required flags for module missing  [required=",req.to_string(),",got=",fmod.to_string(),"]");
            }
            return  LDNDC_ERR_REQUEST_MISMATCH;
        }
    }
    /* deny construction if any of the deny-flags was set */
    if ( _mflags_den != LMOD_FLAG_NONE)
    {
        ldndc::bitset  den( LMOD_FLAG_CNT, static_cast< ldndc::bitset::bitset_base_t>( _mflags_den));
        ldndc::bitset  fmod( LMOD_FLAG_CNT, static_cast< ldndc::bitset::bitset_base_t>( _m_factory.info().mflags));
        if ( fmod.any( den))
        {
// sk:??            if ( _mcp_flags & MCP_FLAG_MFLAG_MISMATCH_FATAL)
            {
                KLOGWARN( "not allowed flags for module set  [denied=",den.to_string(),",got=",fmod.to_string(),"]");
            }
            return  LDNDC_ERR_REQUEST_MISMATCH;
        }
    }

    /* check for supported time interval */
    timemode_e  tm = _timemode;
    if ( tm == TMODE_NONE)
    {
        unsigned int  tm_p = cbm::lsb_pos( _m_factory.info().timemode);
        ldndc_assert( tm_p > 0);
        tm = static_cast< timemode_e >( 1 << ( tm_p - 1));
    }
    if ( !( tm & _m_factory.info().timemode))
    {
        KLOGERROR( "requested time interval does not match any for module",
              "  [id=",_m_factory.info().id,",timemode=",tm," vs ",_m_factory.info().timemode,"]");
        return  LDNDC_ERR_INVALID_ARGUMENT;
    }

    _me->module = NULL;
    _me->mbe_state = &this->m_state;
    _me->mbe_tmmode = tm;
    _me->mbe_info = &this->m_modulessetup;
    _me->iokcomm = this->m_iokcomm;
    _me->factory = _m_factory.constructor;

    return  LDNDC_ERR_OK;
}

lerr_t
ldndc::LD_MMMasterControlProgram::m_modulecreate(
        ldndc::MoBiLE_ModuleEnvelope *  _modenv, char const *  _id, timemode_e  _timemode,
        lflags_t  _mflags_req, lflags_t  _mflags_den)
{
    ldndc_kassert( _modenv);

    size_t  k( 0);
    /* find module's factory and create if found, fail otherwise */
    while ( module_factories[k].exists())
    {
        if ( ! cbm::is_equal( _id, module_factories[k].info().id))
        {
            ++k;
            continue;
        }

        /* if factory was found, try to construct module */
        return  this->m_modulecreate( _modenv, module_factories[k],
                _timemode, _mflags_req, _mflags_den);
    }

    /* if we got here without valid module, ID must be invalid */
    KLOGERROR( "unknown module  [module=\"",_id,"\"]");
    return  LDNDC_ERR_ATTRIBUTE_NOT_FOUND;
}


lerr_t
ldndc::LD_MMMasterControlProgram::m_modulefind(
        char const *  _id,
        ldndc::MBE_LegacyModel **  _mod)
{
    ldndc_kassert( _id);

    ldndc::module_container_t::iterator  mod_i( this->m_modules.begin());
    for ( ; mod_i != this->m_modules.end();  ++mod_i)
    {
        if ( cbm::is_equal( _id, mod_i->id.c_str()))
        {
            if ( _mod)
                { *_mod = mod_i->module; }
            return  LDNDC_ERR_OK;
        }
    }
    return  LDNDC_ERR_ATTRIBUTE_NOT_FOUND;
}


lerr_t
ldndc::LD_MMMasterControlProgram::m_modulemount(
        ldndc::MoBiLE_ModuleEnvelope *  _modenv)
{
    ldndc_kassert( _modenv);

    /* order strategy type */
    LD_MMMasterControlProgramModuleOrder  mmodule_order;

    /* position of module class */
    unsigned int  c_pos = mmodule_order.get( _modenv->factory->info().mflags, _modenv->mbe_tmmode);

    ldndc::module_container_t::iterator  mod_i( this->m_modules.begin());
    while (( mod_i != this->m_modules.end()) &&
            ( c_pos >= mmodule_order.get( mod_i->factory->info().mflags, mod_i->mbe_tmmode)))
    {
        ++mod_i;
    }
    if ( mod_i == this->m_modules.end())
    {
        this->m_modules.push_back( *_modenv);
    }
    else
    {
        this->m_modules.insert( mod_i, *_modenv);
    }

    return  LDNDC_ERR_FAIL;
}

#include "mm/ld_mm.h"
lerr_t
ldndc::LD_MMMasterControlProgram::m_modulesenqueue()
{
    ldndc_kassert( this->owner_kernel);

    cbm::kmount_t  kmount = this->m_iokcomm->spatial_controller(
            static_cast< LK_LegacyModel * >( this->owner_kernel));

    int  k = 0;
    for ( module_container_t::iterator  modenv = this->m_modules.begin();
        modenv != this->m_modules.end();  ++modenv)
    {
        ldndc_kassert( modenv->module==NULL);

        cbm::string_t  k_id;
        k_id << "_LD_MM.module" << k;
        cbm::mountargs_t  mountargs;
        mountargs.ptr = &(*modenv);

        /* translate timemode to deltaT */
        char const *  tm = time::timemode_name( modenv->mbe_tmmode);
        char  dT[24];
        static char const *  TM[] = { "?", "1d", "1M", "1Y" };
        char const *  tm_value = NULL;
        if ( cbm::is_equal( tm, "daily") || cbm::is_equal( tm, "predaily"))
            { tm_value = TM[1]; }
        else if ( cbm::is_equal( tm, "monthly") || cbm::is_equal( tm, "premonthly"))
            { tm_value = TM[2]; }
        else if ( cbm::is_equal( tm, "yearly") || cbm::is_equal( tm, "preyearly"))
            { tm_value = TM[3]; }
        else /*including subdaily*/
            { cbm::snprintf( dT, 24, "{\"dt\":\"%ds\"}", (int)this->lclock()->dt()); }
        if ( tm_value)
            { cbm::snprintf( dT, 24, "{\"dt\":\"%s\"}", tm_value); }
        mountargs.cfg = dT;

        lerr_t  rc_kmount = kmount.enqueue_for_mount(
                _LD_MoBiLE, LD_NOID, k_id, //modenv->id(),
            static_cast< LK_LegacyModel * >( this->owner_kernel), &mountargs);
        if ( rc_kmount)
            { return LDNDC_ERR_FAIL; }
        k+=1;
    }

    return LDNDC_ERR_OK;
}

/* module list modification actions (for initialization, LUC, ...) */
lerr_t
ldndc::LD_MMMasterControlProgram::m_moduleadd(
        char const *  _id, timemode_e  _timemode,
        lflags_t  _mflags_req, lflags_t  _mflags_den)
{
    /* NOTE  for now we need to disallow adding same module multiple
     * times because LUC depends on unique IDs in the module chain
     * to be able to replace or delete modules without ambiguity
     */
    if ( this->m_modulefind( _id) == LDNDC_ERR_OK)
    {
// sk:off        if ( _mcp_flags & MCP_FLAG_MODULE_DUPLICATE_FATAL)
// sk:off        {
// sk:off            KLOGERROR( "adding module more than once is currently not supported  [module=",_id,"]");
// sk:off            return  LDNDC_ERR_INVALID_ARGUMENT;
// sk:off        }
// sk:off        else
        {
            KLOGWARN( "adding module more than once is currently not supported.",
                        " ignoring duplicate module entry [module=",_id,"]");
            return  LDNDC_ERR_OK;
        }
    }

    ldndc::MoBiLE_ModuleEnvelope  mbe_modenv( NULL, _id);
    lerr_t  rc = this->m_modulecreate( &mbe_modenv,
        _id, _timemode, _mflags_req, _mflags_den);
    if ( rc == LDNDC_ERR_REQUEST_MISMATCH)
    {
        /* we tolerate a module flag request mismatch */
        return  rc;
    }
    if ( !mbe_modenv.factory)
        { return  LDNDC_ERR_OBJECT_CREATE_FAILED; }
    
    /* finally attach it to container */
    this->m_modulemount( &mbe_modenv);

    return  LDNDC_ERR_OK;
}

lerr_t
ldndc::LD_MMMasterControlProgram::m_addmaintenancemodules(
        lmodule_flag_e  _ini_fin)
{
    unsigned int  k( 0);

    /* module factory is assumed to hold no duplicates */
    while ( module_factories[k].exists())
    {
        module_info_t const &  m_inf( module_factories[k].info());
        if (( m_inf.mflags & LMOD_FLAG_MAINTENANCE) && ( m_inf.mflags & _ini_fin))
        {
            /* by definition maintenance modules support only a single timemode */
            ldndc_kassert( cbm::bits_set( m_inf.timemode) == 1u);

            lerr_t  rc = this->m_moduleadd( m_inf.id, (timemode_e)m_inf.timemode,
                    LMOD_FLAG_SYSTEM|LMOD_FLAG_MAINTENANCE|_ini_fin, LMOD_FLAG_NONE);

            if (( rc != LDNDC_ERR_OK) && ( rc != LDNDC_ERR_REQUEST_MISMATCH))
            {
                return  rc;
            }
        }
        ++k;
    }

    return  LDNDC_ERR_OK;
}


/*
 * build user module list - given setup's module selection
 */
lerr_t
ldndc::LD_MMMasterControlProgram::m_addusermodules(
        lflags_t  _mflags_req, lflags_t  _mflags_den)
{
    for ( size_t  m = 0;  m < this->m_modulessetup.number_of_modules();  ++m)
    {
        mobile_module_info_t const *  m_setup =
            this->m_modulessetup.get_mobile_module_info_by_slot( m);
        if ( !m_setup)
            { return LDNDC_ERR_RUNTIME_ERROR; }

        lerr_t  rc = this->m_moduleadd( m_setup->id.c_str(),
            m_setup->timemode, _mflags_req, LMOD_FLAG_SYSTEM|_mflags_den);
        if ( rc == LDNDC_ERR_REQUEST_MISMATCH)
        {
            /* that's ok, module was not added due to flag configuration */
            continue;
        }
        if ( rc != LDNDC_ERR_OK)
            { return  rc; }
    }
    return  LDNDC_ERR_OK;
}


// TODO  remove .. should be individually handled in models
#include  <input/species/species.h>
#include  <input/speciesparameters/speciesparameters.h>
#include  <input/soillayers/soillayers.h>
lerr_t
ldndc::LD_MMMasterControlProgram::m_mountspecies()
{
    setup::input_class_setup_t const *  setup_in =
        this->m_iokcomm->get_input_class< setup::input_class_setup_t >();
    soillayers::input_class_soillayers_t const *  soillayers_in =
        this->m_iokcomm->get_input_class< soillayers::input_class_soillayers_t >();
    species::input_class_species_t const *  species_in =
        this->m_iokcomm->get_input_class< species::input_class_species_t >();
    speciesparameters::input_class_speciesparameters_t const *  speciesparameters_in =
        this->m_iokcomm->get_input_class< speciesparameters::input_class_speciesparameters_t >();

    lerr_t  rc_mount = LDNDC_ERR_OK;

    EventAttributes const *  ev_plant = NULL;
    while (( ev_plant = this->m_PlantEvents.pop()) != NULL)
    {
        char const *  p_name = ev_plant->get( "/name", "?");
        char const *  p_type = ev_plant->get( "/type", "?");
        char const *  p_group = ev_plant->get( "/group", "?");
        if ( this->m_state.vegetation.have_plant( p_name))
        {
            KLOGERROR( "Plant '", p_name, "' already exists");
            rc_mount = LDNDC_ERR_RUNTIME_ERROR /*should be LDNDC_ERR_EXISTS*/;
            this->m_PlantEvents.clear();
            break;
        }

        species_t const *  p = NULL;
        if ( species_in)
            { p = species_in->get_species( p_name); }
        MoBiLE_PlantParameters  p_parameters;
        if ( p)
            { p_parameters = p->get_parameters(); }
        else if ( speciesparameters_in) /* assume event was not provided via input */
            { p_parameters = (*speciesparameters_in)[p_type]; }
        else
            { /* no parameters.. */ }

        MoBiLE_PlantSettings  plant_settings( setup_in->canopylayers(), p_name, p_type, p_group, &p_parameters);
        plant_settings.nb_soillayers = 2;
        if ( soillayers_in)
        {
            plant_settings.nb_soillayers = soillayers_in->soil_layer_cnt();
        }

        MoBiLE_Plant *  plant = this->m_state.vegetation.new_plant( &plant_settings);
        if ( plant)
        {
            plant->initialize();
        }
        else
        { rc_mount = LDNDC_ERR_FAIL; }
    }

    /* remove after harvest */
    for ( PlantIterator p = this->m_state.vegetation.begin();
            p != this->m_state.vegetation.end(); ++p)
    {
        if ((*p)->state == MoBiLE_Plant::harvested)
            { this->m_state.vegetation.delete_plant((*p)->cname()); }
    }

    /* mark as harvested */
    EventAttributes const *  ev_harv = NULL;
    while (( ev_harv = this->m_HarvestEvents.pop()) != NULL)
    {
        char const *  p_name = ev_harv->get( "/name", "?");

        MoBiLE_Plant *  plant = this->m_state.vegetation.get_plant( p_name);
        if ( plant)
        {
            plant->state = MoBiLE_Plant::harvested;
        }
    }

    return  rc_mount;
}

lerr_t
ldndc::LD_MMMasterControlProgram::m_switchparameters()
{
    EventAttributes const *  ev_rp = NULL;
    while ((ev_rp = m_ReparameterizeEvents.pop()) != NULL)
    {
        char const *  rp_name = ev_rp->get( "/name", "?");
        for ( PlantIterator p = this->m_state.vegetation.begin(); p != this->m_state.vegetation.end(); ++p)
        {
            if ( cbm::is_equal_i( (*p)->name().c_str(), rp_name))
            {
                species::input_class_species_t const *  species_in =
                    this->m_iokcomm->get_input_class< species::input_class_species_t >();

                species_t const *  sp = NULL;
                if ( species_in)
                {
                    sp = species_in->get_species( rp_name);
                    if ( sp)
                    {
                        MoBiLE_PlantParameters  p_parameters = sp->get_parameters();
                        (*p)->switchparameters( p_parameters);
                    }
                    else
                    {
                        KLOGERROR("species reparametrization not possible. species [",rp_name,"] does not exist.");
                        return LDNDC_ERR_FAIL;
                    }
                }
            }
        }
    }

    return  LDNDC_ERR_OK;
}


lerr_t
ldndc::LD_MMMasterControlProgram::register_ports( cbm::io_kcomm_t * _iokcomm)
{
    this->m_PlantEvents.subscribe( "plant", _iokcomm);
    if ( !this->m_PlantEvents.is_subscribed())
        { return LDNDC_ERR_FAIL; }
    this->m_HarvestEvents.subscribe( "harvest", _iokcomm);
    if ( !this->m_HarvestEvents.is_subscribed())
        { return LDNDC_ERR_FAIL; }
    this->m_ReparameterizeEvents.subscribe( "reparameterizespecies", _iokcomm);
    if ( !this->m_ReparameterizeEvents.is_subscribed())
        { return  LDNDC_ERR_FAIL; }

    return  this->m_ports.register_ports( _iokcomm);
}

lerr_t
ldndc::LD_MMMasterControlProgram::unregister_ports( cbm::io_kcomm_t * _iokcomm)
{
    this->m_PlantEvents.unsubscribe();
    this->m_HarvestEvents.unsubscribe();
    this->m_ReparameterizeEvents.unsubscribe();

    return  this->m_ports.unregister_ports( _iokcomm);
}

lerr_t
ldndc::LD_MMMasterControlProgram::read()
{
    return  LDNDC_ERR_OK;
}

lerr_t
ldndc::LD_MMMasterControlProgram::solve()
{
    lerr_t  rc_recv = this->m_ports.receive( m_state);
    if ( rc_recv)
        { return  LDNDC_ERR_FAIL; }
    
    if ( this->m_mountspecies() != LDNDC_ERR_OK)
    {
        KLOGERROR( "failed to replace or initialize species");
        return  LDNDC_ERR_FAIL;
    }

    if ( this->m_switchparameters() != LDNDC_ERR_OK)
    {
        KLOGERROR( "resetting species parameter failed");
        return  LDNDC_ERR_FAIL;
    }

    return  LDNDC_ERR_OK;
}

lerr_t
ldndc::LD_MMMasterControlProgram::integrate()
{
    return  LDNDC_ERR_OK;
}

lerr_t
ldndc::LD_MMMasterControlProgram::write()
{
    lerr_t  rc_send = this->m_ports.send( m_state);
    if ( rc_send)
        { return  LDNDC_ERR_FAIL; }
    
    return  LDNDC_ERR_OK;
}

