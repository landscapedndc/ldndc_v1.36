/*!
 * @brief
 *    Share fields from the global state of MoBiLE modules
 *
 * @author
 *    steffen klatt (created on: apr 12, 2017)
 */

#include  "mm/mm_ports.h"
#include  "legacy/state/mbe_state.h"

#include  <logging/cbm_logging.h>

#define NB_F 40


ldndc::LD_MMPorts::LD_MMPorts()
{
    nd_soil_layers = 0;
}

lerr_t
ldndc::LD_MMPorts::register_ports( cbm::io_kcomm_t *  _iokcomm)
{
    this->WaterContent20cm.publish( "WaterContent20cm", "m3/m3", _iokcomm);
    this->FieldCapacity20cm.publish( "FieldCapacity20cm", "m3/m3", _iokcomm);
    
    this->SurfaceWater.publish_and_subscribe( "SurfaceWater", "m", _iokcomm);
    this->SurfaceSnow.publish_and_subscribe( "SurfaceSnow", "kg", _iokcomm);
 
    this->AccumulatedPrecipitation.publish( "AccumulatedPrecipitation", "m", _iokcomm);
    this->AccumulatedThroughfall.publish_and_subscribe( "AccumulatedThroughfall", "m", _iokcomm);
    this->AccumulatedTranspiration.publish_and_subscribe( "AccumulatedTranspiration", "m", nd_soil_layers, _iokcomm);
    this->AccumulatedInterceptionevaporation.publish_and_subscribe( "AccumulatedInterceptionevaporation", "m", _iokcomm);
    this->AccumulatedSoilevaporation.publish_and_subscribe( "AccumulatedSoilevaporation", "m", _iokcomm);
    this->AccumulatedSurfacewaterevaporation.publish_and_subscribe( "AccumulatedSurfacewaterevaporation", "m", _iokcomm);
    this->AccumulatedRunoff.publish_and_subscribe( "AccumulatedRunoff", "m", _iokcomm);
    this->AccumulatedPercolation.publish_and_subscribe( "AccumulatedPercolation", "m", _iokcomm);
    this->AccumulatedInfiltration.publish_and_subscribe( "AccumulatedInfiltration", "m", _iokcomm);
    this->AccumulatedGroundwaterAccess.publish_and_subscribe( "AccumulatedGroundwaterAccess", "m", _iokcomm);
    this->AccumulatedGroundwaterLoss.publish_and_subscribe( "AccumulatedGroundwaterLoss", "m", _iokcomm);
    
    this->SoilPorosity.publish_and_subscribe( "SoilPorosity", "m3/m3", nd_soil_layers, _iokcomm);
    this->SoilDepth.publish_and_subscribe( "SoilDepth", "m", nd_soil_layers, _iokcomm);
    this->VolumetricWaterContent.publish_and_subscribe( "VolumetricWaterContent", "m3/m3", nd_soil_layers, _iokcomm);
    this->VolumetricFieldCapacity.publish_and_subscribe( "VolumetricFieldCapacity", "m3/m3", nd_soil_layers, _iokcomm);
    this->MaximumWaterFilledPoreSpace.publish_and_subscribe( "MaximumWaterFilledPoreSpace", "m3/m3", nd_soil_layers, _iokcomm);
    this->SaturatedHydraulicConductivity.publish_and_subscribe( "SaturatedHydraulicConductivity", "cm/min", nd_soil_layers, _iokcomm);
    this->IceContent.publish_and_subscribe( "IceContent", "kg/m3", nd_soil_layers, _iokcomm);

    /* Soilchemistry */
    this->SoilSO4.publish_and_subscribe( "S_SO4_Soil", "kg/m2", nd_soil_layers, _iokcomm);
    this->SoilNO3.publish_and_subscribe( "N_NO3_Soil", "kg/m2", nd_soil_layers, _iokcomm);
    this->SoilNH4.publish_and_subscribe( "N_NH4_Soil", "kg/m2", nd_soil_layers, _iokcomm);
    this->SoilDON.publish_and_subscribe( "N_DON_Soil", "kg/m2", nd_soil_layers, _iokcomm);
    
    this->SoilDOC.publish_and_subscribe( "C_DOC_Soil", "kg/m2", nd_soil_layers, _iokcomm);

    this->AccumulatedLeachingNO3.publish_and_subscribe( "AccumulatedLeaching_N_NO3", "kg/m2", _iokcomm);
    this->AccumulatedLeachingNH4.publish_and_subscribe( "AccumulatedLeaching_N_NH4", "kg/m2", _iokcomm);
    this->AccumulatedLeachingDON.publish_and_subscribe( "AccumulatedLeaching_N_DON", "kg/m2", _iokcomm);
    
    this->AccumulatedLeachingDOC.publish_and_subscribe( "AccumulatedLeaching_C_DOC", "kg/m2", _iokcomm);

    return LDNDC_ERR_OK;
}

lerr_t
ldndc::LD_MMPorts::unregister_ports( cbm::io_kcomm_t *)
{
    this->WaterContent20cm.unpublish();
    this->FieldCapacity20cm.unpublish();
    
    this->SurfaceWater.unpublish_and_unsubscribe();
    this->SurfaceSnow.unpublish_and_unsubscribe();
 
    this->AccumulatedPrecipitation.unpublish();
    this->AccumulatedThroughfall.unpublish_and_unsubscribe();
    this->AccumulatedTranspiration.unpublish_and_unsubscribe();
    this->AccumulatedInterceptionevaporation.unpublish_and_unsubscribe();
    this->AccumulatedSoilevaporation.unpublish_and_unsubscribe();
    this->AccumulatedSurfacewaterevaporation.unpublish_and_unsubscribe();
    this->AccumulatedRunoff.unpublish_and_unsubscribe();
    this->AccumulatedPercolation.unpublish_and_unsubscribe();
    this->AccumulatedInfiltration.unpublish_and_unsubscribe();
    this->AccumulatedGroundwaterAccess.unpublish_and_unsubscribe();
    this->AccumulatedGroundwaterLoss.unpublish_and_unsubscribe();
    
    this->SoilPorosity.unpublish_and_unsubscribe();
    this->SoilDepth.unpublish_and_unsubscribe();
    this->VolumetricWaterContent.unpublish_and_unsubscribe();
    this->VolumetricFieldCapacity.unpublish_and_unsubscribe();
    this->MaximumWaterFilledPoreSpace.unpublish_and_unsubscribe();
    this->SaturatedHydraulicConductivity.unpublish_and_unsubscribe();
    this->IceContent.unpublish_and_unsubscribe();

    /* Soilchemistry */
    this->SoilSO4.unpublish_and_unsubscribe();
    this->SoilNO3.unpublish_and_unsubscribe();
    this->SoilNH4.unpublish_and_unsubscribe();
    this->SoilDON.unpublish_and_unsubscribe();

    this->SoilDOC.unpublish_and_unsubscribe();

    this->AccumulatedLeachingNO3.unpublish_and_unsubscribe();
    this->AccumulatedLeachingNH4.unpublish_and_unsubscribe();
    this->AccumulatedLeachingDON.unpublish_and_unsubscribe();

    this->AccumulatedLeachingDOC.unpublish_and_unsubscribe();

    return LDNDC_ERR_OK;
}

lerr_t
ldndc::LD_MMPorts::receive( MoBiLE_State &_m_state)
{
    substate_watercycle_t *  wc = _m_state.get_substate< substate_watercycle_t >();
    substate_soilchemistry_t *  sc = _m_state.get_substate< substate_soilchemistry_t >();

    wc->surface_water = this->SurfaceWater.receive();
    wc->surface_ice = this->SurfaceSnow.receive();
    
    wc->accumulated_throughfall = this->AccumulatedThroughfall.receive();
    this->AccumulatedTranspiration.receive( wc->accumulated_transpiration_sl);
    wc->accumulated_interceptionevaporation = this->AccumulatedInterceptionevaporation.receive();
    wc->accumulated_soilevaporation = this->AccumulatedSoilevaporation.receive();
    wc->accumulated_surfacewaterevaporation = this->AccumulatedSurfacewaterevaporation.receive();
    wc->accumulated_runoff = this->AccumulatedRunoff.receive();
    wc->accumulated_percolation = this->AccumulatedPercolation.receive();
    wc->accumulated_infiltration = this->AccumulatedInfiltration.receive();
    wc->accumulated_groundwater_access = this->AccumulatedGroundwaterAccess.receive();
    wc->accumulated_groundwater_loss = this->AccumulatedGroundwaterLoss.receive();
    
    this->SoilPorosity.receive( sc->poro_sl);
    this->SoilDepth.receive( sc->depth_sl);
    this->VolumetricWaterContent.receive( wc->wc_sl);
    this->VolumetricFieldCapacity.receive( sc->wcmax_sl);
    this->MaximumWaterFilledPoreSpace.receive( sc->wfps_max_sl);
    this->SaturatedHydraulicConductivity.receive( sc->sks_sl);
    this->IceContent.receive( wc->ice_sl);
    
    /* Soilchemistry */
    this->SoilSO4.receive( sc->so4_sl);
    this->SoilNO3.receive( sc->no3_sl);
    this->SoilNH4.receive( sc->nh4_sl);
    this->SoilDON.receive( sc->don_sl);

    this->SoilDOC.receive( sc->doc_sl);

    sc->accumulated_no3_leach = this->AccumulatedLeachingNO3.receive();
    sc->accumulated_nh4_leach = this->AccumulatedLeachingNH4.receive();
    sc->accumulated_don_leach = this->AccumulatedLeachingDON.receive();

    sc->accumulated_doc_leach = this->AccumulatedLeachingDOC.receive();

    return LDNDC_ERR_OK;
}

lerr_t
ldndc::LD_MMPorts::send( MoBiLE_State &_m_state)
{
    substate_watercycle_t *  wc = _m_state.get_substate< substate_watercycle_t >();
    substate_soilchemistry_t *  sc = _m_state.get_substate< substate_soilchemistry_t >();

    double wc_20cm( wc->wc_sl[0]);
    double fc_20cm( sc->wcmax_sl[0]);
    size_t layer_count( 1);
    for (size_t sl = 1; sl < nd_soil_layers; sl++)
    {
        if ( cbm::flt_less_equal( sc->depth_sl[sl], 0.2))
        {
            wc_20cm += wc->wc_sl[sl];
            fc_20cm += sc->wcmax_sl[sl];
            layer_count += 1;
        }
        else
        {
            break;
        }
    }
    this->WaterContent20cm.send( wc_20cm / layer_count);
    this->FieldCapacity20cm.send( fc_20cm / layer_count);

    this->SurfaceWater.send( wc->surface_water);
    this->SurfaceSnow.send( wc->surface_ice);

    this->AccumulatedPrecipitation.send( wc->accumulated_precipitation);
    this->AccumulatedThroughfall.send( wc->accumulated_throughfall);
    this->AccumulatedTranspiration.send( wc->accumulated_transpiration_sl);
    this->AccumulatedInterceptionevaporation.send( wc->accumulated_interceptionevaporation);
    this->AccumulatedSoilevaporation.send( wc->accumulated_soilevaporation);
    this->AccumulatedSurfacewaterevaporation.send( wc->accumulated_surfacewaterevaporation);
    this->AccumulatedRunoff.send( wc->accumulated_runoff);
    this->AccumulatedPercolation.send( wc->accumulated_percolation);
    this->AccumulatedInfiltration.send( wc->accumulated_infiltration);
    this->AccumulatedGroundwaterAccess.send( wc->accumulated_groundwater_access);
    this->AccumulatedGroundwaterLoss.send( wc->accumulated_groundwater_loss);
    
    this->SoilPorosity.send( sc->poro_sl);
    this->SoilDepth.send( sc->depth_sl);
    this->VolumetricWaterContent.send( wc->wc_sl);
    this->VolumetricFieldCapacity.send( sc->wcmax_sl);
    this->MaximumWaterFilledPoreSpace.send( sc->wfps_max_sl);
    this->SaturatedHydraulicConductivity.send( sc->sks_sl);
    this->IceContent.send( wc->ice_sl);

    /* Soilchemistry */
    this->SoilSO4.send( sc->so4_sl);
    this->SoilNO3.send( sc->no3_sl);
    this->SoilNH4.send( sc->nh4_sl);
    this->SoilDON.send( sc->don_sl);

    this->SoilDOC.send( sc->doc_sl);

    this->AccumulatedLeachingNO3.send( sc->accumulated_no3_leach);
    this->AccumulatedLeachingNH4.send( sc->accumulated_nh4_leach);
    this->AccumulatedLeachingDON.send( sc->accumulated_don_leach);

    this->AccumulatedLeachingDOC.send( sc->accumulated_doc_leach);
    
    return LDNDC_ERR_OK;
}

