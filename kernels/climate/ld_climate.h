/*!
 * @brief
 *    Provides climatic driving data.
 *
 * @author
 *    steffen klatt (created on: feb 28, 2017)
 */

#ifndef  LDNDC_KERNEL_LDCLIMATE_H_
#define  LDNDC_KERNEL_LDCLIMATE_H_

#include  "ld_kernel.h"
#include  "ld_shared.h"

namespace cbm {
    struct  entities_t;
    struct  services_t;
}

namespace ldndc {

class LDNDC_API LK_Climate : public cbm::kernel_t
{
    LDNDC_KERNEL_OBJECT(LK_Climate,climate)
    public:
        LK_Climate();
        ~LK_Climate();

    public:
        lerr_t  configure( cbm::RunLevelArgs *);
        lerr_t  register_ports( cbm::RunLevelArgs *);
        lerr_t  initialize( cbm::RunLevelArgs *);
        lerr_t  read( cbm::RunLevelArgs *);
        lerr_t  unregister_ports( cbm::RunLevelArgs *);

    protected:
        input_class_srv_base_t *  m_input;
        struct  LD_ClimatePorts
        {
            PublishedField<double>  LongWaveRadiance;
            PublishedField<double>  Precipitation;
            PublishedField<double>  AirPressure;
            PublishedField<double>  ShortWaveRadiance;
            PublishedField<double>  MinimumAirTemperature;
            PublishedField<double>  AirTemperature;
            PublishedField<double>  MaximumAirTemperature;
            PublishedField<double>  RelativeHumidity;
            PublishedField<double>  WaterVaporSaturationDeficit;
            PublishedField<double>  Windspeed;
        };
        LD_ClimatePorts  m_HourPorts, m_DayPorts;

    private:
        /* hide these buggers for now */
        LK_Climate( LK_Climate const &);
        LK_Climate &  operator=( LK_Climate const &);
};
} /* namespace ldndc */

#endif /* !LDNDC_KERNEL_LDCLIMATE_H_ */

