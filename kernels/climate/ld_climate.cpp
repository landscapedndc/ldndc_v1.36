/*!
 * @author
 *    steffen klatt (created on: feb 28, 2017)
 */

#include  "climate/ld_climate.h"
#include  <input/climate/climate.h>

#include  <time/cbm_time.h>
#include  <logging/cbm_logging.h>

LDNDC_KERNEL_OBJECT_DEFN(LK_Climate,climate,_LD_Climate,"LandscapeDNDC Climate")
ldndc::LK_Climate::LK_Climate()
        : cbm::kernel_t(), m_input( NULL)
    { }

ldndc::LK_Climate::~LK_Climate()
    { }

lerr_t
ldndc::LK_Climate::register_ports( cbm::RunLevelArgs *  _args)
{
    this->m_DayPorts.LongWaveRadiance.publish( "DayAverageLongWaveRadiance", "W/m2", _args->iokcomm);
    this->m_HourPorts.LongWaveRadiance.publish( "LongWaveRadiance", "W/m2", _args->iokcomm);

    this->m_DayPorts.Precipitation.publish( "DayPrecipitation", "mm/m2", _args->iokcomm);
    this->m_HourPorts.Precipitation.publish( "Precipitation", "mm/m2", _args->iokcomm);

    this->m_DayPorts.AirPressure.publish( "DayAverageAirPressure", "mbar", _args->iokcomm);
    this->m_HourPorts.AirPressure.publish( "AirPressure", "mbar", _args->iokcomm);

    this->m_DayPorts.ShortWaveRadiance.publish( "DayAverageShortWaveRadiance", "W/m2", _args->iokcomm);
    this->m_HourPorts.ShortWaveRadiance.publish( "ShortWaveRadiance", "W/m2", _args->iokcomm);

    this->m_DayPorts.MinimumAirTemperature.publish( "DayAverageMinimumAirTemperature", "°C", _args->iokcomm);
    this->m_HourPorts.MinimumAirTemperature.publish( "MinimumAirTemperature", "°C", _args->iokcomm);

    this->m_DayPorts.AirTemperature.publish( "DayAverageAirTemperature", "°C", _args->iokcomm);
    this->m_HourPorts.AirTemperature.publish( "AirTemperature", "°C", _args->iokcomm);

    this->m_DayPorts.MaximumAirTemperature.publish( "DayAverageMaximumAirTemperature", "°C", _args->iokcomm);
    this->m_HourPorts.MaximumAirTemperature.publish( "MaximumAirTemperature", "°C", _args->iokcomm);

    this->m_DayPorts.RelativeHumidity.publish( "DayAverageRelativeHumidity", "%", _args->iokcomm);
    this->m_HourPorts.RelativeHumidity.publish( "RelativeHumidity", "%", _args->iokcomm);

    this->m_DayPorts.WaterVaporSaturationDeficit.publish( "DayAverageWaterVaporSaturationDeficit", "kPa", _args->iokcomm);
    this->m_HourPorts.WaterVaporSaturationDeficit.publish( "WaterVaporSaturationDeficit", "kPa", _args->iokcomm);

    this->m_DayPorts.Windspeed.publish( "DayAverageWindspeed", "m/s", _args->iokcomm);
    this->m_HourPorts.Windspeed.publish( "Windspeed", "m/s", _args->iokcomm);

    return  LDNDC_ERR_OK;
}
lerr_t
ldndc::LK_Climate::unregister_ports( cbm::RunLevelArgs *)
{
    this->m_HourPorts.AirPressure.unpublish();
    this->m_HourPorts.AirTemperature.unpublish();
    this->m_HourPorts.LongWaveRadiance.unpublish();
    this->m_HourPorts.MaximumAirTemperature.unpublish();
    this->m_HourPorts.MinimumAirTemperature.unpublish();
    this->m_HourPorts.Precipitation.unpublish();
    this->m_HourPorts.RelativeHumidity.unpublish();
    this->m_HourPorts.ShortWaveRadiance.unpublish();
    this->m_HourPorts.WaterVaporSaturationDeficit.unpublish();
    this->m_HourPorts.Windspeed.unpublish();
    
    this->m_DayPorts.AirPressure.unpublish();
    this->m_DayPorts.AirTemperature.unpublish();
    this->m_DayPorts.LongWaveRadiance.unpublish();
    this->m_DayPorts.MaximumAirTemperature.unpublish();
    this->m_DayPorts.MinimumAirTemperature.unpublish();
    this->m_DayPorts.Precipitation.unpublish();
    this->m_DayPorts.RelativeHumidity.unpublish();
    this->m_DayPorts.ShortWaveRadiance.unpublish();
    this->m_DayPorts.WaterVaporSaturationDeficit.unpublish();
    this->m_DayPorts.Windspeed.unpublish();

    return  LDNDC_ERR_OK;
}

static lerr_t  s_FetchInput( ldndc::input_class_srv_base_t ** _input,
            char const * _itype, cbm::RunLevelArgs *  _args)
{
    *_input = NULL;

    cbm::source_descriptor_t  objId;
    lid_t const  inputId =
        _args->iokcomm->resolve_source( _itype, &objId);
    if ( inputId == invalid_lid)
        { return LDNDC_ERR_RUNTIME_ERROR; }
    else
    {
        *_input = _args->iokcomm->fetch_input( _itype, &objId);
        if ( *_input == NULL)
            { return LDNDC_ERR_FAIL; }
    }
    return LDNDC_ERR_OK;
}
static lerr_t  s_UpdateInput( ldndc::input_class_srv_base_t * _input,
                cbm::RunLevelArgs *  _args)
{
    lerr_t rc_update = LDNDC_ERR_OK;
    if ( _input)
        { rc_update = _input->update_internal_state( *_args->clk); }
    return rc_update;
}

lerr_t
ldndc::LK_Climate::configure( cbm::RunLevelArgs *  _args)
{
    if ( s_FetchInput( &this->m_input, "climate", _args) != LDNDC_ERR_OK)
        { return LDNDC_ERR_FAIL; }
    return LDNDC_ERR_OK;
}

lerr_t
ldndc::LK_Climate::initialize( cbm::RunLevelArgs *)
    { return LDNDC_ERR_OK; }

lerr_t
ldndc::LK_Climate::read( cbm::RunLevelArgs *  _args)
{
    climate::input_class_climate_t const *  climate_in =
            _args->iokcomm->get_input_class< climate::input_class_climate_t >();
    if ( !climate_in)
        { return LDNDC_ERR_OK; }

    if ( s_UpdateInput( this->m_input, _args) != LDNDC_ERR_OK)
        { return LDNDC_ERR_FAIL; }

    cbm::sclock_t const &  clk = *_args->clk;
    if ( clk.is_position( TMODE_SUBDAILY))
    {
        double const  LongWaveRadiance = climate_in->long_rad_subday( clk);
        this->m_HourPorts.LongWaveRadiance.send( LongWaveRadiance);
        double const  Precipitation = climate_in->precip_subday( clk);
        this->m_HourPorts.Precipitation.send( Precipitation);
        double const  AirPressure = climate_in->air_pressure_subday( clk);
        this->m_HourPorts.AirPressure.send( AirPressure);
        double const  ShortWaveRadiance = climate_in->ex_rad_subday( clk);
        this->m_HourPorts.ShortWaveRadiance.send( ShortWaveRadiance);
        double const  MinimumAirTemperature = climate_in->temp_min_subday( clk);
        this->m_HourPorts.MinimumAirTemperature.send( MinimumAirTemperature);
        double const  AirTemperature = climate_in->temp_avg_subday( clk);
        this->m_HourPorts.AirTemperature.send( AirTemperature);
        double const  MaximumAirTemperature = climate_in->temp_max_subday( clk);
        this->m_HourPorts.MaximumAirTemperature.send( MaximumAirTemperature);
        double const  RelativeHumidity = climate_in->rel_humidity_subday( clk);
        this->m_HourPorts.RelativeHumidity.send( RelativeHumidity);
        double const  WaterVaporSaturationDeficit = climate_in->vpd_subday( clk);
        this->m_HourPorts.WaterVaporSaturationDeficit.send( WaterVaporSaturationDeficit);
        double const  Windspeed = climate_in->wind_speed_subday( clk);
        this->m_HourPorts.Windspeed.send( Windspeed);

// sk:dbg        CBM_LogWrite( LongWaveRadiance, "; ",Precipitation, "; ",AirPressure, "; ",ShortWaveRadiance, "; ",MinimumAirTemperature, "; ",AirTemperature,
// sk:dbg                "; ",MaximumAirTemperature, "; ",RelativeHumidity, "; ",WaterVaporSaturationDeficit, "; ",Windspeed);
    }

    if ( clk.is_position( TMODE_PRE_DAILY))
    {
        double const  LongWaveRadiance = climate_in->long_rad_day( clk);
        this->m_DayPorts.LongWaveRadiance.send( LongWaveRadiance);
        double const  Precipitation = climate_in->precip_day( clk);
        this->m_DayPorts.Precipitation.send( Precipitation);
        double const  AirPressure = climate_in->air_pressure_day( clk);
        this->m_DayPorts.AirPressure.send( AirPressure);
        double const  ShortWaveRadiance = climate_in->ex_rad_day( clk);
        this->m_DayPorts.ShortWaveRadiance.send( ShortWaveRadiance);
        double const  MinimumAirTemperature = climate_in->temp_min_day( clk);
        this->m_DayPorts.MinimumAirTemperature.send( MinimumAirTemperature);
        double const  AirTemperature = climate_in->temp_avg_day( clk);
        this->m_DayPorts.AirTemperature.send( AirTemperature);
        double const  MaximumAirTemperature = climate_in->temp_max_day( clk);
        this->m_DayPorts.MaximumAirTemperature.send( MaximumAirTemperature);
        double const  RelativeHumidity = climate_in->rel_humidity_day( clk);
        this->m_DayPorts.RelativeHumidity.send( RelativeHumidity);
        double const  WaterVaporSaturationDeficit = climate_in->vpd_day( clk);
        this->m_DayPorts.WaterVaporSaturationDeficit.send( WaterVaporSaturationDeficit);
        double const  Windspeed = climate_in->wind_speed_day( clk);
        this->m_DayPorts.Windspeed.send( Windspeed);
    }

    return LDNDC_ERR_OK;
}

