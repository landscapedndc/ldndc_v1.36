/*!
 * @author
 *    steffen klatt (created on: aug 26, 2012)
 */
#ifndef  LDNDC_KERNELS_H_
#define  LDNDC_KERNELS_H_

#include  "ldndc-dllexport.h"
#include  "ldndc-config.h.inc"

namespace ldndc {
struct kernels_info_t
{
	static void const *  registered_kernels();
    static void list_available_kernels( unsigned char  /*verbose level*/);
};
} /* namespace ldndc */


#ifdef __cplusplus
extern "C" {
#endif

LDNDC_API
extern void const *  ldndc_registered_kernels();

LDNDC_API
extern void  ldndc_list_available_kernels( unsigned char /*verbose level*/);

LDNDC_API
extern int  ldndc_query_kernel( char ** /*result*/,
        char const * /*kernel*/, char const * /*query*/);

#ifdef __cplusplus
}
#endif


#endif  /*  !LDNDC_KERNELS_H_  */

