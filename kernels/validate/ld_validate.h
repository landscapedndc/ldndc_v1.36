/*!
 * @brief
 *    input check is a kernel that validates model inputs
 *
 * @author
 *    steffen klatt (created on: aug 23, 2012),
 *    edwin haas
 */

#ifndef LDNDC_KERNEL_VALIDATE_H_
#define LDNDC_KERNEL_VALIDATE_H_

#include  "ld_kernel.h"

namespace ldndc {

class LDNDC_API LK_Validate : public cbm::kernel_t
{
    LDNDC_KERNEL_OBJECT(LK_Validate,validate)
    public:
        LK_Validate();
        ~LK_Validate();

    public:
        lerr_t  configure( cbm::RunLevelArgs *);
        lerr_t  initialize( cbm::RunLevelArgs *);

        lerr_t  read( cbm::RunLevelArgs *);

    protected:
        bool  abort_on_error_;

    protected:
        lerr_t (LK_Validate::*chk_ic_[INPUT_CNT])();

        lerr_t  chk_air_chemistry();
        lerr_t  chk_climate();
        lerr_t  chk_event();
        lerr_t  chk_setup();
        lerr_t  chk_site();
        lerr_t  chk_site_parameters();
        lerr_t  chk_soil_parameters();
        lerr_t  chk_species_parameters();

    private:
        /* hide these buggers for now */
        LK_Validate( LK_Validate const &);
        LK_Validate &  operator=( LK_Validate const &);
};

} /* namespace ldndc */

#endif /* !LDNDC_KERNEL_VALIDATE_H_ */

