/*!
 * @brief
 *    input check is a kernel that validates model inputs
 *
 * @author
 *    steffen klatt (created on: aug 23, 2012),
 *    edwin haas
 */

#include  "validate/ld_validate.h"
#include  <logging/cbm_logging.h>

/* input classes */
#include  <input/setup/setup.h>

LDNDC_KERNEL_OBJECT_DEFN(LK_Validate,validate,"Validate","Validate")
ldndc::LK_Validate::LK_Validate()
        : cbm::kernel_t(),
          cbm::clock_reference_owner_t( NULL),
          abort_on_error_( false)
    { }

ldndc::LK_Validate::~LK_Validate()
    { }

lerr_t
ldndc::LK_Validate::configure( cbm::RunLevelArgs *  _args)
{
    this->set_lclock( _args->clk);

    // TODO  configure "abort on error"
    this->abort_on_error_ = false;
    _args->cfg->query( "kernel:validate", "abort_on_error", &this->abort_on_error_);

    /* unset all ic validation routines */
    for ( size_t  c = 0;  c < INPUT_CNT;  ++c)
    {
        this->chk_ic_[c] = NULL;
    }

    /* set all ic validation routines */
    this->chk_ic_[INPUT_AIRCHEMISTRY] = &LK_Validate::chk_air_chemistry;
    this->chk_ic_[INPUT_CLIMATE] = &LK_Validate::chk_climate;
    this->chk_ic_[INPUT_EVENT] = &LK_Validate::chk_event;
    this->chk_ic_[INPUT_SETUP] = &LK_Validate::chk_setup;
    this->chk_ic_[INPUT_SITE] = &LK_Validate::chk_site;
    this->chk_ic_[INPUT_SITEPARAMETERS] = &LK_Validate::chk_site_parameters;
    this->chk_ic_[INPUT_SOILPARAMETERS] = &LK_Validate::chk_soil_parameters;
    this->chk_ic_[INPUT_SPECIESPARAMETERS] = &LK_Validate::chk_species_parameters;

    // TODO  set up test routine vector
    for ( size_t  c = __INPUT_AIC_BEGIN;  c < __INPUT_AIC_END;  ++c)
    {
        if ( !this->chk_ic_[c])
        {
            KLOGWARN( "missing validate routine for input-class \"",INPUT_NAMES[c],"\"");
            continue;
        }

        std::string const  have_ic_option( "have_"+cbm::n2s( INPUT_NAMES[c]));
        bool  do_chk_ic( true);

        if ( _config->query( "kernel:validate", have_ic_option.c_str(), &do_chk_ic) == LDNDC_ERR_OK)
        {
            if ( !do_chk_ic)
            {
                KLOGDEBUG( "deactivating input class data check:  class=", INPUT_NAMES[c]);
                this->chk_ic_[c] = NULL;
            }
        }
    }

    return  LDNDC_ERR_OK;
}

/*
 * check each input-class' input data
 */
lerr_t
ldndc::LK_Validate::read( cbm::RunLevelArgs *)
{
    lerr_t  rc_ic[INPUT_CNT];
    for ( size_t  c = 0;  c < INPUT_CNT;  ++c)
    {
        if ( this->chk_ic_[c])
        {
            rc_ic[c] = (this->*LK_Validate::chk_ic_[c])();
        }
        else
        {
            rc_ic[c] = LDNDC_ERR_OK;
        }
    }

    /* check outcome */
    unsigned int  rc( LDNDC_ERR_OK);
    if ( abort_on_error_)
    {
        for ( size_t  c = 0;  c < INPUT_CNT;  ++c)
        {
            // ...
            rc |= rc_ic[c];
        }
    }
    else
    {
        rc = LDNDC_ERR_OK;
    }

        return  ( rc == LDNDC_ERR_OK) ? LDNDC_ERR_OK : LDNDC_ERR_FAIL;
}

lerr_t
ldndc::LK_Validate::chk_air_chemistry()
{
    KLOGDEBUG( "checking air chemistry IC,  time=", this->lclock()->to_string());
    return  LDNDC_ERR_OK;
}

lerr_t
ldndc::LK_Validate::chk_climate()
{
    KLOGDEBUG( "checking climate IC,  time=", this->lclock()->to_string());
    return  LDNDC_ERR_OK;
}

lerr_t
ldndc::LK_Validate::chk_event()
{
    /* input-class is not (yet) time-dependent */
    KLOGDEBUG( "checking events IC,  time=", this->lclock()->to_string());
    this->chk_ic_[INPUT_EVENT] = NULL;

    return  LDNDC_ERR_OK;
}

lerr_t
ldndc::LK_Validate::chk_setup()
{
    /* input-class is not time-dependent */
    KLOGDEBUG( "checking setup IC,  time=", this->lclock()->to_string());
    this->chk_ic_[INPUT_SETUP] = NULL;

    return  LDNDC_ERR_OK;
}

lerr_t
ldndc::LK_Validate::chk_site()
{
    /* input-class is not time-dependent */
    KLOGDEBUG( "checking site IC,  time=", this->lclock()->to_string());
    this->chk_ic_[INPUT_SITE] = NULL;

    return  LDNDC_ERR_OK;
}

lerr_t
ldndc::LK_Validate::chk_site_parameters()
{
    /* input-class is not time-dependent */
    KLOGDEBUG( "checking site parameters IC,  time=", this->lclock()->to_string());
    this->chk_ic_[INPUT_SITEPARAMETERS] = NULL;

    return  LDNDC_ERR_OK;
}

lerr_t
ldndc::LK_Validate::chk_soil_parameters()
{
    /* input-class is not time-dependent */
    KLOGDEBUG( "checking soil parameters IC,  time=", this->lclock()->to_string());
    this->chk_ic_[INPUT_SOILPARAMETERS] = NULL;

    return  LDNDC_ERR_OK;
}

lerr_t
ldndc::LK_Validate::chk_species_parameters()
{
    /* input-class is not time-dependent */
    KLOGDEBUG( "checking species parameters IC,  time=", this->lclock()->to_string());
    this->chk_ic_[INPUT_SPECIESPARAMETERS] = NULL;

    return  LDNDC_ERR_OK;
}

