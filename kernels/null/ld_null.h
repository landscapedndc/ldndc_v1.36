/*!
 * @brief
 *    an empty model
 *
 * @author
 *    steffen klatt (created on: oct 8, 2011)
 */

#ifndef  LDNDC_KERNEL_NULL_H_
#define  LDNDC_KERNEL_NULL_H_

#include  "ld_kernel.h"

namespace ldndc {

class LDNDC_API LK_Null : public cbm::kernel_t
{
    LDNDC_KERNEL_OBJECT(LK_Null,null)
    public:
        LK_Null();
        ~LK_Null();

    private:
        /* hide them.. */
        LK_Null( LK_Null const &);
        LK_Null &  operator=( LK_Null const &);
};

} /* namespace ldndc */

#endif /* !LDNDC_KERNEL_NULL_H_ */

