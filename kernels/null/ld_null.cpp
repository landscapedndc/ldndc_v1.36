/*!
 * @brief
 *    an empty model (implementation)
 *
 * @author
 *    steffen klatt (created on: oct 8, 2011)
 */

#include  "null/ld_null.h"

LDNDC_KERNEL_OBJECT_DEFN(LK_Null,null,"Null","Null")
ldndc::LK_Null::LK_Null()
        : cbm::kernel_t()
    { }

ldndc::LK_Null::~LK_Null()
    { }

