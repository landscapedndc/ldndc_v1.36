/*!
 * @author
 *    steffen klatt (created on: jul 24, 2017)
 */

#include  "groundwater/ld_groundwater.h"
#include  <input/groundwater/groundwater.h>

#include  <time/cbm_time.h>
#include  <logging/cbm_logging.h>

LDNDC_KERNEL_OBJECT_DEFN(LK_Groundwater,groundwater,_LD_Groundwater,"LandscapeDNDC Groundwater")
ldndc::LK_Groundwater::LK_Groundwater()
        : cbm::kernel_t(), m_input( NULL)
    { }

ldndc::LK_Groundwater::~LK_Groundwater()
    { }

lerr_t
ldndc::LK_Groundwater::register_ports( cbm::RunLevelArgs *)
{
    /*TODO register day/hour ports*/
    return  LDNDC_ERR_OK;
}
lerr_t
ldndc::LK_Groundwater::unregister_ports( cbm::RunLevelArgs *)
{
    /*TODO unregister day/hour ports*/
    return  LDNDC_ERR_OK;
}

static lerr_t  s_FetchInput( ldndc::input_class_srv_base_t ** _input,
            char const * _itype, cbm::RunLevelArgs *  _args)
{
    *_input = NULL;

    cbm::source_descriptor_t  objId;
    lid_t const  inputId =
        _args->iokcomm->resolve_source( _itype, &objId);
    if ( inputId == invalid_lid)
        { return LDNDC_ERR_RUNTIME_ERROR; }
    else
    {
        *_input = _args->iokcomm->fetch_input( _itype, &objId);
        if ( *_input == NULL)
            { return LDNDC_ERR_FAIL; }
    }
    return LDNDC_ERR_OK;
}
static lerr_t  s_UpdateInput( ldndc::input_class_srv_base_t * _input,
                cbm::RunLevelArgs *  _args)
{
    lerr_t rc_update = LDNDC_ERR_OK;
    if ( _input)
        { rc_update = _input->update_internal_state( *_args->clk); }
    return rc_update;
}

lerr_t
ldndc::LK_Groundwater::configure( cbm::RunLevelArgs *  _args)
{
    if ( s_FetchInput( &this->m_input, "groundwater", _args) != LDNDC_ERR_OK)
        { return LDNDC_ERR_FAIL; }
    return LDNDC_ERR_OK;
}

lerr_t
ldndc::LK_Groundwater::initialize( cbm::RunLevelArgs *)
    { return LDNDC_ERR_OK; }

lerr_t
ldndc::LK_Groundwater::read( cbm::RunLevelArgs *  _args)
{
    groundwater::input_class_groundwater_t const *  groundwater_in =
            _args->iokcomm->get_input_class< groundwater::input_class_groundwater_t >();
    if ( !groundwater_in)
        { return LDNDC_ERR_OK; }

    if ( s_UpdateInput( this->m_input, _args) != LDNDC_ERR_OK)
        { return LDNDC_ERR_FAIL; }

    cbm::sclock_t const &  clk = *_args->clk;
    if ( clk.is_position( TMODE_SUBDAILY))
    {
        /*TODO send hour ports*/
    }

    if ( clk.is_position( TMODE_PRE_DAILY))
    {
        /*TODO send day ports*/
    }

    return LDNDC_ERR_OK;
}

