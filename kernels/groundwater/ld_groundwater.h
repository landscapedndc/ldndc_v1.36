/*!
 * @brief
 *    Provides groundwater related driving data.
 *
 * @author
 *    steffen klatt (created on: jul 24, 2017)
 */

#ifndef  LDNDC_KERNEL_LDGROUNDWATER_H_
#define  LDNDC_KERNEL_LDGROUNDWATER_H_

#include  "ld_kernel.h"
//TODO #include  "ld_shared.h"

namespace ldndc {

class LDNDC_API LK_Groundwater : public cbm::kernel_t
{
    LDNDC_KERNEL_OBJECT(LK_Groundwater,groundwater)
    public:
        LK_Groundwater();
        ~LK_Groundwater();

    public:
        lerr_t  configure( cbm::RunLevelArgs *);
        lerr_t  register_ports( cbm::RunLevelArgs *);
        lerr_t  initialize( cbm::RunLevelArgs *);
        lerr_t  read( cbm::RunLevelArgs *);
        lerr_t  unregister_ports( cbm::RunLevelArgs *);

    protected:
        input_class_srv_base_t *  m_input;
        struct  LD_GroundwaterPorts
        {
//TODO            PublishedField<double>  WaterTable;
        };
//TODO        LD_GroundwaterPorts  m_HourPorts, m_DayPorts;

    private:
        /* hide these buggers for now */
        LK_Groundwater( LK_Groundwater const &);
        LK_Groundwater &  operator=( LK_Groundwater const &);
};
} /* namespace ldndc */

#endif /* !LDNDC_KERNEL_LDGROUNDWATER_H_ */

