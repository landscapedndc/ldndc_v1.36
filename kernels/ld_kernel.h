/*!
 * @brief
 *  base header for LandscapeDNDC kernels
 *
 * @author
 *  steffen klatt (created on: may 14, 2017)
 */

#ifndef  LD_KERNEL_H_
#define  LD_KERNEL_H_


#include  "ldndc.h"
#include  "ld_defaults.h"

#include  <kernel/cbm_kernel.h>

#define  LDNDC_KERNEL_OBJECT(__type__,__id__) \
    CBM_KERNEL_OBJECT(__type__,__id__)

#define  LDNDC_KERNEL_OBJECT_DEFN(__type__,__id__,__factory_name__,__kname__) \
    CBM_KERNEL_LINKME(__id__) \
    CBM_KERNEL_OBJECT_DEFN(ldndc,__type__,__id__,__factory_name__,__kname__)

/*signals unused source descriptor*/
#define  LD_NOSOURCE ""
#define  LD_NOID invalid_lid

#endif /* !LD_KERNEL_H_ */

