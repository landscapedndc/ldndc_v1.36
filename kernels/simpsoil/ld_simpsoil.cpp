/*!
 * @author
 *    David Kraus
 */

#include  "simpsoil/ld_simpsoil.h"
#include  <input/setup/setup.h>
#include  <logging/cbm_logging.h>
#include  "app/ld_init.h"
#include  <io-dcomm.h>

LDNDC_KERNEL_OBJECT_DEFN(SimpSoil,simpsoil,"SimpSoil","LandscapeDNDC SimpSoil")

//cbm::string_t ldndc::SimpSoil::data_file = "";
//std::string ldndc::SimpSoil::data_file_content = "";

ldndc::SimpSoil::SimpSoil() : cbm::kernel_t(),
                            m_soil( NULL),
                            no3_sl( lvector_t< double >( 0)),
                            nh4_sl( lvector_t< double >( 0)),
                            urea_sl( lvector_t< double >( 0))
{
}


ldndc::SimpSoil::~SimpSoil()
{}



lerr_t
ldndc::SimpSoil::configure( cbm::RunLevelArgs *_args)
{
    m_soil = _args->iokcomm->get_input_class< input_class_soillayers_t >();

    return LDNDC_ERR_OK;
}

lerr_t
ldndc::SimpSoil::initialize( cbm::RunLevelArgs *)
{
    if ( m_soil)
    {
        no3_sl.resize_and_preserve( m_soil->soil_layer_cnt(), 0.0);
        nh4_sl.resize_and_preserve( m_soil->soil_layer_cnt(), 0.0);
        urea_sl.resize_and_preserve( m_soil->soil_layer_cnt(), 0.0);
    }
    
    return LDNDC_ERR_OK;
}


int
ldndc::SimpSoil::onFertilize( ldndc::EventAttributes const * _fert)
{
    NH4.receive( nh4_sl);

    double const fertilizer_recovery( 0.75);
    double const amount = _fert->get( "/amount", 0.0) * fertilizer_recovery;
    nh4_sl[0] += amount;

    NH4.send( nh4_sl);

    return 0;
}


lerr_t
ldndc::SimpSoil::register_ports( cbm::RunLevelArgs *_args)
{
    m_FertilizeEvent.subscribe( "fertilize", _args->iokcomm);
    m_FertilizeEvent.handler( &SimpSoil::onFertilize, this);
    
    NO3.publish_and_subscribe( "N_NO3_Soil", "kg/ha", m_soil->soil_layer_cnt(), _args->iokcomm);
    NH4.publish_and_subscribe( "N_NH4_Soil", "kg/ha", m_soil->soil_layer_cnt(), _args->iokcomm);
    
    return LDNDC_ERR_OK;
}


lerr_t
ldndc::SimpSoil::unregister_ports( cbm::RunLevelArgs *)
{
    NO3.unpublish_and_unsubscribe();
    NH4.unpublish_and_unsubscribe();
    
    return LDNDC_ERR_OK;
}

lerr_t
ldndc::SimpSoil::read( cbm::RunLevelArgs *)
{
    return LDNDC_ERR_OK;
}

lerr_t
ldndc::SimpSoil::solve( cbm::RunLevelArgs *_args)
{
    NO3.receive( no3_sl);
    NH4.receive( nh4_sl);

    //0.8 soil supply
    if ( cbm::flt_less( no3_sl.sum() + nh4_sl.sum(), 1.0))
    {
        no3_sl[0] += 0.4 / _args->clk->time_resolution();
        nh4_sl[0] += 0.4 / _args->clk->time_resolution();
    }

    if ( !cbm::flt_greater_zero( no3_sl.sum()))
    {
        no3_sl = 0.0;
    }
    if ( !cbm::flt_greater_zero( nh4_sl.sum()))
    {
        nh4_sl = 0.0;
    }

    NO3.send( no3_sl);
    NH4.send( nh4_sl);

    return LDNDC_ERR_OK;
}

lerr_t
ldndc::SimpSoil::finalize( cbm::RunLevelArgs *)
{
    if ( m_soil)
    {
        m_soil = NULL;
    }
    
    return LDNDC_ERR_OK;
}

