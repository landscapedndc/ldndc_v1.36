/*!
 * @brief
 *
 * @author
 *    David Kraus
 */

#ifndef  LDNDC_KERNEL_SIMPSOIL_H_
#define  LDNDC_KERNEL_SIMPSOIL_H_

#include  "ld_kernel.h"
#include  "ld_shared.h"
#include  "state/mbe_state.h"

namespace ldndc {

class LDNDC_API SimpSoil : public cbm::kernel_t
{
    LDNDC_KERNEL_OBJECT(SimpSoil,simpsoil)

public:
    SimpSoil();
    ~SimpSoil();

    lerr_t  configure( cbm::RunLevelArgs *);
    lerr_t  initialize( cbm::RunLevelArgs *);
    
    lerr_t  register_ports( cbm::RunLevelArgs *);
    lerr_t  unregister_ports( cbm::RunLevelArgs *);
    
    //we solve with run due to mobile communication
    lerr_t  read( cbm::RunLevelArgs *);
    lerr_t  solve(cbm::RunLevelArgs *);
    lerr_t  finalize( cbm::RunLevelArgs *);
    
    int  onFertilize( ldndc::EventAttributes const *);
    
private:
    
    input_class_soillayers_t const * m_soil;

    lvector_t< double > no3_sl;
    lvector_t< double > nh4_sl;
    lvector_t< double > urea_sl;
    
    SubscribedEvent<LD_EventHandlerCall<SimpSoil> >  m_FertilizeEvent;
    
    PublishedAndSubscribedVectorField<double> NO3;
    PublishedAndSubscribedVectorField<double> NH4;

    /* hide these buggers for now */
    SimpSoil( SimpSoil const &);
    SimpSoil &  operator=( SimpSoil const &);
};
} /* namespace ldndc */

#endif /* !LDNDC_KERNEL_SIMPSOIL_H_ */

