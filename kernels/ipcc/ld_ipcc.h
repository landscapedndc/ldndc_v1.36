/*!
 * @brief
 *    Dynamic ipcc
 *
 * @author
 *    David Kraus
 */

#ifndef  LDNDC_KERNEL_IPCC_H_
#define  LDNDC_KERNEL_IPCC_H_

#include  "ld_kernel.h"
#include  "ld_shared.h"
#include  "ld_sinkinterface.h"

#include  "state/mbe_state.h"

namespace ldndc {
class LDNDC_API IPCC : public cbm::kernel_t
{
LDNDC_KERNEL_OBJECT(IPCC,ipcc)
public:
    IPCC();
    ~IPCC();

public:
    lerr_t  configure( cbm::RunLevelArgs *);
    lerr_t  register_ports( cbm::RunLevelArgs *);
    lerr_t  initialize( cbm::RunLevelArgs *);
    lerr_t  finalize( cbm::RunLevelArgs *);
    lerr_t  solve( cbm::RunLevelArgs *);
    lerr_t  unregister_ports( cbm::RunLevelArgs *);

protected:
    MoBiLE_State *  m_modelstate;
    substate_soilchemistry_t *  sc;
    
public:
    int  onFertilize( ldndc::EventAttributes const *);
    int  onManure( ldndc::EventAttributes const *);
    int  onTilling( ldndc::EventAttributes const *);
    int  onPlant( ldndc::EventAttributes const *);
    int  onHarvest( ldndc::EventAttributes const *);

private:

    /* cropping status */
    enum cropping_status_e
    {
        PRESEASON,
        ONFIELD
    };

    SubscribedEvent<LD_EventHandlerCall<IPCC> >  m_FertilizeEvent;
    SubscribedEvent<LD_EventHandlerCall<IPCC> >  m_ManureEvent;
    SubscribedEvent<LD_EventHandlerCall<IPCC> >  m_TillingEvent;
    SubscribedEvent<LD_EventHandlerCall<IPCC> >  m_PlantEvent;
    SubscribedEvent<LD_EventHandlerCall<IPCC> >  m_HarvestEvent;

    size_t seconds;

    CBM_Vector<double>  wfps;
    SubscribedVectorField<double>  SoilWaterSaturation;

    double  c_stubble;
    SubscribedField<double>  StubbleCarbon;

private:

    cropping_status_e cropping_status;

    double acc_n2o;
    double acc_ch4;

    double sf_aeration_ch4;
    double sf_preseason_ch4;
    double sf_straw_ch4;

    int seconds_at_planting;
    int nd_aeration_cycles;
    int nd_days_on_field;

    int nd_days_nonflooded;
    int nd_days_flooded;

    int nd_preseason_days_nonflooded;
    int nd_preseason_days_flooded;

    int nd_season_days_nonflooded;
    int nd_season_days_flooded;


    struct organic_amendment
    {
        size_t seconds_at_application;
        cbm::string_t type;
        double amount;
    };

    std::vector< organic_amendment > organic_amendments;
    std::vector< double > mineral_fertilizer;

    lerr_t ch4_emissions();
    lerr_t n2o_emissions();

    lerr_t
    step_init( cbm::RunLevelArgs * /* run time argument */);

    lerr_t
    water_status( cbm::RunLevelArgs * /* run time argument */);

    double
    topsoil_water_saturation();

    lerr_t
    reset();
    
private:
    /* hide these buggers for now */
    IPCC( IPCC const &);
    IPCC &  operator=( IPCC const &);

    lerr_t
    collect_datarecord(
                       ldndc_flt64_t * /* data buffer */);

    lerr_t  m_writerecord( ldndc_flt64_t *);
    ldndc::sink_handle_t  m_sink;
    ldndc::SinkInterface  m_sif;
};
} /* namespace ldndc */

#endif /* !LDNDC_KERNEL_IPCC_H_ */

