/*!
 * @author
 *    David Kraus
 */

#include  "ipcc/ld_ipcc.h"

#include  <logging/cbm_logging.h>

LDNDC_KERNEL_OBJECT_DEFN(IPCC,ipcc,"IPCC","LandscapeDNDC IPCC")


static ldndc_string_t const  IPCC_Ids[] =
{
    "gsC_ch4_emis[kgCha-1]",
    "gsN_n2o_emis[kgNha-1]",
    "nd_days_flooded_preseason[-]",
    "nd_days_nonflooded_preseason[-]",
    "nd_days_flooded_season[-]",
    "nd_days_nonflooded_season[-]",
    "nd_aeration_cycles[-]",
    "sf_aeration_ch4[-]",
    "sf_preseason_ch4[-]",
    "sf_straw_ch4[-]"
};

static ldndc_string_t const *  IPCC_Header = IPCC_Ids;
#define  IPCC_Datasize  (sizeof( IPCC_Ids) / sizeof( IPCC_Ids[0]))
static ldndc_output_size_t const  IPCC_Sizes[] =
{
    IPCC_Datasize,
    IPCC_Datasize /*total size*/
};

static ldndc_output_size_t const *  IPCC_EntitySizes = NULL;
#define  IPCC_Rank  ((ldndc_output_rank_t)(sizeof( IPCC_Sizes) / sizeof( IPCC_Sizes[0])) - 1)
static atomic_datatype_t const  IPCC_Types[] =
{
    LDNDC_FLOAT64
};


ldndc::IPCC::IPCC()
        : cbm::kernel_t(),
          m_modelstate( NULL),
          sc( NULL)
{
    seconds = 0;
    reset();
}


ldndc::IPCC::~IPCC()
{
    if ( this->m_modelstate)
    {
        this->m_modelstate->delete_instance();
        this->m_modelstate = NULL;
    }
}


int
ldndc::IPCC::onFertilize( ldndc::EventAttributes const * _fert)
{
    double const amount = _fert->get( "/amount", 0.0) * cbm::HA_IN_M2;
    mineral_fertilizer.push_back( amount);

    return 0;
}


int
ldndc::IPCC::onManure( ldndc::EventAttributes const * _manure)
{
    organic_amendment om;

    om.type = _manure->get( "/type", "-");
    om.amount = _manure->get( "/carbon", 0.0) / cbm::CCDM * cbm::HA_IN_M2;
    om.seconds_at_application = seconds;

    organic_amendments.push_back( om);

    return 0;
}


int
ldndc::IPCC::onTilling( ldndc::EventAttributes const * /* till */)
{
    /* amount of short stubble (approx. 10cm) not considered in ipcc approach */
    double const dw_stubble_base( 0.15);
    double const dw_stubble( cbm::bound_min( 0.0, c_stubble / cbm::CCDM - dw_stubble_base));
    if ( cbm::flt_greater_zero( dw_stubble))
    {
        organic_amendment om;

        om.type = "straw";
        om.amount = dw_stubble;
        om.seconds_at_application = seconds;
        organic_amendments.push_back( om);
    }

    return 0;
}


int
ldndc::IPCC::onPlant( ldndc::EventAttributes const * )
{
    seconds_at_planting = seconds;
    nd_days_on_field = 1;

    cropping_status = ONFIELD;

    return 0;
}


lerr_t
ldndc::IPCC::n2o_emissions()
{
    /* paddy defined by at least tow weeks of flooded conditions */
    bool const is_paddy( nd_season_days_flooded > 14);
    double const scaling( is_paddy ? 0.003 : 0.01);

    while ( !mineral_fertilizer.empty())
    {
        acc_n2o += mineral_fertilizer.back() * cbm::M2_IN_HA * scaling;
        mineral_fertilizer.pop_back();
    }

    return LDNDC_ERR_OK;
}


lerr_t
ldndc::IPCC::ch4_emissions()
{
    /* paddy defined by at least tow weeks of flooded conditions */
    bool const is_paddy( nd_season_days_flooded > 14);
    if ( is_paddy)
    {
        /* Base emissions */
        acc_ch4 = 1.3 * nd_days_on_field;


        /* Water management */

        /* Aeration cycles
         *  End-season drainage is not accounted for as aeration cycle:
         *  Multiple drainage: more than 2 aeration cycles
         *  Single drainage: 2 aeration cycles
         *  No drainage: 1 aeration cycle
         */
        if ( nd_aeration_cycles > 2)
        {
            sf_aeration_ch4 = 0.52;
        }
        else if ( nd_aeration_cycles > 1)
        {
            sf_aeration_ch4 = 0.60;
        }
        else
        {
            sf_aeration_ch4 = 1.0;
        }


        /* Preseason water status */
        if ( nd_preseason_days_nonflooded >= 180)
        {
            sf_preseason_ch4 = 0.68;
        }
        else if ( nd_preseason_days_flooded > 30)
        {
            sf_preseason_ch4 = 1.9;
        }
        else
        {
            sf_preseason_ch4 = 1.0;
        }

        /* Organic amendments */
        double ef( 1.0);
        while ( !organic_amendments.empty())
        {
            if ( organic_amendments.back().type == "straw")
            {
                size_t application_days_before_planting =
                        (seconds_at_planting - organic_amendments.back().seconds_at_application)
                        / cbm::SEC_IN_DAY;

                if ( application_days_before_planting > 30)
                {
                    ef += organic_amendments.back().amount * cbm::M2_IN_HA * cbm::T_IN_KG * 0.29;
                }
                else
                {
                    ef += organic_amendments.back().amount * cbm::M2_IN_HA * cbm::T_IN_KG;
                }
            }
            organic_amendments.pop_back();
        }

        sf_straw_ch4 = std::pow( ef, 0.59);
    }
    else
    {
        acc_ch4 = 0.0;
    }

    return LDNDC_ERR_OK;
}


lerr_t
ldndc::IPCC::reset()
{
    acc_n2o = 0.0;
    acc_ch4 = 0.0;

    sf_aeration_ch4 = 1.0;
    sf_preseason_ch4 = 1.0;
    sf_straw_ch4 = 1.0;

    seconds_at_planting = 0;
    nd_aeration_cycles = 0;
    nd_days_on_field = 0;

    nd_days_nonflooded = 0;
    nd_days_flooded = 0;

    nd_preseason_days_nonflooded = 0;
    nd_preseason_days_flooded = 0;

    nd_season_days_nonflooded = 0;
    nd_season_days_flooded = 0;

    cropping_status = PRESEASON;

    return LDNDC_ERR_OK;
}



int
ldndc::IPCC::onHarvest( ldndc::EventAttributes const * )
{
    ch4_emissions();

    n2o_emissions();

    ldndc_flt64_t  data_flt64_0[IPCC_Datasize];
    lerr_t  rc_dump = collect_datarecord( data_flt64_0);
    if ( rc_dump){ return  LDNDC_ERR_FAIL; }

    cbm::sclock_t const &  clk( *LD_RtCfg.clk);
    void *  data[] = { data_flt64_0};
    lerr_t  rc_write = this->m_sif.write_fixed_record(
                                                      &this->m_sink, data, &clk);
    if ( rc_write){ return  LDNDC_ERR_FAIL; }

    reset();

    return 0;
}


lerr_t
ldndc::IPCC::register_ports( cbm::RunLevelArgs * _args)
{
    m_FertilizeEvent.subscribe( "fertilize", _args->iokcomm);
    m_FertilizeEvent.handler( &IPCC::onFertilize, this);

    m_ManureEvent.subscribe( "manure", _args->iokcomm);
    m_ManureEvent.handler( &IPCC::onManure, this);

    m_TillingEvent.subscribe( "till", _args->iokcomm);
    m_TillingEvent.handler( &IPCC::onTilling, this);

    m_PlantEvent.subscribe( "plant", _args->iokcomm);
    m_PlantEvent.handler( &IPCC::onPlant, this);

    m_HarvestEvent.subscribe( "harvest", _args->iokcomm);
    m_HarvestEvent.handler( &IPCC::onHarvest, this);

    SoilWaterSaturation.subscribe( "VolumetricWaterSaturation", "m3/m3", sc->depth_sl.size(), _args->iokcomm);
    StubbleCarbon.subscribe( "StubbleCarbon", "kg/m2", _args->iokcomm);
    
    return LDNDC_ERR_OK;
}



lerr_t
ldndc::IPCC::unregister_ports( cbm::RunLevelArgs *  )
{
    m_FertilizeEvent.unsubscribe();
    m_ManureEvent.unsubscribe();
    m_TillingEvent.unsubscribe();
    m_PlantEvent.unsubscribe();
    m_HarvestEvent.unsubscribe();

    SoilWaterSaturation.unsubscribe();
    StubbleCarbon.unsubscribe();

    return  LDNDC_ERR_OK;
}



lerr_t
ldndc::IPCC::configure( cbm::RunLevelArgs *  _args)
{
    m_sif = ldndc::SinkInterface( _args->iokcomm);
    lerr_t  rc_setflags = this->m_sif.set_metaflags(
                                            _args->cfg, this->ID(), RM_CLIENTSOURCE|RM_CLIENTID|RM_DATETIME);
    if ( rc_setflags)
    { return LDNDC_ERR_FAIL; }

    this->m_modelstate = MoBiLE_State::new_instance();
    if ( !this->m_modelstate)
    { return LDNDC_ERR_NOMEM; }

    lerr_t rc_ini = this->m_modelstate->initialize( _args->iokcomm);
    if ( rc_ini)
    { return LDNDC_ERR_FAIL; }

    sc = m_modelstate->get_substate< substate_soilchemistry_t >();

    return LDNDC_ERR_OK;
}



lerr_t
ldndc::IPCC::initialize( cbm::RunLevelArgs *_args)
{
    this->m_sink = _args->iokcomm->sink_handle_acquire( "ipcc");
    if ( this->m_sink.status() == LDNDC_ERR_OK)
    {
        lerr_t  rc_layout = LDNDC_OUTPUT_DEFINE_SINK_LAYOUT(
                                                            this->m_sink,IPCC,this->m_sif.get_metaflags());

        RETURN_IF_NOT_OK(rc_layout);
    }
    else
    {
        KLOGERROR( "sink status bad  [sink=","ipcc","]");
        return  this->m_sink.status();
    }

    wfps.resize( sc->depth_sl.size());
    wfps = 0.0;

    return LDNDC_ERR_OK;
}


lerr_t
ldndc::IPCC::finalize( cbm::RunLevelArgs *_args)
{
    _args->iokcomm->sink_handle_release( &this->m_sink);
    return  LDNDC_ERR_OK;
}


lerr_t
ldndc::IPCC::collect_datarecord(
                                ldndc_flt64_t *  _buf)
{
    *_buf = acc_ch4 * sf_aeration_ch4 * sf_preseason_ch4 * sf_straw_ch4;
    ++_buf;
    *_buf = acc_n2o;
    ++_buf;

    *_buf = (double)nd_preseason_days_flooded;
    ++_buf;
    *_buf = (double)nd_preseason_days_nonflooded;
    ++_buf;
    *_buf = (double)nd_season_days_flooded;
    ++_buf;
    *_buf = (double)nd_season_days_nonflooded;
    ++_buf;
    *_buf = (double)nd_aeration_cycles;
    ++_buf;
    *_buf = (double)sf_aeration_ch4;
    ++_buf;
    *_buf = (double)sf_preseason_ch4;
    ++_buf;
    *_buf = (double)sf_straw_ch4;
    ++_buf;
    return LDNDC_ERR_OK;
}



lerr_t
ldndc::IPCC::step_init( cbm::RunLevelArgs * _args)
{
    seconds = _args->clk->seconds();

    SoilWaterSaturation.receive( wfps);
    c_stubble = StubbleCarbon.receive();

    return LDNDC_ERR_OK;
}



double
ldndc::IPCC::topsoil_water_saturation()
{
    double wfps_sum( wfps[0]);
    size_t sl_cnt( 1);

    for ( size_t  sl = 1;  sl < sc->depth_sl.size();  sl++)
    {
        wfps_sum += wfps[sl];
        sl_cnt += 1;
        if ( sc->depth_sl[sl] > 0.05)
        {
            break;
        }
    }

    return wfps_sum / (double)sl_cnt;
}



lerr_t
ldndc::IPCC::water_status( cbm::RunLevelArgs * _args)
{
    if ( (int)_args->clk->subday() == (int)_args->clk->time_resolution())
    {
        if ( cbm::flt_greater( topsoil_water_saturation(), 0.95))
        {
            nd_days_flooded += 1;
            nd_days_nonflooded = 0;
            if ( cropping_status == ONFIELD)
            {
                nd_season_days_flooded += 1;
            }
        }
        else
        {
            nd_days_flooded = 0;
            nd_days_nonflooded += 1;
            if ( cropping_status == ONFIELD)
            {
                nd_season_days_nonflooded += 1;
            }
        }

        /* Aeration cycles are only accounted for during cropping period
         * Aeration cycles need to be drained for at least three days
         */
        if ( (cropping_status == ONFIELD) &&
             (nd_days_nonflooded == 3))
        {
            nd_aeration_cycles += 1;
        }
    }

    if ( cropping_status == PRESEASON)
    {
        nd_preseason_days_flooded = nd_days_flooded;
        nd_preseason_days_nonflooded = nd_days_nonflooded;
    }

    return LDNDC_ERR_OK;
}



lerr_t
ldndc::IPCC::solve( cbm::RunLevelArgs * _args)
{
    if ( !this->m_sink.is_acquired())
    { return  LDNDC_ERR_OK; }

    step_init( _args);

    water_status( _args);

    if ( (int)_args->clk->subday() == (int)_args->clk->time_resolution())
    {
        if ( nd_days_on_field > 0)
        {
            nd_days_on_field++;
        }
    }

    return LDNDC_ERR_OK;
}

