/*!
 * @brief
 *    cross-platform solution to force linking of
 *    "unreferenced" objects
 *
 * @author
 *    steffen klatt (created on: feb 08, 2015)
 *
 * @note
 *    http://stackoverflow.com/questions/599035/force-visual-studio-to-link-all-symbols-in-a-lib-file
 */

#include  "ldndc-config.h.inc"
#ifdef  LDNDC_KERNELS
#  include  <kernel/kbase.h>
#  include  "ld_link.cpp.inc"
#endif

