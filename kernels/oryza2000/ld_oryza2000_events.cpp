/*!
 * @file
 * @author
 *    steffen klatt
 *    david kraus (created on: june 30, 2016)
 */

#include  "oryza2000/ld_oryza2000.h"


/*!
 * @brief
 *
 */
lerr_t
ldndc::LK_Oryza2000::OryzaTransplanting()
{
    /* Transplanting */
    if ( m_oryza->cropsta == 3)
    {
        m_oryza->zrt = m_oryza->zrttr;
    }

    return LDNDC_ERR_OK;
}



lerr_t
ldndc::LK_Oryza2000::OryzaPlant(
                                size_t _seedbedduration,
                                double _plant_number,
                                char const * _species_name)
{
    m_oryza->sbdur = _seedbedduration;
    m_oryza->plant_count = _plant_number;
    if ( !cbm::flt_greater_zero( m_oryza->plant_count))
    {
        m_oryza->plant_count = 150.0;
    }

    OryzaStateInitialization( this->m_oryza, _species_name);

    /* Switch to use ULTR and LLTR as given above or function built in ORYZA
     * for the reduction in relative transpiration:
     *SWIRTR = 'DATA'    ! Use data
     m_oryza->SWIRTR = 'FUNCTION' ! Use function
     */

    /*===================================================================*
     * Drought stress effect parameters for aerobic rice                 *
     * Values are for wheat, taken from SUCROS2 model                    *
     *===================================================================*
     * characteristic potential transpiration rate at a soil water
     * content halfway wilting point and field capacity (mm.d-1)*/
    //m_oryza->transc = 6.0;

    /* SOIL DATA FILE */
    m_oryza->zrtms = 1.0;

    /* IRRIGATION DATA FILE */
    m_oryza->nplsb = 1000.0;      //number of plants in seedbed
    m_oryza->nplds = 2000.0;      //number of plants direct-seeded in main field

    m_oryza->lape = 0.0001;       //initial leaf area per plant
    m_oryza->dvsi = 0.0;          //initial development stage
    m_oryza->wlvgi = 0.0;         //initial leaf weight
    m_oryza->wrti = 0.0;          //initial root weight
    m_oryza->wsoi = 0.0;          //initial weight storage organs
    m_oryza->wsti = 0.0;          //initial stem weight
    m_oryza->zrti = 0.0001;       //initial root depth (m)
    m_oryza->zrttr = 0.05;        //root depth at transplanting (m)

    /* no stress initialized */
    m_oryza->ldstrs = 1.0;
    m_oryza->lestrs = 1.0;
    m_oryza->lrstrs = 1.0;

    m_oryza->rnstrs = 1.0;
    m_oryza->pcew = 1.0;
    m_oryza->cpew = 1.0;

    m_oryza->nsllv = 1.0;

    m_oryza->tshckd = 0.0;


    m_oryza->dae = 0;
    m_oryza->tslv = 0.0;
    m_oryza->tnass = 0.0;
    m_oryza->wlvgit = 0.0;

    m_oryza->hulv = 0.0;
    m_oryza->hu = 0.0;

    m_oryza->dleaf = false;
    m_oryza->drout = false;
    m_oryza->grains = false;

    /* MISSING */
    m_oryza->coldmin = 5.0;

    /* ZERO */
    m_oryza->dvs = 0.0;
    m_oryza->keep = 0.0;
    
    m_oryza->ts = 0.0;
    m_oryza->ncold = 0.0;

    m_oryza->pwrr = 0.0;
    m_oryza->ngr = 0.0;
    m_oryza->ngrm2 = 0.0;
    m_oryza->nsp = 0.0;
    m_oryza->nspm2 = 0.0;

    m_oryza->cropsta = 1;

    this->m_ncrop.NcropInitialize( m_oryza);


    /* Day of emergence */
    m_oryza->nflv = m_oryza->nflvi;

    m_oryza->dvs  = m_oryza->dvsi;
    m_oryza->wlvg = m_oryza->wlvgi;
    m_oryza->wlvd = 0.0;
    m_oryza->wsts = m_oryza->wsti;
    m_oryza->wstr = 0.0;
    m_oryza->wst  = m_oryza->wsts + m_oryza->wstr;
    m_oryza->wso  = m_oryza->wsoi;
    m_oryza->wrt  = m_oryza->wrti;
    m_oryza->zrt  = m_oryza->zrti;

    if ( m_oryza->sbdur > 0)
    {
        m_oryza->lai= m_oryza->lape * m_oryza->nplsb;
    }
    else
    {
        m_oryza->lai= m_oryza->lape * m_oryza->nplds;
    }

    return  LDNDC_ERR_OK;
}



lerr_t
ldndc::LK_Oryza2000::OryzaHarvest()
{
    this->m_phenol.reset();
    
    m_oryza->dvs = 0.0;

    m_oryza->crglv = 0.0;
    m_oryza->crgso = 0.0;
    m_oryza->crgst = 0.0;
    m_oryza->crgstr = 0.0;
    m_oryza->crgrt = 0.0;

    m_oryza->dldr = 0.0;
    m_oryza->dldrt = 0.0;

//    m_oryza->fclv = 0.0;
//    m_oryza->fcrt = 0.0;
//    m_oryza->fcso = 0.0;
//    m_oryza->fcst = 0.0;
//    m_oryza->fcstr = 0.0;
    m_oryza->fstr = 0.0;

    m_oryza->zrt = 0.0;
    m_oryza->llv = 0.0;
    m_oryza->pwrr = 0.0;

    m_oryza->ngr = 0.0;
    m_oryza->ngrm2 = 0.0;
    m_oryza->nsp = 0.0;
    m_oryza->nspm2 = 0.0;

    m_oryza->drout = false;

    m_oryza->frpar = 0.0;
    m_oryza->scp = 0.0;
    m_oryza->co2ref = 0.0;
    m_oryza->co2 = 0.0;

    m_oryza->tbd = 0.0;
    m_oryza->tblv = 0.0;
    m_oryza->tmd = 0.0;
    m_oryza->tod = 0.0;

    m_oryza->mainlv = 0.0;
    m_oryza->mainrt = 0.0;
    m_oryza->mainso = 0.0;
    m_oryza->mainst = 0.0;

    m_oryza->tclstr = 0.0;
    m_oryza->tref = 0.0;
    m_oryza->wgrmx = 0.0;
    m_oryza->zrtmcw = 0.0;
    m_oryza->zrtmcd = 0.0;
    m_oryza->gzrt = 0.0;

    m_oryza->nflvi = 0.0;
    m_oryza->nflv = 0.0;

    m_oryza->rmcr_rt = 0.0;
    m_oryza->rmcr_lv = 0.0;
    m_oryza->rmcr_so = 0.0;
    m_oryza->rmcr_st = 0.0;

    m_oryza->rgcr_rt = 0.0;
    m_oryza->rgcr = 0.0;

    m_oryza->shckd = 0.0;
    m_oryza->spgf = 0.0;

    m_oryza->hu = 0.0;
    m_oryza->hulv = 0.0;

    m_oryza->ts_dtga = 0.0;
    m_oryza->d_dtga = 0.0;
    m_oryza->c_root_turnover = 0.0;
    m_oryza->c_root_exsudation = 0.0;
    m_oryza->dtr = 0.0;
    m_oryza->tnass = 0.0;
    m_oryza->tslv = 0.0;
    m_oryza->dae = 0;
    m_oryza->dvr = 0.0;
    m_oryza->dvrj = 0.0;
    m_oryza->dvri = 0.0;
    m_oryza->dvrp = 0.0;
    m_oryza->dvrr = 0.0;

    m_oryza->pltr = 1.0;
    
    m_oryza->lai = 0.0;
    m_oryza->dldr = 0.0;

    m_oryza->pcew = 0.0;
    m_oryza->gcr = 0.0;

    m_oryza->tmax = 0.0;
    m_oryza->tmin = 0.0;
    m_oryza->tav = 0.0;

    m_oryza->nsllv = 0.0;
    m_oryza->rnstrs = 1.0;
    m_oryza->sbdur = 0;

    m_oryza->plant_count = 0.0;
    m_oryza->nplsb = 0.0;
    m_oryza->nplds = 0.0;

    m_oryza->scp = 0.0;

    m_oryza->lape = 0.0;
    m_oryza->dvsi = 0.0;
    m_oryza->wlvgi = 0.0;
    m_oryza->wrti = 0.0;
    m_oryza->wsoi = 0.0;
    m_oryza->wsti = 0.0;
    m_oryza->zrti = 0.0;
    m_oryza->zrttr = 0.0;

    m_oryza->tdrw = 0.0;
    m_oryza->wag = 0.0;
    m_oryza->wagt = 0.0;
    m_oryza->wlv = 0.0;
    m_oryza->wrr = 0.0;
    m_oryza->wrt = 0.0;
    m_oryza->wso = 0.0;
    m_oryza->wst = 0.0;

    m_oryza->wlvgit = 0.0;
    m_oryza->rwlvg = 0.0;

    m_oryza->zrtm = 0.0;
    m_oryza->zrtmcd = 0.0;
    m_oryza->zrtms = 0.0;
    
    m_oryza->coldtt = 0.0;
    m_oryza->tfert  = 0.0;
    m_oryza->ntfert = 0.0;

    m_oryza->cropsta = 0;

    maturity_status.send( m_oryza->dvs / 2.0);
    
    return  LDNDC_ERR_OK;
}

