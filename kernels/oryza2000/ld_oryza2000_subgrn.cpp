/*!
 * @brief
 *
 * @author:
 *      david kraus (created on: october 5, 2016),
 *      steffen klatt
 */


#include  "oryza2000/ld_oryza2000_state.h"
#include  "oryza2000/ld_oryza2000_subgrn.h"

ldndc::Oryza2000Subgrn::~Oryza2000Subgrn()
{
}

ldndc::Oryza2000Subgrn::Oryza2000Subgrn()
{
    coldtt = ( 0.0);
    tfert = ( 0.0);
    ntfert = ( 0.0);
}

void
ldndc::Oryza2000Subgrn::run( ldndc::Oryza2000State *  _os)
{
    /*
     *Temperature increase due to leaf rolling:
     *1.6 degree per unit leaf rolling (Turner et al., 1986; p 269)
     */
    double const tincr( 5.0 * (1.0 - _os->lrstrs) * 1.6);


    /*Spikelet formation between panicle initiation and flowering */
    double const dvspi( 0.65);
    double const dvsf( 1.0);

    if((_os->dvs >= dvspi) && (_os->dvs <= dvsf))
    {
        _os->gnsp = _os->gcr * _os->spgf;
    }
    else
    {
        _os->gnsp = 0.0;
    }


    /*Grain formation from spikelets (gngr) and gngr reduction factors */
    if((_os->dvs >= 0.75) && (_os->dvs <= 1.2))
    {
        double const ctt = std::max( 0.0, 22.0 - (_os->tav - tincr));
        this->coldtt = this->coldtt + ctt;
    }

    if((_os->dvs >= 0.96) && (_os->dvs <= 1.2))
    {
        this->tfert  = this->tfert + (_os->tmax + tincr);
        this->ntfert = this->ntfert + 1.0;
    }


    /* Apply gngr reduction factors when dvs is 1.2 or more */
    if( _os->dvs >= 1.2) // ! grains
    {
        _os->grains = true;

        double sf1( 1.0 - (4.6 + 0.054 * pow(this->coldtt, 1.56)) / 100.0);
        sf1 = std::min(1.0, std::max(0.0, sf1));

        this->tfert  = this->tfert / (this->ntfert);

        double sf2( 1.0 / (1.0 + std::exp(0.853 * (this->tfert - 36.6))));
        sf2 = std::min(1.0, std::max(0.0, sf2));

        double spfert( std::min(sf1, sf2));
        _os->gngr   = _os->nsp * spfert;
    }
    else
    {
        _os->gngr =0.0;
    }
}

