/*!
 * @file
 *
 * @author:
 *      david kraus (created on: october 5, 2016),
 *      steffen klatt
 */

#ifndef  ORYZA2000_NCROP_H_
#define  ORYZA2000_NCROP_H_

#include  <containers/lvector.h>

namespace ldndc {

class  Oryza2000State;
class  Oryza2000Ncrop
{

public:

    Oryza2000Ncrop();

    ~Oryza2000Ncrop();


    lerr_t NcropReset();

    lerr_t NcropInitialize( Oryza2000State *);

    lerr_t NcropRateCalculation( bool , Oryza2000State *);

    lerr_t NcropIntegrate( bool , Oryza2000State *);

public:

    double anrt;         //Amount of N in roots
    double anst;         //Amount of N in stems
    double anso;         //Amount of N in storage organ
    double anlv;         //Amount of N in leaves
    double anld;         //Amount of N in dead leaves

    //Amount of N in crop (live and dead material)
    inline double ancr() const { return this->anso + this->anlv + this->anrt + this->anld + this->anst; };

    double anlva;        //Amount of N in leaves till flowering
    double ansta;        //Amount of N in stems till flowering
    
    //Amount of N in crop till flowering
    inline double ancrf() const { return this->ansta + this->anlva; };

    double atnlv;        //Total available N for translocation from leaves
    double atnst;        //Total available N for translocation from stems
    double atnrt;        //Total available N for translocation from roots
    inline double atn() const { return this->atnlv + this->atnst + this->atnrt; };          //Total available N for translocation from leaves, stems, and roots

    double fnrt;         //Fraction of N in roots (kg N kg−1 DM)
    double fnlv;         //Fraction of N in leaves (kg N kg−1 DM)
    double fnst;         //Fraction of N in stems (kg N kg−1 DM)
    double fnso;         //Fraction of N in storage organs (kg N kg−1 DM)

    double ntso;         //Actual N translocation rate to storage organs from leaves, stems and roots (kg N ha−1 d−1)
    double nacr;         //Actual nitrogen uptake rate by crop (kg N ha−1 d−1)

    double nrt;
    double nlv;         //Daily net flow rate of N to the leaves
    double nst;
    double nso;
    double nldlv;       //N loss rate because of death of leaves

    double nlvan;
    double nstan;

    double nart;
    double nalv;        //Actual nitrogen uptake rate by leaves
    double nast;
    double naso;

    double ntrt;
    double nmaxl;        //maximum nitrogen content of dry matter [kg N kg-1 DM-1]

    double ntlv;        //Actual N translocation rate to storage organs from leaves
    double ntst;
    double nflvp;        //maximum (potential) N fraction in leaves on leaf area basis [g N m-2 leaf-1]
    

    double nupp;

    double nminl;
    double ndeml;
    double ndemc;

    double soiln;
};

} /* namespace ldndc */


#endif /* !ORYZA2000_NCROP_H_ */

