/*!
 * @file
 *
 * @author:
 *      david kraus (created on: june 25, 2016),
 *      steffen klatt
 */

#ifndef  ORYZA2000_STATE_H_
#define  ORYZA2000_STATE_H_

#include  "ld_kernel.h"
#include  "oryza2000/ld_oryza2000_table.h"
#include  "physiology/photofarquhar/berryball.h"

#include  <containers/cbm_vector.h>
#include  <input/soillayers/soillayers.h>


namespace ldndc {

class LDNDC_API Oryza2000State
{
public:
    
    Oryza2000State(
                   cbm::io_kcomm_t *,
                   double,
                   size_t);

    ~Oryza2000State();

    lerr_t  initialize( cbm::io_kcomm_t *);

#ifdef  _HAVE_SERIALIZE
public:
    int  create_checkpoint( cbm::io_kcomm_t *)
        { return -1; }
    int  restore_checkpoint( cbm::io_kcomm_t *, ldate_t const *)
        { return -1; }
#endif
#ifdef  _LDNDC_HAVE_ONLINECONTROL
public:
    int  process_request( lreply_t * /*reply*/,
            lrequest_t const * /*request*/)
        { return -1; }
#endif

private:


    
    
    void  reset();


private:
    /* hide or implement */
    Oryza2000State( Oryza2000State const &);
    Oryza2000State &  operator=( Oryza2000State const &);


public:

    /*!
     * @brief
     *      iterates over complete soilchemistry state and checks
     *      for negative values
     */
    lerr_t
    Oryza_check_for_negative_value(
                                   size_t /* id */,
                                   char const * /*name*/);

#define SWISLA_SIZE 40
    char *  swisla;   //Switch to select method of imposed SLA calculation

    /* state variables */

    double wlvg;     //Dry weight of green leaves (ka ha-1)
    double wlvd;     //Dry weight of dead leaves (ka ha-1)
    double wsts;     //Dry weight of structural stems (ka ha-1)
    double wstr;     //Dry weight of stem reserves (ka ha-1)

    double wso;      //Dry weight of storage organs (ka ha-1)
    double wrt;      //Dry weight of roots (kg ha-1)
    double wst;      //Dry weight of stems (kg ha-1)
    double wlv;      //Dry weight of leaves (kg ha-1)

    double wag;      //Total aboveground green dry matter (kg DM ha-1)
    double wagt;     //Total aboveground dry matter (kg DM ha-1)
    double tdrw;     //Total aboveground and belowground dry biomass (kg ha-1)
    double wrr;      //Dry weight of rough rice (final yield) (kg ha-1)

    double pwrr;     //Potential weight of rough rice (kg ha-1)

    double ngr;      //Number of grains (no. ha-1)
    double ngrm2;    //Number of grains (no. m-2)
    double nsp;      //Number of spikelets (no. ha-1)
    double nspm2;    //Number of spikelets (no. m-2)

    size_t dae;      //Days after emergence (d)
    size_t sbdur;    //Seedbed duration (d)
    
    double tslv;     //Temperature sum for leaf area development (oC)
    double tnass;    //Total net CO2 assimilation (kg CO2 ha−1)

    double lai;      //Leaf area index (m2 m-2)

    double tshckd;   //Transplanting shock for phenol. development (oCd)
    double tshckl;   //Transplanting shock for leaf area development (oCd)

    double hu;       //Daily heat units effective for phenological development (oC d-1)
    double hulv;     //Daily heat units effective for leaf area development (oC d-1)

    double ncold;        //Number of cold days (-)
    double zrt;          //Root length or rooting deth (m)

    double dldr;         //Death rate of leaves caused by drought (kg DM ha−1 d−1)
    double dldrt;        //Total death rate of leaves caused by drought (kg DM ha−1 d−1)

    double keep;         //Variable to temporarily store value of LDSTRS (-)

    bool dleaf;         //Control variable for start of leaf senescence by drought (kg DM ha−1 d−1)
    bool drout;         //Control variable indicating drought/no drought (-)
    bool grains;        //Logical parameter indicating whether grains are formed (-)

    /* state variables read from input on demand */

    double lape;         //Leaf area per plant at emergence (m-2 pl-1)
    double dvsi;         //Initial value of development stage of crop ()
    double wlvgi;        //Initial dry weight of leaves (kg ha-1)
    double wlvgit;       //Temporary storage variable of WLVG
    double wrti;         //Initial dry weight of roots (kg ha-1)
    double wsoi;         //Initial dry weight of storage organs (kg ha-1)
    double wsti;         //Initial dry weight of stems (kg ha-1)
    double zrti;         //Initial root length/depth at emergence (m)
    double zrttr;        //Root length/depth at transplanting (m)

    /* crop parameters */

    int idoy;           //Time of simulation (d)
    double time;         //Time of simulation (d)
    double tblv;         //Base temperature for juvenile leaf area growth (oC)

    double dvs;          //development stage of the crop (-)
    double dvr;          //development rate of the crop (d-1)
    double dvrj;         //development rate juvenile ((oCd)-1)
    double dvri;         //development rate, photoperiod-sensitive phase ((oCd)-1)
    double dvrp;         //development rate, PI phase ((oCd)-1)
    double dvrr;         //development rate, reproductive phase ((oCd)-1)


    double mopp;         //maximum optimum photoperiod (h)
    double ppse;         //photoperiod sensitivity (h-1)

    double shckd;        //delay parameter in phenology ((oCd)(oCd)-1)

    int cropsta;        //crop stage (-): 0=before sowing; 1=sowing; 2=seedbed; 3=transplanting; 4=main growth period

    OryzaTable flvtb;
    OryzaTable slatb;

    double ts;           //temperature sum (oCd)

    double ldstrs;       //leaf death stress factor (-)    
    double lestrs;       //leaf expansion stress factor (-)
    double lrstrs;       //leaf rolling stress factor (-)

    double nh;           //number of hills
    double nplsb;        //number of plants in seedbed
    double nplds;        //number of plants direct-seeded in main field

    /* climate */
    double tmax;         //maximum daily temperature
    double tmin;         //minimum daily temperature
    double tav;          //average daily temperature

    double tbd;         //Base temperature for development [oC]
    double tod;         //Optimum temperature for development [oC]
    double tmd;         //Mean daily temperature [oC]

    double coldmin;      //lower temperature threshold for growth

    double nflvi;        //Initial nitrogen fraction in the leaves (g N m-2 leaf)
    double nflv;         //nitrogen fraction in the leaves (g N m-2 leaf)
    double knf;          //extinction coefficient of nitrogen profile in canopy
    double redft;

    OryzaTable nminsottb;   //Table of minimum N fraction in storage organs as function of N in crop till flowering

    OryzaTable nmaxlttb;

    OryzaTable nminlttb;

    OryzaTable nsllvttb;

    double frpar;        //fraction of total shortwave irradiation that is photosynthetically active (-)
    double cslv;         //scattering coefficient of leaves for PAR

    double ecpdf;
    double gai;          //green area index (ha leaf-1 ha-1 ground)

    double co2;
    double co2ref;

    OryzaTable efftb;
    OryzaTable ssgatb;
    OryzaTable kdftb;
    OryzaTable redftt;
    OryzaTable knftb;   //KNF (extinction coefficient of N profile in canopy) as function of development stage (DVS)


    OryzaTable nflvtb;
    OryzaTable fshtb;
    OryzaTable drlvt;

    double scp;          //scattering coefficient of leaves for photosynthetically active radiation (-)
    double dpar;

    double dtr;          //daily total global radiation (J m-2 d-1)

    double alai;
    double sai;

    double ts_dtga;
    double d_dtga;      //daily total gross CO2 assimilation of crop (kg CO2 ha-1 d-1)
    double c_root_turnover;
    double c_root_exsudation;

    double rnstrs;       //reduction factor on relative leaf growth rate caused by N stress (-)
    double pcew;         //effect of drought stress on daily gpp; reduction in potential transpiration rate
    double cpew;

    double frt;          //fraction of total dry matter allocated to roots
    double fsh;          //fraction of total dry matter allocated to shoots
    OryzaTable fhstb;


    double flv;          //fraction of shoot dry matter allocated to leaves
    double fst;          //fration of shoot dry matter allocated to stems
    OryzaTable fsttb;
    double fso;          //fration of shoot dry matter allocated to storage organs
    OryzaTable fsotb;

    double ggr;

    double llv;          //loss rate of leaf weight (kg DM ha-1 d-1)
    double nsllv;        //nitrogen stress factor that accelerates leaf death (-)

    double lstr;         //loss rate of stem reserves (kg DM ha−1 d−1)
    double tclstr;       //time coefficient for loss of stem reserves (d-1)

    double q10;
    double tref;
    double mainlv;       //Maintenance respiration coefficient of leaves (kg CH2O kg−1 DM d−1)
    double mainst;       //Maintenance respiration coefficient of stems (kg CH2O kg−1 DM d−1)
    double mainso;       //Maintenance respiration coefficient of stems (kg CH2O kg−1 DM d−1)
    double mainrt;       //Maintenance respiration coefficient of stems (kg CH2O kg−1 DM d−1)
    double teff;
    double mndvs;

    double fstr;

    double crglv;        //Carbohydrate requirement for leaf dry matter production (kg CH2O kg−1 DM)
    double crgst;        //Carbohydrate requirement for stem dry matter production (kg CH2O kg−1 DM)
    double crgstr;       //Carbohydrate requirement for stem reserves production (kg CH2O kg−1 DM)
    double crgso;        //Carbohydrate requirement for storage organ dry matter production (kg CH2O kg−1 DM)
    double crgrt;        //carbohydrate requirement for root dry matter production (kg CH2O kg−1 DM)

    double lrstr;        //fraction of allocated stem reserves that is available for growth (-)
    double fcstr;        //mass fraction of carbon in stem reserves (kg C kg−1 DM)
    double ngcr;         //net growth rate of crop, including translocation (kg ha−1 d−1)

    double plant_count;  //number of plants (-)

    double grt;          //growth rate of roots (kg ha-1 d-1)
    double glv;          //growth rate of leaves (kg ha-1 d-1)

    double rwlvg;        //growth rate of green leaves (kg ha-1 d-1)
    double gst;
    double gstr;
    double rwstr;
    double gso;         //growth rate of storage organs (kg DM ha-1 d-1)

    double sf2;
    double sf1;
    double spgf;         //Spikelet growth factor (no. kg-1)


    double gnsp;         //Rate of increase in spikelet number (no. ha-1 d-1)
    double gngr;         //Rate of increase in grain number (no. ha-1 d-1)

    double sla;          //Specific leaf area (ha leaf kg−1 leaf)
    double asla;
    double bsla;
    double csla;
    double dsla;

    double slamax;

    double rgrlmx;
    double rgrlmn;

    double shckl;

    double glai;     //growth rate of leaf area index (ha ha-1 d-1)
    double rgrl;    //Relative growth rate for leaf development

    double coldtt;

    double co2rt;    //CO2 production factor for growth of roots (kg CO2 kg-1 DM-1)
    double co2so;    //CO2 production factor for growth of storage organs (kg CO2 kg-1 DM-1)
    double co2st;    //CO2 production factor for growth of stems (kg CO2 kg-1 DM-1)
    double co2str;   //CO2 production factor for growth of stem reserves (kg CO2 kg-1 DM-1)

    double co2lv;

    double coldead;

    double ctrans;       //carbon losses at transplanting (kg C ha-1)
    double fcso;
    double fclv;         //mass fraction of carbon in leaves (kg C kg-1 DM-1)
    double fcst;         //mass fraction of carbon in stems (kg C kg-1 DM-1)
    double fcrt;         //mass fraction of carbon in roots (kg C kg-1 DM-1)
    double gzrt;
    double gcr;          //gross growth rate of crop (kg DM ha-1 d-1)
    double ntfert;

    double rgcr_rt;      //growth respiration rate of roots (kg CO2 ha-1 d-1)
    double rgcr;         //growth respiration rate of crop (kg CO2 ha-1 d-1)
    double rmcr_rt;      //maintenance respiration rate of roots (kg CH2O ha-1 d-1)
    double rmcr_lv;      //maintenance respiration rate of leaves (kg CH2O ha-1 d-1)
    double rmcr_so;      //maintenance respiration rate of storage organs (kg CH2O ha-1 d-1)
    double rmcr_st;      //maintenance respiration rate of stem (kg CH2O ha-1 d-1)

    //total maintenance respiration rate of crop (kg CH2O ha-1 d-1)
    double rmcr() const { return rmcr_lv + rmcr_rt + rmcr_so + rmcr_st; };

    double ulls;         //Upper limit leaf rolling (kPa)
    double llls;         //Lower limit leaf rolling (kPa)
    double uldl;         //Upper limit death of leaves (kPa)
    double lldl;         //Lower limit death of leaves (kPa)
    double ulle;         //Upper limit leaf expansion (kPa)
    double llle;         //Lower limit leaf expansion (kPa)
    double ulrt;         //Upper limit relative transpiration reduction (kPa)
    double llrt;         //Lower limit relative transpiration reduction (kPa)

    double nmaxup;       //Maximum daily N uptake (kg N ha-1 d-1)
    double rfnlv;        //Residual N fraction of leaves (kg N kg-1 leaves)
    double fntrt;        //Fraction N translocation from roots, as (additonal) fraction of total N translocation from stems and leaves (-)
    double rfnst;        //Residual N fraction of stems (kg N kg-1 stems)
    double tcntrf;       //Time coefficient for N translocation to grains (d)

    double fnlvi;        //Initial leaf N fraction (on weight basis: kg N kg-1 leaf)

    double nmaxso;       //Maximum N concentration in storage organs (kg N kg-1)

    double pltr;
    double rtnass;       //Net rate of total CO2 assimilation by crop (kg CO2 ha-1 d-1)
    
    double tfert;
    double tklt;         //Thickness of combined soil layers (m)
    double wgrmx;        //Maximum individual grain weight (kg grain-1)
    double zrtmcw;
    double zrtm;         //Maximum root length/depth (m)
    double zrtmcd;       //Maximum root length/depth as crop characteristic under drought (m)
    double zrtms;        //Maximum depth that roots can penetrate into soil (m)

    soillayers::input_class_soillayers_t const &  sl_;

    lvector_t< double > tkl_sl;
    lvector_t< double > mskpa_sl;
    lvector_t< double > trr_sl;
    lvector_t< double > trwl_sl;
    lvector_t< double > wc_sl;
    lvector_t< double > poro_sl;
    lvector_t< double > wcwp_sl;

    BerryBall m_photo;

    double surface_water;
    double potentialevapotranspiration;
    double potentialtranspiration;
    double potentialevaporation;
    
    double *vcAct25_fl;
    double *jAct25_fl;
    double *rdAct25_fl;
    double *sla_fl;

    double *vpd_fl;
    double *rh_fl;
    double *temp_fl;
    double *parsun_fl;
    double *parshd_fl;
    double *tFol_fl;
    double *co2_concentration_fl;
    double *sunlitfoliagefraction_fl;
    double *carbonuptake_fl;

    size_t nb_foliagelayers;
    double airpressure;

    double f_h2o;
    double f_fac;
    double mFol;
    double height_max;

    double CSR_REF;
    double CWP_REF;
    double PSI_REF;
    double PSI_EXP;
    double RPMIN;

    double KC25;
    double AEKC;
    double AEVO;
    double KO25;
    double AEKO;
    double AEVC;
    double QVOVC;
    double GSMIN;
    double GSMAX;
    double SLOPE_GSA;
    double AERD;
    double SDJ;
    double HDJ;
    double THETA;
    double AEJM;
    double QJVC;
    bool C4_TYPE;

    double TOFRTBAS;
    double DOC_RESP_RATIO;
    
public:
    double inline fFol_fl( size_t ){ return 1.0 / nb_foliagelayers; };
    double inline lai_fl( size_t _fl){ return this->lai * this->fFol_fl(_fl); };
};

} /* namespace ldndc */

#endif /* !ORYZA2000_STATE_H_ */

