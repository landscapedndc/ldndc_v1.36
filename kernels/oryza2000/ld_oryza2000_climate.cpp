/*!
 * @file
 * @author
 *    steffen klatt
 *    david kraus (created on: june 30, 2016)
 */


#include  "oryza2000/ld_oryza2000.h"

#include  <input/climate/climate.h>
#include  <scientific/meteo/ld_meteo.h>
#include  <input/setup/setup.h>


/*!
 * @brief
 *
 */
lerr_t
ldndc::LK_Oryza2000::OryzaDrout()
{
    /*
     * Set drout when leaf expansion is reduced in the
     * vegetative growth phase (=> extra root growth)
     */
    if (   m_oryza->dvs    < 1.0
        && m_oryza->lestrs < 1.0)
    {
        m_oryza->drout = true;
    }
    else
    {
        m_oryza->drout = false;
    }

    return LDNDC_ERR_OK;
}



/*!
 * @brief
 *      This subroutine calculates reference evapotranspiration
 *      in a manner similar to Penman (1948). To obtain crop evapo-
 *      transpiration, multiplication with a Penman crop factor
 *      should be done. Calculations can be carried out for three
 *      types of surfaces: water, wet soil, and short grass
 *      (ISURF=1,2,3 resp.). When the input variable TMDI is set to
 *      zero, a single calculation is done and an estimate is
 *      provided of temperature difference between the environment
 *      and the surface (DT). If the absolute value of DT is large
 *      an iterative Penman can be carried out which continues until
 *      the new surface temperature differs by no more than TMDI
 *      from the old surface temperature. Two types of long-wave
 *      radiation calculations are available Swinbank and Brunt.
 *      The switch between the two is made by choosing the right
 *      values for ANGA and ANGB. If ANGA and ANGB are zero,
 *      Swinbank is used, if both are positive, Brunt is used and
 *      the ANGA and ANGB values are in the calculation of the
 *      cloud cover.
 *
 * @author
 *      Daniel van Kraalingen (original version)
 *      David Kraus (implementation into LandscapeDNDC)
 */
lerr_t
ldndc::LK_Oryza2000::OryzaPenman(
                                 cbm::RunLevelArgs *  _r_level,
                                 int _isurf,
                                 int _idoy,
                                 double _lat,
                                 double _rf,
                                 double _anga,
                                 double _angb,
                                 double _tmdi,
                                 double _rdd,
                                 double _tmda,
                                 double _wn,
                                 double _vp,
                                 double _dt,
                                 double &_etd,
                                 double &_etrd,
                                 double &_etae)
{
    cbm::sclock_t const * clk = _r_level->clk;

    double const sigma = 5.668e-8;
    double const lhvap = 2454.e3;
    double const psch = 0.067;
    double const rhocp = 1240.0;
    double const rbgl = 8.31436;


    /* calculation for longwave radiation */
    size_t ilw( 0);
    if (   cbm::flt_equal_zero( _anga)
        && cbm::flt_equal_zero( _angb))
    {
        ilw = 1;
    }
    else if (   cbm::flt_greater_zero( _anga)
             && cbm::flt_greater_zero( _angb)
             && cbm::flt_greater( _anga + _angb, 0.5)
             && cbm::flt_greater(0.9, _anga + _angb))
    {
        ilw = 2;
    }

    double vps( 0.0);
    double vpsl( 0.0);
    OryzaSaturatedVapourPressure( _tmda, vps, vpsl);

    double const hum( _vp / vps);
    double const vpd( cbm::flt_greater( hum, 1.0) ? 0.0 : vps - _vp);

    double solcon( cbm::AVG_SOLAR_CONSTANT * ldndc::meteo::eccentricity( _idoy, clk->days_in_year()));
    double angot( ldndc::meteo::daily_solar_radiation(_lat, _idoy, 365) * cbm::JDCM_IN_WM * cbm::CM2_IN_M2);

    double datmtr = cbm::bound( 0.0, _rdd / angot, 1.0);

    double rdloi = sigma * std::pow(_tmda + 273.16, 4.0);

    double rdlo  = 86400.0 * rdloi;

    double rdlii( 0.0);
    double rdli( 0.0);
    if ( ilw == 1)
    {
        /* swinbank formula for net longwave radiation */
        rdlii = datmtr * (5.31e-13 * std::pow(_tmda + 273.16, 6.0) - rdloi) / 0.7 + rdloi;
        rdli  = 86400.0 * rdlii;
    }
    else if (ilw == 2)
    {
        /* brunt formula for net longwave radiation */
        double const clear = cbm::bound(0.0, (datmtr - _anga) / _angb, 1.0);
        rdlii = sigma * std::pow(_tmda + 273.16, 4.0) * (1.0 - (0.53 - 0.212 * sqrt( _vp)) * (0.2 + 0.8 * clear));
        rdli  = 86400.0 * rdlii;
    }

    double rdn = (1.0 - _rf) * _rdd + rdli - rdlo;

    /*
     * wind functions and isothermal evaporation
     * 2.63 is conversion from mm hg to kpa
     */
    double fu2( 0.0);
    if ( (_isurf == 1) || (_isurf == 2))
    {
        /* open water and soils */
        fu2 = 2.63 * (0.5 + 0.54 * _wn);
    }
    else if (_isurf == 3)
    {
        /* short grass crops */
        fu2 = 2.63 * (1.0 + 0.54 * _wn);
    }

    double ea = vpd * fu2;

    //actual water loss (separated in radiation term and
    //aerodynamic term) and resistance to transfer of vapour (s/m)
    //and estimated temperature difference

    _etrd = (rdn * (vpsl / (vpsl + psch))) / lhvap;
    _etae = (psch * ea) / (vpsl + psch);
    _etd  = _etrd + _etae;
    double re   = 86400.0 * 1000.0 * 0.018016 / (fu2 * rbgl * (_tmda + 273.16));
    _dt   = re * ((rdn - lhvap * _etd) / 86400.0) / rhocp;

    /* iteration on surface temperature if required with do-while loop */

    if ( _tmdi > 0.0)
    {
        double dtn    = 0.0;
        int inloop = 0;
        bool equil  = false;
        while ( (inloop == 0) || !equil)
        {
            _dt = (_dt + dtn) / 2.0;

            /* net radiation and slope of saturated vapour pressure */
            rdloi = sigma * std::pow( _tmda + _dt + 273.16, 4.0);
            rdlo  = 86400.0 * rdloi;
            rdn   = (1.0 - _rf) * _rdd + rdli - rdlo;
            double vps2( 0.0);

            OryzaSaturatedVapourPressure( (_tmda + _dt), vps2, solcon);
            vpsl = (vps2 - vps) / _dt;

            /*
             * actual water loss, resistance to vapour transfer and
             * estimated temperature difference
             */

            _etrd = (rdn * (vpsl / (vpsl + psch))) / lhvap;
            _etae = (psch * ea) / (vpsl + psch);
            _etd  = _etrd + _etae;
            re   = 86400.0 * 1000.0 * 0.018016 / (fu2 * rbgl * (_tmda + 0.5 * _dt + 273.16));
            dtn  = re * ((rdn - lhvap * _etd) / 86400.0) / rhocp;

            /* check on equilibrium and maximum number of iterations */

            equil  = (std::abs( dtn - _dt) < _tmdi);
            inloop = inloop + 1;

            _dt = dtn;
        }
    }

    return  LDNDC_ERR_OK;
}



/*!
 * @brief
 *      This subroutine calculates saturated vapour pressure and
 *      slope of saturated vapour pressure. Parameters of the
 *      formula were fitted on the Goff-Gratch formula used in the
 *      Smithsonian Handbook of Meteorological Tables. The
 *      saturated vapour following the Goff-Gratch formula is also
 *      available as a subroutine. (Note that 1kPa = 10 mbar)
 * @author
 *      Daniel van Kraalingen (original version)
 *      David Kraus (implementation into LandscapeDNDC)
 *
 * @param
 *      temperature (°C)
 * @param
 *      saturated vapour pressure (kPa)
 * @param
 *      slope of saturated vapour pressure at given temperature (kPa °C-1)
 */
lerr_t
ldndc::LK_Oryza2000::OryzaSaturatedVapourPressure(
                                                  double _temperature,
                                                  double &_saturated_vapour_pressure,
                                                  double &_slope)
{
    _saturated_vapour_pressure  = 0.1 * 6.10588 * std::exp( 17.32491 * _temperature / (_temperature + 238.102));
    _slope = 238.102 * 17.32491 * _saturated_vapour_pressure / std::pow( _temperature + 238.102, 2.0);

    return  LDNDC_ERR_OK;
}



/*!
 * @brief
 *
 */
lerr_t
ldndc::LK_Oryza2000::OryzaHydrology( cbm::RunLevelArgs *_r_level)
{
    if ( m_oryza->cropsta < 4)
    {
        /* set no stress */
        m_oryza->lrstrs = 1.0;
        m_oryza->ldstrs = 1.0;
        m_oryza->lestrs = 1.0;
        m_oryza->cpew = 1.0;
        m_oryza->pcew = 1.0;
    }
    else
    {
        cbm::sclock_t const * clk = _r_level->clk;

        //Latitude
        double lat( _r_level->iokcomm->get_input_class_ref< ldndc::setup::input_class_setup_t >().latitude());

        //A value of Angstrom formula (-)
        double anga( 0.29);

        //B value of Angstrom formula (-)
        double angb( 0.42);

        //Daily short-wave radiation (J.m-2.d)
        double rdd( (m_climate->ex_rad_day( *clk) * cbm::CM2_IN_M2 * cbm::JDCM_IN_WM));

        //Average temperature (degrees C)
        double tmda( m_climate->temp_avg_day( *clk));

        double scale( 1.0);
        if ( (timemode = TMODE_SUBDAILY) && (tmda > 0.0))
        {
            double t_subdaily = m_climate->temp_avg_subday( *clk);
            scale = t_subdaily / tmda * clk->day_fraction();
        }

        //Average wind speed (m s-1)
        double wn( m_climate->wind_speed_day( *clk));

        //Early morning vapour pressure (kPa)
        double vps( 0.0);
        ldndc::meteo::vps( tmda, &vps);
        double vp( vps - m_climate->vpd_day( *clk));
        vp = m_climate->vpd_day( *clk);

        //reflection coefficient of soil or water background
        //differentiate between standing water or moist/dry soil
        double alb( (m_oryza->surface_water > 0.005) ? 0.05 : 0.25);
        double rfs( (m_oryza->surface_water > 0.005) ? alb : alb * (1.0 - 0.5 * m_oryza->wc_sl[0] / m_oryza->poro_sl[0]));

        // soil or water background is shielded by the crop
        double rf( rfs * std::exp( -0.5 * m_oryza->lai) + 0.25 * (1.0 - std::exp(-0.5 * m_oryza->lai))); //Reflection (=albedo) of surface (-)

        //switch for different surface types (-)
        int isurf( (m_oryza->lai > 0.0) ? 3 : ((m_oryza->surface_water > 0.005) ? 1 : 2));

        //Temperature tolerance (switches between single and iterative Penman)
        double tmdi( 0.0);

        //Potential evapotranspiration (mm d-1)
        double etd( 0.0);

        //Radiation driven part of potential evapotranspiration (mm d-1)
        double etrd( 0.0);

        //Dryness driven part of potential evapotranspiration (mm.d-1)
        double etae( 0.0);

        //Estimated temperature difference between surface height and reference height (degrees C)
        double dt( 0.0);

        //evapotranspiration following penman
        OryzaPenman( _r_level, isurf, m_oryza->idoy, lat, rf, anga, angb, tmdi, rdd, tmda, wn, vp, dt, etd, etrd, etae);

        //multiplication by factor according to fao (1998)
        double const FAOF( 1.3);
        etd  *= FAOF;
        etrd *= FAOF;
        etae *= FAOF;

        //write state
        m_oryza->potentialevapotranspiration = etd * cbm::M_IN_MM * scale;
        if ( cbm::flt_greater_zero( m_oryza->potentialevapotranspiration))
        {
            m_oryza->potentialevaporation = (std::max(0.0, std::exp(-0.5 * m_oryza->lai) * (etrd + etae))) * cbm::M_IN_MM * scale;
            m_oryza->potentialtranspiration = (etrd * (1.0 - std::exp(-0.5 * m_oryza->lai)) + etae * std::min(2.0, m_oryza->lai)) * cbm::M_IN_MM * scale;
            if ( cbm::flt_greater(m_oryza->potentialevaporation + m_oryza->potentialtranspiration, m_oryza->potentialevapotranspiration))
            {
                double const scale_down( m_oryza->potentialevapotranspiration / (m_oryza->potentialevaporation + m_oryza->potentialtranspiration));
                m_oryza->potentialevaporation = m_oryza->potentialevaporation * scale_down;
                m_oryza->potentialtranspiration = m_oryza->potentialtranspiration * scale_down;
            }
        }

        double trc( m_oryza->potentialtranspiration * cbm::MM_IN_M);
        double tiny( 1.0e-9);

        //Potential transpiration rate of crop with given lai per unit root length (mm d−1 m−1)
        double trrm( trc / (m_oryza->zrt + 1.0e-10));

        double trw( 0.0);
        double zll( 0.0);
        double lrav( 0.0);
        double leav( 0.0);
        double ldav( 0.0);

        for ( size_t i = 0; i < m_oryza->sl_.soil_layer_cnt(); i++)
        {
            /*root length in each soil layer */
            double zrtl( std::min( m_oryza->tkl_sl[i], std::max((m_oryza->zrt - zll), 0.0)));

            /*leaf-rolling factor */
            double lr = ( (std::log10( m_oryza->mskpa_sl[i] + tiny) - std::log10( m_oryza->llls)) /
                          (std::log10( m_oryza->ulls)               - std::log10( m_oryza->llls)));
            lr = cbm::bound(0.0, lr, 1.0);
            lrav += (zrtl / (m_oryza->zrt + tiny)) * lr;

            /*relative leaf expansion rate factor */
            double le = ( (std::log10( m_oryza->mskpa_sl[i] + tiny) - std::log10( m_oryza->llle)) /
                          (std::log10( m_oryza->ulle)               - std::log10( m_oryza->llle)));
            le = cbm::bound(0.0, le, 1.0);
            leav += (zrtl / (m_oryza->zrt + tiny)) * le;

            /*relative death rate factor */
            double ld = ( (std::log10( m_oryza->mskpa_sl[i] + tiny) - std::log10( m_oryza->lldl)) /
                          (std::log10( m_oryza->uldl)               - std::log10( m_oryza->lldl)));
            ld = cbm::bound(0.0, ld, 1.0);
            ldav += (zrtl / (m_oryza->zrt + tiny)) * ld;

            /*relative transpiration ratio (actual/potential) */
            if ( m_oryza->mskpa_sl[i] >= 10000.0)
            {
                m_oryza->trr_sl[i] = 0.0;
            }
            else
            {
                m_oryza->trr_sl[i] = cbm::bound( 0.0,
                                                 (std::log10( m_oryza->mskpa_sl[i] + tiny) - std::log10( m_oryza->llrt)) /
                                                 (std::log10( m_oryza->ulrt)               - std::log10( m_oryza->llrt)),
                                                 1.0);
            }

            double const wla( std::max(0.0, (m_oryza->wc_sl[i] - m_oryza->wcwp_sl[i]) * zrtl * 1000.0));
            m_oryza->trwl_sl[i] = std::min( m_oryza->trr_sl[i] * zrtl * trrm, wla);
            trw += m_oryza->trwl_sl[i];
            zll += m_oryza->tkl_sl[i];
        }


        if ( this->wcrop_potential)
        {
            /* set no stress */
            m_oryza->lrstrs = 1.0;
            m_oryza->ldstrs = 1.0;
            m_oryza->lestrs = 1.0;
            m_oryza->cpew = 1.0;
            m_oryza->pcew = 1.0;
        }
        else
        {
            if ( cbm::flt_greater(trw, trc))
            {
                for ( size_t i = 0; i < m_oryza->sl_.soil_layer_cnt(); i++)
                {
                    m_oryza->trwl_sl[i] *= trc / trw;
                }
                m_oryza->pcew = 1.0;
            }
            else
            {
                m_oryza->pcew = (trc > 0.0) ? trw / trc : 1.0;
            }

            m_oryza->lrstrs = lrav;
            m_oryza->ldstrs = ldav;
            m_oryza->lestrs = leav;
            m_oryza->cpew = m_oryza->lestrs;
        }
    }

    return  LDNDC_ERR_OK;
}




