/*!
 * @brief
 *
 * @author:
 *      david kraus (created on: november 4, 2016),
 */


#include  "oryza2000/ld_oryza2000_state.h"
#include  "oryza2000/ld_oryza2000_sublai3.h"

#include  <math/cbm_math.h>

ldndc::Oryza2000Sublai3::~Oryza2000Sublai3()
{
}



ldndc::Oryza2000Sublai3::Oryza2000Sublai3()
{
    this->testl   = false;
    this->x       = 0.0;
    this->test    = 0.0;
    this->testset = 0.0;

    this->wlvgexp = 0.0;
    this->wlvgexs = 0.0;

    this->laiexp  = 0.0;
    this->laiexs  = 0.0;

    this->glai1   = 0.0;
    this->glai2   = 0.0;

    this->tslvtr  = 0.0;
    this->tshckl  = 0.0;

    this->dvse    = 0.0;
}



void
ldndc::Oryza2000Sublai3::run( ldndc::Oryza2000State *  _os)
{
    if ( _os->cropsta <= 1)
    {
        this->x       = 1.0;
        this->testl   = false;
        this->testset = 0.01;
    }


    //Calculate _os->rgrl as function of N stress limitation
    _os->rgrl = _os->rgrlmx - (1.0 - _os->rnstrs) * (_os->rgrlmx - _os->rgrlmn);

    if ( _os->sbdur > 0)
    {
        /* 1. Seed-bed; no drought stress effects in seed-bed! */
        if ( _os->cropsta < 3)
        {
            if ( cbm::flt_less( _os->lai, 1.0))
            {
                _os->glai = _os->lestrs * _os->lai * _os->rgrl * _os->hulv;
                this->wlvgexs = _os->wlvg;
                this->laiexs  = _os->lai;
            }
            else
            {
                if ( !testl)
                {
                    this->test = fabs((_os->lai / _os->wlvg) - _os->sla) / _os->sla;
                    if ( cbm::flt_less( test, testset))
                    {
                        this->testl = true;
                    }

                    if ( this->testl)
                    {
                        _os->glai = ((_os->wlvg + _os->rwlvg) * _os->sla) - _os->lai;
                    }
                    else
                    {
                        this->glai1 = ((_os->wlvg + _os->rwlvg - wlvgexs)   * _os->sla + this->laiexs)  - _os->lai;
                        this->glai2 = ((_os->wlvg + _os->rwlvg)             * _os->sla)                 - _os->lai;

                        if ( cbm::flt_greater_zero( this->glai2))
                        {
                            _os->glai  = (this->glai1 + this->x * this->glai2) / (this->x + 1.0);
                        }
                        else
                        {
                            _os->glai = this->glai1;
                        }
                        this->x = this->x + 1.0;
                    }
                }
            }
        }
        /* 2. Transplanting effects: dilution and shock-setting */
        else if ( _os->cropsta == 3)
        {
            this->tslvtr    = _os->tslv;
            this->tshckl    = _os->shckl * this->tslvtr;
            _os->glai       = (_os->lai * _os->plant_count / _os->nplsb) - _os->lai;
            this->testl     = false;
            this->x         = 1.0;
        }
    }

    if ( _os->cropsta == 4)
    {
        /* 3.1. During transplanting shock-period */
        if ( cbm::flt_less( _os->tslv, (this->tslvtr + this->tshckl)))
        {
            _os->glai = 0.0;
            this->dvse = _os->dvs;
        }
        /* 3.2. After transplanting shock; drought stress effects */
        else
        {
            if ( (cbm::flt_less( _os->lai, 1.0)) && (cbm::flt_less( _os->dvs, 1.0)))
            {
                _os->glai = _os->lestrs * _os->lai * _os->rgrl * _os->hulv;
                this->wlvgexp = _os->wlvg;
                this->laiexp  = _os->lai;
            }
            else
            {
                /* There is a transition from _os->rgrl to _os->sla determined growth
                 * when difference between simulated and imposed _os->sla is less than 1%
                 */

                if ( cbm::flt_less( _os->rwlvg, 0.0))
                {
                    this->testl = true;
                }
                else if ( cbm::flt_greater_equal_zero( _os->rwlvg) && this->testl)
                {
                    this->testl = false;
                }

                if ( !this->testl)
                {
                    test = fabs((_os->lai / _os->wlvg) - _os->sla) / _os->sla;
                    if ( cbm::flt_less( test, testset))
                    {
                        this->testl = true;
                    }
                }

                if ( this->testl)
                {
                    _os->glai = ((_os->wlvg + _os->rwlvg - _os->dldr) * _os->sla) - _os->lai;
                }
                else
                {
                    this->glai1 = ((_os->wlvg + _os->rwlvg - _os->dldr - wlvgexp)   * _os->sla + this->laiexp)  - _os->lai;
                    this->glai2 = ((_os->wlvg + _os->rwlvg - _os->dldr)             * _os->sla)                 - _os->lai;

                    if ( cbm::flt_less( this->glai2, 0.0) && cbm::flt_greater_zero( this->glai1))
                    {
                        _os->glai = this->glai1 / (this->x + 1.0);
                    }
                    else
                    {
                        _os->glai  = (this->glai1 + this->x * this->glai2) / (this->x + 1.0);
                    }
                    this->x = this->x + 1.0;
                }
            }
        }
    }
}

