/*!
 * @file
 *    ldndc kernel wrapper for the Oryza2000 model
 *
 *    Oryza2000 is a product of the International Rice
 *    Research Institute (IRRI) and written by Tao Li.
 *
 * @author
 *    steffen klatt
 *    david kraus (created on: june 30, 2016)
 */

#ifndef  LDNDC_KERNEL_ORYZA2000_H_
#define  LDNDC_KERNEL_ORYZA2000_H_

#include  "ld_kernel.h"
#include  "ld_shared.h"

#include  "oryza2000/ld_oryza2000_state.h"
#include  "oryza2000/ld_oryza2000_subgrn.h"
#include  "oryza2000/ld_oryza2000_phenol.h"
#include  "oryza2000/ld_oryza2000_ncrop.h"
#include  "oryza2000/ld_oryza2000_sublai3.h"
#include  "oryza2000/ld_oryza2000_output.h"

namespace ldndc
{
    namespace  climate
        { class  input_class_climate_t; }
    namespace  setup
        { class  input_class_setup_t; }

    class  substate_soilchemistry_t;
    class  substate_watercycle_t;
}

namespace ldndc {
class LDNDC_API LK_Oryza2000 : public cbm::kernel_t
{
    LDNDC_KERNEL_OBJECT(LK_Oryza2000,oryza2000)

    public:
        LK_Oryza2000();
        ~LK_Oryza2000();

        lerr_t  configure( cbm::RunLevelArgs *);
        lerr_t  register_ports( cbm::RunLevelArgs *);
        lerr_t  initialize( cbm::RunLevelArgs *);

        lerr_t  read( cbm::RunLevelArgs *);
        lerr_t  solve( cbm::RunLevelArgs *);
        lerr_t  integrate( cbm::RunLevelArgs *);
        lerr_t  write( cbm::RunLevelArgs *);
        lerr_t  finalize( cbm::RunLevelArgs *);

        lerr_t  unregister_ports( cbm::RunLevelArgs *);


    protected:
        CBM_Handle  KernelHandle;

    public:

        static const unsigned int IGSN;
        static const double GSX[];
        static const double GSW[];

    public:

        timemode_e timemode;        
    
        /*!
         * @brief
         *      sets unlimited nitrogen availability
         *
         */
        bool ncrop_potential;

        /*!
         * @brief
         *      sets unlimited water availability
         *
         */
        bool wcrop_potential;

        bool farquhar;
        bool root_turnover;

        /*!
         * @brief
         *
         */
        lerr_t OryzaPlant(
                          size_t /* seedbed duration */,
                          double /* seedling number */,
                          char const *);

        /*!
         * @brief
         *
         */
        lerr_t OryzaHarvest();

        /*!
         * @brief
         *
         */
        lerr_t OryzaReset();

        /*!
         * @brief
         *
         */
        lerr_t OryzaBerryBall();

        /*!
         * @brief
         *
         */
        lerr_t OryzaRateCalculation( cbm::RunLevelArgs * );

        /*!
         * @brief
         *
         */
        lerr_t OryzaIntegrate();

        /*!
         * @brief
         *
         */
        lerr_t OryzaUpdateCropstage();

        /*!
         * @brief
         *
         */
        Oryza2000Ncrop m_ncrop;

        /*!
         * @brief
         *
         */
        Oryza2000State *  m_oryza;


        Oryza2000State *  get_state() { return this->m_oryza; }


    private:
        /* hide these buggers for now */
        LK_Oryza2000( LK_Oryza2000 const &);
        LK_Oryza2000 &  operator=( LK_Oryza2000 const &);


    /*!
     * @brief
     *
     */
    lerr_t OryzaTransplanting();


    /*!
     * @brief
     *
     */
    Oryza2000Subgrn m_subgrn;

    /*!
     * @brief
     *
     */
    Oryza2000Sublai3 m_sublai3;
    
    /*!
     * @brief
     *
     */
    Oryza2000Phenol m_phenol;

    /*!
     * @brief
     *
     */
    lerr_t
    OryzaDrout();

    /*!
     * @brief
     *
     */
    lerr_t
    OryzaPenman(
                cbm::RunLevelArgs *  /* run level */,
                int _isurf,
                int _idoy,
                double _lat,
                double _rf,
                double _anga,
                double _angb,
                double _tmdi,
                double _rdd,
                double _tmda,
                double _wn,
                double _vp,
                double _dt,
                double &_etd,
                double &_etrd,
                double &_etae);

    /*!
     * @brief
     *
     */
    lerr_t
    OryzaSaturatedVapourPressure(
                                 double _temperature,
                                 double &_saturated_vapour_pressure,
                                 double &_slope);

    /*!
     * @brief
     *
     */
    lerr_t
    OryzaHydrology(
                   cbm::RunLevelArgs *  /* run level */ );

    /*!
     * @brief
     *
     */
    ldndc::climate::input_class_climate_t const *  m_climate;

    /*!
     * @brief
     *
     */
    ldndc::substate_watercycle_t const *  m_wc;


    /*!
     * @brief
     *  wrapper for kernel-specific output sink; attach/detach and
     *  send to data sink.
     */
    Oryza2000Output  m_output;

private:

    /*!
     * @brief
     *      Low temperature and crop survival
     *      subcd2 calculates number of cold days (ncold)
     */
    lerr_t  subcd2();

    /*!
     * @brief
     *      This subroutine calculates the daily amount of heat units
     *      for calculation of the phenological development rate and
     *      early leaf area growth
     *
     */
    lerr_t  subdd(
                  double /* optimum temperature for development */,
                  double /* maximum temperature for development */,
                  double /* minimum temperature for development */,
                  double & /* heat units */);

    lerr_t OryzaStateInitialization(
                                    Oryza2000State *,
                                    char const *);

    /* helper functions */
    lerr_t  str_c2f(
                    char *,
                    size_t,
                    char const *);

    lerr_t
    sgpc1(
          double _cslv,
          double _eff,
          double _ecpdf,
          double _gai,
          double _sinb,
          double _rdpdr,
          double _rdpdf,
          double &_gpc);

    lerr_t
    gpparget(
             double _xgai,
             double _xgaid,
             double _xeffin,
             double &_xamaxout,
             double &_xeffout);


    lerr_t
    sgpl(
         double _cslv,
         double _eff1,
         double _ecpdf,
         double _gai,
         double _gaid,
         double _sinb,
         double _rdpdr,
         double _rdpdf,
         double &_gpl);

    lerr_t
    srdprf(
           double _gaid,
           double _cslv,
           double _sinb,
           double _ecpdf,
           double _rdpdr,
           double _rdpdf,
           double &_rapshl,
           double &_rapppl,
           double &_fslla);

    lerr_t
    sgpcdt(
           int /* day of year */,
           double /* latitude */,
           double _rdd,
           double _frpar,
           double _cslv,
           double _eff,
           double _ecpdf,
           double _gai,
           double /* daylength */,
           double &_gpcdt);

    lerr_t
    sskyc(
          double _sinb,
          double _solcon,
          double _frpar,
          double _dsinbe,
          double _rdd,
          double &_rdpdr,
          double &_rdpdf);

    PublishedField<double>  maturity_status;
};

} /* namespace ldndc */

#endif /* !LDNDC_KERNEL_ORYZA2000_H_ */

