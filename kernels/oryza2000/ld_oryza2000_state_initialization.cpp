/*!
 * @brief
 *
 * @author:
 *      david kraus (created on: november 7, 2016),
 *
 */

#include  <string/cbm_string.h>

#include  "oryza2000/ld_oryza2000.h"
#include  "oryza2000/ld_oryza2000_state.h"

#include  <logging/cbm_logging.h>

lerr_t
ldndc::LK_Oryza2000::OryzaStateInitialization(
        ldndc::Oryza2000State *_os, char const *_species_name)
{
    /********************************/
    /* DEFAULT PARAMETER SET (IR64) */
    /********************************/
    {
        /* Phenological development parameters */
        _os->tbd = 8.0;   //Base temperature for development (oC)
        _os->tblv = 8.0;  //Base temperature for juvenile leaf area growth (oC)
        _os->tmd = 42.0;  //Maximum temperature for development (oC)
        _os->tod = 30.0;  //Optimum temperature for development (oC)

        _os->dvrj = 0.000651; //Development rate in juvenile phase (oCd-1)
        _os->dvri = 0.000758; //Development rate in photoperiod-sensitive phase (oCd-1)
        _os->dvrp = 0.001093; //Development rate in panicle development (oCd-1)
        _os->dvrr = 0.002191; //Development rate in reproductive phase (oCd-1)

        _os->mopp = 11.50;    //Maximum optimum photoperiod (h)
        _os->ppse = 0.0;      //Photoperiod sensitivity (h-1)
        _os->shckd = 0.4;     //Relation between seedling age and delay in phenological development (oCd oCd-1)

        /* Leaf and stem growth parameters */
        _os->rgrlmx = 0.0085; //Maximum relative growth rate of leaf area (oCd-1)
        _os->rgrlmn = 0.0040; //Minimum relative growth rate of leaf area (oCd-1)
        _os->shckl = 0.25;    //Relation between seedling age and delay in leaf area development (oCd oCd-1)

        /* Switch to use SLA as table (give values below) or as fixed function */
        // Give function parameters  ASLA, BSLA, CSLA, DSLA, SLAMAX
        this->str_c2f( _os->swisla, SWISLA_SIZE, "FUNCTION");

        //SLA = ASLA + BSLA*EXP(CSLA*(DVS-DSLA)), and SLAMAX
        _os->asla = 0.0021;
        _os->bsla = 0.0025;
        _os->csla = -3.0;
        _os->dsla = 0.3;
        _os->slamax = 0.0060; //maximum value of SLA (ha/kg)

        /* If SWISLA='TABLE', supply table of specific leaf area (ha kg-1; Y value)
         * as a function of development stage (-; X value): */
        double const slatb_values[] =   {
            0.00, 0.006,
            0.24, 0.006,
            0.31, 0.004,
            0.58, 0.004,
            0.67, 0.003,
            1.13, 0.003,
            2.54, 0.002,
            2.50, 0.002};
        _os->slatb = OryzaTable("SLATB", slatb_values, 16);

        /* Table of specific green stem area (ha kg-1; Y value) as a function of
         * development stage (-; X value): */
        double const ssgatb_values[] =  {
            0.00, 0.0003,
            0.90, 0.0003,
            2.00, 0.0000,
            2.50, 0.0000};
        _os->ssgatb = OryzaTable("SSGATB", ssgatb_values, 8);

        /* 3. Photosynthesis parameters */
        _os->frpar  = 0.5;   //Fraction of photosynthetically active sunlight energy (-)
        _os->scp    = 0.2;   //Scattering coefficient of leaves for PAR (-)
        _os->co2ref = 340.0; //Reference level of atmospheric CO2 (ppm)
        _os->co2    = 340.0; //Ambient CO2 concentration (ppm)

        /* Table of light extinction coefficient for leaves (-; Y-value) as a function
         * of development stage (-; X value):*/
        double const kdftb_values[] =   {
            0.00, 0.4,
            0.65, 0.4,
            1.00, 0.6,
            2.50, 0.6};
        _os->kdftb = OryzaTable("KDFTB", kdftb_values, 8);

        /* Table of extinction coefficient of N profile in the canopy (-; Y-value)
         * as a functionof development stage (-; X value): */
        double const knftb_values[] =   {
            0.0, 0.4,
            2.5, 0.4};
        _os->knftb = OryzaTable("KNFTB", knftb_values, 4);

        /* Table of light use effiency (-; Y-value) as a function of
         * temperature (oC; X value): */
        double const efftb_values[] =   {
            10.0 ,0.54,
            40.0 ,0.36};
        _os->efftb = OryzaTable("EFFTB", efftb_values, 4);

        /* Table of effect of temperature on AMAX (-; Y-value) as a function of
         * temperature (oC; X value): */
        double const redftt_values[] = {
            -10.0, 0.0,
            10.0, 0.0,
            20.0, 1.0,
            37.0, 1.0,
            43.0, 0.0};
        _os->redftt = OryzaTable("REDFTT", redftt_values, 10);

        /* Table of N fraction in leaves on leaf area basis (g N m-2 leaf; Y-value)
         * as a function of development stage (-; X value): */
        double const nflvtb_values[] =  {
            0.00, 0.8395,
            0.14, 0.8395,
            0.18, 1.8860,
            0.29, 2.3805,
            0.38, 1.5295,
            0.65, 1.4260,
            0.89, 1.0695,
            1.00, 1.0580,
            1.50, 1.0925,
            2.12, 0.7130,
            2.50, 0.7130};
        _os->nflvtb = OryzaTable("NFLVTB", nflvtb_values, 22);

        /* Maintenance parameters */
        /* Maintenance respiration coefficient (kg CH2O kg-1 DM d-1) */
        _os->mainlv = 0.02;   //Leaves
        _os->mainst = 0.015;  //Stems
        _os->mainso = 0.003;  //Storage organs (panicles)
        _os->mainrt = 0.01;   //Roots

        _os->tref   = 25.0;   //Reference temperature (oC)
        _os->q10    = 2.0;    //Factor accounting for increase of maintenance respiration with a 10 oC rise in temperature (-)

        /* Growth respiration parameters */
        /* Carbohydrate requirement for dry matter production (kg CH2O kg-1 DM leaf) of: */
        _os->crglv  = 1.326;  //Leaves
        _os->crgst  = 1.326;  //Stems
        _os->crgso  = 1.462;  //Storage organs (panicles)
        _os->crgrt  = 1.326;  //Roots
        _os->crgstr = 1.11;   //Stem reserves

        _os->lrstr  = 0.947;  //Fraction of allocated stem reserves that is available for growth (-)

        /* Growth parameters */
        _os->fstr   = 0.20;       //Fraction of carbohydrates allocated to stems that is stored as reserves (-)
        _os->tclstr = 10.0;       //Time coefficient for loss of stem reserves (1 d-1)
        _os->spgf   = 64900.0;    //Spikelet growth factor (no kg-1)
        _os->wgrmx  = 0.0000249;  //Maximum individual grain weight (kg grain-1)

        /* Partitioning tables
         * Table of fraction total dry matter partitioned to the shoot (-; Y-value)
         * as a function of development stage (-; X value): */
        double const fshtb_values[] =   {
            0.00,  0.50,
            0.43,  0.75,
            1.00,  1.00,
            2.50,  1.00};
        _os->fshtb = OryzaTable("FSHTB", fshtb_values, 8);

        /* Table of fraction shoot dry matter partitioned to the leaves (-; Y-value)
         * as a function of development stage (-; X value): */
        double const flvtb_values[] =   {
            0.000, 0.50,
            0.350, 0.50,
            0.610, 0.30,
            0.720, 0.20,
            0.880, 0.06,
            1.230, 0.00,
            2.500, 0.00};
        _os->flvtb = OryzaTable("FLVTB", flvtb_values, 14);

        /* Table of fraction shoot dry matter partitioned to the stems (-; Y-value)
         * as a function of development stage (-; X value): */
        double const fsttb_values[] =   {
            0.000, 0.50,
            0.350, 0.50,
            0.610, 0.70,
            0.720, 0.80,
            0.880, 0.55,
            1.230, 0.00,
            2.500, 0.00};
        _os->fsttb = OryzaTable("FSTTB", fsttb_values, 14);

        /* Table of fraction shoot dry matter partitioned to the panicles (-; Y-value)
         * as a function of development stage (-; X value): */
        double const fsotb_values[] =   {
            0.000, 0.000,
            0.610, 0.000,
            0.720, 0.000,
            0.880, 0.390,
            1.230, 1.000,
            2.500, 1.000};
        _os->fsotb = OryzaTable("FSOTB", fsotb_values, 12);

        /* Table of leaf death coefficient (d-1; Y-value) as a function of development
         * stage (-; X value): */
        double const drlvt_values[] =   {
            0.0,  0.00,
            0.9,  0.00,
            2.0,  0.11,
            2.5,  0.11};
        _os->drlvt = OryzaTable("DRLVT", drlvt_values, 8);

        /* 7. Carbon balance parameters */
        /* Mass fraction carbon (kg C kg-1 DM) in the: */
        _os->fclv   = 0.419;  //Leaves
        _os->fcst   = 0.431;  //Stems
        _os->fcso   = 0.487;  //Storage organs (panicles)
        _os->fcrt   = 0.431;  //Roots
        _os->fcstr  = 0.444;  //Stem reserves

        /* 8. Root parameters (Taken from IR72 data file!!!) */
        _os->gzrt   = 0.01;   //Growth rate of roots (m d-1)
        _os->zrtmcw = 0.25;   //Maximum depth of roots if no drought stress (m)
        _os->zrtmcd = 0.25;   //Maximum depth of roots if drought (m)

        /* 9. Drought stress parameters (Taken from IR72 data file!!!)
         * Upper and lower limits for drought stress effects*/
        _os->ulls = 74.31;        //Upper limit leaf rolling (kPa)
        _os->llls = 794.33;       //Lower limit leaf rolling (kPa)
        _os->uldl = 630.95;       //Upper limit death of leaves (kPa)
        _os->lldl = 1584.89;      //Lower limit death of leaves (kPa)
        _os->ulle = 1.45;         //Upper limit leaf expansion (kPa)
        _os->llle = 1404.0;       //Lower limit leaf expansion (kPa)
        _os->ulrt = 74.13;        //Upper limit relative transpiration reduction (kPa)
        _os->llrt = 1584.89;      //Lower limit relative transpiration reduction (kPa)

        /* 10. Nitrogen parameters */
        _os->nmaxup  = 8.0;       //Maximum daily N uptake (kg N ha-1 d-1)
        _os->rfnlv   = 0.004;     //Residual N fraction of leaves (kg N kg-1 leaves)
        _os->fntrt   = 0.15;      //Fraction N translocation from roots, as (additonal) fraction of total N translocation from stems and leaves (-)
        _os->rfnst   = 0.0015;    //Residual N fraction of stems (kg N kg-1 stems)
        _os->tcntrf  = 10.0;      //Time coefficient for N translocation to grains (d)
        _os->nflvi   = 0.5;       //Initial leaf N fraction (on area basis: g N m-2 leaf)
        _os->fnlvi   = 0.025;     //Initial leaf N fraction (on weight basis: kg N kg-1 leaf)

        _os->nmaxso = 0.0175;     //Maximum N concentration in storage organs (kg N kg-1)

        /* Table of minimum N concentration in storage organ (kg N kg-1 DM; Y value)
         * as a function of the amount of N in the crop till flowering (kg N ha-1; X value):
         */
        double const nminsottb_values[] = {
            0., .006,
            50., .0008,
            150., .0125,
            250., .015,
            400., .017,
            1000., .017};
        _os->nminsottb = OryzaTable("NMINSOT", nminsottb_values, 12);

        /* Table of maximum leaf N fraction on weight basis (kg N kg-1 leaves; Y value)
         * as a function of development stage (-; X value):
         */
        double const nmaxlttb_values[] = {
            0.0,  .053,
            0.4,  .053,
            0.75, .040,
            1.0,  .028,
            2.0,  .022,
            2.5,  .015};
        _os->nmaxlttb = OryzaTable("NMAXLT", nmaxlttb_values, 12);

        /* Table of minimum leaf N fraction on weight basis (kg N kg-1 leaves; Y value)
         * as a function of development stage (-; X value):
         */
        double const nminlttb_values[] = {
            0.0, 0.025,
            1.0, 0.012,
            2.1, 0.007,
            2.5, 0.007};
        _os->nminlttb = OryzaTable("NMINLT", nminlttb_values, 8);

        /*--- Table of effect of N stress on leaf death rate (-; Y value)
         * as a function of N stress level (-; X value):
         */
        double const nsllvttb_values[] = {
            0.,  1.0,
            1.1, 1.0,
            1.5, 1.4,
            2.0, 1.5,
            2.5, 1.5};
        _os->nsllvttb = OryzaTable("NSLLVT", nsllvttb_values, 10);
    }

    /********/
    /* IR72 */
    /********/
    if ( cbm::is_equal_i( _species_name, "IR72"))
    {
        /* Phenological development parameters */
        _os->dvrj = 0.000773; //Development rate in juvenile phase (oCd-1)
        _os->dvri = 0.000758; //Development rate in photoperiod-sensitive phase (oCd-1)
        _os->dvrp = 0.000784; //Development rate in panicle development (oCd-1)
        _os->dvrr = 0.001784; //Development rate in reproductive phase (oCd-1)

        /* SLA function parameters */
        _os->asla = 0.0024;
        _os->bsla = 0.0025;
        _os->csla = -4.5;
        _os->dsla = 0.14;
        _os->slamax = 0.0045; //maximum value of SLA (ha/kg)

        /* If SWISLA='TABLE', supply table of specific leaf area (ha kg-1; Y value)
         * as a function of development stage (-; X value):
         */
        double const slatb_values[] = {
            0.00, 0.0045,
            0.16, 0.0045,
            0.33, 0.0033,
            0.65, 0.0028,
            0.79, 0.0024,
            2.10, 0.0023,
            2.50, 0.0023};
        _os->slatb = OryzaTable("SLATB", slatb_values, 14);

        /* Table of specific green stem area (ha kg-1; Y value) as a function of
         * development stage (-; X value):
         */
        double const ssgatb_values[] = {
            0.00, 0.0003,
            0.90, 0.0003,
            2.10, 0.0000,
            2.50, 0.0000};
        _os->ssgatb = OryzaTable("SSGATB", ssgatb_values, 8);

        /* Table of light use effiency (-; Y-value) as a function of
         * temperature (oC; X value): */
        double const efftb_values[] = {
            0.0 ,0.54,
            10.0 ,0.54,
            40.0 ,0.36};
        _os->efftb = OryzaTable("EFFTB", efftb_values, 6);

        /* Table of N fraction in leaves on leaf area basis (g N m-2 leaf; Y-value)
         * as a function of development stage (-; X value): */
        double const nflvtb_values[] = {
            0.00, 0.54,
            0.16, 0.54,
            0.33, 1.53,
            0.65, 1.22,
            0.79, 1.56,
            1.00, 1.29,
            1.46, 1.37,
            2.02, 0.83,
            2.50, 0.83};
        _os->nflvtb = OryzaTable("NFLVTB", nflvtb_values, 18);

        /* Table of fraction shoot dry matter partitioned to the leaves (-; Y-value)
         * as a function of development stage (-; X value): */
        double const flvtb_values[] = {
            0.000, 0.60,
            0.500, 0.60,
            0.750, 0.30,
            1.000, 0.00,
            1.200, 0.00,
            2.500, 0.00};
        _os->flvtb = OryzaTable("FLVTB", flvtb_values, 12);

        /* Table of fraction shoot dry matter partitioned to the stems (-; Y-value)
         * as a function of development stage (-; X value): */
        double const fsttb_values[] = {
            0.000, 0.40,
            0.500, 0.40,
            0.750, 0.70,
            1.000, 0.40,
            1.200, 0.00,
            2.5  , 0.00};
        _os->fsttb = OryzaTable("FSTTB", fsttb_values, 12);

        /* Table of fraction shoot dry matter partitioned to the panicles (-; Y-value)
         * as a function of development stage (-; X value): */
        double const fsotb_values[] = {
            0.000, 0.000,
            0.500, 0.000,
            0.750, 0.000,
            1.000, 0.600,
            1.200, 1.000,
            2.5  , 1.000};
        _os->fsotb = OryzaTable("FSOTB", fsotb_values, 12);

        /* Table of leaf death coefficient (d-1; Y-value) as a function of development
         * stage (-; X value): */
        double const drlvt_values[] = {
            0.00, 0.000,
            0.60, 0.000,
            1.00, 0.015,
            1.60, 0.025,
            2.10, 0.050,
            2.50, 0.050};
        _os->drlvt = OryzaTable("DRLVT", drlvt_values, 12);

        /* 8. Root parameters (Taken from IR72 data file!!!) */
        _os->zrtmcd = 0.4;   //Maximum depth of roots if drought (m)

        /* 9. Drought stress parameters (Taken from IR72 data file!!!)
         * Upper and lower limits for drought stress effects*/
        _os->llle = 1404.0;        //Lower limit leaf expansion (kPa)
    }

    /**********/
    /* PAU201 */
    /**********/
    else if ( cbm::is_equal_i( _species_name, "PAU201"))
    {
        /* Phenological development parameters */
        _os->dvrj = 0.000683; //Development rate in juvenile phase (oCd-1)
        _os->dvrp = 0.000779; //Development rate in panicle development (oCd-1)
        _os->dvrr = 0.002170; //Development rate in reproductive phase (oCd-1)

        /* SLA function parameters */
        _os->asla = 0.0024;
        _os->bsla = 0.0025;
        _os->csla = -4.5;
        _os->dsla = 0.14;
        _os->slamax = 0.0045; //maximum value of SLA (ha/kg)

        /* If SWISLA='TABLE', supply table of specific leaf area (ha kg-1; Y value)
         * as a function of development stage (-; X value): */
        double const slatb_values[] =   {
            0.00, 0.0020,
            0.25, 0.0018,
            0.33, 0.0012,
            0.64, 0.0011,
            0.93, 0.0010,
            1.30, 0.0010,
            1.75, 0.0010,
            2.10, 0.0010,
            2.50, 0.0010};
        _os->slatb = OryzaTable("SLATB", slatb_values, 18);

        /* Table of specific green stem area (ha kg-1; Y value) as a function of
         * development stage (-; X value): */
        double const ssgatb_values[] =  {
            0.00, 0.0003,
            0.90, 0.0003,
            2.10, 0.0000,
            2.50, 0.0000};
        _os->ssgatb = OryzaTable("SSGATB", ssgatb_values, 8);

        /* 3. Photosynthesis parameters */
        /* Table of light extinction coefficient for leaves (-; Y-value) as a function
         * of development stage (-; X value):*/
        double const kdftb_values[] =   {
            0.00, 0.38,
            0.65, 0.38,
            1.00, 0.55,
            2.50, 0.38};
        _os->kdftb = OryzaTable("KDFTB", kdftb_values, 8);

        /* Table of light use effiency (-; Y-value) as a function of
         * temperature (oC; X value): */
        double const efftb_values[] =   {
            0.0, 0.52,
            10.0, 0.52,
            40.0, 0.34,
            60.0, 0.22};
        _os->efftb = OryzaTable("EFFTB", efftb_values, 8);

        /* Table of effect of temperature on AMAX (-; Y-value) as a function of
         * temperature (oC; X value): */
        double const redftt_values[] = {
            -10.0, 0.0,
            10.0, 0.0,
            20.0, 1.0,
            37.0, 1.0,
            43.0, 0.0,
            60.0, 0.0};
        _os->redftt = OryzaTable("REDFTT", redftt_values, 12);

        /* Table of N fraction in leaves on leaf area basis (g N m-2 leaf; Y-value)
         * as a function of development stage (-; X value): */
        double const nflvtb_values[] =  {
            0.00, 0.54,
            0.16, 0.54,
            0.33, 1.53,
            0.65, 1.22,
            0.79, 1.56,
            1.00, 1.29,
            1.46, 1.37,
            2.02, 0.83,
            2.50, 0.83};
        _os->nflvtb = OryzaTable("NFLVTB", nflvtb_values, 18);

        /* Growth parameters */
        _os->fstr   = 0.115;      //Fraction of carbohydrates allocated to stems that is stored as reserves (-)

        /* Partitioning tables
         * Table of fraction shoot dry matter partitioned to the leaves (-; Y-value)
         * as a function of development stage (-; X value): */
        double const flvtb_values[] =   {
            0.000, 0.50,
            0.500, 0.50,
            0.750, 0.23,
            1.000, 0.00,
            1.200, 0.00,
            2.5  , 0.00};
        _os->flvtb = OryzaTable("FLVTB", flvtb_values, 12);

        /* Table of fraction shoot dry matter partitioned to the stems (-; Y-value)
         * as a function of development stage (-; X value): */
        double const fsttb_values[] =   {
            0.000, 0.50,
            0.500, 0.50,
            0.750, 0.77,
            1.000, 0.40,
            1.200, 0.00,
            2.5  , 0.00};
        _os->fsttb = OryzaTable("FSTTB", fsttb_values, 12);

        /* Table of fraction shoot dry matter partitioned to the panicles (-; Y-value)
         * as a function of development stage (-; X value): */
        double const fsotb_values[] = {
            0.000, 0.000,
            0.500, 0.000,
            0.750, 0.000,
            1.000, 0.600,
            1.200, 1.000,
            2.500, 1.000};
        _os->fsotb = OryzaTable("FSOTB", fsotb_values, 12);

        /* Table of leaf death coefficient (d-1; Y-value) as a function of development
         * stage (-; X value): */
        double const drlvt_values[] = {
            0.00, 0.000,
            0.60, 0.000,
            1.00, 0.015,
            1.60, 0.025,
            2.10, 0.050,
            2.50, 0.050};
        _os->drlvt = OryzaTable("DRLVT", drlvt_values, 12);

        /* 8. Root parameters (Taken from IR72 data file!!!) */
        _os->gzrt   = 0.01;   //Growth rate of roots (m d-1)
        _os->zrtmcw = 0.25;   //Maximum depth of roots if no drought stress (m)
        _os->zrtmcd = 0.40;   //Maximum depth of roots if drought (m)
    }

    /********/
    /* IR64 */
    /********/
    else if ( cbm::is_equal_i( _species_name, "IR64"))
    {
        //_os->llle = 1404.0;       //Lower limit leaf expansion (kPa)
    }

    /*********/
    /* JD305 */
    /*********/
    else if ( cbm::is_equal_i( _species_name, "JD305"))
    {
        /* Phenological development parameters */
        _os->dvrj = 0.000419; //Development rate in juvenile phase (oCd-1)
        _os->dvrp = 0.000586; //Development rate in panicle development (oCd-1)
        _os->dvrr = 0.002154; //Development rate in reproductive phase (oCd-1)

        _os->mopp = 11.50;    //Maximum optimum photoperiod (h)
        _os->ppse = 0.0;      //Photoperiod sensitivity (h-1)
        _os->shckd = 0.4;     //Relation between seedling age and delay in phenological development (oCd oCd-1)

        //SLA = ASLA + BSLA*EXP(CSLA*(DVS-DSLA)), and SLAMAX
        _os->asla = 0.0024;
        _os->bsla = 0.0025;
        _os->csla = -4.5;
        _os->dsla = 0.14;
        _os->slamax = 0.0045; //maximum value of SLA (ha/kg)

        /* If SWISLA='TABLE', supply table of specific leaf area (ha kg-1; Y value)
         * as a function of development stage (-; X value): */
        double const slatb_values[] =   {
            0.00, 0.026,
            2.50, 0.026};
        _os->slatb = OryzaTable("SLATB", slatb_values, 4);

        /* 3. Photosynthesis parameters */
        /* Table of light use effiency (-; Y-value) as a function of
         * temperature (oC; X value): */
        double const efftb_values[] =   {
            0.0, 0.54,
            10.0 ,0.54,
            40.0 ,0.36};
        _os->efftb = OryzaTable("EFFTB", efftb_values, 6);

        /* Table of N fraction in leaves on leaf area basis (g N m-2 leaf; Y-value)
         * as a function of development stage (-; X value): */
        double const nflvtb_values[] =  {
            0.00, 0.54,
            0.16, 0.54,
            0.33, 1.53,
            0.65, 1.22,
            0.79, 1.56,
            1.00, 1.29,
            1.46, 1.37,
            2.02, 0.83,
            2.50, 0.83};
        _os->nflvtb = OryzaTable("NFLVTB", nflvtb_values, 18);

        /* Growth parameters */
        _os->fstr   = 0.40;       //Fraction of carbohydrates allocated to stems that is stored as reserves (-)

        /* Partitioning tables
         * Table of fraction shoot dry matter partitioned to the leaves (-; Y-value)
         * as a function of development stage (-; X value): */
        double const flvtb_values[] =   {
            0.000, 0.50,
            0.400, 0.50,
            0.750, 0.30,
            1.000, 0.00,
            1.300, 0.00,
            2.500, 0.00};
        _os->flvtb = OryzaTable("FLVTB", flvtb_values, 12);

        /* Table of fraction shoot dry matter partitioned to the stems (-; Y-value)
         * as a function of development stage (-; X value): */
        double const fsttb_values[] =   {
            0.000, 0.50,
            0.400, 0.50,
            0.750, 0.70,
            1.000, 0.50,
            1.300, 0.00,
            2.500, 0.00};
        _os->fsttb = OryzaTable("FSTTB", fsttb_values, 12);
        
        /* Table of fraction shoot dry matter partitioned to the panicles (-; Y-value)
         * as a function of development stage (-; X value): */
        double const fsotb_values[] =   {
            0.000, 0.000,
            0.400, 0.000,
            0.750, 0.000,
            1.000, 0.500,
            1.300, 1.000,
            2.500, 1.000};
        _os->fsotb = OryzaTable("FSOTB", fsotb_values, 12);
        
        /* Table of leaf death coefficient (d-1; Y-value) as a function of development
         * stage (-; X value): */
        double const drlvt_values[] =   {
            0.00,  0.000,
            1.00,  0.015,
            1.25,  0.025,
            1.50,  0.045,
            2.10,  0.070,
            2.50,  0.070};
        _os->drlvt = OryzaTable("DRLVT", drlvt_values, 12);
        
        /* 8. Root parameters (Taken from IR72 data file!!!) */
        _os->gzrt   = 0.01;   //Growth rate of roots (m d-1)
        _os->zrtmcw = 0.25;   //Maximum depth of roots if no drought stress (m)
        _os->zrtmcd = 0.40;   //Maximum depth of roots if drought (m)
    }
    
    /*
     * Tubigan 18
     * IRRI variety released for irrigated and favourable rainfed areas 
     * in the Philippines
     */
    else if ( cbm::is_equal_i( _species_name, "tubigan18")
             || cbm::is_equal_i( _species_name, "nsicrc222")
             || cbm::is_equal_i( _species_name, "irri154")
             || cbm::is_equal_i( _species_name, "ir154"))
    {
        /* Growth respiration parameters */
        /* Carbohydrate requirement for dry matter production (kg CH2O kg-1 DM leaf) of: */
        _os->crglv  = 1.2;  //Leaves
        _os->crgst  = 1.2;  //Stems
        _os->crgso  = 1.2;  //Storage organs (panicles)
        _os->crgrt  = 1.2;  //Roots
        _os->crgstr = 1.1;   //Stem reserves


        _os->dvrj = 0.000576; //Development rate in juvenile phase (oCd-1)
        _os->dvri = 0.000758; //Development rate in photoperiod-sensitive phase (oCd-1)
        _os->dvrp = 0.00075;  //Development rate in panicle development (oCd-1)
        _os->dvrr = 0.001787; //Development rate in reproductive phase (oCd-1)

        /* Leaf and stem growth parameters */
        _os->rgrlmx = 0.00644284; //Maximum relative growth rate of leaf area (oCd-1)
        _os->rgrlmn = 0.00449677; //Minimum relative growth rate of leaf area (oCd-1)

        /* Switch to use SLA as table (give values below) or as fixed function */
        // Give function parameters  ASLA, BSLA, CSLA, DSLA, SLAMAX
        this->str_c2f( _os->swisla, SWISLA_SIZE, "FUNCTION");

        //SLA = ASLA + BSLA*EXP(CSLA*(DVS-DSLA)), and SLAMAX
        _os->asla = 0.0018;
        _os->bsla = 0.0024;
        _os->csla = -4.5;
        _os->dsla = 0.13223921;
        _os->slamax = 0.004; //maximum value of SLA (ha/kg)




        /* If SWISLA='TABLE', supply table of specific leaf area (ha kg-1; Y value)
         * as a function of development stage (-; X value): */
//        double const slatb_values[] =   {
//            0.00, 0.00303833,
//            0.16, 0.00258598,
//            0.33, 0.00196212,
//            0.65, 0.00417748,
//            0.79, 0.00176629,
//            2.09, 0.00140471,
//            2.50, 0.00201711};
//        _os->slatb = OryzaTable("SLATB", slatb_values, 14);

        /* Table of specific green stem area (ha kg-1; Y value) as a function of
         * development stage (-; X value): */
        double const ssgatb_values[] =  {
            0.00, 0.0003,
            0.90, 0.0003,
            2.10, 0.0000,
            2.50, 0.0000};
        _os->ssgatb = OryzaTable("SSGATB", ssgatb_values, 8);

        /* Table of light extinction coefficient for leaves (-; Y-value) as a function
         * of development stage (-; X value):*/
        double const kdftb_values[] =   {
            0.00, 0.27452961,
            0.65, 0.47602484,
            1.00, 0.52242482,
            2.50, 0.50222266};
        _os->kdftb = OryzaTable("KDFTB", kdftb_values, 8);

        /* Table of light use effiency (-; Y-value) as a function of
         * temperature (oC; X value): */
        double const efftb_values[] =   {
            0.0, 0.7,//0.62863976,
            10.0 , 0.6,//0.51997662,
            40.0 , 0.45,//0.36342824,
            60, 0.25};//0.22876880};
        _os->efftb = OryzaTable("EFFTB", efftb_values, 8);


        /* Table of effect of temperature on AMAX (-; Y-value) as a function of
         * temperature (oC; X value): */
        double const redftt_values[] = {
            -10.0, 0.0,
            10.0, 0.0,
            20.0, 1.0,
            37.0, 1.0,
            43.0, 0.0,
            60.0, 0.0};
        _os->redftt = OryzaTable("REDFTT", redftt_values, 12);

        /* Table of N fraction in leaves on leaf area basis (g N m-2 leaf; Y-value)
         * as a function of development stage (-; X value): */
        double const nflvtb_values[] =  {
            0.00, 0.54,
            0.16, 0.54,
            0.33, 1.53,
            0.65, 1.22,
            0.79, 1.56,
            1.00, 1.29,
            1.46, 1.37,
            2.02, 0.83,
            2.50, 0.83};
        _os->nflvtb = OryzaTable("NFLVTB", nflvtb_values, 18);

        /* Growth parameters */
        _os->fstr   = 0.20868260;   //Fraction of carbohydrates allocated to stems that is stored as reserves (-)

        /* Partitioning tables
         * Table of fraction total dry matter partitioned to the shoot (-; Y-value)
         * as a function of development stage (-; X value): */
        double const fshtb_values[] =   {
            0.00,  0.50,
            0.43,  0.75,
            1.00,  1.00,
            2.50,  1.00};
        _os->fshtb = OryzaTable("FSHTB", fshtb_values, 8);

        /* Table of fraction shoot dry matter partitioned to the leaves (-; Y-value)
         * as a function of development stage (-; X value): */
        double const flvtb_values[] =   {
            0.00000000,    0.27373058,
            0.50000000,    0.66748786,
            0.75000000,    0.44403929,
            1.00000000,    0.12287974,
            1.20000005,    0.00335920,
            2.50000000,    0.00711048};
        _os->flvtb = OryzaTable("FLVTB", flvtb_values, 12);

        /* Table of fraction shoot dry matter partitioned to the stems (-; Y-value)
         * as a function of development stage (-; X value): */
        double const fsttb_values[] =   {
            0.00000000,    0.72626942,
            0.50000000,    0.33251214,
            0.75000000,    0.55596071,
            1.00000000,    0.41947722,
            1.20000005,    0.32426178,
            2.50000000,    0.15687069};
        _os->fsttb = OryzaTable("FSTTB", fsttb_values, 12);

        /* Table of fraction shoot dry matter partitioned to the panicles (-; Y-value)
         * as a function of development stage (-; X value): */
        double const fsotb_values[] =   {
            0.00000000,    0.00000000,
            0.50000000,    0.00000000,
            0.75000000,    0.00000000,
            1.00000000,    0.45764303,
            1.20000005,    0.67237902,
            2.50000000,    0.83601880};
        _os->fsotb = OryzaTable("FSOTB", fsotb_values, 12);

        /* Table of leaf death coefficient (d-1; Y-value) as a function of development
         * stage (-; X value): */
        double const drlvt_values[] =   {
            0.00000000,    0.00000000,
            0.60000002,    0.00000000,
            1.00000000,    0.02066444,
            1.60000002,    0.02792809,
            2.09999990,    0.09583403,
            2.50000000,    0.02775621};
        _os->drlvt = OryzaTable("DRLVT", drlvt_values, 12);

        /* 8. Root parameters (Taken from IR72 data file!!!) */
        _os->zrtmcd = 27.86956787;   //Maximum depth of roots if drought (m)

        /* 9. Drought stress parameters (Taken from IR72 data file!!!)
         * Upper and lower limits for drought stress effects*/
        _os->ulls = 42.70002365;        //Upper limit leaf rolling (kPa)
        _os->llls = 675.81939697;       //Lower limit leaf rolling (kPa)
        _os->uldl = 750.77191162;       //Upper limit death of leaves (kPa)
        _os->lldl = 1903.58630371;      //Lower limit death of leaves (kPa)
        _os->ulle = 0.97901726;         //Upper limit leaf expansion (kPa)
        _os->llle = 1339.32299805;       //Lower limit leaf expansion (kPa)

        /* 10. Nitrogen parameters */
        _os->fntrt   = 0.15651770;      //Fraction N translocation from roots, as (additonal) fraction of total N translocation from stems and leaves (-)

        _os->nmaxso = 0.01809942;     //Maximum N concentration in storage organs (kg N kg-1)

        /* Table of maximum leaf N fraction on weight basis (kg N kg-1 leaves; Y value)
         * as a function of development stage (-; X value):
         */
        double const nmaxlttb_values[] = {
            0.00000000,    0.04396891,
            0.40000001,    0.06740371,
            0.75000000,    0.05380322,
            1.00000000,    0.02199621,
            2.00000000,    0.02259898,
            2.50000000,    0.01043205};
        _os->nmaxlttb = OryzaTable("NMAXLT", nmaxlttb_values, 12);
    }

    /*
     * Sahod Ulan 1
     * Drought tolerant IRRI variety released for the Philippines
     */
    else if ( cbm::is_equal_i( _species_name, "sahodulan1")
             || cbm::is_equal_i( _species_name, "nsicrc192")
             || cbm::is_equal_i( _species_name, "irri148")
             || cbm::is_equal_i( _species_name, "ir148"))
    {
        /* Phenological development parameters */
        _os->tbd = 8.0;   //Base temperature for development (oC)
        _os->tblv = 8.0;  //Base temperature for juvenile leaf area growth (oC)
        _os->tmd = 42.0;  //Maximum temperature for development (oC)
        _os->tod = 30.0;  //Optimum temperature for development (oC)

        _os->dvrj = 0.000651; //Development rate in juvenile phase (oCd-1)
        _os->dvri = 0.000758; //Development rate in photoperiod-sensitive phase (oCd-1)
        _os->dvrp = 0.001093; //Development rate in panicle development (oCd-1)
        _os->dvrr = 0.002191; //Development rate in reproductive phase (oCd-1)

        _os->mopp = 11.50;    //Maximum optimum photoperiod (h)
        _os->ppse = 0.0;      //Photoperiod sensitivity (h-1)
        _os->shckd = 0.4;     //Relation between seedling age and delay in phenological development (oCd oCd-1)

        /* Leaf and stem growth parameters */
        _os->rgrlmx = 0.0085; //Maximum relative growth rate of leaf area (oCd-1)
        _os->rgrlmn = 0.0040; //Minimum relative growth rate of leaf area (oCd-1)
        _os->shckl = 0.25;    //Relation between seedling age and delay in leaf area development (oCd oCd-1)

        /* Switch to use SLA as table (give values below) or as fixed function */
        // Give function parameters  ASLA, BSLA, CSLA, DSLA, SLAMAX
        this->str_c2f( _os->swisla, SWISLA_SIZE, "FUNCTION");

        //SLA = ASLA + BSLA*EXP(CSLA*(DVS-DSLA)), and SLAMAX
        _os->asla = 0.0021;
        _os->bsla = 0.0025;
        _os->csla = -3.0;
        _os->dsla = 0.3;
        _os->slamax = 0.0060; //maximum value of SLA (ha/kg)

        /* If SWISLA='TABLE', supply table of specific leaf area (ha kg-1; Y value)
         * as a function of development stage (-; X value): */
        double const slatb_values[] =   {
            0.00, 0.006,
            0.24, 0.006,
            0.31, 0.004,
            0.58, 0.004,
            0.67, 0.003,
            1.13, 0.003,
            2.54, 0.002,
            2.50, 0.002};
        _os->slatb = OryzaTable("SLATB", slatb_values, 16);

        /* Table of specific green stem area (ha kg-1; Y value) as a function of
         * development stage (-; X value): */
        double const ssgatb_values[] =  {
            0.00, 0.0003,
            0.90, 0.0003,
            2.00, 0.0000,
            2.50, 0.0000};
        _os->ssgatb = OryzaTable("SSGATB", ssgatb_values, 8);

        /* 3. Photosynthesis parameters */
        _os->frpar  = 0.5;   //Fraction of photosynthetically active sunlight energy (-)
        _os->scp    = 0.2;   //Scattering coefficient of leaves for PAR (-)
        _os->co2ref = 340.0; //Reference level of atmospheric CO2 (ppm)
        _os->co2    = 340.0; //Ambient CO2 concentration (ppm)

        /* Table of light extinction coefficient for leaves (-; Y-value) as a function
         * of development stage (-; X value):*/
        double const kdftb_values[] =   {
            0.00, 0.4,
            0.65, 0.4,
            1.00, 0.6,
            2.50, 0.6};
        _os->kdftb = OryzaTable("KDFTB", kdftb_values, 8);

        /* Table of extinction coefficient of N profile in the canopy (-; Y-value)
         * as a functionof development stage (-; X value): */
        double const knftb_values[] =   {
            0.0, 0.4,
            2.5, 0.4};
        _os->knftb = OryzaTable("KNFTB", knftb_values, 4);

        /* Table of light use effiency (-; Y-value) as a function of
         * temperature (oC; X value): */
        double const efftb_values[] =   {
            10.0 ,0.54,
            40.0 ,0.36};
        _os->efftb = OryzaTable("EFFTB", efftb_values, 4);

        /* Table of effect of temperature on AMAX (-; Y-value) as a function of
         * temperature (oC; X value): */
        double const redftt_values[] = {
            -10.0, 0.0,
            10.0, 0.0,
            20.0, 1.0,
            37.0, 1.0,
            43.0, 0.0};
        _os->redftt = OryzaTable("REDFTT", redftt_values, 10);

        /* Table of N fraction in leaves on leaf area basis (g N m-2 leaf; Y-value)
         * as a function of development stage (-; X value): */
        double const nflvtb_values[] =  {
            0.00, 0.8395,
            0.14, 0.8395,
            0.18, 1.8860,
            0.29, 2.3805,
            0.38, 1.5295,
            0.65, 1.4260,
            0.89, 1.0695,
            1.00, 1.0580,
            1.50, 1.0925,
            2.12, 0.7130,
            2.50, 0.7130};
        _os->nflvtb = OryzaTable("NFLVTB", nflvtb_values, 22);

        /* Maintenance parameters */
        /* Maintenance respiration coefficient (kg CH2O kg-1 DM d-1) */
        _os->mainlv = 0.02;   //Leaves
        _os->mainst = 0.015;  //Stems
        _os->mainso = 0.003;  //Storage organs (panicles)
        _os->mainrt = 0.01;   //Roots

        _os->tref   = 25.0;   //Reference temperature (oC)
        _os->q10    = 2.0;    //Factor accounting for increase of maintenance respiration with a 10 oC rise in temperature (-)

        /* Growth respiration parameters */
        /* Carbohydrate requirement for dry matter production (kg CH2O kg-1 DM leaf) of: */
        _os->crglv  = 1.326;  //Leaves
        _os->crgst  = 1.326;  //Stems
        _os->crgso  = 1.462;  //Storage organs (panicles)
        _os->crgrt  = 1.326;  //Roots
        _os->crgstr = 1.11;   //Stem reserves

        _os->lrstr  = 0.947;  //Fraction of allocated stem reserves that is available for growth (-)

        /* Growth parameters */
        _os->fstr   = 0.20;       //Fraction of carbohydrates allocated to stems that is stored as reserves (-)
        _os->tclstr = 10.0;       //Time coefficient for loss of stem reserves (1 d-1)
        _os->spgf   = 64900.0;    //Spikelet growth factor (no kg-1)
        _os->wgrmx  = 0.0000249;  //Maximum individual grain weight (kg grain-1)

        /* Partitioning tables
         * Table of fraction total dry matter partitioned to the shoot (-; Y-value)
         * as a function of development stage (-; X value): */
        double const fshtb_values[] =   {
            0.00,  0.50,
            0.43,  0.75,
            1.00,  1.00,
            2.50,  1.00};
        _os->fshtb = OryzaTable("FSHTB", fshtb_values, 8);

        /* Table of fraction shoot dry matter partitioned to the leaves (-; Y-value)
         * as a function of development stage (-; X value): */
        double const flvtb_values[] =   {
            0.000, 0.50,
            0.350, 0.50,
            0.610, 0.30,
            0.720, 0.20,
            0.880, 0.06,
            1.230, 0.00,
            2.500, 0.00};
        _os->flvtb = OryzaTable("FLVTB", flvtb_values, 14);

        /* Table of fraction shoot dry matter partitioned to the stems (-; Y-value)
         * as a function of development stage (-; X value): */
        double const fsttb_values[] =   {
            0.000, 0.50,
            0.350, 0.50,
            0.610, 0.70,
            0.720, 0.80,
            0.880, 0.55,
            1.230, 0.00,
            2.500, 0.00};
        _os->fsttb = OryzaTable("FSTTB", fsttb_values, 14);

        /* Table of fraction shoot dry matter partitioned to the panicles (-; Y-value)
         * as a function of development stage (-; X value): */
        double const fsotb_values[] =   {
            0.000, 0.000,
            0.350, 0.000,
            0.610, 0.000,
            0.720, 0.000,
            0.880, 0.390,
            1.230, 1.000,
            2.500, 1.000};
        _os->fsotb = OryzaTable("FSOTB", fsotb_values, 12);

        /* Table of leaf death coefficient (d-1; Y-value) as a function of development
         * stage (-; X value): */
        double const drlvt_values[] =   {
            0.0,  0.00,
            0.9,  0.00,
            2.0,  0.11,
            2.5,  0.11};
        _os->drlvt = OryzaTable("DRLVT", drlvt_values, 8);

        /* 7. Carbon balance parameters */
        /* Mass fraction carbon (kg C kg-1 DM) in the: */
        _os->fclv   = 0.419;  //Leaves
        _os->fcst   = 0.431;  //Stems
        _os->fcso   = 0.487;  //Storage organs (panicles)
        _os->fcrt   = 0.431;  //Roots
        _os->fcstr  = 0.444;  //Stem reserves

        /* 8. Root parameters (Taken from IR72 data file!!!) */
        _os->gzrt   = 0.01;   //Growth rate of roots (m d-1)
        _os->zrtmcw = 0.25;   //Maximum depth of roots if no drought stress (m)
        _os->zrtmcd = 0.25;   //Maximum depth of roots if drought (m)

        /* 9. Drought stress parameters (Taken from IR72 data file!!!)
         * Upper and lower limits for drought stress effects*/
        _os->ulls = 74.31;        //Upper limit leaf rolling (kPa)
        _os->llls = 794.33;       //Lower limit leaf rolling (kPa)
        _os->uldl = 630.95;       //Upper limit death of leaves (kPa)
        _os->lldl = 1584.89;      //Lower limit death of leaves (kPa)
        _os->ulle = 1.45;         //Upper limit leaf expansion (kPa)
        _os->llle = 1404.0;       //Lower limit leaf expansion (kPa)
        _os->ulrt = 74.13;        //Upper limit relative transpiration reduction (kPa)
        _os->llrt = 1584.89;      //Lower limit relative transpiration reduction (kPa)

        /* 10. Nitrogen parameters */
        _os->nmaxup  = 8.0;       //Maximum daily N uptake (kg N ha-1 d-1)
        _os->rfnlv   = 0.004;     //Residual N fraction of leaves (kg N kg-1 leaves)
        _os->fntrt   = 0.15;      //Fraction N translocation from roots, as (additonal) fraction of total N translocation from stems and leaves (-)
        _os->rfnst   = 0.0015;    //Residual N fraction of stems (kg N kg-1 stems)
        _os->tcntrf  = 10.0;      //Time coefficient for N translocation to grains (d)
        _os->fnlvi   = 0.025;     //Initial leaf N fraction (on weight basis: kg N kg-1 leaf)

        _os->nmaxso = 0.0175;     //Maximum N concentration in storage organs (kg N kg-1)

        /* Table of minimum N concentration in storage organ (kg N kg-1 DM; Y value)
         * as a function of the amount of N in the crop till flowering (kg N ha-1; X value):
         */
        double const nminsottb_values[] = {
            0., .006,
            50., .0008,
            150., .0125,
            250., .015,
            400., .017,
            1000., .017};
        _os->nminsottb = OryzaTable("NMINSOT", nminsottb_values, 12);

        /* Table of maximum leaf N fraction on weight basis (kg N kg-1 leaves; Y value)
         * as a function of development stage (-; X value):
         */
        double const nmaxlttb_values[] = {
            0.0,  .053,
            0.4,  .053,
            0.75, .040,
            1.0,  .028,
            2.0,  .022,
            2.5,  .015};
        _os->nmaxlttb = OryzaTable("NMAXLT", nmaxlttb_values, 12);

        /* Table of minimum leaf N fraction on weight basis (kg N kg-1 leaves; Y value)
         * as a function of development stage (-; X value):
         */
        double const nminlttb_values[] = {
            0.0, 0.025,
            1.0, 0.012,
            2.1, 0.007,
            2.5, 0.007};
        _os->nminlttb = OryzaTable("NMINLT", nminlttb_values, 8);

        /*--- Table of effect of N stress on leaf death rate (-; Y value)
         * as a function of N stress level (-; X value):
         */
        double const nsllvttb_values[] = {
            0.,  1.0,
            1.1, 1.0,
            1.5, 1.4,
            2.0, 1.5,
            2.5, 1.5};
        _os->nsllvttb = OryzaTable("NSLLVT", nsllvttb_values, 10);
    }
    else
    {
        KLOGERROR( "species \"",_species_name,"\" cannot be handeled");
        return LDNDC_ERR_RUNTIME_ERROR;
    }
    
    return LDNDC_ERR_OK;
}




lerr_t
ldndc::LK_Oryza2000::str_c2f(
                             char * _target,
                             size_t _target_size,
                             char const * _source)
{
    size_t const  source_size = cbm::strlen( _source);

    if ( source_size > _target_size)
    {
        KLOGERROR( "target buffer too small  [|T|=",_target_size,",|S|=",source_size,"]");
        return  LDNDC_ERR_FAIL;
    }

    cbm::strncpy( _target, _source, _target_size);
    std::memset( _target+source_size, ' ', _target_size-source_size);

#ifdef  _DEBUG
    char save_chr = _target[_target_size-1];
    _target[_target_size-1] = '\0';
    LOGDEBUG( "fortran string: '", _target, "'");
    _target[_target_size-1] = save_chr;
#endif

    return  LDNDC_ERR_OK;
}
