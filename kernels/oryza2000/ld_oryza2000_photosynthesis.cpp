/*!
 * @file
 * @author
 *    steffen klatt
 *    david kraus (created on: june 30, 2016)
 */


#include  "oryza2000/ld_oryza2000.h"
#include  <scientific/meteo/ld_meteo.h>


lerr_t
ldndc::LK_Oryza2000::OryzaBerryBall()
{
    for (size_t fl = 0; fl < m_oryza->nb_foliagelayers; ++fl)
    {
        m_oryza->m_photo.vcAct25_fl[fl] = m_oryza->vcAct25_fl[fl];
        m_oryza->m_photo.jAct25_fl[fl] = m_oryza->jAct25_fl[fl];
        m_oryza->m_photo.rdAct25_fl[fl] = m_oryza->rdAct25_fl[fl];

        m_oryza->m_photo.fFol_fl[fl] = m_oryza->fFol_fl(fl);
        m_oryza->m_photo.sla_fl[fl] = m_oryza->sla_fl[fl];
        m_oryza->m_photo.lai_fl[fl] = m_oryza->lai_fl(fl);

        m_oryza->m_photo.vpd_fl[fl] = m_oryza->vpd_fl[fl];
        m_oryza->m_photo.rh_fl[fl] = m_oryza->rh_fl[fl];
        m_oryza->m_photo.temp_fl[fl] = m_oryza->temp_fl[fl];
        m_oryza->m_photo.parsun_fl[fl] = m_oryza->parsun_fl[fl];
        m_oryza->m_photo.parshd_fl[fl] = m_oryza->parshd_fl[fl];
        m_oryza->m_photo.tFol_fl[fl] = m_oryza->tFol_fl[fl];
        m_oryza->m_photo.co2_concentration_fl[fl] = m_oryza->co2_concentration_fl[fl];
        m_oryza->m_photo.sunlitfoliagefraction_fl[fl] = m_oryza->sunlitfoliagefraction_fl[fl];
    }

    m_oryza->m_photo.fl_cnt = m_oryza->nb_foliagelayers;
    m_oryza->m_photo.nd_airpressure = m_oryza->airpressure;

    m_oryza->m_photo.f_h2o = m_oryza->f_h2o;
    m_oryza->m_photo.f_fac = m_oryza->f_fac;
    m_oryza->m_photo.mFol = m_oryza->wlv * m_oryza->fclv / cbm::CCDM * cbm::HA_IN_M2;
    m_oryza->m_photo.height_max = m_oryza->height_max;

    m_oryza->m_photo.solve();

    m_oryza->ts_dtga = 0.0;
    for (size_t fl = 0; fl < m_oryza->nb_foliagelayers; ++fl)
    {
        m_oryza->ts_dtga += m_oryza->m_photo.carbonuptake_fl[fl] * cbm::M2_IN_HA * cbm::CO2_IN_C;
    }
    m_oryza->d_dtga += m_oryza->ts_dtga;
    
    return LDNDC_ERR_OK;
}



/*!
 * @brief
 *
 */
lerr_t
sastro(
       double _jday,
       double _dayl,
       double _latitude,
       double &_dsinbe,
       double &_sinld,
       double &_cosld)
{
    double  lat_r( ldndc::meteo::latitude_rad( _latitude));

    double  sdec( ldndc::meteo::sun_declination( _latitude, _jday));
    _sinld = sin( lat_r) * sin( sdec);
    _cosld = cos( lat_r) * cos( sdec);

    double const aob( _sinld / _cosld);
    double const zza( cbm::PI * (12.0 + _dayl) / 24.0);
    double zzcos( cos( zza));
    double zzsin( sin( zza));

    if ( aob < -1.0)
    {
        zzcos = 0.0;
        zzsin = 1.0;
    }
    else if ( aob > 1.0)
    {
        zzcos = 0.0;
        zzsin = -1.0;
    }

    _dsinbe = 2.0 * 3600.0 * (_dayl * (0.5 * _sinld + 0.2 * pow(_sinld, 2.0) + 0.1 * pow(_cosld, 2.0))
                              -(12.0 * _cosld * zzcos + 9.6 * _sinld * _cosld * zzcos
                                + 2.4 * pow(_cosld, 2.0) * zzcos * zzsin) / cbm::PI);

    return  LDNDC_ERR_OK;
}



/*!
 * @brief
 *
 */
lerr_t
ldndc::LK_Oryza2000::sgpcdt(
                            int _idoy,
                            double _lat,
                            double _rdd,
                            double _frpar,
                            double _cslv,
                            double _eff,
                            double _ecpdf,
                            double _gai,
                            double _dayl,
                            double &_gpcdt)
{
    /* assimilation set to zero and three different times of the day */
    _gpcdt = 0.0;

    double const solcon( cbm::AVG_SOLAR_CONSTANT * ldndc::meteo::eccentricity( _idoy, 365));

    double dsinbe( 0.0);
    double sinld( 0.0);
    double cosld( 0.0);
    sastro( _idoy, _dayl, _lat, dsinbe, sinld, cosld);


    for (size_t i = 0; i < IGSN; ++i)
    {
        /* at the specified hour, external radiation conditions are computed */
        double const hour( 12.0 + _dayl * 0.5 * GSX[i]);

        /* hour on day that solar height is at maximum */
        double const solhm( 12.0);

        /* sine of solar inclination, 0.2617993 is 15 degrees in radians */
        double const sinb( sinld + cosld * cos((hour - solhm) * 0.2617993));

        double rdpdr( 0.0);
        double rdpdf( 0.0);
        sskyc( sinb, solcon, _frpar, dsinbe, _rdd, rdpdr, rdpdf);


        /* assimilation rate of canopy is computed */

        double gpc( 0.0);
        sgpc1( _cslv, _eff, _ecpdf, _gai, sinb, rdpdr, rdpdf, gpc);

        _gpcdt  += gpc * GSW[i];
    }

    /* integration of assimilation rate to a daily total (gpcdt) */
    _gpcdt  *= _dayl;

    return  LDNDC_ERR_OK;
}








/*!
 * @brief
 *      this subroutine estimates solar inclination and fluxes of
 *      diffuse and direct irradiation at a particular time of
 *      the day.
 *
 * @author
 *      Daniel van Kraalingen (original version)
 */
lerr_t
ldndc::LK_Oryza2000::sskyc(
                           double _sinb /* sine of solar inclination at hour */,
                           double _solcon /* solar constant */,
                           double _frpar /* fraction of total shortwave irradiation taht is par */,
                           double _dsinbe /* integral of sine of solar height corrected for elevation */,
                           double _rdd /* daily shortwave radiation */,
                           double &_rdpdr /* instantaneous flux of direct par */,
                           double &_rdpdf /* instantaneous flux of diffuse par */)
{
    if ( _sinb > 0.0)
    {
        /* sun is above the horizon */
        double const tmpr1( _rdd * _sinb * (1.0 + 0.4 * _sinb) / _dsinbe);
        double const atmtr( tmpr1 / (_solcon * _sinb));

        double frdif( 0.0);
        if (atmtr <= 0.22)
        {
            frdif = 1.0;
        }
        else if ( (atmtr > 0.22) && (atmtr <= 0.35))
        {
            frdif = 1.0 - 6.4 * std::pow(atmtr - 0.22, 2.0);
        }
        else
        {
            frdif = 1.47 - 1.66 * atmtr;
        }

        /* apply lower limit to fraction diffuse */
        frdif = std::max( frdif, 0.15 + 0.85 * (1.0 - std::exp( -0.1 / _sinb)));

        /* diffuse and direct par */
        _rdpdf = tmpr1 * _frpar * frdif;
        _rdpdr = tmpr1 * _frpar * (1.0 - frdif);
    }
    else
    {
        _rdpdf = 0.0;
        _rdpdr = 0.0;
    }

    return  LDNDC_ERR_OK;
}



//!----------------------------------------------------------------------*
//! subroutine sgpl                                                      *
//! authors: daniel van kraalingen                                       *
//! date   : 15-sept-1994, version: 1.0                                  *
//! purpose: this subroutine calculates assimilation at a single depth   *
//!          in the canopy                                               *
//!                                                                      *
//! formal parameters:  (i=input,o=output,c=control,in=init,t=time)      *
//! name   type meaning                                     units  class *
//! ----   ---- -------                                     -----  ----- *
//! cslv    r4  scattering coefficient of leaves for visible   -      i  *
//!             radiation (par)                                          *
//! amax1   r4  assimilation rate at light saturation       kg co2/   i  *
//!                                                        ha leaf/h     *
//! eff1    r4  initial light use efficiency               kg co2/j/  i  *
//!                                                       (ha/h/m2/s)    *
//! ecpdf   r4  extinction coefficient for diffuse light              i  *
//! gai     r4  total green area                             ha/ha    i  *
//! gaid    r4  green area index above selected height       ha/ha    i  *
//! sinb    r4  sine of solar height                           -      i  *
//! rdpdr   r4  instantaneous flux of direct photo-          w/m2     o  *
//!             synthetically active radiation (par)                     *
//! rdpdf   r4  instantaneous flux of diffuse photo-         w/m2     o  *
//!             synthetically active irradiation (par)                   *
//! gpl     r4  instantaneous assimilation rate of          kg co2/   o  *
//!             leaves at depth gai                        ha leaf/h     *
//! rapl    r4  absorbed radiation at depth gai            w/m2 leaf  o  *
//!                                                                      *
//! fatal error checks: none                                             *
//! warnings          : none                                             *
//! subprograms called: srdprf                                           *
//! file usage        : none                                             *
//!----------------------------------------------------------------------*
/*!
 * @brief
 *      assimilation at a single depth in the canopy
 *
 * @author
 *      Daniel van Kraalingen (original version)
 */
lerr_t
ldndc::LK_Oryza2000::sgpl(
                          double _cslv,
                          double _eff1,
                          double _ecpdf,
                          double _gai,
                          double _gaid,
                          double _sinb,
                          double _rdpdr,
                          double _rdpdf,
                          double &_gpl)
{
    /* selection of depth of canopy, canopy assimilation is set to zero */
    double rapshl( 0.0);
    double rapppl( 0.0);
    double fslla( 0.0);
    srdprf( _gaid, _cslv, _sinb, _ecpdf, _rdpdr, _rdpdf, rapshl, rapppl, fslla);

    /* get photosynthesis parameters from user routine */
    double amax2( 0.0);
    double eff2( 0.0);
    gpparget( _gai, _gaid, _eff1, amax2, eff2);

    /* assimilation of shaded leaf area */
    double gpshl( 0.0);
    if ( amax2 > 0.0)
    {
        gpshl = amax2 * ( 1.0 - std::exp( -rapshl * eff2 / amax2));
    }


    /* assimilation of sunlit leaf area */
    double gpsll  = 0.0;
//    double rapsll = 0.0;
    for (size_t i = 0; i < IGSN; ++i)
    {
        double const tmpr1( rapshl + rapppl * GSX[i]);
        if ( amax2 > 0.0)
        {
            gpsll += amax2 * (1.0 - std::exp( -tmpr1 * eff2 / amax2)) * GSW[i];
        }

//        rapsll += tmpr1 * GSW[i];
    }

    /*
     * local assimilation rate (gpl) and rate of
     * absorption of par by canopy (rapl)
     */
    _gpl  = fslla * gpsll  + (1.0 - fslla) * gpshl;

    return  LDNDC_ERR_OK;
}



/*!
 * @brief
 *      this subroutine performs a gaussian integration over
 *      depth of canopy by selecting three different gai's and
 *      computing assimilation at these gai levels. the
 *      integrated variable is gpc. also absorbed par is
 *      integrated in rapc
 *
 * @author
 *      Daniel van Kraalingen (original version)
 */
lerr_t
ldndc::LK_Oryza2000::sgpc1(
                           double _cslv /* scattering coefficient of leaves for visible radiation */,
                           double _eff /* initial light use efficiency */,
                           double _ecpdf /* extinction coefficient for diffuse light */,
                           double _gai /* green area index */,
                           double _sinb /* sine of solar height */,
                           double _rdpdr /* instantaneous flux of direct par */,
                           double _rdpdf /* instantaneous flux of diffuse par */,
                           double &_gpc /* instantaneous assimilation rate of whole canopy */)
{
    /* selection of depth of canopy, canopy assimilation is set to zero */
    for (size_t i = 0; i < IGSN; ++i)
    {
        double gaid( _gai * GSX[i]);

        double gpl( 0.0);
        sgpl( _cslv, _eff, _ecpdf, _gai, gaid, _sinb, _rdpdr, _rdpdf, gpl);

        /*
         * integration of local assimilation rate to canopy
         * assimilation (gpc) and absorption of par by canopy (rapc)
         */
        _gpc  += gpl  * GSW[i];
    }

    _gpc  *= _gai;

    return  LDNDC_ERR_OK;
}




/*!
 * @brief
 *      this subroutine calculates the absorbed flux of radiation
 *      for shaded leaves, the direct flux absorbed by leaves and
 *      the fraction of sunlit leaf area.
 *
 * @author
 *      Daniel van Kraalingen (original version)
 */
lerr_t
ldndc::LK_Oryza2000::srdprf(
                            double _gaid /* green area index above selected height */,
                            double _cslv /* scattering coefficient of leaves for par */,
                            double _sinb /* sine of solar height */,
                            double _ecpdf /* extinction coefficient for diffuse light */,
                            double _rdpdr /* instantaneous flux of direct photosynth. active radiation (par) */ ,
                            double _rdpdf /* instantaneous flux of direct par */,
                            double &_rapshl /* absorbed flux for shaded leaves */,
                            double &_rapppl /* direct flux absorbed by leaves perpendicular on direct beam */,
                            double &_fslla /* fraction of leaf area that is sunlit */ )
{
    /* reflection of horizontal and spherical leaf angle distribution */
    double const tmpr1( sqrt(1.0 - _cslv));
    double const rflh( (1.0 - tmpr1) / (1.0 + tmpr1));
    double const rfls( rflh * 2.0 / (1.0 + 2.0 * _sinb));

    /* extinction coefficient for direct radiation and total direct flux */
    double clustf = _ecpdf / (0.8 * tmpr1);
    double ecpbl  = (0.5 / _sinb) * clustf;
    double ecptd  = ecpbl * tmpr1;

    /*
     * absorbed fluxes per unit leaf area: diffuse flux, total direct
     * flux, direct component of direct flux
     */
    double rapdfl = (1.0 - rflh) * _rdpdf * _ecpdf * std::exp(-_ecpdf * _gaid);
    double raptdl = (1.0 - rfls) * _rdpdr * ecptd * std::exp(-ecptd * _gaid);
    double rapddl = (1.0 - _cslv) * _rdpdr * ecpbl * std::exp(-ecpbl * _gaid);

    /* absorbed flux (j/m2 leaf/s) for shaded leaves */
    _rapshl = rapdfl + raptdl - rapddl;

    /* direct flux absorbed by leaves perpendicular on direct beam */
    _rapppl = (1.0 - _cslv) * _rdpdr / _sinb;

    /* fraction sunlit leaf area (fslla) */
    _fslla = clustf * std::exp(-ecpbl* _gaid);

    return  LDNDC_ERR_OK;
}



/*!
 * @brief
 *
 * @author
 *      Daniel van Kraalingen (original version)
 */
lerr_t
ldndc::LK_Oryza2000::gpparget(
                              double _xgai,
                              double _xgaid,
                              double _xeffin,
                              double &_xamaxout,
                              double &_xeffout)
{

    double amaxco2 = 49.57 / 34.26 * (1.0 - std::exp(-0.208 * (m_oryza->co2 - 60.0) / 49.57));
    amaxco2 = std::max(0.0, amaxco2);

    double slni( 0.0);
    if ( (_xgai > 0.01) && (m_oryza->knf > 0.0))
    {
        slni = m_oryza->nflv * _xgai * m_oryza->knf * std::exp( -m_oryza->knf * _xgaid) / (1.0 - std::exp(-m_oryza->knf * _xgai));
    }
    else
    {
        slni = m_oryza->nflv;
    }

    /*
     * calculate actual photosynthesis from sln, co2 and temperature
     * calculation of amax according to van keulen & seligman (1987):
     * amax = 32.4 * (slni-0.2) * redft * co2amx
     */

    double amax(0.0);
    if ( slni >= 0.5)
    {
        /* according to shaobing peng (irri, unpublished data): */
        amax = 9.5 + (22.0 * slni) * m_oryza->redft * amaxco2;
    }
    else
    {
        amax = std::max( 0.0, 68.33 * (slni-0.2) * m_oryza->redft * amaxco2);
    }

    _xamaxout = amax;
    _xeffout  = _xeffin;

    return  LDNDC_ERR_OK;
}


