/*!
 * @file
 *    steffen klatt
 *    david kraus (created on: june 30, 2016)
 */


#include  "oryza2000/ld_oryza2000.h"
#include  <input/climate/climate.h>



namespace ldndc
{
    const unsigned int  LK_Oryza2000::IGSN = 3;
    const double  LK_Oryza2000::GSX[LK_Oryza2000::IGSN] = { 0.112702, 0.500000, 0.887298 };
    const double  LK_Oryza2000::GSW[LK_Oryza2000::IGSN] = { 0.277778, 0.444444, 0.277778 };
}



LDNDC_KERNEL_OBJECT_DEFN(LK_Oryza2000,oryza2000,"Oryza2000","ORYZA2000")
ldndc::LK_Oryza2000::LK_Oryza2000()
        : cbm::kernel_t(),
          m_oryza( NULL),
          m_climate( NULL),
          m_output( NULL)
{}

ldndc::LK_Oryza2000::~LK_Oryza2000()
{}



lerr_t
ldndc::LK_Oryza2000::configure(
                               cbm::RunLevelArgs *  _r_level)
{
    this->m_output = Oryza2000Output( _r_level->iokcomm);

    lerr_t  rc_conf = this->m_output.configure( _r_level->cfg);
    if ( rc_conf)
    {
        return  LDNDC_ERR_FAIL;
    }
    return  LDNDC_ERR_OK;
}



lerr_t
ldndc::LK_Oryza2000::register_ports(
                                    cbm::RunLevelArgs * _r_level)
{
    KernelHandle = _r_level->iokcomm->publish_port< char >(
                                                           "Oryza2000.Kernel",
                                                           CBM_NoUnit,
                                                           NULL,
                                                           CBM_DynamicSize);

    maturity_status.publish( "MaturityStatus", "-", _r_level->iokcomm);
    
    if ( !CBM_HandleOk( KernelHandle))
    {
        return LDNDC_ERR_FAIL;
    }

    return LDNDC_ERR_OK;
}



lerr_t
ldndc::LK_Oryza2000::unregister_ports(
                                      cbm::RunLevelArgs * _r_level)
{
    _r_level->iokcomm->unpublish_port( KernelHandle);

    maturity_status.unpublish();
    
    return LDNDC_ERR_OK;
}



lerr_t
ldndc::LK_Oryza2000::initialize(
                                cbm::RunLevelArgs *  _r_level)
{
    ldndc_assert( _r_level->iokcomm);
    ldndc_assert( _r_level->clk);

    this->m_oryza = new Oryza2000State( _r_level->iokcomm,
                                        _r_level->clk->time_resolution(),
                                        40);

    this->m_subgrn = Oryza2000Subgrn();
    this->m_sublai3 = Oryza2000Sublai3();
    this->m_phenol = Oryza2000Phenol();
    this->m_ncrop = Oryza2000Ncrop();

    this->m_climate = _r_level->iokcomm->get_input_class< ldndc::climate::input_class_climate_t >();

    lerr_t  rc_ini = this->m_output.initialize();
    if ( rc_ini)
    {
        return  LDNDC_ERR_FAIL;
    }

    this->ncrop_potential = false;
    this->wcrop_potential = false;

    return  LDNDC_ERR_OK;
}



lerr_t
ldndc::LK_Oryza2000::finalize(
                              cbm::RunLevelArgs *  /* _r_level */)
{
    delete  this->m_oryza;

    this->m_output.finalize();

    return  LDNDC_ERR_OK;
}


