/*!
 * @brief
 *
 * @author:
 *      david kraus (created on: november 4, 2016),

 */

#include  "oryza2000/ld_oryza2000.h"

#include  "oryza2000/ld_oryza2000_state.h"
#include  "oryza2000/ld_oryza2000_phenol.h"

#include  <math/cbm_math.h>


ldndc::Oryza2000Phenol::~Oryza2000Phenol()
{
}


ldndc::Oryza2000Phenol::Oryza2000Phenol()
{
    this->tstr = 0.0;
}



void
ldndc::Oryza2000Phenol::reset()
{
    this->tstr = 0.0;
}



void
ldndc::Oryza2000Phenol::run( Oryza2000State *_os, double _dayl)
{
    if ( cbm::flt_greater_equal_zero( _os->dvs) && cbm::flt_less( _os->dvs, 0.4))
    {
        _os->dvr = _os->dvrj * _os->hu;
    }
    else if ( cbm::flt_greater_equal( _os->dvs, 0.4) && cbm::flt_less( _os->dvs, 0.65))
    {
        double const dl( _dayl + 0.9);
        double ppfac( (dl < _os->mopp) ? 1.0 : (1.0 - (dl - _os->mopp) * _os->ppse));

        ppfac = cbm::bound(0.0, ppfac, 1.0);
        _os->dvr   = _os->dvri * _os->hu * ppfac;

    }
    else if ( cbm::flt_greater_equal( _os->dvs, 0.65) && cbm::flt_less( _os->dvs, 1.0))
    {
        _os->dvr = _os->dvrp * _os->hu;
    }
    else if ( cbm::flt_greater_equal( _os->dvs, 1.0))
    {
        _os->dvr = _os->dvrr * _os->hu;
    }

    if ( _os->cropsta == 3)
    {
        this->tstr = _os->ts;
    }
    else if ( _os->cropsta < 3)
    {
        this->tstr = 0.0;
    }

    _os->tshckd = _os->shckd * this->tstr;

    if (   (_os->cropsta > 3)
        && cbm::flt_less( _os->ts, (this->tstr + _os->tshckd)))
    {
        _os->dvr = 0.0;
    }
}



/*!
 * @brief
 *      This subroutine calculates the daily amount of heat units
 *      for calculation of the phenological development rate and
 *      early leaf area growth
 *
 */
lerr_t
ldndc::LK_Oryza2000::subdd(
                           double _t_opt,
                           double _t_max,
                           double _t_min,
                           double &_hu)
{
    double const tm( (m_oryza->tmax + m_oryza->tmin) / 2.0);

    _hu = 0.0;
    for (int hr = 1; hr <= 24; ++hr)
    {
        double td( tm + 0.5 * (m_oryza->tmax - m_oryza->tmin) * cos( 0.2618 * (double)(hr - 14)));
        if (   (td > _t_min)
            && (td < _t_max))
        {
            if ( td > _t_opt)
            {
                td = _t_opt - (td - _t_opt) * (_t_opt - _t_min) / (_t_max - _t_opt);
            }
            _hu += (td - _t_min) / 24.0;
        }
    }

    return  LDNDC_ERR_OK;
}



lerr_t
ldndc::LK_Oryza2000::subcd2()
{
    if ( m_oryza->cropsta == 3)
    {
        m_oryza->ncold = 0.0;
    }

    if ( m_oryza->tav < m_oryza->coldmin)
    {
        m_oryza->ncold += 1;
    }
    else
    {
        m_oryza->ncold = 0;
    }

    return  LDNDC_ERR_OK;
}

