/*!
 * @file
 *
 * @author:
 *      david kraus (created on: october 5, 2016),
 *      steffen klatt
 */

#ifndef ORYZA2000_SUBLAI3_H_
#define ORYZA2000_SUBLAI3_H_

namespace ldndc {

class Oryza2000State;
class Oryza2000Sublai3
{

public:
    Oryza2000Sublai3();
    ~Oryza2000Sublai3();

    void run( Oryza2000State *);

private:

    double x;
    bool testl;
    double test;
    double testset;

    double wlvgexp;
    double wlvgexs;

    double laiexp;
    double laiexs;

    double glai1;
    double glai2;

    double tslvtr;
    double tshckl;

    double dvse;
};

} /* namespace ldndc */


#endif /* !ORYZA2000_SUBLAI3_H_ */

