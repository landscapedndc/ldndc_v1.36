/*!
 * @brief
 *
 * @author:
 *      david kraus (created on: october 5, 2016),
 *      steffen klatt
 */


#include  "oryza2000/ld_oryza2000_table.h"
#include  <string/cbm_string.h>

ldndc::OryzaTable::OryzaTable()
{
    this->m_name = NULL;
}


ldndc::OryzaTable::OryzaTable(
                       char const *_name,
                       double const *_nodes,
                       int _length)
{
    this->m_name = cbm::strdup( _name);
    this->m_nodes = std::vector<double>( _nodes, _nodes+_length);
}

ldndc::OryzaTable::OryzaTable( ldndc::OryzaTable const &  _rhs)
    : m_name( NULL), m_nodes( _rhs.m_nodes)
{
    if ( _rhs.m_name)
    {
        this->m_name = cbm::strdup( _rhs.m_name);
    }
}

ldndc::OryzaTable &
ldndc::OryzaTable::operator=( ldndc::OryzaTable const &  _rhs)
{
    if ( this != &_rhs)
    {
        this->m_name = NULL;
        if ( _rhs.m_name)
        {
            this->m_name = cbm::strdup( _rhs.m_name);
        }
        this->m_nodes = _rhs.m_nodes;
    }
    return  *this;
}

ldndc::OryzaTable::~OryzaTable()
{
    if ( this->m_name)
    {
        cbm::strfree( this->m_name);
    }
}


double
ldndc::OryzaTable::linear_interpolate( double _x)
{
    int len( static_cast<int>( this->m_nodes.size()));

    int iup( 2);
    if ( _x < this->m_nodes[0])
    {
        iup = 2;
    }
    else if ( _x > this->m_nodes[len-2])
    {
        iup = len-2;
    }
    else
    {
        for ( int i = 2; i < len; i=i+2)
        {
            if ( _x < this->m_nodes[i])
            {
                break;
            }
            else
            {
                iup = i+2;
            }
        }
    }

    double slope( (this->m_nodes[iup+1] - this->m_nodes[iup-1]) / (this->m_nodes[iup] - this->m_nodes[iup-2]));
    double result( this->m_nodes[iup-1] + (_x - this->m_nodes[iup-2]) * slope);

    return result;
}

