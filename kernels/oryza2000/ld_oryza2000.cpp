/*!
 * @file
 * @author
 *  - Steffen Klatt
 *  - David Kraus (created on: june 30, 2016)
 */


#include  "oryza2000/ld_oryza2000.h"

#include  <input/climate/climate.h>
#include  <scientific/meteo/ld_meteo.h>
#include  <input/setup/setup.h>


/*!
 * @page ldndc_oryza2000
 * @section ldndc_oryza2000_guide User guide
 * ...
 */
lerr_t
ldndc::LK_Oryza2000::read(
                          cbm::RunLevelArgs * _r_level)
{
    /* Daily reset */
    if ( _r_level->clk->subday() == 1)
    {
        lerr_t  rc_reset = OryzaReset();
        if ( rc_reset){ return  LDNDC_ERR_FAIL;}
    }

    return LDNDC_ERR_OK;
}



lerr_t
ldndc::LK_Oryza2000::solve(
                           cbm::RunLevelArgs * _r_level)
{
    if ( m_oryza->cropsta > 0)
    {
        /* OryzaBerryBall runs subdaily */
        if ( farquhar)
        {
            lerr_t  rc_berryball = OryzaBerryBall();
            if ( rc_berryball){ return  LDNDC_ERR_FAIL;}
        }

        /* Oryza2000 runs daily */
        if ( (int)_r_level->clk->subday() == (int)_r_level->clk->time_resolution())
        {
            lerr_t  rc_cropstage = this->OryzaUpdateCropstage();
            if ( rc_cropstage){ return  LDNDC_ERR_FAIL;}

            lerr_t  rc_oryza_rate = this->OryzaRateCalculation( _r_level);
            if ( rc_oryza_rate){ return  LDNDC_ERR_FAIL;}

            lerr_t  rc_ncrop_rate = this->m_ncrop.NcropRateCalculation( this->ncrop_potential, this->m_oryza);
            if ( rc_ncrop_rate){ return  LDNDC_ERR_FAIL;}
        }
    }

    return  LDNDC_ERR_OK;
}



lerr_t
ldndc::LK_Oryza2000::integrate(
                               cbm::RunLevelArgs * _r_level)
{
    if ( m_oryza->cropsta > 0)
    {
        /* Oryza2000 runs daily */
        if ( (int)_r_level->clk->subday() == (int)_r_level->clk->time_resolution())
        {
            lerr_t  rc_oryza_integrate = this->OryzaIntegrate();
            if ( rc_oryza_integrate){ return  LDNDC_ERR_FAIL;}

            lerr_t  rc_ncrop_integrate = this->m_ncrop.NcropIntegrate( this->ncrop_potential, this->m_oryza);
            if ( rc_ncrop_integrate){ return  LDNDC_ERR_FAIL;}
        }

        /* OryzaHydrology runs subdaily */
        lerr_t  rc_transpiration = this->OryzaHydrology( _r_level);
        if ( rc_transpiration){ return  LDNDC_ERR_FAIL;}
    }
    return  LDNDC_ERR_OK;
}



lerr_t
ldndc::LK_Oryza2000::write(
                           cbm::RunLevelArgs * _r_level)
{
    if ( m_oryza->cropsta > 0)
    {
        /* Oryza2000 runs daily */
        if ( (int)_r_level->clk->subday() == (int)_r_level->clk->time_resolution())
        {
            lerr_t  rc_out = this->m_output.step(this->m_oryza, &this->m_ncrop, LD_RtCfg.clk);
            if ( rc_out){ return  LDNDC_ERR_FAIL;}
        }
    }
    return  LDNDC_ERR_OK;
}



lerr_t
ldndc::LK_Oryza2000::OryzaReset()
{
    m_oryza->d_dtga = 0.0;
    m_ncrop.nacr = 0.0;

    return LDNDC_ERR_OK;
}



/*!
 * run method for all selected modules including output and helper modules
 */
lerr_t
ldndc::LK_Oryza2000::OryzaRateCalculation(
                                          cbm::RunLevelArgs *  _r_level)
{
    lerr_t rc_entry = m_oryza->Oryza_check_for_negative_value( object_id(), "ORYZA2000 rate entry");
    if ( rc_entry) return LDNDC_ERR_FAIL;

    cbm::sclock_t const &  clk( *LD_RtCfg.clk);
    m_oryza->tmax = m_climate->temp_max_day( clk);
    m_oryza->tmin = m_climate->temp_min_day( clk);
    m_oryza->tav = m_climate->temp_avg_day( clk);
    double const tavd( 0.5 * (m_oryza->tmax + m_oryza->tav));

    m_oryza->time = (double)clk.yearday();
    m_oryza->idoy = (double)clk.yearday();

    double const latitude( _r_level->iokcomm->get_input_class_ref< ldndc::setup::input_class_setup_t >().latitude());
    double const daylength( meteo::daylength( latitude, clk.yearday()));

    m_oryza->dtr = (m_climate->ex_rad_day( clk) * cbm::CM2_IN_M2 * cbm::JDCM_IN_WM);

    /*
     * Skip all rate calculations before emergence
     * and after maturity (dvs == 2.0)
     */
    if ( (m_oryza->cropsta > 0)
        && cbm::flt_less_equal(m_oryza->dvs, 2.0))
    {
        /*
         * Reinitialization at transplanting
         */
        OryzaTransplanting();


        /*
         * Drought stress
         */
        OryzaDrout();

        this->subdd( m_oryza->tod, m_oryza->tmd, m_oryza->tbd, m_oryza->hu);

        this->subcd2();

        this->m_phenol.run( m_oryza, daylength);


        /*
         * CO2 concentration
         */
        double const co2eff(  (1.0 - exp(-0.00305 * m_oryza->co2    - 0.222))
                           / (1.0 - exp(-0.00305 * m_oryza->co2ref - 0.222)));
        double eff( m_oryza->efftb.at( tavd) * co2eff);


        /*
         * Leaf rolling under drought stress (only for photosynthesis)
         */
        double const lairol( m_oryza->lai * ((0.5 * m_oryza->lrstrs) + 0.5));


        /*
         * Add specific stem area to leaf area
         */
        double const ssga( m_oryza->ssgatb.at( m_oryza->dvs));
        double const sai( ssga * m_oryza->wst);
        double alai( lairol + (0.5 * sai));


        /*
         * Intercepted solar radiation
         */
        double kdf( m_oryza->kdftb.at( m_oryza->dvs));
        m_oryza->redft = m_oryza->redftt.at( tavd);
        m_oryza->knf = m_oryza->knftb.at( m_oryza->dvs);

        /*
         * Daily gross canopy CO2 assimilation
         */
        if ( (!farquhar) ||    /* if not farquahr is used */
             (m_oryza->cropsta < 4) || /* if crop has not yet been transplanted */
             !cbm::flt_greater_zero(m_oryza->m_photo.mFol) /* if no foliage has yet been established */)
        {
            sgpcdt(
                   m_oryza->idoy,
                   latitude,
                   m_oryza->dtr,
                   m_oryza->frpar,
                   m_oryza->scp,
                   eff,
                   kdf,
                   alai,
                   daylength,
                   m_oryza->d_dtga);

            /* was so far not included */
            m_oryza->d_dtga += m_oryza->c_root_exsudation * cbm::CO2_IN_C;
        }

        m_oryza->dpar  = m_oryza->frpar * m_oryza->dtr * 1.0e-6;

        /* Unrolling of lai again */
        m_oryza->alai  = m_oryza->lai + (0.5 * m_oryza->sai);
        m_oryza->d_dtga *= m_oryza->pcew;
        
        /* Relative growth rates of shoots and roots */
        m_oryza->fsh = m_oryza->fshtb.at( m_oryza->dvs);

        /* Effect of drought stress on shoot-root partitioning */
        if( m_oryza->dvs < 1.0)
        {
            m_oryza->fsh = (m_oryza->fsh * m_oryza->cpew) / (1.0 + (m_oryza->cpew - 1.0) * m_oryza->fsh);
        }
        m_oryza->frt = 1.0 - m_oryza->fsh;


        /* Relative growth rates of shoot organs */
        m_oryza->flv = m_oryza->flvtb.at( m_oryza->dvs);
        m_oryza->fst = m_oryza->fsttb.at( m_oryza->dvs);
        m_oryza->fso = m_oryza->fsotb.at( m_oryza->dvs);


        /* Check sink limitation based on yesterday's growth rates
         and adapt partitioning stem-storage organ accordingly */
        if ( m_oryza->grains)
        {
            if ( (m_oryza->ggr >= (m_oryza->pwrr - m_oryza->wrr)) &&
                cbm::flt_greater_zero((m_oryza->gcr * m_oryza->fsh)))
            {
                m_oryza->fso = std::max( 0.0, (m_oryza->pwrr - m_oryza->wrr)/(m_oryza->gcr * m_oryza->fsh));
                m_oryza->fst = 1.0 - m_oryza->fso - m_oryza->flv;
            }
        }


        /* Loss rates of green leaves and stem reserves */
        m_oryza->llv = m_oryza->nsllv * m_oryza->wlvg * m_oryza->drlvt.at( m_oryza->dvs);
        m_oryza->lstr = (m_oryza->dvs > 1.0) ? (m_oryza->wstr / m_oryza->tclstr) : 0.0;


        /* Maintenance requirements */
        double const teff( pow(m_oryza->q10, ((m_oryza->tav - m_oryza->tref) / 10.0)));
        double const mndvs( ((m_oryza->wlvg + m_oryza->wlvd) > 0.0) ? m_oryza->wlvg / (m_oryza->wlvg + m_oryza->wlvd) : 0.0);
        m_oryza->rmcr_rt = (m_oryza->wrt * m_oryza->mainrt)  * teff * mndvs;
        m_oryza->rmcr_lv = (m_oryza->wlvg * m_oryza->mainlv) * teff * mndvs;
        m_oryza->rmcr_so = (m_oryza->wso * m_oryza->mainso)  * teff * mndvs;
        m_oryza->rmcr_st = (m_oryza->wst * m_oryza->mainst)  * teff * mndvs;        


        /* Carbohydrate requirement for dry matter production (growth respiration) */
        double const crgcr(  (m_oryza->fsh * (m_oryza->crglv * m_oryza->flv
                                              + m_oryza->crgst * m_oryza->fst * (1.0 - m_oryza->fstr)
                                              + m_oryza->crgstr * m_oryza->fstr * m_oryza->fst
                                              + m_oryza->crgso * m_oryza->fso))
                           + (m_oryza->frt * m_oryza->crgrt));


        /* Gross and net growth rate of crop (gcr, ngcr) */
        if ( cbm::flt_greater_zero(crgcr))
        {
            m_oryza->gcr = ( ((m_oryza->d_dtga * 30.0/44.0)
                            - m_oryza->rmcr()
                            + (m_oryza->lstr * m_oryza->lrstr * m_oryza->fcstr * 30.0/12.0))
                          / crgcr);
        }
        else
        {
            m_oryza->gcr = 0.0;
        }


        m_oryza->ngcr = std::max(0.0, m_oryza->gcr - m_oryza->lstr * m_oryza->lrstr * m_oryza->fcstr * 30.0/12.0);

        /* Set transplanting effect */
        double rwlvg1( 0.0); //reduction in leaf weight at transplanting (kg ha-1)
        double gst1( 0.0);
        double rwstr1( 0.0);
        double grt1( 0.0);
        if ( m_oryza->cropsta == 3)
        {
            m_oryza->pltr = (m_oryza->plant_count / m_oryza->nplsb);

            /* Growth rates of crop organs at transplanting */
            rwlvg1 = m_oryza->wlvg * (1.0 - m_oryza->pltr);
            gst1   = m_oryza->wsts * (1.0 - m_oryza->pltr);
            rwstr1 = m_oryza->wstr * (1.0 - m_oryza->pltr);
            grt1   = m_oryza->wrt  * (1.0 - m_oryza->pltr);
        }
        else
        {
            m_oryza->pltr = 1.0;
        }

        /* Growth rates of crop organs */
        if ( cbm::flt_greater_zero( m_oryza->gcr))
        {
            m_oryza->grt    = m_oryza->gcr * m_oryza->frt - grt1;
            m_oryza->glv    = m_oryza->gcr * m_oryza->fsh * m_oryza->flv - rwlvg1;
            m_oryza->gst    = m_oryza->gcr * m_oryza->fsh * m_oryza->fst * (1.0 - m_oryza->fstr) - gst1;
            m_oryza->gstr   = m_oryza->gcr * m_oryza->fsh * m_oryza->fst * m_oryza->fstr - rwstr1;
            m_oryza->gso    = m_oryza->gcr * m_oryza->fsh * m_oryza->fso;
        }
        else
        {
            double const wtot( m_oryza->wrt + m_oryza->wlvg + m_oryza->wlvd + m_oryza->wsts + m_oryza->wstr + m_oryza->wso);
            m_oryza->grt    = m_oryza->gcr * m_oryza->wrt / wtot - grt1;
            m_oryza->glv    = m_oryza->gcr * (m_oryza->wlvg + m_oryza->wlvd) / wtot - rwlvg1;
            m_oryza->gst    = m_oryza->gcr * m_oryza->wsts / wtot - gst1;
            m_oryza->gstr   = m_oryza->gcr * m_oryza->wstr / wtot - rwstr1;
            m_oryza->gso    = m_oryza->gcr * m_oryza->wso / wtot;
        }

        m_oryza->rwlvg  = m_oryza->glv - m_oryza->llv;
        m_oryza->rwstr  = m_oryza->gstr - m_oryza->lstr;


        if( m_oryza->dvs > 0.95)
        {
            m_oryza->ggr = m_oryza->gso;
        }
        else
        {
            m_oryza->ggr = 0.0;
        }

        /*
         * Growth rate of number of spikelets and grains
         */
        this->m_subgrn.run( m_oryza);

        double const tod( 30.0);
        double const tmd( 42.0);
        this->subdd( tod, tmd, m_oryza->tblv, m_oryza->hulv);


        /* Specific leaf area */
        if ( strncmp( m_oryza->swisla, "TABLE", 5) == 0)
        {
            m_oryza->sla = m_oryza->slatb.at( m_oryza->dvs);
        }
        else
        {
            m_oryza->sla = m_oryza->asla + m_oryza->bsla * exp( m_oryza->csla * (m_oryza->dvs - m_oryza->dsla));
            m_oryza->sla = std::min(m_oryza->slamax, m_oryza->sla);
        }

        /*  New Leaf area index growth */
        this->m_sublai3.run( m_oryza);

        /*
         * Leaf death as caused by drought stress
         */
        m_oryza->dldr = 0.0;
        if ( cbm::flt_equal(m_oryza->ldstrs, 1.0))
        {
            m_oryza->dleaf = false;
            m_oryza->dldrt = 0.0;
        }
        else if ( (m_oryza->ldstrs < 1.0) && (!m_oryza->dleaf))
        {
            m_oryza->dleaf  = true;
            m_oryza->wlvgit = m_oryza->wlvg;
            m_oryza->keep = m_oryza->ldstrs;
        }

        if( m_oryza->dleaf && cbm::flt_less_equal(m_oryza->ldstrs, m_oryza->keep))
        {
            m_oryza->dldr  = std::max(m_oryza->wlvgit * (1.0 - m_oryza->ldstrs) - m_oryza->dldrt, 0.0);
            m_oryza->dldrt += m_oryza->dldr;
            m_oryza->keep  = m_oryza->ldstrs;
        }


        /*
         * Growth respiration of the crop (RGCR)
         */
        m_oryza->co2rt  = 44.0/12.0 * (m_oryza->crgrt * 12.0/30.0 - m_oryza->fcrt );
        m_oryza->co2lv  = 44.0/12.0 * (m_oryza->crglv * 12.0/30.0 - m_oryza->fclv );
        m_oryza->co2st  = 44.0/12.0 * (m_oryza->crgst * 12.0/30.0 - m_oryza->fcst );
        m_oryza->co2str = 44.0/12.0 * (m_oryza->crgstr* 12.0/30.0 - m_oryza->fcstr);
        m_oryza->co2so  = 44.0/12.0 * (m_oryza->crgso * 12.0/30.0 - m_oryza->fcso );

        m_oryza->rgcr_rt = ((m_oryza->grt + grt1) * m_oryza->co2rt);
        m_oryza->rgcr = (   m_oryza->rgcr_rt
                         + (m_oryza->glv + rwlvg1) * m_oryza->co2lv
                         + (m_oryza->gst + gst1) * m_oryza->co2st
                         +  m_oryza->gso * m_oryza->co2so
                         + (m_oryza->gstr + rwstr1) * m_oryza->co2str
                         + (1.0 - m_oryza->lrstr) * m_oryza->lstr * m_oryza->fcstr * 44.0/12.0);

        /* carbon losses at transplanting */
        m_oryza->ctrans = (rwlvg1 * m_oryza->fclv +
                         gst1 * m_oryza->fcst +
                         rwstr1 * m_oryza->fcstr +
                         grt1 * m_oryza->fcrt);

        /* net rate of total co2 assimilation */
        m_oryza->rtnass = (((m_oryza->d_dtga * 30.0/44.0 - m_oryza->rmcr()) * 44.0/30.0)
                         - m_oryza->rgcr
                         - (m_oryza->ctrans * 44.0/12.0));

        /* fine root turnover and root exsudation */
        if ( root_turnover)
        {
            m_oryza->c_root_turnover = m_oryza->TOFRTBAS * m_oryza->dvs / 2.0 * m_oryza->wrt * m_oryza->fcrt;
            m_oryza->c_root_exsudation = m_oryza->DOC_RESP_RATIO * (m_oryza->rgcr_rt + m_oryza->rmcr_rt) * cbm::C_IN_CO2;
        }
        else
        {
            m_oryza->c_root_turnover = 0.0;
            m_oryza->c_root_exsudation = 0.0;
        }

        m_oryza->ts_dtga = m_oryza->d_dtga / _r_level->clk->time_resolution();
    }

    lerr_t rc_out = m_oryza->Oryza_check_for_negative_value( object_id(), "ORYZA2000 rate out");
    if ( rc_out) return LDNDC_ERR_FAIL;

    return  LDNDC_ERR_OK;
}



/*!
 * @brief
 *
 */
lerr_t
ldndc::LK_Oryza2000::OryzaIntegrate()
{
    if ( cbm::flt_less_equal( m_oryza->dvs, 2.0))
    {
        lerr_t rc_entry = m_oryza->Oryza_check_for_negative_value( object_id(),"ORYZA2000 integrate entry");
        if ( rc_entry) return LDNDC_ERR_FAIL;
        
        m_oryza->wlvg += m_oryza->rwlvg - m_oryza->dldr;
        m_oryza->wlvd += m_oryza->llv + m_oryza->dldr;
        m_oryza->ts += m_oryza->hu;
        m_oryza->tslv += m_oryza->hulv;
        m_oryza->dvs = cbm::bound_max( m_oryza->dvs + m_oryza->dvr, 2.0);
        m_oryza->wsts += m_oryza->gst;
        m_oryza->wstr += m_oryza->rwstr;
        m_oryza->wso += m_oryza->gso;
        m_oryza->wrt = cbm::bound_min(0.0, m_oryza->wrt + m_oryza->grt - (m_oryza->c_root_turnover + m_oryza->c_root_exsudation) / m_oryza->fcrt);

        m_oryza->wrr += m_oryza->ggr;
        m_oryza->ngr = cbm::bound_min( 0.0, m_oryza->ngr + m_oryza->gngr);
        m_oryza->nsp = cbm::bound_min( 0.0, m_oryza->nsp + m_oryza->gnsp);
        m_oryza->dae += 1;
        m_oryza->tnass += m_oryza->rtnass;

        /* Calculate sums of states */
        m_oryza->wst    = m_oryza->wsts + m_oryza->wstr;
        m_oryza->wlv    = m_oryza->wlvg + m_oryza->wlvd;
        m_oryza->wag    = m_oryza->wlvg + m_oryza->wst  + m_oryza->wso;
        m_oryza->wagt   = m_oryza->wlv  + m_oryza->wst  + m_oryza->wso;
        m_oryza->tdrw   = m_oryza->wlv  + m_oryza->wst  + m_oryza->wso + m_oryza->wrt;

        m_oryza->pwrr   = m_oryza->ngr * m_oryza->wgrmx;
        m_oryza->ngrm2  = m_oryza->ngr / 10000.0;
        m_oryza->nspm2  = m_oryza->nsp / 10000.0;

        /* Leaf area index and total area index (leaves + stems) */
        m_oryza->lai = cbm::bound_min(0.0, m_oryza->lai + m_oryza->glai);
        m_oryza->alai = m_oryza->lai + 0.5 * m_oryza->sai;

        /* Root length */
        if ( (! m_oryza->drout) && cbm::flt_greater_equal(m_oryza->zrtmcw, m_oryza->zrt))
        {
            m_oryza->zrtm = std::min( m_oryza->zrtmcw, std::min(m_oryza->zrtms, m_oryza->tklt));
        }
        else if ( (! m_oryza->drout) && cbm::flt_greater(m_oryza->zrt, m_oryza->zrtmcw))
        {
            m_oryza->zrtm = std::min( m_oryza->zrt, std::min(m_oryza->zrtms, m_oryza->tklt));
        }
        else if ( m_oryza->drout)
        {
            m_oryza->zrtm = std::min(m_oryza->zrtmcd, std::min(m_oryza->zrtms, m_oryza->tklt));
        }

        m_oryza->zrt += m_oryza->gzrt;
        m_oryza->zrt = std::min( m_oryza->zrt, m_oryza->zrtm);

        m_oryza->height_max = std::min(m_oryza->dvs, 1.0);
        lerr_t rc_out = m_oryza->Oryza_check_for_negative_value( object_id(), "ORYZA2000 integrate out");
        if ( rc_out) return LDNDC_ERR_FAIL;
    }

    maturity_status.send( m_oryza->dvs / 2.0);
    
    return  LDNDC_ERR_OK;
}



/*!
 * @brief
 *
 */
lerr_t
ldndc::LK_Oryza2000::OryzaUpdateCropstage()
{
    //transplanting
    if ( m_oryza->cropsta == 3)
    {
        m_oryza->cropsta = 4;
    }

    //in seedbed
    if ( m_oryza->cropsta == 2)
    {
        if ( m_oryza->dae == m_oryza->sbdur)
        {
            m_oryza->cropsta = 3;
        }
    }

    if ( m_oryza->cropsta == 1)
    {
        if ( m_oryza->sbdur > 0)
        {
            m_oryza->cropsta = 2;
        }
        else
        {
            m_oryza->cropsta = 4;
        }
    }

    return  LDNDC_ERR_OK;
}



