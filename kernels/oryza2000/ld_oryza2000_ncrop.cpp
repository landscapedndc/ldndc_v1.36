/*!
 * @file
 *
 * @author:
 *      david kraus (created on: november 4, 2016),
 *
 */

#include  "oryza2000/ld_oryza2000_state.h"
#include  "oryza2000/ld_oryza2000_ncrop.h"

#include  <math/cbm_math.h>



ldndc::Oryza2000Ncrop::Oryza2000Ncrop()
{}



ldndc::Oryza2000Ncrop::~Oryza2000Ncrop()
{}



lerr_t
ldndc::Oryza2000Ncrop::NcropReset()
{
    this->anrt = 0.0;
    this->anlv = 0.0;
    this->anlva = 0.0;
    this->anld = 0.0;
    this->anso = 0.0;
    this->anst = 0.0;
    this->ansta = 0.0;

    this->atnlv = 0.0;
    this->atnst = 0.0;
    this->atnrt = 0.0;

    this->ntso = 0.0;
    this->nacr = 0.0;

    this->fnrt = 0.0;
    this->fnlv = 0.0;
    this->fnst = 0.0;
    this->fnso = 0.0;

    this->nrt = 0.0;
    this->nlv = 0.0;
    this->nst = 0.0;
    this->nso = 0.0;
    this->nldlv = 0.0;

    this->nlvan = 0.0;
    this->nstan = 0.0;

    this->nart = 0.0;
    this->nalv = 0.0;
    this->nast = 0.0;
    this->naso = 0.0;

    this->ntrt = 0.0;
    this->ntlv = 0.0;
    this->ntst = 0.0;

    this->nmaxl = 0.0;
    this->nminl = 0.0;

    this->nflvp = 0.0;

    this->ndeml = 0.0;
    this->nupp = 0.0;
    this->ndemc = 0.0;

    this->soiln = 0.0;

    return  LDNDC_ERR_OK;
}



lerr_t
ldndc::Oryza2000Ncrop::NcropInitialize(
                                ldndc::Oryza2000State *_os)
{
    /* reset all members */
    this->NcropReset();

    /* Initialize variables from oryza state */
    this->fnlv = _os->fnlvi;
    this->fnst = (0.5 * _os->fnlvi);

    _os->nsllv = 1.0;
    _os->rnstrs = 1.0;

    this->nmaxl = _os->nmaxlttb.at( _os->dvs);

    return  LDNDC_ERR_OK;
}



lerr_t
ldndc::Oryza2000Ncrop::NcropRateCalculation(
                                     bool _potential,
                                     ldndc::Oryza2000State *_os)
{
    if ( _potential)
    {
        if ( _os->cropsta < 4)
        {
            _os->nflv = _os->nflvi;
        }
        else
        {
            this->nmaxl = _os->nmaxlttb.at(_os->dvs);
            _os->nflv = this->nmaxl / (10.0 * _os->sla);
        }

        _os->rnstrs = 1.0;
        _os->nsllv = 1.0;
    }
    else //if ( _os->cropsta == 4)
    {
        double const nminso( _os->nminsottb.at( this->ancrf()));
        this->nmaxl = _os->nmaxlttb.at( _os->dvs);       //kg N kg-1 leaves
        this->nminl = _os->nminlttb.at( _os->dvs);       //not needed since crops are not allowed to die

        /* Potential leaf n content (on lai basis) */
        this->nflvp = _os->nflvtb.at( _os->dvs);

        /* Potential N demand of crop organs */
        //maximum n demand of leaves
        this->ndeml = std::max( (this->nmaxl * (_os->wlvg + _os->glv) - this->anlv), 0.0);
        //maximum n demand of roots
        double ndemr = std::max( (this->nmaxl * 0.5 * (_os->wrt + _os->grt) - this->anrt), 0.0);
        //maximum n demand of stems
        double ndems( std::max( (this->nmaxl * 0.5 * (_os->wst + _os->gst) - this->anst), 0.0));
        //maximum n demand of storage organs
        double ndemsx( std::max( _os->nmaxso * _os->gso, 0.0));
        //minimum n demand of storage organs
        double ndemsn( std::max( nminso * _os->gso, 0.0));

        /* Translocation of n from organs, in kg/ha/d */

        //no translocation before dvs = 0.95
        if ( cbm::flt_less( _os->dvs, 0.95))
        {
            this->atnrt = 0.0;
            this->atnlv = 0.0;
            this->atnst = 0.0;
            this->ntso = 0.0;
        }
        else
        {
            //maximum translocation amount from leaves and stems
            this->atnrt = std::max( 0.0, this->anrt - _os->wrt * _os->rfnst);
            this->atnlv = std::max( 0.0, this->anlv - _os->wlvg * _os->rfnlv);
            this->atnst = std::max( 0.0, this->anst - _os->wst * _os->rfnst);

            //daily translocation to storage organs is total pool divided by time constant
            this->ntso = this->atn() / _os->tcntrf;

            //translocation is limited between minimum (ndemsn) and maximum (ndemsx)
            this->ntso = cbm::bound(ndemsn, ndemsx, this->ntso);
        }

        /* Actual n translocation rates from plant organs, in kg/ha/d */
        if ( cbm::flt_greater_zero( this->atn()))
        {
            this->ntrt = this->ntso * this->atnrt / this->atn();
            this->ntlv = this->ntso * this->atnlv / this->atn();
            this->ntst = this->ntso * this->atnst / this->atn();
        }
        else
        {
            this->ntrt = 0.0;
            this->ntlv = 0.0;
            this->ntst = 0.0;
        }

        /* Available n uptake is minimum of soil supply and maximum of crop uptake */
        if ( _os->cropsta == 4)
        {
            this->nupp = cbm::bound(0.0, 0.99 * this->soiln, _os->nmaxup);
        }
        else
        {
            this->nupp = _os->nmaxup;
        }


        /* Sum of total potential n demand from roots, leaves, stems, and storage organs */
        this->ndemc =    ndemr + this->ntrt
                       + this->ndeml + this->ntlv
                       + ndems + this->ntst
                       + ndemsx - this->ntso;

        /* Actual uptake per plant organ is minimum of availability and demand */
        if ( cbm::flt_greater_zero( this->ndemc))
        {
            this->nart = cbm::bound(0.0,
                                      ndemr + this->ntrt,
                                      this->nupp * ((ndemr + this->ntrt) / ndemc));
            this->nalv = cbm::bound(0.0,
                                      this->ndeml + this->ntlv,
                                      this->nupp * ((this->ndeml + this->ntlv) / ndemc));
            this->nast = cbm::bound(0.0,
                                      ndems + this->ntst,
                                      this->nupp * ((ndems + this->ntst) / ndemc));
            this->naso = cbm::bound(0.0,
                                      ndemsx - this->ntso,
                                      this->nupp * ((ndemsx - this->ntso) / ndemc));
        }
        else
        {
            this->nart = this->nalv = this->nast = this->naso = 0.0;
        }


        /* Total uptake by crop from the soil */
        this->nacr = (nart + nalv + nast + naso);

        /* Calculate net N flows to plant organs (daily rates) transplanting shock: remove n */
        double nshklv( this->anlv * (1.0 - _os->pltr));
        double nshkst( this->anst * (1.0 - _os->pltr));

        /* Loss of N from leaves by leaf death (use minimum of residual and actual N concentration) */
        this->nldlv = ((_os->llv + _os->dldr) * std::min(_os->rfnlv, fnlv));

        /* Net flow to roots, stems and leaves */
        this->nrt = (nart - ntrt);
        this->nlv = (nalv - ntlv - this->nldlv - nshklv);
        this->nst = (this->nast - this->ntst - nshkst);

        /* Net N flow to storage organs */
        this->nso = (ntso + naso);

        /* Net flow to stems and leaves before flowering */
        if ( cbm::flt_less( _os->dvs,  1.0))
        {
            this->nstan = nst;
            this->nlvan = nlv;
        }
        else
        {
            this->nstan = 0.0;
            this->nlvan = 0.0;
        }
    }

    return  LDNDC_ERR_OK;
}



lerr_t
ldndc::Oryza2000Ncrop::NcropIntegrate(
                                      bool _ncrop_potential,
                                      ldndc::Oryza2000State *_os)
{
    if ( _ncrop_potential)
    {
        return  LDNDC_ERR_OK;
    }
    
    /* N amount in plant organs */
    this->anso += this->nso;
    this->anrt += this->nrt;
    this->anlv += this->nlv;
    this->anst += this->nst;
    this->anld += this->nldlv;

    /* N amount in plant organs before flowering */
    this->anlva += this->nlvan;
    this->ansta += this->nstan;

    /* N uptake from soil */
    this->soiln -= this->nacr;

    if ( _os->cropsta < 4)
    {
        this->fnrt = _os->fnlvi;
        this->fnlv = _os->fnlvi;
        this->fnst = 0.5 * _os->fnlvi;
        this->fnso = 0.0;

        _os->nflv = _os->nflvi;
        _os->nsllv = 1.0;
        _os->rnstrs = 1.0;
    }
    else
    {
        /* Fraction of N in plant organs */
        if ( _os->wrt > 0.0)
        {
            this->fnrt = (this->anrt / _os->wrt);
        }
        if ( _os->wlvg > 0.0)
        {
            this->fnlv = (this->anlv / _os->wlvg);
        }
        if ( _os->wst > 0.0)
        {
            this->fnst = (this->anst / _os->wst);
        }
        if ( _os->wso > 0.0)
        {
            this->fnso = (this->anso / _os->wso);
        }

        /* Leaf N content in g N m-2 leaf */
        if ( cbm::flt_equal_zero( _os->lai))
        {
            _os->nflv = _os->nflvi;
        }
        else
        {
            if ( _os->sla > 0.0)
            {
                _os->nflv = (this->fnlv / (10.0 * _os->sla));
            }
            else if (   cbm::flt_less( _os->lai, 1.0)
                     && cbm::flt_less( _os->dvs, 1.0))
            {
                _os->nflv = ( (this->nmaxl > 0.0) ? (this->fnlv / this->nmaxl) * this->nflvp : 0.0);
            }
            else
            {
                _os->nflv = ( (_os->lai > 0.0) ? this->anlv / (10.0 * _os->lai) : 0.0);
            }
        }

        /* Set n stress factor for rgrl */
        _os->rnstrs = cbm::bound( 0.0,
                                  (this->fnlv - 0.9 * this->nmaxl) / (this->nmaxl - 0.9 * this->nmaxl),
                                  1.0);

        /* Set n stress factor for leaf death */
        double ancrpt = (_os->wlvg * this->nmaxl + _os->wst * this->nmaxl * 0.5 + _os->wso * _os->nmaxso);
        double nstres( cbm::bound( 1.0,
                                   cbm::flt_equal_zero( this->ancr()) ? 2.0 : (ancrpt / this->ancr()),
                                   2.0));
        
        _os->nsllv = _os->nsllvttb.at( nstres);
    }

    return  LDNDC_ERR_OK;
}

