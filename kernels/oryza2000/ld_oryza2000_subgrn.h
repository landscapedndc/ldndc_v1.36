/*!
 * @file
 *
 * @author:
 *      david kraus (created on: october 5, 2016),
 *      steffen klatt
 */

#ifndef  ORYZA2000_SUBGRN_H_
#define  ORYZA2000_SUBGRN_H_

namespace ldndc {

class Oryza2000State;
class Oryza2000Subgrn
{

public:
    Oryza2000Subgrn();

    ~Oryza2000Subgrn();

    void run( Oryza2000State *);

private:
    double coldtt;
    double tfert;
    double ntfert;
};

} /* namespace ldndc */

#endif /* !ORYZA2000_SUBGRN_H_ */

