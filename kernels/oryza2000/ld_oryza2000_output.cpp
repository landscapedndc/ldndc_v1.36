/*!
 * @file
 *    hydrological outputs
 *
 * @author
 *    steffen klatt
 */

#include  "oryza2000/ld_oryza2000_output.h"
#include  "oryza2000/ld_oryza2000_state.h"


#include  <kernel/io-kcomm.h>

#define  LMOD_OUTPUT_MODULE_BASE  OutputModuleBase


/*!
 * @page ldndc_oryza2000
 * @tableofcontents
 * @section ldndc_oryza2000_output Output
 * ...
 */
static
ldndc_string_t const  Oryza2000Output_Ids[] =
{
    "DVS[-]",
    "RDD",
    "TMIN[oC]", "TMAX[oC]",
    "NFLV",
    "SLA",
    "LESTRS", "LRSTRS",
    "PCEW",
    "NSP",
    "LAI[-]",
    "WAGT", "WST", "WSTS",
    "GST",
    "WSTR", "WLVG", "WLVD", "WLV", "WSO",
    "WRR14",
    "ZRT",
    "CROPSTA",
    "NDEML",
    "NDEMC",
    "NUPP",
    "ANCR",
    "ANLV",
    "ANLD",
    "ANST",
    "ANSO",
    "NMAXL",
    "NMINL",
    "FNLV",
    "TNSOIL",
    "NACR",
    "MSKPA1"
};
static
ldndc_string_t const *  Oryza2000Output_Header =
    Oryza2000Output_Ids;
#define  Oryza2000Output_Datasize  (sizeof( Oryza2000Output_Ids) / sizeof( Oryza2000Output_Ids[0]))
static
ldndc_output_size_t const  Oryza2000Output_Sizes[] =
{
    Oryza2000Output_Datasize,

    Oryza2000Output_Datasize /*total size*/
};
static
ldndc_output_size_t const *  Oryza2000Output_EntitySizes = NULL;
#define  Oryza2000Output_Rank  ((ldndc_output_rank_t)(sizeof( Oryza2000Output_Sizes) / sizeof( Oryza2000Output_Sizes[0])) - 1)
static
atomic_datatype_t const  Oryza2000Output_Types[] =
{
    LDNDC_FLOAT64
};


ldndc::Oryza2000Output::Oryza2000Output( cbm::io_kcomm_t *  _io_kcomm)
        : io_kcomm( _io_kcomm),
          m_sif( _io_kcomm)
    { }


ldndc::Oryza2000Output::~Oryza2000Output()
    { }


lerr_t
ldndc::Oryza2000Output::configure(
                ldndc::config_file_t const *  _cf)
{
    lerr_t  rc_setflags = this->m_sif.set_metaflags(
        _cf, "oryza2000", RM_CLIENTSOURCE|RM_CLIENTID|RM_DATETIME);
    if ( rc_setflags)
        { return LDNDC_ERR_FAIL; }

    return  LDNDC_ERR_OK;
}


lerr_t
ldndc::Oryza2000Output::initialize()
{
    this->m_sink = this->io_kcomm->sink_handle_acquire( "oryza2000");
    if ( this->m_sink.status() == LDNDC_ERR_OK)
    {
        lerr_t  rc_layout = LDNDC_OUTPUT_DEFINE_SINK_LAYOUT(
            this->m_sink,Oryza2000Output,this->m_sif.get_metaflags());
        if ( rc_layout)
            { return  LDNDC_ERR_FAIL; }
    }
    else
    {
        LOGVERBOSE( "no such sink; not producing output  [sink=","oryza2000","]");
    }

    return  LDNDC_ERR_OK;
}

lerr_t
ldndc::Oryza2000Output::step( ldndc::Oryza2000State const *  _os,
    ldndc::Oryza2000Ncrop const *  _ncrop, cbm::sclock_t const *  _clock)
{
    if ( !this->m_sink.is_acquired())
    { return  LDNDC_ERR_OK; }

    ldndc_flt64_t  data_flt64_0[Oryza2000Output_Datasize];
    lerr_t  rc_dump = collect_datarecord( data_flt64_0, _os, _ncrop);
    if ( rc_dump)
    { return  LDNDC_ERR_FAIL; }

    void *  data[] = { data_flt64_0};
    lerr_t  rc_write =
    this->m_sif.write_fixed_record(
                                   &this->m_sink, data, _clock);
    if ( rc_write)
    { return  LDNDC_ERR_FAIL; }

    return  LDNDC_ERR_OK;
}

lerr_t
ldndc::Oryza2000Output::finalize()
{
    this->io_kcomm->sink_handle_release( &this->m_sink);
    return  LDNDC_ERR_OK;
}

#define  OryzaPut(__x__) { *_buf=(__x__); ++_buf; }
lerr_t
ldndc::Oryza2000Output::collect_datarecord(
                                    ldndc_flt64_t *  _buf,
                                    ldndc::Oryza2000State const *  _os,
                                    ldndc::Oryza2000Ncrop const *  _ncrop)
{
    OryzaPut( _os->dvs);
    OryzaPut( _os->dtr);
    OryzaPut( _os->tmin);
    OryzaPut( _os->tmax);
    OryzaPut( _os->nflv);
    OryzaPut( _os->sla);
    OryzaPut( _os->lestrs);
    OryzaPut( _os->lrstrs);
    OryzaPut( _os->pcew);
    OryzaPut( _os->nsp);
    OryzaPut( _os->lai);
    OryzaPut( _os->wagt);
    OryzaPut( _os->wst);
    OryzaPut( _os->wsts);
    OryzaPut( _os->gst);
    OryzaPut( _os->wstr);
    OryzaPut( _os->wlvg);
    OryzaPut( _os->wlvd);
    OryzaPut( _os->wlv);
    OryzaPut( _os->wso);
    OryzaPut( _os->wrr/0.86);//Dry weight of rough rice (14% moisture) (kg ha-1));
    OryzaPut( _os->zrt);
    OryzaPut( _os->cropsta);
    OryzaPut( _ncrop->ndeml);
    OryzaPut( _ncrop->ndemc);
    OryzaPut( _ncrop->nupp);
    OryzaPut( _ncrop->ancr());
    OryzaPut( _ncrop->anlv);
    OryzaPut( _ncrop->anld);
    OryzaPut( _ncrop->anst);
    OryzaPut( _ncrop->anso);
    OryzaPut( _ncrop->nmaxl);
    OryzaPut( _ncrop->nminl);
    OryzaPut( _ncrop->fnlv);
    OryzaPut( _ncrop->soiln);
    OryzaPut( _ncrop->nacr);
    OryzaPut( _os->mskpa_sl[0]);


    return  LDNDC_ERR_OK;
}
#undef  OryzaPut

#undef  Oryza2000Output_Rank
#undef  Oryza2000Output_Datasize

