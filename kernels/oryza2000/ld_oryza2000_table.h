/*!
 * @file
 *
 * @author:
 *      david kraus (created on: october 5, 2016),
 *      steffen klatt
 */

#include  <vector>

#ifndef ORYZA2000_TABLE_H_
#define ORYZA2000_TABLE_H_

namespace ldndc {
class OryzaTable
{

public:
    OryzaTable();
    OryzaTable( char const * /*name*/,
        double const * /*nodes*/, int /*nodes size*/);
    OryzaTable( OryzaTable const &);
    OryzaTable &  operator=( OryzaTable const &);

    ~OryzaTable();

    char const *  name() const
        { return this->m_name; }

    double  at( double  _x)
        { return  this->linear_interpolate( _x); }

private:
    char *  m_name;
    std::vector<double>  m_nodes;

    double  linear_interpolate( double /*x*/);
};

} /* namespace ldndc */

#endif /* !ORYZA2000_TABLE_H_ */

