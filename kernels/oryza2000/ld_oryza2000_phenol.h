/*!
 * @file
 *
 * @author:
 *      david kraus (created on: october 5, 2016),
 *      steffen klatt
 */

#ifndef  ORYZA2000_PHENOL_H_
#define  ORYZA2000_PHENOL_H_

namespace ldndc {

class  Oryza2000State;
class  Oryza2000Phenol
{

public:
    Oryza2000Phenol();
    ~Oryza2000Phenol();

    void run( Oryza2000State *, double);

    void reset();

private:
    double tstr;    //Temperature sum for phenological development at transplanting (°Cd)
};
} /* namespace ldndc */


#endif /* !ORYZA2000_PHENOL_H_ */

