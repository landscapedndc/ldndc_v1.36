/*!
 * @file
 *    Oryza2000 outputs
 *
 * @author
 *    steffen klatt (created on: oct 18, 2016),
 *    david kraus
 */

#ifndef  ORYZA2000_OUTPUT_H_
#define  ORYZA2000_OUTPUT_H_

#include  "oryza2000/ld_oryza2000_state.h"
#include  "oryza2000/ld_oryza2000_ncrop.h"
#include  "ld_sinkinterface.h"

namespace ldndc {

class  LDNDC_API  Oryza2000Output
{
    public:
        Oryza2000Output( cbm::io_kcomm_t * = NULL);
        ~Oryza2000Output();

        lerr_t  configure( ldndc::config_file_t const *);

        lerr_t  initialize();
        lerr_t  step(
                     Oryza2000State const *,
                     Oryza2000Ncrop const *,
                     cbm::sclock_t const *);
        lerr_t  finalize();

    private:
        lerr_t  collect_datarecord(
                                   ldndc_flt64_t *,
                                   Oryza2000State const *,
                                   Oryza2000Ncrop const *);

    private:
        cbm::io_kcomm_t *  io_kcomm;
    private:
        ldndc::sink_handle_t  m_sink;
        ldndc::SinkInterface  m_sif;
};

} /* namespace ldndc */

#endif  /*  !ORYZA2000_OUTPUT_H_  */

