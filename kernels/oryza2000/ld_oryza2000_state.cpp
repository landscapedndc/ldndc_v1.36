/*!
 * @brief
 *
 * @author:
 *      david kraus (created on: june 25, 2016),
 *      steffen klatt
 */

#include  "oryza2000/ld_oryza2000_state.h"


ldndc::Oryza2000State::Oryza2000State(
                                      cbm::io_kcomm_t *  _io_kcomm,
                                      double _time_resolution,
                                      size_t _max_foliage_layers):
                            sl_( _io_kcomm->get_input_class_ref< soillayers::input_class_soillayers_t >()),
                            tkl_sl( sl_.soil_layer_cnt(), 0.0),
                            mskpa_sl( sl_.soil_layer_cnt(), 0.0),
                            trr_sl( sl_.soil_layer_cnt(), 0.0),
                            trwl_sl( sl_.soil_layer_cnt(), 0.0),
                            wc_sl( sl_.soil_layer_cnt(), 0.0),
                            poro_sl( sl_.soil_layer_cnt(), 0.0),
                            wcwp_sl( sl_.soil_layer_cnt(), 0.0),
                            m_photo( 1.0/_time_resolution, _max_foliage_layers)
{
    vcAct25_fl = new double[_max_foliage_layers];
    jAct25_fl = new double[_max_foliage_layers];
    rdAct25_fl = new double[_max_foliage_layers];
    sla_fl = new double[_max_foliage_layers];

    vpd_fl = new double[_max_foliage_layers];
    rh_fl = new double[_max_foliage_layers];
    temp_fl = new double[_max_foliage_layers];
    parsun_fl = new double[_max_foliage_layers];
    parshd_fl = new double[_max_foliage_layers];
    tFol_fl = new double[_max_foliage_layers];
    co2_concentration_fl = new double[_max_foliage_layers];
    sunlitfoliagefraction_fl = new double[_max_foliage_layers];
    carbonuptake_fl = new double[_max_foliage_layers];

    
    this->cropsta = 0;
    this->swisla = 0;
    this->tklt = 1.0;   //if not set elsewhere default value of soil profile depth 0.5 m


    this->reset();
}


void
ldndc::Oryza2000State::reset()
{
    if ( this->swisla)
    { delete[] this->swisla; }
    this->swisla = new char[SWISLA_SIZE];

    crglv = 0.0;
    crgso = 0.0;
    crgst = 0.0;
    crgstr = 0.0;
    crgrt = 0.0;

    dldr = 0.0;
    dldrt = 0.0;

    fclv = 0.0;
    fcrt = 0.0;
    fcso = 0.0;
    fcst = 0.0;
    fcstr = 0.0;
    fstr = 0.0;

    zrt = 0.0;
    llv = 0.0;
    pwrr = 0.0;

    ngr = 0.0;
    ngrm2 = 0.0;
    nsp = 0.0;
    nspm2 = 0.0;

    drout = false;

    frpar = 0.0;
    scp = 0.0;
    co2ref = 0.0;
    co2 = 0.0;

    tbd = 0.0;
    tblv = 0.0;
    tmd = 0.0;
    tod = 0.0;

    mainlv = 0.0;
    mainrt = 0.0;
    mainso = 0.0;
    mainst = 0.0;

    tclstr = 0.0;
    tref = 0.0;
    wgrmx = 0.0;
    zrtmcw = 0.0;
    zrtmcd = 0.0;
    gzrt = 0.0;

    nflvi = 0.0;
    nflv = 0.0;
    knf = 0.0;
    redft = 0.0;

    shckd = 0.0;
    spgf = 0.0;

    nh = 0.0;
    hu = 0.0;
    hulv = 0.0;

    ts_dtga = 0.0;
    d_dtga = 0.0;
    c_root_turnover = 0.0;
    c_root_exsudation = 0.0;
    dtr = 0.0;
    tnass = 0.0;
    tslv = 0.0;
    dae = 0;
    dvr = 0.0;
    dvrj = 0.0;
    dvri = 0.0;
    dvrp = 0.0;
    dvrr = 0.0;

    lai = 0.0;
    dldr = 0.0;

    pcew = 0.0;
    cpew = 1.0;
    
    gcr = 0.0;

    rmcr_rt = 0.0;
    rmcr_lv = 0.0;
    rmcr_so = 0.0;
    rmcr_st = 0.0;

    rgcr_rt = 0.0;
    rgcr = 0.0;

    ulls = 0.0;
    llls = 0.0;
    uldl = 0.0;
    lldl = 0.0;
    ulle = 0.0;
    llle = 0.0;
    ulrt = 0.0;
    llrt = 0.0;

    nmaxup = 0.0;
    rfnlv = 0.0;
    fntrt = 0.0;
    rfnst = 0.0;
    tcntrf = 0.0;
    fnlvi = 0.0;

    nmaxso = 0.0;

    tmax = 0.0;
    tmin = 0.0;
    tav = 0.0;

    nsllv = 0.0;
    rnstrs = 1.0;
    sbdur = 0;

    plant_count = 0.0;
    nplsb = 0.0;
    nplds = 0.0;

    scp = 0.0;

    lape = 0.0;
    dvsi = 0.0;
    wlvgi = 0.0;
    wrti = 0.0;
    wsoi = 0.0;
    wsti = 0.0;
    zrti = 0.0;
    zrttr = 0.0;

    pltr = 1.0;

    tdrw = 0.0;
    wag = 0.0;
    wagt = 0.0;
    wlv = 0.0;
    wrr = 0.0;    
    wrt = 0.0;
    wso = 0.0;
    wst = 0.0;

    wlvgit = 0.0;
    rwlvg = 0.0;

    zrtm = 0.0;
    zrtmcd = 0.0;
    zrtms = 0.0;
    
    ncold = 0.0;

    coldtt = 0.0;
    tfert  = 0.0;
    ntfert = 0.0;

    surface_water = 0.0;
    potentialevapotranspiration = 0.0;
    potentialtranspiration = 0.0;
    potentialevaporation = 0.0;

    TOFRTBAS = 0.0;
    DOC_RESP_RATIO = 0.0;
}


#define  Oryza_log_invalid(__var__,__name__)                 \
if (!((__var__) >= 0.0))                                     \
{                                                            \
++n_err;                                                     \
LOGERROR( "Simulation [id=", _id,                            \
          "]. Illegal value at ", _position, ": ",           \
__name__, ": ", __var__);                                    \
}

lerr_t
ldndc::Oryza2000State::Oryza_check_for_negative_value(
                                                  size_t _id,
                                                  char const *  _position)
{
    int n_err = 0;

    Oryza_log_invalid(crglv, "crglv");
    Oryza_log_invalid(crgso, "crgso");
    Oryza_log_invalid(crgst, "crgst");
    Oryza_log_invalid(crgstr, "crgstr");
    Oryza_log_invalid(crgrt, "crgrt");

    Oryza_log_invalid(fclv, "fclv");
    Oryza_log_invalid(fcrt, "fcrt");
    Oryza_log_invalid(fcso, "fcso");
    Oryza_log_invalid(fcst, "fcst");
    Oryza_log_invalid(fcstr, "fcstr");
    Oryza_log_invalid(fstr, "fstr");

    Oryza_log_invalid(zrt, "zrt");
    Oryza_log_invalid(llv, "llv");
    Oryza_log_invalid(pwrr, "pwrr");

    Oryza_log_invalid(ngr, "ngr");
    Oryza_log_invalid(ngrm2, "ngrm2");
    Oryza_log_invalid(nsp, "nsp");
    
    Oryza_log_invalid(nspm2, "nspm2");

    Oryza_log_invalid(frpar, "frpar");
    Oryza_log_invalid(scp, "scp");
    Oryza_log_invalid(co2ref, "co2ref");
    Oryza_log_invalid(co2, "co2");

    Oryza_log_invalid(tbd, "tbd");
    Oryza_log_invalid(tblv, "tblv");
    Oryza_log_invalid(tmd, "tmd");
    Oryza_log_invalid(tod, "tod");

    Oryza_log_invalid(mainlv, "mainlv");
    Oryza_log_invalid(mainrt, "mainrt");
    Oryza_log_invalid(mainso, "mainso");
    Oryza_log_invalid(mainst, "mainst");

    Oryza_log_invalid(tclstr, "tclstr");
    Oryza_log_invalid(tref, "tref");
    Oryza_log_invalid(wgrmx, "wgrmx");
    Oryza_log_invalid(zrtmcw, "zrtmcw");
    Oryza_log_invalid(zrtmcd, "zrtmcd");
    Oryza_log_invalid(gzrt, "gzrt");

    Oryza_log_invalid(nflvi, "nflvi");
    Oryza_log_invalid(nflv, "nflv");

    Oryza_log_invalid(shckd, "shckd");
    Oryza_log_invalid(spgf, "spgf");

    Oryza_log_invalid(nh, "nh");
    Oryza_log_invalid(hu, "hu");
    Oryza_log_invalid(hulv, "hulv");

    Oryza_log_invalid(d_dtga, "d_dtga");
    Oryza_log_invalid(dtr, "dtr");
    Oryza_log_invalid(tnass, "tnass");
    Oryza_log_invalid(tslv, "tslv");
    Oryza_log_invalid(dae, "dae");
    Oryza_log_invalid(dvr, "dvr");
    Oryza_log_invalid(dvrj, "dvrj");
    Oryza_log_invalid(dvri, "dvri");
    Oryza_log_invalid(dvrp, "dvrp");
    Oryza_log_invalid(dvrr, "dvrr");

    Oryza_log_invalid(lai, "lai");
    Oryza_log_invalid(dldr, "dldr");

    Oryza_log_invalid(pcew, "pcew");
    //Oryza_log_invalid(, "gcr");
    //
    //Oryza_log_invalid(, "rmcr_rt");
    //Oryza_log_invalid(, "rmcr_lv");
    //Oryza_log_invalid(, "rmcr_so");
    //Oryza_log_invalid(, "rmcr_st");
    //
    //Oryza_log_invalid(, "rgcr_rt");
    //Oryza_log_invalid(, "rgcr");
    //
    //Oryza_log_invalid(, "ulls");
    //Oryza_log_invalid(, "llls");
    //Oryza_log_invalid(, "uldl");
    //Oryza_log_invalid(, "lldl");
    //Oryza_log_invalid(, "ulle");
    //Oryza_log_invalid(, "llle");
    //Oryza_log_invalid(, "ulrt");
    //Oryza_log_invalid(, "llrt");
    //
    //Oryza_log_invalid(, "nmaxup");
    //Oryza_log_invalid(, "rfnlv");
    //Oryza_log_invalid(, "fntrt");
    //Oryza_log_invalid(, "rfnst");
    //Oryza_log_invalid(, "tcntrf");
    //Oryza_log_invalid(, "fnlvi");
    //
    //Oryza_log_invalid(, "nmaxso");
    //
    //Oryza_log_invalid(, "tmax");
    //Oryza_log_invalid(, "tmin");
    //Oryza_log_invalid(, "tav");
    //
    //
    //Oryza_log_invalid(, "nsllv");
    //Oryza_log_invalid(, "rnstrs");
    Oryza_log_invalid(sbdur, "sbdur");
    //
    Oryza_log_invalid(plant_count, "plant_count");
    //Oryza_log_invalid(, "nplsb");
    //Oryza_log_invalid(, "nplds");
    //
    //Oryza_log_invalid(, "scp");

    Oryza_log_invalid(lape, "lape");
    Oryza_log_invalid(dvsi, "dvsi");
    Oryza_log_invalid(wlvgi, "wlvgi");
    Oryza_log_invalid(wrti, "wrti");
    Oryza_log_invalid(wsoi, "wsoi");
    Oryza_log_invalid(wsti, "wsti");
    Oryza_log_invalid(zrti, "zrti");
    Oryza_log_invalid(zrttr, "zrttr");

    Oryza_log_invalid(pltr, "pltr");

    Oryza_log_invalid(tdrw, "tdrw");
    Oryza_log_invalid(wag, "wag");
    Oryza_log_invalid(wagt, "wagt");
    Oryza_log_invalid(wlv, "wlv");
    Oryza_log_invalid(wrr, "wrr");
    Oryza_log_invalid(wrt, "wrt");
    Oryza_log_invalid(wso, "wso");
    Oryza_log_invalid(wst, "wst");

    Oryza_log_invalid(wlvgit, "wlvgit");


    Oryza_log_invalid(zrtm, "zrtm");
    Oryza_log_invalid(zrtmcd, "zrtmcd");

    Oryza_log_invalid(coldtt, "coldtt");
    Oryza_log_invalid(tfert, "tfert ");
    Oryza_log_invalid(ntfert, "ntfert");

    if (n_err > 0)
    {        
        return LDNDC_ERR_FAIL;
    }
    return LDNDC_ERR_OK;
}


ldndc::Oryza2000State::~Oryza2000State()
{
    delete[] this->swisla;

    delete[] vcAct25_fl;
    delete[] jAct25_fl;
    delete[] rdAct25_fl;
    delete[] sla_fl;
    
    delete[] vpd_fl;
    delete[] rh_fl;
    delete[] temp_fl;
    delete[] parsun_fl;
    delete[] parshd_fl;
    delete[] tFol_fl;
    delete[] co2_concentration_fl;
    delete[] sunlitfoliagefraction_fl;
    delete[] carbonuptake_fl;
}


lerr_t
ldndc::Oryza2000State::initialize( cbm::io_kcomm_t *)
    { return  LDNDC_ERR_OK; }

