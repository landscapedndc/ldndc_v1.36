/*!
 * @brief
 *    declare default kernel if none
 *    was explicitely specified
 *
 * @author
 *    steffen klatt (created on: jun 04, 2013)
 */
#ifndef  LD_DEFAULTS_H_
#define  LD_DEFAULTS_H_

/* kernel mounter (NULL means fallback to internal default) */
#define  _LD_KSpatial NULL
/* kernel scheduler (NULL means fallback to internal default) */
#define  _LD_KTemporal NULL
/* default kernel to use as model composition controller */
#define  _LD_ModelCompositor "_LD_Setup"
/* internal air chemistry record emitter */
#define  _LD_Airchemistry "_LD_Airchemistry"
/* internal climate record emitter */
#define  _LD_Climate "_LD_Climate"
/* internal event emitter */
#define  _LD_Event "_LD_Event"
/* internal groundwater record emitter */
#define  _LD_Groundwater "_LD_Groundwater"
/* internal parameter service */
#define  _LD_Parameters "_LD_Parameters"
/* internal species property provider */
#define  _LD_Species "_LD_Species"

/* legacy MoBiLE kernel */
#define  _LD_MoBiLE "_MoBiLE"

#endif  /*  !LD_DEFAULTS_H_  */

