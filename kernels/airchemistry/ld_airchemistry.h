/*!
 * @brief
 *    Provides air chemistry driving data.
 *
 * @author
 *    steffen klatt (created on: jul 24, 2017)
 */

#ifndef  LDNDC_KERNEL_LDAIRCHEMISTRY_H_
#define  LDNDC_KERNEL_LDAIRCHEMISTRY_H_

#include  "ld_kernel.h"
//TODO #include  "ld_shared.h"

namespace ldndc {

class LDNDC_API LK_Airchemistry : public cbm::kernel_t
{
    LDNDC_KERNEL_OBJECT(LK_Airchemistry,airchemistry)
    public:
        LK_Airchemistry();
        ~LK_Airchemistry();

    public:
        lerr_t  configure( cbm::RunLevelArgs *);
        lerr_t  register_ports( cbm::RunLevelArgs *);
        lerr_t  initialize( cbm::RunLevelArgs *);
        lerr_t  read( cbm::RunLevelArgs *);
        lerr_t  unregister_ports( cbm::RunLevelArgs *);

    protected:
        input_class_srv_base_t *  m_input;
        struct  LD_AirchemistryPorts
        {
//TODO            PublishedField<double>  NitrateDryDeposition;
        };
//TODO        LD_AirchemistryPorts  m_HourPorts, m_DayPorts;

    private:
        /* hide these buggers for now */
        LK_Airchemistry( LK_Airchemistry const &);
        LK_Airchemistry &  operator=( LK_Airchemistry const &);
};
} /* namespace ldndc */

#endif /* !LDNDC_KERNEL_LDAIRCHEMISTRY_H_ */

