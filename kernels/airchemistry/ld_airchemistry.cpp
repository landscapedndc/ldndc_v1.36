/*!
 * @author
 *    steffen klatt (created on: jul 24, 2017)
 */

#include  "airchemistry/ld_airchemistry.h"
#include  <input/airchemistry/airchemistry.h>

#include  <time/cbm_time.h>
#include  <logging/cbm_logging.h>

LDNDC_KERNEL_OBJECT_DEFN(LK_Airchemistry,airchemistry,_LD_Airchemistry,"LandscapeDNDC AirChemistry")
ldndc::LK_Airchemistry::LK_Airchemistry()
        : cbm::kernel_t(), m_input( NULL)
    { }

ldndc::LK_Airchemistry::~LK_Airchemistry()
    { }

lerr_t
ldndc::LK_Airchemistry::register_ports( cbm::RunLevelArgs *)
{
    /*TODO register day/hour ports*/
    return  LDNDC_ERR_OK;
}
lerr_t
ldndc::LK_Airchemistry::unregister_ports( cbm::RunLevelArgs *)
{
    /*TODO unregister day/hour ports*/
    return  LDNDC_ERR_OK;
}

static lerr_t  s_FetchInput( ldndc::input_class_srv_base_t ** _input,
            char const * _itype, cbm::RunLevelArgs *  _args)
{
    *_input = NULL;

    cbm::source_descriptor_t  objId;
    lid_t const  inputId =
        _args->iokcomm->resolve_source( _itype, &objId);
    if ( inputId == invalid_lid)
        { return LDNDC_ERR_RUNTIME_ERROR; }
    else
    {
        *_input = _args->iokcomm->fetch_input( _itype, &objId);
        if ( *_input == NULL)
            { return LDNDC_ERR_FAIL; }
    }
    return LDNDC_ERR_OK;
}
static lerr_t  s_UpdateInput( ldndc::input_class_srv_base_t * _input,
                cbm::RunLevelArgs *  _args)
{
    lerr_t rc_update = LDNDC_ERR_OK;
    if ( _input)
        { rc_update = _input->update_internal_state( *_args->clk); }
    return rc_update;
}

lerr_t
ldndc::LK_Airchemistry::configure( cbm::RunLevelArgs *  _args)
{
    if ( s_FetchInput( &this->m_input, "airchemistry", _args) != LDNDC_ERR_OK)
        { return LDNDC_ERR_FAIL; }
    return LDNDC_ERR_OK;
}

lerr_t
ldndc::LK_Airchemistry::initialize( cbm::RunLevelArgs *)
    { return LDNDC_ERR_OK; }

lerr_t
ldndc::LK_Airchemistry::read( cbm::RunLevelArgs *  _args)
{
    airchemistry::input_class_airchemistry_t const *  airchemistry_in =
            _args->iokcomm->get_input_class< airchemistry::input_class_airchemistry_t >();
    if ( !airchemistry_in)
        { return LDNDC_ERR_OK; }

    if ( s_UpdateInput( this->m_input, _args) != LDNDC_ERR_OK)
        { return LDNDC_ERR_FAIL; }

    cbm::sclock_t const &  clk = *_args->clk;
    if ( clk.is_position( TMODE_SUBDAILY))
    {
        /*TODO send hour ports*/
    }

    if ( clk.is_position( TMODE_PRE_DAILY))
    {
        /*TODO send day ports*/
    }

    return LDNDC_ERR_OK;
}

