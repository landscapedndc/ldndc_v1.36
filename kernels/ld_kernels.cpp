/*!
 * @author
 *    steffen klatt (created on: aug 26, 2012)
 */

#include  "ld_kernels.h"

#include  <kernel/kfactory.h>
#include  <kernel/kfactorystore.h>

#include  <string/cbm_string.h>

void const *
ldndc::kernels_info_t::registered_kernels()
{
	return  cbm::kernelfactoryregister_t::registry();
}

#include  <cstdio>
void
ldndc::kernels_info_t::list_available_kernels( unsigned char  _verbose)
{
    cbm::factorystore_t< cbm::kernelfactory_base_t >  kernelfactorystore(
            cbm::kernelfactoryregister_t::registry());
    cbm::factorystore_t< cbm::kernelfactory_base_t >::iterator_t  kf = kernelfactorystore.begin();
    while ( kf != kernelfactorystore.end())
    {
        char const *  kname =
            kernelfactorystore.resolve_name( kf);
        bool  is_system_kernel =
            (( cbm::strlen( kname) > 1) && ( kname[0]=='_'));
        if ( !is_system_kernel || _verbose > 0)
        {
            fprintf( stdout, "%s", kernelfactorystore.resolve_name( kf));
            fprintf( stdout, "\n");
        }
        ++kf;
    }
}


#ifdef __cplusplus
extern "C" {
#endif

void const *  ldndc_registered_kernels()
{
    return  ldndc::kernels_info_t::registered_kernels();
}

extern void  ldndc_list_available_kernels( unsigned char  _verbose)
{
    ldndc::kernels_info_t::list_available_kernels( _verbose);
}


extern int  ldndc_query_kernel( char ** _query_result,
        char const *  _kernel_name, char const *  _query)
{
    cbm::factorystore_t< cbm::kernelfactory_base_t >  kfactorystore(
            cbm::kernelfactoryregister_t::registry());
    cbm::kernelfactorystore_t  kfactory( &kfactorystore);
    cbm::kernel_t *  kernel =
        kfactory.construct_kernel( _kernel_name, 0);
    if ( !kernel)
        { return  -1; }

    /* execute query */
    int  rc_query = kernel->query_feature( _query_result, _query);
    /* free kernel */
    kernel->delete_instance();

    return  rc_query;
}

#ifdef __cplusplus
}
#endif

