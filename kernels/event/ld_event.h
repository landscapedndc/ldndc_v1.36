/*!
 * @brief
 *    Dispatches events
 *
 * @author
 *    steffen klatt (created on: mar 10, 2017)
 */

#ifndef  LDNDC_KERNEL_LDEVENT_H_
#define  LDNDC_KERNEL_LDEVENT_H_

#include  "ld_kernel.h"
#include  <input/event/event.h>

namespace ldndc {
    class EventAttributesCollector;

class LDNDC_API LK_Event : public cbm::kernel_t
{
LDNDC_KERNEL_OBJECT(LK_Event,event)
public:
    LK_Event();
    ~LK_Event();

public:
    lerr_t  configure( cbm::RunLevelArgs *);
    lerr_t  register_ports( cbm::RunLevelArgs *);
    lerr_t  initialize( cbm::RunLevelArgs *);
    lerr_t  read( cbm::RunLevelArgs *);
    lerr_t  unregister_ports( cbm::RunLevelArgs *);

#ifdef  _HAVE_SERIALIZE
    int  create_checkpoint() { return  0; }
    int  restore_checkpoint( ldate_t) { return  0; }
#endif

public:
    struct _Event
    {
        CBM_Handle  handle;
        typedef  int (*SerializerFn)( event::Event const *,
            ldndc::EventAttributesCollector *, double /*dT*/);
        SerializerFn  serializer;
    };
private:
    std::map< std::string, _Event >  m_events;
protected:
    input_class_srv_base_t *  m_input;

private:
    /* hide these buggers for now */
    LK_Event( LK_Event const &);
    LK_Event &  operator=( LK_Event const &);
};
} /* namespace ldndc */

#endif /* !LDNDC_KERNEL_LDEVENT_H_ */

