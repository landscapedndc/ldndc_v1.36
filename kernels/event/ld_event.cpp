/*!
 * @author
 *   Steffen Klatt (created on: mar 10, 2017)
 *   David Kraus
 */

#include  "event/ld_event.h"

#include  <input/event/event.h>
#include  <input/event/events.h>

#include  <logging/cbm_logging.h>

LDNDC_KERNEL_OBJECT_DEFN(LK_Event,event,_LD_Event,"LandscapeDNDC Event")
ldndc::LK_Event::LK_Event()
        : cbm::kernel_t(), m_input( NULL)
{ }

ldndc::LK_Event::~LK_Event()
{ }


#include  "ld_eventqueue.h"
static int  _event_to_json_Cut( ldndc::event::Event const *  _event,
                                ldndc::EventAttributesCollector *  _eac, 
                                double)
{
    ldndc::event::EventCut const *  e = static_cast< ldndc::event::EventCut const * >( _event);
    if ( !e)
    { return -1; }

    _eac->put( "/name", e->species_name());

    _eac->put( "/height", e->height());

    _eac->put( "/remains_relative", e->remains_relative());
    _eac->put( "/remains_absolute", e->remains_absolute());

    _eac->put( "/remains_absolute_fruit", e->remains_absolute_fruit());
    _eac->put( "/remains_absolute_foliage", e->remains_absolute_foliage());
    _eac->put( "/remains_absolute_living_structural_tissue", e->remains_absolute_living_structural_tissue());
    _eac->put( "/remains_absolute_dead_structural_tissue", e->remains_absolute_dead_structural_tissue());
    _eac->put( "/remains_absolute_root", e->remains_absolute_root());

    _eac->put( "/export_fruit", e->export_fruit());
    _eac->put( "/export_foliage", e->export_foliage());
    _eac->put( "/export_living_structural_tissue", e->export_living_structural_tissue());
    _eac->put( "/export_dead_structural_tissue", e->export_dead_structural_tissue());
    _eac->put( "/export_root", e->export_root());

    _eac->put( "/mulching", e->mulching());

    return 0;
}

static int  _event_to_json_Defoliate( ldndc::event::Event const *  _event,
                                      ldndc::EventAttributesCollector *  _eac, 
                                      double)
{
    ldndc::event::EventDefoliate const *  e = static_cast< ldndc::event::EventDefoliate const * >( _event);
    if ( !e)
    { return -1; }

    _eac->put( "/name", e->species_name());
    _eac->put( "/reduction-volume", e->reduction_volume());
    _eac->put( "/export-foliage", e->export_foliage());
    return 0;
}

static int  _event_to_json_Fire( ldndc::event::Event const *  _event,
                                 ldndc::EventAttributesCollector *  _eac,
                                 double  _dt)
{
    ldndc::event::EventFire const *  e = static_cast< ldndc::event::EventFire const * >( _event);
    if ( !e)
    { return -1; }

    _eac->put( "/time-span", _dt);
    _eac->put( "/burned-area", e->burned_area());
    return 0;
}

static int  _event_to_json_Fertilize( ldndc::event::Event const *  _event,
                                      ldndc::EventAttributesCollector *  _eac,
                                      double  _dt)
{
    ldndc::event::EventFertilize const *  e = static_cast< ldndc::event::EventFertilize const * >( _event);
    if ( !e)
    { return -1; }

    _eac->put( "/type", e->type().c_str());
    _eac->put( "/location", e->location().c_str());
    _eac->put( "/amount", e->amount()/_dt);
    _eac->put( "/ni_amount", e->ni_amount()/_dt);
    _eac->put( "/ui_amount", e->ui_amount()/_dt);
    _eac->put( "/depth", e->depth());
    return 0;
}

static int  _event_to_json_Flood( ldndc::event::Event const *  _event,
                                  ldndc::EventAttributesCollector *  _eac,
                                  double  _dt)
{
    ldndc::event::EventFlood const *  e = static_cast< ldndc::event::EventFlood const * >( _event);
    if ( !e)
    { return -1; }

    _eac->put( "/time-span", _dt);
    _eac->put( "/watertable", e->water_table_height());
    _eac->put( "/bund-height", e->bund_height());
    _eac->put( "/percolation-rate", e->percolation_rate());
    _eac->put( "/irrigation-height", e->irrigation_height());
    _eac->put( "/irrigation-amount", e->irrigation_amount());
    _eac->put( "/unlimited-water", e->unlimited_water());
    _eac->put( "/saturation-level", e->saturation_level());
    _eac->put( "/soil-depth", e->soil_depth());
    return 0;
}

static int  _event_to_json_Graze( ldndc::event::Event const *  _event,
                                  ldndc::EventAttributesCollector *  _eac,
                                  double  _dt)
{
    ldndc::event::EventGraze const *  e = static_cast< ldndc::event::EventGraze const * >( _event);
    if ( !e)
    { return -1; }

    _eac->put( "/time-span", _dt);
    _eac->put( "/type", e->livestock_name());
    _eac->put( "/head-count", e->head_count());
    _eac->put( "/grazing-hours", e->grazing_hours());
    _eac->put( "/demand-carbon", e->livestock_properties().demand_carbon);
    _eac->put( "/dung-carbon", e->livestock_properties().dung_carbon);
    _eac->put( "/dung-nitrogen", e->livestock_properties().dung_nitrogen);
    _eac->put( "/urine-nitrogen", e->livestock_properties().urine_nitrogen);
    return 0;
}

static int  _event_to_json_Harvest( ldndc::event::Event const *  _event,
                                    ldndc::EventAttributesCollector *  _eac,
                                    double)
{
    ldndc::event::EventHarvest const *  e = static_cast< ldndc::event::EventHarvest const * >( _event);
    if ( !e)
    { return -1; }

    _eac->put( "/name", e->species_name());

    _eac->put( "/height", e->height());

    _eac->put( "/remains_relative", e->remains_relative());
    _eac->put( "/remains_absolute", e->remains_absolute());
    
    _eac->put( "/fraction-export-stemwood", e->export_stem_wood());
    _eac->put( "/fraction-export-branchwood", e->export_branch_wood());
    _eac->put( "/fraction-export-rootwood", e->export_root_wood());

    _eac->put( "/mulching", e->mulching());

    return 0;
}

static int  _event_to_json_Irrigate( ldndc::event::Event const *  _event,
                                     ldndc::EventAttributesCollector *  _eac,
                                     double)
{
    ldndc::event::EventIrrigate const *  e =
        static_cast< ldndc::event::EventIrrigate const * >( _event);
    if ( !e)
        { return -1; }

    _eac->put( "/amount", e->amount());
    _eac->put( "/is-reservoir", e->reservoir());
    return 0;
}

static int  _event_to_json_Manure( ldndc::event::Event const *  _event,
                                   ldndc::EventAttributesCollector *  _eac,
                                   double)
{
    ldndc::event::EventManure const * e = static_cast< ldndc::event::EventManure const * >( _event);
    if ( !e)
    { return -1; }

    _eac->put( "/type", e->type().c_str());
    _eac->put( "/depth", e->depth());
    _eac->put( "/carbon", e->c());
    _eac->put( "/carbon-nitrogen-ratio", e->cn());
    _eac->put( "/carbon-available", e->avail_c());
    _eac->put( "/nitrogen-available", e->avail_n());
    _eac->put( "/ph", e->ph());
    _eac->put( "/volume", e->volume());
    _eac->put( "/nh4-fraction", e->nh4_fraction());
    _eac->put( "/no3-fraction", e->no3_fraction());
    _eac->put( "/don-fraction", e->don_fraction());
    _eac->put( "/urea-fraction", e->urea_fraction());
    _eac->put( "/cellulose-fraction", e->cellulose_fraction());
    _eac->put( "/lignin-fraction", e->lignin_fraction());
    return 0;
}

static int  _event_to_json_Plant( ldndc::event::Event const *  _event,
                                  ldndc::EventAttributesCollector *  _eac,
                                  double)
{
    ldndc::event::EventPlant const *  e = static_cast< ldndc::event::EventPlant const * >( _event);
    if ( !e)
    { return -1; }

    _eac->put( "/type", e->species_type());
    _eac->put( "/name", e->species_name());
    _eac->put( "/longname", e->species_longname());
    char const *  group = ldndc::species::SPECIES_GROUP_NAMES[e->group()];
    _eac->put( "/group", group);
    _eac->put( "/location", e->location());
    ldndc::species::species_properties_t const *  p = e->species_properties();
    if ( p)
    {
        _eac->put( "/initial-biomass", p->initial_biomass);
        _eac->put( "/fractional-cover", p->fractional_cover);
        if ( cbm::is_equal( group, "crop"))
        {
            ldndc::species::crop_properties_t const &  c = e->crop();
            _eac->put( "/cover-crop", c.cover_crop);
            _eac->put( "/seedling-number", c.seedling_number);
            _eac->put( "/seedbed-duration", c.seedbed_duration);
        }
        else if ( cbm::is_equal( group, "grass"))
        {
            ldndc::species::grass_properties_t const &  g = e->grass();
            _eac->put( "/cover-crop", g.cover_crop);
            _eac->put( "/root-depth", g.root_depth);
            _eac->put( "/maximum-height", g.height_max);
        }
        else if ( cbm::is_equal( group, "wood"))
        {
            ldndc::species::wood_properties_t const &  w = e->wood();
            _eac->put( "/root-depth", w.root_depth);
            _eac->put( "/maximum-height", w.height_max);
            _eac->put( "/minimum-height", w.height_min);
            _eac->put( "/reduction-factor-of-carbon", w.reduc_fac_c);
            _eac->put( "/diameter-at-breast-height", w.dbh);
            _eac->put( "/tree-number", w.number);
            _eac->put( "/tree-volume", w.volume);
        }
        else
            { CBM_LogWarn( "Unknown species group"); }
    }

    return 0;
}

static int  _event_to_json_Regrow( ldndc::event::Event const *  _event,
                                   ldndc::EventAttributesCollector *  _eac,
                                   double)
{
    ldndc::event::EventRegrow const *  e = static_cast< ldndc::event::EventRegrow const * >( _event);
    if ( !e)
    { return -1; }

    _eac->put( "/name", e->species_name());
    _eac->put( "/tree-number", e->tree_number());
    _eac->put( "/tree-number-resize-factor", e->tree_number_resize_factor());
    _eac->put( "/maximum-height", e->height_max());
    _eac->put( "/fraction-export-aboveground-biomass", e->export_aboveground_biomass());
    return 0;
}

static int  _event_to_json_Reparameterizespecies( ldndc::event::Event const *  _event,
                                                  ldndc::EventAttributesCollector *  _eac,
                                                  double)
{
    ldndc::event::EventReparameterizespecies const *  e = static_cast< ldndc::event::EventReparameterizespecies const * >( _event);
    if ( !e)
    { return -1; }

    _eac->put( "/name", e->species_name());
    _eac->put( "/type", e->species_type());
    return 0;
}

static int  _event_to_json_Thin( ldndc::event::Event const *  _event,
                                 ldndc::EventAttributesCollector *  _eac,
                                 double)
{
    ldndc::event::EventThin const *  e = static_cast< ldndc::event::EventThin const * >( _event);
    if ( !e)
    { return -1; }

    _eac->put( "/name", e->species_name());
    _eac->put( "/reduction-number", e->reduction_number());
    _eac->put( "/reduction-volume", e->reduction_volume());
    _eac->put( "/export-sapwood", e->export_sapwood());
    _eac->put( "/export-corewood", e->export_corewood());
    _eac->put( "/export-foliage", e->export_foliage());
    return 0;
}

static int  _event_to_json_Throw( ldndc::event::Event const *  _event,
                                  ldndc::EventAttributesCollector *  _eac,
                                  double)
{
    ldndc::event::EventThrow const *  e = static_cast< ldndc::event::EventThrow const * >( _event);
    if ( !e)
    { return -1; }

    _eac->put( "/name", e->species_name());
    _eac->put( "/reduction-number", e->reduction_number());
    _eac->put( "/reduction-number-is-absolute", e->reduction_number_is_absolute());
    _eac->put( "/reduction-volume", e->reduction_volume());
    _eac->put( "/reduction-volume-is-absolute", e->reduction_volume_is_absolute());
    _eac->put( "/reason", e->reason().c_str());
    return 0;
}

static int  _event_to_json_Till( ldndc::event::Event const *  _event,
                                 ldndc::EventAttributesCollector *  _eac,
                                 double)
{
    ldndc::event::EventTill const *  e = static_cast< ldndc::event::EventTill const * >( _event);
    if ( !e)
    { return -1; }

    _eac->put( "/depth", e->depth());
    return 0;
}
 
struct _EventSerializer
{
    char const *  name;
    ldndc::LK_Event::_Event::SerializerFn  serializer;
};

lerr_t
ldndc::LK_Event::register_ports( cbm::RunLevelArgs *  _args)
{
    static _EventSerializer const  EVENTS[] =
        {
            { "cut", &_event_to_json_Cut },
            { "defoliate", &_event_to_json_Defoliate },
            { "fire", &_event_to_json_Fire },
            { "fertilize", &_event_to_json_Fertilize },
            { "flood", &_event_to_json_Flood },
            { "graze", &_event_to_json_Graze },
            { "irrigate", &_event_to_json_Irrigate },
            { "harvest", &_event_to_json_Harvest },
// sk:TODO            { "luc", &_event_to_json_LandUseChange },
            { "manure", &_event_to_json_Manure },
            { "plant", &_event_to_json_Plant },
            { "regrow", &_event_to_json_Regrow },
            { "reparameterizespecies", &_event_to_json_Reparameterizespecies },
            { "thin", &_event_to_json_Thin },
            { "throw", &_event_to_json_Throw },
            { "till", &_event_to_json_Till },
            { NULL, NULL /*sentinel*/ }
        };

    _EventSerializer const *  event = EVENTS;
    while ( event->name)
    {
        CBM_Handle  event_handle = _args->iokcomm->publish_event( event->name);
        if ( !CBM_HandleOk(event_handle))
            { return LDNDC_ERR_FAIL; }

        _Event  _event;
        _event.handle = event_handle;
        _event.serializer = event->serializer;

        this->m_events[std::string( event->name)] = _event;
        ++event;
    }

    return LDNDC_ERR_OK;
}

lerr_t
ldndc::LK_Event::unregister_ports( cbm::RunLevelArgs *  _args)
{
    for ( std::map< std::string, _Event >::iterator  ea = this->m_events.begin();
            ea != this->m_events.end();  ++ea)
    {
        _args->iokcomm->unpublish_event( ea->second.handle);
    }

    return  LDNDC_ERR_OK;
}

static lerr_t  s_FetchInput( ldndc::input_class_srv_base_t ** _input,
                             char const * _itype,
                             cbm::RunLevelArgs *  _args)
{
    *_input = NULL;

    cbm::source_descriptor_t  objId;
    lid_t const  inputId =
        _args->iokcomm->resolve_source( _itype, &objId);
    if ( inputId == invalid_lid)
        { return LDNDC_ERR_RUNTIME_ERROR; }
    else
    {
        *_input = _args->iokcomm->fetch_input( _itype, &objId);
        if ( *_input == NULL)
            { return LDNDC_ERR_FAIL; }
    }
    return LDNDC_ERR_OK;
}

static lerr_t  s_UpdateInput( ldndc::input_class_srv_base_t * _input,
                cbm::RunLevelArgs *  _args)
{
    lerr_t rc_update = LDNDC_ERR_OK;
    if ( _input)
    { rc_update = _input->update_internal_state( *_args->clk); }
    return rc_update;
}

lerr_t
ldndc::LK_Event::configure( cbm::RunLevelArgs * _args)
{
    if ( s_FetchInput( &this->m_input, "event", _args) != LDNDC_ERR_OK)
        { return LDNDC_ERR_FAIL; }
    return LDNDC_ERR_OK;
}

lerr_t
ldndc::LK_Event::initialize( cbm::RunLevelArgs *)
{ return LDNDC_ERR_OK; }

lerr_t
ldndc::LK_Event::read( cbm::RunLevelArgs *  _args)
{
    event::input_class_event_t const *  event_in =
            _args->iokcomm->get_input_class< event::input_class_event_t >();
    if ( !event_in)
        { return LDNDC_ERR_OK; }

    if ( s_UpdateInput( this->m_input, _args) != LDNDC_ERR_OK)
        { return LDNDC_ERR_FAIL; }

    /* iterate through current events and dispatch them */
    event::EventsView  events = event_in->events_view();
    for ( event::EventsView::const_iterator  e_k = events.begin();
            e_k != events.end();  ++e_k)
    {
        event::Event const *  event = &(*e_k);
        if ( this->m_events.find( event->name()) == this->m_events.end())
            { continue; }

        _Event &  _event = this->m_events[event->name()];

        EventAttributesCollector  eac;
        int  rc_serialize = _event.serializer( event, &eac, e_k.t_span());
        if ( rc_serialize < 0)
            { CBM_LogError( "Failed to serialize event"); }
        else
        {
// sk:dbg            CBM_LogDebug( "sending event.. '", event->name(),"'");
            size_t  event_sz = 0;
            char const *  event_data = eac.serialize( &event_sz);
// sk:dbg            CBM_LogDebug( "'", eac.serialize(),"'");
            _args->iokcomm->send_event( _event.handle, event_data, event_sz+1);
        }
    }

    return LDNDC_ERR_OK;
}

