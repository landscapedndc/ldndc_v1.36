/*!
 * @brief
 *    Provides domain discretization and model composition information.
 *
 * @author
 *    steffen klatt (created on: jul 01, 2016)
 */

#ifndef  LDNDC_KERNEL_LDSETUP_H_
#define  LDNDC_KERNEL_LDSETUP_H_

#include  "ld_kernel.h"

namespace ldndc {
    namespace setup
        { class input_class_setup_t; }

class LDNDC_API LK_Setup : public cbm::kernel_t
{
    LDNDC_KERNEL_OBJECT(LK_Setup,setup)
    public:
        LK_Setup();
        ~LK_Setup();

    public:
        lerr_t  configure( cbm::RunLevelArgs *);

#ifdef  _HAVE_SERIALIZE
        int  create_checkpoint()
            { return  0; }
        int  restore_checkpoint( ldate_t)
            { return  0; }
#endif
#ifdef  _LDNDC_HAVE_ONLINECONTROL
        int  process_request( lreply_t *  /*_reply*/,
                lrequest_t const *  /*_req*/)
            { return  0; }
    private:
        int  process_get( lreply_t *  /*_reply*/,
                lrequest_t const *  /*_req*/)
            { return  0; }
        int  process_put( lreply_t *  /*_reply*/,
                lrequest_t const *  /*_req*/)
            { return  0; }
#endif

    /* module specific extensions */
    public:
        virtual lerr_t  sleep()
            { return  LDNDC_ERR_OK; }
        virtual lerr_t  wake()
            { return  LDNDC_ERR_OK; }


    protected:
        /* input */
        setup::input_class_setup_t const *  m_setup;

    private:
        lerr_t  m_mount_filereaders( cbm::RunLevelArgs *);

        /*handle old-style MoBiLE modules list*/
        lerr_t  m_mount_legacy_MoBiLE( cbm::RunLevelArgs *);
        /*handle requested models and resolve model composition*/
        lerr_t  m_mount_models( cbm::RunLevelArgs *);
        /*handle mounting progressbar*/
        lerr_t  m_mount_progressbar( cbm::RunLevelArgs *);

    private:
        /* hide these buggers for now */
        LK_Setup( LK_Setup const &);
        LK_Setup &  operator=( LK_Setup const &);
};
} /* namespace ldndc */

#endif /* !LDNDC_KERNEL_LDSETUP_H_ */

