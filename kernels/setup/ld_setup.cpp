/*!
 * @author
 *    steffen klatt (created on: jul 01, 2016)
 */

#include  "setup/ld_setup.h"

#include  <input/setup/setup.h>

#include  <logging/cbm_logging.h>
#include  <string/cbm_string.h>
#include  <utils/cbm_utils.h>

LDNDC_KERNEL_OBJECT_DEFN(LK_Setup,setup,_LD_ModelCompositor,"LandscapeDNDC ModelCompositor")
ldndc::LK_Setup::LK_Setup()
        : cbm::kernel_t(),
          m_setup( NULL)
    { }

ldndc::LK_Setup::~LK_Setup()
    { }

lerr_t
ldndc::LK_Setup::configure( cbm::RunLevelArgs *  _args)
{
    lerr_t  rc_conf = LDNDC_ERR_OK;

    if ( _args->iokcomm)
    {
        this->m_setup =
            _args->iokcomm->get_input_class< setup::input_class_setup_t >();
        if ( this->m_setup)
        {
            /* implicitely mount input "reader" kernels */
            if ( !rc_conf)
            {
                lerr_t  rc_filereaders = this->m_mount_filereaders( _args);
                if ( rc_filereaders)
                    { rc_conf = LDNDC_ERR_FAIL; }
            }
            /* mount requested "model" kernels */
            if ( !rc_conf)
            {
                lerr_t  rc_models = this->m_mount_models( _args);
                if ( rc_models)
                    { rc_conf = LDNDC_ERR_FAIL; }
            }
            if (( this->rank() == 0) && _args->cfg->progress_bar())
            {
                lerr_t  rc_progressbar = this->m_mount_progressbar( _args);
                if ( rc_progressbar)
                    { rc_conf = LDNDC_ERR_FAIL; }
            }
        }
        else
        {
            KLOGWARN( "no such kernel.. [",_args->iokcomm->object_id(),"]");
        }
    }
    else
    {
        KLOGERROR( "kernel communicator is NULL.. :-(");
    }
    return  rc_conf;
}

template < typename _I >
static lerr_t mount_filereader( cbm::RunLevelArgs *  _args,
        ldndc::LK_Setup *  _kclient, 
        ldndc::setup::input_class_setup_t const *  _setup_in,
        char const *  _kname, char const * _streamname)
{
    cbm::source_descriptor_t  stream_desc;
    lid_t const  sid =
        _setup_in->block_consume(
                    &stream_desc, _streamname);
    if ( cbm::is_invalid( sid))
        { return LDNDC_ERR_OK; }

    _I const *  stream_in = static_cast< _I const * >(
            _args->iokcomm->acquire_input( _streamname, &stream_desc));
    if ( stream_in)
    {
        cbm::kmount_t  kmount =
            _args->iokcomm->spatial_controller( _kclient);
        lerr_t  rc_kmount = kmount.enqueue_for_mount( _kname,
            stream_desc.block_id, LD_NOSOURCE, _kclient, NULL);

        _args->iokcomm->release_input( stream_in);

        if ( rc_kmount)
            { return  LDNDC_ERR_FAIL; }
    }
    return  LDNDC_ERR_OK;
}
#include  <input/airchemistry/airchemistry.h>
static lerr_t
mount_airchemistry_filereader( cbm::RunLevelArgs *  _args,
            ldndc::LK_Setup *  _kclient,
        ldndc::setup::input_class_setup_t const *  _setup_in)
{
    lerr_t  rc_airchemistry =
        mount_filereader< ldndc::airchemistry::input_class_airchemistry_t >(
            _args, _kclient, _setup_in, _LD_Airchemistry, "airchemistry");
    if ( rc_airchemistry)
        { return  LDNDC_ERR_FAIL; }
    return  LDNDC_ERR_OK;
}
#include  <input/climate/climate.h>
static lerr_t
mount_climate_filereader( cbm::RunLevelArgs *  _args,
            ldndc::LK_Setup *  _kclient,
        ldndc::setup::input_class_setup_t const *  _setup_in)
{
    lerr_t  rc_climate =
        mount_filereader< ldndc::climate::input_class_climate_t >(
            _args, _kclient, _setup_in, _LD_Climate, "climate");
    if ( rc_climate)
        { return  LDNDC_ERR_FAIL; }
    return  LDNDC_ERR_OK;
}
#include  <input/event/event.h>
static lerr_t
mount_event_filereader( cbm::RunLevelArgs *  _args,
            ldndc::LK_Setup *  _kclient,
        ldndc::setup::input_class_setup_t const *  _setup_in)
{
    lerr_t  rc_event =
        mount_filereader< ldndc::event::input_class_event_t >(
            _args, _kclient, _setup_in, _LD_Event, "event");
    if ( rc_event)
        { return  LDNDC_ERR_FAIL; }
    return  LDNDC_ERR_OK;
}
#include  <input/groundwater/groundwater.h>
static lerr_t
mount_groundwater_filereader( cbm::RunLevelArgs *  _args,
            ldndc::LK_Setup *  _kclient,
        ldndc::setup::input_class_setup_t const *  _setup_in)
{
    lerr_t  rc_groundwater =
        mount_filereader< ldndc::groundwater::input_class_groundwater_t >(
            _args, _kclient, _setup_in, _LD_Groundwater, "groundwater");
    if ( rc_groundwater)
        { return  LDNDC_ERR_FAIL; }
    return  LDNDC_ERR_OK;
}
#include  <input/siteparameters/siteparameters.h>
#include  <input/soilparameters/soilparameters.h>
#include  <input/speciesparameters/speciesparameters.h>
static lerr_t
mount_parameters_filereader( cbm::RunLevelArgs *  _args,
            ldndc::LK_Setup *  _kclient,
        ldndc::setup::input_class_setup_t const *  _setup_in)
{
    ldndc::siteparameters::input_class_siteparameters_t const *  siteparameters_in =
        _args->iokcomm->get_input_class<ldndc::siteparameters::input_class_siteparameters_t>();
    ldndc::soilparameters::input_class_soilparameters_t const *  soilparameters_in =
        _args->iokcomm->get_input_class<ldndc::soilparameters::input_class_soilparameters_t>();
    ldndc::speciesparameters::input_class_speciesparameters_t const *  speciesparameters_in =
        _args->iokcomm->get_input_class<ldndc::speciesparameters::input_class_speciesparameters_t>();
    if ( siteparameters_in || soilparameters_in || speciesparameters_in)
    {
        cbm::kmount_t  kmount =
            _args->iokcomm->spatial_controller( _kclient);
        lerr_t  rc_kmount = kmount.enqueue_for_mount( _LD_Parameters,
            _setup_in->object_id(), LD_NOSOURCE, _kclient, NULL);

        if ( rc_kmount)
            { return  LDNDC_ERR_FAIL; }
    }
    return  LDNDC_ERR_OK;
}


#include  <input/species/species.h>
static lerr_t
mount_species_filereader( cbm::RunLevelArgs *  _args,
            ldndc::LK_Setup *  _kclient,
        ldndc::setup::input_class_setup_t const *)
{
    ldndc::species::input_class_species_t const *  stream_in =
        _args->iokcomm->get_input_class<ldndc::species::input_class_species_t>();
    if ( stream_in)
    {
        cbm::kmount_t  kmount =
            _args->iokcomm->spatial_controller( _kclient);
        lerr_t  rc_kmount = kmount.enqueue_for_mount( _LD_Species,
            stream_in->object_id(), LD_NOSOURCE, _kclient, NULL);

        if ( rc_kmount)
            { return  LDNDC_ERR_FAIL; }
    }
    return  LDNDC_ERR_OK;
}

lerr_t
ldndc::LK_Setup::m_mount_filereaders( cbm::RunLevelArgs * _args)
{
    lerr_t  rc = LDNDC_ERR_OK;

    rc = mount_airchemistry_filereader( _args, this, this->m_setup);
    if ( rc)
        { return  LDNDC_ERR_FAIL; }

    rc = mount_climate_filereader( _args, this, this->m_setup);
    if ( rc)
        { return  LDNDC_ERR_FAIL; }

    rc = mount_event_filereader( _args, this, this->m_setup);
    if ( rc)
        { return LDNDC_ERR_FAIL; }

    rc = mount_groundwater_filereader( _args, this, this->m_setup);
    if ( rc)
        { return  LDNDC_ERR_FAIL; }

    rc = mount_parameters_filereader( _args, this, this->m_setup);
    if ( rc)
        { return  LDNDC_ERR_FAIL; }


    /*lower priority*/
    rc = mount_species_filereader( _args, this, this->m_setup);
    if ( rc)
        { return  LDNDC_ERR_FAIL; }

    return  rc;
}


#include  "legacy/mbe_setup.h"
lerr_t
ldndc::LK_Setup::m_mount_legacy_MoBiLE( cbm::RunLevelArgs *  _args)
{
    KLOGWARN( "No model selection found. Assuming MoBiLE ..");
    KLOGDEBUG( "request for queuing kernel '",_LD_MoBiLE,"'",
            " with ID ", this->m_setup->object_id());
    cbm::kmount_t  kmount =
        _args->iokcomm->spatial_controller( this);
    lerr_t  rc_kmount = kmount.enqueue_for_mount( _LD_MoBiLE,
            LD_NOID, "_LD_MM.master", this, NULL);
    if ( rc_kmount)
        { return LDNDC_ERR_RUNTIME_ERROR; }
    return LDNDC_ERR_OK;
}

lerr_t
ldndc::LK_Setup::m_mount_models( cbm::RunLevelArgs *  _args)
{
    char *  modelcomposition = NULL;
    this->m_setup->section_data( &modelcomposition, "models");
    if ( modelcomposition)
    {
        cbm::ksetup_t  models_cfg = this->setup_parser( modelcomposition, "/models");
        CBM_FreeSectionData( modelcomposition);
        if ( !models_cfg.parse_ok())
        {
            KLOGERROR( "Failed to parse model list");
            return  LDNDC_ERR_FAIL;
        }

        int  nb_models = models_cfg.query_size( "/model");
        if ( nb_models == 0)
        {
            KLOGVERBOSE( "No model selection found. Doing without..");
            return  LDNDC_ERR_OK;
        }
        else if ( nb_models == -1)
        {
            lerr_t  rc_mobile = this->m_mount_legacy_MoBiLE( _args);
            if ( rc_mobile)
                { return  LDNDC_ERR_FAIL; }
        }
        KLOGVERBOSE( "seeing ", nb_models, " models");

        for ( int m = 0;  m < nb_models;  ++m)
        {
            cbm::string_t  model_id;
            {
                cbm::string_t  path;
                path << "/model/" << m << "/id";
                model_id = models_cfg.query_string( path, "");
            }

            cbm::mountargs_t  mountargs;
            {
                cbm::string_t  path;
                path << "/model/" << m;
                mountargs.cfg = models_cfg.nested_setup( path);
            }

            if ( model_id.is_empty() || cbm::is_invalid( model_id.str()))
            {
                KLOGERROR( "Empty or missing model ID is not valid");
                return LDNDC_ERR_FAIL;
            }

            KLOGDEBUG( "request for queuing kernel '",model_id,"'",
                        " with ID ", this->m_setup->object_id());
            cbm::kmount_t  kmount =
                    _args->iokcomm->spatial_controller( this);
            lerr_t  rc_kmount = kmount.enqueue_for_mount( model_id,
                this->m_setup->object_id(), LD_NOSOURCE, this, &mountargs);
            if ( rc_kmount)
                { return LDNDC_ERR_RUNTIME_ERROR; }
        }

    }
    else
    {
        lerr_t  rc_mobile = this->m_mount_legacy_MoBiLE( _args);
        if ( rc_mobile)
            { return  LDNDC_ERR_FAIL; }
    }
    return LDNDC_ERR_OK;
}

lerr_t
ldndc::LK_Setup::m_mount_progressbar( cbm::RunLevelArgs *  _args)
{
    cbm::kmount_t  kmount =
        _args->iokcomm->spatial_controller( this);
    lerr_t  rc_kmount = kmount.enqueue_for_mount( "_LD_ProgressBar",
            LD_NOID, LD_NOSOURCE, this, NULL);
    if ( rc_kmount)
        { return LDNDC_ERR_RUNTIME_ERROR; }
    return LDNDC_ERR_OK;
}

