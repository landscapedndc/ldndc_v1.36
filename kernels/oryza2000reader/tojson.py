
def  todict( _filename) :
    ## OryzaX input file
    F = open( _filename)
    ## Dictionary holding data read from input file
    D = dict()

    K, V = None, None
    line_continues = False
    for s in F :
        L = s.strip()
        ## discard empty lines and comments
        if len(L)==0 or L[0]=='*' or L[0]=='!' :
            continue
##        sys.stderr.write( '"%s"\n' % ( L))
        if L.find( '!') >= 0 :
            L = L[0:L.find('!')]
            L = L.strip()
##        sys.stderr.write( '"%s"\n\n' % ( L))
        if not line_continues :
            K, V = L.split( '=') ## assume this always succeeds ;-)
            K, V = K.strip(), V.strip()
        else :
            V = L.strip()
##        sys.stderr.write( 'K="%s";  V="%s"\n' % (K, V))
        line_continues = True if len(V)==0 or V[-1]==',' else False

        if K in D :
            D[K] += V
        else :
            D[K] = V
    return D

def  isfloat( _x) :
    try :
        a = float( _x)
    except ValueError:
        return False
    else :
        return True

def  isint( _x) :
    try :
        a = float( _x)
        b = int( a)
    except ValueError :
        pass
    else :
        if a == b and not '.' in _x :
            return True
    return  False

def  expand_value( _v) :
    v = _v.strip( ',')
    if '*' in _v :
        v = [ w.split('*') for w in v.split( ',') ]
        v = [ w if len(w)>1 else [ '1', w[0]] for w in v ]
        v = ','.join( [ ','.join( [ str( float(w[1])) ] * int( w[0])) for w in v if int( w[0])>0])
    return  v.split( ',')

def  tojson( _filename) :
    D = todict( _filename)
    if D is None :
        return ''
    J = '{'
    for K,V in zip( D.keys(), D.values()) :
        V = expand_value( V)
        if len(V)==1 :
            v = V[0].strip("'")
            if isint( v) :
                J += '"%s":%s,' % ( K, v)
            elif isfloat( v) :
                J += '"%s":%s,' % ( K, str(float(v)))
            else :
                J += '"%s":"%s",' % ( K, v)
        else :
            ## blindly assume numbers
            J += '"%s":[ %s],' % ( K, ','.join([ '%s' % ( str(float(v.strip()))) for v in V]))
    J += ' "STATUS":0 }'
    return  J

#if __name__ == '__main__' :
#    import sys
#    J = tojson( sys.argv[1])
#    print J

