/*!
 * @brief
 *    ldndc kernel wrapper for the Oryza2000 model
 *
 *    Oryza2000 is a product of the International Rice
 *    Research Institute (IRRI) and written by Tao Li.
 *
 * @author
 *    steffen klatt
 *    david kraus (created on: june 30, 2016)
 */

#ifndef  LDNDC_KERNEL_ORYZA2000READER_H_
#define  LDNDC_KERNEL_ORYZA2000READER_H_

#include  "ld_kernel.h"
#include  "ld_eventqueue.h"

#include  <json/cbm_json.h>

namespace ldndc {

class LDNDC_API LK_Oryza2000Reader : public cbm::kernel_t
{
    LDNDC_KERNEL_OBJECT(LK_Oryza2000Reader,oryza2000reader)
    public:
        LK_Oryza2000Reader();
        ~LK_Oryza2000Reader();

        lerr_t  register_ports( cbm::RunLevelArgs *);
        lerr_t  unregister_ports( cbm::RunLevelArgs *);

        lerr_t  read( cbm::RunLevelArgs *);

    protected:
        CBM_Handle  m_PlantInfoHandle;

        EventQueue  m_PlantEvents;
        CBM_Handle  m_PlantHandle;

    private:
        /* hide these buggers for now */
        LK_Oryza2000Reader( LK_Oryza2000Reader const &);
        LK_Oryza2000Reader &  operator=( LK_Oryza2000Reader const &);
};

} /* namespace ldndc */

#endif /* !LDNDC_KERNEL_ORYZA2000READER_H_ */

