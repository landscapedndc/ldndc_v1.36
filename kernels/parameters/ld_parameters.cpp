/*!
 * @author
 *    steffen klatt (created on: jul 28, 2017)
 */

#include  "parameters/ld_parameters.h"
#include  <input/siteparameters/siteparameters.h>
#include  <input/soilparameters/soilparameters.h>
#include  <input/speciesparameters/speciesparameters.h>

#include  <time/cbm_time.h>
#include  <logging/cbm_logging.h>

LDNDC_KERNEL_OBJECT_DEFN(LK_Parameters,parameters,_LD_Parameters,"LandscapeDNDC Parameters")
ldndc::LK_Parameters::LK_Parameters()
        : cbm::kernel_t(),
            m_parametersite( NULL), m_parametersoil( NULL),
                m_parameterspecies( NULL)
    { }

ldndc::LK_Parameters::~LK_Parameters()
    { }

static lerr_t  s_FetchInput( ldndc::input_class_srv_base_t ** _input,
            char const * _itype, cbm::RunLevelArgs *  _args)
{
    *_input = NULL;

    cbm::source_descriptor_t  objId;
    lid_t const  inputId =
        _args->iokcomm->resolve_source( _itype, &objId);
    if ( inputId == invalid_lid)
        { return LDNDC_ERR_RUNTIME_ERROR; }
    else
    {
        *_input = _args->iokcomm->fetch_input( _itype, &objId);
        if ( *_input == NULL)
            { return LDNDC_ERR_FAIL; }
    }
    return LDNDC_ERR_OK;
}
static lerr_t  s_UpdateInput( ldndc::input_class_srv_base_t * _input,
                cbm::RunLevelArgs *  _args)
{
    lerr_t rc_update = LDNDC_ERR_OK;
    if ( _input)
        { rc_update = _input->update_internal_state( *_args->clk); }
    return rc_update;
}

lerr_t
ldndc::LK_Parameters::configure( cbm::RunLevelArgs *  _args)
{
    if ( s_FetchInput( &this->m_parametersite, "siteparameters", _args) != LDNDC_ERR_OK)
        { return LDNDC_ERR_FAIL; }
    if ( s_FetchInput( &this->m_parametersoil, "soilparameters", _args) != LDNDC_ERR_OK)
        { return LDNDC_ERR_FAIL; }
    if ( s_FetchInput( &this->m_parameterspecies, "speciesparameters", _args) != LDNDC_ERR_OK)
        { return LDNDC_ERR_FAIL; }
    return LDNDC_ERR_OK;

}

lerr_t
ldndc::LK_Parameters::read( cbm::RunLevelArgs *  _args)
{
    lerr_t rc_read = LDNDC_ERR_OK;

    siteparameters::input_class_siteparameters_t const *  siteparameters_in =
            _args->iokcomm->get_input_class< siteparameters::input_class_siteparameters_t >();
    if ( siteparameters_in)
    {
        if ( s_UpdateInput( this->m_parametersite, _args) != LDNDC_ERR_OK)
            { rc_read = LDNDC_ERR_FAIL; }
    }

    soilparameters::input_class_soilparameters_t const *  soilparameters_in =
            _args->iokcomm->get_input_class< soilparameters::input_class_soilparameters_t >();
    if ( soilparameters_in)
    {
        if ( s_UpdateInput( this->m_parametersoil, _args) != LDNDC_ERR_OK)
            { rc_read = LDNDC_ERR_FAIL; }
    }

    speciesparameters::input_class_speciesparameters_t const *  speciesparameters_in =
            _args->iokcomm->get_input_class< speciesparameters::input_class_speciesparameters_t >();
    if ( speciesparameters_in)
    {
        if ( s_UpdateInput( this->m_parameterspecies, _args) != LDNDC_ERR_OK)
            { rc_read = LDNDC_ERR_FAIL; }
    }

    return rc_read;
}

