/*!
 * @author
 *    steffen klatt (created on: jul 28, 2017)
 */

#ifndef  LDNDC_KERNEL_LDPARAMETERS_H_
#define  LDNDC_KERNEL_LDPARAMETERS_H_

#include  "ld_kernel.h"
//TODO #include  "ld_shared.h"

namespace ldndc {

class LDNDC_API LK_Parameters : public cbm::kernel_t
{
    LDNDC_KERNEL_OBJECT(LK_Parameters,parameters)
    public:
        LK_Parameters();
        ~LK_Parameters();

    public:
        lerr_t  configure( cbm::RunLevelArgs *);
        lerr_t  read( cbm::RunLevelArgs *);

    protected:
        input_class_srv_base_t *  m_parametersite;
        input_class_srv_base_t *  m_parametersoil;
        input_class_srv_base_t *  m_parameterspecies;
    private:
        /* hide these buggers for now */
        LK_Parameters( LK_Parameters const &);
        LK_Parameters &  operator=( LK_Parameters const &);
};
} /* namespace ldndc */

#endif /* !LDNDC_KERNEL_LDPARAMETERS_H_ */

