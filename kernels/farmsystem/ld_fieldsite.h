/*!
 * @file
 * @author
 *    David Kraus
 */

#ifndef  LDNDC_KERNEL_FARMSYSTEM_FIELDSITE_H_
#define  LDNDC_KERNEL_FARMSYSTEM_FIELDSITE_H_

#include <cbm_errors.h>
#include <kernel/kbase.h>
#include  "ld_shared.h"
#include  "farmsystem/ld_vegetation_period.h"
#include  "farmsystem/ld_farmsystem_output.h"

namespace ldndc {

class FieldSite
{

public:

    FieldSite( cbm::io_kcomm_t * /* io communicator */,
               int /* field id */,
               cbm::string_t /* field name */,
               double /* field size */,
               std::vector< VegetationPeriod > /* vegetation periods */);
//    FieldSite( const FieldSite &);

    ~FieldSite();

    lerr_t initialize( cbm::io_kcomm_t *);
    lerr_t finalize();

    lerr_t  register_ports( cbm::io_kcomm_t *);
    lerr_t  unregister_ports();

private:

    SubscribedField<double> maturity_status;
    SubscribedField<double> growing_degree_days;

    SubscribedField<double> surface_water;
    SubscribedField<double> water_content_20cm;
    SubscribedField<double> field_capacity_20cm;

    SubscribedField<double> carbon_plant_biomass_total_exported_from_field;
    SubscribedField<double> carbon_plant_biomass_fruit_exported_from_field;
    SubscribedField<double> nitrogen_plant_biomass_total_exported_from_field;
    SubscribedField<double> nitrogen_plant_biomass_fruit_exported_from_field;

    cbm::string_t f_name;
    double f_size;
    unsigned int f_id;

    std::vector< VegetationPeriod > vegetation_periods;
    std::vector< VegetationPeriod > vegetation_schedule;

private:

    PublishedEvent m_TillEvent;
    lerr_t FieldSite_tilling(
                         cbm::RunLevelArgs * ,
                         VegetationPeriod &,
                         FarmSystemOutput *);

    PublishedEvent m_IrrigationEvent;
    lerr_t FieldSite_irrigation(
                            cbm::RunLevelArgs *,
                            VegetationPeriod &,
                            FarmSystemOutput *);

    PublishedEvent m_FloodEvent;
    lerr_t FieldSite_flooding(
                          cbm::RunLevelArgs *,
                          VegetationPeriod &,
                          FarmSystemOutput *);

    PublishedEvent m_FertilizeEvent;
    lerr_t FieldSite_fertilization(
                               cbm::RunLevelArgs *,
                               VegetationPeriod &,
                               FarmSystemOutput *);

    PublishedEvent m_ManureEvent;
    lerr_t FieldSite_manuring(
                          cbm::RunLevelArgs *,
                          VegetationPeriod &,
                          FarmSystemOutput *);

    PublishedEvent m_CutEvent;
    lerr_t FieldSite_cutting(
                          cbm::RunLevelArgs *,
                          VegetationPeriod &,
                          FarmSystemOutput *);

    PublishedEvent m_PlantEvent;
    lerr_t FieldSite_planting(
                          cbm::RunLevelArgs *,
                          VegetationPeriod &,
                          FarmSystemOutput *);

    PublishedEvent m_HarvestEvent;
    lerr_t FieldSite_harvest(
                         cbm::RunLevelArgs *,
                         VegetationPeriod &,
                         FarmSystemOutput *);

public:

    lerr_t schedule( cbm::RunLevelArgs *);
    lerr_t handle_field( cbm::RunLevelArgs *,
                         FarmSystemOutput *);

    inline cbm::string_t name(){ return f_name; };
};

} /* namespace ldndc */

#endif /* !LDNDC_KERNEL_FARMSYSTEM_FIELDSITE_H_ */
