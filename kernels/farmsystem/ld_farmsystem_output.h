/*!
 * @file
 * @author
 *    David Kraus
 */

#ifndef  FARMSYSTEM_OUTPUT_H_
#define  FARMSYSTEM_OUTPUT_H_

#include  "ld_sinkinterface.h"

namespace ldndc {

class  LDNDC_API  FarmSystemOutput
{
    public:
        FarmSystemOutput( cbm::io_kcomm_t * = NULL);
        ~FarmSystemOutput();

        lerr_t  configure( ldndc::config_file_t const *);

        lerr_t  initialize();
        lerr_t  step( cbm::sclock_t const *);
        lerr_t  finalize();

    double planting;
    double harvest;
    double tilling;
    double fertilization;
    double flooding;
    double irrigation;
    double cutting;

    private:
        lerr_t  collect_datarecord(
                                   ldndc_flt64_t *);

    private:
        cbm::io_kcomm_t *  io_kcomm;
    private:
        ldndc::sink_handle_t  m_sink;
        ldndc::SinkInterface  m_sif;
};

} /* namespace ldndc */

#endif  /*  !FARMSYSTEM_OUTPUT_H_  */

