/*!
 * @file
 * @author
 *    Steffen Klatt
 *    David Kraus
 */

#ifndef  LDNDC_KERNEL_FARMSYSTEM_DYNAMICVEGETATION_H_
#define  LDNDC_KERNEL_FARMSYSTEM_DYNAMICVEGETATION_H_

#include  "ld_kernel.h"
#include  "ld_eventqueue.h"
#include  <containers/lvector.h>
#include  <json/cbm_json.h>

namespace ldndc {

class VegetationPeriod
{
private:

   static int nd_vegetation_periods;

public:

    struct tilling_t
    {
        tilling_t();
        int day_of_application;
        int days_of_application_after_harvest;
        double depth;
    };

    struct irrigation_t
    {
        irrigation_t();
        int day_of_application;
        double irrigation_height;
        double fraction_dvs_min_of_application;
        double fraction_dvs_max_of_application;
        double fraction_field_capacity;
        bool is_reservoir;
        std::vector< int > years;
    };

    enum event_status_e
    {
        RECEIVABLE,
        APPOINTED,
        INITIALIZED,
        STARTED,
        CONTINUING,
        FINALIZED,
        DONE
    };
    
    struct flooding_t
    {
        flooding_t();
        int start_static;
        int end_static;
        double start_dynamic;
        double end_dynamic;
        double irrigation_height;
        double watertable;
        double bundheight;
        double percolation_rate;
        bool drainage;
        bool unlimited_water;
        double saturation_level;
        double soil_depth;
        event_status_e status;
    };

    struct fertilizer_t
    {
        fertilizer_t();
        int day_of_application;
        double fraction_dvs_min_of_application;
        double fraction_dvs_max_of_application;
        double fraction_field_capacity;
        cbm::string_t name;
        double c_amount;
        double n_amount;
        double depth;
        cbm::string_t water_management;
        double irrigation_height;
        std::vector< int > years;
        event_status_e status;
    };

    struct planting_t
    {
        int day_of_application;
        cbm::string_t name_base;
        cbm::string_t name_extension;
        cbm::string_t group;
        inline cbm::string_t name() { return name_base + name_extension; };
        double initialbiomass;
        double height;
        double remains_relative;
        int days_to_maturity;
        int days_to_latest_harvest;
        int days_on_field_after_maturity;
        int gdd_min;
        int gdd_max;
        std::vector< int > years;
    };

    struct cutting_t
    {
        cutting_t();
        int day_of_cut;
        double remains_absolute;
        double height;
        double export_fruit;
        double export_foliage;
        double export_living_structural_tissue;
        double export_dead_structural_tissue;
        double export_root;
    };

public:

    VegetationPeriod(
                 std::vector< int > const &,
                 size_t /* day of year of start */,
                 planting_t /* planting event */,
                 std::vector< tilling_t > const & /* tilling events */,
                 std::vector< fertilizer_t > const & /* fertilization events */,
                 std::vector< fertilizer_t > const & /* manure events */,
                 double /* irrigation reservoir */,
                 std::vector< irrigation_t > const & /* irrigation events */,
                 std::vector< flooding_t > const & /* flooding events */,
                 std::vector< cutting_t > const & /* cutting events */,
                 size_t /* dynamic parametrization */);

    ~VegetationPeriod();

    /* considered years for vegetation period */
    std::vector< int > years;

    /*! start of vegetation period */
    size_t start;

    planting_t planting_event;

    std::vector< tilling_t > tilling_events;
    std::vector< irrigation_t > irrigation_events;
    std::vector< flooding_t > flooding_events;
    std::vector< fertilizer_t > fertilization_events;
    std::vector< fertilizer_t > manure_events;
    std::vector< cutting_t > cutting_events;

    /* */
    long long int seconds_at_field_preparation;

    /* simulation time in seconds at planting event */
    long long int seconds_at_planting;

    /* simulation time in second at maturity */
    long long int seconds_at_maturity;

    /* simulation time in second at maturity */
    long long int seconds_at_harvest;

    double gdd_at_anticipated_maturity;

    double gdd_target;
    bool harvested;

    size_t nd_till_events;
    
    double irrigation_reservoir;
    double irrigation_reservoir_withdrawal;
    
    size_t dynamic_parametrization;

    int id_vegetation_period;
    
public:

    int inline id(){ return id_vegetation_period; };

    lerr_t
    assign_tilling_event( tilling_t*,
                          int /* current day */,
                          bool _pop = true);

    lerr_t
    assign_irrigation_event( irrigation_t*,
                             int /* current day */,
                             double /* maturity_status */,
                             double /* soil water content */,
                             double /* field capacity */,
                             bool _pop = true);

    lerr_t
    assign_flooding_event( std::vector< VegetationPeriod::flooding_t > &,
                           int /* current day */,
                           double /* maturity status */,
                           bool _pop = true);

    lerr_t
    assign_fertilization_event( fertilizer_t*,
                                int /* current day */,
                                int /* current year */,
                                double /* maturity status */,
                                double /* surface water */,
                                double /* soil water content */,
                                double /* field capacity */,
                                bool _pop = true);

    lerr_t
    assign_manuring_event( fertilizer_t*,
                           int /* current day */,
                           int /* current year */,
                           double /* maturity status */,
                           double /* surface water */,
                           double /* soil water content */,
                           double /* field capacity */,
                           bool _pop = true);

    lerr_t
    assign_cutting_event( cutting_t*,
                                int /* current day */,
                                bool _pop = true);

    lerr_t
    assign_planting_event(
                          planting_t*,
                          int /* current day */,
                          long long int /* current second */);

    lerr_t
    assign_harvest_event(
                         planting_t*,
                         int * /* vegetation period id */,
                         double * /* target of growing degree days for harvest */,
                         cbm::sclock_t const * /* clock */,
                         double /* growing degree days */,
                         double /* maturity status */);

    /* returns days calculated from seconds */
    int days_since(
                   long long int /* seconds */,
                   cbm::sclock_t const * /* clock */);
};


} /* namespace ldndc */

#endif /* !LDNDC_KERNEL_FARMSYSTEM_DYNAMICVEGETATION_H_ */
