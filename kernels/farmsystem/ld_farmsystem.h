/*!
 * @file
 * @author
 *    David Kraus
 */

#ifndef  LDNDC_KERNEL_FARMSYSTEM_H_
#define  LDNDC_KERNEL_FARMSYSTEM_H_

#include  "ld_kernel.h"
#include  "farmsystem/ld_fieldsite.h"
#include  "farmsystem/ld_farmsystem_output.h"

namespace ldndc {

class LDNDC_API FarmSystem : public cbm::kernel_t
{
    LDNDC_KERNEL_OBJECT(FarmSystem,farmsystem)

public:
    FarmSystem();
    ~FarmSystem();

    lerr_t  configure( cbm::RunLevelArgs *);
    lerr_t  initialize( cbm::RunLevelArgs *);
    lerr_t  solve( cbm::RunLevelArgs *);
    lerr_t  finalize( cbm::RunLevelArgs *);

    lerr_t  register_ports( cbm::RunLevelArgs *);
    lerr_t  unregister_ports( cbm::RunLevelArgs *);

private:
    
    int event_id;

    static cbm::string_t data_file;
    static std::string data_file_content;

    lerr_t read_file( cbm::RunLevelArgs *);
    
    std::vector< VegetationPeriod::tilling_t >
    read_tilling( cbm::jquery_t _ji,
                  cbm::string_t _basequery,
                  int _field,
                  int _pi);

    std::vector< VegetationPeriod::irrigation_t >
    read_irrigation( cbm::jquery_t _ji,
                     cbm::string_t _basequery,
                     int _field,
                     int _pi);

    std::vector< VegetationPeriod::flooding_t >
    read_flooding( cbm::jquery_t _ji,
                   cbm::string_t _basequery,
                   int _field,
                   int _pi);

    std::vector< VegetationPeriod::fertilizer_t >
    read_fertilization( cbm::jquery_t _ji,
                        cbm::string_t _basequery,
                        int _field,
                        int _pi);

    std::vector< VegetationPeriod::fertilizer_t >
    read_manuring( cbm::jquery_t _ji,
                   cbm::string_t _basequery,
                   int _field,
                   int _pi);

    std::vector< VegetationPeriod::cutting_t >
    read_cutting( cbm::jquery_t _ji,
                   cbm::string_t _basequery,
                   int _field,
                   int _pi);

    std::vector< FieldSite > fieldsites;

public:
    /*!
     * @brief
     *  wrapper for kernel-specific output sink; attach/detach and
     *  send to data sink.
     */
    FarmSystemOutput  m_output;

private:

    /* hide these buggers for now */
    FarmSystem( FarmSystem const &);
    FarmSystem &  operator=( FarmSystem const &);
};
} /* namespace ldndc */

#endif /* !LDNDC_KERNEL_FARMSYSTEM_H_ */

