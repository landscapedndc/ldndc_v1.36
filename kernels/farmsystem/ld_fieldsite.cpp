/*!
 * @file
 * @author
 *    David Kraus
 */


#include "farmsystem/ld_fieldsite.h"
#include <array>


/*!
 * @page ldndc_farmsystem
 * @section farmsystem_field Field site
 * A field site includes the following attributes:
 * - \em id: Identifier of the setup/gridcell responsable for the ecosystem simulation
 * - \em name: A name identifier
 * - \em area: The size in [m2] of the field (default: one hectare or 10000 m2)
 * - \em dynamic_parametrization: Integer number 0 (no dynamic parametrization),1 (one adjustment after first harvest), or larger (continuous)
 * - \em periods: list of cropping seasons (see below)
 */
ldndc::FieldSite::FieldSite( cbm::io_kcomm_t *,
                             int _field_id,
                             cbm::string_t _field_name,
                             double _field_size,
                             std::vector< VegetationPeriod > _dvp):
                        f_name( _field_name),
                        f_size( _field_size),
                        f_id( _field_id),
                        vegetation_periods( _dvp),
                        m_TillEvent(),
                        m_IrrigationEvent(),
                        m_FloodEvent(),
                        m_FertilizeEvent(),
                        m_ManureEvent(),
                        m_CutEvent(),
                        m_PlantEvent(),
                        m_HarvestEvent()
{}


ldndc::FieldSite::~FieldSite()
{}


lerr_t
ldndc::FieldSite::register_ports( cbm::io_kcomm_t *  _iokcomm)
{
    maturity_status.subscribe( "MaturityStatus", "-", _iokcomm);
    growing_degree_days.subscribe( "GrowingDegreeDays", "-", _iokcomm);

    surface_water.subscribe( "SurfaceWater", "m", _iokcomm);
    water_content_20cm.subscribe( "WaterContent20cm", "m3/m3", _iokcomm);
    field_capacity_20cm.subscribe( "FieldCapacity20cm", "m3/m3", _iokcomm);

    carbon_plant_biomass_total_exported_from_field.subscribe( "carbon_plant_biomass_total_exported_from_field", "kg/m2", f_id, _iokcomm);
    carbon_plant_biomass_fruit_exported_from_field.subscribe( "carbon_plant_biomass_fruit_exported_from_field", "kg/m2", f_id, _iokcomm);
    nitrogen_plant_biomass_total_exported_from_field.subscribe( "nitrogen_plant_biomass_total_exported_from_field", "kg/m2", f_id, _iokcomm);
    nitrogen_plant_biomass_fruit_exported_from_field.subscribe( "nitrogen_plant_biomass_fruit_exported_from_field", "kg/m2", f_id, _iokcomm);

    m_TillEvent.publish( "till", f_id, _iokcomm);
    m_IrrigationEvent.publish( "irrigate", f_id, _iokcomm);
    m_FloodEvent.publish( "flood", f_id, _iokcomm);
    m_FertilizeEvent.publish( "fertilize", f_id, _iokcomm);
    m_ManureEvent.publish( "manure", f_id, _iokcomm);
    m_CutEvent.publish( "cut", f_id, _iokcomm);
    m_PlantEvent.publish( "plant", f_id, _iokcomm);
    m_HarvestEvent.publish( "harvest", f_id, _iokcomm);

    return LDNDC_ERR_OK;
}


lerr_t
ldndc::FieldSite::unregister_ports()
{
    maturity_status.unsubscribe();
    growing_degree_days.unsubscribe();

    surface_water.unsubscribe();
    water_content_20cm.unsubscribe();
    field_capacity_20cm.unsubscribe();
    
    carbon_plant_biomass_total_exported_from_field.unsubscribe();
    carbon_plant_biomass_fruit_exported_from_field.unsubscribe();
    nitrogen_plant_biomass_total_exported_from_field.unsubscribe();
    nitrogen_plant_biomass_fruit_exported_from_field.unsubscribe();

    m_TillEvent.unpublish();
    m_IrrigationEvent.unpublish();
    m_FloodEvent.unpublish();
    m_FertilizeEvent.unpublish();
    m_ManureEvent.unpublish();
    m_CutEvent.unpublish();
    m_PlantEvent.unpublish();
    m_HarvestEvent.unpublish();

    return LDNDC_ERR_OK;
}


lerr_t
ldndc::FieldSite::initialize( cbm::io_kcomm_t *_io)
{
    register_ports( _io);
    return LDNDC_ERR_OK;
}


lerr_t
ldndc::FieldSite::finalize()
{
    unregister_ports();
    return LDNDC_ERR_OK;
}


/*!
 * @details
 * Each first subday of each day, vegetation periods are scheduled
 * that meet the following criteria:
 * - if years are specified: year of vegetation year matches current year
 * - start of vegetation period matches current day of year
 */
lerr_t
ldndc::FieldSite::schedule(cbm::RunLevelArgs *_args)
{
    if ( _args->clk->subday() == 1)
    {
        for ( size_t vi = 0; vi < vegetation_periods.size(); ++vi)
        {
            /* vegetation periods need to be defined for at least one year */
            for (size_t vp_yr = 0; vp_yr < vegetation_periods[vi].years.size(); ++vp_yr)
            {
                /* either year matches or year equals -1 which means any year is valid */
                if ( (vegetation_periods[vi].years[vp_yr] == (int)_args->clk->year()) ||
                     (vegetation_periods[vi].years[vp_yr] == -1))
                {
                    for (size_t pl_yr = 0; pl_yr < vegetation_periods[vi].planting_event.years.size(); ++pl_yr)
                    {
                        if ( (vegetation_periods[vi].planting_event.years[pl_yr] == (int)_args->clk->year()) ||
                             (vegetation_periods[vi].planting_event.years[pl_yr] == -1))
                        {
                            /* schedule vegetation period as soon as day of year is reached */
                            if ( vegetation_periods[vi].start == _args->clk->yearday())
                            {
                                vegetation_schedule.push_back( vegetation_periods[vi]);
                                
                                /* remove year from list */
                                if (vegetation_periods[vi].years[vp_yr] != -1)
                                {
                                    vegetation_periods[vi].years.erase( vegetation_periods[vi].years.begin() + vp_yr);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    return LDNDC_ERR_OK;
}


/*!
 * @details
 * For the current scheduled vegetation period the following events are considered:
 * - Tilling
 * - Irrigation
 * - Flooding
 * - Fertilization
 * - Manuring
 * - Cutting
 * - Planting
 * - Harvesting
 */
lerr_t
ldndc::FieldSite::handle_field( cbm::RunLevelArgs *_args,
                                FarmSystemOutput *_output)
{
    /* active vegetation period */
    if ( vegetation_schedule.size() > 0)
    {
        VegetationPeriod *vi = &vegetation_schedule.front();

        /* start vegetation period */
        if ( vi->seconds_at_field_preparation < 0)
        {
            vi->seconds_at_field_preparation = _args->clk->seconds();
        }

        /** till event **/
        FieldSite_tilling( _args, *vi, _output);

        /** irrigation event **/
        FieldSite_irrigation( _args, *vi, _output);

        /** flood event **/
        FieldSite_flooding( _args, *vi, _output);

        /** fertilize event **/
        FieldSite_fertilization( _args, *vi, _output);

        /** manure event **/
        FieldSite_manuring( _args, *vi, _output);

        /** cutting event **/
        FieldSite_cutting( _args, *vi, _output);

        /** plant event **/
        FieldSite_planting( _args, *vi, _output);

        /** harvest event **/
        FieldSite_harvest( _args, *vi, _output);

        /* remove last vegetation after harvest */
        if ( (vi->days_since( vi->seconds_at_harvest, _args->clk) > 1) &&
             (vi->nd_till_events == vi->tilling_events.size()))
        {
            vegetation_schedule.erase( vegetation_schedule.begin());
        }
    }

    return LDNDC_ERR_OK;
}


lerr_t
ldndc::FieldSite::FieldSite_tilling(
                                    cbm::RunLevelArgs * _args,
                                    VegetationPeriod &_dvp,
                                    FarmSystemOutput *_output)
{
    int day_of_vp( _dvp.days_since( _dvp.seconds_at_field_preparation, _args->clk));
    VegetationPeriod::tilling_t ev_till;
    lerr_t rc = _dvp.assign_tilling_event(
                             &ev_till,
                             day_of_vp);
    if ( rc == LDNDC_ERR_EVENT_MATCH)
    {
        ldndc::PublishedEvent::EventAttributes  event_attrs;
        event_attrs.set( "/depth", ev_till.depth);
        m_TillEvent.send( event_attrs);
        _output->tilling += 1.0;
    }

    return LDNDC_ERR_OK;
}


lerr_t
ldndc::FieldSite::FieldSite_irrigation(
                                       cbm::RunLevelArgs * _args,
                                       VegetationPeriod &_dvp,
                                       FarmSystemOutput *_output)
{
    int day_of_vp( _dvp.days_since( _dvp.seconds_at_field_preparation, _args->clk));
    VegetationPeriod::irrigation_t ev_irri;
    lerr_t rc = _dvp.assign_irrigation_event(
                             &ev_irri,
                             day_of_vp,
                             maturity_status.receive(),
                             water_content_20cm.receive(),
                             field_capacity_20cm.receive());
    if ( rc == LDNDC_ERR_EVENT_MATCH)
    {
        ldndc::PublishedEvent::EventAttributes  event_attrs;
        event_attrs.set( "/amount", ev_irri.irrigation_height);
        event_attrs.set( "/is-reservoir", ev_irri.is_reservoir);
        m_IrrigationEvent.send( event_attrs);
        _output->irrigation += 1.0;
    }

    /* Continuous filling of irrigation reservoir */
    if ( cbm::flt_greater_zero( _dvp.irrigation_reservoir))
    {
        int const length_of_vp( _dvp.planting_event.day_of_application + _dvp.planting_event.days_to_maturity);
        double dvp_progress( ((double)day_of_vp) / ((double)length_of_vp));
        
        std::array<double, 10> irrigation_steps = { 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0 };
        size_t step( 0);
        for ( ; step < irrigation_steps.size()-1; ++step)
        {
            if ( !cbm::flt_greater( dvp_progress, irrigation_steps[step]))
            {
                break;
            }
        }

        double pot_irrigation_reservoir_withdrawal( irrigation_steps[step] * _dvp.irrigation_reservoir);
        if ( cbm::flt_greater( pot_irrigation_reservoir_withdrawal, _dvp.irrigation_reservoir_withdrawal))
        {
            double const irrigation_height( pot_irrigation_reservoir_withdrawal -
                                            _dvp.irrigation_reservoir_withdrawal);

            VegetationPeriod::irrigation_t add_irrigation;
            add_irrigation.day_of_application = day_of_vp + 1;
            add_irrigation.irrigation_height = irrigation_height;
            add_irrigation.is_reservoir = true;

            _dvp.irrigation_events.push_back( add_irrigation);
            _dvp.irrigation_reservoir_withdrawal += irrigation_height;
        }
    }

    return LDNDC_ERR_OK;
}


lerr_t
ldndc::FieldSite::FieldSite_flooding(
                                     cbm::RunLevelArgs * _args,
                                     VegetationPeriod &_dvp,
                                     FarmSystemOutput *_output)
{
    int day_of_vp( _dvp.days_since( _dvp.seconds_at_field_preparation, _args->clk));
    std::vector< VegetationPeriod::flooding_t > ev_flood_v;
    lerr_t rc = _dvp.assign_flooding_event(
                                       ev_flood_v,
                                       day_of_vp,
                                       maturity_status.receive());
    if ( rc == LDNDC_ERR_EVENT_MATCH)
    {
        for (size_t i = 0; i < ev_flood_v.size(); ++i)
        {
            ldndc::PublishedEvent::EventAttributes  event_attrs;
            event_attrs.set( "/watertable", ev_flood_v[i].watertable);
            event_attrs.set( "/bund-height", ev_flood_v[i].bundheight);
            event_attrs.set( "/percolation-rate", ev_flood_v[i].percolation_rate);
            event_attrs.set( "/irrigation-height", ev_flood_v[i].irrigation_height);
            event_attrs.set( "/unlimited-water", ev_flood_v[i].unlimited_water);
            event_attrs.set( "/saturation-level", ev_flood_v[i].saturation_level);
            event_attrs.set( "/soil-depth", ev_flood_v[i].soil_depth);
            m_FloodEvent.send( event_attrs);
            _output->flooding += 1.0;
        }
    }

    return LDNDC_ERR_OK;
}


lerr_t
ldndc::FieldSite::FieldSite_fertilization(
                                          cbm::RunLevelArgs * _args,
                                          VegetationPeriod &_dvp,
                                          FarmSystemOutput *_output)
{
    int day_of_vp( _dvp.days_since( _dvp.seconds_at_field_preparation, _args->clk));
    VegetationPeriod::fertilizer_t ev_fert;
    lerr_t rc = _dvp.assign_fertilization_event(
                                            &ev_fert,
                                            day_of_vp,
                                            _args->clk->year(),
                                            maturity_status.receive(),
                                            surface_water.receive(),
                                            water_content_20cm.receive(),
                                            field_capacity_20cm.receive());

    if ( rc == LDNDC_ERR_EVENT_MATCH)
    {
        double scale = f_size / cbm::M2_IN_HA;
        ldndc::PublishedEvent::EventAttributes  event_attrs;
        event_attrs.set( "/type", ev_fert.name.c_str());
        event_attrs.set( "/amount", ev_fert.n_amount * scale);
        event_attrs.set( "/depth", ev_fert.depth);
        m_FertilizeEvent.send( event_attrs);
        _output->fertilization += 1;
    }

    return LDNDC_ERR_OK;
}


lerr_t
ldndc::FieldSite::FieldSite_manuring(
                                     cbm::RunLevelArgs * _args,
                                     VegetationPeriod &_dvp,
                                     FarmSystemOutput *_output)
{
    int day_of_vp( _dvp.days_since( _dvp.seconds_at_field_preparation, _args->clk));
    VegetationPeriod::fertilizer_t ev_fert;
    lerr_t rc = _dvp.assign_manuring_event(
                                       &ev_fert,
                                       day_of_vp,
                                       _args->clk->year(),
                                       maturity_status.receive(),
                                       surface_water.receive(),
                                       water_content_20cm.receive(),
                                       field_capacity_20cm.receive());
    if ( rc == LDNDC_ERR_EVENT_MATCH)
    {
        double scale = f_size / cbm::M2_IN_HA;
        ldndc::PublishedEvent::EventAttributes  event_attrs;
        event_attrs.set( "/type", ev_fert.name.c_str());
        event_attrs.set( "/carbon", ev_fert.c_amount * scale);
        event_attrs.set( "/carbon-nitrogen-ratio", ev_fert.c_amount / ev_fert.n_amount);
        event_attrs.set( "/depth", ev_fert.depth);
        m_ManureEvent.send( event_attrs);
        _output->fertilization += 1;
    }

    return LDNDC_ERR_OK;
}

lerr_t
ldndc::FieldSite::FieldSite_cutting(
                                    cbm::RunLevelArgs * _args,
                                    VegetationPeriod &_dvp,
                                    FarmSystemOutput *_output)
{
    int day_of_vp( _dvp.days_since( _dvp.seconds_at_field_preparation, _args->clk));
    VegetationPeriod::cutting_t ev_cut;
    lerr_t rc = _dvp.assign_cutting_event(
                             &ev_cut,
                             day_of_vp);
    if ( rc == LDNDC_ERR_EVENT_MATCH)
    {
        ldndc::PublishedEvent::EventAttributes  event_attrs;
        event_attrs.set( "/remains_absolute", ev_cut.remains_absolute);
        event_attrs.set( "/height", ev_cut.height);
        event_attrs.set( "/export_fruit", ev_cut.export_fruit);
        event_attrs.set( "/export_foliage", ev_cut.export_foliage);
        event_attrs.set( "/export_living_structural_tissue", ev_cut.export_living_structural_tissue);
        event_attrs.set( "/export_dead_structural_tissue", ev_cut.export_dead_structural_tissue);
        event_attrs.set( "/export_root", ev_cut.export_root);
        m_CutEvent.send( event_attrs);
        _output->cutting += 1.0;
    }

    return LDNDC_ERR_OK;
}


lerr_t
ldndc::FieldSite::FieldSite_planting(
                                     cbm::RunLevelArgs *_args,
                                     VegetationPeriod &_dvp,
                                     FarmSystemOutput *_output)
{
    int day_of_vp( _dvp.days_since( _dvp.seconds_at_field_preparation, _args->clk));
    VegetationPeriod::planting_t ev_plant;
    lerr_t rc = _dvp.assign_planting_event(
                                       &ev_plant,
                                       day_of_vp,
                                       _args->clk->seconds());
    if ( rc == LDNDC_ERR_EVENT_MATCH)
    {
        ldndc::PublishedEvent::EventAttributes  event_attrs;
        event_attrs.set( "/name", ev_plant.name().c_str());
        event_attrs.set( "/type", ev_plant.name().c_str());
        event_attrs.set( "/group", ev_plant.group.c_str());
        event_attrs.set( "/initial-biomass", ev_plant.initialbiomass);
        m_PlantEvent.send( event_attrs);
        _output->planting += 1;
    }

    return LDNDC_ERR_OK;
}


lerr_t
ldndc::FieldSite::FieldSite_harvest(
                                    cbm::RunLevelArgs * _args,
                                    VegetationPeriod &_dvp,
                                    FarmSystemOutput *_output)
{
    VegetationPeriod::planting_t ev_harvest;
    int dvp_id( -1);
    double gdd_target( invalid_dbl);
    lerr_t rc = _dvp.assign_harvest_event(
                                      &ev_harvest,
                                      &dvp_id,
                                      &gdd_target,
                                      _args->clk,
                                      growing_degree_days.receive(),
                                      maturity_status.receive());
    if ( rc == LDNDC_ERR_EVENT_MATCH)
    {
        ldndc::PublishedEvent::EventAttributes  event_attrs;
        event_attrs.set( "/name", ev_harvest.name().c_str());
        event_attrs.set( "/height", ev_harvest.height);
        event_attrs.set( "/remains_relative", ev_harvest.remains_relative);
        m_HarvestEvent.send( event_attrs);
        _output->harvest += 1;

        //find correct vegetation period and adapt planted species
        if ( (dvp_id >= 0) && cbm::is_valid( gdd_target))
        {
            for ( size_t vi = 0; vi < vegetation_periods.size(); ++vi)
            {
                if ( cbm::is_equal_i( vegetation_periods[vi].planting_event.name_base.c_str(),
                                      _dvp.planting_event.name_base.c_str()) &&
                     (vegetation_periods[vi].dynamic_parametrization > 0) &&
                     (vegetation_periods[vi].start == _dvp.start))
                {
                    // after first season gdd_target is fully adjusted
                    if ( cbm::flt_less( vegetation_periods[vi].gdd_target, 0.0))
                    {
                        vegetation_periods[vi].gdd_target = gdd_target;
                    }
                    // for following seasons gdd_target is gradually adjusted in order to not
                    // over-interpret outlier seasons with very low or high temperatures
                    else if ( vegetation_periods[vi].dynamic_parametrization > 1)
                    {
                        vegetation_periods[vi].gdd_target -= (vegetation_periods[vi].gdd_target - gdd_target) * 0.5;
                    }

                    //round gdd target to nearest 100, ensuring that it is within allowed range
                    int gdd_target_rnd( 100 * cbm::round( vegetation_periods[vi].gdd_target / 100));
                    if ( gdd_target_rnd < vegetation_periods[vi].planting_event.gdd_min)
                    {
                        gdd_target_rnd = vegetation_periods[vi].planting_event.gdd_min;
                    }
                    else if ( gdd_target_rnd > vegetation_periods[vi].planting_event.gdd_max)
                    {
                        gdd_target_rnd = vegetation_periods[vi].planting_event.gdd_max;
                    }

                    //set name extension for crop to plant in subsequent year
                    char name_ext[6];
                    cbm::snprintf(name_ext, 6, "_%d", gdd_target_rnd);
                    vegetation_periods[vi].planting_event.name_extension = name_ext;
                }
            }
        }
    }

    return LDNDC_ERR_OK;
}
