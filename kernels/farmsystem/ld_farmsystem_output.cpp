/*!
 * @file
 * @author
 *    David Kraus
 */

#include  "farmsystem/ld_farmsystem_output.h"
#include  <kernel/io-kcomm.h>

#define  LMOD_OUTPUT_MODULE_BASE  OutputModuleBase


/*!
 * @page ldndc_farmsystem
 * @section farmsystem_output Output
 * ...
 */
static ldndc_string_t const  FarmSystemOutput_Ids[] =
{
    "planting",
    "harvest",
    "tilling",
    "fertilization",
    "flooding",
    "irrigation",
    "cutting"
};

static ldndc_string_t const *  FarmSystemOutput_Header = FarmSystemOutput_Ids;

#define  FarmSystemOutput_Datasize  (sizeof( FarmSystemOutput_Ids) / sizeof( FarmSystemOutput_Ids[0]))

static ldndc_output_size_t const  FarmSystemOutput_Sizes[] =
{
    FarmSystemOutput_Datasize,
    FarmSystemOutput_Datasize /*total size*/
};

static ldndc_output_size_t const *  FarmSystemOutput_EntitySizes = NULL;

#define  FarmSystemOutput_Rank  ((ldndc_output_rank_t)(sizeof( FarmSystemOutput_Sizes) / sizeof( FarmSystemOutput_Sizes[0])) - 1)
static atomic_datatype_t const  FarmSystemOutput_Types[] =
{
    LDNDC_FLOAT64
};


ldndc::FarmSystemOutput::FarmSystemOutput( cbm::io_kcomm_t *  _io_kcomm)
: io_kcomm( _io_kcomm),
m_sif( _io_kcomm)
{ }


ldndc::FarmSystemOutput::~FarmSystemOutput()
{ }


lerr_t
ldndc::FarmSystemOutput::configure(
                                   ldndc::config_file_t const *  _cf)
{
    lerr_t  rc_setflags = this->m_sif.set_metaflags(
                                                    _cf, "farmsystem", RM_CLIENTSOURCE|RM_CLIENTID|RM_DATETIME);
    if ( rc_setflags)
    { return LDNDC_ERR_FAIL; }

    return  LDNDC_ERR_OK;
}


lerr_t
ldndc::FarmSystemOutput::initialize()
{
    this->m_sink = this->io_kcomm->sink_handle_acquire( "farmsystem");
    if ( this->m_sink.status() == LDNDC_ERR_OK)
    {
        lerr_t  rc_layout = LDNDC_OUTPUT_DEFINE_SINK_LAYOUT(
                                                            this->m_sink,FarmSystemOutput,this->m_sif.get_metaflags());
        if ( rc_layout)
        { return  LDNDC_ERR_FAIL; }
    }
    else
    {
        LOGVERBOSE( "no such sink; not producing output  [sink=","farmsystem","]");
    }

    planting = 0.0;
    harvest = 0.0;
    tilling = 0.0;
    fertilization = 0.0;
    flooding = 0.0;
    irrigation = 0.0;
    cutting = 0.0;

    return  LDNDC_ERR_OK;
}

lerr_t
ldndc::FarmSystemOutput::step( cbm::sclock_t const *  _clock)
{
    if ( !this->m_sink.is_acquired())
    { return  LDNDC_ERR_OK; }

    ldndc_flt64_t  data_flt64_0[FarmSystemOutput_Datasize];
    lerr_t  rc_dump = collect_datarecord( data_flt64_0);
    if ( rc_dump)
    { return  LDNDC_ERR_FAIL; }

    void *  data[] = { data_flt64_0};
    lerr_t  rc_write =
    this->m_sif.write_fixed_record(
                                   &this->m_sink, data, _clock);
    if ( rc_write)
    { return  LDNDC_ERR_FAIL; }

    planting = 0.0;
    harvest = 0.0;
    tilling = 0.0;
    fertilization = 0.0;
    flooding = 0.0;
    irrigation = 0.0;
    cutting = 0.0;

    return  LDNDC_ERR_OK;
}

lerr_t
ldndc::FarmSystemOutput::finalize()
{
    if ( io_kcomm && m_sink.is_acquired())
    {
        io_kcomm->sink_handle_release( &m_sink);
    }
    return  LDNDC_ERR_OK;
}

#define  FarmSystemPut(__x__) { *_buf=(__x__); ++_buf; }
lerr_t
ldndc::FarmSystemOutput::collect_datarecord(
                                            ldndc_flt64_t *  _buf)
{
    FarmSystemPut( planting);
    FarmSystemPut( harvest);
    FarmSystemPut( tilling);
    FarmSystemPut( fertilization);
    FarmSystemPut( flooding);
    FarmSystemPut( irrigation);
    FarmSystemPut( cutting);

    return  LDNDC_ERR_OK;
}
#undef  FarmSystemPut

#undef  FarmSystemOutput_Rank
#undef  FarmSystemOutput_Datasize

