/*!
 * @file
 * @author
 *    David Kraus
 *    Steffen Klatt
 */

#include  "ld_farmsystem.h"
#include  "ld_vegetation_period.h"
#include  <logging/cbm_logging.h>


lerr_t
ldndc::FarmSystem::read_file(
                             cbm::RunLevelArgs *_args)
{
    cbm::jquery_t  ji( data_file_content.c_str());
    if ( ! ji.parse_ok())
    {
        LOGERROR("Deficient json format [", data_file,"]");
        return LDNDC_ERR_FAIL;
    }

    cbm::string_t basequery( "/");
    basequery << event_id;

    cbm::string_t query = basequery;
    query << "/fields/";
    int const nd_fields( ji.query_size( query.c_str()));

    for ( int field = 0; field < nd_fields; ++field)
    {
        /* field id */
        query = basequery;
        query << "/fields/" << field << "/id";
        int field_id = ji.query_int( query.c_str(), -1);

        /* field name */
        query = basequery;
        query << "/fields/" << field << "/name";
        cbm::string_t field_name = ji.query_string( query.c_str(), "-");

        /* field size */
        query = basequery;
        query << "/fields/" << field << "/area";
        double field_size = ji.query_double_and_int( query.c_str(), cbm::M2_IN_HA);

        query = basequery;
        query << "/fields/" << field << "/years";
        int const nd_years_field( ji.query_size( query.c_str()));

        std::vector< int > years_field;
        if ( nd_years_field > 0)
        {
            for ( int yr = 0; yr < nd_years_field; ++yr)
            {
                query = basequery;
                query << "/fields/" << field << "/years/" << yr;
                years_field.push_back( ji.query_int( query.c_str(), -1));
            }
        }
        else
        {
            years_field.push_back( -1);
        }

        /* default dynamic parametrization for all vegetation periods, may be overwritten by vegetation period specific value */
        query = basequery;
        query << "/fields/" << field << "/dynamic_parametrization";
        int dynamic_parametrization_field = ji.query_int( query.c_str(), 0);

        query = basequery;
        query << "/fields/" << field << "/periods/";
        int const nd_periods( ji.query_size( query.c_str()));

        std::vector< VegetationPeriod > vegetation_periods;
        for (int pi = 0; pi < nd_periods; ++pi)
        {
            query = basequery;
            query << "/fields/" << field << "/periods/" << pi << "/start";
            int const doy_start( ji.query_int( query.c_str(), -1));

            /* skip vegetation period if no start defined */
            if ( doy_start < 0)
            {
                LOGWARN("No start defined for vegetation period: ", event_id);
                continue;
            }

            query = basequery;
            query << "/fields/" << field << "/periods/" << pi << "/years";
            int const nd_years_periods( ji.query_size( query.c_str()));

            std::vector< int > years_periods;
            if ( nd_years_periods > 0)
            {
                for (int yr = 0; yr < nd_years_periods; ++yr)
                {
                    query = basequery;
                    query << "/fields/" << field << "/periods/" << pi << "/years/" << yr;
                    years_periods.push_back( ji.query_int( query.c_str(), -1));
                }

                //nd_years_field > 0 implies field is handled only for user-defined years
                if ( nd_years_field > 0)
                {
                    std::vector<int>::iterator it = years_periods.begin();
                    while ( it != years_periods.end())
                    {
                        //check if year of period is NOT in years of field handling
                        if ( std::find( years_field.begin(), years_field.end(), *it)
                             == years_field.end())
                        {
                            //remove year of period if NOT in years of field handling
                            it = years_periods.erase( it);
                            continue;
                        }
                        it++;
                    }
                }
            }
            else
            {
                years_periods = years_field;
            }

            /* dynamic parametrization */

            query = basequery;
            query << "/fields/" << field << "/periods/" << pi << "/dynamic_parametrization";
            int dynamic_parametrization_vp = ji.query_int( query.c_str(), -1);
            size_t dynamic_parametrization( (dynamic_parametrization_vp >= 0) ?
                                                   dynamic_parametrization_vp :         //use vegetation period specific if given
                                                   dynamic_parametrization_field);      //fall back to default

            /* planting information*/

            VegetationPeriod::planting_t add_planting;

            query = basequery;
            query << "/fields/" << field << "/periods/" << pi << "/planting/day";
            add_planting.day_of_application = ji.query_int( query.c_str(), 1);

            query = basequery;
            query << "/fields/" << field << "/periods/" << pi << "/planting/name";
            add_planting.name_base = ji.query_string( query.c_str(), "-");
            add_planting.name_extension = "";

            query = basequery;
            query << "/fields/" << field << "/periods/" << pi << "/planting/group";
            add_planting.group = ji.query_string( query.c_str(), "?");
            if ( cbm::is_equal( add_planting.group.c_str(), "?"))
            {
                LOGERROR("Group not defined for planting event.");
                return LDNDC_ERR_FAIL;
            }

            query = basequery;
            query << "/fields/" << field << "/periods/" << pi << "/planting/initialbiomass";
            add_planting.initialbiomass = ji.query_double_and_int( query.c_str(), 0.0);

            query = basequery;
            query << "/fields/" << field << "/periods/" << pi << "/planting/stubbleheight";
            add_planting.height = ji.query_double_and_int( query.c_str(), invalid_dbl);

            query = basequery;
            query << "/fields/" << field << "/periods/" << pi << "/planting/remains";
            add_planting.remains_relative = ji.query_double_and_int( query.c_str(), invalid_dbl);

            query = basequery;
            query << "/fields/" << field << "/periods/" << pi << "/planting/days_to_maturity";
            add_planting.days_to_maturity = ji.query_int( query.c_str(), -1);

            query = basequery;
            query << "/fields/" << field << "/periods/" << pi << "/planting/days_to_latest_harvest";
            add_planting.days_to_latest_harvest =  ji.query_int( query.c_str(), -1);

            query = basequery;
            query << "/fields/" << field << "/periods/" << pi << "/planting/days_on_field_after_maturity";
            add_planting.days_on_field_after_maturity = ji.query_int( query.c_str(), 0);

            query = basequery;
            query << "/fields/" << field << "/periods/" << pi << "/planting/gdd_min";
            add_planting.gdd_min = ji.query_int( query.c_str(), -1);

            query = basequery;
            query << "/fields/" << field << "/periods/" << pi << "/planting/gdd_max";
            add_planting.gdd_max = ji.query_int( query.c_str(), -1);

            if ( (dynamic_parametrization > 0) &&
                 ((add_planting.gdd_min < 0) || (add_planting.gdd_min < 0)))
            {
                LOGWARN("Dynamic parametrization disabled! Minimum (gdd_min) and maximum (gdd_max) range for GDD adjustion required!");
                dynamic_parametrization = 0;
                add_planting.gdd_min = -1;
                add_planting.gdd_max = -1;
            }
            
            query = basequery;
            query << "/fields/" << field << "/periods/" << pi << "/planting/years";
            int const nd_years_planting( ji.query_size( query.c_str()));

            std::vector< int > years_planting;
            if ( nd_years_planting > 0)
            {
                for (int yr = 0; yr < nd_years_planting; ++yr)
                {
                    query = basequery;
                    query << "/fields/" << field << "/periods/" << pi << "/planting/years/" << yr;
                    years_planting.push_back( ji.query_int( query.c_str(), -1));
                }
            }
            else
            {
                years_planting.push_back( -1);
            }
            add_planting.years = years_planting;


            /* tilling information*/
            std::vector< VegetationPeriod::tilling_t > tilling_events(
                                                          read_tilling( ji, basequery, field, pi));

            /* irrigation information*/
            std::vector< VegetationPeriod::irrigation_t > irrigation_events(
                                                                read_irrigation( ji, basequery, field, pi));

            /* flooding information*/
            std::vector< VegetationPeriod::flooding_t > flooding_events(
                                                            read_flooding( ji, basequery, field, pi));

            query = basequery;
            query << "/fields/" << field << "/periods/" << pi << "/irrigation_reservoir";
            double const irrigation_reservoir = ji.query_double_and_int( query.c_str(), 0.0);

            /* fertilizer information*/
            std::vector< VegetationPeriod::fertilizer_t > fertilization_events(
                                                                   read_fertilization( ji, basequery, field, pi));

            /* manure information*/
            std::vector< VegetationPeriod::fertilizer_t > manure_events(
                                                                   read_manuring( ji, basequery, field, pi));

            /* manure information*/
            std::vector< VegetationPeriod::cutting_t > cutting_events(
                                                                   read_cutting( ji, basequery, field, pi));

            VegetationPeriod dvp( VegetationPeriod(
                                           years_periods,
                                           doy_start,
                                           add_planting,
                                           tilling_events,
                                           fertilization_events,
                                           manure_events,
                                           irrigation_reservoir,
                                           irrigation_events,
                                           flooding_events,
                                           cutting_events,
                                           dynamic_parametrization));

            vegetation_periods.push_back( dvp);
        }

        FieldSite fieldsite( _args->iokcomm,
                             field_id,
                             field_name,
                             field_size,
                             vegetation_periods);
        fieldsites.push_back( fieldsite);
    }

    for ( size_t f = 0; f < fieldsites.size(); ++f)
    {
        fieldsites[f].initialize( _args->iokcomm);
    }

    return  LDNDC_ERR_OK;
}


/*!
 * @page ldndc_farmsystem
 * @subsection  farmsystem_events Events
 * @subsubsection farmsystem_events_tilling Tilling
 * Farmsystem tilling includes:
 * - day
 * - days_after_harvest
 * - depth
 */
std::vector< ldndc::VegetationPeriod::tilling_t >
ldndc::FarmSystem::read_tilling(
                                cbm::jquery_t _ji,
                                cbm::string_t _basequery,
                                int _field,
                                int _pi)
{
    cbm::string_t query = _basequery;
    query << "/fields/" << _field << "/periods/" << _pi << "/tilling";
    int const nd_tilling( _ji.query_size( query.c_str()));
    std::vector< VegetationPeriod::tilling_t > tilling_events;
    for ( int pit = 0; pit < nd_tilling; ++pit)
    {
        VegetationPeriod::tilling_t add_tilling;

        query = _basequery;
        query << "/fields/" << _field << "/periods/" << _pi << "/tilling/" << pit << "/day";
        int const day_of_application( _ji.query_int( query.c_str(), -1));
        add_tilling.day_of_application = day_of_application;

        query = _basequery;
        query << "/fields/" << _field << "/periods/" << _pi << "/tilling/" << pit << "/days_after_harvest";
        int const days_of_application_after_harvest( _ji.query_int( query.c_str(), -1));
        add_tilling.days_of_application_after_harvest = days_of_application_after_harvest;

        query = _basequery;
        query << "/fields/" << _field << "/periods/" << _pi << "/tilling/" << pit << "/depth";
        double const depth( _ji.query_double_and_int( query.c_str(), invalid_dbl));
        add_tilling.depth = depth;

        tilling_events.push_back( add_tilling);
    }
    return tilling_events;
}


/*!
 * @page ldndc_farmsystem
 * @subsubsection farmsystem_irrigation Irrigation
 * Farmsystem irrigation includes:
 * - day
 * - years
 * - fraction_dvs_min
 * - fraction_dvs_max
 * - fraction_field_capacity
 * - amount
 */
std::vector< ldndc::VegetationPeriod::irrigation_t >
ldndc::FarmSystem::read_irrigation(
                                   cbm::jquery_t _ji,
                                   cbm::string_t _basequery,
                                   int _field,
                                   int _pi)
{
    cbm::string_t query = _basequery;
    query << "/fields/" << _field << "/periods/" << _pi << "/irrigation";
    int const nd_irrigation( _ji.query_size( query.c_str()));
    std::vector< VegetationPeriod::irrigation_t > irrigation_events;
    for (int pif = 0; pif < nd_irrigation; ++pif)
    {
        VegetationPeriod::irrigation_t add_irrigation;

        query = _basequery;
        query << "/fields/" << _field << "/periods/" << _pi << "/irrigation/" << pif << "/day";
        add_irrigation.day_of_application = _ji.query_int( query.c_str(), -1);

        query = _basequery;
        query << "/fields/" << _field << "/periods/" << _pi << "/irrigation/" << pif << "/years";
        int const nd_years_irrigation( _ji.query_size( query.c_str()));

        std::vector< int > years_irrigation;
        if ( nd_years_irrigation > 0)
        {
            for (int yr = 0; yr < nd_years_irrigation; ++yr)
            {
                query = _basequery;
                query << "/fields/" << _field << "/periods/" << _pi << "/irrigation/" << pif << "/years/" << yr;
                years_irrigation.push_back( _ji.query_int( query.c_str(), -1));
            }
        }
        else
        {
            years_irrigation.push_back( -1);
        }

        add_irrigation.years = years_irrigation;

        query = _basequery;
        query << "/fields/" << _field << "/periods/" << _pi << "/irrigation/" << pif << "/fraction_dvs_min";
        add_irrigation.fraction_dvs_min_of_application = _ji.query_double_and_int( query.c_str(), invalid_dbl);

        query = _basequery;
        query << "/fields/" << _field << "/periods/" << _pi << "/irrigation/" << pif << "/fraction_dvs_max";
        add_irrigation.fraction_dvs_max_of_application = _ji.query_double_and_int( query.c_str(), invalid_dbl);

        query = _basequery;
        query << "/fields/" << _field << "/periods/" << _pi << "/irrigation/" << pif << "/fraction_field_capacity";
        add_irrigation.fraction_field_capacity = _ji.query_double_and_int( query.c_str(), invalid_dbl);

        query = _basequery;
        query << "/fields/" << _field << "/periods/" << _pi << "/irrigation/" << pif << "/amount";
        add_irrigation.irrigation_height = _ji.query_double_and_int( query.c_str(), invalid_dbl);

        if ( cbm::flt_greater_equal_zero( add_irrigation.irrigation_height))
        {
            irrigation_events.push_back( add_irrigation);
        }
    }
    return irrigation_events;
}


/*!
 * @page ldndc_farmsystem
 * @subsubsection farmsystem_flooding Flooding
 * Farmsystem flooding includes:
 * - start_static / start_dynamic
 * - end_static / end_dynamic
 * - bundheight
 * - watertable
 * - irrigationheight
 * - percolationrate
 * - drainage
 */
std::vector< ldndc::VegetationPeriod::flooding_t >
ldndc::FarmSystem::read_flooding(
                                 cbm::jquery_t _ji,
                                 cbm::string_t _basequery,
                                 int _field,
                                 int _pi)
{
    cbm::string_t query = _basequery;
    query << "/fields/" << _field << "/periods/" << _pi << "/flooding";
    int const nd_flooding( _ji.query_size( query.c_str()));
    std::vector< VegetationPeriod::flooding_t > flooding_events;
    for (int fi = 0; fi < nd_flooding; ++fi)
    {
        VegetationPeriod::flooding_t add_flooding;

        query = _basequery;
        query << "/fields/" << _field << "/periods/" << _pi << "/flooding/" << fi << "/start_static";
        add_flooding.start_static = _ji.query_int( query.c_str(), invalid_int);
        if ( add_flooding.start_static == invalid_int)
        {
            query = _basequery;
            query << "/fields/" << _field << "/periods/" << _pi << "/flooding/" << fi << "/start";
            add_flooding.start_static = _ji.query_int( query.c_str(), invalid_int);
        }

        query = _basequery;
        query << "/fields/" << _field << "/periods/" << _pi << "/flooding/" << fi << "/end_static";
        add_flooding.end_static = _ji.query_int( query.c_str(), invalid_int);
        if ( add_flooding.end_static == invalid_int)
        {
            query = _basequery;
            query << "/fields/" << _field << "/periods/" << _pi << "/flooding/" << fi << "/end";
            add_flooding.end_static = _ji.query_int( query.c_str(), invalid_int);
        }

        query = _basequery;
        query << "/fields/" << _field << "/periods/" << _pi << "/flooding/" << fi << "/start_dynamic";
        add_flooding.start_dynamic = _ji.query_double_and_int( query.c_str(), invalid_dbl);

        query = _basequery;
        query << "/fields/" << _field << "/periods/" << _pi << "/flooding/" << fi << "/end_dynamic";
        add_flooding.end_dynamic = _ji.query_double_and_int( query.c_str(), invalid_dbl);


        query = _basequery;
        query << "/fields/" << _field << "/periods/" << _pi << "/flooding/" << fi << "/bundheight";
        add_flooding.bundheight = _ji.query_double_and_int( query.c_str(), invalid_dbl);

        query = _basequery;
        query << "/fields/" << _field << "/periods/" << _pi << "/flooding/" << fi << "/watertable";
        add_flooding.watertable = _ji.query_double_and_int( query.c_str(), invalid_dbl);

        query = _basequery;
        query << "/fields/" << _field << "/periods/" << _pi << "/flooding/" << fi << "/irrigationheight";
        add_flooding.irrigation_height = _ji.query_double_and_int( query.c_str(), invalid_dbl);

        query = _basequery;
        query << "/fields/" << _field << "/periods/" << _pi << "/flooding/" << fi << "/percolationrate";
        add_flooding.percolation_rate = _ji.query_double_and_int( query.c_str(), invalid_dbl);

        query = _basequery;
        query << "/fields/" << _field << "/periods/" << _pi << "/flooding/" << fi << "/drainage";
        add_flooding.drainage = _ji.query_bool( query.c_str(), false);

        query = _basequery;
        query << "/fields/" << _field << "/periods/" << _pi << "/flooding/" << fi << "/unlimitedwater";
        add_flooding.unlimited_water = _ji.query_bool( query.c_str(), true);

        flooding_events.push_back( add_flooding);
    }

    return flooding_events;
}


/*!
 * @page ldndc_farmsystem
 * @subsubsection farmsystem_fertilization Fertilization
 * Farmsystem fertilization includes:
 * - day
 * - years
 * - fraction_dvs_min
 * - fraction_dvs_max
 * - fraction_gdd
 * - type
 * - amount
 * - depth
 * - watermanagement
 * - irrigationheight
 */
std::vector< ldndc::VegetationPeriod::fertilizer_t >
ldndc::FarmSystem::read_fertilization(
                                 cbm::jquery_t _ji,
                                 cbm::string_t _basequery,
                                 int _field,
                                 int _pi)
{
    cbm::string_t query = _basequery;
    query << "/fields/" << _field << "/periods/" << _pi << "/fertilizer";
    int const nd_fertilizer( _ji.query_size( query.c_str()));
    std::vector< VegetationPeriod::fertilizer_t > fertilization_events;
    for (int pif = 0; pif < nd_fertilizer; ++pif)
    {
        VegetationPeriod::fertilizer_t add_fertilizer;

        query = _basequery;
        query << "/fields/" << _field << "/periods/" << _pi << "/fertilizer/" << pif << "/day";
        add_fertilizer.day_of_application = _ji.query_int( query.c_str(), -1);

        query = _basequery;
        query << "/fields/" << _field << "/periods/" << _pi << "/fertilizer/" << pif << "/years";
        int const nd_years_fert( _ji.query_size( query.c_str()));

        std::vector< int > years_fert;
        if ( nd_years_fert > 0)
        {
            for (int yr = 0; yr < nd_years_fert; ++yr)
            {
                query = _basequery;
                query << "/fields/" << _field << "/periods/" << _pi << "/fertilizer/" << pif << "/years/" << yr;
                years_fert.push_back( _ji.query_int( query.c_str(), -1));
            }
        }
        else
        {
            years_fert.push_back( -1);
        }

        add_fertilizer.years = years_fert;

        query = _basequery;
        query << "/fields/" << _field << "/periods/" << _pi << "/fertilizer/" << pif << "/fraction_dvs_min";
        add_fertilizer.fraction_dvs_min_of_application = _ji.query_double_and_int( query.c_str(), invalid_dbl);

        if ( cbm::flt_less( add_fertilizer.fraction_dvs_min_of_application, 0.0))
        {
            query = _basequery;
            query << "/fields/" << _field << "/periods/" << _pi << "/fertilizer/" << pif << "/fraction_gdd";
            add_fertilizer.fraction_dvs_min_of_application = _ji.query_double_and_int( query.c_str(), invalid_dbl);
        }

        query = _basequery;
        query << "/fields/" << _field << "/periods/" << _pi << "/fertilizer/" << pif << "/fraction_dvs_max";
        add_fertilizer.fraction_dvs_max_of_application = _ji.query_double_and_int( query.c_str(), invalid_dbl);

        query = _basequery;
        query << "/fields/" << _field << "/periods/" << _pi << "/fertilizer/" << pif << "/fraction_field_capacity";
        add_fertilizer.fraction_field_capacity = _ji.query_double_and_int( query.c_str(), invalid_dbl);

        query = _basequery;
        query << "/fields/" << _field << "/periods/" << _pi << "/fertilizer/" << pif << "/type";
        add_fertilizer.name = _ji.query_string( query.c_str(), "-");

        query = _basequery;
        query << "/fields/" << _field << "/periods/" << _pi << "/fertilizer/" << pif << "/amount";
        add_fertilizer.n_amount = _ji.query_double_and_int( query.c_str(), 0.0);

        query = _basequery;
        query << "/fields/" << _field << "/periods/" << _pi << "/fertilizer/" << pif << "/depth";
        add_fertilizer.depth = _ji.query_double_and_int( query.c_str(), 0.0);

        query = _basequery;
        query << "/fields/" << _field << "/periods/" << _pi << "/fertilizer/" << pif << "/watermanagement";
        add_fertilizer.water_management = _ji.query_string( query.c_str(), "none");

        query = _basequery;
        query << "/fields/" << _field << "/periods/" << _pi << "/fertilizer/" << pif << "/irrigationheight";
        add_fertilizer.irrigation_height = _ji.query_double_and_int( query.c_str(), 0.0);

        add_fertilizer.status = VegetationPeriod::RECEIVABLE;

        fertilization_events.push_back( add_fertilizer);
    }

    return fertilization_events;
}


/*!
 * @page ldndc_farmsystem 
 * @subsubsection farmsystem_fertilization Fertilization
 * Farmsystem fertilization includes:
 * - day
 */
std::vector< ldndc::VegetationPeriod::fertilizer_t >
ldndc::FarmSystem::read_manuring(
                                 cbm::jquery_t _ji,
                                 cbm::string_t _basequery,
                                 int _field,
                                 int _pi)
{
    cbm::string_t query = _basequery;

    query << "/fields/" << _field << "/periods/" << _pi << "/manure";
    int const nd_manure( _ji.query_size( query.c_str()));
    std::vector< VegetationPeriod::fertilizer_t > manure_events;
    for (int pif = 0; pif < nd_manure; ++pif)
    {
        VegetationPeriod::fertilizer_t add_manure;

        query = _basequery;
        query << "/fields/" << _field << "/periods/" << _pi << "/manure/" << pif << "/day";
        add_manure.day_of_application = _ji.query_int( query.c_str(), -1);

        query = _basequery;
        query << "/fields/" << _field << "/periods/" << _pi << "/manure/" << pif << "/years";
        int const nd_years_manure( _ji.query_size( query.c_str()));

        std::vector< int > years_manure;
        if ( nd_years_manure > 0)
        {
            for (int yr = 0; yr < nd_years_manure; ++yr)
            {
                query = _basequery;
                query << "/fields/" << _field << "/periods/" << _pi << "/manure/" << pif << "/years/" << yr;
                years_manure.push_back( _ji.query_int( query.c_str(), -1));
            }
        }
        else
        {
            years_manure.push_back( -1);
        }

        add_manure.years = years_manure;

        query = _basequery;
        query << "/fields/" << _field << "/periods/" << _pi << "/manure/" << pif << "/fraction_dvs_min";
        add_manure.fraction_dvs_min_of_application = _ji.query_double_and_int( query.c_str(), invalid_dbl);

        if ( cbm::flt_less( add_manure.fraction_dvs_min_of_application, 0.0))
        {
            query = _basequery;
            query << "/fields/" << _field << "/periods/" << _pi << "/manure/" << pif << "/fraction_gdd";
            add_manure.fraction_dvs_min_of_application = _ji.query_double_and_int( query.c_str(), invalid_dbl);
        }

        query = _basequery;
        query << "/fields/" << _field << "/periods/" << _pi << "/manure/" << pif << "/fraction_dvs_max";
        add_manure.fraction_dvs_max_of_application = _ji.query_double_and_int( query.c_str(), invalid_dbl);

        query = _basequery;
        query << "/fields/" << _field << "/periods/" << _pi << "/manure/" << pif << "/fraction_field_capacity";
        add_manure.fraction_field_capacity = _ji.query_double_and_int( query.c_str(), invalid_dbl);

        query = _basequery;
        query << "/fields/" << _field << "/periods/" << _pi << "/manure/" << pif << "/type";
        add_manure.name = _ji.query_string( query.c_str(), "-");

        query = _basequery;
        query << "/fields/" << _field << "/periods/" << _pi << "/manure/" << pif << "/c_amount";
        add_manure.c_amount = _ji.query_double_and_int( query.c_str(), 0.0);

        query = _basequery;
        query << "/fields/" << _field << "/periods/" << _pi << "/manure/" << pif << "/n_amount";
        add_manure.n_amount = _ji.query_double_and_int( query.c_str(), 0.0);

        query = _basequery;
        query << "/fields/" << _field << "/periods/" << _pi << "/manure/" << pif << "/depth";
        add_manure.depth = _ji.query_double_and_int( query.c_str(), 0.0);

        add_manure.status = VegetationPeriod::RECEIVABLE;

        if ( cbm::flt_greater_zero( add_manure.c_amount * add_manure.n_amount))
        {
            manure_events.push_back( add_manure);
        }
    }

    return manure_events;
}

std::vector< ldndc::VegetationPeriod::cutting_t >
ldndc::FarmSystem::read_cutting(
                                cbm::jquery_t _ji,
                                cbm::string_t _basequery,
                                int _field,
                                int _pi)
{
    cbm::string_t query = _basequery;
    query << "/fields/" << _field << "/periods/" << _pi << "/cutting";
    int const nd_cutting( _ji.query_size( query.c_str()));
    std::vector< VegetationPeriod::cutting_t > cutting_events;
    for ( int pit = 0; pit < nd_cutting; ++pit)
    {
        VegetationPeriod::cutting_t add_cutting;

        query = _basequery;
        query << "/fields/" << _field << "/periods/" << _pi << "/cutting/" << pit << "/day";
        int const day_of_cut( _ji.query_int( query.c_str(), -1));
        add_cutting.day_of_cut = day_of_cut;

        query = _basequery;
        query << "/fields/" << _field << "/periods/" << _pi << "/cutting/" << pit << "/remains";
        double const remains_absolute( _ji.query_double_and_int( query.c_str(), invalid_dbl));
        add_cutting.remains_absolute = remains_absolute;

        query = _basequery;
        query << "/fields/" << _field << "/periods/" << _pi << "/cutting/" << pit << "/height";
        double const height( _ji.query_double_and_int( query.c_str(), invalid_dbl));
        add_cutting.height = height;

        query = _basequery;
        query << "/fields/" << _field << "/periods/" << _pi << "/cutting/" << pit << "/export_fruit";
        double const export_fruit( _ji.query_double_and_int( query.c_str(), invalid_dbl));
        add_cutting.export_fruit = export_fruit;

        query = _basequery;
        query << "/fields/" << _field << "/periods/" << _pi << "/cutting/" << pit << "/export_foliage";
        double const export_foliage( _ji.query_double_and_int( query.c_str(), invalid_dbl));
        add_cutting.export_foliage = export_foliage;

        query = _basequery;
        query << "/fields/" << _field << "/periods/" << _pi << "/cutting/" << pit << "/export_living_structural_tissue";
        double const export_living_structural_tissue( _ji.query_double_and_int( query.c_str(), invalid_dbl));
        add_cutting.export_living_structural_tissue = export_living_structural_tissue;

        query = _basequery;
        query << "/fields/" << _field << "/periods/" << _pi << "/cutting/" << pit << "/export_dead_structural_tissue";
        double const export_dead_structural_tissue( _ji.query_double_and_int( query.c_str(), invalid_dbl));
        add_cutting.export_dead_structural_tissue = export_dead_structural_tissue;

        query = _basequery;
        query << "/fields/" << _field << "/periods/" << _pi << "/cutting/" << pit << "/export_root";
        double const export_root( _ji.query_double_and_int( query.c_str(), invalid_dbl));
        add_cutting.export_root = export_root;

        cutting_events.push_back( add_cutting);
    }
    return cutting_events;
}
