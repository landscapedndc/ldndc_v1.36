/*!
 * @file
 * @author
 *    David Kraus
 */

#include  "farmsystem/ld_vegetation_period.h"
#include  <constants/cbm_const.h>

int ldndc::VegetationPeriod::nd_vegetation_periods = 0;


/*!
 * @page ldndc_farmsystem
 * @section farmsystem_vegetation_period Vegetation period
 *
 * A vegetation period / cropping season includes the following attributes:
 * - \em start: day of year of the start of the season
 * - \em years: a list of years in which the defined season should be handled (if not given, season is handled every year)
 * - \em planting: list of planting attributes (see below)
 * - \em tilling: list of tilling attributes (see below)
 * - \em fertilizer: list of fertilizer attributes (see below)
 * - \em manure: list of manure attributes (see below)
 * - \em irrigation: list of irrigation attributes (see below)
 * - \em flooding: list of flooding attributes (see below)
 */
ldndc::VegetationPeriod::VegetationPeriod(
                                  std::vector< int > const &_years,
                                  size_t _start,
                                  planting_t _planting,
                                  std::vector< tilling_t > const &_tilling,
                                  std::vector< fertilizer_t > const &_fertilizer,
                                  std::vector< fertilizer_t > const &_manure,
                                  double _irrigation_reservoir,
                                  std::vector< irrigation_t > const &_irrigation,
                                  std::vector< flooding_t > const &_flooding,
                                  std::vector< cutting_t > const &_cutting,
                                  size_t _dynamic_parametrization):
                                    start( _start),
                                    planting_event( _planting),
                                    tilling_events( _tilling),
                                    irrigation_events( _irrigation),
                                    flooding_events( _flooding),
                                    fertilization_events( _fertilizer),
                                    manure_events( _manure),
                                    cutting_events(_cutting),
                                    irrigation_reservoir( _irrigation_reservoir),
                                    dynamic_parametrization( _dynamic_parametrization),
                                    id_vegetation_period( nd_vegetation_periods++)
{
    years = _years;
    gdd_target = -1.0;

    seconds_at_field_preparation = -1;
    seconds_at_planting = -1;
    seconds_at_maturity = -1;
    seconds_at_harvest = -1;

    gdd_at_anticipated_maturity = invalid_dbl;

    irrigation_reservoir_withdrawal = 0.0;

    harvested = false;
    nd_till_events = 0;

    for (size_t i = 0; i < fertilization_events.size(); ++i)
    {
        fertilization_events[i].status = VegetationPeriod::RECEIVABLE;
    }
    for (size_t i = 0; i < manure_events.size(); ++i)
    {
        manure_events[i].status = VegetationPeriod::RECEIVABLE;
    }
}


ldndc::VegetationPeriod::~VegetationPeriod()
{}


ldndc::VegetationPeriod::tilling_t::tilling_t()
{
    day_of_application = -1;
    days_of_application_after_harvest = -1;
    depth = invalid_dbl;
}


lerr_t
ldndc::VegetationPeriod::assign_tilling_event(
                                          tilling_t* _ev,
                                          int _day,
                                          bool _pop)
{
    int tilling_index( -1);
    for ( size_t i = 0; i < tilling_events.size(); ++i)
    {
        if ( tilling_events[i].day_of_application >= 0)
        {
            if ( _day >= tilling_events[i].day_of_application)
            {
                tilling_index = (int)i;
                break;
            }
        }
        else if ( tilling_events[i].days_of_application_after_harvest >= 0)
        {
            if ( _day >= tilling_events[i].days_of_application_after_harvest)
            {
                tilling_index = (int)i;
                break;
            }
        }
    }
    
    if ( tilling_index >= 0)
    {
        _ev->depth = tilling_events[tilling_index].depth;
        if ( _pop)
        {
            tilling_events.erase( std::next( tilling_events.begin(), tilling_index));
        }
        return LDNDC_ERR_EVENT_MATCH;
    }
    else
    {
        return LDNDC_ERR_OK;
    }
}


ldndc::VegetationPeriod::irrigation_t::irrigation_t()
{
    day_of_application = -1;
    irrigation_height = invalid_dbl;
    fraction_dvs_min_of_application = invalid_dbl;
    fraction_dvs_max_of_application = invalid_dbl;
    fraction_field_capacity = invalid_dbl;
    is_reservoir = false;
}


lerr_t
ldndc::VegetationPeriod::assign_irrigation_event(
                                             irrigation_t* _ev,
                                             int _day,
                                             double _maturity_status,
                                             double _wc,
                                             double _fc,
                                             bool _pop)
{
    int irrigation_index( -1);
    for ( size_t i = 0; i < irrigation_events.size(); ++i)
    {
        /* static irrigation event */
        if ( irrigation_events[i].day_of_application >= 0)
        {
            if ( _day >= irrigation_events[i].day_of_application)
            {
                irrigation_index = (int)i;
                break;
            }
        }
        
        /* dynamic irrigation event */
        else
        {
            bool have_dynamic_event( false);
            if ( cbm::is_valid( irrigation_events[i].fraction_dvs_min_of_application))
            {
                if ( cbm::flt_less( _maturity_status, irrigation_events[i].fraction_dvs_min_of_application))
                {
                    continue;
                }
                have_dynamic_event = true;
            }
            
            if ( cbm::is_valid( irrigation_events[i].fraction_dvs_max_of_application))
            {
                if ( cbm::flt_greater( _maturity_status, irrigation_events[i].fraction_dvs_max_of_application))
                {
                    continue;
                }
                have_dynamic_event = true;
            }
            
            if ( cbm::is_valid( irrigation_events[i].fraction_field_capacity))
            {
                if ( cbm::flt_greater( _wc, irrigation_events[i].fraction_field_capacity * _fc))
                {
                    continue;
                }
                have_dynamic_event = true;
            }

            if ( have_dynamic_event)
            {
                irrigation_index = (int)i;
                break;
            }
        }
    }

    if ( irrigation_index >= 0)
    {
        _ev->irrigation_height = irrigation_events[irrigation_index].irrigation_height;
        _ev->is_reservoir = irrigation_events[irrigation_index].is_reservoir;
        if ( _pop)
        {
            irrigation_events.erase( std::next( irrigation_events.begin(), irrigation_index));
        }
        return LDNDC_ERR_EVENT_MATCH;
    }
    else
    {
        return LDNDC_ERR_OK;
    }
}


ldndc::VegetationPeriod::flooding_t::flooding_t()
{
    start_static = -1;
    end_static = -1;
    start_dynamic = invalid_dbl;
    end_dynamic = invalid_dbl;
    irrigation_height = invalid_dbl;
    watertable = invalid_dbl;
    bundheight = invalid_dbl;
    percolation_rate = invalid_dbl;
    drainage = false;
    unlimited_water = true;
    saturation_level = invalid_dbl;
    soil_depth = invalid_dbl;
    status = VegetationPeriod::RECEIVABLE;
}


lerr_t
ldndc::VegetationPeriod::assign_flooding_event(
                                           std::vector< VegetationPeriod::flooding_t > &_ev,
                                           int _day,
                                           double _maturity_status,
                                           bool _pop)
{
    for ( size_t i = 0; i < flooding_events.size(); ++i)
    {
        if ( (cbm::is_valid( flooding_events[i].start_static) &&
                (_day >= flooding_events[i].start_static)) ||
             (cbm::is_valid( flooding_events[i].start_dynamic) &&
                (_maturity_status >= flooding_events[i].start_dynamic)))
        {
            if ( flooding_events[i].status == VegetationPeriod::RECEIVABLE)
            {
                flooding_events[i].status = VegetationPeriod::STARTED;
            }
        }

        if ( flooding_events[i].status >= VegetationPeriod::STARTED)
        {
            //static end
            if ( (cbm::is_valid( flooding_events[i].end_static) &&
                    (_day <= flooding_events[i].end_static)) ||
            //dynamic end
                 (cbm::is_valid( flooding_events[i].end_dynamic) &&
                    cbm::flt_less( _maturity_status, flooding_events[i].end_dynamic)))
            {
                flooding_events[i].status = VegetationPeriod::CONTINUING;
            }
            else if ( (flooding_events[i].status == VegetationPeriod::CONTINUING) &&
                      (flooding_events[i].drainage == true))
            {
                flooding_events[i].watertable = 0.0;
                flooding_events[i].irrigation_height = invalid_dbl;
                flooding_events[i].status = VegetationPeriod::FINALIZED;
            }
            else
            {
                flooding_events[i].status = VegetationPeriod::DONE;
            }
        }

        if ( (flooding_events[i].status >= VegetationPeriod::STARTED) &&
             (flooding_events[i].status < VegetationPeriod::DONE))
        {
            VegetationPeriod::flooding_t add_ev;
            add_ev.watertable = flooding_events[i].watertable;
            add_ev.bundheight = flooding_events[i].bundheight;
            add_ev.percolation_rate = flooding_events[i].percolation_rate;
            add_ev.irrigation_height = flooding_events[i].irrigation_height;
            add_ev.unlimited_water = flooding_events[i].unlimited_water;
            _ev.push_back( add_ev);
        }
    }

    if ( _pop)
    {
        for ( size_t i = 0; i < flooding_events.size(); ++i)
        {
            if ( flooding_events[i].status == VegetationPeriod::DONE)
            {
                flooding_events.erase( std::next( flooding_events.begin(), i));
            }
        }
    }

    if ( _ev.size() > 0)
    {
        return LDNDC_ERR_EVENT_MATCH;
    }
    else
    {
        return LDNDC_ERR_OK;
    }
}


ldndc::VegetationPeriod::fertilizer_t::fertilizer_t()
{
    day_of_application = -1;
    fraction_dvs_min_of_application = invalid_dbl;
    fraction_dvs_max_of_application = invalid_dbl;
    fraction_field_capacity = invalid_dbl;
    c_amount = invalid_dbl;
    n_amount = invalid_dbl;
    depth = invalid_dbl;
    irrigation_height = invalid_dbl;
    status = VegetationPeriod::RECEIVABLE;
}


lerr_t
ldndc::VegetationPeriod::assign_fertilization_event(
                                                fertilizer_t* _ev,
                                                int _day,
                                                int _year,
                                                double _maturity_status,
                                                double _surface_water,
                                                double _wc,
                                                double _fc,
                                                bool _pop)
{
    /* Appoint fertilization */
    for ( size_t i = 0; i < fertilization_events.size(); ++i)
    {
        /* collect scheduled vegetation periods */
        for (size_t yr = 0; yr < fertilization_events[i].years.size(); ++yr)
        {
            if ( (fertilization_events[i].years[yr] == _year) ||
                 (fertilization_events[i].years[yr] == -1))
            {
                if ( fertilization_events[i].status == VegetationPeriod::RECEIVABLE)
                {
                    if ( fertilization_events[i].day_of_application > 0)
                    {
                        if ( _day >= fertilization_events[i].day_of_application)
                        {
                            fertilization_events[i].status = VegetationPeriod::APPOINTED;
                        }
                    }
                    else
                    {
                        bool have_dynamic_event( false);
                        if ( cbm::is_valid( fertilization_events[i].fraction_dvs_min_of_application))
                        {
                            if ( cbm::flt_less( _maturity_status, fertilization_events[i].fraction_dvs_min_of_application))
                            {
                                continue;
                            }
                            have_dynamic_event = true;
                        }

                        if ( cbm::is_valid( fertilization_events[i].fraction_dvs_max_of_application))
                        {
                            if ( cbm::flt_greater( _maturity_status, fertilization_events[i].fraction_dvs_max_of_application))
                            {
                                continue;
                            }
                            have_dynamic_event = true;
                        }

                        if ( cbm::is_valid( fertilization_events[i].fraction_field_capacity))
                        {
                            if ( cbm::flt_greater( _wc, fertilization_events[i].fraction_field_capacity * _fc))
                            {
                                continue;
                            }
                            have_dynamic_event = true;
                        }

                        if ( have_dynamic_event)
                        {
                            fertilization_events[i].status = VegetationPeriod::APPOINTED;
                            break;
                        }
                    }
                }
            }
        }
    }

    int fertilization_index( -1);
    for (size_t i = 0; i < fertilization_events.size(); ++i)
    {
        if ( fertilization_events[i].status == VegetationPeriod::APPOINTED)
        {
            if ( cbm::is_equal_i( fertilization_events[i].water_management.c_str(), "flooded"))
            {
                if ( cbm::flt_greater( _surface_water, 0.02))
                {
                    fertilization_index = (int)i;
                }
            }
            else if ( cbm::is_equal_i( fertilization_events[i].water_management.c_str(), "drained"))
            {
                if ( cbm::flt_equal_zero( _surface_water))
                {
                    fertilization_index = (int)i;
                }
            }
            else
            {
                fertilization_index = (int)i;
            }

            if ( fertilization_index >= 0)
            {
                if ( cbm::flt_greater_zero( fertilization_events[i].irrigation_height))
                {
                    irrigation_t add_irrigation;
                    add_irrigation.day_of_application = _day + 1;
                    add_irrigation.irrigation_height = fertilization_events[i].irrigation_height;
                    add_irrigation.is_reservoir = false;
                    irrigation_events.push_back( add_irrigation);
                }
            }
        }
    }

    if ( fertilization_index >= 0)
    {
        _ev->name = fertilization_events[fertilization_index].name;
        _ev->n_amount = fertilization_events[fertilization_index].n_amount;
        _ev->depth = fertilization_events[fertilization_index].depth;
        if ( _pop)
        {
            fertilization_events.erase( std::next( fertilization_events.begin(), fertilization_index));
        }
        return LDNDC_ERR_EVENT_MATCH;
    }
    else
    {
        return LDNDC_ERR_OK;
    }
}


lerr_t
ldndc::VegetationPeriod::assign_manuring_event(
                                           fertilizer_t* _ev,
                                           int _day,
                                           int _year,
                                           double _maturity_status,
                                           double _surface_water,
                                           double _wc,
                                           double _fc,
                                           bool _pop)
{
    /* Appoint manure */
    for ( size_t i = 0; i < manure_events.size(); ++i)
    {
        /* collect scheduled vegetation periods */
        for (size_t yr = 0; yr < manure_events[i].years.size(); ++yr)
        {
            if ( (manure_events[i].years[yr] == _year) ||
                 (manure_events[i].years[yr] == -1))
            {
                if ( manure_events[i].status == VegetationPeriod::RECEIVABLE)
                {
                    if ( manure_events[i].day_of_application > 0)
                    {
                        if ( _day >= manure_events[i].day_of_application)
                        {
                            manure_events[i].status = VegetationPeriod::APPOINTED;
                        }
                    }
                    else
                    {
                        bool have_dynamic_event( false);
                        if ( cbm::is_valid( manure_events[i].fraction_dvs_min_of_application))
                        {
                            if ( cbm::flt_less( _maturity_status, manure_events[i].fraction_dvs_min_of_application))
                            {
                                continue;
                            }
                            have_dynamic_event = true;
                        }

                        if ( cbm::is_valid( manure_events[i].fraction_dvs_max_of_application))
                        {
                            if ( cbm::flt_greater( _maturity_status, manure_events[i].fraction_dvs_max_of_application))
                            {
                                continue;
                            }
                            have_dynamic_event = true;
                        }

                        if ( cbm::is_valid( manure_events[i].fraction_field_capacity))
                        {
                            if ( cbm::flt_greater( _wc, manure_events[i].fraction_field_capacity * _fc))
                            {
                                continue;
                            }
                            have_dynamic_event = true;
                        }

                        if ( have_dynamic_event)
                        {
                            manure_events[i].status = VegetationPeriod::APPOINTED;
                            break;
                        }
                    }
                }
            }
        }
    }

    int manuring_index( -1);
    for (size_t i = 0; i < manure_events.size(); ++i)
    {
        if ( manure_events[i].status == VegetationPeriod::APPOINTED)
        {
            if ( cbm::is_equal_i( manure_events[i].water_management.c_str(), "flooded"))
            {
                if ( cbm::flt_greater( _surface_water, 0.02))
                {
                    manuring_index = (int)i;
                }
            }
            else if ( cbm::is_equal_i( manure_events[i].water_management.c_str(), "drained"))
            {
                if ( cbm::flt_equal_zero( _surface_water))
                {
                    manuring_index = (int)i;
                }
            }
            else
            {
                manuring_index = (int)i;
            }

            if ( manuring_index >= 0)
            {
                if ( cbm::flt_greater_zero( manure_events[i].irrigation_height))
                 {
                     irrigation_t add_irrigation;
                     add_irrigation.day_of_application = _day + 1;
                     add_irrigation.irrigation_height = manure_events[i].irrigation_height;
                     irrigation_events.push_back( add_irrigation);
                 }
            }
        }
    }

    if ( manuring_index >= 0)
    {
        _ev->name = manure_events[manuring_index].name;
        _ev->c_amount = manure_events[manuring_index].c_amount;
        _ev->n_amount = manure_events[manuring_index].n_amount;
        _ev->depth = manure_events[manuring_index].depth;
        if ( _pop)
        {
            manure_events.erase( std::next( manure_events.begin(), manuring_index));
        }
        return LDNDC_ERR_EVENT_MATCH;
    }
    else
    {
        return LDNDC_ERR_OK;
    }
}

ldndc::VegetationPeriod::cutting_t::cutting_t()
{
    day_of_cut = -1;
    remains_absolute = invalid_dbl;
    height = invalid_dbl;
    export_fruit = invalid_dbl;
    export_foliage = invalid_dbl;
    export_living_structural_tissue = invalid_dbl;
    export_dead_structural_tissue = invalid_dbl;
    export_root = invalid_dbl;
}


lerr_t
ldndc::VegetationPeriod::assign_cutting_event(
                                          cutting_t* _ev,
                                          int _day,
                                          bool _pop)
{
    int cutting_index( -1);
    //std::cout << cutting_events.size() << std::endl;
    for ( size_t i = 0; i < cutting_events.size(); ++i)
    {
        if ( cutting_events[i].day_of_cut >= 0)
        {
            if ( _day >= cutting_events[i].day_of_cut)
            {
                cutting_index = (int)i;
                break;
            }
        }
    }
    
    if ( cutting_index >= 0)
    {
        _ev->remains_absolute = cutting_events[cutting_index].remains_absolute;
        _ev->height = cutting_events[cutting_index].height;
        _ev->export_fruit = cutting_events[cutting_index].export_fruit;
        _ev->export_foliage = cutting_events[cutting_index].export_foliage;
        _ev->export_living_structural_tissue = cutting_events[cutting_index].export_living_structural_tissue;
        _ev->export_dead_structural_tissue = cutting_events[cutting_index].export_dead_structural_tissue;
        _ev->export_root = cutting_events[cutting_index].export_root;
        if ( _pop)
        {
            cutting_events.erase( std::next( cutting_events.begin(), cutting_index));
        }
        return LDNDC_ERR_EVENT_MATCH;
    }
    else
    {
        return LDNDC_ERR_OK;
    }
}


lerr_t
ldndc::VegetationPeriod::assign_planting_event(
                                               planting_t* _ev,
                                               int _day,
                                               long long int _seconds)
{
    if ( seconds_at_planting < 0)
    {
        if ( _day >= planting_event.day_of_application)
        {
            seconds_at_planting = _seconds;

            _ev->name_base = planting_event.name_base;
            _ev->name_extension = planting_event.name_extension;
            _ev->group = planting_event.group;
            _ev->initialbiomass = planting_event.initialbiomass;
            return LDNDC_ERR_EVENT_MATCH;
        }
    }
    return LDNDC_ERR_OK;
}


lerr_t
ldndc::VegetationPeriod::assign_harvest_event(
                                              planting_t* _ev,
                                              int *_dvp_id,
                                              double *_gdd_target,
                                              cbm::sclock_t const *_clk,
                                              double _growing_degree_days,
                                              double _maturity_status)
{
    if ( !harvested && (planting_event.group == "crop" || planting_event.days_to_latest_harvest != -1))
    {
        int const days_on_field( days_since( seconds_at_planting, _clk));
        
        if ( !cbm::is_valid( gdd_at_anticipated_maturity) &&
             (days_on_field >= planting_event.days_to_maturity))
        {
            gdd_at_anticipated_maturity = _growing_degree_days;
        }
        if ( cbm::flt_greater( _maturity_status, 0.99) && planting_event.group == "crop")
        {
            if ( seconds_at_maturity < 0)
            {
                seconds_at_maturity = _clk->seconds();
            }

            if ( planting_event.days_on_field_after_maturity > 0)
            {
                int const days_after_maturity( days_since( seconds_at_maturity, _clk));
                if ( days_after_maturity >= planting_event.days_on_field_after_maturity)
                {
                    harvested = true;
                }
            }
            else
            {
                harvested = true;
            }
        }

        if ( planting_event.days_to_latest_harvest > 0)
        {
            if ( days_on_field >= (int)planting_event.days_to_latest_harvest)
            {
                harvested = true;
            }
        }
        
        if ( harvested)
        {
            seconds_at_harvest = _clk->seconds();
            *_dvp_id = id();

            if ( (dynamic_parametrization > 0) &&
                 (planting_event.days_to_maturity > 0))
            {
                if ( cbm::is_valid( gdd_at_anticipated_maturity) )
                {
                    *_gdd_target = gdd_at_anticipated_maturity;
                }
                else if ( seconds_at_maturity > 0)
                {
                    int const days_on_field_until_maturity(  std::max( 1,
                                                              days_since( seconds_at_planting, _clk) -
                                                              days_since( seconds_at_maturity, _clk)));
                    //scale can be lower or greater than 1.0
                    double const scale( std::pow(
                                            cbm::bound( 0.5,
                                                        ((double)planting_event.days_to_maturity) /
                                                        ((double)days_on_field_until_maturity),
                                                        1.5), 0.2));
                    *_gdd_target = _growing_degree_days * scale;
                }
            }

            _ev->name_base = planting_event.name_base;
            _ev->name_extension = planting_event.name_extension;
            _ev->height = planting_event.height;
            _ev->remains_relative = planting_event.remains_relative;
            return LDNDC_ERR_EVENT_MATCH;
        }
    }

    return LDNDC_ERR_OK;
}


int
ldndc::VegetationPeriod::days_since(
                                    long long int _seconds,
                                    cbm::sclock_t const *_clk)
{
    if ( _seconds > -1)
    {
        if ( _clk->seconds() >= _seconds)
        {
            return (int)((_clk->seconds() - _seconds) / cbm::SEC_IN_DAY);
        }
        else
        {
            return 0;
        }
    }
    else
    {
        return -1;
    }
}
