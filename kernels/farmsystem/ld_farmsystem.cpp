/*!
 * @file
 * @author
 *  David Kraus
 *
 * @date
 *  2020
 */


/*!
 * @page ldndc_farmsystem
 * @tableofcontents
 * @section farmsystem_userguide User guide
 *
 * The FarmSystem model is meant to represent a farm that handles a list of field sites.
 * Field sites refer to individual LandscapeDNDC simulation instances.
 * The definition of an explicit farm that manges a field site is meant to:
 * - replace the static management definitions especially with regard to harvest dates
 * (so far mainly in croplands). This is especially useful for longterm-simulations when climate change
 * may require adjusted growing degree parametrizations and/or growing season lengths.
 * - establish communication between gridcells/fields (e.g., manure produced in one gridcell/field can be
 * transported and incorporated into the soil of another gridcell/field)
 *
 * @subsection farmsystem_userguide_setup Setup configurartion
 * The FarmSystem model requires two input definitions:
 * - Path to the json-formatted FarmSystem input: <em>file="source/to/input/file.farmsystem"</em>
 * - FarmSystem identifier, i.e. id that defines the FarmSystem-specific input information: <em>id="1"</em>
 *
 *  For \em LandscapeDNDC, the definition of a gridcell/setup  including the \em MoBiLE model as well as the \em FarmSystem model looks as follows:
 *  \code{.xml}
 *  <models>
 *    <model id="FarmSystem"/>
 *    <model id="_MoBiLE"/>
 *  </models>
 *  <FarmSystem file="%I/regional/testfarm/testfarm.farmsystem" id="1"/>
 *  <mobile>
 *    <modulelist>
 *    ...
 *    </modulelist>
 *  </mobile>
 *  \endcode
 */

#include  "farmsystem/ld_farmsystem.h"
#include  <input/setup/setup.h>
#include  <logging/cbm_logging.h>

LDNDC_KERNEL_OBJECT_DEFN(FarmSystem,farmsystem,"FarmSystem","LandscapeDNDC FarmSystem")

cbm::string_t ldndc::FarmSystem::data_file = "";
std::string   ldndc::FarmSystem::data_file_content = "";

ldndc::FarmSystem::FarmSystem()
        : cbm::kernel_t(),
        event_id( 0)
{
    m_output = NULL;
}


ldndc::FarmSystem::~FarmSystem()
{}


lerr_t
ldndc::FarmSystem::configure( cbm::RunLevelArgs *_args)
{
    setup::input_class_setup_t const * setup_in = _args->iokcomm->get_input_class< setup::input_class_setup_t >();
    if ( !setup_in)
    {
        KLOGERROR( "Failed to get setup input interface");
        return LDNDC_ERR_FAIL;
    }

    char *  farmsystem_cfg = NULL;
    setup_in->section_data( &farmsystem_cfg, ID());
    if ( !farmsystem_cfg)
    {
        KLOGERROR( "Failed to retrieve FarmSystem input");
        return LDNDC_ERR_FAIL;
    }

    cbm::ksetup_t  ksetup = setup_parser( farmsystem_cfg);
    CBM_FreeSectionData( farmsystem_cfg);
    if ( !ksetup.parse_ok())
    {
        KLOGERROR( "Failed to parse FarmSystem input");
        return LDNDC_ERR_FAIL;
    }

    event_id = ksetup.query_int( "/id", event_id);

    //data_file is only read once and its content is stored in static member
    if ( rank() == 0)
    {
        data_file = ksetup.query_string( "/file", data_file.c_str());
        data_file.format_expand();

        if ( !cbm::is_file( data_file.c_str()))
        {
            KLOGERROR( "opening data source failed  [source=",data_file,"]");
            return  LDNDC_ERR_READER_OPEN_FAILED;
        }

        std::ifstream ifs( data_file.c_str());
        std::string content( (std::istreambuf_iterator<char>(ifs)),
                             (std::istreambuf_iterator<char>()));
        data_file_content = content;
    }

    m_output = FarmSystemOutput( _args->iokcomm);

    lerr_t  rc_conf = m_output.configure( _args->cfg);
    if ( rc_conf)
    {
        KLOGERROR( "sink definition of ", ID(), " not successful");
        return  LDNDC_ERR_FAIL;
    }

    return LDNDC_ERR_OK;
}


lerr_t
ldndc::FarmSystem::initialize( cbm::RunLevelArgs *_args)
{
    lerr_t rc = read_file( _args);
    if ( rc)
    { return rc; }

    rc = m_output.initialize();
    if ( rc)
    { return  rc; }

    return LDNDC_ERR_OK;
}


lerr_t
ldndc::FarmSystem::solve( cbm::RunLevelArgs * _args)
{
    for ( size_t f = 0; f < fieldsites.size(); ++f)
    {
        lerr_t rc = fieldsites[f].schedule( _args);
        if ( rc)
        {
            LOGERROR("Schedule of field site: [",fieldsites[f].name(),"] not successful.");
            return LDNDC_ERR_FAIL;
        }

        rc = fieldsites[f].handle_field( _args, &m_output);
        if ( rc)
        {
            LOGERROR("Handling field site: [",fieldsites[f].name(),"] not successful.");
            return LDNDC_ERR_FAIL;
        }
    }

    if ( (int)_args->clk->subday() == (int)_args->clk->time_resolution())
    {
        lerr_t  rc_out = m_output.step( LD_RtCfg.clk);
        if ( rc_out)
        {
            LOGERROR("Writing output not successful.");
            return  LDNDC_ERR_FAIL;
        }
    }

    return LDNDC_ERR_OK;
}


lerr_t
ldndc::FarmSystem::finalize( cbm::RunLevelArgs *)
{
    for ( size_t f = 0; f < fieldsites.size(); ++f)
    {
        fieldsites[f].finalize();
    }

    lerr_t rc = m_output.finalize();
    if ( rc)
    { return  rc; }

    return LDNDC_ERR_OK;
}


lerr_t
ldndc::FarmSystem::register_ports( cbm::RunLevelArgs *)
{
    return LDNDC_ERR_OK;
}


lerr_t
ldndc::FarmSystem::unregister_ports( cbm::RunLevelArgs *)
{
    return LDNDC_ERR_OK;
}
