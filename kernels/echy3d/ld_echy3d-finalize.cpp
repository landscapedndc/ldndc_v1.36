/*!
 * @author
 *    David Kraus
 */

#include  "echy3d/ld_echy3d.h"

lerr_t
ldndc::EcHy3D::finalize( cbm::RunLevelArgs *_args)
{
    int rc = LDNDC_ERR_OK;
    for ( size_t c = 0; c < cells.size(); ++c)
    {
        EcHy3DCell *cell = &cells[c];

        rc = cell->AccumulatedPrecipitation.unsubscribe();
        if ( rc) { return LDNDC_ERR_FAIL; }

        rc = cell->AccumulatedThroughfall.unsubscribe();
        if ( rc) { return LDNDC_ERR_FAIL; }

        rc = cell->AccumulatedTranspiration.unsubscribe();
        if ( rc) { return LDNDC_ERR_FAIL; }

        rc = cell->AccumulatedInterceptionevaporation.unsubscribe();
        if ( rc) { return LDNDC_ERR_FAIL; }

        rc = cell->AccumulatedSoilevaporation.unsubscribe();
        if ( rc) { return LDNDC_ERR_FAIL; }

        rc = cell->AccumulatedSurfacewaterevaporation.unsubscribe();
        if ( rc) { return LDNDC_ERR_FAIL; }

        rc = cell->AccumulatedRunoff.unpublish_and_unsubscribe();
        if ( rc) { return LDNDC_ERR_FAIL; }

        rc = cell->AccumulatedPercolation.unpublish_and_unsubscribe();
        if ( rc) { return LDNDC_ERR_FAIL; }

        rc = cell->AccumulatedInfiltration.unpublish_and_unsubscribe();
        if ( rc) { return LDNDC_ERR_FAIL; }

        rc = cell->AccumulatedGroundwaterAccess.unpublish_and_unsubscribe();
        if ( rc) { return LDNDC_ERR_FAIL; }

        rc = cell->AccumulatedGroundwaterLoss.unpublish_and_unsubscribe();
        if ( rc) { return LDNDC_ERR_FAIL; }

        rc = cell->SoilPorosity.unsubscribe();
        if ( rc) { return LDNDC_ERR_FAIL; }

        rc = cell->SurfaceSnow.unpublish_and_unsubscribe();
        if ( rc) { return LDNDC_ERR_FAIL; }

        rc = cell->SurfaceWater.unpublish_and_unsubscribe();
        if ( rc) { return LDNDC_ERR_FAIL; }

        rc = cell->VolumetricWaterContent.unpublish_and_unsubscribe();
        if ( rc) { return LDNDC_ERR_FAIL; }

        rc = cell->SaturatedHydraulicConductivity.unpublish_and_unsubscribe();
        if ( rc) { return LDNDC_ERR_FAIL; }

        rc = cell->VolumetricFieldCapacity.unsubscribe();
        if ( rc) { return LDNDC_ERR_FAIL; }

        rc = cell->MaximumWaterFilledPoreSpace.unsubscribe();
        if ( rc) { return LDNDC_ERR_FAIL; }

        rc = cell->IceContent.unsubscribe();
        if ( rc) { return LDNDC_ERR_FAIL; }

        rc = cell->Soildepth.unsubscribe();
        if ( rc) { return LDNDC_ERR_FAIL; }

        rc = cell->SoilSO4.unpublish_and_unsubscribe();
        if ( rc) { return LDNDC_ERR_FAIL; }

        rc = cell->SoilNO3.unpublish_and_unsubscribe();
        if ( rc) { return LDNDC_ERR_FAIL; }

        rc = cell->SoilNH4.unpublish_and_unsubscribe();
        if ( rc) { return LDNDC_ERR_FAIL; }

        rc = cell->SoilDON.unpublish_and_unsubscribe();
        if ( rc) { return LDNDC_ERR_FAIL; }

        rc = cell->SoilDOC.unpublish_and_unsubscribe();
        if ( rc) { return LDNDC_ERR_FAIL; }

        rc = cell->AccumulatedLeachingNO3.unpublish_and_unsubscribe();
        if ( rc) { return LDNDC_ERR_FAIL; }

        rc = cell->AccumulatedLeachingNH4.unpublish_and_unsubscribe();
        if ( rc) { return LDNDC_ERR_FAIL; }

        rc = cell->AccumulatedLeachingDON.unpublish_and_unsubscribe();
        if ( rc) { return LDNDC_ERR_FAIL; }

        rc = cell->AccumulatedLeachingDOC.unpublish_and_unsubscribe();
        if ( rc) { return LDNDC_ERR_FAIL; }
    }

    _args->iokcomm->sink_handle_release( &this->m_sink);

    return LDNDC_ERR_OK;
}

