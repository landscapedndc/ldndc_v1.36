/*!
 * @author
 *    David Kraus
 */

#include  "echy3d/ld_echy3d.h"


lerr_t
ldndc::EcHy3D::EcHy3D_receive_state()
{
    for ( size_t c = 0; c < cells.size(); ++c)
    {
        EcHy3DCell *cell = &cells[c];

        cell->accumulated_precipitation = cell->AccumulatedPrecipitation.receive();
        cell->accumulated_throughfall = cell->AccumulatedThroughfall.receive();
        cell->AccumulatedTranspiration.receive( cell->accumulated_transpiration_sl);
        cell->accumulated_interceptionevaporation = cell->AccumulatedInterceptionevaporation.receive();
        cell->accumulated_soilevaporation = cell->AccumulatedSoilevaporation.receive();
        cell->accumulated_surfacewaterevaporation = cell->AccumulatedSurfacewaterevaporation.receive();
        cell->accumulated_surfacewater_loss = cell->AccumulatedRunoff.receive();
        cell->accumulated_percolation = cell->AccumulatedPercolation.receive();
        cell->accumulated_infiltration = cell->AccumulatedInfiltration.receive();
        cell->accumulated_groundwater_access = cell->AccumulatedGroundwaterAccess.receive();
        cell->accumulated_groundwater_loss = cell->AccumulatedGroundwaterLoss.receive();
        
        cell->SoilPorosity.receive( cell->porosity_sl);
        cell->surface_snow = cell->SurfaceSnow.receive();
        cell->surface_water = cell->SurfaceWater.receive();
        cell->VolumetricWaterContent.receive( cell->wc_sl);
        cell->SaturatedHydraulicConductivity.receive( cell->sks_sl);
        cell->VolumetricFieldCapacity.receive( cell->wc_fc_sl);
        cell->MaximumWaterFilledPoreSpace.receive( cell->wfps_max_sl);
        cell->IceContent.receive( cell->ice_sl);
        cell->Soildepth.receive( cell->depth_sl);
        cell->height_sl[0] = cell->depth_sl[0];
        for (int sl = 1; sl <= cell->nd_soil_layers-1; sl++)
        {
            cell->height_sl[sl] = cell->depth_sl[sl] - cell->depth_sl[sl-1];
        }

        cell->SoilSO4.receive( cell->so4_sl);
        cell->SoilNO3.receive( cell->no3_sl);
        cell->SoilNH4.receive( cell->nh4_sl);
        cell->SoilDON.receive( cell->don_sl);
        cell->SoilDOC.receive( cell->doc_sl);

        cell->accumulated_outflow_no3 = cell->AccumulatedLeachingNO3.receive();
        cell->accumulated_outflow_nh4 = cell->AccumulatedLeachingNH4.receive();
        cell->accumulated_outflow_don = cell->AccumulatedLeachingDON.receive();

        cell->accumulated_outflow_doc = cell->AccumulatedLeachingDOC.receive();
    }

    return LDNDC_ERR_OK;
}


lerr_t
ldndc::EcHy3D::EcHy3D_send_state( cbm::RunLevelArgs *)
{
    for ( size_t c = 0; c < cells.size(); ++c)
    {
        EcHy3DCell *cell = &cells[c];

        cell->AccumulatedRunoff.send( cell->accumulated_surfacewater_loss);
        cell->AccumulatedPercolation.send( cell->accumulated_percolation);
        cell->AccumulatedInfiltration.send( cell->accumulated_infiltration);
        cell->AccumulatedGroundwaterAccess.send( cell->accumulated_groundwater_access);
        cell->AccumulatedGroundwaterLoss.send( cell->accumulated_groundwater_loss);

        cell->SurfaceWater.send( cell->surface_water);
        cell->VolumetricWaterContent.send( cell->wc_sl);
        cell->SaturatedHydraulicConductivity.send( cell->sks_sl);

        cell->SoilSO4.send( cell->so4_sl);
        cell->SoilNO3.send( cell->no3_sl);
        cell->SoilNH4.send( cell->nh4_sl);
        cell->SoilDON.send( cell->don_sl);
        cell->SoilDOC.send( cell->doc_sl);

        cell->AccumulatedLeachingNO3.send( cell->accumulated_outflow_no3);
        cell->AccumulatedLeachingNH4.send( cell->accumulated_outflow_nh4);
        cell->AccumulatedLeachingDON.send( cell->accumulated_outflow_don);
        cell->AccumulatedLeachingDOC.send( cell->accumulated_outflow_doc);
    }
    return LDNDC_ERR_OK;
}

