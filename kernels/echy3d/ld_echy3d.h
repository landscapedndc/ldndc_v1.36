/*!
 * @brief
 *    Dynamic farmer
 *
 * @author
 *    David Kraus
 */

#ifndef  LDNDC_KERNEL_ECHY3D_H_
#define  LDNDC_KERNEL_ECHY3D_H_

#include  "ld_kernel.h"
#include  "ld_shared.h"
#include  "ld_sinkinterface.h"
#include  "state/mbe_state.h"
#include <map>

namespace ldndc {

struct EcHy3DNeighbor
{
    EcHy3DNeighbor( cbm::source_descriptor_t, double /* intersect */);

    cbm::source_descriptor_t  desc;
    double  intersect;  // [m]
    double  water_flux; // [m3]
    double  so4_flux;   // [kg S]
    double  no3_flux;   // [kg N]
    double  nh4_flux;   // [kg N]
    double  don_flux;   // [kg N]
    double  doc_flux;   // [kg C]

    //entry of neighbor in parent EcHy3DCell vector
    int cell_map;
};

class EcHy3DCell
{

public:

    EcHy3DCell( int, int, double, double, double, double);
    ~EcHy3DCell();

    int setup_id;
    int site_id;

    double x;   // [m]
    double y;   // [m]
    double z;   //height of soil surface [m]

    bool outlet;
    double river_output_potential;  // downwards from the soil surface [m]
    double river_length;            // [m]
    double river_elevation;         // [m]

    double area;                    // [m2]

    cbm::string_t ecosystem_type;   // {arable, forest, grassland, ...}

    int nd_soil_layers;             // number of soil layers

    std::vector< EcHy3DNeighbor > neighbors;

    lerr_t set_soil_layers( input_class_site_t *);

    double sks_mean;
    double sks_gw;
    double sks_bottom;
    double delta_groundwater_volume;
    double delta_surfacewater_volume;
    double delta_riverwater_volume;
    double delta_so4;                           // [kg S]
    double delta_no3;                           // [kg N]
    double delta_no3_gain_ecosystem_specific;   // [kg N]
    double delta_no3_loss_ecosystem_specific;   // [kg N]
    double delta_nh4;                           // [kg N]
    double delta_don;                           // [kg N]
    double delta_doc;                           // [kg C]
    double delta_groundwater_gain_ecosystem_specific;   // []
    double delta_groundwater_loss_ecosystem_specific;   // []
    int sl_gw;
    
    double scale_out;

    double available_so4;   // [kg S]
    double available_no3;   // [kg N]
    double available_nh4;   // [kg N]
    double available_don;   // [kg N]
    double available_doc;   // [kg C]

    lerr_t reset();
    
    double pressure_head;   // [m]
    lerr_t step_init( cbm::RunLevelArgs *);

    lerr_t EcHy3DCell_update_soil_water_content( cbm::RunLevelArgs *);
    lerr_t EcHy3DCell_update_surface_water_content( cbm::RunLevelArgs *);
    lerr_t EcHy3DCell_update_river_water_content( cbm::RunLevelArgs *);
    lerr_t EcHy3DCell_boundary_flows( cbm::RunLevelArgs *);

    lerr_t EcHy3DCell_update_nutrient_transport( double &_delta_val,
                                                 double &_accumulated_val,
                                                 lvector_t< double > &_val,
                                                 cbm::string_t _name);

    double accumulated_precipitation;
    double accumulated_throughfall;
    lvector_t< double > accumulated_transpiration_sl;
    double accumulated_interceptionevaporation;
    double accumulated_soilevaporation;
    double accumulated_surfacewaterevaporation;
    double accumulated_percolation;
    double accumulated_infiltration;
    double accumulated_groundwater_access;      // [m3:m-2]
    double accumulated_groundwater_loss;        // [m3:m-2]
    double accumulated_surfacewater_access;     // [m3:m-2]
    double accumulated_surfacewater_loss;       // [m3:m-2]
    double accumulated_outlet;
    double accumulated_outflow_soil;
    double accumulated_outflow_surface;

    double accumulated_outflow_so4; // [kg S m-2]
    double accumulated_outflow_no3; // [kg N m-2]
    double accumulated_outflow_nh4; // [kg N m-2]
    double accumulated_outflow_don; // [kg N m-2]
    double accumulated_outflow_doc; // [kg C m-2]

    double accumulated_precipitation_old;
    double accumulated_throughfall_old;
    double accumulated_transpiration_old;
    double accumulated_interceptionevaporation_old;
    double accumulated_soilevaporation_old;
    double accumulated_surfacewaterevaporation_old;
    double accumulated_infiltration_old;
    double accumulated_groundwater_access_old;      // [m3:m-2]
    double accumulated_groundwater_loss_old;        // [m3:m-2]
    double accumulated_surfacewater_access_old;     // [m3:m-2]
    double accumulated_surfacewater_loss_old;       // [m3:m-2]
    double accumulated_outlet_old;
    double accumulated_outflow_soil_old;
    double accumulated_outflow_surface_old;

    double accumulated_outflow_so4_old; // [kg S m-2]
    double accumulated_outflow_no3_old; // [kg N m-2]
    double accumulated_outflow_nh4_old; // [kg N m-2]
    double accumulated_outflow_don_old; // [kg N m-2]
    double accumulated_outflow_doc_old; // [kg C m-2]

    SubscribedField<double>  AccumulatedPrecipitation;
    SubscribedField<double>  AccumulatedThroughfall;
    SubscribedVectorField<double>  AccumulatedTranspiration;
    SubscribedField<double>  AccumulatedInterceptionevaporation;
    SubscribedField<double>  AccumulatedSoilevaporation;
    SubscribedField<double>  AccumulatedSurfacewaterevaporation;
    PublishedAndSubscribedField<double>  AccumulatedRunoff;
    PublishedAndSubscribedField<double>  AccumulatedPercolation;
    PublishedAndSubscribedField<double>  AccumulatedInfiltration;
    PublishedAndSubscribedField<double>  AccumulatedGroundwaterAccess;
    PublishedAndSubscribedField<double>  AccumulatedGroundwaterLoss;

    lvector_t< double > porosity_sl;
    SubscribedVectorField<double> SoilPorosity;

    double river_water;     // [m]

    double surface_snow;
    PublishedAndSubscribedField<double> SurfaceSnow;

    double surface_water;   // [m]
    PublishedAndSubscribedField<double> SurfaceWater;

    lvector_t< double > wc_sl;
    PublishedAndSubscribedVectorField<double> VolumetricWaterContent;
    
    lvector_t< double > sks_sl;
    PublishedAndSubscribedVectorField<double> SaturatedHydraulicConductivity;
    
    lvector_t< double > wc_fc_sl;
    SubscribedVectorField<double> VolumetricFieldCapacity;

    lvector_t< double > wfps_max_sl;
    SubscribedVectorField<double> MaximumWaterFilledPoreSpace;

    lvector_t< double > ice_sl;
    SubscribedVectorField<double> IceContent;

    lvector_t< double > height_sl;
    lvector_t< double > depth_sl;
    SubscribedVectorField<double> Soildepth;

    lvector_t< double > so4_sl;
    PublishedAndSubscribedVectorField<double> SoilSO4;

    lvector_t< double > no3_sl;
    PublishedAndSubscribedVectorField<double> SoilNO3;

    lvector_t< double > nh4_sl;
    PublishedAndSubscribedVectorField<double> SoilNH4;
    
    lvector_t< double > don_sl;
    PublishedAndSubscribedVectorField<double> SoilDON;
    
    lvector_t< double > doc_sl;
    PublishedAndSubscribedVectorField<double> SoilDOC;

    PublishedAndSubscribedField<double> AccumulatedLeachingNO3;
    PublishedAndSubscribedField<double> AccumulatedLeachingNH4;
    PublishedAndSubscribedField<double> AccumulatedLeachingDON;
    PublishedAndSubscribedField<double> AccumulatedLeachingDOC;

    double
    max_depth();

    double
    get_runoff();

    double
    get_ground_water_available();

    double
    get_ground_water();

    double
    get_soil_water();

    double
    get_total_water();

    double
    get_river_potential();

    double
    get_groundwater_share( lvector_t< double > &_source);
};



class LDNDC_API EcHy3D : public cbm::kernel_t
{
    LDNDC_KERNEL_OBJECT(EcHy3D,echy3d)

public:
    static double const WFPS_THRESHOLD;

public:
    EcHy3D();
    ~EcHy3D();

    lerr_t  configure( cbm::RunLevelArgs *);
    lerr_t  initialize( cbm::RunLevelArgs *);
    
    //we solve with read due to mobile communication
    lerr_t  read( cbm::RunLevelArgs *);
    lerr_t  write( cbm::RunLevelArgs *);

    lerr_t  finalize( cbm::RunLevelArgs *);


private:
    
    lerr_t EcHy3D_initialize_cells();
    lerr_t EcHy3D_initialize_cell_communication( cbm::RunLevelArgs *);
    lerr_t EcHy3D_initialize_sinks( cbm::RunLevelArgs *);
    
    lerr_t EcHy3D_receive_state();
    lerr_t EcHy3D_send_state( cbm::RunLevelArgs *);
    lerr_t EcHy3D_write( cbm::RunLevelArgs *);


    std::vector< EcHy3DCell > cells;

    cbm::string_t data_file;
    std::string data_file_content;

    int kernel_setup_id;

private:
    
    lerr_t
    collect_datarecord(
                       ldndc_flt64_t * /* data buffer */);

    lerr_t  m_writerecord( ldndc_flt64_t *);
    ldndc::sink_handle_t  m_sink;
    ldndc::SinkInterface  m_sif;

    /*!
     * @brief
     *  Returns pointer to cell with given id
     */
    EcHy3DCell *
    get_cell( int /* cell id */);
    
private:

    /* hide these buggers for now */
    EcHy3D( EcHy3D const &);
    EcHy3D &  operator=( EcHy3D const &);
};
} /* namespace ldndc */

#endif /* !LDNDC_KERNEL_ECHY3D_H_ */

