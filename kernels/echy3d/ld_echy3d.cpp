/*!
 * @file
 * @author
 *    David Kraus
 */

#include  "echy3d/ld_echy3d.h"
#include  <input/setup/setup.h>
#include  <logging/cbm_logging.h>
#include  "app/ld_init.h"
#include  <io-dcomm.h>


/*!
 * @page ldndc_echy3d
 * @section ldndc_echy3d_guide User guide
 * ...
 */

double const ldndc::EcHy3D::WFPS_THRESHOLD = 0.8;

lerr_t
ldndc::EcHy3D::read( cbm::RunLevelArgs *_args)
{
    lerr_t rc = EcHy3D_receive_state();
    if (rc){ return rc; }
    
    for ( size_t c = 0; c < cells.size(); ++c)
    {
        EcHy3DCell *cell = &cells[c];
        cell->step_init( _args);
    }
    
    //BALANCE
    double water_balance( 0.0);
    double no3_balance( 0.0);
    for ( size_t c = 0; c < cells.size(); ++c)
    {
        EcHy3DCell *cell = &cells[c];
        water_balance += cell->get_total_water() * cell->area;
        water_balance += cell->accumulated_outlet * cell->area;
        no3_balance += cell->no3_sl.sum() * cell->area;
        no3_balance += cell->accumulated_outflow_no3 * cell->area;
    }

    //SOLVE
    for ( size_t c = 0; c < cells.size(); ++c)
    {
        EcHy3DCell *cell = &cells[c];

        double water_flux_sum( 0.0);
        double gradient_surface_inv_sum( 0.0);
        double gradient_river_inv_sum( 0.0);
        for (size_t n=0; n < cell->neighbors.size(); ++n)
        {
            EcHy3DCell *neighbor = &cells[cell->neighbors[n].cell_map];

            // Pythagorean theorem
            double const delta_y( std::abs( cell->y - neighbor->y));    // [m]
            double const delta_x( std::abs( cell->x - neighbor->x));    // [m]
            double const distance( cbm::sqrt( cbm::sqr( delta_x) + cbm::sqr( delta_y)));    // [m]

            //soil water flux
            double const gradient_soil_inv( (cell->pressure_head - neighbor->pressure_head) / distance);    // [-]
            if ( cbm::flt_greater_zero( gradient_soil_inv))
            {
                //height of interface taken from outflow cell
                double const interface( cell->neighbors[n].intersect * cbm::bound_min( 0.0, cell->pressure_head - cell->max_depth()));
                double const water_flux( cbm::harmonic_mean2( cell->sks_mean, neighbor->sks_mean) * gradient_soil_inv * interface);
                cell->neighbors[n].water_flux = water_flux;
                water_flux_sum += water_flux;
            }

            //surface water flux
            double const gradient_surface_inv( (cell->z - neighbor->z) / distance);
            if ( cbm::flt_greater_zero( gradient_surface_inv))
            {
                gradient_surface_inv_sum += 1.000000001 * gradient_surface_inv;
            }

            //river water flux
            if ( cbm::is_valid( neighbor->river_output_potential))
            {
                double const gradient_river_inv( (cell->get_river_potential() - neighbor->get_river_potential()) / distance);
                if ( cbm::flt_greater_zero( gradient_river_inv))
                {
                    gradient_river_inv_sum += 1.000000001 * gradient_river_inv;
                }
            }
        }

        //sum of surface runoff from cell [m3]
        double const surface_flux_sum( cbm::flt_greater_zero( gradient_surface_inv_sum) ?
                                       cell->get_runoff() * cell->area :
                                       0.0);
        
        double const river_flux_sum( cbm::flt_greater_zero( gradient_river_inv_sum) ?
                                     cell->river_water * cell->area :
                                     0.0);

        //soil water flux
        if ( cbm::flt_greater_zero( water_flux_sum))
        {
            double const groundwater_total( cell->get_ground_water() * cell->area);
            double const groundwater_avail( cell->get_ground_water_available() * cell->area);
            
            //scale down if predicted water flux is larger than available water volume
            double const water_scale( cbm::bound_max( groundwater_avail / water_flux_sum, 1.0));
            
            for (size_t n=0; n < cell->neighbors.size(); ++n)
            {
                EcHy3DCell *neighbor = &cells[cell->neighbors[n].cell_map];
                if ( !cbm::flt_greater_zero( cell->neighbors[n].water_flux))
                {
                    continue;
                }

                //scale down water flux if needed
                cell->neighbors[n].water_flux *= water_scale;

                cell->delta_groundwater_volume -= cell->neighbors[n].water_flux;
                cell->accumulated_groundwater_loss += cell->neighbors[n].water_flux / cell->area;

                neighbor->delta_groundwater_volume += cell->neighbors[n].water_flux;
                neighbor->accumulated_groundwater_access += cell->neighbors[n].water_flux / neighbor->area;

                if ( cell->ecosystem_type != neighbor->ecosystem_type)
                {
                    cell->delta_groundwater_loss_ecosystem_specific += cell->neighbors[n].water_flux;
                    neighbor->delta_groundwater_gain_ecosystem_specific += cell->neighbors[n].water_flux;
                }

                //nutrient flow
                if ( cbm::flt_greater_zero( groundwater_total))
                {
                    double const transport_fraction( cbm::bound_max( 0.99 * cell->neighbors[n].water_flux / groundwater_total, 0.99));
                    cell->neighbors[n].so4_flux = transport_fraction * cell->available_so4;
                    cell->delta_so4 -= cell->neighbors[n].so4_flux;
                    neighbor->delta_so4 += cell->neighbors[n].so4_flux;

                    cell->neighbors[n].no3_flux = transport_fraction * cell->available_no3;
                    cell->delta_no3 -= cell->neighbors[n].no3_flux;
                    neighbor->delta_no3 += cell->neighbors[n].no3_flux;

                    if ( cell->ecosystem_type != neighbor->ecosystem_type)
                    {
                        cell->delta_no3_loss_ecosystem_specific += cell->neighbors[n].no3_flux;
                        neighbor->delta_no3_gain_ecosystem_specific += cell->neighbors[n].no3_flux;
                    }

                    cell->neighbors[n].nh4_flux = transport_fraction * cell->available_nh4;
                    cell->delta_nh4 -= cell->neighbors[n].nh4_flux;
                    neighbor->delta_nh4 += cell->neighbors[n].nh4_flux;

                    cell->neighbors[n].don_flux = transport_fraction * cell->available_don;
                    cell->delta_don -= cell->neighbors[n].don_flux;
                    neighbor->delta_don += cell->neighbors[n].don_flux;

                    cell->neighbors[n].doc_flux = transport_fraction * cell->available_doc;
                    cell->delta_doc -= cell->neighbors[n].doc_flux;
                    neighbor->delta_doc += cell->neighbors[n].doc_flux;
                }
            }
        }

        //surface water flux
        if ( cbm::flt_greater_zero( surface_flux_sum))
        {
            for ( size_t n=0; n < cell->neighbors.size(); ++n)
            {
                EcHy3DCell *neighbor = &cells[cell->neighbors[n].cell_map];

                double const delta_y( std::abs( cell->y - neighbor->y));
                double const delta_x( std::abs( cell->x - neighbor->x));
                double const distance( cbm::sqrt( cbm::sqr( delta_x) + cbm::sqr( delta_y)));

                double const gradient_surface_inv( (cell->z - neighbor->z) / distance);
                if ( cbm::flt_greater_zero( gradient_surface_inv))
                {
                    double const gradient_surface_fraction( cbm::bound( 0.0,
                                                                        gradient_surface_inv / gradient_surface_inv_sum,
                                                                        0.99));
                    double const surface_flux( gradient_surface_fraction * surface_flux_sum);

                    cell->delta_surfacewater_volume -= surface_flux;
                    cell->accumulated_surfacewater_loss += surface_flux / cell->area;

                    neighbor->delta_surfacewater_volume += surface_flux;
                    neighbor->accumulated_surfacewater_access += surface_flux / neighbor->area;
                }
            }
        }

        //river water flux
        if ( cbm::flt_greater_zero( river_flux_sum))
        {
            for ( size_t n=0; n < cell->neighbors.size(); ++n)
            {
                EcHy3DCell *neighbor = &cells[cell->neighbors[n].cell_map];
                if ( cbm::is_valid( neighbor->river_output_potential))
                {
                    double const delta_y( std::abs( cell->y - neighbor->y));
                    double const delta_x( std::abs( cell->x - neighbor->x));
                    double const distance( cbm::sqrt( cbm::sqr( delta_x) + cbm::sqr( delta_y)));

                    double const gradient_river_inv( (cell->get_river_potential() - neighbor->get_river_potential()) / distance);
                    if ( cbm::flt_greater_zero( gradient_river_inv))
                    {
                        double const gradient_river_fraction( cbm::bound( 0.0,
                                                                          gradient_river_inv / gradient_river_inv_sum,
                                                                          0.99));
                        double const river_flux( gradient_river_fraction * river_flux_sum);

                        cell->delta_riverwater_volume -= river_flux;
                        neighbor->delta_riverwater_volume += river_flux;
                    }
                }
            }
        }
    }

    //UPDATE
    for ( size_t c = 0; c < cells.size(); ++c)
    {
        EcHy3DCell *cell = &cells[c];

        rc = cell->EcHy3DCell_update_soil_water_content( _args);
        if (rc){ return rc; }

        rc = cell->EcHy3DCell_update_surface_water_content( _args);
        if (rc){ return rc; }

        rc = cell->EcHy3DCell_update_river_water_content( _args);
        if (rc){ return rc; }

        rc = cell->EcHy3DCell_boundary_flows( _args);
        if (rc){ return rc; }

        rc = cell->EcHy3DCell_update_nutrient_transport( cell->delta_so4, cell->accumulated_outflow_so4, cell->so4_sl, "so4");
        if (rc){ return rc; }

        rc = cell->EcHy3DCell_update_nutrient_transport( cell->delta_no3, cell->accumulated_outflow_no3, cell->no3_sl, "no3");
        if (rc){ return rc; }

        rc = cell->EcHy3DCell_update_nutrient_transport( cell->delta_nh4, cell->accumulated_outflow_nh4, cell->nh4_sl, "nh4");
        if (rc){ return rc; }

        rc = cell->EcHy3DCell_update_nutrient_transport( cell->delta_don, cell->accumulated_outflow_don, cell->don_sl, "don");
        if (rc){ return rc; }

        rc = cell->EcHy3DCell_update_nutrient_transport( cell->delta_doc, cell->accumulated_outflow_doc, cell->doc_sl, "doc");
        if (rc){ return rc; }
    }

    //BALANCE
    for ( size_t c = 0; c < cells.size(); ++c)
    {
        EcHy3DCell *cell = &cells[c];
        water_balance -= cell->get_total_water() * cell->area;
        water_balance -= cell->accumulated_outlet * cell->area;
        no3_balance -= cell->no3_sl.sum() * cell->area;
        no3_balance -= cell->accumulated_outflow_no3 * cell->area;
    }

    if ( cbm::flt_greater( std::abs( water_balance), 1.0e-6))
    {
        KLOGERROR(name(),": water-leakage of: ", water_balance);
        return LDNDC_ERR_FAIL;
    }

    if ( cbm::flt_greater( std::abs( no3_balance), 1.0e-6))
    {
        KLOGERROR(name(),": no3-leakage of: ", no3_balance);
        return LDNDC_ERR_FAIL;
    }

    rc = EcHy3D_send_state( _args);
    if (rc){ return rc; }

    return LDNDC_ERR_OK;
}


lerr_t
ldndc::EcHy3D::write( cbm::RunLevelArgs *_args)
{
    lerr_t rc = EcHy3D_receive_state();
    if (rc){ return rc; }

    EcHy3D_write( _args);

    return LDNDC_ERR_OK;
}


ldndc::EcHy3DCell *
ldndc::EcHy3D::get_cell( int _cell_id)
{
    for ( size_t c = 0; c < cells.size(); ++c)
    {
        EcHy3DCell *cell = &cells[c];
        if ( cell->setup_id == _cell_id)
        {
            return cell;
        }
    }
    return NULL;
}
