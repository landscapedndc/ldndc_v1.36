/*!
 * @file
 * @author
 *    David Kraus
 */

#include  "echy3d/ld_echy3d.h"


/*!
 * @page ldndc_echy3d
 * @section ldndc_echy3d_output Output
 * ...
 */
static ldndc_string_t const EcHy3D_Ids[] =
{
    "cell_id[-]",
    "cell_x[-]",
    "cell_y[-]",
    "cell_z[-]",
    "cell_area[ha]",

    "groundwater_head[m]",

    "soilwater[m3]",
    "groundwater[m3]",
    "riverwater[m3]",
    "surfacewater[m3]",
    "surfacesnow[m3]",

    "N_sol[kg]",
    "N_no3[kg]",
    "N_no3_groundwater[kg]",

    "dN_no3_inflow_ecosystem_domain[kg]",
    "dN_no3_outflow_ecosystem_domain[kg]",
    "groundwater_inflow_ecosystem_domain[m3]",
    "groundwater_outflow_ecosystem_domain[m3]",

    "precipitation[m3]",
    "throughfall[m3]",
    "infiltration[m3]",
    "surfacewater_access[m3]",
    "surfacewater_loss[m3]",
    "groundwater_access[m3]",
    "groundwater_loss[m3]",
    "transpiration[m3]",
    "soil_evaporation[m3]",
    "surface_evaporation[m3]",
    "interception_evaporation[m3]",

    "outflow_outlet[m3]",
    "outflow_soil_to_river[m3]",
    "outflow_surface_to_river[m3]",
    "dN_no3_outflow_total_domain[kg]"
};

static ldndc_string_t const *  EcHy3D_Header = EcHy3D_Ids;
#define  EcHy3D_Datasize  (sizeof( EcHy3D_Ids) / sizeof( EcHy3D_Ids[0]))
static ldndc_output_size_t const  EcHy3D_Sizes[] =
{
    EcHy3D_Datasize,
    EcHy3D_Datasize /*total size*/
};

static ldndc_output_size_t const *  EcHy3D_EntitySizes = NULL;
#define  EcHy3D_Rank  ((ldndc_output_rank_t)(sizeof( EcHy3D_Sizes) / sizeof( EcHy3D_Sizes[0])) - 1)
static atomic_datatype_t const  EcHy3D_Types[] =
{
    LDNDC_FLOAT64
};


lerr_t
ldndc::EcHy3D::EcHy3D_initialize_sinks( cbm::RunLevelArgs *_args)
{
    this->m_sink = _args->iokcomm->sink_handle_acquire( "echy3d");
    if ( this->m_sink.status() == LDNDC_ERR_OK)
    {
        lerr_t  rc_layout = LDNDC_OUTPUT_DEFINE_SINK_LAYOUT(
                                                            this->m_sink,EcHy3D,this->m_sif.get_metaflags());
        
        RETURN_IF_NOT_OK(rc_layout);
    }
    else
    {
        KLOGERROR( "sink status bad  [sink=","echy3d","]");
        return  this->m_sink.status();
    }
    
    return LDNDC_ERR_OK;
}


#define REGIONAL_WATER_BALANCE_OUTPUT( __val__, __sl__)                                                         \
{                                                                                                               \
__val__##_regional += (cell->accumulated_##__val__##__sl__ - cell->accumulated_##__val__##_old) * cell->area;   \
data_flt64_0[i] = (cell->accumulated_##__val__##__sl__ - cell->accumulated_##__val__##_old) * cell->area;       \
cell->accumulated_##__val__##_old = cell->accumulated_##__val__##__sl__;                                        \
i++;                                                                                                            \
}                                                                                                               \



lerr_t
ldndc::EcHy3D::EcHy3D_write( cbm::RunLevelArgs *_args)
{
    if ( _args->clk->subday() == (unsigned int)_args->clk->time_resolution())
    {
        ldndc_flt64_t  data_flt64_0[EcHy3D_Datasize];

        double soil_water_regional( 0.0);
        double ground_water_regional( 0.0);
        double river_water_regional( 0.0);
        double surface_water_regional( 0.0);
        double surface_snow_regional( 0.0);
        double n_sol_regional( 0.0);
        double no3_regional( 0.0);
        double no3_groundwater_regional( 0.0);
        double area_regional( 0.0);

        double precipitation_regional( 0.0);
        double throughfall_regional( 0.0);
        double infiltration_regional( 0.0);
        double surfacewater_access_regional( 0.0);
        double surfacewater_loss_regional( 0.0);
        double groundwater_access_regional( 0.0);
        double groundwater_loss_regional( 0.0);
        double transpiration_regional( 0.0);
        double soilevaporation_regional( 0.0);
        double surfacewaterevaporation_regional( 0.0);
        double interceptionevaporation_regional( 0.0);
        double outlet_regional( 0.0);
        double outflow_soil_regional( 0.0);
        double outflow_surface_regional( 0.0);
        double outflow_no3_regional( 0.0);

        for ( size_t c = 0; c < cells.size(); ++c)
        {
            EcHy3DCell *cell = &cells[c];
            size_t i( 0);

            data_flt64_0[i] = cell->setup_id;
            i++;
            data_flt64_0[i] = cell->x;
            i++;
            data_flt64_0[i] = cell->y;
            i++;
            data_flt64_0[i] = cell->z;
            i++;
            area_regional += cell->area * cbm::HA_IN_M2;
            data_flt64_0[i] = cell->area * cbm::HA_IN_M2;
            i++;

            data_flt64_0[i] = cell->pressure_head;
            i++;

            soil_water_regional += cell->get_soil_water() * cell->area;
            data_flt64_0[i] = cell->get_soil_water() * cell->area;
            i++;
            ground_water_regional += cell->get_ground_water() * cell->area;
            data_flt64_0[i] = cell->get_ground_water() * cell->area;
            i++;
            river_water_regional += cell->river_water * cell->area;
            data_flt64_0[i] = cell->river_water * cell->area;
            i++;
            surface_water_regional += cell->surface_water * cell->area;
            data_flt64_0[i] = cell->surface_water * cell->area;
            i++;
            surface_snow_regional += cell->surface_snow * cell->area;
            data_flt64_0[i] = cell->surface_snow * cell->area;
            i++;

            n_sol_regional += (cell->no3_sl.sum()+cell->nh4_sl.sum()+cell->don_sl.sum()) * cell->area;
            data_flt64_0[i] = (cell->no3_sl.sum()+cell->nh4_sl.sum()+cell->don_sl.sum()) * cell->area;
            i++;
            no3_regional += cell->no3_sl.sum() * cell->area;
            data_flt64_0[i] = cell->no3_sl.sum() * cell->area;
            i++;
            no3_groundwater_regional += cell->get_groundwater_share( cell->no3_sl) * cell->area;
            data_flt64_0[i] = cell->get_groundwater_share( cell->no3_sl) * cell->area;
            i++;

            data_flt64_0[i] = cell->delta_no3_gain_ecosystem_specific;
            i++;
            data_flt64_0[i] = cell->delta_no3_loss_ecosystem_specific;
            i++;
            cell->delta_no3_gain_ecosystem_specific = 0.0;
            cell->delta_no3_loss_ecosystem_specific = 0.0;

            data_flt64_0[i] = cell->delta_groundwater_gain_ecosystem_specific;
            i++;
            data_flt64_0[i] = cell->delta_groundwater_loss_ecosystem_specific;
            i++;
            cell->delta_groundwater_gain_ecosystem_specific = 0.0;
            cell->delta_groundwater_loss_ecosystem_specific = 0.0;

            REGIONAL_WATER_BALANCE_OUTPUT( precipitation, )
            REGIONAL_WATER_BALANCE_OUTPUT( throughfall, )
            REGIONAL_WATER_BALANCE_OUTPUT( infiltration, )
            REGIONAL_WATER_BALANCE_OUTPUT( surfacewater_access, )
            REGIONAL_WATER_BALANCE_OUTPUT( surfacewater_loss, )
            REGIONAL_WATER_BALANCE_OUTPUT( groundwater_access, )
            REGIONAL_WATER_BALANCE_OUTPUT( groundwater_loss, )
            REGIONAL_WATER_BALANCE_OUTPUT( transpiration, _sl.sum())
            REGIONAL_WATER_BALANCE_OUTPUT( soilevaporation, )
            REGIONAL_WATER_BALANCE_OUTPUT( surfacewaterevaporation, )
            REGIONAL_WATER_BALANCE_OUTPUT( interceptionevaporation, )

            REGIONAL_WATER_BALANCE_OUTPUT( outlet, )
            REGIONAL_WATER_BALANCE_OUTPUT( outflow_soil, )
            REGIONAL_WATER_BALANCE_OUTPUT( outflow_surface, )
            REGIONAL_WATER_BALANCE_OUTPUT( outflow_no3, )

            void *  data[] = { data_flt64_0};
            lerr_t  rc_write = this->m_sif.write_fixed_record( &this->m_sink, data, _args->clk);
            if ( rc_write){ return  LDNDC_ERR_FAIL; }
        }

        size_t i( 0);

        data_flt64_0[i] = (double)this->object_id();
        i++;
        data_flt64_0[i] = invalid_dbl;  //x-location
        i++;
        data_flt64_0[i] = invalid_dbl;  //y-location
        i++;
        data_flt64_0[i] = invalid_dbl;  //z-location
        i++;
        data_flt64_0[i] = area_regional;
        i++;

        data_flt64_0[i] = invalid_dbl;  //z-location
        i++;

        data_flt64_0[i] = soil_water_regional;
        i++;
        data_flt64_0[i] = ground_water_regional;
        i++;
        data_flt64_0[i] = river_water_regional;
        i++;
        data_flt64_0[i] = surface_water_regional;
        i++;
        data_flt64_0[i] = surface_snow_regional;
        i++;

        data_flt64_0[i] = n_sol_regional;
        i++;
        data_flt64_0[i] = no3_regional;
        i++;
        data_flt64_0[i] = no3_groundwater_regional;
        i++;

        data_flt64_0[i] = invalid_dbl;  //no3 gain
        i++;
        data_flt64_0[i] = invalid_dbl;  //no3 loss
        i++;
        data_flt64_0[i] = invalid_dbl;  //groundwater gain
        i++;
        data_flt64_0[i] = invalid_dbl;  //groundwater loss
        i++;

        data_flt64_0[i] = precipitation_regional;
        i++;
        data_flt64_0[i] = throughfall_regional;
        i++;
        data_flt64_0[i] = infiltration_regional;
        i++;
        data_flt64_0[i] = surfacewater_access_regional;
        i++;
        data_flt64_0[i] = surfacewater_loss_regional;
        i++;
        data_flt64_0[i] = groundwater_access_regional;
        i++;
        data_flt64_0[i] = groundwater_loss_regional;
        i++;
        data_flt64_0[i] = transpiration_regional;
        i++;
        data_flt64_0[i] = soilevaporation_regional;
        i++;
        data_flt64_0[i] = surfacewaterevaporation_regional;
        i++;
        data_flt64_0[i] = interceptionevaporation_regional;
        i++;

        data_flt64_0[i] = outlet_regional;
        i++;
        data_flt64_0[i] = outflow_soil_regional;
        i++;
        data_flt64_0[i] = outflow_surface_regional;
        i++;
        data_flt64_0[i] = outflow_no3_regional;

        void *  data[] = { data_flt64_0};
        lerr_t  rc_write = this->m_sif.write_fixed_record( &this->m_sink, data, _args->clk);
        if ( rc_write){ return  LDNDC_ERR_FAIL; }
    }

    return LDNDC_ERR_OK;
}
