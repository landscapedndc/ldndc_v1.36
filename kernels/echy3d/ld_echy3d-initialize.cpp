/*!
 * @author
 *    David Kraus
 */

#include  "echy3d/ld_echy3d.h"
#include  <input/setup/setup.h>
#include  <logging/cbm_logging.h>
#include  "app/ld_init.h"
#include  <io-dcomm.h>

LDNDC_KERNEL_OBJECT_DEFN(EcHy3D,echy3d,"EcHy3D","LandscapeDNDC EcHy3D")

ldndc::EcHy3D::EcHy3D() : cbm::kernel_t()
{}

ldndc::EcHy3D::~EcHy3D()
{}


lerr_t
ldndc::EcHy3D::configure( cbm::RunLevelArgs *_args)
{
    setup::input_class_setup_t const * setup_in = _args->iokcomm->get_input_class< setup::input_class_setup_t >();
    if ( !setup_in)
    {
        KLOGERROR( "Failed to get setup input interface");
        return LDNDC_ERR_FAIL;
    }

    kernel_setup_id = setup_in->object_id();

    char *  echy3d_cfg = NULL;
    setup_in->section_data( &echy3d_cfg, ID());
    if ( !echy3d_cfg)
    {
        KLOGERROR( "Failed to retrieve EcHy3D input");
        return LDNDC_ERR_FAIL;
    }

    cbm::ksetup_t  ksetup = setup_parser( echy3d_cfg);
    CBM_FreeSectionData( echy3d_cfg);
    if ( !ksetup.parse_ok())
    {
        KLOGERROR( "Failed to parse EcHy3D input");
        return LDNDC_ERR_FAIL;
    }

    data_file = ksetup.query_string( "/file", data_file.c_str());
    data_file.format_expand();

    if ( !cbm::is_file( data_file.c_str()))
    {
        KLOGERROR( "opening data source failed  [source=",data_file,"]");
        return  LDNDC_ERR_READER_OPEN_FAILED;
    }

    std::ifstream ifs( data_file.c_str());
    std::string content( (std::istreambuf_iterator<char>(ifs)),
                         (std::istreambuf_iterator<char>()));
    data_file_content = content;

    m_sif = ldndc::SinkInterface( _args->iokcomm);
    lerr_t  rc_setflags = this->m_sif.set_metaflags( _args->cfg, this->ID(), RM_CLIENTSOURCE|RM_CLIENTID|RM_DATETIME);
    if ( rc_setflags)
    {
        KLOGERROR( "sink interface could not be established");
        return LDNDC_ERR_FAIL;
    }

    return LDNDC_ERR_OK;
}


lerr_t
ldndc::EcHy3D::EcHy3D_initialize_cells()
{
    cbm::jquery_t  ji( data_file_content.c_str());
    if ( ! ji.parse_ok())
    {
        LOGERROR("Deficient json format [", data_file,"]");
        return LDNDC_ERR_FAIL;
    }

    cbm::string_t setupname_query( "/setup_name");
    cbm::string_t setupname = ji.query_string( setupname_query.c_str(), "-");
    if (cbm::is_equal_i(setupname.c_str(), "-"))
    {
        LOGERROR("No valid setup provided to Echy3D!");
        return LDNDC_ERR_FAIL;
    }

    cbm::string_t sitename_query( "/site_name");
    cbm::string_t sitename = ji.query_string( sitename_query.c_str(), "-");
    if (cbm::is_equal_i(sitename.c_str(), "-"))
    {
        LOGERROR("No valid site provided to Echy3D!");
        return LDNDC_ERR_FAIL;
    }

    cbm::string_t setup_query( "/setup_ids");
    int const nd_setups( ji.query_size( setup_query.c_str()));

    for (int c = 0; c < nd_setups; ++c)
    {
        cbm::string_t query = setup_query;
        query << "/" << c;
        int const cell_setup_id( ji.query_int( query.c_str(), -1));

        //kernel setup not part of region
        if ( cell_setup_id == kernel_setup_id)
        {
            continue;
        }

        cbm::project_t *  lproj_setup = cbm::project_t::new_instance( 0);
        cbm::io_dcomm_t  io_dcomm_setup;

        input_class_setup_t  in_setup;
        lerr_t  rc_open = ldndc_open_source< input_class_setup_t >( &in_setup, cell_setup_id, lproj_setup, &io_dcomm_setup, setupname.c_str(), NULL);
        if ( rc_open)
        {
            LOGERROR("Setup could not be opened [", setupname,"]");
            return  LDNDC_ERR_FAIL;
        }

        cbm::source_descriptor_t  site_source;
        in_setup.block_consume( &site_source, "site");
        int cell_site_id( site_source.block_id);

        cbm::project_t *  lproj_site = cbm::project_t::new_instance( 0);
        cbm::io_dcomm_t  io_dcomm_site;

        input_class_site_t  in_site;
        rc_open = ldndc_open_source< input_class_site_t >( &in_site, cell_site_id, lproj_site, &io_dcomm_site, sitename.c_str(), NULL);
        if ( rc_open)
        {
            LOGERROR("Site could not be opened [", sitename,"]");
            return  LDNDC_ERR_FAIL;
        }

        double const x_pos( in_setup.x());
        double const y_pos( in_setup.y());
        double const z_pos( in_setup.z());
        double const area( in_setup.area());
        if ( !cbm::flt_greater_equal_zero( area))
        {
            LOGERROR("area needs to be greater/equal zero");
            return  LDNDC_ERR_FAIL;
        }
        
        EcHy3DCell new_cell( cell_setup_id, cell_site_id, x_pos, y_pos, z_pos, area);

        new_cell.set_soil_layers( &in_site);

        size_t const  n_neighbors( in_setup.get_number_of_neighbors());
        if ( n_neighbors > 0)
        {
            cbm::neighbor_t const *  cell_neighbors = in_setup.get_neighbors();
            for ( size_t  n = 0;  n < n_neighbors;  ++n)
            {
                EcHy3DNeighbor neighbor( cell_neighbors[n].desc, cell_neighbors[n].intersect);
                if ( cbm::flt_greater_zero( neighbor.intersect))
                {
                    new_cell.neighbors.push_back( neighbor);
                }
            }
        }

        cells.push_back( new_cell);
    }

    //set neighbor mapping
    for ( size_t c = 0; c < cells.size(); ++c)
    {
        EcHy3DCell *cell = &cells[c];
        for (size_t n=0; n < cell->neighbors.size(); ++n)
        {
            //neighbor knows its setup_id/block_id
            EcHy3DNeighbor *neighbor = &cell->neighbors[n];

            //find vector entry of cell matching neighbor
            for ( size_t m = 0; m < cells.size(); ++m)
            {
                if ( (int)neighbor->desc.block_id == cells[m].setup_id)
                {
                    neighbor->cell_map = m;
                    break;
                }

                //could not find vetor entry
                if ( m == cells.size()-1)
                {
                    LOGERROR("Neighbor of cell not existing!");
                    return LDNDC_ERR_FAIL;
                }
            }
        }
    }

    cbm::string_t boundary_query( "/boundary_ids");
    int const nd_boundaries( ji.query_size( boundary_query.c_str()));

    for (int c = 0; c < nd_boundaries; ++c)
    {
        cbm::string_t query = boundary_query;
        query << "/" << c <<"/setup_id";
        int const setup_id( ji.query_int( query.c_str(), -1));

        query = boundary_query;
        query << "/" << c <<"/potential";
        double const potential( ji.query_float( query.c_str(), invalid_flt));

        query = boundary_query;
        query << "/" << c <<"/length";
        double const length( ji.query_float( query.c_str(), invalid_flt));

        query = boundary_query;
        query << "/" << c <<"/elevation";
        double const elevation( ji.query_float( query.c_str(), invalid_flt));

        EcHy3DCell *cell = get_cell( setup_id);
        cell->river_output_potential = potential;
        cell->river_length = length;
        cell->river_elevation = elevation;
    }

    cbm::string_t outlet_query( "/outlet");
    int outlet_id( ji.query_int( outlet_query.c_str(), invalid_int));
    if ( cbm::is_valid( outlet_id))
    {
        EcHy3DCell *cell = get_cell( outlet_id);
        cell->outlet = true;
    }
    else
    {
        LOGERROR("No valid outlet defined!");
        return LDNDC_ERR_FAIL;
    }

    cbm::string_t sks_query( "/sks");
    int const nd_sks( ji.query_size( sks_query.c_str()));

    for (int c = 0; c < nd_sks; ++c)
    {
        cbm::string_t query = sks_query;
        query << "/" << c <<"/setup_id";
        int const setup_id( ji.query_int( query.c_str(), -1));
        
        query = sks_query;
        query << "/" << c <<"/sks";
        double const sks( ji.query_float( query.c_str(), invalid_flt));

        query = sks_query;
        query << "/" << c <<"/sks_bottom";
        double const sks_bottom( ji.query_float( query.c_str(), invalid_flt));

        EcHy3DCell *cell = get_cell( setup_id);
        cell->sks_gw = sks;
        cell->sks_bottom = sks_bottom;
    }

    cbm::string_t ecosystem_query( "/cellname");
    int const nd_ecosystem( ji.query_size( ecosystem_query.c_str()));

    for (int c = 0; c < nd_ecosystem; ++c)
    {
        cbm::string_t query = ecosystem_query;
        query << "/" << c <<"/setup_id";
        int const setup_id( ji.query_int( query.c_str(), -1));
        
        query = ecosystem_query;
        query << "/" << c <<"/name";
        cbm::string_t ecosystem( ji.query_string( query.c_str(), "none"));
        
        EcHy3DCell *cell = get_cell( setup_id);
        cell->ecosystem_type = ecosystem;
    }

    return LDNDC_ERR_OK;
}


lerr_t
ldndc::EcHy3D::EcHy3D_initialize_cell_communication( cbm::RunLevelArgs *_args)
{
    int rc = LDNDC_ERR_OK;

    for ( size_t c = 0; c < cells.size(); ++c)
    {
        EcHy3DCell *cell = &cells[c];

        rc = cell->AccumulatedPrecipitation.subscribe( "AccumulatedPrecipitation", "m", cell->setup_id, _args->iokcomm);
        if ( rc) { return LDNDC_ERR_FAIL; }

        rc = cell->AccumulatedThroughfall.subscribe( "AccumulatedThroughfall", "m", cell->setup_id, _args->iokcomm);
        if ( rc) { return LDNDC_ERR_FAIL; }

        rc = cell->AccumulatedTranspiration.subscribe( "AccumulatedTranspiration", "m", cell->nd_soil_layers, cell->setup_id, _args->iokcomm);
        if ( rc) { return LDNDC_ERR_FAIL; }

        rc = cell->AccumulatedInterceptionevaporation.subscribe( "AccumulatedInterceptionevaporation", "m", cell->setup_id, _args->iokcomm);
        if ( rc) { return LDNDC_ERR_FAIL; }

        rc = cell->AccumulatedSoilevaporation.subscribe( "AccumulatedSoilevaporation", "m", cell->setup_id, _args->iokcomm);
        if ( rc) { return LDNDC_ERR_FAIL; }

        rc = cell->AccumulatedSurfacewaterevaporation.subscribe( "AccumulatedSurfacewaterevaporation", "m", cell->setup_id, _args->iokcomm);
        if ( rc) { return LDNDC_ERR_FAIL; }

        rc = cell->AccumulatedRunoff.publish_and_subscribe( "AccumulatedRunoff", "m", cell->setup_id, _args->iokcomm);
        if ( rc) { return LDNDC_ERR_FAIL; }

        rc = cell->AccumulatedPercolation.publish_and_subscribe( "AccumulatedPercolation", "m", cell->setup_id, _args->iokcomm);
        if ( rc) { return LDNDC_ERR_FAIL; }

        rc = cell->AccumulatedInfiltration.publish_and_subscribe( "AccumulatedInfiltration", "m", cell->setup_id, _args->iokcomm);
        if ( rc) { return LDNDC_ERR_FAIL; }

        rc = cell->AccumulatedGroundwaterAccess.publish_and_subscribe( "AccumulatedGroundwaterAccess", "m", cell->setup_id, _args->iokcomm);
        if ( rc) { return LDNDC_ERR_FAIL; }

        rc = cell->AccumulatedGroundwaterLoss.publish_and_subscribe( "AccumulatedGroundwaterLoss", "m", cell->setup_id, _args->iokcomm);
        if ( rc) { return LDNDC_ERR_FAIL; }

        rc = cell->AccumulatedGroundwaterLoss.publish_and_subscribe( "AccumulatedGroundwaterLoss", "m", cell->setup_id, _args->iokcomm);
        if ( rc) { return LDNDC_ERR_FAIL; }
        
        rc = cell->SoilPorosity.subscribe( "SoilPorosity", "m3/m3", cell->nd_soil_layers, cell->setup_id, _args->iokcomm);
        if ( rc) { return LDNDC_ERR_FAIL; }

        rc = cell->SurfaceSnow.publish_and_subscribe( "SurfaceSnow", "kg", cell->setup_id, _args->iokcomm);
        if ( rc) { return LDNDC_ERR_FAIL; }

        rc = cell->SurfaceWater.publish_and_subscribe( "SurfaceWater", "m", cell->setup_id, _args->iokcomm);
        if ( rc) { return LDNDC_ERR_FAIL; }

        rc = cell->VolumetricWaterContent.publish_and_subscribe( "VolumetricWaterContent", "m3/m3", cell->nd_soil_layers, cell->setup_id, _args->iokcomm);
        if ( rc) { return LDNDC_ERR_FAIL; }

        rc = cell->VolumetricFieldCapacity.subscribe( "VolumetricFieldCapacity", "m3/m3", cell->nd_soil_layers, cell->setup_id, _args->iokcomm);
        if ( rc) { return LDNDC_ERR_FAIL; }

        rc = cell->MaximumWaterFilledPoreSpace.subscribe( "MaximumWaterFilledPoreSpace", "m3/m3", cell->nd_soil_layers, cell->setup_id, _args->iokcomm);
        if ( rc) { return LDNDC_ERR_FAIL; }

        rc = cell->SaturatedHydraulicConductivity.publish_and_subscribe( "SaturatedHydraulicConductivity", "cm/min", cell->nd_soil_layers, cell->setup_id, _args->iokcomm);
        if ( rc) { return LDNDC_ERR_FAIL; }

        rc = cell->IceContent.subscribe( "IceContent", "kg/m3", cell->nd_soil_layers, cell->setup_id, _args->iokcomm);
        if ( rc) { return LDNDC_ERR_FAIL; }

        rc = cell->Soildepth.subscribe( "SoilDepth", "m", cell->nd_soil_layers, cell->setup_id, _args->iokcomm);
        if ( rc) { return LDNDC_ERR_FAIL; }

        /* Soilchemistry */
        rc = cell->SoilSO4.publish_and_subscribe( "S_SO4_Soil", "kg/m2", cell->nd_soil_layers, cell->setup_id, _args->iokcomm);
        if ( rc) { return LDNDC_ERR_FAIL; }

        rc = cell->SoilNO3.publish_and_subscribe( "N_NO3_Soil", "kg/m2", cell->nd_soil_layers, cell->setup_id, _args->iokcomm);
        if ( rc) { return LDNDC_ERR_FAIL; }

        rc = cell->SoilNH4.publish_and_subscribe( "N_NH4_Soil", "kg/m2", cell->nd_soil_layers, cell->setup_id, _args->iokcomm);
        if ( rc) { return LDNDC_ERR_FAIL; }

        rc = cell->SoilDON.publish_and_subscribe( "N_DON_Soil", "kg/m2", cell->nd_soil_layers, cell->setup_id, _args->iokcomm);
        if ( rc) { return LDNDC_ERR_FAIL; }

        rc = cell->SoilDOC.publish_and_subscribe( "C_DOC_Soil", "kg/m2", cell->nd_soil_layers, cell->setup_id, _args->iokcomm);
        if ( rc) { return LDNDC_ERR_FAIL; }

        rc = cell->AccumulatedLeachingNO3.publish_and_subscribe( "AccumulatedLeaching_N_NO3", "kg/m2", cell->setup_id, _args->iokcomm);
        if ( rc) { return LDNDC_ERR_FAIL; }

        rc = cell->AccumulatedLeachingNH4.publish_and_subscribe( "AccumulatedLeaching_N_NH4", "kg/m2", cell->setup_id, _args->iokcomm);
        if ( rc) { return LDNDC_ERR_FAIL; }

        rc = cell->AccumulatedLeachingDON.publish_and_subscribe( "AccumulatedLeaching_N_DON", "kg/m2", cell->setup_id, _args->iokcomm);
        if ( rc) { return LDNDC_ERR_FAIL; }

        rc = cell->AccumulatedLeachingDOC.publish_and_subscribe( "AccumulatedLeaching_C_DOC", "kg/m2", cell->setup_id, _args->iokcomm);
        if ( rc) { return LDNDC_ERR_FAIL; }
    }

    return LDNDC_ERR_OK;
}



lerr_t
ldndc::EcHy3D::initialize( cbm::RunLevelArgs *_args)
{
    lerr_t rc = LDNDC_ERR_OK;

    rc = EcHy3D_initialize_cells();
    if ( rc) { return LDNDC_ERR_FAIL; }

    rc = EcHy3D_initialize_cell_communication( _args);
    if ( rc) { return LDNDC_ERR_FAIL; }

    rc = EcHy3D_initialize_sinks( _args);
    if ( rc) { return LDNDC_ERR_FAIL; }

    return rc;
}


