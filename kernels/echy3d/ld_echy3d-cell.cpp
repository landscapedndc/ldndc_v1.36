/*!
 * @author
 *    David Kraus
 */


#include  "echy3d/ld_echy3d.h"
#include  <input/setup/setup.h>
#include  <logging/cbm_logging.h>
#include  "app/ld_init.h"
#include  <io-dcomm.h>


ldndc::EcHy3DNeighbor::EcHy3DNeighbor( cbm::source_descriptor_t _desc, double _intersect):
                            desc( _desc),
                            intersect( _intersect),
                            water_flux( 0.0),
                            so4_flux( 0.0),
                            no3_flux( 0.0),
                            nh4_flux( 0.0),
                            don_flux( 0.0),
                            doc_flux( 0.0),
                            cell_map( -1)
{}


ldndc::EcHy3DCell::EcHy3DCell( int _setup_id,
                               int _site_id,
                               double _x,
                               double _y,
                               double _z,
                               double _area):
                            setup_id( _setup_id),
                            site_id( _site_id),
                            x( _x),
                            y( _y),
                            z( _z),
                            outlet( false),
                            river_output_potential( invalid_flt),
                            river_length( invalid_flt),
                            river_elevation( invalid_flt),
                            area( _area),
                            ecosystem_type( "none"),
                            sks_mean( 0.0),
                            sks_gw( invalid_dbl),
                            sks_bottom( invalid_dbl),
                            delta_groundwater_volume( 0.0),
                            delta_surfacewater_volume( 0.0),
                            delta_riverwater_volume( 0.0),
                            delta_so4( 0.0),
                            delta_no3( 0.0),
                            delta_no3_gain_ecosystem_specific( 0.0),
                            delta_no3_loss_ecosystem_specific( 0.0),
                            delta_nh4( 0.0),
                            delta_don( 0.0),
                            delta_doc( 0.0),
                            delta_groundwater_gain_ecosystem_specific( 0.0),
                            delta_groundwater_loss_ecosystem_specific( 0.0),
                            sl_gw( 0),
                            scale_out( 1.0),
                            accumulated_precipitation( 0.0),
                            accumulated_throughfall( 0.0),
                            accumulated_transpiration_sl( lvector_t< double >( 0)),
                            accumulated_interceptionevaporation( 0.0),
                            accumulated_soilevaporation( 0.0),
                            accumulated_surfacewaterevaporation( 0.0),
                            accumulated_percolation( 0.0),
                            accumulated_infiltration( 0.0),
                            accumulated_groundwater_access( 0.0),
                            accumulated_groundwater_loss( 0.0),
                            accumulated_surfacewater_access( 0.0),
                            accumulated_surfacewater_loss( 0.0),
                            accumulated_outlet( 0.0),
                            accumulated_outflow_soil( 0.0),
                            accumulated_outflow_surface( 0.0),
                            accumulated_outflow_so4( 0.0),
                            accumulated_outflow_no3( 0.0),
                            accumulated_outflow_nh4( 0.0),
                            accumulated_outflow_don( 0.0),
                            accumulated_outflow_doc( 0.0),
                            accumulated_precipitation_old( 0.0),
                            accumulated_throughfall_old( 0.0),
                            accumulated_transpiration_old( 0.0),
                            accumulated_interceptionevaporation_old( 0.0),
                            accumulated_soilevaporation_old( 0.0),
                            accumulated_surfacewaterevaporation_old( 0.0),
                            accumulated_infiltration_old( 0.0),
                            accumulated_groundwater_access_old( 0.0),
                            accumulated_groundwater_loss_old( 0.0),
                            accumulated_surfacewater_access_old( 0.0),
                            accumulated_surfacewater_loss_old( 0.0),
                            accumulated_outlet_old( 0.0),
                            accumulated_outflow_soil_old( 0.0),
                            accumulated_outflow_surface_old( 0.0),
                            accumulated_outflow_so4_old( 0.0),
                            accumulated_outflow_no3_old( 0.0),
                            accumulated_outflow_nh4_old( 0.0),
                            accumulated_outflow_don_old( 0.0),
                            accumulated_outflow_doc_old( 0.0),
                            porosity_sl( lvector_t< double >( 0)),
                            river_water( 0.0),
                            surface_snow( 0.0),
                            surface_water( 0.0),
                            wc_sl( lvector_t< double >( 0)),
                            sks_sl( lvector_t< double >( 0)),
                            wc_fc_sl( lvector_t< double >( 0)),
                            wfps_max_sl( lvector_t< double >( 0)),
                            ice_sl( lvector_t< double >( 0)),
                            height_sl( lvector_t< double >( 0)),
                            depth_sl( lvector_t< double >( 0)),
                            so4_sl( lvector_t< double >( 0)),
                            no3_sl( lvector_t< double >( 0)),
                            nh4_sl( lvector_t< double >( 0)),
                            don_sl( lvector_t< double >( 0)),
                            doc_sl( lvector_t< double >( 0))
{}


ldndc::EcHy3DCell::~EcHy3DCell()
{}


lerr_t
ldndc::EcHy3DCell::set_soil_layers( input_class_site_t *_site)
{
    nd_soil_layers = 0;
    for ( size_t  l = 0;  l < _site->strata_cnt();  ++l)
    {
        iclass_site_stratum_t  stratum;
        _site->stratum( l, &stratum);

        if ( stratum.split > 0)
        {
            nd_soil_layers += stratum.split;
        }
        else
        {
            LOGERROR("Invalid stratum split given: ", stratum.split);
            return LDNDC_ERR_FAIL;
        }
    }

    accumulated_transpiration_sl.resize_and_preserve( nd_soil_layers, 0.0);

    porosity_sl.resize_and_preserve( nd_soil_layers, 0.0);
    wc_sl.resize_and_preserve( nd_soil_layers, 0.0);
    sks_sl.resize_and_preserve( nd_soil_layers, 0.0);
    wc_fc_sl.resize_and_preserve( nd_soil_layers, 0.0);
    wfps_max_sl.resize_and_preserve( nd_soil_layers, 0.0);
    ice_sl.resize_and_preserve( nd_soil_layers, 0.0);
    height_sl.resize_and_preserve( nd_soil_layers, 0.0);
    depth_sl.resize_and_preserve( nd_soil_layers, 0.0);

    so4_sl.resize_and_preserve( nd_soil_layers, 0.0);
    no3_sl.resize_and_preserve( nd_soil_layers, 0.0);
    nh4_sl.resize_and_preserve( nd_soil_layers, 0.0);
    don_sl.resize_and_preserve( nd_soil_layers, 0.0);
    doc_sl.resize_and_preserve( nd_soil_layers, 0.0);

    return  LDNDC_ERR_OK;
}


lerr_t
ldndc::EcHy3DCell::reset()
{
    delta_groundwater_volume = 0.0;
    delta_surfacewater_volume = 0.0;
    delta_riverwater_volume = 0.0;

    delta_so4 = 0.0;
    delta_no3 = 0.0;
    delta_nh4 = 0.0;
    delta_don = 0.0;
    delta_doc = 0.0;

    sl_gw = nd_soil_layers-1;
    scale_out = 1.0;
    available_so4 = 0.0;
    available_no3 = 0.0;
    available_nh4 = 0.0;
    available_don = 0.0;
    available_doc = 0.0;

    for (size_t n=0; n < neighbors.size(); ++n)
    {
        neighbors[n].water_flux = 0.0;
        neighbors[n].so4_flux = 0.0;
        neighbors[n].no3_flux = 0.0;
        neighbors[n].nh4_flux = 0.0;
        neighbors[n].don_flux = 0.0;
        neighbors[n].doc_flux = 0.0;
    }

    return  LDNDC_ERR_OK;
}


double
ldndc::EcHy3DCell::get_river_potential()
{
    if ( cbm::flt_greater_zero( river_length))
    {
        double river_height( river_water * area / river_length);
        return river_elevation + river_height;
    }
    else
    {
        return 0.0;
    }
}


double
ldndc::EcHy3DCell::get_soil_water()
{
    double water_volume( 0.0);
    for (int sl = 0; sl < nd_soil_layers; sl++)
    {
        water_volume += wc_sl[sl] * height_sl[sl];
    }

    return water_volume;
}


double
EcHy3DCell::get_ground_water_available()
{
    double water_volume( 0.0);
    for ( int sl = nd_soil_layers-1; sl >= 0; sl--)
    {
        double const wfps( wc_sl[sl] / porosity_sl[sl]);
        if ( cbm::flt_greater_equal( wfps, ldndc::EcHy3D::WFPS_THRESHOLD))
        {
            water_volume += cbm::bound_min( 0.0, wc_sl[sl] - wc_fc_sl[sl]) * height_sl[sl];
        }
        else
        {
            return water_volume;
        }
    }

    return water_volume;
}


double
ldndc::EcHy3DCell::get_ground_water()
{
    double water_volume( 0.0);
    for ( int sl = nd_soil_layers-1; sl >= 0; sl--)
    {
        double const wfps( wc_sl[sl] / porosity_sl[sl]);
        if ( cbm::flt_greater_equal( wfps, ldndc::EcHy3D::WFPS_THRESHOLD))
        {
            water_volume += wc_sl[sl] * height_sl[sl];
        }
        else
        {
            return water_volume;
        }
    }

    return water_volume;
}


double
ldndc::EcHy3DCell::get_total_water()
{
    return river_water + surface_water + get_soil_water();
}


lerr_t
ldndc::EcHy3DCell::step_init( cbm::RunLevelArgs *_args)
{
    reset();

    /* pressure head and groundwater soil layer initialized with last soil layer */
    pressure_head = z - depth_sl[nd_soil_layers-1]; // [m]
    sl_gw = nd_soil_layers-1;

    /* saturated hydraulic conductivity averaged across groundwater layers*/
    double sks_height( 0.0);
    double sks_loc( 0.0);
    for ( int sl = nd_soil_layers-1; sl >= 0; sl--)
    {
        double const wfps( wc_sl[sl] / porosity_sl[sl]);
        if ( cbm::flt_greater_equal( wfps, ldndc::EcHy3D::WFPS_THRESHOLD))
        {
            //pressure head
            pressure_head = (sl == 0) ? z + surface_water : z - depth_sl[sl] + (wfps - ldndc::EcHy3D::WFPS_THRESHOLD) / (1.0 - ldndc::EcHy3D::WFPS_THRESHOLD) * height_sl[sl];

            //nutrients
            available_so4 += so4_sl[sl] * area;
            available_no3 += no3_sl[sl] * area;
            available_nh4 += nh4_sl[sl] * area;
            available_don += don_sl[sl] * area;
            available_doc += doc_sl[sl] * area;

            sl_gw = sl;

            //saturated hydraulic conductivity
            sks_height += height_sl[sl];
            sks_loc += sks_sl[sl] * height_sl[sl];
        }
        else
        {
            break;
        }
    }

    double const sks_scale( cbm::M_IN_CM * cbm::MIN_IN_DAY / _args->clk->time_resolution());

    //always use groundwater sks if given
    if ( cbm::is_valid( sks_gw))
    {
        sks_mean = sks_gw * sks_scale;
    }
    else if ( cbm::flt_greater_zero( sks_height))
    {
        sks_mean = sks_loc / sks_height * sks_scale;
    }
    else
    {
        sks_mean = sks_sl[nd_soil_layers-1] * sks_scale;
    }

    return  LDNDC_ERR_OK;
}


lerr_t
ldndc::EcHy3DCell::EcHy3DCell_update_nutrient_transport( double &_delta_val,            //[kg]
                                                         double &_accumulated_val,      //[kg:m-2]
                                                         lvector_t< double > &_val_sl,  //[kg:m-2]
                                                         cbm::string_t _name)
{
    if ( cbm::is_equal_i( _name.c_str(), "no3"))
    {
        delta_no3_loss_ecosystem_specific -= accumulated_outflow_no3 * area;
    }

    if ( cbm::flt_greater_zero( _delta_val))
    {
        double height_sum( 0.0);
        for (int sl = nd_soil_layers-1; sl >= sl_gw; sl--)
        {
            height_sum += height_sl[sl];
        }

        if ( cbm::flt_greater_zero( height_sum))
        {
            for (int sl = nd_soil_layers-1; sl >= sl_gw; sl--)
            {
                double const scale( height_sl[sl] / height_sum);
                _val_sl[sl] += scale * _delta_val / area;
            }
        }
        else
        {
            _val_sl[nd_soil_layers-1] += _delta_val / area;
        }
    }
    else if ( cbm::flt_less( _delta_val, 0.0))
    {
        double val_sum( 0.0);
        for (int sl = nd_soil_layers-1; sl >= sl_gw; sl--)
        {
            val_sum += _val_sl[sl];
        }

        if ( cbm::flt_greater( val_sum * area, -_delta_val))
        {
            for (int sl = nd_soil_layers-1; sl >= sl_gw; sl--)
            {
                double const scale( _val_sl[sl] / val_sum);
                _val_sl[sl] += scale * _delta_val / area;
            }
        }
        else if ( cbm::flt_equal( val_sum * area, -_delta_val))
        {
            for (int sl = nd_soil_layers-1; sl >= sl_gw; sl--)
            {
                _val_sl[sl] = 0.0;
            }
        }
        else
        {
            LOGERROR("Calculated transport of ", _name, " out of cell greater than ", _name, " content! ", val_sum,"  delta: ",_delta_val);
            return LDNDC_ERR_FAIL;
        }
    }
    _delta_val = 0.0;

    if ( cbm::is_valid( river_output_potential))
    {
        for (int sl = nd_soil_layers-1; sl >= sl_gw; sl--)
        {
            double const val_old( _val_sl[sl]);
            _val_sl[sl] *= scale_out;
            _accumulated_val += val_old - _val_sl[sl];
        }
    }

    if ( cbm::is_equal_i( _name.c_str(), "no3"))
    {
        delta_no3_loss_ecosystem_specific += accumulated_outflow_no3 * area;
    }

    return  LDNDC_ERR_OK;
}


lerr_t
ldndc::EcHy3DCell::EcHy3DCell_update_soil_water_content( cbm::RunLevelArgs *)
{
    //net inflow
    if ( cbm::flt_greater_zero( delta_groundwater_volume))
    {
        //fill water layers from bottom to top
        for (int sl = nd_soil_layers-1; sl >= 0; sl--)
        {
            if ( cbm::flt_greater_zero( delta_groundwater_volume))
            {
                double const wfps( (ice_sl[sl] + wc_sl[sl]) / porosity_sl[sl]);
                if ( cbm::flt_less( wfps, wfps_max_sl[sl]))
                {
                    double const water_volume_old( wc_sl[sl] * height_sl[sl] * area);
                    double const space( cbm::bound_min( 0.0, wfps_max_sl[sl] * porosity_sl[sl] - wc_sl[sl] - ice_sl[sl]) * height_sl[sl] * area);
                    double const fill( cbm::bound_max( delta_groundwater_volume, space));
                    delta_groundwater_volume -= fill;

                    wc_sl[sl] = (water_volume_old + fill) / (height_sl[sl] * area);

                    if( cbm::flt_less( wc_sl[sl], 0.0))
                    {
                        LOGERROR("Negative water content after groundwater inflow in soil layer: ", sl," Illegal situation!");
                        return LDNDC_ERR_FAIL;
                    }
                    if( cbm::flt_greater( wc_sl[sl], porosity_sl[sl]))
                    {
                        LOGERROR("Water content greater porosity after groundwater inflow in soil layer: ", sl," Illegal situation!");
                        return LDNDC_ERR_FAIL;
                    }
                }
            }
            else
            {
                break;
            }
        }
        
        //add to surface if soil if fully saturated
        if ( cbm::flt_greater_zero( delta_groundwater_volume))
        {
            surface_water += delta_groundwater_volume / area;
            delta_groundwater_volume = 0.0;
        }
    }
    
    //net outflow
    else if ( cbm::flt_less( delta_groundwater_volume, 0.0))
    {
        double groundwater_volume_old( get_ground_water() * area);

        //note: delta_groundwater_volume < 0.0
        double const scale( cbm::bound_min( 0.01, 1.0 + delta_groundwater_volume / groundwater_volume_old));

        if (delta_groundwater_volume + groundwater_volume_old < 0.0)
        {
            LOGINFO("outflow greater content!  ", delta_groundwater_volume,"  ",groundwater_volume_old);
            return LDNDC_ERR_FAIL;
        }
        delta_groundwater_volume = 0.0;

        for (int sl = nd_soil_layers-1; sl >= 0; sl--)
        {
            if ( cbm::flt_greater_equal( wc_sl[sl] / porosity_sl[sl], ldndc::EcHy3D::WFPS_THRESHOLD))
            {
                wc_sl[sl] *= scale;

                if( cbm::flt_less( wc_sl[sl], 0.0))
                {
                    LOGERROR("Negative water content after groundwater outflow in soil layer: ", sl," Illegal situation!");
                    return LDNDC_ERR_FAIL;
                }

                if( cbm::flt_greater( wc_sl[sl], porosity_sl[sl]))
                {
                    LOGERROR("Water content greater porosity after groundwater outflow in soil layer: ", sl," Illegal situation!");
                    return LDNDC_ERR_FAIL;
                }
            }
            else
            {
                break;
            }
        }
    }

    return  LDNDC_ERR_OK;
}


lerr_t
ldndc::EcHy3DCell::EcHy3DCell_update_surface_water_content( cbm::RunLevelArgs *)
{
    surface_water += delta_surfacewater_volume / area;
    delta_surfacewater_volume = 0.0;

    if( cbm::flt_less( surface_water, -1.0e-9))
    {
        LOGERROR("Negative surface water after update: ",surface_water," [m]. Illegal situation!");
        surface_water = 0.0;
        return LDNDC_ERR_FAIL;
    }
    else if( cbm::flt_less( surface_water, 0.0))
    {
        surface_water = 0.0;
    }

    return  LDNDC_ERR_OK;
}


lerr_t
ldndc::EcHy3DCell::EcHy3DCell_update_river_water_content( cbm::RunLevelArgs *)
{
    river_water += delta_riverwater_volume / area;
    delta_riverwater_volume = 0.0;

    if( cbm::flt_less( river_water, -1.0e-9))
    {
        LOGERROR("Negative river water after update: ",river_water," [m]. Illegal situation!");
        river_water = 0.0;
        return LDNDC_ERR_FAIL;
    }
    else if( cbm::flt_less( river_water, 0.0))
    {
        river_water = 0.0;
    }

    return  LDNDC_ERR_OK;
}


lerr_t
ldndc::EcHy3DCell::EcHy3DCell_boundary_flows( cbm::RunLevelArgs *)
{
    delta_groundwater_loss_ecosystem_specific -= accumulated_outflow_surface + accumulated_outflow_soil;

    //SURFACE -> RIVER
    if ( cbm::is_valid( river_output_potential))
    {
        double runoff( get_runoff());
        surface_water -= runoff;
        river_water += runoff;

        accumulated_outflow_surface += runoff;
        accumulated_surfacewater_loss += runoff;
    }

    //SOIL -> RIVER
    if ( cbm::is_valid( river_output_potential))
    {
        double const pressure_head_river_output( z + river_output_potential);
        if ( cbm::flt_greater( pressure_head, pressure_head_river_output))
        {
            //rough estimate
            double const water_volume_old( get_ground_water() * area);
            double const water_volume_available( get_ground_water_available() * area);

            double const interface( (pressure_head - pressure_head_river_output) * river_length);
            double const outflow( cbm::bound_max( sks_mean * interface, water_volume_available));

            scale_out = 1.0 - cbm::bound_max( outflow / water_volume_old, 0.99);

            for (int sl = nd_soil_layers-1; sl >= 0; sl--)
            {
                double const wfps( wc_sl[sl] / porosity_sl[sl]);
                if ( cbm::flt_greater_equal( wfps, ldndc::EcHy3D::WFPS_THRESHOLD))
                {
                    double const delta_soil_water( (1.0 - scale_out) * wc_sl[sl] * height_sl[sl]);

                    wc_sl[sl] -= delta_soil_water /  height_sl[sl];
                    river_water += delta_soil_water;

                    accumulated_outflow_soil += delta_soil_water;
                    accumulated_groundwater_loss += delta_soil_water;

                    if( cbm::flt_less( wc_sl[sl], 0.0))
                    {
                        LOGERROR("Negative water content after boundary outflow in soil layer: ", sl," Illegal situation!");
                        return LDNDC_ERR_FAIL;
                    }
                    if( cbm::flt_greater( wc_sl[sl], porosity_sl[sl]))
                    {
                        LOGERROR("Water content greater porosity after boundary outflow in soil layer: ", sl," Illegal situation!");
                        return LDNDC_ERR_FAIL;
                    }
                }
                else
                {
                    break;
                }
            }
        }
    }

    //RIVER -> OUTPUT
    if ( cbm::flt_greater_zero( river_water))
    {
        accumulated_outlet += river_water;
        river_water = 0.0;
    }

    delta_groundwater_loss_ecosystem_specific += accumulated_outflow_surface + accumulated_outflow_soil;

    return  LDNDC_ERR_OK;
}


double EcHy3DCell::max_depth()
{
    return z - depth_sl[nd_soil_layers-1];
}


double
EcHy3DCell::get_runoff()
{
    double const surface_water_in_mm( surface_water * cbm::MM_IN_M);
    double const curvenumber( 80);

    //retention parameter [mm]
    double const retention_factor( 25.4 * (1000.0 / curvenumber - 10));

    //runoff [mm]
    double const q_surf( cbm::sqr( surface_water_in_mm) / (surface_water_in_mm + retention_factor));

    return cbm::bound_max( q_surf * cbm::M_IN_MM, 0.99 * surface_water);
}



double
EcHy3DCell::get_groundwater_share( lvector_t< double > &_source)
{
    double out( 0.0);
    for (int sl = nd_soil_layers-1; sl >= 0; sl--)
    {
        double const wfps( wc_sl[sl] / porosity_sl[sl]);
        if ( cbm::flt_greater_equal( wfps, ldndc::EcHy3D::WFPS_THRESHOLD))
        {
            out += _source[sl];
        }
        else
        {
            break;
        }
    }
    
    return out;
}
