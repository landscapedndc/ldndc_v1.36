#!/bin/bash

userconf="$1"

base_path="`dirname $0`"
figbasedir="${base_path}"
figobasedir="${figbasedir}"
figlist="${base_path}/figures.list"

if [ -r "$base_path/figures.conf" ]
then
	. "$base_path/figures.conf"
else
	figdevbin="fig2dev"
	egrepbin="egrep"
fi

if [ -r "$userconf" ]
then
	. "$userconf"
fi

if [ ! -f "$figlist" -o ! -r "$figlist" ]
then
        echo  "figure list file not found, not a file or unreadable  [file=$figlist]" 1>&2
        exit  1
fi


$egrepbin -v '^\ *($|#)' $figlist | while read  _fname _obase _mag _rest
do
        #echo  "fname=$figbasedir/$_fname  obase=$figbasedir/$_obase  mag=$_mag"
        rm -vf $figbasedir/$_obase.pdf $figbasedir/$_obase.pdf_t
done

exit  0

