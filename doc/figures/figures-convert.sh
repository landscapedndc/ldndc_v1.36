#!/bin/bash

userconf="$1"

base_path="`dirname $0`"
figbasedir="${base_path}"
figobasedir="${figbasedir}"
figlist="${base_path}/figures.list"

if [ -r "$base_path/figures.conf" ]
then
    . "$base_path/figures.conf"
else
    figdevbin="fig2dev"
    egrepbin="egrep"

    forceoverwrite=1
fi

if [ ! -x "/`which $figdevbin`" -o ! -x "/`which $egrepbin`" ]
then
    echo "missing tools 'fig2dev' or 'egrep'" 1>&2
    exit 1
fi

if [ -r "$userconf" ]
then
    . "$userconf"
fi

if [ ! -f "$figlist" -o ! -r "$figlist" ]
then
    echo  "figure list file not found, not a file or unreadable  [file=$figlist]" 1>&2
    exit  1
fi


$egrepbin -v '^\ *($|#)' $figlist | while read  _fname _obase _mag _rest
do
    if [ ! -e "$figobasedir/$_obase.pdf" -o ! -e "$figobasedir/$_obase.pdf_t" -o $forceoverwrite -eq 1 ]
    then
        echo  "fname=$figbasedir/$_fname  obase=$figobasedir/$_obase  mag=$_mag"
        $figdevbin -L pdftex_t -m $_mag -p "$figobasedir/$_obase.pdf" "$figbasedir/$_fname" "$figobasedir/$_obase.pdf_t"
        $figdevbin -L pdftex   -m $_mag "$figbasedir/$_fname" "$figobasedir/$_obase.pdf"
    else
        echo "fname=$figbasedir/$_obase  exists. skipping."
    fi
done

exit  0

