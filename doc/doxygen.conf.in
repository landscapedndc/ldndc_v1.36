# Doxyfile 1.5.6

#---------------------------------------------------------------------------
# Project related configuration options
#---------------------------------------------------------------------------
DOXYFILE_ENCODING      = UTF-8
PROJECT_NAME           = LandscapeDNDC
PROJECT_LOGO           = @CMAKE_SOURCE_DIR@/doc/logos/ldndclogo-tiny.png
PROJECT_NUMBER         = @CMAKE_LDNDC_PACKAGE_VERSION@
OUTPUT_DIRECTORY       = @CMAKE_BINARY_DIR@/doc/
CREATE_SUBDIRS         = NO
OUTPUT_LANGUAGE        = English
BRIEF_MEMBER_DESC      = YES
REPEAT_BRIEF           = YES
ABBREVIATE_BRIEF       = "The $name class" \
                         "The $name widget" \
                         "The $name file" \
                         is \
                         provides \
                         specifies \
                         contains \
                         represents \
                         a \
                         an \
                         the
ALWAYS_DETAILED_SEC    = NO
INLINE_INHERITED_MEMB  = NO
FULL_PATH_NAMES        = YES
STRIP_FROM_PATH        = @CMAKE_SOURCE_DIR@
STRIP_FROM_INC_PATH    = @CMAKE_SOURCE_DIR@
SHORT_NAMES            = NO
JAVADOC_AUTOBRIEF      = NO
QT_AUTOBRIEF           = NO
MULTILINE_CPP_IS_BRIEF = NO
#DETAILS_AT_TOP         = NO
INHERIT_DOCS           = YES
SEPARATE_MEMBER_PAGES  = NO
TAB_SIZE               = 8
ALIASES                = 
OPTIMIZE_OUTPUT_FOR_C  = NO
OPTIMIZE_OUTPUT_JAVA   = NO
OPTIMIZE_FOR_FORTRAN   = NO
OPTIMIZE_OUTPUT_VHDL   = NO
BUILTIN_STL_SUPPORT    = NO
CPP_CLI_SUPPORT        = NO
SIP_SUPPORT            = NO
IDL_PROPERTY_SUPPORT   = YES
DISTRIBUTE_GROUP_DOC   = NO
SUBGROUPING            = YES
TYPEDEF_HIDES_STRUCT   = NO
#---------------------------------------------------------------------------
# Build related configuration options
#---------------------------------------------------------------------------
CITE_BIB_FILES         = @CMAKE_SOURCE_DIR@/doc/usersguide/references.bib \
                         @CMAKE_SOURCE_DIR@/doc/models-description/references.bib
EXTRACT_ALL            = NO
EXTRACT_PACKAGE        = YES  
EXTRACT_PRIVATE        = YES
EXTRACT_STATIC         = YES
EXTRACT_LOCAL_CLASSES  = YES
EXTRACT_LOCAL_METHODS  = YES
EXTRACT_ANON_NSPACES   = YES
EXTRA_PACKAGES         = amsmath xr amsfonts
HIDE_UNDOC_MEMBERS     = YES
HIDE_UNDOC_CLASSES     = YES
HIDE_FRIEND_COMPOUNDS  = NO
HIDE_IN_BODY_DOCS      = NO
INTERNAL_DOCS          = NO
CASE_SENSE_NAMES       = YES
HIDE_SCOPE_NAMES       = NO
SHOW_INCLUDE_FILES     = NO
INLINE_INFO            = YES
PREDEFINED             = DOXYGEN_SHOULD_SKIP_THIS
SORT_MEMBER_DOCS       = YES
SORT_BRIEF_DOCS        = NO
SORT_GROUP_NAMES       = NO
SORT_BY_SCOPE_NAME     = NO
GENERATE_TODOLIST      = NO
GENERATE_TESTLIST      = NO
GENERATE_BUGLIST       = NO
GENERATE_DEPRECATEDLIST= NO
ENABLED_SECTIONS       = 
MAX_INITIALIZER_LINES  = 30
SHOW_USED_FILES        = NO
SHOW_FILES             = NO
SHOW_NAMESPACES        = NO
FILE_VERSION_FILTER    = 
#---------------------------------------------------------------------------
# configuration options related to warning and progress messages
#---------------------------------------------------------------------------
QUIET                  = NO
WARNINGS               = YES
WARN_IF_UNDOCUMENTED   = NO
WARN_IF_DOC_ERROR      = YES
WARN_NO_PARAMDOC       = NO
WARN_FORMAT            = "$file:$line: $text"
WARN_LOGFILE           = @CMAKE_BINARY_DIR@/doxygen-err.log
#---------------------------------------------------------------------------
# configuration options related to the input files
#---------------------------------------------------------------------------
INPUT                  = @CMAKE_SOURCE_DIR@/doc/doxygen/mainpage.txt \
                         @CMAKE_SOURCE_DIR@/doc/doxygen/ \
                         @CMAKE_SOURCE_DIR@/resources/ \
                         @CMAKE_SOURCE_DIR@/models/physiology/doc \
                         @CMAKE_SOURCE_DIR@/models/doc/ \
                         @CMAKE_SOURCE_DIR@/models/output/doc/ \
                         @CMAKE_SOURCE_DIR@/models/legacy/ \
                         @CMAKE_SOURCE_DIR@/models/eventhandler/doc \
                         @CMAKE_SOURCE_DIR@/models/eventhandler/cut \
                         @CMAKE_SOURCE_DIR@/models/eventhandler/fertilize \
                         @CMAKE_SOURCE_DIR@/models/eventhandler/flood \
                         @CMAKE_SOURCE_DIR@/models/eventhandler/graze \
                         @CMAKE_SOURCE_DIR@/models/eventhandler/irrigate \
                         @CMAKE_SOURCE_DIR@/models/eventhandler/till \
                         @CMAKE_SOURCE_DIR@/models/microclimate/doc \
                         @CMAKE_SOURCE_DIR@/models/microclimate/canopyecm/canopyecm.cpp \
                         @CMAKE_SOURCE_DIR@/models/microclimate/canopyecm/canopyecm-temperature.cpp \
                         @CMAKE_SOURCE_DIR@/models/microclimate/ \
                         @CMAKE_SOURCE_DIR@/models/physiology/doc \
                         @CMAKE_SOURCE_DIR@/models/physiology/photofarquhar.cpp \
                         @CMAKE_SOURCE_DIR@/models/physiology/photofarquhar \
                         @CMAKE_SOURCE_DIR@/models/physiology/ \
                         @CMAKE_SOURCE_DIR@/models/soilchemistry/doc \
                         @CMAKE_SOURCE_DIR@/models/soilchemistry/metrx/doc/ \
                         @CMAKE_SOURCE_DIR@/models/soilchemistry/metrx/soilchemistry-metrx-init.cpp \
                         @CMAKE_SOURCE_DIR@/models/soilchemistry/metrx/soilchemistry-metrx-fragmentation.cpp \
                         @CMAKE_SOURCE_DIR@/models/soilchemistry/metrx/soilchemistry-metrx-decomposition.cpp \
                         @CMAKE_SOURCE_DIR@/models/soilchemistry/metrx/soilchemistry-metrx-microbial-dynamics.cpp \
                         @CMAKE_SOURCE_DIR@/models/soilchemistry/metrx/soilchemistry-metrx-nitrification.cpp \
                         @CMAKE_SOURCE_DIR@/models/soilchemistry/metrx/soilchemistry-metrx-denitrification.cpp \
                         @CMAKE_SOURCE_DIR@/models/soilchemistry/metrx/soilchemistry-metrx-fermentation.cpp \
                         @CMAKE_SOURCE_DIR@/models/soilchemistry/metrx/soilchemistry-metrx-iron.cpp \
                         @CMAKE_SOURCE_DIR@/models/soilchemistry/metrx/soilchemistry-metrx-methane.cpp \
                         @CMAKE_SOURCE_DIR@/models/soilchemistry/metrx/soilchemistry-metrx-algae.cpp \
                         @CMAKE_SOURCE_DIR@/models/soilchemistry/metrx/soilchemistry-metrx-transport.cpp \
                         @CMAKE_SOURCE_DIR@/models/soilchemistry/metrx/output-soilchemistry-metrx-daily.cpp \
                         @CMAKE_SOURCE_DIR@/models/soilchemistry/metrx/output-soilchemistry-metrx-subdaily.cpp \
                         @CMAKE_SOURCE_DIR@/models/soilchemistry/ \
                         @CMAKE_SOURCE_DIR@/models/watercycle/doc \
                         @CMAKE_SOURCE_DIR@/models/watercycle/dndc \
                         @CMAKE_SOURCE_DIR@/models/watercycle/echy/echy.cpp \
                         @CMAKE_SOURCE_DIR@/models/watercycle/echy/echy-initialize.cpp \
                         @CMAKE_SOURCE_DIR@/models/watercycle/ \
                         @CMAKE_SOURCE_DIR@/models/output/ \
                         @CMAKE_SOURCE_DIR@/kernels/farmsystem/ld_farmsystem.cpp \
                         @CMAKE_SOURCE_DIR@/kernels/farmsystem/ld_fieldsite.cpp \
                         @CMAKE_SOURCE_DIR@/kernels/farmsystem/ld_vegetation_period.cpp \
                         @CMAKE_SOURCE_DIR@/kernels/farmsystem/ld_farmsystem_reader.cpp \
                         @CMAKE_SOURCE_DIR@/kernels/farmsystem/ \
                         @CMAKE_SOURCE_DIR@/kernels/echy3d/ \
                         @CMAKE_SOURCE_DIR@/kernels/oryza2000/ \
                         @CMAKE_BINARY_DIR@/doc/ 
INPUT_ENCODING         = UTF-8
FILE_PATTERNS          = *.c \
                         *.cc \
                         *.txt \
                         *.cxx \
                         *.cpp \
                         *.c++ \
                         *.d \
                         *.java \
                         *.ii \
                         *.ixx \
                         *.ipp \
                         *.i++ \
                         *.inl \
                         *.h \
                         *.hh \
                         *.hxx \
                         *.hpp \
                         *.h++ \
                         *.idl \
                         *.odl \
                         *.cs \
                         *.php \
                         *.php3 \
                         *.inc \
                         *.m \
                         *.mm \
                         *.dox \
                         *.py \
                         *.f90 \
                         *.f \
                         *.vhd \
                         *.vhdl \
                         *.C \
                         *.CC \
                         *.C++ \
                         *.II \
                         *.I++ \
                         *.H \
                         *.HH \
                         *.H++ \
                         *.CS \
                         *.PHP \
                         *.PHP3 \
                         *.M \
                         *.MM \
                         *.PY \
                         *.F90 \
                         *.F \
                         *.VHD \
                         *.VHDL \
                         *.h
RECURSIVE              = YES
EXCLUDE                = 
EXCLUDE_SYMLINKS       = NO
EXCLUDE_PATTERNS       = g_*.h g_*.cpp 
EXCLUDE_SYMBOLS        = 
EXAMPLE_PATH           = 
EXAMPLE_PATTERNS       = *
EXAMPLE_RECURSIVE      = NO
IMAGE_PATH             = @CMAKE_SOURCE_DIR@/doc/logos \ 
                         @CMAKE_SOURCE_DIR@/doc/doxygen/mainpage.txt \
                         @CMAKE_SOURCE_DIR@/models/doc \
                         @CMAKE_SOURCE_DIR@/models/physiology/doc \
                         @CMAKE_SOURCE_DIR@/models/physiology/plamox \
                         @CMAKE_SOURCE_DIR@/models/soilchemistry/doc \
                         @CMAKE_SOURCE_DIR@/models/soilchemistry/metrx/soilchemistry-metrx-init.cpp \
                         @CMAKE_SOURCE_DIR@/models/soilchemistry/metrx/ \
                         @CMAKE_SOURCE_DIR@/models/soilchemistry/dndc2/ \
                         @CMAKE_SOURCE_DIR@/models/watercycle/doc \
                         @CMAKE_SOURCE_DIR@/models/watercycle/dndc \
                         @CMAKE_SOURCE_DIR@/models/watercycle/echy \
                         @CMAKE_SOURCE_DIR@/kernels/farmer/doc/figures
INPUT_FILTER           = 
FILTER_PATTERNS        = 
FILTER_SOURCE_FILES    = NO
#---------------------------------------------------------------------------
# configuration options related to source browsing
#---------------------------------------------------------------------------
SOURCE_BROWSER         = NO
INLINE_SOURCES         = YES
STRIP_CODE_COMMENTS    = YES
REFERENCED_BY_RELATION = YES
REFERENCES_RELATION    = YES
REFERENCES_LINK_SOURCE = YES
USE_HTAGS              = NO
VERBATIM_HEADERS       = YES
#---------------------------------------------------------------------------
# configuration options related to the alphabetical class index
#---------------------------------------------------------------------------
ALPHABETICAL_INDEX     = YES
COLS_IN_ALPHA_INDEX    = 5
IGNORE_PREFIX          = 
#---------------------------------------------------------------------------
# configuration options related to the HTML output
#---------------------------------------------------------------------------
GENERATE_HTML          = YES
HTML_OUTPUT            = @CMAKE_BINARY_DIR@/doc/html
HTML_FILE_EXTENSION    = .html
HTML_HEADER            = 
HTML_FOOTER            = 
HTML_STYLESHEET        = 
HTML_EXTRA_STYLESHEET  = @CMAKE_BINARY_DIR@/doc/doxygen-extra-stylesheet.css
GENERATE_HTMLHELP      = NO
GENERATE_DOCSET        = NO
DOCSET_FEEDNAME        = "Doxygen generated docs"
DOCSET_BUNDLE_ID       = org.doxygen.Project
HTML_DYNAMIC_SECTIONS  = NO
CHM_FILE               = 
HHC_LOCATION           = 
GENERATE_CHI           = NO
CHM_INDEX_ENCODING     = 
BINARY_TOC             = NO
TOC_EXPAND             = NO
DISABLE_INDEX          = NO
ENUM_VALUES_PER_LINE   = 1
GENERATE_TREEVIEW      = YES
TREEVIEW_WIDTH         = 250
FORMULA_FONTSIZE       = 10
#---------------------------------------------------------------------------
# configuration options related to the LaTeX output
#---------------------------------------------------------------------------
GENERATE_LATEX         = YES
LATEX_OUTPUT           = @CMAKE_BINARY_DIR@/doc/latex
LATEX_CMD_NAME         = latex
MAKEINDEX_CMD_NAME     = makeindex
COMPACT_LATEX          = YES
PAPER_TYPE             = a4
EXTRA_PACKAGES         = 
LATEX_HEADER           = 
PDF_HYPERLINKS         = YES
USE_PDFLATEX           = YES
USE_MATHJAX            = YES
LATEX_BATCHMODE        = YES
LATEX_HIDE_INDICES     = YES
LATEX_BIB_STYLE        = plain
LATEX_SOURCE_CODE      = NO

#---------------------------------------------------------------------------
# configuration options related to the RTF output
#---------------------------------------------------------------------------
GENERATE_RTF           = NO
RTF_OUTPUT             = refman/rtf
COMPACT_RTF            = NO
RTF_HYPERLINKS         = NO
RTF_STYLESHEET_FILE    = 
RTF_EXTENSIONS_FILE    = 
#---------------------------------------------------------------------------
# configuration options related to the man page output
#---------------------------------------------------------------------------
GENERATE_MAN           = NO
MAN_OUTPUT             = refman/man
MAN_EXTENSION          = .3
MAN_LINKS              = NO
#---------------------------------------------------------------------------
# configuration options related to the XML output
#---------------------------------------------------------------------------
GENERATE_XML           = NO
XML_OUTPUT             = refman/xml
XML_PROGRAMLISTING     = YES
#---------------------------------------------------------------------------
# configuration options for the AutoGen Definitions output
#---------------------------------------------------------------------------
GENERATE_AUTOGEN_DEF   = NO
#---------------------------------------------------------------------------
# configuration options related to the Perl module output
#---------------------------------------------------------------------------
GENERATE_PERLMOD       = NO
PERLMOD_LATEX          = NO
PERLMOD_PRETTY         = YES
PERLMOD_MAKEVAR_PREFIX = 
#---------------------------------------------------------------------------
# Configuration options related to the preprocessor   
#---------------------------------------------------------------------------
ENABLE_PREPROCESSING   = YES
MACRO_EXPANSION        = YES
EXPAND_ONLY_PREDEF     = NO
SEARCH_INCLUDES        = YES
INCLUDE_PATH           = 
INCLUDE_FILE_PATTERNS  = 
PREDEFINED             = 
EXPAND_AS_DEFINED      = 
SKIP_FUNCTION_MACROS   = YES
#---------------------------------------------------------------------------
# Configuration::additions related to external references   
#---------------------------------------------------------------------------
TAGFILES               = 
GENERATE_TAGFILE       = 
ALLEXTERNALS           = NO
EXTERNAL_GROUPS        = YES
PERL_PATH              = /usr/bin/perl
#---------------------------------------------------------------------------
# Configuration options related to the dot tool   
#---------------------------------------------------------------------------
CLASS_DIAGRAMS         = YES
MSCGEN_PATH            = 
HIDE_UNDOC_RELATIONS   = YES
HAVE_DOT               = YES
DOT_FONTNAME           = Helvetica
DOT_FONTPATH           = 
CLASS_GRAPH            = YES
COLLABORATION_GRAPH    = YES
GROUP_GRAPHS           = YES
UML_LOOK               = NO
TEMPLATE_RELATIONS     = YES
INCLUDE_GRAPH          = YES
INCLUDED_BY_GRAPH      = YES
CALL_GRAPH             = YES
CALLER_GRAPH           = YES
GRAPHICAL_HIERARCHY    = YES
DIRECTORY_GRAPH        = YES
DOT_IMAGE_FORMAT       = png
DOT_PATH               = 
DOTFILE_DIRS           = 
DOT_GRAPH_MAX_NODES    = 50
MAX_DOT_GRAPH_DEPTH    = 1000
DOT_TRANSPARENT        = YES
DOT_MULTI_TARGETS      = NO
GENERATE_LEGEND        = YES
DOT_CLEANUP            = YES
#---------------------------------------------------------------------------
# Configuration::additions related to the search engine   
#---------------------------------------------------------------------------
SEARCHENGINE           = YES

