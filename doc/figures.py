
import os

script_dir = os.path.dirname( __file__)
f = open( script_dir+'/figures.list', 'r')

for l in f :
    l = l.strip()
    try:
        os.system( 'python %s' %script_dir+'/../'+l+'/doc/scripts/figures.py')
    except:
        continue
