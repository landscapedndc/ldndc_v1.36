## Model descriptions:

-   The html version can be created in build/doc/ by doxygen doxygen.conf
-   After commiting changes in the code the online version is automatically updated after some hours.
-   The .pdf file gets created in ldndc/doc/models-description/ by `./make-md.sh pdf`\
    The path in line 4: `crabmeat_dir="${2:-$HOME/crabmeat}"` might need to be modified.
-   To include changes made in the code, some files need to be copied from the build directory after running doxygen. From `build/doc/latex/*.tex` to `ldndc/doc/doxygen/latex/`

It is all done with a script like this:

``` bash
cd /home/boos-c/ldndc/build
doxygen doc/doxygen.conf
cp doc/latex/vegetation.tex ../doc/doxygen/latex/
cp doc/latex/eventhandler.tex ../doc/doxygen/latex/
cp doc/latex/watercycle.tex ../doc/doxygen/latex/
cp doc/latex/microclimate.tex ../doc/doxygen/latex/
cp doc/latex/soilchemistry.tex ../doc/doxygen/latex/
cd /home/boos-c/ldndc/doc/models-description
sh ./make-md.sh pdf
```

## User guide:

-   The .pdf file gets created in `/ldndc/doc/usersguide/` by running:
    -   Linux/MacOS:

        ```{bash}
        ./make-ug.sh pdf 
        ```

    -   Windows:

        ```{bash}
        bash make-ug.sh pdf /path/to/crabmeat/trunk
        ```
-   The path in line 4: `crabmeat_dir="${2:-$HOME/crabmeat}"` might need to be modified.
-   The parameter lists in the user guide (e.g. site paramters) are taken from crabmeat/scientific (e.g. siteparameters/siteparameters.txt).
