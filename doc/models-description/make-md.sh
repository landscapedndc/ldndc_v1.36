#!/bin/bash

make_command="${1:-"help"}"

if [ -z $doxygenbasedir ]
then
    doxygenbasedir="${2:-}"
fi

builddir="build"
mkdir -p $builddir
[ $? -ne 0 ] && exit 1

## base name for document
document_name="ldndc-models-description"
md="md"
ldndcmd_texinputs="$builddir/${md}-texinputs"
printf "export TEXINPUTS=./build\".:" > $ldndcmd_texinputs

cp $doxygenbasedir/latex/*.sty ./$builddir
#cp $doxygenbasedir/latex/longtable_doxygen.sty ./
#cp $doxygenbasedir/latex/tabu_doxygen.sty ./



have_raise=0
verbose=0
papers_bibtex_dir="../references"
pdflatex_opts="-halt-on-error -interaction errorstopmode -file-line-error -output-directory ./build"


dev=/dev/stdout
if [ $verbose -eq 0 ]
then
    dev=/dev/null
fi

function  build_pdf()
{
    echo "build pdf"
    pdflatex  $pdflatex_opts main.tex >$dev
    [ $? -ne 0 ] && exit 2
    echo "stage 1                   done."
    bibtex $builddir/main
    
    pdflatex  $pdflatex_opts main.tex >$dev
    [ $? -ne 0 ] && exit 3
    echo "stage 2                   done."
    pdflatex  $pdflatex_opts main.tex >$dev
    [ $? -ne 0 ] && exit 4
    echo "stage 3                   done."
    
    cp $builddir/main.pdf ${document_name}.pdf
    xdotool_bin="/usr/bin/xdotool"
    if [ $have_raise -eq 1 -a -x "$xdotool_bin" ]
    then
        ## if logged in from remote, set DISPLAY environment variable
        hostname=`who am i | cut -f2  -d\( | cut -f1 -d:`
        if [ "_$hostname" != "_" ]
        then
            export DISPLAY=':0.0'
        fi
        wid="not_set"
        if [ -r ".${document_name}-wid" ]
        then
            ## used cached window ID
            wid=`cat .${document_name}-wid`
        elif [ "$wid" = "not_set" ]
        then
            ## determine window ID
            wid_desktop=`$xdotool_bin get_desktop`
            wid=`$xdotool_bin search --onlyvisible --desktop $wid_desktop --limit 1 --name "${document_name}.pdf"`
            if [ $? -ne 0 ]
            then
                ## pdf viewer not running
                exit 0
            fi
            echo "WID=$wid"
            echo "$wid" > .${document_name}-wid
        fi
        ## check if pdf viewer is running
        $xdotool_bin getwindowname $wid 2>/dev/null | grep -q "${document_name}.pdf" || exit 0
    
        ## send key stroke and raise window
        $xdotool_bin key --window $wid --clearmodifiers "r"
        $xdotool_bin windowraise $wid
    fi
}

function build_html()
{
    htlatex.sh main $builddir/ ""
}

function grep_version()
{
    local version_file="$1"
    local version="`head -n1 $version_file`"
    if [ -z "$version" ]
    then
        version='?'
    fi
    printf '\\newcommand\\ldndcversion{%s}\n' "$version"
}

function grep_revision()
{
    local  revision_file="$1"
    local  revision="`head -n1 $revision_file`"
    if [ -z "$revision" ]
    then
        revision='?'
    fi
    printf '\\newcommand\\ldndcmdrevision{%s}\n' "$revision"
}

function generate_model_doctexts1()
{
    #local  modelsbasedir="../../"
    if [ ! -d "$doxygenbasedir/latex/" ]
    then
        echo "$doxygenbasedir/latex/ does not exist" >&2
        continue
    fi
    modeltex=$1
    #printf '%s:' "$doxygenbasedir/latex/" >> $ldndcmd_texinputs
    cp "$doxygenbasedir/latex/$modeltex.tex" $builddir
    
    sed -e '/\\hypertarget/,$!d' $builddir/$modeltex.tex > $builddir/${modeltex}_tmp.tex
    mv $builddir/${modeltex}_tmp.tex $builddir/${modeltex}.tex
    sed -e '1s/^.*\\hypertarget/\\hypertarget/'  $builddir/${modeltex}.tex > $builddir/${modeltex}_tmp.tex
    mv $builddir/${modeltex}_tmp.tex $builddir/${modeltex}.tex

    sed -e 's/\\doxysubsubsection/\\section/g' $builddir/$modeltex.tex > $builddir/${modeltex}_tmp.tex
    mv $builddir/${modeltex}_tmp.tex $builddir/${modeltex}.tex
    sed -e 's/\\doxysubsubsubsection/\\subsection/g' $builddir/$modeltex.tex > $builddir/${modeltex}_tmp.tex
    mv $builddir/${modeltex}_tmp.tex $builddir/${modeltex}.tex
    sed -e 's/\\doxysubsubsubsubsection/\\subsubsection/g' $builddir/$modeltex.tex > $builddir/${modeltex}_tmp.tex
    mv $builddir/${modeltex}_tmp.tex $builddir/${modeltex}.tex
    sed -e 's/\\doxysubsubsubsubsubsection/\\paragraph/g' $builddir/$modeltex.tex > $builddir/${modeltex}_tmp.tex
    mv $builddir/${modeltex}_tmp.tex $builddir/${modeltex}.tex
}
function generate_model_doctexts2()
{
    #local  modelsbasedir="../../"
    if [ ! -d "$doxygenbasedir/latex/" ]
    then
        echo "$doxygenbasedir/latex/ does not exist" >&2
        continue
    fi
    modeltex=$1
    #printf '%s:' "$doxygenbasedir/latex/" >> $ldndcmd_texinputs
    cp "$doxygenbasedir/latex/$modeltex.tex" $builddir
    
    sed -e 's/\\doxysubsubsection/\\section/g' $builddir/$modeltex.tex > $builddir/${modeltex}_tmp.tex
    mv $builddir/${modeltex}_tmp.tex $builddir/${modeltex}.tex
    sed -e 's/\\doxysubsubsubsection/\\subsection/g' $builddir/$modeltex.tex > $builddir/${modeltex}_tmp.tex
    mv $builddir/${modeltex}_tmp.tex $builddir/${modeltex}.tex
    sed -e 's/\\doxysubsubsubsubsection/\\subsubsection/g' $builddir/$modeltex.tex > $builddir/${modeltex}_tmp.tex
    mv $builddir/${modeltex}_tmp.tex $builddir/${modeltex}.tex
    sed -e 's/\\doxysubsubsubsubsubsection/\\paragraph/g' $builddir/$modeltex.tex > $builddir/${modeltex}_tmp.tex
    mv $builddir/${modeltex}_tmp.tex $builddir/${modeltex}.tex
}
function generate_model_doctexts()
{
    #local  modelsbasedir="../../"
    if [ ! -d "$doxygenbasedir/latex/" ]
    then
        echo "$doxygenbasedir/latex/ does not exist" >&2
        continue
    fi
    modeltex=$1
    #printf '%s:' "$doxygenbasedir/latex/" >> $ldndcmd_texinputs
    cp "$doxygenbasedir/latex/$modeltex.tex" $builddir
    
    
    sed -e 's/\\doxysubsection/\\section/g' $builddir/$modeltex.tex > $builddir/${modeltex}_tmp.tex
    mv $builddir/${modeltex}_tmp.tex $builddir/${modeltex}.tex
    sed -e 's/\\doxysubsubsection/\\subsection/g' $builddir/$modeltex.tex > $builddir/${modeltex}_tmp.tex
    mv $builddir/${modeltex}_tmp.tex $builddir/${modeltex}.tex
    sed -e 's/\\doxyparagraph/\\subsubsection/g' $builddir/$modeltex.tex > $builddir/${modeltex}_tmp.tex
    mv $builddir/${modeltex}_tmp.tex $builddir/${modeltex}.tex
    sed -e 's/\\doxysubparagraph/\\paragraph/g' $builddir/$modeltex.tex > $builddir/${modeltex}_tmp.tex
    mv $builddir/${modeltex}_tmp.tex $builddir/${modeltex}.tex
}

function usage()
{
    echo  -e "usage:"
    echo  -e "`basename $0` [command]"
    echo  -e ""
    echo  -e "COMMANDS:"
    echo  -e "\tpdf     build pdf version"
    echo  -e "\thtml    build html version"
    echo  -e ""
    echo  -e "\tclean   delete temporary files"
    echo  -e "\thelp    show this help"
}


if [ "$make_command" = "clean" ]
then
    rm -f -r "$builddir" "${document_name}.pdf" ".${document_name}-wid"
    exit 0
elif [ "$make_command" = "help" -o "$make_command" = "--help" -o "$make_command" = "h" -o "$make_command" = "-h" ]
then
    usage
    exit 0
elif [ "$make_command" = "pdf" ]
then
    :
elif [ "$make_command" = "html" ]
then
    :
else
    echo "command not understood  [$make_command]" 1>&2
    echo ""
    usage
    exit 1
fi

ldndcmd_version_file="$builddir/.${document_name}-version"
ldndcmd_version_texfile="$builddir/${md}-version.tex"
ldndc_version_file="../../ldndcversion"
if [ -r "$ldndc_version_file" ]
then
    head -n1 "$ldndc_version_file" > "$ldndcmd_version_file"
else
    echo -en '?' > "$ldndcmd_version_file"
fi
grep_version "$ldndcmd_version_file" > "$ldndcmd_version_texfile"
rm -f "$ldndcmd_version_file"

ldndcmd_revision_file="$builddir/.${document_name}-revision"
ldndcmd_revision_texfile="$builddir/${md}-revision.tex"
which svnversion >/dev/null && svnversion -n > "${ldndcmd_revision_file}"
if [ ! -r "$ldndcmd_revision_file" ]
then
    echo -n '<unknown>' > "$ldndcmd_revision_file"
fi
grep_revision "$ldndcmd_revision_file" > "$ldndcmd_revision_texfile"
rm -f "$ldndcmd_revision_file"

echo "" > references.bib
if [ -d "$papers_bibtex_dir" ]
then
    cat $papers_bibtex_dir/* >> references.bib
fi

generate_model_doctexts1 "ldndc_mobile"
generate_model_doctexts2 "microclimate"
generate_model_doctexts2 "canopyecm"
generate_model_doctexts2 "miclibs"
generate_model_doctexts2 "watercycle"
generate_model_doctexts2 "echy"
generate_model_doctexts2 "watercycledndc"
generate_model_doctexts2 "waterlibs"
generate_model_doctexts2 "vegetation"
generate_model_doctexts2 "arabledndc"
generate_model_doctexts2 "grasslanddndc"
generate_model_doctexts2 "plamox"
generate_model_doctexts2 "psim"
generate_model_doctexts2 "pnet"
generate_model_doctexts2 "treedyn"
generate_model_doctexts2 "photofarquhar"
generate_model_doctexts2 "veglibs"
generate_model_doctexts2 "mobile_plant"
generate_model_doctexts2 "soilchemistry"
generate_model_doctexts2 "metrx"
generate_model_doctexts2 "soildndc"
generate_model_doctexts2 "soillibs"
generate_model_doctexts2 "ldndc_mobile_outputs"

generate_model_doctexts2 "soilchemistryoutput"
generate_model_doctexts2 "physiologyoutput"
generate_model_doctexts2 "vegetationstructureoutput"
generate_model_doctexts2 "microclimateoutput"
generate_model_doctexts2 "managementoutput"
generate_model_doctexts2 "watercycleoutput"
generate_model_doctexts2 "ecosystemoutput"
generate_model_doctexts2 "inventoryoutput"
generate_model_doctexts2 "ggcmioutput"
generate_model_doctexts2 "dssoutput"


generate_model_doctexts1 "ldndc_farmsystem"
generate_model_doctexts1 "ldndc_echy3d"
generate_model_doctexts1 "ldndc_oryza2000"

for f in `find ../../models/ -name '*.png'`; 
    do cp $f build;  
done

for f in `find ../../kernels/ -name '*.png'`; 
    do cp $f build;  
done


printf "$builddir:$TEXINPUTS\"" >> $ldndcmd_texinputs
source $ldndcmd_texinputs

case "$make_command" in
    pdf)
        build_pdf
        ;;

    html)
        build_html
        ;;

    *)
        echo "command not understood [$make_command]" 1>&2
        usage
        ;;
esac
