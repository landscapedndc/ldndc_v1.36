
note 1: not completely implemented.

note 2: issue of  time difference vs. time period  still open...


specification (quasi EBNF, extended backus-naur form):

    <TIME> ::=  
    	[<DATE_OFFS_NOREPEAT> "<"["="]] 
    		<DATE_OFFS_REPEAT>|(<DATE_OFFS_NOREPEAT>["/"<R>]) [ "->" <DATE_OFFS_NOREPEAT>]
    	["<"["="] <DATE_OFFS_NOREPEAT>]
    
    <DATE_OFFS_REPEAT> ::= [<N> "*"]"@"<TIME_REPEAT>[("-"|"+")<PERIOD>]
    <DATE_OFFS_NOREPEAT> ::= ("@"<TIME_NOREPEAT>[("-"|"+")<PERIOD>])|<DATE>
    
    <TIME_REPEAT> ::= "subdaily"|"predaily"|"daily"|"premonthly"|"monthly"|"preyearly"|"yearly"
    <TIME_NOREPEAT> ::= "start"|"end"
    
    <DATE> ::= ([<N>"-"]([<M>"-"]<D>))["-"<S>]
    <PERIOD> ::= ([<N>"-"]([<N>"-"]<N>))["-"<N>]
    
    <N> ::= "0"|"1"|"2"|...
    <M> ::= "1"|"2"|...|"12"
    <D> ::= "1"|"2"|...|"366"
    <S> ::= "1"|...|<R>
    <R> ::= N
======================================================================

N (year offset or period time units), M (month), D (day), S (subday),
R is time resolution (i.e. tsmax)

nb: 1S*R = 1D, {28,29,30,31}D = 1M, 12M = {365,366}D

(a second date must be given for period events)

and symbolic time <SYMTIME> keywords mean (PERIOD defaults to +00-00-00)
	start					first day of rotation
	end					last day of simulation

	subdaily				every single timestep
	predaily,[post]daily			first/last subdaily step in day
[?]	preweekly, [post]weekly			weekly
[?]	prefortnightly, [post]fortnightly	fortnightly
	premonthly, [post]monthly		monthly
	preyearly, [post]yearly			yearly



examples:
	1-11-01
	first of november in the second year

	@start+03-00
	three months passed the start of simulation.

	@daily+00-00-00-12			(let R=24)
	each day at 12 oclock

	@daily+00-00-00-12 -> +00-00-00-06	(let R=24)
	each day from 12 oclock to 6pm

	@start+08 =< @monthly+00-02 -> +7 < 03-09-01
	event lasts one week from second day of each month starting
	with the eighth month after simulation take off until august
	in the forth year.

