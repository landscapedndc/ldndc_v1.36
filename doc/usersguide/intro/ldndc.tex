% vim: smartindent wrap lw=80 tabstop=4 shiftwidth=4 expandtab

In this section \ldndc's components and their interaction will be briefly
introduced. By components we mean parts of the code that are assigned specific
functionalities, e.g. supplying climate data. Components include the basic
functionality (e.g., defining various datatypes, logging system, configuration
file), simulation driver (e.g., input/output subsystem, project management) and
the client interface (i.e., I/O client library). Furthermore, the model pool
(i.e., the collection of models), the site instances manager (i.e. handling
domains, initialization, time stepping) and \defemph{models drivers} (within
technical contexts also termed \emph{kernels}). Models contain the (e.g.,
biogeochemical) process descriptions while kernels use and orchestrate models
to build new models addressing site and ecosystem specific requirements. As
such, kernels are an abstraction from concrete modelling code and form the
smallest building block in a domain. Kernels are controlled by the simulation's
initialization and runtime system without any knowledge of the underlying
concepts, processes or geographical extents.

\begin{figure}
    \begin{center}
        %% scale 68%
        \inputfigure{flowchart.pdf_t}
        \caption{\label{fig:flow_chart}\ldndc{} flow chart}
    \end{center}
\end{figure}

\subsection{\label{sec:design:abstract}A high-level perspective}

From ten thousand miles away we can summarize \ldndc\ as follows. After program
start-up input data will be read and prepared to be used by the kernels and
specifically by their models. After successfully reading \defemph{size limited}
(e.g. site parameters) and scanning parts of \defemph{size unlimited}
\todo{time dependent/ streamed?} (e.g. climate) input data, the simulation domain (i.e.,
each kernel) will be initialized. If initialization was successful the runtime
system takes over and calls each kernel to run for one time step. For the time
being we assume there is no communication between models. It is explained later
how communication is added, e.g., by means of ``attaching'' software components
that accomplish inter-model data exchange. Simulation continues until the
predefined number of time steps have been completed or no more active kernels
exist. A model is either deactivated by request or after an unsuccessful step.
During simulation output may be written. A schematic description of the
simulation system's flow of control can be seen in Figure~\ref{fig:flow_chart}.


\subsection{\label{sec:design:detail}Again in more detail}

The architecture of \ldndc\ is depicted in Figure~\ref{fig:ldndc_design}. In
this section each of these boxes in this figure is discussed. 

First of all some terms are introduced. Let a single simulation setup be called
a \defemph{project} not to be confused with a single \ldndc\ program
invocation. Currently, a single but complete set of input data forms a project.
In essence, this means that enough input data was provided to successfully set
up a simulation. A projects input data comprises multiple \defemph{sources}. By
definition, sources are read in isolation, i.e., they do not communicate or
share any data among themselves, that is to say, they exist independent of each
other.

\begin{figure}
        \begin{center}
                \inputfigure{ldndcdesigndg.pdf_t}
                %\hspace{10mm}\parbox{140mm}
                \caption{\label{fig:ldndc_design}\ldndc\ architecture and its
                components relation (here illustrated with the \mobile\ kernel)}
        \end{center}
\end{figure}


\subsection{\label{sec:design:domain}Domain}

One of the principal features of \ldndc\ is its capability to run large numbers
of single site simulations with one program invocation. Such a setup is called
\defemph{domain} (or alternatively \defemph{landscape}) and forms the largest
and most abstract building block within a single \ldndc\ simulation project. A
domain in this context must not strictly represent a geographical area but is
rather a collection of single sites that may or may not map to an existing
contiguous ecosystem. In particular when \ldndc\ is run in stand-alone mode
where no communication exists among the kernels. For example, consider simulating
the same site with a different parameter set for each kernel. Internally, there
always exists at least one domain, that is, the smallest possible domain has
size one. Even though the kernels do not need to be geographically related (in
the sense that each site has at least one neighbor) the fact that they in
general do is a major driving force for the development of this software. It is
this scenario that asks for the exchange of state information between the
single sites. How data is exchanged is explained later and is not relevant for
now. More fundamental is the fact that all kernels need to be synchronized with
respect to time. In contrast to existing batch execution paradigms, i.e. run
each site simulation after the other, each running from start to end, \ldndc\
runs all kernels concurrently. Even though this amounts to no more than
exchanging the order of the kernel- and time loop to have the kernel loop
within the time loop it introduces a drastic increase in memory consumption.
Having all input data loaded at the same time obviously yields a memory demand
that grows linearly with the number of kernels.


\subsection{\label{sec:design:model}Kernels revisited}

The evolution of a single site is dictated by the models that were ``assigned''
to it. Several different models might be used across the simulation domain. To
be able to mount any model into the simulation domain, \ldndc\ deals with
kernels that hide away model details. To stress the difference between
terminology we summarize: (a) \defemph{site} (also plot, field) denotes the
smallest geographical area of study within a simulation project, (b)
\defemph{model} is a mathematical description of a concept or process and (c)
\defemph{kernel} is the technical term for the collection of models associated
with one or multiple sites.


\subsection{\label{sec:design:bc}Boundary conditions}

All sources (e.g. climate, soil properties, agricultural or forest management)
form the set~$C$ that is part of the \defemph{boundary conditions}. These
sources are split into categories called \defemph{input classes}. Each kernel
in the domain is granted access to the inputs that were configured for it in
the sources provided for the \defemph{setup} input class (see
Section~\ref{sec:inputs:setup}). There exists an additional set of input
classes~$D$ that are obtained from combining several input classes from set~$C$
(e.g., living species are obtained from event input and species parameters).
Internally, all inputs are pooled and identified by their \defemph{source
identifier} and an additional numeric ID. This allows, in order to save time
and memory, to share input data among those kernel instances that request the
same inputs. Note, that this measure enforces strict read-only access to
inputs.


\subsection{\label{sec:design:bcpool}Boundary condition pool}

Input data is organized in kernel blocks and categories. That means each kernel
only reads input from one kernel block per input class type. Recall, that input
data that was not provided will not pose a problem as long as it is not
requested.

As a result, after successfully reading all input sources and creating input
classes for each category the following sets of input class blocks
\begin{eqnarray*}
        C & := & \bigcup_{j=1}^{\Omega}
                 \left\{
                        C^j_0, \ldots, C^j_{n_j}
                 \right\}, \;\;\;n_j \mbox{ number of blocks for class~$j$}
                 \cr
        D & := & \bigcup_{j=1}^{\Sigma}
                 \left\{
                         D^j_0, \ldots, D^j_{m_j}
                 \right\}, \;\;\;m_j \mbox{ number of resulting blocks for class~$j$}
\end{eqnarray*}
of all input classes will exist. For the sake of readablity input
classes~$C^{j}_0$ ($\il[j]{1}{\Omega}$) and $D^{j}_0$ ($\il[j]{1}{\Sigma}$)
always exist and represent ``empty'' input classes, i.e. those related to an
input source that was not provided. Note that for depending input classes all
dependencies have to be fulfilled, otherwise the class will not be created.

The \defemph{boundary condition pool} is now defined as
$$
        \bcpool := C \cup D
$$
holding all kernel blocks available during the simulation. Note that only those
input classes are created that are infact requested by at least one kernel. In
a project each participating kernel~$\theta$ is handed a set of boundary
conditions~$\bcset_{\theta}$ dictated by its setup. This set contains one input
class object of each type, that is
$$
        \bcset_{\theta} = \left\{
             C^1_{i^1_{\theta}},
             C^2_{i^2_{\theta}},
             \ldots,
             C^{\Omega}_{i^{\Omega}_{\theta}},
             D^1_{\pi_1( \mat{A}_{\text{dep}}, i^1_{\theta},\ldots,i^{\Omega}_{\theta})},
             \ldots,
             D^{\Sigma}_{\pi_{\Sigma}( \mat{A}_{\text{dep}},i^1_{\theta}, \ldots, i^{\Omega}_{\theta})}
             \right\}
$$
where indexes $i^1_{\theta}, i^2_{\theta}, \ldots, i^{\Omega}_{\theta}$ are
either $0$ or read from the Setup input class. The functions~$\pi_1, \ldots,
\pi_{\Sigma}$ yield a unique index for each dependency set~$d_j \subseteq
\left\{ i^1_{\theta}, \ldots, i^{\Omega}_{\theta} \right\}$, $\forall
j=1,\ldots, \Sigma$ based on dependency matrix~$\mat{A}_{\text{dep}}$.
Currently, $\pi_i$ evaluates to a $64$bit unsigned integer created from a
portion of an SHA1 hash of the source descriptors of the dependency set. No
duplicate input classes are created in $D$ which is the result of $\pi_j$
returning the same index for identical sets of dependencies. Consequently, IDs
for input classes~$D$ are assigned to kernels as opposed to being read
from input. 


\subsection{\label{sec:design:state}State}

The \defemph{state} is the changing and therefore modifiable part of the
ecosystem properties (e.g. plant biomasses, soil temperatures) as opposed to,
for example, latitude or considered species types. For example, the \mobile\
state is split into multiple \defemph{substates} each representing a part
of the ecosystem state. Each model in the domain has its own state. In a
coupled model run, the communication component provides means of reading and
writing states across kernels.


\subsection{\label{sec:design:iosubsys}Input/output subsystem}

As mentioned earlier, large scale simulations in general require huge amounts
of input data and produce even larger amounts of output. Infact, dealing with
I/O is a central concern of the framework. In contrast to previous versions of
the framework the handling of input sources and output sinks was completely
removed from all the components used by kernels and put into a dedicated
component of the framework. All input and output is processed exclusively by
this component. This ensures aggregated I/O which for example relieves the
filesystem of too much load. 

Within each project a set of input data is processed by \defemph{readers}.
Readers are created on demand depending on \defemph{input source format} and
\defemph{input class}. Input source format refers to the layout or
encoding of the input source (e.g. extended markup language (XML)). For
supported input source formats and known input classes in \ldndc\ see
Chapter~\ref{chap:inputs}, the attachment or browse the source code. Ideally,
there exist for each input class a reader for each input source format. Note
that the structure (here \emph{not} layout or encoding) of each input source is
predefined. Within a single input class type the readers typically extract
information from different formats but exhibit the exact same interface. This
mechanism hides the underlying source from the client fetching the data via the
reader. Output handling follows an analogous approach where the assumption that
the data contained in the output is known at compile time is dropped. For this
reason, components that wish to write data to a sink need to publish the data's
structure before using the sink.


\subsection{\label{sec:design:wd}Work dispatcher}

Parallelizing \ldndc\ using MPI (i.e. not OpenMP) distributing kernels across
available processing units is accomplished by assigning to them disjunct sets
of kernel setups of the full project input. This task is handled by the
\defemph{work dispatcher}. After such a partitioning has been done,
initialization can proceed in the normal fashion on each node.

Note that the kernel-to-node mapping might change over the course of a
simulation run due to an imbalance in the workload of CPUs. A project can hand
over kernels to projects running on different CPUs that have enough idle time
during one time step (bear in mind that even though running in parallel all
kernels still need to be synchronized in coupled mode). An imbalance might be
caused by differing configurations in process descriptions but is also likely
to occur in a heterogeneous computing environment or with non-dedicated
processing units with changing load. Such \defemph{load balancing} is planned
to appear in a future version of \ldndc.

