% vim: smartindent wrap lw=80

At various places in \ldndc, execution paths depend on time. For example, a
model might perform certain processes depending on season, day or night time or
the beginning of a year. The framework uses time information to find the
position from where to start to read records in time-dependent input data (e.g.
climate). The most notable user for time is without doubt the event subsystem.
Last but not least the simulation running time is given as a time period with a
start and end date.

Within
\ldndc\ we distinguish time objects representing fixed points in time, fixed
time periods and clock objects that start from a fixed point in time and have
the ability to advance forward in steps of one time unit (e.g. day). The later
being used to keep track of the position in terms of time the simulation has
reached. By design, a simulation is running strictly forward in time.

The most generic time object is the fixed period time object which contains two
fixed time objects representing the beginning and end of the period. Note that
minimum period-length is $1$ timestep, which depends on time resolution.

\ldndc\ can be compiled to either honor leap years or treat all years as
equal in length (i.e. 365 days). Be aware that all input data containing
references specific to leap years will most likely not work with a leap year
disabled version of \ldndc.

\section{\label{sec:clock:timespec}Time specification}

Within \ldndc\ the smallest time unit is the \defemph{subday}~$t_{\text{s}}$
which is an integer $0 < t_{\text{s}} \le t_{\text{res}}$ where $t_{\text{res}}
\in \setN$ is the \defemph{time resolution} denoting the number of steps a
single day is split into. For example, $t_{\text{res}} = 24$ yields a time
discretization of one hour. The four components of a fixed time are:
\defemph{subday}, \defemph{day}, \defemph{month} and \defemph{year}.


By design, all time specifications are mapped to the most generic
representation provided in the framework. That means any time specification
read from some source is represented as a time period. There are two formats
for valid time specifications
$$
        t_{\text{start}} \rightarrow t_{\text{end}}
        \qquad\quad\text{and}\quad\qquad
        t_{\text{start}} \rightarrow +t_{\text{diff}}
$$
where $t_{\text{end}} \ge t_{\text{start}}$ and $t_{\text{diff}}\ge0$. The
second format internally maps to the first by adding the difference
$t_{\text{diff}}$ to the start time $t_{\text{start}}$. Valid expressions for
time and periods are explained in detail in
section~\ref{sec:clock:timespec:ebnf}.


\subsection{\label{sec:clock:timespec:ebnf}Formal grammar}

Formally, a valid time expression is given in extended backus-naur form (EBNF).
Note that the earliest date~$T_0$ is the first subday at the first of January
1901 (\code{1901-01-01-01}). For the sake of a simpler notation the grammar
given does not consider the different month lengths and leap years. The
implementation, however, rejects such invalid dates (e.g. 31st of June).
\begin{verbatim}
    <TIME PERIOD> ::=  <DATE> "->" <DATE>|("+"<INTERVAL>)

    <DATE>        ::= [<Y>"-"][<M>"-"]<D>["-"<S>]["/"<R>]
    <INTERVAL>    ::= [<C>"-"][<C>"-"]<C>["-"<C>]

    <Y> ::= "1901"|"1902"|"1903"|...
    <M> ::= "1"|"2"|...|"12"
    <D> ::= "1"|"2"|...|"31"
    <S> ::= "1"|...|"<R>"
    
    <R> ::= "1"|"2"|"3"|...

    <C> ::= "0"|<R>
\end{verbatim}
where the time components are year~$Y$ (1901 based), month~$M$, day~$D$ and
subday~$S$. Only the day component is obligatory; components not given will be
substituted if possible or require the user to provide them. Substituting only
works if a \defemph{reference date} is available, e.g. the simulation's start
date is used as reference date in case input data is lacking time settings in
its global block.

The position of the component and the number of given components determines how
the component is interpreted, e.g. \code{02-01} will be the first of February.
Period time units are not limited, e.g. a period of $17$ month is valid while
in a date this would be invalid. Time resolution~$R$ is optional and defaults
to $1$. It may be added to the start date following a slash (``/''). Some
symbolic equivalences
$$
        1SR \equiv 1D
        \qquad
        \{28,29,30,31\}D \equiv 1M
        \qquad
        12M \equiv \{365,366\}D \equiv 1Y.
$$

\textbf{Some examples}
\begin{center}
        \begin{tabular}{r|l}
                \textbf{time specification} & \textbf{result in words} \cr

                \hline

                \code{01-01} & first of January of reference year \cr

                \code{1950-02-13/6} &  February 1950, time resolution of four hours\cr

                \code{1950-02-13-04/6} &  same as above, but subday offset is $16$ hours \cr

                \code{1950-02-13-07/6} &  invalid because subday is out of range \cr

                \code{2012-02-29} & 29th of February of 2012 \cr

                \code{2011-02-29} & invalid, because 2011 is no leap year \cr

                \code{2012-01-01 -> 2013-03-01} & a period of fourteen months \cr

                \code{2012-01-01 -> +01-02-00} & a period of fourteen months \cr

                \code{2012-01-01 -> +14-00} & a period of fourteen months \cr

        \end{tabular}
\end{center}


\subsection{\label{sec:clock:timespec:diff+period}Time difference vs time period}

Let $t$ be a valid \ldndc\ time. In addition to time difference
$t_{\text{diff}}$ the framework defines $t_{\text{period}}$ as time difference
increased by $1$. The unit of $1$ is determined from the time resolution of
$t$. The reason for this is in interpreting durations given only a single time
step.%

\textbf{example} In effect, this means simulation time specifications like
\begin{center}
        \code{2001-01-02/2} and \code{2001-01-02/2 -> +0}
\end{center}
will cause the simulation to do one subday timestep.


\section{\label{sec:clock:clock}Simulation clock}

Within each project exists a single (official) clock object made available for
read-only access to all kernels. It is initialized with the simulations start
date read from the project file (value optionally overwritten by command line
argument). After simulation start (i.e. models evolve in time) runtime control
advances the clock by one subday unit.

\ldndc\ defines the following discrete \emph{time positions}. Obviously, each
timestep is at a subday position. The beginning of a day, i.e. $t_{\text{s}} =
1$, is refered to as the pre-day position while the post-day position is
reached if $t_{\text{s}} = t_{\text{res}}$. In a similar way this can be
applied to pre- and post-year.

\begin{center}
        \begin{tabular}{r|l}
                \textbf{timemode identifier} & \textbf{meaning} \cr

                \hline

                \defemph{subdaily} & each timestep \\[3mm]

                \defemph{predaily} & beginning of day \cr
                \defemph{daily} & end of day \\[3mm]

                \defemph{premonthly} & beginning of day at beginning of month \cr
                \defemph{monthly} & end of day at end of month \\[3mm]

                \defemph{preyearly} & beginning of day at beginning of year \cr
                \defemph{yearly} & end of day at end of year
        \end{tabular}
\end{center}

