#!/bin/bash

make_command="${1:-"help"}"

if [ -z $crabmeat_dir ]
then
    #crabmeat_dir="${2:-$HOME/local/roots/crabmeat}"
    crabmeat_dir="${2:-$HOME/branches/crabmeat}"
fi

builddir="build"
mkdir -p $builddir
[ $? -ne 0 ] && exit 1

cp ../../doc/doxygen/latex/* $builddir

## base name for document
document_name="ldndc-usersguide"
ug="ug"
ldndcug_texinputs="$builddir/${ug}-texinputs"
printf "export TEXINPUTS=../../doc/doxygen/latex/:\".:" > $ldndcug_texinputs

have_raise=0
verbose=0
papers_bibtex_dir="../references"
pdflatex_opts="-halt-on-error -interaction errorstopmode -file-line-error -output-directory ./build"

cfgfile="$HOME/.ldndc/${ug}.cfg"
if [ -r "$cfgfile" ]
then
    echo "reading configuration... [$cfgfile]" >&2
    . "$cfgfile"
fi

dev=/dev/stdout
if [ $verbose -eq 0 ]
then
    dev=/dev/null
fi

function  build_pdf()
{
    echo "build pdf"
    pdflatex  $pdflatex_opts main.tex >$dev
    [ $? -ne 0 ] && exit 2
    echo "stage 1                   done."
    bibtex $builddir/main
    
    pdflatex  $pdflatex_opts main.tex >$dev
    [ $? -ne 0 ] && exit 3
    echo "stage 2                   done."
    pdflatex  $pdflatex_opts main.tex >$dev
    [ $? -ne 0 ] && exit 4
    echo "stage 3                   done."
    
    cp $builddir/main.pdf ${document_name}.pdf
    xdotool_bin="/usr/bin/xdotool"
    if [ $have_raise -eq 1 -a -x "$xdotool_bin" ]
    then
        ## if logged in from remote, set DISPLAY environment variable
        hostname=`who am i | cut -f2  -d\( | cut -f1 -d:`
        if [ "_$hostname" != "_" ]
        then
            export DISPLAY=':0.0'
        fi
        wid="not_set"
        if [ -r ".${document_name}-wid" ]
        then
            ## used cached window ID
            wid=`cat .${document_name}-wid`
        elif [ "$wid" = "not_set" ]
        then
            ## determine window ID
            wid_desktop=`$xdotool_bin get_desktop`
            wid=`$xdotool_bin search --onlyvisible --desktop $wid_desktop --limit 1 --name "${document_name}.pdf"`
            if [ $? -ne 0 ]
            then
                ## pdf viewer not running
                exit 0
            fi
            echo "WID=$wid"
            echo "$wid" > .${document_name}-wid
        fi
        ## check if pdf viewer is running
        $xdotool_bin getwindowname $wid 2>/dev/null | grep -q "${document_name}.pdf" || exit 0
    
        ## send key stroke and raise window
        $xdotool_bin key --window $wid --clearmodifiers "r"
        $xdotool_bin windowraise $wid
    fi
}

function build_html()
{
    htlatex.sh main $builddir/ ""
}

function grep_version()
{
    local version_file="$1"
    local version="`head -n1 $version_file`"
    if [ -z "$version" ]
    then
        version='?'
    fi
    printf '\\newcommand\\ldndcversion{%s}\n' "$version"
}

function grep_revision()
{
    local  revision_file="$1"
    local  revision="`head -n1 $revision_file`"
    if [ -z "$revision" ]
    then
        revision='?'
    fi
    printf '\\newcommand\\ldndcugrevision{%s}\n' "$revision"
}

function generate_parameter_doctexts()
{
    local  parameterset="$1"
    local  parameterfile="$crabmeat_dir/scientific/${parameterset}parameters/${parameterset}parameters.txt"
    if [ ! -r "$parameterfile" ]
    then
        echo "parameter file not readable [$parameterfile]" 1>&2
        exit 
    fi
    python ug-p2l.py $parameterset "$parameterfile" "latex" > $builddir/inputs-source-${parameterset}parameters-list.tex  2> /dev/null
    python ug-p2l.py $parameterset "$parameterfile" "xml" > $builddir/inputs-source-${parameterset}parameters-xml-list.tex  2> /dev/null
}

function usage()
{
    echo  -e "usage:"
    echo  -e "`basename $0` [command]"
    echo  -e ""
    echo  -e "COMMANDS:"
    echo  -e "\tpdf     build pdf version"
    echo  -e "\thtml    build html version"
    echo  -e ""
    echo  -e "\tclean   delete temporary files"
    echo  -e "\thelp    show this help"
}

## crabmeat_cmake_module="../../Lconfig/components.d/Findcrabmeat.cmake"
## if [ ! -r "$crabmeat_cmake_module" ]
## then
##     echo "we require cmake module for 'crabmeat' (Findcrabmeat.cmake)" 1>&2
##     exit 7
## fi
## crabmeat_dir=( `egrep -m1 -o 'CRABMEAT_SOURCE_DIR\ *\".*\"' $crabmeat_cmake_module` )
## crabmeat_dir="`echo -n ${crabmeat_dir[1]} | tr -d '"'`"

if [ "$make_command" = "clean" ]
then
    rm -f -r "$builddir" "${document_name}.pdf" ".${document_name}-wid"
    exit 0
elif [ "$make_command" = "help" -o "$make_command" = "--help" -o "$make_command" = "h" -o "$make_command" = "-h" ]
then
    usage
    exit 0
elif [ "$make_command" = "pdf" ]
then
    :
elif [ "$make_command" = "html" ]
then
    :
else
    echo "command not understood  [$make_command]" 1>&2
    echo ""
    usage
    exit 1
fi

ldndcug_version_file="$builddir/.${document_name}-version"
ldndcug_version_texfile="$builddir/${ug}-version.tex"
ldndc_version_file="../../ldndcversion"
if [ -r "$ldndc_version_file" ]
then
    head -n1 "$ldndc_version_file" > "$ldndcug_version_file"
else
    echo -en '?' > "$ldndcug_version_file"
fi
grep_version "$ldndcug_version_file" > "$ldndcug_version_texfile"
rm -f "$ldndcug_version_file"

ldndcug_revision_file="$builddir/.${document_name}-revision"
ldndcug_revision_texfile="$builddir/${ug}-revision.tex"
which svnversion >/dev/null && svnversion -n > "${ldndcug_revision_file}"
if [ ! -r "$ldndcug_revision_file" ]
then
    echo -n '<unknown>' > "$ldndcug_revision_file"
fi
grep_revision "$ldndcug_revision_file" > "$ldndcug_revision_texfile"
rm -f "$ldndcug_revision_file"

echo "" > references.bib
if [ -d "$papers_bibtex_dir" ]
then
    cat $papers_bibtex_dir/* >> references.bib
fi

mkdir -p figures
[ $? -ne 0 ] && exit 1
figconvert_bin="../figures/figures-convert.sh"
if [ -r "$figconvert_bin" ]
then
    bash "$figconvert_bin" ${ug}-figures.cfg
else
    echo "not converting figures, hoping they exist :-)" 1>&2
fi

generate_parameter_doctexts "site"
## TODO generate_parameter_doctexts "humus"
generate_parameter_doctexts "soil"
generate_parameter_doctexts "species"

#generate tutorial texts
python tutorials/tutorial-2.py
#generate species lists
python inputs/generate_species_list.py

printf "$builddir:$TEXINPUTS\"" >> $ldndcug_texinputs
source $ldndcug_texinputs

case "$make_command" in
    pdf)
        build_pdf
        ;;

    html)
        build_html
        ;;

    *)
        echo "command not understood [$make_command]" 1>&2
        usage
        ;;
esac
