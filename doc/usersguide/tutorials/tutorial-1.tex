
%do not show subsections in Contents
%\addtocontents{toc}{\protect\setcounter{tocdepth}{1}}

\section{\label{sec:tutorials:tutorial1}Tutorial I: General model setup}

The purpose of tutorial I is to make the user familiar with the general model
setup, i.e., what kind of model inputs are needed and how they are created. The
tutorial refers to an experimental site, which is located on the Central Swiss
Plateau near the village of Oensingen in the north-western part of Switzerland
(7$^{\circ}$44'E, 47$^{\circ}$17'N, 450 m a.s.l.). For the interested
readership, further details regarding, e.g., site conditions, experimental design
can be found in \cite{ammann:2007a, ammann:2009a}. 

In order to run a valid \ldndc simulation, several model inputs
need to be provided. As a first step, open the tutorials folder
\code{CH\_oensingen} and find the following model input files:

\begin{enumerate}
    \item Project file (\code{CH\_oensingen.ldndc})
    \item Model setup file (\code{CH\_oensingen\_setup.xml})
    \item Site properties file (\code{CH\_oensingen\_site.xml})
    \item Climate file (\code{CH\_oensingen\_climate.txt})
    \item Airchemistry file (\code{CH\_oensingen\_airchemistry.txt})
    \item Events file (\code{CH\_oensingen\_events.xml})
    \item Site parameter file (\code{CH\_oensingen\_siteparameters.xml})
    \item Species parameter file (\code{CH\_oensingen\_speciesparameters.xml})
\end{enumerate}

Each of the files listed is incomplete in some way and prevents \ldndc\ from
running a successful simulation. The goal of this tutorial is to add the missing
information, which is outlined in the following list, and finally run a
successful simulation. Pay attention to the correct syntax, the use of a
text editor will help. For MS Windows users we recommend Notepad++.

\begin{enumerate}
    \item Project file \\
    The project file defines, e.g., the start and end of a simulation, the
    location of the required simulation input as well as the location of the
    model output. Note that the specification of input and output location
    within the project file are expanded by the particular configuration
    file (see Sec.~\ref{sec:configure}~and~\ref{sec:inputs:projectfile}). 
    Open the project file \code{CH\_oensingen.ldndc} and define:
    \begin{itemize}
        \item Start of simulation to: 1 January 2001 (see
            Sec.~\ref{sec:projectfile:schedule})
        \item Time resolution (1 hr) and duration of the simulation (5 years)
            \lstset{language=XML}
            \begin{lstlisting}
<schedule  time="2001-01-01/24 -> +5-0-0" />
            \end{lstlisting}
        \item Complete set of model inputs (i.e., setup, site, airchemistry,
            climate, events)
            \lstset{language=XML}
            \begin{lstlisting}
<sources sourceprefix="tutorials/tutorial_1/CH_oensingen_" >
  <setup source="setup.xml" />
  <site source="site.xml" />
  <airchemistry source="airchemistry.txt" />
  <climate source="climate.txt" />
  <event source="events.xml" />
  <speciesparameters source="speciesparameters.xml" />
  <siteparameters source="siteparameters.xml" />
</sources>
            \end{lstlisting}
        \item Location of the model output
            \begin{lstlisting}
<sinks sinkprefix="tutorials/tutorial_1/CH_oensingen_output/CH_oensingen_" />
            \end{lstlisting}
    \end{itemize}
    
    \item Setup file \\
    The setup file defines, e.g., the actual model selection for a particular
    simulation (see Sec.~\ref{sec:inputs:setup} and \ref{sec:modelsselections}).
    Open the setup file \code{CH\_oensingen\_setup.xml} and define: 
    \begin{itemize}
        \item Simulation setup id of "0" and a meaningful name
            \begin{lstlisting}
<setup id="0" name="oensingen intensive" >
            \end{lstlisting}
        \item Geographic location (7$^{\circ}$44'E, 47$^{\circ}$17'N, 450 m a.s.l.)
            \begin{lstlisting}
<location elevation="450.0" latitude="47.17" longitude="7.44" />
            \end{lstlisting}
        \item Suitable model selection for temperate grassland simulation
            \begin{lstlisting}
<modulelist>
    <module id="microclimate:canopyecm" timemode="subdaily" />
    <module id="watercycle:watercycledndc" timemode="subdaily" />
    <module id="airchemistry:airchemistrydndc" timemode="subdaily"/>
    <module id="physiology:plamox" timemode="subdaily" />
    <module id="soilchemistry:metrx" timemode="subdaily" />

    <!-- outputs -->
    <module id="output:ecosystem:daily" />
    <module id="output:microclimate:daily" />
    <module id="output:watercycle:daily" />
    <module id="output:physiology:daily" />
    <module id="output:vegstructure:daily" />
    <module id="output:soilchemistry:daily" />
    <module id="output:soilchemistry:yearly" />
    <module id="output:report:arable" timemode="subdaily" />
</modulelist>
            \end{lstlisting}
    \end{itemize}

    \item Site file \\
    Open the site file \code{CH\_oensingen\_site.txt} and define:
    \begin{itemize}
        \item Soil type as silty clay (see Sec.~\ref{sec:inputs:site}) and soil
            organic carbon content in 5cm (3\%) and 30 cm (0.5\%).
            \begin{lstlisting}
<general soil="SLCL" corg5="0.03" corg30="0.005" />
            \end{lstlisting}
        \item Define an explicit 2 cm soil layer discretization (see
            Sec.~\ref{sec:inputs:site}) for the upper soil stratum.
            \begin{lstlisting}
<layer depth="100" split="5" corg="0.029" bd="1.2" 
 wcmax="450.0" wcmin="220.0" clay="0.44"/>            
            \end{lstlisting}
        \item Add an additional soil stratum at the bottom with an extension of
            1m \begin{lstlisting}
<layer depth="1000" corg="0.005" bd="1.2" 
 wcmax="450.0" wcmin="220.0" clay="0.44"/>            
            \end{lstlisting}
    \end{itemize}

    \item Climate file \\
    Open the climate file \code{CH\_oensingen\_climate.txt} and define:
    \begin{itemize}
        \item Time setting (start: '2001-01-01', daily time resolution)
        \item Climate id
            \begin{lstlisting}
%global
    time = "2001-01-01/1"

%climate
    id = 0            
            \end{lstlisting}
%        \item Climate attributes (see Sec.~\ref{sec:inputs:climate} and~\ref{input:if:txt:climate}):
%        \begin{itemize}
%            \item elevation: 400 [m]
%            \item latitude: 47.17
%            \item longitude: 7.44
%            \item wind speed: 3.0 [m s$^{-1}$]
%            \item annual precipitation: 512 [mm]
%            \item temperature average: 9.9247 [$^{\circ}$C]
%            \item temperature amplitude: 20 [$^{\circ}$C]
%        \end{itemize}        
    \end{itemize}
    
    \item Airchemistry file \\
    Open the airchemistry file \code{CH\_oensingen\_airchemistry.txt} and define:
    \begin{itemize}
        \item CO$_2$ concentration: 380 [ppm] (see Sec.~\ref{sec:inputs:airchemistry})
        \item Nitrogen wet deposition (see Sec.~\ref{sec:inputs:airchemistry} ): 
        \begin{itemize}
            \item Ammonium: 0.6 [mg l$^{-1}$]
            \item Nitrate: 0.3 [mg l$^{-1}$]
        \end{itemize}
            \begin{lstlisting}
%data 
nh4 no3 co2
0.6 0.3 380.0
            \end{lstlisting}
    \end{itemize}
    
    \item Event file \\
    Open the event file \code{CH\_oensingen\_events.txt} and add a planting
    event for perennial grass (species = \code{perg}) at the first day of the
    simulation.  Note that in case that your planting event is scheduled before
    the start of the simulation it will not be considered!  The initial biomass
    should be 200 [kg DW ha$^{-1}$] (see Sec.~\ref{sec:inputs:event}).

        \begin{lstlisting}
<event type="plant" time="2001-01-01" >
    <plant name="perg" type="perg" >
        <grass initialbiomass="200.0" />
    </plant>
</event>    
        \end{lstlisting}

    \item Site parameter file \\
            Open the site parameter file
            \code{CH\_oensingen\_siteparameters.xml} and assign a value of 0.5 to
            the locally changed site parameter \emph{RETNO3}.
        \begin{lstlisting}
<siteparameters id="0" >
    <par name="retno3" value ="0.5" />
</siteparameters>        
        \end{lstlisting}            
    \item Species parameter file \\
            Open the species parameter file 
            \code{CH\_oensingen\_speciesparameters.xml} and assign a value of 0.5 to the
            locally changed species parameter \emph{INI\_N\_FIX} for the plant species
            \emph{PERG} of the group \emph{grass}.
        \begin{lstlisting}
<ldndcspeciesparameters>
    <speciesparameters id="0" >
        <species mnemonic="perg" group="grass" >
            <par name="ini_n_fix" value="0.5" />
        </species>
    </speciesparameters>
</ldndcspeciesparameters>
        \end{lstlisting}            
\end{enumerate}








%\subsection{\label{sec:tutorials:tutorial1:solution}Solution tutorial I}
%
%\begin{enumerate}
%
%    \item Project file \\
%    Simulation start and duration needs to be given as 'schedule'-attribute. The
%    location of simulation input and output can be simplified by prefixes. The
%    default configuration file after successful \ldndc\ installation assumes
%    model input to be in: 'source-to-ldndc-package/projects/'. The path can be
%    further refined by the project file, here: 'tutorials/...'.
%    \lstset{language=XML}
%    \begin{lstlisting}
%<?xml version="1.0" ?>
%<ldndcproject>
%  <schedule  time="2001-01-01/24 -> +8-0-0" />
%    <input>
%      <sources  sourceprefix="tutorials/tutorial_1/CH_oensingen_" >
%        <setup  source="setup.xml" />
%        <site  source="site.xml" />
%        <airchemistry  source="airchemistry.txt" />
%        <climate  source="climate.txt" />
%        <event  source="events.xml" />
%      </sources>
%      <attributes  use="0" endless="yes" />
%    </input>
%    <output>
%      <sinks sinkprefix="tutorials/tutorial_1/CH_oensingen_output/CH_oensingen_" />
%    </output>
%</ldndcproject>
%    \end{lstlisting}
%
%
%    \item Setup file \\
%    Each setup block refers to a single simulation unit, which is identified by
%    an according setup-id. The setup-id is set as 'setup' attribute. Geographic
%    location needs to be defined as 'location' attribute. In order to simulate
%    Oensingen, we choose the '\_MoBiLE' model whose submodel-setup can be defined
%    within the 'mobile' section.
%    \lstset{language=XML}
%    \begin{lstlisting}
%<?xml version="1.0" encoding="UTF-8"?>
%<ldndcsetup>
%  <setup id="0" name="Oensingen tutorial" >
%    <location elevation="450.0" latitude="47.17" longitude="7.44" />
%    <models>
%      <model id="_MoBiLE" />
%    </models>
%    <mobile>
%      <modulelist>
%        <module id="microclimate:canopyecm" timemode="subdaily" />
%        <module id="microclimate:dndc-impl" timemode="subdaily" />
%        <module id="watercycle:dndc" timemode="subdaily"/>
%        <module id="airchemistry:depositiondndc" timemode="subdaily"/>
%        <module id="physiology:photofarquhar" timemode="subdaily" />
%        <module id="physiology:plamox" timemode="subdaily" />
%        <module id="soilchemistry:metrx" timemode="subdaily" />
%
%        <!-- outputs -->
%        <module id="output:ecosystem:daily" />
%        <module id="output:microclimate:daily" />
%        <module id="output:watercycle:daily" />
%        <module id="output:physiology:daily" />
%        <module id="output:soilchemistry:daily" />
%      </modulelist>
%    </mobile>
%  </setup>
%</ldndcsetup>
%    \end{lstlisting}
%
%
%    \item Site file \\
%    The land use history and the litter height need to be set in the 'general'
%    section of the site file. Respective information is needed for appropriate soil
%    initialization. The litter height should only be non-zero for forest
%    ecosystems. The discretization of single soil layers is calculated by the
%    height of a soil stratum (set via the attribute 'depth') and the associated attribute
%    'split'. In order to get a 2 cm discretization for a predefined stratum
%    height of 60.0 mm, 'split' has to be set to 3.
%    \lstset{language=XML}
%    \begin{lstlisting}
%<?xml version="1.0"?>
%<site>
%  <soil>
%    <general usehistory="grassland" litterheight="0.0" soil="SICL"/>
%    <layers>
%      <layer depth="60.00" split="3"  bd="1.15" clay="0.42" 
%            corg="0.037" ph="7.30" wcmax="500.0" wcmin="150.0" />
%      <layer depth="100.0" split="5"  bd="1.15" clay="0.42" 
%            corg="0.034" ph="7.30" wcmax="490.0" wcmin="150.0" />
%      <layer depth="100.0" split="5"  bd="1.15" clay="0.43" 
%            corg="0.030" ph="7.30" wcmax="490.0" wcmin="220.0" />
%      <layer depth="140.0" split="7"  bd="1.20" clay="0.43" 
%            corg="0.023" ph="7.25" wcmax="490.0" wcmin="340.0" />
%      <layer depth="100.0" split="5"  bd="1.25" clay="0.43" 
%            corg="0.021" ph="7.25" wcmax="360.0" wcmin="350.0" />
%      <layer depth="100.0" split="5"  bd="1.26" clay="0.43" 
%            corg="0.009" ph="7.25" wcmax="360.0" wcmin="350.0" />
%      <layer depth="100.0" split="5"  bd="1.29" clay="0.43" 
%            corg="0.008" ph="7.33" wcmax="360.0" wcmin="350.0" />
%      <layer depth="200.0" split="10" bd="1.29" clay="0.43" 
%            corg="0.007" ph="7.33" wcmax="360.0" wcmin="350.0" />
%    </layers>
%  </soil>
%</site>
%    \end{lstlisting}
%
%
%    \item Climate file \\
%    Each climate file needs a global time definition, which a) defines the start
%    of the climate file and b) the time resolution. Note that there is no data column
%    expected that defines the actual date for a row. In fact, such a column can given but
%    will be ignored by \ldndc. 
%    Climate attributes are used for climate generation, which can be the case
%    if, e.g., simulation time is longer than available climate and no 'endless'
%    option (reload of climate) has been defined for the climate input.
%    \lstset{language=sh}
%    \begin{lstlisting}
%%global
%    time = "2001-01-01/1"
%
%%climate
%    id = 0
%
%%attributes
%    elevation = "450.0"
%    latitude = "47.17"
%    longitude = "7.44"
%    wind speed = "3.0"
%    annual precipitation = "512"
%    temperature average = "9.9247"
%    temperature amplitude = "20"
%
%%data
%*	*	prec	tavg	tmax	tmin	grad	rh	wind
%2001	1	0	-5.69	-99.99	-99.99	-99.99	78.74	1.63
%    \end{lstlisting}
%
%    \item Airchemistry file \\
%    Corresponding to climate data, nitrogen deposition and  CO$_2$ concentration
%    can be given on a daily basis. However, CO$_2$ concentration is fairly
%    constant on shorter time scale and daily data for nitrogen deposition
%    is mostly not available. Therefor, one often provides only average values,
%    which are reloaded every day.
%    \lstset{language=sh}
%    \begin{lstlisting}
%%global
%    time = "2001-01-01/1"
%
%%airchemistry
%    id = "0"
%
%%attributes
%    co2 = "370.0"
%
%%data 
%nh4   no3   co2
%0.635 0.365 370.0
%    \end{lstlisting}
%
%    \item Event file \\
%    Every plant species has to be planted by a corresponding event before it can
%    be simulated by \ldndc. For forest and grassland ecosystems, respective
%    planting events are usually defined at the first day of the simulation.
%    \lstset{language=sh}
%    \begin{lstlisting}
%<event type="plant" time="2001-01-01" >
%  <plant name="perennial_grass" type="perg" >
%    <grass initialbiomass="200" />
%  </plant>
%</event>
%    \end{lstlisting}
%
%\end{enumerate}

%\addtocontents{toc}{\protect\setcounter{tocdepth}{3}}

