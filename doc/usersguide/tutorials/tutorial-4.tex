
%do not show subsubsections in Contents
%\addtocontents{toc}{\protect\setcounter{tocdepth}{1}}


\section{\label{sec:tutorials:tutorial4}Tutorial IV: Simulations of
rice-based cropping systems}


In this tutorial the user learns step by step how to model rice cropping systems
with LandscapeDNDC. Rice cropping is often subject to field flooding and
therefore subject to fundamentally different ecosystem processes compared to
upland cropping systems. This tutorial is divided into two main components,
focusing first on the carbon cycle and second on the nitrogen cycle.

\subsection{\label{sec:tutorials:tutorial4:carboncycle}Carbon cycle}

\paragraph{Contents}

\begin{enumerate}
    \item How to implement important management steps (e.g., field flooding,
          tilling, harvest residues) required for rice cropping.
    \item How field flooding affects plant growth and the overall carbon cycling.
    \item The effect of different water regimes on CH$_4$ emissions
\end{enumerate}

\paragraph{Tasks}

\begin{enumerate}
    \item Find the project directory of tutorial IV.
    \item The current management defines the planting and harvest of one rice crop:

          \begin{lstlisting}
<event  type="plant" time="2007-02-01">
    <plant group="crop" type="ir72" name="ir72" >
        <crop initialbiomass="200.0" ></crop>
    </plant>
</event>
<event  type="harvest" time="2007-05-10">
    <harvest remains="0.0" name="ir72" />
</event>
          \end{lstlisting}
          Run a baseline simulation of \emph{methane.ldndc} with the preset
          setup and plot the simulated aboveground biomass.
    \item Rice crops display high sensitivity to drought, and as a result, they
          are commonly cultivated under flooded conditions. To maintain the required
          flooding, side bunds are built, and the soil is puddled. Puddling involves
          repeated ploughing under wet conditions over extended periods, leading to
          the formation of a compact soil layer at a depth of 10-30 cm, characterized
          by low saturated hydraulic conductivity. In order to establish a realistic
          water regime for rice cultivation, the following points are essential:

        \begin{itemize}
            \item Define field bunds from the
                  start of land preparation (approximately 3-4 weeks before planting)
                  until harvest.
                 \begin{lstlisting}
<event  type="flood" time="2007-01-01->2007-05-10">
    <flood bundheight="100.0" />
</event>
                 \end{lstlisting}  
            \item Add a tilling event under flooded condtions at the onset of field preparation.
                  Wet conditions can be established by defining a one-day flooding event.
                  event.
                  \begin{lstlisting}
<event  type="till" time="2007-01-01">
    <till depth="0.1" />
</event>            
<event  type="flood" time="2007-01-01->2007-01-02">
    <flood watertable="50.0" />
</event>
                 \end{lstlisting}
            \item During the planting period, the field is maintained without
                  flooding, and there is no presence of surface water. Instead, the
                  field is kept adequately moist to support the germination and
                  initial growth of the crops.  Defining a negative watertable of,
                  e.g., -10 mm yields water saturated conditions below 10 mm soil
                  depth but prevents creation of a surface watertable.
                 \begin{lstlisting}
<event  type="flood" time="2007-01-24->2007-02-07">
    <flood watertable="-10.0" />
</event>
                 \end{lstlisting}  
            \item Throughout the main cropping period, the field is
                  flooded, maintaining a surface water table at a depth of 5 to
                  10 cm. The surface water table is maintained from one week
                  after planting until maturity. This flooding practice ensures
                  a continuous water supply, supporting the optimal growth and
                  development of the crops until they are ready for harvest.
            \begin{lstlisting}
<event  type="flood" time="2007-02-07->2007-05-04">
    <flood watertable="50.0" />
</event>
            \end{lstlisting}                
        \end{itemize}

        Run the simulation again and compare the simulated aboveground biomass
        with the previous run. Compare also the drought stress factor of both
        simulations. 

        \fbox{\begin{minipage}{\textwidth}
        The drought stress factor
        $\phi_d$ is defined for the interval [0,1] (0: maximum drought stress;
        1: no drought stress). The factor depends on the actual soil water content
        ($\theta$) in relation to the soil water content at wilting point
        ($\theta_{wp}$), and field capacity ($\theta_{fc}$). The factor scales
        linearly depending on the species-specific parameter $H2OREF\_A$ that
        describes the critical water content when drought stress starts:

        \begin{align*}
        \phi_d = 
        \begin{cases}
                1 &            \; \theta \ge H2OREF\_A \cdot \theta_{fc} \\
                \frac{\theta - \theta_{wp}}{H2OREF\_A \cdot \theta_{fc} -
                \theta_{wp}} & \; H2OREF\_A \cdot \theta_{fc} \ge
                \theta \ge \theta_{wp} \\
                0 &            \; \theta \le \theta_{wp}
        \end{cases}
        \end{align*}
        \end{minipage}}

    \item Add a second rice crop with similar water management in the second
        half of the year. Check the phenological development of the second crop,
        which is given in the physiology output (dvs\_flush). The crop should
        reach maturity before harvest, which is indicated by dvs\_flush = 1.0.
        Change the \emph{remains} attribute of the harvest event to \emph{0.5}
        and \emph{1.0} and compare the simulated methane emissions during the
        second cropping period of the year. 
    \item Add a drainage event of two weeks (i.e., a drainage event can be
        realized by two flooding events, with the second flooding event
        beginning two weeks after the end of the first flooding event.  one
        month after planting and compare the results of plant biomass and CH$_4$
        emissions to the previous simulations.
\end{enumerate}




\subsection{\label{sec:tutorials:tutorial4:nitrogencycle}Nitrogen cycle}

\paragraph{Contents}

\begin{enumerate}
    \item Add fertilization events (i.e., fertilizer type, amount and way
        of application)
    \item Nitrogen input via atmospheric N-fixation
        \begin{itemize}
            \item Introduce a nitrogen-fixing catch crop in the cropping rotation.
            \item Evaluate the effect of photosynthetic active aquatic biomass.
        \end{itemize}
    \item Emissions of trace gases (e.g., NH$_3$, N$_2$O) and soil organic
        matter.
\end{enumerate}

\paragraph{Tasks}

\begin{enumerate}
    \item Consider the baseline simulation from the previous paragraph with two
        continuously flooded unfertilized rice crops.
    \item Add one or more uread-based fertilization event for each cropping
        period (e.g., 100 kg N-urea fertilizer). Compare the simulated plant
        biomass?
\begin{lstlisting}
<event  type="fertilize" time="2007-02-01" >
    <fertilize  type="urea" amount="100.0" depth="0.0" />
</event>
\end{lstlisting}          
        You can also change the depth of the applied fertilizer and evaluate the
        effect on trace gas emissions. By default, the fertilizer application
        depth is assumed to be 0.0 (surface application).
    \item Change the fertilizer type to \emph{NH4NO3} and compare not only the
        biomass but also the emissions of NH$_3$, N$_2$O and N$_2$ (see also
        ~\ref{sec:inputs:event:fertilize}).
    \item Add a nitrogen-fixing catch crop (e.g., mungbean) between the two cropping
        seasons and plow the whole crop into the soil after harvest. How much
        can the amount of fertilizer be reduced without yield penalty? (see also
        ~\ref{sec:inputs:species})
\begin{lstlisting}
<event  type="plant" time="2007-05-12">
<plant name="mungbean" group="crop" type="mungbean" >
    <crop initialbiomass="50.0" covercrop="yes" ></crop>
</plant>
</event>
<event  type="harvest" time="2007-06-30">
    <harvest name="mungbean" />
</event>        
\end{lstlisting}
If you set the planting event attribute \emph{covercrop = true} the complete
        biomass including the fruit remains on the field.
    \item Set the \metrx model option \emph{algae} to \emph{true} and run the 
        simulation without catch crop (see also ~\ref{sec:inputs:setup}). 
        Compare the simulated biomass of both simulations and discuss the effect
        on simulated CH$_4$ emissions. 
\begin{lstlisting}
<module id="soilchemistry:metrx" timemode="subdaily">
    <options algae="yes" wetland="yes" />
</module>       
\end{lstlisting}        
    \item Run three 10-yr simulations and compare/discuss the nitrogen balances of
        those simulations on an annual basis. Set the \metrx model option \emph{algae} to
        \emph{true} for each simulation.
        \begin{itemize}
            \item Two fertilized rice crops per year
            \item Two non-fertilized rice crops per year
            \item Two non-fertilized rice crops and one catch crop
        \end{itemize}
\end{enumerate}

