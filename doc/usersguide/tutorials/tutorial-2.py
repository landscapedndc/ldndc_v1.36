
import math
import numpy as np
import sys
from tabulate import tabulate
import subprocess

sand = [6.4,  4.9,  3.5,  1.0,  3.1,  7.0,  3.7,  4.0,  2.5,  1.6]
clay = [58.5, 59.2, 55.2, 60.5, 57.2, 55.6, 53.0, 54.0, 54.3, 63.4]
corg = [5.81, 2.04, 1.02, 1.60, 0.71, 0.49, 0.40, 0.37, 0.39, 1.89]
bulk = [0.82, 1.17, 1.35, 1.19, 1.38, 1.44, 1.48, 1.50, 1.54, 1.1]

# clay (%)
# sand (%)
def get_sks_cosby( _sand, _clay) :
    #multiplicative conversion factor from (cm day-1) to (cm min-1)
    cm_d_to_cm_min = 1.0 / (60.0 * 24.0)

    return round((60.96 * 10.0**(-0.6 + 0.0126 * _sand - 0.0064 * _clay)) * cm_d_to_cm_min, 5)

# clay (%)
# sand (%)
def get_sks_saxton( _sand, _clay) :
    #multiplicative conversion factor from (cm day-1) to (cm min-1)
    cm_d_to_cm_min = 1.0 / (60.0 * 24.0)

    return round( 24.0 * math.exp( 12.012-7.55*10**(-2)*_sand + \
            (-3.895+3.671*10**(-2)*_sand-0.1103*_clay+8.7546*10**(-4)*_clay**2) / \
            (0.332-7.251*10**(-4)*_sand+0.1276*np.log10(_clay)))
            * cm_d_to_cm_min, 5)

# clay (%)
# sand (%)
def get_sks_vereecken( _sand, _clay, _som, _bd) :
    #multiplicative conversion factor from (cm day-1) to (cm min-1)
    cm_d_to_cm_min = 1.0 / (60.0 * 24.0)

    return round(math.exp(20.62 - \
                          0.96*np.log(_clay) - \
                          0.66*np.log(_sand) - \
                          0.46*np.log(_som) - \
                          8.43*_bd) * cm_d_to_cm_min, 5)

# corg (%)
# clay (%)
# sand (%)
# bd (g cm-3)  
def get_hydraulic_properties2( _d):
    corg = _d['corg'] 
    clay = _d['clay']
    sand = _d['sand']
    bd   = _d['bd']
    poro = 0.4
    om = corg * 2.0
    hb = math.exp(5.3396738 + 0.1845038 * clay - 2.48394546 * poro \
         - 0.00213853 * clay**2.0 - 0.04356349 * sand * poro - 0.61745089 * clay * poro \
         + 0.00143598 * sand**2.0 * poro**2.0 - 0.00855375 * clay**2.0 * poro**2.0 \
         - 1.282e-5 * sand**2.0 * clay + 0.00895359 * clay**2.0 * poro \
         - 7.2472e-4 * sand**2.0 + poro + 5.4e-6 * clay**2.0 * sand + 0.5002806 * poro**2.0 * clay)
    lamb = math.exp( 0.7842831 + 0.0177544 * sand - 1.062498 * poro - 5.304e-5 * sand**2.0 \
         - 0.00273493 * clay**2.0 + 1.11134946 * poro**2.0 - 0.03088295 * sand * poro \
         + 2.6587e-4 * sand**2.0 * poro**2.0 - 0.00610522 * clay**2.0 * poro**2.0 \
         - 2.35e-6 * sand**2.0 * clay + 0.00798746 * clay**2.0 * poro \
         - 0.00674491 * poro**2.0 * clay)
    n_star = -25.23 - 0.02195 * clay + 0.0074 * sand - 0.1940 * om + 45.5 * bd - 7.24 * bd**2.0 \
            + 0.0003658 * clay**2.0 + 0.002885 * om**2.0 -12.81 / bd \
            - 0.1524 / sand - 0.01958 / om - 0.2876 * np.log(sand) - 0.0709 * np.log(om) \
            - 44.6 * np.log(bd) - 0.02264 * bd * clay + 0.0896 * bd * om + 0.00718 * clay
    return math.exp(n_star) + 1
    #return lamb + 1

# corg (%)
# clay (%)
# sand (%)
# bd (g cm-3)  
def get_hydraulic_properties( _d):
    corg = _d['corg'] 
    clay = _d['clay']
    sand = _d['sand']
    bd   = _d['bd']

    theta_r = 0.015 + 0.005 * clay + 0.014 * corg
    theta_s = 0.81 - 0.283 * bd + 0.001 * clay

    log_alpha = -2.486 + 0.025 * sand - 0.351 * corg - 2.617   * bd - 0.023 * clay
    log_n     =  0.053 - 0.009 * sand - 0.013 * clay + 0.00015 * (sand**2.0)

    try:
        vg_alpha = math.exp(log_alpha)
    except:
        print( _d)
    vg_n = math.exp(log_n)
    vg_m = 1.0 

    field_capacity = theta_r + (theta_s - theta_r) / math.pow( ( 1.0 + math.pow ( vg_alpha * 100.0, vg_n ) ), vg_m )  
    wilting_point = theta_r + (theta_s - theta_r) / math.pow( ( 1.0 + math.pow ( vg_alpha * 15800.0, vg_n ) ), vg_m )

    d_out = dict( {'theta_r': theta_r, 'theta_s': theta_s, 'vg_alpha': vg_alpha, 'vg_n': vg_n, 
                   'field_capacity': round(field_capacity * 100.0, 0), 
                   'wilting_point': round(wilting_point * 100.0, 0)})
    #print 'corg ', corg, '  clay ', clay, '  sand ', sand, '  bd ', bd, '  vg_n ', vg_n, 'n ', get_hydraulic_properties2( _d)
    return d_out


sks_cosby = []
sks_saxton = []
sks_vereecken = []
vg_n = []
vg_alpha = []
theta_r = []
theta_s = []
field_capacity = []
wilting_point = []

for co, sa, cl, bd in zip(corg, sand, clay, bulk) :
    sks_cosby.append( get_sks_cosby( sa, cl))
    sks_saxton.append( get_sks_saxton( sa, cl))
    sks_vereecken.append( get_sks_vereecken( sa, cl, co*2.0, bd))

    hp = get_hydraulic_properties( dict({'corg': co, 'clay': cl, 'sand': sa, 'bd': bd}))
    vg_n.append( hp['vg_n'])
    vg_alpha.append( hp['vg_alpha'])
    theta_r.append( hp['theta_r'])
    theta_s.append( hp['theta_s'])
    #convert to mm:m-1
    field_capacity.append( hp['field_capacity']*10)
    wilting_point.append( hp['wilting_point']*10)

#print('sks [cm:min-1]')
#print(sks_cosby)
#print('field_capacity [mm:m-1]')
#print(field_capacity)
#print('wilting_point [mm:m-1]')
#print(wilting_point)
#
#exit()
#Saturated hydraulic conductivity
table_out = open("./build/sks.tex", "w")
table_out.write('''\\begin{table}[H]
\centering
\caption{\label{tab:tutorials:tutorial2:solution:sks}
         Estimated saturated hydraulic conductivities 
         based on the methods after \cite{cosby:1984a},
         \cite{saxton:1986a} and \cite{vereecken:1990a}.}\n''')
table_out.write( tabulate([[i+1,cosby, saxton, vereecken] for i, cosby, saxton, vereecken in zip(range(len(sks_cosby)), sks_cosby, sks_saxton, sks_vereecken)], 
headers=['no.','Cosby', 'Saxton', 'Vereecken'], tablefmt="latex"))    
table_out.write('\n\end{table}\n')
table_out.close()

#Van Genuchten parameters
table_out = open("./build/vg.tex", "w")
table_out.write('''\\begin{table}[H]
\centering
\caption{\label{tab:tutorials:tutorial2:solution:vg}
         Estimated van Genuchten 
         parameters after \cite{agboden:1999a}.}\n''')
table_out.write( tabulate([[i+1, t_r, t_s, n, a] for i, t_r, t_s, n, a in zip(range(len(theta_r)), theta_r, theta_s, vg_n, vg_alpha)], 
headers=['no.', 'toggle1', 'toggle2', 'n', 'toggle3'], tablefmt="latex"))    
table_out.write('\n\end{table}\n')
table_out.close()
subprocess.call(["sed", "-i", "-e",  "s/toggle1/$\\\\theta_r$/g", "./build/vg.tex"])
subprocess.call(["sed", "-i", "-e",  "s/toggle2/$\\\\theta_s$/g", "./build/vg.tex"])
subprocess.call(["sed", "-i", "-e",  "s/toggle3/$\\\\alpha$/g", "./build/vg.tex"])

#Field capacity and wilting point
table_out = open("./build/fcwp.tex", "w")
table_out.write('''\\begin{table}
\centering
\caption{\label{tab:tutorials:tutorial2:solution:fcwp}
         Estimated field capacity and wilting point after \cite{agboden:1999a}.}\n''')
table_out.write( tabulate([[i+1, fc, wp] for i, fc, wp in zip(range(len(field_capacity)), field_capacity, wilting_point)], 
headers=['no.', 'toggle1', 'toggle2'], tablefmt="latex"))    
table_out.write('\n\end{table}\n')
table_out.close()
subprocess.call(["sed", "-i", "-e",  "s/toggle1/$\\\\gamma_{\\\\text{max}}$/g", "./build/fcwp.tex"])
subprocess.call(["sed", "-i", "-e",  "s/toggle2/$\\\\gamma_{\\\\text{min}}$/g", "./build/fcwp.tex"])
