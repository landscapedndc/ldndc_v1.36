
%do not show subsubsections in Contents
%\addtocontents{toc}{\protect\setcounter{tocdepth}{1}}


\section{\label{sec:tutorials:tutorial2}Tutorial II: Accurate soil input description}

The purpose of this tutorial is to familiarise the user with the creation of a
site file from a realistic soil identification. The site considered here,
Graswang, is an extensive grassland system located in the Ammer catchment in
southern Germany (11$^{\circ}$03'E, 47$^{\circ}$57'N, 870 m a.s.l.).
The soil profile including soil texture measurements are presented in 
Figure~\ref{fig:soilprofile-graswang} and Table ~\ref{tab:soilprofile-graswang}.

\begin{figure}[h]
    \begin{center}
    \fbox{\includegraphics[scale=0.3]{tutorials/figures/soilprofile-graswang.png}}
        \caption{\label{fig:soilprofile-graswang}Soil profile of the Graswang
        site with carved identified soil horizons. Photo: J. Kuehn (2011),
        University of Hohenheim.}
    \end{center}
\end{figure}


\begin{table}[H]
 \centering
 \caption{\label{tab:soilprofile-graswang}Soil texture of the Graswang site. 
          \textbf{lb}: Cumulative depth of lower boundary [cm]; 
          \textbf{sc}: Stone content [\%];
          \textbf{clay}: Clay content [\%]; \textbf{sand}: Sand content [\%];
          \textbf{c$_{\text{org.}}$}: Organic carbon [\%];
          \textbf{n$_{\text{tot.}}$}: Total nitrogen [\%]; 
          \textbf{$\rho$}: Bulk density [kg dm$^{-3}$]; \textbf{pH}: [-].}

  \begin{tabular}{r|c|c|c|c|c|c|c|c|c}
    \textbf{no.} & 
    \textbf{lb} &
    \textbf{sc} &
    \textbf{clay} &
    \textbf{sand} &
    \textbf{c$_{\text{org.}}$} & 
    \textbf{n$_{\text{tot.}}$} & 
    \textbf{$\rho$} & 
    \textbf{pH} & 
    \textbf{name} \\
 
    \hline
 
    %1  & 5.0   & 1.0 & -   & -   & -    & -    & -    & -   & aeAh1 \\
    %2  & 16.0  & 1.5 &58.5 &6.4 & 5.81 & 0.67 & 0.82 & 7.1  & aeAh2 \\
    1  & 21.0  & 1.5 &58.5 &6.4 & 5.81 & 0.67 & 0.82 & 7.1  & aeAh \\
    2  & 37.0  & 0.0 &59.2 &4.9 & 2.04 & 0.25 & 1.17 & 7.6 & aeM \\
    3  & 46.0  & 0.0 &55.2 &3.5 & 1.02 & 0.12 & 1.35 & 7.5 & Sw-aeM1 \\
    4  & 53.0  & 0.0 &60.5 &1.0 & 1.60 & 0.18 & 1.19 & 7.4 & Sw-aeM1 \\
    5  & 72.0  & 0.0 &57.2 &3.1 & 0.71 & 0.08 & 1.38 & 7.5 & IIaelC1 \\
    6  & 86.0  & 0.0 &55.6 &7.0 & 0.49 & 0.06 & 1.44 & 7.6 & aelC2 \\
    7  & 109.0 & 0.0 &53.0 &3.7 & 0.40 & 0.05 & 1.48 & 7.6 & aelC3 \\
    8  & 124.0 & 0.0 &54.0 &4.0 & 0.37 & 0.03 & 1.50 & 7.6 & aelC-Swd \\
    9 & 143.0 & 0.0 &54.3 &2.5 & 0.39 & 0.04 & 1.54 & 7.7 & aeSd \\
    10 & 155.0 & 0.0 &63.4 &1.6 & 1.89 & 0.15 & 1.10 & 7.5 & IIIaeolC-Sd1 \\
 
  \end{tabular}

\end{table} 

\subsection{\label{sec:tutorials:tutorial2:sks}Determination of saturated hydraulic conductivity}

The saturated hydraulic conductivity determines a saturated soil's ability to
transmit water along a hydraulic gradient and can be estimated by pedotransfer
functions such as given by \cite{cosby:1984a}:

 \begin {align}
  k_f = 60.96 \; \cdot \; 10^{-0.6 + 0.0126 \; \cdot \; \text{sand} - 0.0064 \; \cdot \; \text{clay}}
 \end {align}
 $k_f$: Saturated hydraulic conductivity $\left[\frac{\text{cm}}{\text{day}}\right]$ \\
 clay: Clay content (<2$\mu$m) [mass \%] \\
 sand: Sand content (50-2000$\mu$m) [mass \%]

% \item Method after \cite{saxton:1986a}:
% \begin {align}
%  \begin{split}
%   k_f & = 24.0 \cdot \text{exp} \left ( 12.012 - 7.55 \cdot 10^{-2} \cdot \text{sand} \right . \\
%       & + \left . \frac{-3.895 + 3.671 \cdot 10^{-2} \cdot \text{sand} -
%         0.1103 \cdot \text{clay} + 8.7546 \cdot 10^{-4} \cdot \text{clay}^2}
%         {0.332 - 7.251 \cdot 10^{-4} \cdot \text{sand}
%         + 0.1276 \cdot \text{log}_{10}(\text{clay})} \right )
%  \end{split}
% \end {align}
% $k_f$: Saturated hydraulic conductivity $\left[\frac{\text{cm}}{\text{d}}\right]$ \\
% clay: Clay content (<2$\mu$m) [mass \%] \\
% sand: Sand content (50-2000$\mu$m) [mass \%]
%
% \item Method after \cite{vereecken:1990a}:
% \begin {align}
%   k_f = \text{exp}  ( 20.62 - 0.96 \cdot \text{ln} (\text{clay} ) 
%                                    - 0.66 \cdot \text{ln} ( \text{sand} )
%                                    - 0.46 \cdot \text{ln} ( \text{som} )
%                                    - 8.43 \cdot \rho )
% \end {align}
% $k_f$: Saturated hydraulic conductivity $\left[\frac{\text{cm}}{\text{d}}\right]$ \\
% clay: Clay content (<2$\mu$m) [mass \%] \\
% sand: Sand content (50-2000$\mu$m) [mass \%] \\
% som: Soil organic matter [mass \%] \\
% $\rho$: Bulk density [g cm$^{-3}$] 
%
%\end{enumerate}

Use the above outlined approach and calculate the saturated hydraulic
conductivity for each soil layer of the Graswang site. Make sure you use the
right unit, e.g., in the above formula $k_f$ is given
$\left[\frac{\text{cm}}{\text{day}}\right]$, however, \ldndc\ expects
$\left[\frac{\text{cm}}{\text{min}}\right]$.

%Note that soil organic matter can be calculated from soil organic carbon by
%assuming that it contains approx. 50\% carbon.
%Solution table of estimated saturated hydraulic conductivities are provided in
%~\ref{sec:tutorials:tutorial2:solution:sks}.



\subsection{\label{sec:tutorials:tutorial2:fieldcapacity}Determination of field
capacity, wilting point and van Genuchten parameters}

Soil water content and movement depend on several forces. Besides gravity, soil
adhesion and capillarity are important determining factors that are commonly
summarized by effective macroscopic quantities. Within \ldndc the effective
macroscopic quantities soil water content at wilting point and field capacity as
well as the van Genuchten parametrization of the soil water retention curve can
be defined.  Table ~\ref{tab:soilprofile-graswang} includes measured soil
characteristics of the Graswang site. Based thereon, van Genuchten parameters
can be derived, which can then be used to estimate further hydraulic properties
such as field capacity and wilting point \citep{vereecken:1990a, agboden:1999a}.

\fbox{\begin{minipage}{\textwidth}
  The van Genuchten equation describes soil matrix
  potential at the macroscale $\Psi$ depending on soil water content $\theta$
  \citep{vangenuchten:1980a}:
  
  \begin{align*} 
  \label{eqn: Van Genuchten} 
  \Psi (\theta_e) = \frac{1}{\alpha}\left[ \theta_e^{-\frac{1}{m}} \;
                   - 1 \right]^ {\frac{1}{n}} 
  \end{align*}
  
  with the effective soil water content 
  
  \begin{align*} 
  \theta_e = \frac{\theta - \theta_r}{\theta_s - \theta_r} \;.  
  \end{align*}
  
  $\theta_r$: Residual soil water $[\frac{m^3}{m^3}]$ \\
  $\theta_s$: Soil water content at saturation $[\frac{m^3}{m^3}]$ \\
  $\Psi$: Capillary pressure: [hPa]
\end{minipage}}

Following \cite{vereecken:1990a, agboden:1999a} the van Genuchten parameters can be
derived by:
\begin {align}
  \theta_r &= 0.015 + 0.005  \cdot \text{clay} + 0.014 \cdot \text{c}_{\text{org.}} \\
  \theta_s &= 0.81 - 0.283 \; \rho + 0.001 \cdot \text{clay} \\
  \alpha &= \text{exp} (-2.486 + 0.025 \cdot \text{sand} - 0.351 \cdot
                        \text{c}_{\text{org.}} - 2.617 \; 
                        \rho - 0.023 \cdot \text{clay}) \\
  \text{n} &= \text{exp} ( 0.053 - 0.009 \cdot \text{sand} - 0.013
                          \cdot \text{clay} + 0.00015 \cdot \text{sand}^2 ) \\
  \text{m} &= 1 
\end {align}
clay: clay content (<2$\mu$m) [mass \%] \\
sand: sand content (50-2000$\mu$m) [mass \%] \\
$\text{c}_{\text{org.}}$: Soil organic carbon [mass \%] \\
$\theta_s$: Soil water content at saturation $[\frac{m^3}{m^3}]$\\
$\rho$: Bulk density [g cm$^{-3}$] 

Note that the parameter m equals 1, which has the consequence that the
relationship between the parameters n and m does not correspond to the
parameterization of the relative permeability after \cite{mualem:1976a}. 

Field capacity and wilting point can now be estimated from the van Genuchten
parameterized soil water retention curve assuming typical matrix potentials of
100 and 15000 [hPa], respectively:

\begin {align}
  \gamma_{\text{max}} &= \theta_r + \frac{\theta_s- \theta_r}
                         {( 1.0 + (100 \cdot \alpha)^
                         {\text{n}})^{\text{m}}} \\
  \gamma_{\text{min}} &= \theta_r + \frac{\theta_s- \theta_r}
                         {( 1.0 + (15000 \cdot \alpha)^
                         {\text{n}})^{\text{m}}}
 \end {align}
 $\gamma_{\text{max}}$: Field capacity $[\frac{m^3}{m^3}]$ \\
 $\gamma_{\text{min}}$: Wilting point $[\frac{m^3}{m^3}]$

Follow the above outlined approaches and calculate the van Genuchten parameters
$\alpha$ and n as well as the values of the field capacity and the wilting point
for each soil layer of the Graswang site. Run two simulations, one without
(leaving the corresponding attributes empty) and one with the calculated soil
parameters.  Compare the simulated soil water content at 10 and 30 cm soil depth
of the two simulations with each other and with the experimental observations
(see Fig.  ~\ref{fig:swc-graswang}). Compare also simulated annual values of
soil water percolation and NO$_3^-$ leaching. Again, make sure you use the right
unit, e.g., in the above formulas wilting point and field capacity are given in 
$\left[\frac{m3}{m3}\right]$, however, \ldndc\ expects
$\left[\frac{mm}{m}\right]$.

\begin{figure}[h]
    \begin{center}
    \fbox{\includegraphics[scale=0.3]{tutorials/figures/DE_graswang_swc.png}}
        \caption{\label{fig:swc-graswang}Observed soil water content of
        the Graswang site in 10cm and 30cm soil depth.}
    \end{center}
\end{figure}

%Solution table of estimated field capacity and wilting point is provided in
%~\ref{sec:tutorials:tutorial2:solution:fcwp}.



%\subsection{\label{sec:tutorials:tutorial2:solution}Solution tutorial II}
%
%\subsubsection{\label{sec:tutorials:tutorial2:solution:sks}Saturated hydraulic conductivity}
%\input{build/sks.tex}
%
%\subsubsection{\label{sec:tutorials:tutorial2:solution:fcwp}Field capacity and wilting point}
%\input{build/fcwp.tex}

