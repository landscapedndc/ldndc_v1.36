% vim: smartindent wrap lw=72

\section{\label{sec:inputs:projectfile}Project file}


The project file (\code{project\_name.ldndc}) can be interpreted as the top-level input. It is written in the XML format: 

\lstset{language=XML}
\begin{lstlisting}
	<ldndcproject>
	.
	.
	.
	</ldndcproject>
\end{lstlisting}

The file contains: 


\lstset{language=XML}
\begin{enumerate}
	\item meta data describing the content of the simulation project 
	\begin{lstlisting}
		<description> ... </description>
	\end{lstlisting}
	\item simulation start time and duration 
	\begin{lstlisting}
		<schedule  ...  />
	\end{lstlisting}
	\item input source definitions (input type + filenames) 
	\begin{lstlisting}
		<inputs> ... </inputs>
	\end{lstlisting}    
	\item output sink definition (output type + filenames)
	\begin{lstlisting}
		<outputs> ... </outputs>
	\end{lstlisting}
	
\end{enumerate}

Some things may be omitted, forcing \ldndc{} to use defaults
unless unavailable in which case it will abort with an error message. 



\subsection{\label{sec:projectfile:version}Target version}

To enforce a \emph{specific} \ldndc\ version or a \emph{minimum} version to use
for the simulation project, set either the attribute
\code{PackageVersionRequired} (specific version) or
\code{PackageMinimumVersionRequired} (minimum version) in the project file's
root element (\code{<ldndcproject target version>}).  The value of these attributes are version strings with the format
\code{a.b.c} where \code{c} or \code{b} and \code{c} can be omitted, \eg the
following are equivalent (same for \code{PackageVersionRequired}):
\begin{itemize}
    \item
        \code{PackageMinimumVersionRequired="1"}
    \item
        \code{PackageMinimumVersionRequired="1.0"}
    \item
        \code{PackageMinimumVersionRequired="1.0.0"}
\end{itemize}
To check what version you are currently using, see the package that you
downloaded (\eg version file) or run \code{bin/ldndc {-}V} on the command line.

% not used internally
\subsection{\label{sec:projectfile:description}Description}

This section allows to set meta information about the project, like
\emph{project name} and \emph{author}. Following the same pattern, any number
of additional meta information may be set. The listed items (name,author,etc.)
are read and may be used in the future (\eg summary sheets).
\lstset{language=XML}
\begin{lstlisting}
<description>
    <name>PROJECT NAME</name>
    <author>AUTHOR</author>
    <email>EMAIL ADDRESS</email>
    <date>CREATION DATE</date>
</description>
\end{lstlisting}

\subsection{\label{sec:projectfile:schedule}Schedule}

A block holding the simulation schedule. Time specification adheres to the
rules described in Sec.~\ref{chap:clock}. Examples:
\lstset{language=XML}
\begin{itemize}
    \item Simulation schedule with daily time resolution between two defined
    datetimes: 
    \begin{lstlisting}
<schedule  time="2006-01-01/1 -> 2008-12-31" />
    \end{lstlisting}
    \item Simulation schedule with hourly time resolution between two defined
    datetimes: 
    \begin{lstlisting}
<schedule  time="2006-01-01/24 -> 2008-12-31" />
    \end{lstlisting}
    \item Simulation schedule with two-hourly time resolution, defined starting
    datetime and simulation lenght of 7 years, 6 months and 2 days:
    \begin{lstlisting}
<schedule  time="2006-01-01/12 -> +7-6-2" />
    \end{lstlisting}    
\end{itemize}

No default available. Even though the schedule can be omitted and
specified on the command line (\ie
\code{{-}{-}schedule="2006-01-01/1->2008-12-31"}) it is not recommended
to do so. Note, that when both are given the schedule read from the
command line has precedence.

\subsection{\label{sec:projectfile:inputs}Input source definition}

Within the \code{input} section the user must provide for each known
input class zero or more input source definitions along with source
attributes (\eg default ID). The XML element name (example \code{<name
/>}) inside the \code{sources} block is forming the \defemph{source
identifier}. This identifier must be unique within the scope of the list
of sources and may be any valid element name not exceeding a size of
\ldndcstringmaxsize\ characters. Usually, the source identifier is
chosen to be the name of the corresponding input class: the
\defemph{default source}. If a non-default source identifier is used it
is required to provide the corresponding input class via the
\code{class} attribute. Where the source
format is different from the class' default format (\eg site input
assumes an XML file by default) it must be explicitly specified via the
\code{format} attribute (\eg \code{txt}). For class name specifiers (\eg
\code{site}) and source format specifiers see the corresponding sections
in Chapter~\ref{sec:inputs:source}.

\label{sec:projectfile:inputs:sources}A source definition example:
\lstset{language=XML}
\begin{lstlisting}
<input>
    <sources  sourceprefix="exampleprefix/" >
        <setup  source="setup.xml" format="xml" />
        <arablesite  class="site" source="site.xml" format="xml" />
        <warmclimate  class="climate" source="climate.txt" format="txt" />
        <event  sourceprefix="eventpool/" source="arable-events.xml" />
    </sources>
</input>
\end{lstlisting}

The \code{source} attribute of each source definition is prefixed with
the value given for the attribute \code{sourceprefix}. This attribute
may be set globally for all source definitions (defaults to empty
string) or locally for each source definition overwriting the global
value (Note, that the prefix is not necessarily representing a directory
path. \Ie a path separator (\eg \code{"/"}) between the source prefix
and the source is neither implied nor automatically inserted).

Also, take into account, that inputs are generally not read until they
are requested by some client object (\eg model requiring climate input).
Failure, to open a requested input source will generally cause \ldndc\
to terminate (optionally, input synthesizers may kick in).

Source definitions and source prefixes are subject to string expansion,
see section~\ref{sec:string:expansion}.

See section~\ref{sec:cf} for information on the input base path.

Additionally, a single set of attributes for each input class may be
provided. For example, the default ID (\code{use}) which is used
whenever a site does not provide an explicit link to an input (see
section~\ref{sec:inputs:source}). Again global defaults may be given,
which can be overwritten on a per input class basis. Find more details
on input linking in section~\ref{sec:inputs:source:aicpool}.

\label{sec:projectfile:inputs:attr}A source attributes example:
\lstset{language=XML}
\begin{lstlisting}
<input>
    <!-- here go the sources -->
    <sources>
      ...
    </sources>

    <attributes  use="0" endless="no" >
        <airchemistry  endless="yes" />
        <siteparameters  use="1" />
    </attributes>
</input>
\end{lstlisting}


\subsection{\label{sec:projectfile:outputs}Outputs}

Within the \defemph{output} section the user may define custom sinks or
reconfigure \ldndc\ internal \defemph{standard sinks} (\eg species
physiological outputs). See section~\ref{sec:outputs:standardsinks} for
a detailed description of (standard) sinks. In a similar way the input
sources are uniquely identified by their source identifier each sink is
uniquely identified by its \defemph{sink identifier}/\defemph{index}
pair. The identifier is given by the XML element name. By default the
sink's index is $0$ but may be set to any positive integer. For indices
greater zero the sinkname (if applicable) will be suffixed with the
index value. A sink definition accepts a sink descriptor (\eg filename)
and a format (\eg txt). In addition, attributes may be provided which
are used depending on the format selected, \eg \code{delimiter}.

\label{sec:projectfile:outputs:sinks}A sink definition example:
\lstset{language=XML}
\begin{lstlisting}
<output>
    <sinks sinkprefix="example/output-" >
        <soilchemistrydaily  sink="stdout" format="txt" delimiter=";" />
        <myspecialsink  sink="bar.txt" format="txt" />
        <myspecialsink  sink="foo.sqlite3" index="2" format="sqlite3" />
        <myspecialsink  sink="!one.sqlite3" index="1" format="sqlite3" />
    </sinks>
</output>
\end{lstlisting}

The example above would cause the standard output for daily
soilchemistry entities to write data columns separated by semicolons to
the screen. Further, two non-standard sinks are linked to files named
\code{output-bar.txt} writing tab-separated data columns into a text
file and \code{output-foo-2.sqlite3} writing columns to a table of a
SQLite3 database. The third version of the \code{myspecialsink} sink
forces the name to be \code{output-one.sqlite3} omitting the index
insertion (Be aware of name conflicts!). Note that, especially files are
only created if they are internally requested. For example, if no model
wishes to write soilchemistry data, this sink will not be attached. In
the event, that directories along the path of an output sink do not
exists, they will be created. Failure, \eg due to missing write
permissions, will cause \ldndc\ to terminate.

Sink prefixes are analogous to source prefixes and are also subject to
string expansion, see section~\ref{sec:string:expansion}.

See section~\ref{sec:cf} for information on the output base path.

\paragraph{Standard streams} The names \code{stdout}, \code{stderr} and
\code{stdlog} are reserved and refer to the process' standard output
channels. For sinks whose sink attribute was set to \code{*} (asterisk)
all data is discarded (standard stream \defemph{nullsink} is such a
sink).

