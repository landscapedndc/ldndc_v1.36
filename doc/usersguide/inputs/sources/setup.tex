% vim: tw=72 wrap smartindent tabstop=4 shiftwidth=4 expandtab

\subsection[Setup]{\label{sec:inputs:setup}Setup\hfill[\code{setup}]}

In this section, we describe all input entities for the input class setup.
Information given in this input class describe geographic location, model
configuration and input selection. The sum of all setup input can be
interpreted as the domain decomposition, \ie the discretization of the region.

\paragraph{List of entities}
\begin{enumerate}
    \item\label{input:setup:ent:id} \defemph{id}
        \entmeta[0]{integer}{-}

        Unique id for a site simulation or one grid cell in a regional
        simulation (see~\ref{sec:inputs:miscellaneous:regional})

    \item\label{input:setup:ent:name} \defemph{name}
        \entmeta[<empty>]{string}{-}

        A human readable name for a site

    \item\label{input:setup:ent:model} \defemph{model}
        \entmeta[mobile]{string}{-}

        The model to apply for the site

    \item\label{input:setup:ent:active} \defemph{active}
        \entmeta[yes]{bool}{-}

        Switch specifying if site is participating in simulation

    \item\label{input:setup:ent:inputs} \defemph{source links}
        \entmeta[(<class>,<default ID>)]{(\lstring,integer) list}{-}

        Input linking: list source identifier and numerical ID of input
        block to use for input class (\eg climate: (warmclimate, 0))
        unless defaults yield correct linking

    \item\label{input:setup:ent:elevation} \defemph{elevation}
        \entmeta[100]{real}{m}

        The site's elevation; use values greater 0.

    \item\label{input:setup:ent:longitude} \defemph{longitude}
        \entmeta[10]{real}{degree}

        The site's longitude; the allowed range for longitudes is
        $[-180,180]$. Note that this entity's nodata value is within
        this range. For this reason, unfortunately longitude $-99.99$
        cannot be used. We kindly ask the user to use a slightly
        "disturbed" value, \eg $-99.990001$.

    \item\label{input:setup:ent:latitude} \defemph{latitude}
        \entmeta[45]{real}{degree}

        The site's latitude; the allowed range for latitudes is
        $[-90,90]$.

    \item\label{input:setup:ent:latitude} \defemph{centroid}
        \entmeta[(0,0,0)]{(real,real,real)}{m}

        The site's centroid, i.e., the $x,y,z \in \mathbb{R}$
        coordinates with respect to some coordinate system.

    \item\label{input:setup:ent:latitude} \defemph{bounding box}
        \entmeta[(0,0,0)]{(real,real,real)}{m}

        The site's bounding box in $\mathbb{R}^3 = (w,h,d)$
        coordinates.

    \item\label{input:setup:ent:area} \defemph{area}
        \entmeta[1]{real}{$\text{m}^2$}

        The site's area. The volume must not be larger than
        the product $w h$, i.e., width and height of the
        bounding box.

    \item\label{input:setup:ent:volume} \defemph{volume}
        \entmeta[1]{real}{$\text{m}^3$}

        The site's volume. The volume must not be larger than
        the volume of the bounding box.

        %% sk:off        \item\label{input:setup:ent:aspect} \defemph{aspect}
        %% sk:off	\entmeta[0]{real}{degree}
        %% sk:off
        %% sk:off                The site's aspect

    \item\label{input:setup:ent:slope} \defemph{slope}
        \entmeta[0]{real}{degree}

        The site's slope

    \item\label{input:setup:ent:zone} \defemph{timezone}
        \entmeta[1]{integer}{-}

        The site's timezone given as the offset in hours from
        Coordinated Universal Time (UTC) ranging from $-11$ to
        $12$

    \item\label{input:setup:ent:modeloptions} \defemph{model/kernel specific options}

        Model specific options

        \begin{itemize}
            \item
                \mobile
                \begin{enumerate}
                    \item\label{input:setup:ent:modeloptions:mobile:modulelist} \defemph{module list}
                        \entmeta{(\lstring,\lstring) list}{-}

                        List of submodels to run during the simulation. The
                        submodels name as well as a timemode
                        (see~\ref{sec:clock:clock}) must be given.

                    \item\label{input:setup:ent:modeloptions:mobile:moduleoptions} \defemph{module options}
                        \entmeta{(string,string) list}{-}

                        Submodels, that is \mobile\ \defemph{modules},
                        can be globally configured via the configuration
                        file or individually by options (key/value
                        pairs) provided in the setup input. These
                        options are passed to the module at
                        instantiation time. For supported options
                        consult the respective module documentation.

                \end{enumerate}
%            \item
%                \cmf
%                \emph{no inputs}
        \end{itemize}

\end{enumerate}


\subsubsection{\label{sec:inputs:setup:formats}Formats}

\paragraph{XML} Example of setup input given as XML input:


%    <!--
%      id .. block id
%      x,y,z .. centroid
%      dx,dy,dz .. bounding box
%    -->

\lstset{language=XML}
\begin{lstlisting}
<?xml version="1.0"?>
<ldndcsetup>
  <setup id="0" name="example name" model="mobile" active="on" >
    <use>
      <climate  source="warmclimate" id="0" />
    </use>
    <location  elevation="161.5" latitude="51.06" longitude="10.54"
     slope="0" timezone="" />
    <topology  x="100" y="50" z="161.5"  area="100.0"
     dx="10.0" dy="10.0" dz="1.5" volume="95.75" />
    <mobile>
      <modulelist>
        <module id="microclimate:canopyecm" timemode="daily" />
        <module id="watercycle:watercycledndc" timemode="daily" >
          <!-- set model options -->
          <options  potentialevaporationmodel="thornthwaite" />
        </module>
        <module id="physiology:arabledndc" />
        <module id="soilchemistry:metrx" />
        <module id="output:watercycle:daily" />
        <module id="output:soilchemistry:daily" >
          <!-- write additional columns -->
          <options  writexyz="true" writearea="true" />
        </module>
        <module id="output:physiology:daily" />
      </modulelist>
    </mobile>
  </setup>

  <setup id="1" name="garmischer acker" model="arabledndc" >
    <use>
      <climate  source="coldclimate" id="1" />
    </use>
    ...
  </setup>
</ldndcsetup>
\end{lstlisting}

And an example for using the grassland farmer:
\lstset{language=XML}
\begin{lstlisting}
<?xml version="1.0" encoding="UTF-8"?>
<ldndcsetup>
    <setup id="0" name="example name" > <location elevation="860.0" latitude="47.5" longitude="11.0" />
   <models>
      <model id="Farmer" /> 
      <model id="_MoBiLE" /> 
   </models>
    <Farmer file="some path/DE_graswang_general.farmer" id="1" >
    <mobile>
        <modulelist>
    </mobile>
    </setup>
</ldndcsetup>
\end{lstlisting}



