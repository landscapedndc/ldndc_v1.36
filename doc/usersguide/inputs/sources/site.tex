% vim: tw=72 wrap smartindent tabstop=4 shiftwidth=4 expandtab

\subsection[Site]{\label{sec:inputs:site}Site\hfill[\code{site}]}

In this section, we describe all input entities for the input class site.
Primarly, site input defines soil properties such as pH value and wilting point 
for a variable number of soil strata. Soil strata can be further
discretized by one or more soil layer that all have these properties. If
properties are not supplied (indicated by the appropriate nodata value)
they are estimated from available information such as humus or soil type
classification. For the purpose of aiding heuristics in the
initialization of soil properties in the models ecosystem
state, \ldndc\ requires the user to specify the land use history of the site and
may specify a few additional characteristics (\eg soil type).

%\ldndc\ allows to define a variable soil depth. This input data is provided as
%soil strata. Each soil strata is defined by a number of properties and it can
%consist of one or more homogeneous layers that all have these properties. If
%properties are not supplied (indicated by the appropriate nodata value) they
%are estimated from available information such as humus or soil type
%classification.

%It is highly recommended to choose the total soil layer depth equal (or higher)
%than the deepest rooting depth indicated in the vegetation settings
%(see~\ref{sec:inputs:event}), otherwise, the water and nutrient balance may be
%corrupted.

Soil characterization may include or exclude stones/rocks depending
on different measurement approaches and/or points of view. While smaller
stones are most likely included in soil samples of, e.g., bulk density,
greater rocks are commonly removed. Within the context of LandscapeDNDC,
all soil input information, with the exception of stone content, has to
be provided for the bulk soil excluding any kind of stones/rocks.

\paragraph{Ecosystem names} Known ecosystem names are \defemph{arable},
\defemph{grassland} and \defemph{forest}.

\paragraph{Humus type names} Known humus type names are \defemph{humus},
\defemph{raw humus}, \defemph{moder} (with variants for beech, birch,
eucalyptus, oak, pine and spruce) and \defemph{mull} (with variants for beech,
birch, eucalyptus, oak, pine, spruce).

\paragraph{Soil type names} Known soil type names are 
\defemph{bedrock}, \defemph{clay}, \defemph{clay loam}, \defemph{loam},
\defemph{loamy sand}, \defemph{organic material}, \defemph{sandy clay},
\defemph{sandy loam}, \defemph{sand}, \defemph{silty clay}, \defemph{silty
loam}, \defemph{silt}, \defemph{silty clay loam} and \defemph{sandy clay loam}.

\paragraph{List of entities}
The following list provides the name and the description of soil entities. The
most common input format for the soil is XML. See 
\ref{sec:inputs:site:formats:xml} for the respective XML specification.

\begin{enumerate}

        \item\label{input:site:ent:watertable} \defemph{watertable}
    	\entmeta[99]{real}{m}

        Annual average water table depth (distance between soil surface
        and ground water table) is set equal to daily water table
        depth. In soil layers below the groundwater table, the porosity
        is assumed water filled. Soil layers above this value may or
        may not be influenced depending on the model assumption.

        \item\label{input:site:ent:usehistory} \defemph{land use history}
	    \entmeta{string}{-}

        Land use history, drives heuristics during initialization of
        the model. Accepts any valid ecosystem name.

        \item\label{input:site:ent:corg05} \defemph{organic carbon content (5cm)}
    	\entmeta[$U_\text{real}$]{real}{$\frac{kg}{kg}$}

        Organic carbon content in 5 cm depth is used to estimate the fraction of
        organic carbon content throughout the soil profile if this is not given
        explicitly as a soil strata property. If both the parameter and
        the specific strata information are missing, the carbon
        distribution is empirically estimated by a default
        function.

        \item\label{input:site:ent:corg30} \defemph{organic carbon content (30cm)}
    	\entmeta[$U_\text{real}$]{real}{$\frac{kg}{kg}$}

        Same as~\ref{input:site:ent:corg05} for 30cm depth.

        \item\label{input:site:ent:humustype} \defemph{humus type}
    	\entmeta{string}{-}

        Humus type characterizes the basic features of the litter layer when
        not given explicitly in the soil strata description (otherwise
        they are not used). Accepts any valid humus type name.

        \item\label{input:site:ent:soiltype} \defemph{soil type}
    	\entmeta{string}{-}

        Soil type characterizes the basic properties of the soil layers
        when these are not provided explicitly in the soil strata
        description. If all values for each soil layers are provided
        (see soil layer section), they are not used. The available
        choices are referring to the relative content of sand, loam,
		and silt in the soil texture. Accepts any valid soil type name
		(see your input format to find soil type names).

        \item\label{input:site:ent:litterheight} \defemph{litter height}
    	\entmeta[$h_H$]{real}{mm}

        For all soil layers within the litter dimension, a number of soil 
        properties, which are not provided (no data value) are set
        considering humus type specific properties. 
                
        NOTE: It is advisable to set a litter layer for all ecosystems
        where leaves are at least temporally covering the ground.
                
        If no value is specified it is assumed to be $0$ which effectively
		eliminates the litter.

        \item\label{input:site:ent:depth} \defemph{soil stratum depth}
        \entmeta[1]{real list}{mm}

        Vertical dimension/thickness of the strata. The depth does not
        define the position in the soil horizon.
                
        \item\label{input:site:ent:split} \defemph{discretization}
	    \entmeta[1]{integer list}{-}

		Specifies the number of soil layers $\lambda_k$
		($\il[k]{1}{s}$, $s$ is number of stratum) that stratum~$k$ is
		internally split into.

		Each stratum is split into layers of equal height. The stratum
		depth~$H_k$ is given in the input for each stratum and used to
		calculate the individual layer height~$h_k$ within stratum~$k$.
		This means the equations,
		$$
		        \Lambda = \sum_{k=1}^{s} \lambda_k
		        \mbox{, \ \ \ }
		        h_k := \frac{H_k}{\lambda_k}
		$$
		yield the number of resulting soil layers~$\Lambda$ for the
		simulation and layer heights~$h_k$ within stratum~$k$.
		Obviously, the depth~$D$ of the total soil column is given by
		$$
		        D = \sum_{k=1}^{s} \lambda_k h_k.
		$$

        \item\label{input:site:ent:bulkdensity} \defemph{bulk density}
	    \entmeta{real list}{$\frac{\text{kg}}{\text{dm}^3}$}

        The bulk density relative to the soil matrix.

        \item\label{input:site:ent:fieldcapacity} \defemph{field capacity}
    	\entmeta{real list}{$\frac{\text{dm}^3}{\text{m}^3}$}
		
        The field capacity relative to the soil matrix.

        \item\label{input:site:ent:wiltingpoint} \defemph{wilting point}
    	\entmeta{real list}{$\frac{\text{dm}^3}{\text{m}^3}$}
		
        The wilting point relative to the soil matrix.

        \item\label{input:site:ent:corg} \defemph{organic carbon content}
    	\entmeta{real list}{$\frac{kg}{kg}$}
		
        The carbon content relative to the soil matrix.

        \item\label{input:site:ent:norg} \defemph{total nitrogen content}
	    \entmeta{real list}{$\frac{kg}{kg}$}
		
        The nitrogen content relative to the soil matrix.

        \item\label{input:site:ent:clay} \defemph{clay content}
    	\entmeta{real list}{$\frac{kg}{kg}$}
		
        The clay content relative to the soil matrix.

        \item\label{input:site:ent:sand} \defemph{sand content}
    	\entmeta{real list}{$\frac{kg}{kg}$}
		
        The sand content relative to the soil matrix.

        \item\label{input:site:ent:silt} \defemph{silt content}
    	\entmeta{real list}{$\frac{kg}{kg}$}
		
        The silt content relative to the soil matrix.

        \item\label{input:site:ent:stonefraction} \defemph{stone fraction}
    	\entmeta{real list}{$\frac{kg}{kg}$}
        
        Volumetric stone fraction.

        \item\label{input:site:ent:ph} \defemph{pH value}
	    \entmeta{real list}{-}

        \item\label{input:site:ent:sks} \defemph{saturated hydraulic conductivity}
	    \entmeta{real list}{$\frac{\text{cm}}{\text{min}}$}
        
        Saturated hydraulic conductivity.

        \item\label{input:site:ent:iron} \defemph{iron content}
    	\entmeta{real list}{$\frac{kg}{kg}$}    
		
        Soil iron content.

        \item\label{input:site:ent:vangenuchten_n} \defemph{VanGenuchten parameter n}
    	\entmeta{real list}{-}

        \item\label{input:site:ent:vangenuchten_alpha} \defemph{VanGenuchten parameter $\alpha$}
    	\entmeta{real list}{1/m}

        \item\label{input:site:ent:temp_init} \defemph{Initial soil temperature}
    	\entmeta{real list}{$^{\circ}$C}

        \item\label{input:site:ent:wc_init} \defemph{Initial soil water content}
            \entmeta{real list}{$\frac{m^3}{m^3}$}

        \item\label{input:site:ent:wc_init} \defemph{Initial NH4 content}
            \entmeta{real list}{$\frac{mg}{dm^3}$}

        \item\label{input:site:ent:wc_init} \defemph{Initial NO3 content}
            \entmeta{real list}{$\frac{mg}{dm^3}$}

        \item\label{input:site:ent:wc_init} \defemph{Initial DON content}
            \entmeta{real list}{$\frac{mg}{dm^3}$}

        \item\label{input:site:ent:wc_init} \defemph{Initial DOC content}
            \entmeta{real list}{$\frac{mg}{dm^3}$}

        \item\label{input:site:ent:wc_init} \defemph{Initial POM content}
            \entmeta{real list}{$\frac{mg}{dm^3}$}

        \item\label{input:site:ent:wc_init} \defemph{Initial AORG content}
            \entmeta{real list}{$\frac{mg}{dm^3}$}
\end{enumerate}

\paragraph{Remark on discretization} Many physical processes in the soil and
their adjacent numerical treatment, like vertical transport of diffusion,
struggle when differences in layer thickness are large. Therefore it is
advisable to use approximately 20 mm layer thickness (not strata depth) for the
top soil (soil parts of large carbon contents, generally the upper 300 mm).
Furthermore, the layer heights should not get smaller with depth.

%To interpret the site file for a German forest site given in Listing 1 it
%describes 8 strata layers. The first strata is the litter layer. It has the
%same thickness than the litter layer height given in the general element of the
%soil section. The following 7 strata layers define the mineral soil below the
%litter layer. The first of them has a thickness of depth=\"40\" and
%height=\"20\", which means the strata layer is 40 mm thick, and its layers are
%20 mm which will result in 2 layers. The next strata layer has depth=\"60\" and
%height=\"20\", such that it is 60mm thick and each layer has a thickness of 20
%mm. Summing up the seven mineral soil strata layers, we have a total soil depth
%of 40 mm + 60 mm + 100 mm + 100 mm + 100 mm + 200 mm + 200 mm = 800 mm. This
%concept is quite different from the Forest-DNDC and the agricultural DNDC which
%both use a fixed soil stratification for the numerical treatment of the
%governing equations. 




\subsubsection{\label{sec:inputs:site:formats}Formats}

\paragraph{\label{sec:inputs:site:formats:xml}XML} Example of site input given as XML input:

\lstset{language=XML}
\begin{lstlisting}
<?xml version="1.0"?>
<ldndcsite>
  <site id="..." >
    <general>
      <misc  watertable="..." />
    </general>
    <soil>
      <general usehistory="..." corg30="..." corg5="..."
       humus="..." soil="..." litterheight="..." />
      <layers>
        <layer  depth="..." split="..." bd="..." clay="..." ph="..."
          scel="..." sks="..." corg="..." norg="..." clay="..."
          sand="..." wcmin="..." wcmax="..." iron="..."
          wfps_max="..." wfps_min="..." porosity="..."
          macropores="..." vangenuchten_n="..." vangenuchten_alpha="..."
          wc_init="..." temp_init="..." nh4_init="..." no3_init="..."
          don_init="..." doc_init="..." pom_init="..." aorg_init="..." 
        />

        <layer  depth="..." split="..." bd="..." clay="..." ph="..."
          scel="..." sks="..." corg="..." norg="..." clay="..."
          sand="..." wcmin="..." wcmax="..." iron="..."
          wfps_max="..." wfps_min="..." porosity="..."
          macropores="..." vangenuchten_n="..." vangenuchten_alpha="..."
          wc_init="..." temp_init="..." nh4_init="..." no3_init="..."
          don_init="..." doc_init="..." pom_init="..." aorg_init="..." 
        />
        <!-- any number of more strata -->
      </layers>
  </site> 
</ldndcsite>
\end{lstlisting}


