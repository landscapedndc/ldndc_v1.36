% vim: tw=72 wrap smartindent tabstop=4 shiftwidth=4 expandtab

\subsection{\label{sec:inputs:event}Event}

In this section, we describe all input entities for the input class event.
Events provide a mechanism to notify models about time triggered actions that
may or may not be handled during simulation. For example, a planting event with
\defemph{time} attribute value April 1st informs the models on the 1st of April
in the first simulation year that a new species entered the simulation. To
perform appropriate actions is now up to the model developer.
For grassland simulation a dynamic management kernel (farmer) exists. If this is applied only a planting event needs to be scheduled in the event file. Planting and manuring are then handled in accordance with the chosen farmer parameters.

All events have in common a \defemph{type} and \defemph{time} attribute. While
the later specifies when the event is to be fired the former specifies what
type of event is to be fired. For time specifications see
section~\ref{chap:clock}. Furthermore, each event is equipped with a set of
type-specific attributes. Hence, the type additionally hints the event reader
what attributes to expect in the input source.

\paragraph{List of entities}
\begin{enumerate}
    \item\label{input:event:ent:readaheadsize} \defemph{Read-ahead size}
        \entmeta[$U_{\text{int}}$]{int}{-}

        (\textit{Not relevant to most users}) This integral value specifies the
        number of events to read during a single data reload \todo{explain
        reload}: events are likely to be read sequentially in the order they
        appear in the input stream (\eg xml file). However, the order of
        appearance may not match the chronological order of their execution.
        For this reason, reading only a subset of the full event input may miss
        an event further down the stream which trigger time will be in the past
        (relative to the simulation clock) at the time of the next reload. In
        effect, this event will be dropped and never dispatched. If events are
        dropped due to a too small read-ahead value, a warning is emitted. You
        may then increase the given value or reorder your input.

        If no value is given the complete input is read at once.

    \item\label{input:event:ent:events} \defemph{Events}

        \textbf{Event types} The complete list of known events
        \defemph{cut},
        %\defemph{defoliate},
        %\defemph{die},
        \defemph{fertilize},
        \defemph{flood},
        \defemph{graze},
        \defemph{harvest},
        \defemph{irrigate},
        %\defemph{luc},
        \defemph{manure},
        \defemph{plant},
        %\defemph{regenerate},
        \defemph{regrow},
        %\defemph{reparameterizespecies},
        \defemph{thin},
        \defemph{throw},
        and
        \defemph{till}.

        Find the list of known events and their attributes below.

\end{enumerate}


\subsubsection{\label{sec:inputs:event:cut}Cut}

Specifies that biomass is cut. This event applies to all living species equally.

\begin{enumerate}
    \item\label{input:event:cut:ent:remains_absolute} \defemph{remains\_absolute}
        \entmeta[$\mathcal{P}$]{real}{$\frac{\text{kg C}}{\text{ha}}$}

        The amount of plant biomass remaining on the field after
        cutting. 

        See also \emph{cut belowground biomass fraction}
        (see~\ref{input:siteparameters:frcutr}) for controlling
        removal of plant biomass.

    \item\label{input:event:cut:ent:remains_relative}\defemph{remains\_relative}
        \entmeta[$\mathcal{P}$]{real}{-}

        The amount of plant biomass remaining on the field after
        cutting as relative fraction between [0-1].

    \item\label{input:event:cut:ent:height} \defemph{height}
        \entmeta{real}{m}

        the stubble height after cutting event

\end{enumerate}

\textbf{Formats - XML}

\lstset{language=XML}
\begin{lstlisting}
<!-- Left 200 kg C ha-1 on the field -->
<event  type="cut" time="2006-03-20" >
  <cut  remains_absolute="200.0" />
</event>

<!-- Left 200 kg C ha-1 on the field -->
<event  type="cut" time="2006-03-20" >
  <cut  remains_relative="0.1" />
</event>

<!-- Cut until 5cm stubble height -->
<event  type="cut" time="2006-03-20" >
  <cut  height="0.05" />
</event>
\end{lstlisting}


\subsubsection{\label{sec:inputs:event:fertilize}Fertilize}

Specifies that fertilizer is applied to the field. Different fertilizer
types are defined in \ldndc.

\begin{enumerate}
    \item\label{input:event:fertilize:ent:type} \defemph{type}
        \entmeta{\lstring}{-}

        The type of fertilizer that is applied. Available fertilizer
        types are \defemph{ammonium bicarbonate}, \defemph{anhydrous
        ammonia}, \defemph{nitrate}, \defemph{diammonium hydrogen
        phosphate}, \defemph{ammonium nitrate}, \defemph{ammonium
        sulphate}, \defemph{sulphate} and \defemph{urea}.

    \item\label{input:event:fertilize:ent:amount} \defemph{amount}
        \entmeta{real}{$\frac{\text{kg N}}{\text{ha}}$}

        The amount of fertilizer to apply.

    \item\label{input:event:fertilize:ent:depth} \defemph{depth}
        \entmeta{real}{m}

        The depth the fertilizer is brought into the soil.

\end{enumerate}

\textbf{Formats - XML}

\lstset{language=XML}
\begin{lstlisting}

<!-- fertilizer type names used to select one of the previously
documented types:
ammonium bicarbonate:  "nh4hco3"
anhydrous ammonia:  "nh3"
nitrate:  "no3"
diammonium hydrogen phosphate:  "nh4hpo4"
ammonium nitrate:  "nh4no3"
ammonium sulphate:  "nh4so4"
sulphate:  "so4"
urea:  "urea"
-->

<!--   surface fertilizer application -->
<event  type="fertilize" time="2006-04-10" >
  <fertilize  type="nh4no3" amount="31.5" />
</event>

<!--   fertilizer injected in 5cm soil depth -->
<event  type="fertilize" time="2006-04-10" >
  <fertilize  type="nh4no3" amount="31.5" depth="0.05" />
</event>

<!--   sequence of fertilizer application for one month in each timestep -->
<event  type="fertilize" time="2006-04-10 -> +1-0" >
  <fertilize  type="nh4no3" amount="1.5" depth="0.01" />
</event>
\end{lstlisting}


\subsubsection{\label{sec:inputs:event:flood}Flood}

\begin{enumerate}
    \item\label{input:event:flood:ent:watertable} \defemph{watertable}
        \entmeta{real}{mm}

        Water table height above soil surface

    \item\label{input:event:flood:ent:bundheight} \defemph{bundheight}
        \entmeta{real}{mm}

        Height of a surrounding wall of a bund preventing runoff

    \item\label{input:event:flood:ent:percolationrate} \defemph{percolationrate}
        \entmeta{real}{mm}

        Maximum allowed percolation rate from deepest soil layer

    \item\label{input:event:flood:ent:irrigationheight} \defemph{irrigationheight}
        \entmeta{real}{mm}

        Target height of watertable for a single irrigation event as soon
        as watertable drops below threshold (above defined watertable).

\end{enumerate}

\textbf{Formats - XML}

\lstset{language=XML}
\begin{lstlisting}
<!-- flood event with constant water table of 10cm -->
<event  type="flood" time="2006-10-30->2006-09-30" >
  <flood  watertable="100.0" bundheight="200.0" />
</event>

<!-- irrigation triggered when watertable drops 15cm below soil surface -->
<event  type="flood" time="2006-10-30->2006-09-30" >
  <flood  watertable="-150.0" irrigationheight="50.0" bundheight="200.0" />
</event>

<!-- preventing runoff no water added  -->
<event  type="flood" time="2006-10-30->2006-09-30" >
  <flood  bundheight="200.0" />
</event>
\end{lstlisting}


\subsubsection{\label{sec:inputs:event:graze}Graze}

\begin{enumerate}
    \item\label{input:event:graze:ent:name} \defemph{livestock name}
        \entmeta{\lstring}{NONE}

        Name of a predefined livestock. Predefined livestock names are
        \defemph{cattle}, \defemph{horse} and \defemph{sheep}. If a
        livestock name is given type-specific defaults are used. Such
        defaults are hardwired in the simulation system but may be
        overwritten (see the table below for those parameters).

        If no livestock name is given, the parameterization is read
        from input (see items
        \ref{input:event:graze:ent:demandcarbon}--\ref{input:event:graze:ent:urinenitrogen})
        below.

    \item\label{input:event:graze:ent:headcount} \defemph{head count}
        \entmeta{real}{-}

        Number of livestock entities per hectar on the field.

    \item\label{input:event:graze:ent:hours} \defemph{hours}
        \entmeta{real}{h}

        Number of hours the livestock is on the field grazing.

    \item\label{input:event:graze:ent:demandcarbon} \defemph{demand carbon}
        \entmeta{real}{$\frac{\text{kg C}}{\text{d head}}$}

        Demand of carbon: the potential amount of carbon consumed by one entity livestock.

    \item\label{input:event:graze:ent:dungcarbon} \defemph{dung carbon}
        \entmeta{real}{$\frac{\text{kg C}}{\text{d head}}$}

        ?

    \item\label{input:event:graze:ent:dungnitrogen} \defemph{dung nitrogen}
        \entmeta{real}{$\frac{\text{kg C}}{\text{d head}}$}

        ?

    \item\label{input:event:graze:ent:urinenitrogen} \defemph{urine nitrogen}
        \entmeta{real}{$\frac{\text{kg C}}{\text{d head}}$}

        ?

\end{enumerate}

\textbf{Parameterization of predefined livestock types}
\begin{center}
    \begin{tabular}{r|c|c|c|c}
        \textbf{livestock name} & \textbf{demand C} & \textbf{dung C} & \textbf{dung N} & \textbf{urine N} \\

        \hline

        cattle & 4.6 & 1.44 & 0.064 & 0.096 \\
        horse & 3.0 & 1.35 & 0.044 & 0.066 \\
        sheep & 0.88 & 0.277 & 0.01 & 0.015

    \end{tabular}
\end{center}

\textbf{Formats - XML}

\lstset{language=XML}
\begin{lstlisting}
<!-- graze -->
<!-- livestock type names used to select one of the previously
documented predefined types (give zero (i.e. customized animal) or
one type)
cattle:  "cattle"
horse:  "horse"
sheep:  "sheep"
-->
<event  type="graze" time="2006-05-05 -> 2006-05-11" >
  <graze  livestock="cattle" headcount="28" grazinghours="9.5" />
</event>
<event  type="graze" time="2006-05-05 -> +7" >
  <graze  headcount="28" grazinghours="9.5" demandcarbon="4.6"
  dungcarbon="1.44" dungnitrogen="0.064" urinenitrogen="0.096" />
</event>
\end{lstlisting}


\subsubsection{\label{sec:inputs:event:harvest}Harvest}

Specifies that a previously planted species is going to be removed from
the simulation. In the timestep immediately following the timestep a
harvest event occured the species is finally evicted from the
simulation, which means internal structures representing the species
have vanished.

Note, that harvesting a species which was not planted causes the
simulation to terminate prematurely.
\begin{enumerate}
    \item\label{input:event:harvest:ent:name} \defemph{species name}
        \entmeta{\lstring}{-}

        Specifies the name (\ie unique identification) of the species
        to be harvested. Species names are case-insensitive and leading
        and trailing whitespaces are removed

    \item\label{input:event:harvest:ent:remains} \defemph{remains}
        \entmeta{real}{\%} 

        The fraction of the aboveground biomass that is left on the
        field as residuals (\emph{remains} and \emph{stubbleheight} are
        mutually exclusive)

    \item\label{input:event:harvest:ent:stubbleheight} \defemph{stubbleheight}
        \entmeta{real}{m}

        Height of stubble left on the field as residuals.
        (\emph{stubbleheight} and \emph{remains} are mutually
        exclusive)

\end{enumerate}

\textbf{Formats - XML}

\lstset{language=XML}
\begin{lstlisting}
<!--   harvest wheat -->
<event  type="harvest" time="2006-10-27" >
  <harvest  name="powerwheat" remains="0.7" />
</event>

<!--   harvest yummyrice -->
<event  type="harvest" time="2006-09-01" >
  <harvest  name="yummyrice" stubbleheight="0.3" />
</event>
\end{lstlisting}


\subsubsection{\label{sec:inputs:event:irrigate}Irrigate}
\begin{enumerate}
    \item\label{input:event:irrigate:ent:amount} \defemph{amount}
        \entmeta{real}{mm}

        amount of water
\end{enumerate}

\textbf{Formats - XML}

\lstset{language=XML}
\begin{lstlisting}
<!-- irrigate event -->
<event  type="irrigate" time="2006-04-30" >
  <irrigate  amount="4" />
</event>
\end{lstlisting}


\subsubsection{\label{sec:inputs:event:manure}Manure}
\begin{enumerate}
    \item\label{input:event:manure:ent:type} \defemph{manure type}
        \entmeta{\lstring}{-}

        The name of the type of manure to apply. Available choices of
        manure types are \defemph{beancake}, \defemph{compost},
        \defemph{farmyard}, \defemph{green}, \defemph{straw} and
        \defemph{slurry}

    \item\label{input:event:manure:ent:c} \defemph{carbon}
        \entmeta{real}{$\frac{\text{kg C}}{\text{ha}}$}

        The amount of total carbon

    \item\label{input:event:manure:ent:cn} \defemph{carbon/nitrogen ratio}
        \entmeta{real}{-}

        The carbon/nitrogen ratio

    \item\label{input:event:manure:ent:availc} \defemph{available carbon}
        \entmeta{real}{\%}

        The amount of dissolved organic carbon (does not apply to all manure types)

    \item\label{input:event:manure:ent:availn} \defemph{available nitrogen}
        \entmeta{real}{\%}

        The amount of dissolved organic and inorganic nitrogen (does not apply to all manure types)
\end{enumerate}

\textbf{Formats - XML}

\lstset{language=XML}
\begin{lstlisting}
<!-- manure event -->
<!-- manure type names used to select one of the previously
documented types:  
beancake:  "beancake"
compost:  "compost"
farmyard:  "farmyard"
green manure:  "green"
straw:  "straw"
slurry:  "slurry"
-->
<event  type="manure" time="2006-04-28" >
  <manure  type="slurry" c="100" cn="12" availc="72" availn="6"  />
</event>
\end{lstlisting}

\subsubsection{\label{sec:inputs:event:plant}Plant}

Specifies that a species is to be added to the simulation. 
\begin{enumerate}
    \item\label{input:event:plant:ent:type} \defemph{type}
        \entmeta{\lstring}{-}

        The type specifies the parameter block to use from the species
        parameter input (see~\ref{sec:inputs:speciesparameters}). Type
        names are case-insensitive and leading and trailing whitespaces
        are removed.

    \item\label{input:event:plant:ent:name} \defemph{name}
        \entmeta[<type>]{\lstring}{-}

        The name assigns a unique identifier to the species which can
        further be used to relate to this species (\eg see harvest
        event). If no name is given the type is used instead. Species
        names are case-insensitive and leading and trailing whitespaces
        are removed.

    \item\label{input:event:plant:ent:name} \defemph{species resource name}
        \entmeta[<type>]{\lstring}{-}

        The resource name is an optional property that may be used
        by some models to identify species-related resources
        (\eg files). This string is not modified (\eg no
        uppercasing) and read as-is.

    \item\label{input:event:plant:ent:fractionalcover} \defemph{fractionalcover}
        \entmeta{real}{\%}

        Specifies how much of the area is covered by the species.

    \item\label{input:event:plant:ent:initialbiomass} \defemph{initialbiomass}
        \entmeta{real}{$\frac{\text{kg}}{\text{ha}}$}

        Total aboveground biomass

    \item\label{input:event:plant:properties} \defemph{group specific properties}

        For each species a list of group specific properties may be set
        (for a list of valid species groups see
        section~\ref{input:speciesparameters:ent:group}).

        \begin{itemize}
            \item\label{input:event:plant:properties:crop} CROP
                \begin{enumerate}
                    \item\label{input:event:plant:properties:crop:covercrop} \defemph{covercrop}
                        \entmeta{bool}{false}

                        specifies if this crop is a cover crop

                    \item\label{input:event:plant:properties:crop:seedbedduration} \defemph{seedbedduration}
                        \entmeta{int}{days}

                        number of days in seedbed

                    \item\label{input:event:plant:properties:crop:seedlingnumber} \defemph{seedlingnumber}
                        \entmeta{real}{$\frac{1}{\text{ha}}$}

                        number of individuals per ha

                \end{enumerate}

            \item\label{input:event:plant:properties:grass} GRASS
                \begin{enumerate}
                    \item\label{input:event:plant:properties:grass:covercrop} \defemph{covercrop}
                        \entmeta{bool}{false}

                        specifies if this grass is a cover crop

                        %                       \item\label{input:event:plant:properties:grass:heightmin} \defemph{minimum height}
                        %                       \entmeta{real}{m}
                        %
                        %                           height of canopy start (from the ground)

                    \item\label{input:event:plant:properties:grass:heightmax} \defemph{maximum height}
                        \entmeta{real}{m}

                        average height of highest vegetation cohort

                    \item\label{input:event:plant:properties:grass:rootingdepth} \defemph{rooting depth}
                        \entmeta{real}{m}

                        depth of roots
                \end{enumerate}

            \item\label{input:event:plant:properties:wood} WOOD
                \begin{enumerate}
                    \item\label{input:event:plant:properties:wood:creduc} \defemph{carbon reduction factor}
                        \entmeta{real}{\%}

                        reduction factor for initialization of the nitrogen
                        and basic cation concentration relative to the
                        maximum

                    \item\label{input:event:plant:properties:wood:dbh} \defemph{breast height diameter}
                        \entmeta{real}{m}

                        average individual diameter at 1.3 m height

                    \item\label{input:event:plant:properties:wood:heightmin} \defemph{minimum height}
                        \entmeta{real}{m}

                        height of canopy start (from the ground)

                    \item\label{input:event:plant:properties:wood:heightmax} \defemph{maximum height}
                        \entmeta{real}{m}

                        average height of highest vegetation cohort

                    \item\label{input:event:plant:properties:wood:rootingdepth} \defemph{rooting depth}
                        \entmeta{real}{m}

                        depth of roots

                    \item\label{input:event:plant:properties:wood:number} \defemph{number of individuals}
                        \entmeta{integer}{-}

                        number of individuals per ha

                    \item\label{input:event:plant:properties:wood:volume} \defemph{volume}
                        \entmeta{real}{$\frac{\text{m}^3}{\text{ha}}$}

                        stemwood (or stand) volume
                \end{enumerate}
        \end{itemize}

    \end{enumerate}

\textbf{Formats - XML}

\lstset{language=XML}
\begin{lstlisting}
<!--   seeding a crop -->
<event  type="plant" time="2006-05-20" >
  <plant  name="powerwheat" type="wiwh" longname="WinterWheat" >
    <crop  initialbiomass="10.0" fractionalcover="1.0" covercrop="no" />
    <params>
      <par  name="tlimit" value="8.0" />
    </params>
  </plant>
</event>

<!--   seeding rice crop -->
<event  type="plant" time="2006-01-10" >
  <plant  name="yummyrice" type="sahodulan1" >
    <crop  initialbiomass="10.0" fractionalcover="1.0" covercrop="no" />
  </plant>
</event>

<!--   seeding a grass -->
<event  type="plant" time="2006-05-20" >
  <plant  name="greencarpet" type="perg" >
    <grass  initialbiomass="500.0" fractionalcover="1.0" />
  </plant>
</event>

<!--   putting trees -->
<event  type="plant" time="2006-05-20" >
  <plant  name="hayacomun" type="fasy" >
    <wood dbh="0.41" heightmax="35.0" treenumber="600" />
  </plant>
</event>
\end{lstlisting}


\subsubsection{\label{sec:inputs:event:regrow}Regrow}

Present trees are harvested and new trees are planted.
\begin{enumerate}
    \item\label{input:event:regrow:ent:name} \defemph{species name}
        \entmeta{\lstring}{-}

        Name identifying species to regrow

    \item\label{input:event:regrow:ent:treenumber} \defemph{tree number}
        \entmeta[0]{real}{-}

        Number of individuals to be newly set. The attributes
        \defemph{tree number resize factor} and \defemph{tree number}
        are mutually exclusive

    \item\label{input:event:regrow:ent:treenumberresizefactor} \defemph{tree number resize factor}
        \entmeta[0]{real}{-}

        New number of individuals is given by the current number of
        trees multiplied with this factor. The attributes \defemph{tree
        number resize factor} and \defemph{tree number} are mutually
        exclusive

    \item\label{input:event:regrow:ent:exportabovegroundbiomass} \defemph{export biomass}
        \entmeta[1]{real}{\%}

        This factor determines the fraction of harvested aboveground
        biomass that is exported. The other fraction is allocated as
        plant litter to the corresponding soil pools. Default value set
        to 1.0, which means total removal (export) of harvested
        aboveground biomass. Roots always remain in the soil.

\end{enumerate}

\textbf{Formats - XML}

\lstset{language=XML}
\begin{lstlisting}
<event  type="regrow" time="2006-05-13" >
  <regrow  name="hayacomun" treenumberresizefactor="1.3"
  heightmax="0.6" exportabovegroundbiomass="0.9"/>
</event>
\end{lstlisting}


\subsubsection{\label{sec:inputs:event:reparameterize}Reparameterize species}
\begin{enumerate}
    \item\label{input:event:reparameterizespecies:ent:name} \defemph{species name}
        \entmeta{\lstring}{-}

        Name identifying species to reparameterize

    \item\label{input:event:reparameterizespecies:ent:type} \defemph{species type}
        \entmeta{\lstring}{-}

        Type listed in species parameter input used to replace current
        parameter set (see~\ref{sec:inputs:speciesparameters} for
        details)

\end{enumerate}

\textbf{Formats - XML}

\lstset{language=XML}
\begin{lstlisting}
<!-- reparameterization event -->
<event  type="reparameterizespecies" time="2006-04-30" >
  <reparameterizespecies  name="hayacomun" type="fagussylvatica-tortuosa" />
</event>
\end{lstlisting}


\subsubsection{\label{sec:inputs:event:thin}Thin}
\begin{enumerate}
    \item\label{input:event:thin:ent:name} \defemph{species name}
        \entmeta{\lstring}{-}

        Name identifying species to thin

    \item\label{input:event:thin:ent:reductionnumber} \defemph{reduction number}
        \entmeta{real}{\%}

        Number of individuals to be removed

    \item\label{input:event:thin:ent:reductionvolume} \defemph{reduction volume}
        \entmeta{real}{\%}

        Stemwood (or stand) volume to be removed

    \item\label{input:event:thin:ent:exportfoliage} \defemph{export foliage}
        \entmeta{bool}{false}

        Specifies whether foliage is exported or not

    \item\label{input:event:thin:ent:exportsapwood} \defemph{export sapwood}
        \entmeta{bool}{false}

        Specifies whether sapwood is exported or not

    \item\label{input:event:thin:ent:exportcorewood} \defemph{export corewood}
        \entmeta{bool}{false}

        Specifies whether corewood is exported or not

\end{enumerate}

\textbf{Formats - XML}

\lstset{language=XML}
\begin{lstlisting}
<!-- thin event -->
<event  type="thin" time="2007-02-01" >
  <thin  name="poplar" reductionvolume="" reductionnumber=""
  exportfoliage="yes|no" exportsapwood="yes|no"
  exportcorewood="yes|no" />
</event> 
\end{lstlisting}


\subsubsection{\label{sec:inputs:event:throw}Throw}
\begin{enumerate}
    \item\label{input:event:throw:ent:name} \defemph{species name}
        \entmeta{\lstring}{-}

        Name identifying species subject to windthrow. If
        species name is $\ast$ (asterisk) affected species are
        selected based on some strategy (\eg tallest trees
        first).

    \item\label{input:event:throw:ent:reductionvolume} \defemph{reduction volume}
        \entmeta{real}{\%, m$^3$}

        Specifies reduction factor or absolute amount of stand volume reduction.

        %% sk:??            \item\label{input:event:throw:ent:reason} \defemph{reason}
        %% sk:??            \entmeta{\lstring}{-}
\end{enumerate}

\paragraph{XML} Example of event input given as XML input:

\textbf{Formats - XML}

\lstset{language=XML}
\begin{lstlisting}

<!-- throw event -->
<event  type="throw" time="2007-02-01" >
  <!-- relative reduction -->
  <throw  name="poplar" reductionvolume="0.4" />
</event>

<!-- throw event -->
<event  type="throw" time="2007-02-01" >
  <!-- absolute reduction -->
  <throw  name="poplar" reductionvolume="!1000.0" />
</event>
\end{lstlisting}


\subsubsection{\label{sec:inputs:event:till}Till}
\begin{enumerate}
    \item\label{input:event:till:ent:depth} \defemph{depth}
        \entmeta{real}{m}

        Tilling depth (represents tilling method used)

\end{enumerate}

\textbf{Formats - XML}

\lstset{language=XML}
\begin{lstlisting}
<event  type="till" time="2006-11-01" >
  <till  depth="0.15" />
</event>
\end{lstlisting}



