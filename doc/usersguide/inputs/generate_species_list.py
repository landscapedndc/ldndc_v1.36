groups = ['grass', 'crop', 'wood']
groupnames = {'grass': 'grasses', 'crop': 'crops', 'wood': 'trees'}

out = dict()
for t in groups:
    out.update( {t: open("./inputs/species/"+groupnames[t]+".tex", "w")})
    out[t].write('% vim: tw=72 wrap smartindent tabstop=4 shiftwidth=4 expandtab\n\n\\begin{itemize}\n')

group = 'grass'
f = open("../../resources/parameters-species.xml", "r")
level = 0
wr = False
for l in f.readlines():
    if 'mnemonic="' in l:
        mnemonic = l.split('mnemonic')[1].split('"')[1]
        name = l.split('name')[1].split('"')[1]
        group_tmp = l.split('group')[1].split('"')[1]
        if group_tmp in groups:
            group = group_tmp

        if ':' in mnemonic:        
            mnemonic_clean = mnemonic.split(':')[1]
            if mnemonic_clean != 'any' \
            and mnemonic_clean not in groups \
            and mnemonic_clean not in groupnames.values() \
            and 'default' not in mnemonic_clean:
                out[group].write('\t' * (level+1) +'\\item %s \n' %mnemonic_clean)
                out[group].write('\t' * (level+1) +'\\begin{itemize}\n')                
                level += 1
        else:
            out[group].write('\t' * (level+1) + '\\item\\label{input:species:%s:%s}\n' %(group, mnemonic))
            out[group].write('\t' * (level+1) + '%s\hfill[\\texttt{%s}]\n' %(name, mnemonic.upper()))
            if "/>" not in l:
                wr = True

    if '</species>' in l and level > 0:
        if wr == True:
            wr = False
        else:
            out[group].write('\t' * (level) +'\\end{itemize}\n')
            level -= 1

for t in groups:
    out[t].write('\n\\end{itemize}')
    out[t].close()
