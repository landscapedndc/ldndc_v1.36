% vim: tw=72 wrap smartindent tabstop=4 shiftwidth=4 expandtab

\section{\label{sec:inputs:source}Input sources}

%In this section the \ldndc\ input is described. The set of inputs is
%decomposed into subsets grouping input entities by category. For
%example, a category is \emph{climate} and an entity associated to this
%category is \emph{average temperature}. The categories are refered to as
%\defemph{input classes} according to the source specification in the
%project file discussed in Section~\ref{sec:projectfile:inputs}. Below,
%details for each of them are documented including the class name
%specifier in brackets at the right hand side of the heading. For source
%format specifiers consult the references in the \emph{supported formats}
%paragraph at the end of each input class section.

In this chapter we define each input entity that is used by \ldndc\
during a simulation. Depending on your actual simulation setup, \ie
selection of submodels, input entities may be used or not. Missing data
is synthesized by \ldndc\ based on available data unless it is
unreasonable to imply uninformed defaults. However, we strongly
recommend to provide as much information as you can to drive the models
to ensure meaningful results. 

Definition of entities is first done in an input format independent way,
that is, no details about how it has to appear in some specific source (e.g.
file) is given. The purpose of the following sections is to give the user an
overview of the entities \ldndc\ reads, their meaning, units and data types.


\paragraph{Input entity description} Each input entity is described by a
datatype and unit.  Additionally, a range and default value may be given
if applicable. We use the notation \{\emph{datatype} [\emph{range}],
[\emph{default}], \emph{unit}\} where \emph{range} and
\emph{default} may be omitted.  Datatypes may be tuple, which are
written as $( \text{T}_1, \text{T}_2, \ldots, \text{T}_N)$ for
arbitrary $N$-tuple. See explanation of datatypes below
($U_\text{T}$ denotes undefined value (\ie nodata value) of type T):
\nopagebreak[4]
\begin{center}\begin{tabular}{r|p{75mm}|l}
        \textbf{data type, set} & \textbf{meaning (example)} & \textbf{nodata value} \cr
        \hline

        bool, $\insetB$  &  truth values (true, false)  &  \emph{none exists} \\[2mm]

        string  &  arbitrary sized character sequences (``spruce'')  &  ``NONE'' \cr
        string<$N$>  &  character sequence with maximum size $N$, $N\ge4$  &  ``NONE'' \\[2mm]

        integer, $\insetZ$  &  integer number ($-1,0,3$)  &  $-9999$ \cr
        unsigned integer, $\insetN$  &  positive integer number ($1,0,3$)  &  $-1$ \cr
        real, $\insetR$  &  floating point number ($3.141$)  &  $-99.99$ \\[2mm]

        $(\text{T}_1,\ldots,\text{T}_N)$  &   $N$-tuple of values of datatypes $\text{T}_1,\ldots,\text{T}_N$ ((integer,real))  &  $(U_{\text{T}_1},\ldots,U_{\text{T}_N})$ \cr
        T list  &  list of values of datatype T  &  $U_{\text{T}}, U_{\text{T}}, U_{\text{T}}, \ldots$
\end{tabular}\end{center}

The notation for units is based on common symbols (\eg m for meter) used to
qualify the magnitude of a physical quantity. Units for entities which are void
of unit will be written [-].

Throughout all inputs percentage quantities are scaled between $0$ and $1$.

Throughout all inputs boolean quantities may be written as \defemph{0},
\defemph{no}, \defemph{off} or \defemph{false} to express the respective switch
to be off. The opposite may be written as one of \defemph{1}, \defemph{yes},
\defemph{on} or \defemph{true}. These boolean quantity values are
case-insensitive.


\paragraph{Example} Consider the water table depth whose value is a real
number, has unit meter and defaults to $99$ meters below soil surface. The
expression \{\entmetai[99]{real}{m}\} will reflect these properties. Further,
assume the input entity average temperature~$T_{\text{avg}}$ which is given for
each timestep as a real number without meaningful internal defaults. Properties
of such entity will be expressed as \{\entmetai[$U_{\text{real}}$]{real
list}{$\degreeC$}\}.


%subsections
\readfile[100]{inputs/sources/setup.tex}
\readfile[100]{inputs/sources/site.tex}
\readfile[100]{inputs/sources/event.tex}
\readfile[100]{inputs/sources/climate.tex}
\readfile[100]{inputs/sources/airchemistry.tex}
\readfile[100]{inputs/sources/groundwater.tex}
\readfile[100]{inputs/sources/siteparameters.tex}
\readfile[100]{inputs/sources/soilparameters.tex}
\readfile[100]{inputs/sources/speciesparameters.tex}


\readfile[100]{inputs/sources/farmer.tex}
\readfile[100]{inputs/sources/farmsystem.tex}

