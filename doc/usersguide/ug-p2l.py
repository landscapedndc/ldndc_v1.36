
import sys

PTYPE = { 'f':'real', 'i':'integer', 'u':'unsigned integer', 'b':'bool'}
PTYPETEX = { 'f':'\\insetR', 'i':'\\insetZ', 'u':'\\insetN', 'b':'\\insetB'}
PRANGEOPENTEX =  { 'f':'[', 'i':'[', 'u':'[', 'b':'\\{'}
PRANGECLOSETEX = { 'f':']', 'i':']', 'u':']', 'b':'\\}'}

def make_latexmacro( _name) :
    validchars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
    numbers = { '0':'Zero', '1':'One', '2':'Two', '3':'Three', '4':'Four', '5':'Five', '6':'Six', '7':'Seven', '8':'Eight', '9':'Nine'}

    name = _name.strip( ' \t./\\')
    canonicalized_name = ''
    for c in name :
        if not c in validchars :
            if c in numbers :
                canonicalized_name += numbers[c]
        else :
            canonicalized_name += c
    return canonicalized_name


class parameter_info_t( object) :
    def __init__( self, _name, _datatype, **_kwargs) :
        self._id = _name
        self._name = _name
        self._datatype = _datatype
        self._description = _kwargs.get( 'description', '')
        self._unit = _kwargs.get( 'unit', '?')
        self._minmax = ( _kwargs.get( 'minvalue'), _kwargs.get( 'maxvalue'))

    def __str__( self) :
        return '%s[%s]: "%s" in %s  [%s]' \
            % ( self.name, self.datatype, self.description, self.minmax, self.unit)

    def as_latex( self, _refid, _parameter_type, _collision_detect) :
        param_id = self._id.lower()
        name_out = self.name.replace( '_', '\_')
        param_name = self.name.replace( '_', ' ').title()
        param_default = ''  # TODO no information
        param_unit = self.unit
        if param_unit == '%' :
            param_unit = '\\%'
        elif param_unit == 'oC' :
            param_unit = '$\\degreeC$'
        if '$' not in param_unit:
            param_unit = '$'+param_unit+'$'
        theta_subscript = '%s' % ( ''.join([ n[0].upper() for n in param_name.split()]))
        theta_subscript_rev = theta_subscript[::-1]
        theta_index, theta_name = '', ''
        for c in theta_subscript_rev :
            if theta_name == '' and c in '0123456789' : ## TODO only digits not numbers
                theta_index += ',%s' % c
            else :
                theta_name += c
        theta_subscript = theta_name[::-1]
        if theta_index != '' :
            theta_subscript += '$_{%s}$' % ( theta_index[::-1].rstrip( ','))
        theta_supscript = ''
        if theta_subscript in _collision_detect :
            _collision_detect[theta_subscript] += 1
            theta_supscript = '^{%d}' % ( _collision_detect[theta_subscript])
        else :
            _collision_detect[theta_subscript] = 0
        theta = '\\%s%s_{\\text{%s}}' % ( 'theta', theta_supscript, theta_subscript)

        param_dtype = PTYPETEX[self.datatype]
        param_minmax = '%s%s,%s%s' % ( PRANGEOPENTEX[self.datatype], \
            self._cast( self._minmax[0]), self._cast( self._minmax[1]), PRANGECLOSETEX[self.datatype])
        param_range = '$%s \\in %s \\subseteq %s$' % ( theta, param_minmax, param_dtype)

        return '''\t\\item[\#%d]\\label{input:%sparameters:%s} \\emph{%s},\;\;$%s$
            \t\\globalnewcommand{\\%sparameter%s}{%s}
            \t\\globalnewcommand{\\%sparametername%s}{\\emph{%s}}
            \t\\%sparametermeta[%s]{%s}{%s}\n\n\t\t%s.''' \
            % ( _refid, _parameter_type, param_id, name_out, theta, \
                _parameter_type, make_latexmacro( param_name), theta, \
                _parameter_type, make_latexmacro( param_name), name_out, \
                _parameter_type, param_default, param_range, param_unit, self.description)

    def as_xml( self, _refid, _parameter_type, _childindent=0) :
        param_name = self.name.lower()
        param_value = '%s .. %s' % ( self._cast( self._minmax[0]), self._cast( self._minmax[1]))
        param_desc = ''
        for k, c in enumerate( self.description) :
            if c == '.' or k > 52-int(_childindent/2) :
                param_desc += '...'
                break
            param_desc += c

        childindent = ' ' * _childindent

        return '''%s<!-- #%d  %s -->\n%s<par name="%s" value="%s" />''' \
            % ( childindent, _refid, param_desc, childindent, param_name, param_value)
 
    def _cast( self, _value) :
        if _value == '-' or _value == '?' :
            return _value
        if self._datatype == 'f' :
            float_value = '%.12f' % ( float( _value))
            return float_value.rstrip( '0').rstrip( '.')
        elif self._datatype == 'i' :
            return '%.0f' % ( float( _value))
        elif self._datatype == 'u' :
            if float( _value) < 0 :
                sys.stderr.write( 'illegal unsigned value  [value=%s]\n' % ( _value))
                raise RuntimeError( 'illegal unsigned value  [value=%s]' % ( _value))
            return '%.0f' % ( float( _value))
        elif self._datatype == 'b' :
            return str( _value)
        else :
            raise RuntimeError( 'unknown datatype  [datatype=%s]' % ( self._datatype))

    @property
    def name( self) :
        return self._name
    @property
    def datatype( self) :
        return self._datatype
    @property
    def description( self) :
        return self._description
    @property
    def unit( self) :
        return self._unit
    @property
    def minmax( self) :
        return self._minmax

class parameters_t( object) :
    def __init__( self, _parameter_type, _parameter_file) :
        self._parameter_type = _parameter_type
        self._pinfos = self._read_parameter_infos( _parameter_file)
        if self._pinfos is None :
            raise RuntimeError( 'failed to read parameter file')

    def _read_parameter_infos( self, _parameter_file) :
        pinfos = list()
        with open( _parameter_file, 'r') as pinf :
            comment = None
            parameter = None
            for line in pinf :
                line = line.strip()
                if len( line) == 0 :
                    continue

                if comment is not None and line[0] in '_abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ' :
                    parameter = line
                    pinfo = self._make_parameter( parameter, comment)
                    #print pinfo
                    pinfos.append( pinfo)

                    parameter = None
                    comment = None

                if parameter is None and line.startswith( '## ') :
                    comment = line

        return pinfos

    def _make_parameter( self, _parameter_line, _comment_line) :
        pinfo = _parameter_line.split()
        name, datatype, description, unit, minvalue, maxvalue = None, 'f', '', '?', '?', '?'
        if len( pinfo) == 0 :
            return None
        if len( pinfo) > 0 :
            name = pinfo[0]
        if len( pinfo) > 1 :
            datatype = pinfo[1]
        if len( pinfo) > 2 :
            minvalue = pinfo[2]
            if minvalue == 'nan' :
                minvalue = '?'
        if len( pinfo) > 3 :
            maxvalue = pinfo[3]
            if maxvalue == 'nan' :
                maxvalue = '?'
        if len( pinfo) > 4 :
            unit = pinfo[4]
        description = _comment_line[2:]
        description = description.strip()
        description = description.strip( '.;:')
        description = description.replace( '^', '\^')
        description = description.replace( '_', '\_')
        description = description[0].upper() + description[1:]

        return parameter_info_t( name, datatype, unit=unit, \
            description=description, minvalue=minvalue, maxvalue=maxvalue)

    def as_fmt( self, _format) :
        if _format == 'latex' :
            return self.as_latex()
        elif _format == 'list' :
            return self.as_list()
        elif _format == 'xml' :
            return self.as_xml()
        else :
            return '?'

    def as_latex( self) :
        collision_detect = dict()
        doctext = ''
        for refid, pinfo in enumerate( self._pinfos) :
            doctext += '%s\n\n' % ( pinfo.as_latex( refid+1, self._parameter_type, collision_detect))
        return doctext

    def as_list( self) :
        listtext = ''
        for pinfo in self._pinfos :
            listtext += '## %s\n%s\n' % ( pinfo.description, pinfo.name)
        return listtext

    def as_xml( self) :
        childopen = ''
        childclose = ''
        childindent = 4
        if self._parameter_type == 'site' :
            pass
        elif self._parameter_type == 'species' :
            childopen = '<species  mnemonic="loewenzahn" group="grass" >'
            childclose = '</species> <!-- close species loewenzahn -->'
            childindent = 6
        else :
            pass
        doctext = '''
\\lstset{language=XML}
\\begin{lstlisting}
<?xml version="1.0"?>
<ldndc%sparameters>
  <!--
    id .. block id
  -->

  <%sparameters  id="0" >
    %s
''' % ( self._parameter_type, self._parameter_type, childopen)
        for refid, pinfo in enumerate( self._pinfos) :
            doctext += '%s\n' % ( pinfo.as_xml( refid+1, self._parameter_type, childindent))
        doctext += '''
    %s
  </%sparameters>

</ldndc%sparameters>
\\end{lstlisting}\n
''' % ( childclose, self._parameter_type, self._parameter_type)
        return doctext


def  main() :
    if len( sys.argv) < 3 :
        sys.stderr.write( 'parameter type and file are required\n')
        sys.stderr.write( '\texample: python %s site ../../common/input/siteparameters/siteparameters.txt\n' % ( sys.argv[0]))
        return 1
    parameter_format = 'latex'
    if len( sys.argv) > 3 :
        parameter_format = sys.argv[3]

    parameter_type = sys.argv[1]
    parameter_file = sys.argv[2]
    try :
        parameters = parameters_t( parameter_type, parameter_file)
        doctext = parameters.as_fmt( parameter_format)
        sys.stdout.write( doctext)
        listtext = parameters.as_list()
        sys.stderr.write( listtext)
    except RuntimeError as runtime_error :
        sys.stderr.write( 'failed to read parameter file  [file=%s]\n' % ( parameter_file))
        return 2
    return 0


if __name__ == '__main__' :
    sys.exit( main())
        
