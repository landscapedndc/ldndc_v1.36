


/*!

@image html ldndclogo-big-notext.png "LandscapeDNDC" width=400

@mainpage LandscapeDNDC

LandscapeDNDC is a simulation framework for terrestrial ecosystem models on site
and regional scales @cite haas:2013a. LandscapeDNDC emerged from the site scale
model MoBiLE @cite grote:2009b, which was based on the Arable-DNDC and
Forest-DNDC models  @cite li:1992a @cite li:1994a  @cite li:2000a @cite stange:2000a.
The modular design of LandscapeDNDC allows plugging in any choice of process
descriptions for various parts of different natural ecosystems. 

- @subpage ldndc_inputs
- @subpage ldndc_mobile
- @subpage ldndc_farmsystem
- @subpage ldndc_echy3d
- @subpage ldndc_oryza2000
- @subpage troubleshooting

*/



/*!

@page ldndc_inputs LandscapeDNDC inputs

- @subpage ldndc_inputs_siteparameters 
- @subpage ldndc_inputs_species
- @subpage ldndc_inputs_soil
- @subpage ldndc_inputs_eventhandler



@page ldndc_mobile LandscapeDNDC - MoBiLe

@section ldndc_models_overview Overview

@image html mobile-models-communication.png "Models communication" width=400
@image latex mobile-models-communication.png "Models communication"

Models are presented according to the respective ecosystem domain:

- @subpage microclimate
- @subpage watercycle
- @subpage vegetation
- @subpage soilchemistry
- @subpage ldndc_mobile_outputs



@page ldndc_mobile_outputs Standard outputs

LandscapeDNDC will write simulation results into several output files. These
files are in default mode plain text files using the line endings according to
the platform where the simulations have been performed. Be aware that, for
example, Windows and Unix/Linux do not have identical line endings in text
files.

The standard outputs are classified according to the ecosystem compartments /
process functionalities:

- @subpage soilchemistryoutput \n
Soil items, e.g., soil organic carbon and nitrogen pools, fluxes
of various carbon and nitrogen species in and out of the soil
system

- @subpage physiologyoutput \n
Vegetation items such as biomasses, photosynthesis and
respiration rates, stress indicators, ...

- @subpage vegetationstructureoutput \n
Strucutral components of the vegetaion such as stem diameter and
height, crown width. Majorly used for forest simulations.

- @subpage microclimateoutput \n
This holds everything with regard to temperature and radiation
within the canopy and soil.

- @subpage managementoutput \n
...

- @subpage watercycleoutput \n
This holds everything regarding hydrology, e.g., water contents
in deffierent depths, water fluxes in and out of the system, ...

- @subpage ecosystemoutput \n
...

- @subpage inventoryoutput \n
...

- @subpage ggcmioutput \n
...

- @subpage dssoutput \n
...


Nearly all ecosystem standard sinks offer outputs for the three major timemodes:
 - subdaily output (by default having suffix subdaily)
 - daily output (by default having suffix daily)
 - yearly output (by default having suffix yearly)


For more detailed output LandscapeDNDC provides for most ecosystem standard sinks
'layer-sinks' writing entities reflecting internal discretization.

In the following all output files are described in detail. Each data record in
all files contains information about the kernel associated to the record and
the simulation time of the record:

 - id \n
The ID of the kernel (e.g. grid cell) that produced the output
record. Note that currently the source identifier associated
with a kernel (i.e. cell) is not written to the output sinks.
This means, that reading kernel setups from more than one
source may cause ambigious outputs, if IDs are not unique.

 - year \n
The simulation year of the output record

 - day \n
The simulation Julian day of the output record (only day and subday sinks)

 - subday \n
The simulation subday of the output record (only subday sinks)

 - layer \n
The soil layer of the output record (only 'layer-sinks')

*/


/*!

@page ldndc_farmsystem LandscapeDNDC - Farmsystem
@tableofcontents

@page ldndc_echy3d LandscapeDNDC - EcHy3D
@tableofcontents

@page ldndc_oryza2000 LandscapeDNDC - ORYZA2000
@tableofcontents

@page troubleshooting Troubleshooting
@tableofcontents

*/
