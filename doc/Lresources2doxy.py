import sys
import json

siteparameters_desc = {}
if len(sys.argv) > 1:
    data_site = open(sys.argv[1]+"/parameters-site.xml", "r")
    for line in data_site:
        if "par name" in line and "desc" in line:
            par = line.split('"')[1]
            desc = line.split('desc')[1].split('"')[1]
            siteparameters_desc.update({par: desc})

soilparameters_desc = {}
if len(sys.argv) > 1:
    data_soil = open(sys.argv[1]+"/parameters-soil.xml", "r")
    for line in data_soil:
        if "par name" in line and "desc" in line:
            par = line.split('"')[1]
            desc = line.split('desc')[1].split('"')[1]
            soilparameters_desc.update({par: desc})

sp_info_dict = {}
sp_info = open(sys.argv[1]+"/parameters-species.xml", "r")
for line in sp_info:
    if ('mnemonic' in line) and ('name' in line):
        mnemonic = line.split('mnemonic="')[1].split('"')[0]
        name = line.split('name="')[1].split('"')[0]
        sp_info_dict.update({mnemonic.lower(): name})

data = open(sys.argv[2]+"/Lresources", "r")
sp = 'none'
species = {}
for line in data:
    if ( ('species' in line) and (':' not in line) ):
        if 'mnemonic' in line:
            sp = line.split('"')[1]
            species.update({sp: {'name': sp_info_dict[sp.lower()]}})
            continue
        if ( (sp in line) and ('group' in line) ):
            group = line.split('"')[1]
            species[sp].update( {'group': group})
            continue
        if ( (sp in line) and ('value' in line) ):
            par = line.split('.')[3]
            val = line.split('"')[1]
            species[sp].update( {par: val})
            continue    
data.close()

def get_header( _sp, _name):
    return '''/*!
 * @page '''+str(_sp)+'''
 * Species name: '''+str(_name)+'''
 * Parameter | value
 * ----------- | ----------
'''

def write_parameters( _type, _name):
    sps_out = open(sys.argv[2]+"doc/species_parameters/"+str(_type)+"_species.txt", "w")
    sps_out.write('''/*!\n * @page ldndc_inputs_'''+str(_type)+'''_parameters \n''')
    for sp, par in species.items():
        if par['group'] == _type:
            sp_out = open(sys.argv[2]+"doc/species_parameters/"+str(sp)+".txt", "w")
            sp_out.write(get_header( sp, par['name']))
            sps_out.write(' * @subpage '+str(sp)+'\n')
            sps_out.write(' * '+par["name"]+'\n *\n')
            for p,v in par.items():
                sp_out.write(' * '+str(p)+' | '+str(v)+' \n')
            sp_out.write(' */')        
            sp_out.close()

    sps_out.write(' */')        
    sps_out.close()
    
write_parameters('wood', "Wood")            
write_parameters('grass', "Grass")            
write_parameters('crop', "Crops")  


with open(sys.argv[1]+"/species/DefaultParameters.any.json") as f:
  speciesparams = json.load(f)
speciesparams = speciesparams['parameters']

spspar_out = open(sys.argv[2]+"doc/species_parameters.txt", "w")
spspar_out.write('''/*!\n * @page ldndc_inputs_speciesparameters \n *\n * Parameter | value | description \n * ----------- | ---------- | ----------\n''')
for par, val in speciesparams.items():
    spspar_out.write((' * ' + str(par).lower() + ' | ' + str(val['value']) + ' | ' + str(val['description']) + '\n'))
spspar_out.write(' */')        
spspar_out.close()


data = open(sys.argv[2]+"/Lresources", "r")
site = {}
for line in data:
    if ( 'site.parameter' in line):
        par = line.split('.')[2]
        val = line.split('"')[1]
        desc = line.split('"')[1]
        site.update( {par: [val,desc]})
        
site_out = open(sys.argv[2]+"doc/site_parameters.txt", "w")
site_out.write('''/*!\n * @page ldndc_inputs_siteparameters \n *\n * Parameter | value | description \n * ----------- | ---------- | ----------\n''')
for par, val in site.items():
    site_out.write(' * ' + par.lower() + ' | ' + val[0] + ' | ' + siteparameters_desc[par] + '\n')

site_out.write(' */')        
data.close()     


data = open(sys.argv[1]+"/parameters-soil.xml", "r")
soil = []
for line in data:
    if 'mnemonic' in line:
        soil.append( dict( {"soiltype": line.split('mnemonic')[1].split('"')[1],
                            "soilname": line.split('name')[1].split('"')[1]} ))
    if ( '<par name=' in line):
        if 'name' in line and 'value' in line :
            par = line.split('name')[1].split('"')[1]
            val = line.split('value')[1].split('"')[1]
            if 'desc' in line :
                desc = line.split('desc')[1].split('"')[1]
            else:
                desc = '-'
            soil[-1].update( {par: [val,desc]})
soil_out = open(sys.argv[2]+"doc/soil_parameters.txt", "w")
soil_out.write('''/*!\n * @page ldndc_inputs_soiltypes \n * \n''')
for so in soil:
    if "HUMUS" in so['soiltype']:
        break
    if len(so) > 3 and so['soiltype'] != 'NONE':
        soil_out.write(' * - ' + so['soiltype'] + ' (' + so['soilname'] + ')\n')
soil_out.write(' */\n\n')        

soil_out.write('''/*!\n * @page ldndc_inputs_soilparameters \n *\n * Parameter | value | description \n * ----------- | ---------- | ----------\n''')
for so in soil:
    if 'NONE' in so['soiltype']:
        for par, val in so.items():
            if par in soilparameters_desc:
                soil_out.write(' * ' + par.lower() + ' | ' + val[0] + ' | ' + soilparameters_desc[par] + '\n')
        break
soil_out.write(' */')        