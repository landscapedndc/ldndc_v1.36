
   EDIT SITE

   SECTION Description

   brief: Information about the SITE file

   description: This section holds information about the SITE file
   that is not used for the model runs but is intended to inform the user
   about the origin of the site information.


   element: Author

   brief: Author of the SITE File

   description: Whenever a SITE file is created or changed, the
   author of this change should either write his name into this field or
   add it to the name already given. (Member of SECTION: Description)


   element: Date

   brief: Date of the last change

   description: Whenever a change has been applied to this SITE file,
   the date and time of this change should appear here. This is done
   automatically by the program. (Member of SECTION: Description)


   element: Dataset

   brief: Datasets used for initializing the site

   description: If the data used for initialization have are taken
   from publications, these references can be put here. The user is
   principally free in the format of this information. Possible components
   could also include personal communication or estimates. (Member of
   SECTION: Description)


   element: Site name

   brief: Site name

   description: This field repeats the name of the setup file as
   called in the EDIT SETUP menu. It normally contains a short form of the
   site name (e.g. 5 letter abbreviation), but may also include the year
   of data gathering (important particularly for fast developing
   ecosystems) or indicate a particular management (e.g. C for control, F
   for fertilized, I for irrigated, etc.). (Member of SECTION:
   Description)



   SECTION General

   brief: General site information

   description: In this section, general site information is
   presented. This includes geographical properties (longitude, latitude,
   elevation, slope, aspect) as well as long term climate means (annual
   temperature, precipitation, cloudiness, wind speed) and deposition. The
   data are used to generate idealized climate/air chemistry conditions.
   These calculated environmental conditions are only used for simulation,
   when climate/air chemistry information is not explicitly given (see
   EDIT SETUP). The general site information is also directly available
   for the models in case this information is needed to estimate
   particular processes, and some parameters are only used by specific
   models (e.g. slope may be used in runoff calculations to estimate the
   resistance to water runoff at the surface).


   element: Latitude

   brief: Latitude (degree)

   description: Latitude is primarily used for calculating daily
   solar radiation in cases this is not provided from files. It is also
   used to modify the distribution of precipitation in case this is
   generated. Default value is 45. (Member of SECTION: General)


   element: Longitude

   brief: Longitude (degree)

   description: Latitude may be used for calculating daily solar
   radiation in addition to latitude in cases this is not provided from
   files. Default value is 10. (Member of SECTION: General)


   element: Elevation

   brief: Elevation (meter above sea level)

   description: Elevation may be used for calculating daily
   temperature (and eventually also daily solar radiation in addition to
   latitude) in cases this is not provided from files. It may also be used
   to empirically estimate potential evaporation. Default value is 100.
   (Member of SECTION: General)


   element: Time zone

   brief: GMT time zone

   description: Similar as longitude, GMT time may also be used for
   calculating daily solar radiation in addition to latitude in cases this
   is not provided from files. Default value is -1. (Member of SECTION:
   General)


   element: Slope

   brief: Slope (degree)

   description: Slope may be used for calculating daily solar
   radiation in addition to latitude in cases this is not provided from
   files. It may also be used to empirically estimate the potential water
   amount on the soil surface and thus may play a role for water runoff
   calculations. Default value is 0. (Member of SECTION: General)


   element: Aspect

   brief: Aspect (degree)

   description: Aspect may be used for calculating daily solar
   radiation in addition to latitude in cases this is not provided from
   files. Default value is 0. (Member of SECTION: General)


   element: Average Temp.

   brief: Annual (long-term) average temperature (oC)

   description: Annual (long-term) average temperature is used
   together with temperature amplitude for calculating daily average
   temperature in cases this is not provided from files. Default value is
   calculated from elevation, but it is strongly recommended to define
   this parameter because daily climate generation is quite sensitive to
   it. (Member of SECTION: General)


   element: Temp. Amplitude

   brief: Annual (long-term) temperature amplitude (oC)

   description: Annual (long-term) temperature amplitude is used
   together with average temperature for calculating daily average
   temperature in cases this is not provided from files. Default value is
   20, but it is strongly recommended to define this parameter because
   daily climate generation is quite sensitive to it. (Member of SECTION:
   General)


   element: Annual Prec.

   brief: Annual sum of precipitation (mm)

   description: Annual sum of precipitation is used together with
   rainfall intensity (and latitude) to generate an estimation of daily
   precipitation in case this is not provided from files. Default value is
   800, but it is strongly recommended to define this parameter because
   daily climate generation is quite sensitive to it. (Member of SECTION:
   General)


   element: Prec. Intensity

   brief: Average rainfall intensity (mm h-1)

   description: Rainfall intensity is required for internal
   distribution of precipitation within the day (respectively to determine
   the length of rainfall duration). This is either fed into models
   directly or used to calculate daily and sub-daily rainfall intensity in
   case this is not provided from files. Default value is 5. (Member of
   SECTION: General)


   element: Rel. Radiation Ext.

   brief: Relative radiation absorption (0-1)

   description: Relative cloudiness or radiation absorption is the
   relation between solar radiation (radiation at the upper boundary of
   the atmosphere) and global radiation that arrives at the vegetation
   surface. The latter results from the absorption properties of the
   atmosphere, which depends on its particle concentration and
   composition, i.e. cloud occurrence. The range is in principle between 0
   (no absorption at all) and 1 (no radiation reaches the site) but
   general between 0.3 and 0.7, depending on weather conditions. Default
   value is 0.4. (Member of SECTION: General)


   element: Wind Speed

   brief: Average wind speed (m s-1)

   description: Average wind speed is set equal to daily average wind
   speed in case this is not provided from files. Wind speed is only used
   by few models (Penman-Monteith) to estimate potential evaporation. If
   these models are not selected, it is not used. Default value is 1.5.
   (Member of SECTION: General)


   element: Nitrogen Depo.

   brief: Average concentration of total nitrogen in
   precipitation (mg L-1)

   description: Average nitrogen concentration in precipitation is
   set equal to daily concentration in case this is not provided from
   files. Nitrogen concentration is used to define total nitrogen
   deposition and should therefore include dry and wet deposition if it is
   derived from deposition data. Default value is 1. (Member of SECTION:
   General)


   element: Water Table

   brief: Average depth of the water table (m)

   description: Annual average water table depth (distance between
   soil surface and ground water table) is set equal to daily water table
   depth. In soil layers below the groundwater table, the porosity is
   assumed water filled. Soil layers above this value may or may not be
   influenced depending on the model assumption. Default value is 99.
   (Member of SECTION: General)



   SECTION Soil

   brief: Soil data

   description: The soil section includes a short list of general
   parameters and a section to specify the properties in each particular
   strata of the soil. The general indications are used to estimate the
   more specific properties in case these are missing. The term ‘strata’
   is indicating a part of the soil that is (approximately) homogenous. It
   may consist of one or many layers. The uppermost layer is indicated
   with “-1”. If a litter layer exists, this is equal to the uppermost
   litter layer.
   NOTE: It is highly recommended to choose the total soil layer depth
   equal (or higher) than the deepest rooting depth indicated in the
   vegetation settings, otherwise, the water and nutrient balance may be
   corrupted.


   element: CORG05

   brief: organic carbon in 5 cm depth (%)

   description: Organic carbon in 5 and 30 cm depth is used to
   estimate the fraction of organic carbon throughout the soil profile if
   this is not given explicitly as a soil strata property. If both the
   parameter and the specific strata information are missing, the carbon
   distribution is empirically estimated based on the measurements from
   Level II forest plots in Germany.
   NOTE: The empirical estimations are based on forestry plots. It is not
   advisable to use them on other vegetation coverages. (Member of
   SECTION: Soil)


   element: CORG30

   brief: organic carbon in 30 cm depth (%)

   description: Organic carbon in 5 and 30 cm depth is used to
   estimate the fraction of organic carbon throughout the soil profile if
   this is not given explicitly as a soil strata property. If both the
   parameter and the specific strata information are missing, the carbon
   distribution is empirically estimated based on the measurements from
   Level II forest plots in Germany. (Member of SECTION: Soil)
   NOTE: The empirical estimations are based on forestry plots. It is not
   advisable to use them on other vegetation coverage.


   element: Litter Layer Height

   brief: litter layer height (mm)

   description: Within the litter layer, a number of soil properties
   are set considering humus type specific properties (e.g. carbon content
   is based on a humus type specific soil bulk density). These settings
   will take effect for all soil layers within the litter dimension.
   (Member of SECTION: Soil)
   NOTE: It is advisable to set a litter layer for all ecosystems where
   leaves are at least temporally covering the ground. Otherwise the model
   will not be able to consider the physical properties of a litter layer
   at all!


   element: Humus Type

   brief: humus type

   description: Humus types are selected with a drop down menu. They
   characterize the basic features of the litter layer when not given
   explicitly in the soil strata description (otherwise they are not
   used). The available choices are defined in the virtual-site folder
   (for example of Mull, Moder, and Rawhumus) (Member of SECTION: Soil)


   element: Soil Type

   brief: soil type

   description: Soil types are selected with a drop down menu. They
   characterize the basic features of the soil layers when not given
   explicitly in the soil strata description (otherwise they are not
   used). The available choices are referring to the relative content of
   sand, loam, and sand in the soil texture. Soil types are defined with a
   four letter abbreviation that has to be listed in the virtual-site
   folder (e.g. SAND, LOAM, CLAY, and SALO, SACL for intermediate types).
   (Member of SECTION: Soil)


   element: < lines of soil strata information>

   brief: Soil strata information

   description: Each soil strata is defined by a number of
   properties. It can consist of one or more layers that all have the same
   properties. If properties are not indicated (-99.99), they are
   estimated from available information such as humus or soil type
   classification. The properties are:
   1) depth: vertical dimension of the strata (mm)
   2) height: vertical dimension of the layers within the strata (the
   vertical dimension of all layers in one strata should add up to the
   depth of the strata (mm)
   3) bd: bulk density (kg L-1)
   4) wcMax: field capacity (mm m-3)
   5) wcMin: wilting point (mm m-3)
   6) corg: fraction of carbon content (%)
   7) norg: fraction of nitrogen content (%)
   8) clay: fraction of clay (%)
   9) scel: volumentric stone fraction (%)
   10) pH: pH value (in K?)
   11) sks: hydraulic conductivity when water saturated (cm min-1)
   (Member of SECTION: Soil)
   NOTE1: The assumption of a soil strata being homogeneous is sometimes
   oversimplified. In terms of carbon (and nitrogen) content, it is often
   advisable to set the strata specific values to -99.99 and set the
   CORG30 and CORG05 values appropriately.
   NOTE2: Many soil chemistry calculations cannot cope with large (thick)
   layers. Therefore it is advisable to use app. 20 mm layer heights (not
   strata depth) for the soil parts of large carbon concentrations
   (generally the upper 300 mm). Furthermore, the layer heights should not
   get smaller with depth.
