/*!
 * @brief
 *  configuration header
 *
 * @author
 *  steffen klatt (created on: nov. 18, 2016)
 */

#ifndef  LDNDC_H_
#define  LDNDC_H_


#include  "ldndc-dllexport.h"
#include  "ldndc-config.h.inc"

#include  <time/cbm_time.h>
#include  <cfgfile/cbm_cfgfile.h>
#include  <prjfile/cbm_prjfile.h>

#include  <logging/cbm_logging.h>
#define  LD_DefaultLogger CBM_DefaultLogger
#include  <cbm_rtcfg.h>
#define  LD_RtCfg CBM_LibRuntimeConfig
#include  <memory/cbm_mem.h>
#define  LD_Allocator CBM_DefaultAllocator
//#include  <resources/Lresources.h>
#define  LD_Resources CBM_DefaultResources

#include  <memory/cbm_mem.h>
#define  LD_Construct(alloc_type,alloc_size) \
    LD_Allocator->allocate_type< alloc_type >( alloc_size)
#define  LD_Destroy(obj) \
    LD_Allocator->destroy( obj)
#define  LD_CAlloc(alloc_size,ini_value) \
    LD_Allocator->allocate_init_type( alloc_size, ini_value)
#define  LD_Free(alloc_mem) \
    LD_Allocator->deallocate(alloc_mem)

#endif /* !LDNDC_H_ */

