
#ifndef LDNDC_DLLEXPORT_H_
#define LDNDC_DLLEXPORT_H_

#include  "ldndc-compiler.h"

/* see https://gcc.gnu.org/wiki/Visibility */
#if defined(LDNDC_COMPILER_MSC)
#  define LDNDC_HELPER_DLL_IMPORT __declspec(dllimport)
#  define LDNDC_HELPER_DLL_EXPORT __declspec(dllexport)
#  define LDNDC_HELPER_DLL_NOEXPORT
#else
#  if (defined(LDNDC_COMPILER_GCC) && (__GNUC__ >= 4)) || defined(LDNDC_COMPILER_CLANG)
#    define LDNDC_HELPER_DLL_IMPORT __attribute__ ((visibility ("default")))
#    define LDNDC_HELPER_DLL_EXPORT __attribute__ ((visibility ("default")))
#    define LDNDC_HELPER_DLL_NOEXPORT  __attribute__ ((visibility ("hidden")))
#  else
#    define LDNDC_HELPER_DLL_IMPORT
#    define LDNDC_HELPER_DLL_EXPORT
#    define LDNDC_HELPER_DLL_NOEXPORT
#  endif
#endif

#ifdef LDNDC_TARGETS_SHARED_LIBRARIES
#  if  defined(ldndc_EXPORTS) || defined(pldndc_EXPORTS)
#    define LDNDC_API LDNDC_HELPER_DLL_EXPORT
#  else
#    define LDNDC_API LDNDC_HELPER_DLL_IMPORT
#  endif
#  define LDNDC_LOCAL LDNDC_HELPER_DLL_NOEXPORT
#else
#  define LDNDC_API
#  define LDNDC_LOCAL
#endif


#endif /* !LDNDC_DLLEXPORT_H_ */
