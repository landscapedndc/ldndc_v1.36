
import sys
import os
import xml.etree.cElementTree as xml
import xml.dom.minidom as MD

def open_entityfile( _format, _filename) :
    fname = _filename
    try :
        fp = open( fname, 'r')
    except :
        fp.close()
        sys.stderr.write( 'failed to open file: "%s"\n' % ( fname))
        sys.exit( 255)

    return  fp

def read_parameters_yaml( _filename) :
    import yaml
    fp = open_entityfile( 'yaml', _filename)
    params = yaml.load( fp)
    fp.close()
    return  params
def read_parameters_json( _filename) :
    import json
    fp = open_entityfile( 'json', _filename)
    params = json.load( fp)
    fp.close()
    return  params


def write_parameters_species_xml( _xf, _kind, _path, _params) :
    path = _path[0]
    newpath = path.pop()
    entity = _path[1]
    entity_name, entity_mnemonic, suffix = entity.split( '.')

    mnemonic = _params['mnemonic']
    if newpath == entity_mnemonic :
        mnemonic = ':%s:' % ( mnemonic)
    else :
        path = path + [ newpath ]

    xf = _xf
    for p in path :
        xf_children = xf.findall( './%s[@mnemonic=\':%s:\']' % ( _kind, p))
        if len( xf_children) > 1 :
            sys.stderr.write( 'ambiguous query  [%s]\n' % ( p))
            sys.stderr.write( '\n'.join( [ str( x.attrib) for x in xf_children ]))
            sys.exit( 255)
        xf = xf_children[0]

    if _params['mnemonic'] == 'any' :
        anyelem = xml.Element( 'species', dict( mnemonic=':any:', name='any', group='any'))
        xf.append( anyelem)
        mnemonic = 'none'

    elem = xml.Element( 'species', \
        dict( mnemonic=mnemonic, name=_params['name'], group=_params['group']))
    xf.append( elem)

    n_params = -1
    parameters = _params.get( 'parameters')
    if parameters :
        n_params = len( parameters)
        for param in parameters :
            par_props = dict( name=param, value=str(parameters[param]['value']))
            parelem = xml.Element( 'par', par_props)
            elem.append( parelem)
    else :
        ## hmmm, no empty dict :|
        n_params = 0

    return  n_params

def write_parameters_kind_xml( _xf, _kind, _path, _params) :
    F=dict( species=write_parameters_species_xml)
    write_parameters_kind = F.get( _kind)
    if write_parameters_kind is None :
        sys.stderr.write( 'sorry, writer for "%s" not implemented\n' % ( _kind))
        sys.exit( 255)
    return  write_parameters_kind( _xf, _kind, _path, _params)

def write_parameters_xml( _fp, _kind, _entities, _read_parameters) :

    xmlskel  = '<?xml version="1.0"?>'
    xmlskel += '<ldndc%sparameters>' % ( _kind)
    xmlskel += '<%sparameters id="0" useresources="false">' % ( _kind)
    xmlskel += '</%sparameters>' % ( _kind)
    xmlskel += '</ldndc%sparameters>' % ( _kind)

    xf = xml.fromstring( xmlskel)
    xroot =  xml.ElementTree( xf).getroot()
    xf = xf[0]

    for entity in _entities :
        params = _read_parameters( entity)
        if params is None :
            sys.exit( 255)

        path = entity.replace( _kind, 'any')
        path = path.split( os.sep)
        elem = path.pop()

        n_params = write_parameters_kind_xml( \
                    xf, _kind, ( path, elem), params)
        if n_params < 0 :
            sys.stderr.write( 'error  [%s]\n' % ( entity))
            sys.exit( 1)

    _fp.write( MD.parseString( xml.tostring(xroot)).toprettyxml())

def read_ostree( _kind, _rootdir, _targetfile, _sourceformat, _read_parameters, _write_parameters) :

    entities = list()
    parent_entities = list()
    for root, dirs, efiles  in os.walk( _rootdir) :
        path = root.split( os.sep)
        for efile in efiles :
            ## ignore the file if it does not end with expected suffix, e.g., json
            if not efile.endswith( '.%s' % ( _sourceformat)) :
                sys.stderr.write( 'ignoring file "%s"\n' % ( efile))
                continue

            entname, entmnemonic, suffix = efile.split( '.')
            entfilename = '%s%s%s' % ( root, os.sep, efile)
            if path[-1] == entmnemonic or entmnemonic == 'any' :
                parent_entities.append( entfilename)
            else :
                entities.append( entfilename)
    entities = parent_entities + entities

    _write_parameters( _targetfile, _kind, entities, _read_parameters)

parameterkind = sys.argv[1] if len( sys.argv) > 1 else 'species'

READ_PARAMETERS = dict( json=read_parameters_json, yaml=read_parameters_yaml)
sourceformat = sys.argv[2] if len( sys.argv) > 2 else 'json'
read_parameters = READ_PARAMETERS.get( sourceformat)
if read_parameters is None :
    sys.stderr.write( 'source format unknown: %s\n' % ( sourceformat))
    sys.stderr.write( 'source formats: %s\n' % ( READ_PARAMETERS.keys()))
    sys.exit(1)

WRITE_PARAMETERS = dict( xml=write_parameters_xml)
targetformat = sys.argv[3] if len( sys.argv) > 3 else 'xml'
write_parameters = WRITE_PARAMETERS.get( targetformat)
if write_parameters is None :
    sys.stderr.write( 'target format unknown: %s\n' % ( targetformat))
    sys.stderr.write( 'target formats: %s\n' % ( WRITE_PARAMETERS.keys()))
    sys.exit(2)

targetparameterfilename = 'parameters-%s.%s' % ( parameterkind, targetformat)

makeaction = sys.argv[4] if len( sys.argv) > 4 else 'all'
if makeaction == 'all' :
    targetparameterfile = sys.stdout if targetparameterfilename == '-' else open( targetparameterfilename, 'w')
    read_ostree( parameterkind, '%s' % ( parameterkind), \
        targetparameterfile, sourceformat, read_parameters, write_parameters)
    targetparameterfile.close()
elif makeaction == 'clean' :
    os.remove( targetparameterfilename)    
    sys.stderr.write( 'removed target file "%s"\n' % ( targetparameterfilename))
else :
    sys.stderr.write( 'unknown action "%s"\n' % ( makeaction))
    sys.exit( 3)

