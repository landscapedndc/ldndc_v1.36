
import sys
import os
import xml.etree.ElementTree as xml

def  mkdir( _dir) :
    try :
        os.mkdir( _dir)
    except OSError :
        sys.stderr.write( 'failed to create directory: "%s"\n' % ( _dir))
        return  -1
    return  0


def canonicalize_name( _name) :
    validchars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_0123456789'
    name = _name.strip( ' \t./\\')
    name = name.title()

    k = name.find( '(')
    if k > 0 :
        name = name[:k]
    name = name.strip()

    canonicalized_name = ''
    for c in name :
        if c in validchars :
            canonicalized_name += c
    return canonicalized_name


def open_entityfile( _format, _mnemonic, _name, _dir) :
    fname = _dir+os.sep+_name
    if _mnemonic != '' :
        fname += '.'+_mnemonic

    fname = '%s.%s' % ( fname, _format)
    try :
        fp = open( fname, 'r')
    except :
        pass # all ok
    else :
        fp.close()
        sys.stderr.write( 'file exists: "%s"\n' % ( fname))
        sys.exit( 255)

    fp = open( fname, 'w')
    return  fp

def make_entityname( _s) :
    name = _s.attrib['name'].strip()
    namesuppl = ''
    p1, p2 = name.find( '('), name.find( ')')
    if ( p1 < p2) :
        namesuppl = name[p1+1:p2]
        namesuppl = namesuppl.strip()
        name = name[:p1]

    name = name.title().strip()
    return  name, namesuppl

def parameter_sortkey( _param) :
    return  _param.attrib['name'].strip().lower()
def write_parameters_yaml( _s, _mnemonic, _name, _dir) :
    sys.stderr.write( 'mnemonic="%s"  name="%s"  dir="%s"\n' % ( _mnemonic, _name, _dir))

    fp = open_entityfile( 'yaml', _mnemonic, _name, _dir)
    name, namesuppl = make_entityname( _s)

    fp.write( 'mnemonic: "%s"\n' % ( _mnemonic))
    fp.write( 'name: "%s"\n' % ( name))
    if namesuppl != '' :
        fp.write( 'namesupplement: "%s"\n' % ( namesuppl))

    fp.write( 'group: "%s"\n' % ( _s.attrib.get( 'group', '').strip().lower()))

    fp.write( '\n')
    fp.write( 'parameters:\n')

    params = _s.findall( 'par')
    params = sorted( params, key=parameter_sortkey)
    for k, param in enumerate( params) :
        pname = param.attrib['name'].strip()
        pvalue = param.attrib['value'].strip()
        pcomment = param.attrib.get('comments', '').strip()
        if pcomment :
            if k > 0 :
                fp.write( '\n')
            fp.write( '  # %s\n' % ( pcomment))

        fp.write( '  %s:\n' % ( pname.lower()))
        fp.write( '    value: %s\n' % ( pvalue))

        psource = param.attrib.get( 'source', None)
        if psource :
            fp.write( '    source: "%s"\n' % ( psource))
        pdepends = param.attrib.get( 'depends', None)
        if pdepends :
            fp.write( '    depends: "%s"\n' % ( pdepends))
        pformula = param.attrib.get( 'formula', None)
        if pformula :
            fp.write( '    formular: "%s"\n' % ( pformula))

    fp.write( '\n')
    fp.close()

def write_parameters_json( _s, _mnemonic, _name, _dir) :
    print 'mnemonic=', _mnemonic, '  name=',_name, '  dir=',_dir

    fp = open_entityfile( 'json', _mnemonic, _name, _dir)
    name, namesuppl = make_entityname( _s)

    fp.write( '{\n')
    fp.write( '  "mnemonic": "%s",\n' % ( _mnemonic))
    fp.write( '  "name": "%s",\n' % ( name))
    if namesuppl != '' :
        fp.write( '  "namesupplement": "%s",\n' % ( namesuppl))

    fp.write( '  "group": "%s",\n' % ( _s.attrib.get( 'group', '').strip().lower()))

    fp.write( '\n')
    fp.write( '  "parameters": {\n')

    params = _s.findall( 'par')
    params = sorted( params, key=parameter_sortkey)
    for k, param in enumerate( params) :
        pname = param.attrib['name'].strip()
        pvalue = param.attrib['value'].strip()

        if k > 0 :
            fp.write( ',\n')
        fp.write( '    "%s": {\n' % ( pname.lower()))
        fp.write( '        "value": %s' % ( pvalue))

        psource = param.attrib.get( 'source', None)
        if psource :
            fp.write( ',\n          "source": "%s"' % ( psource))
        pcomment = param.attrib.get('comments', '').strip()
        if pcomment :
            fp.write( ',\n          "comments": "%s"' % ( pcomment))

        pdata = []
        pdepends = param.attrib.get( 'depends', None)
        if pdepends :
            pdata += [ '"depends": "%s"' % ( pdepends) ]
        pformula = param.attrib.get( 'formula', None)
        if pformula :
            pdata += [ '"formular": "%s"' % ( pformula) ]
        if pdata != [] :
            fp.write( ',\n          %s' % ( ', '.join( pdata)))

        fp.write( ' }')

    fp.write( '\n  }\n')
    fp.write( '}\n')
    fp.close()


def read_tree( _S, _nodename, _dir, _write_parameters) :
    for s in _S :
        mnemonic = s.attrib.get( 'mnemonic', '').strip().lower()
        if len( mnemonic) == 0 :
            sys.stderr.write( 'invalid element: "mnemonic" empty or missing\n')
            sys.exit( 2)

        name = canonicalize_name( s.attrib.get( 'name', ''))

        c = s.findall( _nodename)
        if c == [] :
            if mnemonic == 'none' :
                mnemonic = 'any'
            _write_parameters( s, mnemonic, name, _dir)
        else :
            if mnemonic[0]==':' and mnemonic[-1]==':' :
                pass
            else :
                sys.stderr.write( 'invalid parent "%s"\n' % ( mnemonic))
                sys.exit( 1)
            mnemonic = mnemonic[1:-1]

            ## drop the "any" type
            if mnemonic != 'any' :
                newdir = _dir+os.sep+mnemonic
                mkdir( newdir)
                _write_parameters( s, mnemonic, name, newdir)
            else :
                newdir = _dir

            read_tree( c, _nodename, newdir, _write_parameters)


parameterkind = sys.argv[1] if len( sys.argv) > 1 else 'species'
parameterfile = 'parameters-%s.xml' % ( parameterkind)

WRITE_PARAMETERS = dict( json=write_parameters_json, yaml=write_parameters_yaml)
targetformat = sys.argv[2] if len( sys.argv) > 2 else 'json'
write_parameters = WRITE_PARAMETERS.get( targetformat)
if write_parameters is None :
    sys.stderr.write( 'target format unknown: %s\n' % ( targetformat))
    sys.stderr.write( 'target formats: %s\n' % ( WRITE_PARAMETERS.keys()))
    sys.exit(1)

xmlparameters = xml.parse( parameterfile)
xmlparameters = xmlparameters.getroot()
xmlparameters = xmlparameters.find( '%sparameters' % ( parameterkind))

mkdir( parameterkind)
read_tree( xmlparameters, parameterkind, parameterkind, write_parameters)

