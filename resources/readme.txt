
The resources directory contains various entities controlling the model at
runtime, e.g. species parameters, site specific parameters. Many of the
resources here are used to generate the LandscapeDNDC version-specific
Lresources database which provides defaults for all parameters used within
LandscapeDNDC and many predefined species.

site parameters (parameters-site.xml):

        The site parameter database is used to maintain the default parameter
        values related to site specific control parameters. The Lresources
        generator tool (see app/utilities folder) reads this file and creates
        a version-specific resources database containing exactly the same
        information as this file.


soil and humus parameters (parameters-soil.xml):

        The soil and humus parameter database is used to maintain soil and
        humus type parameterizations. The Lresources generator tool (see
        app/utilities folder) reads this file and creates a version-specific
        resources database containing exactly the same information as this
        file.


species paramaters (species directory):

        The species parameter database is used to manage and maintain predefined
        species for use with LandscapeDNDC simulations. There exists a
        parameter file (JSON) for each species and "genus" organized
        hierarchically according to their "inheritance" structure.
        
        To create the condensed XML parameter database, which is still needed by
        LandscapeDNDC, use the python script x2xml.py; by running

          $> python x2xml.py species json
        
        the file "parameter-species.xml" is created. This step is included in
        the build process, so usually you will not need to do it yourself. The
        Lresources generator tool (see app/utilities folder) reads this
        XML file and creates a version-specific resources database containing
        exactly the same information as this XML.

