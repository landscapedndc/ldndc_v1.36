{
  "mnemonic": "migi",
  "name": "Miscanthus Giganteus",
  "namesupplement": "Miscanthus",
  "group": "grass",

  "parameters": {
    "dfrtopt": {
        "value": 0.012,
          "source": "Monti and Zatta 2009",
          "comments": "approximated" },
    "diammax": {
        "value": 0.03,
          "comments": "estimated" },
    "dleafshed": {
        "value": 310,
          "comments": "Doleman 2008 (presentation): no green leaves anymore in November" },
    "ef_iso": {
        "value": 0.0,
          "source": "Copeland et al. 2012" },
    "ext": {
        "value": 0.68,
          "source": "Clifton-Brown et al. 2000",
          "comments": "Garcia-Quijano et al. 2008 0.5" },
    "fret_n": {
        "value": 0.25,
          "source": "Amougou 2011",
          "comments": "Cardoux et al. 2012: 0.09-0.21; Dohleman Presentation: app. 0.8 (app. 0.005 in harvested material)" },
    "gddfolend": {
        "value": 2500.0,
          "source": "Hastings et al. 2009" },
    "gddfolstart": {
        "value": 800.0,
          "source": "Hastings et al. 2009" },
    "gsmax": {
        "value": 250.0,
          "source": "Beale et al. 1996",
          "comments": "Glowacka et al. 2013: 100-141; Dohleman et al. 2009: 155; Sun et al. 2012 (PCE): app. 200" },
    "gsmin": {
        "value": 18.75,
          "source": "Li et al. 2018 (default)" },
    "h2oref_a": {
        "value": 0.5,
          "source": "Weng et al. 1993 cit. in Hastings et al. 2009" },
    "h2oref_gs": {
        "value": 0.5,
          "comments": "assumes that drought effects are the same on stomata and other processes",
          "depends": "H2OREF_A", "formular": "H2OREF_A" },
    "ini_n_fix": {
        "value": 0.16,
          "source": "Keymer and Kent 2014" },
    "mfolopt": {
        "value": 0.71,
          "source": "Cosentino et al. 2007 (= 5-6/7.7)",
          "comments": "Urrego et al. 2021: 0.4 (6.05/SLAMAX); Clifton-Brown et al. 2000: 0.45 (= 3.5/7.7); Beale and Long 1995: 0.71 (= 5-6/7.7)" },
    "ncfolopt": {
        "value": 0.033,
          "source": "Garcia-Quijano et al. 2008",
          "comments": "Wang et al. 2012 (GCB): 0.01-0.028; Dohleman et al. 2009: app. 0.03; Boersma et al. 2015: app. 0.02" },
    "nc_fineroots_max": {
        "value": 0.015,
          "source": "Beale and Long 1995",
          "comments": "Amougou 2011: 0.007-0.015; Neukirchen et al. 1999: 0.007-0.014" },
    "ndflush": {
        "value": 85,
          "source": "Hastings et al. 2009",
          "comments": "approximated" },
    "ndmorta": {
        "value": 85,
          "source": "Hastings et al. 2009",
          "comments": "approximated" },
    "psl": {
        "value": 0.9,
          "source": "Monti and Zatta 2009",
          "comments": "however Neukirchen et al. 1999: biomass does not decrease below 40cm" },
    "psntmax": {
        "value": 46.0,
          "formular": "PSNTOPT + (PSNTOPT - PSNTMIN)" },
    "psntmin": {
        "value": 4.0,
          "source": "Beale et al. 1996",
          "comments": "estimated from graph" },
    "psntopt": {
        "value": 25.0,
          "source": "Beale et al. 1996",
          "comments": "estimated from graph" },
    "qhrd": {
        "value": 0.7,
          "comments": "Urrego et al. 2021 (172/246)" },
    "qjvc": {
        "value": 4.0,
          "source": "Wang et al. 2012 (GCB): jmax=40-150(80)",
          "comments": "Li et al. 2018: 3.5; Yan et al. 2015 (M. sacchariflorus, M.sinensis, M. lutarioriparius): app. 110/150/105; Dohleman et al. 2009: jmax=79.3; Kromdijk et al. 2008: jmax=142; Garcia-Quijano et al. 2008: jmax=1.8 ???" },
    "qrf": {
        "value": 1.07,
          "source": "Monti and Zatta 2009",
          "depends": "MFOLOPT", "formula": "0.76 / (0.71*MFOLOPT)" },
    "qsf_p1": {
        "value": 20.0,
          "comments": "estimated" },
    "qsf_p2": {
        "value": 0.0,
          "comments": "estimated" },
    "slamax": {
        "value": 15.3,
          "source": "Dohleman et al. 2009 (two sided)",
          "comments": "Morrison et al. 2016: 10.5; Garcia-Quijano et al. 2008: 7.7 (?)" },
    "slamin": {
        "value": 15.3,
          "comments": "assumed to be equal",
          "depends": "SLAMAX", "formula": "SLAMAX" },
    "theta": {
        "value": 0.97,
          "source": "Beale et al. 1996",
          "comments": "Dohleman and Long 2009: 0.84 (also cited in Li et al. 2018)" },
    "tofrtbas": {
        "value": 0.0014,
          "source": "Garcia-Quijano et al. 2008" },
    "vcmax25": {
        "value": 40.0,
          "source": "Wang et al. 2012 (GCB): 10-40(20)",
          "comments": "Li et al. 2018: 25; Glowacka et al. 2013: 10-20; Sun et al. 2012 (PCE): 52; Dohleman et al. 2009: 45" },
    "wuecmax": {
        "value": 18.0,
          "source": "VanLooke et al. 2012 (approximated)",
          "comments": "Glowacka et al. 2013: 6.9-14.3mmol/mol; Cosentino et al. 2007: app. 9 (4.5 gDW kgH2O-1); Clifton-Brown and Lewandowski 2000: 13.4; Beale et al. 1999: app. 18 (9.4 gDW kgH2O-1)" },
    "wuecmin": {
        "value": 5.0,
          "source": "Cosentino et al. 2007 (2.55 gDW kgH2O-1)" }
  }
}
