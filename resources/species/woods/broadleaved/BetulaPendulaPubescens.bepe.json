{
  "mnemonic": "bepe",
  "name": "Betula Pendula/Pubescens",
  "namesupplement": "Silver birch",
  "group": "wood",

  "parameters": {
    "aeis": {
        "value": 51164.8,
          "source": "Lehning et al. 2001 (f. Q.robur)",
          "comments": "needed for application of SIM module for BVOC emission" },
    "aejm": {
        "value": 50400.0,
          "source": "Dreyer et al. 2001",
          "comments": "Aalto and Juurola 2001: 370000.0 (also in Juurola et al. 2005) - probably wrong, refers to Farquhar: 37000" },
    "aekc": {
        "value": 80500.0,
          "source": "Aalto and Juurola 2001 (taken from Harley and Baldocchi 1995 for q.alba)" },
    "aeko": {
        "value": 14500.0,
          "source": "Aalto and Juurola 2001 (taken from Harley and Baldocchi 1995 for q.alba)" },
    "aerd": {
        "value": 26000.0,
          "source": "Aalto and Juurola 2001",
          "comments": "Dreyer et al. 2001: 24950.0" },
    "aevc": {
        "value": 67119.0,
          "source": "Kattge and Knorr 2007",
          "comments": "Medlyn et al. 2002(b): 50600.0 (for greenhouse plants, in open top chambers: 37190.0); Aalto and Juurola 2001: 64500.0; Dreyer et al. 2001: 69700.0 (cit. in Kattge and Knorr 2007: 67119; in Leuning 2002: 60800)" },
    "alb": {
        "value": 0.31,
          "source": "Montheith and Unsworth 1990 (cit in Breuer and Frede 2003)",
          "comments": "Kuuisinen et al. 2013 (cit. in Thom et al. 2017): 0.137" },
    "alpha0_is": {
        "value": 0.014,
          "source": "Lehning et al. 2001 (f. Q.robur)",
          "depends": "EF_ISO" },
    "alpha0_mt": {
        "value": 0.014,
          "source": "Lehning et al. 2001 (f. Q.robur, isoprene)",
          "comments": "needed for application of SIM module for monoterpne emission",
          "depends": "EF_MONO" },
    "amaxb": {
        "value": 41.5,
          "source": "Kattge et al. 2009 (for temperate broad leaved)",
          "comments": "Kurbatova et al. 2008: 71.9 (= Aber et al. 1996 for broad leaved deciduous)",
          "depends": "SLAMIN", "formula": "290.81 / SLAMIN" },
    "cdr_p1": {
        "value": 3.266,
          "source": "adjusted to Russell and Weiskittel 2011 for B. papyrifera" ,
          "depends": "CDR_P2, CDR_P3" },
    "cdr_p2": {
        "value": 0.625,
          "source": "adjusted to Russell and Weiskittel 2011 for B. papyrifera" ,
          "depends": "CDR_P2, CDR_P3" },
    "cdr_p3": {
        "value": 0.0,
          "source": "adjusted to Russell and Weiskittel 2011 for B. papyrifera" ,
          "depends": "CDR_P2, CDR_P3" },
    "csr_ref": {
        "value": 1.8,
          "source": "Kangur et al. 2021 (predawn wp at lowest measured soil water content)" },
    "deciduous": {
        "value": true },
    "diammax": {
        "value": 0.4,
          "source": "Hyytiaelae long-term site measurements (pers. comm.)" ,
          "comments": "only for old (diameter dependent) branch fraction calculations" },
    "dleafshed": {
        "value": 315,
          "source": "Hyytiaelae litterfall data" },
    "dsap": {
        "value": 0.43,
          "source": "Johansson 2007",
          "comments": "Liepins and Rieksts-Riekstins 2013: 0.45 (0.41-0.52); Cai et al. 2013 (B.platyphylla): 0.53-0.55; Heraejaervi 2004 (B.pubescens): 0.51 (0.41-0.65); Ilomaeki et al. 2003: 0.48 (stem); Woodcock and Shier 2002 (B.papyrifera): 0.48-0.55; Martin et al. 1998 (B.lenta): 0.61; Brzeziecki and Kienast 1994 (cit. in Forrester et al. 2017 suppl): 0.525; Bossard 1984 (cit. in Droessler et al. 2015): 0.61" },
    "ef_iso": {
        "value": 0.1,
          "source": "Tarvainen et al. 2007",
          "comments": "Simon et al. 2006: 0.0; Simpson et al. 1999: 0.1" },
    "ef_mono": {
        "value": 3.8,
          "source": "Tarvainen et al. 2007" },
    "ef_monos": {
        "value": 3.0,
          "source": "Karl et al. 2009",
          "comments": "Hakola et al. 1998/99 (cit. in Lindfors et al. 2000 (late summer)): 5.8" },
    "ext": {
        "value": 0.44,
          "source": "Kurbatova et al. 2008",
          "comments": "Breda 2003: 0.57, Aubin et al. 2000: 0.44" },
    "fbraf_m": {
        "value": 0.2,
          "source": "Hyytiaelae long-term site measurements (pers. comm.)" ,
          "depends": "DIAMMAX" },
    "fbraf_y": {
        "value": 0.2,
          "source": "Hyytiaelae long-term site measurements (pers. comm.)" },
    "freegrowth": {
        "value": true },
    "fret_n": {
        "value": 0.69,
          "source": "Hagen-Thorn et al. 2006",
          "comments": "Jonczak et al. 2023: up to 0.72; Escudero et al. 1992: 0.69; Brandtberg et al. 2004: 0.65" },
    "gddfolend": {
        "value": 900.0,
          "source": "Kurbatova et al. 2008: 900 (= Aber et al. 1996 for broad leaved deciduous)" },
    "gddfolstart": {
        "value": 11.0,
          "source": "Linkosalo et al. 2006 (111)",
          "comments": "Kurbatova et al. 2008: 100; Linkosalo et al. 2008: 241; Rousi and Pusenius 2005: 85-125; literature - 100" },
    "gddwodend": {
        "value": 1600.0,
          "source": "adjusted to Schmitt et al. 2004" },
    "gddwodstart": {
        "value": 500.0,
          "source": "adjusted to Schmitt et al. 2004" },
    "gsmax": {
        "value": 405.0,
          "source": "Uttling et al. 2005 (361-449)",
          "comments": "Davidson et al. 2023 (preprint) (seasonal average, B. alleghaniensis): 247; Murray et al. 2020: 1127 (B. neoalaskana); Büker et al. 2007: 200; Sellin and Kupper 2005: 143-262 (depends on canopy position); Kruijt et al. 1999 (based on Rey 1997): 300" },
    "gsmin": {
        "value": 5.6,
          "source": "Kangur et al. 2021",
          "comments": "Davidson et al. 2023 seasonal average, B. alleghaniensis):3.3; Hoad et al. 1998 (cit. in Schuster et al. 2017): 3-8" },
    "h2oref_a": {
        "value": 0.25,
          "source": "Richardson et al. 2004 (B. papyrifera seedlings)" },
    "h2oref_gs": {
        "value": 0.45,
          "comments": "assumes that drought effects are the same on stomata and other processes",
          "depends": "H2OREF_A", "formula": "H2OREF_A" },
    "hd_exp": {
        "value": 5.0,
          "source": "adjusted to Schobers yield table (needs to be improved)",
          "depends": "HD_MIN, HD_MAX", "formula": "hd = HD_MIN + (HD_MAX-HD_MIN) * exp(-HD_EXP*dbh)",
          "comments": "HD_MAX: 85, HD_MIN: 25, HD_EXP: 5.0" },
    "hd_max": {
        "value": 85.0,
          "source": "adjusted to Schobers yield table (needs to be improved)",
          "depends": "HD_MIN, HD_EXP", "formula": "hd = HD_MIN + (HD_MAX-HD_MIN) * exp(-HD_EXP*dbh)",
          "comments": "HD_MAX: 85, HD_MIN: 25, HD_EXP: 5.0" },
    "hd_min": {
        "value": 25.0,
          "source": "adjusted to Schobers yield table (needs to be improved)",
          "depends": "HD_MAX, HD_EXP", "formula": "hd = HD_MIN+(HD_MAX-HD_MIN) * exp(-HD_EXP*dbh)",
          "comments": "HD_MAX: 85, HD_MIN: 25, HD_EXP: 5.0" },
    "hdj": {
        "value": 200000.0,
          "source": "Kattge and Knorr 2007",
          "comments": "if SDJ Kattge and Knorr then 200000; Leuning 2002: 154000",
          "depends": "SDJ" },
    "kc25": {
        "value": 178,
          "source": "Eichelmann et al. 2004: 178",
          "comments": "Wang et al. 2003: 299.5; Medlyn et al. 1999 (cit. in Eichelmann et al. 2004): 200-450; Rey and Jarvis 1997 (cit. in Eichelmann et al. 2004) 594" },
    "km20": {
        "value": 0.1,
          "comments": "estimated based on Hyytiala stand" },
    "ko25": {
        "value": 374.0,
          "source": "Eichelmann et al. 2004: 374",
          "comments": "Wang et al. 2003: 159.6; Aalto and Juurola 2001 (taken from Harley and Baldocchi 1995 for q.alba): 420" },
    "mfolopt": {
        "value": 0.22,
          "source": "Meir et al. 2002",
          "comments": "Ovington 1957 (cit. in Murakami et al. 2000): 0.24; Uri et al. 2007: 0.09-0.39;" },
    "mortcrowd": {
        "value": 3.8,
          "source": "approximated to Schobers yield table" },
    "nc_foliage_min": {
        "value": 0.002,
          "source": "estimated from Sullivan et al. 1996 (0.161 gN m-2, seedlings)",
          "comments": "Ingestad 1994 (cit. in Agren and Franklin 2003): 0.004" },
    "ncfolopt": {
        "value": 0.025,
          "source": "Alriksson and Eriksson 1998",
          "comments": "Thomas 2010 (B. alleghaniensis): 0.02-0.032; Kurbatova et al. 2008: 0.022; Uri et al. 2007: 0.025-0.039; Portsmuth and Niinemets 2006: ca. 2.0-3.0 (light independent); Uddling et al. 2005: max. 0.019 (not opt.); Brandtberg et al. 2004: 0.0247; Mediavilla and Escudero 2003: 0.0286; Perry and Hickman 2002: 0.022-0.034; Meir et al. 2002w: 0.029; Wang et al. 2000: 0.0198" },
    "nc_fineroots_max": {
        "value": 0.019,
          "source": "Yuan and Chen 2010",
          "comments": "Wang et al. 2000: 0.0033; Hoegberg et al. 1998: 0.014; Alriksson and Eriksson 1998: 0.0037" },
    "ncsapopt": {
        "value": 0.0017,
          "source": "Alriksson and Eriksson 1998",
          "comments": "Wang et al. 2000: 0.0013; Uri et al. 2007: 0.0022-0.0029" },
    "ndflush": {
        "value": 25,
          "source": "Zapater et al. 2013 (from graph)" ,
          "comments": "estimated from Richardson et al. 2006 (B. alleghaniensis): 32" },
    "ndmorta": {
        "value": 80,
          "source": "Zapater et al. 2013: 80 (from graph)" ,
          "comments": "Hyytiaelae litterfall data: 120" },
    "pa": {
        "value": 660.1e+06,
          "source": "Lehning et al. 2001 (f. Q.robur)",
          "comments": "needed for application of SIM module for BVOC emission" },
    "pfl": {
        "value": 1.3,
          "source": "estimated from Hagemeier et al. 2019",
          "comments": "supported in Meir et al. 2002: 1.3" },
    "psl": {
        "value": 0.93,
          "source": "approximated to Yuan and Chen 2010" },
    "psi_exp": {
        "value": 4.0,
        "source": "Li et al. 2020" ,
        "comments": "Zhang et al. 2018: 2.2" },
    "psi_ref": {
        "value": -2.2,
        "source": "Li et al. 2020",
        "comments": "Zhang et al. 2018: -1.71" },
    "psntmax": {
        "value": 45.0,
          "source": "Kurbatova et al. 2008 (= Aber et al. 1996 for broad leaved deciduous)",
          "comments": "Büker et al. 2007: 39" },
    "psntmin": {
        "value": 4.0,
          "source": "Kurbatova et al. 2008 (= Aber et al. 1996 for broad leaved deciduous)",
          "comments": "Büker et al. 2007: -5; Aalto und Juurola 2001: app. 3-9" },
    "psntopt": {
        "value": 24.0,
          "source": "Kurbatova et al. 2008 (= Aber et al. 1996 for broad leaved deciduous)",
          "comments": "Büker et al. 2007: 20; Aalto und Juurola 2001: 20-25" },
    "qjvc": {
        "value": 2.4,
          "source": "Aalto and Juurola 2001",
          "comments": "Wang et al. 2019 (B. occidentalis): 1.86; Su et al. 2009 (method6): 2.7; Eichelmann et al. 2004: 1.07; Medlyn et al. 2002: 1.69; Meir et al. 2002: 1.6; Dreyer et al. 2001: 1.94 (cit. in Kattge and Knorr 2007: 1.75); Kruijt et al. 1999 (based on Rey 1997): 2.5; Matyssek et al. 1991 (cit. in Wullschleger 1993): 3.11" },
    "qrd25": {
        "value": 0.017,
          "source": "Aalto and Juurola 2001",
          "comments": "Davidson et al. 2023 (preprint) (seasonal average, B. alleghaniensis): 0.0295; ; Su et al. 2009 (method6): 0.0025; Ito et al. 2006: 0.023; Dreyer et al. 2001: 0.031" },
    "qrf": {
        "value": 1.65,
          "source": "derived from Yuan and Chen 2010: 1.65",
          "comments": "Ding et al. 2019: app. 1 (0.234 gm-2); Johanssen 2007: app. 1-2; Aspelmeier and Leuschner 2006: app. 1.2-1.7; Van Hees and Clerkx 2003 (saplings, depends on resources): 0.3-0.63; Chertov et al. 1999: 0.5" },
    "qsf_p1": {
        "value": 0.178,
          "source": "adjusted to Tang et al. 2006 (B. alleghaniesis, mixed)",
          "comments": "Wang et al. 2019 (B. occidentalis): 0.177 (0.377 for few trees); Sellin and Kupper 2006 (branch basis): 0.5; Zhang et al. 2016(d) for sunlit and shaded branches: 1.7/1.9" },
    "qsf_p2": {
        "value": 0.0,
          "comments": "assumed since QSF_P1 is calculated without height influence" },
    "sdj": {
        "value": 633.6,
          "source": "Kattge and Knorr 2007",
          "comments": "Leuning 2002: 494.0; Dreyer et al. 2001: 1285" },
    "senescstart": {
        "value": 260,
          "source": "Zapater et al. 2013: 260 (from graph)",
          "comments": "Davidson et al. 2023 (B. alleghaniensis): 257; Kurbatova et al. 2008: 260" },
    "slamax": {
        "value": 17.0,
          "source": "Portsmuths and Niinemets 2006",
          "comments": "Hagemeier 2019: 24.0 (app. 50g m-2); Uddling et al. 2005: 17.4; Meir et al. 2002: 17; Oren et al. 1986: ca. 16.5" },
    "slamin": {
        "value": 7.0,
          "source": "Portsmuths and Niinemets 2006",
          "comments": "Hagemeier 2019: 11.0 (app. 90g m-2); Uddling et al. 2005: 12.1; Meir et al. 2002: ca. 9; Oren et al. 1986: ca. 8.0" },
    "slope_gsa": {
        "value": 9.4,
          "source": "Medlyn et al. 2001 (cit. in Baldocchi and Xu 2005)" },
    "slope_gsh2o": {
        "value": 8.5,
          "source": "Richardson et al. 2004 (B. papyrifera seedlings)" },
    "tap_p1": {
        "value": 1.8906,
          "source": "Dik 1984 (cit. in Zianis et al. 2005)" },
    "tap_p2": {
        "value": 0.26595,
          "source": "Dik 1984 (cit. in Zianis et al. 2005)" },
    "tap_p3": {
        "value": -1.07055,
          "source": "Dik 1984 (cit. in Zianis et al. 2005)" },
    "theta": {
        "value": 0.93,
          "source": "Aalto and Juurola 2001 (without consideration of decrease with temperature): 0.93; Kruijt et al. 1999 (based on Rey 1997): 0.9" },
    "tofrtbas": {
        "value": 0.0027,
          "source": "Ding et al. 2019: 0.0027",
          "comments": "Yuan and Chen 2010: 0.0033; Chertov et al. 1999: 0.0014" },
    "tosapmax": {
        "value": 0.0015,
          "comments": "assumed (2 years)" },
    "ugwdf": {
        "value": 0.12,
          "source": "Hyytiaelae long-term site measurements (pers. comm.)",
          "comments": "Konopka et al. 2023: 0.27" },
    "vcmax25": {
        "value": 68.3,
          "source": "Kattge and Knorr 2007",
          "comments": "Davidson et al. 2023 (preprint, B. alleghaniensis): app. 65 for high N (average 35.9); Wang et al. 2019 (B. occidentalis): 51-97; Su et al. 2009 (method6): 39.57; Bueker et al. 2007: 28-169(35); Wang unpublished after Kattge and Knorr 2007: 101.9; Ito et al. 2006: 70.6; Portsmuth and Niinemets 2006: ca. 58; Laisk et al. 2005: 83.0; Medlyn et al. 2002(b): 69.09 (fitted to simple arrhenius: 68.85) (for greenhouse plants, in open top chambers: 101.9/85.07); Meir et al. 2002 (cit. in Lloyd et al. 2010 (suppl)): 114; Aalto und Juurola 2001: 26.7; Dreyer et al. 2001: 70.5; ECOCRAFT database (Medlyn and Jarvis 1999): 44.7; Kruijt et al. 1999 (based on Rey 1997): 40; Matyssek et al. 1991 (cit. in Wullschleger 1993): 34" },
    "vpdref": {
        "value": 1.25,
          "source": "Uddling et al. 2005: 1.25",
          "comments": "Osonubi and Davies 1980: 4.0" },
    "wuecmax": {
        "value": 12.2,
          "source": "Maurer and Matyssek 1997: 12.2 (app. 700 molH2O/molCO2)",
          "comments": "Thomas 2010 (B. alleghaniensis): app. 6.0; Kurbatova et al. 2008: 13.9" },
    "wuecmin": {
        "value": 3.5,
          "source": "Maurer and Matyssek 1997: 3.5 (app. 200 molH2O/molCO2)",
          "comments": "Thomas 2010 (B. alleghaniensis): app. 3.5; Aspelmeier 2001 (theses): 3.0" }
  }
}
