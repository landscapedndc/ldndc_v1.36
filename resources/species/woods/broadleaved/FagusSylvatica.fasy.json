{
  "mnemonic": "fasy",
  "name": "Fagus Sylvatica",
  "namesupplement": "European beech",
  "group": "wood",

  "parameters": {
    "aeis": {
        "value": 51164.8,
          "source": "Lehning et al. 2001 (f. Q.robur)",
          "comments": "needed for application of SIM module for BVOC emission" },
    "aejm": {
        "value": 48090.0,
          "source": "Medlyn et al. 2002b",
          "comments": "Dreyer et al. 2001: 86900" },
    "aerd": {
        "value": 36500.0,
          "source": "Dreyer et al. 2001" },
    "aevc": {
        "value": 70627.0,
          "source": "Kattge and Knorr 2007",
          "comments": "Medlyn et al. 2002(b) 41380.0 (for greenhouse plants, in mini-ecosystems: 46810.0); Dreyer et al. 2001: 75400" },
    "alb": {
        "value": 0.05,
          "source": "Dufrene et al. 2005 (for PAR))",
          "comments": "Tutzet et al. 2014: 0.14; Otto et al. 2014: 0.15 (effective single scattering albedo); Welipolage et al. 2013 0.131; Gates 1980 (cit. in Breuer and Frede 2003): 0.2-0.25" },
    "alpha0_mt": {
        "value": 0.014,
          "source": "Lehning et al. 2001 (f. Q.robur, isoprene)",
          "comments": "needed for application of SIM module for monoterpne emission",
          "depends": "EF_MONO" },
    "amaxb": {
        "value": 62.6,
          "source": "Molina et al. 2015 (adjusted with AMAXA 5.73 for broadleaved trees)",
          "comments": "Kattge et al. 2009 (for temperate broad leaved): 290.81 / SLAMIN[vt] (app. 36)" },
    "basefolrespfrac": {
        "value": 0.085,
          "source": "Molina et al. 2015" },
    "cb": {
        "value": 0.3,
          "source": "Petritan et al. 2009" },
    "cdamp": {
        "value": 10.0,
          "source": "Holst et al. 2010" },
    "cdr_p1": {
        "value": 2.5,
          "source": "Sharma et al. 2017: 2.5",
          "comments": "adjusted to Hasenauer et al. 1997 and Condes and Sterba 2005: 3.142 ",
          "depends": "CDR_P2, CDR_P3" },
    "cdr_p2": {
        "value": 0.5,
          "source": "Sharma et al. 2017: ",
          "comments": "adjusted to Hasenauer et al. 1997 and Condes and Sterba 2005: 0.67 ",
          "depends": "CDR_P2, CDR_P3" },
    "cdr_p3": {
        "value": 0.0,
          "source": "according to Sharma et al. 2017, Hasenauer et al. 1997 and Condes and Sterba 2005",
          "depends": "CDR_P2, CDR_P3" },
    "cfcropt": {
        "value": 0.25,
          "source": "Meyer et al. 2011",
          "comments": "estimated for Tuttlingen site (mycorrhiza/ root relation)" },
    "cl_p1": {
        "value": 2.52,
          "source": "estimated from data of Bartelink 1997" ,
          "comments": "Lianes Revilla 2010: 2.6",
          "depends": "CL_P2, HREF" },
    "cl_p2": {
        "value": 0.85,
          "source": "estimated from data of Bartelink 1997" ,
          "comments": "Lianes Revilla 2010: 0.41",
          "depends": "CL_P1, HREF" },
    "csr_ref": {
        "value": -2.4,
          "source": "Kangur et al. 2021" ,
          "comments": "predawn wp at lowest measured soil water content" },
    "cwp_ref": {
        "value": 0.01,
	    "source": "Lemoine et al. 2002 (branches) (ks=5-10 [mmol m-2 MPa-1 s-1]",
	    "comments": "Sevanto et al. 2008, (ks=0.0145 [kg m-2 MPa-1 s-1] *18 *1000 *0.00046 mol MPa-1 m-2Leaf s-1(qsf for F.sylvatica with dbh 27cm)): 0.12??",
	    "comments": "Heath et al. 1997 (seedlings), (kl=110 [ug cm-2 MPa-1 s-1] *18 *0.000001 *10000 mol MPa-1 m-2Leaf s-1(qsf for F.sylvatica with dbh 27cm)): 19.8"},
    "dbranch": {
        "value": 0.6,
          "source": "estimated from Soroe biomass data" ,
          "comments": "runs of other sites indicate values between 0.3 and 0.8, probably decreasing with size" },
    "df_exp": {
        "value": 0.35,
          "comments": "estimated based on Hasenauer 1997, Schober 1979, Bolte et al. 2004" },
    "diammax": {
        "value": 0.7,
          "source": "Seidl et al. 2010",
          "comments": "only for old (diameter dependent) branch fraction calculations" },
    "dleafshed": {
        "value": 315,
          "source": "Holst et al. 2004",
          "comments": "Kutsch et al. 2005: ca. 310; adjusted to Wang et al. 2004 (Hesse): app. 320; Sabate et al. 2002: 251 and 287; D'Andrea et al. 2020 (Collelongo): 300" },
    "dragc": {
        "value": 0.03,
          "source": "Holst et al. 2010" },
    "dsap": {
        "value": 0.584,
          "source": "Grote et al. 2003",
          "comments": "Tutzet et al. 2017: 0.8; Paladinic et al. 2009: 0.69; Hoch et al. 2003: 0.67; Damesin et al. 2002: 0.636 (young trees); Van de Walle et al. 2001: 0.55-0.566 (age dependent); Beck 1996: 0.558; Brzeziecki and Kienast 1994 (cit. in Forrester et al. 2017 suppl): 0.585; Knigge and Schulz 1966 (cit. in Droessler et al. 2015): 0.66; Trendelenburg and Meyer-Wegelin 1955: 0.554" },
    "ef_iso": {
        "value": 0.0,
          "source": "Koenig et al. 1995" },
    "ef_mono": {
        "value": 21.0,
          "source": "Karl et al. 2009",
          "comments": "Schurgers et al. 2009: 10.0; Steinbrecher et al. 2009: 21.1; Smiatek and Steinbrecher 2006: 15.0; Dindorf et al. 2006: 3.1-12.9 (up to 26.9 with Schuh97 algorithm); Moukthar et al. 2005: 43.5; Solmon et al. 2004: 0.3; Simpson et al. 1999: 0.0" },
    "ef_monos": {
        "value": 0.28,
          "source": "Tollsten and Mueller 1996 (cit. in Moukhtar et al. 2006)",
          "comments": "Koenig 1995 (cit. in Moukhtar et al. 2006): 0.47" },
    "expl_nh4": {
        "value": 0.245,
          "source": "Molina et al. 2015" },
    "expl_no3": {
        "value": 0.301,
          "source": "Molina et al. 2015",
          "comments": "ratio found by Gessler et al. 1998 (beech nitrogen uptake (app. 1.4:1)" },
    "ext": {
        "value": 0.532,
          "source": "Molina et al. 2015",
          "comments": "Forrester et al. 2014: 0.45; Vitale et al. 2012: 0.71; Lebourgeois et al. 2005: 0.5; Davi et al. 2005: 0.65; Wang et al. 2004(c): gt 0.35 (seasonal variable); Breda 2003: 0.43-0.44; Granier et al. 2000: 0.4; Bartelink 1998: 0.5 (0.3-0.63); Waring and Schlesinger 1985 (cit. in BIOME-BGC parameterisation report): 0.4 (cit. in Pietsch et al. 2005: 0.6)" },
    "fbraf_m": {
        "value": 0.1,
          "source": "estimated from Granier et al. 2000",
          "comments": "Tutzet et al. 2017 (medium age): 0.151; Damesin et al. 2002 (cit. in Dufrene et al. 2005): 0.125; Hoffmann 1995: 0.1",
          "depends": "DIAMMAX" },
    "fbraf_y": {
        "value": 0.3,
          "source": "Skovsgaard and Nord-Larsen 2012: 0.3 (site 1416)" ,
          "comments": "Bartelink et al. 1997 (function): 0.5" },
    "ffacmax": {
        "value": 0.2,
          "source": "Hoch et al. 2003 (max. branch content from graph)" ,
          "comments": "Bauer et al. 1997: 0.13" },
    "folrelgromax": {
        "value": 2.0,
          "comments": "estimated (assuming twice the average regeneration capacity)" },
    "fret_n": {
        "value": 0.9,
          "source": "PRELIMINARY FIX, Wang et al. 2013: 0.7",
          "comments": "Molina et al. 2015: 0.52; Templer et al. 2005: unfertilized 0.7, fertilized 0.55; Hoffmann 1995: 0.2 (??); Staaf 1982: 0.7" },
    "frtalloc_base": {
        "value": 86.0,
          "source": "Molina et al. 2015" },
    "frtloss_scale": {
        "value": 2.423,
          "source": "Molina et al. 2015" },
    "fyield": {
        "value": 0.24,
          "source": "Molina et al. 2015, Hoffmann 1995" },
    "gddfolend": {
        "value": 521.3,
          "source": "Molina et al. 2015" },
    "gddfolstart": {
        "value": 184.4,
          "source": "Molina et al. 2015",
          "comments": "D'Andrea et al. 2020: 110; Holst et al. 2004: 255; Granier et al. 2007: 350; Davi et al. 2006: 450; Dufrene et al. 2008: 580 (base temp. = 1); Wang et al. 2004: app. 350" },
    "gddwodend": {
        "value": 1738.9,
          "source": "Molina et al. 2015" },
    "gddwodstart": {
        "value": 139.7,
          "source": "Molina et al. 2015" },
    "gsmax": {
        "value": 81.5,
          "source": "average value of ECOCRAFT database (Medlyn and Jarvis 1999)",
          "comments": "De Caceres et la. 2023: 340; Keel et al. 2007: 300; Büker et al. 2007: 340; Leuzinger and Körner 2007 (cit. in Meier and Scherer 2012): 610; Bergh et al. 2003: 300; Aranda et al. 2000: 265; Schaefer et al. 2000: 160-320 (depends on tree size); Koerner et al. 1979 (cit. in Breuer and Frede 2003): 280" },
    "gsmin": {
        "value": 4.0,
          "source": "De Caceres et la. 2023",
          "comments": "Kangur et al. 2021: 10.3; Duursma et al. 2019 (Fagales, from graph): 4.5; Schuster et al. 2017 (three references): 1.6-5.6" },
    "h2oref_a": {
        "value": 0.349,
          "source": "Molina et al. 2015",
          "comments": "Dufrene et al. 2005: 0.4; Granier et al. 2007: app. 0.35" },
    "h2oref_gs": {
        "value": 0.349,
          "comments": "assumes that drought effects are the same on stomata and other processes",
          "depends": "H2OREF_A", "formula": "H2OREF_A" },
    "hd_exp": {
        "value": 5.0,
          "source": "adjusted to various authors",
          "depends": "HD_MIN, HD_MAX", "formula": "hd = HD_MIN + (HD_MAX-HD_MIN) * exp(-HD_EXP*dbh)",
          "comments": "HD_MAX: 90, HD_MIN: 30, HD_EXP: 5" },
    "hd_max": {
        "value": 90.0,
          "source": "adjusted to various authors",
          "depends": "HD_MIN, HD_EXP", "formula": "hd = HD_MIN + (HD_MAX-HD_MIN) * exp(-HD_EXP*dbh)",
          "comments": "HD_MAX: 90, HD_MIN: 30, HD_EXP: 5" },
    "hd_min": {
        "value": 30.0,
          "source": "adjusted to various authors",
          "depends": "HD_MAX, HD_EXP", "formula": "hd = HD_MIN+(HD_MAX-HD_MIN) * exp(-HD_EXP*dbh)",
          "comments": "HD_MAX: 90, HD_MIN: 30, HD_EXP: 5" },
    "hdj": {
        "value": 200000.0,
          "source": "Kattge and Knorr 2007",
          "comments": "if SDJ Kattge and Knorr then 200000; Dreyer et al. 2007: 175000; Leuning 2002: 129000.0",
          "depends": "SDJ" },
    "href": {
        "value": 2.9,
          "source": "estimated from Bartelink 1997" ,
          "comments": "Lianes Revilla 2010: 5.8",
          "depends": "CL_P1, CL_P2" },
    "km20": {
        "value": 1.0,
          "comments": "estimated based on various beech stands (ranges from 0.55 to 3.0)" },
    "mfolopt": {
        "value": 0.26,
          "source": "Holst et al. 2004",
          "comments": "Molina et al. 2015: 0.332 (Pnet); Withington et al. 2006: 0.33; Leuschner et al. 2006: 0.29-0.39; Dufrene et al. 2005: 0.32; Cutini 2002: 0.23-0.46; Granier et al. 2000: 0.24; various authors from unpublished review of leaf mass: 0.21-0.33" },
    "mortcrowd": {
        "value": 5.5,
          "source": "approximated to Schobers yield table" },
    "mwfm": {
        "value": 0.0002,
          "source": "Nizinki and Saugier (cit. Dufrene et al. 2005)" ,
          "comments": "Klaassen et al. 1996 (cit. in Pietsch et al. 2005): 0.00034" },
    "mwwm": {
        "value": 0.0,
          "source": "assumed to be too smooth to hold any water",
          "comments": "estimated from Tuttlingen 2001-2004 data (appr. maximum effect in Tuttlingen): 0.000000013" },
    "nc_fineroots_max": {
        "value": 0.009,
          "source": "Molina et al. 2015",
          "comments": "Terzaghi et al. 2013: 0.008-0.011; Lang and Polle 2011: 0.009; Hobbie et al. 2010 (CN 51.5): 0.009; Jacobsen et al. 2003: 0.007; Templer and Dawson 2004: 0.010; Kastner-Maresch (cit. in Sonntag 1998): 0.008; van Praag et al. 1988 (cit. in Dufrene et al. 2005): 0.0099" },
    "ncfolopt": {
        "value": 0.0254,
          "source": "Mellert & Goettlein 2012 (average of luxury supply)",
          "comments": "Teglia et al. 2022: 0.023-0.027; Molina et al. 2015: 0.03; Wang et al. 2013: 0.027 (CN=19); Franzaring et al. 2010: 0.024; Verbeek et al. 2008: max 0.0234; Nahm et al. 2007: max. 0.035 (in spring); Sariyildiz and Anderson 2005: 0.0246; Aranda et al. 2004: 0.0214; Wang et al. 2003(d): 0.015; Meir et al. 2002: app. 0.028; Duquesnay et al. 2000: 0.0259+-0.0014; Sonntag 1997 (cit. in Sonntag 1998): 0.025; Bergmann 1993 (cit. in Thimonier et al. 2010): 0.019-0.025" },
    "ncsapopt": {
        "value": 0.001,
          "source": "Molina et al. 2015",
          "comments": "Templer et al. 2005: 0.0017; Meerts et al. 2002: 0.0015; Ceschia et al. 2002 (cit. in Dufrene et al. 2005): 0.0012" },
    "ndflush": {
        "value": 30,
          "source": "Holst et al. 2004",
          "comments": "Zapater et al. 2013: app. 40; Kutsch et al. 2005: ca. 15" },
    "ndmorta": {
        "value": 70,
          "source": "Holst et al. 2004",
          "comments": "Zapater et al. 2013: 60-70; Kutsch et al. 2005: app. 70" },
    "pa": {
        "value": 660.1e+06,
          "source": "Lehning et al. 2001 (f. Q.robur)",
          "comments": "needed for application of SIM module for BVOC emission" },
    "pfl": {
        "value": 1.8,
          "source": "estimated from Grote and Reiter 2004",
          "comments": "estimated from Hagemeier et al. 2019: 1.2; Vicca et al. 2016: 1.25" },
    "psl": {
        "value": 1.0,
          "source": "estimated from Gessler et al. 2005 (Tuttlingen)",
          "comments": "supported by measurements in Vielsalm under beech, pers. comm. Ariane Faures" },
    "psi_exp": {
        "value": 5.0,
        "source": "Knuever et al. 2022: 5.0",
        "comments": "Weithmann et al. 2022: 4.7; Rosner et al. 2019: 1.6; Zhang et al. 2018: 5.7; Barigah et al. 2013: 3.5; Urli et al. 2013: 8.0; Hacke et al. 1995: 5.0" },
    "psi_ref": {
        "value": -3.1,
        "source": "Knuever et al. 2022: -3.1",
        "comments": "Weithmann et al. 2022: -4.1; Rosner et al. 2019: -2.5; Zhang et al. 2018: -3.4; Barigah et al. 2013: -2.8; Urli et al. 2013: -3.9; Hacke et al. 1995: -2.9" },
    "psntmax": {
        "value": 45.1,
          "source": "Molina et al. 2015",
          "comments": "Büker et al. 2007: 39; Bergh et al. 2003: 35" },
    "psntmin": {
        "value": 4.45,
          "source": "Molina et al. 2015",
          "comments": "Büker et al. 2007: 9; Bergh et al. 2003: 5" },
    "psntopt": {
        "value": 34.5,
          "source": "Molina et al. 2015",
          "comments": "Büker et al. 2007: 32; Freeman 1998 (cit. in Bergh et al. 2003): 24" },
    "qjvc": {
        "value": 2.24,
          "source": "Yan et al. 2023",
          "comments": "De Caceres et la. 2023: 1.69; Verbeek et al. 2008: 1.8; Op de Beeck et al. 2007: 1.68; Davi et al. 2006: 2.2; Meir et al. 2002: 2.1; Dreyer et al. 2001: 1.92; Liozon et al. 2000 (cit. in Davi et al. 2006): 2.2; Liozon et al. 2000 (cit. in Dufrene et al. 2005): 2.1" },
    "qhrd": {
        "value": 0.15,
          "source": "Granier et al. 2007" },
    "qrd25": {
        "value": 0.0149,
          "source": "Yan et al. 2023",
          "comments": "Verbeek et al. 2008: 0.0124; Dreyer et al. 2001: 0.029" },
    "qrf": {
        "value": 0.6,
          "source": " estimated to obtain a fine root /foliage ratio in summer approximated to 330 mFrt (Leuschner et al.) and 260 MFOLOPT (Holst et al.) gDW m-2)",
          "comments": "Pietsch et al. 2005: 0.545",
          "comments": "fine root biomass for evaluation: Lwila et al. 2022: 460 (avg. from 4 sites, 610 (northern), 290 (southern); Hertel et al. 2013: 650-850 gDW m-2 on a drought gradient; Finer et al. 2007: 0.389 (+-0.206) kgDW m-2; Bolte et al. 2006: 412 gDW m-2; Dufrene et al. 2005: 342 gDW m-2 (0.171 gC): 1.07; Leuschner et al. 2004: 330 (110-460) gDW m-2; Leuschner et al. 1998: 330-450 gDW m-2; Ellenberg et al. 1986: 257-361 gDW m-2",
          "comments": "relation fine roots to foliage biomass for evaluation: Van Hees and Clerkx 2003 (saplings): 1.34; Claus and George 2003 (cit. in Mund et al. 2010): 0.9",
          "depends": "MFOLOPT, TOFRTBAS" },
    "qsf_p1": {
        "value": 1.25,
          "source": "adjusted to Schaefer et al. 2000" },
    "qsf_p2": {
        "value": 0.123,
          "source": "adjusted to Schaefer et al. 2000",
          "comments": "Vincente et al. 0.5-1.5, depending on site aridity" },
    "qwodfolmin": {
        "value": 3.052,
          "source": "Molina et al. 2015" },
    "rbuddem": {
        "value": 5.0,
          "comments": "estimated to get rid of oscillations" },
    "respq10": {
        "value": 1.693,
          "source": "Molina et al. 2015",
          "comments": "Davi et al. 2006: 1.84-2.3; Bergh et al. 2003: 2.2; Damesin et al. 2002: 1.8" },
    "rootmrespfrac": {
        "value": 0.662,
          "source": "Molina et al. 2015" },
    "sdj": {
        "value": 638.4,
          "source": "Kattge and Knorr 2007",
          "comments": "Leuning 2002: 420.0; ; Dreyer et al. 2007: 559" },
    "senescstart": {
        "value": 208.9,
          "source": "Molina et al. 2015" },
    "slamax": {
        "value": 31.0,
          "source": "Aranda et al. 2004",
          "comments": "Paz-Dyderska et al. 2020: 30.4; Hagemeier 2019: 45.0 (app. 20g m-2); Rajsnerova et al. 2015: app. 28.5 (depends on site); Montpied et al. 2009: app. 43 (23 gm-2); Davi et al. 2008: 17.2-40.0; Thornton et al. 2007: app. 45 (75 m2 kgC-1); Eriksson et al. 2005: 30.9; Meir et al. 2002: 31; Bartelink 1998: 34.0" },
    "slamin": {
        "value": 11.0,
          "source": "Aranda et al. 2004",
          "comments": "Paz-Dyderska et al. 2020: 21.0; Hagemeier 2019: 11.0 (app. 90g m-2); Rajsnerova et al. 2015: app. 10.5 (depends on site); Montpied et al. 2009: app. 9 (110 gm-2); Davi et al. 2008: 8.7-10.5; Thornton et al. 2007: app. 15  (25 m2 kgC-1); Eriksson et al. 2005: 10.3; Meir et al. 2002: 11; Bartelink 1998: 8.0; Dufrene et al. 2005: 9.9" },
    "slope_gsa": {
        "value": 11.8,
          "source": "Medlyn et al. 2001 (cit. in Baldocchi and Xu 2005: 12.7; cit. in Dufrene et al. 2005: 11.8)",
          "comments": "Davi et al. 2006: 6.2-11.8" },
    "slope_gsco2": {
        "value": 0.61,
          "source": "Medlyn et al. 2001" },
    "slope_gsh2o": {
        "value": 3.3,
          "source": "Granier et al. 2000 (AFM)" },
    "tap_p1": {
        "value": 1.55448,
          "source": "Dik 1984 (cit. in Zianis et al. 2005)" },
    "tap_p2": {
        "value": 1.5588,
          "source": "Dik 1984 (cit. in Zianis et al. 2005)" },
    "tap_p3": {
        "value": -3.57875,
          "source": "Dik 1984 (cit. in Zianis et al. 2005)" },
    "tau": {
        "value": 0.11,
          "source": "Dufrene et al. 2005 (for PAR)",
          "comments": "Forrester et al. 2014: 0.05 (for PAR); Gates 1980 (cit. in Breuer and Frede 2003): 0.2-0.25" },
    "theta": {
          "value": 0.882,
          "source": "Yan et al. 2023" },
    "tofrtbas": {
        "value": 0.0037,
          "source": "Mainiero et al. 2010",
          "comments": "Zeleznik et al. 2019: 0.001-0.0016; Withington et al. 2006: 0.0048; Stober et al. 2000 (cit. in Persson 2007): 0.0047; Hoffmann 1995: 0.0027; Bauhus and Bartsch 1996 (cit. in Dufrene et al. 2005): 0.0027" },
    "tosapmax": {
        "value": 0.00003,
          "source": "Hillis 1987 (cit. in Bartelink 1997 as 80-100 years)" },
    "ugwdf": {
        "value": 0.158,
          "source": "Wu et al. 2013 (avg. 2006-2010)",
          "comments": "Tutzet et al. 2017: 0.184; Granier et al. 2000: 0.24; Bolte et al. 2004: app. 0.09; Ellenberg et al. 1981 (cit. in Pietzsch et al. 2005): 0.12" },
    "us_nh4": {
        "value": 0.00082,
          "source": "Gessler et al. 1998 (1.10 umol gFW-1)",
          "comments": "Gessler et al. 2005: 0.0006 (0.8 umol gFW-1 h-1); Hoegberg et al. 1998 (cit. in Bassirirad 2000): 0.00114 (3.4 umol gDW-1 h-1); Goeransson 2006(b): 0.00026 (ca. 0.35 umol gFW-1 h-1)" },
    "us_no3": {
        "value": 0.00025,
          "source": "Gessler et al. 1998 (0.33 umol gFW-1)",
          "comments": "Gessler et al. 2005: 0.00007 (0.1 umol gFW-1 h-1); Hoegberg et al. 1998 (cit. in Bassirirad 2000): 0.00020 (0.6 umol gDW-1 h-1)" },
    "vcmax25": {
        "value": 59.6,
          "source": "Yan et al. 2023",
          "comments": "De Caceres et la. 2023: 94.5; Verbeek et al. 2008: 29-50; Kattge and Knorr 2007: 62.8; Op de Beeck et al. 2007: 58.4; Wang et al. 2003(d): 29.7; Bergh et al. 2003: 82.9-110.5; Medlyn et al. 2002(b): 63.83 (fitted to simple arrhenius: 60.95); Meir et al. 2002: 64; Dreyer et al. 2001: 66.3" },
    "vpdref": {
        "value": 3.4,
          "source": "Medlyn et al. 2001 (cit. in Strassemeyer and Forstreuter 1998)",
          "comments": "Kersteins 1995: 3.0" },
    "woodmrespa": {
        "value": 0.166,
          "source": "Molina et al. 2015" },
    "wuecmax": {
        "value": 12.3,
          "source": "Molina et al. 2015: 12.3",
          "comments": "De Caceres et la. 2023: 7.92; van Meeningen et al. 2016: 5.3-6.3; Grote et al. 2011: 3; report on drought stress effects on beech ecotypes: 3-6; LWF data: app. 3" },
    "wuecmin": {
        "value": 12.3,
          "depends": "WUECMAX", "formula": "WUECMIN=WUECMAX" }
  }
}
