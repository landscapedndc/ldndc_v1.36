{
  "mnemonic": "woods",
  "name": "Trees",
  "group": "wood",

  "parameters": {
    "aejm": {
        "value": 47170.0,
          "source": "Wang et al. 2003 (GCB)",
          "comments": "for pines, spruces and beech" },
    "aekc": {
        "value": 65000.0,
          "source": "Wang et al. 2003 (GCB)",
          "comments": "for pines, spruces and beech" },
    "aeko": {
        "value": 36000.0,
          "source": "Wang et al. 2003 (GCB)",
          "comments": "for pines, spruces and beech" },
    "aerd": {
        "value": 47170.0,
          "source": "Wang et al. 2003 (GCB)",
          "comments": "for pines, spruces and beech" },
    "aevc": {
        "value": 47170.0,
          "source": "Wang et al. 2003 (GCB)",
          "comments": "for pines, spruces and beech" },
    "amaxfrac": {
        "value": 0.76,
          "source": "Aber et al. 1995, 1996",
          "comments": "for coniferous as well as deciduous trees" },
    "basefolrespfrac": {
        "value": 0.1,
          "source": "Aber and Federer 1992, Aber et al. 1995, 1996",
          "comments": "for coniferous as well as deciduous trees" },
    "cb": {
        "value": 0.5,
          "comments": "assumption that the crown covers half the tree height" },
    "cwp_ref": {
          "description": "reference (=maximum) value for the (leaf area-normalized) specific xylem conductance (mol MPa-1 m-2Leaf s-1)",
          "value": 0.08,
          "comments": "assumed similar to PIHA" },
    "diammax": {
        "value": 0.4,
          "source": "assumption" ,
          "comments": "used for old (diameter dependent) calculation of branch fraction together with FBRAF_M and FBRAF_Y" },
    "dvpd1": {
        "value": 0.05,
          "source": "Aber et al. 1995, 1996" },
    "dvpd2": {
        "value": 2.0,
          "source": "Aber et al. 1995, 1996" },
    "fbraf_m": {
        "value": 0.2,
          "source": "assumption" },
    "fbraf_y": {
        "value": 0.5,
          "source": "assumption" },
    "ffacmax": {
        "value": 0.16,
          "source": "Jones et al. 2020 (for tropical forests)" ,
          "comments": "Shibata et al. 2011: 0.135 (average from 14 species in a Japanese temperate forest)" },
    "folrelgromax": {
        "value": 0.95,
          "source": "PNET-2 download",
          "comments": "Aber et al. 1995: 0.3" },
    "fret_n": {
        "value": 0.484,
          "source": "Yan et al. 2018 (various woody species" },
    "frtalloc_base": {
        "value": 0.0,
          "source": "Aber et al. 1995",
          "comments": "Aber and Federer 1992: 130.0" },
    "frtalloc_rel": {
        "value": 2.0,
          "source": "Aber et al. 1995",
          "comments": "Aber and Federer 1992: 1.92" },
    "fyield": {
        "value": 0.25,
          "source": "Thornley and Cannell 2000: 0.25",
          "comments": "Aber and Federer 1992: 0.25 Aber et al. 1995:0.25" },
    "gddfolend": {
        "value": 1100.0,
          "source": "PNET-2 download" },
    "gddfolstart": {
        "value": 150.0,
          "source": "PNET-2 download (-100)" },
    "gddwodend": {
        "value": 1100.0,
          "depends": "GDDFOLEND", "formula": "GDDFOLEND" },
    "gddwodstart": {
        "value": 250.0,
          "depends": "GDDFOLSTART", "formula": "GDDFOLSTART+100" },
    "gsmin": {
        "value": 3.9,
          "source": "Duursma et al. 2019 (from graph S_A und S_B)" },
    "h2oref_a": {
        "value": 0.4,
          "comments": "assumes that drought effects are the same on stomata and other processes",
          "depends": "H2OREF_GS", "formula": "H2OREF_GS" },
    "h2oref_gs": {
        "value": 0.4,
          "source": "Granier et al. 2000",
          "comments": "for forests, approximated from graph" },
    "halfsat": {
        "value": 200.0,
          "source": "Aber et al. 1995, 1996",
          "comments": "for coniferous as well as deciduous trees" },
    "hdj": {
        "value": 200000.0,
          "source": "Wang et al. 2003 (GCB)",
          "comments": "for pines, spruces and beech" },
    "height_max": {
        "value": 100.0,
          "comments": "heighest trees of the world - should not be used for trees anyway" },
    "href": {
        "value": 4.0,
          "comments": "assumes full foliage can be grown only at a crown height of at least 4 m" },
    "kc25": {
        "value": 299.5,
          "source": "Wang et al. 2003 (GCB)",
          "comments": "for pines, spruces and beech" },
    "ko25": {
        "value": 159.6,
          "source": "Wang et al. 2003 (GCB)(for pines, spruces and beech)" },
    "mfolopt": {
        "value": 0.52,
          "source": "Neumann et al. 2018" },
    "mortcrowd": {
        "value": 2.5,
          "comments": "assumed limit for crown coverage to affect mortality" },
    "mortnorm": {
        "value": 0.002,
          "source": "Bossel 1994" },
    "mwwm": {
        "value": 0.000000001,
          "comments": "assumed maximum water fraction on tree barks" },
    "ncsapopt": {
        "value": 0.001,
          "comments": "assumed for woody species" },
    "pexs": {
        "value": 0.05,
          "source": "Zobitz et al. 2008 (estimated in alpine forests)" },
    "psl": {
        "value": 0.9,
          "sourse": "Jackson et al. 1996" },
    "qhrd": {
        "value": 0.36,
          "source": "Tumber-Davila et al. 2022 (0.08+0.28x for woody species)" },
    "qjvc": {
        "value": 2.18,
          "source": "Ainsworth and Rogers 2007 (for trees)",
          "comments": "Medlyn et al. 2002(b): app. 1.67 (almost all trees)" },
    "qsf_p1": {
        "value": 4.2,
          "source": "Togashi et al. 2015 (2363 m2 m-2, for Australien trees)" },
    "qsf_p2": {
        "value": 0.0,
          "comments": "assumed since QSF_P1 is calculated without height influence" },
    "qrf": {
        "value": 0.6,
          "comments": "Neumann et al. 2020 (average root: 332; average foliate 547 g m-2)" },
    "respq10": {
        "value": 2.0,
          "source": "Aber et al. 1995",
          "comments": "for coniferous as well as deciduous trees" },
    "rootmrespfrac": {
        "value": 1.0,
          "source": "Aber et al. 1995",
          "comments": "Aber and Federer 1992: 0.5" },
    "sdj": {
        "value": 656.0,
          "source": "Wang et al. 2003(GCB) (various temperate trees)" },
    "senescstart": {
        "value": 270,
          "source": "Aber et al. 1996",
          "comments": "for coniferous as well as deciduous trees" },
    "slope_gsa": {
        "value": 10.4,
          "source": "Baldocchi and Xu 2005",
          "comments": "for all non-C4 species = 10.4",
          "depends": "C4_TYPE" },
    "tofrtbas": {
        "value": 0.0021,
          "source": "Neumann et al. 2020 (production: 250, average mass: 332)" },
    "ugwdf": {
        "value": 0.26,
          "source": "Easdale et al. 2019 (=root:shoot ratio, southern hemishphere only)" },
    "vcmax25": {
        "value": 57.0,
          "source": "Ainsworth and Rogers 2007: 57 (trees)" ,
          "comments": "Scafaro et al. 2017: 45 (average between tropical and temperate trees)" },
    "woodmrespa": {
        "value": 0.07,
          "source": "Aber et al. 1995",
          "comments": "Aber and Federer 1992: 0.35" },
    "wuecmax": {
        "value": 10.9,
          "source": "Aber et al. 1995",
          "comments": "Aber and Federer 1992: 10.9" },
    "wuecmin": {
        "value": 10.9,
          "depends": "WUECMAX", "formula": "WUECMAX" },
    "zrtmc": {
        "value": 10.0,
          "comments": "assumed for woody species, currently not used in forest models" }
  }
}
