{
  "mnemonic": "piab",
  "name": "Picea Abies",
  "namesupplement": "Norway spruce",
  "group": "wood",

  "parameters": {
    "aekc": {
        "value": 65000.0,
          "source": "Falge et al. 1997" },
    "aeko": {
        "value": 36000.0,
          "source": "Falge et al. 1997" },
    "aerd": {
        "value": 63500.0,
          "source": "Falge et al. 1997" },
    "aevc": {
        "value": 75750.0,
          "source": "Falge et al. 1997" },
    "alb": {
        "value": 0.06,
          "source": "Jarvis et al. 1976 (cit. in Breuer and Frede 2003) (0.05-0.06)",
          "comments": "Kuuisinen et al. 2013 (cit. in Thom et al. 2017): 0.102; Koivusalo and Kokkonen 2002: 0.18" },
    "amaxa": {
        "value": 9.3,
          "source": "Kurbatova et al. 2008: 9.3; Aber et al. 1996: 5.3; Postek et al. 1995: 2.25; Andersson et al. 2002: 24.7 (as AMAXA[vt] value but indicated as umol m-2 s-1?)" },
    "amaxb": {
        "value": 23.3,
          "source": "Molina et al. 2015",
          "comments": "Kurbatova et al. 2008: 21.5; Andersson et al. 2002: 2.33 (as AMAXB value but indicated as umol m-2 s-1?); Bergh et al. 1998: 38.9 (10.6 umol m-2 s-1); Roberntz and Stockfors 1998: ca. 75.6 (ca. 27.0 umol m-2 s-1, with 0.35 mgN cm-2 (app. 2.5perc.) and assuming SLA = 7); Postek et al. 1995: 0" },
    "basefolrespfrac": {
        "value": 0.133,
          "source": "Molina et al. 2015",
          "comments": "adjusted to Gruenwald and Bernhofer 2007: 0.04; Kurbatova et al. 2008: 0.075" },
    "cb": {
        "value": 0.55,
          "source": "Kantola and Maekelae 2006 (based on centre of figure 1)",
          "comments": "Kantola and Maekelae 2004: 0.486 (average from table for mature trees); Grote and Reiter 2004: 0.51" },
    "cdr_p1": {
        "value": 2.647,
          "source": "adjusted to Hasenauer et al. 1997",
          "depends": "CDR_P2, CDR_P3" },
    "cdr_p2": {
        "value": 0.646,
          "source": "adjusted to Hasenauer et al. 1997",
          "depends": "CDR_P1, CDR_P3" },
    "cdr_p3": {
        "value": 0.0,
          "source": "adjusted to Hasenauer et al. 1997",
          "depends": "CDR_P1, CDR_P2" },
    "cfcropt": {
        "value": 0.25,
          "source": "Meyer et al. 2012" },
    "cl_p1": {
        "value": 1.8,
          "source": "estimated from data of Thorpe et al. 2010 ",
          "depends": "CL_P2, HREF" },
    "cl_p2": {
        "value": 1.0,
          "source": "estimated from data of Thorpe et al. 2010 ",
          "depends": "CL_P1, HREF" },
    "csr_ref": {
        "value": -1.9,
        "comments": "developed based on plc curve and assumptions" },
    "ct_is": {
        "value": 33.25,
          "source": "adjusted to data in Zimmer et al. 2000 (f. Quercus robur)" },
    "ct_mt": {
        "value": 17.86,
          "source": "adjusted to data Fischbach 1999 (f. Quercus ilex)" },
    "dbranch": {
        "value": 0.35,
          "comments": "estimated based on site data Bily Criz" },
    "df_exp": {
        "value": 0.4,
          "comments": "estimated based on Hasenauer 1997, Roehle 1991" },
    "diammax": {
        "value": 0.4,
          "source": "Bossel 1994",
          "comments": "only for old (diameter dependent) branch fraction calculations" },
    "dleafshed": {
        "value": 1825,
          "source": "Gower et al. 1993: 5-6 years",
          "comments": "Withington et al. 2006: 8-9 years; Muukonen and Lehtonen 2004: 9 years; Bossel and Schaefer 1989: 6 years" },
    "ds_is": {
        "value": 873.0,
          "source": "adjusted to data in Zimmer et al. 2000 (f. Quercus robur)" },
    "ds_mt": {
        "value": 895.0,
          "source": "adjusted to data Fischbach 1999 (f. Quercus ilex)" },
    "dsap": {
        "value": 0.41,
          "source": "Weis et al. 2009",
          "comments": "Kantola and Maekelae 2006: 0.374(old)-0.425(young); Wirth et al. 2004: 0.377; Hoch et al. 2003: 0.47; Wilhelmsson et al. 2002: 0.383; Mund et al. 2002: 0.36-0.404; Joosten and Schulte 2002: 0.368 (0.32-0.42); Bossel 1994 (0.2 volumetric carbon content): 0.41; Brzeziecki and Kienast 1994 (cit. in Forrester et al. 2017 suppl): 0.37; Cerny 1990: 0.423; Knigge and Schulz 1966 (cit. in Droessler et al. 2015): 0.43" },
    "ef_iso": {
        "value": 1.0,
          "source": "Karl et al. 2009",
          "comments": "Simon et al. 2006: 1.0; Grabmer et al. 2006: 1.7; Solmon et al. 2004: 1.0; Simpson et al. 1999: 3.0; Steinbrecher et al. 1999 (cit. in Lindfors et al. 2000): 1.0; Kesselmeier and Staudt 1999: 0.2-1.2 (4 sources); Simpson et al. 1995: 1.75; Guenther 1999: 2.0" },
    "ef_mono": {
        "value": 2.1,
          "source": "Karl et al. 2009",
          "comments": "Simon et al. 2006: 1.4; Schurgers et al. 2009: 3.0; Smiatek and Steinbrecher 2006: 3.52; Solmon et al. 2004: 4.5; Stewart et al. 2003: 4.0; Janson et al. 2003: 1.0; Hakola et al. 2003: 1.0; Simpson et al. 1999: 3.0; Kesselmeier and Staudt 1999: 0.6-31 (4 sources); Guenther 1999: 3.0" },
    "ef_monos": {
        "value": 0.4,
          "source": "Karl et al. 2009",
          "comments": "Tarvainen et al. 2007: 0.8; Steinbrecher et al. 1999 (cit. in Lindfors et al. 2000): 1.5; Grabmer et al. 2006: 0.5 but with other empirical function" },
    "expl_nh4": {
        "value": 0.306,
          "source": "Molina et al. 2015",
          "comments": "adjusted to several spruce sites; ratio derived from Gessler et al. 1998 (NH4:NO3 app. 17:1)" },
    "expl_no3": {
        "value": 0.189,
          "source": "Molina et al. 2015",
          "comments": "ratio derived from Gessler et al. 1998 (NH4:NO3 app. 17:1)" },
    "ext": {
        "value": 0.42,
          "source": "Forrester et al. 2014",
          "comments": "Molina et al. 2015: 0.632; Dewar et al. 2012: 0.43; Weisberg et al. 2005: 0.5; Breda 2003: 0.28-0.37; Sampson and Allen 1998 (cit. in Pietsch et al. 2005): 0.67; Cienciala et al. 1998 (pine/spruce): 0.5; Postek et al. 1995: 0.4" },
    "fbraf_m": {
        "value": 0.08,
          "source": "Bossel 1994: 0.08",
          "comments": "Wirth et al 2004" ,
          "depends": "DIAMMAX" },
    "fbraf_y": {
        "value": 0.7,
          "source": "approximated to Norway spruces in Hyytiala mixed stand" },
    "ffacmax": {
        "value": 0.175,
          "source": "Brunner et al. 2002",
          "comments": "Hoch et al. 2003 (max. for branches) 0.1 (max. 0.25 for needles); Bergh et al. 1998: 0.3" },
    "fret_n": {
        "value": 0.66,
          "source": "Oren et al. 1988(Oeco)",
          "comments": "Molina et al. 2015: 0.42; Berger et al. 2009: 0.3; Bauer et al. 1997: 0.5; Bossel and Schaefer 1989: 0.15; Warfinge et al. 1998(b) (ref. to Kreutzer 1979 and Beier and Rasmussen 1993): 0.24" },
    "frtalloc_base": {
        "value": 17.7,
          "source": "Molina et al. 2015" },
    "frtloss_scale": {
        "value": 5.689,
          "source": "Molina et al. 2015" },
    "fyield": {
        "value": 0.214,
          "source": "Molina et al. 2015",
          "comments": "Hoffmann 1995: 0.2" },
    "gddfolend": {
        "value": 1257.7,
          "source": "Molina et al. 2015",
          "comments": "Kurbatova et al. 2008: 1100 (orginial Pnet-N-DNDC value); Andersson et al. 2002: 1350" },
    "gddfolstart": {
        "value": 311.3,
          "source": "Molina et al. 2015",
          "comments": "Kurbatova et al. 2008: 250 (orginial Pnet-N-DNDC value); Andersson et al. 2002: 350; Bergh et al. 1998: 290; (Roetzer et al. 2004: avg. appearance at day 135)" },
    "gddwodend": {
        "value": 1012.9,
          "source": "Molina et al. 2015",
          "comments": "Kurbatova et al. 2008: 1100" },
    "gddwodstart": {
        "value": 256.9,
          "source": "Molina et al. 2015",
          "comments": "Kurbatova et al. 2008: 250" },
    "gsmax": {
        "value": 125.0,
          "source": "Sellin and Kupper 2004",
          "comments": "Petek-Petrik et al. 2023: 100; database of Breuer and Frede 2003: 50-190 (based on Oren and Zimmermann 1988, Zimmermann et al. 1988, Falge et al. 1996, Lu et al. 1995); Dobson et al. 1990: max. 120" },
    "gsmin": {
        "value": 3.5,
          "source": "Schuster et al. 2017 (0.6-6.3)" },
    "h2oref_a": {
        "value": 0.295,
          "source": "Molina et al. 2015",
          "comments": "Havranek and Benecke 1978: 0.25 (approximated from graph)" },
    "h2oref_gs": {
        "value": 0.227,
          "source": "Lagergren and Lindroth 2001 (tree conductance, whole soil)",
          "comments": "Ewers et al. 2001: close to 1, except for fertilized plots" },
    "ha_is": {
        "value": 83129.0,
          "source": "Niinemets et al. 1999 (f. Quercus)" },
    "ha_mt": {
        "value": 45000.0,
          "source": "adjusted to data Fischbach 1999 (f. Quercus ilex)" },
    "halfsat": {
        "value": 200.0,
          "source": "Kurbatova et al. 2008",
          "comments": "Roberntz and Stockfors 1998: 200; Postek et al. 1995: 70" },
    "hd_exp": {
        "value": 6.0,
          "source": "adjusted to various authors",
          "depends": "HD_MIN, HD_MAX", "formula": "hd = HD_MIN + (HD_MAX-HD_MIN) * exp(-HD_EXP*dbh)",
          "comments": "HD_MAX: 80, HD_MIN: 40, HD_EXP: 6.0" },
    "hd_max": {
        "value": 80.0,
          "source": "adjusted to various authors",
          "depends": "HD_MIN, HD_EXP", "formula": "hd = HD_MIN + (HD_MAX-HD_MIN) * exp(-HD_EXP*dbh)",
          "comments": "HD_MAX: 80, HD_MIN: 40, HD_EXP: 6.0" },
    "hd_min": {
        "value": 40.0,
          "source": "adjusted to various authors",
          "depends": "HD_MAX, HD_EXP", "formula": "hd = HD_MIN+(HD_MAX-HD_MIN) * exp(-HD_EXP*dbh)",
          "comments": "HD_MAX: 80, HD_MIN: 40, HD_EXP: 6.0" },
    "href": {
        "value": 4.0,
          "source": "estimated from data of Thorpe et al. 2010",
          "depends": "CL_P2, CL_P1" },
    "km20": {
        "value": 0.2,
          "comments": "estimated based on various spruce stands" },
    "km_gdps_dmadp": {
        "value": 16.7,
          "source": "Tholl et al. 2001 (f. Abies alba)",
          "comments": "Tholl et al. 2004: 24.1 (f. A. majus); Clastre et al.1993: 56.8 (f. Vitis vinifera)" },
    "km_gdps_idp": {
        "value": 14.3,
          "source": "Tholl et al. 2001 (f. Abies alba)",
          "comments": "Tholl et al. 2004: 8.3 (f. A. majus); Clastre et al.1993: 8.5 (f. Vitis vinifera)" },
    "km_is": {
        "value": 530.0,
          "source": "Lehning et al. 1999 (f. Quercus robur)" },
    "km_mt": {
        "value": 0.7,
          "source": "Luecker et al. 2002 (b-pinen synthase f. Citrus limon): 0.7-3.1",
          "comments": "Fischbach et al. 2000: 17.9" },
    "mfolopt": {
        "value": 1.583,
          "source": "Molina et al. 2015",
          "comments": "Karpechko et al. 2022: 0.964; Meir et al. 2002: 1.24; Mund 1996: 1.66 (in July); Gower et al. 1993: 3.05; Droste zu Huelshoff 1969: 1.59; various authors cited in unpublished leaf mass review: 0.91-2.46" },
    "mortcrowd": {
        "value": 3.0,
          "source": "approximated to Hoeglwald site" },
    "mwfm": {
        "value": 0.00036,
          "source": "Pietsch et al. 2005",
          "comments": "Cienciala et al. 1998 (pine/spruce): 0.00025; Postek et al. 1995: 0.00015; Bringfelt and Lindroth 1987: 0.00034" },
    "nc_fineroots_max": {
        "value": 0.011,
          "source": "Jacobsen et al. 2003",
          "comments": "Molina et al. 2015: 0.02; Hobbie et al. 2010 (CN 53.3): 0.0084; Withington et al 2006: 0.010 (0.0053 g gC-1); Hoegberg et al. 2002: 0.015; Widen and Majdi 2001: 0.0053-0.0085 (prob. g gC-1); Hoegberg et al. 1998: ca. 0.020" },
    "ncfolopt": {
        "value": 0.0187,
          "source": "Mellert & Goettlein 2012 (average of luxury supply)",
          "comments": "Molina et al. 2015: 0.016; Dewar et al. 2012: 0.0145; Weis et al. 2009 (for first year needles): 0.017; Ukonmaanho et al. 2008: 0.0123; Wang et al. 2003(d): 0.015; Hoegberg et al. 2002: 0.017; Meir et al. 2002: app. 0.02; Grassi and Bagnaresi 2001: 0.011; Fischer et al. 2000: 0.016; Alriksson adn Eriksson 1998: 0.011; Warfinge et al. 1998(b) (ref. to Kreutzer 1979 and Beier and Rasmussen 1993): 0.014; Beier et al. 1995: lt 0.012; Berger et al. 2009: 0.0132; Bergmann 1993 (cit. in Thimonier et al. 2010): 0.014-0.017; Gower et al. 1993: 0.025 (1.78 mmol g-1); Gundersen 1998: 0.014" },
    "ncsapopt": {
        "value": 0.0007,
          "source": "Cihak and Veypustkova 2023",
          "comments": "Molina et al. 2015: 0.001; Martin et al. 2015: 0.00003; Warfinge et al. 1998(b) (ref. to Kreutzer 1979 and Beier and Rasmussen 1993): 0.001; Ukonmaanho et al. 2008: 0.001; Meerts et al. 2002: 0.0016" },
    "ndflush": {
        "value": 90,
          "source": "Bergh et al. 1998" },
    "ndmorta": {
        "value": 590,
          "comments": "estimated (based on Hyytiaelae observations" },
    "pfl": {
        "value": 1.2,
          "source": "estimated from Grote and Reiter 2004",
          "comments": "estimated Thornton and Zimmermann 2007: 1.1; estimated from Kantola and Maekelae 2006: 1.15; estimated Meir et al. 2002: 1.2" },
    "psi_exp": {
        "value": 4.7,
        "source": "Rosner et al. 2019",
        "comments": "Knuever et al. 2022: 7.0; Rosner et al. 2021: 3.0; Arend et al. 2021: 7.0" },
    "psi_ref": {
        "value": -5.0,
        "source": "Rosner et al. 2019",
        "comments": "Knuever et al. 2022: -4.7; Rosner et al. 2021: -7.0; Arend et al. 2021: -4.3" },
    "psntmax": {
        "value": 38.8,
          "source": "Molina et al. 2015",
          "comments": "Kurbatova et al. 2008: 45; Bergh et al. 1998: 42" },
    "psntmin": {
        "value": -2.494,
          "source": "Molina et al. 2015",
          "comments": "Kurbatova et al. 2008: -4; Andersson et al. 2002: -3; Bergh et al. 1998: -3; Aber et al. 1996: 0.0; Pnet-N-DNDC: 2.0" },
    "psntopt": {
        "value": 35.1,
          "source": "Molina et al. 2015",
          "comments": "Kurbatova et al. 2008: 24; Andersson et al. 2002: 15, Bergh et al. 1998: 15; Aber et al. 1996: 20.0; Pnet-N-DNDC: 24.0" },
    "qjvc": {
        "value": 2.47,
          "source": "Grassi et al. 2001",
          "comments": "Ibrom et al. 2006: 1.67; Bergh et al. 2003: 1.7-3.3 (higher at northernmost latitudes); Meir et al. 2002: 2.4; Benner et al. 1988 (cit. in Wullschleger 1993): 2.8; Lange et al. 1986 (cit. in Wullschleger 1993): 2.7" },
    "qrf": {
        "value": 0.35,
          "source": " estimated to obtain a foliage fine root ratio in summer of app. 0.35 (550 (mFrt) /1600 (MFOLOPT) gDW m-2)",
          "comments": "Karpechko et al. 2022: 0.177 (171 (roots below 2mm)/964); fine root biomass for evaluation: Lwila et al. 2022: 326 (avg. from 4 sites, 280 southern, 380 northern), Finer et al. 2007: 280 (temperate), 330 (boreal)(kg m-2) gDW m-2; Bolte et al. 2004: 290 gDW m-2; Bergh et al. 2003: 1210 gDW m-2; Nadelhoffer et al. 1985 (cit. in Pietsch et al. 2005): 622 gDW m-2",
          "comments": "relation fine roots to foliage biomass for evaluation: Helmisaari et al. 2007: 0.16-0.5",
          "depends": "MFOLOPT, TOFRTBAS" },
    "qsf_p1": {
        "value": 4.7,
          "source": "adjusted to Koestner et al. 2002",
          "comments": "Dvorak et al. 1996 (approximated): 1.75; Eckmueller and Sterba 2005: 1.2-1.9 (site dependent); Ewers et al. 2001: 3.03 (decreases with irrigation)" },
    "qsf_p2": {
        "value": -0.09,
          "source": "adjusted to Koestner et al. 2002" },
    "qwodfolmin": {
        "value": 4.123,
          "source": "Molina et al. 2015" ,
          "comments": "Ho 2000 (not filed master thesis): 1.75 (1.6-2.0)" },
    "rbuddem": {
        "value": 0.17,
          "depends": "DLEAFSHED", "formula": "1.0 / floor(1.0 + DLEAFSHED[vt]*0.99 / 365.0)" },
    "respq10": {
        "value": 2.637,
          "source": "Molina et al. 2015",
          "comments": "Andersson et al. 2002: 2.3; Roberntz and Stockfors 1998: 2.0-2.7; Kurbatova et al. 2008: 2.0; Widen and Majdi 2001: 5.0 (fine roots)" },
    "rootmrespfrac": {
        "value": 0.553,
          "source": "Molina et al. 2015 " },
    "senescstart": {
        "value": 207.3,
          "source": "Molina et al. 2015",
          "comments": "Kurbatova et al. 2008: 270" },
    "slamax": {
        "value": 6.8,
          "source": "Perterer and Koerner 1990",
          "comments": "Paz-Dyderska et al. 2020: 7.8; Goude et al. 2019: 6.3 (upper limit of 1st crown class); Dewar et al. 2012: 5.52 (0.181 kgDW m-2); Thornton and Zimmermann 2007: app. 7 (18 m2 kgC-1); Meir et al. 2002: 7.0; Moren et al. 2000: 7.1; Sellin 2000: 10.1; Reich et al. 1998: 5" },
    "slamin": {
        "value": 2.1,
          "source": "Perterer and Koerner 1990",
          "comments": "Paz-Dyderska et al. 2020: 6.6; Goude et al. 2019: 3.8 (lower limit of 3th crown class); Dewar et al. 2012: 2.63 (0.38 kgDW m-2); Thornton and Zimmermann 2007: app. 3 (8 m2 kgC-1); Hoch et al. 2003: 2.6; Meir et al. 2002: 4.0; Moren et al. 2000: 2.4; Sellin 2000: 2.2; Bergh et al. 1998: 2.5; Reich et al. 1998: 4" },
    "slope_gsa": {
        "value": 6.4,
          "source": "Medlyn et al. 2001: 2.9-6.4",
          "comments": "Falge et al. 1996: 6.5 (old) - 9.8 (middle aged)" },
    "slope_gsco2": {
        "value": 0.79,
          "source": "Barton and Jarvis 1999 (cit. Medlyn et al. 2001)" },
    "tap_p1": {
        "value": 1.75055,
          "source": "Dik 1984 (cit. in Zianis et al. 2005)" },
    "tap_p2": {
        "value": 1.10897,
          "source": "Dik 1984 (cit. in Zianis et al. 2005)" },
    "tap_p3": {
        "value": -2.75863,
          "source": "Dik 1984 (cit. in Zianis et al. 2005)" },
    "tau": {
        "value": 0.03,
          "source": "Forrester et al. 2014" },
    "tofrtbas": {
        "value": 0.003,
          "source": "Brunner et al. 2013 (1.11 yr-1)",
          "comments": "Bossel and Schaefer 1989: 0.004; Mainiero et al. 2010: 0.013; Withington et al. 2006: 0.0039; Majdi and Andersson 2005: 0.0025-0.0033 (lt 1mm); Ostonen et al. 2005: 0.0038-0.0049; Stober et al. 2000 (cit. in Persson 2007): 0.0038; Chertov et al. 1999: 0.0047" },
    "tosapmax": {
        "value": 0.0007,
          "source": "Longetaud et al. 2006",
          "comments": "Leemanns 1991: 0.004" },
    "ugwdf": {
        "value": 0.2,
          "source": "Bossel 1994",
          "comments": "derived from Bolte et al. 2004: 0.15; Ellenberg et al. 1981 (cit. in Pietzsch et al. 2005): 0.19" },
    "us_nh4": {
        "value": 1.0e-04,
          "source": "McFarlane and Yanai 2006",
          "comments": "Gessler et al. 1998: 0.000054 (1735 nmol gFW-1); Hoegberg et al. 1998 (cit. in Bassirirad 2000): 0.000136 (8.1 umol g-1 h-1)" },
    "us_no3": {
        "value": 3.36e-05,
          "source": "McFarlane and Yanai 2006",
          "comments": "Hoegberg et al. 1998 (cit. in Bassirirad 2000): 0.000022 (1.33 umol g-1 h-1)" },
    "vcmax25": {
        "value": 65.0,
          "source": "average from Bergh et al. 2003 (21.2-110.5)",
          "comments": "Ibrom et al. 2006: 43; Wang et al. 2003: 39.5; Medlyn et al. 2002(b): 40.7-119.5; Grassi and Bagnarese 2001: app. 50; Grassi et al. 2001: 19.5-35.2 (23.4); Falge et al. 1996,1997: 33.72" },
    "vpdref": {
        "value": 4.0,
          "source": "Barton and Jarvis 1999 (cit. in Medlyn et al. 2001)",
          "comments": "Ditmarova et al. 2010: 2.0 (seedlings)" },
    "woodmrespa": {
        "value": 0.13,
          "source": "Molina et al. 2015" },
    "wuecmax": {
        "value": 13.7,
          "source": "Molina et al. 2015",
          "comments": "Kurbatova et al. 2008: 13.9; Kram et al. 1999: 7.3 (consistent with Moren et al. 2001, fits app. for Ebersberg and Flossenbuerg site); Cienciala et al. 1994 (cit. in Lindroth and Cienciala 1996): 4.8" },
    "wuecmin": {
        "value": 4.8,
          "comments": "min from literature list for wuecmax" }
  }
}
