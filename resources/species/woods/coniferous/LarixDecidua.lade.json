{
  "mnemonic": "lade",
  "name": "Larix Decidua",
  "namesupplement": "European larch",
  "group": "wood",

  "parameters": {
    "aeis": {
        "value": 51164.8,
          "source": "Lehning et al. 2001 (f. Q.robur)",
          "comments": "needed for application of SIM module for BVOC emission" },
    "alb": {
        "value": 0.12,
          "source": "Shuman et al. 2011 (cit. in Thom et al. 2017)" },
    "alpha0_is": {
        "value": 0.014,
          "source": "Lehning et al. 2001 (f. Q.robur)",
          "comments": "needed for application of SIM module for monoterpne emission",
          "depends": "EF_ISO" },
    "alpha0_mt": {
        "value": 0.014,
          "source": "Lehning et al. 2001 (f. Q.robur, isoprene)",
          "comments": "needed for application of SIM module for monoterpne emission",
          "depends": "EF_MONO" },
    "cdr_p1": {
        "value": 2.8,
          "source": "adjusted to Hasenauer et al. 1997",
          "depends": "CDR_P2, CDR_P3" },
    "cdr_p2": {
        "value": 0.68,
          "source": "adjusted to Hasenauer et al. 1997",
          "depends": "CDR_P1, CDR_P3" },
    "cdr_p3": {
        "value": 0.0, 
          "source": "adjusted to Hasenauer et al. 1997",
          "depends": "CDR_P1, CDR_P2" },
    "deciduous": {
        "value": true },
    "dsap": {
        "value": 0.464,
          "source": "Thurner et al. 2014 (mean data from the global wood density data base for needleleaf deciduous)",
          "comments": "Hoch et al. 2003: 0.54; Brzeziecki and Kienast 1994 (cit. in Forrester et al. 2017 suppl): 0.474; Knigge and Schulz 1966 (cit. in Droessler et al. 2015): 0.55" },
    "ef_iso": {
        "value": 0.1,
          "source": "Kajos et al. 2013: 1-2 percent of total",
          "comments": "two trees L. cajanderi; Karl et al. 2009: 0.0; Simon et al. 2006: 0.5" },
    "ef_mono": {
        "value": 7.1,
          "source": "Kajos et al. 2013: 6.95MT + 0.18",
          "comments": "two trees L. cajanderi of 2.4/0.04 and 11.5/0.31 in average; Karl et al. 2009: 0.0; Simon et al. 2006: 1.5; Isidorov et al. 1985 (cit. in Solmon et al. 2004: 8.0)" },
    "ef_monos": {
        "value": 5.9,
          "source": "Kajos et al. 2013: 5.75MT + 0.14ST",
          "comments": "two trees L. cajanderi of 1.9/0.03 and 9.6/0.25 in average; Karl et al. 2009: 5.0" },
    "ext": {
        "value": 0.32,
          "source": "Breda 2003",
          "comments": "Pierce and Running 1998 (cit. in Pietsch et al. 2005): 0.51" },
    "ffacmax": {
        "value": 0.18,
          "source": "Mei et al. 2015 (max. from graph, L. gmelinii)",
          "comments": "Rinne et al. 2015: app. 0.12; Hoch et al. 2003: app. 0.15 (for leaves) and 0.08 (for branches)" },
    "fret_n": {
        "value": 0.8,
          "source": "Kloeppel et al. 1998 (fol. litter, cit. in Pietsch et al. 2005); also: Ueyame et al. 2009",
          "comments": "Enta et al. 2020 (L.kaempferi): 0.5; Yan et al. 2016: 0.75; Chapin and Kedrowski 1983 (cit. in Aerts and Gleason 2006): 0.79 (L. laricina); Gower and Richards 1990: 0.7-0.84 (0.77)" },
    "gsmax": {
        "value": 580.0,
          "source": "Handa et al. 2005 (cit. in Meier and Scherer 2012)",
          "comments": "Hoshika et al. 2018 (for needleleaved summergreen trees): 140" },
    "gsmin": {
        "value": 1.93,
          "source": "Schuster et al. 2017 (deciduous woody plants)" },
    "h2oref_a": {
        "value": 0.2,
          "source": "Havranek and Benecke 1978",
          "comments": "approximated from graph" },
    "h2oref_gs": {
        "value": 0.2,
          "comments": "assumes that drought effects are the same on stomata and other processes",
          "depends": "H2OREF_A", "formula": "H2OREF_A" },
    "hd_exp": {
        "value": 4.0,
          "source": "adjusted to Schober yield table",
          "depends": "HD_MIN, HD_MAX", "formula": "hd = HD_MIN + (HD_MAX-HD_MIN) * exp(-HD_EXP*dbh)",
          "comments": "HD_MAX: 100, HD_MIN: 35, HD_EXP: 4.0" },
    "hd_max": {
        "value": 100.0,
          "source": "adjusted to Schober yield table",
          "depends": "HD_MIN, HD_EXP", "formula": "hd = HD_MIN + (HD_MAX-HD_MIN) * exp(-HD_EXP*dbh)",
          "comments": "HD_MAX: 100, HD_MIN: 35, HD_EXP: 4.0" },
    "hd_min": {
        "value": 35.0,
          "source": "adjusted to Schober yield table",
          "depends": "HD_MAX, HD_EXP", "formula": "hd = HD_MIN+(HD_MAX-HD_MIN) * exp(-HD_EXP*dbh)",
          "comments": "HD_MAX: 100, HD_MIN: 35, HD_EXP: 4.0" },
    "mfolopt": {
        "value": 0.12,
          "source": "Withington et al. 2006",
          "comments": "Gower et al. 1993 (0.357+-0.003 average foliage production); Moller, cited in unpublished leaf mass review: 0.2; Burger, cited in unpublished leaf mass review: 0.18-0.26)" },
    "mortcrowd": {
        "value": 2.0,
          "source": "approximated to Schobers yield table" },
    "mwfm": {
        "value": 0.00041,
          "source": "Pietsch et al. 2005",
          "comments": "Ueyame et al. 2009: 0.00045" },
    "ncfolopt": {
        "value": 0.0185,
          "source": "Ueyama et al. 2009",
          "comments": "Kloeppel et al. 1998: 0.0193; Gower et al. 1993: 0.043 (3.1 mmol g-1); Mei et al 2015: 2.2 (L. gmelinii); Alriksson and Eriksson 1998 (L. sibirica): 0.027; Schulze et al. 1994: 0.0207 (deciduous conifers)" },
    "nc_fineroots_max": {
        "value": 0.011,
          "source": "Nambiar 1987 (cit. in Pietsch et al. 2205)",
          "comments": "Hobbie et al. 2010 (CN 54.7): 0.0082; Ueyame et al. 2009: 0.0086; Alriksson and Eriksson 1998 (L. sibirica): 0.0028" },
    "nc_fineroots_min": {
        "value": 0.0082,
          "source": "Hobbie et al. 2010 (CN 54.7)" },
    "ncsapopt": {
        "value": 0.011,
          "source": "Nambiar 1987 (cit. in Pietsch et al. 2005)",
          "comments": "Ueyame et al. 2009: 0.01; Alriksson and Eriksson 1998 (L. sibirica): 0.0016" },
    "pa": {
        "value": 660.1e+06,
          "source": "Lehning et al. 2001 (f. Q.robur)",
          "comments": "needed for application of SIM module for BVOC emission" },
    "psi_exp": {
        "value": 3.0,
        "source": "Rosner et al. 2021",
        "comments": "Feng et al. 2021: 7.0" },
    "psi_ref": {
        "value": -5.0,
        "source": "Rosner et al. 2021",
        "comments": "Feng et al. 2021: -3.8" },
    "qjvc": {
        "value": 2.0,
          "source": "Xue et al. 2011 (probably estimated)" },
    "qrd25": {
        "value": 0.015,
          "source": "Xue et al. 2011",
          "comments": "probably estimated or taken from Collatz" },
    "qrf": {
        "value": 1.0,
          "source": "Havranek 1982 (cit. in Pietsch et al. 2005)" },
    "rbuddem": {
        "value": 1.0,
          "comments": "for deciduous always = 1 if not free growth",
          "formula": "1.0 / floor(1.0 + DLEAFSHED[vt]*0.99 / 365.0)" },
    "rpmin": {
        "value": 8.0,
        "source": "Eller et al. 2020 (needleleaf deciduous)" },
    "slamax": {
        "value": 15.0,
          "source": "Oren et al. 1986" ,
          "comments": "Paz-Dyderska et al. 2020 (L. sibirica): 16.0" },
    "slamin": {
        "value": 8.5,
          "source": "Oren et al. 1986" ,
          "comments": "Paz-Dyderska et al. 2020 (L. sibirica): 13.3; Hoch et al. 2003: 9.8" },
    "slope_gsa": {
        "value": 7.0,
          "source": "Xue et al. 2011" },
    "tap_p1": {
        "value": 1.8667,
          "source": "Dik 1984 (cit. in Zianis et al. 2005)" },
    "tap_p2": {
        "value": 1.08118,
          "source": "Dik 1984 (cit. in Zianis et al. 2005)" },
    "tap_p3": {
        "value": -3.0488,
          "source": "Dik 1984 (cit. in Zianis et al. 2005)" },
    "tofrtbas": {
        "value": 0.0025,
          "source": "Withington et al. 2006" },
    "ugwdf": {
        "value": 0.23,
          "source": "Havranek 1982 (cit. in Pietsch et al. 2005)" },
    "us_nh4": {
        "value": 0.000791,
          "source": "Malagoli et al. 2000 (1060 nmol g-1 FW h-1 = 1060*14*24*1000/(0.45*1000.000.000*1000) kgN kgDW-1 d-1)" },
    "us_no3": {
        "value": 0.000121,
          "source": "Malagoli et al. 2000 (163 nmol g-1 FW h-1)" },
    "vcmax25": {
        "value": 49.1,
          "source": "Xue et al. 2011",
          "comments": "Matyssek and Schulze 1988 (cit. in Wullschleger 1993): 32" }
  }
}
