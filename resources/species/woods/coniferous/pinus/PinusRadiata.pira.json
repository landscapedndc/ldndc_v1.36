{
  "mnemonic": "pira",
  "name": "Pinus Radiata",
  "namesupplement": "Monterey pine",
  "group": "wood",

  "parameters": {
    "aejm": {
        "value": 46152.0,
          "source": "Walcroft et al. 1997 (cit. in Leuning 2002, average from two datasets)" },
    "aekc": {
        "value": 97010.0,
          "source": "Medlyn et al. 2002" },
    "aeko": {
        "value": 49070.0,
          "source": "Medlyn et al. 2002" },
    "aerd": {
        "value": 10000.0,
          "source": "Walcroft et al. 1997" },
    "aevc": {
        "value": 64780.0,
          "source": "Kattge and Knorr 2007 (64780, 51320) ",
          "comments": "Walcroft and Kelliher 1997 (cit. in Leuning 2002, average from two datasets): 51320; Medlyn et al. 2002: 49070 (fert.), 61310 (unfert.)" },
    "alb": {
        "value": 0.1,
          "source": "Jarvis et al. 1976 (cit. in Breuer and Frede 2003)" },
    "amaxa": {
        "value": 5.42,
          "source": "Warren and Adams 2000" },
    "amaxb": {
        "value": 19.2,
          "source": "Warren and Adams 2000" },
    "cdamp": {
        "value": 0.3,
          "comments": "estimated form Australia data (uncertain)" },
    "cdr_p1": {
        "value": 2.753,
          "source": "adjusted to Condes and Sterba 2005",
          "depends": "CDR_P2, CDR_P3" },
    "cdr_p2": {
        "value": 0.635,
          "source": "adjusted to Condes and Sterba 2005",
          "depends": "CDR_P2, CDR_P3" },
    "cdr_p3": {
        "value": 0.0,
          "source": "adjusted to Condes and Sterba 2005",
          "depends": "CDR_P2, CDR_P3" },
    "dleafshed": {
        "value": 912,
          "source": "Warren and Adams 2000" },
    "dsap": {
        "value": 0.44,
          "source": "calculated from Paul et al. 2006 for a 50 year old stand" },
    "ef_iso": {
        "value": 0.0,
          "source": "Karl et al. 2009: 0.0" },
    "ef_mono": {
        "value": 0.0,
          "source": "Karl et al. 2009: 0.0" },
    "ef_monos": {
        "value": 3.0,
          "source": "Karl et al. 2009: 3.0" },
    "ext": {
        "value": 0.5,
          "source": "Breda 2003" },
    "fret_n": {
        "value": 0.44,
          "source": "Sadanandan and Fife 1991" },
    "frtalloc_base": {
        "value": 130.0,
          "source": "Aber and Federer 1992 (for all stands)" },
    "frtalloc_rel": {
        "value": 1.92,
          "source": "Aber and Federer 1992 (for all stands)" },
    "gsmax": {
        "value": 850.0,
          "source": "Arneth et al. 1998" },
    "h2oref_a": {
        "value": 0.5,
          "comments": "assumes that drought effects are the same on stomata and other processes",
          "depends": "H2OREF_GS", "formula": "H2OREF_GS" },
    "h2oref_gs": {
        "value": 0.5,
          "source": "Arneth et al. 1998" },
    "hd_exp": {
        "value": 6.0,
          "source": "adjusted to Condes and Sterba 2005",
          "depends": "HD_MIN, HD_MAX", "formula": "hd = HD_MIN + (HD_MAX-HD_MIN) * exp(-HD_EXP*dbh)",
          "comments": "HD_MAX: 60, HD_MIN: 30, HD_EXP: 6.0" },
    "hd_max": {
        "value": 60.0,
          "source": "adjusted to Condes and Sterba 2005",
         "depends": "HD_MIN, HD_EXP", "formula": "hd = HD_MIN + (HD_MAX-HD_MIN) * exp(-HD_EXP*dbh)",
          "comments": "HD_MAX: 60, HD_MIN: 30, HD_EXP: 6.0" },
    "hd_min": {
        "value": 30.0,
          "source": "adjusted to Condes and Sterba 2005",
          "depends": "HD_MAX, HD_EXP", "formula": "hd = HD_MIN+(HD_MAX-HD_MIN) * exp(-HD_EXP*dbh)",
          "comments": "HD_MAX: 60, HD_MIN: 30, HD_EXP: 6.0" },
    "hdj": {
        "value": 200000.0,
          "source": "Kattge and Knorr 2007",
          "depends": "SDJ", "formula": "if SDJ Kattge and Knorr, then HDJ = 200000" ,
          "comments": "Walcroft et al. 1997: 198924" },
    "mfolopt": {
        "value": 0.97,
          "source": "Warren and Adams 2000",
          "comments": "Guo et al. 2008: 0.87; Raison et al. 1992 (cit. in Kirschbaum 1999): 1.2(control)-1.6(fertil.)" },
    "mwfm": {
        "value": 0.0005,
          "source": "McMurtrie et al. 1990" },
    "ncfolopt": {
        "value": 0.016,
          "source": "Beets and Whitehead 1996; Warren and Adams 2000" },
    "ndflush": {
        "value": 36,
          "source": "Whitehead et al. 1994??" },
    "ndmorta": {
        "value": 324,
          "source": "Whitehead et al. 1994??" },
    "qjvc": {
        "value": 1.81,
          "source": "Su et al. 2009 (method5)" ,
          "comments": "Walcroft and Kelliher 1997 (cit. in Kattge and Knorr 2007): 1.59" },
    "qrd25": {
        "value": 0.0187,
          "source": "Su et al. 2009 (method5)" },
    "qrf": {
        "value": 0.51,
          "source": "Guo et al. 2008",
          "comments": "Santantonio and Grace 1987 (cit. in Kirschbaum 1999): 0.5" },
    "qsf_p1": {
        "value": 3.8,
          "source": "Teskey and Sheriff 1996, cit. in DeLucia et al. 2000"},
    "qsf_p2": {
        "value": 0.0,
          "source": "no height dependency assumed" },
    "rbuddem": {
        "value": 0.33,
          "depends": "DLEAFSHED", "formula": "1.0 / floor(1.0 + DLEAFSHED[vt]*0.99 / 365.0)" },
    "sdj": {
        "value": 637.4,
          "source": "Kattge and Knorr 2007 (637.4, 634.8)" ,
          "comments": "Walcroft et al. 1997: 650" },
    "senescstart": {
        "value": 206,
          "source": "Whitehead et al. 1994" },
    "slamax": {
        "value": 4.7,
          "source": "Warren and Adams 2000" },
    "slamin": {
        "value": 3.0,
          "comments": "??" },
    "slope_gsh2o": {
        "value": 3.5,
          "source": "Arneth et al. 1998" },
    "theta": {
        "value": 0.9,
          "source": "Walcroft et al. 1997" },
    "ugwdf": {
        "value": 0.2,
          "source": "Paul et al. 2019" },
    "vcmax25": {
        "value": 85.9,
          "source": "Kattge and Knorr 2007 (85.9, 99.2)",
          "comments": "Su et al. 2009 (method5): 87.49;Medlyn et al. 2002(b): 99.15 (fert.), 85.86 (unfert.) (fitted to simple arrhenius: 97.01/83.57); Walcroft and Kelliher 1997 (cit. in Knorr and Kattge 2007): 99.2" },
    "wuecmax": {
        "value": 6.3,
          "comments": "parameterization in CABALA (12.5 gDW kgH2O-1)" },
    "wuecmin": {
        "value": 6.3,
          "depends": "WUECMAX", "formula": "WUECMAX" }
  }
}
