/*!
 * @file
 * @author
 *    - David Kraus
 *
 * @date
 *    Dec, 2017
 */

#ifndef  LD_PENMAN_H_
#define  LD_PENMAN_H_

#include <cbm_errors.h>

namespace ldndc {

/*!
 * @brief
 *
 */
enum
surface_type
{
    water,
    bare_soil,
    short_grass
};

/*!
 * @brief
 *
 */
double
penman(
       surface_type /* surface charaterization */,
       int /* day of year */,
       int /* number of days of year */,
       double /* albedo */,
       double /* latitude */,
       double /* short-wave radiation */,
       double /* temperature */,
       double /* wind speed */,
       double /* vapour pressure */);

/*!
 * @brief
 *
 */
double
wind_function(
              surface_type /* surface charaterization */,
              double /* wind velocity */);

/*!
 * @brief
 *
 */
lerr_t
saturated_vapour_pressure(
                          double /* temperature */,
                          double * /* saturated vapour pressure */,
                          double * /* slope */);

} /* end namespace ldndc */

#endif  /*  !LD_PENMAN_H_  */
