/*!
 * @file
 * @authors
 *  - Ruediger Grote
 *  - David Kraus
 * @date
 *  May 21, 2017
 */

#include  "watercycle/ld_thornthwaite.h"

#include  <math/cbm_math.h>
#include  <constants/cbm_const.h>
#include  <scientific/meteo/ld_meteo.h>



/*!
 * @page waterlibs
 * @subsection thornthwaite Thornthwaite
 * Daily potential evapotranspiration \f$ PET \f$ [m] after @cite thornthwaite:1948a
 * with additions from @cite willmott:1985a is calculated depending on temperature by:
 * \f{eqnarray*}{
 * PET (T > 37.5) &=& -415.85 + 32.24 \cdot 37.5 - 0.43 \cdot 37.5^2  \\
 * PET (37.5 \ge T > 26) &=& -415.85 + 32.24 \cdot T - 0.43 \cdot T^2 \\
 * PET (26 \le T) &=& 16 \cdot \left( 10 \cdot \frac{T}{h_i} \right)^{0.49239 + 0.0179 h_i - 0.0000771 h_i^2 + 0.000000675 h_i^3}
 * \f}
 * These formulas give monthly values for an average of 12 hours daylight.
 * They are corrected to the appropriate number of hours and devided to a daily value.
 * 
 * Depending on the heat index \f$ h_i \f$ there can be a jump at 26degreeC (maybe of 2mm per day).
 * 
 * The method is purely empirical.
 */

/*!
 * @brief
 * Potential evapotranspiration after @cite thornthwaite:1948a
 */
double
ldndc::thornthwaite(
                    double _air_temperature,
                    double _day_length,
                    double _heat_index)
{
    /* daily average temperature larger zero */
    double tAvg( cbm::bound_min( 0.0, _air_temperature));

    /* daily potential evaporation */
    double etm( 0.0);

    /* addition from Willmott et al. 1985 for high temperatures */
    if ( tAvg > 37.5)
    {
        etm = -415.85 + 32.24 * 37.5 - 0.43 * cbm::sqr(37.5);
    }
    else if ( tAvg > 26.0)
    {
        etm = -415.85 + 32.24 * tAvg - 0.43 * cbm::sqr(tAvg);
    }
    else
    {
        /* Thornthwaite exponent A */
        double A( cbm::poly3( _heat_index, 0.49239, 0.0179, -0.0000771, 0.000000675));

        etm = 16.0 * pow(10.0 * tAvg / _heat_index, A);
    }

    /* potential evaporation, conversion to daily values (assumes 30 days per month, 30*12h=360h) */
    // _day_length gives number of hours between sun rise and set
    double etd( etm * cbm::M_IN_MM * _day_length / 360.0);

    /* daily potential evaporation */
    return  cbm::bound_min( 0.0, etd);
}



/*!
 * @page waterlibs
 *  The Thornthwaite heat index \f$ h_i \f$ is given by:
 *  \f[
 *  h_i = \sum_m (0.2 \cdot T_m)^{1.514}
 *  \f]
 *  The monthly temperature \f$ T_m \f$ is derived from the annual mean temperature \f$ T_a \f$:
 *  \f[
 *  T_m = -0.5 \cdot T_a \cdot cos \left(2 \pi \; \frac{d_m}{d_y} \right)
 *  \f]
 *  wherein \f$ d_m \f$ and \f$ d_y \f$ refer to the midmonth day of year
 *  and total number of days per year, respectively.
 */

/*!
 * @brief
 * Thornthwaite heat index
 */
double
ldndc::thornthwaite_heat_index(
                               double _latitude,
                               size_t _days_in_year,
                               double _annual_temperature_average,
                               double _annual_temperature_amplitude)
{
    static unsigned int const MIDMONTH[] =
    { 15, 45, 75, 105, 135, 166, 196, 227, 258, 289, 319, 350, 0 /*sentinel*/};

    // Initialized to 1 to prevent a zero-division if all months temps are below zero
    double heat_index( 1.0);
    for ( size_t i = 0;  MIDMONTH[i] != 0; ++i)
    {
        // seasonal development factor for temperature
        double dayfact_md( MIDMONTH[i]);
        if ( _latitude < 0.0)
        {
            dayfact_md += ( MIDMONTH[i] < 183) ? 183.0 : -182.0;
        }

        // estimated average monthly temperature
        double const tmonth( _annual_temperature_average
                             + 0.5 * _annual_temperature_amplitude
                               * (-cos( 2.0 * cbm::PI * dayfact_md / _days_in_year)));

        if ( cbm::flt_greater_zero( tmonth))
        {
            heat_index += pow( 0.2 * tmonth, 1.514);
        }
    }

    return  heat_index;
}


