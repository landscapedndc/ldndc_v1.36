/*!
 * @file
 * @author
 *    - David Kraus
 * @date
 *    May, 2014
 */

#ifndef  LD_ICESNOW_H_
#define  LD_ICESNOW_H_

#include  "ld_modelsconfig.h"
#include  <input/siteparameters/siteparameters.h>
#include  <containers/cbm_vector.h>

namespace ldndc {

class  LDNDC_API  WaterCycleSnowDNDC
{
public:

    /*! Temperature limit for snowpack formation (oC) */
    static const double  TLIMIT;
    
public:
    WaterCycleSnowDNDC( cbm::io_kcomm_t *);
    ~WaterCycleSnowDNDC();

    /*! Timestep scaling */
    double dt;

public:

    struct SnowPackStateIn
    {
        /*in*/

        /*! Temperature below lowest foliage layer and above watertable or snowlayer [oC] */
        double  air_temperature_above_canopy;
        double  air_temperature_below_canopy;

        /*in/out*/

        /*! Amount of rainfall [m] */
        double  rainfall;

        /*! Snow [m] */
        double  surface_ice;

        /*! Surface water [m] */
        double  surface_water;
    };

    struct SnowPackStateOut
    {
        /*out*/

        /*! Snowfall [m] */
        double  snowfall;

        /*! Amount of melted snow [m] */
        double  snow_melt;

        /*! Throughfall [m] */
        double  throughfall;

        /*in/out*/

        /*! Amount of rainfall [m] */
        double  rainfall;

        /*! Snow [m] */
        double  surface_ice;

        /*! Surface water [m] */
        double  surface_water;
    };
    lerr_t  SnowPack( SnowPackStateIn const &, SnowPackStateOut &);

    struct IceContentStateIn
    {
        /*in*/
        CBM_Vector<double>  h_sl;
        CBM_Vector<double>  bulkdensity_sl;
        CBM_Vector<double>  fcorg_sl;

        /*in/out*/
        CBM_Vector<double>  wc_sl;
        CBM_Vector<double>  ice_sl;
        CBM_Vector<double>  soil_temperature_sl;
    };

    struct IceContentStateOut
    {
        /*in/out*/
        CBM_Vector<double>  wc_sl;
        CBM_Vector<double>  ice_sl;
        CBM_Vector<double>  soil_temperature_sl;
    };

    lerr_t  IceContent( IceContentStateIn const &, IceContentStateOut &);

private:
    
    siteparameters::input_class_siteparameters_t const *  m_param;

    /*!
     * @brief
     *  Returns ice temperature [oC]
     */
    double
    SnowDNDCGetIceTemperature(
                              double /* water mass [kg] */,
                              double /* ice mass [kg] */);

    /*!
     * @brief
     *  Returns heat capacity of wet soil [J:K-1]
     */
    double
    SnowDNDCGetSoilHeatCapacity(
                                double /* organic carbon fraction [-] */,
                                double /* soil mass [kg] */,
                                double /* water mass [kg] */,
                                double /* ice mass [kg] */);
};

} /* namespace ldndc */

#endif  /*  LD_ICESNOW_H_  */
