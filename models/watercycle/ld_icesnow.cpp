/*!
 * @file
 * @author
 *    - David Kraus
 * @date
 *    May, 2014
 */

#include  "watercycle/ld_icesnow.h"

#include  <math/cbm_math.h>
#include  <constants/cbm_const.h>

#include  <logging/cbm_logging.h>


const double  ldndc::WaterCycleSnowDNDC::TLIMIT = 0.0;

ldndc::WaterCycleSnowDNDC::WaterCycleSnowDNDC( cbm::io_kcomm_t *  _iokcomm)
    : m_param( _iokcomm->get_input_class< siteparameters::input_class_siteparameters_t >())
{}

ldndc::WaterCycleSnowDNDC::~WaterCycleSnowDNDC()
{}



/*!
 * @page waterlibs
 * @section snow_ice Snow and ice calculations (SnowDNDC) 
 *  Includes snow and ice formation and melting at the surface
 *  and soil layers as well as the resulting soil temperature changes.
 */

/*!
 * @subsection snowpack_formation Snowpack formation and melting
 */
lerr_t
ldndc::WaterCycleSnowDNDC::SnowPack(
                            ldndc::WaterCycleSnowDNDC::SnowPackStateIn const &  _in,
                            ldndc::WaterCycleSnowDNDC::SnowPackStateOut &  _out)
{
    _out.snowfall = 0.0;
    _out.snow_melt = 0.0;
    _out.throughfall = 0.0;
    _out.rainfall = _in.rainfall;
    _out.surface_ice = _in.surface_ice;
    _out.surface_water = _in.surface_water;

    /*!
     * @page waterlibs
     * All precipitation is assumed to be snow when air temperature is
     * less or equal a given temperature limit (SNOWFALL_TEMPERATURE_LIMIT).
     * It can be intercepted by the canopy, but otherwise accumulates at 
     * the soil surface.
     */
        if ( cbm::flt_greater_zero( _out.rainfall) &&
         cbm::flt_greater_equal( m_param->SNOWDNDC_SNOWFALL_TEMPERATURE_LIMIT(),
                                 _in.air_temperature_above_canopy))
    {
        _out.surface_ice += _out.rainfall;
        _out.snowfall = _out.rainfall;
        _out.throughfall += _out.rainfall;
        _out.rainfall = 0.0;
    }

    /*!
     * @page waterlibs
     *  When air temperatures (tair) exceeds a limit temperature (TLIMIT = 0 oC),
     *  snow melts with a rate depending on air temperature and an empirical
     *  parameter \a MCOEFF:
     *  \f[
     *    \Delta \theta_{snow} = MCOEFF \cdot (tair - TLIMIT)
     *  \f]
     */
    if ( cbm::flt_greater_zero( _out.surface_ice) &&
         cbm::flt_greater( _in.air_temperature_below_canopy,
                           ldndc::WaterCycleSnowDNDC::TLIMIT))
    {
        double const delta_temp( _in.air_temperature_below_canopy - ldndc::WaterCycleSnowDNDC::TLIMIT);
        _out.snow_melt = m_param->MCOEFF() * delta_temp * this->dt;

        if ( cbm::flt_greater( _out.snow_melt, _out.surface_ice))
        {
            _out.snow_melt = _out.surface_ice;
            _out.surface_ice = 0.0;
        }
        else
        {
            _out.surface_ice -= _out.snow_melt;
        }
    }
    else
    {
        _out.snow_melt = 0.0;
    }

    _out.surface_water += _out.snow_melt;

    return  LDNDC_ERR_OK;
}



/*!
 * @page waterlibs
 * @subsection ice_content Soil ice formation and melting
 */
lerr_t
ldndc::WaterCycleSnowDNDC::IceContent(
                              ldndc::WaterCycleSnowDNDC::IceContentStateIn const &  _in,
                              ldndc::WaterCycleSnowDNDC::IceContentStateOut &  _out)
{
    _out.wc_sl = _in.wc_sl;
    _out.ice_sl = _in.ice_sl;
    _out.soil_temperature_sl = _in.soil_temperature_sl;

    for( size_t  sl = 0;  sl < _in.h_sl.size();  ++sl)
    {
        /* Water mass in a soil layer [kg] */
        double const water_mass_old( _in.wc_sl[sl] * cbm::MM_IN_M * _in.h_sl[sl]);
        
        /* Ice mass in a soil layer [kg] */
        double const ice_mass_old( _in.ice_sl[sl] * cbm::MM_IN_M * _in.h_sl[sl]);
        
        /* Soil mass [kg] */
        double const soil_mass( _in.bulkdensity_sl[sl] * cbm::DM3_IN_M3 * _in.h_sl[sl]);

        /* Heat capacity [J:K-1] */
        double const c_wet_soil( SnowDNDCGetSoilHeatCapacity( _in.fcorg_sl[sl], soil_mass,
                                                              water_mass_old, ice_mass_old));

        /* Ice temperature [oC] */
        double const ice_temp( SnowDNDCGetIceTemperature( water_mass_old, ice_mass_old));

        /*!
         * @page waterlibs
         *  Soil ice formation occurs when soil temperature \f$ T_{soil }\f$ drops below
         *  ice temperature \f$ T_{ice}\f$. The amount of ice \f$\Delta \theta_{ice}\f$
         *  that is formed depends on the enthalpy of ice formation \f$H_{m}\f$ and the
         *  energy release \f$ Q_{r}\f$, which is calculated by the difference between
         *  ice and soil temperature multiplied by the heat capacity of the wet soil:
         *  \f{eqnarray*}{
         *    Q_r &=& (T_{ice} - T_{soil}) \cdot C_{p,wetsoil} \\
         *    \Delta \theta_{ice} &=& \frac{Q_r}{H_m}
         *  \f}
         *  The change of soil temperature due to energy release is hence given by:
         *  \f[
         *    \Delta T = \frac{\Delta \theta_{ice} H_m}{C_{p,wetsoil}}
         *  \f]
         */
        if ( cbm::flt_greater( ice_temp, _out.soil_temperature_sl[sl]) &&
             cbm::flt_greater_zero( water_mass_old))
        {
            /* Energy release [J] */
            double const q_r( (ice_temp - _out.soil_temperature_sl[sl]) * c_wet_soil);

            /* Amount of ice that can be formed by energy release [10^3:g] */
            double const delta_ice( q_r / cbm::MELT);

            if ( cbm::flt_greater( delta_ice, water_mass_old))
            {
                /* Amount of ice that is formed by energy release [g:m^-1] */
                double const delta_ice_g( water_mass_old * cbm::M_IN_MM / _in.h_sl[sl]);
                _out.ice_sl[sl] += delta_ice_g;
                _out.wc_sl[sl] = cbm::bound_min( 0.0, _out.wc_sl[sl] - delta_ice_g);

                /* temperature change due to aggregate change [K] */
                _out.soil_temperature_sl[sl] += (water_mass_old * cbm::MELT / c_wet_soil);
            }
            else
            {
                /* Amount of ice that is formed by energy release [g:m^-1] */
                double const delta_ice_g( delta_ice * cbm::M_IN_MM / _in.h_sl[sl]);
                _out.ice_sl[sl] += delta_ice_g;
                _out.wc_sl[sl] = cbm::bound_min( 0.0, _out.wc_sl[sl] - delta_ice_g);

                /* temperature change due to aggregate change [K] */
                _out.soil_temperature_sl[sl] += (delta_ice * cbm::MELT / c_wet_soil);
            }
        }

        /*!
         * @page waterlibs 
         *  Soil ice melting occurs for \f$ T_{soil} > 0\f$oC. The amount of melted ice
         *  and associated energy uptake is given by:
         *  \f{eqnarray*}{
         *    Q_u &=& T_{soil} \cdot C_{p,wetsoil} \\
         *  \Delta \theta_{ice} &=& -\frac{Q_u}{H_m}
         *  \f}
         *  The change of soil temperature due to energy uptake is hence given by:
         *  \f[
         *    \Delta T = \frac{\Delta \theta_{ice} H_m}{C_{p,wetsoil}}
         *  \f]
         */
        double const ice_mass_new( _out.ice_sl[sl] * _in.h_sl[sl]);
        if ( cbm::flt_greater_zero( _out.soil_temperature_sl[sl]) &&
             cbm::flt_greater_zero( ice_mass_new))
        {
            /* Available energy [J] */
            double const q_avail( _out.soil_temperature_sl[sl] * c_wet_soil);

            /* Energy needed for total melting [J] */
            double const q_melt( ice_mass_new * cbm::MELT);

            /* Complete melting */
            if ( cbm::flt_greater_equal( q_avail, q_melt))
            {
                _out.wc_sl[sl] += _out.ice_sl[sl];
                _out.ice_sl[sl] = 0.0;

                /* temperature change due to aggregate change [K] */
                _out.soil_temperature_sl[sl] -= q_melt / c_wet_soil;
            }
            /* Uncomplete melting */
            else
            {
                /* Melting water quantity with energy input and melting enthalpy */
                double const melting_water( q_avail / q_melt * _out.ice_sl[sl]);
                _out.wc_sl[sl] += melting_water;
                _out.ice_sl[sl] = cbm::bound_min( 0.0, _out.ice_sl[sl] - melting_water);

                /* temperature change due to aggregate change [K] */
                _out.soil_temperature_sl[sl] -= q_avail / c_wet_soil;
            }
        }
    }

    return  LDNDC_ERR_OK;
}


/*!
 * @page waterlibs
 *  Heat capacity of the (wet) soil \f$ C_{p,wetsoil}\f$ is calculated by
 *  heat capacity values of its components, i.e., soil organic matter,
 *  mineral soil, water and ice (air is neglected):
 *  \f{eqnarray*}{
 *   C_{p,wetsoil} &=& C_{p,drysoil} + C_{p,water} + C_{p,ice} \\
 *   C_{p,drysoil} &=& (c_{p,som} \cdot c_{som} + c_{p,min} \cdot (1 - c_{som})) \cdot m_{soil} \\
 *   C_{p,water} &=& c_{p,water} \cdot m_{water} \\
 *   C_{p,ice} &=& c_{p,ice} \cdot m_{ice}
 *  \f}
 */
double
ldndc::WaterCycleSnowDNDC::SnowDNDCGetSoilHeatCapacity(
    double _c_org /* organic carbon fraction [-] */,
    double _soil_mass /* soil mass [kg] */,
    double _water_mass /* water mass [kg] */,
    double _ice_mass /* ice mass [kg] */)
{
    /* Soil organic matter fraction [-] */
    double const som(_c_org / cbm::CCORG);

    /* Dry soil heat capacity [J:K^-1] */
    double const c_dry_soil((cbm::CORG * som + cbm::CMIN * (1.0 - som)) * _soil_mass);

    /* Water and ice heat capacity [J:K^-1] */
    double const c_water_ice(cbm::CWAT * _water_mass + cbm::CICE * _ice_mass);

    /* Wet soil heat capacity [J:K-1] */
    return c_dry_soil + c_water_ice;
}


/*!
 * @page waterlibs
 * @subsection ice_temperature Ice temperature
 * Ice temperature \f$ T_{ice}\f$ depends on the ratio
 * of ice and water mass and the parameter \a TICE:
 * \f[
 *   T_{ice} = TICE \cdot \frac{m_{ice}}{m_{water}}
 * \f]
 */
double
ldndc::WaterCycleSnowDNDC::SnowDNDCGetIceTemperature(
                                                     double _water_mass /* water mass [kg] */,
                                                     double _ice_mass /* ice mass [kg] */)
{
    if ( cbm::flt_greater_zero( _water_mass))
    {
        return -m_param->TICE() * _ice_mass / _water_mass;
    }
    else
    {
        return -100.0;
    }
}

