/*!
 * @file
 * @authors
 *  - Ruediger Grote
 *  - David Kraus
 *
 * @date
 *    Dec, 2017
 */

#include  "watercycle/ld_priestley-taylor.h"
#include  "microclimate/ld_longwave_radiation.h"

#include  <math/cbm_math.h>
#include  <constants/cbm_const.h>
#include  <scientific/meteo/ld_meteo.h>



/*!
 * @page waterlibs
 * @subsection priestley_taylor Priestley and Taylor
 * The Priestley-Taylor evapotranspiration is a simplified Penman method.
 * It is based on the assumption that the radiation driven part of evapotranspiration dominates.
 * Therefore, it uses only the radiation driven part of the Penman equation
 * and an empirical coefficient \f$ \alpha_{PT}\f$ = \b 1.1, which adapts the radiation term for the neglected aerodynamic term:
 * \f[
 * PET = \alpha_{PT} \frac{1}{\lambda} \frac{R_n s}{s + \gamma} \; ,
 * \f]
 * wherein \f$ \lambda \f$ and \f$ \gamma \f$ refer to the latent heat
 * of vaporization and the psychrometric constant, respecitvely.
 * This method should work well for humid and semi-arid climates.
 * It fails, when net radiation gets negative (as in Dutch winters).
 * It is set up for short grass.
 * If one wants to use it for crops, the result should (originally) be multiplied by a Penman crop factor.
 * Instead , in the present implementation, the lai is inculded in the albedo.
 */
double
ldndc::priestleytaylor(
                       int _doy /* day of year */,
                       int _nd_doy /* number of days of year */,
                       double _albedo /* albedo */,
                       double _lat /* latitude */,
                       double _temp /* air temperature [oC] */,
                       double _rad_short /* shortwave radiation [J:s^-1:m^-2]*/,
                       double _vp /* vapour pressure */,
                       double _alpha /* Priestley Taylor coefficient [-] */)
{
    double const rad_short_day( _rad_short * cbm::SEC_IN_DAY);
    
    /* saturation vapor pressure [1e3:Pa] */
    double vps( 0.0);
    ldndc::meteo::vps( _temp, &vps);

    /*!
     * @page waterlibs
     * The slope of saturation vapor pressure for changing temperature at the temperature at 2m height is estimated by:
     *  \f[
     *  s = vps \cdot \frac{4098}{\sqrt{T + 237.3}}
     *  \f]
     */
    double const slope( vps * 4098.0 / cbm::sqr( _temp + 237.3));   // [1e3:Pa:K^-1]

    /* outgoing longwave radiation */
    double rad_long_out_sec( ldndc::meteo::stefan_boltzmann( _temp)); // [J:m^-2:s^-1]
    double rad_long_out_day( rad_long_out_sec * cbm::SEC_IN_DAY); // [J:m^-2:d^-1]

    /* incoming longwave radiation */
    double const cloud_fraction( cloud_fraction_angstrom( _doy, _nd_doy, _lat, rad_short_day));
    double const rad_long_in_day( longwave_radiation( cloud_fraction,_temp, _vp));

    /*!
     * @page waterlibs
     *  Net radiation \f$ R_n \f$ is given by:
     *  \f[
     *  R_n = (1-a) R_s + R_{l,in} - R_{l,out}
     *  \f]
     * - The albedo \f$a\f$ is determined in the same way as for Penman.
	 * 
	 * For more details, see @cite kraalingen:1997a.
     */
    double rad_net( (1.0 - _albedo) * rad_short_day + rad_long_in_day - rad_long_out_day);

    /* daily potential evapotanspiration [kg:m^-2:d^-1] */
    double const et( _alpha / (cbm::LAMBDA * cbm::J_IN_MJ) * (rad_net * slope) / (slope + cbm::GAMMA));

    /* unit conversion (1kg water = 1mm x 1m^2 water) */
    return  cbm::bound_min( 0.0, et * cbm::M_IN_MM);
}


