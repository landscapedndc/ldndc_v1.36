/*!
 * @file
 * @authors
 *  - Ruediger Grote
 *  - David Kraus
 *
 * @date
 *    Dec, 2017
 */

#ifndef  LD_PRIESTLEYTAYLOR_H_
#define  LD_PRIESTLEYTAYLOR_H_

#include  "ld_modelsconfig.h"

namespace ldndc {

double
LDNDC_API priestleytaylor(
                          int /* day of year */,
                          int /* number of days of year */,
                          double /* albedo */,
                          double /* latitude */,
                          double /* air temperature [oC] */,
                          double /* shortwave radiation [W:m^-2]*/,
                          double /* vapour pressure */,
                          double /* Priestley Taylor coefficient [-] */);

} /*namespace ldndc*/

#endif  /*  !LD_PRIESTLEYTAYLOR_H_  */

