/*!
 * @file
 * @brief
 *    Various kinds of drought stress models
 *
 * @author
 *    Ruediger Grote,
 *    David Kraus (created on: may 10, 2016)
 */

#ifndef  LD_DROUGHTSTRESS_H_
#define  LD_DROUGHTSTRESS_H_

#include  "ld_modelsconfig.h"
#include  "state/mbe_state.h"
#include  <model/tinymodelcoll.h>

struct LDNDC_API DroughtStress : public cbm::TinyModelCollection< DroughtStress, double, double >
{
    DroughtStress( char const * = 0 /*kind*/);

    /* evaluate for given "water stress factor" (WS) */
    double  evaluate( double /*WS*/,
                char const * = 0 /*kind*/) const;

    /*! soil drought impact on stomatal conductance according to Van Wijk et al. 2000 */
    double  exponential( double /*WS*/) const;

    /*! soil drought impact on stomatal conductance according to Wang and Leuning 1998 */
    double  linear_threshold( double /*WS*/) const; /*linear*/

    /*! soil drought impact on stomatal conductance according to Knauer et al. 2015 */
    double  linear( double /*WS*/) const; /*linear*/

    /* relative available soil water content at which stomata are fully closed */
    double  fh2o_ref;
    
    /* slope of foliage conductivity in response to relative available soil water content */
    double  gs_slope;

    private:
        typedef EvaluateFunction DroughtStressFunction;
        DroughtStressFunction  evaluate_function( char const * = 0 /*kind*/) const;
};


namespace ldndc {

/*!
 * @brief
 * Non stomatal water limitation
 *
 * @param
 *    _var ...
 * @param
 *    _var_ref ...
 * @param
 *    _var_scale ...
 *
 * @return
 *    Drought stress scaling factor [0-1]
 */
double
non_stomatal_water_limitation(
                              double const & _var,
                              double const & _var_ref,
                              double const & _var_scale);

} /* namespace ldndc */

#endif  /*  !LD_DROUGHTSTRESS_H_  */

