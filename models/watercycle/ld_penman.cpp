/*!
 * @file
 * @author
 *    - David Kraus
 *
 * @date
 *    Dec, 2017
 */

#include  "watercycle/ld_penman.h"
#include  "microclimate/ld_longwave_radiation.h"

#include  <math/cbm_math.h>
#include  <scientific/meteo/ld_meteo.h>
#include  <constants/lconstants-conv.h>
#include  <constants/lconstants-math.h>
#include  <constants/lconstants-phys.h>
#include  <logging/cbm_logging.h>


/*!
 * @page waterlibs
 * @section potential_evaporation Potential evaporation
 * @subsection penman Penman
 * @authors
 *  - Daniel van Kraalingen (original version @cite kraalingen:1997a)
 *  - David Kraus (implementation into LandscapeDNDC)
 * 
 * The unterlying principle is that the energy between radiation, heat flux, and vaporizing water is balanced.
 * It is set up for grass. For higher growing crops it should be adapted by a Penman crop factor in the original version.
 * In the present implementation, the lai fullfils this role to some extend.
 */
double
ldndc::penman(
              surface_type _surface,
              int _doy /* day of year */,
              int _nd_doy /* number of days of year */,
              double _albedo /* albedo */,
              double _lat /* latitude */,
              double _rad_short_day /* short-wave radiation [J:m^-2:d^-1] */,
              double _temp /* temperature */,
              double _wind /* wind speed */,
              double _vp /* vapour pressure */)
{
    //reflection coefficient of soil or water background
    //differentiate between standing water or moist/dry soil
//    double albedo( (_wt > 0.005) ? 0.05 : 0.25);
//    double rfs( (_wt > 0.005) ? _albedo : _albedo * (1.0 - 0.5 * _ws));

    /*!
     * @page waterlibs
     * The <b> albedo </b> (reflection coefficient) \f$a\f$ is determined by distinguishing
     * - dry soil (water table of less than 0.005mm): \f$rfs = 0.25 \cdot (1 - 0.5\cdot watersaturation) \f$
     * - wet soil (water table of more than 0.005mm): \f$rfs = 0.05 \f$
     * \f[
     * a = rfs \cdot exp(-0.5\cdot lai) + 0.25 \cdot (1-exp(-0.5\cdot lai))
     * \f]
     * This equation represents, that the background (soil or water) is shielded by the crop/grass.
     * In @cite kraalingen:1997a the albedo is a fixed parameter. The size of the plant is taken into account
     * by the crop coefficient curve.
     */
//    double rf( rfs * std::exp( -0.5 * _lai) + 0.25 * (1.0 - std::exp(-0.5 * _lai))); //Reflection (=albedo) of surface (-)

    /* saturated vapour pressure */
    double vps( 0.0);
    double vps_slope( 0.0);
    saturated_vapour_pressure( _temp, &vps, &vps_slope);

    /* vapour pressure deficit */
    double const vpd( cbm::bound_min( 0.0, vps - _vp));

    /* outgoing longwave radiation */
    double rad_long_out_sec( ldndc::meteo::stefan_boltzmann( _temp)); // [J:m^-2:s^-1]
    double rad_long_out_day( rad_long_out_sec * cbm::SEC_IN_DAY); // [J:m^-2:d^-1]

    /* incoming longwave radiation */
    double const cloud_fraction( cloud_fraction_angstrom( _doy, _nd_doy, _lat, _rad_short_day));
    double const rad_long_in_day( longwave_radiation( cloud_fraction,_temp, _vp));
	
    /*!
     * @page waterlibs
     *  <b> Net radiation </b> \f$ R_n \f$ is given by:
     *  \f[
     *  R_n = (1-a) R_{shortwave} + R_{longwave,in} - R_{longwave,out}
     *  \f]
     */
    double rad_net( (1.0 - _albedo) * _rad_short_day + rad_long_in_day - rad_long_out_day);

    /* wind function */
    double const f_w( wind_function( _surface, _wind));

    /* isothermal evaporation */
    double ea( vpd * f_w);

    /*!
     * @page waterlibs
     *  <b>Evapotranspiration </b> is separated in a
     *  radiation and an aerodynamic term:
     *  \f[
     *  PET = PET_r + PET_d = \frac{1}{\lambda}
     *      \left( \frac{ \frac{\text{d}p_s}{\text{d}T} R_n}{\frac{\text{d}p_s}{\text{d}T}+\gamma} +
     *      \frac{\gamma \lambda E_a}{\frac{\text{d}p_s}{\text{d}T}+\gamma} \right)
     *  \f]
     * - \f$ \lambda\f$: latent heat of vaporising water
     * - \f$ \gamma \f$: the psychrometric constant
     * - \f$ E_a \f$: isothermal evaporation, \f$E_a = f_w \cdot (p_s(T_2) - p_2) \f$
     * - \f$p_s(T_2)\f$: saturated vapour pressure at the temperature at 2m height
     * - \f$p_2\f$: vapour pressure at 2m height
     */

    //Radiation driven part of potential evapotranspiration (mm d-1)
    double etrd( (rad_net * (vps_slope / (vps_slope + cbm::GAMMA))) / (cbm::LAMBDA * cbm::J_IN_MJ));

    //Dryness driven part of potential evapotranspiration (mm.d-1)
    double etae( (cbm::GAMMA * ea) / (vps_slope + cbm::GAMMA));

    //Potential evapotranspiration (mm d-1)
    double etd( etrd + etae);


    double re( cbm::SEC_IN_DAY * 1000.0 * 0.018016 / (f_w * cbm::RGAS * (_temp + 273.16)));

    //Estimated temperature difference between surface height and reference height (degrees C)
    double dt( re * ((rad_net - (cbm::LAMBDA * cbm::J_IN_MJ) * etd) / cbm::SEC_IN_DAY) / (cbm::DAIR * cbm::CAIR * cbm::DM3_IN_M3));

    /* Temperature tolerance (switches between single and iterative Penman) */
    double const tmdi( 0.0);
    if ( cbm::flt_greater_zero( tmdi))
    {
        double dtn( 0.0);
        int inloop = 0;
        bool equil  = false;
        while ( (inloop == 0) || !equil)
        {
            dt = (dt + dtn) / 2.0;

            /* net radiation and slope of saturated vapour pressure */
            rad_long_out_sec = ldndc::meteo::stefan_boltzmann( _temp + dt); // [J m-2 s-1]
            rad_long_out_day = rad_long_out_sec * cbm::SEC_IN_DAY; // [J m-2 day-1]

            rad_net = (1.0 - _albedo) * _rad_short_day + rad_long_out_day - rad_long_in_day;

            double vps2( 0.0);
            saturated_vapour_pressure( (_temp + dt), &vps2, NULL);
            vps_slope = (vps2 - vps) / dt;

            /*
             * actual water loss, resistance to vapour transfer and
             * estimated temperature difference
             */
            etrd = (rad_net * (vps_slope / (vps_slope + cbm::GAMMA))) / (cbm::LAMBDA * cbm::J_IN_MJ);
            etae = (cbm::GAMMA * ea) / (vps_slope + cbm::GAMMA);
            etd  = etrd + etae;

            re   = cbm::SEC_IN_DAY * 1000.0 * 0.018016 / (f_w * cbm::RGAS * (_temp + 0.5 * dt + 273.16));
            dtn  = re * ((rad_net - (cbm::LAMBDA * cbm::J_IN_MJ) * etd) / cbm::SEC_IN_DAY) / (cbm::DAIR * cbm::CAIR * cbm::DM3_IN_M3);

            /* check on equilibrium and maximum number of iterations */
            equil  = (std::abs( dtn - dt) < tmdi);
            inloop = inloop + 1;

            dt = dtn;
        }
    }

    return  cbm::bound_min( 0.0, etd * cbm::M_IN_MM);
}



/*!
 * @page waterlibs
 *  <b> Saturated vapour pressure </b> \n
 *  Saturated vapour pressure \f$ p_s \f$ and respective
 *  derivative with respect to temperature are given by:
 *  \f{eqnarray*}{
 *  p_s  &=& 0.61 \cdot \mathrm{e}^{ \left (\frac{17.32 \cdot T}{T + 238.102} \right)} \\
 *  \frac{\text{d}p_s}{\text{d}T} &=& 238.102 \cdot 17.32 \cdot \frac{p_s}{(T + 238.102)^{2}}
 *  @f}
 */
lerr_t
ldndc::saturated_vapour_pressure(
                                 double _temp /* temperature [oC] */,
                                 double *_svp /* saturated vapour pressure [kPa] */,
                                 double *_slope /* slope of saturated vapour pressure [k:Pa:oC^-1] */)
{
    if ( _svp)
    {
        *_svp  = 0.1 * 6.10588 * std::exp( 17.32491 * _temp / (_temp + 238.102));
        if ( _slope)
        {
            *_slope = 238.102 * 17.32491 * (*_svp) / std::pow( _temp + 238.102, 2.0);
        }
    }

    return  LDNDC_ERR_OK;
}



/*!
 * @page waterlibs
 *  <b> Wind function </b> \n
 *  The wind function is chosen depending on the surface type.
 */
double
ldndc::wind_function(
                     surface_type _surface,
                     double _wind)
{
    if ( (_surface == water) || (_surface == bare_soil))
    {
        /*!
         * @page waterlibs
         * Bare soil @cite kraalingen:1997a :
         *  \f[ f_w = 2.63 \cdot (0.5 + 0.54 v_w) \f]
         */
        return 2.63 * (0.5 + 0.54 * _wind);
    }
    else if (_surface == short_grass)
    {
        /*!
         * @page waterlibs
         * Short grass @cite kraalingen:1997a :
         *  \f[ f_w = 2.63 \cdot (1.0 + 0.54 v_w) \f]
         * - \f$v_w\f$: wind speed
         */
        return 2.63 * (1.0 + 0.54 * _wind);
    }
    else
    {
        return 0.0;
    }
}


