/*!
 * @file ld_droughtstress.cpp
 * Various kinds of drought stress models (implementation)
 *
 * @author
 * - Ruediger Grote
 * - David Kraus (created: may, 2016)
 */

#include  "watercycle/ld_droughtstress.h"
#include  "math/cbm_math.h"
#include  "string/cbm_string.h"

DroughtStress::DroughtStress( char const *  _kind):
                              cbm::TinyModelCollection< DroughtStress,
                              double,
                              double >(),
                              fh2o_ref( 0.0),
                              gs_slope( 0.0)
{
    if ( !_kind)
    {
        this->f_eval = &DroughtStress::exponential;
    }
    else
    {
        this->f_eval = this->evaluate_function( _kind);
        if ( !this->f_eval)
        {
            LOGERROR( "no such drought stress model '",_kind,
                      "'; falling back to 'exponential'");
            this->f_eval = &DroughtStress::exponential;
        }
    }
}

DroughtStress::DroughtStressFunction
DroughtStress::evaluate_function( char const *  _kind) const
{
    if ( !_kind /*default */)
        { return this->f_eval; }
    else if ( cbm::is_equal( _kind, "exponential"))
        { return &DroughtStress::exponential; }
    else if ( cbm::is_equal( _kind, "linear_threshold"))
        { return &DroughtStress::linear_threshold; }
    else if ( cbm::is_equal( _kind, "linear"))
        { return &DroughtStress::linear; }
    else
        { return 0; }
}

double
DroughtStress::evaluate( double  _fh2o, char const *  _kind) const
{
    DroughtStressFunction  ds_func = this->evaluate_function( _kind);
    if ( !ds_func)
    { return invalid_dbl; }

    return (this->*ds_func)( _fh2o);
}


/*!
 * @page veglibs
 * @section veglibs_drough_stress Drought stress
 * @subsection veglibs_drough_stress_exponential Exponential relationship
 * Exponential drought stress factor \f$ \phi_d \f$:
 * \f[
 * \phi_d = \frac{1 - e^{-f_{H2O} gsslope}}{1 - e^{-gsslope}}
 * \f]
 *
 * Related literature:
 * Soil drought impact on stomatal conductance @cite vanwijk:2000a
 * \f[
 * \phi_d = 1.016 - 0.016 \cdot e^{ 4.1 \cdot ( 1.0 - _fh2o)}
 * \f]
 *
 * A general relationship for 5 tree species was developed by @cite granier:2000a
 * \f[
 * \phi_d = (1.154 + 3.0195 \cdot _fh2o - ((1.154 + 3.0195 * _fh2o)^2 - 2.8 \cdot 1.154 \cdot 3.0195 \cdot _fh2o)^{0.5}) /1.4));
 * \f]
 */
double
DroughtStress::exponential( double  _fh2o) const
{
    /* General function for exponential decline between 1 (no drought)
     * and 0 (no available water) with parameter controlled curvatur
     */
    return cbm::bound( 0.0,
                       (1.0 - std::pow( std::exp( -(_fh2o)), this->gs_slope)) /
                       (1.0 - std::pow( std::exp( -1.0), this->gs_slope)),
                       1.0);

}


/*!
 * @page veglibs
 * @subsection veglibs_drough_stress_linear Linear relationship
 * Linearly calculated soil drought impact on stomatal conductance (\f$ \phi_d \f$)
 * according to @cite wang:1998a (more flexible due to species-specific sensitivity):
 * \f[
 * \phi_d = min \left ( \frac{f_{H2O}}{f_{H2O,ref}} , 1 \right )
 * \f]
 */
double
DroughtStress::linear_threshold( double  _fh2o) const
{
    /*!
     *
     */
    if ( cbm::flt_greater_zero( fh2o_ref))
    {
        return cbm::bound_max( _fh2o / fh2o_ref, 1.0);
    }
    else
    {
        return 1.0;
    }
}

double
DroughtStress::linear( double  _fh2o) const
{
    /* soil drought impact as used e.g. in @cite knauer:2015a */
    return std::min( 1.0, _fh2o);
}


/*!
 * @page veglibs
 * @subsection veglibs_drough_stress_non_stomatal_water_limitation Non-stomatal water limitation
 * Non-stomatal impact of drought stress (Tuzet et al. 2003)
 */
double
ldndc::non_stomatal_water_limitation(
                                     double const & _var,
                                     double const & _var_ref,
                                     double const & _var_scale)
{
    // water impact on non-stomatal limitations based on Tuzet et al.
    if ( cbm::flt_greater_equal_zero( _var))
    {
        return 1.0;
    }
    else
    {
        return cbm::bound(0.0, (1.0 + std::exp(_var_ref * _var_scale)) / (1.0 + std::exp((_var_ref - _var) * _var_scale)), 1.0);
    }
}
