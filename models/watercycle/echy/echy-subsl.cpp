/*!
 * @brief
 *      david kraus
 *
 */

#include  "watercycle/echy/echy.h"
#include  <scientific/hydrology/ld_vangenuchten.h>

namespace ldndc {


/*!
 * @brief
 *
 */
lerr_t
EcHy::EcHyGroundwater()
{
    if ( gw_depth < 99.0)
    {
        /*!
         *  Negative groundwater table represents
         *  water on the soil surface
         */
        if ( !cbm::flt_greater_equal_zero( gw_depth))
        {
            wl0fill = -gw_depth;
        }

        //groundwater access
        for (int i = soillayers_in->soil_layer_cnt()-1; i >= 0; i--)
        {
            gw_fill_sl[i] = 0.0;

            double const layer_midpoint_depth( sc_.depth_sl[i] - 0.5 * sc_.h_sl[i]);
            if ( cbm::flt_greater_equal( layer_midpoint_depth, gw_depth))
            {
                if ( i == 1)
                {
                    gw_fill_sl[i] = std::max(0.0, ((wlst_sl[i] - wl_sl[i])
                                               +trwl_sl[i] + wlfl_sl[i+1] + evsws_sl[i] - wlfl_sl[i]));
                }
                else
                {
                    gw_fill_sl[i] = std::max(0.0, ((wlst_sl[i] - wl_sl[i])
                                               + trwl_sl[i] + wlfl_sl[i+1] - wlfl_sl[i]));
                }
                wc_.accumulated_groundwater_access += gw_fill_sl[i];
            }

            //capillary rise
            else if ( (wl_sl[i] > 0.01) && (wl_sl[i] < wlfc_sl[i]))
            {
                //matrix moisture suction in kpa
                double capillary_pressure( ldndc::hydrology::capillary_pressure( wl_sl[i] / sc_.h_sl[i],
                                                                                vga_sl[i], vgn_sl[i], vgm_sl[i], wlst_sl[i] / sc_.h_sl[i], 0.001));

                if ( capillary_pressure > 100.0)
                {
                    double flow( 0.0);
                    EcHySubsl2( capillary_pressure,
                                (gw_depth - layer_midpoint_depth) * cbm::CM_IN_M,
                                i, wlst_sl[i] / sc_.h_sl[i], flow);

                    //If flow negative (percolation) then reset at zero
                    if (flow > 0.0)
                    {
                        if (i == 0)
                        {
                            cr_fill_sl[i] = cbm::bound( 0.0,
                                                  wlst_sl[i] - wl_sl[i] + trwl_sl[i] + evsws_sl[i] + wlfl_sl[i+1] - wlfl_sl[i] - gw_fill_sl[i],
                                                  flow);
                        }
                        else
                        {
                            cr_fill_sl[i] = cbm::bound( 0.0,
                                                  wlst_sl[i] - wl_sl[i] + trwl_sl[i] + wlfl_sl[i+1] - wlfl_sl[i] - gw_fill_sl[i],
                                                  flow);
                            wc_.accumulated_capillaryrise += cr_fill_sl[i];
                        }
                    }
                }
            }
        }
    }

    return LDNDC_ERR_OK;
}


    
/*!
 * @brief
 *      This routine calculates the rate of capillary flow or percolation between
 *      groundwater table and root zone. The stationary flow is found by
 *      integration of dZL = K.d(MH)/(K + FLW), where Z= height above
 *      groundwater, MH= matric head, K= conductivity and FLW= chosen flow.
 *      In an iteration loop the correct flow is found. The integration
 *      goes at most over four intervals: [0,45], [45,170], [170,330] and
 *      [330,MH-rootzone] (last one on logarithmic scale).
 *
 * @author
 *      C. Rappoldt
 *      M. Wopereis
 *
 * @date
 *      January 1986, revised June 1990
 *
 * @documentation
 *      Chapter 15 in documentation WOFOST Version 4.1 (1988)
 */
lerr_t
EcHy::EcHySubsl2(
                 double const &_cp /* capillary pressure [kPa] */,
                 double const &_delta_z /* distance to groundwater [m] */,
                 int const &_sl /* soil layer [-] */,
                 double const &_saturation /* water content at saturation (porosity) [m3 m-3] */,
                 double &_flow /* resulting upward flow from gropundwater [mm day-1] */)
{
    double elog10( 2.302585);
    double logst4( 2.518514);

    double start[4] = {0.0, 45.0, 170.0, 330.0};


    //calculation of pf from matrix head
    double pf( std::log10( _cp));

    //in case of small matric head (saturated conditions)
    if (pf <= 0.0)
    {
        //flow in mm/d
        _flow = kst_sl[_sl] * (_cp / _delta_z - 1.0);

        return LDNDC_ERR_OK;
    }


    //number and width of integration intervals
    int iint( 0);
    double del[4] = { 0.0, 0.0, 0.0, 0.0};
    for (int i1 = 0; i1 < 4; i1++)
    {
        if (i1 < 3)
        {
            del[i1] = std::min( start[i1+1], _cp) - start[i1];
        }
        if (i1 == 3)
        {
            del[i1] = pf - logst4;
        }
        if (del[i1] <= 0.0)
        {
            break;
        }
        iint += 1;
    }

    //preparation of three-point gaussian integration
    double hulp[12] = { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
    double conduc[12] = { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
    for (int i1 = 0; i1 < iint; i1++)
    {
        for (int i2 = 0; i2 < 3; i2++)
        {
            double pgau[3] = {0.1127016654, 0.5, 0.8872983346};
            double wgau[3] = {0.2777778, 0.4444444, 0.2777778};

            int i3( 3 * i1 + i2);

            //the three points in the full-width intervals are standard
            double pfstan[9] = {0.705143, 1.352183, 1.601282, 1.771497, 2.031409, 2.192880, 2.274233, 2.397940, 2.494110};
            double pfgau( pfstan[i3]);

            //the three points in the last interval are calculated
            if ( i1 == (iint-1))
            {
                if ( iint < 4)
                {
                    pfgau = std::log10( start[iint-1] + pgau[i2] * del[iint-1]);
                }
                else
                {
                    pfgau = logst4 + pgau[i2] * del[iint-1];
                }
            }

            double const wcl_loc( ldndc::hydrology::water_content( std::exp(elog10 * pfgau), vga_sl[_sl], vgn_sl[_sl], vgm_sl[_sl], _saturation, 0.1));
            conduc[i3] = ldndc::hydrology::hydraulic_conductivity(wcl_loc, vgm_sl[_sl], _saturation, sc_.wcmin_sl[_sl], kst_sl[_sl] * cbm::CM_IN_M);
            hulp[i3] = del[i1] * wgau[i2] * conduc[i3];
            if (i3 > 8)
            {
                hulp[i3] = hulp[i3] * elog10 * std::exp( elog10 * pfgau);
            }
        }
    }


    //setting upper and lower limit
    double const wcl_loc( ldndc::hydrology::water_content( _cp, vga_sl[_sl], vgn_sl[_sl], vgm_sl[_sl], _saturation, 0.1));
    double const kms( ldndc::hydrology::hydraulic_conductivity(wcl_loc, vgm_sl[_sl], _saturation, sc_.wcmin_sl[_sl], kst_sl[_sl] * cbm::CM_IN_M));
    double fu( (_cp <= _delta_z) ? 0.0 : 1.27);
    double fl( (_cp >= _delta_z) ? 0.0 : -kms);


    if ( !cbm::flt_equal( _cp, _delta_z))
    {
        //iteration loop
        int imax( 3 * iint);
        for ( int i1 = 0; i1 < 14; i1++)
        {
            double flw( (fu + fl) / 2.0);
            double df( (fu - fl) / 2.0);
            if ( (df < 0.01) &&
                ((df / std::abs(flw)) < 0.1))
            {
                break;
            }

            double z( 0.0);
            for ( int i2 = 0; i2 < imax; i2++)
            {
                z  = z + hulp[i2] / (conduc[i2] + flw);
            }
            if (z >= _delta_z)
            {
                fl = flw;
            }
            if (z <= _delta_z)
            {
                fu = flw;
            }
        }
    }
    
    //flow output in mm/d
    _flow = 10.0 * (fu + fl) / 2.0;
    
    return LDNDC_ERR_OK;
}



} /*namespace ldndc*/

