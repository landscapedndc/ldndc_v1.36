/*!
 * @file
 * @author
 *    - David Kraus
 *
 * @date
 *    Nov, 2017
 */

#include  "watercycle/echy/echy.h"
#include  "watercycle/ld_thornthwaite.h"

namespace ldndc {



/*!
 * @details
 *
 */
EcHy::EcHy(
         MoBiLE_State *  _state,
         cbm::io_kcomm_t *  _io,
         timemode_e  _timemode)
          : MBE_LegacyModel( _state, _timemode),
            io_kcomm( _io),
            gw_( NULL),
            m_climate( _io->get_input_class< climate::input_class_climate_t >()),
            m_setup( _io->get_input_class< setup::input_class_setup_t >()),
            soillayers_in( _io->get_input_class< input_class_soillayers_t >()),
            m_param( _io->get_input_class< siteparameters::input_class_siteparameters_t >()),
            sc_( _state->get_substate_ref< substate_soilchemistry_t >()),
            mc_( _state->get_substate_ref< substate_microclimate_t >()),
            wc_( _state->get_substate_ref< substate_watercycle_t >()),
            m_veg( &_state->vegetation),
            m_eventflood( _state, _io, _timemode),
            m_snowdndc( _io),
            gw_depth_static( ldndc::site::site_info_defaults.watertable),
            accumulated_potentialtranspiration_old( wc_.accumulated_potentialtranspiration),
            accumulated_irrigation_old( wc_.accumulated_irrigation),
            trwl_sl( lvector_t< double >( soillayers_in->soil_layer_cnt(), 0.0)),
            wlfl_sl( lvector_t< double >( soillayers_in->soil_layer_cnt()+1, 0.0)),

            cr_fill_sl( lvector_t< double >( soillayers_in->soil_layer_cnt(), 0.0)),
            gw_fill_sl( lvector_t< double >( soillayers_in->soil_layer_cnt(), 0.0)),
            bypass_fill_sl( lvector_t< double >( soillayers_in->soil_layer_cnt(), 0.0)),
            wc_max_sl( lvector_t< double >( soillayers_in->soil_layer_cnt(), 0.0)),
            wc_min_sl( lvector_t< double >( soillayers_in->soil_layer_cnt(), 0.0)),
            evsws_sl( lvector_t< double >( soillayers_in->soil_layer_cnt(), 0.0)),
            kst_sl( lvector_t< double >( soillayers_in->soil_layer_cnt(), 0.0)),
            wl_sl( lvector_t< double >( soillayers_in->soil_layer_cnt(), 0.0)),
            wlfc_sl( lvector_t< double >( soillayers_in->soil_layer_cnt(), 0.0)),
            wlwp_sl( lvector_t< double >( soillayers_in->soil_layer_cnt(), 0.0)),
            wlst_sl( lvector_t< double >( soillayers_in->soil_layer_cnt(), 0.0)),
            ev_leaf( 0.0),
            gw_fill_surface( 0.0),
            cr_fill_groundwater( 0.0),
            runoff( 0.0),
            snowfall( 0.0),
            throughfall( 0.0),
            interception_water( 0.0),
            thornthwaite_heat_index( 0.0),
            daily_potential_evapotranspiration( 0.0),
            daily_potential_leaf_evaporation( 0.0),
            daily_potential_soil_evaporation( 0.0),
            daily_potential_transpiration( 0.0),
            minimum_watertable_height( 0.0),
            bund_height( 0.0),
            irrigation( 0.0),
            irrigation_event( 0.0),
            irrigation_switch( NONE),
            surface_water( 0.0),
            surface_ice( 0.0)
{
    m_snowdndc.dt = 1.0 / lclock()->time_resolution();

    size_t S( soillayers_in->soil_layer_cnt());
    m_icecontent_in.h_sl.resize( S);
    m_icecontent_in.bulkdensity_sl.resize( S);
    m_icecontent_in.fcorg_sl.resize( S);
    m_icecontent_in.wc_sl.resize( S);
    m_icecontent_in.ice_sl.resize( S);
    m_icecontent_in.soil_temperature_sl.resize( S);
    m_icecontent_out.wc_sl.resize( S);
    m_icecontent_out.ice_sl.resize( S);
    m_icecontent_out.soil_temperature_sl.resize( S);
}



/*!
 * @details
 *
 */
EcHy::~EcHy()
{}



/*!
 * @details
 *
 */
lerr_t
EcHy::finalize()
{
    return  LDNDC_ERR_OK;
}



/*!
 * @details
 *
 */
lerr_t
EcHy::sleep()
{
    return  LDNDC_ERR_OK;
}



/*!
 * @details
 *
 */
lerr_t
EcHy::wake()
{
    return  LDNDC_ERR_OK; 
}




/*!
 * @page echy
 * @section echyoptions Model options
 * Available options:
 * Default options are marked with bold letters.
 *  - Potential evapotranspiration model (default: "potentialevapotranspiration" = \b thornthwaite / penman, priestleytaylor)
 */
lerr_t
EcHy::configure(
                ldndc::config_file_t const *  )
{
    /* choose evapotranspiration method */
    evapotranspiration_method = get_option< char const * >( "potentialevapotranspiration", "thornthwaite");

    return  LDNDC_ERR_OK;
}



/*!
 * @details
 *
 */
lerr_t
EcHy::register_ports(
                     cbm::io_kcomm_t *  _io_kcomm)
{
    lerr_t rc_flood = m_eventflood.register_ports( _io_kcomm);
    if ( rc_flood) { return LDNDC_ERR_FAIL; }

    int rc_manure = this->m_ManureEvents.subscribe( "manure", _io_kcomm);
    if ( rc_manure != LD_PortOk)
    { return  LDNDC_ERR_FAIL; }

    int rc = this->m_IrrigationEvents.subscribe( "irrigate", _io_kcomm);
    if ( rc != LD_PortOk)
    { return  LDNDC_ERR_FAIL; }

    return  LDNDC_ERR_OK;
}



/*!
 * @details
 *
 */
lerr_t
EcHy::unregister_ports(
                       cbm::io_kcomm_t *  _io_kcomm)
{
    m_eventflood.unregister_ports( _io_kcomm);

    m_ManureEvents.unsubscribe();

    m_IrrigationEvents.unsubscribe();

    return  LDNDC_ERR_OK;
}



/*!
 * @details
 *
 */
lerr_t
EcHy::initialize()
{
    /* check for proper time resolution */
    if ( ( lclock()->time_resolution() > 1) &&
         ( timemode() != TMODE_SUBDAILY))
    {
        KLOGERROR( "module \"", name(), "\" must run in subdaily resolution when simulation time resolution is greater 1");
        return  LDNDC_ERR_OBJECT_INIT_FAILED;
    }

    /* flooding */
    m_eventflood.initialize();

    /* groundwater
     * check if groundwater input is available
     * note: check gw_ for not NULL for any usage
     */
    cbm::source_descriptor_t  gw_source_info;
    lid_t  gw_id = io_kcomm->get_input_class_source_info< input_class_groundwater_t >( &gw_source_info);
    if ( gw_id != invalid_lid)
    {
        gw_ = io_kcomm->acquire_input< input_class_groundwater_t >( &gw_source_info);
    }
    else
    {
        gw_ = NULL;
    }

    for ( size_t i = 0; i < soillayers_in->soil_layer_cnt(); i++)
    {
        //saturated hydraulic conductivity in (m ts-1)
        kst_sl[i] = (sc_.sks_sl[i] * cbm::M_IN_CM * cbm::MIN_IN_DAY / lclock()->time_resolution());

        //calculate water contents in (m)
        wlfc_sl[i] = sc_.wcmax_sl[i] * sc_.h_sl[i];           //water content at field capacity
        wlwp_sl[i] = sc_.wcmin_sl[i] * sc_.h_sl[i];           //water content at wilting point

        EcHy_set_water_min_max( i, 1.0);

        //Initial (total) water content in soil profile (m)
        wl_sl[i] = wc_.wc_sl[i] * sc_.h_sl[i];
    }

    thornthwaite_heat_index = ldndc::thornthwaite_heat_index(
                                                     m_setup->latitude(),
                                                     lclock()->days_in_year(),
                                                     m_climate->annual_temperature_average(),
                                                     m_climate->annual_temperature_amplitude());
    
    return  LDNDC_ERR_OK;
}


lerr_t
EcHy::EcHy_set_water_min_max(
                    size_t _sl,
                    double const &_pore_space_reduction)
{
    wc_max_sl[_sl] = sc_.wfps_max_sl[_sl] * sc_.poro_sl[_sl];
    wc_min_sl[_sl] = sc_.wfps_min_sl[_sl] * sc_.poro_sl[_sl];

    double const porosity( _pore_space_reduction * sc_.poro_sl[_sl]);
    wlst_sl[_sl] = porosity * sc_.h_sl[_sl];

    return  LDNDC_ERR_OK;
}


} /*namespace ldndc*/
