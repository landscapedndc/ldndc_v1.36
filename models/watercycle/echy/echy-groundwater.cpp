/*!
 * @file
 *      david kraus
 *
 */

#include  "watercycle/echy/echy.h"
#include  <scientific/hydrology/ld_vangenuchten.h>

namespace ldndc {


/*!
 * @brief
 *
 */
lerr_t
EcHy::EcHyGroundwater()
{
    size_t sl_down( soillayers_in->soil_layer_cnt()-1);
    if ( cbm::flt_greater( sc_.depth_sl[sl_down], gw_depth_static))
    {
        /*!
         *  Negative groundwater table represents
         *  water on the soil surface
         */
        if ( cbm::flt_greater( -gw_depth_static, surface_water))
        {
            gw_fill_surface = -gw_depth_static - surface_water;
        }

        //groundwater access
        {
            double const layer_midpoint_depth( 0.5 * sc_.h_sl[0]);
            if ( cbm::flt_greater_equal( layer_midpoint_depth, gw_depth_static))
            {
                gw_fill_sl[0] = cbm::bound_min( 0.0,
                                                wlst_sl[0] - (wl_sl[0] + EcHySoilWaterChange( 0)));
                wc_.accumulated_groundwater_access += gw_fill_sl[0];
            }
            else
            {
                gw_fill_sl[0] = 0.0;
            }
        }

        for (size_t sl = 1; sl < soillayers_in->soil_layer_cnt()-1; sl++)
        {
            double const layer_midpoint_depth( sc_.depth_sl[sl] - 0.5 * sc_.h_sl[sl]);
            if ( cbm::flt_greater_equal( layer_midpoint_depth, gw_depth_static))
            {
                gw_fill_sl[sl] = cbm::bound_min( 0.0,
                                                 wlst_sl[sl] - (wl_sl[sl] + EcHySoilWaterChange( sl)));
                wc_.accumulated_groundwater_access += gw_fill_sl[sl];
            }
            else
            {
                gw_fill_sl[sl] = 0.0;
            }
        }

        {
            size_t sl_max( soillayers_in->soil_layer_cnt()-1);
            double const layer_midpoint_depth( sc_.depth_sl[sl_max] - 0.5 * sc_.h_sl[sl_max]);
            if ( cbm::flt_greater_equal( layer_midpoint_depth, gw_depth_static))
            {
                gw_fill_sl[sl_max] = cbm::bound_min( 0.0,
                                                     wlst_sl[sl_max] - (wl_sl[sl_max] + EcHySoilWaterChange( sl_max)));
                wc_.accumulated_groundwater_access += gw_fill_sl[sl_max];
            }
            else
            {
                gw_fill_sl[sl_max] = 0.0;
            }
        }
    }

    //capillary rise
    if ( m_param->WCDNDC_HAVE_CAPILLARY_ACTION())
    {
        size_t const STEPS( 10);
        for (size_t steps = 0; steps < STEPS; steps++)
        {
            for (size_t sl = 0; sl < soillayers_in->soil_layer_cnt()-1; sl++)
            {
                double const layer_midpoint_depth( sc_.depth_sl[sl] - 0.5 * sc_.h_sl[sl]);
                if ( cbm::flt_greater_equal( layer_midpoint_depth, gw_depth_static))
                {
                    break;
                }
                
                /* Water contents with until now calculated water balance */
                double const wl_new_0( wl_sl[sl] + EcHySoilWaterChange( sl));
                double const wl_new_1( wl_sl[sl+1] + EcHySoilWaterChange( sl+1));
                
                double const wc_vg_0( cbm::bound( wc_min_sl[sl], wl_new_0 / sc_.h_sl[sl], wc_max_sl[sl]));
                double const wc_vg_1( cbm::bound( wc_min_sl[sl+1], wl_new_1 / sc_.h_sl[sl+1], wc_max_sl[sl+1]));
                
                /* Unsaturated hydraulic conductivity with until now calculated water balance */
                double const kust_0( cbm::bound_min( 0.0,
                                                    ldndc::hydrology::hydraulic_conductivity(
                                                                                             wc_vg_0, sc_.vgm_sl[sl],
                                                                                             wc_max_sl[sl], wc_min_sl[sl], 1.0/double(STEPS)*kst_sl[sl])));
                double const kust_1( cbm::bound_min( 0.0,
                                                    ldndc::hydrology::hydraulic_conductivity(
                                                                                             wc_vg_1, sc_.vgm_sl[sl+1],
                                                                                             wc_max_sl[sl+1], wc_min_sl[sl+1], 1.0/double(STEPS)*kst_sl[sl+1])));

                double const capillary_pressure_0( ldndc::hydrology::capillary_pressure(
                                                                                        wl_new_0/sc_.h_sl[sl], sc_.vga_sl[sl], sc_.vgn_sl[sl], sc_.vgm_sl[sl],
                                                                                        wc_max_sl[sl], wc_min_sl[sl]));
                double const capillary_pressure_1( ldndc::hydrology::capillary_pressure(
                                                                                        wl_new_1/sc_.h_sl[sl+1], sc_.vga_sl[sl+1], sc_.vgn_sl[sl+1], sc_.vgm_sl[sl+1],
                                                                                        wc_max_sl[sl], wc_min_sl[sl+1]));

                double const kust( cbm::flt_greater( capillary_pressure_0, capillary_pressure_1) ? kust_1 : kust_0 );
                double const flow( (capillary_pressure_1 - capillary_pressure_0) / (0.5 * (sc_.h_sl[sl] + sc_.h_sl[sl+1])) * kust);

                /* downward flow*/
                if ( cbm::flt_greater_zero( flow))
                {
                    double const left( wl_sl[sl] + EcHySoilWaterChange( sl) - (wc_min_sl[sl] * sc_.h_sl[sl]));
                    double const space( wlst_sl[sl+1] - (wl_sl[sl+1] + EcHySoilWaterChange( sl+1)));
                    
                    if ( cbm::flt_greater_zero( left) &&
                         cbm::flt_greater_zero( space))
                    {
                        double const bound_flow( 1.0 / static_cast<double>(STEPS) * cbm::bound_max( flow, std::min( left, space)));
                        cr_fill_sl[sl] -= bound_flow;
                        cr_fill_sl[sl+1] += bound_flow;
                    }
                }
                /* upward flow */
                else if ( cbm::flt_less( flow, 0.0))
                {
                    double const left( wl_sl[sl+1] + EcHySoilWaterChange( sl+1) - (wc_min_sl[sl+1] * sc_.h_sl[sl+1]));
                    double const space( wlst_sl[sl] - (wl_sl[sl] + EcHySoilWaterChange( sl)));
                    
                    if ( cbm::flt_greater_zero( left) &&
                         cbm::flt_greater_zero( space))
                    {
                        double const bound_flow( cbm::bound_max( -flow, 1.0 / static_cast<double>(STEPS) * std::min( left, space)));
                        cr_fill_sl[sl] += bound_flow;
                        cr_fill_sl[sl+1] -= bound_flow;
                    }
                }
            }

            size_t sl( soillayers_in->soil_layer_cnt()-1);
            double const layer_midpoint_depth( sc_.depth_sl[sl] - 0.5 * sc_.h_sl[sl]);
            if ( cbm::flt_greater( gw_depth_static, layer_midpoint_depth))
            {
                /* Water contents with until now calculated water balance */
                double const wl_new( wl_sl[sl] + EcHySoilWaterChange( sl));
                
                double const wc_vg( cbm::bound( wc_min_sl[sl], wl_new / sc_.h_sl[sl], wc_max_sl[sl]));
                
                /* Unsaturated hydraulic conductivity with until now calculated water balance */
                double const kust( cbm::bound_min( 0.0,
                                                  ldndc::hydrology::hydraulic_conductivity(
                                                                                           wc_vg, sc_.vgm_sl[sl],
                                                                                           wc_max_sl[sl], wc_min_sl[sl], 1.0/double(STEPS)*kst_sl[sl])));
                
                double const capillary_pressure( ldndc::hydrology::capillary_pressure(
                                                                                      wl_new/sc_.h_sl[sl], sc_.vga_sl[sl], sc_.vgn_sl[sl], sc_.vgm_sl[sl],
                                                                                      wc_max_sl[sl], wc_min_sl[sl]));

                /* upward flow */
                double const flow( -capillary_pressure / (gw_depth_static - layer_midpoint_depth) * kust);
                if ( cbm::flt_less( flow, 0.0))
                {
                    double const space( wlst_sl[sl] - (wl_sl[sl] + EcHySoilWaterChange( sl)));
                    if ( cbm::flt_greater_zero( space))
                    {
                        double const bound_flow( cbm::bound_max( -flow, 1.0 / static_cast<double>(STEPS) * space));
                        cr_fill_sl[sl] += bound_flow;
                        cr_fill_groundwater += bound_flow;
                    }
                }
            }
        }
    }

    return LDNDC_ERR_OK;
}


    
/*!
 * @brief
 *      This routine calculates the rate of capillary flow or percolation between
 *      groundwater table and root zone. The stationary flow is found by
 *      integration of dZL = K.d(MH)/(K + FLW), where Z= height above
 *      groundwater, MH= matric head, K= conductivity and FLW= chosen flow.
 *      In an iteration loop the correct flow is found. The integration
 *      goes at most over four intervals: [0,45], [45,170], [170,330] and
 *      [330,MH-rootzone] (last one on logarithmic scale).
 *
 * @author
 *      C. Rappoldt
 *      M. Wopereis
 *
 * @date
 *      January 1986, revised June 1990
 *
 * @documentation
 *      Chapter 15 in documentation WOFOST Version 4.1 (1988)
 */
lerr_t
EcHy::EcHySubsl2(
                 double const &_cp /* capillary pressure [kPa] */,
                 double const &_delta_z /* distance to groundwater [m] */,
                 int const &_sl /* soil layer [-] */,
                 double const &_saturation /* water content at saturation (porosity) [m3 m-3] */,
                 double &_flow /* resulting upward flow from gropundwater [mm day-1] */)
{
    //calculation of pf from matrix head
    double pf( std::log10( _cp));

    //in case of small matric head (high water contents)
    if ( cbm::flt_greater( 1.0, pf))
    {
        _flow = 0.0;
        return LDNDC_ERR_OK;
    }

    double elog10( 2.302585);
    double logst4( 2.518514);

    double start[4] = {0.0, 45.0, 170.0, 330.0};

    //number and width of integration intervals
    int iint( 0);
    double del[4] = { 0.0, 0.0, 0.0, 0.0};
    for (int i1 = 0; i1 < 4; i1++)
    {
        if (i1 < 3)
        {
            del[i1] = std::min( start[i1+1], _cp) - start[i1];
        }
        else
        {
            del[i1] = pf - logst4;
        }
        if (del[i1] <= 0.0)
        {
            break;
        }
        iint += 1;
    }

    //preparation of three-point gaussian integration
    double hulp[12] = { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
    double conduc[12] = { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
    for (int i1 = 0; i1 < iint; i1++)
    {
        for (int i2 = 0; i2 < 3; i2++)
        {
            double pgau[3] = {0.1127016654, 0.5, 0.8872983346};
            double wgau[3] = {0.2777778, 0.4444444, 0.2777778};

            int i3( 3 * i1 + i2);

            //the three points in the full-width intervals are standard
            double pfstan[9] = {0.705143, 1.352183, 1.601282, 1.771497, 2.031409, 2.192880, 2.274233, 2.397940, 2.494110};
            double pfgau( pfstan[i3]);

            //the three points in the last interval are calculated
            if ( i1 == (iint-1))
            {
                if ( iint < 4)
                {
                    pfgau = std::log10( start[iint-1] + pgau[i2] * del[iint-1]);
                }
                else
                {
                    pfgau = logst4 + pgau[i2] * del[iint-1];
                }
            }

            double const wcl_loc( ldndc::hydrology::water_content( std::exp(elog10 * pfgau), sc_.vga_sl[_sl], sc_.vgn_sl[_sl], sc_.vgm_sl[_sl], _saturation, 0.1));
            conduc[i3] = ldndc::hydrology::hydraulic_conductivity(wcl_loc, sc_.vgm_sl[_sl], _saturation, sc_.wcmin_sl[_sl], kst_sl[_sl] * cbm::CM_IN_M);
            hulp[i3] = del[i1] * wgau[i2] * conduc[i3];
            if (i3 > 8)
            {
                hulp[i3] = hulp[i3] * elog10 * std::exp( elog10 * pfgau);
            }
        }
    }


    //setting upper and lower limit
    double const wcl_loc( ldndc::hydrology::water_content( _cp, sc_.vga_sl[_sl], sc_.vgn_sl[_sl], sc_.vgm_sl[_sl], _saturation, 0.1));
    double const kms( ldndc::hydrology::hydraulic_conductivity(wcl_loc, sc_.vgm_sl[_sl], _saturation, sc_.wcmin_sl[_sl], kst_sl[_sl] * cbm::CM_IN_M));
    double fu( (_cp <= _delta_z) ? 0.0 : 1.27);
    double fl( (_cp >= _delta_z) ? 0.0 : -kms);


    if ( !cbm::flt_equal( _cp, _delta_z))
    {
        //iteration loop
        int imax( 3 * iint);
        for ( int i1 = 0; i1 < 14; i1++)
        {
            double flw( (fu + fl) / 2.0);
            double df( (fu - fl) / 2.0);
            if ( (df < 0.01) &&
                ((df / std::abs(flw)) < 0.1))
            {
                break;
            }

            double z( 0.0);
            for ( int i2 = 0; i2 < imax; i2++)
            {
                z  = z + hulp[i2] / (conduc[i2] + flw);
            }
            if (z >= _delta_z)
            {
                fl = flw;
            }
            if (z <= _delta_z)
            {
                fu = flw;
            }
        }
    }

    //flow output in mm
    _flow = 10.0 * (fu + fl) / 2.0;

    return LDNDC_ERR_OK;
}



} /*namespace ldndc*/

