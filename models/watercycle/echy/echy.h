
#ifndef  MOBILE_MODULE_ECHY_H_
#define  MOBILE_MODULE_ECHY_H_

#include  "mbe_legacymodel.h"
#include  "state/mbe_state.h"
#include  "ld_shared.h"

#include  "ld_eventqueue.h"
#include  "eventhandler/flood/flood.h"

#include  "watercycle/ld_icesnow.h"

namespace ldndc {

class  substate_microclimate_t;
class  substate_soilchemistry_t;
class  substate_watercycle_t;

/*!
 * @brief
 *  Watercycle model EcosystemHydrology - EcHy
 */
class  LDNDC_API  EcHy  :  public  MBE_LegacyModel
{
    LMOD_EXPORT_MODULE_INFO(EcHy,"watercycle:echy","EcHy");


public:
    EcHy( MoBiLE_State *,
           cbm::io_kcomm_t *, timemode_e);
    ~EcHy();

    lerr_t  configure( ldndc::config_file_t const *);
    lerr_t  register_ports( cbm::io_kcomm_t *);
    lerr_t  initialize();

    lerr_t  solve();
    lerr_t  unregister_ports( cbm::io_kcomm_t *);
    lerr_t  finalize();

    lerr_t  sleep();
    lerr_t  wake();

private:

    cbm::io_kcomm_t *  io_kcomm;

    input_class_groundwater_t const *  gw_;
    input_class_climate_t const *  m_climate;
    input_class_setup_t const *  m_setup;
    input_class_soillayers_t const *  soillayers_in;
    input_class_siteparameters_t const *  m_param;

    substate_soilchemistry_t const &  sc_;
    substate_microclimate_t &  mc_;
    substate_watercycle_t &  wc_;
    MoBiLE_PlantVegetation *  m_veg;

    EventHandlerFlood  m_eventflood;
    WaterCycleSnowDNDC  m_snowdndc;

    SubscribedEvent<LD_EventHandlerQueue>  m_ManureEvents;
    SubscribedEvent<LD_EventHandlerQueue>  m_IrrigationEvents;

    /*! Saturated hydraulic conductivity below last soil layer [cm:min-1] */
    double kst_bottom;

    /*! Depth of groundwater table [m] */
    double gw_depth_static;

    /*! Accumulated potential transpiration of last time step [m] */
    double accumulated_potentialtranspiration_old;

    /*! Accumulated irrigation of last time step [m] */
    double accumulated_irrigation_old;

    /*! Water withdrawl by transpiration [m d−1] */
    lvector_t< double > trwl_sl;

    /*! Water flux at boundaries of soil layer [m d-1]
     *  the flux of water between soil layer is tracked by wlfl (mm d−1).
     *  in total, there are nl+1 flow rates, where nl is the number of soil layers,
     *  wlfl[1] is the flow rate between the ponded water layer and the soil surface,
     *  wlfl[2] is the flow rate between soil layer 1 and soil layer 2, etc.
     */
    lvector_t< double > wlfl_sl;

    /*! Soillayer water input due to capillary rise */
    lvector_t< double > cr_fill_sl;

    /*! Soillayer water input due to groundwater flow */
    lvector_t< double > gw_fill_sl;

    /*! Soillayer water input due to bypass flow */
    lvector_t< double > bypass_fill_sl;

    /*! Maximum water content */
    lvector_t< double > wc_max_sl;

    /*! Minimum water content */
    lvector_t< double > wc_min_sl;

    /*! Evaporation from soil layer */
    lvector_t< double > evsws_sl;

    /*! Saturated hydraulic conductivity */
    lvector_t< double > kst_sl;

    /*! Amount of water in soil layer  */
    lvector_t< double > wl_sl;

    /*! Amount of water in soil layer at field capacity  */
    lvector_t< double > wlfc_sl;

    /*! Amount of water in soil layer at wilting point  */
    lvector_t< double > wlwp_sl;

    /*! Amount of water in soil layer at saturation  */
    lvector_t< double > wlst_sl;

    /*! Evaporation from plant surface */
    double ev_leaf;

    /*! Water addition to surface water from groundwater water boundary condition */
    double gw_fill_surface;

    /*! Water addition to last soil layer from groundwater water by cappillary rise (only used for water balance calculation) */
    double cr_fill_groundwater;

    /*! Lateral surface runoff */
    double runoff;

    /*! Snowfall */
    double snowfall;

    /*! Throughfall to soil */
    double throughfall;

    /*! Amount of canopy intercepted water */
    double interception_water;

    /*! ... */
    double thornthwaite_heat_index;

    /*! ... */
    double daily_potential_evapotranspiration;

    /*! ... */
    double daily_potential_leaf_evaporation;

    /*! ... */
    double daily_potential_soil_evaporation;

    /*! ... */
    double daily_potential_transpiration;

    /* minimum height of surface watertable during flooding event (m) */
    double minimum_watertable_height;

    /* bund height during flooding event (m) */
    double bund_height;

    /* total amount of irrigation water applied including e.g., flooding (m) */
    double irrigation;

    /* irrigation by irrigation event (m) */
    double irrigation_event;

    enum irrigation_e
    {
        NONE,
        CONSTANT_POSITIVE_WATER_TABLE,
        CONSTANT_NEGATIVE_WATER_TABLE
    };
    irrigation_e irrigation_switch;

    //depth of ponded water at soil surface (mm)
    double surface_water;

    /* Snow [m] */
    double surface_ice;

    /*! ... */
    WaterCycleSnowDNDC::IceContentStateIn  m_icecontent_in;
    WaterCycleSnowDNDC::IceContentStateOut  m_icecontent_out;

    /*! ... */
    cbm::string_t evapotranspiration_method;

private:

    /*!
     * @brief
     */
    lerr_t
    EcHyIrrigation();

    /*!
     * @brief
     *      sets hydrologic conditions during flooding events, e.g.,
     *      - surface water table
     *      - bund height
     */
    lerr_t
    EcHyFlood();

    /*!
     * @brief
     *  Calculates water percolation within the soil profile.
     */
    lerr_t
    EcHyPercolation(
                    size_t /* integration step */,
                    size_t /* soil layer */);

    /*!
     * @brief
     *  Calculates water percolation within the soil profile.
     */
    lerr_t
    EcHyBypassFlow(
                   double,
                   double &/* bypassing water */);

    /*!
     * @brief
     *  Calculates evapotranspiration within the soil profile.
     */
    lerr_t
    EcHyEvapotranspiration();

    /*!
     * @brief
     *      ...
     */
    lerr_t
    EcHySubsl2(
               double const &,
               double const &,
               int const &,
               double const &,
               double &);

    /*!
     * @brief
     *      ...
     */
    lerr_t
    EcHyBalanceCheck(
                     double &);

    /*!
     * @brief
     *      ...
     */
    void
    EcHyreset();

    /*!
     * @brief
     *      ...
     */
    lerr_t
    EcHyGroundwater();

    /*!
     * @brief
     *      ...
     */
    lerr_t
    EcHyIntegration();

    /*!
     * @brief
     *      ...
     */
    lerr_t
    EcHyStepInit();

    /*!
     * @brief
     *      ...
     */
    lerr_t
    EcHyStepExit();

    /*!
     * @brief
     *      ...
     */
    double
    EcHyGetInterceptionCapacity();

    /*!
     * @brief
     *      ...
     */
    lerr_t
    EcHyCalculateLeafWaterDistribution(
                                       lvector_t< double > &/* leaf water */);

    /*!
     * @brief
     *  Calculates potential evapotranspiration.
     *  Specific concept can be given as model option.
     */
    lerr_t
    EcHyPotentialEvapotranspiration();

    /*!
     * @brief
     *  Calls SnowDNDC for the calculation
     *  of snowpack and soil ice formation.
     */
    lerr_t
    EcHySnowIce();

    /*!
     * @brief
     *      ...
     */
    double
    EcHyGetMinimumWater(
                        size_t /* soil layer */);

    /*!
     * @brief
     *      ...
     */
    double
    EcHyGetWiltingPoint(
                        size_t /* soil layer */);

    /*!
     * @brief
     *      ...
     */
    double
    EcHyGetAvailableWaterTranspiration(
                                       size_t /* soil layer */);

    /*!
     * @brief
     *      ...
     */
    double
    EcHyGetAvailableWaterEvaporation(
                                     size_t /* soil layer */);

    double
    EcHyGetDepthLimitation(
                           double _depth /* soil depth [m] */);

    /*!
     * @brief
     *      ...
     */
    double
    EcHyGetRootLimitation(
                          double /* dry weight fine roots [kg:m^-2] */);

    /*!
     * @brief
     *      ...
     */
    double
    EcHyGetWaterLimitationTranspiration(
                           double /* available water */,
                           size_t /* soil layer */);

    double
    EcHyGetWaterLimitationEvaporation(
                           double /* available water */,
                           size_t /* soil layer */);

    double
    EcHySoilWaterChange(
                    size_t /* soil layer */);

    lerr_t
    EcHy_set_water_min_max(
                    size_t /* soil layer */,
                    double const & /* porespace reduction */);

    lerr_t
    EcHy_check_for_negative_value(
                                  char const * /* position */);
};

} /*namespace ldndc*/

#endif  /*  !MOBILE_MODULE_ECHY_H_  */
