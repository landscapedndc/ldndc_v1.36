/*!
 * @file
 * @author
 *    - David Kraus
 * @date
 *    Nov, 2017
 */

#include  "watercycle/echy/echy.h"
#include  "watercycle/ld_penman.h"
#include  "watercycle/ld_priestley-taylor.h"
#include  "watercycle/ld_thornthwaite.h"

#include  <scientific/meteo/ld_meteo.h>

namespace ldndc {

/*!
 * @page echy
 * @section echy_evapotranspiration Potential evaporation (Echy)
 *  Actual evapotranspiration is the balance between atmospheric demand of
 *  water (potential evapotranspiration) and water supply by the surface.
 *  Implemented concepts for the calculation of potential evapotranspiration
 *  (default: Thornthwaite):
 *  @li @link thornthwaite Thornthwaite @endlink
 *  @li @link priestley_taylor Priestley and Taylor @endlink
 *  @li @link penman Penman @endlink
 */
lerr_t
EcHy::EcHyPotentialEvapotranspiration()
{
    /* clock */
    cbm::sclock_t const &  clk( this->lclock_ref());

    /* calculate all potential rates on a daily basis */
    if ( lclock()->is_position( TMODE_PRE_DAILY))
    {
        if ( evapotranspiration_method == "thornthwaite")
        {
            /* calculate thornthwaite heat index once per year */
            if ( lclock()->is_position( TMODE_PRE_YEARLY))
            {
                thornthwaite_heat_index = ldndc::thornthwaite_heat_index(
                                                             m_setup->latitude(),
                                                             lclock()->days_in_year(),
                                                             m_climate->annual_temperature_average(),
                                                             m_climate->annual_temperature_amplitude());
            }

            double const daylength( ldndc::meteo::daylength(
                                                     m_setup->latitude(),
                                                     lclock()->yearday()));

            daily_potential_evapotranspiration = ldndc::thornthwaite(
                                                          mc_.nd_airtemperature,
                                                          daylength,
                                                          thornthwaite_heat_index);
        }
        else if ( (evapotranspiration_method == "penman") ||
                  (evapotranspiration_method == "priestleytaylor"))
        {
            /* leaf area index */
            double lai( 0.0);
            for( PlantIterator  vt = m_veg->begin(); vt != m_veg->end(); ++vt)
            {
                lai += (*vt)->lai();
            }

            /* vapour pressure [10^3:Pa] */
            double vps( 0.0);
            ldndc::meteo::vps( mc_.nd_airtemperature, &vps);
            double vp( cbm::bound_min( 0.0, vps - m_climate->vpd_day( clk)));

            double const latitude( io_kcomm->get_input_class_ref< ldndc::setup::input_class_setup_t >().latitude());

            if ( evapotranspiration_method == "penman")
            {
                ldndc::surface_type surface( cbm::flt_greater_zero( lai) ? short_grass : bare_soil);

                daily_potential_evapotranspiration = ldndc::penman(
                                                           surface,
                                                           clk.yearday(),
                                                           clk.days_in_year(),
                                                           mc_.albedo,
                                                           latitude,
                                                           mc_.nd_shortwaveradiation_in * cbm::SEC_IN_DAY,
                                                           mc_.nd_airtemperature,
                                                           mc_.nd_windspeed,
                                                           vp);
            }
            else
            {
                daily_potential_evapotranspiration = ldndc::priestleytaylor(
                                                            clk.yearday(),
                                                            clk.days_in_year(),
                                                            mc_.albedo,
                                                            latitude,
                                                            mc_.nd_airtemperature,
                                                            mc_.shortwaveradiation_in,
                                                            vp,
                                                            m_param->PT_ALPHA());
            }
        }


        /* Scale potential rate */
        daily_potential_evapotranspiration *= m_param->WCDNDC_INCREASE_POT_EVAPOTRANS();

        /* potential evaporation from leafs
         * bound by potential evapotranspiration
         */
        daily_potential_leaf_evaporation = cbm::bound_min( 0.0,
                                                           std::min( interception_water,
                                                                     daily_potential_evapotranspiration));

        /* potential transpiration given as model input (calculated by vegetation models)
         * and bound by potential evapotranspiration
         */
        daily_potential_transpiration = cbm::bound_max( wc_.accumulated_potentialtranspiration
                                                        - accumulated_potentialtranspiration_old,
                                                        daily_potential_evapotranspiration
                                                        - daily_potential_leaf_evaporation);
        accumulated_potentialtranspiration_old = wc_.accumulated_potentialtranspiration;

        /* potential evaporation given by difference between potential evapotranspiration
         * and evaporation from leafs + transpiration
         */
        daily_potential_soil_evaporation = cbm::bound_min( 0.0, daily_potential_evapotranspiration
                                                                - daily_potential_leaf_evaporation
                                                                - daily_potential_transpiration);
    }

    return LDNDC_ERR_OK;
}



/*!
 * @page echy
 *  Evapotranspiration distinguishes evaporation from leaf and
 *  soil water as well as transpiration of the prevalent vegetation.
 *  While leaf evaporation is not limited, soil evaporation and
 *  transpiration depend on:
 *  - Soil water availability
 *  - Soil texture and depth
 *  - Fine root distribution
 *
 *  Soil layer specific transpiration \f$ TR(z) \f$ and soil
 *  evaporation \f$ EV(z) \f$ are given by:
 *  \f{eqnarray*}{
 *  TR(z) &=& f_l(\theta) \cdot f_l(m_{fr}) \cdot f_{r,w}(z) \cdot PTR \\
 *  EV(z) &=& f_l(\theta) \cdot f_{e}(z) \cdot PSE
 *  \f}
 *  The factors \f$ f_l(\theta) \f$ and \f$ f_l(m_{fr}) \f$ refer to
 *  limitations due to water availabilty and fine roots abundance,
 *  while \f$ f_{r,w}(z) \f$ and \f$ f_{e}(z) \f$ are distribution factors
 *  of total transpiration and evaporation water needs across the soil profile.
 *  \f$PTR\f$ and \f$PSE\f$ refer to potential transpiration and soil
 *  evaporation, respectively.
 */
lerr_t
EcHy::EcHyEvapotranspiration()
{
    /* clock */
    cbm::sclock_t const &  clk( this->lclock_ref());

    if ( cbm::flt_greater_zero( daily_potential_leaf_evaporation))
    {
        ev_leaf = daily_potential_leaf_evaporation / clk.time_resolution();
    }
    else
    {
        ev_leaf = 0.0;
    }

    if ( cbm::flt_greater(interception_water, ev_leaf))
    {
        interception_water -= ev_leaf;
    }
    else
    {
        ev_leaf = interception_water;
        interception_water = 0.0;
    }

    /*!
     * @page echy
     *  <b>Distribution factor for soil layer specific transpiration</b> \n
     *  Soillayer-specific transpiration depends on fine root abundance
     *  \f$ m_{fr}(z) \f$ and water availability \f$ \Delta\theta(z) \f$.
     *  For both, a soil layer specific relative share \f$ f_{x}(z) \f$ is
     *  calculated and, in a second step, harmonically weighted:
     *  \f{eqnarray*}{
     *  f_{r}(z) &=& \frac{m_{fr}(z)}{\sum m_{fr}} \\
     *  f_{w}(z) &=& \frac{\Delta\theta(z)}{\sum \Delta\theta} \\
     *  f_{r,w}(z) &=& \frac{(f_{r}, f_{w})_{harm.}(z)} {\sum (f_{r}, f_{w})_{harm.}}
     *  \f}
     *  Note, \f$ \sum f_{r} = \sum f_{r} = \sum f_{r,w} = 1 \f$. Hence respective
     *  factors do not limit transpiration but only distribute total transpiration
     *  needs across soil layers.
     */
    if ( cbm::flt_greater_zero( daily_potential_transpiration))
    {
        double mFrt_sum( 0.0);
        double w_avail_sum( 0.0);
        for ( size_t sl = 0; sl < soillayers_in->soil_layer_cnt(); sl++)
        {
            double const mfrt_veg_sl( m_veg->mfrt_sl( sl));
            if ( cbm::flt_greater_zero( mfrt_veg_sl))
            {
                if ( cbm::flt_greater_zero( EcHyGetAvailableWaterTranspiration( sl)))
                {
                    mFrt_sum += mfrt_veg_sl;
                    w_avail_sum += EcHyGetAvailableWaterTranspiration( sl);
                }
            }
            /* break below rooting zone */
            else{ break;}
        }

        if ( cbm::flt_greater_zero( w_avail_sum) &&
             cbm::flt_greater_zero( mFrt_sum))
        {
            double tr_rel_sum( 0.0);
            for ( size_t sl = 0; sl < soillayers_in->soil_layer_cnt(); sl++)
            {
                double const mfrt_veg_sl( m_veg->mfrt_sl( sl));
                if ( cbm::flt_greater_zero( mfrt_veg_sl))
                {
                    if ( cbm::flt_greater_zero( EcHyGetAvailableWaterTranspiration( sl)))
                    {
                        tr_rel_sum += cbm::harmonic_mean_weighted2(
                                                               mfrt_veg_sl / mFrt_sum,
                                                               EcHyGetAvailableWaterTranspiration( sl) / w_avail_sum,
                                                               m_param->ROOT_DEPENDENT_TRANS(), 1.0 - m_param->ROOT_DEPENDENT_TRANS());
                    }
                }
                /* break below rooting zone */
                else{ break;}
            }

            for ( size_t sl = 0; sl < soillayers_in->soil_layer_cnt(); sl++)
            {
                double const mfrt_veg_sl( m_veg->mfrt_sl( sl));
                if ( cbm::flt_greater_zero( mfrt_veg_sl))
                {
                    if ( cbm::flt_greater_zero( EcHyGetAvailableWaterTranspiration( sl)))
                    {
                        trwl_sl[sl] = daily_potential_transpiration / clk.time_resolution()
                                            * cbm::harmonic_mean_weighted2(
                                                       mfrt_veg_sl / mFrt_sum,
                                                       EcHyGetAvailableWaterTranspiration( sl) / w_avail_sum,
                                                       m_param->ROOT_DEPENDENT_TRANS(), 1.0 - m_param->ROOT_DEPENDENT_TRANS())
                                      / tr_rel_sum;
                    }
                }
                /* no transpiration below rooting zone */
                else
                {
                    trwl_sl[sl] = 0.0;
                }
            }
        }
    }

    if ( cbm::flt_greater_zero( daily_potential_soil_evaporation))
    {
        double depth_factor_sum( 0.0);
        for (size_t sl = 0; sl < soillayers_in->soil_layer_cnt(); sl++)
        {
            if ( cbm::flt_greater( m_param->EVALIM(), sc_.depth_sl[sl]))
            {
                depth_factor_sum += EcHyGetDepthLimitation( sc_.depth_sl[sl]);
            }
            else{ break;}
        }

        for (size_t sl = 0; sl < soillayers_in->soil_layer_cnt(); sl++)
        {
            if ( cbm::flt_greater( m_param->EVALIM(), sc_.depth_sl[sl]))
            {
                evsws_sl[sl] = daily_potential_soil_evaporation / clk.time_resolution()
                               * EcHyGetDepthLimitation( sc_.depth_sl[sl]) / depth_factor_sum;
            }
            else
            {
                evsws_sl[sl] = 0.0;
            }
        }
    }

    for (size_t sl = 0; sl < soillayers_in->soil_layer_cnt(); sl++)
    {
        /* Available water for evapotranspiration */
        double const w_avail_tr( EcHyGetAvailableWaterTranspiration( sl));
        double const w_avail_ev( EcHyGetAvailableWaterEvaporation( sl));
        double const w_avail_max( std::max( w_avail_tr, w_avail_ev));

        if ( cbm::flt_greater_zero( w_avail_max))
        {
            /* Reduction factor for evapotranspiration */
            double const r_limitation_tr( EcHyGetRootLimitation( m_veg->mfrt_sl( sl)));
            double const w_limitation_tr( EcHyGetWaterLimitationTranspiration( w_avail_tr, sl));
            double const w_limitation_ev( EcHyGetWaterLimitationEvaporation( w_avail_ev, sl));

            /* scale evapotranspiration if potential evapotranspiration is exceeded */
            double ev_pot( w_limitation_ev * EcHyGetDepthLimitation( sc_.depth_sl[sl]) * evsws_sl[sl]);
            double tr_pot( r_limitation_tr * w_limitation_tr * trwl_sl[sl]);
            double const evtr_pot( ev_pot + tr_pot);
            double const scale( cbm::flt_greater( evtr_pot, w_avail_max) ? w_avail_max / evtr_pot : 1.0);

            trwl_sl[sl] = scale * tr_pot;
            evsws_sl[sl] = scale * ev_pot;
        }
        else
        {
            trwl_sl[sl] = 0.0;
            evsws_sl[sl] = 0.0;
        }
    }

    return  LDNDC_ERR_OK;
}



/*!
 * @page echy
 *  <b>Distribution factor for soil layer specific evaporation</b> \n
 *  Soillayer-specific evaporation depends on depth:
 *  \f{eqnarray*}{
 *  f_d(z) &=& 1 - \frac{z}{z_{limit}} \\
 *  f_e(z) &=& \frac{f_d(z)}{\sum f_d}
 *  \f}
 *  Note, \f$ \sum f_{d} = \sum f_{e} = 1 \f$. Hence respective
 *  factors do not limit evaporation but only distribute total
 *  soil evaporation across soil layers.
 */
double
EcHy::EcHyGetDepthLimitation(
                             double _depth /* soil depth [m] */)
{
    return cbm::bound( 0.0,
                       1.0 - pow( _depth / m_param->EVALIM(), m_param->ECHY_EVAPORATION_DEPTH_LIMIT_EXPONENT()),
                       1.0);
}



/*!
 * @page echy
 *  <b>Water limitation for transpiration </b> \n
 *  The reduction of transpiration depending on soil
 *  water availability \f$\Delta\theta\f$ is given by:
 *  \f[
 *  f_l(\theta) = \frac{\Delta\theta}{\Delta\theta^{\ast}}
 *  \f]
 *  The reference amount of available water \f$ \Delta \theta^{\ast} \f$
 *  that does not limit transpiration is given by the difference
 *  between field capacity and wilting point:
 *  \f[
 *  \Delta \theta^{\ast} = \theta_{max} - \theta_{min}
 *  \f]
 */
double
EcHy::EcHyGetWaterLimitationTranspiration(
                                          double _wl_avail /* available water */,
                                          size_t _sl /* soil layer */)
{
    /* reference water availabilty for which no limitation occurs */
    double const wl_ref( wlfc_sl[_sl] - EcHyGetWiltingPoint( _sl));

    return cbm::bound( 0.0, pow(_wl_avail / wl_ref, 0.5), 1.0);
}



/*!
 * @page echy
 *  The amount of available water for transpiration
 *  \f$ \Delta \theta \f$ is given by:
 *  \f[
 *  \Delta \theta = \theta - \theta_{min}
 *  \f]
 */
double
EcHy::EcHyGetAvailableWaterTranspiration(
                                         size_t _sl /* soil layer [-] */)
{
    return cbm::bound_min( 0.0, wl_sl[_sl] - EcHyGetWiltingPoint( _sl));
}



/*!
 * @page echy
 *  <b>Water limitation for soil evaporation </b> \n
 *  The reduction of soil evaporation depending on soil
 *  water availability \f$\Delta\theta\f$ is given by:
 *  \f[
 *  f_l(\theta) = \frac{\Delta\theta}{\Delta\theta^{\ast}}
 *  \f]
 *  The reference amount of available water \f$ \Delta \theta^{\ast} \f$
 *  that does not limit soil evaporation is given by the difference
 *  between field capacity and residual water content:
 *  \f[
 *  \Delta \theta^{\ast} = \theta_{max} - \theta_{r,w}
 *  \f]
 */
double
EcHy::EcHyGetWaterLimitationEvaporation(
                                        double _wl_avail /* available water */,
                                        size_t _sl /* soil layer */)
{
    double const wc_min( EcHyGetMinimumWater( _sl));
    if ( cbm::flt_greater( _wl_avail, wc_min))
    {
        /* reference water availabilty for which no limitation occurs */
        double const wl_ref( wlfc_sl[_sl] - wc_min);
        return cbm::bound( 0.0, _wl_avail / wl_ref, 1.0);
    }
    else
    {
        return 0.0;
    }
}



/*!
 * @page echy
 *  The amount of available water for soil evaporation
 *  \f$ \Delta \theta \f$ is given by:
 *  \f[
 *  \Delta \theta = \theta - \theta_{r,w}
 *  \f]
 */
double
EcHy::EcHyGetAvailableWaterEvaporation(
                                       size_t _sl /* soil layer [-] */)
{
    return cbm::bound_min( 0.0, wl_sl[_sl] - EcHyGetMinimumWater( _sl));
}



double
EcHy::EcHyGetMinimumWater(
                          size_t _sl /* soil layer */)
{
    return wc_min_sl[_sl] * sc_.h_sl[_sl];
}



double
EcHy::EcHyGetWiltingPoint(
                          size_t _sl /* soil layer */)
{
    return m_param->WCDNDC_EVALIM_FRAC_WCMIN() * wlwp_sl[_sl];
}



/*!
 * @page echy
 *  <b>Fine roots limitation </b> \n
 *  The reduction of transpiration depending on fine roots abundance
 *  is calculated by a Michaelis-Menten relationship:
 *  \f[
 *  f_l(m_{fr}) = \frac{m^l_{fr}}{m^l_{fr} + K_{mm,fr}}
 *  \f]
 *  The Michaelis-Menten constant is given in the site parameter ECHY_KMM_ROOTS = \b ECHY_KMM_ROOTS .
 */
double
EcHy::EcHyGetRootLimitation(
                            double _m_frt /* dry weight fine roots [kg:m^-2] */)
{
    if ( cbm::flt_greater_zero( _m_frt))
    {
        return _m_frt / (_m_frt + m_param->ECHY_KMM_ROOTS());
    }
    else
    {
        return 0.0;
    }
}


} /*namespace ldndc*/

