/*!
 * @file
 * @author
 *  - David Kraus
 * @date
 *    Nov, 2017
 */

/*!
 * @page echy
 * @tableofcontents
 *
 * @section echy_guide User guide
 *
 *  @image html echy_overview.png "EcHy overview" width=500
 *  @image latex echy_overview.png "EcHy overview"
 *
 * @subsection echy_guide_structure Model structure
 *  EcHy requires further models for:
 *  - plant growth (e.g., transpiration demand)
 *
 * @subsection echy_guide_parametrization Parametrization
 * The following lists include site parameters that might be calibrated in order to represent the hydrological cycle.
 *
 * Evapotranspiration:
 * - \f$ WCDNDC\_INCREASE\_POT\_EVAPOTRANS \f$
 */

#include  "watercycle/echy/echy.h"
#include  <scientific/hydrology/ld_vangenuchten.h>

LMOD_MODULE_INFO(EcHy,TMODE_SUBDAILY|TMODE_POST_DAILY,LMOD_FLAG_USER);

REGISTER_OPTION(EcHy, potentialevapotranspiration,"  [char]");

namespace ldndc {


/*!
 * @details
 *  Kicks off computation for one time step.
 */
lerr_t
EcHy::solve()
{
    /* reset of all fluxes */
    EcHyreset();

    /* flooding events */
    lerr_t rc = EcHyFlood();
    if ( rc != LDNDC_ERR_OK)
    {
        KLOGERROR("Handling flooding event failed in: ", name());
        return  rc;
    }

    /* tasks:
     *  - update internal state (e.g., throughfall, ...)
     *  - calculate evapotranspiration on demand
     */
    rc = EcHyStepInit();
    if ( rc != LDNDC_ERR_OK)
    { return  rc; }
    
    /* begin water balance */
    double balance( -1.0);
    EcHyBalanceCheck( balance);

    /* snow and ice calculated */
    EcHySnowIce();

    /* 
     * apply irrigation triggered by
     *  - irrigation events
     *  - flooding events
     */
    EcHyIrrigation();

    double const potential_infiltration( surface_water + irrigation + throughfall);
    double bypass_flow( m_param->CRACK_FRACTION() * potential_infiltration);

    /* bypass flow */
    EcHyBypassFlow( m_param->CRACK_DEPTH(), bypass_flow);
    double const infiltration( potential_infiltration - bypass_flow);

    /* downward water flow */
    wlfl_sl[0] = cbm::bound_max( infiltration, kst_sl[0]);
    for ( size_t sl = 0; sl < soillayers_in->soil_layer_cnt();  ++sl)
    {
        EcHyPercolation( 1, sl);
    }

    if ( cbm::flt_equal_zero( kst_bottom))
    {
        wlfl_sl[soillayers_in->soil_layer_cnt()] = 0.0;
    }

    EcHyEvapotranspiration();

    /* upward water flow */
    for ( int sl = soillayers_in->soil_layer_cnt()-1;  sl >= 0;  --sl)
    {
        EcHyPercolation( 2, sl);
    }

    /* runoff */
    double const f_runoff( cbm::bound_max( m_param->FRUNOFF() / lclock()->time_resolution(), 1.0));
    runoff = cbm::bound_min( 0.0, f_runoff * (surface_water + throughfall + irrigation - wlfl_sl[0] - bypass_flow - bund_height));

    /* groundwater interaction:
     *  - set soil layers water-saturated
     *  - capillary rise
     */
    EcHyGroundwater();

    /* integration of state variables */
    EcHyIntegration();

    /* update external state */
    EcHyStepExit();

    /* perform water balance */
    rc = EcHyBalanceCheck( balance);
    if ( rc){ return rc; }

    rc = EcHy_check_for_negative_value("exit");
    if ( rc){ return rc; }

    return  LDNDC_ERR_OK;
}



/*!
 * @details
 *
 */
void
EcHy::EcHyreset()
{
    //reset rates
    snowfall = 0.0;
    gw_fill_surface = 0.0;
    cr_fill_groundwater = 0.0;
    runoff = 0.0;

    for ( size_t sl = 0; sl < soillayers_in->soil_layer_cnt(); sl++)
    {
        wlfl_sl[sl] = 0.0;
        cr_fill_sl[sl] = 0.0;
        gw_fill_sl[sl] = 0.0;
        bypass_fill_sl[sl] = 0.0;
        evsws_sl[sl] = 0.0;
        trwl_sl[sl] = 0.0;
    }

    wlfl_sl[soillayers_in->soil_layer_cnt()] = 0.0;

    /* irrigation */
    irrigation = 0.0;
    irrigation_event = 0.0;
}



/*!
 * @brief
 *
 */
lerr_t
EcHy::EcHyStepInit()
{
    site::input_class_site_t const *s_site( io_kcomm->get_input_class< site::input_class_site_t >());

    double const sks_scale( cbm::M_IN_CM * cbm::MIN_IN_DAY / lclock()->time_resolution());
    if ( cbm::is_valid( s_site->sksbottom()))
    {
        kst_bottom = s_site->sksbottom() * sks_scale;
    }
    else
    {
        kst_bottom = sc_.sks_sl[soillayers_in->soil_layer_cnt()-1] * sks_scale;
    }

    /* Set groundwater table to given model input */
    if ( gw_ != NULL)
    {
        gw_depth_static = gw_->watertable_subday( lclock_ref());
    }
    else
    {
        gw_depth_static = s_site->watertable();
    }

    double gw_depth_dynamic( gw_depth_static);
    for ( int sl = soillayers_in->soil_layer_cnt()-1;  sl >= 0;  sl--)
    {
        if ( cbm::flt_greater( wc_.wc_sl[sl], 0.95 * sc_.poro_sl[sl]))
        {
            gw_depth_dynamic = sc_.depth_sl[sl];
        }
        else
        {
            break;
        }
    }

    double const piezo_dynamic( cbm::bound_min( 0.0, gw_depth_static - gw_depth_dynamic));
    if ( cbm::flt_greater_zero( m_param->GROUNDWATER_LATERAL_GRADIENT()))
    {
        kst_sl[soillayers_in->soil_layer_cnt()-1] = piezo_dynamic / m_param->GROUNDWATER_LATERAL_GRADIENT() * kst_bottom;
    }
    else
    {
        kst_sl[soillayers_in->soil_layer_cnt()-1] = sc_.sks_sl[soillayers_in->soil_layer_cnt()-1] * sks_scale;
    }

    /* Set water amount */
    for ( size_t sl = 0; sl < soillayers_in->soil_layer_cnt(); sl++)
    {
        wl_sl[sl] = wc_.wc_sl[sl] * sc_.h_sl[sl];
        if ( cbm::flt_greater( wl_sl[sl], sc_.poro_sl[sl] * sc_.h_sl[sl] + 1.0e-4))
        {
            KLOGERROR("Soil water content in layer ", sl," (", wl_sl[sl] / sc_.h_sl[sl],") larger than porespace (",sc_.poro_sl[sl],")");
            return LDNDC_ERR_FAIL;
        }
    }

    /* Set intercepted canopy leaf water */
    interception_water = 0.0;
    for ( size_t fl = 0; fl < m_setup->canopylayers(); fl++)
    {
        interception_water += wc_.wc_fl[fl];
    }

    /* Add precipitation to intercepted canopy leaf water  */
    if ( timemode() == TMODE_SUBDAILY)
    {
        interception_water += mc_.ts_precipitation;
    }
    else
    {
        interception_water += mc_.nd_precipitation;
    }

    /* Get interception water capacity */
    double const interception_capacity( EcHyGetInterceptionCapacity());

    /* Calculate throughfall */
    if ( cbm::flt_greater( interception_water, interception_capacity))
    {        
        throughfall = interception_water - interception_capacity;
        interception_water -= throughfall;
    }
    else
    {
        throughfall = 0.0;
    }

    /* Set snow and surface water */
    surface_ice = wc_.surface_ice;
    surface_water = wc_.surface_water;

    /* Potential esvapotranspiration */
    EcHyPotentialEvapotranspiration();

    return  LDNDC_ERR_OK;
}



/*!
 * @brief
 *
 */
lerr_t
EcHy::EcHyStepExit()
{
    /* clock */
    cbm::sclock_t const &  clk( this->lclock_ref());

    EcHyCalculateLeafWaterDistribution( wc_.wc_fl);

    wc_.surface_ice = surface_ice;

    wc_.surface_water = surface_water;

    if ( timemode() == TMODE_SUBDAILY)
    {
        wc_.accumulated_precipitation += mc_.ts_precipitation;
    }
    else
    {
        wc_.accumulated_precipitation += mc_.nd_precipitation;
    }
    
    wc_.accumulated_irrigation_automatic += (irrigation - irrigation_event);

    wc_.accumulated_infiltration += wlfl_sl[0];

    wc_.accumulated_percolation += wlfl_sl[soillayers_in->soil_layer_cnt()];

    wc_.accumulated_runoff += runoff;

    wc_.accumulated_potentialevapotranspiration += daily_potential_evapotranspiration / clk.time_resolution();

    wc_.accumulated_interceptionevaporation += ev_leaf;

    wc_.accumulated_throughfall += throughfall;

    for (size_t sl = 0; sl < soillayers_in->soil_layer_cnt(); sl++)
    {
        wc_.wc_sl[sl] = wl_sl[sl] / sc_.h_sl[sl];
        wc_.accumulated_waterflux_sl[sl] += wlfl_sl[sl+1];
        wc_.accumulated_transpiration_sl[sl] += trwl_sl[sl];
        wc_.accumulated_soilevaporation += evsws_sl[sl];
    }

    return  LDNDC_ERR_OK;
}



/*!
 * @page echy
 * @section interception Interception
 *  Interception describes the amount of precipitation,
 *  which is hold by the canopy, i.e., not reaching soil surface
 *  (see  @ref watercyle_interception_capacity for more details)
 */
double
EcHy::EcHyGetInterceptionCapacity()
{
    double interception_capacity( 0.0);
    for ( PlantIterator vt = m_veg->begin(); vt != m_veg->end(); ++vt)
    {
        interception_capacity += (*vt)->interception_capacity();
    }

    return interception_capacity;
}


lerr_t
EcHy::EcHyCalculateLeafWaterDistribution(
                                         lvector_t< double > &_wc_fl)
{
    /* Set intercepted canopy leaf water */
    double lai_sum( 0.0);
    for ( PlantIterator vt = m_veg->begin(); vt != m_veg->end(); ++vt)
    {
        lai_sum += (*vt)->lai();
    }

    if ( cbm::flt_greater_zero( lai_sum) &&
        cbm::flt_greater_zero( interception_water))
    {
        for ( size_t fl = 0; fl < m_setup->canopylayers(); ++fl)
        {
            _wc_fl[fl] = 0.0;
            for ( PlantIterator vt = m_veg->begin(); vt != m_veg->end(); ++vt)
            {
                _wc_fl[fl] += (*vt)->lai_fl[fl] / lai_sum * interception_water;
            }
        }
    }
    else
    {
        for ( size_t fl = 0; fl < m_setup->canopylayers(); ++fl)
        {
            _wc_fl[fl] = 0.0;
        }
        surface_water += interception_water;
        interception_water = 0.0;
    }

    return LDNDC_ERR_OK;
}



/*!
 * @details
 *  Integrates all water state variables.
 */
double
EcHy::EcHySoilWaterChange(
                          size_t _sl)
{
    return wlfl_sl[_sl] - wlfl_sl[_sl+1] + cr_fill_sl[_sl] - trwl_sl[_sl] - evsws_sl[_sl] + gw_fill_sl[_sl];
}



/*!
 * @details
 *  Integrates all water state variables.
 */
lerr_t
EcHy::EcHyIntegration()
{
    surface_water = cbm::bound_min( 0.0,
                                    surface_water +
                                   (throughfall + gw_fill_surface + irrigation - wlfl_sl[0] - runoff - bypass_fill_sl.sum()));

    for (size_t sl = 0; sl < soillayers_in->soil_layer_cnt(); sl++)
    {
        wl_sl[sl] = cbm::bound_min( 0.0, wl_sl[sl] + EcHySoilWaterChange( sl));
        wc_.mskpa_sl[sl] = ldndc::hydrology::capillary_pressure( wl_sl[sl]/sc_.h_sl[sl], sc_.vga_sl[sl],
                                                                 sc_.vgn_sl[sl], sc_.vgm_sl[sl], wc_max_sl[sl], wc_min_sl[sl]);
    }

    return  LDNDC_ERR_OK;
}



/*!
 * @details
 *  Checks balance between all incoming and outgoing water fluxes.
 */
lerr_t
EcHy::EcHyBalanceCheck(
                       double &_balance)
{
    double  balance( interception_water + surface_water + surface_ice);
    for ( size_t sl = 0;  sl < soillayers_in->soil_layer_cnt();  ++sl)
    {
        balance += wl_sl[sl] + wc_.ice_sl[sl] * sc_.h_sl[sl];
    }

    if ( _balance > 0.0)
    {
        balance +=  wlfl_sl[soillayers_in->soil_layer_cnt()]
                    + ev_leaf + evsws_sl.sum() + trwl_sl.sum() + runoff
                    - gw_fill_surface - cr_fill_groundwater -  gw_fill_sl.sum()
                    - irrigation - throughfall - snowfall;

        double const balance_delta( std::abs( _balance - balance));
        if ( cbm::flt_greater( balance_delta, 1.0e-4))
        {
            KLOGWARN( "Water leakage in: ", name(),
                      " Difference: ", balance - _balance);
            return LDNDC_ERR_FAIL;
        }
    }
    else
    {
        _balance = balance;
    }
    
    return  LDNDC_ERR_OK;
}



/*
 * @brief
 *      checks all kind of non-positive numbers (including nan)
 */
#define  EcHy_log_invalid(__var__,__index__,__name__)        \
if (!((__var__) >= 0.0))                                      \
{                                                             \
    if ((__var__) >= (-1.0e-20))                              \
    {                                                         \
        (__var__) = 0.0;                                      \
    }                                                         \
    else                                                      \
    {                                                         \
        ++n_err;                                              \
        KLOGERROR( "Illegal value at ", _position, ": ",      \
        __name__, "[",__index__, "] = ", __var__);            \
    }                                                         \
}



/*
 * @brief
 *      helping function for debugging
 *      checks complete state for all kind of non-positive numbers (including nan)
 */
lerr_t
EcHy::EcHy_check_for_negative_value(
                                    char const *  _position)
{
    int  n_err = 0;

#define  EcHy_log_invalid_metrx(__var__)                   \
EcHy_log_invalid( __var__,-99,#__var__)

    EcHy_log_invalid_metrx( ev_leaf)
    EcHy_log_invalid_metrx( runoff)
    EcHy_log_invalid_metrx( gw_fill_surface)
    EcHy_log_invalid_metrx( cr_fill_groundwater)
    EcHy_log_invalid_metrx( irrigation)
    EcHy_log_invalid_metrx( throughfall)
    EcHy_log_invalid_metrx( snowfall)
    EcHy_log_invalid_metrx( interception_water)
    EcHy_log_invalid_metrx( surface_water)
    EcHy_log_invalid_metrx( surface_ice)

    for( size_t  sl = 0;  sl < soillayers_in->soil_layer_cnt();  ++sl)
    {
#define  EcHy_log_invalid_metrx_sl(__var__)                   \
EcHy_log_invalid(this->__var__[sl],sl,#__var__)
        EcHy_log_invalid_metrx_sl( wlfl_sl)
        EcHy_log_invalid_metrx_sl( evsws_sl)
        EcHy_log_invalid_metrx_sl( trwl_sl)
        EcHy_log_invalid_metrx_sl( gw_fill_sl)
    }
    return  n_err ? LDNDC_ERR_FAIL : LDNDC_ERR_OK;
}

} /*namespace ldndc*/
