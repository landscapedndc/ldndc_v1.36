/*!
 * @file
 * @author
 *    David Kraus
 *
 */

#include  "watercycle/echy/echy.h"

namespace ldndc {

/*!
 * @brief
 *      Flooding
 */
lerr_t
EcHy::EcHyFlood()
{
    lerr_t  rc = m_eventflood.solve();
    if ( rc != LDNDC_ERR_OK){ return  rc; }

    bund_height = m_eventflood.get_bund_height();

    /* todo */
    //irrigation_amount = m_eventflood.get_irrigation_amount();

    /* todo */
    //double const irrigation_height = m_eventflood.get_irrigation_height();
    double const max_percolation = m_eventflood.get_maximum_percolation();
    double const water_table_flooding = m_eventflood.get_water_table();


    irrigation_switch = NONE;
    minimum_watertable_height = 0.0;
    if ( cbm::is_valid( water_table_flooding))
    {
        if ( cbm::flt_greater_zero( water_table_flooding))
        {
            irrigation_switch = CONSTANT_POSITIVE_WATER_TABLE;
            minimum_watertable_height = water_table_flooding;
            bund_height = water_table_flooding;
        }
        else
        {
            irrigation_switch = CONSTANT_NEGATIVE_WATER_TABLE;
            minimum_watertable_height = water_table_flooding;
            bund_height = 0.0;
        }
    }


    size_t sl_max( soillayers_in->soil_layer_cnt()-1);
    if ( cbm::is_valid( max_percolation))
    {
        kst_sl[sl_max] = max_percolation / lclock()->time_resolution();
    }
    else
    {
        double const time_rate( 0.1 / lclock()->time_resolution());
        kst_sl[sl_max] -= (kst_sl[sl_max] - kst_bottom) * time_rate;
    }

    return  LDNDC_ERR_OK;
}



/*!
 * @brief
 *      Irrigation
 */
lerr_t
EcHy::EcHyIrrigation()
{
    EventAttributes const *  ev_manure = NULL;
    while (( ev_manure = this->m_ManureEvents.pop()) != NULL)
    {
        double const volume = ev_manure->get( "/volume", 0.0);  //given in m3:ha-1
        wc_.accumulated_irrigation += volume * cbm::HA_IN_M2;
    }

    EventAttributes const *  ev_irri = NULL;
    while (( ev_irri = this->m_IrrigationEvents.pop()) != NULL)
    {
        double const  amount = ev_irri->get( "/amount", 0.0);

        // check if irrigation water is stored in reservoir
        if ( ev_irri->get( "/is-reservoir", false) == true)
        {
            //negative amount resets irrigation reservoir
            if ( !cbm::flt_greater_equal_zero( amount))
            {
                wc_.irrigation_reservoir = 0.0;
            }
            else
            {
                wc_.irrigation_reservoir += amount * cbm::M_IN_MM;
            }
        }
        else
        {
            if ( !cbm::flt_greater_equal_zero( amount))
            {
                KLOGERROR( "negative amount of irrigation water [amount=", amount,"]");
                return LDNDC_ERR_FAIL;
            }
            wc_.accumulated_irrigation += amount * cbm::M_IN_MM;
        }
    }

    if ( irrigation_switch == CONSTANT_POSITIVE_WATER_TABLE)
    {
        if ( surface_water < minimum_watertable_height)
        {
            irrigation = minimum_watertable_height - surface_water;
        }
    }
    else if ( irrigation_switch == CONSTANT_NEGATIVE_WATER_TABLE)
    {
        irrigation = 0.0;
        for ( size_t sl = 0;  sl < soillayers_in->soil_layer_cnt();  ++sl)
        {
            if ( cbm::flt_less( sc_.depth_sl[sl], -minimum_watertable_height))
            {
                if ( wl_sl[sl] < wlfc_sl[sl])
                {
                    irrigation += wlfc_sl[sl] - wl_sl[sl];
                }
            }
            else
            {
                if ( cbm::flt_less( wl_sl[sl], wlst_sl[sl]))
                {
                    irrigation += wlst_sl[sl] - (wl_sl[sl] + EcHySoilWaterChange( sl));
                }
            }
        }
    }
    else
    {
        irrigation = 0.0;
    }

    if ( cbm::flt_greater( wc_.accumulated_irrigation, accumulated_irrigation_old))
    {
        irrigation += wc_.accumulated_irrigation - accumulated_irrigation_old;
        irrigation_event += wc_.accumulated_irrigation - accumulated_irrigation_old;
        accumulated_irrigation_old = wc_.accumulated_irrigation;
    }

    return  LDNDC_ERR_OK;
}

} /*namespace ldndc*/
