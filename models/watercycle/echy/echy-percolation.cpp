/*!
 * @file
 * @author
 *    - David Kraus
 * @date
 *    Nov, 2017
 */


#include  "watercycle/echy/echy.h"
#include  <scientific/hydrology/ld_vangenuchten.h>

namespace ldndc {


/*!
 * @page echy
 * @section percolation Percolation
 *
 *  <b> Vertical downward movement </b> \n
 *  Vertical downward movement of water \f$ q_z \f$ is calculated via the saturated
 *  hydraulic conductivity \f$ K_f \f$ and the relative permeability \f$ k_r \f$,
 *  which depends on water saturation:
 *  \f[ q_z = K_f \cdot k_r (\theta) \f]
 *  The relative permeability is derived from the van Genuchten
 *  soil parametrization following the work of @cite mualem:1976a :
 *  \f[
 *  k_{r} (\theta_e) = \theta_e \left [ 1 - \left( 1 - \theta_e^{\frac{1}{m}}
 *                     \right )^m \right ]^2
 *  \f]
 */
lerr_t
EcHy::EcHyPercolation(
                      size_t _step,
                      size_t _sl)
{
    /* potential downward flow */
    if ( _step == 1)
    {
        /* new water content after inflow from above soil layer */
        double wl_new( wl_sl[_sl] + wlfl_sl[_sl]);
        
        /* unsaturated hydraulic conductivity */
        double kust( 0.0);

        /* percolation is calculated using an explicit numerical integration
         * scheme for which time resolution is locally refined depending on
         * the water flow velocity
         */
        size_t steps( 1);
        if ( cbm::flt_greater( wl_new / sc_.h_sl[_sl], wc_min_sl[_sl]))
        {
            /* current water content in soil layer (bound by minimum and maximum water content) */
            double const wc_vg( cbm::bound_max( wl_new / sc_.h_sl[_sl],
                                                wc_max_sl[_sl]));
            double const wl_vg( wc_vg * sc_.h_sl[_sl]);

            /* unsaturated hydraulic conductivity */
            double const kust_loc( cbm::bound_min( 0.0,
                                                   ldndc::hydrology::hydraulic_conductivity( wc_vg,
                                                                                             sc_.vgm_sl[_sl],
                                                                                             wc_max_sl[_sl],
                                                                                             wc_min_sl[_sl],
                                                                                             kst_sl[_sl] / double(steps))));
            //if kust_loc exceeds 10% of current soil water content additional iteration steps are included for numerical stability
            if ( cbm::flt_greater( kust_loc, 0.1 * wl_vg))
            {
                //increase steps in order to decrease kust_loc
                steps = static_cast<size_t>( cbm::bound_max( std::ceil( kust_loc / (0.1 * wl_vg)), 10.0));
            }
        }
        else
        {
            wlfl_sl[_sl+1] = 0.0;
            return  LDNDC_ERR_OK;
        }

        for ( size_t j = 0; j < steps; ++j)
        {
            /*!
             * @page echy
             *  <b> Effective soil water saturation </b> \n
             *  The effective soil water saturation (\f$ \theta_e \f$) linearly scales between
             *  the residual water saturation (\f$ \theta_e = 0 \f$) and the residual air-filled
             *  porespace (\f$ \theta_e = 1 \f$). The residual water saturation is set by
             *  default to \f$ \theta_{r,w} = 0.01 \f$, while the residual air-filled porespace
             *  can be given as model input (default value is set to \f$ \theta_{r,a} = 0 \f$).
             */
            if ( cbm::flt_greater( wl_new / sc_.h_sl[_sl], wc_min_sl[_sl]))
            {
                /* current water content in soil layer (bound by minimum and maximum water content) */
                double const wc_vg( cbm::bound_max( wl_new / sc_.h_sl[_sl],
                                                    wc_max_sl[_sl]));

                /* unsaturated hydraulic conductivity */
                double const kust_loc( cbm::bound( 0.0,
                                                   ldndc::hydrology::hydraulic_conductivity( wc_vg,
                                                                                             sc_.vgm_sl[_sl],
                                                                                             wc_max_sl[_sl],
                                                                                             wc_min_sl[_sl],
                                                                                             kst_sl[_sl] / double(steps)),
                                                   0.99 * wl_new));

                kust += kust_loc;
                wl_new -= kust_loc;
            }
            else
            {
                break;
            }
        }

        wlfl_sl[_sl+1] = kust;

        /*!
         * @page echy
         *  <b> Groundwater influence </b> \n
         *  Percolation within the static groundwater table as given in the model input can be reduced by the
         *  site parameter GROUNDWATER_PERCOLATION.
         */
        if ( cbm::flt_greater( sc_.depth_sl[_sl], gw_depth_static))
        {
            wlfl_sl[_sl+1] = cbm::bound_max( wlfl_sl[_sl+1], m_param->GROUNDWATER_PERCOLATION() * kst_sl[_sl]);
        }
    }

    /* correct downward flow in case water cannot be transported
     * fast enought through the soil profile
     */
    else
    {
        double const wl_new( wl_sl[_sl] + (wlfl_sl[_sl] - wlfl_sl[_sl+1] - evsws_sl[_sl] - trwl_sl[_sl]));
        if ( cbm::flt_greater( wl_new, wlst_sl[_sl]))
        {
            wlfl_sl[_sl] = cbm::bound_min( 0.0, wlfl_sl[_sl] - (wl_new - wlst_sl[_sl]));
        }
    }

    return  LDNDC_ERR_OK;
}



/*!
 * @page echy
 * @section echy_preferentialflow Preferential flow
 *  Preferential flow due to, e.g., soil cracks is included by a
 *  simple concept, which is based on two parameters representing:
 *  - Fraction of infiltrating surface water that is
 *    subject to preferential flow \f$ \alpha_{pf} \f$
 *  - Specific soil depth \f$ z_{pf} \f$ until which preferential
 *    flow occurs
 */
lerr_t
EcHy::EcHyBypassFlow(
                     double _depth,
                     double &_wl_bypass /* bypassing water */)
{
    if ( cbm::flt_greater_zero( _wl_bypass))
    {
        size_t sl_bypass( 0);
        for ( size_t sl = 1; sl < soillayers_in->soil_layer_cnt();  ++sl)
        {
            if ( cbm::flt_greater( sc_.depth_sl[sl], _depth))
            {
                break;
            }
            else
            {
                sl_bypass = sl;
            }
        }

        /*!
         * @page echy
         *  The amount of water that is subject to preferential flow is
         *  redistributed within \f$ z_{pf} \f$. Soil layers are iteratively
         *  filled upwards untill field capacity beginning with the soil layer
         *  at \f$ z_{pf} \f$.
         */
        while ( cbm::flt_greater_zero( _wl_bypass))
        {
            double const add_water( cbm::bound( 0.0,
                                                wlst_sl[sl_bypass] - wl_sl[sl_bypass],
                                                _wl_bypass));

            wl_sl[sl_bypass] += add_water;
            bypass_fill_sl[sl_bypass] += add_water;
            _wl_bypass = cbm::bound_min( 0.0, _wl_bypass - add_water);

            if ( cbm::flt_equal_zero( _wl_bypass) || (sl_bypass == 0))
            {
                break;
            }
            else
            {
                sl_bypass--;
            }
        }
        _wl_bypass = bypass_fill_sl.sum();
    }

    return LDNDC_ERR_OK;
}

} /*namespace ldndc*/

