/*!
 * @file
 * @author
 *    - David Kraus
 * @date
 *    Nov, 2017
 */


#include  "watercycle/echy/echy.h"

namespace ldndc {


/*!
 * @page echy
 * @section echy-snow Snow and ice
 *  Snowfall and soil ice formation are calculated
 *  as presented in the library functions @link snow_ice Snow and ice \endlink.
 */
lerr_t
EcHy::EcHySnowIce()
{
    /* Snowfall */
    {
        /* copy state to SnowDNDC */
        WaterCycleSnowDNDC::SnowPackStateIn  state_in;
        state_in.air_temperature_above_canopy = mc_.nd_airtemperature;
        state_in.air_temperature_below_canopy = mc_.temp_a;
        state_in.rainfall = throughfall;
        state_in.surface_ice = surface_ice;
        state_in.surface_water = surface_water;

        /* run snow pack simulation */
        WaterCycleSnowDNDC::SnowPackStateOut  state_out;
        lerr_t  rc_snowpack = m_snowdndc.SnowPack( state_in, state_out);
        if ( rc_snowpack)
        {
            KLOGERROR("Handling snowpack calculation failed in: ", name());
            return  rc_snowpack;
        }

        /* copy state from SnowDNDC */
        snowfall = state_out.snowfall;
        throughfall = state_out.rainfall;
        surface_ice = state_out.surface_ice;
        surface_water = state_out.surface_water;
    }

    /* Soilice */
    {
        /* copy state to SnowDNDC */
        WaterCycleSnowDNDC::IceContentStateIn &  state_in = m_icecontent_in;
        for (size_t sl = 0; sl < soillayers_in->soil_layer_cnt(); sl++)
        {
            state_in.h_sl[sl] = sc_.h_sl[sl];
            state_in.bulkdensity_sl[sl] = sc_.dens_sl[sl];
            state_in.fcorg_sl[sl] = sc_.fcorg_sl[sl];
            state_in.ice_sl[sl] = wc_.ice_sl[sl];
            state_in.soil_temperature_sl[sl] = mc_.temp_sl[sl];
            state_in.wc_sl[sl] = wl_sl[sl] / sc_.h_sl[sl];
        }

        /* run soil ice simulation */
        WaterCycleSnowDNDC::IceContentStateOut &  state_out = m_icecontent_out;
        lerr_t  rc_icecontent = m_snowdndc.IceContent( state_in, state_out);
        if ( rc_icecontent)
        {
            KLOGERROR("Handling soilice calculation failed in: ", name());
            return  rc_icecontent;
        }

        /* copy state from SnowDNDC */
        for (size_t sl = 0; sl < soillayers_in->soil_layer_cnt(); sl++)
        {
            wl_sl[sl] = state_out.wc_sl[sl] * sc_.h_sl[sl];
            wc_.ice_sl[sl] = state_out.ice_sl[sl];
            mc_.temp_sl[sl] = state_out.soil_temperature_sl[sl];
            
            /*!
             * @page echy
             *  <b> Pore space reduction </b> \n
             *  Soil ice formation leads to reduction of pore space \f$ \phi \f$.
             *  Likewise, field capacity \f$ \theta_{max} \f$ and wilting point
             *  \f$ \theta_{min} \f$ are reduced:
             *  \f{eqnarray*}{
             *  f(\theta_{ice}) &=& 1 - \frac{\theta_{ice}}{\phi} \\
             *  \theta_{max}^{\ast} &=& \theta_{max} \cdot f(\theta_{ice}) \\
             *  \theta_{min}^{\ast} &=& \theta_{min} \cdot f(\theta_{ice})
             *  \f}
             */
            double const porespace_reduction( cbm::bound_max( 1.0 - wc_.ice_sl[sl] / sc_.poro_sl[sl], 0.99));
            wlfc_sl[sl] = sc_.wcmax_sl[sl] * sc_.h_sl[sl] * porespace_reduction;
            wlwp_sl[sl] = sc_.wcmin_sl[sl] * sc_.h_sl[sl] * porespace_reduction;

            EcHy_set_water_min_max( sl, porespace_reduction);
        }
    }

    return  LDNDC_ERR_OK;
}


} /*namespace ldndc*/

