/*!
 * @file
 * @authors
 *  - Ruediger Grote
 *  - David Kraus
 * @date
 *  May 21, 2017
 */

#ifndef  LD_POTENTIALEVAPOTRANSPIRATION_H_
#define  LD_POTENTIALEVAPOTRANSPIRATION_H_

#include  "ld_modelsconfig.h"

namespace ldndc {

double
LDNDC_API thornthwaite_heat_index(
                                  double /* latitude */,
                                  size_t /* days in year */,
                                  double /* annual average temperature */,
                                  double /* annual temperature amplitude */);

double
LDNDC_API thornthwaite(
                       double /* air temperature */,
                       double /* daylength */,
                       double /* heat index */);

} /*namespace ldndc*/

#endif  /*  !LD_POTENTIALEVAPOTRANSPIRATION_H_  */

