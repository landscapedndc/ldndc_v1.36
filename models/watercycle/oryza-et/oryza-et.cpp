/*!
 * @brief
 *      WaterCycleOryzaET origins from the Oryza2000 model
 *      and calculates:
 *          - potential evapotranspiration
 *          - potential evaporation
 *          - potential transpiration
 *      Potential evapotranspiration is calculated based on penman
 *
 *
 * @author
 *      David Kraus
 *
 */


#include  "watercycle/oryza-et/oryza-et.h"

#include  <constants/cbm_const.h>
#include  <math/cbm_math.h>

#include  <input/climate/climate.h>
#include  <input/soillayers/soillayers.h>

#include  <logging/cbm_logging.h>

#include  <scientific/meteo/ld_meteo.h>

LMOD_MODULE_INFO(WaterCycleOryzaET,TMODE_SUBDAILY|TMODE_POST_DAILY,LMOD_FLAG_USER);
namespace ldndc {


WaterCycleOryzaET::WaterCycleOryzaET(
                                     MoBiLE_State *  _state,
                                     cbm::io_kcomm_t *  _io,
                                     timemode_e  _timemode)
                                    : MBE_LegacyModel(
                                                      _state,
                                                      _timemode),
                                    io_kcomm( _io),
                                    m_veg( &_state->vegetation),
                                    climate_in( _io->get_input_class< input_class_climate_t >()),
                                    species_in( _io->get_input_class< input_class_species_t >()),
                                    ph_( _state->get_substate_ref< substate_physiology_t >()),
                                    sc_( _state->get_substate_ref< substate_soilchemistry_t >()),
                                    wc_( _state->get_substate_ref< substate_watercycle_t >()),
                                    time_mode_subdaily_( (_timemode == TMODE_SUBDAILY) ? true : false),
                                    day_fraction_( (_timemode == TMODE_SUBDAILY) ? LD_RtCfg.clk->day_fraction() : 1.0 )
{}



WaterCycleOryzaET::~WaterCycleOryzaET()
{}



lerr_t
WaterCycleOryzaET::finalize()
{
    return  LDNDC_ERR_OK;
}



lerr_t
WaterCycleOryzaET::sleep()
{
    return  LDNDC_ERR_OK;
}



lerr_t
WaterCycleOryzaET::wake()
{
    return  LDNDC_ERR_OK;
}



lerr_t
WaterCycleOryzaET::configure(
                             ldndc::config_file_t const *)
{
    return  LDNDC_ERR_OK;
}



lerr_t
WaterCycleOryzaET::initialize()
{
    /* check for proper time resolution */
    if ( lclock()->time_resolution() > 1 && ( timemode() != TMODE_SUBDAILY))
    {
        KLOGERROR( "module \"", name(), "\" must run in subdaily resolution when simulation time resolution is greater 1");
        return  LDNDC_ERR_OBJECT_INIT_FAILED;
    }

    return  LDNDC_ERR_OK;
}



/*!
 * @brief
 *      kicks off computation for one time step
 */
lerr_t
WaterCycleOryzaET::solve()
{
    //Leaf area index
    double const lai( m_veg->lai());

    //Day number within year of simulation (d)
    cbm::sclock_t const &  clk( this->lclock_ref());
    int idoy( clk.yearday());

    //Latitude
    double lat( this->io_kcomm->get_input_class_ref< ldndc::setup::input_class_setup_t >().latitude());

    //A value of Angstrom formula (-)
    double anga( 0.29);

    //B value of Angstrom formula (-)
    double angb( 0.42);

    //Daily short-wave radiation (J.m-2.d)
    double rdd( (climate_in->ex_rad_day( clk) * cbm::CM2_IN_M2 * cbm::JDCM_IN_WM));

    //Average temperature (degrees C)
    double tmda( climate_in->temp_avg_day( clk));

    double scale( 1.0);
    if ( time_mode_subdaily_ && (tmda > 0.0))
    {
        double t_subdaily = climate_in->temp_avg_subday( clk);
        scale = t_subdaily / tmda * day_fraction_;
    }

    //Average wind speed (m s-1)
    double wn( climate_in->wind_speed_day( clk));

    //Early morning vapour pressure (kPa)
    double vps( 0.0);
    ldndc::meteo::vps( tmda, &vps);
    double vp( vps - climate_in->vpd_day( clk));
    vp = climate_in->vpd_day( clk);

    //reflection coefficient of soil or water background
    //differentiate between standing water or moist/dry soil
    double alb( (wc_.surface_water > 0.005) ? 0.05 : 0.25);
    double rfs( (wc_.surface_water > 0.005) ? alb : alb * (1.0 - 0.5 * wc_.wc_sl[0] / sc_.poro_sl[0]));

    // soil or water background is shielded by the crop
    double rf( rfs * std::exp( -0.5 * lai) + 0.25 * (1.0 - std::exp(-0.5 * lai))); //Reflection (=albedo) of surface (-)

    //switch for different surface types (-)
    int isurf( (lai > 0.0) ? 3 : ((wc_.surface_water > 0.005) ? 1 : 2));

    //Temperature tolerance (switches between single and iterative Penman)
    double tmdi( 0.0);

    //Potential evapotranspiration (mm d-1)
    double etd( 0.0);

    //Radiation driven part of potential evapotranspiration (mm d-1)
    double etrd( 0.0);

    //Dryness driven part of potential evapotranspiration (mm.d-1)
    double etae( 0.0);

    //Estimated temperature difference between surface height and reference height (degrees C)
    double dt( 0.0);

    penman( isurf, idoy, lat, rf, anga, angb, tmdi, rdd, tmda, wn, vp, dt, etd, etrd, etae);

    //multiplication by factor according to fao (1998)
    double const FAOF( 1.3);
    etd  *= FAOF;
    etrd *= FAOF;
    etae *= FAOF;

    //write state
    double const potentialevapotranspiration( etd * cbm::M_IN_MM * scale);
    if ( cbm::flt_greater_zero( potentialevapotranspiration))
    {
        wc_.accumulated_potentialevapotranspiration += potentialevapotranspiration;

        double const potentialevaporation( (std::max(0.0, std::exp(-0.5 * lai) * (etrd + etae))) * cbm::M_IN_MM * scale);
        double const potentialtranspiration( (etrd * (1.0 - std::exp(-0.5 * lai)) + etae * std::min(2.0, lai)) * cbm::M_IN_MM * scale);
        if ( cbm::flt_greater(potentialevaporation + potentialtranspiration, potentialevapotranspiration))
        {
            double const scale_down( potentialevapotranspiration / (potentialevaporation + potentialtranspiration));
            wc_.accumulated_potentialevaporation += potentialevaporation * scale_down;
            wc_.accumulated_potentialtranspiration += potentialtranspiration * scale_down;
        }
        else
        {
            wc_.accumulated_potentialevaporation += potentialevaporation;
            wc_.accumulated_potentialtranspiration += potentialtranspiration;
        }
    }

    return  LDNDC_ERR_OK;
}



/*!
 * @brief
 *      This subroutine calculates saturated vapour pressure and
 *      slope of saturated vapour pressure. Parameters of the
 *      formula were fitted on the Goff-Gratch formula used in the
 *      Smithsonian Handbook of Meteorological Tables. The
 *      saturated vapour following the Goff-Gratch formula is also
 *      available as a subroutine. (Note that 1kPa = 10 mbar)
 * @author
 *      Daniel van Kraalingen (original version)
 *      David Kraus (implementation into LandscapeDNDC)
 *
 * @param
 *      temperature (°C)
 * @param
 *      saturated vapour pressure (kPa)
 * @param
 *      slope of saturated vapour pressure at given temperature (kPa °C-1)
 */
lerr_t
WaterCycleOryzaET::saturated_vapour_pressure(
                                             double _temperature,
                                             double &_saturated_vapour_pressure,
                                             double &_slope)
{
    _saturated_vapour_pressure  = 0.1 * 6.10588 * std::exp( 17.32491 * _temperature / (_temperature + 238.102));
    _slope = 238.102 * 17.32491 * _saturated_vapour_pressure / std::pow( _temperature + 238.102, 2.0);
    
    return  LDNDC_ERR_OK;
}



/*!
 * @brief
 *      This subroutine calculates reference evapotranspiration
 *      in a manner similar to Penman (1948). To obtain crop evapo-
 *      transpiration, multiplication with a Penman crop factor
 *      should be done. Calculations can be carried out for three
 *      types of surfaces: water, wet soil, and short grass
 *      (ISURF=1,2,3 resp.). When the input variable TMDI is set to
 *      zero, a single calculation is done and an estimate is
 *      provided of temperature difference between the environment
 *      and the surface (DT). If the absolute value of DT is large
 *      an iterative Penman can be carried out which continues until
 *      the new surface temperature differs by no more than TMDI
 *      from the old surface temperature. Two types of long-wave
 *      radiation calculations are available Swinbank and Brunt.
 *      The switch between the two is made by choosing the right
 *      values for ANGA and ANGB. If ANGA and ANGB are zero,
 *      Swinbank is used, if both are positive, Brunt is used and
 *      the ANGA and ANGB values are in the calculation of the
 *      cloud cover.
 *
 * @author
 *      Daniel van Kraalingen (original version)
 *      David Kraus (implementation into LandscapeDNDC)
 */
lerr_t
WaterCycleOryzaET::penman(
                          int _isurf,
                          int _idoy,
                          double _lat,
                          double _rf,
                          double _anga,
                          double _angb,
                          double _tmdi,
                          double _rdd,
                          double _tmda,
                          double _wn,
                          double _vp,
                          double _dt,
                          double &_etd,
                          double &_etrd,
                          double &_etae)
{
    double const sigma = 5.668e-8;
    double const lhvap = 2454.e3;
    double const psch = 0.067;
    double const rhocp = 1240.0;
    double const rbgl = 8.31436;


    /* calculation for longwave radiation */
    size_t ilw( 0);
    if (   cbm::flt_equal_zero( _anga)
        && cbm::flt_equal_zero( _angb))
    {
        ilw = 1;
    }
    else if (   cbm::flt_greater_zero( _anga)
             && cbm::flt_greater_zero( _angb)
             && cbm::flt_greater( _anga + _angb, 0.5)
             && cbm::flt_greater(0.9, _anga + _angb))
    {
        ilw = 2;
    }

    double vps( 0.0);
    double vpsl( 0.0);
    saturated_vapour_pressure( _tmda, vps, vpsl);

    double const hum( _vp / vps);
    double const vpd( cbm::flt_greater( hum, 1.0) ? 0.0 : vps - _vp);

    double solcon( cbm::AVG_SOLAR_CONSTANT * ldndc::meteo::eccentricity( _idoy, lclock()->days_in_year()));
    double angot( ldndc::meteo::daily_solar_radiation(_lat, _idoy, 365) * cbm::JDCM_IN_WM * cbm::CM2_IN_M2);

    double datmtr = cbm::bound( 0.0, _rdd / angot, 1.0);

    double rdloi = sigma * std::pow(_tmda + 273.16, 4.0);

    double rdlo  = 86400.0 * rdloi;

    double rdlii( 0.0);
    double rdli( 0.0);
    if ( ilw == 1)
    {
        /* swinbank formula for net longwave radiation */
        rdlii = datmtr * (5.31e-13 * std::pow(_tmda + 273.16, 6.0) - rdloi) / 0.7 + rdloi;
        rdli  = 86400.0 * rdlii;
    }
    else if (ilw == 2)
    {
        /* brunt formula for net longwave radiation */
        double const clear = cbm::bound(0.0, (datmtr - _anga) / _angb, 1.0);
        rdlii = sigma * std::pow(_tmda + 273.16, 4.0) * (1.0 - (0.53 - 0.212 * sqrt( _vp)) * (0.2 + 0.8 * clear));
        rdli  = 86400.0 * rdlii;
    }

    double rdn = (1.0 - _rf) * _rdd + rdli - rdlo;

    /*
     * wind functions and isothermal evaporation
     * 2.63 is conversion from mm hg to kpa
     */
    double fu2( 0.0);
    if ( (_isurf == 1) || (_isurf == 2))
    {
        /* open water and soils */
        fu2 = 2.63 * (0.5 + 0.54 * _wn);
    }
    else if (_isurf == 3)
    {
        /* short grass crops */
        fu2 = 2.63 * (1.0 + 0.54 * _wn);
    }

    double ea = vpd * fu2;

    //actual water loss (separated in radiation term and
    //aerodynamic term) and resistance to transfer of vapour (s/m)
    //and estimated temperature difference

    _etrd = (rdn * (vpsl / (vpsl + psch))) / lhvap;
    _etae = (psch * ea) / (vpsl + psch);
    _etd  = _etrd + _etae;
    double re   = 86400.0 * 1000.0 * 0.018016 / (fu2 * rbgl * (_tmda + 273.16));
    _dt   = re * ((rdn - lhvap * _etd) / 86400.0) / rhocp;

    /* iteration on surface temperature if required with do-while loop */

    if ( _tmdi > 0.0)
    {
        double dtn    = 0.0;
        int inloop = 0;
        bool equil  = false;
        while ( (inloop == 0) || !equil)
        {
            _dt = (_dt + dtn) / 2.0;

            /* net radiation and slope of saturated vapour pressure */
            rdloi = sigma * std::pow( _tmda + _dt + 273.16, 4.0);
            rdlo  = 86400.0 * rdloi;
            rdn   = (1.0 - _rf) * _rdd + rdli - rdlo;
            double vps2( 0.0);

            saturated_vapour_pressure( (_tmda + _dt), vps2, solcon);
            vpsl = (vps2 - vps) / _dt;

            /*
             * actual water loss, resistance to vapour transfer and
             * estimated temperature difference
             */

            _etrd = (rdn * (vpsl / (vpsl + psch))) / lhvap;
            _etae = (psch * ea) / (vpsl + psch);
            _etd  = _etrd + _etae;
            re   = 86400.0 * 1000.0 * 0.018016 / (fu2 * rbgl * (_tmda + 0.5 * _dt + 273.16));
            dtn  = re * ((rdn - lhvap * _etd) / 86400.0) / rhocp;

            /* check on equilibrium and maximum number of iterations */

            equil  = (std::abs( dtn - _dt) < _tmdi);
            inloop = inloop + 1;

            _dt = dtn;
        }
    }

    return  LDNDC_ERR_OK;
}


} /*namespace ldndc*/

