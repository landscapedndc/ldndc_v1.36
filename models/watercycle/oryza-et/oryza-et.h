
#ifndef  MOBILE_MODULE_ORYZA_ET_H_
#define  MOBILE_MODULE_ORYZA_ET_H_

#include  "mbe_legacymodel.h"
#include  "state/mbe_state.h"

namespace ldndc {

class  substate_physiology_t;
class  substate_soilchemistry_t;
class  substate_watercycle_t;

/*
 * @brief
 *      WaterCycleOryzaET origins from the Oryza2000 model
 *      and calculates:
 *          - potential evapotranspiration
 *          - potential evaporation
 *          - potential transpiration
 *      Potential evapotranspiration is calculated based on penman
 *
 * @author
 *      David Kraus
 *
 */
class  LDNDC_API  WaterCycleOryzaET  :  public  MBE_LegacyModel
{
    LMOD_EXPORT_MODULE_INFO(WaterCycleOryzaET,"watercycle:oryza-et","Oryza ET");


public:
    WaterCycleOryzaET(
                      MoBiLE_State *,
                      cbm::io_kcomm_t *,
                      timemode_e);

    ~WaterCycleOryzaET();

    lerr_t  configure( ldndc::config_file_t const *);

    lerr_t  initialize();
    lerr_t  solve();
    lerr_t  finalize();

    lerr_t  sleep();
    lerr_t  wake();

private:

    cbm::io_kcomm_t *  io_kcomm;

    MoBiLE_PlantVegetation *  m_veg;
    
    input_class_climate_t const *  climate_in;
    input_class_species_t const *  species_in;

    substate_physiology_t const &  ph_;
    substate_soilchemistry_t const &  sc_;
    substate_watercycle_t &  wc_;

    bool time_mode_subdaily_;
    double day_fraction_;

    /*!
     * @brief
     *      This subroutine calculates saturated vapour pressure and
     *      slope of saturated vapour pressure. Parameters of the
     *      formula were fitted on the Goff-Gratch formula used in the
     *      Smithsonian Handbook of Meteorological Tables. The
     *      saturated vapour following the Goff-Gratch formula is also
     *      available as a subroutine. (Note that 1kPa = 10 mbar)
     * @author
     *      Daniel van Kraalingen (original version)
     *      David Kraus (implementation into LandscapeDNDC)
     *
     * @param
     *      temperature (°C)
     * @param
     *      saturated vapour pressure (kPa)
     * @param
     *      slope of saturated vapour pressure at given temperature (kPa °C-1)
     */
    lerr_t
    saturated_vapour_pressure(
                              double /* temperature */,
                              double & /* saturated vapour pressure */,
                              double & /* slope of saturated vapour pressure at given temperature */);

    /*!
     * @brief
     *      This subroutine calculates reference evapotranspiration
     *      in a manner similar to Penman (1948). To obtain crop evapo-
     *      transpiration, multiplication with a Penman crop factor
     *      should be done. Calculations can be carried out for three
     *      types of surfaces: water, wet soil, and short grass
     *      (ISURF=1,2,3 resp.). When the input variable TMDI is set to
     *      zero, a single calculation is done and an estimate is
     *      provided of temperature difference between the environment
     *      and the surface (DT). If the absolute value of DT is large
     *      an iterative Penman can be carried out which continues until
     *      the new surface temperature differs by no more than TMDI
     *      from the old surface temperature. Two types of long-wave
     *      radiation calculations are available Swinbank and Brunt.
     *      The switch between the two is made by choosing the right
     *      values for ANGA and ANGB. If ANGA and ANGB are zero,
     *      Swinbank is used, if both are positive, Brunt is used and
     *      the ANGA and ANGB values are in the calculation of the
     *      cloud cover.
     *
     * @author
     *      Daniel van Kraalingen (original version)
     *      David Kraus (implementation into LandscapeDNDC)
     */
    lerr_t penman(
                  int /* switch for different surface types */,
                  int /* day of year */,
                  double /* latitude */,
                  double /* albedo */,
                  double /* A value of Angstrom formula */,
                  double /* B value of Angstrom formula */,
                  double /* temperature tolerance */,
                  double /* short-wave radiation */,
                  double /* temperature */,
                  double /* wind speed */,
                  double /* vapour pressure */,
                  double /* estimated temperature difference between surface height and reference height */,
                  double & /* potential evapotranspiration */,
                  double & /* radiation driven part of potential evapotranspiration */,
                  double & /* dryness driven part of potential evapotranspiration */);
};

} /*namespace ldndc*/

#endif  //  MOBILE_MODULE_ORYZA_ET_H_
