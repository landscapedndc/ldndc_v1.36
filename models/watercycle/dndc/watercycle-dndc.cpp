/*!
 * @file
 * @author
 *  - Ralf Kiese
 *  - David Kraus
 */

/*!
 * @page watercycledndc
 * @tableofcontents
 *
 * @section watercycledndc_guide User guide
 *  This WatercycleDNDC model is taken from the PnET-N-DNDC Model (Li 2000).
 *  It calculates daily dynamics of snowcover, soil ice content, potential
 *  evaporation and hourly dynamics of rainfall, interception, transpiration,
 *  percolation, and runoff.
 *
 *  @image html watercycledndc_overview.png "Watercycle overview" width=500
 *  @image latex watercycledndc_overview.png "Watercycle overview"
 *
 * @subsection watercycledndc_guide_structure Model structure
 *  WatercycleDNDC requires further models for:
 *  - plant growth (e.g., transpiration demand, fine roots distribution)
 *
 * @subsection watercycledndc_guide_parametrization Parametrization
 * The following lists include parameters that might be calibrated
 * in order to adapt the model to site conditions:
 *
 * Interception:
 * - MWWM (speciesparameter: specific interception capacity of wood mass (m kg-1DW))
 * - MWFM (speciesparameter: specific interception capacity of foliage (m m-2LAI))
 *
 * Evapotranspiration:
 * - \f$ ROOT\_DEPENDENT\_TRANS \f$
 * - \f$ WCDNDC\_INCREASE\_POT\_EVAPOTRANS \f$
 *
 * Preferential flow
 * - \f$ BY\_PASSF \f$
 *
 * Lateral runoff
 * - \f$ FRUNOFF \f$
 *
 * Percolation
 * - \f$ FPERCOL \f$
 * - \f$ IMPEDANCE\_PAR \f$
 *
 */


#include  "watercycle/dndc/watercycle-dndc.h"
#include  "watercycle/ld_droughtstress.h"
#include  "watercycle/ld_priestley-taylor.h"
#include  "watercycle/ld_thornthwaite.h"
#include  "watercycle/ld_penman.h"

#include  <constants/cbm_const.h>
#include  <math/cbm_math.h>

#include  <input/climate/climate.h>
#include  <input/groundwater/groundwater.h>
#include  <input/setup/setup.h>
#include  <input/siteparameters/siteparameters.h>
#include  <input/soillayers/soillayers.h>

#include  <utils/cbm_utils.h>
#include  <logging/cbm_logging.h>

#include  <scientific/meteo/ld_meteo.h>
#include  <scientific/hydrology/ld_vangenuchten.h>

LMOD_MODULE_INFO(WatercycleDNDC,TMODE_SUBDAILY|TMODE_POST_DAILY,LMOD_FLAG_USER);

REGISTER_OPTION(WatercycleDNDC, automaticirrigation,"  [float]");
REGISTER_OPTION(WatercycleDNDC, ggcmi_irrigation,"  [bool]");
REGISTER_OPTION(WatercycleDNDC, evaporation,"  [bool]");
REGISTER_OPTION(WatercycleDNDC, potentialevapotranspiration,"  [char]");

namespace ldndc {

const int unsigned WatercycleDNDC::IMAX_W = 24;


WatercycleDNDC::WatercycleDNDC(
                   MoBiLE_State *  _state,
                   cbm::io_kcomm_t *  _iokcomm,
                   timemode_e  _timemode)
                    : MBE_LegacyModel( _state, _timemode),
                      m_iokcomm( _iokcomm),
                      m_groundwater( NULL),
                      m_climate( _iokcomm->get_input_class< climate::input_class_climate_t >()),
                      m_setup( _iokcomm->get_input_class< setup::input_class_setup_t >()),
                      m_param( _iokcomm->get_input_class< siteparameters::input_class_siteparameters_t >()),
                      m_soillayers( _iokcomm->get_input_class< soillayers::input_class_soillayers_t >()),
                      m_veg( &_state->vegetation),
                      sc_( _state->get_substate_ref< substate_soilchemistry_t >()),
                      wc_( _state->get_substate_ref< substate_watercycle_t >()),
                      mc_( _state->get_substate_ref< substate_microclimate_t >()),
                      m_snowdndc( _iokcomm),
                      m_eventflood( _state, _iokcomm, _timemode)
{
    if ( m_iokcomm->get_input_class< input_class_site_t >())
    {
        ground_water_table = m_iokcomm->get_input_class< site::input_class_site_t >()->watertable();
    }

    water_table_flooding = invalid_dbl;
    max_percolation = invalid_dbl;
    bund_height = 0.0;
    irrigation_height = 0.0;
    unlimited_water = true;
    saturation_level = 0.0;
    soil_depth_flooding = 0.0;

    ground_water_table = ldndc::site::site_info_defaults.watertable;

    m_cfg.evapotranspiration_method = "thornthwaite";
    m_cfg.snowpack = 1;
    m_cfg.icecontent = 1;
    m_cfg.automaticirrigation = -1.0;
    m_cfg.ggcmi_irrigation = false;
}


/*!
 * fixed amount of maximum rainfall/irrigation triggered per time step
 */
double
WatercycleDNDC::rainfall_intensity()
{
    if ( m_iokcomm->get_input_class< input_class_climate_t >())
    {
        return m_iokcomm->get_input_class< climate::input_class_climate_t >()->station_info()->rainfall_intensity * cbm::M_IN_MM;
    }
    else
    {
        return ldndc::climate::climate_info_defaults.rainfall_intensity * cbm::M_IN_MM;
    }
}


WatercycleDNDC::~WatercycleDNDC()
{
}


lerr_t
WatercycleDNDC::finalize()
{
    return  LDNDC_ERR_OK;
}


lerr_t
WatercycleDNDC::sleep()
{
    return  LDNDC_ERR_OK;
}


lerr_t
WatercycleDNDC::wake()
{
    return  LDNDC_ERR_OK;
}


/*!
 * @page watercycledndc
 * @section watercycledndc_options Model options
 * Available options:
 * Default options are marked with bold letters.
 */
lerr_t
WatercycleDNDC::configure( ldndc::config_file_t const *  _cf)
{
    /*!
     * @page watercycledndc
     *  - Potential evapotranspiration model (default: "potentialevapotranspiration" = \b thornthwaite / penman, priestleytaylor)
     */
    m_cfg.evapotranspiration_method = get_option< char const * >( "potentialevapotranspiration", "thornthwaite");

    /*!
     * @page watercycledndc
     *  - Automatic irrigation (default: "automaticirrigation" = \b -1.0)
     *    Set to a number \f$ x \f$ within the interval [0.0, 1.0] in order to automatically trigger irrigation as soon as
     *    the soil water content \f$ wc \f$ drops below a defined target water content depending on wilting point \f$ wp \f$ and field capacity \f$ fc \f$:
     *    \f[
     *    wc < wp + x (fc - wp)
     *    \f]
     */
    m_cfg.automaticirrigation = get_option< float >( "automaticirrigation", -1.0);

    //ggcmi style irrigation set to false by default
    m_cfg.ggcmi_irrigation = get_option< bool >( "ggcmi_irrigation", false);

    bool  calculate_snowpack = false;
    CF_LMOD_QUERY(_cf, "snowpack", calculate_snowpack, true);
    m_cfg.snowpack = ( calculate_snowpack) ? 1 : 0;
    /* TODO module option: snow pack */

    bool  calculate_icecontent = false;
    CF_LMOD_QUERY(_cf, "ice_content", calculate_icecontent, true);
    m_cfg.icecontent = ( calculate_icecontent) ? 1 : 0;
    /* TODO module option: ice content */

    return  LDNDC_ERR_OK;
}


lerr_t
WatercycleDNDC::register_ports( cbm::io_kcomm_t *  _io_kcomm)
{
    lerr_t rc_flood = m_eventflood.register_ports( _io_kcomm);
    if ( rc_flood){ return LDNDC_ERR_FAIL; }

    int rc_manure = this->m_ManureEvents.subscribe( "manure", _io_kcomm);
    if ( rc_manure != LD_PortOk)
    { return  LDNDC_ERR_FAIL; }

    int rc = this->m_IrrigationEvents.subscribe( "irrigate", _io_kcomm);
    if ( rc != LD_PortOk)
    { return  LDNDC_ERR_FAIL; }

    return  LDNDC_ERR_OK;
}


lerr_t
WatercycleDNDC::unregister_ports( cbm::io_kcomm_t *  _io_kcomm)
{
    m_eventflood.unregister_ports( _io_kcomm);

    m_ManureEvents.unsubscribe();

    m_IrrigationEvents.unsubscribe();

    return  LDNDC_ERR_OK;
}


lerr_t
WatercycleDNDC::initialize()
{
    /* check for proper time resolution */
    if ( lclock()->time_resolution() > 1 && ( timemode() != TMODE_SUBDAILY))
    {
        KLOGERROR( "module \"", name(), "\" must run in subdaily resolution when simulation time resolution is greater 1");
        return  LDNDC_ERR_OBJECT_INIT_FAILED;
    }

    /*
     * check if groundwater input is available
     * note: check m_groundwater for not NULL for any usage
     */
    cbm::source_descriptor_t  gw_source_info;
    lid_t  gw_id = m_iokcomm->get_input_class_source_info< input_class_groundwater_t >( &gw_source_info);
    if ( gw_id != invalid_lid)
    {
        m_groundwater = m_iokcomm->acquire_input< input_class_groundwater_t >( &gw_source_info);
    }
    else
    {
        m_groundwater = NULL;
    }

    if ( m_cfg.evapotranspiration_method == "thornthwaite")
    {
        thornthwaite_heat_index = ldndc::thornthwaite_heat_index(m_setup->latitude(),
                                                                 lclock()->days_in_year(),
                                                                 m_climate->annual_temperature_average(),
                                                                 m_climate->annual_temperature_amplitude());
    }

    /*
     * set time resolution multipliers
     * and assign proper time-mode depending variables
     */
    if ( timemode() & TMODE_SUBDAILY)
    {
        //define number of internal time steps if time_resolution < IMAX_W
        double const  t_res = lclock()->time_resolution();
        nwc = ( t_res >= IMAX_W) ? 1 : int( IMAX_W / double( t_res));
        nhr = IMAX_W / double( t_res * nwc);
    }
    else if ( timemode() & (TMODE_PRE_DAILY|TMODE_POST_DAILY))
    {
        nwc = IMAX_W;
        nhr = 24.0 / double(IMAX_W);
    }
    else
    {
        KLOGERROR("Unknown timemode!?");
        return LDNDC_ERR_FAIL;
    }

    max_percolation = WaterFlowSaturated( m_soillayers->soil_layer_cnt()-1, 1.0);

    m_eventflood.initialize();

    /* initialize state */

    m_mc.air_temperature_below_canopy = 0.0;
    m_mc.precipitation = 0.0;
    m_mc.rainfall = 0.0;

    potentialtranspiration = 0.0;
    accumulated_potentialtranspiration_old = 0.0;

    m_wc.accumulated_irrigation_event_executed = 0.0;
    m_wc.irrigation = 0.0;

    wc_.irrigation_reservoir = 0.0;

    wc_.accumulated_potentialevapotranspiration = 0.0;
    wc_.accumulated_infiltration = 0.0;
    wc_.accumulated_interceptionevaporation = 0.0;
    wc_.accumulated_soilevaporation = 0.0;
    wc_.accumulated_surfacewaterevaporation = 0.0;
    wc_.accumulated_runoff = 0.0;
    wc_.accumulated_throughfall = 0.0;
    wc_.accumulated_groundwater_access = 0.0;
    wc_.accumulated_irrigation_reservoir_withdrawal = 0.0;

    size_t const  S = m_soillayers->soil_layer_cnt();

    m_mc.soil_temperature_sl.resize( S);
    m_mc.soil_temperature_sl = 0.0;

    m_wc.percolation_sl.resize( S);
    m_wc.percolation_sl = 0.0;

    /* initialize variables for snow and ice calculation */
    m_snowdndc.dt = nhr / double( IMAX_W);
    m_icecontent_in.h_sl.resize( S);
    m_icecontent_in.bulkdensity_sl.resize( S);
    m_icecontent_in.fcorg_sl.resize( S);
    m_icecontent_in.wc_sl.resize( S);
    m_icecontent_in.ice_sl.resize( S);
    m_icecontent_in.soil_temperature_sl.resize( S);
    m_icecontent_out.wc_sl.resize( S);
    m_icecontent_out.ice_sl.resize( S);
    m_icecontent_out.soil_temperature_sl.resize( S);

    return  LDNDC_ERR_OK;
}


lerr_t
WatercycleDNDC::receive_state()
{
    /* microclimate */

    m_mc.rainfall = 0.0;

    if ( timemode() & TMODE_SUBDAILY)
    {
        m_mc.air_temperature_below_canopy = mc_.temp_fl[0];
        m_mc.precipitation = mc_.ts_precipitation;
        m_mc.soil_temperature_sl = mc_.temp_sl;
    }
    else if ( timemode() & (TMODE_PRE_DAILY|TMODE_POST_DAILY))
    {
        m_mc.air_temperature_below_canopy = mc_.nd_temp_fl[0];
        m_mc.precipitation = mc_.nd_precipitation;
        m_mc.soil_temperature_sl = mc_.nd_temp_sl;
    }
    else
    {
        KLOGFATAL( "Error: Wrong time mode");
        return LDNDC_ERR_FAIL;
    }

    /* watercycle */

    m_wc.irrigation = 0.0;

    if ( lclock()->subday() == 1)
    {
        potentialtranspiration = wc_.accumulated_potentialtranspiration -
                                 accumulated_potentialtranspiration_old;
        accumulated_potentialtranspiration_old = wc_.accumulated_potentialtranspiration;
    }

    m_wc.plant_waterdeficit_compensation = 0.0;

    if ( timemode() & TMODE_SUBDAILY)
    {
        /* read groundwater table from input file if provided */
        if ( m_groundwater != NULL)
        {
            ground_water_table = m_groundwater->watertable_subday( lclock_ref());
        }
    }
    else
    {
        /* read groundwater table from input file if provided */
        if ( m_groundwater != NULL)
        {
            ground_water_table = m_groundwater->watertable_day( lclock_ref());
        }
    }

    return  LDNDC_ERR_OK;
}


lerr_t
WatercycleDNDC::send_state()
{
    /* microclimate */

    if ( timemode() & TMODE_SUBDAILY)
    {
        mc_.temp_sl = m_mc.soil_temperature_sl;
    }
    else if ( timemode() & (TMODE_PRE_DAILY|TMODE_POST_DAILY))
    {
        mc_.nd_temp_sl = m_mc.soil_temperature_sl;
    }
    else
    {
        KLOGFATAL( "Error: Wrong time mode");
        return LDNDC_ERR_FAIL;
    }

    return  LDNDC_ERR_OK;
}


/*!
 * @brief
 * Kicks off computation for one time step
 */
lerr_t
WatercycleDNDC::solve()
{
    receive_state();

    /* irrigation events */
    lerr_t  rc_irri = event_irrigate();
    if ( rc_irri != LDNDC_ERR_OK)
    {
        KLOGERROR("Irrigation event not successful!");
        return  LDNDC_ERR_FAIL;
    }

    /* flooding events */
    lerr_t rc_flood = event_flood();
    if ( rc_flood != LDNDC_ERR_OK)
    {
        KLOGERROR("Flood event not successful!");
        return  rc_flood;
    }

    /* begin water balance check */
    double  balance_in = balance_check( NULL);

    /* internal time loop */
    for ( size_t i = 0;  i < nwc;  i++)
    {
        /* rainfall */
        CalcRaining( i);

        /* irrigation */
        CalcIrrigation( i);

        /* snowfall and ice formation */
        if ( m_cfg.snowpack)
        {
            lerr_t  rc_snowpack = CalcSnowPack();
            if ( rc_snowpack)
            { return  rc_snowpack; }
        }
        if ( m_cfg.icecontent)
        {
            lerr_t  rc_icecontent = CalcIceContent();
            if ( rc_icecontent)
            { return  rc_icecontent; }
        }

        /* potential evaporation */
        lerr_t rc_potevapotrans = CalcPotEvapoTranspiration();
        if ( rc_potevapotrans)
        { return rc_potevapotrans; }

        /* interception and evaporation from interception */
        CalcInterception();

        /* transpiration */
        lerr_t rc_trans = CalcTranspiration();
        if ( rc_trans)
        { return rc_trans; }

        /* snow evaporation */
        CalcSnowEvaporation();

        /* surface water evaporation */
        CalcSurfaceWaterEvaporation();

        /* Water that bypasses the soil profile via e.g., cracks and wormholes */
        WatercycleDNDC_preferential_flow();

        /* soil evaporation and percolation */
        CalcSoilEvapoPercolation();
    }

    /* check water balance */
    balance_check( &balance_in);

    send_state();

    return  LDNDC_ERR_OK;
}


lerr_t
WatercycleDNDC::event_irrigate()
{
    EventAttributes const *  ev_manure = NULL;
    while (( ev_manure = this->m_ManureEvents.pop()) != NULL)
    {
        double const volume = ev_manure->get( "/volume", 0.0);  //given in m3:ha-1
        wc_.accumulated_irrigation += volume * cbm::HA_IN_M2;
    }

    EventAttributes const *  ev_irri = NULL;
    while (( ev_irri = this->m_IrrigationEvents.pop()) != NULL)
    {
        double const  amount = ev_irri->get( "/amount", 0.0);

        // check if irrigation water is stored in reservoir
        if ( ev_irri->get( "/is-reservoir", false) == true)
        {
            //negative amount resets irrigation reservoir
            if ( !cbm::flt_greater_equal_zero( amount))
            {
                wc_.irrigation_reservoir = 0.0;
            }
            else
            {
                wc_.irrigation_reservoir += amount * cbm::M_IN_MM;
            }
        }
        else
        {
            if ( !cbm::flt_greater_equal_zero( amount))
            {
                KLOGERROR( "negative amount of irrigation water [amount=", amount,"]");
                return LDNDC_ERR_FAIL;
            }
            wc_.accumulated_irrigation += amount * cbm::M_IN_MM;
        }
    }

    return  LDNDC_ERR_OK;

}


/*!
 * @page watercycledndc
 * @section watercycledndc_management Management
 * @subsection watercycledndc_management_flooding Flooding
 *  Selectable flooding regimes via event input:
 *  - Constant surfacewater height and fully saturated soil profile (\b watertable > 0.0).
 *    Height of water table is set to \b watertable.
 *  - Moist conditions (\b watertable < 0.0).
 *    Upper soil layers are set to field capacity (soil depth < - \b watertable)
 *    Lower soil layers are set fully water saturated (soil depth > - \b watertable)
 *  - Fixed bund height (\b bundheight > 0.0 ). Water table is calculated dynamically depening on
 *    water input (precipitation & irrigation)
 *  - Percolation rate (percolationrate > 0.0). Maximum value for soil water percolation.
 *    Can be defined in order to reflect soil puddling, which decreases soil water percolation
 *    under flooded conditions.
 */
lerr_t
WatercycleDNDC::event_flood()
{
    lerr_t  rc = m_eventflood.solve();
    if ( rc != LDNDC_ERR_OK)
    {
        KLOGERROR("Irrigation event not successful!");
        return  rc;
    }

    water_table_flooding = m_eventflood.get_water_table();

    bund_height = m_eventflood.get_bund_height();
    if ( cbm::flt_greater_zero( water_table_flooding))
    {
        bund_height = cbm::bound_min( water_table_flooding, bund_height);
    }

    irrigation_height = m_eventflood.get_irrigation_height();

    /* max_percolation is gradually decreased after end of flooding event to inital value */
    double const maximum_percolation = m_eventflood.get_maximum_percolation();
    if ( cbm::is_valid( maximum_percolation))
    {
        if ( cbm::flt_greater_zero( maximum_percolation))
        {
            max_percolation = maximum_percolation * nhr / 24.0;
        }
    }
    else
    {
        /* time rate for gradual recovery of max_percolation */
        double const time_rate( get_time_step_duration() * 0.1);
        max_percolation -= (max_percolation - WaterFlowSaturated( m_soillayers->soil_layer_cnt()-1, 1.0)) * time_rate;
    }

    unlimited_water = m_eventflood.have_unlimited_water();

    saturation_level = m_eventflood.get_saturation_level();
    soil_depth_flooding = m_eventflood.get_soil_depth();

    return  LDNDC_ERR_OK;
}


/*!
 * @brief
 * @page watercycledndc
 * @subsection watercycledndc_management_irrigation Irrigation
 * ...
 */
void WatercycleDNDC::CalcIrrigation(
                                    unsigned int _i)
{
    //reset irrigation
    m_wc.irrigation = 0.0;

    /*!
     *  Automatic irrigation to prevent water stress
     */
    if ( cbm::flt_in_range_lu(0.0, m_cfg.automaticirrigation, 1.0))
    {
        for ( size_t l = 0;  l < m_soillayers->soil_layer_cnt();  ++l)
        {
            // trigger irrigation events as soon as drought stress appears
            double const wc_target( sc_.wcmin_sl[l] + m_cfg.automaticirrigation * (sc_.wcmax_sl[l] -  sc_.wcmin_sl[l]));
            if ( cbm::flt_less( wc_.wc_sl[l], wc_target))
            {
                // irrigation assumed until target water content
                double const irrigate_layer( (wc_target - wc_.wc_sl[l]) * sc_.h_sl[l]);
                m_wc.irrigation += irrigate_layer;
                wc_.accumulated_irrigation_automatic += irrigate_layer;
            }
        }
    }

    if ( cbm::flt_in_range_lu( 0.0, saturation_level, 1.0))
    {
        double wc_sum( 0.0);
        double wcmax_sum( 0.0);
        double wcmin_sum( 0.0);
        for ( size_t l = 0;  l < m_soillayers->soil_layer_cnt();  ++l)
        {
            wc_sum += wc_.wc_sl[l] * sc_.h_sl[l];
            wcmax_sum += sc_.wcmax_sl[l] * sc_.h_sl[l];
            wcmin_sum += sc_.wcmin_sl[l] * sc_.h_sl[l];

            if ( cbm::flt_greater_equal( sc_.depth_sl[l], soil_depth_flooding))
            {
                break;
            }
        }
        
        // trigger irrigation events as soon as drought stress appears
        double const wc_target( wcmin_sum + saturation_level * (wcmax_sum -  wcmin_sum));
        if ( cbm::flt_less( wc_sum, wc_target))
        {
            m_wc.irrigation += irrigation_height;
            wc_.accumulated_irrigation_automatic += irrigation_height;
        }
    }

    /*!
     * GGCMI style irrigation
     * - fill up the water content of each soil layer to field capacity at the start of each day
     * - water supplied just injected into soil layer
     * - no interception, no run off, no surface-water
     * - only irrigate if a crop is present
     */
    if ( m_cfg.ggcmi_irrigation)
    {
        if( (lclock()->subday() == 1) && (_i == 0))
        {
            if( m_veg->size() > 0 ) //check crop is present
            {
                double water_supply( 0.0);
                for ( size_t l = 0;  l < m_soillayers->soil_layer_cnt();  ++l)
                {
                    if ( cbm::flt_greater_zero( m_veg->mfrt_sl( l))) //only irrigate soil layers that contain roots
                    {
                        if( cbm::flt_less( wc_.wc_sl[l], sc_.wcmax_sl[l]) &&
                            cbm::flt_equal_zero( wc_.ice_sl[l])) //only add water if no ice is present
                        {
                            water_supply += (sc_.wcmax_sl[l] - wc_.wc_sl[l]) * sc_.h_sl[l]; //amount of water added (m)
                            wc_.wc_sl[l] = sc_.wcmax_sl[l];
                        }
                    }
                }
                wc_.accumulated_irrigation_ggcmi += water_supply;
            }
        }
    }

    /*!
     *  Irrigation due to flooding
     */
    if ( cbm::is_valid( water_table_flooding))
    {
        /* Dynamic flooding */
        if ( cbm::is_valid( irrigation_height))
        {
            bool irrigate( false);

            /* Irrigate after surface water table drop */
            if ( cbm::flt_greater_equal_zero( water_table_flooding))
            {
                if ( cbm::flt_greater( water_table_flooding, wc_.surface_water))
                {
                    irrigate = true;
                }
            }
            else
            {
                if ( !cbm::flt_greater_zero( wc_.surface_water))
                {
                    for ( size_t sl = 0;  sl < m_soillayers->soil_layer_cnt();  ++sl)
                    {
                        if ( cbm::flt_greater_equal( sc_.depth_sl[sl], -water_table_flooding))
                        {
                            //check if water content below AWD_DEPTH is saturated
                            if ( cbm::flt_less( wc_.wc_sl[sl], 0.9 * sc_.poro_sl[sl]))
                            {
                                irrigate = true;
                            }
                            break;
                        }
                    }
                }
            }
            // trigger AWD irrigation event
            if ( irrigate)
            {
                double water_supply( cbm::bound_min( 0.0, irrigation_height - wc_.surface_water));
                for ( size_t sl = 0;  sl < m_soillayers->soil_layer_cnt();  ++sl)
                {
                    // set soil fully water saturated
                    if ( cbm::flt_greater( sc_.poro_sl[sl], wc_.wc_sl[sl]))
                    {
                        water_supply += ((sc_.poro_sl[sl] - wc_.wc_sl[sl]) * sc_.h_sl[sl]);
                    }
                }

                //remove rainfall (less irrigation water is supplied if it if raining)
                water_supply = cbm::bound_min( 0.0, water_supply - m_mc.rainfall);

                if ( !unlimited_water)
                {
                    if ( cbm::flt_greater( wc_.irrigation_reservoir, water_supply))
                    {
                        wc_.irrigation_reservoir -= water_supply;
                    }
                    else
                    {
                        water_supply = wc_.irrigation_reservoir;
                        wc_.irrigation_reservoir = 0.0;
                    }
                    wc_.accumulated_irrigation_reservoir_withdrawal += water_supply;
                }
                else
                {
                    wc_.accumulated_irrigation_automatic += water_supply;
                }
                m_wc.irrigation += water_supply;
            }
        }
        /* Static flooding */
        else
        {
            if ( !cbm::flt_greater_zero( water_table_flooding))
            {
                /* Surface water will be actively drained */
                if ( cbm::flt_greater_zero( wc_.surface_water))
                {
                    wc_.accumulated_runoff += wc_.surface_water;
                    wc_.surface_water = 0.0;
                }

                double water_supply( 0.0);
                for ( size_t sl = 0;  sl < m_soillayers->soil_layer_cnt();  ++sl)
                {
                    if ( cbm::flt_greater( -water_table_flooding, sc_.depth_sl[sl]) &&
                         cbm::flt_greater( sc_.wcmax_sl[sl], wc_.wc_sl[sl]))
                    {
                        water_supply += (sc_.wcmax_sl[sl] - wc_.wc_sl[sl]) * sc_.h_sl[sl];
                    }
                    else
                    {
                        if ( cbm::flt_greater( sc_.poro_sl[sl], wc_.wc_sl[sl]))
                        {
                            water_supply += (sc_.poro_sl[sl] - wc_.wc_sl[sl]) * sc_.h_sl[sl];
                        }
                    }
                }

                //remove rainfall (less irrigation water is supplied if it if raining)
                water_supply = cbm::bound_min( 0.0, water_supply - m_mc.rainfall);

                if ( !unlimited_water)
                {
                    if ( cbm::flt_greater( wc_.irrigation_reservoir, water_supply))
                    {
                        wc_.irrigation_reservoir -= water_supply;
                    }
                    else
                    {
                        water_supply = wc_.irrigation_reservoir;
                        wc_.irrigation_reservoir = 0.0;
                    }
                    wc_.accumulated_irrigation_reservoir_withdrawal += water_supply;
                }
                else
                {
                    wc_.accumulated_irrigation_automatic += water_supply;
                }
                m_wc.irrigation += water_supply;
            }
            /*!
             *  irrigation triggered by flooding management
             */
            else if ( cbm::flt_greater( water_table_flooding, wc_.surface_water))
            {
                //adjust surface and soil water in case of flooding event
                //water required to fill up surface water
                double water_supply( water_table_flooding - wc_.surface_water);
                
                //add water required to fill top x layers up to saturation
                unsigned long no_layers(3);
                if( no_layers > m_soillayers->soil_layer_cnt())
                {
                    no_layers = m_soillayers->soil_layer_cnt();
                }
                
                for ( size_t sl = 0;  sl < no_layers;  ++sl)
                {
                    water_supply += ((sc_.poro_sl[sl] - wc_.wc_sl[sl]) * sc_.h_sl[sl]);
                }
                
                //remove rainfall (less irrigation water is supplied if it if raining)
                water_supply = cbm::bound_min( 0.0, water_supply - m_mc.rainfall);
                
                if ( !unlimited_water)
                {
                    if ( cbm::flt_greater( wc_.irrigation_reservoir, water_supply))
                    {
                        wc_.irrigation_reservoir -= water_supply;
                    }
                    else
                    {
                        water_supply = wc_.irrigation_reservoir;
                        wc_.irrigation_reservoir = 0.0;
                    }
                    wc_.accumulated_irrigation_reservoir_withdrawal += water_supply;
                }
                else
                {
                    wc_.accumulated_irrigation_automatic += water_supply;
                }
                m_wc.irrigation += water_supply;
            }
        }
    }

    /*!
     *  Irrigation due to explicit irrigation event
     */
    double const irrigation_event( cbm::bound( 0.0,
                                               wc_.accumulated_irrigation - m_wc.accumulated_irrigation_event_executed,
                                               (double)hours_per_time_step() * rainfall_intensity()));
    m_wc.irrigation += irrigation_event;
    m_wc.accumulated_irrigation_event_executed += irrigation_event;
}


/*!
 * @page watercycledndc
 * @section watercycledndc_raining Rainfall distribution
 * Rainfall per timestep set by a parameterized rainfall intensity (RI, mm timestep-1). 
 * Thus, the number of timesteps (usually hours) with rainfall is given by:
 * \f[
 *   nts_{rain} = \frac {P}{RI}
 * \f]
 * with
 * - P: precipitation (mm day-1)
 * 
 * If the precipitation per day is larger than the parameterized rainfall intensity times
 * the number of timesteps, rainfall intensity will be increased accordingly:
 * \f[
 *   ri_{act} = \frac { P }{ n_{timesteps} }
 * \f]
 with
 - n_timesteps: number of timesteps per day
  */
void WatercycleDNDC::CalcRaining(
                                 unsigned int  _i)
{
    double hours_since_start_of_time_step( double(_i + 1) * nhr);

    // if precipitation per time step exceeds total rainfall intensity per time step
    // precipitation is evenly distributed
    if ( cbm::flt_greater_equal( m_mc.precipitation, (double)hours_per_time_step() * rainfall_intensity()))
    {
        m_mc.rainfall = m_mc.precipitation / nwc;
    }
    // precipitation is set to rainfall intensity
    else if ( cbm::flt_greater_equal( m_mc.precipitation, hours_since_start_of_time_step * rainfall_intensity()))
    {
        m_mc.rainfall = nhr * rainfall_intensity();
    }
    // precipitation is
    else
    {
        m_mc.rainfall = std::max( 0.0, m_mc.precipitation - (double( _i) * nhr * rainfall_intensity()));
    }

    wc_.accumulated_precipitation += m_mc.rainfall;
}



/*!
 * @page watercycledndc
 * @section watercycledndc_snow Snow and ice
 *  Snowfall and soil ice formation are calculated
 *  as presented in the library functions @link snow_ice Snow and ice \endlink.
 */
lerr_t
WatercycleDNDC::CalcSnowPack()
{
    WaterCycleSnowDNDC::SnowPackStateIn  state_in;
    state_in.air_temperature_above_canopy = mc_.nd_airtemperature;
    state_in.air_temperature_below_canopy = m_mc.air_temperature_below_canopy;
    state_in.rainfall = m_mc.rainfall;
    state_in.surface_ice = wc_.surface_ice;
    state_in.surface_water = wc_.surface_water;

    WaterCycleSnowDNDC::SnowPackStateOut  state_out;

    lerr_t  rc_snowpack = m_snowdndc.SnowPack( state_in, state_out);
    if ( rc_snowpack)
    {
        KLOGERROR("Snow calculation not successful!");
        return  LDNDC_ERR_FAIL;
    }

    m_wc.snowfall = state_out.snowfall;
    m_wc.snow_melt = state_out.snow_melt;

    m_mc.rainfall = state_out.rainfall;
    wc_.surface_ice = state_out.surface_ice;
    wc_.surface_water = state_out.surface_water;

    wc_.accumulated_throughfall += state_out.throughfall;

    return  LDNDC_ERR_OK;
}


/*!
 * @brief
 * Call of ice-content related processes
 */
lerr_t
WatercycleDNDC::CalcIceContent()
{
    WaterCycleSnowDNDC::IceContentStateIn &  state_in = m_icecontent_in;
    for (size_t sl = 0; sl < m_soillayers->soil_layer_cnt(); sl++)
    {
        state_in.h_sl[sl] = sc_.h_sl[sl];
        state_in.bulkdensity_sl[sl] = sc_.dens_sl[sl];
        state_in.fcorg_sl[sl] = sc_.fcorg_sl[sl];
        state_in.ice_sl[sl] = wc_.ice_sl[sl];
        state_in.wc_sl[sl] = wc_.wc_sl[sl];
    }

    state_in.soil_temperature_sl = m_mc.soil_temperature_sl;

    WaterCycleSnowDNDC::IceContentStateOut &  state_out = m_icecontent_out;

    lerr_t  rc_icecontent = m_snowdndc.IceContent( state_in, state_out);
    if ( rc_icecontent)
    {
        KLOGERROR("Ice content calculation not successful!");
        return  LDNDC_ERR_FAIL;
    }

    wc_.wc_sl = state_out.wc_sl;
    wc_.ice_sl = state_out.ice_sl;
    m_mc.soil_temperature_sl = state_out.soil_temperature_sl;

    return  LDNDC_ERR_OK;
}


/*!
 * @page watercycledndc
 * @section watercycledndc_evapotranspiration Potential evaporation (DNDC)
 *  Actual evapotranspiration is the balance between atmospheric demand of
 *  water (potential evapotranspiration) and water supply by the surface.
 *  Implemented concepts for the calculation of potential evapotranspiration
 *  (default: Thornthwaite):
 *  @li @link thornthwaite Thornthwaite @endlink
 *  @li @link priestley_taylor Priestley and Taylor @endlink
 *  @li @link penman Penman @endlink
 */
lerr_t
WatercycleDNDC::CalcPotEvapoTranspiration()
{
    if ( m_cfg.evapotranspiration_method == "thornthwaite")
    {
        if ( lclock()->is_position( TMODE_PRE_YEARLY))
        {
            thornthwaite_heat_index = ldndc::thornthwaite_heat_index( m_setup->latitude(),
                                                                      lclock()->days_in_year(),
                                                                      m_climate->annual_temperature_average(),
                                                                      m_climate->annual_temperature_amplitude());
        }

        double const daylength( ldndc::meteo::daylength( m_setup->latitude(),
                                                         lclock()->yearday()));

        hr_pot_evapotrans = m_param->WCDNDC_INCREASE_POT_EVAPOTRANS() * nhr / 24.0
                            * ldndc::thornthwaite(
                                           mc_.nd_airtemperature,
                                           daylength,
                                           thornthwaite_heat_index);
    }
    else if ( (m_cfg.evapotranspiration_method == "penman") ||
              (m_cfg.evapotranspiration_method == "priestleytaylor"))
    {
        /* clock */
        cbm::sclock_t const &  clk( this->lclock_ref());

        /* leaf area index */
        double lai( 0.0);
        for( PlantIterator  vt = m_veg->begin(); vt != m_veg->end(); ++vt)
        {
            lai += (*vt)->lai();
        }

        /* vapour pressure [10^3:Pa] */
        double vps( 0.0);
        ldndc::meteo::vps( mc_.nd_airtemperature, &vps);
        double vp( vps - m_climate->vpd_day( clk));

        if( m_cfg.evapotranspiration_method == "priestleytaylor")
        {
            hr_pot_evapotrans = m_param->WCDNDC_INCREASE_POT_EVAPOTRANS() * nhr / 24.0
                                * ldndc::priestleytaylor( clk.yearday(),
                                                         clk.days_in_year(),
                                                         mc_.albedo,
                                                         m_setup->latitude(),
                                                         mc_.nd_airtemperature,
                                                         mc_.nd_shortwaveradiation_in,
                                                         vp,
                                                         m_param->PT_ALPHA());
        }
        else if( m_cfg.evapotranspiration_method == "penman")
        {
            ldndc::surface_type surface( cbm::flt_greater_zero( lai) ? short_grass : bare_soil);

            hr_pot_evapotrans = m_param->WCDNDC_INCREASE_POT_EVAPOTRANS() * nhr / 24.0
                                * ldndc::penman(  surface,
                                                clk.yearday(),
                                                clk.days_in_year(),
                                                mc_.albedo,
                                                m_setup->latitude(),
                                                mc_.nd_shortwaveradiation_in * cbm::SEC_IN_DAY,
                                                mc_.nd_airtemperature,
                                                mc_.nd_windspeed,
                                                vp);
        }
    }
    else
    {
        KLOGERROR("[BUG] huh :o how did you get here? ",
                  "tell the maintainers refering to this error message");
        return  LDNDC_ERR_FAIL;
    }

    
    wc_.accumulated_potentialevapotranspiration += hr_pot_evapotrans;

    return  LDNDC_ERR_OK;
}


/*!
 * @brief
 * Collects leaf water from all canopy layers
 */
double
WatercycleDNDC::get_leaf_water()
{
    if ( m_veg->size() > 0u)
    {
        return wc_.wc_fl.sum();
    }
    return 0.0;
}



/*!
 * @page watercycledndc
 * @section watercycledndc_interception Interception
 */
void
WatercycleDNDC::CalcInterception()
{
    /* vegetation structure */

    /*!
     * @page watercycledndc
     * Both, rainfall and irrigation are subject to interception.
     * The amount of water that can be intercepted depends on:
     * - vegetation coverage
     * - vegetation specific interception capacity
     */
    double const add_water(  m_mc.rainfall + m_wc.irrigation);
    double leaf_water( get_leaf_water() + m_veg->area_cover() * add_water);
    double hr_through( (1.0 - m_veg->area_cover()) * add_water);

    /*!
     * @page watercycledndc
     * The interception capacity is defined as the moisture quantity
     * a plant species can harbour in the stems and in the
     * foliage system (see @ref watercyle_interception_capacity).
     */
    double interception_capacity( 0.0);
    for ( PlantIterator vt = m_veg->begin(); vt != m_veg->end(); ++vt)
    {
        interception_capacity += (*vt)->interception_capacity();
    }

    /*!
     * @page watercycledndc
     * All water that is not intercepted feeds into surface water
     * from where it either infiltrates into the soil or is subject to lateral runoff.
     */
    if ( !cbm::flt_greater_zero( interception_capacity))
    {
        hr_through += leaf_water;
        wc_.surface_water += hr_through ;
        wc_.accumulated_throughfall += hr_through;
        wc_.wc_fl = 0.0;
        return;
    }
    else if ( cbm::flt_greater( leaf_water, interception_capacity))
    {
        double const throughfall( leaf_water - interception_capacity);
        leaf_water -= throughfall;
        hr_through += throughfall;
    }

    wc_.surface_water += hr_through ;
    wc_.accumulated_throughfall += hr_through;

    /*!
     * @page watercycledndc
     * Intercepted water contributes to total evapotranspiration.
     * Leaf water evaporation equals the minimum of leaf water and potential evapotranspiration.
     */
    if ( cbm::flt_greater_zero( leaf_water))
    {
        if (  cbm::flt_greater_equal( hr_pot_evapotrans, leaf_water))
        {
            hr_pot_evapotrans = cbm::bound_min( 0.0, hr_pot_evapotrans - leaf_water);
            wc_.accumulated_interceptionevaporation += leaf_water;
            leaf_water = 0.0;
        }
        else if ( cbm::flt_greater_zero( hr_pot_evapotrans))
        {
            leaf_water -= hr_pot_evapotrans;
            wc_.accumulated_interceptionevaporation += hr_pot_evapotrans;
            hr_pot_evapotrans = 0.0;
        }

        if ( !cbm::flt_greater_zero( leaf_water))
        {
            leaf_water = 0.0;
            wc_.wc_fl = 0.0;
        }
    }
    else
    {
        wc_.wc_fl = 0.0;
        return;
    }

    /*!
     * @page watercycledndc
     * Remaining leaf water is redistributed depending on leaf area
     */
    if ( cbm::flt_greater_zero( leaf_water))
    {
        /* leaf area index */
        double lai(0.0);
        for (PlantIterator vt = m_veg->begin(); vt != m_veg->end(); ++vt)
        {
            lai += (*vt)->lai();
        }

        wc_.wc_fl = 0.0;
        if ( cbm::flt_greater_zero( lai))
        {
            for ( PlantIterator vt = m_veg->begin(); vt != m_veg->end(); ++vt)
            {
                MoBiLE_Plant *  p = *vt;
                for ( size_t  l = 0; l < p->nb_foliagelayers(); ++l)
                {
                    wc_.wc_fl[l] += leaf_water * (p->lai_fl[l] / lai);
                }
            }
        }
        else
        {
            size_t  nb_foliagelayers = 1;
            for ( PlantIterator vt = m_veg->begin(); vt != m_veg->end(); ++vt)
            {
                nb_foliagelayers = std::max( nb_foliagelayers, (*vt)->nb_foliagelayers());
            }

            double const  wc_fl = leaf_water / (double)nb_foliagelayers;
            for ( size_t  l = 0;  l < nb_foliagelayers;  ++l)
            {
                wc_.wc_fl[l] = wc_fl;
            }
        }
    }
}


double
WatercycleDNDC::get_clay_limitation( size_t _sl,
                                     double _water_avail,
                                     double _water_ref)
{
    // layer specific water availability
    // organic fraction  [%]
    double const orgf( sc_.fcorg_sl[_sl] / cbm::CCORG);
    // clay factor "1"  [-]
    double const clay_f1( pow(10.0, (-sc_.clay_sl[_sl] - m_param->RCLAY())));
    // clay factor "2"  [-]
    double const clay_f2( std::max(0.0, 1.0 + std::min(1.0 - orgf, sc_.clay_sl[_sl])));

    // fraction of max uptake that can be used according to soil conditions  [%]
    return cbm::bound(0.0, clay_f1 + pow( _water_avail / _water_ref, clay_f2), 1.0);
}


/*!
 * @page watercycledndc
 * @section watercycledndc-transpiration Transpiration
 * Update soilwater content due to transpiration.
 * @subsection Modeloptions Model options /n
 *  a) model configuration transpiration = true:
 *          water content is updated /n
 *
 *  b) model configuration transpiration = false:
 *          water content has been updated by external model
 * 
 * @subsection watercycledndc_transpiration_algorithm Minimum rule
 * 
 * - The water potentially used for transpiration (\f$pottrans\f$) is taken as the minimum value of the 
 *   potential transpiration (provided by the physiology module, see @ref secpotentialtranspiration) 
 *   and the potential evaporation (see @ref potential_evaporation), reduced by evaporation of 
 *   intercepted water. Both potential values are derived as daily averages. 
 *   Therefore, transpiration occurs in the model also at night.
 * 
 * - Rooting depth and root mass distribution are taken into account.
 *
 * - The potentially available water in a layer (wc-wcmin) is reduced by soil properties (clay, cord), 
 *   resulting in an reduced available water amount.
 */
lerr_t
WatercycleDNDC::CalcTranspiration()
{
    /* potential transpiration */
    double hr_potTrans( std::min( potentialtranspiration * nhr / 24.0, hr_pot_evapotrans));
    if ( ! cbm::flt_greater_equal_zero( hr_potTrans))
    {
        KLOGERROR("Negative potential transpiration rate calculated: [",hr_potTrans,"]");
        return  LDNDC_ERR_FAIL;
    }

    /* sum of leaf area index and average of reference canopy conductivity */
    double lai( 0.0);
    double cwp_avg( 0.0);
    int n( 0);
    for ( PlantIterator vt = m_veg->begin(); vt != m_veg->end(); ++vt)
    {
        lai += (*vt)->lai();
        double cwp_vt( vt->CWP_REF());
        if ( cbm::flt_greater( (*vt)->xylem_resistance, vt->RPMIN()) && cbm::flt_greater_zero( vt->RPMIN()))
        {
            cwp_vt *= vt->RPMIN() / (*vt)->xylem_resistance;
        }
        cwp_avg += cwp_vt;
        n += 1;
    }
    if ( n > 1)
    {
        cwp_avg /= n;
    }

    if( m_param->ROOT_WATER_UPTAKE_COUVREUR())
    {
        return CalcTranspirationCouvreur( hr_potTrans);
    }
    else
    {
        /*!
         * @page watercycledndc
         * 1) By iterating rooted layers, determine:
         *     - the total available soil water for uptake by transpiration
         *     - the total available soil water weighted by soil properties
         *     - the fine root density sum needed for further weighing.
         */
        double uptakeAvaSum( 0.0);
        double rootdensitySum( 0.0);
        for ( size_t sl = 0;  sl < m_soillayers->soil_layer_cnt();  ++sl)
        {
            // fine root condition to consider only soil water volumes with fine roots
            if ( cbm::flt_greater_zero( m_veg->mfrt_sl( sl)) &&
                 cbm::flt_greater( wc_.wc_sl[sl], sc_.wcmin_sl[sl]))
            {
                // water availability  [m]
                double const water_avail( (wc_.wc_sl[sl] - sc_.wcmin_sl[sl]) * sc_.h_sl[sl]);
                // maximum water availabiliy (field capacity-wilting point)  [m]
                double const water_ref( (sc_.wcmax_sl[sl] - sc_.wcmin_sl[sl]) * sc_.h_sl[sl]);
                // fraction of max uptake that can be used according to soil conditions  [%]
                double const limit_wa( get_clay_limitation( sl, water_avail, water_ref));

                double const uptake_avail( limit_wa * water_avail);
                double const rootdensity( m_veg->mfrt_sl( sl) / sc_.h_sl[sl]);

                uptakeAvaSum += uptake_avail;
                rootdensitySum += rootdensity;
            }
            // without roots no uptake (and there are no roots in deeper layers because roots are not wireless ;-)
            else if ( ! cbm::flt_greater_zero( m_veg->mfrt_sl( sl)))
            { break; }
        }

        // water uptake is limited to available water
        double const trans_target( cbm::bound_max(hr_potTrans, uptakeAvaSum));

        // transpiration demand that could not be supplied by soil is put into a deficit term
        wc_.plant_waterdeficit = cbm::bound_min(0.0, wc_.plant_waterdeficit + hr_potTrans - uptakeAvaSum);

        if ( cbm::flt_greater_zero( uptakeAvaSum * rootdensitySum))
        {
            /*!
             * @page watercycledndc
             * 2) Determine the weighted water uptake capacity:
             * \f[ c = \frac{rootdensity_l}{\sum\limits_l rootdensity} \cdot \frac{H2O available for uptake_l}{\sum\limits_l H2O available for uptake} \f]
             * and its sum over all layers.
             */
            double compositeSum( 0.0);
            for (size_t sl = 0; sl < m_soillayers->soil_layer_cnt(); ++sl)
            {
                // fine root condition to prevent transpiration without uptake organs
                if ( cbm::flt_greater_zero( m_veg->mfrt_sl( sl)) &&
                     cbm::flt_greater( wc_.wc_sl[sl], sc_.wcmin_sl[sl]))
                {
                    double const water_avail( (wc_.wc_sl[sl] - sc_.wcmin_sl[sl]) * sc_.h_sl[sl]);
                    double const water_ref( (sc_.wcmax_sl[sl] - sc_.wcmin_sl[sl]) * sc_.h_sl[sl]);
                    double const limit_wa( get_clay_limitation( sl, water_avail, water_ref));
                    
                    double const uptake_avail( limit_wa * water_avail);
                    double const rootdensity( m_veg->mfrt_sl( sl) / sc_.h_sl[sl]);

                    double const ratio_uptake_avail( uptake_avail / uptakeAvaSum);
                    double const ratio_rootdensity( rootdensity / rootdensitySum);

                    double const composite( ratio_rootdensity * ratio_uptake_avail);
                    compositeSum += composite;
                }
                // without roots no uptake (and there are no roots in deeper layers because roots are not wireless ;-)
                else if ( ! cbm::flt_greater_zero( m_veg->mfrt_sl( sl)))
                { break; }
            }

            if ( !cbm::flt_greater_equal_zero( compositeSum))
            {
                KLOGERROR("Transpiration calculation not successful. Negatively scaled fine root biomass");
                return  LDNDC_ERR_FAIL;
            }

            /*!
             * @page watercycledndc
             * 3) Calculate actual uptake:
             * \f[ up_l = \min( \frac{c}{\sum\limits_l c} * pottrans , H2O available for uptake_l) \f]
             * The soil water reservoirs are decreased accordingly.
             * The routine iterates from top to bottom and stops as soon as the water demand is fullfilled
             * (usually water is taken up from all rooted layers if available).
             */
            double hr_trans(0.0);
            double hr_deficit(0.0);
            for ( size_t sl = 0;  sl < m_soillayers->soil_layer_cnt();  ++sl)
            {
                // fine root condition to prevent transpiration without uptake organs
                if ( cbm::flt_greater_zero( m_veg->mfrt_sl( sl)) &&
                     cbm::flt_greater( wc_.wc_sl[sl], sc_.wcmin_sl[sl]))
                {
                    double const water_avail( (wc_.wc_sl[sl] - sc_.wcmin_sl[sl]) * sc_.h_sl[sl]);
                    double const water_ref( (sc_.wcmax_sl[sl] - sc_.wcmin_sl[sl]) * sc_.h_sl[sl]);
                    double const limit_wa( get_clay_limitation( sl, water_avail, water_ref));
                    
                    double const uptake_avail( limit_wa * water_avail);
                    double const rootdensity( m_veg->mfrt_sl( sl) / sc_.h_sl[sl]);
                    
                    double const ratio_uptake_avail( uptake_avail / uptakeAvaSum);
                    double const ratio_rootdensity( rootdensity / rootdensitySum);

                    double const composite( ratio_rootdensity * ratio_uptake_avail);

                    // water uptake to fullfill evaporative demand (limited by available water)
                    double uptake_target( trans_target * (composite / compositeSum));
                    double const uptake_layer( cbm::bound_min(0.0, std::min(uptake_target, uptake_avail)));

                    wc_.accumulated_transpiration_sl[sl] += uptake_layer;
                    hr_trans += uptake_layer;
                    wc_.wc_sl[sl] -= uptake_layer / sc_.h_sl[sl];
                    
                    // plant water deficit decreases (if waterdeficit exists and some water is available)
                    if (cbm::flt_greater_zero( wc_.plant_waterdeficit) && cbm::flt_greater_zero(uptake_avail))
                    {
                        // uptake limitation by conductance
                        double const uptake_max = cwp_avg * cbm::MH2O * cbm::KG_IN_G * lai * cbm::SEC_IN_HR * hours_per_time_step() * cbm::M_IN_MM;

                        // uptake limitation by demand per layer
                        uptake_target = std::min(uptake_max, wc_.plant_waterdeficit) * (composite / compositeSum);
                        
                        // uptake limitation by availability
                        double const uptake_avail_new = cbm::bound_min(0.0, uptake_avail - uptake_layer);

                        // actual uptake
                        double const uptake_deficit = std::min(uptake_target, uptake_avail_new);

                        hr_deficit += uptake_deficit;
                        wc_.wc_sl[sl] -= uptake_deficit / sc_.h_sl[sl];
                        m_wc.plant_waterdeficit_compensation += uptake_deficit;
                    }

                    // stop if water demand for transpiration is fulfilled
                    if ( cbm::flt_greater_equal( hr_trans, trans_target))
                    { break; }
                }
                // without roots no uptake (and there are no roots in deeper layers because roots are not wireless ;-)
                else if ( ! cbm::flt_greater_zero( m_veg->mfrt_sl( sl)))
                { break; }
            }

            hr_pot_evapotrans = cbm::bound_min( 0.0, hr_pot_evapotrans - hr_trans);
            wc_.plant_waterdeficit = cbm::bound_min( 0.0, wc_.plant_waterdeficit - hr_deficit);
        }
    }
    return  LDNDC_ERR_OK;
}




/*!
 * @page watercycledndc
 * @subsection watercycledndc_transpiration_couvreur Transpiration after Couvreur
 * 
 * This function is called if ROOT_WATER_UPTAKE_COUVREUR = true (\b false) and model configuration transpiration = true.
 *
 * It updates the soil water content regarding the root water uptake for transpiration.
 * The root water uptake is modeled by the Couvreur model
 * (first derived in 3d @cite couvreur:2012a, 1d in @cite couvreur:2014a, in the notation in Ref. @cite cai:2018a for every plant species individually.
 * The potential transpiration is split up for different species by the mass fractions.
 * As species parameters KRSINIT, KCOMPINIT, FRTMASSINIT, and HLEAFCRIT = \b -16000cm (constant by stomatal regulation) enter.
 *
 * An effective soil water pressure head (\f$ h<0 \f$ for unsaturated soil, [L][cm]) is determined by
 * \f[
 * h_{eff} = \sum\limits_{sl=0}^{deepest rooted layer} h(sl) \cdot frcfrts(sl)
 * \f]
 * The water potentials are determined via the van-Genuchten parametrisation.
 *
 * The hydraulic conductivites of the root system \f$K_{rs}\f$ [m/m/h] and for compensation \f$K_{comp}\f$ [m/m/h]
 * scale linearly with the root biomass in respect to the initial root biomass FRTMASSINIT.
 * The parameters for initialisation relate to the same plant stage/time.
 * \f[
 * K_{rs} = KRSINIT \cdot m_{frts} / FRTMASSINIT\\
 * K_{comp} = KCOMPINIT \cdot m_{frts} / FRTMASSINIT\, .
 * \f]
 * They can be understood as the volumetric fraction of water, which is extracted in an hour.
 *
 * The root water uptake, as the intensive volumentric water fraction [m/m/timestep], from every layer is calculated by
 * \f[
 * S(sl) = \left( K_{rs} \cdot (h_{eff} - h_{leaf} ) + K_{comp} \cdot (h(sl) - h_{eff}) \right) \cdot \frac{massfrts(sl)/layerwidth}{massallfrts}\, .
 * \f]
 * In contrast to Cai 2017, we therefore use the fine root mass density fractions instead of the normalised root length density.
 *
 * The potentially used water for transpiration (\f$T_{pot}\f$) is provided by another module (physiology).
 * The pressure head of the leaves is derived from the potential transpiration, as long as this value remains above
 * the critical leaf water pressure head HLEAFCRIT.
 * \f[
 * \tilde h_{leaf} = h_{eff} - \frac{T_{Pot}}{K_{rs}}
 * \f]
 *
 * a) If \f$\tilde h_{leaf} > HLEAFCRIT\f$, it is \f$h_{leaf} = \tilde h_{leaf}\f$, then \f$T_{act} = T_{pot} \f$,
 *
 * b) otherwise \f$ h_{leaf} = HLEAFCRIT\f$, then \f$ T_{act} < T_{pot} \f$.
 *
 * The values wcmin and wcmax are not used with this root water uptake model, but the water potentials related to the Van-Genuchten
 * parameters.
 * In contrast, in PlaMox, for the drought stress factor, which enters photosynthesis, wcmin and wcmax are used.
 * Therefore, if the hydraulic conductivity of the root system is unrealisitcally small, or if the Van-Genuchten
 * parameters are not chosen in accordance to wcmin and wcmax, the actual transpiration can be smaller than the potential
 * transpiration without the photosynthesis being affected from this.
 * The Van-Genuchten parameters and wcmin, wcmax should be initialised consistently in the site file.
 */
lerr_t
WatercycleDNDC::CalcTranspirationCouvreur( double const &hr_potTransinm)
{
	// hourly potential transpiration
	double const hr_potTrans( hr_potTransinm * cbm::CM_IN_M);
	// hourly transpiration
	double hr_trans( 0.0);

	// Calculate the root water uptake and hence the transpiration for every plant individually
	for( PlantIterator vt = m_veg->begin(); vt != m_veg->end(); ++vt)
	{
		// split up the potential transpiration equally to the various plants
		// frt mass of the current species
		double const mfrt_species( (*vt)->mFrt);
		// total frt mass
		double const mfrt_sum( m_veg->dw_frt());
		double const hr_potTransspecies = hr_potTrans * mfrt_species/mfrt_sum;	// in cm

		// If some soil layers are frozen and contain only ice, no fluid water is present and the water potential diverges.
		// The uptake from these layers is prevented by considering only roots in layers with more water than ice.
		// In this case the root distribution needs to be rescaled (otherwise the non-contributing layers would effectively contribute a potential = 0).
		double fFrt_unfrozen( 1.0);

		/* TODO: include icetowatercrit as proper parameter */
		double icetowatercrit( 0.1);
		double effsoilwatpot( 0.0);
		for( size_t sl = 0;  sl < m_soillayers->soil_layer_cnt();  ++sl)
		{
			// fine root conditions to prevent transpiration without uptake organs, wateruptake from frozen ground
			if( cbm::flt_greater_zero( (*vt)->fFrt_sl[sl] * mfrt_species))
			{
				if(cbm::flt_less( wc_.ice_sl[sl]/wc_.wc_sl[sl], icetowatercrit))
				{
					// overall water potential in this layer
					double const watpotl( Soillayerwaterhead( sl));	// negative
					effsoilwatpot += watpotl * (*vt)->fFrt_sl[sl];	// negative
				}
				else
				{
					fFrt_unfrozen -= (*vt)->fFrt_sl[sl];
				}
			}
			else
			{
				// roots are not wireless ;-)
				break;
			}
		}
		// normalize the root distribution in the potential, if some soil is frozen
		effsoilwatpot = effsoilwatpot / fFrt_unfrozen;

		// hydraulic conductivities of plant system
		double const Krs(vt->KRSINIT() * mfrt_species / vt->FRTMASSINIT() * nhr);		// cm(water)/cm(from pressure)/timestep
		double const Kcomp(vt->KCOMPINIT() * mfrt_species / vt->FRTMASSINIT() * nhr);	// cm(water)/cm(from pressure)/timestep

		// water potential in the leafs, depending on the potential transpiration
		double const leafwatpot( cbm::bound_min(vt->HLEAFCRIT(), effsoilwatpot - (hr_potTransspecies / Krs)));		// cm

		if( cbm::flt_greater_zero( leafwatpot))
		{
			KLOGERROR( "leafwatpot is positive");
            return  LDNDC_ERR_FAIL;
		}
		if( !cbm::flt_greater_zero( effsoilwatpot - leafwatpot))
		{
			KLOGERROR( "effsoilwatpot - leafwatpot is smaller 0: ", effsoilwatpot - leafwatpot, ", effsoilwatpot = ", effsoilwatpot, ", leafwatpot = ", leafwatpot, "\n-> Hourly transpiration ", hr_trans, " is negative!\nPlant dies :-( Try with a smaller KRSINIT or different KCOMPINIT or different EXP_ROOT_DISTRIBUTION.\nOr wcmin is to close to the residual water content.. increase it.");
            return  LDNDC_ERR_FAIL;
		}

		double const effuptake( Krs * (effsoilwatpot - leafwatpot));		// cm/timestep
		double uptake_tot( 0.0);
		double uptakecomp_tot( 0.0);
//		double uptakecompabs_tot( 0.0);

		for( size_t sl = 0;  sl < m_soillayers->soil_layer_cnt();  ++sl)
		{
			// fine root condition to prevent transpiration without uptake organs
			if( cbm::flt_greater_zero( (*vt)->fFrt_sl[sl] * mfrt_species))
			{
				if(cbm::flt_less( wc_.ice_sl[sl]/wc_.wc_sl[sl], icetowatercrit))
				{
					double const watpotl( Soillayerwaterhead( sl));
					double const compuptake( Kcomp * (watpotl - effsoilwatpot));
					double uptake_layer( (effuptake + compuptake) * (*vt)->fFrt_sl[sl]);	// absolute value in cm

                    wc_.accumulated_transpiration_sl[sl] += uptake_layer * cbm::M_IN_CM;
					uptake_tot += uptake_layer;												// absolute value in cm
					uptakecomp_tot += compuptake * (*vt)->fFrt_sl[sl];						// absolute value in cm
//					uptakecompabs_tot += fabs(compuptake * (*vt)->fFrt_sl[sl]);				// absolute value in cm

					wc_.wc_sl[sl] -= uptake_layer * cbm::M_IN_CM /sc_.h_sl[sl];		// fraction of water in this layer, in m

					if( !cbm::flt_greater_zero( wc_.wc_sl[sl]))
					{
						KLOGERROR( "In soil layer ", sl, " the water content is negative: ", wc_.wc_sl[sl]);
                        return  LDNDC_ERR_FAIL;
					}
				}
			}
			else
			{
				// roots are not wireless ;-)
				break;
			}
		}
		hr_trans += uptake_tot * cbm::M_IN_CM;							// absolute value in m for all species


		if( cbm::flt_greater_zero( fabs(uptakecomp_tot))){
			KLOGWARN( "Compensatory water uptake ", uptakecomp_tot, " > 0.");
		}
		if( cbm::flt_greater( uptake_tot, hr_potTransspecies, 0.0000000001))
		{
			KLOGERROR( "Total water uptake ", uptake_tot, " larger than potential transpiration ", hr_potTransspecies);
		}
		if(cbm::flt_equal_eps(fFrt_unfrozen, 1.0, 0.0000000001) && !cbm::flt_equal_eps( uptake_tot, hr_potTransspecies, 0.0000000001) && !cbm::flt_equal_eps( leafwatpot, vt->HLEAFCRIT(), 0.0000000001))
		{
			KLOGERROR( "T ", uptake_tot, "cm < Tpot ", hr_potTransspecies, "cm even though leafwatpot ", leafwatpot, " > HLEAFCRIT", vt->HLEAFCRIT());
		}
	}
	hr_pot_evapotrans -= hr_trans;
    
    return  LDNDC_ERR_OK;
}


/*!
 * @page watercycledndc
 * @section watercycledndc_evaporation Evaporation
 * @subsection watercycledndc_evaporation_surface_water Evaporation from surface water
 * 
 * If the potential evapotranspiration (from \link watercycledndc-evapotranspiration CalcPotEvapoTranspiration() \endlink
 * and reduced by evaporation from intercepted water, transpiration and evaporation from snow) is not yet zero,
 * and if there is water remaining at the surface,
 * water from this remaining surface water evaporates.
 *
 * If the potential evapotranspiration is larger than the surface water, all the surface water evaporates.
 *
 * If the amount of surface water is larger than the potential evapotranspiration, the potential evapotranspiration is
 * reached as the actual evapotranspiration and some surface water still remains.
 */
void
WatercycleDNDC::CalcSurfaceWaterEvaporation()
{
    if ( cbm::flt_greater_zero( wc_.surface_water) &&
         cbm::flt_greater_zero( hr_pot_evapotrans))
    {
        if ( hr_pot_evapotrans > wc_.surface_water)
        {
            wc_.accumulated_surfacewaterevaporation += wc_.surface_water;
            hr_pot_evapotrans -= wc_.surface_water;
            wc_.surface_water = 0.0;
        }
        else
        {
            wc_.surface_water -= hr_pot_evapotrans;
            wc_.accumulated_surfacewaterevaporation += hr_pot_evapotrans;
            hr_pot_evapotrans = 0.0;
        }
    }
}


/*!
 * @page watercycledndc
 * @subsection watercycledndc_evaporation_snow Snow evaporation
 * 
 * Snow decrease from evaporation (original in unit: evaporation_of_snow)
 * Since potential evaporation is 0 at freezing temperatures, snow
 * evaporation occurs only for snow that can not melt fast enough
 * If there is more evaporation than can be expected with the sum of gross potential evaporation, hourly foliage
 * evaporation and hourly transpiration, the remaining evaporation is assumes to occur from the snow surface.
 */
void WatercycleDNDC::CalcSnowEvaporation()
{
    //Average limit of snowEvaporation is 0.00013m day-1 (Granger and Male 1978)
    double hr_potESnow( cbm::bound(0.0, hr_pot_evapotrans, 0.00013 * nhr/24.0));

    if ( cbm::flt_greater_zero( wc_.surface_ice) &&
         cbm::flt_greater_zero( hr_potESnow))
    {
        //If there is a snowpack and there is a potential to evaporate
        if ( wc_.surface_ice > hr_potESnow)
        {
            //Partial evaporation from the snow
            hr_pot_evapotrans = cbm::bound_min( 0.0, hr_pot_evapotrans - hr_potESnow);
            wc_.accumulated_surfacewaterevaporation += hr_potESnow;
            wc_.surface_ice -= hr_potESnow;
        }
        else
        {
            //Total evaporation of the snowpack
            hr_pot_evapotrans = cbm::bound_min( 0.0, hr_pot_evapotrans - wc_.surface_ice);;
            wc_.accumulated_surfacewaterevaporation += wc_.surface_ice;
            wc_.surface_ice = 0.0;
        }
    }
}


/*!
 * @page watercycledndc
 * @subsection watercycledndc_soilwaterevaporation Soil water evaporation
 * Water from the soil evaporates
 * - down to \f$ EVALIM \f$
 * - as long as the water content is above wilting point
 * \f[
 * \theta(sl) = WCDNDC\_EVALIM\_FRAC\_WCMIN \cdot \theta_{wp}(sl)
 * \f]
 * - potential evapotranspiration is larger zero
 *
 * Amount of water evaporating from a soil layer \f$ sl \f$ :
 * \f[
 * \frac{dE}{dT} = \text{bound}\left(0,
 * \frac{potevapotrans}{\text{EVALIM}} \cdot limitwi \cdot \max\left( 0,
 * 1-\frac{\min(\text{EVALIM}, depth(sl))}{\text{EVALIM}} \right),
 * \theta(sl) \cdot limitwi \right)
 * \f]
 * - \f$ potevapotrans\f$ : water for potential evapotranspiration
 * - limiting factor for soil water infiltration \f$ limitwi \f$:
 * \f[
 * limitwi = \begin{cases}
 * &0, \qquad\qquad\qquad\qquad\qquad \theta(sl) < wcminevap(sl)\\
 * & \min\left(\left(\frac{wc(sl)-wcminevap(sl)}{wcmax(sl)}\right)^{clayfact}, 1\right), \qquad  \text{else}
 * \end{cases}
 * \f]
 * - factor including further soil properties
 * \f[
 * clayfact = 1+ SLOPE\_CLAYF \cdot \min\left(1- forg(sl), clay(sl)\right)
 * \f]
 * - \f$forg(sl)\f$: fraction of organic matter
 *
 * Comments:
 * - The water available for evapotranspiration is distributed homogeneously over all soil layers from which water
 * potentially evaporates (down to EVALIM).
 * The amount of water actually evaporating from every layer is further reduced linearly with the layer depth.
 * - Soil water evaporation for every layer takes place after the downwards percolation from that layer.
 */
void
WatercycleDNDC::SoilWaterEvaporation(
                                     size_t _sl)
{
    //percolation and direct evaporation from the (upper) soil
    //if there is snow cover, evaporation from soil will be set to 0, and only sublimation is allowed.
    double const orgf( sc_.fcorg_sl[_sl] / cbm::CCORG);
    double const clay_fact( 1.0 + m_param->SLOPE_CLAYF() * std::min(1.0 - orgf, sc_.clay_sl[_sl]));
    double const wc_min_evaporation( m_param->WCDNDC_EVALIM_FRAC_WCMIN() * sc_.wcmin_sl[_sl]);

    if ( ((sc_.depth_sl[_sl] - sc_.h_sl[_sl]) < m_param->EVALIM()) &&
        (wc_.wc_sl[_sl] > wc_min_evaporation) &&
        cbm::flt_greater_zero( hr_pot_evapotrans))
    {
        //limiting factor for soil water infiltration
        double limit_wi( cbm::bound_min( 0.0,
                                         (wc_.wc_sl[_sl] - wc_min_evaporation) / sc_.wcmax_sl[_sl]));
        limit_wi = cbm::bound_max( pow(limit_wi, clay_fact), 1.0);

        double const hrest( std::min( sc_.h_sl[_sl], m_param->EVALIM() - ( sc_.depth_sl[_sl] - sc_.h_sl[_sl])));

        //soil water evaporation from the upper soil layers due to physical evaporation (not transpiration)
        //linear decrease, consistent with variable soil thickness
        double const layer_Evapo = cbm::bound(0.0,
                                                hr_pot_evapotrans * hrest / m_param->EVALIM()
                                                * std::max(0.0,
                                                           1.0 - std::min((double)m_param->EVALIM(),
                                                                          sc_.depth_sl[_sl] - ( 0.5 * sc_.h_sl[_sl]))
                                                           / m_param->EVALIM())
                                                * limit_wi,
                                                limit_wi * wc_.wc_sl[_sl]*sc_.h_sl[_sl]);

        //update of the current soil layer water content
        wc_.wc_sl[_sl] -= (layer_Evapo / sc_.h_sl[_sl]);
        wc_.accumulated_soilevaporation += layer_Evapo;
        hr_pot_evapotrans = cbm::bound_min( 0.0, hr_pot_evapotrans - layer_Evapo);
    }
}


/*!
 * @page watercycledndc
 * @section watercycledndc_percolation Soil water flow
 *
 * \subsection watercycledndc_infiltration Infiltration
 *
 * For the top soil layer, water inflow equals total surface water.
 * For deeper soil layers, water inflow equals water outflow of the overlying soil layer.
 *
 * \subsection watercycledndc_gw_ineraction Groundwater interaction
 * For soil layers lying within the groundwater table, water flow out of the layer equals
 * water inflow. In this way, soil water is discharged with groundwater with the percolation
 * flow rate of the soil layer above the groundwater table.
 *
 * \subsection watercycledndc_wf_at_bottom Water flow at bottom
 * If the last soil layer is above the groundwater, water flow out of the domain is given by:
 * 1. Fully saturated conditions \n
 *    Saturated hydraulic conductivity
 * 2. Water content below saturation and above field capacity (\f$\phi > \theta > \theta_{fc} \f$)  \n
 *    Water flow is calculated in the same way as for all other soil layers (see below)
 * 3. Water content between field capacity and wilting point (\f$\theta_{fc} > \theta > \theta_{wp} \f$)  \n
 *    Water flow out of the layer is calculated depending on the flow into the layer:
 *    \f$ q_{out} = FPERCOL \cdot q_{in} \f$
 * 4. Water content below wilting point (\f$\theta_{wp} > \theta\f$) \n
 *    No flow \n
 *
 * \subsection watercycledndc_wf_within_domain Water flow within domain
 * Calculation of water flow ouf of soil layer is distinguished for different water contents:
 * 1. Fully saturated conditions \n
 *    Saturated hydraulic conductivity
 * 2. Water content below saturation and above field capacity (\f$\phi > \theta > \theta_{fc} \f$)  \n
 *    See @ref watercycledndc_water_flow_above_fc
 * 3. Water content between field capacity and wilting point (\f$\theta_{fc} > \theta > \theta_{wp} \f$)  \n
 *    See @ref watercycledndc_water_flow_below_fc
 * 4. Water content below wilting point (\f$\theta_{wp} > \theta\f$) \n
 *    No flow \n
 */
void
WatercycleDNDC::CalcSoilEvapoPercolation()
{
    /* Reset water fluxes */
    for ( size_t sl = 0;  sl < m_soillayers->soil_layer_cnt();  ++sl)
    {
        m_wc.percolation_sl[sl] = 0.0;
    }

    /* Fill groundwater layer with water */
    for ( int sl = m_soillayers->soil_layer_cnt()-1;  sl >= 0; --sl)
    {
        if ( cbm::flt_greater_equal( sc_.depth_sl[sl], ground_water_table))
        {
            /* ensure that ice volume is less than porosity */
            double const ice_vol( wc_.ice_sl[sl] / cbm::DICE);
            if ( cbm::flt_greater( sc_.poro_sl[sl], ice_vol))
            {
                //air filled porosity is filled with groundwater
                double const gw_access( cbm::bound_min( 0.0,
                                                        (sc_.poro_sl[sl] - ice_vol - wc_.wc_sl[sl]) * sc_.h_sl[sl]));
                wc_.wc_sl[sl] += gw_access / sc_.h_sl[sl];
                wc_.accumulated_groundwater_access += gw_access;
            }
        }
        else
        {
            break;
        }
    }

    for ( size_t sl = 0;  sl < m_soillayers->soil_layer_cnt();  ++sl)
    {
        if ( sl == 0)
        {
            wc_.accumulated_infiltration += wc_.surface_water;
            wc_.wc_sl[0] += wc_.surface_water / sc_.h_sl[0];
            wc_.surface_water = 0.0;
        }
        else
        {
            wc_.wc_sl[sl] += m_wc.percolation_sl[sl-1] / sc_.h_sl[sl];
        }

        if ( cbm::flt_greater( sc_.depth_sl[sl], ground_water_table))
        {
            //set constant flux for all ground water layers depending on last non-groundwater layer
            if ( sl > 0)
            {
                m_wc.percolation_sl[sl] = m_wc.percolation_sl[sl-1];
            }

            //in case of groundwater until soil surface first outflux is estimated by saturated water flow
            else
            {
                m_wc.percolation_sl[sl] = WaterFlowSaturated( sl, get_impedance_factor( sl));
            }
        }
        else if ( sl + 1 == m_soillayers->soil_layer_cnt())
        {
            m_wc.percolation_sl[sl] = 0.0;
            if ( cbm::flt_greater_zero( sc_.sks_sl[sl]))
            {
                if ( cbm::flt_greater( wc_.wc_sl[sl], sc_.poro_sl[sl]) &&
                     cbm::flt_greater( wc_.wc_sl[sl], sc_.wcmax_sl[sl])) //security, should be always true if first condition is met
                {
                    //maximum allowed flow until field capacity
                    double const max_flow( (wc_.wc_sl[sl] - sc_.wcmax_sl[sl]) * sc_.h_sl[sl]);
                    m_wc.percolation_sl[sl] = cbm::bound_max( WaterFlowSaturated( sl, get_impedance_factor( sl)),
                                                             max_flow);
                }

                //water flux above field capacity
                else if ( cbm::flt_greater_equal( wc_.wc_sl[sl] + wc_.ice_sl[sl], sc_.wcmax_sl[sl]))
                {
                    m_wc.percolation_sl[sl] = WaterFlowAboveFieldCapacity( sl, get_impedance_factor( sl));
                }

                //water flux below field capacity and above wilting point
                else if (   cbm::flt_greater_zero( m_wc.percolation_sl[sl-1])
                         && cbm::flt_greater( wc_.wc_sl[sl], sc_.wcmin_sl[sl]))
                {
                    m_wc.percolation_sl[sl] = m_param->FPERCOL() * m_wc.percolation_sl[sl-1];
                }

                //bound water flux by defined maximal percolation rate (e.g., during flooding events)
                m_wc.percolation_sl[sl] = cbm::bound_max( m_wc.percolation_sl[sl], max_percolation);
            }
        }

        //infiltrating water in current time step is exceeding available pore space of last time step
        //second condition: security, should be always true if first condition is met
        else if ( cbm::flt_greater( wc_.wc_sl[sl], sc_.poro_sl[sl]) &&
                  cbm::flt_greater( wc_.wc_sl[sl], sc_.wcmax_sl[sl]))
        {
            //maximum allowed flow until field capacity
            double const max_flow( (wc_.wc_sl[sl] - sc_.wcmax_sl[sl]) * sc_.h_sl[sl]);
            m_wc.percolation_sl[sl] = cbm::bound_max( WaterFlowSaturated( sl, get_impedance_factor( sl)),
                                                      max_flow);
        }
        else if ( cbm::flt_greater_equal( wc_.wc_sl[sl] + wc_.ice_sl[sl], sc_.wcmax_sl[sl]))
        {
            m_wc.percolation_sl[sl] = WaterFlowAboveFieldCapacity( sl, get_impedance_factor( sl));
        }
        else if ( cbm::flt_greater( wc_.wc_sl[sl], sc_.wcmin_sl[sl]))
        {
            m_wc.percolation_sl[sl] = WaterFlowCapillaryRise( sl, get_impedance_factor( sl));
        }
        else
        {
            m_wc.percolation_sl[sl] = 0.0;
        }


        // New water content must be greater equal wilting point
        if ( cbm::flt_less( (wc_.wc_sl[sl] + wc_.ice_sl[sl]) * sc_.h_sl[sl] - m_wc.percolation_sl[sl],
                             sc_.wcmin_sl[sl] * sc_.h_sl[sl]) &&
             cbm::flt_greater_zero( m_wc.percolation_sl[sl]))
        {
            m_wc.percolation_sl[sl] = cbm::bound( 0.0,
                                                  (wc_.wc_sl[sl] + wc_.ice_sl[sl] - sc_.wcmin_sl[sl]) * sc_.h_sl[sl],
                                                  m_wc.percolation_sl[sl]);
        }

        // Update of the current soil layer water content
        wc_.wc_sl[sl] -= m_wc.percolation_sl[sl] / sc_.h_sl[sl];

        SoilWaterEvaporation( sl);
    }

    /* Correct water content and percolation if water content is above porosity
     * If the layer below cannot take up all of the water assigned for percolation,
     * so if the air filled pore space is not sufficient,
     * the water remains in the upper layer.
     */
    for ( size_t sl = m_soillayers->soil_layer_cnt()-1;  sl > 0; sl--)
    {
        double const ice_vol( wc_.ice_sl[sl] / cbm::DICE);
        if ( cbm::flt_greater( wc_.wc_sl[sl] + ice_vol,
                               sc_.poro_sl[sl]))
        {
            double delta_wc( wc_.wc_sl[sl] + ice_vol - sc_.poro_sl[sl]);
            if ( cbm::flt_greater_equal( ice_vol, sc_.poro_sl[sl]))
            {
                delta_wc = wc_.wc_sl[sl];
                wc_.wc_sl[sl] = 0.0;
            }
            else
            {
                wc_.wc_sl[sl] = sc_.poro_sl[sl] - ice_vol;
            }
            m_wc.percolation_sl[sl-1] -= delta_wc * sc_.h_sl[sl];
            wc_.wc_sl[sl-1] += delta_wc * sc_.h_sl[sl] / sc_.h_sl[sl-1];
        }
    }

    double const ice_vol( wc_.ice_sl[0] / cbm::DICE);
    if ( cbm::flt_greater( wc_.wc_sl[0] + ice_vol,
                           sc_.poro_sl[0]))
    {

        double delta_wc( wc_.wc_sl[0] + ice_vol - sc_.poro_sl[0]);
        if ( cbm::flt_greater_equal( ice_vol, sc_.poro_sl[0]))
        {
            delta_wc = wc_.wc_sl[0];
            wc_.wc_sl[0] = 0.0;
        }
        else
        {
            wc_.wc_sl[0] = sc_.poro_sl[0] - ice_vol;
        }
        wc_.accumulated_infiltration -= delta_wc * sc_.h_sl[0];
        wc_.surface_water += delta_wc * sc_.h_sl[0];
    }


    CalcSurfaceFlux();

    wc_.accumulated_waterflux_sl += m_wc.percolation_sl;
    wc_.accumulated_percolation += m_wc.percolation_sl[ m_soillayers->soil_layer_cnt()-1];
}


/*!
 * @page watercycledndc
 * @subsubsection watercycledndc_water_flow_above_fc Water flow above field capacity
 *
 * Water flow from layer \f$sl\f$ to the layer \f$sl+1\f$ below:
 * \f[
 * q = \left(1-\frac{\theta_{fc}(sl)}{\theta(sl)}\right)^2 \cdot \theta(sl) \cdot \left(1 - e^{\frac{1}{log(sks)}} \right) \cdot factimpedence
 * \f]
 */
double
WatercycleDNDC::WaterFlowAboveFieldCapacity(
                                   size_t _sl,
                                   double const &_fact_impedance)
{
    double const sks( WaterFlowSaturated( _sl, _fact_impedance));
    double const fsl( get_fsl( _sl));
    double const fhr( get_time_step_duration());

    /*! factor accounting for different water travelling characteristics in litter and mineral soil*/
    double slope(( _sl < m_soillayers->soil_layers_in_litter_cnt()) ? m_param->SLOPE_FF() : m_param->SLOPE_MS());

    //max 0.005 means that no sks values up to 9.88 cm min-1 are allowed !!
    double const travelTime( std::max( 0.005, 1.0 - log10( sc_.sks_sl[_sl] * cbm::M_IN_CM * cbm::MIN_IN_DAY * fhr)));
    return( cbm::bound_max( cbm::sqr(1.0 - (sc_.wcmax_sl[_sl] / ((wc_.wc_sl[_sl] + wc_.ice_sl[_sl]) * slope)))
                            * 0.5 * wc_.wc_sl[_sl] * sc_.h_sl[_sl] * fsl * (1.0 - exp(-1.0 / travelTime)) * _fact_impedance,
                            sks));
}


/*!
 * @page watercycledndc
 * @subsubsection watercycledndc_water_flow_below_fc Water flow below field capacity
 * Water flow from layer \f$sl\f$ to the layer \f$sl+1\f$ below:
 * \f[
 * q = \left(\frac{\theta(sl)}{\theta_{fc}(sl)} - \frac{\theta(sl+1)}{\theta_{fc}(sl+1)} \right) \cdot \theta(sl) \cdot \left(1 - e^{\frac{1}{log(sks)}} \right) \cdot factimpedence
 * \f]
 */
double
WatercycleDNDC::WaterFlowBelowFieldCapacity(
                                     size_t _sl,
                                     double const &_fact_impedance)
{
    double const fsl( get_fsl( _sl));
    double const fhr( get_time_step_duration());

    //max 0.005 means that no sks values larger than (?) 9.88 cm min-1 are allowed !!
    double const travelTime( std::max( 0.005, 1.0 - log10( sc_.sks_sl[_sl] * cbm::M_IN_CM * cbm::MIN_IN_DAY * fhr)));
    return( ( wc_.wc_sl[_sl] / sc_.wcmax_sl[_sl] - wc_.wc_sl[_sl+1] / sc_.wcmax_sl[_sl+1])
           * wc_.wc_sl[_sl] * sc_.h_sl[_sl] * fsl * (1.0 - exp(-1.0 / travelTime)) * _fact_impedance);
}


/*!
 * @page watercycledndc
 * @subsubsection watercycledndc_water_flow_capillary_rise Capillary rise
 * Similar to @WaterFlowBelowFieldCapacity but restricts water flow by
 * water availability from soil layer below
 */
double
WatercycleDNDC::WaterFlowCapillaryRise(
                                       size_t _sl,
                                       double const &_fact_impedance)
{
    double const water_flow( WaterFlowBelowFieldCapacity( _sl, _fact_impedance));

    if ( !cbm::flt_greater_equal_zero( water_flow))
    {
        if ( m_param->WCDNDC_HAVE_CAPILLARY_ACTION())
        {
            return std::min( -water_flow, wc_.wc_sl[_sl+1] * sc_.h_sl[_sl+1]) * -1.0;
        }
        else
        {
            return 0.0;
        }
    }
    else
    {
        return water_flow;
    }
}


/*!
 * @brief
 * @subsubsection watercycledndc_water_flow_saturated Saturated water flow
 * Returns saturated water flow per time step accounting for impedance
 */
double
WatercycleDNDC::WaterFlowSaturated( size_t _sl,
                                    double const &_fact_impedance)
{
    double const sks( sc_.sks_sl[_sl] * cbm::M_IN_CM * cbm::MIN_IN_DAY * get_time_step_duration() * _fact_impedance);
    return sks;
}


/*!
 * @page watercycledndc
 * @subsubsection watercycledndc_preferential_flow Preferential flow
 * ...
 */
void
WatercycleDNDC::WatercycleDNDC_preferential_flow()
{
    //BY_PASSF is defined as the fraction of surface water that passes per hour (0.0 by default)
    if ( cbm::flt_greater_zero( m_param->BY_PASSF()) &&
         cbm::flt_greater_zero( wc_.surface_water))
    {

        double water_def_total( 0.0);
        for ( size_t sl = 0;  sl < m_soillayers->soil_layer_cnt();  ++sl)
        {
            //water flux that flow through macro pores can maximally store
            water_def_total += cbm::bound_min( 0.0, (sc_.wcmax_sl[sl] - wc_.wc_sl[sl] - wc_.ice_sl[sl]) * sc_.h_sl[sl]);
        }

        //duration of timestep (hr-1)
        double const fhr( get_time_step_duration());


        //fraction of surface water that is put into bypass flow (m timestep-1)
        double const bypass_fraction( cbm::bound_max( m_param->BY_PASSF() * fhr, 0.99));
        double const bypass_water( wc_.surface_water * bypass_fraction);
        wc_.surface_water -= bypass_water;
        wc_.accumulated_infiltration += bypass_water;

        if ( cbm::flt_greater_equal( water_def_total, bypass_water))
        {

            for ( size_t sl = 0;  sl < m_soillayers->soil_layer_cnt();  ++sl)
            {
                double const water_def( cbm::bound_min( 0.0, (sc_.wcmax_sl[sl] - wc_.wc_sl[sl] - wc_.ice_sl[sl]) * sc_.h_sl[sl]));
                double const water_add( water_def / water_def_total * bypass_water);

                wc_.wc_sl[sl] += water_add / sc_.h_sl[sl];
            }
        }
        else
        {
            for ( size_t sl = 0;  sl < m_soillayers->soil_layer_cnt();  ++sl)
            {
                double const water_def( cbm::bound_min( 0.0, (sc_.wcmax_sl[sl] - wc_.wc_sl[sl] - wc_.ice_sl[sl]) * sc_.h_sl[sl]));
                wc_.wc_sl[sl] += water_def / sc_.h_sl[sl];
            }
            /* add surplus to percolation out of last soil layer */
            wc_.accumulated_percolation += bypass_water - water_def_total;
            wc_.accumulated_waterflux_sl[m_soillayers->soil_layer_cnt()-1] += bypass_water - water_def_total;
        }
    }
}


/*!
 * @page watercycledndc
 * @subsubsection watercycledndc_impedance Impedance
 * Percolating water is imepded by an impedance factor,
 * which considers the formation of ice lenses in (partially) frozen soil
 * [Lundin (1990), Hansson et al. (2004).]
 * The impedence factor depends on the parameter IMPEDANCE_PAR = \b 0.
 * - If no ice is present the impedence factor is 1 and no reduction occurs.
 * - If IMPEDANCE_PAR = \b 0 the impedence factor is 1 as well and no reduction occurs.
 *
 */
double
WatercycleDNDC::get_impedance_factor( size_t _sl)
{
    if ( cbm::flt_greater_zero( wc_.ice_sl[_sl]))
    {
        double const ice_fraction( wc_.ice_sl[_sl] / (wc_.wc_sl[_sl] + wc_.ice_sl[_sl]));
        return std::pow( 10.0, -m_param->IMPEDANCE_PAR() * ice_fraction);
    }
    else
    {
        return 1.0;
    }
}


/*!
 * @brief
 * Returns factor accounting for soil layer depth.
 * Originally, a constant soil layer depth of 0.02 m has been assumed. Due to flexible layer depth,
 * an additional factor has been introduced that accounts for a higher resistance of larger layers.
 */
double
WatercycleDNDC::get_fsl( size_t _sl)
{
    return( m_param->PSL_WC() / std::max( 0.01, sc_.h_sl[_sl]));
}


/*!
 * @page watercycledndc
 * @section watercycledndc_runoff Surface runoff
 * The surface flux gives the water, which runs off horizontally.
 * This only happens if more water than stopped by the bunding (bund height is set in flooding events)
 * is present.
 *
 * - If the bund height > 0:
 *  All water above the bund height runs off immediately.
 *
 * - If the bund height == 0:
 *
 *    The fraction of water running off is determined by the site parameter FRUNOFF.
 *    - If a time step is 24h (daily simulation), FRUNOFF corresponds to the fraction of water running off
 *     in these 24h. Hence, FRUNOFF = 1 corresponds to a complete run off, i.e. 100\% of the
 *     surface water
 *    - If a time step is 1h (hourky simulation), only the fraction FRUNOFF/24 runs off in this time step,
 *     the rest of the water remains as surface water for the next time step.
 *     For an hourly time step a complete run off corresponds to FRUNOFF = 24.
 *     A run off of 50% would be FRUNOFF=12.
 */
void
WatercycleDNDC::CalcSurfaceFlux()
{
    if ( cbm::flt_greater_zero( bund_height))
    {
        if ( cbm::flt_greater( wc_.surface_water, bund_height))
        {
            wc_.accumulated_runoff += (wc_.surface_water - bund_height);
            wc_.surface_water = bund_height;
        }
    }
    else
    {
        /* runoff fraction per time step equals RO fraction per hour * length of a time step */
        double const runoff_fraction( m_param->FRUNOFF() * get_time_step_duration());

        // if frunoff_ts < 1
        if ( cbm::flt_less( runoff_fraction, 1.0))
        {
            double const runoff( runoff_fraction * wc_.surface_water);
            wc_.surface_water -= runoff;
            wc_.accumulated_runoff += runoff;
        }
        else
        {
            wc_.accumulated_runoff += wc_.surface_water;
            wc_.surface_water = 0.0;
        }
    }
}


/*!
 * @page waterlibs
 * @section watercycledndc_soillayerwaterhead Layered water pressure head
 * Returns the water pressure head [cm] of a specific soil layer: matrix \f$h_{m}\f$ + gravitational/elevation \f$h_{g}\f$.
 * The result is given in cm.
 *
 * - \f$ h_{m} \f$ is determined by the van Genuchten equation of the water retention curve for the soil WITHOUT stones.
 *
 * - \f$h_{g} = layer depth\f$, [cm], (only relevant for moist soils close to field capacity)
 *
 */
double
WatercycleDNDC::Soillayerwaterhead( size_t _sl)
{
    // a) matrix water head
    double transformwcforfinesoil( 1./(1. - sc_.stone_frac_sl[_sl]));
    // 1. Saturated water content and residual water content
    double wcs_sl( sc_.poro_sl[_sl] * transformwcforfinesoil * sc_.wfps_max_sl[_sl]);
    double wcr_sl( sc_.poro_sl[_sl] * transformwcforfinesoil * sc_.wfps_min_sl[_sl]);

    // with parameters from texture, soc, bd
    // why *100? 1/cm -> 1/m
    double matrixpressurehead( ldndc::hydrology::capillary_pressure( wc_.wc_sl[_sl]*transformwcforfinesoil, sc_.vga_sl[_sl] * 100.0, sc_.vgn_sl[_sl], sc_.vgm_sl[_sl], wcs_sl, wcr_sl) * 100.);    // in hPa
//    (hPa) or (cm water column)
//    double matrixpressurehead( ldndc::hydrology::capillary_pressure( wc_.wc_sl[_sl] * transformwcforfinesoil,
//                                                                     sc_.vga_sl[_sl], sc_.vgn_sl[_sl], sc_.vgm_sl[_sl],
//                                                                     wcs_sl, wcr_sl) * cbm::CM_IN_M);
    
    matrixpressurehead = - matrixpressurehead/0.980665;    // transfer from hPa into cm

    // b) grativational water head
    // take lower layer boundaries
    double const layerdepth = sc_.depth_sl[_sl] * cbm::CM_IN_M;        // in cm
    double gravitationalhead( - layerdepth);

    return matrixpressurehead + gravitationalhead;
}


/*!
 * @brief
 * Time fraction with regard to daily rates
 */
double
WatercycleDNDC::get_time_step_duration()
{
    return (nhr / 24.0);
}


/*!
 * @brief
 * Balance check of water
 */
double
WatercycleDNDC::balance_check( double *  _balance)
{
    double  balance = get_leaf_water() + wc_.surface_ice + wc_.surface_water + m_wc.plant_waterdeficit_compensation;
    for ( size_t sl = 0; sl < m_soillayers->soil_layer_cnt(); ++sl)
    {
        balance += (wc_.wc_sl[sl] + wc_.ice_sl[sl]) * sc_.h_sl[sl];
    }
    balance += (   - m_wc.accumulated_irrigation_event_executed
                   - wc_.accumulated_irrigation_automatic
                   - wc_.accumulated_irrigation_ggcmi
                   - wc_.accumulated_irrigation_reservoir_withdrawal
                   - wc_.accumulated_precipitation
                   - wc_.accumulated_groundwater_access
                   + wc_.accumulated_percolation
                   + wc_.accumulated_runoff
                   + wc_.accumulated_transpiration_sl.sum()
                   + wc_.accumulated_interceptionevaporation
                   + wc_.accumulated_soilevaporation
                   + wc_.accumulated_surfacewaterevaporation);

    if ( _balance)
    {
        double const  balance_delta( std::abs( *_balance - balance));
        if ( balance_delta > cbm::DIFFMAX)
        {
            KLOGWARN( "Water leakage:  difference is ", balance - *_balance);
        }
    }

    for ( size_t sl = 0;  sl < m_soillayers->soil_layer_cnt();  ++sl)
    {
        if ( cbm::flt_less( wc_.wc_sl[sl], 0.0))
        {
            KLOGWARN( "Negative water content of: ", wc_.wc_sl[sl], " in layer: ", sl, ". Water content set to zero.");
            wc_.wc_sl[sl] = 0.0;
        }
        if ( cbm::flt_less( wc_.ice_sl[sl], 0.0))
        {
            KLOGWARN( "Negative ice content of: ", wc_.ice_sl[sl], " in layer: ", sl, ". Ice content set to zero.");
            wc_.ice_sl[sl] = 0.0;
        }
    }

    return  balance;
}


size_t
WatercycleDNDC::hours_per_time_step()
{
    return nhr * nwc;
}

} /*namespace ldndc*/
