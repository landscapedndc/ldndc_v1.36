
#ifndef  LM_WATERCYCLE_DNDC_H_
#define  LM_WATERCYCLE_DNDC_H_

#include  "mbe_legacymodel.h"
#include  "state/mbe_state.h"
#include  "ld_shared.h"

#include  "ld_eventqueue.h"
#include  "eventhandler/flood/flood.h"

#include  "watercycle/ld_icesnow.h"

namespace ldndc {

/*!
 * @brief
 *  Watercycle model WatercycleDNDC
 */
class  LDNDC_API  WatercycleDNDC  :  public  MBE_LegacyModel
{
    LMOD_EXPORT_MODULE_INFO(WatercycleDNDC,"watercycle:watercycledndc","WatercycleDNDC");

public:
    /*! number of iteration steps within a day */
    static const unsigned int  IMAX_W;

public:
    WatercycleDNDC( MoBiLE_State *,
            cbm::io_kcomm_t *, timemode_e);

    ~WatercycleDNDC();

    lerr_t  configure( ldndc::config_file_t const *);
    lerr_t  register_ports( cbm::io_kcomm_t *);
    lerr_t  initialize();

    lerr_t  solve();

    lerr_t  unregister_ports( cbm::io_kcomm_t *);
    lerr_t  finalize();

    lerr_t  sleep();
    lerr_t  wake();

private:
    cbm::io_kcomm_t *  m_iokcomm;

    input_class_groundwater_t const *  m_groundwater;
    input_class_climate_t const *  m_climate;
    input_class_setup_t const *  m_setup;
    input_class_siteparameters_t const *  m_param;
    input_class_soillayers_t const *  m_soillayers;

    struct cfg_t
    {
        bool  snowpack:1;
        bool  icecontent:1;

        cbm::string_t evapotranspiration_method;
        double  automaticirrigation;    // automatically triggers irrigation in order to avoid drought stress
        bool ggcmi_irrigation;          //at the start of each day magically reset water level to field capacity (if below)
    };
    struct cfg_t  m_cfg;

    struct StateMicroclimate
    {
        /*in*/
        double  air_temperature_below_canopy;
        double  precipitation;
        double  nd_windspeed;
        /*out*/
        double  rainfall;
        /*in/out*/
        CBM_Vector<double>  soil_temperature_sl;
    };
    StateMicroclimate  m_mc;

    double  potentialtranspiration;
    double  accumulated_potentialtranspiration_old;

    struct StateWatercycle
    {
        double  snowfall;
        double  snow_melt;

        double  accumulated_irrigation_event_executed;
        double  irrigation;

        double plant_waterdeficit_compensation;

        CBM_Vector<double>  percolation_sl;
    };
    StateWatercycle  m_wc;

    MoBiLE_PlantVegetation *  m_veg;

    substate_soilchemistry_t &  sc_;
    substate_watercycle_t &  wc_;
    substate_microclimate_t &  mc_;

private:

    lerr_t  receive_state();
    lerr_t  send_state();

    WaterCycleSnowDNDC  m_snowdndc;
    lerr_t  CalcSnowPack();
    WaterCycleSnowDNDC::IceContentStateIn  m_icecontent_in;
    WaterCycleSnowDNDC::IceContentStateOut  m_icecontent_out;
    lerr_t  CalcIceContent();

    EventHandlerFlood  m_eventflood;

    SubscribedEvent<LD_EventHandlerQueue>  m_ManureEvents;
    SubscribedEvent<LD_EventHandlerQueue>  m_IrrigationEvents;

private:
    size_t nwc;                         // number of loops per time step
    double nhr;                         // number of hours per internal loop

    /* water fluxes */
    double  hr_pot_evapotrans;           // hourly transpiration [m]
    double  plant_waterdeficit_compensation;
    /* flooding and irrigation event */
    double  water_table_flooding;        // water table set during flooding events
    double  bund_height;                 // bund height set during flooding events
    double  irrigation_height;           // bund height set during flooding events
    double  max_percolation;             // maximum water percolation rate during flooding event
    bool    unlimited_water;             // 
    double  saturation_level;            //
    double  soil_depth_flooding;         //
    
    double  rainfall_intensity();
    double  ground_water_table;

    /********************/
    /* member functions */
    /********************/

    size_t hours_per_time_step();

    /*!
     * @brief
     *      considers water input due to irrigation and/or liquid manure evetns
     */
    lerr_t event_irrigate();

    /*!
     * @brief
     *      sets hydrologic conditions during flooding events, e.g.,
     *      - surface water table
     *      - bund height
     */
    lerr_t event_flood();

    /*!
     * @brief
     *      checks each time step for correct water balance
     */
    double balance_check( double * /*old balance (in=NULL, out!=NULL)*/);

    /*!
     * @brief
     *      apportion daily rainfall to subdaily rainfall
     */
    void CalcRaining( unsigned int);

    /*!
     * @brief
     *      apportion daily irrigation to subdaily irrigation
     */
    void CalcIrrigation( unsigned int);

    /*!
     * @brief
     *      Potential evaporation
     *
     *      @n
     *      there are two methods of calculating potential evaporation
     *      (choose via macro POTENTIAL_EVAPORATION above)
     *      @li
     *              [0] Procedure according to Thornthwaite 1948 with
     *              modifications by Camargo et al. 1999 and Peireira
     *              and Pruitt 2004 (implemented from Pereira and
     *              Pruitt 2004) to better account for dry environments
     *              and daylength.
     *              @n
     *              NOTE:
     *              - monthly temperatures are assumed to be equal to
     *                daily average temperature of the middle of the
     *                month, calculated from continous equations equal
     *                to those used in the weather generator.
     *
     *              - the model uses always daily average temperature
     *              as input even if it is run in sub-daily mode
     *
     *              - the original PnET-N-DNDC code which contains
     *              modification that could not be found in the
     *              literature has been outcommented.
     *
     *      @li
     *              [1] uses the Priestley Taylor equation (Priestly
     *              and Taylor 1972) to calculate daily and hourly
     *              potential evapotranspiration (implemented from internet)
     *              vps calculation according to Allen et al. 1998,
     *              cit. in Cai et al. 2007
     */
    lerr_t  CalcPotEvapoTranspiration();

    /*!
     * @brief
     *      heat index for potential evaporation using approach after Thornthwaite
     */
    double  thornthwaite_heat_index;

    /*!
     * @brief
     *      Calculates water quantity that is retained on leaf and
     *      stem bark surfaces
     *
     *      @li
     *              Rainfall and interception are assumed to happen
     *              at the end of the timestep and evaporation is
     *              realised at once at the start of the timestep.
     *
     *      @li
     *              The model assumes a homogeneous horizontal
     *              distribution of LAI although a non-linear dependency
     *              to crown cover (relative foliage clumping) has
     *              been reported (e.g. Teklehaimanot et al. 1991).
     *
     *      @li
     *              The default water-interception for foliage almost
     *              equal to the largest species specific value
     */
    void  CalcInterception();

    /*!
     * @param[in] None
     * @param[out] None
     * @return Leaf water
     */
    double get_leaf_water();

    /*!
     * @brief
     *      Reduces water fluxes out of a layer if ice lenses have formed
     */
    double get_impedance_factor( size_t /* soil layer */);

    /*!
     * @param[in] _sl Soil layer
     * @param[in] _water_avail Available water content
     * @param[in] _water_ref Reference water content
     * @param[out] None
     * @return Clay limitation
     */
    double get_clay_limitation( size_t, double, double);

    /*!
     * @param[in] None
     * @param[out] None
     * @return None
     */
    lerr_t CalcTranspiration();

    /*!
     * @param[in] None
     * @param[out] None
     * @return None
     */
    lerr_t CalcTranspirationCouvreur( double const &hr_potTransinm /* potential transpiration */);

    /*!
     * @param[in] None
     * @param[out] None
     * @return None
     */
    void CalcSnowEvaporation();

    /*!
     * @param[in] None
     * @param[out] None
     * @return None
     */
    void CalcSurfaceFlux();

    /*!
     * @param[in] None
     * @param[out] None
     * @return None
     */
    void WatercycleDNDC_preferential_flow();

    /*!
     * @param[in] None
     * @param[out] None
     * @return None
     */
    void CalcSoilEvapoPercolation();

    /*!
     * @param[in] _sl Soil layer
     * @param[out] None
     * @return None
     */
    void SoilWaterEvaporation( size_t);

    /*!
     * @param[in] None
     * @param[out] None
     * @return None
     */
    void CalcSurfaceWaterEvaporation();

    /*!
     * @brief
     *
     */
    double WaterFlowBelowFieldCapacity( size_t /* layer */,
            double const & /* impedance factor */);

    /*!
     * @brief
     *      Similar to WaterFlowBelowFieldCapacity(...) but restricts water flow by
     *      water availability from soil layer below
     */
    double WaterFlowCapillaryRise( size_t /* layer */,
                                  double const & /* impedance factor */);

    /*!
     * @brief
     *
     */
    double WaterFlowAboveFieldCapacity( size_t /* layer */,
                                        double const & /* impedance factor */);

    /*!
     * @brief
     *
     */
    double WaterFlowSaturated( size_t /* layer */,
                               double const & /* impedance factor */);

    /*!
     * @brief
     *
     */
    double WaterFlowSaturated( size_t /* layer */);

    /*!
     * @brief
     *
     */
    double Soillayerwaterhead( size_t /* soil layer */);

    /*!
     * @brief
     *
     */
    double get_fsl( size_t /* soil layer */);

    /*!
     * @brief
     *
     */
    double get_time_step_duration();
};
} /*namespace ldndc*/

#endif  /*  !LM_WATERCYCLE_DNDC_H_  */
