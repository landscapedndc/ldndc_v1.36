/*!
 * @author
 *    steffen klatt (created on: dec 18, 2013)
 */

#include  "test/species-echo.h"

#include  <stdio.h>

LMOD_MODULE_INFO(SpeciesEcho,TMODE_DEFAULTS,LMOD_FLAG_USER);

ldndc::SpeciesEcho::SpeciesEcho( MoBiLE_State *  _state,
        cbm::io_kcomm_t *, timemode_e  _timemode)
        : MBE_LegacyModel( _state, _timemode),
          m_veg( &_state->vegetation), m_printempty( true)
    { }

lerr_t
ldndc::SpeciesEcho::configure( ldndc::config_file_t const *)
{
    this->m_printempty = false;
    return  LDNDC_ERR_OK;
}

lerr_t
ldndc::SpeciesEcho::read()
{
    if ( this->m_veg->size() == 0)
    {
        if ( this->m_printempty)
            { printf( "no species\n"); }
        return  LDNDC_ERR_OK;
    }
    printf( "%s\t", LD_RtCfg.now_as_iso8601().c_str());
    for ( PlantIterator  p = this->m_veg->begin(); p != this->m_veg->end(); ++p)
        { printf( "%10s  ", (*p)->cname()); }
    printf( "\n");

    return  LDNDC_ERR_OK;
}

