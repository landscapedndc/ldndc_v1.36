/*!
 * @brief
 *    dump air chemistry data for current time step
 *
 * @author
 *    steffen klatt
 */

#ifndef  LM_AIRCHEMISTRYECHO_H_
#define  LM_AIRCHEMISTRYECHO_H_

#include  "mbe_legacymodel.h"
#include  "state/mbe_state.h"

namespace ldndc {
class  LDNDC_API  AirchemistryEcho  :  public  MBE_LegacyModel
{
    LMOD_EXPORT_MODULE_INFO(AirchemistryEcho,"airchemistry:echo","Airchemistry Echo");
    enum  target_e
    {
        TGT_INPUT,
        TGT_STATE
    };
    public:
        AirchemistryEcho( MoBiLE_State *,
                cbm::io_kcomm_t *, timemode_e);

        lerr_t  configure( ldndc::config_file_t const *);
        lerr_t  initialize();
        lerr_t  solve();
        lerr_t  finalize();

        lerr_t  sleep() { return  LDNDC_ERR_OK; }
        lerr_t  wake() { return  LDNDC_ERR_OK; }

    private:
        airchemistry::input_class_airchemistry_t const *  m_ad;
        substate_airchemistry_t const *  m_ac;

        std::ostream  ostr_;

        target_e  target_;
        bool  have_header_;
};
} /*namespace ldndc*/

#endif  /*  !LM_AIRCHEMISTRYECHO_H_  */

