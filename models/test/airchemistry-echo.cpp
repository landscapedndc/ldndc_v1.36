/*!
 * @brief
 *    ...
 *
 * @author
 *    steffen klatt
 */

#include  "test/airchemistry-echo.h"

#include  <input/airchemistry/airchemistry.h>

#include  <cfgfile/cbm_cfgfile.h>
#include  <constants/cbm_const.h>

#include  <iostream>

LMOD_MODULE_INFO(AirchemistryEcho,TMODE_DEFAULTS,LMOD_FLAG_USER);
namespace ldndc {

AirchemistryEcho::AirchemistryEcho( MoBiLE_State *  _state,
        cbm::io_kcomm_t *  _iokcomm, timemode_e  _timemode)
        : MBE_LegacyModel( _state, _timemode),

          m_ad( _iokcomm->get_input_class< airchemistry::input_class_airchemistry_t >()),
          m_ac( _state->get_substate< substate_airchemistry_t >()),
          ostr_( std::cout.rdbuf()),

          target_( TGT_INPUT),
          have_header_( false)
{
}

lerr_t
AirchemistryEcho::configure(
        ldndc::config_file_t const *  _cf)
{
    std::string  target( "input");
    lerr_t  q_rc = _cf->query( info().id, "target", &target);
    if (( q_rc != LDNDC_ERR_ATTRIBUTE_NOT_FOUND) && ( q_rc != LDNDC_ERR_OK))
    {
        return  LDNDC_ERR_FAIL;
    }

    if ( cbm::is_equal( target, "state"))
    {
        KLOGINFO( "dumping from state air chemistry buffer");
        target_ = TGT_STATE;
    }
    else if ( cbm::is_equal( target, "input"))
    {
        KLOGINFO( "dumping from input air chemistry buffer");
        target_ = TGT_INPUT;
    }
    else
    {
        KLOGERROR( "unknown dump target");
        return  LDNDC_ERR_FAIL;
    }

    /* query if header was requested */
    if ( _cf->query( info().id, "header", &have_header_) == LDNDC_ERR_FAIL)
    {
        return  LDNDC_ERR_FAIL;
    }


    return  LDNDC_ERR_OK;
}


lerr_t
AirchemistryEcho::initialize()
{
    //std::string  ostr_name( std::string( "./cdump/climate_") + cbm::n2s( cl_.id()) + ".txt");
    //ostr_.open( ostr_name.c_str(), std::ios_base::out|std::ios_base::trunc);

    //ostr_.rdbuf( std::cout.rdbuf());

    if ( have_header_)
    {
        /* write header */
        ostr_    << "*"
                
        << "\t" << airchemistry::RECORD_ITEM_NAMES[airchemistry::record::RECORD_ITEM_CH4]
        << "\t" << airchemistry::RECORD_ITEM_NAMES[airchemistry::record::RECORD_ITEM_CO2]
        << "\t" << airchemistry::RECORD_ITEM_NAMES[airchemistry::record::RECORD_ITEM_O2]
        << "\t" << airchemistry::RECORD_ITEM_NAMES[airchemistry::record::RECORD_ITEM_O3]
        << "\t" << airchemistry::RECORD_ITEM_NAMES[airchemistry::record::RECORD_ITEM_NH3]
        << "\t" << airchemistry::RECORD_ITEM_NAMES[airchemistry::record::RECORD_ITEM_NH4]
        << "\t" << airchemistry::RECORD_ITEM_NAMES[airchemistry::record::RECORD_ITEM_NH4_DRY]
        << "\t" << airchemistry::RECORD_ITEM_NAMES[airchemistry::record::RECORD_ITEM_NO]
        << "\t" << airchemistry::RECORD_ITEM_NAMES[airchemistry::record::RECORD_ITEM_NO2]
        << "\t" << airchemistry::RECORD_ITEM_NAMES[airchemistry::record::RECORD_ITEM_NO3]
        << "\t" << airchemistry::RECORD_ITEM_NAMES[airchemistry::record::RECORD_ITEM_NO3_DRY]

        << std::endl;
    }

    return  LDNDC_ERR_OK;
}


lerr_t
AirchemistryEcho::solve()
{
    cbm::sclock_t const &  clk( *LD_RtCfg.clk);

    if ( target_ == TGT_INPUT)
    {
        if ( timemode() & TMODE_SUBDAILY)
        {
            ostr_   << clk.now().as_iso8601()
                << "\t" << m_ad->conc_ch4_subday( clk)
                << "\t" << m_ad->conc_co2_subday( clk)
                << "\t" << m_ad->conc_o2_subday( clk)
                << "\t" << m_ad->conc_o3_subday( clk)
                << "\t" << m_ad->conc_nh3_subday( clk)
                << "\t" << m_ad->conc_nh4_subday( clk)
                << "\t" << m_ad->conc_nh4dry_subday( clk)
                << "\t" << m_ad->conc_no_subday( clk)
                << "\t" << m_ad->conc_no2_subday( clk)
                << "\t" << m_ad->conc_no3_subday( clk)
                << "\t" << m_ad->conc_no3dry_subday( clk);
        }
        else
        {
            ostr_   << clk.now().as_iso8601()
                << "\t" << m_ad->conc_ch4_day( clk)
                << "\t" << m_ad->conc_co2_day( clk)
                << "\t" << m_ad->conc_o2_day( clk)
                << "\t" << m_ad->conc_o3_day( clk)
                << "\t" << m_ad->conc_nh3_day( clk)
                << "\t" << m_ad->conc_nh4_day( clk)
                << "\t" << m_ad->conc_nh4dry_day( clk)
                << "\t" << m_ad->conc_no_day( clk)
                << "\t" << m_ad->conc_no2_day( clk)
                << "\t" << m_ad->conc_no3_day( clk)
                << "\t" << m_ad->conc_no3dry_day( clk);
        }
        this->ostr_  << std::endl;
    }
    else if ( target_ == TGT_STATE)
    {
        if ( timemode() & TMODE_SUBDAILY)
        {
            ostr_   << LD_RtCfg.now_as_iso8601()
                << "\t" << m_ac->ts_ch4_concentration
                << "\t" << m_ac->ts_co2_concentration
                << "\t" << m_ac->ts_o2_concentration
                << "\t" << m_ac->ts_o3_concentration
                << "\t" << m_ac->ts_nh3_concentration
                << "\t" << "*"
                << "\t" << "*"
                << "\t" << m_ac->ts_no_concentration
                << "\t" << m_ac->ts_no2_concentration
                << "\t" << "*"
                << "\t" << "*";
        }
        else
        {
            ostr_   << LD_RtCfg.now_as_iso8601()
                << "\t" << m_ac->nd_ch4_concentration
                << "\t" << m_ac->nd_co2_concentration
                << "\t" << m_ac->nd_o2_concentration
                << "\t" << m_ac->nd_o3_concentration
                << "\t" << m_ac->nd_nh3_concentration
                << "\t" << m_ac->nd_nh4_wet_deposition
                << "\t" << m_ac->nd_nh4_dry_deposition
                << "\t" << m_ac->nd_no_concentration
                << "\t" << m_ac->nd_no2_concentration
                << "\t" << m_ac->nd_no3_wet_deposition
                << "\t" << m_ac->nd_no3_dry_deposition;
        }
        this->ostr_  << std::endl;
    }
    else
    {
        KLOGFATAL( "how did you get here...?!");
    }

    return  LDNDC_ERR_OK;
}

lerr_t
AirchemistryEcho::finalize()
{
    //ostr_.close();

    return  LDNDC_ERR_OK;
}

} /*namespace ldndc*/

