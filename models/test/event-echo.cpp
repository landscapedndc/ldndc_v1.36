/*!
 * @brief
 *    testing module that lists(dumps) event information
 *    for events lined up for execution at time step
 *
 * @author
 *    steffen klatt (created on: dec 10, 2011)
 */

#include  "test/event-echo.h"

#include  <input/event/event.h>
#include  <input/event/events.h>
#include  <input/species/species.h>

#include  <logging/cbm_logging.h>
#include  <cfgfile/cbm_cfgfile.h>

#include  <stdio.h>

LMOD_MODULE_INFO(EventEcho,TMODE_DEFAULTS,LMOD_FLAG_USER);
namespace ldndc {
/* configuration options for this module */
#define  EVLIST_CONF_OPT_PRINT_EMPTY  "print_empty"    /* bool */
#define  EVLIST_CONF_OPT_PRINT_EMPTY_DFLT  true
#define  EVLIST_CONF_OPT_FORMAT_LONG  "format_long"    /* bool */
#define  EVLIST_CONF_OPT_FORMAT_LONG_DFLT  false


EventEcho::EventEcho( MoBiLE_State *  _state,
    cbm::io_kcomm_t *  _iokcomm, timemode_e  _timemode)
        : MBE_LegacyModel( _state, _timemode),

          m_events( _iokcomm->get_input_class< event::input_class_event_t >()),
          m_printempty( true), m_formatlong( false)
    { }


EventEcho::~EventEcho()
    { }

lerr_t
EventEcho::configure(
        ldndc::config_file_t const *  _cf)
{
    CF_LMOD_QUERY(_cf,EVLIST_CONF_OPT_PRINT_EMPTY,m_printempty,EVLIST_CONF_OPT_PRINT_EMPTY_DFLT);
    CF_LMOD_QUERY(_cf,EVLIST_CONF_OPT_FORMAT_LONG,m_formatlong,EVLIST_CONF_OPT_FORMAT_LONG_DFLT);
    return  LDNDC_ERR_OK;
}

lerr_t
EventEcho::initialize()
{
    return  LDNDC_ERR_OK;
}

lerr_t
EventEcho::solve()
{
    if ( !this->m_events)
        { return  LDNDC_ERR_OK; }

    event::EventsView  events = this->m_events->events_view();
    if ( events.size() == 0)
    {
        if ( m_printempty)
        {
            printf( "t=%14s: ", this->lclock()->now().as_iso8601().c_str());
            printf( "no events\n");
        }
    }
    else
    {
        printf( "t=%14s: ", this->lclock()->now().as_iso8601().c_str());
        printf( "events (%u)\n", (unsigned int)events.size());
    }

    int k = 0;
    for ( event::EventsView::const_iterator  e_k = events.begin();
                    e_k != events.end();  ++e_k)
    {
        event::Event const *  event = &(*e_k);

        printf( "event %2u: %s[%lu]\n", k++,
            event->name(), (long unsigned int)event->id());
        if ( m_formatlong)
            { printf( "%s\n", event->to_string().c_str()); }
    }

    return  LDNDC_ERR_OK;
}

lerr_t
EventEcho::finalize()
{
    return  LDNDC_ERR_OK;
}


lerr_t
EventEcho::sleep()
{
    return  LDNDC_ERR_OK;
}
lerr_t
EventEcho::wake()
{
    return  LDNDC_ERR_OK;
}

} /*namespace ldndc*/

