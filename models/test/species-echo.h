/*!
 * @brief
 *    echo names of currently living species to stdout
 *
 * @author
 *    steffen klatt (created on: dec 18, 2013)
 */

#ifndef  LM_SPECIESECHO_H_
#define  LM_SPECIESECHO_H_

#include  "mbe_legacymodel.h"
#include  "state/mbe_state.h"

namespace ldndc {
class  LDNDC_API  SpeciesEcho  :  public  MBE_LegacyModel
{
    LMOD_EXPORT_MODULE_INFO(SpeciesEcho,"species:echo","Species Echo");
    public:
        SpeciesEcho( MoBiLE_State *,
                cbm::io_kcomm_t *, timemode_e);

        lerr_t  configure( ldndc::config_file_t const *);
        lerr_t  read();

        lerr_t  initialize() { return LDNDC_ERR_OK; }
        lerr_t  wake() { return LDNDC_ERR_OK; }
        lerr_t  sleep() { return LDNDC_ERR_OK; }
        lerr_t  finalize() { return LDNDC_ERR_OK; }

    private:
        MoBiLE_PlantVegetation *  m_veg;
        bool  m_printempty;
};
} /* namespace ldndc */

#endif  /*  !LM_SPECIESECHO_H_  */

