/*!
 * @brief
 *    ...
 *
 * @author
 *    steffen klatt
 */

#include  "test/climate-echo.h"

#include  <input/climate/climate.h>
#include  <constants/cbm_const.h>
#include  <cfgfile/cbm_cfgfile.h>

#include  <iostream>

LMOD_MODULE_INFO(ClimateEcho,TMODE_SUBDAILY|TMODE_POST_DAILY,LMOD_FLAG_USER);
namespace ldndc {

ClimateEcho::ClimateEcho( MoBiLE_State *  _state,
        cbm::io_kcomm_t *  _iokcomm, timemode_e  _timemode)
        : MBE_LegacyModel( _state, _timemode),

          m_cl( _iokcomm->get_input_class< climate::input_class_climate_t >()),
          m_mc( _state->get_substate< substate_microclimate_t >()),
          ostr_( std::cout.rdbuf()),

          target_( TGT_INPUT),
          have_header_( false)
{
}

lerr_t
ClimateEcho::configure(
        ldndc::config_file_t const *  _cf)
{
    std::string  target( "input");
    lerr_t  q_rc = _cf->query( info().id, "target", &target);
    if (( q_rc != LDNDC_ERR_ATTRIBUTE_NOT_FOUND) && ( q_rc != LDNDC_ERR_OK))
    {
        return  LDNDC_ERR_FAIL;
    }

    if ( cbm::is_equal( target, "state"))
    {
        KLOGINFO( "dumping from state climate buffer  [time=",ldndc::time::timemode_name( this->timemode()),"]");
        target_ = TGT_STATE;
    }
    else if ( cbm::is_equal( target, "input"))
    {
        KLOGINFO( "dumping from input climate buffer  [time=",ldndc::time::timemode_name( this->timemode()),"]");
        target_ = TGT_INPUT;
    }
    else
    {
        KLOGERROR( "unknown dump target");
        return  LDNDC_ERR_FAIL;
    }

    /* query if header was requested */
    if ( _cf->query( info().id, "header", &have_header_) == LDNDC_ERR_FAIL)
    {
        return  LDNDC_ERR_FAIL;
    }

           return  LDNDC_ERR_OK;
}

lerr_t
ClimateEcho::initialize()
{
    if ( have_header_)
    {
        /* write header */
        ostr_    << "*"

        << "\t" << climate::RECORD_ITEM_NAMES[climate::record::RECORD_ITEM_PRECIP]
        << "\t" << climate::RECORD_ITEM_NAMES[climate::record::RECORD_ITEM_TEMP_MIN]
        << "\t" << climate::RECORD_ITEM_NAMES[climate::record::RECORD_ITEM_TEMP_AVG]
        << "\t" << climate::RECORD_ITEM_NAMES[climate::record::RECORD_ITEM_TEMP_MAX]
        << "\t" << climate::RECORD_ITEM_NAMES[climate::record::RECORD_ITEM_REL_HUMUDITY]
        << "\t" << climate::RECORD_ITEM_NAMES[climate::record::RECORD_ITEM_VPD]
        << "\t" << climate::RECORD_ITEM_NAMES[climate::record::RECORD_ITEM_WIND_SPEED]
        << "\t" << climate::RECORD_ITEM_NAMES[climate::record::RECORD_ITEM_EX_RAD]
        << "\t" << climate::RECORD_ITEM_NAMES[climate::record::RECORD_ITEM_LONGWAVE_RAD]
        << "\t" << climate::RECORD_ITEM_NAMES[climate::record::RECORD_ITEM_AIR_PRESSURE]

        << std::endl;
    }

    return  LDNDC_ERR_OK;
}


lerr_t
ClimateEcho::solve()
{
    if ( target_ == TGT_INPUT)
    {
        cbm::sclock_t const &  clk( *LD_RtCfg.clk);

        if ( this->timemode() & TMODE_SUBDAILY)
        {
            ostr_   << LD_RtCfg.now_as_iso8601()
                << "\t" << m_cl->precip_subday( clk)
                << "\t" << m_cl->temp_min_subday( clk)
                << "\t" << m_cl->temp_avg_subday( clk)
                << "\t" << m_cl->temp_max_subday( clk)
                << "\t" << m_cl->rel_humidity_subday( clk)
                << "\t" << m_cl->vpd_subday( clk)
                << "\t" << m_cl->wind_speed_subday( clk)
                << "\t" << m_cl->ex_rad_subday( clk)
                << "\t" << m_cl->long_rad_subday( clk)
                << "\t" << m_cl->air_pressure_subday( clk);
        }
        else
        {
            ostr_   << clk.now().as_iso8601()
                << "\t" << m_cl->precip_day( clk)
                << "\t" << m_cl->temp_min_day( clk)
                << "\t" << m_cl->temp_avg_day( clk)
                << "\t" << m_cl->temp_max_day( clk)
                << "\t" << m_cl->rel_humidity_day( clk)
                << "\t" << m_cl->vpd_day( clk)
                << "\t" << m_cl->wind_speed_day( clk)
                << "\t" << m_cl->ex_rad_day( clk)
                << "\t" << m_cl->long_rad_day( clk)
                << "\t" << m_cl->air_pressure_day( clk);

        }
        this->ostr_  << std::endl;
    }
    else if ( target_ == TGT_STATE)
    {
        if ( timemode() & TMODE_SUBDAILY)
        {
            ostr_   << LD_RtCfg.now_as_iso8601()
                << "\t" << m_mc->ts_precipitation*cbm::MM_IN_M
                << "\t" << m_mc->ts_minimumairtemperature
                << "\t" << m_mc->ts_airtemperature
                << "\t" << m_mc->ts_maximumairtemperature
                << "\t" << m_mc->ts_relativehumidity
                << "\t" << m_mc->ts_watervaporsaturationdeficit
                << "\t" << m_mc->ts_windspeed
                << "\t" << m_mc->shortwaveradiation_in
                << "\t" << m_mc->longwaveradiation_in
                << "\t" << m_mc->ts_airpressure;
        }
        else
        {
            ostr_   << LD_RtCfg.now_as_iso8601()
                << "\t" << m_mc->nd_precipitation*cbm::MM_IN_M
                << "\t" << m_mc->nd_minimumairtemperature
                << "\t" << m_mc->nd_airtemperature
                << "\t" << m_mc->nd_maximumairtemperature
                << "\t" << m_mc->nd_relativehumidity
                << "\t" << m_mc->nd_watervaporsaturationdeficit
                << "\t" << m_mc->nd_windspeed
                << "\t" << m_mc->nd_shortwaveradiation_in
                << "\t" << m_mc->nd_longwaveradiation_in
                << "\t" << m_mc->nd_airpressure;
        }
        this->ostr_  << std::endl;
    }
    else
    {
        KLOGFATAL( "how did you get here...?!");
    }

    return  LDNDC_ERR_OK;
}

lerr_t
ClimateEcho::finalize()
{
    return  LDNDC_ERR_OK;
}

} /*namespace ldndc*/

