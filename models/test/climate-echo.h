/*!
 * @brief
 *    dump climate data for current time step
 *
 * @author
 *    steffen klatt
 */

#ifndef  LM_CLIMATEECHO_H_
#define  LM_CLIMATEECHO_H_

#include  "mbe_legacymodel.h"
#include  "state/mbe_state.h"

namespace ldndc {
class  LDNDC_API  ClimateEcho  :  public  MBE_LegacyModel
{
    LMOD_EXPORT_MODULE_INFO(ClimateEcho,"climate:echo","Climate Echo");
    enum  target_e
    {
        TGT_INPUT,
        TGT_STATE
    };
    public:
        ClimateEcho( MoBiLE_State *,
                cbm::io_kcomm_t *, timemode_e);

        lerr_t  configure( ldndc::config_file_t const *);
        lerr_t  initialize();
        lerr_t  solve();
        lerr_t  finalize();

        lerr_t  sleep() { return  LDNDC_ERR_OK; }
        lerr_t  wake() { return  LDNDC_ERR_OK; }

    private:
        climate::input_class_climate_t const *  m_cl;
        substate_microclimate_t const *  m_mc;

        std::ostream  ostr_;

        target_e  target_;
        bool  have_header_;
};
} /*namespace ldndc*/

#endif  /*  !LM_CLIMATEECHO_H_  */

