/*!
 * @brief
 *    test the event update and query infrastructure
 *
 * @author
 *    steffen klatt (created on: dec 10, 2011)
 */

#ifndef  LM_EVENTECHO_H_
#define  LM_EVENTECHO_H_

#include  "mbe_legacymodel.h"

namespace ldndc {
class  LDNDC_API  EventEcho  :  public  MBE_LegacyModel
{
    LMOD_EXPORT_MODULE_INFO(EventEcho,"event:echo","ManagmentEvent Echo");
    public:
        EventEcho( MoBiLE_State *,
                cbm::io_kcomm_t *, timemode_e);
        ~EventEcho();

        lerr_t  configure( ldndc::config_file_t const *);
        lerr_t  initialize();
        lerr_t  solve();
        lerr_t  finalize();

        lerr_t  sleep();
        lerr_t  wake();

    private:
        event::input_class_event_t const *  m_events;

        bool  m_printempty;
        bool  m_formatlong;
};
} /*namespace ldndc*/

#endif  /*  !LM_EVENTECHO_H_  */

