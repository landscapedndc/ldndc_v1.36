/*!
 * @brief
 *    access attributes in formatted messages
 *
 * @author
 *    steffen klatt (created on: mar 12, 2017)
 */

#ifndef  LDNDC_LD_EVENT_H_
#define  LDNDC_LD_EVENT_H_

#include  "ld_attributes.h"

namespace ldndc {

class LDNDC_API EventAttributes : public AttributeQuery
{
public:
    EventAttributes()
        : AttributeQuery( NULL, 0)
    { }
    EventAttributes( char const *  _event_name,
            char const *  _attributes, size_t  _attributes_sz = 0)
        : AttributeQuery( _attributes, _attributes_sz),
            m_eventname( _event_name ? _event_name : "")
    { }
    cbm::string_t const &  name() const
        { return this->m_eventname; }
private:
    cbm::string_t  m_eventname;
};

class LDNDC_API EventAttributesCollector : public AttributeBuild
{
public:
    EventAttributesCollector()
        : AttributeBuild( true /*drop invalid*/)
    { }
};
} /* namespace ldndc */


#include  <list>
namespace ldndc {
class LDNDC_API EventQueue
{
public:
    void  push( EventAttributes const &  _event)
        { this->m_events.push_back( _event); }
    EventAttributes  pop()
    {
        if ( this->m_events.empty())
        {
            static EventAttributes  no_such_event( NULL, NULL);
            return  no_such_event;
        }
        EventAttributes  event_head = this->m_events.front();
        this->m_events.pop_front();
        return  event_head;
    }

    void  clear()
        { this->m_events.clear(); }

    operator bool()
        { return  !this->is_empty(); }
    bool  is_empty() const
        { return  this->m_events.empty(); }

private:
    std::list< EventAttributes >  m_events;
};

} /* namespace ldndc */

#endif  /* !LDNDC_LD_EVENT_H_ */

