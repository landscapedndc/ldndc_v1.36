

#ifndef  LD_ADVECTIONDIFFUSION_H_
#define  LD_ADVECTIONDIFFUSION_H_


#include  "containers/cbm_vector.h"

namespace ldndc {

class Grid{

public:
    Grid();

    ~Grid();


    void initialize_grid();

    void update_grid();

private:

    CBM_Vector< double > grid_points;
    CBM_Vector< double > grid_interspaces;
};



Grid::Grid()
{}



void
Grid::initialize_grid()
{

}



Grid::~Grid()
{}



class AdvectionDiffusion{

public:
    AdvectionDiffusion();

    ~AdvectionDiffusion();

private:

    Grid grid_vertical;
};



AdvectionDiffusion::AdvectionDiffusion()
{}



AdvectionDiffusion::~AdvectionDiffusion()
{}

} /* namespace ldndc */

#endif  /*  !LD_ADVECTIONDIFFUSION_H_  */

