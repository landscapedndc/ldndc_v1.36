/*!
 * @brief
 *  convenience header for models
 *
 * @author
 *  Steffen Klatt (created on: nov. 18, 2016)
 */

#ifndef  LD_MODELSCONFIG_H_
#define  LD_MODELSCONFIG_H_


#include  "ldndc.h"

#include  "models/ld_isotopes.h.inc"
#include  "models/soilchemistry/metrx/soilchemistry-metrx.h.inc"

/* TODO for backward compatibility; shall not be required */
#include  <kernel/io-kcomm.h>
#include  <kernel/kscratch.h>

#endif /* !LD_MODELSCONFIG_H_ */

