
#ifndef  LM_EVENTHANDLER_CUT_H_
#define  LM_EVENTHANDLER_CUT_H_

#include  "mbe_legacymodel.h"
#include  "state/mbe_state.h"
#include  "ld_eventqueue.h"

namespace  ldndc { namespace event {
class  EventCut;
}}

namespace ldndc {

class  LDNDC_API  EventHandlerCut  :  public  MBE_LegacyModel
{
    LMOD_EXPORT_MODULE_INFO(EventHandlerCut,"eventhandler:cut","EventHandler Cut");
        
    public:
        EventHandlerCut(
                MoBiLE_State *, cbm::io_kcomm_t *, timemode_e);
        ~EventHandlerCut();
        
        lerr_t  configure( ldndc::config_file_t const *) { return  LDNDC_ERR_OK; }
        lerr_t  register_ports( cbm::io_kcomm_t *);
        lerr_t  initialize() { return  LDNDC_ERR_OK; }

        lerr_t  solve();
        lerr_t  unregister_ports( cbm::io_kcomm_t *);
        lerr_t  finalize() { return  LDNDC_ERR_OK; }
        
        lerr_t  wake() { return  LDNDC_ERR_OK; }
        lerr_t  sleep() { return  LDNDC_ERR_OK; }
        
    public:
        /*!
         * @brief
         *    handle cut event. the method checks itself
         *    whether a cut event is pending.
         *
         * @param
         *    species
         *
         * @return
         *    LDNDC_ERR_OK if no cut event is pending
         *    or everything went well.
         *
         *    ...
         */
        lerr_t
        event_cut_physiology(
                             MoBiLE_PlantVegetation *,
                             species_groups_selector_t const &  _groups);

    private:
        cbm::io_kcomm_t *  io_kcomm;
        input_class_setup_t const &  m_setup;
        input_class_siteparameters_t const &  sipar_;
        input_class_soillayers_t const *  sl_;
        substate_physiology_t *  ph_;
        substate_soilchemistry_t &  sc_;

        EventQueue  m_CutEvents;
        CBM_Handle  m_CutHandle;

    private:

        lerr_t
        m_cut_multi_species(
                    MoBiLE_PlantVegetation *,
                    species_groups_selector_t const &,
                    ldndc::EventAttributes);

        double
        get_c_available_cut(
                            MoBiLE_Plant *);

        bool
        taken_into_account(
                           species_groups_selector_t const &,
                           MoBiLE_Plant *,
                           char const *);

        void
        update_biomass_after_cut_(
                                  double * /*biomass dry weight*/,
                                  double * /*total loss carbon*/,
                                  double * /*total loss nitrogen*/,
                                  double /*loss fraction*/,
                                  double /*n/c ratio*/);
};

}

#endif    /*  !LM_EVENTHANDLER_CUT_H_  */

