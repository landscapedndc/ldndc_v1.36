/*!
 * @brief
 *    cut event
 *
 */

#include  "eventhandler/cut/cut.h"

#include  <constants/cbm_const.h>

#include  <input/event/event.h>
#include  <input/soillayers/soillayers.h>

#include  <logging/cbm_logging.h>

LMOD_MODULE_INFO(EventHandlerCut,TMODE_SUBDAILY|TMODE_PRE_DAILY|TMODE_POST_DAILY,LMOD_FLAG_USER);

ldndc::EventHandlerCut::EventHandlerCut(
        MoBiLE_State *  _state,
        cbm::io_kcomm_t *  _iokcomm,
        timemode_e  _timemode)
        : MBE_LegacyModel( _state, _timemode),
          io_kcomm( _iokcomm),
          m_setup( _iokcomm->get_input_class_ref< input_class_setup_t >()),
          sipar_( _iokcomm->get_input_class_ref< siteparameters::input_class_siteparameters_t >()),
          sl_( _iokcomm->get_input_class< soillayers::input_class_soillayers_t >()),
          ph_( _state->get_substate< substate_physiology_t >()),
          sc_( _state->get_substate_ref< substate_soilchemistry_t >())
{
    /*!
     * @todo
     *  FIXME: Two cut eventhandlers are overwriting each other.
     *  This can cause false negatives!
     */
    this->io_kcomm->get_scratch()->set("producer.cut.timemode", (int)_timemode);
}


/*!
 * @brief
 *
 */
ldndc::EventHandlerCut::~EventHandlerCut()
{
}


/*!
 * @brief
 *
 */
lerr_t
ldndc::EventHandlerCut::solve()
{
    return  LDNDC_ERR_OK;
}


/*!
 * @brief
 *
 */
bool
ldndc::EventHandlerCut::taken_into_account(
                                           species_groups_selector_t const &  _groups,
                                           MoBiLE_Plant *_p,
                                           char const * _name)
{
    if ( !_groups.is_selected( _p->groupId()))
    {
        return false;
    }

    if ( !cbm::is_valid( _name))
    {
        return true;
    }

    return cbm::is_equal(_p->cname(), _name);
}


/*!
 * @brief
 *  Returns total amount of carbon that is available for cutting.
 */
double
ldndc::EventHandlerCut::get_c_available_cut(
                                            MoBiLE_Plant *  _p)
{
    return _p->aboveground_biomass() * cbm::CCDM;
}


/*!
 * @brief
 *
 */
lerr_t
ldndc::EventHandlerCut::event_cut_physiology(
                                             MoBiLE_PlantVegetation *  _veg,
                                             species_groups_selector_t const &  _groups)
{
    if ( m_CutEvents.is_empty())
    {
        return  LDNDC_ERR_EVENT_EMPTY_QUEUE;
    }

    while ( m_CutEvents)
    {
        ldndc::EventAttributes  cut_event = m_CutEvents.pop();

        m_cut_multi_species( _veg, _groups, cut_event);

        /* disallow multiple cuts */
        if ( !m_CutEvents.is_empty())
        {
            KLOGWARN( "multiple cut events not allowed! only one event used.");
            this->m_CutEvents.clear();
        }
        return  LDNDC_ERR_EVENT_MATCH;
    }
    return  LDNDC_ERR_OK;
}


/*!
 * @brief
 * performs cutting
 *
 */
lerr_t
ldndc::EventHandlerCut::m_cut_multi_species(
                                    MoBiLE_PlantVegetation *  _veg,
                                    species_groups_selector_t const &  _groups,
                                    ldndc::EventAttributes _event_cut)
{
    cbm::state_scratch_t *  mcom = io_kcomm->get_scratch();

    /* carbon */
    double  c_fru_export_old = 0.0,   c_fru_remain_old = 0.0,   c_fru_litter_old = 0.0;
    double  c_fol_export_old = 0.0,   c_fol_remain_old = 0.0,   c_fol_litter_old = 0.0;
    double  c_dfol_export_old = 0.0,  c_dfol_remain_old = 0.0,  c_dfol_litter_old = 0.0;
    double  c_lst_export_old = 0.0,   c_lst_remain_old = 0.0,   c_lst_litter_old = 0.0;
    double  c_dst_export_old = 0.0,   c_dst_remain_old = 0.0,   c_dst_litter_old = 0.0;
    double  c_above_export_old = 0.0, c_above_remain_old = 0.0, c_above_litter_old = 0.0;
    double  c_frt_export_old = 0.0,   c_frt_remain_old = 0.0,   c_frt_litter_old = 0.0;

    mcom->get( "cut:c_fru_export", &c_fru_export_old, 0.0);
    mcom->get( "cut:c_fol_export", &c_fol_export_old, 0.0);
    mcom->get( "cut:c_dfol_export", &c_dfol_export_old, 0.0);
    mcom->get( "cut:c_lst_export", &c_lst_export_old, 0.0);
    mcom->get( "cut:c_dst_export", &c_dst_export_old, 0.0);
    mcom->get( "cut:c_above_export", &c_above_export_old, 0.0);
    mcom->get( "cut:c_frt_export", &c_frt_export_old, 0.0);

    mcom->get( "cut:c_fru_remain", &c_fru_remain_old, 0.0);
    mcom->get( "cut:c_fol_remain", &c_fol_remain_old, 0.0);
    mcom->get( "cut:c_dfol_remain", &c_dfol_remain_old, 0.0);
    mcom->get( "cut:c_lst_remain", &c_lst_remain_old, 0.0);
    mcom->get( "cut:c_dst_remain", &c_dst_remain_old, 0.0);
    mcom->get( "cut:c_above_remain", &c_above_remain_old, 0.0);
    mcom->get( "cut:c_frt_remain", &c_frt_remain_old, 0.0);

    mcom->get( "cut:c_fru_litter", &c_fru_litter_old, 0.0);
    mcom->get( "cut:c_fol_litter", &c_fol_litter_old, 0.0);
    mcom->get( "cut:c_dfol_litter", &c_dfol_litter_old, 0.0);
    mcom->get( "cut:c_lst_litter", &c_lst_litter_old, 0.0);
    mcom->get( "cut:c_dst_litter", &c_dst_litter_old, 0.0);
    mcom->get( "cut:c_above_litter", &c_above_litter_old, 0.0);
    mcom->get( "cut:c_frt_litter", &c_frt_litter_old, 0.0);

    double  c_fru_cut = 0.0;
    double  c_fol_cut = 0.0;
    double  c_dfol_cut = 0.0;
    double  c_lst_cut = 0.0;
    double  c_dst_cut = 0.0;
    double  c_frt_cut = 0.0;

    /* nitrogen */
    double  n_fru_export_old = 0.0,   n_fru_remain_old = 0.0,   n_fru_litter_old = 0.0;
    double  n_fol_export_old = 0.0,   n_fol_remain_old = 0.0,   n_fol_litter_old = 0.0;
    double  n_dfol_export_old = 0.0,  n_dfol_remain_old = 0.0,  n_dfol_litter_old = 0.0;
    double  n_lst_export_old = 0.0,   n_lst_remain_old = 0.0,   n_lst_litter_old = 0.0;
    double  n_dst_export_old = 0.0,   n_dst_remain_old = 0.0,   n_dst_litter_old = 0.0;
    double  n_above_export_old = 0.0, n_above_remain_old = 0.0, n_above_litter_old = 0.0;
    double  n_frt_export_old = 0.0,   n_frt_remain_old = 0.0,   n_frt_litter_old = 0.0;

    mcom->get( "cut:n_fru_export", &n_fru_export_old, 0.0);
    mcom->get( "cut:n_fol_export", &n_fol_export_old, 0.0);
    mcom->get( "cut:n_dfol_export", &n_dfol_export_old, 0.0);
    mcom->get( "cut:n_lst_export", &n_lst_export_old, 0.0);
    mcom->get( "cut:n_dst_export", &n_dst_export_old, 0.0);
    mcom->get( "cut:n_above_export", &n_above_export_old, 0.0);
    mcom->get( "cut:n_frt_export", &n_frt_export_old, 0.0);

    mcom->get( "cut:n_fru_remain", &n_fru_remain_old, 0.0);
    mcom->get( "cut:n_fol_remain", &n_fol_remain_old, 0.0);
    mcom->get( "cut:n_dfol_remain", &n_dfol_remain_old, 0.0);
    mcom->get( "cut:n_lst_remain", &n_lst_remain_old, 0.0);
    mcom->get( "cut:n_dst_remain", &n_dst_remain_old, 0.0);
    mcom->get( "cut:n_above_remain", &n_above_remain_old, 0.0);
    mcom->get( "cut:n_frt_remain", &n_frt_remain_old, 0.0);

    mcom->get( "cut:n_fru_litter", &n_fru_litter_old, 0.0);
    mcom->get( "cut:n_fol_litter", &n_fol_litter_old, 0.0);
    mcom->get( "cut:n_dfol_litter", &n_dfol_litter_old, 0.0);
    mcom->get( "cut:n_lst_litter", &n_lst_litter_old, 0.0);
    mcom->get( "cut:n_dst_litter", &n_dst_litter_old, 0.0);
    mcom->get( "cut:n_above_litter", &n_above_litter_old, 0.0);
    mcom->get( "cut:n_frt_litter", &n_frt_litter_old, 0.0);

    double  n_fru_cut = 0.0;
    double  n_fol_cut = 0.0;
    double  n_dfol_cut = 0.0;
    double  n_lst_cut = 0.0;
    double  n_dst_cut = 0.0;
    double  n_frt_cut = 0.0;

    /* fate of cutted biomass */
    char const * p_name = _event_cut.get( "/name", "NONE");
    bool const mulching( _event_cut.get( "/mulching", false));
    double const export_fraction_default( mulching ? 0.0 : 1.0);
    double const export_fraction_fruit( _event_cut.get( "/export_fruit", export_fraction_default));
    double const export_fraction_foliage( _event_cut.get( "/export_foliage", export_fraction_default));
    double const export_fraction_lst( _event_cut.get( "/export_living_structural_tissue", export_fraction_default));
    double const export_fraction_dst( _event_cut.get( "/export_dead_structural_tissue", export_fraction_default));
    double const export_fraction_root( _event_cut.get( "/export_root", 0.0));

    double c_remains_absolute( _event_cut.get( "/remains_absolute", invalid_dbl));
    if( cbm::is_valid( c_remains_absolute))
    {
        c_remains_absolute *= cbm::HA_IN_M2;
    }
    else
    {
        double const c_remains_relative( _event_cut.get( "/remains_relative", invalid_dbl));
        if( cbm::is_valid( c_remains_relative))
        {
            if ( cbm::flt_greater( c_remains_relative, 1.0))
            {
                LOGERROR( "Invalid value for 'remains_relative' in cutting event ['remains_relative'=",c_remains_relative,
                         "]. Value must be between 0.0 and 1.0!");
                return  LDNDC_ERR_FAIL;
            }
            else if( cbm::flt_less( c_remains_relative, 0.0))
            {
                LOGERROR( "Invalid value for 'remains_relative' in cutting event ['remains_relative'=",c_remains_relative,
                         "]. Value must be between 0.0 and 1.0!");
                return  LDNDC_ERR_FAIL;
            }
            double c_available_cut( 0.0);
            for ( PlantIterator  vt = _veg->begin(); vt != _veg->end();  ++vt)
            {
                MoBiLE_Plant *p = *vt;
                if ( taken_into_account( _groups, p, p_name))
                {
                    c_available_cut += get_c_available_cut( p);
                }
            }
            c_remains_absolute = c_remains_relative * c_available_cut;
        }
        else
        {
            double const height( _event_cut.get( "/height", invalid_dbl));
            if( cbm::is_valid( height))
            {
                c_remains_absolute = 0.0;
                for ( PlantIterator  vt = _veg->begin(); vt != _veg->end();  ++vt)
                {
                    MoBiLE_Plant *p = *vt;
                    if ( taken_into_account( _groups, p, p_name) &&
                        cbm::flt_greater( p->height_max, height))
                    {
                        c_remains_absolute += height / p->height_max * get_c_available_cut( p);
                    }
                }
            }
        }
    }

    if( cbm::is_valid( c_remains_absolute))
    {
        double c_available_cut_total( 0.0);
        for ( PlantIterator  vt = _veg->begin(); vt != _veg->end();  ++vt)
        {
            MoBiLE_Plant *p = *vt;
            if ( taken_into_account( _groups, p, p_name))
            {
                c_available_cut_total += get_c_available_cut( p);
            }
        }
        
        double c_cut_total( 0.0);
        for ( int fl = m_setup.canopylayers()-1; fl >= 0; --fl)
        {
            double const c_missing_cut_total( c_available_cut_total - c_remains_absolute);
            if ( cbm::flt_greater_zero( c_missing_cut_total))
            {
                double c_cut_potential_fl( 0.0);
                for ( PlantIterator  vt = _veg->begin(); vt != _veg->end();  ++vt)
                {
                    MoBiLE_Plant *p = *vt;

                    /* if group selected and plant height reached */
                    if ( taken_into_account( _groups, p, p_name) &&
                         cbm::flt_greater_zero( p->fFol_fl[fl]))
                    {
                        if ( !vt->TUBER())
                        {
                            double const c_cut_bud( p->fFol_fl[fl] * p->c_fru());
                            c_cut_potential_fl += c_cut_bud;
                        }

                        double const c_cut_fol( p->fFol_fl[fl] * p->c_fol());
                        c_cut_potential_fl += c_cut_fol;

                        double const c_cut_dfol( p->fFol_fl[fl] * p->c_dfol());
                        c_cut_potential_fl += c_cut_dfol;

                        double const c_cut_lst( p->fFol_fl[fl] * p->c_lst());
                        c_cut_potential_fl += c_cut_lst;

                        double const c_cut_dst( p->fFol_fl[fl] * p->c_dst());
                        c_cut_potential_fl += c_cut_dst;
                    }
                }

                if ( cbm::flt_greater_zero( c_cut_potential_fl))
                {
                    double const scale( cbm::bound_max( c_missing_cut_total / c_cut_potential_fl, 1.0));
                    double cut_c_actual( 0.0);
                    for ( PlantIterator  vt = _veg->begin(); vt != _veg->end();  ++vt)
                    {
                        MoBiLE_Plant *p = *vt;
                        /* if group selected and plant height reached */
                        if ( taken_into_account( _groups, p, p_name) &&
                             cbm::flt_greater_zero( p->fFol_fl[fl]))
                        {
                            if ( !vt->TUBER())
                            {
                                double const c_cut_fru( p->fFol_fl[fl] * scale * p->c_fru());
                                double const n_cut_fru( p->fFol_fl[fl] * scale * p->n_bud());
                                cut_c_actual += c_cut_fru;
                                c_fru_cut += c_cut_fru;
                                n_fru_cut += n_cut_fru;
                            }

                            double const c_cut_fol( p->fFol_fl[fl] * scale * p->c_fol());
                            double const n_cut_fol( p->fFol_fl[fl] * scale * p->n_fol());
                            cut_c_actual += c_cut_fol;
                            c_fol_cut += c_cut_fol;
                            n_fol_cut += n_cut_fol;

                            double const c_cut_dfol( p->fFol_fl[fl] * scale * p->c_dfol());
                            double const n_cut_dfol( p->fFol_fl[fl] * scale * p->n_dfol);
                            cut_c_actual += c_cut_dfol;
                            c_dfol_cut += c_cut_dfol;
                            n_dfol_cut += n_cut_dfol;

                            double const c_cut_lst( p->fFol_fl[fl] * scale * p->c_lst());
                            double const n_cut_lst( p->fFol_fl[fl] * scale * p->n_lst);
                            cut_c_actual += c_cut_lst;
                            c_lst_cut += c_cut_lst;
                            n_lst_cut += n_cut_lst;

                            double const c_cut_dst( p->fFol_fl[fl] * scale * p->c_dst());
                            double const n_cut_dst( p->fFol_fl[fl] * scale * p->n_dst);
                            cut_c_actual += c_cut_dst;
                            c_dst_cut += c_cut_dst;
                            n_dst_cut += n_cut_dst;

                            if ( cbm::flt_equal( scale, 1.0))
                            {
                                p->fFol_fl[fl] = 0.0;
                            }
                            else
                            {
                                p->fFol_fl[fl] *= (1.0 - scale);
                            }
                        }
                    }

                    c_available_cut_total -= cut_c_actual;
                    c_cut_total += cut_c_actual;
                }
            }
            else
            {
                break;
            }
        }

        if ( cbm::flt_greater_zero( c_cut_total))
        {
            for ( PlantIterator  vt = _veg->begin(); vt != _veg->end();  ++vt)
            {
                MoBiLE_Plant *p = *vt;
                if ( taken_into_account( _groups, p, p_name))
                {
                    double const fraction_remain( cbm::sum( p->fFol_fl, m_setup.canopylayers()));
                    double const fraction_loss( 1.0 - fraction_remain);
                    
                    double const c_fol_litter_vt( (1.0 - export_fraction_foliage) * fraction_loss * p->c_fol());
                    double const n_fol_litter_vt( (1.0 - export_fraction_foliage) * fraction_loss * p->n_fol());
                    p->mFol -= fraction_loss * p->mFol;
                    
                    double const c_dfol_litter_vt( (1.0 - export_fraction_foliage) * fraction_loss * p->c_dfol());
                    double const n_dfol_litter_vt( (1.0 - export_fraction_foliage) * fraction_loss * p->n_dfol);
                    p->dw_dfol -= fraction_loss * p->dw_dfol;
                    p->n_dfol -= fraction_loss * p->n_dfol;
                    
                    double const c_lst_litter_vt( (1.0 - export_fraction_lst) * fraction_loss * p->c_lst());
                    double const n_lst_litter_vt( (1.0 - export_fraction_lst) * fraction_loss * p->n_lst);
                    p->dw_lst -= fraction_loss * p->dw_lst;
                    p->n_lst -= fraction_loss * p->n_lst;
                    
                    double const c_dst_litter_vt( (1.0 - export_fraction_dst) * fraction_loss * p->c_dst());
                    double const n_dst_litter_vt( (1.0 - export_fraction_dst) * fraction_loss * p->n_dst);
                    p->dw_dst -= fraction_loss * p->dw_dst;
                    p->n_dst -= fraction_loss * p->n_dst;
                    
                    double const c_fru_litter_vt( vt->TUBER() ? 0.0 :
                                                 (1.0 - export_fraction_fruit) * fraction_loss * p->c_fru());
                    double const n_fru_litter_vt( vt->TUBER() ? 0.0 :
                                                 (1.0 - export_fraction_fruit) * fraction_loss * p->n_bud());
                    p->mBud -= (vt->TUBER() ? 0.0 :
                                fraction_loss * p->mBud);
                    
                    double const c_frt_litter_vt( (1.0 - export_fraction_root) * fraction_loss * sipar_.FRCUTR() * p->c_frt());
                    double const n_frt_litter_vt( (1.0 - export_fraction_root) * fraction_loss * sipar_.FRCUTR() * p->n_frt());
                    c_frt_cut += fraction_loss * sipar_.FRCUTR() * p->c_frt();
                    n_frt_cut += fraction_loss * sipar_.FRCUTR() * p->n_frt();
                    p->mFrt -= fraction_loss * sipar_.FRCUTR() * p->mFrt;
                    
                    for ( size_t  sl = 0;  sl < sl_->soil_layer_cnt();  ++sl)
                    {
                        sc_.c_raw_lit_1_sl[sl] += c_frt_litter_vt * p->fFrt_sl[sl] * (1.0 - (*p)->CELLULOSE() - (*p)->LIGNIN());
                        sc_.c_raw_lit_2_sl[sl] += c_frt_litter_vt * p->fFrt_sl[sl] * (*p)->CELLULOSE();
                        sc_.c_raw_lit_3_sl[sl] += c_frt_litter_vt * p->fFrt_sl[sl] * (*p)->LIGNIN();
                        sc_.accumulated_c_litter_below_sl[sl] += c_frt_litter_vt * p->fFrt_sl[sl];
                        
                        sc_.n_raw_lit_1_sl[sl] += n_frt_litter_vt * p->fFrt_sl[sl] * (1.0 - (*p)->CELLULOSE() - (*p)->LIGNIN());
                        sc_.n_raw_lit_2_sl[sl] += n_frt_litter_vt * p->fFrt_sl[sl] * (*p)->CELLULOSE();
                        sc_.n_raw_lit_3_sl[sl] += n_frt_litter_vt * p->fFrt_sl[sl] * (*p)->LIGNIN();
                        sc_.accumulated_n_litter_below_sl[sl] += n_frt_litter_vt * p->fFrt_sl[sl];
                    }
                    
                    double const c_litter_above( c_fru_litter_vt + c_fol_litter_vt + c_dfol_litter_vt + c_lst_litter_vt + c_dst_litter_vt);
                    double const n_litter_above( n_fru_litter_vt + n_fol_litter_vt + n_dfol_litter_vt + n_lst_litter_vt + n_dst_litter_vt);
                    
                    sc_.c_raw_lit_1_above += c_litter_above * (1.0 - (*p)->CELLULOSE() - (*p)->LIGNIN());
                    sc_.c_raw_lit_2_above += c_litter_above * (*p)->CELLULOSE();
                    sc_.c_raw_lit_3_above += c_litter_above * (*p)->LIGNIN();
                    sc_.accumulated_c_litter_above += c_litter_above;
                    
                    sc_.n_raw_lit_1_above += n_litter_above * (1.0 - (*p)->CELLULOSE() - (*p)->LIGNIN());
                    sc_.n_raw_lit_2_above += n_litter_above * (*p)->CELLULOSE();
                    sc_.n_raw_lit_3_above += n_litter_above * (*p)->LIGNIN();
                    sc_.accumulated_n_litter_above += n_litter_above;
                }
            }
        }
    }
    else
    {
        double const c_remains_fruit( _event_cut.get( "/remains_absolute_fruit", 0.0) * cbm::HA_IN_M2);
        double const c_remains_foliage( _event_cut.get( "/remains_absolute_foliage", 0.0) * cbm::HA_IN_M2);
        double const c_remains_lst( _event_cut.get( "/remains_absolute_living_structural_tissue", 0.0) * cbm::HA_IN_M2);
        double const c_remains_dst( _event_cut.get( "/remains_absolute_dead_structural_tissue", 0.0) * cbm::HA_IN_M2);
        double const c_remains_frt( _event_cut.get( "/remains_absolute_root", 0.0) * cbm::HA_IN_M2);

        double c_fruit_avail = 0.0;
        double c_foliage_avail = 0.0;
        double c_lst_avail = 0.0;
        double c_dst_avail = 0.0;
        double c_frt_avail = 0.0;
        for ( PlantIterator  vt = _veg->begin(); vt != _veg->end();  ++vt)
        {
            MoBiLE_Plant *p = *vt;
            if ( taken_into_account( _groups, p, p_name))
            {
                if ( !(*p)->TUBER())
                {
                    c_fruit_avail += p->c_fru();
                }
                c_foliage_avail += p->c_fol();
                c_lst_avail += p->c_lst();
                c_dst_avail += p->c_dst();
                c_frt_avail += p->c_frt();
            }
        }
        
        for ( PlantIterator  vt = _veg->begin(); vt != _veg->end();  ++vt)
        {
            MoBiLE_Plant *p = *vt;
            if ( taken_into_account( _groups, p, p_name))
            {
                if ( cbm::flt_greater_zero( p->mBud) && ((*p)->TUBER() == false))
                {
                    double const c_remains_fruit_vt( p->c_fru() / c_fruit_avail * c_remains_fruit);
                    double const loss_fraction_fruit( cbm::bound(0.0, 1.0 - ( c_remains_fruit_vt / p->mBud), 1.0));
                    update_biomass_after_cut_( &p->mBud, &c_fru_cut, &n_fru_cut, loss_fraction_fruit, p->nc_bud());
                }

                if ( cbm::flt_greater_zero( p->mFol))
                {
                    double const c_remains_fol_vt( p->c_fol() / c_foliage_avail * c_remains_foliage);
                    double const loss_fraction_foliage( cbm::bound(0.0, 1.0 - ( c_remains_fol_vt / p->mFol), 1.0));
                    update_biomass_after_cut_( &p->mFol, &c_fol_cut, &n_fol_cut, loss_fraction_foliage, p->nc_fol());
                }

                if ( cbm::flt_greater_zero( p->mFrt))
                {
                    double const c_remains_frt_vt( p->c_frt() / c_frt_avail * c_remains_frt);
                    double const loss_fraction_root( cbm::bound(0.0, 1.0 - ( c_remains_frt_vt / p->mFrt), 1.0));
                    update_biomass_after_cut_( &p->mFrt, &c_frt_cut, &n_frt_cut, loss_fraction_root, p->nc_frt());
                }

                if ( cbm::flt_greater_zero( p->dw_lst))
                {
                    double const nc_lst( p->nc_lst());
                    double const c_remains_lst_vt( p->c_lst() / c_lst_avail * c_remains_lst);
                    double const loss_fraction_lst( cbm::bound(0.0, 1.0 - ( c_remains_lst_vt / p->dw_lst), 1.0));
                    update_biomass_after_cut_( &p->dw_lst, &c_lst_cut, &n_lst_cut, loss_fraction_lst, p->nc_lst());
                    p->n_lst = p->dw_lst * nc_lst;
                }

                if ( cbm::flt_greater_zero( p->dw_dst))
                {
                    double const nc_dst( p->nc_dst());
                    double const c_remains_dst_vt( p->c_dst() / c_dst_avail * c_remains_dst);
                    double const loss_fraction_dst( cbm::bound(0.0, 1.0 - ( c_remains_dst_vt / p->dw_dst), 1.0));
                    update_biomass_after_cut_( &p->dw_dst, &c_dst_cut, &n_dst_cut, loss_fraction_dst, p->nc_dst());
                    p->n_dst = p->dw_dst * nc_dst;
                }
            }
        }
    }

    double c_fru_remain( 0.0);
    double c_fol_remain( 0.0);
    double c_dfol_remain( 0.0);
    double c_lst_remain( 0.0);
    double c_dst_remain( 0.0);
    double c_above_remain( 0.0);
    double c_frt_remain( 0.0);

    double n_fru_remain( 0.0);
    double n_fol_remain( 0.0);
    double n_dfol_remain( 0.0);
    double n_lst_remain( 0.0);
    double n_dst_remain( 0.0);
    double n_above_remain( 0.0);
    double n_frt_remain( 0.0);

    for ( PlantIterator  vt = _veg->begin(); vt != _veg->end();  ++vt)
    {
        MoBiLE_Plant *p = *vt;
        if ( taken_into_account( _groups, p, p_name))
        {
            c_fru_remain += p->c_fru();
            c_fol_remain += p->c_fol();
            c_dfol_remain += p->c_dfol();
            c_lst_remain += p->c_lst();
            c_dst_remain += p->c_dst();
            c_above_remain += p->aboveground_biomass() * cbm::CCDM;
            c_frt_remain += p->c_frt();
            
            n_fru_remain += p->n_bud();
            n_fol_remain += p->n_fol();
            n_dfol_remain += p->n_dfol;
            n_lst_remain += p->n_lst;
            n_dst_remain += p->n_dst;
            n_above_remain += p->aboveground_nitrogen();
            n_frt_remain += p->n_frt();
        }
    }

    double const c_fru_export = export_fraction_fruit * c_fru_cut;
    double const c_fru_litter = (1.0 - export_fraction_fruit) * c_fru_cut;

    double const c_fol_export = export_fraction_foliage * c_fol_cut;
    double const c_fol_litter = (1.0 - export_fraction_foliage) * c_fol_cut;

    double const c_dfol_export = export_fraction_foliage * c_dfol_cut;
    double const c_dfol_litter = (1.0 - export_fraction_foliage) * c_dfol_cut;

    double const c_lst_export = export_fraction_lst * c_lst_cut;
    double const c_lst_litter = (1.0 - export_fraction_lst) * c_lst_cut;

    double const c_dst_export = export_fraction_dst * c_dst_cut;
    double const c_dst_litter = (1.0 - export_fraction_dst) * c_dst_cut;

    double const c_above_export = c_fru_export + c_fol_export + c_dfol_export + c_lst_export + c_dst_export;
    double const c_above_litter = c_fru_litter + c_fol_litter + c_dfol_litter + c_lst_litter + c_dst_litter;

    double const c_frt_export = export_fraction_root * c_frt_cut;
    double const c_frt_litter = (1.0 - export_fraction_root) * c_frt_cut;


    double const n_fru_export = export_fraction_fruit * n_fru_cut;
    double const n_fru_litter = (1.0 - export_fraction_fruit) * n_fru_cut;

    double const n_fol_export = export_fraction_foliage * n_fol_cut;
    double const n_fol_litter = (1.0 - export_fraction_foliage) * n_fol_cut;

    double const n_dfol_export = export_fraction_foliage * n_dfol_cut;
    double const n_dfol_litter = (1.0 - export_fraction_foliage) * n_dfol_cut;

    double const n_lst_export = export_fraction_lst * n_lst_cut;
    double const n_lst_litter = (1.0 - export_fraction_lst) * n_lst_cut;

    double const n_dst_export = export_fraction_dst * n_dst_cut;
    double const n_dst_litter = (1.0 - export_fraction_dst) * n_dst_cut;

    double const n_above_export = n_fru_export + n_fol_export + n_dfol_export + n_lst_export + n_dst_export;
    double const n_above_litter = n_fru_litter + n_fol_litter + n_dfol_litter + n_lst_litter + n_dst_litter;

    double const n_frt_export = export_fraction_root * n_frt_cut;
    double const n_frt_litter = (1.0 - export_fraction_root) * n_frt_cut;

    ph_->accumulated_c_export_harvest += c_fru_export + c_fol_export + c_dfol_export + c_lst_export + c_dst_export + c_frt_export;
    ph_->accumulated_n_export_harvest += n_fru_export + n_fol_export + n_dfol_export + n_lst_export + n_dst_export + n_frt_export;

    ph_->accumulated_c_fru_export_harvest += c_fru_export;
    ph_->accumulated_n_fru_export_harvest += n_fru_export;

    /* carbon */
    mcom->set( "cut:c_fru_export", c_fru_export+c_fru_export_old, 0.0);
    mcom->set( "cut:c_fol_export", c_fol_export+c_fol_export_old, 0.0);
    mcom->set( "cut:c_dfol_export", c_dfol_export+c_dfol_export_old, 0.0);
    mcom->set( "cut:c_lst_export", c_lst_export+c_lst_export_old, 0.0);
    mcom->set( "cut:c_dst_export", c_dst_export+c_dst_export_old, 0.0);
    mcom->set( "cut:c_above_export", c_above_export+c_above_export_old, 0.0);
    mcom->set( "cut:c_frt_export", c_frt_export+c_frt_export_old, 0.0);

    mcom->set( "cut:c_fru_remain", c_fru_remain, 0.0);
    mcom->set( "cut:c_fol_remain", c_fol_remain, 0.0);
    mcom->set( "cut:c_dfol_remain", c_dfol_remain, 0.0);
    mcom->set( "cut:c_lst_remain", c_lst_remain, 0.0);
    mcom->set( "cut:c_dst_remain", c_dst_remain, 0.0);
    mcom->set( "cut:c_above_remain", c_above_remain, 0.0);
    mcom->set( "cut:c_frt_remain", c_frt_remain, 0.0);

    mcom->set( "cut:c_fru_litter", c_fru_litter+c_fru_litter_old, 0.0);
    mcom->set( "cut:c_fol_litter", c_fol_litter+c_fol_litter_old, 0.0);
    mcom->set( "cut:c_dfol_litter", c_dfol_litter+c_dfol_litter_old, 0.0);
    mcom->set( "cut:c_lst_litter", c_lst_litter+c_lst_litter_old, 0.0);
    mcom->set( "cut:c_dst_litter", c_dst_litter+c_dst_litter_old, 0.0);
    mcom->set( "cut:c_above_litter", c_above_litter+c_above_litter_old, 0.0);
    mcom->set( "cut:c_frt_litter", c_frt_litter+c_frt_litter_old, 0.0);

    /* nitrogen */
    mcom->set( "cut:n_fru_export", n_fru_export+n_fru_export_old, 0.0);
    mcom->set( "cut:n_fol_export", n_fol_export+n_fol_export_old, 0.0);
    mcom->set( "cut:n_dfol_export", n_dfol_export+n_dfol_export_old, 0.0);
    mcom->set( "cut:n_lst_export", n_lst_export+n_lst_export_old, 0.0);
    mcom->set( "cut:n_dst_export", n_dst_export+n_dst_export_old, 0.0);
    mcom->set( "cut:n_above_export", n_above_export+n_above_export_old, 0.0);
    mcom->set( "cut:n_frt_export", n_frt_export+n_frt_export_old, 0.0);

    mcom->set( "cut:n_fru_remain", n_fru_remain, 0.0);
    mcom->set( "cut:n_fol_remain", n_fol_remain, 0.0);
    mcom->set( "cut:n_dfol_remain", n_dfol_remain, 0.0);
    mcom->set( "cut:n_lst_remain", n_lst_remain, 0.0);
    mcom->set( "cut:n_dst_remain", n_dst_remain, 0.0);
    mcom->set( "cut:n_above_remain", n_above_remain, 0.0);
    mcom->set( "cut:n_frt_remain", n_frt_remain, 0.0);

    mcom->set( "cut:n_fru_litter", n_fru_litter+n_fru_litter_old, 0.0);
    mcom->set( "cut:n_fol_litter", n_fol_litter+n_fol_litter_old, 0.0);
    mcom->set( "cut:n_dfol_litter", n_dfol_litter+n_dfol_litter_old, 0.0);
    mcom->set( "cut:n_lst_litter", n_lst_litter+n_lst_litter_old, 0.0);
    mcom->set( "cut:n_dst_litter", n_dst_litter+n_dst_litter_old, 0.0);
    mcom->set( "cut:n_above_litter", n_above_litter+n_above_litter_old, 0.0);
    mcom->set( "cut:n_frt_litter", n_frt_litter+n_frt_litter_old, 0.0);

    return LDNDC_ERR_OK;
}


/*!
 * @brief
 *
 */
void
ldndc::EventHandlerCut::update_biomass_after_cut_(
                                           double *  _biomass_dw,
                                           double *  _total_loss_c,
                                           double *  _total_loss_n,
                                           double  _loss_fraction,
                                           double  _nc_biomass)
{
    if ( cbm::flt_equal_zero( _loss_fraction))
    {
        /* nothing removed at all */
    }
    else if ( cbm::flt_equal( _loss_fraction, 1.0))
    {
        /* everything is removed */
        *_total_loss_c += *_biomass_dw * cbm::CCDM;
        *_total_loss_n += *_biomass_dw * _nc_biomass;

        *_biomass_dw = 0.0;
    }
    else
    {
        /* some fraction is removed */
        double const  loss_dw( _loss_fraction * (*_biomass_dw));
        *_total_loss_c += loss_dw * cbm::CCDM;
        *_total_loss_n += loss_dw * _nc_biomass;

        *_biomass_dw *= ( 1.0 - _loss_fraction);
    }
}


/*!
 * @brief
 *
 */
static int  _QueueEventCut( void const *  _msg, size_t  _msg_sz, void *  _queue)
{
    ldndc::EventQueue *  queue = static_cast< ldndc::EventQueue * >( _queue);
    if ( !queue)
        { return -1; }

    ldndc::EventAttributes  cut_event(
                "cut", (char const *)_msg, _msg_sz);
    queue->push( cut_event);
    return 0;
}


/*!
 * @brief
 *
 */
lerr_t
ldndc::EventHandlerCut::register_ports( cbm::io_kcomm_t *  _io_kcomm)
{
    CBM_Callback  cb_cut;
    cb_cut.fn = &_QueueEventCut;
    cb_cut.data = &this->m_CutEvents;
    this->m_CutHandle = _io_kcomm->subscribe_event( "cut", cb_cut);
    if ( !CBM_HandleOk(this->m_CutHandle))
        { return  LDNDC_ERR_FAIL; }

    return  LDNDC_ERR_OK;
}


/*!
 * @brief
 *
 */
lerr_t
ldndc::EventHandlerCut::unregister_ports( cbm::io_kcomm_t *  _io_kcomm)
{
    _io_kcomm->unsubscribe_event( this->m_CutHandle);

    return  LDNDC_ERR_OK;
}
