/*!
 * @brief
 *      SpitFire fire model (implementation)
 *
 * @author
 *      christian werner (created on: may 13, 2013),
 */

#include  "spitfire.h"
#include  "state/state.h"

#include  <constants/lconstants-conv.h>
#include  <constants/lconstants-time.h>
#include  <logging/cbm_logging.h>
#include  <utils/lutils.h>

#include  <input/event/event.h>
#include  <input/event/events/event-fire.h>
#include  <input/event/events/event-harvest.h>
#include  <input/event/events/event-plant.h>

#include  <input/soillayers/soillayers.h>
#include  <input/species/species.h>

LMOD_MODULE_INFO(EventHandlerSpitFire,TMODE_SUBDAILY|TMODE_POST_DAILY,LMOD_FLAG_USER);
namespace ldndc {


// SPITFIRE related constants
double const EventHandlerSpitFire::MIN_NESTEROV(0.001);
double const EventHandlerSpitFire::MINER_TOT(0.055);        // Total mineral content
double const EventHandlerSpitFire::H(18000.0);              // heat content of fuel (kJ kg-1)
double const EventHandlerSpitFire::SIGMA_1HR(66.0);
double const EventHandlerSpitFire::SIGMA_10HR(3.58);
double const EventHandlerSpitFire::SIGMA_100HR(0.98);
double const EventHandlerSpitFire::SIGMA_1000HR(0.5);
double const EventHandlerSpitFire::SIGMA_LIVEGRASS(80.0);
double const EventHandlerSpitFire::MoE_livegrass(0.2);
double const EventHandlerSpitFire::P1(30.0);
double const EventHandlerSpitFire::P2(0.5);
double const EventHandlerSpitFire::fbd_a(1.2);
double const EventHandlerSpitFire::fbd_b(1.4);
double const EventHandlerSpitFire::alpha(0.0015);
double const EventHandlerSpitFire::alpha_1hr(0.001);
double const EventHandlerSpitFire::alpha_10hr(0.00005424);
double const EventHandlerSpitFire::alpha_100hr(0.0000485);
double const EventHandlerSpitFire::MINER_DAMP(0.41739);     // mineral damping coefficient (Pyne 1996)
double const EventHandlerSpitFire::part_dens(513.0);        // kg m-3 (Brown 1981)

double const EventHandlerSpitFire::SAP_FRAC_FUEL_LIT_1HR( 0.045);
double const EventHandlerSpitFire::SAP_FRAC_FUEL_LIT_10HR( 0.075);
double const EventHandlerSpitFire::SAP_FRAC_FUEL_LIT_100HR( 0.27);
double const EventHandlerSpitFire::SAP_FRAC_FUEL_LIT_1000HR( 0.61);

EventHandlerSpitFire::EventHandlerSpitFire(
                                           MoBiLE_State *  _state,
                                          cbm::io_kcomm_t *  _io,
                                           timemode_e  _timemode)
                                            : NoneTypeModuleBase( _state, _timemode),

                                            ev_( _io->get_input_class_ref< input_class_event_t >()),
                                            sipar_( _io->get_input_class_ref< input_class_siteparameters_t >()),
                                            sl_( _io->get_input_class_ref< input_class_soillayers_t >()),

                                            mc_( _state->get_substate_ref< substate_microclimate_t >()),
                                            ph_( _state->get_substate_ref< substate_physiology_t >()),
                                            sc_( _state->get_substate_ref< substate_soilchemistry_t >()),
                                            wc_( _state->get_substate_ref< substate_watercycle_t >()),
                                            species_initializer_( _state, _io),

                                            Nesterov_index(0.0),
                                            Nesterov_index_old(0.0),
                                            alpha_livegrass(0.0),

                                            owriter( _state, _io, _timemode),
                                            odata( NULL)
{
}



EventHandlerSpitFire::~EventHandlerSpitFire()
{
    if ( odata)
    {
        LD_Allocator->deallocate( odata);
    }
}



/* called once before initialize */
lerr_t
EventHandlerSpitFire::configure(
        ldndc::config_file_t const *  _cf)
{
    if ( _cf && _cf->have_output())
    {
        owriter.set_lclock( lclock());

        lerr_t  rc_owriter_configure = owriter.configure(_cf);
        RETURN_IF_NOT_OK(rc_owriter_configure);
        
        odata = LD_Allocator->allocate_type< ldndc_flt64_t >( owriter.record_size());
        if ( !odata)
        {
            return  LDNDC_ERR_OBJECT_CONFIG_FAILED;
        }

        lerr_t  rc_owini = owriter.initialize();
        if ( rc_owini && odata)
        {
            LD_Allocator->deallocate( odata);
            odata = NULL;
        }
        RETURN_IF_NOT_OK(rc_owini);
    }
    else
    {
        odata = NULL;
    }
    return  LDNDC_ERR_OK;
}



/* called once before simulation starts */
lerr_t
EventHandlerSpitFire::initialize()
{
    return  LDNDC_ERR_OK;
}



/* called once after simulation's final time step */
lerr_t
EventHandlerSpitFire::finalize()
{
    if ( odata /*use as indicator for successful writer initialization */)
    {
        lerr_t  rc_owfin = owriter.finalize();
        RETURN_IF_NOT_OK(rc_owfin);
    }

    return  LDNDC_ERR_OK;
}



/* called each time step */
lerr_t
EventHandlerSpitFire::solve()
{    
//    lerr_t  rc_wharv = harvest_fuel_wood_();
//    RETURN_IF_NOT_OK(rc_wharv);
//    lerr_t  rc_wplant = plant_fuel_wood_();
//    RETURN_IF_NOT_OK(rc_wplant);
//
//    lerr_t  rc_gharv = harvest_fuel_grass_();
//    RETURN_IF_NOT_OK(rc_gharv);
//    lerr_t  rc_gplant = plant_fuel_grass_();
//    RETURN_IF_NOT_OK(rc_gplant);
//
//
//    /* start balance check */
//    SPITFIRE_balance_check( 1);
//
//
//    /* nesterov index */
//    if ( lclock_ref().is_position( TMODE_POST_DAILY))
//    {
//        lerr_t  nest = Nesterov();
//        if ( nest)
//        {
//            KLOGERROR( lclock()->now(),":", name(),"[",object_id(),"] failed to calculate Nesterov index");
//            return  LDNDC_ERR_FAIL;
//        }
//    }
//
//
//    /* fuel calculation*/
//    if ( lclock_ref().is_position( TMODE_POST_DAILY))
//    {
//        lerr_t  cfp = calculate_fuel();
//        if ( cfp)
//        {
//            KLOGERROR( lclock()->now(),":", name(),"[",object_id(),"] failed to calculate fuel");
//            return  LDNDC_ERR_FAIL;
//        }
//    }
//
//
//    /* fire event */
//    events_view_t< EventFire > ev_fire_v( ev_.events_view< EventFire >());
//    if ( ev_fire_v.size() == 0)
//    {
//        write_output();
//        return  LDNDC_ERR_OK;
//    }
//    else if ( ev_fire_v.size() > 1)
//    {
//        KLOGWARN("Ignoring superflous fire events. Only first event for this timestep considered!");
//    }
//
//
//    /* fire fraction from input */
//    flux.fire_frac = ev_fire_v[0]->burned_area();
//
//
//    /* effective wind */
//    effective_wind();
//
//
//    /* fire spread speed */
//    lerr_t rc_r = Rothermel();
//    if ( rc_r)
//    {
//        KLOGERROR( lclock()->now(),":", name(),"[",object_id(),"] failed to calculate fire spread");
//        return  LDNDC_ERR_FAIL;
//    }
//
//
//    /* burn fuel */
//    lerr_t ccf = calculate_consumed_fuel();
//    if ( ccf)
//    {
//        KLOGERROR( lclock()->now(),":", name(),"[",object_id(),"] failed to calculate consumed fuel");
//        return  LDNDC_ERR_FAIL;
//    }
//
//
//    /* fire intensity */
//    lerr_t cfi = calculate_fire_intensity();
//    if ( cfi)
//    {
//        KLOGERROR( lclock()->now(),":", name(),"[",object_id(),"]  failed to calculate fire intensity");
//        return  LDNDC_ERR_FAIL;
//    }
//
//
//    /* burn and damage trees */
//    lerr_t rc_bt = burn_trees();
//    if ( rc_bt)
//    {
//        KLOGERROR( lclock()->now(),":", name(),"[",object_id(),"] failed to burn trees");
//        return  LDNDC_ERR_FAIL;
//    }
//
//
//    /* emissions */
//    lerr_t rc_ce = calculate_emissions();
//    if ( rc_ce)
//    {
//        KLOGERROR( lclock()->now(),":", name(),"[",object_id(),"] failed to calculate trace gas emissions");
//        return  LDNDC_ERR_FAIL;
//    }
//
//
//    /* update burned stuff */
//    update_burned_litter();
//    update_burned_grass();
//
//
//    /* output and balance check */
//    write_output();
//    SPITFIRE_balance_check( 2);
//
    return  LDNDC_ERR_OK;
}



lerr_t
EventHandlerSpitFire::harvest_fuel_wood_()
{
//    FuelWoodMap::iterator  w = fuel_wood_vt.begin();
//    for ( ; w != fuel_wood_vt.end();  ++w)
//    {
//        bool  is_still_alive = false;
//        for ( const_wood_species_iterator s( sp_.begin< species::wood >()); s != sp_.end< species::wood >(); ++s)
//        {
//            if ( cbm::is_equal_i( w->first, (*s).name()))
//            {
//                is_still_alive = true;
//                break;
//            }
//        }
//        if ( !is_still_alive)
//        {
//            KLOGINFO( "removing species from container  [species=",w->first,"]");
//            fuel_wood_vt.erase( w);
//        }
//    }
    return  LDNDC_ERR_OK;
}



lerr_t
EventHandlerSpitFire::plant_fuel_wood_()
{
//    for ( const_wood_species_iterator s(sp_.begin< species::wood >()); s != sp_.end< species::wood >(); ++s)
//    {
//        FuelWoodMap::iterator  w = fuel_wood_vt.find((*s).name());
//        if ( w == fuel_wood_vt.end())
//        {
//            fuel_wood_vt.insert( std::make_pair((*s).name(), FuelWoodVT()));
//
//            KLOGINFO( "added fuel  [species=",(*s).name(),"]");
//        }
//    }
    return  LDNDC_ERR_OK;
}



lerr_t
EventHandlerSpitFire::harvest_fuel_grass_()
{
//    FuelGrassMap::iterator  w = fuel_grass_vt.begin();
//    for ( ; w != fuel_grass_vt.end();  ++w)
//    {
//        bool  is_still_alive = false;
//        for ( const_grass_species_iterator s( sp_.begin< species::grass >()); s != sp_.end< species::grass >(); ++s)
//        {
//            if ( cbm::is_equal_i( w->first, (*s).name()))
//            {
//                is_still_alive = true;
//                break;
//            }
//        }
//        if ( !is_still_alive)
//        {
//            KLOGINFO( "removing species from container  [species=",w->first,"]");
//            fuel_grass_vt.erase( w);
//        }
//    }
    return  LDNDC_ERR_OK;
}


lerr_t
EventHandlerSpitFire::plant_fuel_grass_()
{
//    for ( const_grass_species_iterator s(sp_.begin< species::grass >()); s != sp_.end< species::grass >(); ++s)
//    {
//        FuelGrassMap::iterator  w = fuel_grass_vt.find((*s).name());
//        if ( w == fuel_grass_vt.end())
//        {
//            fuel_grass_vt.insert( std::make_pair((*s).name(), FuelGrassVT()));
//            KLOGINFO( "added fuel  [species=",(*s).name(),"]");
//        }
//    }
    return  LDNDC_ERR_OK;
}



/*!
 * @brief
 *    calculates nesterov fire danger index
 *    Done Daily and depends on previous day's Nesterov index
 *    Requires maximum temperature, relative humidity and precip
 *    Returns the original Nesterov index [0,N] where N large
 *    (not the exponentiated 'smooth risk function' [0,1])
 */
lerr_t
EventHandlerSpitFire::Nesterov()
{
//    double const prec_scale( (mc_.nd_precipitation+mc_.nd_irrigation+mc_.nd_irrigation_automatic+mc_.nd_irrigation_reservoir)*cbm::MM_IN_M);
//    double  Nesterov_index_new( 0.0);
//    
//    // calculate NFDI increment provided precipitation < 3mm day^-1 and temp > 0 deg C
//    if (prec_scale < 3.0 && mc_.nd_maximumairtemperature > 0.0) 
//    {
//        double  rhum( cbm::bound(1.0e-9, mc_.nd_relativehumidity, 99.999999999));
//        
//        // Need to calculate dew point temperature
//        double a( 17.271);  // (deg C) constant from wikipedia(!)
//        double b( 237.7);   // (deg C) constant from wikipedia(!)
//        double gamma( (a * mc_.nd_maximumairtemperature) / (b + mc_.nd_maximumairtemperature) + log(rhum / 100.0));
//        double dew_point_temp( (b * gamma) / (a - gamma));
//        double const incr( (mc_.nd_maximumairtemperature - dew_point_temp) * mc_.nd_maximumairtemperature);
//        
//        if(incr >= 10000.0) 
//        {
//            KLOGINFO("Spitfire: calculate_Nesterov_index: Extremely large Nesterov increment = ", incr);
//            KLOGINFO("Spitfire: Nesterov_index: Tmax = ", mc_.nd_maximumairtemperature, " deg C, Rel Humidity = ", rhum, " and Tdewpoint = ", dew_point_temp, " deg C");
//        }
//        else if (incr >= 0.0) 
//        {
//            Nesterov_index_new = incr + Nesterov_index_old;
//        } 
//        else 
//        {
//            KLOGINFO("Spitfire: calculate_Nesterov_index: Negative increment in Nesterov FDI ", incr, " possibly inconsistent input data?");
//            KLOGINFO("Spitfire: Nesterov_index: Tmax = ", mc_.nd_maximumairtemperature, " deg C, Rel Humidity = ", rhum, " and Tdewpoint = ", dew_point_temp, " deg C");
//        }
//    }
//    else if (prec_scale >= 3.0) 
//    {
//        Nesterov_index_new = 0;
//    }
//        
//    // calculate the risk using the exponential formula as  Venevsky et al GCB 2002
//    // cs%d_FDI=1-exp(-udata%fdi_alpha*cs%acc_NI) -- FORTRAN90 from Allan Spessa
//    // sk:unused    double alpha = 0.000337;
//    // sk:unused    riskNFDI = 1 - exp(-alpha * nest);
//    
//    Nesterov_index_old = Nesterov_index;
//    Nesterov_index = Nesterov_index_new;

    return LDNDC_ERR_OK;
}


/*!
 * @brief
 *    calculate potential fuel for a fire event
 */
lerr_t
EventHandlerSpitFire::calculate_fuel()
{
//    /* reset all */
//    flux.reset();
//    fuel.reset();
//    fuel_lit.reset();
//    for (const_grass_species_iterator s(sp_.begin< species::grass >()); s != sp_.end< species::grass >(); ++s)
//    {
//        fuel_grass_vt[(*s).name()].reset();
//    }
//    for (const_wood_species_iterator s(sp_.begin< species::wood >()); s != sp_.end< species::wood >(); ++s)
//    {
//        fuel_wood_vt[(*s).name()].reset();
//    }
//
//
//    /***************/
//    /* LITTER FUEL */
//    /***************/
//
//    double const c_folLit( sc_.c_fol_lit1 + sc_.c_fol_lit2 + sc_.c_fol_lit3);
//    fuel_lit.f_1hr_fol_gC = (c_folLit * cbm::G_IN_KG);
//
//    double const n_folLit( sc_.n_fol_lit1 + sc_.n_fol_lit2 + sc_.n_fol_lit3);
//    fuel_lit.f_1hr_fol_gN = (n_folLit * cbm::G_IN_KG);
//
//    double const c_sapLit( sc_.c_sap_lit1 + sc_.c_sap_lit2 + sc_.c_sap_lit3);
//    fuel_lit.f_1hr_sap_gC    = SAP_FRAC_FUEL_LIT_1HR * c_sapLit * cbm::G_IN_KG;
//    fuel_lit.f_10hr_sap_gC   = SAP_FRAC_FUEL_LIT_10HR * c_sapLit * cbm::G_IN_KG;
//    fuel_lit.f_100hr_sap_gC  = SAP_FRAC_FUEL_LIT_100HR  * c_sapLit * cbm::G_IN_KG;
//    fuel_lit.f_1000hr_sap_gC = SAP_FRAC_FUEL_LIT_1000HR  * c_sapLit * cbm::G_IN_KG;
//
//    double const n_sapLit( sc_.n_sap_lit1 + sc_.n_sap_lit2 + sc_.n_sap_lit3);
//    fuel_lit.f_1hr_sap_gN    = SAP_FRAC_FUEL_LIT_1HR * n_sapLit * cbm::G_IN_KG;
//    fuel_lit.f_10hr_sap_gN   = SAP_FRAC_FUEL_LIT_10HR * n_sapLit * cbm::G_IN_KG;
//    fuel_lit.f_100hr_sap_gN  = SAP_FRAC_FUEL_LIT_100HR  * n_sapLit * cbm::G_IN_KG;
//    fuel_lit.f_1000hr_sap_gN = SAP_FRAC_FUEL_LIT_1000HR  * n_sapLit * cbm::G_IN_KG;
//
//    // Calculate total dead fuel
//    fuel_lit.deadfuel_gC = fuel_lit.f_1hr_fol_gC + fuel_lit.f_1hr_sap_gC + fuel_lit.f_10hr_sap_gC + fuel_lit.f_100hr_sap_gC;
//
//    // Calculate net fuel (accounting for non-flammable mineral content)
//    if (fuel_lit.deadfuel_gC > 0.0)
//    {
//        fuel_lit.net_deadfuel_gC = (1.0 - MINER_TOT) * fuel_lit.deadfuel_gC; // gDM/m^2 - must be convert to kg in RoS
//    }
//    else
//    {
//        fuel_lit.net_deadfuel_gC = 0.0;
//    }
//
//    // Calculate fuel bulk density weighed by fuel class and fuel load
//    double FBD_deadfuel( 0.0) ;
//    if (fuel_lit.deadfuel_gC > 0.0)
//    {
//        // def. locally to preserve code for the moment
//        double const dens_fuel( 25.0);  // set to fuel bulk density of tropical broadleaved summergreen
//        FBD_deadfuel += (dens_fuel * (fuel_lit.f_1hr_sap_gC
//                                      + fbd_a * fuel_lit.f_10hr_sap_gC
//                                      + fbd_b * fuel_lit.f_100hr_sap_gC) / fuel_lit.deadfuel_gC);
//    }
//
//
//    /**************/
//    /* GRASS FUEL */
//    /**************/
//
//    for (const_grass_species_iterator s(sp_.begin< species::grass >()); s != sp_.end< species::grass >(); ++s)
//    {
//        // leave 5% root and leafs biomass to let them grow again.
//        double const mFol_avail( std::max( ph_.mFol_vt[s] - 0.01, 0.0));
//        double const mSap_avail( std::max( ph_.mSap_vt[s] - 0.01, 0.0));
//
//        fuel_grass_vt[(*s).name()].f_livegr_gC = (mFol_avail
//                                                  + mSap_avail) * cbm::G_IN_KG * cbm::CCDM;
//
//        fuel_grass_vt[(*s).name()].f_livegr_gN = (mFol_avail * ph_.ncFol_vt[s]
//                                                  + mSap_avail * ph_.ncSap_vt[s]) * cbm::G_IN_KG;
//    }
//
//    // Calculates litter moisture and bulk density of live grasses
//    double FBD_livegrass( 0.0);
//    if ( get_fuel_grass() > 0.0 )
//    {
//        // fuel.dlm_livegrass = max(0.0, 10.0 / 9.0 * patch.soil.dwcontupper[date.day] - 1.0 / 9.0);
//        fuel.dlm_livegrass = std::max(0.0, 10.0 / 9.0 * wc_.wfps_sl[0] - 1.0 / 9.0);
//
//        for (const_grass_species_iterator s( sp_.begin< species::grass >()); s != sp_.end< species::grass >(); ++s)
//        {
//            FBD_livegrass += (fuel_grass_vt[(*s).name()].f_livegr_gC / get_fuel_grass() * s->FIRE_DENSFUEL());
//        }
//    }
//    else
//    {
//        fuel.dlm_livegrass = 0.0;
//    }
//
//
//    /*********************************/
//    /* COMBINE LITTER AND GRASS FUEL */
//    /*********************************/
//
//    double ratio_deadfuel( 0.0);
//    double ratio_livefuel( 0.0);
//    if (fuel_lit.deadfuel_gC > 0.0 || get_fuel_grass() > 0.0)
//    {
//        ratio_deadfuel = fuel_lit.deadfuel_gC / (fuel_lit.deadfuel_gC + get_fuel_grass());
//        ratio_livefuel = get_fuel_grass() / (fuel_lit.deadfuel_gC + get_fuel_grass());
//        fuel.char_FBD = (FBD_deadfuel * ratio_deadfuel) + (FBD_livegrass * ratio_livefuel);
//    }
//    else
//    {
//        fuel.char_FBD = 1.0; //dummy wegen warnung
//    }
//
//    // moisture of extinction
//    // TODO pft.MoE moisture of extintion per vt type in physiology definieren, hier fuer den Moment 0.3
//    fuel.MoE = 0.3;
//
//    fuel.char_net_fuel_gC = fuel_lit.net_deadfuel_gC + ((1- MINER_TOT) * get_fuel_grass());
//    fuel.char_MoE         = fuel.MoE * ratio_deadfuel + MoE_livegrass * ratio_livefuel;
//
//    if (fuel_lit.deadfuel_gC > 0.0)
//    {
//        // Calculate characteristic surface-to-volume for dead fuel
//        fuel.char_sigma = (fuel_lit.f_1hr_fol_gC   * SIGMA_1HR  +
//                           fuel_lit.f_1hr_sap_gC   * SIGMA_1HR  +
//                           fuel_lit.f_10hr_sap_gC  * SIGMA_10HR +
//                           fuel_lit.f_100hr_sap_gC * SIGMA_100HR) / fuel_lit.deadfuel_gC;
//
//        // Calculate weighted average alpha for dead fuel
//        fuel.alpha_deadfuel = (fuel_lit.f_1hr_fol_gC   * alpha_1hr  +
//                               fuel_lit.f_1hr_sap_gC   * alpha_1hr  +
//                               fuel_lit.f_10hr_sap_gC  * alpha_10hr +
//                               fuel_lit.f_100hr_sap_gC * alpha_100hr) / fuel_lit.deadfuel_gC;
//
//        if (Nesterov_index > MIN_NESTEROV && fuel.dlm_livegrass > 0.0)
//        {
//            alpha_livegrass = -(log(fuel.dlm_livegrass) / Nesterov_index);
//        }
//        else
//        {
//            alpha_livegrass = 0.0;
//        }
//        fuel.char_alpha = fuel.alpha_deadfuel * ratio_deadfuel + alpha_livegrass * ratio_livefuel;
//    }
//    else
//    {
//        fuel.char_sigma = 0.00001;
//        fuel.char_alpha = 0.00001;
//    }
//
//    if (Nesterov_index > MIN_NESTEROV)
//    {
//        fuel.char_dlm = exp(-fuel.char_alpha * Nesterov_index); // relative moisture content
//        fuel.dlm_1hr  = exp(-alpha_1hr * Nesterov_index);
//    }
//    else
//    {
//        fuel.char_dlm = 0.0;
//        fuel.char_sigma = 0.0001;   // set sigam to 0.0001 for zero net fuel loading (char_dlm = 0.0)   // Thonicke A1
//        fuel.dlm_1hr  = 0.0;
//    }
//
//
//    //  calculate the length-to-breadth ration of the fire elipse
//    //    if (fuel.effective_wind < 16.67) //                                  M_5
//    //    {
//    //        fuel.length_to_breadth = 1.0;
//    //    }
//    //    else
//    //    {
//    //        // Spitfire Eq. 24
//    //        fuel.length_to_breadth = ((fpc_tree  * (1.0 + (8.729 * (pow(1.0 - (exp(-0.03 * 0.06 * fuel.effective_wind)), 2.155)))))
//    //                                  + (fpc_grass * (1.1 + pow((0.06 * fuel.effective_wind), 0.0464))));
//    //    }
//    //
//    //    // get the weighted F parameter
//    //    if( cbm::flt_equal_zero( fpc_tree))
//    //    {
//    //        fuel.char_F = 0.0;
//    //    }
//    //    else
//    //    {
//    //        double const f_vt_Sum( vs_.f_vt.sum());
//    //        fuel.char_F = 0.0;
//    //        for (const_wood_species_iterator  s = sp_.begin< species::wood >();  s != sp_.end< species::wood >();  ++s)
//    //        {
//    //            // patchpft.pft.flame mit 1,0 ersetzt !!!!!!!!!!! aendern
//    //            // fuel.char_F += patchpft.pft.flame * (patchpft.fuel.fpc/fpc_tree);
//    //            fuel.char_F += 0.06 * ( vs_.f_vt[s] / f_vt_Sum);
//    //        }
//    //    }
//    //
//    //    // Calculate fire Danger Index                    Eq_9
//    //    if (Nesterov_index <= 0.0)
//    //    {
//    //        fuel.fdi = 0.0;
//    //    }
//    //    else
//    //    {
//    //        fuel.fdi = std::max(0., (1.0 - ((1.0 / fuel.char_MoE) * (exp(-fuel.char_alpha * Nesterov_index)))));
//    //    }
//    //
//    //
//    //    // The main Rothermel calculation for forward ROS
//    //    fuel.ros_f = forward_Rothermel_ROS();
//    //
//    //    // Backward spread
//    //    fuel.ros_b = fuel.ros_f * exp(-0.012 * fuel.effective_wind);    // THONICKE
//    //
//    //    // fire duration                   M_19
//    //    if (fuel.ros_b < 0.05)
//    //    {
//    //        fuel.ros_b = 0.0;
//    //    }
//    //
//    //    fuel.fire_durat = 241 / (1 + (((240)) * exp(-11.06 * fuel.fdi)));
//    //    fuel.db         = fuel.ros_b * fuel.fire_durat;
//    //    fuel.df         = fuel.ros_f * fuel.fire_durat;
//    //    
//    //    if (fuel_lit.net_deadfuel_gC <= 0.0) 
//    //    {
//    //        fuel.ros_b = 0.0;
//    //        fuel.ros_f = 0.0;
//    //        fuel.db    = 0.0;
//    //        fuel.df    = 0.0;
//    //    }

    return  LDNDC_ERR_OK;
}



lerr_t
EventHandlerSpitFire::effective_wind()
{
//    // Calculate fractional percentage cover (fpc) totals for tree and grass vts to affect windspeed
//    // and PFT totals for the 'F' scorch parameter
//    double fpc_tree( 0.0);
//    for (const_wood_species_iterator s(sp_.begin< species::wood >()); s != sp_.end< species::wood >(); ++s)
//    {
//        fpc_tree  += vs_.f_vt[s];
//    }
//    if (fpc_tree > 1.0)
//    {
//        fpc_tree = 1.0;
//    }
//
//    double fpc_grass( 0.0);
//    for (const_grass_species_iterator s(sp_.begin< species::grass >()); s != sp_.end< species::grass >(); ++s)
//    {
//        fpc_grass += vs_.f_vt[s];
//    }
//    if (fpc_grass > 1.0)
//    {
//        fpc_grass = 1.0;
//    }
//
//    // incoming windspeed is m/s, put into m/min
//    double wind_mmin( mc_.ts_windspeed * cbm::SEC_IN_MIN);
//
//    // reduce windspeed according to tree and grass FPC
//    double const fpc_tot( fpc_tree + fpc_grass);
//    fuel.effective_wind = ((fpc_tot > 0.0) ? (fpc_tree / fpc_tot * wind_mmin * 0.4) + (fpc_grass / fpc_tot * wind_mmin * 0.6) : wind_mmin);
//
    return LDNDC_ERR_OK;
}



lerr_t
EventHandlerSpitFire::Rothermel()
{
//    if (fuel.char_MoE > 0.0)
//    {
//        fuel.mw_weight = std::min(1.0, fuel.char_dlm / fuel.char_MoE);
//    }
//    else
//    {
//        fuel.mw_weight = 0.;
//    }
//
//    // Rate of forward spread
//    double const beta( fuel.char_FBD / part_dens);                      // packing ratio (bulk density ratio)
//    double const beta_opt( 0.200395 * (pow(fuel.char_sigma, -0.8189))); // packing density for opt. fire spread (Thonicke et al.: A6) CHECKED!
//    double const bet( beta / beta_opt);                                 // ratio
//
//    // bug correction: was 94 * fuel.char_dlm (in Thonicke paper (A4): 2594 given)
//    double const q_ig( 581.0 + 2594.0 * fuel.char_dlm);                 // heat of pre-ignition (Thonicke et al.: A4)       CHECKED!
//    double const eps( exp(-4.528 / fuel.char_sigma));                   // effective heating number (Thonicke et al.: A3)   CHECKED!
//
//    // influence of wind speed
//    double const b( 0.15988 * pow(fuel.char_sigma, 0.54));
//    double const c( 7.47    * (exp(-0.8711 * pow(fuel.char_sigma, 0.55))));
//    double const e( 0.7515  * (exp(-0.01094 * fuel.char_sigma)));
//
//    // MF first change the windspeed from m/min to ft/min
//    double const wind_forward_feet( fuel.effective_wind * 3.281);
//    double const phi_wind( c * (pow(wind_forward_feet, b)) * pow(bet, -e));        // was base_wind before
//
//    // propagating flux                  M_16
//    double xi( 0.0);
//    if (fuel.char_sigma > 0.00001)
//    {
//        xi = exp((0.792 + 3.7597 * sqrt(fuel.char_sigma)) * (beta + 0.1)) / (192.0 + 7.9095 * fuel.char_sigma);  //(Thonicke et al.: A2) CHECKED!
//    }
//
//    // reaction intensity                M_17
//    double const a( 8.9033  * pow(fuel.char_sigma, -0.7913));           // Pyne 1996, Brown 1994
//
//    // maximum reaction velocity
//    double const gamma_max( 1.0 / (0.0591 + 2.926 * pow(fuel.char_sigma, -1.5)));   //(Thonicke et al.: A1) CHECKED!
//
//    // optimum reaction velocity
//    double const gamma_aptr( gamma_max * pow(bet, a) * exp(a * (1.0 - bet)));   //(Thonicke et al.: A1) CHECKED!
//
//    // moisture dampening coefficient
//    double const moist_damp( std::max(0.0, (1.0 - (2.59 * fuel.mw_weight) + (5.11 * pow(fuel.mw_weight, 2.0)) - (3.52 * pow(fuel.mw_weight, 3))))); // (Thonicke et al. A1) CHECKED!
//
//    // conversion from gDM to kgDM as needed by Rothermel eqn
//    double const ir( gamma_aptr * (fuel.char_net_fuel_gC/cbm::CCDM/cbm::G_IN_KG) * H * moist_damp * MINER_DAMP);  // reaction intensity (kJ m-2 min-1) (Thonicke et al. A1) CHECKED!
//    fuel.gamma_f = gamma_aptr * moist_damp * MINER_DAMP;
//
//    // Forward spread speed
//    if ((fuel.char_FBD > 0.0) && (eps > 0.0) && (q_ig > 0.0) && (ir > 0.0))
//    {
//        fuel.ros_f = (ir * xi * (1.0 + phi_wind)) / (fuel.char_FBD * eps * q_ig);
//    }
//    else
//    {
//        fuel.ros_f = 0.0;
//    }
//
//    // Backward spread speed
//    fuel.ros_b = fuel.ros_f * exp(-0.012 * fuel.effective_wind);

    return  LDNDC_ERR_OK;
}



/*!
 * @brief
 *
 */
lerr_t
EventHandlerSpitFire::calculate_consumed_fuel()
{
//    // fuel consumption depending on fuel moisture
//    // influence of livefuel on 1hr fuel moisture content
//    double moist_10_100hr( 1.0);
//    if (fuel.char_MoE > 0.0 && fuel.MoE > 0.0)
//    {
//        moist_10_100hr = fuel.char_dlm / fuel.char_MoE;
//    }
//    else
//    {
//        fuel.mw_weight = 0.0;
//    }
//
//    double frac_consumed_livegr( 0.0);
//    if (fuel.dlm_livegrass >= 0.0 && fuel.dlm_livegrass <= 0.18)
//    {
//        frac_consumed_livegr = 1.0;
//    }
//    else if (fuel.dlm_livegrass > 0.18 && fuel.dlm_livegrass <= 0.73)
//    {
//        frac_consumed_livegr = 1.1 - (0.62 * fuel.dlm_livegrass);
//    }
//    else if (fuel.dlm_livegrass > 0.73)
//    {
//        frac_consumed_livegr = 2.45 - (2.45 * fuel.dlm_livegrass);
//    }
//    else
//    {
//        //KLOGVERBOSE("Spitfire: dlm_livegrass ", fuel.dlm_livegrass, " which is outside of expected range");
//    }
//
//
//    for (const_grass_species_iterator s(sp_.begin< species::grass >()); s != sp_.end< species::grass >(); ++s)
//    {
//        fuel_grass_vt[(*s).name()].f_frac_consumed_livegr = std::min(flux.fire_frac * frac_consumed_livegr, 0.9);
//        fuel_grass_vt[(*s).name()].f_consumed_livegr_gC   = fuel_grass_vt[(*s).name()].f_livegr_gC * fuel_grass_vt[(*s).name()].f_frac_consumed_livegr;
//        fuel_grass_vt[(*s).name()].f_consumed_livegr_gN   = fuel_grass_vt[(*s).name()].f_livegr_gN * fuel_grass_vt[(*s).name()].f_frac_consumed_livegr;
//    }
//
//
//    /////////////////// Fuel_1hr fuel_consumed_1hr_gC
//    // MF changed this to use a different fraction for 1hr fuel
//    // Previously Veiko said: here the same combustion fraction as for live grass is used
//    double frac_consumed_1hr( 0.0);
//    if (fuel.dlm_1hr >= 0.0 && fuel.dlm_1hr <= 0.18)
//    {
//        frac_consumed_1hr = 1.0;
//    }
//    else if (fuel.dlm_1hr > 0.18 && fuel.dlm_1hr <= 0.73)
//    {
//        frac_consumed_1hr = 1.1 - (0.62 * fuel.dlm_1hr);
//    }
//    else if (fuel.dlm_1hr > 0.73)
//    {
//        frac_consumed_1hr = 2.45 - (2.45 * fuel.dlm_1hr);
//    }
//
//    double const f_consumed_1hr_sap_gC( fuel_lit.f_1hr_sap_gC * flux.fire_frac * frac_consumed_1hr); // calculate amount of combusted fuel
//    double const f_consumed_1hr_fol_gC( fuel_lit.f_1hr_fol_gC * flux.fire_frac * frac_consumed_1hr); // calculate amount of combusted fuel
//    double const f_consumed_1hr_sap_gN( fuel_lit.f_1hr_sap_gN * flux.fire_frac * frac_consumed_1hr); // calculate amount of combusted fuel
//    double const f_consumed_1hr_fol_gN( fuel_lit.f_1hr_fol_gN * flux.fire_frac * frac_consumed_1hr); // calculate amount of combusted fuel
//
//    // 10_hr_fuel consumption                                             M_24
//    double frac_consumed_10hr( 0.0);
//    if (moist_10_100hr >= 0.0 && moist_10_100hr <= 0.12)
//    {
//        frac_consumed_10hr = 1.0;
//    }
//    else if (moist_10_100hr > 0.12 && moist_10_100hr <= 0.51)
//    {
//        frac_consumed_10hr = 1.09 - 0.72 * moist_10_100hr;
//    }
//    else if (moist_10_100hr > 0.51 && moist_10_100hr <= 1.0)
//    {
//        frac_consumed_10hr = 1.47 - 1.47 * moist_10_100hr;
//    }
//
//    double const f_consumed_10hr_gC( fuel_lit.f_10hr_sap_gC * flux.fire_frac * frac_consumed_10hr); // calculate amount of combusted fuel
//    double const f_consumed_10hr_gN( fuel_lit.f_10hr_sap_gN * flux.fire_frac * frac_consumed_10hr); // calculate amount of combusted fuel
//
//
//    // 100hr fuel consumption
//    double frac_consumed_100hr( 0.0);
//    if (moist_10_100hr <= 0.38)
//    {
//        frac_consumed_100hr = 0.98 - (0.85 * moist_10_100hr);
//    }
//    else if (moist_10_100hr > 0.38 && moist_10_100hr <= 1.0)
//    {
//        frac_consumed_100hr = 1.06 - (1.06 * moist_10_100hr);
//    }
//
//    double const f_consumed_100hr_gC( fuel_lit.f_100hr_sap_gC * flux.fire_frac * frac_consumed_100hr); // calculate amount of combusted fuel
//    double const f_consumed_100hr_gN( fuel_lit.f_100hr_sap_gN * flux.fire_frac * frac_consumed_100hr); // calculate amount of combusted fuel
//
//    // 1000hr fuel consumption
//    double const frac_consumed_1000hr( (-0.80 * fuel.mw_weight) + 0.8);
//    double const f_consumed_1000hr_gC( fuel_lit.f_1000hr_sap_gC * flux.fire_frac * frac_consumed_1000hr); // calculate amount of combusted fuel
//    double const f_consumed_1000hr_gN( fuel_lit.f_1000hr_sap_gN * flux.fire_frac * frac_consumed_1000hr); // calculate amount of combusted fuel
//
//    // Summation
//    fuel.fuel_consum_litter_c = (f_consumed_1hr_sap_gC
//                                 + f_consumed_1hr_fol_gC
//                                 + f_consumed_10hr_gC
//                                 + f_consumed_100hr_gC
//                                 + f_consumed_1000hr_gC);
//
//    fuel.fuel_consum_litter_n = (f_consumed_1hr_sap_gN
//                                 + f_consumed_1hr_fol_gN
//                                 + f_consumed_10hr_gN
//                                 + f_consumed_100hr_gN
//                                 + f_consumed_1000hr_gN);
//
//    for (const_grass_species_iterator s(sp_.begin< species::grass >()); s != sp_.end< species::grass >(); ++s)
//    {
//        fuel.fuel_consum_grass_c += fuel_grass_vt[(*s).name()].f_consumed_livegr_gC;
//        fuel.fuel_consum_grass_n += fuel_grass_vt[(*s).name()].f_consumed_livegr_gN;
//    }
//
//
//    ///////////////////////////////////////////////////////////////////////////////////////
//    // calculate residence_time_PandR - INTERNAL FUNCTION
//    // CALCULATES RESIDENCE TIME (tau_L) FOR A PATCH USING THE PETERSON AND RYAN 1986 APPROACH
//    // lerr_t crt = calculate_residence_time_PandR();
//    double const tau_b_livegr(  39.4 * (get_fuel_grass()        / 1e4) * (1.0 - pow((1.0 - frac_consumed_livegr),   0.5)));
//    double const tau_b_1hr(     39.4 * (fuel_lit.f_1hr_sap_gC   / 1e4) * (1.0 - pow((1.0 - frac_consumed_1hr),      0.5)));
//    double const tau_b_10hr(    39.4 * (fuel_lit.f_10hr_sap_gC  / 1e4) * (1.0 - pow((1.0 - frac_consumed_10hr),     0.5)));
//    double const tau_b_100hr(   39.4 * (fuel_lit.f_100hr_sap_gC / 1e4) * (1.0 - pow((1.0 - frac_consumed_100hr),    0.5)));
//    fuel.tau_l = (tau_b_livegr + tau_b_1hr + tau_b_10hr + tau_b_100hr);
//
//
//    /***************************/
//    /* update litter and grass */
//    /***************************/
//
//    fuel_lit.f_1hr_fol_gC    -= f_consumed_1hr_fol_gC;
//    fuel_lit.f_1hr_sap_gC    -= f_consumed_1hr_sap_gC;
//    fuel_lit.f_10hr_sap_gC   -= f_consumed_10hr_gC;
//    fuel_lit.f_100hr_sap_gC  -= f_consumed_100hr_gC;
//    fuel_lit.f_1000hr_sap_gC -= f_consumed_1000hr_gC;
//
//    fuel_lit.f_1hr_fol_gN    -= f_consumed_1hr_fol_gN;
//    fuel_lit.f_1hr_sap_gN    -= f_consumed_1hr_sap_gN;
//    fuel_lit.f_10hr_sap_gN   -= f_consumed_10hr_gN;
//    fuel_lit.f_100hr_sap_gN  -= f_consumed_100hr_gN;
//    fuel_lit.f_1000hr_sap_gN -= f_consumed_1000hr_gN;
//
//    for (const_grass_species_iterator s(sp_.begin< species::grass >()); s != sp_.end< species::grass >(); ++s)
//    {
//        fuel_grass_vt[(*s).name()].f_livegr_gC -= fuel_grass_vt[(*s).name()].f_consumed_livegr_gC;    // balance fuel
//        fuel_grass_vt[(*s).name()].f_livegr_gN -= fuel_grass_vt[(*s).name()].f_consumed_livegr_gN;    // balance fuel
//    }

    return  LDNDC_ERR_OK;
}



lerr_t
EventHandlerSpitFire::calculate_fire_intensity()
{
    // surface intensity in kw/m fireline
    // check numbers and replace by constants, check unit especially dry matter versus carbon
    double const fuel_consum_tot( fuel.fuel_consum_litter_c + fuel.fuel_consum_grass_c);
    fuel.daily_fire_intensity = H * (fuel_consum_tot / cbm::G_IN_KG / cbm::CCDM / flux.fire_frac) * fuel.ros_f / 60.0;
    
    return  LDNDC_ERR_OK;
}



lerr_t
EventHandlerSpitFire::burn_trees()
{
//    // FIRE EFFECTS ------------------------------------
//    // Here the effects of the fire on vegetation as well as the fluxes are calculated
//    // Fuel combustion of the average fire has been calculated before, now it is applied
//    // to the burned area
//
//    // Fire effects on vegetation - mortality and biomass reduction due to fire affects
//    // MF - now putting dead trees to litter!
//    // (previously Veiko said "NB not putting dead trees to litter.")
//
//    // Individual mortality and fire effect loop
//    for (const_wood_species_iterator  s = sp_.begin< species::wood >();  s != sp_.end< species::wood >();  ++s)
//    {
//        double const aboveground_fraction( 1.0 - vs_.ugwdf_vt[s]);
//        double const leafmass_gC( ph_.mFol_vt[s] * cbm::CCDM * cbm::G_IN_KG);                                           // [g C]
//        double const above_sapwoodmass_gC( aboveground_fraction * ph_.mSap_vt[s] * cbm::CCDM * cbm::G_IN_KG);           // [g C]
//        double const above_heartwoodmass_gC( aboveground_fraction * ph_.mCor_vt[s] * cbm::CCDM * cbm::G_IN_KG);         // [g C]
//
//        /*****************/
//        /* scorch height */
//        /*****************/
//
//        // fire height is very high compared to empirical values, but since it represents scorch height and
//        // not flame height is too high at low intensities according to the formula below
//        // Cheney gives the following empirical values <500 kw/m ->max 1.5; 500-3000 ->max 6m 7000=15m this
//        // is approximal I/500*. pft.flame pft.flame is between 0.3 and 0.09 these values are not reasonable
//        // and not reproducable
//        // Veiko: however these are the flame height which are lower than the scorch height this is one of
//        // the places where an improvement should occurr
//        // MF Verfied Eq. 16.
//        fuel.scorch_height = (s->FIRE_FLAME() * pow( fuel.daily_fire_intensity, 0.667));
//
//        // MF Verified.  Crown scorch damage - Eqn. 17 Thonicke et al. 2010
//        double ck( 0.0);                                                // proportion of crown scorched [ratio]
//        if (fuel.scorch_height > vs_.height_min_vt[s])
//        {
//            if (fuel.scorch_height >= vs_.height_max_vt[s])
//            {
//                ck = 0.9;                                                    // scorch height > tree height, whole crown is scorched
//            }
//            else if (vs_.height_max_vt[s] > vs_.height_min_vt[s])
//            {
//                double const cl_t( vs_.height_max_vt[s] - vs_.height_min_vt[s]);        // crown length per height class [m]
//                ck = std::min((fuel.scorch_height - vs_.height_min_vt[s]) / cl_t, 0.9); // scorch comes part way up, kill appropriate proportion of crown
//            }
//        }
//
//
//        /***************/
//        /* fire damage */
//        /***************/
//
//        //calculate fire damage quantities for this individiual: tau_c, cl_t
//        //Verified.  Eqs. 20 and 21 Thonicke et al. 2010
//        // critical time for cambial damage in min                    [min]
//        double const tau_c( 2.9 * pow(s->FIRE_BARKA() * ( vs_.dbh_vt[s] * cbm::CM_IN_M) + s->FIRE_BARKB(), 2.0));
//        double const pm_ck( s->FIRE_RCK() * pow(ck, s->FIRE_P()));    // prob of mort due to crown scortching    [ratio]
//
//        //MF Verified.  Probability cambial kill - Eqn. 17 Thonicke et al. 2010
//        double pm_tau( 0.0);    // prob of mort due to cambial damage                        [ratio]
//        if ((fuel.tau_l / tau_c) >= 2.0)
//        {
//            pm_tau = 1.0;                                        // If fire residence twice critical time definitely kill tree
//        }
//        else if ((fuel.tau_l / tau_c) > 0.22)
//        {
//            pm_tau = (0.563 * (fuel.tau_l / tau_c)) - 0.125;    // If ratio of residence to crit. time > 0.22 apply formula
//        }
//
//        // TREE POST-FIRE MORTALITY
//        // MF Verified. Total post-fire mortality - Eq 18 Thonicke et al. 2010
//        // postfire mortality                                        [ratio]
//        double const postf_mort( std::min(pm_tau + pm_ck - (pm_tau * pm_ck), 0.9));
//
//
//        /******************/
//        /* update biomass */
//        /******************/
//
//        // this carbon taken from all fire affected trees goes to the atmosphere
//        // caution since this calculation involves the actual amount of carbon in the compartments,
//        // here first the adding to the atmosphere has to be performed before reducing the carbon pools
//        const double consumed_leafs( flux.fire_frac * postf_mort * leafmass_gC);
//
//        const double above_consumed_sapwood( flux.fire_frac * postf_mort * above_sapwoodmass_gC);
//        const double above_consumed_heartwood( flux.fire_frac * postf_mort * above_heartwoodmass_gC);
//
//        const double below_consumed_sapwood( above_consumed_sapwood / aboveground_fraction * vs_.ugwdf_vt[s]);
//        const double below_consumed_heartwood( above_consumed_heartwood / aboveground_fraction * vs_.ugwdf_vt[s]);
//
//        fuel_wood_vt[(*s).name()].f_consumed_leaf_gC      += consumed_leafs;
//        fuel_wood_vt[(*s).name()].f_consumed_sapwood_gC   += above_consumed_sapwood;
//        fuel_wood_vt[(*s).name()].f_consumed_heartwood_gC += above_consumed_heartwood;
//
//        fuel_wood_vt[(*s).name()].f_consumed_leaf_gN      += (consumed_leafs / cbm::CCDM * ph_.ncFol_vt[s]);
//        fuel_wood_vt[(*s).name()].f_consumed_sapwood_gN   += (above_consumed_sapwood / cbm::CCDM * ph_.ncSap_vt[s]);
//        fuel_wood_vt[(*s).name()].f_consumed_heartwood_gN += (above_consumed_heartwood / cbm::CCDM * ph_.ncCor_vt[s]);
//
//        // fire_frac = burned area !!!
//        // reduce carbon in fire affected individuals (local SPITFIRE variable in grams)
//        //leafmass_gC      -= prop_fa * ck * leafmass_gC;
//        double const mSapOld( ph_.mSap_vt[s]);
//        double const mCorOld( ph_.mCor_vt[s]);
//        ph_.mFol_vt[s] -= (consumed_leafs           / (cbm::CCDM * cbm::G_IN_KG));
//        ph_.mSap_vt[s] -= ((above_consumed_sapwood + below_consumed_sapwood) / (cbm::CCDM * cbm::G_IN_KG));
//        ph_.mCor_vt[s] -= ((above_consumed_heartwood + below_consumed_heartwood) / (cbm::CCDM * cbm::G_IN_KG));
//
//        for ( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
//        {
//            ph_.nd_sCorBelow_vtsl[s][sl] += (vs_.fFrt_vtsl[s][sl] * below_consumed_heartwood / (cbm::CCDM * cbm::G_IN_KG));
//            ph_.nd_nLitCorBelow_vtsl[s][sl] += (vs_.fFrt_vtsl[s][sl] * below_consumed_heartwood * ph_.ncCor_vt[s] / (cbm::CCDM * cbm::G_IN_KG));
//
//            ph_.nd_sWoodBelow_vtsl[s][sl] += (vs_.fFrt_vtsl[s][sl] * below_consumed_sapwood / (cbm::CCDM * cbm::G_IN_KG));
//            ph_.nd_nLitWoodBelow_vtsl[s][sl] += (vs_.fFrt_vtsl[s][sl] * below_consumed_sapwood * ph_.ncSap_vt[s] / (cbm::CCDM * cbm::G_IN_KG));
//        }
//
//        if ((mSapOld+mCorOld) > 0.0)
//        {
//            vs_.ntree_vt[s] *= ( (ph_.mSap_vt[s]+ph_.mCor_vt[s]) / (mSapOld+mCorOld));
//            vs_.vtree_vt[s] *= ( (ph_.mSap_vt[s]+ph_.mCor_vt[s]) / (mSapOld+mCorOld));
//        }
//
//        species_initializer_.update_area_fraction_wood( (*s));
//    }

    return  LDNDC_ERR_OK;
}



/*!
 * @brief calculates trace gas emissions based emission factors
 *
 */
lerr_t
EventHandlerSpitFire::write_emissions(
                                      species_t const &  _s,
                                      double const &_c_burned_plant,
                                      double const &_n_burned_plant,
                                      double &_c_non_burned,
                                      double &_n_non_burned)
{
//    // use current foliage fraction to determine of how much litter came from particular species
//    double const mFol_tot( ph_.mFol_vt.sum());
//    double const fol_frac( (mFol_tot > 0.0) ? ph_.mFol_vt[_s.slot] / mFol_tot : 1.0 / sp_.species_cnt());
//
//    double const c_litter_burned( fol_frac * fuel.fuel_consum_litter_c);
//    double const n_litter_burned( fol_frac * fuel.fuel_consum_litter_n);
//
//    double const c_burned_tot( _c_burned_plant + c_litter_burned);
//    double const n_burned_tot( _n_burned_plant + n_litter_burned);
//
//    if( cbm::flt_equal_zero( c_burned_tot) ){
//        return  LDNDC_ERR_OK;
//    }
//
//    // molar conversion factors (C / molecule, N / molecule)
//    std::map<std::string, double> fconv;
//    fconv["CO2"] = 12.0 / 44;
//    fconv["CO"]  = 12.0 / 28;
//    fconv["CH4"] = 12.0 / 16;
//    fconv["VOC"] = 1.0;
//    fconv["TPM"] = 1.0;
//    fconv["NOx"] = 14.0 / 30;
//    fconv["N2O"] = 14.0 / 48;
//    fconv["N2"]  = 1.0;
//
//
//    // Akaiga emission factors are based on a C content of 50% !!!
//    // use 0.5 as cbm::CCDM equiv here ...
//
//    double const CCDM_Akagi( 0.5 );
//
//    double const frac_flux_C_BC( sipar_.FIRE_FRAC_BLACKCARBON());
//    double const frac_flux_CO2( fconv["CO2"] * _s->FIRE_EM_CO2() * cbm::KG_IN_G / CCDM_Akagi);
//    double const frac_flux_CO(  fconv["CO"]  * _s->FIRE_EM_CO()  * cbm::KG_IN_G / CCDM_Akagi);
//    double const frac_flux_CH4( fconv["CH4"] * _s->FIRE_EM_CH4() * cbm::KG_IN_G / CCDM_Akagi);
//    double const frac_flux_VOC( fconv["VOC"] * _s->FIRE_EM_VOC() * cbm::KG_IN_G / CCDM_Akagi);
//    double const frac_flux_TPM( fconv["TPM"] * _s->FIRE_EM_TPM() * cbm::KG_IN_G / CCDM_Akagi);
//    double const frac_c_tot( frac_flux_C_BC + frac_flux_CO2 + frac_flux_CO + frac_flux_CH4 + frac_flux_VOC + frac_flux_TPM);
//
//    double const pyro_flux_C_BC( c_burned_tot * frac_flux_C_BC / frac_c_tot);
//    double const pyro_flux_CO2(  c_burned_tot * frac_flux_CO2  / frac_c_tot);
//    double const pyro_flux_CO(   c_burned_tot * frac_flux_CO   / frac_c_tot);
//    double const pyro_flux_CH4(  c_burned_tot * frac_flux_CH4  / frac_c_tot);
//    double const pyro_flux_VOC(  c_burned_tot * frac_flux_VOC  / frac_c_tot);
//    double const pyro_flux_TPM(  c_burned_tot * frac_flux_TPM  / frac_c_tot);
//
//    double const pyro_flux_NOx( n_burned_tot / CCDM_Akagi * cbm::KG_IN_G * fconv["NOx"] * _s->FIRE_EM_NOX());
//    double const pyro_flux_N2O( n_burned_tot / CCDM_Akagi * cbm::KG_IN_G * fconv["N2O"] * _s->FIRE_EM_N2O());
//    double const pyro_flux_N2(  n_burned_tot * 0.36);           // Kuhlbusch et al., 1991
//
//    // update black carbon immediately
//    sc_.black_carbon_sl[0] += (pyro_flux_C_BC * cbm::KG_IN_G);
//
//    flux.pyro_flux_C_BC += pyro_flux_C_BC;
//    flux.pyro_flux_CO2  += pyro_flux_CO2;
//    flux.pyro_flux_CO   += pyro_flux_CO;
//    flux.pyro_flux_CH4  += pyro_flux_CH4;
//    flux.pyro_flux_VOC  += pyro_flux_VOC;
//    flux.pyro_flux_TPM  += pyro_flux_TPM;
//    flux.pyro_flux_NOx  += pyro_flux_NOx;
//    flux.pyro_flux_N2O  += pyro_flux_N2O;
//    flux.pyro_flux_N2   += pyro_flux_N2;
//
//    double const c_emis( pyro_flux_CO2 + pyro_flux_CO + pyro_flux_CH4 + pyro_flux_VOC + pyro_flux_TPM + pyro_flux_C_BC);
//    if ( cbm::flt_greater_zero(c_burned_tot - c_emis))
//    {
//        _c_non_burned += (c_burned_tot - c_emis);
//    }
//    else if ( c_burned_tot / c_emis < 0.99)
//    {
//        KLOGERROR( lclock()->now(),":", name(),"[",object_id(),"] ", c_burned_tot, " - ", c_emis );
//
//        KLOGERROR( lclock()->now(),":", name(),"[",object_id(),"] carbon fire emissions factor of species ", _s.name()," add up to values greater 1.0");
//        return LDNDC_ERR_FAIL;
//    }
//
//    double const n_emis( pyro_flux_NOx + pyro_flux_N2O + pyro_flux_N2);
//    if ( cbm::flt_greater_zero(n_burned_tot - n_emis))
//    {
//        _n_non_burned += (n_burned_tot - n_emis);
//    }
//    else if ( n_burned_tot / n_emis < 0.99)
//    {
//        KLOGERROR( lclock()->now(),":", name(),"[",object_id(),"] nitrogen fire emissions factor of species ", _s.name()," add up to values greater 1.0");
//        return LDNDC_ERR_FAIL;
//    }

    return  LDNDC_ERR_OK;
}



/*!
 * @brief
 *    calculates trace gas emissions based emission factors
 */
lerr_t
EventHandlerSpitFire::calculate_emissions()
{
//    double c_non_burned( 0.0);
//    double n_non_burned( 0.0);
//
//    for (const_wood_species_iterator s(sp_.begin< species::wood >()); s != sp_.end< species::wood >(); ++s)
//    {
//        double const c_burned( fuel_wood_vt[(*s).name()].sum_c_compounds());
//        double const n_burned( fuel_wood_vt[(*s).name()].sum_n_compounds());
//
//        lerr_t rc = write_emissions((*s), c_burned, n_burned, c_non_burned, n_non_burned);
//        if ( rc)
//        {
//            return  LDNDC_ERR_FAIL;
//        }
//    }
//
//    for (const_grass_species_iterator s(sp_.begin< species::grass >()); s != sp_.end< species::grass >(); ++s)
//    {
//        double const c_burned( fuel_grass_vt[(*s).name()].f_consumed_livegr_gC);
//        double const n_burned( fuel_grass_vt[(*s).name()].f_consumed_livegr_gN);
//
//        lerr_t rc = write_emissions((*s), c_burned, n_burned, c_non_burned, n_non_burned);
//        if ( rc)
//        {
//            return  LDNDC_ERR_FAIL;
//        }
//    }
//
//    sc_.c_sap_lit3          += c_non_burned * cbm::KG_IN_G;
//    flux.pyro_flux_C_LITTER += c_non_burned;
//
//    double const FRAC_ORG( 0.5);
//    sc_.n_sap_lit3  += FRAC_ORG * n_non_burned * cbm::KG_IN_G;
//    sc_.nh4_sl[0]   += (1.0 - FRAC_ORG) * sipar_.FIRE_FRAC_NH4() * n_non_burned * cbm::KG_IN_G;
//    sc_.no3_sl[0]   += (1.0 - FRAC_ORG) * sipar_.FIRE_FRAC_NO3() * n_non_burned * cbm::KG_IN_G;
//
//    flux.pyro_flux_N_LITTER += FRAC_ORG * n_non_burned;
//    flux.pyro_flux_N_NH4    += (1.0 - FRAC_ORG) * sipar_.FIRE_FRAC_NH4() * n_non_burned;
//    flux.pyro_flux_N_NO3    += (1.0 - FRAC_ORG) * sipar_.FIRE_FRAC_NO3() * n_non_burned;

    return  LDNDC_ERR_OK;
}



/*!
 * @brief
 *
 */
lerr_t
EventHandlerSpitFire::update_burned_litter()
{
//    /*********************/
//    /* update fol litter */
//    /*********************/
//
//    double const c_fol_lit_all( sc_.c_fol_lit1+sc_.c_fol_lit2+sc_.c_fol_lit3);
//    double const fr_c_fol_lit1( sc_.c_fol_lit1 / c_fol_lit_all);
//    double const fr_c_fol_lit2( sc_.c_fol_lit2 / c_fol_lit_all);
//    double const fr_c_fol_lit3( sc_.c_fol_lit3 / c_fol_lit_all);
//
//    double const n_fol_lit_all( sc_.n_fol_lit1+sc_.n_fol_lit2+sc_.n_fol_lit3);
//    double const fr_n_fol_lit1( sc_.n_fol_lit1 / n_fol_lit_all);
//    double const fr_n_fol_lit2( sc_.n_fol_lit2 / n_fol_lit_all);
//    double const fr_n_fol_lit3( sc_.n_fol_lit3 / n_fol_lit_all);
//
//    double const residual_fuel_c_fol( fuel_lit.f_1hr_fol_gC * cbm::KG_IN_G);
//    if (residual_fuel_c_fol > 0.0)
//    {
//        sc_.c_fol_lit1 = (fr_c_fol_lit1 * residual_fuel_c_fol);
//        sc_.c_fol_lit2 = (fr_c_fol_lit2 * residual_fuel_c_fol);
//        sc_.c_fol_lit3 = (fr_c_fol_lit3 * residual_fuel_c_fol);
//    }
//    else
//    {
//        sc_.c_fol_lit1 = 0.0;
//        sc_.c_fol_lit2 = 0.0;
//        sc_.c_fol_lit3 = 0.0;
//    }
//
//    double const residual_fuel_n_fol( fuel_lit.f_1hr_fol_gN * cbm::KG_IN_G);
//    if (residual_fuel_n_fol > 0.0)
//    {
//        sc_.n_fol_lit1 = (fr_n_fol_lit1 * residual_fuel_n_fol);
//        sc_.n_fol_lit2 = (fr_n_fol_lit2 * residual_fuel_n_fol);
//        sc_.n_fol_lit3 = (fr_n_fol_lit3 * residual_fuel_n_fol);
//    }
//    else
//    {
//        sc_.n_fol_lit1 = 0.0;
//        sc_.n_fol_lit2 = 0.0;
//        sc_.n_fol_lit3 = 0.0;
//    }
//
//
//    /*********************/
//    /* update sap litter */
//    /*********************/
//
//    double const c_sap_lit_all( sc_.c_sap_lit1+sc_.c_sap_lit2+sc_.c_sap_lit3);
//    double const fr_c_sap_lit1( sc_.c_sap_lit1 / c_sap_lit_all);
//    double const fr_c_sap_lit2( sc_.c_sap_lit2 / c_sap_lit_all);
//    double const fr_c_sap_lit3( sc_.c_sap_lit3 / c_sap_lit_all);
//
//    double const n_sap_lit_all( sc_.n_sap_lit1+sc_.n_sap_lit2+sc_.n_sap_lit3);
//    double const fr_n_sap_lit1( sc_.n_sap_lit1 / n_sap_lit_all);
//    double const fr_n_sap_lit2( sc_.n_sap_lit2 / n_sap_lit_all);
//    double const fr_n_sap_lit3( sc_.n_sap_lit3 / n_sap_lit_all);
//
//    double const residual_fuel_c_sap( (fuel_lit.f_1hr_sap_gC + fuel_lit.f_10hr_sap_gC + fuel_lit.f_100hr_sap_gC + fuel_lit.f_1000hr_sap_gC) * cbm::KG_IN_G);
//    if (residual_fuel_c_sap > 0.0)
//    {
//        sc_.c_sap_lit1 = (fr_c_sap_lit1 * residual_fuel_c_sap);
//        sc_.c_sap_lit2 = (fr_c_sap_lit2 * residual_fuel_c_sap);
//        sc_.c_sap_lit3 = (fr_c_sap_lit3 * residual_fuel_c_sap);
//    }
//    else
//    {
//        sc_.c_sap_lit1 = 0.0;
//        sc_.c_sap_lit2 = 0.0;
//        sc_.c_sap_lit3 = 0.0;
//    }
//
//    double const residual_fuel_n_sap( (fuel_lit.f_1hr_sap_gN + fuel_lit.f_10hr_sap_gN + fuel_lit.f_100hr_sap_gN + fuel_lit.f_1000hr_sap_gN) * cbm::KG_IN_G);
//    if (residual_fuel_n_sap > 0.0)
//    {
//        sc_.n_sap_lit1 = (fr_n_sap_lit1 * residual_fuel_n_sap);
//        sc_.n_sap_lit2 = (fr_n_sap_lit2 * residual_fuel_n_sap);
//        sc_.n_sap_lit3 = (fr_n_sap_lit3 * residual_fuel_n_sap);
//    }
//    else
//    {
//        sc_.n_sap_lit1 = 0.0;
//        sc_.n_sap_lit2 = 0.0;
//        sc_.n_sap_lit3 = 0.0;
//    }

    return  LDNDC_ERR_OK;
}



lerr_t
EventHandlerSpitFire::update_burned_grass()
{
//    for (const_grass_species_iterator  s = sp_.begin< species::grass >();  s != sp_.end< species::grass >();  ++s)
//    {
//        double const grass_before( (ph_.mFol_vt[s] + ph_.mSap_vt[s]) * (cbm::G_IN_KG * cbm::CCDM));
//        double const frac_cons( (grass_before > 0.0) ? fuel_grass_vt[(*s).name()].f_consumed_livegr_gC / grass_before : 0.0);
//        ph_.mFol_vt[s] *= (1.0 - frac_cons);
//        ph_.mSap_vt[s] *= (1.0 - frac_cons);
//
//        for ( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
//        {
//            double const frt_litter( ph_.mFrt_vt[s] * (1.0 - frac_cons));
//            ph_.mFrt_vt[s] -= frt_litter;
//
//            ph_.nd_sFrt_vtsl[s][sl] += frt_litter;
//            ph_.nd_nLitFrt_vtsl[s][sl] += frt_litter * ph_.ncFrt_vt[s];
//        }
//    }

    return  LDNDC_ERR_OK;
}



lerr_t
EventHandlerSpitFire::write_output()
{
//    if ( !lclock_ref().is_position( TMODE_POST_DAILY) || !odata)
//    {
//        return LDNDC_ERR_OK;
//    }
//
//    double c_burned_trees( 0.0);
//    for (const_wood_species_iterator  s = sp_.begin< species::wood >();  s != sp_.end< species::wood >();  ++s)
//    {
//        c_burned_trees += fuel_wood_vt[(*s).name()].sum_c_compounds();
//    }
//
//    ldndc_kassert(odata);
//
//#define  SPITFIRE_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(__val__)  LDNDC_OUTPUT_SET_COLUMN_((__val__),=,odata,1)
//    LDNDC_OUTPUT_SET_COLUMN_INDEX(0);
//
//    double const G_M2_IN_KG_HA( cbm::KG_IN_G*cbm::M2_IN_HA);
//    SPITFIRE_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( flux.fire_frac);
//    SPITFIRE_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( Nesterov_index);
//    SPITFIRE_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( fuel.daily_fire_intensity);
//    SPITFIRE_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( fuel.ros_f);
//    SPITFIRE_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( fuel.ros_b);
//    SPITFIRE_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( fuel.scorch_height);
//    SPITFIRE_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( fuel.fuel_consum_litter_c * G_M2_IN_KG_HA);
//    SPITFIRE_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( fuel.fuel_consum_grass_c  * G_M2_IN_KG_HA);
//    SPITFIRE_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( c_burned_trees            * G_M2_IN_KG_HA);
//    SPITFIRE_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( flux.pyro_flux_CO         * G_M2_IN_KG_HA);
//    SPITFIRE_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( flux.pyro_flux_CO2        * G_M2_IN_KG_HA);
//    SPITFIRE_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( flux.pyro_flux_CH4        * G_M2_IN_KG_HA);
//    SPITFIRE_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( flux.pyro_flux_VOC        * G_M2_IN_KG_HA);
//    SPITFIRE_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( flux.pyro_flux_TPM        * G_M2_IN_KG_HA);
//    SPITFIRE_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( flux.pyro_flux_C_BC       * G_M2_IN_KG_HA);
//    SPITFIRE_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( flux.pyro_flux_C_LITTER   * G_M2_IN_KG_HA);
//    SPITFIRE_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( flux.pyro_flux_NOx        * G_M2_IN_KG_HA);
//    SPITFIRE_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( flux.pyro_flux_N2O        * G_M2_IN_KG_HA);
//    SPITFIRE_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( flux.pyro_flux_N2         * G_M2_IN_KG_HA);
//    SPITFIRE_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( flux.pyro_flux_N_LITTER   * G_M2_IN_KG_HA);
//    SPITFIRE_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( flux.pyro_flux_N_NH4      * G_M2_IN_KG_HA);
//    SPITFIRE_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( flux.pyro_flux_N_NO3      * G_M2_IN_KG_HA);

    return  owriter.write_spitfire_output(odata);
}



/*!
 * @brief
 *
 */
void
EventHandlerSpitFire::SPITFIRE_balance_check(
                                             unsigned int _i)
{
//    double tot_c( sc_.c_sap_lit1 + sc_.c_sap_lit2 + sc_.c_sap_lit3
//                 + sc_.c_fol_lit1 + sc_.c_fol_lit2 + sc_.c_fol_lit3);
//
//    double tot_n( sc_.n_sap_lit1 + sc_.n_sap_lit2 + sc_.n_sap_lit3
//                 + sc_.n_fol_lit1 + sc_.n_fol_lit2 + sc_.n_fol_lit3);
//
//    for (input_class_species_t::const_iterator<> vt =sp_.begin_any() ; vt != sp_.end_any();  ++vt)
//    {
//        tot_c += ((ph_.mFol_vt[vt] + ph_.mSap_vt[vt] + ph_.mCor_vt[vt] + ph_.mFrt_vt[vt]
//                   + ph_.nd_sFrt_vtsl[vt].sum() + ph_.nd_sCorBelow_vtsl[vt].sum() + ph_.nd_sWoodBelow_vtsl[vt].sum()) * cbm::CCDM);
//
//        tot_n += ((ph_.mFol_vt[vt] * ph_.ncFol_vt[vt])
//                  + (ph_.mSap_vt[vt] * ph_.ncSap_vt[vt])
//                  + (ph_.mCor_vt[vt] * ph_.ncCor_vt[vt])
//                  + (ph_.mFrt_vt[vt] * ph_.ncFrt_vt[vt])
//                  + ph_.nd_nLitFrt_vtsl[vt].sum() + ph_.nd_nLitCorBelow_vtsl[vt].sum() + ph_.nd_nLitWoodBelow_vtsl[vt].sum());
//    }
//
//    if (_i == 1)
//    {
//        tot_c_balance_1 = tot_c;
//        tot_n_balance_1 = tot_n;
//    }
//    else if (_i == 2)
//    {
//        double c_burned_trees( 0.0);
//        double n_burned_trees( 0.0);
//        for (const_wood_species_iterator  s = sp_.begin< species::wood >();  s != sp_.end< species::wood >();  ++s)
//        {
//            c_burned_trees += (fuel_wood_vt[(*s).name()].f_consumed_leaf_gC
//                               + fuel_wood_vt[(*s).name()].f_consumed_sapwood_gC
//                               + fuel_wood_vt[(*s).name()].f_consumed_heartwood_gC);
//
//            n_burned_trees += (fuel_wood_vt[(*s).name()].f_consumed_leaf_gN
//                               + fuel_wood_vt[(*s).name()].f_consumed_sapwood_gN
//                               + fuel_wood_vt[(*s).name()].f_consumed_heartwood_gN);
//        }
//
//        double c_burned_tot( (fuel.fuel_consum_litter_c + fuel.fuel_consum_grass_c + c_burned_trees) * cbm::KG_IN_G);
//        double n_burned_tot( (fuel.fuel_consum_litter_n + fuel.fuel_consum_grass_n + n_burned_trees) * cbm::KG_IN_G);
//
//        double difference( std::abs(tot_c_balance_1 - (tot_c + c_burned_tot)));
//        if (difference > 1.0e-10)
//        {
//            KLOGERROR("C-leakage\t C before: ", tot_c_balance_1*cbm::M2_IN_HA,
//                      "\t C after: ", tot_c*cbm::M2_IN_HA, ".\t difference: ", difference*cbm::M2_IN_HA);
//        }
//
//        difference = std::abs(tot_n_balance_1 - (tot_n + n_burned_tot));
//        if (difference > 1.0e-10)
//        {
//            KLOGERROR("N-leakage\t N before: ", tot_n_balance_1*cbm::M2_IN_HA,
//                      "\t N after: ", tot_n*cbm::M2_IN_HA, ".\t difference: ", difference*cbm::M2_IN_HA);
//        }
//    }
}



double
EventHandlerSpitFire::get_fuel_grass()
{
//    double fuel_grass( 0.0);
//    for (const_grass_species_iterator s(sp_.begin< species::grass >()); s != sp_.end< species::grass >(); ++s)
//    {
//        fuel_grass += fuel_grass_vt[(*s).name()].f_livegr_gC;
//    }

    return 0.0;//fuel_grass;
}



Flux::Flux()
{
    reset();
}
void
Flux::reset()
{
    pyro_flux_CO2  = 0.0;
    pyro_flux_CO   = 0.0;
    pyro_flux_CH4  = 0.0;
    pyro_flux_VOC  = 0.0;
    pyro_flux_TPM  = 0.0;
    pyro_flux_NOx  = 0.0;
    pyro_flux_N2O  = 0.0;
    pyro_flux_N2  = 0.0;
    pyro_flux_C_BC = 0.0;
    pyro_flux_C_LITTER = 0.0;
    pyro_flux_N_LITTER = 0.0;
    pyro_flux_N_NH4 = 0.0;
    pyro_flux_N_NO3 = 0.0;

    fire_frac      = 0.0;
}


Fuel::Fuel()
{
    reset();
}
void
Fuel::reset()
{
    char_net_fuel_gC = 0.0;

    dlm_livegrass = 0.0;
    dlm_1hr       = 0.0;

    daily_fire_intensity = 0.0;
    MoE                  = 0.0;
    alpha_deadfuel       = 0.0;

    // patch level fuel characteristics
    char_FBD   = 0.0; // kgDM/m^3
    char_MoE   = 0.0;
    char_sigma = 0.0;
    char_alpha = 0.0;
    char_dlm   = 0.0;

    fdi = 0.0;
    effective_wind    = 0.0;
    length_to_breadth = 0.0;

    ros_f = 0.0;
    ros_b = 0.0;
    df    = 0.0;
    db    = 0.0;

    fire_durat = 0.0;
    gamma_f    = 0.0;
    mw_weight  = 0.0;
    char_F     = 0.0; // this is weighted by FPC not fuels classes

    scorch_height    = 0.0;
    fuel_consum_grass_c = 0.0;
    fuel_consum_grass_n = 0.0;
    fuel_consum_litter_c = 0.0;
    fuel_consum_litter_n = 0.0;
}


FuelLit::FuelLit()
{
    reset();
}
void
FuelLit::reset()
{
    f_1hr_fol_gC = 0.0;
    f_1hr_sap_gC = 0.0;
    f_10hr_sap_gC = 0.0;
    f_100hr_sap_gC = 0.0;
    f_1000hr_sap_gC = 0.0;

    f_1hr_fol_gN = 0.0;
    f_1hr_sap_gN = 0.0;
    f_10hr_sap_gN = 0.0;
    f_100hr_sap_gN = 0.0;
    f_1000hr_sap_gN = 0.0;

    deadfuel_gC = 0.0;
    net_deadfuel_gC = 0.0;
}



FuelWoodVT::FuelWoodVT()
{
    reset();
}
void
FuelWoodVT::reset()
{
    f_consumed_leaf_gC = 0.0;
    f_consumed_sapwood_gC = 0.0;
    f_consumed_heartwood_gC = 0.0;

    f_consumed_leaf_gN = 0.0;
    f_consumed_sapwood_gN = 0.0;
    f_consumed_heartwood_gN = 0.0;
}



FuelGrassVT::FuelGrassVT()
{
    reset();
}
void
FuelGrassVT::reset()
{
    f_livegr_gC = 0.0;
    f_livegr_gN = 0.0;
    f_consumed_livegr_gC = 0.0;
    f_consumed_livegr_gN = 0.0;
    f_frac_consumed_livegr = 0.0;
}

} /*namespace ldndc*/

