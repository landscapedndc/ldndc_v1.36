/*!
 * @brief
 *    dumping spitfires output
 *
 * @author
 */

#include  "output-spitfire.h"

#define  LMOD_OUTPUT_MODULE_BASE  NoneTypeModuleBase
#define  LMOD_OUTPUT_MODULE_NAME  EventHandlerSpitfireOutputWriter
#define  LMOD_OUTPUT_MODULE_TIMEMODE  TMODE_POST_DAILY

LMOD_MODULE_INFO(LMOD_OUTPUT_MODULE_NAME,LMOD_OUTPUT_MODULE_TIMEMODE,LMOD_FLAG_USER|LMOD_FLAG_OUTPUT);
namespace ldndc {

ldndc_string_t const  EventHandlerSpitfireOutputWriter_Header[] =
{
    "burnedarea",
    "nesterov",
    "fire_intensity",
    "ros_forward",
    "ros_backward",
    "fire_height",
    "dC_burnlitter",
    "dC_burngrass",
    "dC_burntrees",
    "dC_co",
    "dC_co2",
    "dC_ch4",
    "dC_ovc",
    "dC_tpm",
    "dC_blackcarbon",
    "dC_pyrolitter",
    "dN_nox",
    "dN_n2o",
    "dN_n2",
    "dN_pyrolitter",
    "dN_nh4",
    "dN_no3"
};
ldndc_string_t const *  EventHandlerSpitfireOutputWriter_Ids =
    EventHandlerSpitfireOutputWriter_Header;


#define  EventHandlerSpitfireOutputWriter_Datasize  (sizeof( EventHandlerSpitfireOutputWriter_Header) / sizeof( EventHandlerSpitfireOutputWriter_Header[0]))
ldndc_output_size_t const  EventHandlerSpitfireOutputWriter_Sizes[] =
{
    EventHandlerSpitfireOutputWriter_Datasize,

    EventHandlerSpitfireOutputWriter_Datasize /*total size*/
};
ldndc_output_size_t const *  EventHandlerSpitfireOutputWriter_EntitySizes = NULL;
#define  EventHandlerSpitfireOutputWriter_Rank  ((ldndc_output_rank_t)(sizeof( EventHandlerSpitfireOutputWriter_Sizes) / sizeof( EventHandlerSpitfireOutputWriter_Sizes[0])) - 1)
atomic_datatype_t const  EventHandlerSpitfireOutputWriter_Types[] =
{
    LDNDC_FLOAT64
};


LMOD_OUTPUT_MODULE_NAME::LMOD_OUTPUT_MODULE_NAME(
                MoBiLE_State *  _state,
               cbm::io_kcomm_t *  _io,
                timemode_e  _timemode)
                : LMOD_OUTPUT_MODULE_BASE( _state, _timemode),
                  io_kcomm( _io)
{
}


LMOD_OUTPUT_MODULE_NAME::~LMOD_OUTPUT_MODULE_NAME()
{
}

size_t
LMOD_OUTPUT_MODULE_NAME::record_size()
const
{
    return  EventHandlerSpitfireOutputWriter_Datasize;
}

lerr_t
LMOD_OUTPUT_MODULE_NAME::configure(
                ldndc::config_file_t const *  _cf)
{
    if ( _cf && !_cf->have_output())
    {
        return  LDNDC_ERR_OK;
    }

    this->sink_hdl = this->io_kcomm->sink_handle_acquire( "spitfiredaily");
    if ( this->sink_hdl.status() == LDNDC_ERR_OK)
    {
        lerr_t  rc_layout = LDNDC_OUTPUT_DEFINE_SINK_LAYOUT(this->sink_hdl,
                EventHandlerSpitfireOutputWriter,RM_CLIENTID|RM_DATETIME);
        RETURN_IF_NOT_OK(rc_layout);
    }
    else
    {
        KLOGERROR( "sink status bad  [sink=","spitfiredaily",",status=",this->sink_hdl.status(),"]");
        return  this->sink_hdl.status();
    }

    return  LDNDC_ERR_OK;
}

lerr_t
LMOD_OUTPUT_MODULE_NAME::initialize()
{
        return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::solve()
{
    KLOGDEBUG( "spitfire record size=", this->record_size());
    KLOGFATAL( "do not use solve(), we rock with write_spitfire_output()");
    return  LDNDC_ERR_FAIL;

}
lerr_t
LMOD_OUTPUT_MODULE_NAME::write_spitfire_output(
        ldndc_flt64_t *  _data)
{
    if ( !_data)
    {
        /* we assume there is nothing to dump ... */
        return  LDNDC_ERR_OK;
    }

    /* output */
    void *  data[] = { _data};

    sink_fixed_record_t  rec;
    rec.data = data;
    rec.meta.t.reg.year = this->lclock()->year();
    rec.meta.t.reg.julianday = this->lclock()->yearday();

    sink_client_t  client;
    client.target_id = this->io_kcomm->object_id();

    return  this->sink_hdl.write_fixed_record( &client, &rec);
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::finalize()
{
    return  this->io_kcomm->sink_handle_release( &this->sink_hdl);
}

} /*namespace ldndc*/

#undef  LMOD_OUTPUT_MODULE_BASE
#undef  LMOD_OUTPUT_MODULE_NAME
#undef  LMOD_OUTPUT_MODULE_TIMEMODE

#undef  EventHandlerSpitfireOutputWriter_Rank
#undef  EventHandlerSpitfireOutputWriter_Datasize

