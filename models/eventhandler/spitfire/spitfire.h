/*!
 * @brief
 *      SpitFire fire model
 *
 * @author
 *      christian werner (created on: may 13, 2013),
 */

#ifndef  LMOD_EVENTHANDLER_SPITFIRE_H_
#define  LMOD_EVENTHANDLER_SPITFIRE_H_

#include  "modules/nonetypemodule.h"
#include  "eventhandler/spitfire/output-spitfire.h"
#include  "modules/common/speciesinit.h"
#include  <map>

namespace ldndc {

// was patch.flux in LPJ-Guess


struct  Flux
{
    double pyro_flux_CO2, pyro_flux_CO, pyro_flux_CH4, pyro_flux_VOC, pyro_flux_TPM;
    double pyro_flux_NOx, pyro_flux_N2O, pyro_flux_N2;
    double pyro_flux_C_BC, pyro_flux_C_LITTER, pyro_flux_N_LITTER, pyro_flux_N_NH4, pyro_flux_N_NO3;
    double fire_frac;

    Flux();
    void  reset();
};

// was patch.fuel in LPJ-Guess
struct  Fuel
{        
    double char_net_fuel_gC;
    
    double dlm_livegrass;
    double dlm_1hr;
        
    double daily_fire_intensity;
    double MoE;
    double alpha_deadfuel;
    
    // patch level fuel characteristics
    double char_FBD; // kgDM/m^3
    double char_MoE;
    double char_sigma;
    double char_alpha;
    double char_dlm;
    
    double fdi;
    double effective_wind;
    double length_to_breadth;
    
    double ros_f;
    double ros_b;
    double df;
    double db;
    
    double fire_durat;
    double gamma_f;
    double mw_weight;
    
    double tau_l;            // cw: this was created here, check if valid (init?)
    double scorch_height;   // cw: this was created here, check if valid (init?)
    
    double char_F; // this is weighted by FPC not fuels classes

    double fuel_consum_litter_c;
    double fuel_consum_grass_c;

    double fuel_consum_litter_n;
    double fuel_consum_grass_n;

    Fuel();
    void  reset();
};


struct  FuelLit
{
    double f_1hr_fol_gC, f_1hr_sap_gC, f_10hr_sap_gC, f_100hr_sap_gC, f_1000hr_sap_gC;
    double f_1hr_fol_gN, f_1hr_sap_gN, f_10hr_sap_gN, f_100hr_sap_gN, f_1000hr_sap_gN;
    double deadfuel_gC, net_deadfuel_gC;

    FuelLit();
    void  reset();

    double  sum_c_compounds() const
    {
        return (f_1hr_fol_gC
                + f_1hr_sap_gC
                + f_10hr_sap_gC
                + f_100hr_sap_gC
                + f_1000hr_sap_gC);
    }

    double  sum_n_compounds() const
    {
        return (f_1hr_fol_gN
                + f_1hr_sap_gN
                + f_10hr_sap_gN
                + f_100hr_sap_gN
                + f_1000hr_sap_gN);
    }
};


struct FuelGrassVT
{
    double f_livegr_gC;
    double f_livegr_gN;
    double f_consumed_livegr_gC;
    double f_consumed_livegr_gN;
    double f_frac_consumed_livegr;

    FuelGrassVT();
    void  reset();
};

struct  FuelWoodVT
{
    double f_consumed_leaf_gC;
    double f_consumed_sapwood_gC;
    double f_consumed_heartwood_gC;

    double f_consumed_leaf_gN;
    double f_consumed_sapwood_gN;
    double f_consumed_heartwood_gN;

    FuelWoodVT();
    void  reset();
    
    double  sum_c_compounds() const
    {
        return (f_consumed_sapwood_gC +
                f_consumed_heartwood_gC +
                f_consumed_leaf_gC);
    }

    double  sum_n_compounds() const
    {
        return (f_consumed_sapwood_gN +
                f_consumed_heartwood_gN +
                f_consumed_leaf_gN);
    }
};


class  substate_physiology_t;
class  substate_microclimate_t;
class  substate_soilchemistry_t;
class  substate_watercycle_t;
class  LDNDC_API  EventHandlerSpitFire  :  public  NoneTypeModuleBase
{
    LMOD_EXPORT_MODULE_INFO(EventHandlerSpitFire,"eventhandler:spitfire","EventHandler SpitFire");
    
    /*! Minimum Nesterov for a non-zero alpha_livegrass */
    static double const  MIN_NESTEROV;
    static const double  MINER_TOT; // total mineral content of litter
    static const double  H; // caloric heat content of litter
    static const double  SIGMA_1HR; //surface to volume content cm^2/cm^3 of different fuel classes
    static const double  SIGMA_10HR; //surface to volume content cm^2/cm^3 of different fuel classes
    static const double  SIGMA_100HR; //surface to volume content cm^2/cm^3 of different fuel classes
    static const double  SIGMA_1000HR; //surface to volume content cm^2/cm^3 of different fuel classes
    static const double  SIGMA_LIVEGRASS; //surface to volume content cm^2/cm^3 of different fuel classes
    static const double  MoE_livegrass; // moisture of extinction
    static const double  P1; // 1. parameter in SPITFIRE burned area calculation
    static const double  P2; //  2. parameter in SPITFIRE burned area calculation
    static const double  fbd_a;
    static const double  fbd_b; //scalar to transform FBD of 10hr(100hr) fuels in to 1hr
    static const double  alpha;
    static const double  alpha_1hr;
    static const double  alpha_10hr;
    static const double  alpha_100hr;
    static const double  MINER_DAMP;
    static const double  part_dens;
    
    static const double SAP_FRAC_FUEL_LIT_1HR;
    static const double SAP_FRAC_FUEL_LIT_10HR;
    static const double SAP_FRAC_FUEL_LIT_100HR;
    static const double SAP_FRAC_FUEL_LIT_1000HR;
    
public:
    EventHandlerSpitFire(
            MoBiLE_State *,
           cbm::io_kcomm_t *,
            timemode_e);
    ~EventHandlerSpitFire();
    
    lerr_t  configure( ldndc::config_file_t const *);

    lerr_t  solve();

    lerr_t  initialize();
    lerr_t  finalize();
    
    lerr_t  wake() { return  LDNDC_ERR_OK; }
    lerr_t  sleep() { return  LDNDC_ERR_OK; }
    
private:
    input_class_event_t const &  ev_;
    input_class_siteparameters_t const &  sipar_;
    input_class_soillayers_t const &  sl_;

    substate_microclimate_t const &  mc_;
    substate_physiology_t &  ph_;
    substate_soilchemistry_t &  sc_;
    substate_watercycle_t & wc_;
    LD_PlantFunctions  species_initializer_;

    Flux  flux;
    Fuel  fuel;
    FuelLit fuel_lit;

    void  SPITFIRE_balance_check(unsigned int /* identifier for start or end balance check */);

    lerr_t  harvest_fuel_wood_();
    lerr_t  plant_fuel_wood_();
    typedef  std::map< std::string, FuelWoodVT >  FuelWoodMap;
    FuelWoodMap  fuel_wood_vt;
    lerr_t  harvest_fuel_grass_();
    lerr_t  plant_fuel_grass_();
    typedef  std::map< std::string, FuelGrassVT >  FuelGrassMap;
    FuelGrassMap  fuel_grass_vt;

    double  tot_c_balance_1;
    double  tot_n_balance_1;
    double  Nesterov_index;
    double  Nesterov_index_old;
    double  alpha_livegrass;
    
private:
    /* we hide copy constructor and assignment
     * operator until we need them
     */
    EventHandlerSpitFire( EventHandlerSpitFire const &);
    EventHandlerSpitFire &  operator=( EventHandlerSpitFire const &);
    
    EventHandlerSpitfireOutputWriter  owriter;
    ldndc_flt64_t *  odata;
    lerr_t  write_output();
    
protected:

    double  get_fuel_grass();

    lerr_t  Nesterov();

    lerr_t  Rothermel();

    lerr_t  burn_trees();

    lerr_t  effective_wind();

    lerr_t  calculate_fuel();
    
    lerr_t  calculate_consumed_fuel();
    
    lerr_t  calculate_fire_intensity();

    lerr_t  write_emissions(
                            species_t const &  /* species */,
                            double const & /* burned carbon */,
                            double const & /* burned nitrogen */,
                            double & /* non burned carbon */,
                            double & /* non burned nitrogen */);

    lerr_t  calculate_emissions();

    lerr_t  update_burned_litter();
    lerr_t  update_burned_grass();
};

} /*namespace ldndc*/

#endif    /*  !LMOD_EVENTHANDLER_SPITFIRE_H_  */

