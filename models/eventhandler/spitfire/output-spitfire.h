/*!
 * @brief
 *
 * @author
 *    christian werner
 */

#ifndef  LDNDC_MOD_OUTPUT_EVENTHANDLER_SPITFIRE_H_
#define  LDNDC_MOD_OUTPUT_EVENTHANDLER_SPITFIRE_H_

#include  "output/output-base.h"
#include  "nonetypemodule.h"

#define  LMOD_OUTPUT_MODULE_BASE  NoneTypeModuleBase
#define  LMOD_OUTPUT_MODULE_NAME  EventHandlerSpitfireOutputWriter
#define  LMOD_OUTPUT_MODULE_ID    "output:eventhandler:spitfire"
#define  LMOD_OUTPUT_MODULE_DESC  "Eventhandler Spitfire Output"
namespace ldndc {
class  LDNDC_API  LMOD_OUTPUT_MODULE_NAME  :  public  LMOD_OUTPUT_MODULE_BASE
{
    LMOD_EXPORT_MODULE_INFO(LMOD_OUTPUT_MODULE_NAME,LMOD_OUTPUT_MODULE_ID,LMOD_OUTPUT_MODULE_DESC);
    public:
        LMOD_OUTPUT_MODULE_NAME(
                MoBiLE_State *,
               cbm::io_kcomm_t *,
                timemode_e  _timemode);

        ~LMOD_OUTPUT_MODULE_NAME();


        lerr_t  configure( ldndc::config_file_t const *);

        lerr_t  initialize();

        lerr_t  solve();

        lerr_t  finalize();

        lerr_t  wake() { return  LDNDC_ERR_OK; }
        lerr_t  sleep() { return  LDNDC_ERR_OK; }


        lerr_t  write_spitfire_output(
                ldndc_flt64_t *);
        size_t  record_size() const;

    private:
        cbm::io_kcomm_t *  io_kcomm;

    private:
        ldndc::sink_handle_t  sink_hdl;
};
} /*namespace ldndc*/


#undef  LMOD_OUTPUT_MODULE_BASE
#undef  LMOD_OUTPUT_MODULE_NAME
#undef  LMOD_OUTPUT_MODULE_ID
#undef  LMOD_OUTPUT_MODULE_DESC

#endif  /*  !LDNDC_MOD_OUTPUT_EVENTHANDLER_SPITFIRE_H_  */

