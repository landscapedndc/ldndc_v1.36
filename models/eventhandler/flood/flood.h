
#ifndef  LMOD_EVENTHANDLER_FLOOD_H_
#define  LMOD_EVENTHANDLER_FLOOD_H_

#include  "mbe_legacymodel.h"
#include  "state/mbe_state.h"
#include  "ld_eventqueue.h"

namespace ldndc {


class  LDNDC_API  EventHandlerFlood  :  public  MBE_LegacyModel
{
    LMOD_EXPORT_MODULE_INFO(EventHandlerFlood,"eventhandler:flood","EventHandler Flood");

    public:
        EventHandlerFlood( MoBiLE_State *,
               cbm::io_kcomm_t *, timemode_e);
        ~EventHandlerFlood();

        lerr_t  configure( ldndc::config_file_t const *) { return  LDNDC_ERR_OK; }
        lerr_t  register_ports( cbm::io_kcomm_t *);
        lerr_t  initialize() { return  LDNDC_ERR_OK; }

        lerr_t  solve();
        lerr_t  unregister_ports( cbm::io_kcomm_t *);
        lerr_t  finalize() { return  LDNDC_ERR_OK; }

        lerr_t  wake() { return  LDNDC_ERR_OK; }
        lerr_t  sleep() { return  LDNDC_ERR_OK; }


        double get_bund_height() const { return bund_height; };

        double get_irrigation_amount() const { return irrigation_amount; };

        double get_irrigation_height() const { return irrigation_height; };

        double get_maximum_percolation() const { return maximum_percolation; };

        double get_water_table() const { return water_table; };

        bool have_unlimited_water() const { return unlimited_water; };

        double get_saturation_level() const { return saturation_level; };

        double get_soil_depth() const { return soil_depth; };

    private:
        substate_soilchemistry_t const * const sc;

        EventQueue  m_FloodEvents;
        CBM_Handle  m_FloodHandle;

        double bund_height;
        double irrigation_amount;
        double irrigation_height;
        double maximum_percolation;
        double water_table;
        bool unlimited_water;
        double saturation_level;
        double soil_depth;

    protected:
        virtual lerr_t  flood();
};

} /*namespace ldndc*/

#endif    /*  !LMOD_EVENTHANDLER_FLOOD_H_  */

