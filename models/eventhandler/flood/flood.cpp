/*!
 * @brief
 *    flood event handler (implemention)
 */

#include  "eventhandler/flood/flood.h"

#include  <constants/lconstants-conv.h>
#include  <logging/cbm_logging.h>

LMOD_MODULE_INFO(EventHandlerFlood,TMODE_SUBDAILY|TMODE_PRE_DAILY|TMODE_POST_DAILY,LMOD_FLAG_USER);
namespace ldndc {



EventHandlerFlood::EventHandlerFlood(
                         MoBiLE_State *  _state,
                         cbm::io_kcomm_t *, timemode_e  _timemode):
                            MBE_LegacyModel( _state, _timemode),
                            sc( _state->get_substate< substate_soilchemistry_t >()),
                            bund_height( 0.0),
                            irrigation_amount( invalid_dbl),
                            irrigation_height( invalid_dbl),
                            maximum_percolation( invalid_dbl),
                            water_table( invalid_dbl),
                            unlimited_water( true),
                            saturation_level( invalid_dbl),
                            soil_depth( invalid_dbl)
{
}



EventHandlerFlood::~EventHandlerFlood()
{
}



lerr_t
EventHandlerFlood::solve()
{
    return flood();
}



/* Irrigation
 * Daily precipitation is increased by irrigation.
 */
lerr_t
EventHandlerFlood::flood()
{
    bund_height = 0.0;
    irrigation_amount = invalid_dbl;
    irrigation_height = invalid_dbl;
    maximum_percolation = invalid_dbl;
    water_table = invalid_dbl;
    unlimited_water = true;
    saturation_level = invalid_dbl;
    soil_depth = invalid_dbl;

    size_t water_counter( 0);
    size_t bund_height_counter( 0);
    size_t irrigation_height_counter( 0);
    size_t irrigation_amount_counter( 0);
    size_t saturation_level_counter( 0);
    size_t soil_depth_counter( 0);
    size_t event_counter( 0);
    
    double water_table_sum( 0.0);
    double bund_height_sum( 0.0);
    double percolation_rate_sum( 0.0);
    double irrigation_height_sum( 0.0);
    double irrigation_amount_sum( 0.0);
    bool unlimited_water_all( true);
    double saturation_level_sum( 0.0);
    double soil_depth_sum( 0.0);
    
    while ( this->m_FloodEvents)
    {
        EventAttributes  ev_flood = m_FloodEvents.pop();

        double water_table_ev( ev_flood.get( "/watertable", invalid_dbl));
        if ( cbm::is_valid( water_table_ev))
        {
            water_table_ev = water_table_ev * cbm::M_IN_MM;
            water_table_sum += water_table_ev;
            water_counter += 1;
        }
        
        double bund_height_ev( ev_flood.get( "/bund-height", invalid_dbl));
        if ( cbm::is_valid( bund_height_ev))
        {
            bund_height_ev = bund_height_ev * cbm::M_IN_MM;
            bund_height_sum += bund_height_ev;
            bund_height_counter += 1;
        }

        double percolation_rate_ev( ev_flood.get( "/percolation-rate", invalid_dbl));
        if ( cbm::is_valid( percolation_rate_ev))
        {
            percolation_rate_ev = percolation_rate_ev * cbm::M_IN_MM;
        }
        else
        {
            /*!
             *  Formula has been derive from Razavipour @cite razavipour:2014a
             */
            double const a( 0.25);
            double const b( 0.0025);
            double const c( 0.09);
            percolation_rate_ev = (a + b * exp( c * (sc->sand_sl[0] * 100.0))) * cbm::MM_IN_CM * cbm::M_IN_MM;
        }
        percolation_rate_sum += percolation_rate_ev;

        double irrigation_height_ev( ev_flood.get( "/irrigation-height", invalid_dbl));
        if ( cbm::flt_greater_zero( irrigation_height_ev))
        {
            irrigation_height_ev = irrigation_height_ev * cbm::M_IN_MM;
            irrigation_height_sum += irrigation_height_ev;
            irrigation_height_counter += 1;
        }

        double irrigation_amount_ev( ev_flood.get( "/irrigation-amount", invalid_dbl));
        if ( cbm::flt_greater_zero( irrigation_amount_ev))
        {
            irrigation_amount_ev = irrigation_amount_ev * cbm::M_IN_MM;
            irrigation_amount_sum += irrigation_amount_ev;
            irrigation_amount_counter += 1;
        }

        //multiple flooding events: false is dominant
        bool const unlimited_water_ev( ev_flood.get( "/unlimited-water", true));
        if ( unlimited_water_ev == false)
        {
            unlimited_water_all = unlimited_water_ev;
        }

        double const saturation_level_ev( ev_flood.get( "/saturation-level", invalid_dbl));
        if ( cbm::flt_greater_zero( saturation_level_ev))
        {
            saturation_level_sum += saturation_level_ev;
            saturation_level_counter += 1;
        }

        double const soil_depth_ev( ev_flood.get( "/soil-depth", invalid_dbl));
        if ( cbm::flt_greater_zero( soil_depth_ev))
        {
            soil_depth_sum += soil_depth_ev;
            soil_depth_counter += 1;
        }

        event_counter++;
    }

    if ( event_counter > 0)
    {
        if ( water_counter > 0)
        {
            double const scale( 1.0 / ((double)water_counter));
            water_table = water_table_sum * scale;
        }
        else { water_table = invalid_dbl; }
        
        if ( bund_height_counter > 0)
        {
            double const scale( 1.0 / ((double)bund_height_counter));
            bund_height = bund_height_sum * scale;
        }
        else { bund_height = invalid_dbl; }
        
        if ( irrigation_height_counter > 0)
        {
            double const scale( 1.0 / ((double)irrigation_height_counter));
            irrigation_height = irrigation_height_sum * scale;
        }
        else { irrigation_height = invalid_dbl; }
        
        if ( irrigation_amount_counter > 0)
        {
            double const scale( 1.0 / ((double)irrigation_amount_counter));
            irrigation_amount = irrigation_amount_sum * scale;
        }
        else { irrigation_amount = invalid_dbl; }

        {
            double const scale( 1.0 / ((double)event_counter));
            maximum_percolation = percolation_rate_sum * scale;
            unlimited_water     = unlimited_water_all;
        }

        if ( cbm::flt_greater_zero( saturation_level_sum))
        {
            double const scale( 1.0 / ((double)saturation_level_counter));
            saturation_level = saturation_level_sum * scale;
        }
        else { saturation_level = invalid_dbl; }

        if ( cbm::flt_greater_zero( soil_depth_sum))
        {
            double const scale( 1.0 / ((double)soil_depth_counter));
            soil_depth = soil_depth_sum * scale;
        }
        else { soil_depth = invalid_dbl; }
    }

    return  LDNDC_ERR_OK;
}

static int  _QueueEventFlood( void const *  _msg, size_t  _msg_sz, void *  _queue)
{
    ldndc::EventQueue *  queue =
        static_cast< ldndc::EventQueue * >( _queue);
    if ( !queue)
        { return -1; }

    ldndc::EventAttributes  flood_event(
                "flood", (char const *)_msg, _msg_sz);
    queue->push( flood_event);
    return 0;
}

lerr_t
ldndc::EventHandlerFlood::register_ports( cbm::io_kcomm_t *  _io_kcomm)
{
    CBM_Callback  cb_flood;
    cb_flood.fn = &_QueueEventFlood;
    cb_flood.data = &this->m_FloodEvents;
    this->m_FloodHandle = _io_kcomm->subscribe_event( "flood", cb_flood);
    if ( !CBM_HandleOk(this->m_FloodHandle))
        { return  LDNDC_ERR_FAIL; }

    return  LDNDC_ERR_OK;
}

lerr_t
ldndc::EventHandlerFlood::unregister_ports( cbm::io_kcomm_t *  _io_kcomm)
{
    _io_kcomm->unsubscribe_event( this->m_FloodHandle);

    return  LDNDC_ERR_OK;
}

} /*namespace ldndc*/

