
#ifndef  LMOD_EVENTHANDLER_TILL_H_
#define  LMOD_EVENTHANDLER_TILL_H_

#include  "mbe_legacymodel.h"
#include  "state/mbe_state.h"
#include  "ld_eventqueue.h"
#include  "ld_isotopes.h"

namespace ldndc {


class  LDNDC_API  EventHandlerTill  :  public  MBE_LegacyModel
{
    /*! depth of litter distribution into the soil [m] */
    static const double  DEPTHMAX;
    
    LMOD_EXPORT_MODULE_INFO(EventHandlerTill,"eventhandler:till","EventHandler Till");
    
    public:
        EventHandlerTill( MoBiLE_State *,
               cbm::io_kcomm_t *, timemode_e);
        ~EventHandlerTill();
        
        lerr_t  configure( ldndc::config_file_t const *) { return  LDNDC_ERR_OK; }
        lerr_t  register_ports( cbm::io_kcomm_t *);
        lerr_t  unregister_ports( cbm::io_kcomm_t *);

        lerr_t  initialize() { return  LDNDC_ERR_OK; }
        lerr_t  solve();
        lerr_t  finalize() { return  LDNDC_ERR_OK; }

        lerr_t  wake() { return  LDNDC_ERR_OK; }
        lerr_t  sleep() { return  LDNDC_ERR_OK; }
        
    private:
        input_class_siteparameters_t const *  siteparam;
        input_class_soillayers_t const *  sl_;

        substate_physiology_t &  ph_;
        substate_soilchemistry_t &  sc_;
        substate_watercycle_t &  wc_;

        MoBiLE_PlantVegetation *  m_veg;

        EventQueue  m_TillEvents;
        CBM_Handle  m_TillHandle;

        double till_effect_decay_;

        lerr_t  m_till();
        
        Isotopes  isotope;
};

} /*namespace ldndc*/

#endif    /*  !LMOD_EVENTHANDLER_TILL_H_  */

