/*!
 * @brief
 *    till event
 *
 */

#include  "eventhandler/till/till.h"
#include  "soilchemistry/ld_allocatelitter.h"

#include  <input/siteparameters/siteparameters.h>
#include  <input/soillayers/soillayers.h>
#include  <input/species/species.h>

LMOD_MODULE_INFO(EventHandlerTill,TMODE_SUBDAILY|TMODE_PRE_DAILY|TMODE_POST_DAILY,LMOD_FLAG_USER);

const double  ldndc::EventHandlerTill::DEPTHMAX = 0.04;

ldndc::EventHandlerTill::EventHandlerTill(
                                          MoBiLE_State *  _state,
                                          cbm::io_kcomm_t *  _io_kcomm,
                                          timemode_e  _timemode):
                                    MBE_LegacyModel( _state, _timemode),
                                    siteparam( _io_kcomm->get_input_class< input_class_siteparameters_t >()),
                                    sl_( _io_kcomm->get_input_class< input_class_soillayers_t >()),
                                    ph_( _state->get_substate_ref< substate_physiology_t >()),
                                    sc_( _state->get_substate_ref< substate_soilchemistry_t >()),
                                    wc_( _state->get_substate_ref< substate_watercycle_t >()),
                                    m_veg( &_state->vegetation)
{
    till_effect_decay_ = 1.0;
    this->isotope.io_kcomm = _io_kcomm;
}



ldndc::EventHandlerTill::~EventHandlerTill()
{
}



lerr_t
ldndc::EventHandlerTill::solve()
{
    lerr_t  rc_till = this->m_till();
    if ( rc_till)
        { return LDNDC_ERR_FAIL; }
    return  LDNDC_ERR_OK;
}



#define  ALLOCATE_LITTER_PARTS(__c__,__c_scale__,__n__)                                                \
{                                                                                                      \
double  c( __c__ * __c_scale__);                                                                       \
double  n( __n__);                                                                                     \
                                                                                                       \
allocate_litter( c, n, siteparam->RCNRVL(), siteparam->RCNRL(), siteparam->RCNRR(), add_c, n_diff);    \
sc_.C_lit1_sl[0] += add_c[CPOOL_VL];                                                                   \
sc_.C_lit2_sl[0] += add_c[CPOOL_L];                                                                    \
sc_.C_lit3_sl[0] += add_c[CPOOL_R];                                                                    \
sc_.c_wood_sl[0] += add_c[CPOOL_B];                                                                    \
                                                                                                       \
if ( n_diff > 0.0)                                                                                     \
{                                                                                                      \
sc_.nh4_sl[0] += n_diff;                                                                               \
}                                                                                                      \
                                                                                                       \
/* clear pool */                                                                                       \
__c__ = 0.0;                                                                                           \
}                                                                                                      \


lerr_t
ldndc::EventHandlerTill::m_till()
{
    /* tilling effect decays
     *
     * note:
     * assumes that tilling effect in deeper layers
     * is at most equal to effect at surface layer
     */

    if ( cbm::flt_greater( sc_.till_effect_sl[0], 1.0))
    {
        for ( size_t  l = 0;  l < sl_->soil_layer_cnt();  ++l)
        {
            sc_.till_effect_sl[l] = cbm::bound_min( 1.0, till_effect_decay_ * sc_.till_effect_sl[l]);
        }
    }

    if ( this->m_TillEvents.is_empty())
        { return  LDNDC_ERR_OK; }

    if ( this->m_veg->size() > 0)
    {
        KLOGERROR( "tilling when there were living species (did you forget a harvest with \"remains=1.0\")");
        return  LDNDC_ERR_FAIL;
    }

    for ( size_t  sl = 0;  sl < sl_->soil_layer_cnt();  sl++)
    {
        this->isotope.cumulative_write(-sc_.C_lit1_sl[sl] / siteparam->RCNRVL()- sc_.C_lit2_sl[sl] / siteparam->RCNRL()- sc_.C_lit3_sl[sl] / siteparam->RCNRR(),"till_litter",sl);
        this->isotope.cumulative_write(-sc_.nh4_sl[sl],"till_nh4",sl);
    }

    if ( this->m_veg)
    {
        double  add_c[CPOOL_CNT];
        double  n_diff( 0.0);

        /* stubble incorporation */

        double const c_stubble( sc_.c_stubble_lit1 + sc_.c_stubble_lit2 + sc_.c_stubble_lit3);
        double const n_stubble( sc_.n_stubble_lit1 + sc_.n_stubble_lit2 + sc_.n_stubble_lit3);

        allocate_litter( c_stubble, n_stubble, siteparam->RCNRVL(), siteparam->RCNRL(), siteparam->RCNRR(), add_c, n_diff);
        sc_.C_lit1_sl[0] += add_c[CPOOL_VL];
        sc_.C_lit2_sl[0] += add_c[CPOOL_L];
        sc_.C_lit3_sl[0] += add_c[CPOOL_R];
        sc_.c_wood_sl[0] += add_c[CPOOL_B];
        sc_.nh4_sl[0] += n_diff;

        sc_.c_stubble_lit1 = 0.0;
        sc_.c_stubble_lit2 = 0.0;
        sc_.c_stubble_lit3 = 0.0;

        sc_.n_stubble_lit1 = 0.0;
        sc_.n_stubble_lit2 = 0.0;
        sc_.n_stubble_lit3 = 0.0;

        for ( PlantIterator vt = this->m_veg->begin(); vt != this->m_veg->end(); ++vt)
        {
            MoBiLE_Plant *p = *vt;
            // all biomass parts are incorporated at tilling
            if ( cbm::flt_greater_zero( p->mBud))
            {
                // not incorporated at date of harvest!
                ALLOCATE_LITTER_PARTS( p->mBud, cbm::CCDM, p->n_bud());
            }

            if ( cbm::flt_greater_zero( p->mFol))
            {
                // not incorporated at date of harvest!
                ALLOCATE_LITTER_PARTS( p->mFol, cbm::CCDM, p->n_fol());
            }

            if ( cbm::flt_greater_zero( p->mSap))
            {
                // not incorporated at date of harvest!
                ALLOCATE_LITTER_PARTS( p->mSap, cbm::CCDM, p->n_sap());
            }

            //    all roots are incorporated at tilling;
            if ( cbm::flt_greater_zero( p->mFrt))
            {
                double const root_c( p->mFrt * cbm::CCDM);
                double const root_n( p->n_frt());

                allocate_litter( root_c, root_n, siteparam->RCNRVL(), siteparam->RCNRL(), siteparam->RCNRR(), add_c, n_diff);

                for ( size_t  sl = 0;  sl < sl_->soil_layer_cnt();  sl++)
                {
                    sc_.C_lit1_sl[sl] += add_c[CPOOL_VL] * (*vt)->fFrt_sl[sl];
                    sc_.C_lit2_sl[sl] += add_c[CPOOL_L] * (*vt)->fFrt_sl[sl];
                    sc_.C_lit3_sl[sl] += add_c[CPOOL_R] * (*vt)->fFrt_sl[sl];
                    sc_.c_wood_sl[sl] += add_c[CPOOL_B] * (*vt)->fFrt_sl[sl];
                    sc_.nh4_sl[sl] += n_diff * (*vt)->fFrt_sl[sl];

                    sc_.accumulated_c_litter_below_sl[sl] += root_c * p->fFrt_sl[sl];
                    sc_.accumulated_n_litter_below_sl[sl] += root_n * p->fFrt_sl[sl];
                }
            }
        }
    }

    for ( size_t  sl = 0;  sl < sl_->soil_layer_cnt();  sl++)
    {
        this->isotope.cumulative_write(sc_.C_lit1_sl[sl] / siteparam->RCNRVL()+ sc_.C_lit2_sl[sl] / siteparam->RCNRL()+ sc_.C_lit3_sl[sl] / siteparam->RCNRR(),"till_litter",sl);
        this->isotope.cumulative_write(sc_.nh4_sl[sl],"till_nh4",sl);
    }

    for ( size_t  sl = 0;  sl < sl_->soil_layer_cnt();  sl++)
    {
        this->isotope.cumulative_write(-sc_.C_lit1_sl[sl] / siteparam->RCNRVL() - sc_.C_lit2_sl[sl] / siteparam->RCNRL()
                    - sc_.C_lit3_sl[sl] / siteparam->RCNRR(),"littertilled",sl);
        this->isotope.cumulative_write((-sc_.C_aorg_sl[sl] * siteparam->RBO() / siteparam->RCNB()
                    - sc_.C_aorg_sl[sl] * (1.0 - siteparam->RBO()) / siteparam->RCNH()),"Naorgtilled",sl);
        this->isotope.cumulative_write(-sc_.N_hum_sl[sl],"humustilled",sl);
        this->isotope.cumulative_write(-sc_.no3_sl[sl],"no3tilled",sl);
        this->isotope.cumulative_write(-sc_.an_no3_sl[sl],"anno3tilled",sl);
        this->isotope.cumulative_write(-sc_.no2_sl[sl],"no2tilled",sl);
        this->isotope.cumulative_write(-sc_.an_no2_sl[sl],"anno2tilled",sl);
        this->isotope.cumulative_write(-sc_.nh4_sl[sl],"nh4tilled",sl);
        this->isotope.cumulative_write(-sc_.clay_nh4_sl[sl],"claynh4tilled",sl);
        this->isotope.cumulative_write(-sc_.urea_sl[sl],"ureatilled",sl);
        this->isotope.cumulative_write(-sc_.nh3_liq_sl[sl],"nh3tilled",sl);
        this->isotope.cumulative_write(-sc_.C_micro1_sl[sl] / siteparam->RCNB(),"micro1tilled",sl);
        this->isotope.cumulative_write(-sc_.C_micro3_sl[sl] / siteparam->RCNB(),"micro3tilled",sl);
    }

    // sk:??  how is this supposed to work for multiple till events?
    while ( this->m_TillEvents)
    {
        EventAttributes  ev_till = this->m_TillEvents.pop();

        // tilling depth must be smaller than 0.5m
        double  till_depth( ev_till.get( "/depth", 0.0));
        //KLOGINFO( "tilling depth=", till_depth, "m, time=",lclock_ref().to_string(),"]");
        if ( till_depth > 0.5)
        {
            KLOGINFO( "tilling depth greater than 0.5m, reset to 0.5m [depth=",till_depth, "m,time=",lclock_ref().to_string(),"]");
            till_depth = 0.5;
        }
        double const  till_effect_init( 1.5 + siteparam->TFMAX() * till_depth);
        till_effect_decay_ = pow( till_effect_init, -1.0/siteparam->TFDAY());

        size_t  tilq( sl_->soil_layer_cnt());
        for ( size_t  l = 0;  l < sl_->soil_layer_cnt();  ++l)
        {
            sc_.till_effect_sl[l] = till_effect_init;

            if (( sc_.depth_sl[l]) > till_depth)
            {
                tilq = l;
                break;
            }
        }
        
        if ( tilq == 0)
        {
            KLOGINFO( "tilling ... [depth=",till_depth, "m,time=",lclock_ref().now().to_string(),"]");
            KLOGINFO( "tilling has no effect due to soil layer discretization and till depth");
            continue;
        }

        // rearranging soil pools after tilling
        lvector_t< double >  pool_buf[] = { sc_.C_lit1_sl, sc_.C_lit2_sl, sc_.C_lit3_sl,
                                            sc_.C_aorg_sl, sc_.C_hum_sl, sc_.N_hum_sl,
                                            sc_.doc_sl, sc_.an_doc_sl,
                                            sc_.no3_sl, sc_.an_no3_sl,
                                            sc_.no2_sl, sc_.an_no2_sl,
                                            sc_.nh4_sl, sc_.clay_nh4_sl, sc_.urea_sl, sc_.nh3_gas_sl, sc_.nh3_liq_sl,
                                            wc_.wc_sl,
                                            sc_.C_micro1_sl, sc_.C_micro2_sl, sc_.C_micro3_sl};

        size_t const  pools_cnt( sizeof( pool_buf) / sizeof(lvector_t< double >));

        // - distributing these pools equally throughout tilling depth
        for ( size_t  k = 0;  k < pools_cnt;  ++k)
        {
            // summing up pool down to tilling depth
            double  pool_sum( pool_buf[k].sum( tilq) / sc_.depth_sl[tilq-1]);

            for ( size_t  l = 0;  l < tilq;  l++)
            {
                pool_buf[k][l] = pool_sum * sc_.h_sl[l];
            }
        }
    }

    for ( size_t  sl = 0;  sl < sl_->soil_layer_cnt();  sl++)
    {
        this->isotope.cumulative_write(sc_.C_lit1_sl[sl] / siteparam->RCNRVL() + sc_.C_lit2_sl[sl] / siteparam->RCNRL()
                    + sc_.C_lit3_sl[sl] / siteparam->RCNRR(),"littertilled",sl);
        this->isotope.cumulative_write((sc_.C_aorg_sl[sl] * siteparam->RBO() / siteparam->RCNB()
                    + sc_.C_aorg_sl[sl] * (1.0 - siteparam->RBO()) / siteparam->RCNH()),"Naorgtilled",sl);
        this->isotope.cumulative_write(sc_.N_hum_sl[sl],"humustilled",sl);
        this->isotope.cumulative_write(sc_.no3_sl[sl],"no3tilled",sl);
        this->isotope.cumulative_write(sc_.an_no3_sl[sl],"anno3tilled",sl);
        this->isotope.cumulative_write(sc_.no2_sl[sl],"no2tilled",sl);
        this->isotope.cumulative_write(sc_.an_no2_sl[sl],"anno2tilled",sl);
        this->isotope.cumulative_write(sc_.nh4_sl[sl],"nh4tilled",sl);
        this->isotope.cumulative_write(sc_.clay_nh4_sl[sl],"claynh4tilled",sl);
        this->isotope.cumulative_write(sc_.urea_sl[sl],"ureatilled",sl);
        this->isotope.cumulative_write(sc_.nh3_liq_sl[sl],"nh3tilled",sl);
        this->isotope.cumulative_write(sc_.C_micro1_sl[sl] / siteparam->RCNB(),"micro1tilled",sl);
        this->isotope.cumulative_write(sc_.C_micro3_sl[sl] / siteparam->RCNB(),"micro3tilled",sl);
    }

    return  LDNDC_ERR_OK;
}

static int  _QueueEventTill( void const *  _msg, size_t  _msg_sz, void *  _queue)
{
    ldndc::EventQueue *  queue =
        static_cast< ldndc::EventQueue * >( _queue);
    if ( !queue)
        { return -1; }

    EventAttributes  till_event(
                "till", (char const *)_msg, _msg_sz);
    queue->push( till_event);
    return 0;
}

lerr_t
ldndc::EventHandlerTill::register_ports( cbm::io_kcomm_t *  _io_kcomm)
{
    CBM_Callback  cb_till;
    cb_till.fn = &_QueueEventTill;
    cb_till.data = &this->m_TillEvents;
    this->m_TillHandle = _io_kcomm->subscribe_event( "till", cb_till);
    if ( !CBM_HandleOk(this->m_TillHandle))
        { return  LDNDC_ERR_FAIL; }

    return  LDNDC_ERR_OK;
}

lerr_t
ldndc::EventHandlerTill::unregister_ports( cbm::io_kcomm_t *  _io_kcomm)
{
    _io_kcomm->unsubscribe_event( this->m_TillHandle);

    return  LDNDC_ERR_OK;
}
