
#ifndef  LM_EVENTHANDLER_GRAZE_H_
#define  LM_EVENTHANDLER_GRAZE_H_

#include  "mbe_legacymodel.h"
#include  "state/mbe_state.h"

#include  "ld_eventqueue.h"
#include  "ld_isotopes.h"

namespace  ldndc {

    namespace event {
        class  EventGraze; }

class  LDNDC_API  EventHandlerGraze  :  public  MBE_LegacyModel
{
    LMOD_EXPORT_MODULE_INFO(EventHandlerGraze,"eventhandler:graze","EventHandler Graze");
    public:
        EventHandlerGraze( MoBiLE_State *,
               cbm::io_kcomm_t *, timemode_e);
        ~EventHandlerGraze();
        
        lerr_t  configure( ldndc::config_file_t const *) { return  LDNDC_ERR_OK; }
        lerr_t  register_ports( cbm::io_kcomm_t *);
        lerr_t  initialize() { return  LDNDC_ERR_OK; }
        
        lerr_t  solve();
        lerr_t  unregister_ports( cbm::io_kcomm_t *);
        lerr_t  finalize() { return  LDNDC_ERR_OK; }

        lerr_t  wake() { return  LDNDC_ERR_OK; }
        lerr_t  sleep() { return  LDNDC_ERR_OK; }

    public:

        /*!
         * @brief
         *    handle graze event. the method checks itself
         *    whether a graze event is pending.
         *
         * @param
         *    species
         *
         * @return
         *    LDNDC_ERR_OK if no graze event is pending
         *    or everything went well.
         *
         *    ...
         */
        lerr_t  event_graze_physiology( MoBiLE_Plant *);

        void update_available_freshfood_c(
                MoBiLE_PlantVegetation *  _veg,
                species_groups_selector_t const &);

        void reset_daily_food_consumption();

    private:
        cbm::io_kcomm_t *  io_kcomm;
        
        input_class_soillayers_t const *  sl_;
        input_class_siteparameters_t const &  sipar_;    
        substate_microclimate_t const &  mc_;
        substate_physiology_t *  ph_;
        substate_soilchemistry_t &  sc_;

        EventQueue  m_GrazeEvents;
        CBM_Handle  m_GrazeHandle;

        double total_available_freshfood_c;
        double day_food_consume_c;
    
    private:

        double get_available_freshfood_c( MoBiLE_Plant *);
        double get_available_freshfood_c_crop( MoBiLE_Plant *);
        double get_available_freshfood_c_wood( MoBiLE_Plant *);
        double get_available_freshfood_c_grass( MoBiLE_Plant *);

        double get_total_freshfood_c( MoBiLE_Plant *);
        double get_total_freshfood_c_crop( MoBiLE_Plant *);
        double get_total_freshfood_c_wood( MoBiLE_Plant *);
        double get_total_freshfood_c_grass( MoBiLE_Plant *);

        lerr_t  handle_graze( MoBiLE_Plant *,
                ldndc::EventAttributes const &);

        void  update_biomass_after_graze_(
                                          double * /*biomass dry weight*/,
                                          double * /*total loss carbon*/, double * /*total loss nitrogen*/,
                                          double /*loss carbon*/, double /*n/c ratio*/);

        Isotopes  isotope;
};

} /*namespace ldndc*/

#endif    /*  !LM_EVENTHANDLER_GRAZE_H_  */

