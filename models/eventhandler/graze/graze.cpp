/*!
 * @brief
 *    graze event
 *
 */

#include  "eventhandler/graze/graze.h"

#include  <soilchemistry/ld_allocatelitter.h>
#include  <constants/lconstants-conv.h>
#include  <constants/lconstants-math.h>

#include  <input/soillayers/soillayers.h>

#include  <logging/cbm_logging.h>

LMOD_MODULE_INFO(EventHandlerGraze,TMODE_SUBDAILY|TMODE_PRE_DAILY|TMODE_POST_DAILY,LMOD_FLAG_USER);

ldndc::EventHandlerGraze::EventHandlerGraze(
                                     MoBiLE_State *  _state,
                                    cbm::io_kcomm_t *  _io,
                                     timemode_e  _timemode)
: MBE_LegacyModel( _state, _timemode),
io_kcomm( _io),
sl_( _io->get_input_class< input_class_soillayers_t >()),
sipar_( _io->get_input_class_ref< input_class_siteparameters_t >()),
mc_( _state->get_substate_ref< substate_microclimate_t >()),
ph_( _state->get_substate< substate_physiology_t >()),
sc_( _state->get_substate_ref< substate_soilchemistry_t >())
{
    int  graze_timemode = TMODE_NONE;
    this->io_kcomm->get_scratch()->get( "producer.graze.timemode",
                                       &graze_timemode, graze_timemode);
    if (( graze_timemode != TMODE_NONE)
        && ( graze_timemode != _timemode))
    {
        KLOGWARN( "multiple grazing modules not synchronized.");
    }
    else
    {
        this->io_kcomm->get_scratch()->set(
                                           "producer.graze.timemode", (int)_timemode);
    }

    this->total_available_freshfood_c = 0.0;
    this->day_food_consume_c = 0.0;
    isotope.io_kcomm = this->io_kcomm;
}



ldndc::EventHandlerGraze::~EventHandlerGraze()
{
}



lerr_t
ldndc::EventHandlerGraze::solve()
{
    /* no op */
    return  LDNDC_ERR_OK;
}



double
ldndc::EventHandlerGraze::get_available_freshfood_c_crop( MoBiLE_Plant *  _vt)
{
    KLOGWARN( "grazing not possible for crop species \"", _vt->name(), "\"");
    return  invalid_dbl;
}



double
ldndc::EventHandlerGraze::get_available_freshfood_c_wood( MoBiLE_Plant *  _vt)
{
    KLOGWARN( "grazing not possible for wood species \"", _vt->name(), "\"");
    return  invalid_dbl;
}



double
ldndc::EventHandlerGraze::get_available_freshfood_c_grass( MoBiLE_Plant * _vt)
{
    ldndc_kassert( _vt->groupId() == SPECIES_GROUP_GRASS);
    double const  fresh_food_c( get_total_freshfood_c_grass( _vt));

    double const leftgrass_in_m2( sipar_.LEFTGRASS() * cbm::HA_IN_M2);
    if ( cbm::flt_greater( fresh_food_c, leftgrass_in_m2))
    {
        return fresh_food_c - leftgrass_in_m2;
    }
    else
    {
        return 0.0;
    }
}



double
ldndc::EventHandlerGraze::get_total_freshfood_c_crop( MoBiLE_Plant *  _vt)
{
    KLOGWARN( "grazing not possible for crop species \"", _vt->name(), "\"");
    return  invalid_dbl;
}



double
ldndc::EventHandlerGraze::get_total_freshfood_c_wood( MoBiLE_Plant *  _vt)
{
    KLOGWARN( "grazing not possible for wood species \"", _vt->name(), "\"");
    return  invalid_dbl;
}



double
ldndc::EventHandlerGraze::get_total_freshfood_c_grass( MoBiLE_Plant * _vt)
{
    ldndc_kassert( _vt->groupId() == SPECIES_GROUP_GRASS);
    if ( (*_vt)->TUBER())
    {
        return (_vt->mFol + _vt->dw_lst) * cbm::CCDM;
    }
    else
    {
        return (_vt->mFol + _vt->dw_lst + _vt->mBud) * cbm::CCDM;
    }
}



double
ldndc::EventHandlerGraze::get_available_freshfood_c( MoBiLE_Plant *  _vt)
{
    if ( _vt->groupId() == SPECIES_GROUP_GRASS)
    {
        return  this->get_available_freshfood_c_grass( _vt);
    }
    else if ( _vt->groupId() == SPECIES_GROUP_WOOD)
    {
        return  this->get_available_freshfood_c_wood( _vt);
    }
    else if ( _vt->groupId() == SPECIES_GROUP_CROP)
    {
        return  this->get_available_freshfood_c_crop( _vt);
    }

    KLOGWARN( "graze: unsupported species group",
             "  [group=",SPECIES_GROUP_NAMES[_vt->groupId()],"] for [",_vt->cname(),"]");
    return  invalid_dbl;
}



double
ldndc::EventHandlerGraze::get_total_freshfood_c( MoBiLE_Plant *  _vt)
{
    if ( _vt->groupId() == SPECIES_GROUP_GRASS)
    {
        return  this->get_total_freshfood_c_grass( _vt);
    }
    else if ( _vt->groupId() == SPECIES_GROUP_WOOD)
    {
        return  this->get_total_freshfood_c_wood( _vt);
    }
    else if ( _vt->groupId() == SPECIES_GROUP_CROP)
    {
        return  this->get_total_freshfood_c_crop( _vt);
    }

    KLOGWARN( "graze: unsupported species group",
             "  [group=",SPECIES_GROUP_NAMES[_vt->groupId()],"] for [",_vt->cname(),"]");
    return  invalid_dbl;
}



void
ldndc::EventHandlerGraze::reset_daily_food_consumption()
{
    this->day_food_consume_c = 0.0;
}



void
ldndc::EventHandlerGraze::update_available_freshfood_c(
                                            MoBiLE_PlantVegetation *  _veg,
                                            species_groups_selector_t const &  _groups)
{
    total_available_freshfood_c = 0.0;

    for ( PlantIterator  vt = _veg->begin(); vt != _veg->end(); ++vt)
    {
        if ( _groups.is_selected((*vt)->groupId()))
        {
            double const  fresh_food_c( this->get_available_freshfood_c( *vt));
            if (   cbm::flt_greater_zero( fresh_food_c))
            {
                total_available_freshfood_c += fresh_food_c;
            }
        }
    }
}


/*!
 * @page grazing_event
 *
 *  Observed data from Donna Giltrap in New Zealand
 *  - cattle:
 *    - Daily consumption: 4.6 kg C/head/day
 *  - horse:
 *    - Daily consumption: 3.0 kg C/head/day
 *  - sheep:
 *    - Daily consumption: 0.88 kg C/head/day
 *
 * In both cases the nitrogen in the dung is 40 perc. of the total N, the C/N
 * ratio for dung is 16 and the C/N ratio for urine is 0.5.
 */
lerr_t
ldndc::EventHandlerGraze::event_graze_physiology( MoBiLE_Plant *  _vt)
{
    if ( this->m_GrazeEvents.is_empty())
    { return LDNDC_ERR_EVENT_EMPTY_QUEUE; }

    while ( this->m_GrazeEvents)
    {
        EventAttributes  ev_graze = this->m_GrazeEvents.pop();
        this->handle_graze( _vt, ev_graze);
    }

    return LDNDC_ERR_EVENT_MATCH;
}



lerr_t
ldndc::EventHandlerGraze::handle_graze( MoBiLE_Plant *  vt,
                                        ldndc::EventAttributes const &  _event_graze)
{
    double const  grazing_hours = _event_graze.get( "/grazing-hours", 0.0);
    double const  head_count = _event_graze.get( "/head-count", 0.0);
    if (( grazing_hours > 0.0) && ( head_count > 0.0))
        { /* ok, proceed */ }
    else /* somewhat deactivated event: livestock not grazing or no livestock at all */
        { return  LDNDC_ERR_OK; }
    CBM_LogDebug( "Graze event: head-count=",head_count, "  grazing-hours=",grazing_hours);

    // check if we have fresh food available
    double const total_fresh_food_c( this->get_total_freshfood_c( vt));
    double const available_fresh_food_c( this->get_available_freshfood_c( vt));
    if ( !cbm::flt_greater_zero( available_fresh_food_c))
    {
        return  LDNDC_ERR_OK;
    }

    // food demand regarding all available species [kg C ha-1]
    double const demand_carbon = _event_graze.get( "/demand-carbon", 0.0);
    double const day_food_demand_c = cbm::HA_IN_M2 * head_count * demand_carbon * grazing_hours / 24.0;

    // check if we are already satisfied
    double const time_step_food_demand_c( day_food_demand_c - day_food_consume_c);
    if( !cbm::flt_greater_zero( time_step_food_demand_c))
    {
        return  LDNDC_ERR_OK;
    }

    cbm::state_scratch_t *  mcom = io_kcomm->get_scratch();
    double  grazed_mfru_c = 0.0, grazed_mfol_c = 0.0, grazed_mlst_c = 0.0;
    mcom->get( "graze:mfruC", &grazed_mfru_c);
    mcom->get( "graze:mfolC", &grazed_mfol_c);
    mcom->get( "graze:mlstC", &grazed_mlst_c);

    double  grazed_mfru_n = 0.0, grazed_mfol_n = 0.0, grazed_mlst_n = 0.0;
    mcom->get( "graze:mfruN", &grazed_mfru_n);
    mcom->get( "graze:mfolN", &grazed_mfol_n);
    mcom->get( "graze:mlstN", &grazed_mlst_n);

    // check if food demand is less than total available
    // if true then scale the amount of consumed carbon/biomass from current species by relative amount of biomass
    double const loss_factor( cbm::flt_greater_equal( total_available_freshfood_c, time_step_food_demand_c) ?
                              time_step_food_demand_c / total_fresh_food_c :
                              total_available_freshfood_c / total_fresh_food_c);

    /* Plant losses due to consumption processes */
    double const  fol_loss_c( cbm::CCDM * loss_factor * vt->mFol);
    double const  lst_loss_c( cbm::CCDM * loss_factor * vt->dw_lst);
    double const  fru_loss_c( vt->parameters()->TUBER() ?
                              0.0 :
                              cbm::CCDM * loss_factor * vt->mBud);

    double time_step_food_consume_c( vt->total_biomass() * cbm::CCDM);
    double time_step_food_consume_n( vt->total_nitrogen());

    update_biomass_after_graze_( &vt->mFol, &grazed_mfol_c, &grazed_mfol_n, fol_loss_c, vt->nc_fol());
    double const nc_lst( vt->nc_lst());
    update_biomass_after_graze_( &vt->dw_lst, &grazed_mlst_c, &grazed_mlst_n, lst_loss_c, vt->nc_lst());
    vt->n_lst -= lst_loss_c * nc_lst / cbm::CCDM;
    update_biomass_after_graze_( &vt->mBud, &grazed_mfru_c, &grazed_mfru_n, fru_loss_c, vt->nc_bud());

    time_step_food_consume_c -= vt->total_biomass() * cbm::CCDM;
    time_step_food_consume_n -= vt->total_nitrogen();

    day_food_consume_c += time_step_food_consume_c;
    ph_->accumulated_c_export_grazing += time_step_food_consume_c;
    ph_->accumulated_n_export_grazing += time_step_food_consume_n;

    mcom->set( "graze:mfruC", grazed_mfru_c);
    mcom->set( "graze:mfolC", grazed_mfol_c);
    mcom->set( "graze:mlstC", grazed_mlst_c);

    mcom->set( "graze:mfruN", grazed_mfru_n);
    mcom->set( "graze:mfolN", grazed_mfol_n);
    mcom->set( "graze:mlstN", grazed_mlst_n);

    /* Dung and urine release */
    double const deficit( cbm::bound( 0.0, time_step_food_consume_c / time_step_food_demand_c, 1.0));
    if ( cbm::flt_greater_zero( deficit))
    {
        cbm::state_scratch_t *  scratch = io_kcomm->get_scratch();

        double scale( deficit * head_count * grazing_hours / 24.0 * cbm::HA_IN_M2);
        double const dung_carbon( scale * _event_graze.get( "/dung-carbon", 0.0));
        double const dung_nitrogen( scale * _event_graze.get( "/dung-nitrogen", 0.0));
        double const urine_nitrogen( scale * _event_graze.get( "/urine-nitrogen", 0.0));

        sc_.c_dung += dung_carbon;
        sc_.n_dung += dung_nitrogen;
        sc_.n_urine += urine_nitrogen;

        scratch->set( "graze:dungcarbon", dung_carbon * cbm::M2_IN_HA);
        scratch->set( "graze:dungnitrogen", dung_nitrogen * cbm::M2_IN_HA);
        scratch->set( "graze:urinenitrogen", urine_nitrogen * cbm::M2_IN_HA);
    }
    return  LDNDC_ERR_OK;
}



void
ldndc::EventHandlerGraze::update_biomass_after_graze_(
                                               double *  _biomass_dw,
                                               double *  _total_loss_c,
                                               double *  _total_loss_n,
                                               double  _loss_c,
                                               double  _nc_biomass)
{
    if ( _loss_c < ( *_biomass_dw * cbm::CCDM))
    {
        *_total_loss_c += _loss_c;
        *_total_loss_n += _loss_c * _nc_biomass / cbm::CCDM;
        
        *_biomass_dw -= _loss_c / cbm::CCDM;
    }
    else
    {
        *_total_loss_c += *_biomass_dw * cbm::CCDM;
        *_total_loss_n += *_biomass_dw * _nc_biomass;
        
        *_biomass_dw = 0.0;
    }
}

static int  _QueueEventGraze( void const *  _msg, size_t  _msg_sz, void *  _queue)
{
    ldndc::EventQueue *  queue =
        static_cast< ldndc::EventQueue * >( _queue);
    if ( !queue)
        { return -1; }

    ldndc::EventAttributes  graze_event(
                "graze", (char const *)_msg, _msg_sz);
    queue->push( graze_event);
    return 0;
}

lerr_t
ldndc::EventHandlerGraze::register_ports( cbm::io_kcomm_t *  _io_kcomm)
{
    CBM_Callback  cb_graze;
    cb_graze.fn = &_QueueEventGraze;
    cb_graze.data = &this->m_GrazeEvents;
    this->m_GrazeHandle = _io_kcomm->subscribe_event( "graze", cb_graze);
    if ( !CBM_HandleOk(this->m_GrazeHandle))
        { return  LDNDC_ERR_FAIL; }

    return  LDNDC_ERR_OK;
}

lerr_t
ldndc::EventHandlerGraze::unregister_ports( cbm::io_kcomm_t *  _io_kcomm)
{
    _io_kcomm->unsubscribe_event( this->m_GrazeHandle);

    return  LDNDC_ERR_OK;
}

