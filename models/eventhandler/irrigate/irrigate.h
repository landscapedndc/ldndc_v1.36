

#ifndef  LMOD_EVENTHANDLER_IRRIGATE_H_
#define  LMOD_EVENTHANDLER_IRRIGATE_H_

#include  "mbe_legacymodel.h"
#include  "state/mbe_state.h"
#include  "ld_eventqueue.h"

namespace ldndc {


class  LDNDC_API  EventHandlerIrrigate  :  public  MBE_LegacyModel
{
    LMOD_EXPORT_MODULE_INFO(EventHandlerIrrigate,"eventhandler:irrigate","EventHandler Irrigate");
        
    public:
        EventHandlerIrrigate( MoBiLE_State *,
               cbm::io_kcomm_t *, timemode_e);
        ~EventHandlerIrrigate();

        lerr_t  configure( ldndc::config_file_t const *) { return  LDNDC_ERR_OK; }
        lerr_t  register_ports( cbm::io_kcomm_t *);
        lerr_t  initialize() { return  LDNDC_ERR_OK; }

        lerr_t  solve();
        lerr_t  unregister_ports( cbm::io_kcomm_t *);
        lerr_t  finalize() { return  LDNDC_ERR_OK; }

        lerr_t  wake() { return  LDNDC_ERR_OK; }
        lerr_t  sleep() { return  LDNDC_ERR_OK; }
        
    private:
        substate_watercycle_t *  wc;

        EventQueue  m_IrrigateEvents;
        CBM_Handle  m_IrrigateHandle;

    protected:
        virtual lerr_t  irrigate();
};

} /*namespace ldndc*/

#endif    /*  !LMOD_EVENTHANDLER_IRRIGATE_H_  */

