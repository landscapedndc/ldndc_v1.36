
#include  "eventhandler/irrigate/irrigate.h"

#include  <constants/lconstants-conv.h>
#include  <logging/cbm_logging.h>

LMOD_MODULE_INFO(EventHandlerIrrigate,TMODE_SUBDAILY|TMODE_PRE_DAILY|TMODE_POST_DAILY,LMOD_FLAG_USER);
namespace ldndc {



EventHandlerIrrigate::EventHandlerIrrigate(
        MoBiLE_State *  _state,
       cbm::io_kcomm_t *, timemode_e  _timemode)
        : MBE_LegacyModel( _state, _timemode),
          wc( _state->get_substate< substate_watercycle_t >())
{
}



EventHandlerIrrigate::~EventHandlerIrrigate()
{
}



lerr_t
EventHandlerIrrigate::solve()
{
    return  this->irrigate();
}



/* Irrigation
 * Daily precipitation is increased by irrigation.
 */
lerr_t
EventHandlerIrrigate::irrigate()
{
    if ( this->m_IrrigateEvents.is_empty())
    { return  LDNDC_ERR_OK; }

    lerr_t  rc = LDNDC_ERR_OK;

    while ( this->m_IrrigateEvents)
    {
        EventAttributes  ev_irri = this->m_IrrigateEvents.pop();
        double const  amount = ev_irri.get( "/amount", 0.0);

        // check if irrigation water is stored in reservoir
        if ( ev_irri.get( "/is-reservoir", false) == true)
        {
            //negative amount resets irrigation reservoir
            if ( !cbm::flt_greater_equal_zero( amount))
            {
                wc->irrigation_reservoir = 0.0;
            }
            else
            {
                wc->irrigation_reservoir += ( amount * cbm::M_IN_MM);
            }
        }
        else
        {
            if ( !cbm::flt_greater_equal_zero( amount))
            {
                KLOGERROR( "negative amount of irrigation water [amount=", amount,"]");
                rc = LDNDC_ERR_FAIL;
            }
            wc->accumulated_irrigation += amount * cbm::M_IN_MM;
        }
    }
    
    return  rc;
}

static int  _QueueEventIrrigate( void const *  _msg, size_t  _msg_sz, void *  _queue)
{
    ldndc::EventQueue *  queue =
        static_cast< ldndc::EventQueue * >( _queue);
    if ( !queue)
        { return -1; }

    ldndc::EventAttributes  irrigate_event(
                "irrigate", (char const *)_msg, _msg_sz);
    queue->push( irrigate_event);
    return 0;
}

lerr_t
ldndc::EventHandlerIrrigate::register_ports( cbm::io_kcomm_t *  _io_kcomm)
{
    CBM_Callback  cb_irrigate;
    cb_irrigate.fn = &_QueueEventIrrigate;
    cb_irrigate.data = &this->m_IrrigateEvents;
    this->m_IrrigateHandle = _io_kcomm->subscribe_event( "irrigate", cb_irrigate);
    if ( !CBM_HandleOk(this->m_IrrigateHandle))
        { return  LDNDC_ERR_FAIL; }

    return  LDNDC_ERR_OK;
}

lerr_t
ldndc::EventHandlerIrrigate::unregister_ports( cbm::io_kcomm_t *  _io_kcomm)
{
    _io_kcomm->unsubscribe_event( this->m_IrrigateHandle);

    return  LDNDC_ERR_OK;
}

} /*namespace ldndc*/

