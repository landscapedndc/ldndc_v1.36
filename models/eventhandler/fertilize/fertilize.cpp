 
#include  "eventhandler/fertilize/fertilize.h"
#include  "soilchemistry/ld_allocatelitter.h"

#include  <constants/cbm_const.h>

#include  <input/siteparameters/siteparameters.h>
#include  <input/soillayers/soillayers.h>

#include  <logging/cbm_logging.h>

LMOD_MODULE_INFO(EventHandlerFertilize,TMODE_SUBDAILY|TMODE_PRE_DAILY|TMODE_POST_DAILY,LMOD_FLAG_USER);

const double  ldndc::EventHandlerFertilize::DEPTHMAX = 0.04;
ldndc::EventHandlerFertilize::EventHandlerFertilize(
                                                    MoBiLE_State *  _state,
                                                    cbm::io_kcomm_t *  _iokcomm,
                                                    timemode_e  _timemode)
: MBE_LegacyModel( _state, _timemode),
io_kcomm( _iokcomm),
sipar_in( _iokcomm->get_input_class< siteparameters::input_class_siteparameters_t >()),
soillayers_in( _iokcomm->get_input_class< soillayers::input_class_soillayers_t >()),
soilchem( _state->get_substate< substate_soilchemistry_t >()),
surfacebulk( _state->get_substate< substate_surfacebulk_t >())
{
    /* FIXME  two fertilizer eventhandlers are overwriting
     *        each other. this can cause false negatives!
     */
    this->io_kcomm->get_scratch()->set(
                                       "producer.fertilize.timemode", (int)_timemode);
    this->io_kcomm->get_scratch()->set(
                                       "producer.manure.timemode", (int)_timemode);
}


ldndc::EventHandlerFertilize::~EventHandlerFertilize()
{
}


lerr_t
ldndc::EventHandlerFertilize::solve()
{
    lerr_t  rc_fert = event_fertilize( false);
    if ( rc_fert != LDNDC_ERR_OK)
    {
        KLOGERROR( "error in fertilize event in module \"", this->name(), "\".");
        return  LDNDC_ERR_FAIL;
    }

    lerr_t  rc_manure = event_manure_dndc();
    if ( rc_manure != LDNDC_ERR_OK)
    {
        KLOGERROR( "error in manure event in module \"", this->name(), "\".");
        return  LDNDC_ERR_FAIL;
    }

    return  LDNDC_ERR_OK;
}


/*!
 * @page soillibs
 * @section soillibs_fertilization Fertilization
 *
 */
lerr_t
ldndc::EventHandlerFertilize::event_fertilize(
                                              bool _have_surfacewater,
                                              bool _have_location_specific_fertilization)
{
    if ( this->m_FertilizeEvents.is_empty())
    { return  LDNDC_ERR_OK; }

    double new_fertilizer_n( -1.0 * soilchem->accumulated_n_fertilizer);

    while ( this->m_FertilizeEvents)
    {
        double const n_old( soilchem->nh4_sl.sum() + soilchem->urea_sl.sum() +
                            soilchem->no3_sl.sum() + soilchem->an_no3_sl.sum() +
                            soilchem->don_sl.sum());

        EventAttributes  fertilize_event = m_FertilizeEvents.pop();
        double const amount( fertilize_event.get( "/amount", -1.0));
        if ( ! cbm::flt_greater_zero( amount))
        {
            continue;
        }

        KLOGDEBUG( "fert-type=", fertilize_event.get( "/type", "?"), ", amount=", amount);

        /*!
         * @page soillibs
         * If surface water is present at fertilization,
         * and defined fertilizer depth equals zero,
         * fertilizer is equally added in surfacebulk layers.
         */
        double const depth( fertilize_event.get( "/depth", 0.0));
        if ( _have_surfacewater && cbm::flt_equal_zero( depth))
        {
            lerr_t  rc_fsurf = this->fertilize_surface( fertilize_event);
            if ( rc_fsurf)
            {
                return  LDNDC_ERR_FAIL;
            }
        }
        else
        {
            lerr_t  rc_fsoil = this->fertilize_soil( fertilize_event);
            if ( rc_fsoil)
            {
                return  LDNDC_ERR_FAIL;
            }
        }

        /*!
         * @page soillibs
         * Fertilizer application can be placed hetereogeneously, e.g.,
         * crop-specific in multi-cropping systems. The amount of fertilizer
         * that is placed at a specific location is implemented as fraction
         * of total fertilized nitrogen. For now, different N-species
         * (e.g., NH4, NO3, DON) are not distinguished but only considered as total N.
         */
        if ( _have_location_specific_fertilization)
        {
            std::string f_location = fertilize_event.get( "/location", "?");
            if ( f_location != "?")
            {
                double const n_add( cbm::bound_min( 0.0,
                                                    soilchem->nh4_sl.sum() + soilchem->urea_sl.sum() +
                                                    soilchem->no3_sl.sum() + soilchem->an_no3_sl.sum() +
                                                    soilchem->don_sl.sum() - n_old));
                
                if ( cbm::flt_greater_zero( n_add))
                {
                    bool have_location( false);
                    for ( std::map< std::string , double>::iterator it = soilchem->n_subdivision.begin();
                         it != soilchem->n_subdivision.end(); ++it)
                    {
                        if ( it->first == f_location)
                        {
                            it->second = (it->second * n_old + n_add) / (n_old + n_add);
                            have_location = true;
                            break;
                        }
                    }

                    if ( !have_location)
                    {
                        soilchem->n_subdivision.insert( { f_location, n_add / (n_old + n_add) } );
                    }
                }
            }
        }
    }

    /* sum up total fertilizer applied in that time step */
    new_fertilizer_n += soilchem->accumulated_n_fertilizer;
    if ( cbm::flt_greater_zero( new_fertilizer_n))
    {
        cbm::state_scratch_t *  mcom = io_kcomm->get_scratch();

        double old_fertilizer_n( 0.0);
        mcom->get( "fertilize:amount", &old_fertilizer_n, 0.0);
        mcom->set( "fertilize:amount", old_fertilizer_n + (new_fertilizer_n * cbm::M2_IN_HA));
    }

    return  LDNDC_ERR_OK;
}


lerr_t
ldndc::EventHandlerFertilize::fertilize_surface( EventAttributes const &  _fertilize_event)
{
    // amount of nitrogen [kg N ha-1]
    double const n_amount( _fertilize_event.get( "/amount", 0.0));
    // amount of nitrification inhibitor [kg ha-1]
    double const ni_amount( _fertilize_event.get( "/ni_amount", 0.0));
    // amount of urease inhibitor [kg ha-1]
    double const ui_amount( _fertilize_event.get( "/ui_amount", 0.0));

    double const n_amount_sbl( n_amount / (cbm::M2_IN_HA * surfacebulk->surfacebulk_layer_cnt()));
    double const ni_amount_sbl( ni_amount / (cbm::M2_IN_HA * surfacebulk->surfacebulk_layer_cnt()));
    double const ui_amount_sbl( ui_amount / (cbm::M2_IN_HA * surfacebulk->surfacebulk_layer_cnt()));

    cbm::string_t const  fertilizer = _fertilize_event.get( "/type", "?");

    for (size_t sbl = 0; sbl < surfacebulk->surfacebulk_layer_cnt(); ++sbl)
    {
        if ( fertilizer == "nh4no3" )
        {
            surfacebulk->nh4_sbl[sbl] += 0.5 * n_amount_sbl;
            surfacebulk->no3_sbl[sbl] += 0.5 * n_amount_sbl;
            soilchem->accumulated_n_fertilizer += n_amount_sbl;
        }
        else if ( fertilizer == "nh3" )
        {
            surfacebulk->nh3_sbl[sbl] += n_amount_sbl;
            soilchem->accumulated_n_fertilizer += n_amount_sbl;
        }
        else if ( fertilizer == "no3" )
        {
            surfacebulk->no3_sbl[sbl] += n_amount_sbl;
            soilchem->accumulated_n_fertilizer += n_amount_sbl;
        }
        else if (( fertilizer == "nh4" ) || ( fertilizer == "nh4hco3" ) || ( fertilizer == "nh4hpo4" ))
        {
            surfacebulk->nh4_sbl[sbl] += n_amount_sbl;
            soilchem->accumulated_n_fertilizer += n_amount_sbl;
        }
        else if ( fertilizer == "nh4so4" )
        {
            surfacebulk->nh4_sbl[sbl] += n_amount_sbl;
            surfacebulk->so4_sbl[sbl] += n_amount_sbl * (cbm::MS / cbm::MN);
            soilchem->accumulated_n_fertilizer += n_amount_sbl;
        }
        else if ( fertilizer == "so4" )
        {
            surfacebulk->so4_sbl[sbl] += n_amount_sbl;
        }
        else if ( fertilizer == "urea" )
        {
            surfacebulk->urea_sbl[sbl] += n_amount_sbl;
            soilchem->accumulated_n_fertilizer += n_amount_sbl;
        }
        /* controled release nitrogen fertilizer */
        else if ( fertilizer == "crnf" )
        {
            surfacebulk->coated_nh4_sbl[sbl] += n_amount_sbl;
            soilchem->accumulated_n_fertilizer += n_amount_sbl;
        }
        else
        {
            KLOGWARN( "Unknown fertilizer type \"",fertilizer,"\". Ignoring event!");
        }

        surfacebulk->ni_sbl[sbl] += ni_amount_sbl;
        surfacebulk->ui_sbl[sbl] += ui_amount_sbl;
    }

    return  LDNDC_ERR_OK;
}

lerr_t
ldndc::EventHandlerFertilize::fertilize_soil( EventAttributes const &  _fertilize_event)
{
    cbm::string_t const  f_name = _fertilize_event.get( "/type", "?");

    //l_0 not allowed to be deeper than last but one soil layer
    size_t l_0( soillayers_in->soil_layer_cnt()-2);
    //find first layer (from top) having at least depth <depth(fert)>
    double const  f_depth = _fertilize_event.get( "/depth", 0.0);
    for ( size_t  l = 0;  l < soillayers_in->soil_layer_cnt()-1;  ++l)
    {
        if ( cbm::flt_greater_equal( soilchem->depth_sl[l], f_depth))
        {
            l_0 = l;
            break;
        }
    }
    //l_1 must at least one layer below l_0
    size_t l_1( l_0+1);

    for ( size_t  l = l_0 + 1;  l < soillayers_in->soil_layer_cnt();  ++l)
    {
        /* distribute at least among 4cm soil profile */
        if ( cbm::flt_greater_equal( soilchem->depth_sl[l], f_depth + 0.04))
        {
            l_1 = l;
            break;
        }
    }

    /* check if we found soil layer for given fertilizing depth */
    if ( l_0 == soillayers_in->soil_layer_cnt())
    {
        KLOGERROR( "Fertilizing depth below deepest strata.",
                   " Failed to fulfill your request :-(",
                   " [type=", f_name,",depth=", f_depth,"]");
        return  LDNDC_ERR_FAIL;
    }
    ldndc_kassert( l_1 > l_0);

    // amount of nitrogen [kg N ha-1]
    double const n_amount( _fertilize_event.get( "/amount", 0.0));
    // amount of nitrification inhibitor [kg ha-1]
    double const ni_amount( _fertilize_event.get( "/ni_amount", 0.0));
    // amount of urease inhibitor [kg ha-1]
    double const ui_amount( _fertilize_event.get( "/ui_amount", 0.0));

    double const n_amount_per_layer = (n_amount / cbm::M2_IN_HA) / (double)( l_1 - l_0);
    double const ni_amount_per_layer = (ni_amount / cbm::M2_IN_HA) / (double)( l_1 - l_0);
    double const ui_amount_per_layer = (ui_amount / cbm::M2_IN_HA) / (double)( l_1 - l_0);

    cbm::string_t const  fertilizer = _fertilize_event.get( "/type", "?");

    for ( size_t  l = l_0;  l < l_1;  ++l)
    {
        if ( fertilizer == "nh4no3" )
        {
            soilchem->nh4_sl[l] += 0.5 * n_amount_per_layer;
            soilchem->no3_sl[l] += 0.5 * n_amount_per_layer;
            soilchem->accumulated_n_fertilizer += n_amount_per_layer;
        }
        else if ( fertilizer == "nh3" )
        {
            soilchem->nh3_liq_sl[l] += n_amount_per_layer;
            soilchem->accumulated_n_fertilizer += n_amount_per_layer;
        }
        else if ( fertilizer == "no3" )
        {
            soilchem->no3_sl[l] += n_amount_per_layer;
            soilchem->accumulated_n_fertilizer += n_amount_per_layer;
        }
        else if (( fertilizer == "nh4" ) || ( fertilizer == "nh4hco3" ) || ( fertilizer == "nh4hpo4" ))
        {
            soilchem->nh4_sl[l] += n_amount_per_layer;
            soilchem->accumulated_n_fertilizer += n_amount_per_layer;
        }
        else if ( fertilizer == "nh4so4" )
        {
            soilchem->nh4_sl[l] += n_amount_per_layer;
            soilchem->so4_sl[l] += n_amount_per_layer * (cbm::MS / cbm::MN);
            soilchem->accumulated_n_fertilizer += n_amount_per_layer;
        }
        else if ( fertilizer == "so4" )
        {
            soilchem->so4_sl[l] += n_amount_per_layer;
        }
        else if ( fertilizer == "urea" )
        {
            soilchem->urea_sl[l] += n_amount_per_layer;
            soilchem->accumulated_n_fertilizer += n_amount_per_layer;
        }
        /* controled release nitrogen fertilizer */
        else if ( fertilizer == "crnf" )
        {
            soilchem->coated_nh4_sl[l] += n_amount_per_layer;
            soilchem->accumulated_n_fertilizer += n_amount_per_layer;
        }
        else
        {
            KLOGWARN( "Unknown fertilizer type \"",fertilizer,"\". Skipping event!");
            break;
        }

        soilchem->ni_sl[l] += ni_amount_per_layer;
        soilchem->ui_sl[l] += ui_amount_per_layer;
    }

    return  LDNDC_ERR_OK;
}



 /*!
 * @page fertilization_event
 * @section manuredndc Manure event in DNDC
 *  
 */
lerr_t
ldndc::EventHandlerFertilize::event_manure_dndc()
{
    if ( this->m_ManureEvents.is_empty())
    { return  LDNDC_ERR_OK; }

    /* NOTE  make sure, all entries are (re)set in each manure event iteration */
    double  add_c[CPOOL_CNT];
    double  CInput( 0.0), NInput( 0.0);

    while ( this->m_ManureEvents)
    {
        EventAttributes  manure_event = this->m_ManureEvents.pop();
        double  me_c = manure_event.get( "/carbon", -1.0);
        if ( me_c < 0.0)
        {
            KLOGERROR( "manure event: carbon and nitrogen amount must be greater 0",
                      "  [carbon=",me_c,"]");
            return  LDNDC_ERR_FAIL;
        }
        double  me_cn = manure_event.get( "/carbon-nitrogen-ratio", -1.0);
        if ( me_cn <= 0.0)
        {
            KLOGERROR( "manure event: carbon/nitrogen ratio must be greater 0",
                      "  [carbon/nitrogen ratio=",me_cn,"]");
            return  LDNDC_ERR_FAIL;
        }

        double  manu_ph = manure_event.get( "/ph", -1.0);

        double  manu_c( me_c / cbm::M2_IN_HA );
        double  manu_n( manu_c / me_cn);

        soilchem->accumulated_c_fertilizer += manu_c;
        soilchem->accumulated_n_fertilizer += manu_n;

        CInput += manu_c;
        NInput += manu_n;

        double  add_doc( 0.0);
        double  add_aorg_c( 0.0);
        double  add_liq_n( 0.0);
        double  add_nh4( 0.0);
        
        double add_n_sl_nh4( 0.0);
        double add_n_sl_don( 0.0);
        double add_n_sl_no3( 0.0);
        double add_n_sl_urea( 0.0);

        size_t  layer( invalid_size);    // indicating the layer l for which depth = h_0+...+h_l > DEPTHMAX
        for ( size_t  sl = 0;  sl < soillayers_in->soil_layer_cnt();  ++sl)
        {
            if ( soilchem->depth_sl[sl] > DEPTHMAX)
            {
                layer = (sl+1);
                break;
            }
        }
        if ( layer == invalid_size)
        {
            layer = soillayers_in->soil_layer_cnt();
        }

        cbm::string_t const &  manure = manure_event.get( "/type", "-");
        KLOGDEBUG( "manure-type=", manure, ", c=", manu_c, ", n=",manu_n, ", ph=", manu_ph);
        if ( manure == "farmyard" )
        {
            double const  me_navailable = manure_event.get( "/nitrogen-available", sipar_in->FRAC_LABILE_N_FARMYARD());
            add_liq_n = manu_n * me_navailable;
            manu_n -= add_liq_n;

            if ( allocate_litter( manu_c, manu_n, sipar_in->RCNRVL(), sipar_in->RCNRL(), sipar_in->RCNRR(), add_c, add_nh4) != LDNDC_ERR_OK)
            {
                return  LDNDC_ERR_FAIL;
            }
            
            if( add_liq_n > 0.0){
                double const add_liq_n_sl( add_liq_n / static_cast<double>(layer));
                
                double const  me_f_nh4 = manure_event.get( "/nh4-fraction", sipar_in->LIQNH4_FARMYARD());
                add_n_sl_nh4 = add_liq_n_sl * me_f_nh4;
                double const  me_f_don = manure_event.get( "/don-fraction", sipar_in->LIQDON_FARMYARD());
                add_n_sl_don = add_liq_n_sl * me_f_don;
                double const  me_f_no3 = manure_event.get( "/no3-fraction", sipar_in->LIQNO3_FARMYARD());
                add_n_sl_no3 = add_liq_n_sl * me_f_no3;
                double const  me_f_urea = manure_event.get( "/urea-fraction", sipar_in->LIQUREA_FARMYARD());
                add_n_sl_urea = add_liq_n_sl * me_f_urea;
                
                if (cbm::flt_not_equal_zero(me_f_nh4*add_liq_n+me_f_don*add_liq_n+me_f_no3*add_liq_n+me_f_urea*add_liq_n-add_liq_n))
                {
                    KLOGERROR( "manure event (",manure,"): fractions of liquid nitrogen do not add up to 1.0!",
                            " check UREA fraction, NH4 fraction, DON fraction and NO3 fraction");
                    return  LDNDC_ERR_FAIL;
                }
            }
        }
        else if (( manure == "green" ) || ( manure == "straw" ))
        {
            if ( allocate_litter( manu_c, manu_n, sipar_in->RCNRVL(),
                                 sipar_in->RCNRL(), sipar_in->RCNRR(), add_c, add_nh4) != LDNDC_ERR_OK)
            {
                return  LDNDC_ERR_FAIL;
            }
        }
        else if ( manure == "slurry" )
        {

            if ( !cbm::flt_greater_zero( manu_ph))
            {
                manu_ph = 7.0;
            }

            double const  me_cavailable = manure_event.get( "/carbon-available", sipar_in->FRAC_LABILE_C_SLURRY());
            add_doc = manu_c * me_cavailable;
            manu_c -= add_doc;

            double const  me_navailable = manure_event.get( "/nitrogen-available", sipar_in->FRAC_LABILE_N_SLURRY());
            add_liq_n = manu_n * me_navailable;
            manu_n -= add_liq_n;

            if (cbm::flt_greater_zero(manu_n))
            {
                static int const  l_max( 500);
                int l( 0);
                while ( 1)
                {
                    double const manu_cn( manu_c / manu_n);
                    if ( manu_cn < sipar_in->CNAORG())
                    {
                        double const trans_n( manu_n * 0.1);
                        add_liq_n += trans_n;
                        manu_n -= trans_n;

                        if ( ++l > l_max)
                        {
                            KLOGERROR( "manure event (slurry): maximum iterations exceeded!");
                            return  LDNDC_ERR_FAIL;
                        }
                    }
                    else
                    {
                        break;
                    }
                }
            }

            double const add_c_r( (manu_c - (sipar_in->CNAORG() * manu_n)) / (1.0 - (sipar_in->CNAORG()/sipar_in->RCNRR())));
            if (add_c_r < 0.0)
            {
                KLOGERROR( "manure event (slurry): negative litter allocation! check site parameters: CNAORG, RCNRR");
                return  LDNDC_ERR_FAIL;
            }

            add_aorg_c        = (manu_c - add_c_r);
            add_c[CPOOL_R]    = add_c_r;
            add_c[CPOOL_VL]   = 0.0;
            add_c[CPOOL_L]    = 0.0;
            add_c[CPOOL_B]    = 0.0;
            
            if( add_liq_n > 0.0){
                double const add_liq_n_sl( add_liq_n / static_cast<double>(layer));
                
                double const  me_f_nh4 = manure_event.get( "/nh4-fraction", sipar_in->LIQNH4_SLURRY());
                add_n_sl_nh4 = add_liq_n_sl * me_f_nh4;
                double const  me_f_don = manure_event.get( "/don-fraction", sipar_in->LIQDON_SLURRY());
                add_n_sl_don = add_liq_n_sl * me_f_don;
                double const  me_f_no3 = manure_event.get( "/no3-fraction", sipar_in->LIQNO3_SLURRY());
                add_n_sl_no3 = add_liq_n_sl * me_f_no3;
                double const  me_f_urea = manure_event.get( "/urea-fraction", sipar_in->LIQUREA_SLURRY());
                add_n_sl_urea = add_liq_n_sl * me_f_urea;
                
            
                if (cbm::flt_not_equal_zero(me_f_nh4*add_liq_n+me_f_don*add_liq_n+me_f_no3*add_liq_n+me_f_urea*add_liq_n-add_liq_n))
                {
                    KLOGERROR( "manure event (",manure,"): fractions of liquid nitrogen do not add up to 1.0!",
                            " check UREA fraction, NH4 fraction, DON fraction and NO3 fraction");
                    return  LDNDC_ERR_FAIL;
                }
                
            }
        }
        else if ( manure == "compost" )
        {
            allocate_litter( manu_c, manu_n, sipar_in->RCNRVL(), sipar_in->RCNRL(), sipar_in->RCNRR(), add_c, add_nh4);
        }
        else if ( manure == "beancake" )
        {
            double const  me_cavailable = manure_event.get( "/carbon-available", sipar_in->FRAC_LABILE_C_BEANCAKE());
            add_doc = manu_c * me_cavailable;
            manu_c -= add_doc;

            double const  me_navailable = manure_event.get( "/nitrogen-available", sipar_in->FRAC_LABILE_N_BEANCAKE());
            double add_bean_cake_n = manu_n * me_navailable;
            manu_n -= add_bean_cake_n;

            double const add_bean_cake_n_sl( add_bean_cake_n / static_cast<double>(layer));
            for ( size_t  sl = 0;  sl < layer;  ++sl)
            {
                soilchem->nh4_sl[sl]+= add_bean_cake_n_sl;
            }

            allocate_litter( manu_c, manu_n, sipar_in->RCNRVL(), sipar_in->RCNRL(), sipar_in->RCNRR(), add_c, add_nh4);
        }
        else
        {
            KLOGWARN( "Unknown manure type \"",manure,"\". Skipping event!");
            break;
        }


        static unsigned char const  rc_cnt( 7);
        double  add_x[rc_cnt] = { add_c[CPOOL_VL], add_c[CPOOL_L], add_c[CPOOL_R], add_c[CPOOL_B],
                                  add_aorg_c, add_doc, (( add_nh4 > 0.0) ? add_nh4 : 0.0) };
        lvector_t< double >  rc_a[7] = { soilchem->C_lit1_sl, soilchem->C_lit2_sl, soilchem->C_lit3_sl, soilchem->c_wood_sl,
                                         soilchem->C_aorg_sl, soilchem->doc_sl, soilchem->nh4_sl};

        for ( size_t  sl = 0;  sl < layer;  ++sl)
        {
            for ( unsigned char  k = 0;  k < rc_cnt;  ++k)
            {
                if ( add_x[k] > 0.0)
                {
                    if ( cbm::flt_greater_zero( manu_ph))
                    {
                        soilchem->ph_sl[sl] = manu_ph;
                    }

                    double const add_x_sl( add_x[k] / static_cast<double>(layer));
                    rc_a[k][sl] += add_x_sl;
                }
            }
        }

        if( add_liq_n > 0.0)
        {
            for ( size_t  sl = 0;  sl < layer;  ++sl)
            {
                if ( cbm::flt_greater_zero( manu_ph))
                {
                    soilchem->ph_sl[sl] = manu_ph;
                }

                soilchem->nh4_sl[sl]  += add_n_sl_nh4;
                soilchem->nh4_sl[sl]  += add_n_sl_don;   //add don to nh4 since don is not used in dndc
                soilchem->urea_sl[sl] += add_n_sl_urea;
                soilchem->no3_sl[sl]  += add_n_sl_no3;
            }
        }
    }

    cbm::state_scratch_t *  mcom = io_kcomm->get_scratch();
    double  n_amount_total = 0.0;
    mcom->get( "manure:amountN", &n_amount_total, 0.0);
    n_amount_total += NInput * cbm::M2_IN_HA;
    mcom->set( "manure:amountN", n_amount_total);

    double  c_amount_total = 0.0;
    mcom->get( "manure:amountC", &c_amount_total, 0.0);
    c_amount_total += CInput * cbm::M2_IN_HA;
    mcom->set( "manure:amountC", c_amount_total);

    return  LDNDC_ERR_OK;
}


 /*!
 * @page fertilization_event
 * @section manuremetrx Manure event in MeTrx
 * For this event the C and N contents contained in the manure in various forms are determined depending on the parameters of the event
 * and added to the layered soil stocks.
 * 
 * - Considered event-input parameters are
 * 		- the ones described in the user guide
 * 		- ureafraction
 * 		- nh4fraction
 * 		- donfraction
 * 		- no3fraction
 * 		- ligninfraction
 * 		- cellulosefraction . \n
 * 		All of them only get used for slurry, farmyard, and compost.
 * 
 * @subsection slurry Slurry
 * - If no ph values is given as input, the default is ph = \b 7.
 * 
 * - Considered pre-set parameters are
 * 		- FRAC_LABILE_N_SLURRY
 * 		- FRAC_LABILE_C_SLURRY
 * 		- LIQUREA\_SLURRY
 * 		- LIQNH4\_SLURRY
 * 		- LIQDON\_SLURRY
 * 		- LIQNO3\_SLURRY
 * 
 */
lerr_t
ldndc::EventHandlerFertilize::event_manure_deconit_metrx()
{
    if ( this->m_ManureEvents.is_empty())
    { return  LDNDC_ERR_OK; }

    while ( this->m_ManureEvents)
    {
        EventAttributes  manure_event = this->m_ManureEvents.pop();
        double  me_c = manure_event.get( "/carbon", -1.0);
        if ( me_c < 0.0)
        {
            KLOGERROR( "manure event: carbon and nitrogen amount must be greater 0",
                      "  [carbon=",me_c,"]");
            return  LDNDC_ERR_FAIL;
        }
        double  me_cn = manure_event.get( "/carbon-nitrogen-ratio", -1.0);
        if ( me_cn <= 0.0)
        {
            KLOGERROR( "manure event: carbon/nitrogen ratio must be greater 0",
                      "  [carbon/nitrogen ratio=",me_cn,"]");
            return  LDNDC_ERR_FAIL;
        }

        double add_c( me_c * cbm::HA_IN_M2);
        double add_n( add_c / me_cn);

        soilchem->accumulated_c_fertilizer += add_c;
        soilchem->accumulated_n_fertilizer += add_n;

        double add_n_nh4( 0.0);
        double add_n_no3( 0.0);
        double add_n_don( 0.0);
        double add_n_urea( 0.0);

        double add_c_doc( 0.0);

        double add_c_aorg( 0.0);
        double add_n_aorg( 0.0);

        double add_c_lit_1( 0.0);
        double add_c_lit_2( 0.0);
        double add_c_lit_3( 0.0);

        double add_n_lit_1( 0.0);
        double add_n_lit_2( 0.0);
        double add_n_lit_3( 0.0);

        double  ph_manure = manure_event.get( "/ph", -1.0);

        cbm::string_t const &  manure = manure_event.get( "/type", "-");
        if ( manure == "slurry" )
        {
            if ( !cbm::flt_greater_zero( ph_manure))
            {
                ph_manure = 7.0;
            }

            double const  avail_n = manure_event.get( "/nitrogen-available", sipar_in->FRAC_LABILE_N_SLURRY());
            if ( !cbm::flt_greater_zero( avail_n))
            {
                KLOGERROR( "manure event (",manure,"): fraction of available nitrogen must be at least 0",
                          "  [available nitrogen=",avail_n,"]");
                return  LDNDC_ERR_FAIL;
            }
            double const  avail_c = manure_event.get( "/carbon-available", sipar_in->FRAC_LABILE_C_SLURRY());
            if ( !cbm::flt_greater_zero( avail_c))
            {
                KLOGERROR( "manure event (",manure,"): fraction of available carbon must be at least 0",
                          "  [available carbon=",avail_c,"]");
                return  LDNDC_ERR_FAIL;
            }

            double const  liq_n = avail_n * add_n;
            double const  f_urea = manure_event.get( "/urea-fraction", sipar_in->LIQUREA_SLURRY());
            add_n_urea = f_urea * liq_n;
            double const  f_nh4 = manure_event.get( "/nh4-fraction", sipar_in->LIQNH4_SLURRY());
            add_n_nh4 = f_nh4 * liq_n;
            double const  f_don = manure_event.get( "/don-fraction", sipar_in->LIQDON_SLURRY());
            add_n_don = f_don * liq_n;
            double const  f_no3 = manure_event.get( "/no3-fraction", sipar_in->LIQNO3_SLURRY());
            add_n_no3 = f_no3 * liq_n;

            if (cbm::flt_not_equal_zero( add_n_urea + add_n_nh4 + add_n_don + add_n_no3 - liq_n))
            {
                KLOGERROR( "manure event (",manure,"): fractions of liquid nitrogen do not add up to 1.0!",
                          " check UREA fraction, NH4 fraction, DON fraction and NO3 fraction");
                return  LDNDC_ERR_FAIL;
            }

            add_c_doc = avail_c * add_c;

            double const add_c_res( add_c - add_c_doc);
            double const add_n_res( add_n - add_n_don - add_n_nh4 - add_n_no3 - add_n_urea);

            double const f_lignin = manure_event.get( "/lignin-fraction", 0.2); // Bhogal et al. (2011), Levi-Minzi et al. (1986), Probert et al. (2005)
            double const frac_c_lit_3( f_lignin);
            double const f_cellulose = manure_event.get( "/cellulose-fraction", 0.6); // Bhogal et al. (2011), Levi-Minzi et al. (1986)
            double const frac_c_lit_2( f_cellulose);
            double const frac_c_lit_1( 1.0 - frac_c_lit_2 - frac_c_lit_3);

            if (( f_lignin + f_cellulose) > 1.0)
            {
                KLOGERROR( "manure event (",manure,"): fractions of litter do not add up to 1.0!",
                          " check lignin fraction and cellulose fraction");
                return  LDNDC_ERR_FAIL;
            }

            add_c_lit_1 += ( frac_c_lit_1 * add_c_res);
            add_c_lit_2 += ( frac_c_lit_2 * add_c_res);
            add_c_lit_3 += ( frac_c_lit_3 * add_c_res);

            add_n_lit_1 += ( frac_c_lit_1 * add_n_res);
            add_n_lit_2 += ( frac_c_lit_2 * add_n_res);
            add_n_lit_3 += ( frac_c_lit_3 * add_n_res);
        }
        else if ( manure == "green" )
        {
            add_c_lit_1 = 0.2 * add_c;
            add_c_lit_2 = 0.7 * add_c;
            add_c_lit_3 = 0.1 * add_c;

            add_n_lit_1 = 0.2 * add_n;
            add_n_lit_2 = 0.7 * add_n;
            add_n_lit_3 = 0.1 * add_n;
        }
        else if ( manure == "straw" )
        {
            add_c_lit_1 = 0.2 * add_c;
            add_c_lit_2 = 0.7 * add_c;
            add_c_lit_3 = 0.1 * add_c;

            add_n_lit_1 = 0.2 * add_n;
            add_n_lit_2 = 0.7 * add_n;
            add_n_lit_3 = 0.1 * add_n;
        }
        else if ( manure == "farmyard" )
        {
            double const  avail_n = manure_event.get( "/nitrogen-available", sipar_in->FRAC_LABILE_N_FARMYARD());
            if ( !cbm::flt_greater_zero( avail_n))
            {
                KLOGERROR( "manure event (",manure,"): fraction of available nitrogen must be at least 0",
                          "  [available nitrogen=",avail_n,"]");
                return  LDNDC_ERR_FAIL;
            }
            double const  avail_c = manure_event.get( "/carbon-available", sipar_in->FRAC_LABILE_C_FARMYARD());
            if ( !cbm::flt_greater_zero( avail_c))
            {
                KLOGERROR( "manure event (",manure,"): fraction of available carbon must be at least 0",
                          "  [available carbon=",avail_c,"]");
                return  LDNDC_ERR_FAIL;
            }

            double const  liq_n = avail_n * add_n;
            double const  f_urea = manure_event.get( "/urea-fraction", sipar_in->LIQUREA_FARMYARD());
            add_n_urea = f_urea * liq_n;
            double const  f_nh4 = manure_event.get( "/nh4-fraction", sipar_in->LIQNH4_FARMYARD());
            add_n_nh4 = f_nh4 * liq_n;
            double const  f_don = manure_event.get( "/don-fraction", sipar_in->LIQDON_FARMYARD());
            add_n_don = f_don * liq_n;
            double const  f_no3 = manure_event.get( "/no3-fraction", sipar_in->LIQNO3_FARMYARD());
            add_n_no3 = f_no3 * liq_n;

            if (cbm::flt_not_equal_zero( add_n_urea + add_n_nh4 + add_n_don + add_n_no3 - liq_n))
            {
                KLOGERROR( "manure event (",manure,"): fractions of liquid nitrogen do not add up to 1.0!",
                          " check UREA fraction, NH4 fraction, DON fraction and NO3 fraction");
                return  LDNDC_ERR_FAIL;
            }

            add_c_doc = avail_c * add_c;

            double const add_c_res( add_c - add_c_doc);
            double const add_n_res( add_n - add_n_don - add_n_nh4 - add_n_no3 - add_n_urea);

            double const f_lignin = manure_event.get( "/lignin-fraction", 0.2); // Bhogal et al. (2011), Levi-Minzi et al. (1986), Probert et al. (2005)
            double const frac_c_lit_3( f_lignin);
            double const f_cellulose = manure_event.get( "/cellulose-fraction", 0.6); // Bhogal et al. (2011), Levi-Minzi et al. (1986)
            double const frac_c_lit_2( f_cellulose);
            double const frac_c_lit_1( 1.0 - frac_c_lit_2 - frac_c_lit_3);

            if (( f_lignin + f_cellulose) > 1.0)
            {
                KLOGERROR( "manure event (",manure,"): fractions of litter do not add up to 1.0!",
                          " check lignin fraction and cellulose fraction");
                return  LDNDC_ERR_FAIL;
            }

            add_c_lit_1 += ( frac_c_lit_1 * add_c_res);
            add_c_lit_2 += ( frac_c_lit_2 * add_c_res);
            add_c_lit_3 += ( frac_c_lit_3 * add_c_res);

            add_n_lit_1 += ( frac_c_lit_1 * add_n_res);
            add_n_lit_2 += ( frac_c_lit_2 * add_n_res);
            add_n_lit_3 += ( frac_c_lit_3 * add_n_res);
        }
        else if ( manure == "compost" )
        {
            double const  avail_n = manure_event.get( "/nitrogen-available", sipar_in->FRAC_LABILE_N_COMPOST());
            if ( avail_n < 0.0)
            {
                KLOGERROR( "manure event (",manure,"): fraction of available nitrogen must be at least 0",
                          "  [available nitrogen=",avail_n,"]");
                return  LDNDC_ERR_FAIL;
            }
            double const  avail_c = manure_event.get( "/carbon-available", sipar_in->FRAC_LABILE_C_COMPOST());
            if ( avail_c < 0.0)
            {
                KLOGERROR( "manure event (",manure,"): fraction of available carbon must be at least 0",
                          "  [available carbon=",avail_c,"]");
                return  LDNDC_ERR_FAIL;
            }

            double const  liq_n = avail_n * add_n;
            double const  f_urea = manure_event.get( "/urea-fraction", sipar_in->LIQUREA_COMPOST());
            add_n_urea = f_urea * liq_n;
            double const  f_nh4 = manure_event.get( "/nh4-fraction", sipar_in->LIQNH4_COMPOST());
            add_n_nh4 = f_nh4 * liq_n;
            double const  f_don = manure_event.get( "/don-fraction", sipar_in->LIQDON_COMPOST());
            add_n_don = f_don * liq_n;
            double const  f_no3 = manure_event.get( "/no3-fraction", sipar_in->LIQNO3_COMPOST());
            add_n_no3 = f_no3 * liq_n;

            if (cbm::flt_not_equal_zero(add_n_urea+add_n_nh4+add_n_don+add_n_no3-liq_n))
            {
                KLOGERROR( "manure event (",manure,"): fractions of liquid nitrogen do not add up to 1.0!",
                          " check UREA fraction, NH4 fraction, DON fraction and NO3 fraction");
                return  LDNDC_ERR_FAIL;
            }

            add_c_doc = ( avail_c * add_c);

            double const add_c_res( add_c - add_c_doc);
            double const add_n_res( add_n - add_n_don - add_n_nh4 - add_n_no3 - add_n_urea);

            double const f_lignin = manure_event.get( "/lignin-fraction", 0.8);
            double const frac_c_lit_3( f_lignin);
            double const f_cellulose = manure_event.get( "/cellulose-fraction", 0.1);
            double const frac_c_lit_2( f_cellulose);
            double const frac_c_lit_1( 1.0 - frac_c_lit_2 - frac_c_lit_3);

            if (( f_lignin + f_cellulose) > 1.0)
            {
                KLOGERROR( "manure event (",manure,"): fractions of litter do not add up to 1.0!",
                          " check lignin fraction and cellulose fraction");
                return  LDNDC_ERR_FAIL;
            }

            add_c_lit_1 += ( frac_c_lit_1 * add_c_res);
            add_c_lit_2 += ( frac_c_lit_2 * add_c_res);
            add_c_lit_3 += ( frac_c_lit_3 * add_c_res);

            add_n_lit_1 += ( frac_c_lit_1 * add_n_res);
            add_n_lit_2 += ( frac_c_lit_2 * add_n_res);
            add_n_lit_3 += ( frac_c_lit_3 * add_n_res);
        }
        else
        {
            KLOGWARN( "Unknown manure type \"",manure,"\". Skipping event!");
            break;
        }

        double const  depth( manure_event.get( "/depth", 0.0));

        double sl_max( 0);
        double h_max( soilchem->h_sl[0]);

        for ( size_t  sl = 1;  sl < soillayers_in->soil_layer_cnt();  ++sl)
        {
            if ( cbm::flt_greater_equal( depth, soilchem->depth_sl[sl]))
            {
                h_max += soilchem->h_sl[sl];
                sl_max += 1;
            }
            else{ break; }
        }

        for ( size_t  sl = 0;  sl <= sl_max;  ++sl)
        {
            double const layer_fraction( soilchem->h_sl[sl] / h_max);

            if ( cbm::flt_greater_zero( ph_manure))
            {
                soilchem->ph_sl[sl] = ph_manure;
            }
            double const layer_fraction_ae( (1.0 - soilchem->anvf_sl[sl]) * layer_fraction);
            double const layer_fraction_an( soilchem->anvf_sl[sl] * layer_fraction);

            soilchem->don_sl[sl]    += (add_n_don * layer_fraction);
            soilchem->nh4_sl[sl]    += (add_n_nh4 * layer_fraction);
            soilchem->no3_sl[sl]    += (add_n_no3 * layer_fraction_ae);
            soilchem->an_no3_sl[sl] += (add_n_no3 * layer_fraction_an);
            soilchem->urea_sl[sl]   += (add_n_urea * layer_fraction);

            soilchem->doc_sl[sl]    += (add_c_doc * layer_fraction_ae);
            soilchem->an_doc_sl[sl] += (add_c_doc * layer_fraction_an);

            soilchem->C_aorg_sl[sl] += (add_c_aorg * layer_fraction);
            soilchem->N_aorg_sl[sl] += (add_n_aorg * layer_fraction);

            soilchem->C_lit1_sl[sl] += (add_c_lit_1 * layer_fraction);
            soilchem->C_lit2_sl[sl] += (add_c_lit_2 * layer_fraction);
            soilchem->C_lit3_sl[sl] += (add_c_lit_3 * layer_fraction);

            soilchem->N_lit1_sl[sl] += (add_n_lit_1 * layer_fraction);
            soilchem->N_lit2_sl[sl] += (add_n_lit_2 * layer_fraction);
            soilchem->N_lit3_sl[sl] += (add_n_lit_3 * layer_fraction);
        }

        cbm::state_scratch_t *  mcom = io_kcomm->get_scratch();
        double  n_amount_total = 0.0;
        mcom->get( "manure:amountN", &n_amount_total, 0.0);
        n_amount_total += add_n * cbm::M2_IN_HA;
        mcom->set( "manure:amountN", n_amount_total);

        double  c_amount_total = 0.0;
        mcom->get( "manure:amountC", &c_amount_total, 0.0);
        c_amount_total += add_c * cbm::M2_IN_HA;
        mcom->set( "manure:amountC", c_amount_total);
    }

    return  LDNDC_ERR_OK;
}

static int  _QueueEventFertilize( void const *  _msg, size_t  _msg_sz, void *  _obj)
{
    ldndc::EventHandlerFertilize *  obj =
    static_cast< ldndc::EventHandlerFertilize * >( _obj);
    if ( !obj)
    { return -1; }

    ldndc::EventAttributes  fertilize_event(
                                            "fertilize", (char const *)_msg, _msg_sz);
    lerr_t  rc_queue = obj->queue_event_fertilize( fertilize_event);
    if ( rc_queue)
    { return -1; }
    return 0;
}

static int  _QueueEventManure( void const *  _msg, size_t  _msg_sz, void *  _obj)
{
    ldndc::EventHandlerFertilize *  obj =
    static_cast< ldndc::EventHandlerFertilize * >( _obj);
    if ( !obj)
    { return -1; }

    EventAttributes  manure_event(
                                  "manure", (char const *)_msg, _msg_sz);
    lerr_t  rc_queue = obj->queue_event_manure( manure_event);
    if ( rc_queue)
    { return -1; }
    return 0;
}

lerr_t
ldndc::EventHandlerFertilize::register_ports( cbm::io_kcomm_t *  _io_kcomm)
{
    CBM_Callback  cb_fertilize;
    cb_fertilize.fn = &_QueueEventFertilize;
    cb_fertilize.data = this;
    this->m_FHandle = _io_kcomm->subscribe_event( "fertilize", cb_fertilize);
    if ( !CBM_HandleOk(this->m_FHandle))
    { return  LDNDC_ERR_FAIL; }
    
    CBM_Callback  cb_manure;
    cb_manure.fn = &_QueueEventManure;
    cb_manure.data = this;
    this->m_MHandle = _io_kcomm->subscribe_event( "manure", cb_manure);
    if ( !CBM_HandleOk(this->m_MHandle))
    { return  LDNDC_ERR_FAIL; }
    
    return  LDNDC_ERR_OK;
}

lerr_t
ldndc::EventHandlerFertilize::unregister_ports( cbm::io_kcomm_t *  _io_kcomm)
{
    _io_kcomm->unsubscribe_event( this->m_FHandle);
    _io_kcomm->unsubscribe_event( this->m_MHandle);
    
    return  LDNDC_ERR_OK;
}

lerr_t
ldndc::EventHandlerFertilize::queue_event_fertilize( ldndc::EventAttributes &  _attributes)
{
    this->m_FertilizeEvents.push( _attributes);
    return  LDNDC_ERR_OK;
}

lerr_t
ldndc::EventHandlerFertilize::queue_event_manure( ldndc::EventAttributes &  _attributes)
{
    this->m_ManureEvents.push( _attributes);
    return LDNDC_ERR_OK;
}
