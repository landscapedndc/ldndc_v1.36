
#ifndef  LM_EVENTHANDLER_FERTILIZE_H_
#define  LM_EVENTHANDLER_FERTILIZE_H_

#include  "mbe_legacymodel.h"
#include  "state/mbe_state.h"
#include  "ld_eventqueue.h"

namespace ldndc {
    namespace event
        { class EventFertilize; }

class  LDNDC_API  EventHandlerFertilize  :  public  MBE_LegacyModel
{
    /*! depth of litter distribution into the soil [m] */
    static const double  DEPTHMAX;

    LMOD_EXPORT_MODULE_INFO(EventHandlerFertilize,"eventhandler:fertilize","EventHandler Fertilize");

    public:
        EventHandlerFertilize( MoBiLE_State *,
                cbm::io_kcomm_t *, timemode_e);
        ~EventHandlerFertilize();

        lerr_t  configure( ldndc::config_file_t const *) { return  LDNDC_ERR_OK; }

        lerr_t  initialize() { return  LDNDC_ERR_OK; }
        lerr_t  solve();
        lerr_t  finalize() { return  LDNDC_ERR_OK; }

        lerr_t  wake() { return  LDNDC_ERR_OK; }
        lerr_t  sleep() { return  LDNDC_ERR_OK; }

    private:
        cbm::io_kcomm_t *  io_kcomm;

        input_class_siteparameters_t const *  sipar_in;
        input_class_soillayers_t const *  soillayers_in;

        substate_soilchemistry_t *  soilchem;
        substate_surfacebulk_t *  surfacebulk;

    public:
        /*!
         * @brief
         */
        lerr_t  event_fertilize(
                                bool /* surfacewater yes/no*/,
                                bool = false /* location specific fertilization yes/no */);

        /*!
         * @brief
         */
        lerr_t  event_manure_dndc();

        /*!
         * @brief
         */
        lerr_t  event_manure_deconit_metrx();

        /*!
         * @brief
         *  returns true if fertilize event queue is not empty
         */
        bool have_fertilization_event(){ return ! m_FertilizeEvents.is_empty(); };

        /*!
         * @brief
         *  returns true if fertilize event queue is not empty
         */
        bool have_manuring_event(){ return ! m_ManureEvents.is_empty(); };

    private:
        lerr_t  fertilize_surface( EventAttributes const &);
        lerr_t  fertilize_soil( EventAttributes const &);

    public:
        lerr_t  register_ports( cbm::io_kcomm_t *);
        lerr_t  unregister_ports( cbm::io_kcomm_t *);

        lerr_t  queue_event_fertilize( EventAttributes &);
        lerr_t  queue_event_manure( EventAttributes &);

    private:
        CBM_Handle  m_FHandle;
        EventQueue  m_FertilizeEvents;
        CBM_Handle  m_MHandle;
        EventQueue  m_ManureEvents;
};

}

#endif    /*  !LMOD_EVENTHANDLER_FERTILIZE_H_  */

