#ifndef  LD_MODELS_FORWARD_DECL_H_
#define  LD_MODELS_FORWARD_DECL_H_

#include  <input/ic.h>
using  namespace ldndc;

#include  <input/airchemistry/airchemistry.h>
using  namespace ldndc::airchemistry;
#include  <input/checkpoint/checkpoint.h>
using  namespace ldndc::checkpoint;
#include  <input/climate/climate.h>
using  namespace ldndc::climate;
#include  <input/event/event.h>
using  namespace ldndc::event;
#include  <input/groundwater/groundwater.h>
using  namespace ldndc::groundwater;
#include  <input/setup/setup.h>
using  namespace ldndc::setup;
#include  <input/site/site.h>
using  namespace ldndc::site;
#include  <input/siteparameters/siteparameters.h>
using  namespace ldndc::siteparameters;
#include  <input/soillayers/soillayers.h>
using  namespace ldndc::soillayers;
#include  <input/soilparameters/soilparameters.h>
using  namespace ldndc::soilparameters;
#include  <input/species/species.h>
using  namespace ldndc::species;
#include  <input/speciesparameters/speciesparameters.h>
using  namespace ldndc::speciesparameters;

#include  "mobile/plant.h"

#endif  /*  !LD_MODELS_FORWARD_DECL_H_  */

