/*!
 * @brief
 *    common configuration and execution interface
 *    for sinks.
 *
 * @author
 *    steffen klatt (created on: oct 18, 2016)
 */

#include  "ld_sinkinterface.h"

#include  <input/setup/setup.h>
#include  <input/setup/setuptypes.h>

ldndc::SinkInterface::SinkInterface(
    cbm::io_kcomm_t *  _io_kcomm)
{
    this->m_rc.write_id = 0;
    this->m_rc.write_source = 0;

    this->m_rc.write_xyz = 0;
    this->m_rc.use_location = 0;
    this->m_rc.write_area = 0;

    this->m_rc.write_datetime = 0;

    this->m_rc.write_year = 0;
    this->m_rc.write_month = 0;
    this->m_rc.write_day = 0;
    this->m_rc.write_hour = 0;
    this->m_rc.write_minute = 0;
    this->m_rc.write_second = 0;

    this->m_rc.write_yearday = 0;
    this->m_rc.write_subday = 0;

    this->m_rc.write_seqnb = 0;
    this->m_rc.separate_concurrent = 1;

    if ( _io_kcomm)
        { this->set_client_info( _io_kcomm); }
}

ldndc::SinkInterface::~SinkInterface()
{ }

lerr_t
ldndc::SinkInterface::configure( cbm::io_kcomm_t *  _io_kcomm)
{
    if ( _io_kcomm)
        { return this->set_client_info( _io_kcomm); }
    return LDNDC_ERR_FAIL;
}

lerr_t
ldndc::SinkInterface::set_metaflags(
    ldndc::config_file_t const *  _cf, char const *  _oid,
    lflags_t const &  _flags)
{
    if ( !_cf)
        { return  LDNDC_ERR_RUNTIME_ERROR; }

    bool  cf_setting;
    bool  cf_default;

    cf_default = ( _flags & (RM_CLIENTID)) != 0;
    _cf->query( "output:*", "write_id", &cf_setting, cf_default);
    _cf->query( _oid, "write_id", &cf_setting, cf_setting);
    this->m_rc.write_id = cf_setting;

    cf_default = ( _flags & (RM_CLIENTSOURCE)) != 0;
    _cf->query( "output:*", "write_source", &cf_setting, cf_default);
    _cf->query( _oid, "write_source", &cf_setting, cf_setting);
    this->m_rc.write_source = cf_setting;

    cf_default = ( _flags & (RM_X|RM_Y|RM_Z)) != 0; /* any will do.. */
    _cf->query( "output:*", "write_xyz", &cf_setting, cf_default);
    _cf->query( _oid, "write_xyz", &cf_setting, cf_setting);
    this->m_rc.write_xyz = cf_setting;

    cf_default = ( _flags & (RM_AREA)) != 0;
    _cf->query( "output:*", "write_area", &cf_setting, cf_default);
    _cf->query( _oid, "write_area", &cf_setting, cf_setting);
    this->m_rc.write_area = cf_setting;

    cf_default = ( _flags & (RM_DATETIME)) != 0;
    _cf->query( "output:*", "write_datetime", &cf_setting, cf_default);
    _cf->query( _oid, "write_datetime", &cf_setting, cf_setting);
    this->m_rc.write_datetime = cf_setting;

    cf_default = ( _flags & (RM_YEAR)) != 0;
    _cf->query( "output:*", "write_year", &cf_setting, cf_default);
    _cf->query( _oid, "write_year", &cf_setting, cf_setting);
    this->m_rc.write_year = cf_setting;

    cf_default = ( _flags & (RM_MONTH)) != 0;
    _cf->query( "output:*", "write_month", &cf_setting, cf_default);
    _cf->query( _oid, "write_month", &cf_setting, cf_setting);
    this->m_rc.write_month = cf_setting;

    cf_default = ( _flags & (RM_DAY)) != 0;
    _cf->query( "output:*", "write_day", &cf_setting, cf_default);
    _cf->query( _oid, "write_day", &cf_setting, cf_setting);
    this->m_rc.write_day = cf_setting;

    cf_default = ( _flags & (RM_HOUR)) != 0;
    _cf->query( "output:*", "write_hour", &cf_setting, cf_default);
    _cf->query( _oid, "write_hour", &cf_setting, cf_setting);
    this->m_rc.write_hour = cf_setting;

    cf_default = ( _flags & (RM_MINUTE)) != 0;
    _cf->query( "output:*", "write_minute", &cf_setting, cf_default);
    _cf->query( _oid, "write_minute", &cf_setting, cf_setting);
    this->m_rc.write_minute = cf_setting;

    cf_default = ( _flags & (RM_SECOND)) != 0;
    _cf->query( "output:*", "write_second", &cf_setting, cf_default);
    _cf->query( _oid, "write_second", &cf_setting, cf_setting);
    this->m_rc.write_second = cf_setting;

    cf_default = ( _flags & (RM_YEARDAY)) != 0;
    _cf->query( "output:*", "write_yearday", &cf_setting, cf_default);
    _cf->query( _oid, "write_yearday", &cf_setting, cf_setting);
    this->m_rc.write_yearday = cf_setting;

    cf_default = ( _flags & (RM_SUBDAY)) != 0;
    _cf->query( "output:*", "write_subday", &cf_setting, cf_default);
    _cf->query( _oid, "write_subday", &cf_setting, cf_setting);
    this->m_rc.write_subday = cf_setting;

    cf_default = ( _flags & (RM_LAYER)) != 0;
    _cf->query( "output:*", "write_layer", &cf_setting, cf_default);
    _cf->query( _oid, "write_layer", &cf_setting, cf_setting);
    this->m_rc.write_seqnb = cf_setting;

    /* rather than x,y,z use longitude, latitude, elevation instead */
    _cf->query( "output:*", "use_location", &cf_setting, false);
    _cf->query( _oid, "use_location", &cf_setting, cf_setting);
    this->m_rc.use_location = cf_setting;

    /* write to single or multiple sinks (e.g., concurrent plants) */
    _cf->query( "output:*", "separate_concurrent", &cf_setting, true);
    _cf->query( _oid, "separate_concurrent", &cf_setting, cf_setting);
    this->m_rc.separate_concurrent = cf_setting;


    return  LDNDC_ERR_OK;
}

lflags_t
ldndc::SinkInterface::get_metaflags()
const
{
    lflags_t  metaflags = RM_NONE;

    if ( this->m_rc.write_id) { metaflags |= RM_CLIENTID; }
    if ( this->m_rc.write_source) { metaflags |= RM_CLIENTSOURCE; }
    if ( this->m_rc.write_xyz) { metaflags |= RM_X|RM_Y|RM_Z; }
    if ( this->m_rc.write_area) { metaflags |= RM_AREA; }

    if ( this->m_rc.write_datetime) { metaflags |= RM_DATETIME; }

    if ( this->m_rc.write_year) { metaflags |= RM_YEAR; }
    if ( this->m_rc.write_month) { metaflags |= RM_MONTH; }
    if ( this->m_rc.write_day) { metaflags |= RM_DAY; }
    if ( this->m_rc.write_hour) { metaflags |= RM_HOUR; }
    if ( this->m_rc.write_minute) { metaflags |= RM_MINUTE; }
    if ( this->m_rc.write_second) { metaflags |= RM_SECOND; }

    if ( this->m_rc.write_yearday) { metaflags |= RM_YEARDAY; }
    if ( this->m_rc.write_subday) { metaflags |= RM_SUBDAY; }

    if ( this->m_rc.write_seqnb) { metaflags |= RM_LAYER; }

    return  metaflags;
}

lerr_t
ldndc::SinkInterface::update_meta(
        ldndc::sink_record_meta_t *  _meta,
        cbm::sclock_t const *  _clock)
const
{
    if ( !_meta)
        { return  LDNDC_ERR_RUNTIME_ERROR; }

/*TODO use sse() */

    _meta->t.reg.year = _clock->year();
    _meta->t.reg.month = _clock->month();
    _meta->t.reg.day = _clock->day();
    _meta->t.reg.hour = _clock->hour();
    _meta->t.reg.minute = _clock->minute();
    _meta->t.reg.second = _clock->second();

    _meta->t.reg.julianday = _clock->yearday();

/*DEPRECATED*/
    if ( this->m_rc.write_subday)
        { _meta->t.reg.subday = _clock->subday(); }

    if ( this->m_rc.write_seqnb)
        { _meta->record_seqnb = this->seqnb; }

    return LDNDC_ERR_OK;
}

lerr_t
ldndc::SinkInterface::write_fixed_record(
    ldndc::sink_handle_t *  _sink, void **  _data,
    cbm::sclock_t const *  _clock)
{
    sink_fixed_record_t  record;
    record.data = _data;

    lerr_t  rc_meta = this->update_meta( &record.meta, _clock);
    if ( rc_meta)
        { return  LDNDC_ERR_FAIL; }

    lerr_t  rc_write =
        _sink->write_fixed_record( &this->client, &record);
    if ( rc_write)
        { return  LDNDC_ERR_FAIL; }
    return  LDNDC_ERR_OK;
}
lerr_t
ldndc::SinkInterface::write_fixed_zero_record(
    ldndc::sink_handle_t *  _sink,
    cbm::sclock_t const *  _clock)
{
    sink_fixed_record_t  record;
    record.data = NULL;

    lerr_t  rc_meta = this->update_meta( &record.meta, _clock);
    if ( rc_meta)
        { return  LDNDC_ERR_FAIL; }

    lerr_t  rc_write =
        _sink->write_fixed_zero_record( &this->client, &record);
    if ( rc_write)
        { return  LDNDC_ERR_FAIL; }
    return  LDNDC_ERR_OK;
}

lerr_t
ldndc::SinkInterface::set_client_info(
        cbm::io_kcomm_t *  _io_kcomm)
{
    if ( !_io_kcomm)
        { return  LDNDC_ERR_RUNTIME_ERROR; }

    setup::input_class_setup_t const *  setup_in =
        _io_kcomm->get_input_class< setup::input_class_setup_t >();
    if ( !setup_in)
        { return  LDNDC_ERR_FAIL; }

    this->client.target_id = setup_in->object_id();
    cbm::source_descriptor_t  source_info;
    setup_in->block_consume(
        &source_info, setup_in->input_class_type());
    cbm::as_strcpy(
        this->client.target_sid, source_info.source_identifier);

    this->client.target_id = setup_in->object_id();
    bool  lle = this->m_rc.use_location;
    this->client.x =
        lle ? setup_in->longitude() : setup_in->x();
    this->client.y =
        lle ? setup_in->latitude() : setup_in->y();
    this->client.z =
        lle ? setup_in->elevation() : setup_in->z();
    this->client.area = setup_in->area();

    return  LDNDC_ERR_OK;
}

