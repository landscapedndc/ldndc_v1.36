/*!
 * @brief
 *    common stuff for output modules
 *
 * @author
 *    steffen klatt (created on: nov 08, 2013)
 */

#ifndef  MBE_LEGACYOUTPUTMODEL_H_
#define  MBE_LEGACYOUTPUTMODEL_H_

#include  "mbe_legacymodel.h"
#include  "ld_sinkinterface.h"

/* print zero lines if no species exist */
#define  LDNDC_OUTPUT_HAVE_DUMMY_LINES  1

#define  LDNDC_OUTPUT_DEFINE_SINK_LAYOUT_DEFAULTS(__sink__,__name__) \
    LDNDC_OUTPUT_DEFINE_SINK_LAYOUT(__sink__,__name__,this->get_metaflags())


namespace ldndc {
class LDNDC_API MBE_LegacyOutputModel : public MBE_LegacyModel
{
    protected:
        MBE_LegacyOutputModel( MoBiLE_State * /*state*/,
                cbm::io_kcomm_t * /*i/o*/, timemode_e /*timemode*/);
        virtual ~MBE_LegacyOutputModel();

    public:
        lerr_t  set_metaflags( ldndc::config_file_t const *,
                lflags_t const & = RM_DEFAULTS /*predefines*/);
        lflags_t  get_metaflags() const;
        bool  separate_concurrent() const
            { return this->m_sif.separate_concurrent(); }

        void  set_layernumber( int  _seqnb)
            { this->m_sif.set_layernumber( _seqnb); }

        lerr_t  write_fixed_record( ldndc::sink_handle_t * /*target sink*/,
            void ** /*data record*/);
        lerr_t  write_fixed_zero_record(
                    ldndc::sink_handle_t * /*target sink*/);

    private:
        SinkInterface  m_sif;
};

} /* namespace ldndc */

#endif  /*  !MBE_LEGACYOUTPUTMODEL_H_  */

