/*!
 * @file
 *    kind of helper module to perform additional tasks
 *    before user modules are run
 *
 * @author
 *    steffen klatt,
 *    edwin haas,
 *    david kraus
 */

#ifndef  MBE_SUBDAYINITIALIZER_H_
#define  MBE_SUBDAYINITIALIZER_H_

#include  "mbe_legacymodel.h"

namespace ldndc {

class  LDNDC_API  SubdailyInitializer  :  public  MBE_LegacyModel
{
    LMOD_EXPORT_MODULE_INFO(SubdailyInitializer,"sys:*:subdailyinitializer","house-keeping at beginning of subday");
    public:
        SubdailyInitializer(
                MoBiLE_State *,
                cbm::io_kcomm_t *,
                timemode_e);

        ~SubdailyInitializer();

        lerr_t  configure( ldndc::config_file_t const *);

        lerr_t  initialize();
        lerr_t  solve();
        lerr_t  finalize() { return  LDNDC_ERR_OK; }

        lerr_t  sleep() { return  LDNDC_ERR_OK; }
        lerr_t  wake() { return  LDNDC_ERR_OK; }
    
    private:
        substate_physiology_t &  ph_;
        substate_microclimate_t &  mc_;
        substate_watercycle_t &  wc_;

        MoBiLE_PlantVegetation *  m_veg;
        input_class_setup_t const *  m_setup;
        input_class_soillayers_t const *  soillayers;
};

} /*namespace ldndc*/

#endif  /*  !MBE_SUBDAYINITIALIZER_H_  */

