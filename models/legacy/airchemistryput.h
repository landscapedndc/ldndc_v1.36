/*!
 * @brief
 *    updates air chemistry subday and day buffers
 *
 * @author
 *    steffen klatt,
 *    edwin haas
 */

#ifndef  MBE_AIRCHEMISTRYPUT_H_
#define  MBE_AIRCHEMISTRYPUT_H_

#include  "mbe_legacymodel.h"

namespace ldndc {

class  LDNDC_API  AirChemistryDayBufferUpdate  :  public MBE_LegacyModel
{
    LMOD_EXPORT_MODULE_INFO(AirChemistryDayBufferUpdate,"sys:airchemistry:copy-to-state","update subday and day air chemistry buffers");
    public:
        AirChemistryDayBufferUpdate(
                MoBiLE_State *,
                cbm::io_kcomm_t *,
                timemode_e);

        ~AirChemistryDayBufferUpdate();

        lerr_t  configure( ldndc::config_file_t const *);

        lerr_t  initialize();

        /*!
         * @brief
         *    reads stream data items for current time
         *    step and stores them in the appropriate
         *    variables.
         */
        lerr_t  solve();

        lerr_t  finalize() { return  LDNDC_ERR_OK; }

        lerr_t  sleep() { return  LDNDC_ERR_OK; }
        lerr_t  wake() { return  LDNDC_ERR_OK; }

    private:
        input_class_airchemistry_t const *  ad_;
        substate_airchemistry_t &  ac_;

        lerr_t  copy_air_chemistry_record_to_buffers_();
};

}

#endif /* !MBE_AIRCHEMISTRYPUT_H_ */

