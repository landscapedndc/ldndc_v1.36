/*!
 *  @brief
 *    legacy base class for MoBiLE modules running with
 *    simulation system.
 *
 *  @author
 *    steffen klatt,
 *    edwin haas
 */

#ifndef  MOBILE_MODULE_H_
#define  MOBILE_MODULE_H_

#include  "ld_legacy.h"
#include  "legacy/mbe_setup.h"
#include  "legacy/mbe_plant.h"
#include  "legacy/state/mbe_state.h"

#include  <math/cbm_math.h>
#include  <time/cbm_time.h>
#include  <input/ecosystemtypes.h>
#include  <memory/cbm_mem.h>
#include  <memory/cbm_singleton.h>

#include  <list>

/* oneleaf option has moved */

#ifdef  LDNDC_OPENMP
#  define  CF_LMOD_QUERY_RETURN_(__rc__)    /* no op */
#else
#  define  CF_LMOD_QUERY_RETURN_(__rc__)    return  __rc__;
#endif
#define  CF_LMOD_QUERY(__cf__,__key__,__reg__,__dflt__) \
{ \
    lerr_t  __rc ## __reg__ ( (__cf__)->query( info().id, __key__, &__reg__)); \
    if (( __rc ## __reg__) == LDNDC_ERR_ATTRIBUTE_NOT_FOUND) \
    { \
        __reg__ = __dflt__; \
    } \
    else if (( __rc ## __reg__) == LDNDC_ERR_OK) \
    { \
        /* no op */ \
    } \
    else \
    { \
        /* fallback .. use default, and issue error message */ \
        __reg__ = __dflt__; \
      \
        LOGERROR( "config query: invalid key or value"); \
        CF_LMOD_QUERY_RETURN_(TOKENPASTE(__rc,__reg__)) \
    } \
}

namespace  cbm {
    class  io_kcomm_t; }
namespace  ldndc {
    class  config_file_t; }

namespace ldndc {
class MBE_LegacyModel;

extern LDNDC_API lflags_t const  TMODE_DEFAULTS;
enum  lmodule_flag_e
{
    /*! empty flag */
    LMOD_FLAG_NONE = 0u,

    /*! marks model as control model */
    LMOD_FLAG_CONTROL = 1u << 0,

    /*! marks model as user model */
    LMOD_FLAG_USER = 1u << 1,

    /*! marks model as internal system model */
    LMOD_FLAG_SYSTEM = 1u << 2,
    /*! marks model as initializer (runs before user legacy models) */
    LMOD_FLAG_INITIALIZER = 1u << 3,
    /*! marks model as finalizer (runs after user legacy models) */
    LMOD_FLAG_FINALIZER = 1u << 4,
    /*! marks model as maintenance model (e.g. updates, resets, etc) */
    LMOD_FLAG_MAINTENANCE = 1u << 5,

    /*! marks model as output model (always added last) */
    LMOD_FLAG_OUTPUT = 1u << 6,

    /*! marks model as test model */
    LMOD_FLAG_TEST_MODULE = 1u << 7,

    LMOD_FLAG_CNT_,
    LMOD_FLAG_CNT = 1 + 1 + cbm::log2_t< LMOD_FLAG_CNT_-1 >::LOG2
};

struct LDNDC_API module_info_t
{
    module_info_t(
            char const * /*id*/, char const * /*name*/,
            lflags_t /*timemode*/, lflags_t /*flags*/);

    /* identifier */
    char const *  id;
    /* human readable name */
    char const *  name;
    /* valid time modes */
    lflags_t  timemode;

    /* flags to control and specify behavior of model */
    lflags_t  mflags;
};

struct LDNDC_API MoBiLE_ModuleFactoryBase
{
    virtual  ~MoBiLE_ModuleFactoryBase() = 0;

    virtual  module_info_t const &  info() const = 0;
    virtual  mobile_module_options_t const &  options_info() const = 0;

    virtual  MBE_LegacyModel *  construct(
            MoBiLE_State *, cbm::io_kcomm_t *,
            cbm::sclock_t const *, timemode_e) const = 0;
};

template < class  _M >
struct  MoBiLE_ModuleFactory  :  public  MoBiLE_ModuleFactoryBase
{
    MoBiLE_ModuleFactory() : MoBiLE_ModuleFactoryBase()
        { }

    module_info_t const &  info() const
        { return  _M::m_info(); }

    mobile_module_options_t const &  options_info() const
        { return  *_M::m_options_info(); }

    MBE_LegacyModel *  construct(
            MoBiLE_State *  _state, cbm::io_kcomm_t *  _io,
            cbm::sclock_t const *  _clock, timemode_e  _timemode)
    const
    { return  static_cast< MBE_LegacyModel * >(
                _M::new_instance( _state, _io, _clock, _timemode)); }
};

struct LDNDC_API MoBiLE_ModuleFactoryEnvelope
{    
    bool  exists() const
        { return ( constructor != NULL); }

    module_info_t const &  info() const
        { return  constructor->info(); }

    mobile_module_options_t const &  options_info() const
        { return  constructor->options_info(); }

    /* pointer to factory for class */
    MoBiLE_ModuleFactoryBase const *  constructor;
};

extern LDNDC_API MoBiLE_ModuleFactoryEnvelope const  module_factories[];




#define  __MODULE_FACTORY(__m_type__)    module_factory_ ## __m_type__
#define  MODULE_FACTORY(__m_type__)    ldndc::__m_type__::__MODULE_FACTORY(__m_type__)

#define  __LMOD_EXPORT_MODULE_FACTORY(__m_type__)                \
        static  MoBiLE_ModuleFactory< __m_type__ > const  __MODULE_FACTORY(__m_type__)/*;*/


#define  __LMOD_MODULE_FACTORY(__m_type__)                    \
        ldndc::MoBiLE_ModuleFactory< ldndc::__m_type__ > const  MODULE_FACTORY(__m_type__)/*;*/


#define  EXPORT_MODULE_INFO(__m_id__,__m_name__)                        \
    public:                                                             \
        static  module_info_t const &  m_info() { return  m_info_; }    \
        module_info_t const &  info() const { return this->m_info(); }  \
        char const *   name() const { return  this->name_(); }          \
        char const *   id() const { return  this->id_(); }              \
                                                                        \
    private:                                                            \
        static char const *  name_() { return  __m_name__; }            \
        static char const *  id_() { return  __m_id__; }                \
                                                                        \
        static  module_info_t  m_info_/*;*/


#define  MODULE_INFO(__m_type__,__m_timemode__,__m_flags__) \
    module_info_t  __m_type__::m_info_ = \
        module_info_t( __m_type__::id_(), __m_type__::name_(), \
            (__m_timemode__), (__m_flags__))/*;*/


#define  LDNDC_MOBILE_MODULE_OBJECT(__type__)           \
    public:                                             \
        static  __type__ *  allocate_instance()         \
        {                                               \
            return  cbm::allocatable_t< __type__ >::allocate_instance(); \
        }                                               \
        static  __type__ *  new_instance(               \
                MoBiLE_State *  _state,        \
                cbm::io_kcomm_t *  _io,                 \
                cbm::sclock_t const *,                  \
                timemode_e  _timemode)                  \
        {                                               \
            __type__ *  this_instance =                 \
                cbm::allocatable_t< __type__ >::allocate_instance(); \
                                                        \
            if ( this_instance)                         \
            {                                           \
                new ( this_instance)  __type__( _state, _io, _timemode); \
            }                                           \
            return  this_instance;                      \
        }                                               \
                                                        \
        virtual void  delete_instance()                 \
        {                                               \
            cbm::allocatable_t< __type__ >::delete_instance( this); \
        }                                               \
        virtual void  delete_instance()                 \
        const                                           \
        {                                               \
            const_cast< __type__ * >( this)->delete_instance(); \
        }                                               \
    private:

#define  LDNDC_MOBILE_MODULE_OBJECT_DEFN(__type__)  LDNDC_OBJECT_DEFN(ldndc::__type__)

#define  EXPORT_MODULE_OPTIONS_INFO(__m_type__)                 \
    public:                                                     \
        static  mobile_module_options_t const *  m_options_info()\
            { return  &options_info_for(__m_type__)->options; } \
        mobile_module_options_t const *  options_info()         \
            const { return  __m_type__::m_options_info(); }

#define  REGISTER_OPTION(__m_type__,__optkey__,__optvalue__)\
    static ldndc::module_option_register_t< ldndc::__m_type__ > const \
        __m_type__##_##__optkey__( #__optkey__, __optvalue__)/*;*/

#define  LMOD_EXPORT_MODULE_INFO(__m_type__,__m_id__,__m_name__)    \
    LDNDC_MOBILE_MODULE_OBJECT(__m_type__)          \
    public:                                         \
        __LMOD_EXPORT_MODULE_FACTORY(__m_type__);   \
    private:                                        \
        EXPORT_MODULE_OPTIONS_INFO(__m_type__);     \
    private:                                        \
        EXPORT_MODULE_INFO(__m_id__,__m_name__)


#define  LMOD_MODULE_INFO(__m_type__,__m_timemode__,__m_flags__) \
        MODULE_INFO(ldndc::__m_type__,__m_timemode__,__m_flags__); \
        LDNDC_MOBILE_MODULE_OBJECT_DEFN(__m_type__) \
        /* also instantiate factory */ \
        __LMOD_MODULE_FACTORY(__m_type__)


template < typename  _MODULE >
struct module_options_info_t
    : public ldndc::singleton_t< module_options_info_t< _MODULE > >
{
    mobile_module_options_t  options;
};
#define  options_info_for(__m_type__)   \
    module_options_info_t< __m_type__ >::get_instance()

class LDNDC_API MBE_LegacyModelBase
    :  public cbm::object_t
{
public:
    MBE_LegacyModelBase( lid_t const &  _id)
            : cbm::object_t( _id)
    { }

    cbm::sclock_t const *  lclock() const
        { return  LD_RtCfg.clk; }
    cbm::sclock_t const &  lclock_ref() const
        { return  *this->lclock(); }
};


class LDNDC_API MBE_LegacyModel : public MBE_LegacyModelBase
{
protected:
    /*!
     * @param
     *    time interval, specifies time interval
     *    this model's @fn stepping methods are called
     *    during simulation
     * @param
     *    model's object ID
     */
    explicit  MBE_LegacyModel( MoBiLE_State *, timemode_e);
public:
    virtual  ~MBE_LegacyModel() = 0;

    /* the pure virtual methods in this block
     * are implemented implicitely by the
     * macro EXPORT_MODULE_INFO */
    virtual  char const *  name() const = 0;
    virtual  char const *  id() const = 0;
    virtual  module_info_t const &  info() const = 0;
    virtual  mobile_module_options_t const *
                    options_info() const = 0;

    timemode_e  timemode() const;
    /* test for existence of other legacy models in input */
    bool  have_legacymodel( char const * /*model ID*/) const;

    /*!
     * @brief
     *  merge set of options into current set of
     *  options */
    lerr_t  merge_options( mobile_module_options_t const &);
    /*!
     * @brief
     *  query option value by key (stl string version)
     *
     * @return
     *  reference to string representation of option value
     *  if it exists, NULL otherwise
     */
    cbm::string_t const *  get_option(
        mobile_module_options_t::key_type const & /*option key*/) const;
    /*!
     * @brief
     *  query option value by key (c-string version)
     */
    cbm::string_t const *  get_option( char const * /*option key*/) const;
    /*!
     * @brief
     *  query option and cast result. if option
     *  was not provided return default value */
    template < typename  _VALUE_TYPE >
        _VALUE_TYPE  get_option( mobile_module_options_t::key_type const &  _option_key,
                _VALUE_TYPE const &  _default) const
    {
#ifdef  _DEBUG
        void const *  check_registered_dummy =
                this->get_option( _option_key);
        LDNDC_FIX_UNUSED(check_registered_dummy);
#endif
        return  this->m_options.get< _VALUE_TYPE >( _option_key, _default);
    }

    lerr_t  set_option( char const * /*option key*/,
                    cbm::string_t const * /*option value*/);

    /* get status */
    inline bool  active() const
        { return  this->m_active == 1; }

    /* tag model as active */
    void  set_active_on()
        { this->m_active = 1; }
    /* tag model as inactive */
    void  set_active_off()
        { this->m_active = 0; }

    /*!
     * @brief
     *    global configuration items (ldndc configuration file)
     *    i.e. setting model specific constants, mappings
     */
    virtual  lerr_t  configure( config_file_t const *) = 0;

    /*!
     * @brief
     *      sets the initial values of the objects
     *      properties using the given input data
     *
     * @note
     *    called only once after instantiation
     *
     *    might be removed
     */
    virtual lerr_t  initialize() = 0;

    virtual  lerr_t  register_ports( cbm::io_kcomm_t *)
        { return  LDNDC_ERR_OK; }
    virtual  lerr_t  unregister_ports( cbm::io_kcomm_t *)
        { return  LDNDC_ERR_OK; }


    virtual lerr_t  read()
        { return  LDNDC_ERR_OK; }

    virtual lerr_t  solve()
        { return  LDNDC_ERR_OK; }

    virtual lerr_t  integrate()
        { return  LDNDC_ERR_OK; }

    virtual lerr_t  write()
        { return  LDNDC_ERR_OK; }


    /*!
     * @brief
     *    whatever we wish to do after running the model
     *    code.
     * @note
     *    called from model loop in model core
     *    on time step match
     */
    virtual lerr_t  finalize() = 0;

    /*!
     * @brief
     *    execute before model is put to sleep
     */
    virtual lerr_t  sleep() = 0;

    /*!
     * @brief
     *    execute before model is being waked
     */
    virtual lerr_t  wake() = 0;

#ifdef  _HAVE_SERIALIZE
    virtual int  create_checkpoint(
                    cbm::io_kcomm_t *) { return  0; }
    virtual int  restore_checkpoint(
                    cbm::io_kcomm_t *, ldate_t const *) { return  0; }
#endif

protected:
    MoBiLE_State *  m_state;

private:
    /* run model at this time interval */
    timemode_e  m_timemode;

    /* model options */
    mobile_module_options_t  m_options;

    /* model is skipped if tagged inactive (active_ == 0) */
    mutable int  m_active;

    MBE_LegacyModel( MBE_LegacyModel const &);
    MBE_LegacyModel &  operator=( MBE_LegacyModel const &);

private:
    friend class LK_LegacyModel;
    /* legacy model info */
    mobile_modules_info_t const *  __m_moduleinfos;
    cbm::io_kcomm_t *  __m_iokcomm;
};

/*!
 * @brief
 *  supported options shall be registered before
 *  referencing them
 */
template < typename  _MODULE >
struct module_option_register_t
{
    module_option_register_t(
        char const *  _key, char const *  _description)
    {
        mobile_module_options_t::key_type  option_key =
            mobile_module_options_t::key_type( registered_key_prefix) + _key;
        mobile_module_options_t::value_type  option_description =
            _description ? _description : "";
        module_options_info_t< _MODULE > *  m_options =
            module_options_info_t< _MODULE >::get_instance();
        m_options->options.insert( option_key, option_description);
    }
};

struct LDNDC_API MoBiLE_ModuleEnvelope
{
    MoBiLE_ModuleEnvelope( MBE_LegacyModel *, char const * /*module id*/);
    MoBiLE_ModuleEnvelope( MoBiLE_ModuleEnvelope const &);

    MBE_LegacyModel *  module;
    cbm::string_t const  id;

    MoBiLE_State *  mbe_state;
    timemode_e  mbe_tmmode;
    mobile_modules_info_t const *  mbe_info;
    cbm::io_kcomm_t *  iokcomm;

    MoBiLE_ModuleFactoryBase const *  factory;

    private:
        MoBiLE_ModuleEnvelope &  operator=(
                MoBiLE_ModuleEnvelope const &);
};

typedef  std::list< MoBiLE_ModuleEnvelope >  module_container_t;

} /* namespace ldndc */

#endif  /*  !MOBILE_MODULE_H_  */

