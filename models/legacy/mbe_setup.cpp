/*!
 * @brief
 *
 * @author
 *    steffen klatt (created on: jul 04, 2013),
 *    edwin haas
 */

#include  "legacy/mbe_setup.h"

#include  <time/timemodes.h>
#include  <logging/cbm_logging.h>

bool
ldndc::mobile_module_options_t::exists(
ldndc::mobile_module_options_t::key_type const &  _key)
const
{
    return this->options.find( _key) != this->options.end();
}
ldndc::mobile_module_options_t::value_type const *
ldndc::mobile_module_options_t::get(
ldndc::mobile_module_options_t::key_type const &  _key)
const
{
    if ( this->exists( _key))
        { return  &this->options.find( _key)->second; }
    return  NULL;
}

lerr_t
ldndc::mobile_module_options_t::insert(
        ldndc::mobile_module_options_t::key_type const &  _key,
        ldndc::mobile_module_options_t::value_type const &  _value)
{
    std::pair< std::map< key_type, value_type >::iterator, bool>  rc_insert;
    rc_insert = this->options.insert(
        std::pair< key_type, value_type >( _key, _value));

    return  ( rc_insert.second) ? LDNDC_ERR_OK : LDNDC_ERR_FAIL;
}

cbm::string_t
ldndc::mobile_module_options_t::to_string()
const
{
    cbm::string_t  module_options;
    char const *  delim = "";
    static char const *  semicolon = ";";
    for ( const_iterator  option = this->options.begin();
            option != this->options.end();  ++option)
    {
        if ( cbm::has_prefix( option->first.c_str(), registered_key_prefix))
        {
            module_options += delim;
            module_options +=
                option->first.c_str() + cbm::strlen( registered_key_prefix);
            module_options += ':';
            module_options += option->second.c_str();
            delim = semicolon;
        }
    }
    return  module_options;
}


ldndc::mobile_modules_info_t::mobile_modules_info_t()
    { }
ldndc::mobile_modules_info_t::~mobile_modules_info_t()
    { }

size_t
ldndc::mobile_modules_info_t::number_of_modules() const
    { return  this->m_moduleinfos.size(); }

ldndc::mobile_module_info_t const *
ldndc::mobile_modules_info_t::get_mobile_module_info_by_id(
        char const *  _module_id) const
{
    for ( size_t  k = 0;  k < this->m_moduleinfos.size();  ++k)
    {
        if ( cbm::is_equal( this->m_moduleinfos[k].id.c_str(), _module_id))
            { return  &this->m_moduleinfos[k]; }
    }
    return  NULL;
}

ldndc::mobile_module_info_t const *
ldndc::mobile_modules_info_t::get_mobile_module_info_by_slot(
        size_t  _p) const
{
    if ( _p >= this->number_of_modules())
        { return  NULL; }
    return  &this->m_moduleinfos[_p];
}

int
ldndc::mobile_modules_info_t::add_module_setup(
        ldndc::mobile_module_info_t const &  _modulesetup)
{
    this->m_moduleinfos.push_back( _modulesetup);
    return  this->m_moduleinfos.size()-1;
}

bool
ldndc::mobile_modules_info_t::have_module( char const *  _module_id)
const
{
    if ( this->get_mobile_module_info_by_id( _module_id) == NULL)
        { return  false; }
    return  true;
}

