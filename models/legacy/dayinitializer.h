/*!
 * @brief
 *    kind of helper module to perform additional tasks
 *    before user modules are run.
 *
 * @author
 *    steffen klatt,
 *    edwin haas
 * 
 * @note
 *    Old simmaster methods:
 *        <fluxInitials> + <nd-[CL|AC]arr2scalar> + <ndGeneral> + <readEventHandlingFile>
 */

#ifndef  MBE_DAYINITIALIZER_H_
#define  MBE_DAYINITIALIZER_H_

#include  "mbe_legacymodel.h"

namespace ldndc {

class  LDNDC_API  DailyInitializer  :  public  MBE_LegacyModel
{
    LMOD_EXPORT_MODULE_INFO(DailyInitializer,"sys:*:dailyinitializer","house-keeping at beginning of day");
    public:
        DailyInitializer(
                MoBiLE_State *,
                cbm::io_kcomm_t *,
                timemode_e);
        ~DailyInitializer();

        lerr_t  configure( ldndc::config_file_t const *) { return  LDNDC_ERR_OK; }

        lerr_t  initialize() { return  LDNDC_ERR_OK; }
        lerr_t  solve();
        lerr_t  finalize() { return  LDNDC_ERR_OK; }

        lerr_t  sleep() { return  LDNDC_ERR_OK; }
        lerr_t  wake() { return  LDNDC_ERR_OK; }

    private:
        substate_physiology_t &  ph_;
        substate_soilchemistry_t &  sc_;
        substate_watercycle_t &  wc_ ;
        substate_microclimate_t &  mc_;

        MoBiLE_PlantVegetation *  m_veg;
        input_class_setup_t const *  setup;
        input_class_soillayers_t const *  soillayers;
    
        void  reset_state_items_();
};

} /* namespace ldndc */

#endif  /*  !MBE_DAYINITIALIZER_H_  */

