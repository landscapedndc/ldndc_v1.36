/*!
 * @brief
 *    MoBiLE kernel specific setup inputs
 *
 * @author
 *    steffen klatt (created on: jun 04, 2013),
 *    edwin haas
 */

#ifndef  LDNDC_MOBILE_SETUP_H_
#define  LDNDC_MOBILE_SETUP_H_

#include  "ld_legacy.h"

#include  <map>
#include  <vector>
#include  <string/cbm_string.h>

#define  registered_key_prefix  "registered.option."

namespace ldndc {

struct CBM_API mobile_module_options_t
{
    typedef std::string  key_type;
    typedef cbm::string_t  value_type;
    typedef std::map< key_type, value_type >  mobile_module_options_container_t;
    typedef mobile_module_options_container_t::const_iterator  const_iterator;

    void  clear() { this->options.clear(); }
    bool  exists( key_type const & /*key*/) const;

    value_type const *  get( key_type const & /*key*/) const;
    template < typename _VALUE_TYPE >
    _VALUE_TYPE  get( key_type const &  _key, _VALUE_TYPE const &  _default)
    const
    {
        value_type const *  option_value = this->get( _key);
        if ( !option_value)
            { return  _default; }
        return  option_value->as< _VALUE_TYPE >();
    }

    lerr_t  insert( key_type const & /*key*/, value_type const & /*value*/);
    lerr_t  merge( mobile_module_options_t const *);

    cbm::string_t  to_string() const;

    const_iterator  begin() const
        { return  this->options.begin(); }
    const_iterator  end() const
        { return  this->options.end(); }

    private:
        mobile_module_options_container_t  options;
};

/*!
 * @brief
 *    structure representing module information from
 *    MoBiLE's setup module list block
 */
struct  CBM_API  mobile_module_info_t
{
    /* unique module name */
    cbm::string_t  id;
    /* module time mode */
    timemode_e  timemode;
    /* (arbitrary) module options */
    mobile_module_options_t  options;
};


/*!
 * @brief
 *    structure manages module list information 
 */
class  CBM_API  mobile_modules_info_t
{
    public:
        mobile_modules_info_t();
        ~mobile_modules_info_t();

        size_t  number_of_modules() const;

        mobile_module_info_t const *
            get_mobile_module_info_by_id( char const * /*module ID*/) const;
        mobile_module_info_t const *
            get_mobile_module_info_by_slot( size_t /*index*/) const;
        /* return index */
        int  add_module_setup( mobile_module_info_t const &);

        bool  have_module( char const * /*module ID*/) const;

    private:
        /* information of all listed modules */
        std::vector< mobile_module_info_t >  m_moduleinfos;
};

} /* namespace ldndc */

#endif  /*  !LDNDC_MOBILE_SETUP_H_  */

