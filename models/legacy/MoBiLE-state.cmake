# vim: ft=cmake

include( ${MBE_DIR}/state/settings.cmake )

set( g_substate_hdrs)
set( g_substate_srcs)
foreach( substate ${MBE_SUBSTATES})

        set( g_substate_hdr ${CMAKE_GENERATE_DIR}/legacy/substate/mbe_${substate}.h.inc )
        set( g_substate_src ${CMAKE_GENERATE_DIR}/legacy/substate/mbe_${substate}.cpp.inc )

        file( REMOVE ${g_substate_hdr} ${g_substate_src} )

        list( APPEND g_substate_hdrs ${g_substate_hdr} )
        list( APPEND g_substate_srcs ${g_substate_src} )

        add_custom_command(
                OUTPUT "${g_substate_hdr}" "${g_substate_src}"
                COMMAND "${CMAKE_COMMAND}" -D_substate=${substate} -Dlegacy_source_dir=${MBE_DIR} -DCMAKE_SOURCE_ROOT_DIR=${CMAKE_SOURCE_DIR} -Dlegacy_binary_dir=${CMAKE_BINARY_DIR} -Dlegacy_g_source_dir=${CMAKE_GENERATE_DIR}/legacy -P ${MBE_DIR}/state/mbe_generatecodecmd.cmake
                DEPENDS ${MBE_DIR}/state/mbe_state.txt
                MAIN_DEPENDENCY ${MBE_DIR}/state/mbe_state.txt
                WORKING_DIRECTORY ${CMAKE_GENERATE_DIR}/legacy
                VERBATIM
        )

endforeach()

add_custom_target( mbe-state ALL
        SOURCES ${g_substate_hdrs} ${g_substate_srcs} )

