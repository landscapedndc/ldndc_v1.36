/*!
 * @brief
 *    kind of helper module to perform additional tasks
 *    after all user modules have been run
 *
 * @author
 *    Steffen Klatt (created on: feb 15, 2012)
 *    Edwin Haas,
 *    David Kraus,
 *    Felix Havermann
 */

#include  "subdayfinalizer.h"
#include  "state/mbe_state.h"

#include  <input/site/site.h>
#include  <input/soillayers/soillayers.h>
#include  <input/species/species.h>

#include  "scientific/meteo/ld_meteo.h"

LMOD_MODULE_INFO(SubdailyFinalizer,TMODE_SUBDAILY,LMOD_FLAG_SYSTEM|LMOD_FLAG_MAINTENANCE|LMOD_FLAG_FINALIZER);


ldndc::SubdailyFinalizer::SubdailyFinalizer(
        MoBiLE_State *  _state,
        cbm::io_kcomm_t *  _iokcomm,
        timemode_e  _timemode)
        : MBE_LegacyModel( _state, _timemode),
          se_( _iokcomm->get_input_class< setup::input_class_setup_t >()),
          sl_( _iokcomm->get_input_class< soillayers::input_class_soillayers_t >()),
          mc_( _state->get_substate_ref< substate_microclimate_t >()),
          ph_( _state->get_substate_ref< substate_physiology_t >()),
          wc_( _state->get_substate_ref< substate_watercycle_t >()),
          m_veg( &_state->vegetation),

          latitude_( _iokcomm->get_input_class< setup::input_class_setup_t >()->latitude())
{
}

ldndc::SubdailyFinalizer::~SubdailyFinalizer()
{
    // noop
}


lerr_t
ldndc::SubdailyFinalizer::solve()
{
    if ( this->lclock()->time_resolution() == 1)
    {
        return  LDNDC_ERR_OK;
    }

    // dayfraction and daylengthfraction
    double const  dayf( this->lclock()->day_fraction());
    double const  daylf( 1.0 / floor( ldndc::meteo::daylength_period( latitude_, this->lclock()->yearday())));

    static size_t const  foliage_layer_cnt = se_->canopylayers();

    /* physiology section */
    for ( PlantIterator vt = this->m_veg->begin(); vt != this->m_veg->end(); ++vt)
    {
        MoBiLE_Plant *p = *vt;

        p->d_rTra += p->rTra;
        p->d_rGro += p->rGro;
        p->d_rGroBelow += p->rGroBelow;
        p->d_rFrt += p->rFrt;
        p->d_rSap += p->rSap;
        p->d_rSapBelow += p->rSapBelow;
        p->d_rFol += p->rFol;
        p->d_rBud += p->rBud;
        p->d_rRes += p->rRes;

        p->d_dcFol += p->dcFol;
        p->d_dcFrt += p->dcFrt;
        p->d_dcSap += p->dcSap;
        p->d_dcBud += p->dcBud;
        p->d_dcFac += p->dcFac;

        p->d_sFol += p->sFol;
        p->d_sBud += p->sBud;

        p->d_nLitFol += p->nLitFol;
        p->d_nLitBud += p->nLitBud;
        p->d_n_retention += p->n_retention;

        p->d_exsuLoss += p->exsuLoss;

        p->d_nox_uptake += p->nox_uptake;
        p->d_n2_fixation += p->n2_fixation;

        p->d_sWoodAbove += p->sWoodAbove;
        p->d_nLitWoodAbove += p->nLitWoodAbove;
                
        for( size_t  sl = 0;  sl < sl_->soil_layer_cnt();  ++sl)
        {
            p->d_sFrt_sl[sl] += p->sFrt_sl[sl];
            p->d_nLitFrt_sl[sl] += p->nLitFrt_sl[sl];
            p->d_sWoodBelow_sl[sl] += p->sWoodBelow_sl[sl];
            p->d_nLitWoodBelow_sl[sl] += p->nLitWoodBelow_sl[sl];
        }

        for ( size_t  fl = 0;  fl < foliage_layer_cnt;  ++fl)
        {
            p->d_relativeconductance_fl[fl] += p->relativeconductance_fl[fl] * daylf;
            p->d_jMax_fl[fl] += p->jMax_fl[fl] * daylf;
            p->d_jPot_fl[fl] += p->jPot_fl[fl] * daylf;
            p->d_co2i_fl[fl] += p->co2i_fl[fl] * daylf;

            p->d_carbonuptake_fl[fl] += p->carbonuptake_fl[fl];
            p->d_isoprene_emission_fl[fl] += p->isoprene_emission_fl[fl];
            p->d_monoterpene_emission_fl[fl] += p->monoterpene_emission_fl[fl];
            p->d_monoterpene_s_emission_fl[fl] += p->monoterpene_s_emission_fl[fl];
            p->d_ovoc_emission_fl[fl] += p->ovoc_emission_fl[fl];
        }

        for ( size_t  na = 0;  na < p->nb_ageclasses();  ++na)
        {
            p->d_sFol_na[na] += p->sFol_na[na];
        }
    }

    for ( size_t  fl = 0;  fl < foliage_layer_cnt;  ++fl)
    {
        ph_.nd_nh3_uptake_fl[fl] += ph_.ts_nh3_uptake_fl[fl];
        ph_.nd_nox_uptake_fl[fl] += ph_.ts_nox_uptake_fl[fl];
    }


    /* microclimate section */
    if ( 1u == this->lclock()->subday())
    {
        mc_.nd_tMax_fl = -100.0;
        mc_.nd_tMin_fl = 100.0;
    }

    for ( size_t  fl = 0;  fl < foliage_layer_cnt;  ++fl)
    {
        if (mc_.temp_fl[fl] > mc_.nd_tMax_fl[fl])
        {
            mc_.nd_tMax_fl[fl] = mc_.temp_fl[fl];
        }

        if (mc_.temp_fl[fl] < mc_.nd_tMin_fl[fl])
        {
            mc_.nd_tMin_fl[fl] = mc_.temp_fl[fl];
        }

        mc_.nd_parsun_fl[fl] += mc_.parsun_fl[fl] * dayf;
        mc_.nd_shortwaveradiation_fl[fl] += mc_.shortwaveradiation_fl[fl] * dayf;
        mc_.nd_sunlitfoliagefraction_fl[fl] += (mc_.ts_sunlitfoliagefraction_fl[fl] * dayf);
        mc_.nd_temp_fl[fl] += (mc_.temp_fl[fl] * dayf);
        mc_.nd_vpd_fl[fl]  += (mc_.vpd_fl[fl]  * dayf);
        mc_.nd_win_fl[fl]  += (mc_.win_fl[fl]  * dayf);
    }
    
    mc_.nd_rad_a                  += mc_.rad_a * dayf;
    mc_.nd_shortwaveradiation_out += mc_.shortwaveradiation_out * dayf;
    mc_.nd_longwaveradiation_out  += mc_.longwaveradiation_out * dayf;
    mc_.nd_sensible_heat_out      += mc_.sensible_heat_out * dayf;
    
    if ( 1u == this->lclock()->subday())
    {
        mc_.nd_temp_sl = 0.0;
    }

    for ( size_t  sl = 0;  sl < sl_->soil_layer_cnt();  ++sl)
    {
        mc_.nd_temp_sl[sl]  += (mc_.temp_sl[sl] * dayf);
    }

    mc_.nd_temp_a += (mc_.temp_a * dayf);

    return  LDNDC_ERR_OK;
}

