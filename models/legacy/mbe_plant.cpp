/*!
 * @author
 *    Steffen Klatt (created on: jan 13, 2017),
 *    David Kraus,
 *    Ruediger Grote
 */

#include  "legacy/mbe_plant.h"
#include  "physiology/ld_plantfunctions.h"
#include  "physiology/ld_biomassdistribution.h"

#include  <logging/cbm_logging.h>


ldndc::MoBiLE_PlantSettings::MoBiLE_PlantSettings(
                                                  size_t _canopy_layers,
                                                  char const *  _name,
                                                  char const *  _type,
                                                  char const *  _group,
                                                  MoBiLE_PlantParameters const *  _parameters)
                                                : LD_PlantSettings< MoBiLE_PlantParameters >(),
                                                nb_ageclasses( MoBiLE_MaximumAgeClasses),
                                                nb_foliagelayers( _canopy_layers),
                                                nb_soillayers( 0)
{
    this->name = _name;
    this->type = _type;
    this->group = _group;
    this->parameters = _parameters;
}


#define  NB_AGECLASS_ATTR 3
#define  NB_FOLIAGELAYER_ATTR 58
#define  NB_SOILLAYER_ATTR 10

ldndc::MoBiLE_Plant::MoBiLE_Plant(
        ldndc::MoBiLE_PlantSettings const *  _plantsettings)
    : LD_Plant< ldndc::MoBiLE_PlantSettings >(
            _plantsettings, _plantsettings ? ( sizeof(double) *
        (   _plantsettings->nb_ageclasses * NB_AGECLASS_ATTR
          + _plantsettings->nb_foliagelayers * NB_FOLIAGELAYER_ATTR
          + _plantsettings->nb_soillayers * NB_SOILLAYER_ATTR)) : 0),

      slot( -1), m_plantsettings( *_plantsettings)
{
    this->dEmerg = -1;

    this->a_fix_n = 0.0;
    this->cdr = 0.0;
    this->dbas = 0.0;
    this->dbh = 0.0;
    this->rooting_depth = 0.0;

    this->dcBud = 0.0; this->d_dcBud = 0.0;
    this->dcFac = 0.0; this->d_dcFac = 0.0;
    this->dcFol = 0.0; this->d_dcFol = 0.0;
    this->dcFrt = 0.0; this->d_dcFrt = 0.0;
    this->dcSap = 0.0; this->d_dcSap = 0.0;

    this->nox_uptake = 0.0; this->d_nox_uptake = 0.0;
    this->n2_fixation = 0.0; this->d_n2_fixation = 0.0;

    this->dvsFlush = 0.0; this->dvsFlushOld = 0.0;
    this->dvsMort = 0.0;
    this->dvsWood = 0.0;

    this->exsuLoss = 0.0; this->d_exsuLoss = 0.0;
    this->f_area = 0.0;
    this->f_branch = 0.0;
    this->f_ncc = 1.0;
    this->f_fac = 0.0;
    this->f_h2o = 1.0; this->d_f_h2o = 1.0;
    this->height_max = 0.0;
    this->height_at_canopy_start = 0.0;
    this->lai_pot = 0.0;
    this->lai_max = 0.0;

    this->mBud = 0.0;
    this->mBudStart = 0.0;
    this->mCor = 0.0;
    this->dw_dst = 0.0;
    this->mFol = 0.0;
    this->dw_dfol = 0.0;
    this->mFolMax = 0.0;
    this->mFolMin = 0.0;
    this->mFrt = 0.0;
    this->mSap = 0.0;
    this->dw_lst = 0.0;

    this->ncBud = 0.0;
    this->ncCor = 0.0;
    this->ncFol = 0.0;
    this->ncFrt = 0.0;
    this->ncSap = 0.0;

    this->n_dfol = 0.0;
    this->n_dst = 0.0;
    this->n_lst = 0.0;
    
    this->nLitBud = 0.0; this->d_nLitBud = 0.0;
    this->nLitFol = 0.0; this->d_nLitFol = 0.0;
    this->nLitWoodAbove = 0.0; this->d_nLitWoodAbove = 0.0;

    this->n_retention = 0.0; this->d_n_retention = 0.0;

    this->qsfa = 0.0;
    this->qsfm = 0.0;

    this->psi_mean = 0.0;
    this->psi_thresh = 0.0;
    this->psi_pd = 0.0;
    this->psi_start = 99.99;

    this->rBud = 0.0; this->d_rBud = 0.0;
    this->rFol = 0.0; this->d_rFol = 0.0;
    this->rFrt = 0.0; this->d_rFrt = 0.0;
    this->rGro = 0.0; this->d_rGro = 0.0;
    this->rGroBelow = 0.0; this->d_rGroBelow = 0.0;
    this->rRes = 0.0; this->d_rRes = 0.0;
    this->rSap = 0.0; this->d_rSap = 0.0;
    this->rSapBelow = 0.0; this->d_rSapBelow = 0.0;
    this->rTra = 0.0; this->d_rTra = 0.0;

    this->sBud = 0.0; this->d_sBud = 0.0;
    this->sFol = 0.0; this->d_sFol = 0.0;
    this->sFrt = 0.0; this->d_sFrt = 0.0;
    this->sWoodAbove = 0.0; this->d_sWoodAbove = 0.0;

    this->growing_degree_days = 0.0;

    this->fwatOld = 1.0;

    this->tree_number = 0.0;

    this->root_tc = 0.0;

    this->xylem_resistance = 0.0;

    double **  ageclass_attributes[NB_AGECLASS_ATTR+1] = {
            &this->mFol_na, &this->sFol_na, &this->d_sFol_na, NULL };

    double **  foliage_attributes[NB_FOLIAGELAYER_ATTR+1] = {
        &this->nh3_fl, &this->nh4_fl, &this->no3_fl,
        &this->vcAct25_fl, &this->vcAct_fl, &this->vcMax25_fl, &this->vcMax_fl, &this->vcMax_std_fl,
        &this->jAct25_fl, &this->jPot_fl, &this->d_jPot_fl, &this->jMax25_fl, &this->jMax_fl, &this->jMax_std_fl, &this->d_jMax_fl, &this->rdAct25_fl,
        &this->co2comp25_fl, &this->co2comp25_std_fl, &this->co2i_fl, &this->co2i_std_fl, &this->d_co2i_fl, &this->o2i_fl, &this->o2i_std_fl, &this->rd_fl,
        &this->kco2_fl, &this->kco2_std_fl, &this->ko2_fl, &this->ko2_std_fl,
        &this->isoAct_fl, &this->monoAct_fl, &this->ef_iso_fl, &this->ef_mono_fl,
        &this->dxp_fl, &this->mep_fl, &this->idp_fl, &this->dmadp_fl, &this->gdp_fl,
        &this->relativeconductance_fl, &this->d_relativeconductance_fl,
        &this->atp_fl, &this->atp_pool_fl, &this->tp_fl, &this->tp_pool_fl, &this->nadph_fl, &this->nadph_pool_fl,
        &this->carbonuptake_fl, &this->d_carbonuptake_fl,
        &this->isoprene_emission_fl, &this->d_isoprene_emission_fl,
        &this->monoterpene_emission_fl, &this->d_monoterpene_emission_fl, &this->monoterpene_s_emission_fl, &this->d_monoterpene_s_emission_fl,
        &this->ovoc_emission_fl, &this->d_ovoc_emission_fl, 
        &this->sla_fl, &this->lai_fl, &this->fFol_fl, NULL };

    double **  soil_attributes[NB_SOILLAYER_ATTR+1] = {
        &this->rootlength_sl,
        &this->fFrt_sl,
        &this->nLitFrt_sl, &this->d_nLitFrt_sl,
        &this->sFrt_sl, &this->d_sFrt_sl,
        &this->sWoodBelow_sl, &this->d_sWoodBelow_sl, &this->nLitWoodBelow_sl, &this->d_nLitWoodBelow_sl,
        NULL };


    CBM_LogDebug( "MoBiLE_Plant #AgeClasses=", this->m_plantsettings.nb_ageclasses);
    double ***  a = NULL;
    a = ageclass_attributes;
    while ( *a)
    {
        **a = this->calloc<double>(
            sizeof(double)*this->m_plantsettings.nb_ageclasses);
        ++a;
    }
    CBM_Assert( (a-ageclass_attributes)==NB_AGECLASS_ATTR );
    CBM_LogDebug( "MoBiLE_Plant #FoliageLayers=", this->m_plantsettings.nb_foliagelayers);
    a = foliage_attributes;
    while ( *a)
    {
        **a = this->calloc<double>(
            sizeof(double)*this->m_plantsettings.nb_foliagelayers);
        ++a;
    }
    CBM_Assert( (a-foliage_attributes)==NB_FOLIAGELAYER_ATTR );
    CBM_LogDebug( "MoBiLE_Plant #SoilLayers=", this->m_plantsettings.nb_soillayers);
    a = soil_attributes;
    while ( *a)
    {
        **a = this->calloc<double>(
            sizeof(double)*this->m_plantsettings.nb_soillayers);
        ++a;
    }
    CBM_Assert( (a-soil_attributes)==NB_SOILLAYER_ATTR );


    /* non-zero initializations */
    for ( size_t  l = 0; l < this->m_plantsettings.nb_foliagelayers; ++l)
    {
        this->co2comp25_fl[l] = 30.0;
        this->co2i_fl[l] = 370.0;
        this->o2i_fl[l] = 210.0;
        this->co2comp25_std_fl[l] = 30.0;
        this->co2i_std_fl[l] = 370.0;
        this->o2i_std_fl[l] = 210.0;
    }

    this->seeding_date = LD_RtCfg.clk->now();
    this->is_covercrop = false;
    this->initial_biomass = 0.0;
}

ldndc::MoBiLE_Plant::~MoBiLE_Plant()
{ }

lerr_t ldndc::MoBiLE_Plant::initialize()
{
    return  LDNDC_ERR_OK;
}

bool  ldndc::MoBiLE_Plant::is_family( char const *  _family) const
    { return this->m_plantvegetation->is_family( this, _family); }

size_t  ldndc::MoBiLE_Plant::nb_ageclasses() const
    { return 1 + ( 0.99 * this->parameters()->DLEAFSHED()) / 365.0; }

size_t  ldndc::MoBiLE_Plant::nb_foliagelayers() const
    {
        ldndc_assert( cbm::H_FLMIN > 0.0);

        int fl_cnt( static_cast<int>( ceil( height_max / cbm::H_FLMIN)));
        if ( fl_cnt < MIN_FOLIAGE_LAYER)
        {
            return MIN_FOLIAGE_LAYER;
        }
        else if (fl_cnt > static_cast<int>(m_plantsettings.nb_foliagelayers))
        {
            return m_plantsettings.nb_foliagelayers;
        }

        return fl_cnt;
    }

size_t  ldndc::MoBiLE_Plant::nb_soillayers() const
    { return m_plantsettings.nb_soillayers; }

ldndc::MoBiLE_PlantVegetation::MoBiLE_PlantVegetation(
        LD_PlantsParametersDB const * _parameters_database)
    : LD_PlantVegetation< ldndc::MoBiLE_Plant >( _parameters_database)
    {}
ldndc::MoBiLE_PlantVegetation::~MoBiLE_PlantVegetation()
    { }

static int  MoBiLE_PlantVegetation_find_slot(
            ldndc::MoBiLE_PlantVegetation *  _veg)
{
    int  new_slot = 0;
    for ( size_t  s = 0;  s < _veg->size();  ++s)
    {
        new_slot = s;
        MoBiLE_PlantVegetation::Iterator  pi = _veg->begin();
        while ( pi )
        {
            if ((*pi)->slot == s)
                { new_slot = -1; break; }
            ++pi;
        }
        if ( new_slot != -1)
            { break; }
    }
    return  new_slot;
}

ldndc::MoBiLE_Plant *  ldndc::MoBiLE_PlantVegetation::new_plant(
        MoBiLE_PlantSettings const *  _plantsettings)
{
    MoBiLE_Plant *  plant = LD_PlantVegetation< MoBiLE_Plant >::new_plant( _plantsettings);
    if ( plant)
    {
        /* [BACKWARD COMPATIBILITY] find available slot */
        plant->slot = MoBiLE_PlantVegetation_find_slot( this);
        plant->m_plantvegetation = this;
        CBM_LogDebug( "slot for plant \"",_plantsettings->name,"\" is ", plant->slot);
    }
    return  plant;
}

lerr_t  ldndc::MoBiLE_PlantVegetation::delete_plant(
        char const *  _name)
{
    lerr_t  rc_delete = LD_PlantVegetation< MoBiLE_Plant >::delete_plant( _name);
    if ( rc_delete)
        { return LDNDC_ERR_FAIL; }
    return  LDNDC_ERR_OK;
}

ldndc::MoBiLE_PlantParameters
ldndc::MoBiLE_PlantVegetation::get_parameters( char const * _type)
{
    if ( _type && this->m_speciesparameters)
        { return (*this->m_speciesparameters)[_type]; }
    return MoBiLE_PlantParameters();
}

bool  ldndc::MoBiLE_PlantVegetation::is_family(
        MoBiLE_Plant const *  _plant, char const *  _family) const
    { return this->m_speciesparameters->is_family( _plant->ctype(), _family); }
bool  ldndc::MoBiLE_PlantVegetation::is_family(
        char const *  _type, char const *  _family) const
{
    return this->m_speciesparameters->is_family( _type, _family);
}

size_t  ldndc::MoBiLE_PlantVegetation::slot_cnt()
{
    if ( this->size() == 0)
        { return 0; }

    size_t  slot = 0;
    for ( Iterator  pi = this->begin(); pi != this->end(); ++pi)
    {
        if ((*pi)->slot > slot)
            { slot = (*pi)->slot; }
    }
    return slot+1;
}

double  ldndc::MoBiLE_PlantVegetation::canopy_height()
{
    double height( 0.0);
    for ( Iterator  pi = this->begin(); pi != this->end(); ++pi)
    {
        if ( (*pi)->height_max > height)
        {
            height = (*pi)->height_max;
        }
    }
    return height;
}

size_t  ldndc::MoBiLE_PlantVegetation::canopy_layers_used()
{
    size_t fl_cnt( MIN_FOLIAGE_LAYER);
    for ( Iterator  pi = this->begin(); pi != this->end(); ++pi)
    {
        size_t fl_cnt_pi( (*pi)->nb_foliagelayers());
        if ( fl_cnt_pi > fl_cnt)
        {
            fl_cnt = fl_cnt_pi;
        }
    }
    return fl_cnt;
}

/* returns total vegetation leaf area index */
double
ldndc::MoBiLE_PlantVegetation::lai()
{
    double lai_sum(0.0);
    for ( Iterator vt = this->begin(); vt != this->end(); ++vt)
    {
        lai_sum += (*vt)->lai();
    }
    return lai_sum;
}

/* returns total vegetation leaf area index */
double
ldndc::MoBiLE_PlantVegetation::lai_fl( size_t _fl)
{
    double lai_sum(0.0);
    for ( Iterator vt = this->begin(); vt != this->end(); ++vt)
    {
        lai_sum += (*vt)->lai_fl[_fl];
    }
    return lai_sum;
}

/* returns total vegetation root biomass */
double
ldndc::MoBiLE_PlantVegetation::dw_frt()
{
    double mfrt( 0.0);
    for ( Iterator vt = this->begin(); vt != this->end(); ++vt)
    {
        mfrt += (*vt)->mFrt;
    }
    return mfrt;
}

/* returns total vegetation root biomass per soil layer */
double
ldndc::MoBiLE_PlantVegetation::mfrt_sl( size_t _sl)
{
    double mfrt( 0.0);
    for ( Iterator vt = this->begin(); vt != this->end(); ++vt)
    {
        mfrt += (*vt)->mFrt * (*vt)->fFrt_sl[_sl];
    }
    return mfrt;
}

/* returns total vegetation root length per soil layer */
double
ldndc::MoBiLE_PlantVegetation::rootlength_sl( size_t _sl)
{
    double root_length( 0.0);
    for ( Iterator vt = this->begin(); vt != this->end(); ++vt)
    {
        root_length += (*vt)->rootlength_sl[_sl];
    }
    return root_length;
}

/* area cover */
double
ldndc::MoBiLE_PlantVegetation::area_cover()
{
    double area_cover( 0.0);
    for ( Iterator vt = this->begin(); vt != this->end(); ++vt)
    {
        area_cover += (*vt)->f_area;
    }

    return cbm::bound_max( area_cover, 1.0);;
}

void
ldndc::MoBiLE_Plant::reset()
{
    this->tree_number = 0.0;

    this->mFol = 0.0;
    this->mFolMin = 0.0;
    this->mFolMax = 0.0;

    this->mSap = 0.0;
    this->mCor = 0.0;

    this->mBud = 0.0;
    this->mBudStart = 0.0;

    this->mFrt = 0.0;

    this->height_max = 0.0;
    this->height_at_canopy_start = 0.0;

    this->lai_max = 0.0;
    this->lai_pot = 0.0;
    this->qsfm = 0.0;

    this->psi_pd = 0.0;
    this->psi_mean = 0.0;
    this->psi_start = 99.99;

    this->cdr = 0.0;
    this->dbas = 0.0;
    this->dbh = 0.0;

    this->tree_number = 0.0;

    for ( size_t l = 0; l < this->NB_SOILLAYERS(); ++l)
    {
        this->fFrt_sl[l] = 0.0;
    }
    for ( size_t l = 0; l < this->NB_FOLIAGELAYERS(); ++l)
    {
        this->lai_fl[l] = 0.0;
        this->sla_fl[l] = 0.0;
        this->fFol_fl[l] = 0.0;
    }
}

