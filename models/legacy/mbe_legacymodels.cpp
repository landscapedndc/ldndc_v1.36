/*!
 * @brief
 *    a list of destructors (because i was to lazy
 *    to add a one line file for these base classes..)
 *    @n
 *    module factory array is defined here.
 *
 * @author
 *    - steffen klatt,
 *    - edwin haas,
 */

#include  "legacy/mbe_legacymodels.h"
#include  "legacy/mbe_legacymodels.h.inc"
#include  "legacy/mbe_legacymodel.h"

#ifdef  MoBiLE_MODULES

#include  "legacy/airchemistryput.h"
#include  "legacy/climateput.h"

#include  "legacy/dayinitializer.h"
#include  "legacy/subdayinitializer.h"
#include  "legacy/subdayfinalizer.h"

#ifdef  MoBiLE_AirchemistryDNDC
#  include "airchemistry/dndc/airchemistry-dndc.h"
#endif
#ifdef  MoBiLE_EventHandlerCut
#  include "eventhandler/cut/cut.h"
#endif
#ifdef  MoBiLE_EventHandlerFertilize
#  include "eventhandler/fertilize/fertilize.h"
#endif
#ifdef  MoBiLE_EventHandlerGraze
#  include "eventhandler/graze/graze.h"
#endif
#ifdef  MoBiLE_EventHandlerFlood
#  include "eventhandler/flood/flood.h"
#endif
#ifdef  MoBiLE_EventHandlerIrrigate
#  include "eventhandler/irrigate/irrigate.h"
#endif
#ifdef  MoBiLE_EventHandlerSpitfireOutputWriter
#  include "eventhandler/spitfire/output-spitfire.h"
#endif
#ifdef  MoBiLE_EventHandlerSpitFire
#  include "eventhandler/spitfire/spitfire.h"
#endif
#ifdef  MoBiLE_EventHandlerTill
#  include "eventhandler/till/till.h"
#endif
#ifdef  MoBiLE_MicroClimateCanopyECM
#  include "microclimate/canopyecm/canopyecm.h"
#endif
#ifdef  MoBiLE_OutputArableReport
#  include "output/event-report/output-arable-report.h"
#endif
#ifdef  MoBiLE_OutputArableReportCut
#  include "output/event-report/output-arable-report-cut.h"
#endif
#ifdef  MoBiLE_OutputArableReportFertilize
#  include "output/event-report/output-arable-report-fertilize.h"
#endif
#ifdef  MoBiLE_OutputArableReportGraze
#  include "output/event-report/output-arable-report-graze.h"
#endif
#ifdef  MoBiLE_OutputArableReportHarvest
#  include "output/event-report/output-arable-report-harvest.h"
#endif
#ifdef  MoBiLE_OutputArableReportManure
#  include "output/event-report/output-arable-report-manure.h"
#endif
#ifdef  MoBiLE_OutputEventReportThin
#  include "output/event-report/output-event-report-thin.h"
#endif
#ifdef  MoBiLE_OutputEventReportThrow
#  include "output/event-report/output-event-report-throw.h"
#endif
#ifdef  MoBiLE_Isotopes
#  ifdef  MoBiLE_OutputIsotopesDaily
#    include "output/isotopes/output-isotopes-daily.h"
#  endif
#  ifdef  MoBiLE_OutputIsotopesSubdaily
#    include "output/isotopes/output-isotopes-subdaily.h"
#  endif
#endif
#ifdef  MoBiLE_OutputInventory
#  include "output/inventory/output-inventory.h"
#endif
#ifdef  MoBiLE_OutputEcosystemDaily
#  include "output/ecosystem/output-ecosystem-daily.h"
#endif
#ifdef  MoBiLE_OutputEcosystemYearly
#  include "output/ecosystem/output-ecosystem-yearly.h"
#endif
#ifdef  MoBiLE_OutputMicroclimateDaily
#  include "output/microclimate/output-microclimate-daily.h"
#endif
#ifdef  MoBiLE_OutputMicroclimateLayerDaily
#  include "output/microclimate/output-microclimate-layer-daily.h"
#endif
#ifdef  MoBiLE_OutputMicroclimateSubdaily
#  include "output/microclimate/output-microclimate-subdaily.h"
#endif
#ifdef  MoBiLE_OutputMicroclimateLayerSubdaily
#  include "output/microclimate/output-microclimate-layer-subdaily.h"
#endif
#ifdef  MoBiLE_OutputMicroclimateHorizonsDaily
#  include "output/microclimate/output-microclimate-horizons-daily.h"
#endif
#ifdef  MoBiLE_OutputPhysiologyYearly
#  include "output/physiology/output-physiology-yearly.h"
#endif
#ifdef  MoBiLE_OutputPhysiologyDaily
#  include "output/physiology/output-physiology-daily.h"
#endif
#ifdef  MoBiLE_OutputPhysiologyLayerDaily
#  include "output/physiology/output-physiology-layer-daily.h"
#endif
#ifdef  MoBiLE_OutputPhysiologySubdaily
#  include "output/physiology/output-physiology-subdaily.h"
#endif
#ifdef  MoBiLE_OutputPhysiologyHorizonsDaily
#  include "output/physiology/output-physiology-horizons-daily.h"
#endif
#ifdef  MoBiLE_OutputSoilchemistryDaily
#  include "output/soilchemistry/output-soilchemistry-daily.h"
#endif
#ifdef  MoBiLE_OutputSoilchemistryLayerDaily
#  include "output/soilchemistry/output-soilchemistry-layer-daily.h"
#endif
#ifdef  MoBiLE_OutputSoilchemistryLayerSubdaily
#  include "output/soilchemistry/output-soilchemistry-layer-subdaily.h"
#endif
#ifdef  MoBiLE_OutputSoilchemistrySubdaily
#  include "output/soilchemistry/output-soilchemistry-subdaily.h"
#endif
#ifdef  MoBiLE_OutputSoilchemistryYearly
#  include "output/soilchemistry/output-soilchemistry-yearly.h"
#endif
#ifdef  MoBiLE_OutputSoilchemistryHorizonsDaily
#  include "output/watercycle/output-soilchemistry-horizons-daily.h"
#endif
#ifdef  MoBiLE_OutputVegstructureDaily
#  include "output/vegstructure/output-vegstructure-daily.h"
#endif
#ifdef  MoBiLE_OutputVegstructureHorizonsDaily
#  include "output/vegstructure/output-vegstructure-horizons-daily.h"
#endif
#ifdef  MoBiLE_OutputVegstructureLayerDaily
#  include "output/vegstructure/output-vegstructure-layer-daily.h"
#endif
#ifdef  MoBiLE_OutputVegstructureYearly
#  include "output/vegstructure/output-vegstructure-yearly.h"
#endif
#ifdef  MoBiLE_OutputVegstructureLayerYearly
#  include "output/vegstructure/output-vegstructure-layer-yearly.h"
#endif
#ifdef  MoBiLE_OutputWatercycleYearly
#  include "output/watercycle/output-watercycle-yearly.h"
#endif
#ifdef  MoBiLE_OutputWatercycleDaily
#  include "output/watercycle/output-watercycle-daily.h"
#endif
#ifdef  MoBiLE_OutputWatercycleLayerDaily
#  include "output/watercycle/output-watercycle-layer-daily.h"
#endif
#ifdef  MoBiLE_OutputWatercycleLayerSubdaily
#  include "output/watercycle/output-watercycle-layer-subdaily.h"
#endif
#ifdef  MoBiLE_OutputWatercycleSubdaily
#  include "output/watercycle/output-watercycle-subdaily.h"
#endif
#ifdef  MoBiLE_OutputWatercycleHorizonsDaily
#  include "output/watercycle/output-watercycle-horizons-daily.h"
#endif
#ifdef  MoBiLE_OutputDSSYearly
#  include "output/dss/output-dss-yearly.h"
#endif
#ifdef  MoBiLE_OutputMoBiLEKernelStatistics
#  include "output/stats/output-stats.h"
#endif
#ifdef  MoBiLE_PhysiologyDNDC
#  include "physiology/dndc/arable-dndc.h"
#  include "physiology/dndc/grassland-dndc.h"
#endif
#ifdef  MoBiLE_PhysiologyPlaMox
#  include "physiology/plamox/plamox.h"
#endif
#ifdef  MoBiLE_PhysiologyGrowthPSIM
#  include "physiology/psim/psim.h"
#endif
#ifdef  MoBiLE_PhysiologyOryza2000
#  include "physiology/oryza2000/oryza2000.h"
#endif
#ifdef  MoBiLE_PhysiologyPHOTOFARQUHAR
#  include "physiology/photofarquhar/photofarquhar.h"
#endif
#ifdef  MoBiLE_PhysiologyPNET
#  include "physiology/pnet/pnet.h"
#endif
#ifdef  MoBiLE_SoilChemistryDNDC
#  include "soilchemistry/dndc/soilchemistry-dndc.h"
#endif
#ifdef  MoBiLE_SoilChemistryMeTrX
#  include "soilchemistry/metrx/soilchemistry-metrx.h"
#endif
#ifdef  MoBiLE_SoilChemistrySimplicity
#  include "soilchemistry/simplicity/soilchemistry-simplicity.h"
#endif
#ifdef  MoBiLE_WaterCycleDNDC
#  include "watercycle/dndc/watercycle-dndc.h"
#endif
#ifdef  MoBiLE_EcHy
#  include "watercycle/echy/echy.h"
#endif
#ifdef  MoBiLE_WaterCycleOryzaET
#  include "watercycle/oryza-et/oryza-et.h"
#endif
#ifdef  MoBiLE_AirchemistryEcho
#  include "test/airchemistry-echo.h"
#endif
#ifdef  MoBiLE_ClimateEcho
#  include "test/climate-echo.h"
#endif
#ifdef  MoBiLE_EventEcho
#  include "test/event-echo.h"
#endif
#ifdef  MoBiLE_SpeciesEcho
#  include "test/species-echo.h"
#endif

#endif  /* MoBiLE_MODULES */

/* filling the module factory array. entries in
 * this array are determined at compile time based
 * on the given cmake build options
 */
ldndc::MoBiLE_ModuleFactoryEnvelope const
    ldndc::module_factories[] =
{

    /* entries have the form
     *    { &MODULE_FACTORY(<MODULE_CLASS_NAME>)} */

#ifdef  MoBiLE_MODULES

    { &MODULE_FACTORY(DailyInitializer)},
    { &MODULE_FACTORY(SubdailyInitializer)},
    { &MODULE_FACTORY(SubdailyFinalizer)},

    { &MODULE_FACTORY(AirChemistryDayBufferUpdate)},
    { &MODULE_FACTORY(ClimateDayBufferUpdate)},

#ifdef  MoBiLE_AirchemistryDNDC
    { &MODULE_FACTORY(AirchemistryDNDC)},
#endif
#ifdef  MoBiLE_EventHandlerCut
    { &MODULE_FACTORY(EventHandlerCut)},
#endif
#ifdef  MoBiLE_EventHandlerFertilize
    { &MODULE_FACTORY(EventHandlerFertilize)},
#endif
#ifdef  MoBiLE_EventHandlerGraze
    { &MODULE_FACTORY(EventHandlerGraze)},
#endif
#ifdef  MoBiLE_EventHandlerFlood
    { &MODULE_FACTORY(EventHandlerFlood)},
#endif
#ifdef  MoBiLE_EventHandlerIrrigate
    { &MODULE_FACTORY(EventHandlerIrrigate)},
#endif
#ifdef  MoBiLE_EventHandlerSpitfireOutputWriter
    { &MODULE_FACTORY(EventHandlerSpitfireOutputWriter)},
#endif
#ifdef  MoBiLE_EventHandlerSpitFire
    { &MODULE_FACTORY(EventHandlerSpitFire)},
#endif
#ifdef  MoBiLE_EventHandlerTill
    { &MODULE_FACTORY(EventHandlerTill)},
#endif
#ifdef  MoBiLE_MicroClimateCanopyECM
    { &MODULE_FACTORY(MicroClimateCanopyECM)},
#endif
#ifdef  MoBiLE_OutputArableReport
    { &MODULE_FACTORY(OutputArableReport)},
#endif
#ifdef  MoBiLE_OutputArableReportCut
    { &MODULE_FACTORY(OutputArableReportCut)},
#endif
#ifdef  MoBiLE_OutputArableReportFertilize
    { &MODULE_FACTORY(OutputArableReportFertilize)},
#endif
#ifdef  MoBiLE_OutputArableReportGraze
    { &MODULE_FACTORY(OutputArableReportGraze)},
#endif
#ifdef  MoBiLE_OutputArableReportHarvest
    { &MODULE_FACTORY(OutputArableReportHarvest)},
#endif
#ifdef  MoBiLE_OutputArableReportManure
    { &MODULE_FACTORY(OutputArableReportManure)},
#endif
#ifdef  MoBiLE_OutputEventReportThin
    { &MODULE_FACTORY(OutputEventReportThin)},
#endif
#ifdef  MoBiLE_OutputEventReportThrow
    { &MODULE_FACTORY(OutputEventReportThrow)},
#endif
#ifdef  MoBiLE_Isotopes
#  ifdef  MoBiLE_OutputIsotopesDaily
    { &MODULE_FACTORY(OutputIsotopesDaily)},
#  endif
#  ifdef  MoBiLE_OutputIsotopesSubdaily
    { &MODULE_FACTORY(OutputIsotopesSubdaily)},
#  endif
#endif
#ifdef  MoBiLE_OutputInventory
    { &MODULE_FACTORY(OutputInventory)},
#endif
#ifdef  MoBiLE_OutputEcosystemDaily
    { &MODULE_FACTORY(OutputEcosystemDaily)},
#endif
#ifdef  MoBiLE_OutputEcosystemYearly
    { &MODULE_FACTORY(OutputEcosystemYearly)},
#endif
#ifdef  MoBiLE_OutputMicroclimateDaily
    { &MODULE_FACTORY(OutputMicroclimateDaily)},
#endif
#ifdef  MoBiLE_OutputMicroclimateLayerDaily
    { &MODULE_FACTORY(OutputMicroclimateLayerDaily)},
#endif
#ifdef  MoBiLE_OutputMicroclimateSubdaily
    { &MODULE_FACTORY(OutputMicroclimateSubdaily)},
#endif
#ifdef  MoBiLE_OutputMicroclimateLayerSubdaily
    { &MODULE_FACTORY(OutputMicroclimateLayerSubdaily)},
#endif
#ifdef  MoBiLE_OutputMicroclimateHorizonsDaily
    { &MODULE_FACTORY(OutputMicroclimateHorizonsDaily)},
#endif
#ifdef  MoBiLE_OutputPhysiologyYearly
    { &MODULE_FACTORY(OutputPhysiologyYearly)},
#endif
#ifdef  MoBiLE_OutputPhysiologyDaily
    { &MODULE_FACTORY(OutputPhysiologyDaily)},
#endif
#ifdef  MoBiLE_OutputPhysiologyLayerDaily
    { &MODULE_FACTORY(OutputPhysiologyLayerDaily)},
#endif
#ifdef  MoBiLE_OutputPhysiologySubdaily
    { &MODULE_FACTORY(OutputPhysiologySubdaily)},
#endif
#ifdef  MoBiLE_OutputPhysiologyHorizonsDaily
    { &MODULE_FACTORY(OutputPhysiologyHorizonsDaily)},
#endif
#ifdef  MoBiLE_OutputSoilchemistryDaily
    { &MODULE_FACTORY(OutputSoilchemistryDaily)},
#endif
#ifdef  MoBiLE_OutputSoilchemistryLayerDaily
    { &MODULE_FACTORY(OutputSoilchemistryLayerDaily)},
#endif
#ifdef  MoBiLE_OutputSoilchemistryLayerSubdaily
    { &MODULE_FACTORY(OutputSoilchemistryLayerSubdaily)},
#endif
#ifdef  MoBiLE_OutputSoilchemistrySubdaily
    { &MODULE_FACTORY(OutputSoilchemistrySubdaily)},
#endif
#ifdef  MoBiLE_OutputSoilchemistryYearly
    { &MODULE_FACTORY(OutputSoilchemistryYearly)},
#endif
#ifdef  MoBiLE_OutputSoilchemistryHorizonsDaily
    { &MODULE_FACTORY(OutputSoilchemistryHorizonsDaily)},
#endif
#ifdef  MoBiLE_OutputVegstructureDaily
    { &MODULE_FACTORY(OutputVegstructureDaily)},
#endif
#ifdef  MoBiLE_OutputVegstructureHorizonsDaily
    { &MODULE_FACTORY(OutputVegstructureHorizonsDaily)},
#endif
#ifdef  MoBiLE_OutputVegstructureLayerDaily
    { &MODULE_FACTORY(OutputVegstructureLayerDaily)},
#endif
#ifdef  MoBiLE_OutputVegstructureLayerYearly
    { &MODULE_FACTORY(OutputVegstructureLayerYearly)},
#endif
#ifdef  MoBiLE_OutputVegstructureYearly
    { &MODULE_FACTORY(OutputVegstructureYearly)},
#endif
#ifdef  MoBiLE_OutputVegstructureHorizonsDaily
    { &MODULE_FACTORY(OutputVegstructureHorizonsDaily)},
#endif
#ifdef  MoBiLE_OutputWatercycleYearly
    { &MODULE_FACTORY(OutputWatercycleYearly)},
#endif
#ifdef  MoBiLE_OutputWatercycleDaily
    { &MODULE_FACTORY(OutputWatercycleDaily)},
#endif
#ifdef  MoBiLE_OutputWatercycleLayerDaily
    { &MODULE_FACTORY(OutputWatercycleLayerDaily)},
#endif
#ifdef  MoBiLE_OutputWatercycleLayerSubdaily
    { &MODULE_FACTORY(OutputWatercycleLayerSubdaily)},
#endif
#ifdef  MoBiLE_OutputWatercycleSubdaily
    { &MODULE_FACTORY(OutputWatercycleSubdaily)},
#endif
#ifdef  MoBiLE_OutputWatercycleHorizonsDaily
    { &MODULE_FACTORY(OutputWatercycleHorizonsDaily)},
#endif
#ifdef  MoBiLE_OutputDSSYearly
    { &MODULE_FACTORY(OutputDSSYearly)},
#endif
#ifdef  MoBiLE_OutputMoBiLEKernelStatistics
    { &MODULE_FACTORY(OutputMoBiLEKernelStatistics)},
#endif
#ifdef  MoBiLE_PhysiologyDNDC
    { &MODULE_FACTORY(PhysiologyArableDNDC)},
    { &MODULE_FACTORY(PhysiologyGrasslandDNDC)},
#endif
#ifdef  MoBiLE_PhysiologyPlaMox
    { &MODULE_FACTORY(PhysiologyPlaMox)},
#endif
#ifdef  MoBiLE_PhysiologyGrowthPSIM
    { &MODULE_FACTORY(PhysiologyGrowthPSIM)},
#endif
#ifdef  MoBiLE_PhysiologyMPC
    { &MODULE_FACTORY(PhysiologyMPC)},
#endif
#ifdef  MoBiLE_PhysiologyOryza2000
    { &MODULE_FACTORY(PhysiologyOryza2000)},
#endif
#ifdef  MoBiLE_PhysiologyPHOTOFARQUHAR
    { &MODULE_FACTORY(PhysiologyPHOTOFARQUHAR)},
#endif
#ifdef  MoBiLE_PhysiologyPNET
    { &MODULE_FACTORY(PhysiologyPNET)},
#endif
#ifdef  MoBiLE_SoilChemistryDECONIT
    { &MODULE_FACTORY(SoilChemistryDECONIT)},
#endif
#ifdef  MoBiLE_SoilChemistryDNDC
    { &MODULE_FACTORY(SoilChemistryDNDC)},
#endif
#ifdef  MoBiLE_SoilChemistryMeTrX
    { &MODULE_FACTORY(SoilChemistryMeTrX)},
#endif
#ifdef  MoBiLE_SoilChemistrySimplicity
    { &MODULE_FACTORY(SoilChemistrySimplicity)},
#endif
#ifdef  MoBiLE_WaterCycleDNDC
    { &MODULE_FACTORY(WatercycleDNDC)},
#endif
#ifdef  MoBiLE_EcHy
    { &MODULE_FACTORY(EcHy)},
#endif
#ifdef  MoBiLE_WaterCycleOryzaET
    { &MODULE_FACTORY(WaterCycleOryzaET)},
#endif
#ifdef  MoBiLE_AirchemistryEcho
    { &MODULE_FACTORY(AirchemistryEcho)},
#endif
#ifdef  MoBiLE_ClimateEcho
    { &MODULE_FACTORY(ClimateEcho)},
#endif
#ifdef  MoBiLE_EventEcho
    { &MODULE_FACTORY(EventEcho)},
#endif
#ifdef  MoBiLE_SpeciesEcho
    { &MODULE_FACTORY(SpeciesEcho)},
#endif

#endif  /* MoBiLE_MODULES */

    /* sentinel (constructor must be null) */
    { NULL}
};


#include  <stdio.h>
#ifdef __cplusplus
extern "C" {
#endif
void
ldndc_list_available_legacymodels( unsigned char  _verbose)
{
    static char const *  del_empty( "");
    static char const *  del_sym( "|");
    unsigned int  k( 0);

    while ( module_factories[k].exists())
    {
        if (( !( module_factories[k].info().mflags  &  LMOD_FLAG_SYSTEM)) || ( _verbose > 1))
        {
            fprintf( stdout, "%s", module_factories[k].info().name);
            if ( _verbose > 0)
            {
                fprintf( stdout, "\n%10sid=\"%s\"\n%10stimemode=\"",
                        "", module_factories[k].info().id,
                        "");
                char const *  del( del_empty);
                for ( unsigned char  j = 1;  ldndc::time::TMODE_NAMES[j];  ++j)
                {
                    if (( module_factories[k].info().timemode & ((lflags_t)1 << (j-1))) != 0)
                    {
                        fprintf( stdout, "%s%s", del, ldndc::time::TMODE_NAMES[j]);
                        del = del_sym;
                    }
                }
                fprintf( stdout, "\"");

                cbm::string_t const  options_info =
                    module_factories[k].options_info().to_string();
                if ( options_info.length() > 0)
                {
                        fprintf( stdout, "\n%10soptions=\"%s\"", " ", options_info.c_str());
                }
                fprintf( stdout, "\n");
            }
            fprintf( stdout, "\n");
        }
        ++k;
    }
}
#ifdef __cplusplus
}
#endif
