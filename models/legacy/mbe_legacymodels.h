/*!
 * @brief
 *
 * @author
 *    steffen klatt (created on: feb 16, 2012),
 *    edwin haas,
 */

#ifndef  MBE_LEGACYMODELS_H_
#define  MBE_LEGACYMODELS_H_

#include  "ldndc-dllexport.h"

#ifdef __cplusplus
extern "C" {
#endif

LDNDC_API
extern void  ldndc_list_available_legacymodels( unsigned char /*verbose level*/);

#ifdef __cplusplus
}
#endif

#endif  /* !MBE_LEGACYMODELS_H_ */

