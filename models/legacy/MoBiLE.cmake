# ====================================================================================
#
#  MoBiLE Legacy code
#
# ====================================================================================

option( MoBiLE_ENABLE_MODULES_ldndc "For backward-compatibility (DO NOT CHANGE)" ON )
set( MBE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/legacy )

## MoBiLE Customization
set( MoBiLE_MaximumAgeClasses "10" CACHE STRING "maximum number of foliage age classes" )

file( MAKE_DIRECTORY ${CMAKE_GENERATE_DIR}/legacy )
configure_file( ${MBE_DIR}/mbe_legacymodels.h.in
        ${CMAKE_GENERATE_DIR}/legacy/mbe_legacymodels.h.inc )
configure_file( ${MBE_DIR}/mbe_config.h.in
        ${CMAKE_GENERATE_DIR}/legacy/mbe_config.h.inc )

set( MBE_SOURCES
    ${MBE_DIR}/mbe_legacymodel.h ${MBE_DIR}/mbe_legacymodel.cpp
    ${MBE_DIR}/mbe_legacymodels.h ${MBE_DIR}/mbe_legacymodels.cpp
    ${MBE_DIR}/mbe_legacyoutputmodel.h ${MBE_DIR}/mbe_legacyoutputmodel.cpp
    ${MBE_DIR}/mbe_plant.h ${MBE_DIR}/mbe_plant.cpp
    ${MBE_DIR}/mbe_setup.h ${MBE_DIR}/mbe_setup.cpp

    ## MoBiLE state
    ${MBE_DIR}/state/mbe_state.h ${MBE_DIR}/state/mbe_state.cpp
    ${MBE_DIR}/state/mbe_statecheckpoint.h ${MBE_DIR}/state/mbe_statecheckpoint.cpp
    ${MBE_DIR}/state/mbe_substate.h ${MBE_DIR}/state/mbe_substate.cpp
    ${MBE_DIR}/state/mbe_substates.h ${MBE_DIR}/state/mbe_substates.cpp
    ${MBE_DIR}/state/mbe_substatetypes.h ${MBE_DIR}/state/mbe_substatetypes.cpp
    ${MBE_DIR}/state/substates/mbe_airchemistry.h ${MBE_DIR}/state/substates/mbe_airchemistry.cpp
    ${MBE_DIR}/state/substates/mbe_microclimate.h ${MBE_DIR}/state/substates/mbe_microclimate.cpp
    ${MBE_DIR}/state/substates/mbe_physiology.h ${MBE_DIR}/state/substates/mbe_physiology.cpp
    ${MBE_DIR}/state/substates/mbe_soilchemistry.h ${MBE_DIR}/state/substates/mbe_soilchemistry.cpp
    ${MBE_DIR}/state/substates/mbe_surfacebulk.h ${MBE_DIR}/state/substates/mbe_surfacebulk.cpp
    ${MBE_DIR}/state/substates/mbe_watercycle.h ${MBE_DIR}/state/substates/mbe_watercycle.cpp
    ${MBE_DIR}/state/substates/mbe_common.h ${MBE_DIR}/state/substates/mbe_common.cpp
    
    ${MBE_DIR}/airchemistryput.h ${MBE_DIR}/airchemistryput.cpp
    ${MBE_DIR}/climateput.h ${MBE_DIR}/climateput.cpp

    ${MBE_DIR}/dayinitializer.h ${MBE_DIR}/dayinitializer.cpp
    ${MBE_DIR}/subdayinitializer.h ${MBE_DIR}/subdayinitializer.cpp
    ${MBE_DIR}/subdayfinalizer.h ${MBE_DIR}/subdayfinalizer.cpp )

if( FEATURES_ONLINECONTROL_ldndc )
    set( MBE_SOURCES ${MBE_SOURCES}
        ${MBE_DIR}/state/mbe_staterequests.h ${MBE_DIR}/state/mbe_staterequests.cpp )
endif( FEATURES_ONLINECONTROL_ldndc )

set( MBE_INCLUDE_DIRECTORIES
    ${CMAKE_CURRENT_SOURCE_DIR} ${MBE_DIR}

    ${CMAKE_GENERATE_DIR}/legacy )

## generate state containers
include( ${MBE_DIR}/MoBiLE-state.cmake )

