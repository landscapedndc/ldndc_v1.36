/*!
 * @brief
 *    see header
 *
 * @author
 *    steffen klatt
 *    edwin haas
 */

#include  "climateput.h"
#include  "state/mbe_state.h"

#include  <time/cbm_time.h>
#include  <input/climate/climate.h>
#include  <cfgfile/cbm_cfgfile.h>
#include  <constants/cbm_const.h>

LMOD_MODULE_INFO(ClimateDayBufferUpdate,TMODE_SUBDAILY,LMOD_FLAG_SYSTEM|LMOD_FLAG_MAINTENANCE|LMOD_FLAG_INITIALIZER);
/* configuration options for this module */
#define  CLBUF_CONF_OPT_ALLOW_SYNTH    "allow_synthesize"    /* bool */


ldndc::ClimateDayBufferUpdate::ClimateDayBufferUpdate(
        MoBiLE_State *  _state,
        cbm::io_kcomm_t *  _iokcomm,
        timemode_e  _timemode)
        : MBE_LegacyModel( _state, _timemode),

          cl_( _iokcomm->get_input_class< climate::input_class_climate_t >()),
          mc_( *_state->get_substate< substate_microclimate_t >())
#ifdef  _HAVE_INPUTS_CLIENT_SIDE_SYNTHESIZE_OK
          , cl_time_(( cl_) ? cl_->get_time() : ltime_t()),
          allow_synthesize_( false),
          rec_s_( NULL), rec_d_( NULL)
#endif
{
}

ldndc::ClimateDayBufferUpdate::~ClimateDayBufferUpdate()
{
    // no op
}

lerr_t
ldndc::ClimateDayBufferUpdate::configure(
        ldndc::config_file_t const *  _cf)
{
#ifdef  _HAVE_INPUTS_CLIENT_SIDE_SYNTHESIZE_OK
    this->rec_d_ = this->rec_d_buf_;
    this->rec_s_ = ( this->lclock()->time_resolution() == 1) ? NULL : this->rec_s_buf_;

    CF_LMOD_QUERY(_cf,CLBUF_CONF_OPT_ALLOW_SYNTH,allow_synthesize_,false);
    if ( this->allow_synthesize_)
    {
        if (( this->lclock()->time_resolution() == 1) || 
            ( this->cl_time_.time_resolution() == 1) || 
            ( this->lclock()->time_resolution() == this->cl_time_.time_resolution()))
        {
            ltime_t  this_time = this->lclock()->fromto();
            /* NOTE the output resolution depends on the climate interface implementation:
             *
             *    since data records with higher resolution are aggregated to simulation resolution
             *    input resolution for the synthesizer is in general equal or smaller than data
             *    resolution
             */
            ltime_t  data_time;
                   if ( this->cl_)
            {
                data_time = ltime_t( this->cl_time_, std::min( this_time.time_resolution(), this->cl_time_.time_resolution()));
            }
            else
            {
                data_time = this_time;
            }

            this->cl_synth_ = ldndc::data_synthesizer::climate::lsynth_climate_t(( this->cl_) ? this->cl_->station_info() : NULL, &data_time, &this_time);
            KLOGVERBOSE( "kernel ",this->object_id(), " runs in synthesize mode  [class=",INPUT_NAMES[INPUT_CLIMATE],"]");
        }
        else
        {
            KLOGERROR( "climate data synthesizer incapable of handling different time resolutions greater 1  [res(data)=",this->cl_time_.time_resolution(),",res(sim)=",this->lclock()->time_resolution(),"]");
            return  LDNDC_ERR_FAIL;
        }

        if ( this->rec_s_)
        {
            CLBUF_INVALIDATE_BUFFER( this->rec_s_);
        }
        CLBUF_INVALIDATE_BUFFER( this->rec_d_);
    }
    else
    {
        /* no op */
    }
#else
    LDNDC_FIX_UNUSED(_cf);
#endif
    return  LDNDC_ERR_OK;
}

lerr_t
ldndc::ClimateDayBufferUpdate::initialize()
{
    return  LDNDC_ERR_OK;
}

lerr_t
ldndc::ClimateDayBufferUpdate::solve()
{
#ifndef  _HAVE_INPUTS_CLIENT_SIDE_SYNTHESIZE_OK
    if ( cl_)
#endif
    {
        return  copy_climate_record_to_buffers_();
    }

    return  LDNDC_ERR_OK;
}


#ifndef  _HAVE_INPUTS_CLIENT_SIDE_SYNTHESIZE_OK
lerr_t
ldndc::ClimateDayBufferUpdate::copy_climate_record_to_buffers_()
{

    cbm::sclock_t const &  clk( this->lclock_ref());

    mc_.longwaveradiation_in = cl_->long_rad_subday( clk);
    mc_.ts_precipitation = cl_->precip_subday( clk)*cbm::M_IN_MM;
    mc_.ts_airpressure = cl_->air_pressure_subday( clk);
    mc_.shortwaveradiation_in = cl_->ex_rad_subday( clk);
    mc_.ts_minimumairtemperature = cl_->temp_min_subday( clk);
    mc_.ts_airtemperature = cl_->temp_avg_subday( clk);
    mc_.ts_maximumairtemperature = cl_->temp_max_subday( clk);
    mc_.ts_relativehumidity = cl_->rel_humidity_subday( clk);
    mc_.ts_watervaporsaturationdeficit = cl_->vpd_subday( clk);
    mc_.ts_windspeed = cl_->wind_speed_subday( clk);

    if ( clk.is_position( TMODE_PRE_DAILY))
    {
        mc_.nd_longwaveradiation_in = cl_->long_rad_day( clk);
        mc_.nd_precipitation = cl_->precip_day( clk)*cbm::M_IN_MM;
        mc_.nd_airpressure = cl_->air_pressure_day( clk);
        mc_.nd_shortwaveradiation_in = cl_->ex_rad_day( clk);
        mc_.nd_minimumairtemperature = cl_->temp_min_day( clk);
        mc_.nd_airtemperature = cl_->temp_avg_day( clk);
        mc_.nd_maximumairtemperature = cl_->temp_max_day( clk);
        mc_.nd_relativehumidity = cl_->rel_humidity_day( clk);
        mc_.nd_watervaporsaturationdeficit = cl_->vpd_day( clk);
        mc_.nd_windspeed = cl_->wind_speed_day( clk);
    }


    return  LDNDC_ERR_OK;
}

#else

lerr_t
ldndc::ClimateDayBufferUpdate::copy_climate_record_to_buffers_()
{
    cbm::sclock_t const &  clk( this->lclock_ref());

    if ( this->cl_)
    {
        if ( this->lclock()->time_resolution() == 1)
        {
            this->cl_->get_record( this->rec_d_, clk);
        }
        else /* if ( this->lclock()->time_resolution() > 1) */
        {
            if ( this->cl_time_.time_resolution() == 1)
            {
                CLBUF_INVALIDATE_BUFFER( this->rec_s_);
                if ( this->lclock()->subday() == 1)
                {
                    this->cl_->get_record( this->rec_d_, clk);
                }
                else
                {
                    /* no op */
                }
            }
            else /* if ( this->cl_time_.time_resolution() ==  this->lclock()->time_resolution()) */
            {
                this->cl_->get_record( this->rec_s_, clk);

                /* day buffer written at end of simulation day */
            }
        }
    }
    else
    {
        // TODO  not tested
        if ( this->rec_s_)
        {
            CLBUF_INVALIDATE_BUFFER( this->rec_s_);
        }
        if ( this->lclock()->subday() == 1)
        {
            CLBUF_INVALIDATE_BUFFER( this->rec_d_);
        }
    }

    if ( this->allow_synthesize_)
    {
        ldate_t const  clock_now( this->lclock()->now());
        this->cl_synth_.configure( &clock_now);
        lerr_t  rc_synth = this->cl_synth_.synthesize_record( this->rec_s_, this->rec_s_, this->rec_d_, this->rec_d_, &clock_now);
        if ( rc_synth)
        {
            return  rc_synth;
        }
    }

    if ( this->rec_s_)
    {
        this->array_to_record( this->rec_s_, TMODE_SUBDAILY);
    }
    if ( this->rec_d_)
    {
        this->array_to_record( this->rec_d_, TMODE_POST_DAILY);
        if ( !this->rec_s_)
        {
            this->array_to_record( this->rec_d_, TMODE_SUBDAILY);
        }
    }

    return  LDNDC_ERR_OK;
}



void
ldndc::ClimateDayBufferUpdate::array_to_record(
        double const *  _rec,
        timemode_e  _timemode)
{
    ldndc_kassert( _rec);
    if ( _timemode == TMODE_SUBDAILY)
    {
        mc_.longwaveradiation_in = _rec[climate::record::RECORD_ITEM_LONGWAVE_RAD];
        mc_.ts_precipitation = _rec[climate::record::RECORD_ITEM_PRECIP]*cbm::M_IN_MM;
        mc_.ts_airpressure = _rec[climate::record::RECORD_ITEM_AIR_PRESSURE];
        mc_.shortwaveradiation_in = _rec[climate::record::RECORD_ITEM_EX_RAD];
        mc_.ts_minimumairtemperature = _rec[climate::record::RECORD_ITEM_TEMP_MIN];
        mc_.ts_airtemperature = _rec[climate::record::RECORD_ITEM_TEMP_AVG];
        mc_.ts_maximumairtemperature = _rec[climate::record::RECORD_ITEM_TEMP_MAX];
        mc_.ts_relativehumidity = _rec[climate::record::RECORD_ITEM_REL_HUMUDITY];
        mc_.ts_watervaporsaturationdeficit = _rec[climate::record::RECORD_ITEM_VPD];
        mc_.ts_windspeed = _rec[climate::record::RECORD_ITEM_WIND_SPEED];
    }
    else if ( _timemode == TMODE_POST_DAILY)
    {
        mc_.nd_longwaveradiation_in = _rec[climate::record::RECORD_ITEM_LONGWAVE_RAD];
        mc_.nd_precipitation = _rec[climate::record::RECORD_ITEM_PRECIP]*cbm::M_IN_MM;
        mc_.nd_airpressure= _rec[climate::record::RECORD_ITEM_AIR_PRESSURE];
        mc_.nd_shortwaveradiation_in = _rec[climate::record::RECORD_ITEM_EX_RAD];
        mc_.nd_minimumairtemperature = _rec[climate::record::RECORD_ITEM_TEMP_MIN];
        mc_.nd_airtemperature = _rec[climate::record::RECORD_ITEM_TEMP_AVG];
        mc_.nd_maximumairtemperature = _rec[climate::record::RECORD_ITEM_TEMP_MAX];
        mc_.nd_relativehumidity = _rec[climate::record::RECORD_ITEM_REL_HUMUDITY];
        mc_.nd_watervaporsaturationdeficit = _rec[climate::record::RECORD_ITEM_VPD];
        mc_.nd_windspeed = _rec[climate::record::RECORD_ITEM_WIND_SPEED];
    }
    else
    {
        KLOGFATAL( "better check again, little missy.");
    }
}

#endif  /*  !_HAVE_INPUTS_CLIENT_SIDE_SYNTHESIZE_OK  */

