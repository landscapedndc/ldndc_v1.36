/*!
 * @brief
 *    kind of helper module to perform additional tasks
 *
 * @author
 *    Steffen Klatt,
 *    Edwin Haas,
 *    David Kraus,
 *    Felix Havermann
 */

#include  "subdayinitializer.h"
#include  "state/mbe_state.h"

LMOD_MODULE_INFO(SubdailyInitializer,TMODE_SUBDAILY,LMOD_FLAG_SYSTEM|LMOD_FLAG_MAINTENANCE|LMOD_FLAG_INITIALIZER);


ldndc::SubdailyInitializer::SubdailyInitializer(
        MoBiLE_State *  _state,
        cbm::io_kcomm_t *_io_kcomm,
        timemode_e  _timemode)
        : MBE_LegacyModel( _state, _timemode),
          ph_( _state->get_substate_ref< substate_physiology_t >()),
          mc_( _state->get_substate_ref< substate_microclimate_t >()),
          wc_( _state->get_substate_ref< substate_watercycle_t >()),
          m_veg( &_state->vegetation),
          m_setup( _io_kcomm->get_input_class< input_class_setup_t >()),
          soillayers( _io_kcomm->get_input_class< input_class_soillayers_t >())
{
}

ldndc::SubdailyInitializer::~SubdailyInitializer()
{
    // no op
}

lerr_t
ldndc::SubdailyInitializer::configure(
                                                 ldndc::config_file_t const *)
{
    return  LDNDC_ERR_OK;
}

lerr_t
ldndc::SubdailyInitializer::initialize()
{
    return  LDNDC_ERR_OK;
}

lerr_t
ldndc::SubdailyInitializer::solve()
{
    /*!
     * @todo
     * check if this should be included
     */
//    for (size_t fl = 0; fl < m_setup->canopylayers(); ++fl)
//    {
//        mc_.shortwaveradiation_fl[fl] = 0.0;
//        mc_.temp_fl[fl] = 0.0;
//        mc_.ts_sunlitfoliagefraction_fl[fl] = 0.0;
//        mc_.parshd_fl[fl] = 0.0;
//        mc_.parsun_fl[fl] = 0.0;
//        mc_.vpd_fl[fl] = 0.0;
//        mc_.win_fl[fl] = 0.0;
//    }
    
    /* physiology section */
    for ( PlantIterator vt = this->m_veg->begin(); vt != this->m_veg->end(); ++vt)
    {
        MoBiLE_Plant *p = *vt;

        for (size_t fl = 0; fl < m_setup->canopylayers(); ++fl)
        {
            p->carbonuptake_fl[fl] = 0.0;
            p->tp_fl[fl] = 0.0;
            p->atp_fl[fl] = 0.0;
            p->nadph_fl[fl] = 0.0;
            p->isoprene_emission_fl[fl] = 0.0;
            p->monoterpene_emission_fl[fl] = 0.0;
        }
        
        p->rTra = 0.0;
        p->rGro = 0.0;
        p->rGroBelow = 0.0;
        p->rRes = 0.0;
        p->rFrt = 0.0;
        p->rSap = 0.0;
        p->rSapBelow = 0.0;
        p->rBud = 0.0;
        p->rFol = 0.0;

        p->dcFol = 0.0;
        p->dcFrt = 0.0;
        p->dcSap = 0.0;
        p->dcBud = 0.0;
        p->dcFac = 0.0;

        p->sFol = 0.0;
        p->sBud = 0.0;

        p->nLitFol = 0.0;
        p->nLitBud = 0.0;
        p->n_retention = 0.0;

        p->exsuLoss = 0.0;

        p->nox_uptake = 0.0;
        p->n2_fixation = 0.0;

        p->sWoodAbove = 0.0;
        p->nLitWoodAbove = 0.0;

        for (size_t sl = 0; sl < soillayers->soil_layer_cnt(); ++sl)
        {
            p->sFrt_sl[sl] = 0.0;
            p->nLitFrt_sl[sl] = 0.0;
            p->sWoodBelow_sl[sl] = 0.0;
            p->nLitWoodBelow_sl[sl] = 0.0;
        }

        for ( size_t  na = 0;  na < p->nb_ageclasses();  ++na)
        {
            p->sFol_na[na] = 0.0;
        }
    }

    ph_.ts_nh3_uptake_fl = 0.0;
    ph_.ts_nox_uptake_fl = 0.0;

    return  LDNDC_ERR_OK;
}

