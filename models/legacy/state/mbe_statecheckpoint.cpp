/*!
 * @author
 *    steffen klatt
 */

#include  "state/mbe_statecheckpoint.h"
#ifdef  _HAVE_SERIALIZE
#include  "mbe_legacyoutputmodel.h"

#include  <input/ecosystemtypes.h>

#include  <input/checkpoint/checkpointtypes.h>
#include  <kernel/io-kcomm.h>


int
ldndc::state_checkpoint_acquire_sink_handle(
    cbm::io_kcomm_t *  _io_kcomm, ldndc::sink_handle_t *  _sink)
{
    if ( !_sink || !_io_kcomm)
    {
        return  -1;
    }

    *_sink = _io_kcomm->sink_handle_acquire( "checkpoint");
    if ( _sink->status() == LDNDC_ERR_OK)
    {
        sink_entities_t  checkpoint_entities = sink_entities_defaults;
        checkpoint_entities.size = ldndc::checkpoint::SIZE;
        checkpoint_entities.ids = ldndc::checkpoint::IDS;
        checkpoint_entities.names = ldndc::checkpoint::IDS;

        sink_layout_t  checkpoint_layout = sink_layout_defaults;
        checkpoint_layout.rank = ldndc::checkpoint::RANK;
        checkpoint_layout.sizes = ldndc::checkpoint::SIZES;
        checkpoint_layout.types = ldndc::checkpoint::TYPES;
        checkpoint_layout.entsizes = NULL;

        sink_entity_layout_t  s_layout = sink_entity_layout_defaults;
        s_layout.ents = checkpoint_entities;
        s_layout.layout = checkpoint_layout;

        sink_record_meta_t  record_meta;
        record_meta.useregs = RM_CLIENTID|RM_DATETIME;
        lerr_t  rc_layout = _sink->set_fixed_layout( &record_meta, &s_layout);
        if ( rc_layout)
        {
            return  -2;
        }
        return  0;
    }
    return  -3;
}

int
ldndc::state_checkpoint_release_sink_handle(
    cbm::io_kcomm_t *  _io_kcomm, ldndc::sink_handle_t *  _sink)
{
    if ( !_io_kcomm && _sink)
    {
        return  -1;
    }
    if ( _io_kcomm && _sink)
    {
        _io_kcomm->sink_handle_release( _sink);
    }
    return  0;
}

#include  "utils/lutils-serialize.h"
int
ldndc::state_checkpoint_write_member(
        char const *  _member_class, char const * _member_id, void const *  _data,
        int *  _extents, int _rank, atomic_datatype_t  _datatype,
    substate_checkpoint_write_context_t *  _context)
{
    /* FORMAT:  __class__  __entity__  value */
    if ( !_data)
    {
        /* it's ok when there is no data */
        return  0;
    }


    char *  member_data = NULL;
    size_t  member_data_size = 0;
    lerr_t const  rc_pack = cbm::pack64( &member_data, &member_data_size,
            _data, _rank, _extents, _datatype);
    if ( !member_data)
    {
        LOGERROR( "failed to pack data member \"", _member_class, "/", _member_id, "\"");
        return  -1;
    }
    if ( rc_pack)
    {
        LOGERROR( "failed to pack data member \"", _member_class, "/", _member_id, "\"");
        cbm::free_pack64_buffer( member_data);
        return  -1;
    }

    char const *  checkpoint_data[3] = { _member_class, _member_id, member_data};
    void *  data[] = { checkpoint_data};
    _context->record.data = data;

    lerr_t  rc_write = _context->sink.write_fixed_record(
            &_context->client, &_context->record);
    cbm::free_pack64_buffer( member_data);
    if ( rc_write)
    {
        return  -1;
    }

    return  0;
}
int
ldndc::state_checkpoint_read_member(
        char const *  _member_class, char const * _member_id, void *  _data,
        int *  _extents, int _rank, atomic_datatype_t  _datatype,
    substate_checkpoint_read_context_t *  _context)
{
    /* FORMAT:  __class__  __entity__  value */
    if ( !_data)
    {
        /* it's ok when there is no data */
        return  0;
    }

    ldndc::checkpoint::checkpoint_buffer_t  cp_buf( _member_class, _member_id);
    cp_buf.timestamp = _context->timestamp;

    lerr_t  rc_read = _context->source->retrieve_record( &cp_buf);
    if ( rc_read)
    {
        return  -1;
    }

    lerr_t const  rc_unpack = cbm::unpack64( cp_buf.buffer, cp_buf.n_buffer,
            _data, _rank, _extents, _datatype);
    if ( rc_unpack)
    {
        LOGERROR( "failed to unpack data member \"", _member_class, "/", _member_id, "\"");
        return  -1;
    }

    return  0;
}

#endif /* _HAVE_SERIALIZE */

