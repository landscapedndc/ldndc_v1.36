/*!
 * @brief
 *    name array definition
 *
 * @author 
 *    steffen klatt (created on: dec 05, 2013),
 *    edwin haas
 */

#include  "state/mbe_substatetypes.h"

char const *  SUBSTATE_NAMES[SUBSTATE_CNT+2] = { "airchemistry", "microclimate", "physiology", "soilchemistry", "surfacebulk", "watercycle", "CNT", "none"};
char const *  SUBSTATE_NAMES_LONG[SUBSTATE_CNT+2] = { "air chemistry", "microclimate", "physiology", "soil chemistry", "surface bulk", "water cycle", "<#SUB>", "none"};

