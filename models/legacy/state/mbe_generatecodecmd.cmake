# vim: ft=cmake

message( STATUS "need to regenerate parts of the \"${_substate}\" state container" )

cmake_policy( SET CMP0007 NEW )

include( ${legacy_source_dir}/state/settings.cmake )

## check if parameter list file exists
if( NOT EXISTS ${legacy_source_dir}/state/mbe_state.txt )
	message( FATAL_ERROR "state item list file missing  [file=${legacy_source_dir}/state/mbe_state.txt]" )
endif()

## read state description file
file( STRINGS ${legacy_source_dir}/state/mbe_state.txt state_item_prop_list REGEX "^\ *${_substate}" )
## number of state items read
list( LENGTH state_item_prop_list state_item_prop_list_size )
#message( STATUS "${state_item_prop_list}")
message( STATUS "seeing ${state_item_prop_list_size} \"${_substate}\" state items" )

## generate allocation, initialization, resize and serialization code
include( ${legacy_source_dir}/state/mbe_generatecode.cmake )

