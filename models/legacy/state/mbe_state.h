/*!
 * @brief
 *    container for MoBiLE substates
 *
 * @author
 *    steffen klatt,
 *    edwin haas
 */

#ifndef  MoBiLE_STATE_H_
#define  MoBiLE_STATE_H_

#include  "ld_legacy.h"
#include  "state/mbe_substates.h"

namespace ldndc {

class MoBiLE_Substate;
class LDNDC_API MoBiLE_State : public cbm::object_t
{
    MOBILE_OBJECT(MoBiLE_State)
    public:
        MoBiLE_State();
        ~MoBiLE_State();

        lerr_t  initialize( cbm::io_kcomm_t *);

    public:
        double  get_fh2o( ldndc::MoBiLE_Plant const * /* plant species */,
                          double = 1.0 /* area fraction */,
                          bool = false /* use van genuchten */,
                          bool = false /* root weighted */) const;

        double  get_fh2o_sl( ldndc::MoBiLE_Plant const * /* plant species */,
                             substate_watercycle_t const * /* watercycle state */,
                             substate_soilchemistry_t const * /* soilchemistry state */,
                             size_t /* soil layer */,
                             double const & /* water content at wilting point */,
                             double = 1.0 /* area fraction */) const;

    public:
        MoBiLE_PlantVegetation  vegetation;

    public:
        /*!
         * @brief
         *    retrieve pointer to substate of type _S
         */
        template < class _S >
        _S * get_substate()
            { return  static_cast< _S * >( substates_[_S::substate_type]); }
        /*!
         * @brief
         *    retrieve reference to substate of type _S
         */
        template < class _S >
        _S & get_substate_ref()
            { return  *get_substate< _S >(); }

        /*!
         * @brief
         *    retrieve read-only pointer to substate of type _S
         */
        template < class _S >
        _S const * get_substate() const
            { return  const_cast< MoBiLE_State * >( this)->get_substate< _S >(); }
        /*!
         * @brief
         *    retrieve read-only reference to substate of type _S
         */
        template < class _S >
        _S const & get_substate_ref() const
            { return  *const_cast< MoBiLE_State * >( this)->get_substate< _S >(); }

        MoBiLE_Substate *  get_substate( substate_type_e);
        MoBiLE_Substate const *  get_substate( substate_type_e) const;
		/* get substate by name */
        MoBiLE_Substate *  get_substate( char const * /*substate name*/);
        MoBiLE_Substate const *  get_substate( char const * /*substate name*/) const;

        /*!
         * @brief
         *    total number of substates known to
         *    this state object
         */
        size_t  full_substate_cnt() const;
        /*!
         * @brief
         *    number of instantiated substate ojects
         */
        size_t  substate_cnt() const;
        /*!
         * @brief
         *    total number of members of all substates
         *    known to this state object
         */
        size_t  full_substate_member_cnt() const;
        /*!
         * @brief
         *    number of members of all substates that
         *    are currently instantiated
         */
        size_t  substate_member_cnt() const;
        /*!
         * @brief
         *    total number of bytes dynamically allocated
         *    in all substates currently instantiated
         */
        size_t  alloc_size() const;

        /*!
         * @brief
         *    resize entities (e.g. arrays) in substates
         */
        lerr_t  resize_entities( cbm::io_kcomm_t *);

#ifdef  _HAVE_SERIALIZE
    public:
        int  create_checkpoint( cbm::io_kcomm_t *);
        int  restore_checkpoint( cbm::io_kcomm_t *, ldate_t const *);
    private:
        int  substate_checkpoint_write_context_init(
                cbm::io_kcomm_t *, substate_checkpoint_write_context_t *);
        int  substate_checkpoint_write_context_deinit(
                cbm::io_kcomm_t *, substate_checkpoint_write_context_t *);

        int  substate_checkpoint_read_context_init(
                cbm::io_kcomm_t *, ldate_t const *,
                substate_checkpoint_read_context_t *);
        int  substate_checkpoint_read_context_deinit(
                cbm::io_kcomm_t *, substate_checkpoint_read_context_t *);
#endif
#ifdef  _LDNDC_HAVE_ONLINECONTROL
	public:
        virtual  int  process_request(
                lreply_t * /*reply*/, lrequest_t const * /*request*/);
	private:
        int  substate_request_putget(
                lreply_t * /*reply*/, lrequest_t const * /*request*/);
#endif

    public:

        cbm::string_t wr_type;

private:
        /* convenience array: holds pointers to substate objects */
        MoBiLE_Substate *  substates_[SUBSTATE_CNT];

        /* hide copy construction and assignment operator */
        MoBiLE_State( MoBiLE_State const &);
        MoBiLE_State &  operator=( MoBiLE_State const &);
};

}

#endif  /*  !MoBiLE_STATE_H_  */

