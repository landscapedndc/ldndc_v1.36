# vim: ft=cmake

set( _HAVE_STATE_GEN_DEBUG OFF )  ## be careful, this eats a lot of memory during compilation!
if( LDNDC_RELEASE )
	set( _HAVE_STATE_GEN_DEBUG OFF )
endif()

## dynamic size prefixes used in state item file (e.g. V => SIZE_DIM_V must be defined macro)
set( SIZE_PREFIX "SIZE_DIM_")

set( e_target "0")
set( e_id "1")
set( e_name "2")
set( e_type "3")
set( e_size "4")
set( e_inival "5")
set( e_unit "6")
set( e_desc "7")


## used to auto-generate substate related code, e.g.
## enum values, name array, state include file.
##
## note:
##      make sure orders match across lists

##      class name prefix
set( __LDNDC_NAME_PREFIX_SUBSTATE_CLASS  "substate_")
#set( STATE_larray_memsize_t "lvector_memsize_t")

## enumeration type name
set( MBE_SUBSTATES_ENUM_TYPE "substate_type_e")
## enumeration identifier is prepended with this value (e.g. SUBSTATE_MICROCLIMATE)
set( MBE_SUBSTATES_ENUM_PREFIX "SUBSTATE_")
set( MBE_SUBSTATES_ENUM_PREFIX_SYS "__ldndc_substate_enum_")
set( MBE_SUBSTATES_ENUM_CNT "CNT")

## substate identifiers (machine readable)
set( MBE_SUBSTATES
    "airchemistry"
    "microclimate"
    "physiology"
    "soilchemistry"
    "surfacebulk"
    "watercycle"
)
## "none" substate type
set( MBE_SUBSTATE_NONE "none")
## substate names (human readable)
set( MBE_SUBSTATES_LONG
    "air chemistry"
    "microclimate"
    "physiology"
    "soil chemistry"
    "surface bulk"
    "water cycle"
)

set( MBE_SUBSTATES_ARRAY "SUBSTATE_NAMES")
set( MBE_SUBSTATES_LONGARRAY "SUBSTATE_NAMES_LONG")

set( MBE_SUBSTATES_FACTORY_CLASS "__LDNDC_SUBSTATE_FACTORY_CLASS")
set( MBE_SUBSTATES_FACTORY_LIST_TYPE "__LDNDC_SUBSTATE_FACTORY_BASE_CLASS")
set( MBE_SUBSTATES_FACTORY_LIST_NAME "__LDNDC_SUBSTATE_FACTORY_LIST")


function( defn_prepend _target _add)
	set( tmp ${${_target}})
	set( ${_target} "${_add}${tmp}" PARENT_SCOPE)
endfunction()

function( defn_append _target _add)
	set( tmp ${${_target}})
	set( ${_target} "${tmp}${_add}" PARENT_SCOPE)
endfunction()

function( s_type2type _type _ltype)
        if( _type STREQUAL "double")
                set( ${_ltype} "LDNDC_FLOAT64" PARENT_SCOPE)
	elseif( _type STREQUAL "int")
                set( ${_ltype} "LDNDC_INT32" PARENT_SCOPE)
	elseif( _type STREQUAL "unsigned")
                set( ${_ltype} "LDNDC_UINT32" PARENT_SCOPE)
	elseif( _type STREQUAL "lflags_t")
                set( ${_ltype} "LDNDC_UINT64" PARENT_SCOPE)
	else()
		message( FATAL_ERROR "type not mappable  [type=${_type}]")
	endif()
endfunction( s_type2type)

