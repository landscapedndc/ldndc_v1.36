/*!
 * @brief
 *    substate base class implementation, sources factories
 *
 * @author
 *    edwin haas,
 *    steffen klatt (created on: dec 05, 2013)
 */


#include  "state/mbe_substates.h"

ldndc::substate_factory_t< substate_airchemistry_t > const  ldndc::substate_airchemistry_factory;
ldndc::substate_factory_t< substate_microclimate_t > const  ldndc::substate_microclimate_factory;
ldndc::substate_factory_t< substate_physiology_t > const  ldndc::substate_physiology_factory;
ldndc::substate_factory_t< substate_soilchemistry_t > const  ldndc::substate_soilchemistry_factory;
ldndc::substate_factory_t< substate_surfacebulk_t > const  ldndc::substate_surfacebulk_factory;
ldndc::substate_factory_t< substate_watercycle_t > const  ldndc::substate_watercycle_factory;

ldndc::substate_factory_base_t const *  ldndc::substate_factory[SUBSTATE_CNT] =
{
    &ldndc::substate_airchemistry_factory,
    &ldndc::substate_microclimate_factory,
    &ldndc::substate_physiology_factory,
    &ldndc::substate_soilchemistry_factory,
    &ldndc::substate_surfacebulk_factory,
    &ldndc::substate_watercycle_factory
};

