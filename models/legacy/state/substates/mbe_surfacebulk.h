/*!
 * @file
 *
 * @author
 *    david kraus (created on: mar 17, 2014),
 *    edwin haas
 */

#ifndef  LDNDC_SUBSTATE_SURFACEBULK_H_
#define  LDNDC_SUBSTATE_SURFACEBULK_H_

#include  "state/mbe_substate.h"
#include  "substate/mbe_surfacebulk.h.inc"

#define  SURFACEBULK_LAYER_MAX  5

namespace ldndc {
class LDNDC_API substate_surfacebulk_t : public MoBiLE_Substate
{
    MOBILE_SUBSTATE_OBJECT(surfacebulk,LSUB_FLAG_NONE);
    public:
        /* holds surface bulk state items */
        LDNDC_surfacebulk_SUBSTATE_ITEMS

    public:
        static size_t  surfacebulk_layer_max()
        {
            return  SURFACEBULK_LAYER_MAX;
        }
        size_t  surfacebulk_layer_cnt()
        const
        {
            return  SURFACEBULK_LAYER_MAX;
        }

    public:
#ifdef  _HAVE_SERIALIZE
        /* interface declaration for dump/restore mechanism */
        int  create_checkpoint( substate_checkpoint_write_context_t *);
        int  restore_checkpoint( substate_checkpoint_read_context_t *);
#endif  /* _HAVE_SERIALIZE */
#ifdef  _LDNDC_HAVE_ONLINECONTROL
    public:
        int  process_request(
                lreply_t * /*reply*/, lrequest_t const * /*request*/);
#endif  /* _LDNDC_HAVE_ONLINECONTROL */
};
}


#endif  /*  !LDNDC_SUBSTATE_SURFACEBULK_H_  */

