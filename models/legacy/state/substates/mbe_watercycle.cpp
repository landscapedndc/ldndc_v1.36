/*!
 * @brief
 *    ...
 *
 * @author
 *    Steffen Klatt,
 *    Edwin Haas,
 *    David Kraus
 */


#include  "state/substates/mbe_common.h"
#include  "state/substates/mbe_watercycle.h"
#include  <scientific/soil/ld_soil.h>

#include  <input/soillayers/soillayers.h>
#include  <input/species/species.h>

#include  <constants/cbm_const.h>

#include  <logging/cbm_logging.h>


MOBILE_SUBSTATE_OBJECT_DEFN(watercycle)
ldndc::substate_watercycle_t::substate_watercycle_t()
        : MoBiLE_Substate()
{
    g_construct_();
}
ldndc::substate_watercycle_t::substate_watercycle_t(
        lid_t const &  _id, MoBiLE_PlantVegetation *)
        : MoBiLE_Substate( _id)
{
    g_construct_();
}


ldndc::substate_watercycle_t::~substate_watercycle_t()
{
    /* by definition no deallocations here ! */
}

#define  S_SITE  _io_comm->get_input_class< site::input_class_site_t >()
#define  S_SOILPARAMETERS  _io_comm->get_input_class< soilparameters::input_class_soilparameters_t >()
#define  S_SETUP  _io_comm->get_input_class< setup::input_class_setup_t >()
#define  S_SOILLAYERS  _io_comm->get_input_class< soillayers::input_class_soillayers_t >()

lerr_t
ldndc::substate_watercycle_t::initialize(
        cbm::io_kcomm_t *  _io_comm,
        void *  /*_mem_offs*/)
{
    if ( is_initialized())
    {
        return  LDNDC_ERR_OK;
    }

    if ( !set_required_inputs_( _io_comm))
    {
        return  LDNDC_ERR_OBJECT_INIT_FAILED;
    }

    lerr_t  rc = resize_entities( _io_comm);
    if ( rc != LDNDC_ERR_OK)
    {
        return  rc;
    }

    site::input_class_site_t const *  s_site = S_SITE;
    if ( !s_site)
    {
        LOGERROR( "missing input  [class=site]");
        return LDNDC_ERR_FAIL;
    }
    
    soillayers::input_class_soillayers_t const *  s_soillayers = S_SOILLAYERS;
    if ( !s_soillayers)
    {
        LOGERROR( "missing input  [class=soillayers]");
        return LDNDC_ERR_FAIL;
    }
    soilparameters::input_class_soilparameters_t const *  s_soilparameters = S_SOILPARAMETERS;
    if ( !s_soilparameters)
    {
        LOGERROR( "missing input  [class=soilparameters]");
        return LDNDC_ERR_FAIL;
    }
    
    size_t  sl( 0);
    double depth( 0.0);
    double depth_old( invalid_dbl);
    double cn_old( invalid_dbl);
    for ( size_t  s = 0;  s < S_SOILLAYERS->strata_cnt();  ++s)
    {
        site::iclass_site_stratum_t  stratum;
        S_SOILLAYERS->stratum( s, &stratum);
        
        // water content initialized in the middle between wilting point and field capacity
        for ( size_t  l = 0;  l < (size_t)stratum.split;  ++l)
        {
            /* SOILLAYER DEPTH */
            if ( !cbm::flt_greater_zero( stratum.stratum_height) || (stratum.split <= 0))
            {
                LOGERROR("Stratum discretization invalid! Stratum height: ", stratum.stratum_height,", stratum split: ", stratum.split);
                return  LDNDC_ERR_FAIL;
            }
            depth += (stratum.stratum_height / stratum.split) * cbm::M_IN_MM;


            /* CLAY AND SAND CONTENT */
            double clay( 0.0);
            double sand( 0.0);
            lerr_t rc_texture = assign_soillayer_clay_and_sand_content( clay, sand, s, depth, s_site, s_soillayers, s_soilparameters, false);
            if ( rc_texture)
            {
                LOGERROR( "Initialization of watercycle substate not successfull");
                return  LDNDC_ERR_FAIL;
            }


            /* STONE FRACTION */
            double stone_fraction( 0.0);
            lerr_t rc_stone = assign_soillayer_stone_fraction( stone_fraction, s, depth, s_site, s_soillayers, s_soilparameters, false);
            if ( rc_stone)
            {
                LOGERROR( "Initialization of watercycle substate not successfull");
                return  LDNDC_ERR_FAIL;
            }


            /* BULK DENSITY */
            double bulk_density_without_stones( 0.0);
            lerr_t rc_bd = assign_soillayer_bulk_density( bulk_density_without_stones, s, depth, s_site, s_soillayers, s_soilparameters, false);
            if ( rc_bd)
            {
                LOGERROR( "Initialization of watercycle substate not successfull");
                return  LDNDC_ERR_FAIL;
            }


            /* ORGANIC CARBON AND NITROGEN CONTENT */
            double c_org_without_stones( 0.0);
            double n_without_stones( 0.0);
            lerr_t rc_cn = assign_soillayer_organic_carbon_and_total_nitrogen_content( cn_old, c_org_without_stones, n_without_stones,
                                                                                       s, depth, depth_old, s_site, s_soillayers, s_soilparameters, false);
            if ( rc_cn)
            {
                LOGERROR( "Initialization of watercycle substate not successfull");
                return  LDNDC_ERR_FAIL;
            }


            /* POROSITY */
            double porosity_without_stones( 0.0);
            lerr_t rc_poro = assign_soillayer_porosity( porosity_without_stones, s, depth, c_org_without_stones, bulk_density_without_stones,
                                                       s_site, s_soillayers, s_soilparameters, false);
            if ( rc_poro)
            {
                LOGERROR( "Initialization of watercycle substate not successfull");
                return  LDNDC_ERR_FAIL;
            };


            /* WATER CONTENT */
            double wc_without_stones( 0.0);
            if ( cbm::is_valid( stratum.wc_init))
            {
                wc_without_stones = cbm::bound( 0.0, stratum.wc_init, porosity_without_stones);
            }
            else
            {
                double porosity_with_stones = porosity_without_stones * (1.0 - stone_fraction);
                double wfps_max( 0.0);
                double wfps_min( 0.0);
                lerr_t rc_wfps = assign_soillayer_wfps_max_and_min( wfps_max, wfps_min, porosity_with_stones,
                                                                    s, depth, c_org_without_stones, bulk_density_without_stones,
                                                                   s_site, s_soillayers, s_soilparameters, false);
                if ( rc_wfps)
                {
                    LOGERROR( "Initialization of watercycle substate not successfull");
                    return  LDNDC_ERR_FAIL;
                }
                
                double wc_max_without_stones( 0.0);
                double wc_min_without_stones( 0.0);
                ldndc::assign_soillayer_field_capacity_and_wilting_point(
                                                         wc_max_without_stones, wc_min_without_stones,
                                                         wfps_max, wfps_min, porosity_without_stones,
                                                         s, depth, s_site, s_soillayers, s_soilparameters,
                                                         cbm::is_equal( wr_type.c_str(), "vangenuchten"));
                
                wc_without_stones = cbm::bound( 0.0,
                                                wc_min_without_stones + 0.75 * ( wc_max_without_stones - wc_min_without_stones),
                                                porosity_without_stones);
                if ( cbm::flt_greater( depth, 4.0))
                {
                    wc_without_stones += (porosity_without_stones - wc_without_stones) * (1.0 - 4.0 / depth);
                }
            }

            wc_sl[sl] = cbm::bound( 0.0, wc_without_stones * (1.0 - stone_fraction), porosity_without_stones * (1.0 - stone_fraction));

            depth_old = depth;
            cn_old = c_org_without_stones / n_without_stones;
            ++sl;
        }
    }

    set_initialized();
    return  LDNDC_ERR_OK;
}


double
ldndc::substate_watercycle_t::snow_height() const
{
    return this->surface_ice * cbm::DWAT / cbm::DSNO;
}


bool
ldndc::substate_watercycle_t::has_required_inputs_(
        cbm::io_kcomm_t *  _io_comm)
const
{
    if ( !S_SOILLAYERS)
    {
        LOGERROR( SUBSTATE_NAMES[substate_type]," substate requires input classes {",
                LDNDC_INAME(soillayers),
                "}");
        return  false;
    }
    return  true;
}


bool
ldndc::substate_watercycle_t::set_required_inputs_(
        cbm::io_kcomm_t *  _io_comm)
{
    return  has_required_inputs_( _io_comm);
}


#define  SIZE_DIM_F  (uintptr_t)(S_SETUP->canopylayers())
#define  SIZE_DIM_S  (uintptr_t)(S_SOILLAYERS->soil_layer_cnt())

lerr_t
ldndc::substate_watercycle_t::resize_entities(
         cbm::io_kcomm_t *  _io_comm)
{
    return  g_resize_( _io_comm);
}

#include  "substate/mbe_watercycle.cpp.inc"

#undef  S_SOILLAYERS
#undef  SIZE_DIM_F
#undef  SIZE_DIM_S

#ifdef  _HAVE_SERIALIZE
int
ldndc::substate_watercycle_t::create_checkpoint(
        substate_checkpoint_write_context_t *  _cp_cxt)
{
    return  g_mobile_substate_write_members_s( this, _cp_cxt);
}
int
ldndc::substate_watercycle_t::restore_checkpoint(
        substate_checkpoint_read_context_t *  _cp_cxt)
{
    return  g_mobile_substate_read_members_s( this, _cp_cxt);
}
#endif /* _HAVE_SERIALIZE */

#ifdef  _LDNDC_HAVE_ONLINECONTROL
#include  "comm/lmessage.h"
int
ldndc::substate_watercycle_t::process_request(
        lreply_t *  _reply, lrequest_t const *  _request)
{
    if ( _request->command == "GET")
    {
        if ( cbm::is_equal( _request->uri.scheme(), "inproc"))
        {
            state_request_inproc_t  inproc_query;
            return  g_mobile_substate_get_member_s(
                &inproc_query, _reply, _request->data.get_string( ":item:"), this);
        }
    }
    return  1;
}
#endif  /* _LDNDC_HAVE_ONLINECONTROL */

