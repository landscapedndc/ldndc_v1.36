/*!
 * @brief
 *    ...
 *
 * @author
 *    Steffen Klatt
 *    Edwin Haas
 */


#include  "state/substates/mbe_microclimate.h"

#include  <input/site/site.h>
#include  <input/soillayers/soillayers.h>

#include  <scientific/meteo/ld_meteo.h>

#include  <time/cbm_time.h>
#include  <cbm_rtcfg.h>

#include  <logging/cbm_logging.h>
#include  <utils/cbm_utils.h>

#include  <constants/cbm_const.h>

MOBILE_SUBSTATE_OBJECT_DEFN(microclimate)
ldndc::substate_microclimate_t::substate_microclimate_t()
        : MoBiLE_Substate()
{
    this->g_construct_();
}
ldndc::substate_microclimate_t::substate_microclimate_t(
        lid_t const &  _id, MoBiLE_PlantVegetation *)
        : MoBiLE_Substate( _id)
{
    this->g_construct_();
}

ldndc::substate_microclimate_t::~substate_microclimate_t()
{
    /* by definition no deallocations here ! */
}


#define  S_CLIMATE  _io_comm->get_input_class< climate::input_class_climate_t >()
#define  S_SETUP  _io_comm->get_input_class< setup::input_class_setup_t >()
#define  S_SOILLAYERS  _io_comm->get_input_class< soillayers::input_class_soillayers_t >()

lerr_t
ldndc::substate_microclimate_t::initialize(
        cbm::io_kcomm_t *  _io_comm,
        void *  /*_mem_offs*/)
{
    if ( this->is_initialized())
    {
        return  LDNDC_ERR_OK;
    }

    if ( !this->set_required_inputs_( _io_comm))
    {
        return  LDNDC_ERR_OBJECT_INIT_FAILED;
    }

    lerr_t  rc = this->resize_entities( _io_comm);
    RETURN_IF_NOT_OK(rc);

    rc = this->initialize_( _io_comm);
    RETURN_IF_NOT_OK(rc);


    this->set_initialized();
        return  LDNDC_ERR_OK;
}


bool
ldndc::substate_microclimate_t::has_required_inputs_(
        cbm::io_kcomm_t *  _io_comm)
const
{
    if ( !S_SOILLAYERS || !S_CLIMATE)
    {
        LOGERROR( SUBSTATE_NAMES[substate_type]," substate requires input classes {",
                LDNDC_INAME(climate), ",",
                LDNDC_INAME(soillayers), ",",
                "}");
        return  false;
    }
    return  true;
}
bool
ldndc::substate_microclimate_t::set_required_inputs_(
        cbm::io_kcomm_t *  _io_comm)
{
    return  this->has_required_inputs_( _io_comm);
}


lerr_t
ldndc::substate_microclimate_t::initialize_(
                                            cbm::io_kcomm_t *  _io_comm)
{
// use below information for day of year specific temperature initialization, but include latitude info!
//    double const temp_surface( S_CLIMATE->annual_temperature_average() -
//                               cos(2.0 * cbm::PI * ((double)LD_RtCfg.clk->yearday_zero() - 10.0)
//                                   / (double)LD_RtCfg.clk->days_in_year()));
    this->albedo = 0.1;
    
    size_t  sl = 0;
    for ( size_t  s = 0;  s < S_SOILLAYERS->strata_cnt();  ++s)
    {
        iclass_site_stratum_t  stratum;
        S_SOILLAYERS->stratum( s, &stratum);
        for ( size_t  l = 0;  l < (size_t)stratum.split;  ++l)
        {
            if ( cbm::is_valid( stratum.temp_init))
            {
                this->temp_sl[sl] = stratum.temp_init;
                this->nd_temp_sl[sl] = stratum.temp_init;
            }
            else
            {
                this->temp_sl[sl] = S_CLIMATE->annual_temperature_average();
                this->nd_temp_sl[sl] = this->temp_sl[sl];
            }
            ++sl;
        }
    }

    this->surface_temp = this->temp_sl[0];
    this->nd_surface_temp = this->nd_temp_sl[0];

    // initialisation of above surface variables and albedo
    this->temp_a = this->temp_sl[0]; // was 0.5 * (this->temp_fl[0] + this->temp_sl[0]);
    this->nd_temp_a = this->temp_a;

    this->rad_a  = 0.0;
    this->nd_rad_a  = S_CLIMATE->ex_rad_day( *LD_RtCfg.clk);

    this->shortwaveradiation_out = 0.0;
    this->nd_shortwaveradiation_out = 0.1 * this->nd_rad_a;

    this->nd_longwaveradiation_out = ldndc::meteo::stefan_boltzmann( this->nd_temp_a);

    return  LDNDC_ERR_OK;
}

#define  SIZE_DIM_F  (size_t)(S_SETUP->canopylayers())
#define  SIZE_DIM_S  (size_t)(S_SOILLAYERS->soil_layer_cnt())

lerr_t
ldndc::substate_microclimate_t::resize_entities(
         cbm::io_kcomm_t *  _io_comm)
{
    return  this->g_resize_( _io_comm);
}

#include  "substate/mbe_microclimate.cpp.inc"

#undef  S_CLIMATE
#undef  S_SETUP
#undef  S_SOILLAYERS
#undef  SIZE_DIM_F
#undef  SIZE_DIM_S

#ifdef  _HAVE_SERIALIZE
int
ldndc::substate_microclimate_t::create_checkpoint(
        substate_checkpoint_write_context_t *  _cp_cxt)
{
    return  g_mobile_substate_write_members_s( this, _cp_cxt);
}
int
ldndc::substate_microclimate_t::restore_checkpoint(
        substate_checkpoint_read_context_t *  _cp_cxt)
{
    return  g_mobile_substate_read_members_s( this, _cp_cxt);
}
#endif /* _HAVE_SERIALIZE */

#ifdef  _LDNDC_HAVE_ONLINECONTROL
#include  "comm/lmessage.h"
int
ldndc::substate_microclimate_t::process_request(
        lreply_t *  _reply, lrequest_t const *  _request)
{
    if ( _request->command == "GET")
    {
        if ( cbm::is_equal( _request->uri.scheme(), "inproc"))
        {
            state_request_inproc_t  inproc_query;
            return  g_mobile_substate_get_member_s(
                &inproc_query, _reply, _request->data.get_string( ":item:"), this);
        }
    }
    return  1;
}
#endif  /* _LDNDC_HAVE_ONLINECONTROL */

