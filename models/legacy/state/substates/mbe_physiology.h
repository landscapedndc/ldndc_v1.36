/*!
 * @file
 *    represents the plant physiological variables
 *    in the model
 *
 * @author
 *    Edwin Haas,
 *    David Kraus,
 *    Steffen Klatt
 */

#ifndef  LDNDC_SUBSTATE_PHYSIOLOGY_H_
#define  LDNDC_SUBSTATE_PHYSIOLOGY_H_

#include  <constants/lconstants-plant.h>

#include  "state/mbe_substate.h"
#include  "substate/mbe_physiology.h.inc"
#include  "mbe_plant.h"

namespace ldndc {

class LDNDC_API substate_physiology_t : public MoBiLE_Substate
{
    MOBILE_SUBSTATE_OBJECT(physiology,LSUB_FLAG_NONE);
    public:
        /* holds physiology state items */
        LDNDC_physiology_SUBSTATE_ITEMS

    public:
#ifdef  _HAVE_SERIALIZE
        /* interface declaration for dump/restore mechanism */
        int  create_checkpoint( substate_checkpoint_write_context_t *);
        int  restore_checkpoint( substate_checkpoint_read_context_t *);
#endif  /* _HAVE_SERIALIZE */
#ifdef  _LDNDC_HAVE_ONLINECONTROL
    public:
        int  process_request(
                             lreply_t * /*reply*/,
                             lrequest_t const * /*request*/);
#endif  /* _LDNDC_HAVE_ONLINECONTROL */

    public:

        /* returns total vegetation root biomass per soil layer */
        double  mfrt_sl( MoBiLE_PlantVegetation * _veg, size_t _sl)
        const
        {
            double mfrt( 0.0);
            for ( PlantIterator vt = _veg->begin(); vt != _veg->end(); ++vt)
            {
                mfrt += (*vt)->mFrt * (*vt)->fFrt_sl[_sl];
            }
            return mfrt;
        }

        /* returns total vegetation root length per soil layer */
        double  rootlength_sl( MoBiLE_PlantVegetation * _veg, size_t _sl)
        const
        {
            double rootlength( 0.0);
            for ( PlantIterator vt = _veg->begin(); vt != _veg->end(); ++vt)
            {
                rootlength += (*vt)->rootlength_sl[_sl];
            }
            return rootlength;
        }

        /* vegetation carbon uptake */
        double  carbonuptake( MoBiLE_PlantVegetation * _veg, size_t _fl_cnt)
        const
        {
            double carbonuptake_sum( 0.0);
            for ( PlantIterator vt = _veg->begin(); vt != _veg->end(); ++vt)
            {
                carbonuptake_sum += cbm::sum( (*vt)->carbonuptake_fl, _fl_cnt);
            }
            return carbonuptake_sum;
        }

        lerr_t update_canopy_layers_height( MoBiLE_PlantVegetation *);
};
}


#endif  /*  !LDNDC_SUBSTATE_PHYSIOLOGY_H_  */

