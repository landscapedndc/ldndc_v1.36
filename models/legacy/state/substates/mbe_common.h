/*!
 * @file
 *
 * @author
 *    david kraus
 */

#ifndef  LDNDC_SUBSTATE_COMMON_H_
#define  LDNDC_SUBSTATE_COMMON_H_

#include  <input/soillayers/soillayers.h>
#include  <input/soilparameters/soilparameters.h>
#include  <input/site/site.h>
#include  "state/mbe_state.h"



namespace ldndc {
lerr_t
assign_soillayer_clay_and_sand_content(
                                       double &_clay,
                                       double &_sand,
                                       int const &_nd_stratum,
                                       double const &_depth,
                                       const ldndc::site::input_class_site_t *_site,
                                       const ldndc::soillayers::input_class_soillayers_t *_soillayers,
                                       const ldndc::soilparameters::input_class_soilparameters_t *_soilparameters,
                                       bool _info=false);

lerr_t
assign_soillayer_stone_fraction(
                                double &_stone,
                                int const &_nd_stratum,
                                double const &_depth,
                                const ldndc::site::input_class_site_t *_site,
                                const ldndc::soillayers::input_class_soillayers_t *_soillayers,
                                const ldndc::soilparameters::input_class_soilparameters_t *_soilparameters,
                                bool _info=false);

lerr_t
assign_soillayer_bulk_density(
                              double &_bulk_density,
                              int const &_nd_stratum,
                              double const &_depth,
                              const ldndc::site::input_class_site_t *_site,
                              const ldndc::soillayers::input_class_soillayers_t *_soillayers,
                              const ldndc::soilparameters::input_class_soilparameters_t *_soilparameters,
                              bool _info=false);

lerr_t
assign_soillayer_organic_carbon_and_total_nitrogen_content(
                                                           double _cn_old,
                                                           double &_c_org_without_stones,
                                                           double &_n_without_stones,
                                                           int const &_nd_stratum,
                                                           double _depth,
                                                           double _depth_old,
                                                           const ldndc::site::input_class_site_t *_site,
                                                           const ldndc::soillayers::input_class_soillayers_t *_soillayers,
                                                           const ldndc::soilparameters::input_class_soilparameters_t *_soilparameters,
                                                           bool _info=false);

lerr_t
assign_soillayer_porosity(
                          double &_porosity_without_stones,
                          int const &_nd_stratum,
                          double const &_depth,
                          double const &_c_org_without_stones,
                          double const &_bulk_density_without_stones,
                          const ldndc::site::input_class_site_t *_site,
                          const ldndc::soillayers::input_class_soillayers_t *_soillayers,
                          const ldndc::soilparameters::input_class_soilparameters_t *_soilparameters,
                          bool _info=false);

lerr_t
assign_soillayer_van_genuchten_parameter(
                                 double &_vg_a,
                                 double &_vg_n,
                                 double &_vg_m,
                                 bool &_vg_parameter_from_input,
                                 int const &_nd_stratum,
                                 double const &_depth,
                                 const ldndc::site::input_class_site_t *_site,
                                 const ldndc::soillayers::input_class_soillayers_t *_soillayers,
                                 const ldndc::soilparameters::input_class_soilparameters_t *_soilparameters,
                                 bool _info=false);

lerr_t
assign_soillayer_wfps_max_and_min(
                          double &_wfps_max,
                          double &_wfps_min,
                          double &_porosity_with_stones,
                          int const &_nd_stratum,
                          double const &_depth,
                          double const &_c_org_without_stones,
                          double const &_bulk_density_without_stones,
                          const ldndc::site::input_class_site_t *_site,
                          const ldndc::soillayers::input_class_soillayers_t *_soillayers,
                          const ldndc::soilparameters::input_class_soilparameters_t *_soilparameters,
                          bool _info=false);

lerr_t
assign_soillayer_field_capacity_and_wilting_point(
                                          double &_wcmax_without_stones,
                                          double &_wcmin_without_stones,
                                          double _wfps_max,
                                          double _wfps_min,
                                          double _porosity_without_stones,
                                          int const &_nd_stratum,
                                          double const &_depth,
                                          const ldndc::site::input_class_site_t *_site,
                                          const ldndc::soillayers::input_class_soillayers_t *_soillayers,
                                          const ldndc::soilparameters::input_class_soilparameters_t *_soilparameters,
                                          bool _vg_prio=false,
                                          bool _info=false);

}
#endif  /*  !LDNDC_SUBSTATE_COMMON_H_  */
