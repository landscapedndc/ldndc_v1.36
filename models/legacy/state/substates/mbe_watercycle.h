/*!
 * @brief
 *    container for state variable belonging to
 *    water cycle processes
 *
 * @author
 *    steffen klatt,
 *    edwin haas
 */

#ifndef  LDNDC_SUBSTATE_WATERCYCLE_H_
#define  LDNDC_SUBSTATE_WATERCYCLE_H_

#include  "state/mbe_substate.h"
#include  "substate/mbe_watercycle.h.inc"
#include  "mbe_plant.h"

namespace ldndc {

class LDNDC_API substate_watercycle_t : public MoBiLE_Substate
{
    MOBILE_SUBSTATE_OBJECT(watercycle,LSUB_FLAG_NONE);
    public:
        /* holds water cycle state items */
        LDNDC_watercycle_SUBSTATE_ITEMS

    public:
    
        double snow_height() const;
    
#ifdef  _HAVE_SERIALIZE
        /* interface declaration for dump/restore mechanism */
        int  create_checkpoint( substate_checkpoint_write_context_t *);
        int  restore_checkpoint( substate_checkpoint_read_context_t *);
#endif  /* _HAVE_SERIALIZE */
#ifdef  _LDNDC_HAVE_ONLINECONTROL
    public:
        int  process_request(
                lreply_t * /*reply*/, lrequest_t const * /*request*/);
#endif  /* _LDNDC_HAVE_ONLINECONTROL */

};
}

#endif  /*  !LDNDC_SUBSTATE_WATERCYCLE_H_  */

