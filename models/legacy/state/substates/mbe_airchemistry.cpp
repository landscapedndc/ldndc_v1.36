/*!
 * @brief
 *    ...
 *
 * @author
 *      Edwin Haas
 */

#include  "state/substates/mbe_airchemistry.h"
#include  <input/airchemistry/airchemistry.h>
#include  <input/species/species.h>

#include  <logging/cbm_logging.h>

MOBILE_SUBSTATE_OBJECT_DEFN(airchemistry)

ldndc::substate_airchemistry_t::substate_airchemistry_t()
        : MoBiLE_Substate()
{
    this->g_construct_();
}
ldndc::substate_airchemistry_t::substate_airchemistry_t(
        lid_t const &  _id, MoBiLE_PlantVegetation *)
        : MoBiLE_Substate( _id)
{
    this->g_construct_();
}

ldndc::substate_airchemistry_t::~substate_airchemistry_t()
{
    /* by definition no deallocations here ! */
}

#define  S_SETUP  _io_comm->get_input_class< setup::input_class_setup_t >()
#define  S_AIRCHEMISTRY  _io_comm->get_input_class< airchemistry::input_class_airchemistry_t >()

#define  SIZE_DIM_F  (size_t)(S_SETUP->canopylayers())
#define  SIZE_DIM_A  (size_t)(AC_SPECIES)

lerr_t
ldndc::substate_airchemistry_t::initialize(
        cbm::io_kcomm_t *  _io_comm,
        void *  /*_mem_offs*/)
{
    if ( this->is_initialized())
    {
        return  LDNDC_ERR_OK;
    }

    if ( !this->set_required_inputs_( _io_comm))
    {
        return  LDNDC_ERR_OBJECT_INIT_FAILED;
    }

    lerr_t  rc = this->resize_entities( _io_comm);
    if ( rc != LDNDC_ERR_OK)
    {
        return  rc;
    }

    // atmospheric trace gas concentration
    nd_ch4_concentration = 0.0;
    nd_co2_concentration = S_AIRCHEMISTRY->conc_co2_subday( *LD_RtCfg.clk);
    nd_nh3_concentration = 0.0;
    nd_no2_concentration = 0.0;
    nd_no_concentration  = 0.0;
    nd_o2_concentration  = S_AIRCHEMISTRY->conc_o2_subday( *LD_RtCfg.clk);
    nd_o3_concentration  = 0.0;

    for ( size_t  fl = 0;  fl <  SIZE_DIM_F;  fl++)
    {
        ts_ch4_concentration_fl[fl] = nd_ch4_concentration;
        ts_co2_concentration_fl[fl] = nd_co2_concentration;
        ts_nh3_concentration_fl[fl] = nd_nh3_concentration;
        ts_no2_concentration_fl[fl] = nd_no2_concentration;
        ts_no_concentration_fl[fl]  = nd_no_concentration;
        ts_o3_concentration_fl[fl]  = nd_o3_concentration;
    }


    this->set_initialized();
    return  LDNDC_ERR_OK;
}


bool
ldndc::substate_airchemistry_t::has_required_inputs_(
        cbm::io_kcomm_t *  _io_comm)
const
{
    if ( !S_AIRCHEMISTRY)
    {
        LOGERROR( SUBSTATE_NAMES[substate_type],"substate requires input classes {",
                LDNDC_INAME(airchemistry),
                "}");
        return  false;
    }

    return  true;
}
bool
ldndc::substate_airchemistry_t::set_required_inputs_(
        cbm::io_kcomm_t *  _io_comm)
{
    return  this->has_required_inputs_( _io_comm);
}


lerr_t
ldndc::substate_airchemistry_t::resize_entities(
         cbm::io_kcomm_t *  _io_comm)
{
    return  this->g_resize_( _io_comm);
}

#include  "substate/mbe_airchemistry.cpp.inc"

#undef  S_SETUP
#undef  S_AIRCHEMISTRY
#undef  SIZE_DIM_F
#undef  SIZE_DIM_A

#ifdef  _HAVE_SERIALIZE
int
ldndc::substate_airchemistry_t::create_checkpoint(
        substate_checkpoint_write_context_t *  _cp_cxt)
{
    return  g_mobile_substate_write_members_s( this, _cp_cxt);
}
int
ldndc::substate_airchemistry_t::restore_checkpoint(
        substate_checkpoint_read_context_t *  _cp_cxt)
{
    return  g_mobile_substate_read_members_s( this, _cp_cxt);
}
#endif /* _HAVE_SERIALIZE */

#ifdef  _LDNDC_HAVE_ONLINECONTROL
#include  "comm/lmessage.h"
int
ldndc::substate_airchemistry_t::process_request(
        lreply_t *  _reply, lrequest_t const *  _request)
{
    if ( _request->command == "GET")
    {
        if ( cbm::is_equal( _request->uri.scheme(), "inproc"))
        {
            state_request_inproc_t  inproc_query;
            return  g_mobile_substate_get_member_s(
                &inproc_query, _reply, _request->data.get_string( ":item:"), this);
        }
    }
    return  1;
}
#endif  /* _LDNDC_HAVE_ONLINECONTROL */

