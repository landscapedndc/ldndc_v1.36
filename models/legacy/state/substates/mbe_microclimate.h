/*!
 * @brief
 *    container for state variable belonging to
 *    micro climate processes. it also acts as
 *    a one-timestep buffer for climate data records.
 *
 * @author
 *    steffen klatt
 *    edwin haas
 */

#ifndef  LDNDC_SUBSTATE_MICROCLIMATE_H_
#define  LDNDC_SUBSTATE_MICROCLIMATE_H_

#include  "state/mbe_substate.h"
#include  "substate/mbe_microclimate.h.inc"

namespace ldndc {
class LDNDC_API substate_microclimate_t : public MoBiLE_Substate
{
    MOBILE_SUBSTATE_OBJECT(microclimate,LSUB_FLAG_NONE);
    public:
        /* holds micro climate state items */
        LDNDC_microclimate_SUBSTATE_ITEMS

    public:
#ifdef  _HAVE_SERIALIZE
        /* interface declaration for dump/restore mechanism */
        int  create_checkpoint( substate_checkpoint_write_context_t *);
        int  restore_checkpoint( substate_checkpoint_read_context_t *);
#endif  /* _HAVE_SERIALIZE */
#ifdef  _LDNDC_HAVE_ONLINECONTROL
    public:
        int  process_request(
                lreply_t * /*reply*/, lrequest_t const * /*request*/);
#endif  /* _LDNDC_HAVE_ONLINECONTROL */

    private:
        lerr_t  initialize_( cbm::io_kcomm_t *);
};
}


#endif  /*  !LDNDC_SUBSTATE_MICROCLIMATE_H_  */

