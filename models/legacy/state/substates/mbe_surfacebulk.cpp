/*!
 * @brief
 *    ...
 *
 * @author
 *    david kraus (created on: mar 17, 2014),
 *    edwin haas
 */


#include  "state/substates/mbe_surfacebulk.h"


MOBILE_SUBSTATE_OBJECT_DEFN(surfacebulk)
ldndc::substate_surfacebulk_t::substate_surfacebulk_t()
        : MoBiLE_Substate()
{
    this->g_construct_();
}
ldndc::substate_surfacebulk_t::substate_surfacebulk_t(
        lid_t const &  _id, MoBiLE_PlantVegetation *)
        : MoBiLE_Substate( _id)
{
    this->g_construct_();
}


ldndc::substate_surfacebulk_t::~substate_surfacebulk_t()
{
    /* by definition no deallocations here ! */
}

lerr_t
ldndc::substate_surfacebulk_t::initialize(
        cbm::io_kcomm_t *  _io_comm,
        void *  /*_mem_offs*/)
{
    if ( this->is_initialized())
    {
        return  LDNDC_ERR_OK;
    }

    if ( !this->set_required_inputs_( _io_comm))
    {
        return  LDNDC_ERR_OBJECT_INIT_FAILED;
    }

    lerr_t  rc = this->resize_entities( _io_comm);
    RETURN_IF_NOT_OK(rc);

    this->set_initialized();
        return  LDNDC_ERR_OK;
}


bool
ldndc::substate_surfacebulk_t::has_required_inputs_(
        cbm::io_kcomm_t *)
const
{
    return  true;
}
bool
ldndc::substate_surfacebulk_t::set_required_inputs_(
        cbm::io_kcomm_t *  _io_comm)
{
    return  this->has_required_inputs_( _io_comm);
}


#define  SIZE_DIM_W  this->surfacebulk_layer_cnt()

lerr_t
ldndc::substate_surfacebulk_t::resize_entities(
         cbm::io_kcomm_t *  _io_comm)
{
    return  this->g_resize_( _io_comm);
}

#include  "substate/mbe_surfacebulk.cpp.inc"

#undef  SIZE_DIM_W

#ifdef  _HAVE_SERIALIZE
int
ldndc::substate_surfacebulk_t::create_checkpoint(
        substate_checkpoint_write_context_t *  _cp_cxt)
{
    return  g_mobile_substate_write_members_s( this, _cp_cxt);
}
int
ldndc::substate_surfacebulk_t::restore_checkpoint(
        substate_checkpoint_read_context_t *  _cp_cxt)
{
    return  g_mobile_substate_read_members_s( this, _cp_cxt);
}
#endif /* _HAVE_SERIALIZE */

#ifdef  _LDNDC_HAVE_ONLINECONTROL
#include  "comm/lmessage.h"
int
ldndc::substate_surfacebulk_t::process_request(
        lreply_t *  _reply, lrequest_t const *  _request)
{
    if ( _request->command == "GET")
    {
        if ( cbm::is_equal( _request->uri.scheme(), "inproc"))
        {
            state_request_inproc_t  inproc_query;
            return  g_mobile_substate_get_member_s(
                &inproc_query, _reply, _request->data.get_string( ":item:"), this);
        }
    }
    return  1;
}
#endif  /* _LDNDC_HAVE_ONLINECONTROL */

