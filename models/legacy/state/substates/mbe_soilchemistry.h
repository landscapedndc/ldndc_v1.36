/*!
 * @brief
 *    container for state variable belonging to
 *    soil processes
 *
 * @author
 *    Steffen Klatt,
 *    Edwin Haas,
 *    David Kraus
 */

#ifndef  LDNDC_SUBSTATE_SOILCHEMISTRY_H_
#define  LDNDC_SUBSTATE_SOILCHEMISTRY_H_

#include  "state/mbe_substate.h"
#include  "substate/mbe_soilchemistry.h.inc"

namespace ldndc {
class LDNDC_API substate_soilchemistry_t : public MoBiLE_Substate
{
    /*! Step size for fitting the pools */
    static double const  INCR;

    MOBILE_SUBSTATE_OBJECT(soilchemistry,LSUB_FLAG_NONE);
    public:
        /* holds soil chemistry state items */
        LDNDC_soilchemistry_SUBSTATE_ITEMS

    public:
#ifdef  _HAVE_SERIALIZE
        /* interface declaration for dump/restore mechanism */
        int  create_checkpoint( substate_checkpoint_write_context_t *);
        int  restore_checkpoint( substate_checkpoint_read_context_t *);
#endif  /* _HAVE_SERIALIZE */
#ifdef  _LDNDC_HAVE_ONLINECONTROL
    public:
        int  process_request(
                lreply_t * /*reply*/, lrequest_t const * /*request*/);
#endif  /* _LDNDC_HAVE_ONLINECONTROL */

    private:
        lerr_t  initialize_profile_(
                cbm::io_kcomm_t *);
        lerr_t  initialize_pools_(
                cbm::io_kcomm_t *);
        /*!
         * @brief
         *    alternative soil carbon pools initialisation
         *
         *    The correct CN value of the litter is found by incrementing the litter CN
         *    ratio through the litter fractions with fraction "INCR" until a satisfactory
         *    value is found.
         */
        lerr_t  initialize_soil_carbon_(
                cbm::io_kcomm_t *);
    
    public:

        double get_soil_mass( size_t /* soil layer */) const;

        double get_accumulated_c_net_flux() const;

        double get_bulk_density_without_stones( size_t /* soil layer */) const;

        double get_organic_carbon_content_without_stones( size_t /* soil layer */) const;

        //specifying subdivisions for crop specific fertilization in multi-cropping systems
        std::map< std::string, double > n_subdivision;
};
}


#endif  /*  !LDNDC_SUBSTATE_SOILCHEMISTRY_H_  */

