/*!
 * @brief
 *    container for state variable belonging to
 *    air chemistry processes
 *
 * @author
 *    edwin haas,
 *    steffen klatt
 */

#ifndef  MOBILE_SUBSTATE_AIRCHEMISTRY_H_
#define  MOBILE_SUBSTATE_AIRCHEMISTRY_H_

#include  "state/mbe_substate.h"
#include  "substate/mbe_airchemistry.h.inc"


#define  AC_SPECIES             63

/*! Index declaration for variable species in C and VAR
   VAR(ind_spc) = C(ind_spc)                                      */
#define ind_SULF             0
#define ind_ORA1             1
#define ind_ORA2             2
#define ind_CO2              3
#define ind_ETH              4
#define ind_O1D              5
#define ind_SO2              6
#define ind_HC5              7
#define ind_HC8              8
#define ind_TOL              9
#define ind_XYL              10
#define ind_TPAN             11
#define ind_HONO             12
#define ind_H2O2             13
#define ind_HC3              14
#define ind_N2O5             15
#define ind_CH4              16
#define ind_PAA              17
#define ind_HNO4             18
#define AC_CIDX_O3P              19
#define ind_OP1              20
#define ind_CSL              21
#define AC_CIDX_PAN              22
#define ind_OL2              23
#define AC_CIDX_HNO3             24
#define ind_CO               25
#define AC_CIDX_ISO              26
#define ind_OLT              27
#define ind_OLI              28
#define ind_GLY              29
#define ind_DCB              30
#define ind_XNO2             31
#define ind_KET              32
#define ind_MGLY             33
#define ind_TOLP             34
#define ind_XYLP             35
#define ind_OL2P             36
#define ind_OLTP             37
#define ind_OLN              38
#define ind_XO2              39
#define ind_HC5P             40
#define ind_OP2              41
#define ind_HCHO             42
#define ind_HC8P             43
#define ind_TCO3             44
#define AC_CIDX_O3               45
#define ind_ONIT             46
#define ind_ALD              47
#define ind_OLIP             48
#define ind_KETP             49
#define ind_HC3P             50
#define ind_ACO3             51
#define AC_CIDX_NO3              52
#define ind_HO2              53
#define ind_MO2              54
#define ind_OH               55
#define AC_CIDX_NO2              56
#define AC_CIDX_NO               57
#define ind_ETHP             58

namespace ldndc {
class LDNDC_API substate_airchemistry_t : public MoBiLE_Substate
{
    MOBILE_SUBSTATE_OBJECT(airchemistry,LSUB_FLAG_NONE);
    public:
        /* holds air chemistry state items */
        LDNDC_airchemistry_SUBSTATE_ITEMS

    public:
#ifdef  _HAVE_SERIALIZE
        /* interface declaration for dump/restore mechanism */
        int  create_checkpoint( substate_checkpoint_write_context_t *);
        int  restore_checkpoint( substate_checkpoint_read_context_t *);
#endif  /* _HAVE_SERIALIZE */
#ifdef  _LDNDC_HAVE_ONLINECONTROL
    public:
        int  process_request(
                lreply_t * /*reply*/, lrequest_t const * /*request*/);
#endif  /* _LDNDC_HAVE_ONLINECONTROL */
};
}

#endif  /*  !MOBILE_SUBSTATE_AIRCHEMISTRY_H_  */

