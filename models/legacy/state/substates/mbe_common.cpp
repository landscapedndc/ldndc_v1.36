
#include  "state/substates/mbe_common.h"
#include  "soilchemistry/ld_litterheight.h"

#include  <scientific/soil/ld_soil.h>
#include  <scientific/hydrology/ld_vangenuchten.h>
#include  <logging/cbm_logging.h>

#define STRATUM_INFO(...)      \
{                             \
    if ( _info )              \
    {                         \
        LOGINFO(__VA_ARGS__); \
    }                         \
}                             \


lerr_t
ldndc::assign_soillayer_clay_and_sand_content(
                                              double &_clay,
                                              double &_sand,
                                              int const &_nd_stratum,
                                              double const &_depth,
                                              const ldndc::site::input_class_site_t *_site,
                                              const ldndc::soillayers::input_class_soillayers_t *_soillayers,
                                              const ldndc::soilparameters::input_class_soilparameters_t *_soilparameters,
                                              bool _info)
{
    ldndc::site::iclass_site_stratum_t stratum;
    lerr_t  rc_stratum = _soillayers->stratum( _nd_stratum, &stratum);
    RETURN_IF_NOT_OK( rc_stratum);
    
    /* CLAY CONTENT */
    if ( cbm::flt_greater_equal_zero( stratum.clay))
    {
        _clay = stratum.clay;
    }
    else
    {
        if ( forest_floor( _depth, *_soillayers, *_site))
        {
            _clay = _soilparameters->HUMUS_CLAY( _soillayers->humus_type());
        }
        else
        {
            _clay = _soilparameters->SOIL_CLAY( _soillayers->soil_type());
        }
        
        if ( _info)
        {
            STRATUM_INFO( "Clay content of stratum ", _nd_stratum+1, " not initialized, estimated value: ", _clay," [g g-1]");
        }
    }
    
    if ( cbm::flt_greater( _clay, 1.0))
    {
        LOGERROR( "Clay content of stratum ", _nd_stratum+1, " out of range: ", _clay," [g g-1]");
        return  LDNDC_ERR_FAIL;
    }
    
    /* SAND CONTENT */
    if ( cbm::flt_greater_equal_zero( stratum.sand))
    {
        _sand = stratum.sand;
    }
    else
    {
        if ( forest_floor( _depth, *_soillayers, *_site))
        {
            _sand = _soilparameters->HUMUS_SAND( _soillayers->humus_type());
        }
        else
        {
            _sand = _soilparameters->SOIL_SAND( _soillayers->soil_type());
        }
        STRATUM_INFO( "Sand content of stratum ", _nd_stratum+1, " not initialized, estimated value: ", _sand," [g g-1]");
    }
    if ( cbm::flt_greater( _sand, 1.0))
    {
        LOGERROR( "Sand content of stratum ", _nd_stratum+1, " out of range: ", _nd_stratum," [g g-1]");
        return  LDNDC_ERR_FAIL;
    }
    
    if ( cbm::flt_greater( _clay + _sand, 1.0))
    {
        double const sand_old( _sand);
        double const clay_old( _clay);
        double const scale( 1.0 / (_clay + _sand));
        _clay *= scale;
        _sand *= scale;
        STRATUM_INFO( "Clay and sand content have been changed in stratum ",_nd_stratum+1,
                      ": clay: ", clay_old, " -> ", _clay," [g g-1]",
                      ", sand: ", sand_old, " -> ", _sand," [g g-1]");
    }
    return  LDNDC_ERR_OK;
}


lerr_t
ldndc::assign_soillayer_stone_fraction(
                                       double &_stone,
                                       int const &_nd_stratum,
                                       double const &_depth,
                                       const ldndc::site::input_class_site_t *_site,
                                       const ldndc::soillayers::input_class_soillayers_t *_soillayers,
                                       const ldndc::soilparameters::input_class_soilparameters_t *_soilparameters,
                                       bool _info)
{
    ldndc::site::iclass_site_stratum_t stratum;
    lerr_t  rc_stratum = _soillayers->stratum( _nd_stratum, &stratum);
    RETURN_IF_NOT_OK( rc_stratum);
    
    if ( cbm::flt_greater_equal_zero( stratum.stone_fraction))
    {
        _stone = stratum.stone_fraction;
    }
    else
    {
        if ( forest_floor( _depth, *_soillayers, *_site))
        {
            _stone = _soilparameters->HUMUS_STONEFRACTION( _soillayers->humus_type());
        }
        else
        {
            double clay( 0.0);
            double sand( 0.0);
            lerr_t rc_texture = assign_soillayer_clay_and_sand_content( clay, sand, _nd_stratum, _depth, _site, _soillayers, _soilparameters, false);
            if ( rc_texture)
            {
                LOGERROR( "Initialization of stone fraction of stratum ", _nd_stratum+1, " not successfull due to invalid soil texture info");
                return  LDNDC_ERR_FAIL;
            }

            cbm::string_t texture_name( ldndc::get_soil_texture( sand * 100.0, clay * 100.0));
            ldndc::soillayers::soil_type_e texture_type( ldndc::get_soil_texture_type( texture_name));

            if ( ldndc::use_usda_soil_texture_triangle( _soillayers->soil_type()) &&
                 (texture_type != ldndc::soillayers::SOIL_NONE))
            {
                _stone = _soilparameters->SOIL_STONEFRACTION( texture_type);
            }
            else
            {
                _stone = _soilparameters->SOIL_STONEFRACTION( _soillayers->soil_type());
            }
        }
        STRATUM_INFO( "Stone fraction of stratum ", _nd_stratum+1, " not initialized, estimated value: ", _stone," [m3 m-3]");
    }
    if ( cbm::flt_greater( _stone, 1.0))
    {
        LOGERROR( "Stone fraction of stratum ", _nd_stratum+1, " out of range: ", _stone," [m3 m-3]");
        return  LDNDC_ERR_FAIL;
    }
    return  LDNDC_ERR_OK;
}


lerr_t
ldndc::assign_soillayer_bulk_density(
                              double &_bulk_density,
                              int const &_nd_stratum,
                              double const &_depth,
                              const ldndc::site::input_class_site_t *_site,
                              const ldndc::soillayers::input_class_soillayers_t *_soillayers,
                              const ldndc::soilparameters::input_class_soilparameters_t *_soilparameters,
                              bool _info)
{
    ldndc::site::iclass_site_stratum_t stratum;
    lerr_t  rc_stratum = _soillayers->stratum( _nd_stratum, &stratum);
    RETURN_IF_NOT_OK( rc_stratum);
    
    if ( cbm::flt_greater_zero( stratum.bulk_density))
    {
        _bulk_density = stratum.bulk_density;
    }
    else
    {
        if ( forest_floor( _depth, *_soillayers, *_site))
        {
            _bulk_density = _soilparameters->HUMUS_DENSITY_L( _soillayers->humus_type());
        }
        else
        {
            double clay( 0.0);
            double sand( 0.0);
            lerr_t rc_texture = assign_soillayer_clay_and_sand_content( clay, sand, _nd_stratum, _depth, _site, _soillayers, _soilparameters, false);
            if ( rc_texture)
            {
                LOGERROR( "Initialization of bulk density of stratum ", _nd_stratum+1, " not successfull due to invalid soil texture info");
                return  LDNDC_ERR_FAIL;
            }
            
            cbm::string_t texture_name( ldndc::get_soil_texture( sand * 100.0, clay * 100.0));
            ldndc::soillayers::soil_type_e texture_type( ldndc::get_soil_texture_type( texture_name));
            
            if ( ldndc::use_usda_soil_texture_triangle( _soillayers->soil_type()) &&
                 (texture_type != ldndc::soillayers::SOIL_NONE))
            {
                _bulk_density = _soilparameters->SOIL_DENSITY( texture_type);
            }
            else
            {
                _bulk_density = _soilparameters->SOIL_DENSITY( _soillayers->soil_type());
            }
        }
        STRATUM_INFO( "Soil bulk density of stratum ",_nd_stratum+1," not initialized, estimated value: ",_bulk_density," [kg dm-3]");
    }
    return  LDNDC_ERR_OK;
}


lerr_t
ldndc::assign_soillayer_organic_carbon_and_total_nitrogen_content(
                                                  double _cn_old,
                                                  double &_c_org_without_stones,
                                                  double &_n_without_stones,
                                                  int const &_nd_stratum,
                                                  double _depth,
                                                  double _depth_old,
                                                  const ldndc::site::input_class_site_t *_site,
                                                  const ldndc::soillayers::input_class_soillayers_t *_soillayers,
                                                  const ldndc::soilparameters::input_class_soilparameters_t *_soilparameters,
                                                  bool _info)
{
    ldndc::site::iclass_site_stratum_t stratum;
    lerr_t  rc_stratum = _soillayers->stratum( _nd_stratum, &stratum);
    RETURN_IF_NOT_OK( rc_stratum);
    
    if ( cbm::flt_greater_equal_zero( stratum.c_org))
    {
        _c_org_without_stones = stratum.c_org;
    }
    else
    {
        if ( forest_floor( _depth, *_soillayers, *_site))
        {
            _c_org_without_stones = _soilparameters->HUMUS_CORG( _soillayers->humus_type());
        }
        else if ( cbm::flt_greater_zero( stratum.n_org))
        {
            _c_org_without_stones = stratum.n_org * ldndc::cn_ratio( ECOSYSTEM_CN_RATIO_LEVELS[_site->soil_use_history()],
                                                                    _depth, _depth_old, _cn_old);
        }
        else
        {
            if ( cbm::flt_greater_zero( _soillayers->c_org05()) &&
                 cbm::flt_greater_zero( _soillayers->c_org30()))
            {
                lerr_t rc_corg = ldndc::organic_carbon_fraction( &_c_org_without_stones,
                                                                 _soillayers->c_org05(),
                                                                 _soillayers->c_org30(),
                                                                 _depth);
                if ( rc_corg)
                {
                    LOGERROR( "Failed to estimate organic carbon content for stratum ",_nd_stratum+1);
                    return  LDNDC_ERR_FAIL;
                }
            }
            else
            {
                LOGERROR( "Failed to estimate organic carbon content for stratum ",_nd_stratum+1);
                return  LDNDC_ERR_FAIL;
            }
        }
        STRATUM_INFO( "Organic carbon content of stratum ",_nd_stratum+1," not initialized, estimated value: ", _c_org_without_stones," [g g-1]");
    }

    if ( cbm::flt_greater_equal_zero( stratum.n_org))
    {
        _n_without_stones = stratum.n_org;
    }
    else
    {
        if ( forest_floor( _depth, *_soillayers, *_site))
        {
            _n_without_stones = _c_org_without_stones / _soilparameters->HUMUS_CNORG( _soillayers->humus_type());
        }
        else
        {
            _n_without_stones = _c_org_without_stones / ldndc::cn_ratio( ECOSYSTEM_CN_RATIO_LEVELS[_site->soil_use_history()],
                                                                        _depth, _depth_old, _cn_old);
        }
        STRATUM_INFO( "Total nitrogen content of stratum ",_nd_stratum+1," not initialized, estimated value: ", _n_without_stones," [g g-1]");
    }
    
    return  LDNDC_ERR_OK;
}


lerr_t
ldndc::assign_soillayer_porosity(
                                 double &_porosity_without_stones,
                                 int const &_nd_stratum,
                                 double const &_depth,
                                 double const &_c_org_without_stones,
                                 double const &_bulk_density_without_stones,
                                 const ldndc::site::input_class_site_t *_site,
                                 const ldndc::soillayers::input_class_soillayers_t *_soillayers,
                                 const ldndc::soilparameters::input_class_soilparameters_t *_soilparameters,
                                 bool _info)
{
    ldndc::site::iclass_site_stratum_t stratum;
    lerr_t  rc_stratum = _soillayers->stratum( _nd_stratum, &stratum);
    RETURN_IF_NOT_OK( rc_stratum);
    
    if( cbm::flt_greater_equal_zero( stratum.porosity))
    {
        _porosity_without_stones = stratum.porosity;
    }
    else
    {
        double clay( 0.0);
        double sand( 0.0);
        lerr_t rc_texture = assign_soillayer_clay_and_sand_content( clay, sand, _nd_stratum, _depth, _site, _soillayers, _soilparameters, false);
        if ( rc_texture)
        {
            LOGERROR( "Initialization of porosity of stratum ", _nd_stratum+1, " not successfull due to invalid soil texture info");
            return  LDNDC_ERR_FAIL;
        }
        
        /*!
         *  Porosity is estimated based on organic carbon fraction and bulk density for the soil including stones.
         *  Porosity is given by a value between 0 and 1.
         *  See: @link ldndc::porosity() porosity() @endlink
         */
        double const min_dens( sand * cbm::DSAN + (1.0 - sand - clay) * cbm::DSIL + clay * cbm::DCLA);
        _porosity_without_stones = ldndc::porosity( _c_org_without_stones, _bulk_density_without_stones, min_dens);
        
        STRATUM_INFO( "Porosity of stratum ",_nd_stratum+1," not initialized, estimated value (without stones): ", _porosity_without_stones*100.0," [%]");
    }

    if ( !cbm::flt_in_range_lu( 0.0, _porosity_without_stones, 1.0))
    {
        STRATUM_INFO( "Porosity of stratum ", _nd_stratum+1, " out of range: ", _porosity_without_stones," [m3 m-3]");
        return  LDNDC_ERR_FAIL;
    }
    else
    {
        return  LDNDC_ERR_OK;
    }
}


lerr_t
ldndc::assign_soillayer_van_genuchten_parameter(
                                                double &_vg_a,
                                                double &_vg_n,
                                                double &_vg_m,
                                                bool &_vg_parameter_from_input,
                                                int const &_nd_stratum,
                                                double const &_depth,
                                                const ldndc::site::input_class_site_t *_site,
                                                const ldndc::soillayers::input_class_soillayers_t *_soillayers,
                                                const ldndc::soilparameters::input_class_soilparameters_t *_soilparameters,
                                                bool _info)
{
    ldndc::site::iclass_site_stratum_t stratum;
    lerr_t  rc_stratum = _soillayers->stratum( _nd_stratum, &stratum);
    RETURN_IF_NOT_OK( rc_stratum);

    if( cbm::flt_greater_equal_zero( stratum.vangenuchten_n))
    {
        _vg_n = stratum.vangenuchten_n;
        _vg_parameter_from_input = true;
    }
    else
    {
//                if ( cbm::flt_greater_equal_zero( stratum.clay) &&
//                     cbm::flt_greater_equal_zero( stratum.sand))
//                {
//                    vg_n_sl[sl] = exp(0.053 - (0.009 * sand_sl[sl] * 100.0) - (0.013 * clay_sl[sl] * 100.0) + (0.00015 * sand_sl[sl] * 100.0 * sand_sl[sl] * 100.0));
//                }
//                else
        if ( forest_floor( _depth, *_soillayers, *_site))
        {
            _vg_n = _soilparameters->HUMUS_VANGENUCHTEN_N( _soillayers->humus_type());
        }
        else
        {
            double clay( 0.0);
            double sand( 0.0);
            lerr_t rc_texture = assign_soillayer_clay_and_sand_content( clay, sand, _nd_stratum, _depth, _site, _soillayers, _soilparameters, false);
            if ( rc_texture)
            {
                LOGERROR( "Initialization of van Genuchten parameters of stratum ", _nd_stratum+1, " not successfull due to invalid soil texture info");
                return  LDNDC_ERR_FAIL;
            }

            cbm::string_t texture_name( ldndc::get_soil_texture( sand * 100.0, clay * 100.0));
            ldndc::soillayers::soil_type_e texture_type( ldndc::get_soil_texture_type( texture_name));

            if ( ldndc::use_usda_soil_texture_triangle( _soillayers->soil_type()) &&
                 (texture_type != ldndc::soillayers::SOIL_NONE))
            {
                _vg_n = _soilparameters->SOIL_VANGENUCHTEN_N( texture_type);
            }
            else
            {
                _vg_n = _soilparameters->SOIL_VANGENUCHTEN_N( _soillayers->soil_type());
            }
        }
        _vg_parameter_from_input = false;
        STRATUM_INFO( "van Genuchten parameter n of stratum ", _nd_stratum+1, " not initialized, estimated value: ", _vg_n," [-]");
    }

    if( !cbm::flt_in_range_lu( 1.0, _vg_n, 10.0))
    {
        LOGERROR( "Invalid value for van Genuchten parameter n in stratum ",_nd_stratum+1, ": ",_vg_n,". Value must be within the interval: [1.0, 10.0]!");
        return  LDNDC_ERR_FAIL;
    }
    else
    {
        _vg_m = cbm::bound_min(0.0, 1.0 - (1.0 / _vg_n));
    }

    if( cbm::flt_greater_equal_zero( stratum.vangenuchten_alpha))
    {
        _vg_a = stratum.vangenuchten_alpha;
    }
    else
    {
//                if ( cbm::flt_greater_equal_zero( stratum.clay) &&
//                     cbm::flt_greater_equal_zero( stratum.sand))
//                {
//                    vg_a_sl[sl] = exp(-2.486 + 0.025 * sand_sl[sl] * 100.0 - 0.351 * _c_org_without_stones * 100.0 - 2.617 * bulk_density_without_stones - 0.023 * clay_sl[sl] * 100.0);
//                }
//                else
        _vg_parameter_from_input = false;
        if ( forest_floor( _depth, *_soillayers, *_site))
        {
            _vg_a = _soilparameters->HUMUS_VANGENUCHTEN_ALPHA( _soillayers->humus_type());
        }
        else
        {
            double clay( 0.0);
            double sand( 0.0);
            lerr_t rc_texture = assign_soillayer_clay_and_sand_content( clay, sand, _nd_stratum, _depth, _site, _soillayers, _soilparameters, false);
            if ( rc_texture)
            {
                LOGERROR( "Initialization of van Genuchten parameters of stratum ", _nd_stratum+1, " not successfull due to invalid soil texture info");
                return  LDNDC_ERR_FAIL;
            }

            cbm::string_t texture_name( ldndc::get_soil_texture( sand * 100.0, clay * 100.0));
            ldndc::soillayers::soil_type_e texture_type( ldndc::get_soil_texture_type( texture_name));
            
            if ( ldndc::use_usda_soil_texture_triangle( _soillayers->soil_type()) &&
                 (texture_type != ldndc::soillayers::SOIL_NONE))
            {
                _vg_a = _soilparameters->SOIL_VANGENUCHTEN_ALPHA( texture_type);
            }
            else
            {
                _vg_a = _soilparameters->SOIL_VANGENUCHTEN_ALPHA( _soillayers->soil_type());
            }
        }
        STRATUM_INFO( "van Genuchten parameter alpha of stratum ", _nd_stratum+1, " not initialized, estimated value is ", _vg_a," [m-1]");
    }
    if( !cbm::flt_in_range_lu( 0.0, _vg_a, 50.0))
    {
        LOGERROR( "Invalid value for van Genuchten parameter alpha in stratum ",_nd_stratum+1, ": ",_vg_a," [m-1]");
        return  LDNDC_ERR_FAIL;
    }

    return  LDNDC_ERR_OK;
}


lerr_t
ldndc::assign_soillayer_wfps_max_and_min(
                                         double &_wfps_max,
                                         double &_wfps_min,
                                         double &_porosity_with_stones,
                                         int const &_nd_stratum,
                                         double const &_depth,
                                         double const &_c_org_without_stones,
                                         double const &_bulk_density_without_stones,
                                         const ldndc::site::input_class_site_t *_site,
                                         const ldndc::soillayers::input_class_soillayers_t *_soillayers,
                                         const ldndc::soilparameters::input_class_soilparameters_t *_soilparameters,
                                         bool _info)
{
    ldndc::site::iclass_site_stratum_t stratum;
    lerr_t  rc_stratum = _soillayers->stratum( _nd_stratum, &stratum);
    RETURN_IF_NOT_OK( rc_stratum);
    
    double stone_fraction( 0.0);
    lerr_t rc_stone = assign_soillayer_stone_fraction( stone_fraction, _nd_stratum, _depth, _site, _soillayers, _soilparameters, false);
    if ( rc_stone)
    {
        LOGERROR( "Initialization of max/min water filled pore spaces of stratum ", _nd_stratum+1, " not successfull due to invalid stone content");
        return  LDNDC_ERR_FAIL;
    }
    
    double clay( 0.0);
    double sand( 0.0);
    lerr_t rc_texture = assign_soillayer_clay_and_sand_content( clay, sand, _nd_stratum, _depth, _site, _soillayers, _soilparameters, false);
    if ( rc_texture)
    {
        LOGERROR( "Initialization of max/min water filled pore spaces of stratum ", _nd_stratum+1, " not successfull due to invalid soil texture info");
        return  LDNDC_ERR_FAIL;
    }
    
    cbm::string_t texture_name( ldndc::get_soil_texture( sand * 100.0, clay * 100.0));
    ldndc::soillayers::soil_type_e texture_type( ldndc::get_soil_texture_type( texture_name));
    

    /* MAXIMUM WATER FILLED PORE SPACE */
    if( cbm::flt_greater_equal_zero( stratum.wfps_max))
    {
        _wfps_max = stratum.wfps_max;
    }
    else
    {
        if (   cbm::flt_greater_equal_zero( stratum.clay))
        {
            //original regression boundaries [1.04, 1.23] are de-/increased by +/- 50%
            double const bd( cbm::bound( 0.5 * 1.04 , _bulk_density_without_stones, 1.23 * 1.5));
            double const wc_sat_with_stones( (0.81 - 0.283 * bd + 0.001 * stratum.clay * 100.0) * (1.0 - stone_fraction));
            if( cbm::flt_greater( _porosity_with_stones, wc_sat_with_stones))
            {
                _wfps_max = wc_sat_with_stones / _porosity_with_stones;
            }
            else
            {
                STRATUM_INFO( "Porosity of stratum ", _nd_stratum+1," was smaller than estimated maximum water saturation and was increased from ", _porosity_with_stones," to ", wc_sat_with_stones);
                _porosity_with_stones = wc_sat_with_stones;
                _wfps_max = 1.0;
            }
        }
        else
        {
            if ( forest_floor( _depth, *_soillayers, *_site))
            {
                _wfps_max = _soilparameters->HUMUS_WFPS_MAX( _soillayers->humus_type());
            }
            else
            {
                if ( ldndc::use_usda_soil_texture_triangle( _soillayers->soil_type()) &&
                     (texture_type != ldndc::soillayers::SOIL_NONE))
                {
                    _wfps_max = _soilparameters->SOIL_WFPS_MAX( texture_type);
                }
                else
                {
                    _wfps_max = _soilparameters->SOIL_WFPS_MAX( _soillayers->soil_type());
                }
            }
        }
        STRATUM_INFO( "Maximum water filled pore space of stratum ", _nd_stratum+1," not initialized, estimated value: ", _wfps_max);
    }
    if( cbm::flt_greater(_wfps_max, 1.0))
    {
        STRATUM_INFO( "Maximum water filled pore space of stratum ", _nd_stratum+1," exceeded 1 and was set to 1 from ", _wfps_max);
        _wfps_max = 1.0;
    }


    /* MINIMUM WATER FILLED PORE SPACE */
    if( cbm::flt_greater_equal_zero( stratum.wfps_min))
    {
        _wfps_min = stratum.wfps_min;
    }
    else
    {
        if (   cbm::flt_greater_equal_zero( stratum.clay))
        {
            //original regression boundaries [0.0001, 0.066] are de-/increased by +/- 50%
            double const corg( cbm::bound( 0.5 * 0.0001 , _c_org_without_stones, 0.066 * 1.5));
            double const wc_res_with_stones( (0.015 + 0.005 * stratum.clay * 100.0 + 0.014 * corg * 100.0) * (1.0 - stone_fraction));
            _wfps_min = wc_res_with_stones / _porosity_with_stones;
        }
        else
        {
            if ( forest_floor( _depth, *_soillayers, *_site))
            {
                _wfps_min = _soilparameters->HUMUS_WFPS_MIN( _soillayers->humus_type());
            }
            else
            {
                if ( ldndc::use_usda_soil_texture_triangle( _soillayers->soil_type()) &&
                     (texture_type != ldndc::soillayers::SOIL_NONE))
                {
                    _wfps_min = _soilparameters->SOIL_WFPS_MIN( texture_type);
                }
                else
                {
                    _wfps_min = _soilparameters->SOIL_WFPS_MIN( _soillayers->soil_type());
                }
            }
        }
        STRATUM_INFO( "Minimum water filled pore space of stratum ", _nd_stratum+1," not initialized, estimated value: ", _wfps_min);
    }
    if( cbm::flt_less(_wfps_min, 0.0))
    {
        STRATUM_INFO( "Minimum water filled pore space in stratum ", _nd_stratum+1," was smaller 0 and was set to 0 from ", _wfps_min);
        _wfps_min = 0.0;
    }
    
    return  LDNDC_ERR_OK;
}


lerr_t
ldndc::assign_soillayer_field_capacity_and_wilting_point(
                                                 double &_wcmax_without_stones,
                                                 double &_wcmin_without_stones,
                                                 double _wfps_max,
                                                 double _wfps_min,
                                                 double _porosity_without_stones,
                                                 int const &_nd_stratum,
                                                 double const &_depth,
                                                 const ldndc::site::input_class_site_t *_site,
                                                 const ldndc::soillayers::input_class_soillayers_t *_soillayers,
                                                 const ldndc::soilparameters::input_class_soilparameters_t *_soilparameters,
                                                 bool _vg_prio,
                                                 bool _info)
{
    ldndc::site::iclass_site_stratum_t stratum;
    lerr_t  rc_stratum = _soillayers->stratum( _nd_stratum, &stratum);
    RETURN_IF_NOT_OK( rc_stratum);
    
    bool vg_parameter_from_input( true);
    double vg_a( 0.0);
    double vg_n( 0.0);
    double vg_m( 0.0);
    lerr_t rc_vg = assign_soillayer_van_genuchten_parameter( vg_a, vg_n, vg_m, vg_parameter_from_input,
                                                             _nd_stratum, _depth, _site, _soillayers, _soilparameters, false);
    if ( rc_vg)
    {
        LOGERROR( "Initialization of field capacity and wilting point of stratum ", _nd_stratum+1, " not successfull due to invalid van Genuchen parameters");
        return  LDNDC_ERR_FAIL;
    }

    double stone_fraction( 0.0);
    lerr_t rc_stone = assign_soillayer_stone_fraction( stone_fraction, _nd_stratum, _depth, _site, _soillayers, _soilparameters, false);
    if ( rc_stone)
    {
        LOGERROR( "Initialization of field capacity and wilting point of stratum ", _nd_stratum+1, " not successfull due to invalid stone content");
        return  LDNDC_ERR_FAIL;
    }

    double clay( 0.0);
    double sand( 0.0);
    lerr_t rc_texture = assign_soillayer_clay_and_sand_content( clay, sand, _nd_stratum, _depth, _site, _soillayers, _soilparameters, false);
    if ( rc_texture)
    {
        LOGERROR( "Initialization of field capacity and wilting point of stratum ", _nd_stratum+1, " not successfull due to invalid soil texture info");
        return  LDNDC_ERR_FAIL;
    }

    cbm::string_t texture_name( ldndc::get_soil_texture( sand * 100.0, clay * 100.0));
    ldndc::soillayers::soil_type_e texture_type( ldndc::get_soil_texture_type( texture_name));

    if( cbm::flt_greater_equal_zero( stratum.wcmax))
    {
        _wcmax_without_stones = stratum.wcmax * cbm::M_IN_MM;
    }
    else
    {
        if ( vg_parameter_from_input || _vg_prio)
        {
            //capillary pressure at field capacity (pF value of 1.8)
            double const cp_fc = 0.63; // [m]
            _wcmax_without_stones = ldndc::hydrology::water_content( cp_fc, vg_a, vg_n, vg_m,
                                                                     _wfps_max * _porosity_without_stones,
                                                                     _wfps_min * _porosity_without_stones);
            STRATUM_INFO( "Field capacity in stratum ", _nd_stratum+1," not initialized, estimated value (without stones): ",
                          _wcmax_without_stones  * (1.0 - stone_fraction), " [m3 m-3] (based on van Genuchten)");
        }
        else
        {
            if ( forest_floor( _depth, *_soillayers, *_site))
            {
                _wcmax_without_stones = _soilparameters->HUMUS_WCMAX( _soillayers->humus_type()) * cbm::M_IN_MM;
            }
            else
            {
                if ( ldndc::use_usda_soil_texture_triangle( _soillayers->soil_type()) &&
                     (texture_type != ldndc::soillayers::SOIL_NONE))
                {
                    _wcmax_without_stones = _soilparameters->SOIL_WCMAX( texture_type) * cbm::M_IN_MM;
                }
                else
                {
                    _wcmax_without_stones = _soilparameters->SOIL_WCMAX( _soillayers->soil_type()) * cbm::M_IN_MM;
                }
            }
            STRATUM_INFO( "Field capacity in stratum ", _nd_stratum+1," not initialized, estimated value is (without stones): ",
                          _wcmax_without_stones, " [m3 m-3] (based on soil texture)");
        }
    }
    if( !cbm::flt_in_range_lu( 0.0, _wcmax_without_stones, 1.0))
    {
        LOGERROR( "Invalid value for field capacity in stratum ",_nd_stratum+1, ": ", _wcmax_without_stones," [m3 m-3]");
        return  LDNDC_ERR_FAIL;
    }

    /*!
     *  Wilting point is checked for consistency against
     *  field capacity, i.e., field capacity is not allowed to
     *  exceed 90% of estimated field capacity: \n\n
     *  \f$ \theta_{min} = min(\theta_{min}, 0.9 \; \theta_{max}) \f$ \n\n
     */
    double const wcmin_fraction( 0.9);
    if( cbm::flt_greater_equal_zero( stratum.wcmin))
    {
        _wcmin_without_stones = stratum.wcmin * cbm::M_IN_MM;
    }
    else
    {
        if ( vg_parameter_from_input || _vg_prio)
        {
            double const cp_wp = 150.0; // must be the same unit as 1/alpha -> m
            _wcmin_without_stones = ldndc::hydrology::water_content( cp_wp, vg_a, vg_n, vg_m,
                                                                     _wfps_max * _porosity_without_stones,
                                                                     _wfps_min * _porosity_without_stones);
            STRATUM_INFO( "Wilting point in stratum ", _nd_stratum+1," not initialized, estimated value (without stones): ",
                          _wcmin_without_stones  * (1.0 - stone_fraction), " [m3 m-3] (based on van Genuchten)");
        }
        else
        {
            if ( forest_floor( _depth, *_soillayers, *_site))
            {
                _wcmin_without_stones = _soilparameters->HUMUS_WCMIN( _soillayers->humus_type()) * cbm::M_IN_MM;
            }
            else
            {
                if ( ldndc::use_usda_soil_texture_triangle( _soillayers->soil_type()) &&
                     (texture_type != ldndc::soillayers::SOIL_NONE))
                {
                    _wcmin_without_stones = _soilparameters->SOIL_WCMIN( texture_type) * cbm::M_IN_MM;
                }
                else
                {
                    _wcmin_without_stones = _soilparameters->SOIL_WCMIN( _soillayers->soil_type()) * cbm::M_IN_MM;
                }
            }
            STRATUM_INFO( "Wilting point in stratum ", _nd_stratum+1," not initialized, estimated value is (without stones): ",
                          _wcmin_without_stones, " [m3 m-3] (based on soil texture)");
        }
    }
    if( !cbm::flt_in_range_lu( 0.0, _wcmin_without_stones, 1.0))
    {
        LOGERROR( "Invalid value for wilting point in stratum ", _nd_stratum+1, ": ", _wcmin_without_stones," [m3 m-3]");
        return  LDNDC_ERR_FAIL;
    }
    else if ( cbm::flt_greater( _wcmin_without_stones, wcmin_fraction * _wcmax_without_stones))
    {
        _wcmin_without_stones = wcmin_fraction * _wcmax_without_stones;
        STRATUM_INFO( "Wilting point in stratum ", _nd_stratum+1," too close to field capacity. Wilting point changed to: ", _wcmin_without_stones, " [mm/m]");
    }
    return  LDNDC_ERR_OK;
}

