/*!
 * @brief
 *  Soilchemistry state initialization.
 *
 * @authors
 *  - Steffen Klatt
 *  - Edwin Haas
 *  - David Kraus
 */


#include  "state/substates/mbe_common.h"
#include  "state/substates/mbe_soilchemistry.h"
#include  "soilchemistry/ld_litterheight.h"

#include  <scientific/soil/ld_soil.h>
#include  <scientific/hydrology/ld_vangenuchten.h>
#include  <input/airchemistry/airchemistry.h>
#include  <input/site/site.h>
#include  <input/siteparameters/siteparameters.h>
#include  <input/soillayers/soillayers.h>
#include  <constants/cbm_const.h>
#include  <logging/cbm_logging.h>
#include  <utils/cbm_utils.h>


/* 1 promille */
const double  ldndc::substate_soilchemistry_t::INCR = 0.001;

MOBILE_SUBSTATE_OBJECT_DEFN(soilchemistry)
ldndc::substate_soilchemistry_t::substate_soilchemistry_t()
        : MoBiLE_Substate()
{
    g_construct_();
}


ldndc::substate_soilchemistry_t::substate_soilchemistry_t(
        lid_t const &  _id, MoBiLE_PlantVegetation *)
        : MoBiLE_Substate( _id)
{
    g_construct_();
}


ldndc::substate_soilchemistry_t::~substate_soilchemistry_t()
{
    /* by definition no deallocations here ! */
}


#define  S_AIRCHEMISTRY  _io_comm->get_input_class< airchemistry::input_class_airchemistry_t >()
#define  S_CLIMATE  _io_comm->get_input_class< climate::input_class_climate_t >()
#define  S_SITE  _io_comm->get_input_class< site::input_class_site_t >()
#define  S_SITEPARAMETERS  _io_comm->get_input_class< siteparameters::input_class_siteparameters_t >()
#define  S_SOILPARAMETERS  _io_comm->get_input_class< soilparameters::input_class_soilparameters_t >()
#define  S_SOILLAYERS  _io_comm->get_input_class< soillayers::input_class_soillayers_t >()



lerr_t
ldndc::substate_soilchemistry_t::initialize(
        cbm::io_kcomm_t *  _io_comm,
        void *  /*_mem_offs*/)
{
    if ( is_initialized())
    {
        return  LDNDC_ERR_OK;
    }

    if ( !set_required_inputs_( _io_comm))
    {
        return  LDNDC_ERR_OBJECT_INIT_FAILED;
    }

    lerr_t  rc = resize_entities( _io_comm);
    RETURN_IF_NOT_OK(rc);

    rc = initialize_profile_( _io_comm);
    RETURN_IF_NOT_OK(rc);

    rc = initialize_pools_( _io_comm);
    RETURN_IF_NOT_OK(rc);

    set_initialized();

    return  LDNDC_ERR_OK;
}



bool
ldndc::substate_soilchemistry_t::has_required_inputs_(
        cbm::io_kcomm_t *  _io_comm)
const
{
    if ( !S_SITEPARAMETERS || !S_SOILLAYERS || !S_SITE)
    {
        LOGERROR( SUBSTATE_NAMES[substate_type]," substate requires input classes {",
                  LDNDC_INAME(siteparameters), ",",
                  LDNDC_INAME(soilparameters), ",",
                  LDNDC_INAME(soillayers), ",",
                  LDNDC_INAME(site), ",",
                  "}");
        return  false;
    }
    return  true;
}



bool
ldndc::substate_soilchemistry_t::set_required_inputs_(
        cbm::io_kcomm_t *  _io_comm)
{
    return  has_required_inputs_( _io_comm);
}


#define STRATUMINFO(...)      \
{                             \
    if ( sl == sl_st0 )       \
    {                         \
        LOGINFO(__VA_ARGS__); \
    }                         \
}                             \


/*!
 * @details
 *  Initialize soil profile from site input.
 *  At this point all invalid strata properties
 *  have been already gap-filled by default values.
 */
lerr_t
ldndc::substate_soilchemistry_t::initialize_profile_(
                                                     cbm::io_kcomm_t *  _io_comm)
{
    site::input_class_site_t const *  s_site = S_SITE;
    if ( !s_site)
    {
        LOGERROR( "missing input  [class=site]");
        return LDNDC_ERR_FAIL;
    }
    siteparameters::input_class_siteparameters_t const *  s_siteparameters = S_SITEPARAMETERS;
    if ( !s_siteparameters)
    {
        LOGERROR( "missing input  [class=siteparameters]");
        return LDNDC_ERR_FAIL;
    }
    soillayers::input_class_soillayers_t const *  s_soillayers = S_SOILLAYERS;
    if ( !s_soillayers)
    {
        LOGERROR( "missing input  [class=soillayers]");
        return LDNDC_ERR_FAIL;
    }
    soilparameters::input_class_soilparameters_t const *  s_soilparameters = S_SOILPARAMETERS;
    if ( !s_soilparameters)
    {
        LOGERROR( "missing input  [class=soilparameters]");
        return LDNDC_ERR_FAIL;
    }

    double st_depth_start( 0.0);
    double st_depth_end( 0.0);
    size_t  sl_st0( 0); //first soil layer of stratum
    /* parameter distribution into each layer */
    for ( size_t  st = 0;  st < s_soillayers->strata_cnt();  ++st)
    {
        site::iclass_site_stratum_t  stratum;
        lerr_t  rc_stratum = s_soillayers->stratum( st, &stratum);
        RETURN_IF_NOT_OK(rc_stratum);

        site::iclass_site_stratum_t  stratum_zero;
        rc_stratum = s_soillayers->stratum( 0, &stratum_zero);
        RETURN_IF_NOT_OK(rc_stratum);
        
        site::iclass_site_stratum_t  stratum_above = stratum_zero;
        if ( st > 0)
        {
            rc_stratum = s_soillayers->stratum( st-1, &stratum_above);
            RETURN_IF_NOT_OK(rc_stratum);
        }

        st_depth_end += stratum.stratum_height * cbm::M_IN_MM;

        /*!
         *  Bulk soil characterization may in- or exclude stones/rocks
         *  depending on different measurement approaches and points of view.
         *  While smaller stones are most likely included in soil samples of,
         *  e.g., bulk density, greater rocks are commonly removed.
         *  Within the context of LandscapeDNDC, all soil input
         *  refers to the bulk soil excluding stones/rocks.
         *  In contrast, most internal process variables
         *  refer to the bulk soil including stones/rocks.
         */
        size_t  sl_st1 = sl_st0 + stratum.split;
        for ( size_t  sl = sl_st0;  sl < sl_st1;  sl++)
        {
            ldndc_assert( stratum.split > 0);
            STRATUMINFO("STRATUM (",st_depth_start,"-",st_depth_end, "m) ", st+1, ":");

            /* SOILLAYER HEIGHT AND DEPTH */
            if ( !cbm::flt_greater_zero( stratum.stratum_height) || (stratum.split <= 0))
            {
                LOGERROR("Stratum discretization invalid! Stratum height: ", stratum.stratum_height,", stratum split: ", stratum.split);
                return  LDNDC_ERR_FAIL;
            }
            h_sl[sl] = (stratum.stratum_height / stratum.split) * cbm::M_IN_MM;
            depth_sl[sl] = h_sl[sl];
            if ( sl > 0)
            {
                depth_sl[sl] += depth_sl[sl-1];
            }


            /* CLAY AND SAND CONTENT */
            lerr_t rc_texture = assign_soillayer_clay_and_sand_content( clay_sl[sl], sand_sl[sl], st, depth_sl[sl], s_site, s_soillayers, s_soilparameters, sl == sl_st0);
            if ( rc_texture)
            {
                LOGERROR( "Initialization of clay/sand content of stratum ", st+1, " not successfull");
                return  LDNDC_ERR_FAIL;
            }
            /* get soil type depending on clay and sand, which is used for gapfilling of remaining soil properties */
            cbm::string_t texture_name( ldndc::get_soil_texture( sand_sl[sl] * 100.0, clay_sl[sl] * 100.0));
            soil_type_e texture_type( ldndc::get_soil_texture_type( texture_name));

            if ( ldndc::use_usda_soil_texture_triangle( s_soillayers->soil_type()) &&
                 (texture_type != ldndc::soillayers::SOIL_NONE))
            {
                STRATUMINFO( "Determined soil type of stratum ", st+1, ": ", ldndc::get_soil_texture( sand_sl[sl], clay_sl[sl]));
            }
            else
            {
                STRATUMINFO( "Determined soil type of stratum ", st+1, ": ", ldndc::soillayers::SOIL_NAMES[s_soillayers->soil_type()]);
            }

            /* STONE FRACTION */
            lerr_t rc_stone = assign_soillayer_stone_fraction( stone_frac_sl[sl], st, depth_sl[sl], s_site, s_soillayers, s_soilparameters, sl == sl_st0);
            if ( rc_stone)
            {
                LOGERROR( "Initialization of stone fraction of stratum ", st+1, " not successfull");
                return  LDNDC_ERR_FAIL;
            }


            /* PH */
            if ( cbm::flt_greater_zero( stratum.ph))
            {
                ph_sl[sl] = stratum.ph;
                phi_sl[sl] = stratum.ph;
            }
            else
            {
                if ( forest_floor( sl, *s_soillayers, *s_site))
                {
                    ph_sl[sl] = s_soilparameters->HUMUS_PH( s_soillayers->humus_type());
                    phi_sl[sl] = s_soilparameters->HUMUS_PH( s_soillayers->humus_type());
                }
                else
                {
                    if ( ldndc::use_usda_soil_texture_triangle( s_soillayers->soil_type()) &&
                         (texture_type != ldndc::soillayers::SOIL_NONE))
                    {
                        ph_sl[sl] = s_soilparameters->SOIL_PH( texture_type);
                        phi_sl[sl] = s_soilparameters->SOIL_PH( texture_type);
                    }
                    else
                    {
                        ph_sl[sl] = s_soilparameters->SOIL_PH( s_soillayers->soil_type());
                        phi_sl[sl] = s_soilparameters->SOIL_PH( s_soillayers->soil_type());
                    }
                }
                STRATUMINFO( "pH value of stratum ", st+1, " not initialized, estimated value: ", ph_sl[sl]," [-]");
            }
            if ( !cbm::flt_in_range_lu( 1.0, ph_sl[sl], 14.0))
            {
                LOGERROR( "pH value of stratum ", st+1, " out of range: ", ph_sl[sl]," [-]");
                return  LDNDC_ERR_FAIL;
            }


            /* SKS */
            if ( cbm::flt_greater_equal_zero( stratum.sks))
            {
                sks_sl[sl] = stratum.sks;
            }
            else
            {
                if ( forest_floor( sl, *s_soillayers, *s_site))
                {
                    sks_sl[sl] = s_soilparameters->HUMUS_SKS( s_soillayers->humus_type());
                }
                else
                {
                    if ( ldndc::use_usda_soil_texture_triangle( s_soillayers->soil_type()) &&
                         (texture_type != ldndc::soillayers::SOIL_NONE))
                    {
                        sks_sl[sl] = s_soilparameters->SOIL_SKS( texture_type);
                    }
                    else
                    {
                        sks_sl[sl] = s_soilparameters->SOIL_SKS( s_soillayers->soil_type());
                    }
                }
                STRATUMINFO( "Soil hydraulic conductivity of stratum ", st+1, " not initialized, estimated value: ", sks_sl[sl], " [cm min-1]");
            }
            if ( !cbm::flt_in_range_lu( 0.0, sks_sl[sl], 10.0))
            {
                LOGERROR( "Soil hydraulic conductivity of stratum ", st+1, " out of range: ", sks_sl[sl]," [cm min-1]");
                return  LDNDC_ERR_FAIL;
            }


            /* BULK DENSITY */
            /*!
             *  Internally used bulk density \f$ \rho \f$ includes
             *  bulk density of the soil without stones \f$ \rho^{\ast} \f$
             *  and bulk density of stones \f$ \rho_{min} \f$: \n\n
             *  \f$ \rho = \rho^{\ast} (1 - x_{stone}) + \rho_{min} x_{stone} \f$ \n\n
             */
            double bulk_density_without_stones( 0.0);
            lerr_t rc_bd = assign_soillayer_bulk_density( bulk_density_without_stones, st, depth_sl[sl], s_site, s_soillayers, s_soilparameters, sl == sl_st0);
            if ( rc_bd)
            {
                LOGERROR( "Initialization of bulk density not successfull");
                return  LDNDC_ERR_FAIL;
            }
            dens_sl[sl] = (1.0 - stone_frac_sl[sl]) * bulk_density_without_stones + stone_frac_sl[sl] * cbm::DMIN;
            if ( !cbm::flt_in_range_lu( 0.0, dens_sl[sl], 10.0))
            {
                LOGERROR( "Soil bulk density of stratum ", st+1, " out of range: ", dens_sl[sl]," [kg dm-3]");
                return  LDNDC_ERR_FAIL;
            }


            /* IRON */
            double fe_tot_without_stones( 0.0);
            if ( cbm::flt_greater_equal_zero( stratum.iron))
            {
                fe_tot_without_stones = stratum.iron;
            }
            else
            {
                if ( forest_floor( sl, *s_soillayers, *s_site))
                {
                    fe_tot_without_stones = s_soilparameters->HUMUS_IRON( s_soillayers->humus_type());
                }
                else
                {
                    if ( ldndc::use_usda_soil_texture_triangle( s_soillayers->soil_type()) &&
                         (texture_type != ldndc::soillayers::SOIL_NONE))
                    {
                        fe_tot_without_stones = s_soilparameters->SOIL_IRON( texture_type);
                    }
                    else
                    {
                        fe_tot_without_stones = s_soilparameters->SOIL_IRON( s_soillayers->soil_type());
                    }
                }
                STRATUMINFO( "Iron content of stratum ",st+1," not initialized, estimated value: ", fe_tot_without_stones," [g g-1]");
            }
            if ( cbm::flt_greater( fe_tot_without_stones, 1.0))
            {
                LOGERROR( "Iron content of stratum ",st+1," out of range: ", fe_tot_without_stones," [g g-1]");
                return  LDNDC_ERR_FAIL;
            }
            else
            {
                fe2_sl[sl] = 0.5 * fe_tot_without_stones * get_bulk_density_without_stones( sl) * cbm::DM3_IN_M3 * h_sl[sl] * (1.0 - stone_frac_sl[sl]);
                fe3_sl[sl] = 0.5 * fe_tot_without_stones * get_bulk_density_without_stones( sl) * cbm::DM3_IN_M3 * h_sl[sl] * (1.0 - stone_frac_sl[sl]);
            }
            if ( !cbm::flt_in_range_lu( 0.0, fe_tot_without_stones, 1.0))
            {
                LOGERROR( "Iron content of stratum ", st+1, " out of range: ", fe_tot_without_stones," [g g-1]");
                return  LDNDC_ERR_FAIL;
            }


            /* ORGANIC CARBON AND NITROGEN CONTENT */
            /*!
             *  Internally used organic carbon and nitrogen contents include stones: \n\n
             *  \f$ c_{org} = c_{org}^{\ast} (1 - x_{stone}) \frac{\rho}{\rho^{\ast}} \f$ \n\n
             */
            double c_org_without_stones( 0.0);
            double n_without_stones( 0.0);
            double cn_old( (sl >0) ? cnr_sl[sl-1] : invalid_dbl);
            double depth_old( (sl >0) ? depth_sl[sl-1] : invalid_dbl);
            lerr_t rc_cn = assign_soillayer_organic_carbon_and_total_nitrogen_content( cn_old, c_org_without_stones, n_without_stones, st, depth_sl[sl], depth_old,
                                                                                       s_site, s_soillayers, s_soilparameters, sl == sl_st0);
            if ( rc_cn)
            {
                LOGERROR( "Initialization of organic carbon and total nitrogen content of stratum ", st+1, " not successfull");
                return  LDNDC_ERR_FAIL;
            }
            fcorg_sl[sl] = c_org_without_stones * (1.0 - stone_frac_sl[sl]) * get_bulk_density_without_stones( sl) / dens_sl[sl];
            cnr_sl[sl] = c_org_without_stones / n_without_stones;
            if ( !cbm::flt_in_range_lu( 0.0, fcorg_sl[sl], 1.0))
            {
                LOGERROR( "Organic carbon content of stratum ", st+1, " out of range: ", fcorg_sl[sl]," [g g-1]");
                return  LDNDC_ERR_FAIL;
            }
            if ( !cbm::flt_in_range_lu( 0.0, n_without_stones, 1.0))
            {
                LOGERROR( "Total nitrogen content of stratum ", st+1, " out of range: ", n_without_stones," [g g-1]");
                return  LDNDC_ERR_FAIL;
            }


            /* POROSITY */
            double porosity_without_stones( 0.0);
            lerr_t rc_po = ldndc::assign_soillayer_porosity( porosity_without_stones, st, depth_sl[sl], c_org_without_stones, bulk_density_without_stones,
                                                             s_site, s_soillayers, s_soilparameters, sl == sl_st0);
            if ( rc_po)
            {
                LOGERROR( "Initialization of soil porosity of stratum ", st+1, " not successfull");
                return  LDNDC_ERR_FAIL;
            }
            poro_sl[sl] = porosity_without_stones * (1.0 - stone_frac_sl[sl]);


            /* VAN GENUCHTEN PARAMETERS */
            bool vg_parameter_from_input( true);
            lerr_t rc_vg = assign_soillayer_van_genuchten_parameter( vga_sl[sl], vgn_sl[sl], vgm_sl[sl], vg_parameter_from_input,
                                                                     st, depth_sl[sl], s_site, s_soillayers, s_soilparameters, sl == sl_st0);
            if ( rc_vg)
            {
                LOGERROR( "Initialization of van Genuchten parameters of stratum ", st+1, " not successfull");
                return  LDNDC_ERR_FAIL;
            }


            /* MAXIMUM / MINIMUM WATER FILLED PORE SPACE */
            lerr_t rc_wfps = assign_soillayer_wfps_max_and_min( wfps_max_sl[sl], wfps_min_sl[sl], poro_sl[sl],
                                                                st, depth_sl[sl], c_org_without_stones, bulk_density_without_stones,
                                                                s_site, s_soillayers, s_soilparameters, sl == sl_st0);
            if ( rc_wfps)
            {
                LOGERROR( "Initialization of maximum/minimum water filled pore space of stratum ", st+1, " not successfull");
                return  LDNDC_ERR_FAIL;
            }


            /* FIELD CAPACITY AND WILTING POINT */
            double wcmax_without_stones( 0.0);
            double wcmin_without_stones( 0.0);
            lerr_t rc_wc = assign_soillayer_field_capacity_and_wilting_point(
                                                                 wcmax_without_stones, wcmin_without_stones,
                                                                 wfps_max_sl[sl], wfps_min_sl[sl], porosity_without_stones,
                                                                 st, depth_sl[sl], s_site, s_soillayers, s_soilparameters,
                                                                 cbm::is_equal( wr_type.c_str(), "vangenuchten"), sl == sl_st0);
            if ( rc_wc)
            {
                LOGERROR( "Initialization of field capacity and wilting point of stratum ", st+1, " not successfull");
                return  LDNDC_ERR_FAIL;
            }
            wcmax_sl[sl] = wcmax_without_stones * (1.0 - stone_frac_sl[sl]);
            wcmin_sl[sl] = wcmin_without_stones * (1.0 - stone_frac_sl[sl]);


            /*!
             * Hydraulic parameters are checked for consistency against each other.
             */
            double macropores_without_stones( 0.0);
            if ( cbm::is_valid( stratum.macropores))
            {
                macropores_without_stones = stratum.macropores;
            }
            else
            {
                if ( forest_floor( sl, *s_soillayers, *s_site))
                {
                    macropores_without_stones = s_soilparameters->HUMUS_MACROPORES( s_soillayers->humus_type());
                }
                else
                {
                    macropores_without_stones = s_soilparameters->SOIL_MACROPORES( s_soillayers->soil_type());
                }
            }

            double const macropores( macropores_without_stones * (1.0 - stone_frac_sl[sl]));
            if( cbm::flt_greater( wcmax_sl[sl] + macropores, poro_sl[sl]))
            {
                poro_sl[sl] = wcmax_sl[sl] + macropores;
                STRATUMINFO( "Field capacity exceeded porosity minus macropores in stratum ", st+1,". Porosity changed to: ", poro_sl[sl]," [m3 m-3]")
                if( cbm::flt_greater( poro_sl[sl], 1.0))
                {
                    LOGERROR( "Porosity of stratum ", st+1, " out of range: ", poro_sl[sl]," [m3 m-3]");
                    return  LDNDC_ERR_FAIL;
                }
            }

            if( cbm::flt_greater( wcmax_sl[sl], wfps_max_sl[sl] * poro_sl[sl]))
            {
                wfps_max_sl[sl] = cbm::bound(0.0, wcmax_sl[sl] / poro_sl[sl], 1.0);
                STRATUMINFO( "Field capacity exceeded maximum water filled pore space in stratum ", st+1, ". Maximum water filled pore space changed to: ",wfps_max_sl[sl], " [m3 m-3]")
            }

            if( cbm::flt_greater( wfps_min_sl[sl] * poro_sl[sl], wcmin_sl[sl]))
            {
                wfps_min_sl[sl] = cbm::bound(0.0, wcmin_sl[sl] / poro_sl[sl], wfps_max_sl[sl]);
                STRATUMINFO( "Minimum water filled pore space exceeded wilting point in stratum ", st+1,". Minimum water filled pore space changed to: ", wfps_min_sl[sl], " [m3 m-3]")
            }
            
            if ( cbm::is_valid( stratum.nh4_init))
            {
                nh4_sl[sl] = stratum.nh4_init * cbm::KG_IN_MG * get_bulk_density_without_stones( sl) * cbm::DM3_IN_M3 * h_sl[sl] * (1.0 - stone_frac_sl[sl]);
                clay_nh4_sl[sl] = 0.0;
            }
            else
            {
                nh4_sl[sl] = invalid_dbl;
                clay_nh4_sl[sl] = invalid_dbl;
            }

            if ( cbm::is_valid( stratum.no3_init))
            {
                no3_sl[sl] = stratum.no3_init * 0.5 * cbm::KG_IN_MG * get_bulk_density_without_stones( sl) * cbm::DM3_IN_M3 * h_sl[sl] * (1.0 - stone_frac_sl[sl]);
                an_no3_sl[sl] = stratum.no3_init * 0.5 * cbm::KG_IN_MG * get_bulk_density_without_stones( sl) * cbm::DM3_IN_M3 * h_sl[sl] * (1.0 - stone_frac_sl[sl]);
            }
            else
            {
                no3_sl[sl] = invalid_dbl;
                an_no3_sl[sl] = invalid_dbl;
            }
            
            if ( cbm::is_valid( stratum.doc_init))
            {
                doc_sl[sl] = stratum.doc_init * 0.5 * cbm::KG_IN_MG * get_bulk_density_without_stones( sl) * cbm::DM3_IN_M3 * h_sl[sl] * (1.0 - stone_frac_sl[sl]);
                an_doc_sl[sl] = stratum.doc_init * 0.5 * cbm::KG_IN_MG * get_bulk_density_without_stones( sl) * cbm::DM3_IN_M3 * h_sl[sl] * (1.0 - stone_frac_sl[sl]);
            }
            else
            {
                doc_sl[sl] = invalid_dbl;
                an_doc_sl[sl] = invalid_dbl;
            }
            
            if ( cbm::is_valid( stratum.don_init))
            {
                don_sl[sl] = stratum.don_init;
            }
            else
            {
                don_sl[sl] = invalid_dbl;
            }
        }
        sl_st0 = sl_st1;
        st_depth_start = st_depth_end;
    }

    return  LDNDC_ERR_OK;
}



/*!
 * @brief
 *  More class member initialization.
 *
 * @todo
 *  Move to DNDC soilchemistry model.
 *
 */
lerr_t
ldndc::substate_soilchemistry_t::initialize_pools_(
        cbm::io_kcomm_t *  _io_comm)
{
    airchemistry::input_class_airchemistry_t const *  s_airchemistry = S_AIRCHEMISTRY;
    if ( !s_airchemistry)
    {
        LOGERROR( "missing input  [class=airchemistry]");
        return LDNDC_ERR_FAIL;
    }
    siteparameters::input_class_siteparameters_t const *  s_siteparameters = S_SITEPARAMETERS;
    if ( !s_siteparameters)
    {
        LOGERROR( "missing input  [class=siteparameters]");
        return LDNDC_ERR_FAIL;
    }
    soillayers::input_class_soillayers_t const *  s_soillayers = S_SOILLAYERS;
    if ( !s_soillayers)
    {
        LOGERROR( "missing input  [class=soillayers]");
        return LDNDC_ERR_FAIL;
    }
    
    // organic carbon fractions
    lerr_t  csci_rc = initialize_soil_carbon_( _io_comm);
    RETURN_IF_NOT_OK(csci_rc);
    
    // soluted forms of nitrogen and carbon
    for ( size_t  sl = 0; sl < s_soillayers->soil_layer_cnt();  sl++)
    {
        //assumed homogeneous initial conditions for temperature and air filled pore space
        double const temperature( S_CLIMATE->annual_temperature_average());
        double const air_filled_porespace( 0.1);

        n2_sl[sl] = (cbm::PN2 * cbm::BAR_IN_PA) / (cbm::RGAS * (temperature + cbm::D_IN_K)) * 2.0 * cbm::MN * cbm::KG_IN_G * air_filled_porespace * h_sl[sl];
        o2_sl[sl] = std::exp( -2.0 * depth_sl[sl]) * cbm::PO2;

        if ( !cbm::is_valid( doc_sl[sl])) { doc_sl[sl] = 0.0; }
        if ( !cbm::is_valid( an_doc_sl[sl])) { an_doc_sl[sl] = 0.0; }
        if ( !cbm::is_valid( don_sl[sl])) { don_sl[sl] = 0.0; }
        if ( !cbm::is_valid( nh4_sl[sl])) { nh4_sl[sl] = 0.0; }
        if ( !cbm::is_valid( clay_nh4_sl[sl])) { clay_nh4_sl[sl] = 0.0; }
        if ( !cbm::is_valid( no3_sl[sl])) { no3_sl[sl] = 0.0; }
        if ( !cbm::is_valid( an_no3_sl[sl])) { an_no3_sl[sl] = 0.0; }

        co2_sl[sl]              = 0.0;
        urea_sl[sl]             = 0.0;
        n2o_sl[sl]              = 0.0;
        an_n2o_sl[sl]           = 0.0;
        no2_sl[sl]              = 0.0;
        an_no2_sl[sl]           = 0.0;
        no_sl[sl]               = 0.0;
        an_no_sl[sl]            = 0.0;
        nh3_liq_sl[sl]          = 0.0;
        nh3_gas_sl[sl]          = 0.0;

        // activity  microbial per soil layer
        micro1_act_sl[sl] = 0.1;
        micro2_act_sl[sl] = 0.1;
        micro3_act_sl[sl] = 0.1;

        anvf_sl[sl] = 0.0;
    }

    for ( size_t  sl = 0;  sl < s_soillayers->soil_layer_cnt();  sl++)
    {
        // organic soil fraction (kgDW m-2)
        double const  orgf( fcorg_sl[sl] / cbm::CCORG);
        min_sl[sl] = (1.0 - orgf) * dens_sl[sl] * cbm::DM3_IN_M3 * h_sl[sl];
        som_sl[sl] = orgf * dens_sl[sl] * cbm::DM3_IN_M3 * h_sl[sl];

        double const  soc( som_sl[sl] * cbm::CCORG);
        C_hum_sl[sl] *= soc;

        C_lit1_sl[sl] *= soc;
        C_lit2_sl[sl] *= soc;
        C_lit3_sl[sl] *= soc;
        
        C_micro1_sl[sl] *= soc;
        C_micro2_sl[sl] *= soc;
        C_micro3_sl[sl] *= soc;

        C_aorg_sl[sl] *= soc;

        N_lit1_sl[sl] = (C_lit1_sl[sl] / s_siteparameters->RCNRVL());
        N_lit2_sl[sl] = (C_lit2_sl[sl] / s_siteparameters->RCNRL());
        N_lit3_sl[sl] = (C_lit3_sl[sl] / s_siteparameters->RCNRR());
        
        N_micro_sl[sl] = (C_micro1_sl[sl] + C_micro2_sl[sl] + C_micro3_sl[sl]) / s_siteparameters->RCNB();
        N_hum_sl[sl] = (C_hum_sl[sl] / cn_hum_sl[sl]);
    }

    return  LDNDC_ERR_OK;
}



/*!
 * @brief
 *  Soil carbon initialization.
 *
 * @todo
 *  Move to DNDC soilchemistry model.
 *
 */
lerr_t
ldndc::substate_soilchemistry_t::initialize_soil_carbon_(
        cbm::io_kcomm_t *  _io_comm)
{
    site::input_class_site_t const *  s_site = S_SITE;
    siteparameters::input_class_siteparameters_t const *  s_siteparameters = S_SITEPARAMETERS;
    soillayers::input_class_soillayers_t const *  s_soillayers = S_SOILLAYERS;
    ldndc_assert( s_site && s_siteparameters && s_soillayers);
            
    lvector_t< double, 1 > &  fC_hum_sl = C_hum_sl;
    lvector_t< double, 1 > &  fC_lit1_sl = C_lit1_sl;
    lvector_t< double, 1 > &  fC_lit2_sl = C_lit2_sl;
    lvector_t< double, 1 > &  fC_lit3_sl = C_lit3_sl;
    lvector_t< double, 1 > &  fC_aorg_sl = C_aorg_sl;
    lvector_t< double, 1 > &  fC_micro1_sl = C_micro1_sl;
    lvector_t< double, 1 > &  fC_micro2_sl = C_micro2_sl;
    lvector_t< double, 1 > &  fC_micro3_sl = C_micro3_sl;
        
    double const orgSum( fcorg_sl.sum());
    double orgCum( 0.0);
    
    for ( size_t  sl = 0;  sl < s_soillayers->soil_layer_cnt();  ++sl)
    {
        orgCum += ( 0.5 * fcorg_sl[sl]);
        
        double const fl_1( 1.0e-5);
        double const fl_2( 3.0e-3);
        double const fl_3( 7.0e-3);
        double const fl_sum( fl_1 + fl_2 + fl_3);
        
        double const cn_aorg( 1.0 / (s_siteparameters->RBO() / s_siteparameters->RCNB() + (1.0-s_siteparameters->RBO()) / s_siteparameters->RCNH()));
        cn_hum_sl[sl] = (0.97 * cnr_sl[sl]);
        
        fC_micro1_sl[sl] = 2.0e-4;
        fC_micro2_sl[sl] = 5.0e-6;
        fC_micro3_sl[sl] = 1.0e-6;
        double const fC_micro( fC_micro1_sl[sl] + fC_micro2_sl[sl] + fC_micro3_sl[sl]);
        
        if ( (s_site->soil_use_history() == ECOSYS_ARABLE) || (s_site->soil_use_history() == ECOSYS_GRASSLAND))
        {
            if ( depth_sl[sl] < 0.5)
            {
                fC_lit1_sl[sl] = 0.005;
                fC_lit2_sl[sl] = 0.01;
                fC_lit3_sl[sl] = 0.005;
                fC_aorg_sl[sl] = 0.015;
            }
            else if ( depth_sl[sl] < 1.0)
            {
                cn_hum_sl[sl] = (0.98 * cnr_sl[sl]);
                fC_lit1_sl[sl] = 0.0005;
                fC_lit2_sl[sl] = 0.001;
                fC_lit3_sl[sl] = 0.0005;
                fC_aorg_sl[sl] = std::min((1.0 - fC_micro) * (1.0 - orgCum/orgSum) * 0.1, 0.015);
            }
            else
            {
                cn_hum_sl[sl] = (0.99 * cnr_sl[sl]);
                fC_lit1_sl[sl] = 0.0;
                fC_lit2_sl[sl] = 0.0;
                fC_lit3_sl[sl] = 0.0;
                fC_aorg_sl[sl] = std::min((1.0 - fC_micro) * (1.0 - orgCum/orgSum) * 0.01, 0.0015);
            }
            fC_hum_sl[sl]  = (1.0 - fC_micro - fC_aorg_sl[sl] - fC_lit1_sl[sl] - fC_lit2_sl[sl] - fC_lit3_sl[sl]);
        }
        else
        {    // forest option
            fC_lit1_sl[sl] = 0.0001;
            fC_lit2_sl[sl] = 0.0002;
            fC_lit3_sl[sl] = 0.0001;
            fC_aorg_sl[sl] = ((1.0 - fC_micro) * (1.0 - orgCum/orgSum) * 0.1);
            fC_hum_sl[sl]  = (1.0 - fC_micro - fC_aorg_sl[sl]);
        }
        
        // stepwise incrementing carbon fractions until the actual cn ratio is achieved
        int maxit( 1e6);
        double rss0( 1.0e9);
        for (;;)
        {
            // new CN ratio based on estimated fractions
            double const cnProb( 1.0 / (fC_micro / s_siteparameters->RCNB()
                                        + fC_lit1_sl[sl] / s_siteparameters->RCNRVL()
                                        + fC_lit2_sl[sl] / s_siteparameters->RCNRL()
                                        + fC_lit3_sl[sl] / s_siteparameters->RCNRR()
                                        + fC_hum_sl[sl]  / cn_hum_sl[sl]
                                        + fC_aorg_sl[sl] / cn_aorg));
            
            // adjustment of cn ratio by variation of litter pools
            if (cnProb < cnr_sl[sl])
            {
                fC_lit3_sl[sl] += (INCR * fl_3/fl_sum);
                fC_lit2_sl[sl] += (INCR * fl_2/fl_sum);
                fC_lit1_sl[sl] += (INCR * fl_1/fl_sum);
                fC_hum_sl[sl]  -= INCR;
            }
            else
            {
                fC_lit3_sl[sl] -= (INCR * fl_3/fl_sum);
                fC_lit2_sl[sl] -= (INCR * fl_2/fl_sum);
                fC_lit1_sl[sl] -= (INCR * fl_1/fl_sum);
                fC_hum_sl[sl]  += INCR;
            }
                        
            // coherence of new measured/modeled CN ratio
            double const rss( (cnProb - cnr_sl[sl]) * (cnProb - cnr_sl[sl]));
            if (rss < 0.01)
            {
                // break out of loop if fit satisfies
                break;
            }
            else if (rss > rss0 || maxit == 1)
            {
                LOGWARN( "quality criteria for CN ratio adjustment not reached in layer ",sl+1,", ", cnProb);
                break;
            }
            
            rss0 = rss;
            maxit--;
        }
        
        orgCum += 0.5 * fcorg_sl[sl];
    }
    
    return  LDNDC_ERR_OK;
}

double
ldndc::substate_soilchemistry_t::get_bulk_density_without_stones( size_t _sl) const
{
    return (dens_sl[_sl] - stone_frac_sl[_sl] * cbm::DMIN) / (1.0 - stone_frac_sl[_sl]);
}

double
ldndc::substate_soilchemistry_t::get_organic_carbon_content_without_stones( size_t _sl) const
{
    return fcorg_sl[_sl] / (1.0 - stone_frac_sl[_sl]) * dens_sl[_sl] / get_bulk_density_without_stones( _sl);
}

/*!
 * @brief
 *  Returns soil mass of respective soillayer.
 *  @param[in] _sl Soil layer
 *  @return Soil layer mass [1e3:g]
 */
double
ldndc::substate_soilchemistry_t::get_soil_mass( size_t _sl) const
{
    return dens_sl[_sl] * cbm::DM3_IN_M3 * h_sl[_sl];
}

double
ldndc::substate_soilchemistry_t::get_accumulated_c_net_flux() const
{
    return accumulated_c_fix_algae + accumulated_c_fertilizer
        + accumulated_c_litter_stubble + accumulated_c_litter_wood_above
        + accumulated_c_litter_below_sl.sum() + accumulated_c_litter_wood_below_sl.sum()
        - accumulated_ch4_emis - accumulated_co2_emis_hetero
        - accumulated_ch4_leach - accumulated_doc_leach;
}

#define  SIZE_DIM_S  (uintptr_t)(S_SOILLAYERS->soil_layer_cnt())

lerr_t
ldndc::substate_soilchemistry_t::resize_entities(
        cbm::io_kcomm_t *  _io_comm)
{
    return  g_resize_( _io_comm);
}

#include  "substate/mbe_soilchemistry.cpp.inc"

#undef  S_AIRCHEMISTRY
#undef  S_SITE
#undef  S_SITEPARAMETERS
#undef  S_SOILLAYERS
#undef  SIZE_DIM_S

#ifdef  _HAVE_SERIALIZE
int
ldndc::substate_soilchemistry_t::create_checkpoint(
        substate_checkpoint_write_context_t *  _cp_cxt)
{
    return  g_mobile_substate_write_members_s( this, _cp_cxt);
}
int
ldndc::substate_soilchemistry_t::restore_checkpoint(
        substate_checkpoint_read_context_t *  _cp_cxt)
{
    return  g_mobile_substate_read_members_s( this, _cp_cxt);
}
#endif /* _HAVE_SERIALIZE */

#ifdef  _LDNDC_HAVE_ONLINECONTROL
#include  "comm/lmessage.h"
int
ldndc::substate_soilchemistry_t::process_request(
        lreply_t *  _reply, lrequest_t const *  _request)
{
    if ( _request->command == "GET")
    {
        if ( cbm::is_equal( _request->uri.scheme(), "inproc"))
        {
            state_request_inproc_t  inproc_query;
            return  g_mobile_substate_get_member_s(
                &inproc_query, _reply, _request->data.get_string( ":item:"), this);
        }
    }
    return  1;
}
#endif  /* _LDNDC_HAVE_ONLINECONTROL */

