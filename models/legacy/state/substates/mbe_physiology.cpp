/*!
 * @brief
 *    ...
 *
 * @author
 *    Steffen Klatt
 *    Edwin Haas
 */


#include  "state/substates/mbe_physiology.h"
#include  "physiology/ld_biomassdistribution.h"

#include  "input/setup/setup.h"
#include  "input/soillayers/soillayers.h"
#include  "input/species/species.h"

#include  "logging/cbm_logging.h"


MOBILE_SUBSTATE_OBJECT_DEFN(physiology)
ldndc::substate_physiology_t::substate_physiology_t()
        : MoBiLE_Substate()
{
    this->g_construct_();
}
ldndc::substate_physiology_t::substate_physiology_t(
                                            lid_t const &  _id,
                                            MoBiLE_PlantVegetation *)
                                            : MoBiLE_Substate( _id)
{
    this->g_construct_();
}


ldndc::substate_physiology_t::~substate_physiology_t()
{
    /* by definition no deallocations here ! */
}

#define  S_SETUP  _io_comm->get_input_class< setup::input_class_setup_t >()
#define  S_SOILLAYERS  _io_comm->get_input_class< soillayers::input_class_soillayers_t >()

#define  SIZE_DIM_A  (size_t)(MoBiLE_MaximumAgeClasses)
#define  SIZE_DIM_F  (size_t)(S_SETUP->canopylayers())
#define  SIZE_DIM_S  (size_t)(S_SOILLAYERS->soil_layer_cnt())

lerr_t
ldndc::substate_physiology_t::initialize(
        cbm::io_kcomm_t *  _io_comm,
        void *  /*_mem_offs*/)
{
    if ( this->is_initialized())
    {
        return  LDNDC_ERR_OK;
    }

    if ( !this->has_required_inputs_( _io_comm))
    {
        return  LDNDC_ERR_OBJECT_INIT_FAILED;
    }

    lerr_t  rc = this->resize_entities( _io_comm);
    RETURN_IF_NOT_OK(rc);

    rc = update_canopy_layers_height( NULL);
    if ( rc != LDNDC_ERR_OK)
    {
        return  rc;
    }
    
    this->set_initialized();
    return  LDNDC_ERR_OK;
}


bool
ldndc::substate_physiology_t::has_required_inputs_(
        cbm::io_kcomm_t *  _io_comm)
const
{
    if ( !S_SOILLAYERS)
    {
        LOGERROR( SUBSTATE_NAMES[substate_type]," substate requires input classes {",
                LDNDC_INAME(soillayers),
                "}");
        return  false;
    }

    return  true;
}
bool
ldndc::substate_physiology_t::set_required_inputs_(
        cbm::io_kcomm_t *  _io_comm)
{
    return  this->has_required_inputs_( _io_comm);
}


lerr_t
ldndc::substate_physiology_t::resize_entities(
         cbm::io_kcomm_t *  _io_comm)
{
    return  this->g_resize_( _io_comm);
}

#include  "substate/mbe_physiology.cpp.inc"

#undef  S_SOILLAYERS
#undef  SIZE_DIM_A
#undef  SIZE_DIM_F
#undef  SIZE_DIM_S

#ifdef  _HAVE_SERIALIZE
int
ldndc::substate_physiology_t::create_checkpoint(
        substate_checkpoint_write_context_t *  _cp_cxt)
{
    return  g_mobile_substate_write_members_s( this, _cp_cxt);
}
int
ldndc::substate_physiology_t::restore_checkpoint(
        substate_checkpoint_read_context_t *  _cp_cxt)
{
    return  g_mobile_substate_read_members_s( this, _cp_cxt);
}
#endif /* _HAVE_SERIALIZE */

#ifdef  _LDNDC_HAVE_ONLINECONTROL
#include  "comm/lmessage.h"
int
ldndc::substate_physiology_t::process_request(
        lreply_t *  _reply, lrequest_t const *  _request)
{
    if ( _request->command == "GET")
    {
        if ( cbm::is_equal( _request->uri.scheme(), "inproc"))
        {
            state_request_inproc_t  inproc_query;
            return  g_mobile_substate_get_member_s(
                &inproc_query, _reply, _request->data.get_string( ":item:"), this);
        }
    }
    return  1;
}
#endif  /* _LDNDC_HAVE_ONLINECONTROL */



/*!
 * @brief
 *      calculates number of foliage layer and
 *      respecitve heights equally sized
 *      across the canopy.
 */
lerr_t
ldndc::substate_physiology_t::update_canopy_layers_height( MoBiLE_PlantVegetation *_m_veg)
{
    size_t fl_cnt( MIN_FOLIAGE_LAYER);
    double h_canopy( cbm::H_FLMIN * MIN_FOLIAGE_LAYER);
    if ( _m_veg)
    {
        fl_cnt = _m_veg->canopy_layers_used();
        h_canopy = cbm::bound_min( h_canopy, _m_veg->canopy_height());
    }

    for ( size_t  fl = 0;  fl < fl_cnt;  ++fl)
    {
        h_fl[fl] = h_canopy / (double)fl_cnt;
    }
    for ( size_t  fl = fl_cnt; fl < h_fl.size();  ++fl)
    {
        h_fl[fl] = 0.0;
    }

    return  LDNDC_ERR_OK;
}
