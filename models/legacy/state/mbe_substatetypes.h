/*!
 * @brief
 *    enumeration values and name array
 *
 * @author 
 *    steffen klatt (created on: oct 27, 2012),
 *    edwin haas
 */

#ifndef  LDNDC_SUBSTATE_TYPES_H_
#define  LDNDC_SUBSTATE_TYPES_H_

enum  substate_type_e
{
    SUBSTATE_AIRCHEMISTRY /* airchemistry */, SUBSTATE_MICROCLIMATE /* microclimate */, SUBSTATE_PHYSIOLOGY /* physiology */, SUBSTATE_SOILCHEMISTRY /* soilchemistry */, SUBSTATE_SURFACEBULK /* surfacebulk */, SUBSTATE_WATERCYCLE /* watercycle */, SUBSTATE_CNT /* CNT */, SUBSTATE_NONE /* none */,

    __ldndc_substate_enum_airchemistry = SUBSTATE_AIRCHEMISTRY, 
    __ldndc_substate_enum_microclimate = SUBSTATE_MICROCLIMATE, 
    __ldndc_substate_enum_physiology = SUBSTATE_PHYSIOLOGY, 
    __ldndc_substate_enum_soilchemistry = SUBSTATE_SOILCHEMISTRY, 
    __ldndc_substate_enum_surfacebulk = SUBSTATE_SURFACEBULK, 
    __ldndc_substate_enum_watercycle = SUBSTATE_WATERCYCLE, 
    __ldndc_substate_enum_CNT = SUBSTATE_CNT, 
    __ldndc_substate_enum_none = SUBSTATE_NONE
};

extern char const *  SUBSTATE_NAMES[SUBSTATE_CNT+2];
extern char const *  SUBSTATE_NAMES_LONG[SUBSTATE_CNT+2];

/* bitmask value for substate type */
#define  LSUB_BMASK(__substate_e__)  (1u << __substate_e__)

#endif  /*  !LDNDC_SUBSTATE_TYPES_H_  */

