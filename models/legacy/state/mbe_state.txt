## vim: ft=sh
##
## format of this file
##
## <target> <id> <name> <type> (-|<size_1>:<size_2>:...:<size_r>) <initial value> <unit>
##
## target	currently this is the substate the item belongs to
##		e.g., "watercyle", "soilchemistry"
## id		unique identifier, allowed characters are a-z, A-Z and underscore (_)
##		if id is "-" the state item will be excluded from serialization (use carefully!)
## name		variable name used in code (naming follows C grammar rules)
## type		data type of variable (base type, i.e. pointer is derived from size, no references supported)
##
## size		colon separated list of dimension sizes of item (e.g vector: size_1=N)
##		internally these are prepended by some prefix and the resulting identifier
##		must be defined in the scope of the target substate (in general size N
##		means there is a marco <SIZE_PREFIX>N expanding to some statement
##		evaluating to (unsigned) int. note that for constants this works
##		analogous, e.g. size 2 means that a macro <SIZE_PREFIX>2 must be defined
##		as 2 (i.e. #define  <SIZE_PREFIX>2  2)
##
##		-commonly used dynamic size names (r is implicit rank of item):
##		  F := number of foliage layers
##		  S := number of soil layers
##
##		-scalars are identified by giving size "-"
## initial value	the value the item is set to after creating the item
## unit		a string describing the physical unit of the item (should follow strict rules)
##
##
## examples:
##
##	scalar (double  ts_airtemperature in microclimate)
##	 "microclimate  airtemperature  ts_airtemperature  double  -  0.0  oC  instantaneous ambient air temperature"
##
##	array/matrix
##      (double * som_sl)
##   "soilchemistry  soil_organic_matter  som_sl  double  S  0.0  g:10^3:m^-2"
##	and expects in its scope the identifier
##		SIZE_DIM_S
##	to be defined
##
##
## motivation:
##	this is sort of an attempt to tackle the problem of moving a complete
##	site state from A to B.
##	it also provides a mechanism to generate various state related routines
##	(note that these come for free and are by definiton always complete):
##
##	- allocation, initialization, deallocation
##	- serializing (i.e. full dump of the state)
##	- state item references by id (i.e. give name to each state item, think
##	  output control)
##	- starting point for unit checker (a program checking each statement for
##	  unit correctness)
##
##
## notes:
##	it is somewhat urgently important that all data types that are used do not
##	allocate memory on their own!!!
##

##############################
###                        ###
###     AIR  CHEMISTRY     ###
###                        ###
##############################

airchemistry  nd_no3_dry_deposition  nd_no3_dry_deposition  double  -  0.0  g:m^-2:d^-1
airchemistry  nd_nh4_dry_deposition  nd_nh4_dry_deposition  double  -  0.0  g:m^-2:d^-1

airchemistry  nd_no3_wet_deposition  nd_no3_wet_deposition  double  -  0.0  g:m^-3
airchemistry  nd_nh4_wet_deposition  nd_nh4_wet_deposition  double  -  0.0  g:m^-3

# instantaneous methane concentration
airchemistry  ts_ch4_concentration  ts_ch4_concentration  double  -  0.0  ppm
# methane concentration above the canopy
airchemistry  nd_ch4_concentration   nd_ch4_concentration  double  -  0.0  ppm
# CH4 concentration per canopy layer
airchemistry  ts_ch4_concentration_fl  ts_ch4_concentration_fl  double  F  0.0  ?
# CH4 concentration per canopy layer
airchemistry  nd_ch4_concentration_fl  nd_ch4_concentration_fl  double  F  0.0  ?

# instantaneous carbon dioxide concentration
airchemistry  ts_co2_concentration  ts_co2_concentration  double  -  0.0  ppm
# carbon dioxide concentration above the canopy
airchemistry  nd_co2_concentration   nd_co2_concentration  double  -  0.0  ppm
# CO2 concentration per canopy layer
airchemistry  ts_co2_concentration_fl  ts_co2_concentration_fl  double  F  0.0  ?
# CO2 concentration per canopy layer
airchemistry  nd_co2_concentration_fl  nd_co2_concentration_fl  double  F  0.0  ?

# instantaneous ammoniak concentration
airchemistry  ts_nh3_concentration  ts_nh3_concentration  double  -  0.0  ppm
# ammoniak concentration above the canopy
airchemistry  nd_nh3_concentration  nd_nh3_concentration  double  -  0.0  ppm
# NH3 concentration per canopy layer
airchemistry  ts_nh3_concentration_fl  ts_nh3_concentration_fl  double  F  0.0  ?
# ammoniak concentration above the canopy
airchemistry  nd_nh3_concentration_fl  nd_nh3_concentration_fl  double  F  0.0  ?

# instantaneous nitrous oxide concentration
airchemistry  ts_no_concentration  ts_no_concentration  double  -  0.0  ppm
# nitrous oxide concentration above the canopy
airchemistry  nd_no_concentration  nd_no_concentration  double  -  0.0  ppm
# NO concentration per canopy layer
airchemistry  ts_no_concentration_fl  ts_no_concentration_fl  double  F  0.0  ?
# NO concentration per canopy layer
airchemistry  nd_no_concentration_fl  nd_no_concentration_fl  double  F  0.0  ?

# instantaneous nitrous dioxide concentration
airchemistry  ts_no2_concentration   ts_no2_concentration  double  -  0.0  ppm
# nitrous dioxide concentration above the canopy
airchemistry  nd_no2_concentration  nd_no2_concentration  double  -  0.0  ppm
# NO2 concentration per canopy layer
airchemistry  ts_no2_concentration_fl  ts_no2_concentration_fl  double  F  0.0  ?
# NO2 concentration per canopy layer
airchemistry  nd_no2_concentration_fl  nd_no2_concentration_fl  double  F  0.0  ?

# instantaneous oxygen concentration
airchemistry  ts_o2_concentration  ts_o2_concentration  double  -  0.0  ppm
# oxygen concentration above the canopy
airchemistry  nd_o2_concentration  nd_o2_concentration  double  -  0.0  ppm

# instantaneous ozone concentration
airchemistry  ts_o3_concentration  ts_o3_concentration  double  -  0.0  ppm
# ozone concentration above the canopy
airchemistry  nd_o3_concentration  nd_o3_concentration  double  -  0.0  ppm
# ozone concentration per canopy layer
airchemistry  ts_o3_concentration_fl  ts_o3_concentration_fl  double  F  0.0  ?
# ozone concentration per canopy layer
airchemistry  nd_o3_concentration_fl  nd_o3_concentration_fl  double  F  0.0  ?

##############################
###                        ###
###      MICROCLIMATE      ###
###                        ###
##############################

# albedo
microclimate  albedo  albedo  double  -  0.0  %

# instantaneous (short wave) radiation above the canopy (W m-2)
microclimate  shortwaveradiation_in  shortwaveradiation_in  double  -  0.0  W:m^-2
# daily global radiation above the canopy (W m-2)
microclimate  nd_shortwaveradiation_in  nd_shortwaveradiation_in  double  -  0.0  W:m^-2

# incoming long wave radiation per timestep above the canopy (W m-2)
microclimate  longwaveradiation_in  longwaveradiation_in  double  -  0.0  W:m^-2
# daily incoming long wave radiation above the canopy (W m-2)
microclimate  nd_longwaveradiation_in  nd_longwaveradiation_in  double  -  0.0  W:m^-2

# outgoing long wave radiation per timestep above the canopy (W m-2)
microclimate  longwaveradiation_out  longwaveradiation_out  double  -  0.0  W:m^-2
# daily outgoing long wave radiation above the canopy (W m-2)
microclimate  nd_longwaveradiation_out  nd_longwaveradiation_out  double  -  0.0  W:m^-2

# energy/heat content of the ecosystem (J m-2)
microclimate  heat_content  heat_content  double  -  0.0  J:m^-2

# (W m-2)
microclimate  sensible_heat_out  sensible_heat_out  double  -  0.0  W:m^-2
# (W m-2)
microclimate  nd_sensible_heat_out  nd_sensible_heat_out  double  -  0.0  W:m^-2
    
# precipitation per timestep above the canopy (m)
microclimate  ts_precipitation  ts_precipitation  double  -  0.0  m
# daily precipitation sum above the canopy (m)
microclimate  nd_precipitation  nd_precipitation  double  -  0.0  m

# snowfall per timestep above the canopy (m)
microclimate  ts_snowfall  ts_snowfall  double  -  0.0  m
# daily precipitation sum above the canopy (m)
microclimate  nd_snowfall  nd_snowfall  double  -  0.0  m

# atmospheric pressure per timestep above the canopy (mbar)
microclimate  ts_airpressure  ts_airpressure  double  -  0.0  bar:10^-3
# daily average air pressure (mbar)
microclimate  nd_airpressure  nd_airpressure  double  -  0.0  bar:10^-3



# instantaneous temperature above the canopy (oC)
microclimate  ts_airtemperature  ts_airtemperature  double  -  0.0  oC
# daily average temperature above the canopy (oC)
microclimate  nd_airtemperature  nd_airtemperature  double  -  0.0  oC

# "instantaneous" maximum temperature above the canopy (oC)
microclimate  ts_maximumairtemperature  ts_maximumairtemperature  double  -  0.0  oC
# daily Maximum temperature above the canopy (oC)
microclimate  nd_maximumairtemperature  nd_maximumairtemperature  double  -  0.0  oC

# "instantaneous" minimum temperature above the canopy (oC)
microclimate  ts_minimumairtemperature  ts_minimumairtemperature  double  -  0.0  oC
# daily minimum temperature above the canopy (oC)
microclimate  nd_minimumairtemperature  nd_minimumairtemperature  double  -  0.0  oC

# instantaneous relative humidity [%]
microclimate  ts_relativehumidity  ts_relativehumidity  double  -  0.0  %
# daily relative humidity [%]
microclimate  nd_relativehumidity  nd_relativehumidity  double  -  0.0  %

# instantaneous vapor pressure deficit above the canopy (kPa)
microclimate  ts_watervaporsaturationdeficit  ts_watervaporsaturationdeficit  double  -  0.0  Pa:10^3
# daily vapor pressure deficit above the canopy (kPa)
microclimate  nd_watervaporsaturationdeficit  nd_watervaporsaturationdeficit  double  -  0.0  Pa:10^3

# instantaneous wind speed above the canopy (m s-1)
microclimate  ts_windspeed  ts_windspeed  double  -  0.0  m:s^-1
# daily average wind speed above the canopy (m s-1)
microclimate  nd_windspeed  nd_windspeed  double  -  0.0  m:s^-1

# reflected short wave radiation (W m-2)
microclimate  shortwaveradiation_out  shortwaveradiation_out  double  -  0.0  W:m^-2
# average daily reflected radiation (W m-2)
microclimate  nd_shortwaveradiation_out  nd_shortwaveradiation_out  double  -  0.0  W:m^-2

# radiation above litter (W m-2)
microclimate  rad_a  rad_a  double  -  0.0  W:m^-2
# sum of sub-daily radiation at litter (W m-2)
microclimate  nd_rad_a  nd_rad_a  double  -  0.0  W:m^-2

# radiation per canopy layer (W m-2)
microclimate  shortwaveradiation_fl shortwaveradiation_fl  double  F  0.0  W:m^-2
# sum of sub-daily radiation per canopy layer (W m-2)
microclimate  nd_shortwaveradiation_fl nd_shortwaveradiation_fl  double  F  0.0  W:m^-2

# downwards longwave radiation per canopy layer (W m-2)
microclimate  longwaveradiation_down_fl longwaveradiation_down_fl  double  F  0.0  W:m^-2
# upwards longwave radiation per canopy layer (W m-2)
microclimate  longwaveradiation_up_fl longwaveradiation_up_fl  double  F  0.0  W:m^-2
# sum of sub-daily downwards longwave radiation per canopy layer (W m-2)
microclimate  nd_longwaveradiation_down_fl nd_longwaveradiation_down_fl  double  F  0.0  W:m^-2
# sum of sub-daily upwards longwave radiation per canopy layer (W m-2)
microclimate  nd_longwaveradiation_up_fl nd_longwaveradiation_up_fl  double  F  0.0  W:m^-2



# average daily temperature above litter (oC)
microclimate  nd_temp_a  nd_temp_a  double  -  0.0  oC
# temperature above litter (equal to below vegetation) (oC)
microclimate  temp_a  temp_a  double  -  0.0  oC

# fraction of sunlit foliage per canopy layer
microclimate  ts_sunlitfoliagefraction_fl ts_sunlitfoliagefraction_fl  double  F  0.0  /
# fraction of sunlit foliage over the past 24 hours per canopy layer
microclimate  sunlitfoliagefraction24_fl  sunlitfoliagefraction24_fl  double  F  0.0  /
# fraction of sunlit foliage over the past 240 hours per canopy layer
microclimate  sunlitfoliagefraction240_fl  sunlitfoliagefraction240_fl  double  F  0.0  /
# temperature per canopy layer (oC) per time step
microclimate  temp_fl temp_fl  double  F  0.0  oC
# temperature per canopy layer (oC)
microclimate  tempOld_fl tempOld_fl  double  F  0.0  oC

# vapor pressure deficit per canopy layer (kPa)
microclimate  vpd_fl vpd_fl  double  F  0.0  Pa:10^3
# wind speed per canopy layer (m s-1)
microclimate  win_fl win_fl  double  F  0.0  m:s^-1
# minimum temperature per canopy layer (oC)
microclimate  tMin_fl tMin_fl  double  F  0.0  oC
# Maximum temperature per canopy layer (oC)
microclimate  tMax_fl tMax_fl  double  F  0.0  oC
# foliage temperature per canopy layer (oC)
microclimate  tFol_fl tFol_fl  double  F  0.0  oC
# average daily fraction of sunlit foliage per canopy layer
microclimate  nd_sunlitfoliagefraction_fl nd_sunlitfoliagefraction_fl  double  F  0.0  ?
# average daily temperature per canopy layer (oC)
microclimate  nd_temp_fl nd_temp_fl  double  F  0.0  oC

# average daily vapor pressure deficit per canopy layer
microclimate  nd_vpd_fl nd_vpd_fl  double  F  0.0  ?
# average daily wind speed per canopy layer (m s-1)
microclimate  nd_win_fl nd_win_fl  double  F  0.0  m:s^-1
# average daily maximum temperature per canopy layer
microclimate  nd_tMax_fl nd_tMax_fl  double  F  0.0  oC
# average daily minimum temperature per canopy layer
microclimate  nd_tMin_fl nd_tMin_fl  double  F  0.0  oC
# radiation regime/average over the last 24hours (W m-2)
microclimate  rad24_fl rad24_fl  double  F  0.0  W:m^-2
# radiation regime/averagfe over the last 10days (W m-2)
microclimate  rad240_fl rad240_fl  double  F  0.0  W:m^-2
# temperature regime/average over the last 24hours
microclimate  tFol24_fl tFol24_fl  double  F  0.0  oC
# temperature regime/average over the last 10days
microclimate  tFol240_fl tFol240_fl  double  F  0.0  oC
# temperature per soil layer (oC)
microclimate  temp_sl  temp_sl  double  S  0.0  oC
# used to be temp_sl[0]
microclimate  surface_temp  surface_temp  double  -  0.0  oC
# average daily temperature per soil layer (oC)
microclimate  nd_temp_sl  nd_temp_sl  double  S  0.0  oC
# used to be nd_temp_sl[0]
microclimate  nd_surface_temp  nd_surface_temp  double  -  0.0  oC
# absorbed radiation in the shaded fraction of the canopy layer (umol m-2 s-1)
microclimate  parshd_fl parshd_fl  double  F  0.0  mol:10^-6:m^-2:s^-1
# absorbed radiation over the past 24 hours in the shaded fraction of the canopy layer (umol m-2 s-1)
microclimate  parshd24_fl parshd24_fl  double  F  0.0  mol:10^-6:m^-2:s^-1
# absorbed radiation over the past 240 hours in the shaded fraction of the canopy layer (umol m-2 s-1)
microclimate  parshd240_fl parshd240_fl  double  F  0.0  mol:10^-6:m^-2:s^-1
# absorbed radiation in the sunlit fraction of the canopy layer (umol m-2 s-1)
microclimate  parsun_fl parsun_fl  double  F  0.0  mol:10^-6:m^-2:s^-1
# absorbed radiation over the past 24 hours in the sunlit fraction of the canopy layer (umol m-2 s-1)
microclimate  parsun24_fl parsun24_fl  double  F  0.0  mol:10^-6:m^-2:s^-1
# absorbed radiation over the past 240 hours in the sunlit fraction of the canopy layer (umol m-2 s-1)
microclimate  parsun240_fl parsun240_fl  double  F  0.0  mol:10^-6:m^-2:s^-1
# average daily radiation absorbed in the shaded fraction of the canopy layer (umol m-2 s-1)
microclimate  nd_parshd_fl nd_parshd_fl  double  F  0.0  mol:10^-6:m^-2:s^-1
# average daily radiation absorbed in the sunlit fraction of the canopy layer (umol m-2 s-1)
microclimate  nd_parsun_fl nd_parsun_fl  double  F  0.0  mol:10^-6:m^-2:s^-1

##############################
###                        ###
###       PHYSIOLOGY       ###
###                        ###
##############################

# soil layer specific daily summed ammonia uptake (kgN m-2).
physiology  accumulated_nh3_uptake_sl  accumulated_nh3_uptake_sl  double  S  0.0  g:10^3:m^-2
# soil layer specific daily summed ammonium uptake (kgN m-2).
physiology  accumulated_nh4_uptake_sl  accumulated_nh4_uptake_sl  double  S  0.0  g:10^3:m^-2
# soil layer specific daily summed nitrate uptake (kgN m-2).
physiology  accumulated_no3_uptake_sl  accumulated_no3_uptake_sl double  S  0.0  g:10^3:m^-2
# soil layer specific daily summed organic nitrogen uptake (kgN m-2).
physiology  accumulated_don_uptake_sl  accumulated_don_uptake_sl  double  S  0.0  g:10^3:m^-2
# isoprene emission from the canopy (umol m-2 ground).
physiology  ts_isoprene_emission  ts_isoprene_emission  double  -  0.0  mol:10^-6:m^-2
# monoterpene emission from the canopy (umol m-2 ground).
physiology  ts_monoterpene_emission  ts_monoterpene_emission  double  -  0.0  mol:10^-6:m^-2
# ovoc emission from the canopy (umol m-2 ground).
physiology  ts_ovoc_emission  ts_ovoc_emission  double  -  0.0  mol:10^-6:m^-2
# canopy layer daily summed specific ammoniak uptake (kgN m-2).
physiology  nd_nh3_uptake_fl  nd_nh3_uptake_fl  double  F  0.0  g:10^3:m^-2
# canopy layer specific ammoniak uptake (kgN m-2) per time step.
physiology  ts_nh3_uptake_fl  ts_nh3_uptake_fl  double  F  0.0  g:10^3:m^-2
# canopy layer daily summed specific nitrous oxide uptake (kgN m-2).
physiology  nd_nox_uptake_fl  nd_nox_uptake_fl  double  F  0.0  g:10^3:m^-2
# canopy layer specific nitrous oxide uptake (kgN m-2) per time step.
physiology  ts_nox_uptake_fl  ts_nox_uptake_fl  double  F  0.0  g:10^3:m^-2

# nh4 content in throughfall (kgN m2).
physiology  accumulated_nh4_throughfall  accumulated_nh4_throughfall  double  -  0.0  g:10^3:m^2
# no3 content in throughfall (kgN m2).
physiology  accumulated_no3_throughfall  accumulated_no3_throughfall  double  -  0.0  g:10^3:m^2

# n export by harvest (kgN m2).
physiology  accumulated_n_export_harvest  accumulated_n_export_harvest  double  -  0.0  g:10^3:m^2
# n export of fruit by harvest (kgN m2).
physiology  accumulated_n_fru_export_harvest  accumulated_n_fru_export_harvest  double  -  0.0  g:10^3:m^2
# c export by harvest (kgC m2).
physiology  accumulated_c_export_harvest  accumulated_c_export_harvest  double  -  0.0  g:10^3:m^2
# c export of fruit by harvest (kgC m2).
physiology  accumulated_c_fru_export_harvest  accumulated_c_fru_export_harvest  double  -  0.0  g:10^3:m^2

# n export by grazing (kgN m2).
physiology  accumulated_n_export_grazing  accumulated_n_export_grazing  double  -  0.0  g:10^3:m^2
# c export by grazing (kgN m2).
physiology  accumulated_c_export_grazing  accumulated_c_export_grazing  double  -  0.0  g:10^3:m^2

# height of foliage layers
physiology  height_foliage  h_fl  double  F  0.0  m

##############################
###                        ###
###     SURFACE BULK       ###
###                        ###
##############################

# temperature in bulk layers
surfacebulk  temp_sbl  temp_sbl  double  W  0.0  oC
# nh4 in bulk layers
surfacebulk  nh4_sbl  nh4_sbl  double  W  0.0  g:10^3:m^-2
# nh3 in bulk layers
surfacebulk  nh3_sbl  nh3_sbl  double  W  0.0  g:10^3:m^-2
# urea in bulk layers
surfacebulk  urea_sbl  urea_sbl  double  W  0.0  g:10^3:m^-2
# no3 in bulk layers
surfacebulk  no3_sbl  no3_sbl  double  W  0.0  g:10^3:m^-2
# don in bulk layers
surfacebulk  don_sbl  don_sbl  double  W  0.0  g:10^3:m^-2
# doc in bulk layers
surfacebulk  doc_sbl  doc_sbl  double  W  0.0  g:10^3:m^-2
# N2O in bulk layers
surfacebulk  n2o_sbl  n2o_sbl  double  W  0.0  g:10^3:m^-2
# NO in bulk layers
surfacebulk  no_sbl  no_sbl  double  W  0.0  g:10^3:m^-2
# co2 in bulk layers
surfacebulk  co2_sbl  co2_sbl  double  W  0.0  g:10^3:m^-2
# ch4 in bulk layers
surfacebulk  ch4_sbl  ch4_sbl  double  W  0.0  g:10^3:m^-2
# n2 in bulk layers
surfacebulk  n2_sbl  n2_sbl  double  W  0.0  g:10^3:m^-2
# o2 in bulk layers
surfacebulk  o2_sbl  o2_sbl  double  W  0.0  g:10^3:m^-2
# so4 in bulk layers
surfacebulk  so4_sbl  so4_sbl  double  W  0.0  g:10^3:m^-2
# controled release nitrogen fertilizer in bulk layers
surfacebulk  coated_nh4_sbl  coated_nh4_sbl  double  W  0.0  g:10^3:m^-2
# nitrification inhibitor in bulk layers
surfacebulk  ni_sbl  ni_sbl  double  W  0.0  g:10^3:m^-2
# urease inhibitor in bulk layers
surfacebulk  ui_sbl  ui_sbl  double  W  0.0  g:10^3:m^-2

##############################
###                        ###
###     SOIL CHEMISTRY     ###
###                        ###
##############################

# carbon in aboveground wood (kgC m-2).
soilchemistry  c_wood  c_wood  double  -  0.0  g:10^3:m^-2
# nitrogen in aboveground wood (kgN m-2).
soilchemistry  n_wood  n_wood  double  -  0.0  g:10^3:m^-2
# carbon in belowground wood (kgC m-2).
soilchemistry  c_wood_sl  c_wood_sl  double  S  0.0  g:10^3:m^-2
# nitrogen in belowground  wood (kgN m-2).
soilchemistry  n_wood_sl  n_wood_sl  double  S  0.0  g:10^3:m^-2
# black carbon (kgC m-2).
soilchemistry  black_carbon_sl  black_carbon_sl  double  S  0.0  g:10^3:m^-2
# o2 concentration per soil layer (bar).
soilchemistry  o2_sl  o2_sl  double  S  0.0  bar
# co2 concentration per soil layer (kgC).
soilchemistry  co2_sl  co2_sl  double  S  0.0  g:10^3
# anaerobic volumetric fraction.
soilchemistry  anvf_sl  anvf_sl  double  S  0.0  %
# urea-N content per soil layer (kgN m-2).
soilchemistry  urea_sl  urea_sl  double  S  0.0  g:10^3:m^-2
# don-N per soil layer (kgN m-2).
soilchemistry  don_sl  don_sl  double  S  0.0  g:10^3:m^-2
# nh4-N per soil layer (kgN m-2).
soilchemistry  nh4_sl  nh4_sl  double  S  0.0  g:10^3:m^-2
# liquid nh3-N per soil layer (kgN m-2).
soilchemistry  nh3_liq_sl  nh3_liq_sl  double  S  0.0  g:10^3:m^-2
# gaseous nh3-N per soil layer (kgN m-2).
soilchemistry  nh3_gas_sl  nh3_gas_sl  double  S  0.0  g:10^3:m^-2
# no3-N per soil layer (kgN m-2).
soilchemistry  no3_sl  no3_sl  double  S  0.0  g:10^3:m^-2
# no2-N per soil layer (kgN m-2).
soilchemistry  no2_sl  no2_sl  double  S  0.0  g:10^3:m^-2
# no-N per soil layer (kgN m-2).
soilchemistry  no_sl  no_sl  double  S  0.0  g:10^3:m^-2
# n2o-N per soil layer (kgN m-2).
soilchemistry  n2o_sl  n2o_sl  double  S  0.0  g:10^3:m^-2
# n2-N per soil layer (kgN m-2).
soilchemistry  n2_sl  n2_sl  double  S  0.0  g:10^3:m^-2
# no3 in the anaerobic balloon per soil layer (kgN m-2).
soilchemistry  an_no3_sl  an_no3_sl  double  S  0.0  g:10^3:m^-2
# no2 in the anaerobic balloon per soil layer (kgN m-2).
soilchemistry  an_no2_sl  an_no2_sl  double  S  0.0  g:10^3:m^-2
# no in the anaerobic balloon per soil layer (kgN m-2).
soilchemistry  an_no_sl  an_no_sl  double  S  0.0  g:10^3:m^-2
# n2o in the anaerobic balloon per soil layer (kgN m-2).
soilchemistry  an_n2o_sl  an_n2o_sl  double  S  0.0  g:10^3:m^-2
# clay adsorbed nh4 per soil layer (kgN m-2).
soilchemistry  clay_nh4_sl  clay_nh4_sl  double  S  0.0  g:10^3:m^-2
# controled release nitrogen fertilizer in bulk layers
soilchemistry  coated_nh4_sl  coated_nh4_sl  double  S  0.0  g:10^3:m^-2
# activity of heterotrophic nitrifier microbes per soil layer.
soilchemistry  micro1_act_sl  micro1_act_sl  double  S  0.0  %
# activity of denitrifier microbes per soil layer.
soilchemistry  micro2_act_sl  micro2_act_sl  double  S  0.0  %
# activity of autotrophic nitrifier microbes group 2 per soil layer.
soilchemistry  micro3_act_sl  micro3_act_sl  double  S  0.0  %
# carbon content of heterotrophic nitrifier microbes per soil layer (kgC m-2).
soilchemistry  C_micro1_sl  C_micro1_sl  double  S  0.0  g:10^3:m^-2
# carbon content of denitrifier microbes per soil layer (kgC m-2).
soilchemistry  C_micro2_sl  C_micro2_sl  double  S  0.0  g:10^3:m^-2
# carbon content of autotrophic nitrifier per soil layer (kgC m-2).
soilchemistry  C_micro3_sl  C_micro3_sl  double  S  0.0  g:10^3:m^-2
# first litter fraction per soil layer (kgC m-2).
soilchemistry  C_lit1_sl  C_lit1_sl  double  S  0.0  g:10^3:m^-2
# second litter fraction per soil layer (kgC m-2).
soilchemistry  C_lit2_sl  C_lit2_sl  double  S  0.0  g:10^3:m^-2
# third litter fraction per soil layer (kgC m-2).
soilchemistry  C_lit3_sl  C_lit3_sl  double  S  0.0  g:10^3:m^-2
# passive humus pool per soil layer (kgC m-2).
soilchemistry  C_hum_sl  C_hum_sl  double  S  0.0  g:10^3:m^-2
# carbon of active humus per soil layer (dead microbial tissue and humads) (kgC m-2).
soilchemistry  C_aorg_sl  C_aorg_sl  double  S  0.0  g:10^3:m^-2
# nitrogen of active humus per soil layer (dead microbial tissue and humads) (kgC m-2).
soilchemistry  N_aorg_sl  N_aorg_sl  double  S  0.0  g:10^3:m^-2
# soil organic matter concentration (kgDM m-2).
soilchemistry  som_sl  som_sl  double  S  0.0  g:10^3:m^-2
# mineral concentration in the soil (kgDM m-2).
soilchemistry  min_sl  min_sl  double  S  0.0  g:10^3:m^-2
# doc in anaerobic balloon per soil layer (kgC m-2).
soilchemistry  an_doc_sl  an_doc_sl  double  S  0.0  g:10^3:m^-2
# doc in aerobic air fraction per soil layer (kgC m-2).
soilchemistry  doc_sl  doc_sl  double  S  0.0  g:10^3:m^-2
# nitrogen of very labile litter per soil layer
soilchemistry  N_lit1_sl  N_lit1_sl  double  S  0.0  kg:m^-2
# nitrogen of labile litter per soil layer
soilchemistry  N_lit2_sl  N_lit2_sl  double  S  0.0  kg:m^-2
# nitrogen of recalcitrant litter per soil layer
soilchemistry  N_lit3_sl  N_lit3_sl  double  S  0.0  kg:m^-2
# total nitrogen in humus (kgN m-2).
soilchemistry  N_hum_sl  N_hum_sl  double  S  0.0  g:10^3:m^-2
# total nitrogen in microbial biomass (kgN m-2).
soilchemistry  N_micro_sl  N_micro_sl  double  S  0.0  g:10^3:m^-2

# Ferrous iron (Fe2+) content in kg/kg
soilchemistry  fe2_sl  fe2_sl  double  S  0.0  g:g^-1
# Ferric iron (Fe3+) content in kg/kg
soilchemistry  fe3_sl  fe3_sl  double  S  0.0  g:g^-1
# Sulfate content
soilchemistry  so4_sl  so4_sl  double  S  0.0  g:10^3:m^-2
# Clay content in kg/kg
soilchemistry  clay_sl  clay_sl  double  S  0.0  g:g^-1
# soil layer cn ratio
soilchemistry  cnr_sl  cnr_sl  double  S  0.0  %
# layer specific humus cn ratio
soilchemistry  cn_hum_sl  cn_hum_sl  double  S  0.0  %
# soil layer bulk density in kg/l
soilchemistry  dens_sl  dens_sl  double  S  0.0  g:10^3:m^-3
# soil layer organic carbon fraction
soilchemistry  fcorg_sl  fcorg_sl  double  S  0.0  %
# Thickness of soil layers
soilchemistry  h_sl  h_sl  double  S  0.0  m
# Depth of soil layers
soilchemistry  depth_sl  depth_sl  double  S  0.0  m
# pH value (acidity) of soil layers
soilchemistry  ph_sl  ph_sl  double  S  0.0  -
# pH value (acidity) of soil layers at initialization of simulation
soilchemistry  phi_sl  phi_sl  double  S  0.0  -
# Porosity of layers
soilchemistry  poro_sl  poro_sl  double  S  0.0  m^3:m^-3
# Minimum fraction of water filled pore space
soilchemistry  wfps_min_sl  wfps_min_sl  double  S  0.0  %
# Maximal fraction of water filled pore space
soilchemistry  wfps_max_sl  wfps_max_sl  double  S  0.0  %
# Sand content in kg/kg
soilchemistry  sand_sl  sand_sl  double  S  0.0  g:g^-1
# Saturated conductivity
soilchemistry  sks_sl  sks_sl  double  S  0.0  cm:min^-1
# Stone fraction of layers
soilchemistry  stone_frac_sl  stone_frac_sl  double  S  0.0  %
# Water content of layers at field capacity
soilchemistry  wcmax_sl  wcmax_sl  double  S  0.0  mm:m^-1
# Water content of layers at wilting point
soilchemistry  wcmin_sl  wcmin_sl  double  S  0.0  mm:m^-1
# Vangenuchten parameter alpha
soilchemistry  vga_sl  vga_sl  double  S  0.0  1:L^-1
# Vangenuchten parameter n
soilchemistry  vgn_sl  vgn_sl  double  S  0.0  1
# Vangenuchten parameter m
soilchemistry  vgm_sl  vgm_sl  double  S  0.0  1

# carbon of raw litter that will be allocated to litter pool 1
soilchemistry  c_raw_lit_1_above  c_raw_lit_1_above  double  -  0.0  g:10^3:m^-2
# carbon of raw litter that will be allocated to litter pool 2
soilchemistry  c_raw_lit_2_above  c_raw_lit_2_above  double  -  0.0  g:10^3:m^-2
# carbon of raw litter that will be allocated to litter pool 3
soilchemistry  c_raw_lit_3_above  c_raw_lit_3_above  double  -  0.0  g:10^3:m^-2
# nitrogen of raw litter that will be allocated to litter pool 1
soilchemistry  n_raw_lit_1_above  n_raw_lit_1_above  double  -  0.0  g:10^3:m^-2
# nitrogen of raw litter that will be allocated to litter pool 2
soilchemistry  n_raw_lit_2_above  n_raw_lit_2_above  double  -  0.0  g:10^3:m^-2
# nitrogen of raw litter that will be allocated to litter pool 3
soilchemistry  n_raw_lit_3_above  n_raw_lit_3_above  double  -  0.0  g:10^3:m^-2

# carbon of raw litter that will be allocated to litter pool 1
soilchemistry  c_raw_lit_1_sl  c_raw_lit_1_sl  double  S  0.0  g:10^3:m^-2
# carbon of raw litter that will be allocated to litter pool 2
soilchemistry  c_raw_lit_2_sl  c_raw_lit_2_sl  double  S  0.0  g:10^3:m^-2
# carbon of raw litter that will be allocated to litter pool 3
soilchemistry  c_raw_lit_3_sl  c_raw_lit_3_sl  double  S  0.0  g:10^3:m^-2
# nitrogen of raw litter that will be allocated to litter pool 1
soilchemistry  n_raw_lit_1_sl  n_raw_lit_1_sl  double  S  0.0  g:10^3:m^-2
# nitrogen of raw litter that will be allocated to litter pool 2
soilchemistry  n_raw_lit_2_sl  n_raw_lit_2_sl  double  S  0.0  g:10^3:m^-2
# nitrogen of raw litter that will be allocated to litter pool 3
soilchemistry  n_raw_lit_3_sl  n_raw_lit_3_sl  double  S  0.0  g:10^3:m^-2

# carbon of stubbles that will be allocated to litter pool 1
soilchemistry  c_stubble_lit1  c_stubble_lit1  double  -  0.0  g:10^3:m^-2
# carbon of stubbles that will be allocated to litter pool 2
soilchemistry  c_stubble_lit2  c_stubble_lit2  double  -  0.0  g:10^3:m^-2
# carbon of stubbles that will be allocated to litter pool 3
soilchemistry  c_stubble_lit3  c_stubble_lit3  double  -  0.0  g:10^3:m^-2
# nitrogen of stubbles that will be allocated to litter pool 1
soilchemistry  n_stubble_lit1  n_stubble_lit1  double  -  0.0  g:10^3:m^-2
# nitrogen of stubbles that will be allocated to litter pool 2
soilchemistry  n_stubble_lit2  n_stubble_lit2  double  -  0.0  g:10^3:m^-2
# nitrogen of stubbles that will be allocated to litter pool 3
soilchemistry  n_stubble_lit3  n_stubble_lit3  double  -  0.0  g:10^3:m^-2
# soil layer specific factor for increased mineralization due to tilling
soilchemistry  till_effect  till_effect_sl  double  S  1.0  -
# (graze) carbon in dung from grazing animals
soilchemistry  c_dung  c_dung  double  -  0.0  g:10^3:m^-2
# (graze) nitrogen in dung from grazing animals
soilchemistry  n_dung  n_dung  double  -  0.0  g:10^3:m^-2
# (graze) nitrogen in urine from grazing animals
soilchemistry  n_urine  n_urine  double  -  0.0  g:10^3:m^-2

# nitrogen release by mineralization per layer (kg m-2).
soilchemistry  accumulated_n_mineral_sl  accumulated_n_mineral_sl  double  S  0.0  g:10^3:m^-2
# oxidation to NO3 by nitrification per layer (kg m-2).
soilchemistry  accumulated_n_nitrify_sl  accumulated_n_nitrify_sl  double  S  0.0  g:10^3:m^-2
# reduction of NO3 by denitrification per layer (kg m-2).
soilchemistry  accumulated_no3_denitrify_sl  accumulated_no3_denitrify_sl  double  S  0.0  g:10^3:m^-2
# reduction of NO2 by denitrification per layer (kg m-2).
soilchemistry  accumulated_no2_denitrify_sl  accumulated_no2_denitrify_sl  double  S  0.0  g:10^3:m^-2
# reduction of NO by denitrification per layer (kg m-2).
soilchemistry  accumulated_no_denitrify_sl  accumulated_no_denitrify_sl  double  S  0.0  g:10^3:m^-2
# reduction of N2O by denitrification per layer (kg m-2).
soilchemistry  accumulated_n2o_denitrify_sl  accumulated_n2o_denitrify_sl  double  S  0.0  g:10^3:m^-2
# nitrogen release as NO by chemo-denitrification per layer (kg m-2).
soilchemistry  accumulated_n_chemodenitrify_sl  accumulated_n_chemodenitrify_sl  double  S  0.0  g:10^3:m^-2

# daily soil co2 emissions (kgC m-2).
soilchemistry  accumulated_co2_emis  accumulated_co2_emis  double  -  0.0  g:10^3:m^-2
# daily respiration from fine roots (kgC m-2).
soilchemistry  accumulated_co2_emis_auto  accumulated_co2_emis_auto  double  -  0.0  g:10^3:m^-2
# respiration from soil microbes (kgC m-2).
soilchemistry  accumulated_co2_emis_hetero  accumulated_co2_emis_hetero  double  -  0.0  g:10^3:m^-2

# CH4 exchange between air and soil (kgC m-2).
soilchemistry  accumulated_ch4_emis  accumulated_ch4_emis  double  -  0.0  g:10^3:m^-2
# CH4 lost by percolation (kgC m-2).
soilchemistry  accumulated_ch4_leach  accumulated_ch4_leach  double  -  0.0  g:10^3:m^-2
# lost organic compounds by percolation (kgC m-2).
soilchemistry  accumulated_doc_leach  accumulated_doc_leach  double  -  0.0  g:10^3:m^-2
# lost organic nitrogen by percolation (kgC m-2).
soilchemistry  accumulated_don_leach  accumulated_don_leach  double  -  0.0  g:10^3:m^-2
# NO3 losses by percolation (kgN m-2).
soilchemistry  accumulated_no3_leach  accumulated_no3_leach  double  -  0.0  g:10^3:m^-2
# NH4 losses by percolation (kgN m-2).
soilchemistry  accumulated_nh4_leach  accumulated_nh4_leach  double  -  0.0  g:10^3:m^-2
# NO release into the lower atmosphere (kgN m-2).
soilchemistry  accumulated_no_emis  accumulated_no_emis  double  -  0.0  g:10^3:m^-2
# N2O release into the lower atmosphere (kgN m-2).
soilchemistry  accumulated_n2o_emis  accumulated_n2o_emis  double  -  0.0  g:10^3:m^-2
# N2 release into the lower atmosphere (kgN m-2).
soilchemistry  accumulated_n2_emis  accumulated_n2_emis  double  -  0.0  g:10^3:m^-2
# NH3 release into the lower atmosphere (kgN m-2).
soilchemistry  accumulated_nh3_emis  accumulated_nh3_emis  double  -  0.0  g:10^3:m^-2
# N2 fixation in the soil
soilchemistry  accumulated_n2_fixation  accumulated_n2_fixation  double  -  0.0  g:10^3:m^-2
# SO4 losses by percolation
soilchemistry  accumulated_so4_leach  accumulated_so4_leach  double  -  0.0  g:10^3:m^-2

# daily carbon fixation by algae
soilchemistry  accumulated_c_fix_algae  accumulated_c_fix_algae  double  -  0.0  g:10^3:m^-2
# daily nitrogen fixation by algae
soilchemistry  accumulated_n_fix_algae  accumulated_n_fix_algae  double  -  0.0  g:10^3:m^-2

# Accumulated carbon litter to aboveground wood
soilchemistry  accumulated_c_litter_wood_above  accumulated_c_litter_wood_above  double  -  0.0  g:10^3:m^-2
# Accumulated nitrogen litter to aboveground wood
soilchemistry  accumulated_n_litter_wood_above  accumulated_n_litter_wood_above  double  -  0.0  g:10^3:m^-2
# Accumulated carbon litter to aboveground stubble
soilchemistry  accumulated_c_litter_stubble  accumulated_c_litter_stubble  double  -  0.0  g:10^3:m^-2
# Accumulated nitrogen litter to aboveground stubble
soilchemistry  accumulated_n_litter_stubble  accumulated_n_litter_stubble  double  -  0.0  g:10^3:m^-2
# Accumulated carbon litter to aboveground raw litter
soilchemistry  accumulated_c_litter_above  accumulated_c_litter_above  double  -  0.0  g:10^3:m^-2
# Accumulated nitrogen litter to aboveground raw litter
soilchemistry  accumulated_n_litter_above  accumulated_n_litter_above  double  -  0.0  g:10^3:m^-2

# Accumulated carbon litter to belowground wood
soilchemistry  accumulated_c_litter_wood_below_sl  accumulated_c_litter_wood_below_sl  double  S  0.0  g:10^3:m^-2
# Accumulated nitrogen litter to belowground wood
soilchemistry  accumulated_n_litter_wood_below_sl  accumulated_n_litter_wood_below_sl  double  S  0.0  g:10^3:m^-2
# Accumulated carbon litter to belowground raw litter
soilchemistry  accumulated_c_litter_below_sl  accumulated_c_litter_below_sl  double  S  0.0  g:10^3:m^-2
# Accumulated nitrogen litter to belowground raw litter
soilchemistry  accumulated_n_litter_below_sl  accumulated_n_litter_below_sl  double  S  0.0  g:10^3:m^-2

# Soil layer specific carbon from root exsudation
soilchemistry  accumulated_c_root_exsudates_sl  accumulated_c_root_exsudates_sl  double  S  0.0  g:10^3:m^-2

# Accumulated carbon allocation from fertilizer
soilchemistry  accumulated_c_fertilizer  accumulated_c_fertilizer  double  -  0.0  g:10^3:m^-2
# Accumulated nitrogen allocation from fertilizer
soilchemistry  accumulated_n_fertilizer  accumulated_n_fertilizer  double  -  0.0  g:10^3:m^-2

# Accumulated carbon input from livestock grazing
soilchemistry  accumulated_c_livestock_grazing  accumulated_c_livestock_grazing  double  -  0.0  g:10^3:m^-2
# Accumulated nitrogen input from livestock grazing
soilchemistry  accumulated_n_livestock_grazing  accumulated_n_livestock_grazing  double  -  0.0  g:10^3:m^-2

# nitrification inhibitor [kg m-2]
soilchemistry  ni_sl  ni_sl  double  S  0.0  g:10^3:m^-2
# urease inhibitor [kg m-2]
soilchemistry  ui_sl  ui_sl  double  S  0.0  g:10^3:m^-2

##############################
###                        ###
###      WATERCYCLE        ###
###                        ###
##############################

## amount of water in canopy layers
watercycle  wc_fl  wc_fl  double  F  0.0  m:m^-2
## amount of water in soil layers
watercycle  wc_sl  wc_sl  double  S  0.0  m:m^-3
## surface water
watercycle  surface_water  surface_water  double  -  0.0  m
## amount of ice in soil layers
watercycle  ice_sl  ice_sl  double  S  0.0  m:m^-3
## surface ice
watercycle  surface_ice  surface_ice  double  - 0.0  m
# snow melting
watercycle  ts_snow_melt  ts_snow_melt  double  -  0.0  m
# daily snow melting
watercycle  nd_snow_melt  nd_snow_melt  double  -  0.0  m
# ?
watercycle  mskpa_sl  mskpa_sl  double  S  0.0  m
# irrigation water stored in reservoir (m)
watercycle  irrigation_reservoir  irrigation_reservoir  double  -  0.0  m
## water input via direct groundwater contact
watercycle  accumulated_groundwater_access  accumulated_groundwater_access  double  -  0.0  m
## water loss via direct groundwater contact
watercycle  accumulated_groundwater_loss  accumulated_groundwater_loss  double  -  0.0  m
## water input via capillary rise from groundwater
watercycle  accumulated_capillaryrise  accumulated_capillaryrise  double  -  0.0  m
## plant water deficit (difference between water loss through leaves and water uptake)
watercycle  plant_waterdeficit  plant_waterdeficit  double  -  0.0  m
## surface water flux
watercycle  accumulated_infiltration  accumulated_infiltration  double  -  0.0  m
## water input via irrigation
watercycle  accumulated_irrigation  accumulated_irrigation  double  -  0.0  m
## water input via automatic irrigation
watercycle  accumulated_irrigation_automatic  accumulated_irrigation_automatic  double  -  0.0  m
## water input via ggcmi-style irrigation
watercycle  accumulated_irrigation_ggcmi  accumulated_irrigation_ggcmi  double  -  0.0  m
## water input via reservoir irrigation
watercycle  accumulated_irrigation_reservoir_withdrawal  accumulated_irrigation_reservoir_withdrawal  double  -  0.0  m
## water loss by runoff
watercycle  accumulated_runoff  accumulated_runoff  double  -  0.0  m
## percolation beneath rooted soil
watercycle  accumulated_percolation  accumulated_percolation  double  -  0.0  m
## precipitation
watercycle  accumulated_precipitation  accumulated_precipitation  double  -  0.0  m
## throughfall at the ground
watercycle  accumulated_throughfall  accumulated_throughfall  double  -  0.0  m
## evaporation from intercepted rainfall/snowmelt
watercycle  accumulated_interceptionevapo  accumulated_interceptionevaporation  double  -  0.0  m
## evaporation from soil/litter
watercycle  accumulated_soilevapo  accumulated_soilevaporation  double  -  0.0  m
## evaporation from surface water
watercycle  accumulated_surfacewaterevapo  accumulated_surfacewaterevaporation  double  -  0.0  m
## potential soilevaporation
watercycle  accumulated_potentialtrans  accumulated_potentialtranspiration double  -  0.0  m
## potential transpiration
watercycle  accumulated_potentialevapo  accumulated_potentialevaporation double  -  0.0  m
## potential soilevapotranspiration
watercycle  accumulated_potentialevapotrans  accumulated_potentialevapotranspiration double  -  0.0  m
## soil water flux from one layer to another
watercycle  accumulated_waterflux_sl  accumulated_waterflux_sl  double  S  0.0  m
## transpiration
watercycle  accumulated_transpiration_sl  accumulated_transpiration_sl  double  S  0.0  m
