/*!
 * @author
 *    steffen klatt
 */

#include  "state/state-requests.h"
#include  "comm/lmessage.h"

#include  "logging/cbm_logging.h"

ldndc::state_request_t::~state_request_t()
{
}

int
ldndc::state_request_inproc_t::get_member(
        char const *  _member_class, char const * _member_id,
        void const *  _data, int const *  _extents, int _rank,
        atomic_datatype_t  _datatype, lreply_t *  _reply)
{
    if ( !_data || _rank == 0)
    {
        /* it's ok when there is no data */
        return  0;
    }
    ldndc_assert( _reply && _member_id);

	char  db_keybuf[128];
	int  n_printf =
		cbm::snprintf( db_keybuf, 127, "%s.%s.", _member_class, _member_id);
	if ( n_printf > ( 127 - 8 /*parts: "type", "rank", etc*/))
	{
		LOGERROR( "member id too large  [id=",_member_id,"]");
		return  -1;
	}
	char *  db_key = db_keybuf + n_printf;
	cbm::snprintf( db_key, 8, "data");
// sk:dbg	LOGDEBUG( "data-key=",db_keybuf);
    _reply->data.set_memaddr( db_keybuf, _data);
    cbm::snprintf( db_key, 8, "type");
// sk:dbg	LOGDEBUG( "type-key=",db_keybuf);
    _reply->data.set_int32( db_keybuf, _datatype);
    cbm::snprintf( db_key, 8, "rank");
// sk:dbg	LOGDEBUG( "rank-key=",db_keybuf);
    _reply->data.set_int32( db_keybuf, 1 /* FIXME _rank*/);
// sk:todo    _reply->set_int32_array( "extents", _extents);
// sk:todo    _reply->set_array( "data", _data, _extents, _rank, _datatype);
    int  m_size = _extents[0];
    for ( int  r = 1;  r < _rank;  ++r)
    { m_size *= _extents[r]; }
    cbm::snprintf( db_key, 8, "size");
// sk:dbg	LOGDEBUG( "size-key=",db_keybuf);
    _reply->data.set_int32( db_keybuf, m_size);

    return  0;
}

int
ldndc::state_request_serialize_t::get_member(
        char const *  /*_member_class*/, char const * /*_member_id*/,
        void const *  /*_data*/, int const *  /*_extents*/, int /*_rank*/,
        atomic_datatype_t  /*_datatype*/, lreply_t *  /*_reply*/)
{
	return  -1;
}

int
ldndc::state_request_base64_t::get_member(
        char const *  /*_member_class*/, char const * /*_member_id*/,
        void const *  /*_data*/, int const *  /*_extents*/, int /*_rank*/,
        atomic_datatype_t  /*_datatype*/, lreply_t *  /*_reply*/)
{
	return  -1;
}

