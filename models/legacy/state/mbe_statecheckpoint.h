/*!
 * @brief
 *    helper functions for checkpointing mobile substate
 *
 * @author
 *    steffen klatt
 */

#ifndef  MBE_STATECHECKPOINT_H_
#define  MBE_STATECHECKPOINT_H_

#include  "ld_legacy.h"
#include  "state/mbe_substate.h"

#include  <input/checkpoint/checkpoint.h>
#include  <io/outputtypes.h>
#include  <io/sink-handle.h>

namespace ldndc {

struct  substate_checkpoint_write_context_t
{
    ldndc::sink_handle_t  sink;

    ldndc::sink_fixed_record_t  record;
    ldndc::sink_client_t  client;
};

struct  substate_checkpoint_read_context_t
{
    ldndc::checkpoint::input_class_checkpoint_t const *  source;
    ldndc_sink_meta_timestamp_t  timestamp;
    ldndc::checkpoint::checkpoint_buffer_t  buf;
};

extern LDNDC_API int  state_checkpoint_acquire_sink_handle(
    cbm::io_kcomm_t *, ldndc::sink_handle_t * /*sink*/);

extern LDNDC_API int  state_checkpoint_release_sink_handle(
    cbm::io_kcomm_t *, ldndc::sink_handle_t * /*sink*/);

extern LDNDC_API int  state_checkpoint_write_member(
        char const * /*member class*/, char const * /*member id*/, void const * /*member data*/, int * /*extents*/, int /*rank*/,
        atomic_datatype_t /*datatype*/,
    substate_checkpoint_write_context_t * /*context*/);

extern LDNDC_API int  state_checkpoint_read_member(
        char const * /*member class*/, char const * /*member id*/, void * /*member data*/, int * /*extents*/, int /*rank*/,
        atomic_datatype_t /*datatype*/,
    substate_checkpoint_read_context_t * /*context*/);

} /* namespace ldndc */

#endif  /*  !MBE_STATECHECKPOINT_H_  */

