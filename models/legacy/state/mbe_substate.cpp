/*!
 * @brief
 *    substate base class implementation, sources factories
 *
 * @author
 *    edwin haas,
 *    steffen klatt
 */


#include  "state/mbe_substate.h"

cbm::string_t ldndc::MoBiLE_Substate::wr_type = "parameter";

ldndc::substate_factory_base_t::~substate_factory_base_t()
{
}

ldndc::MoBiLE_Substate::MoBiLE_Substate()
        : ldndc::object_t(),
          is_initialized_( false)
{
        // nothing here
}

ldndc::MoBiLE_Substate::MoBiLE_Substate( lid_t const &  _id)
        : ldndc::object_t( _id),
          is_initialized_( false)
{
        // nothing here
}

ldndc::MoBiLE_Substate::~MoBiLE_Substate()
{
        /* by definition no deallocations here ! */
}

