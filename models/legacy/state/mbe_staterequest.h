/*!
 * @brief
 *    helper functions for inquiring mobile substate
 *
 * @author
 *    steffen klatt (created on: apr 01, 2015)
 */

#ifndef  MOBILE_STATE_REQUESTS_H_
#define  MOBILE_STATE_REQUESTS_H_

#include  "mobile/mobile-config.h"
#include  "state/substate.h"

struct  lreply_t;

namespace ldndc {

struct LDNDC_API state_request_t
{
	virtual  ~state_request_t() = 0;
    virtual int  get_member(
        char const * /*member class*/, char const * /*member id*/,
        void const * /*member data*/, int const * /*extents*/, int /*rank*/,
        atomic_datatype_t /*datatype*/, lreply_t * /*reply*/) = 0;
// sk:todo    virtual int  put_member() = 0;
};

struct LDNDC_API state_request_inproc_t
    :  public  state_request_t
{
    int  get_member(
        char const * /*member class*/, char const * /*member id*/,
        void const * /*member data*/, int const * /*extents*/, int /*rank*/,
        atomic_datatype_t /*datatype*/, lreply_t * /*reply*/);
// sk:todo    virtual int  put_member() = 0;
};

struct LDNDC_API state_request_serialize_t
    :  public  state_request_t
{
    int  get_member(
        char const * /*member class*/, char const * /*member id*/,
        void const * /*member data*/, int const * /*extents*/, int /*rank*/,
        atomic_datatype_t /*datatype*/, lreply_t * /*reply*/);
// sk:todo    virtual int  put_member() = 0;
};

struct LDNDC_API state_request_base64_t
    :  public  state_request_t
{
    int  get_member(
        char const * /*member class*/, char const * /*member id*/,
        void const * /*member data*/, int const * /*extents*/, int /*rank*/,
        atomic_datatype_t /*datatype*/, lreply_t * /*reply*/);
// sk:todo    virtual int  put_member() = 0;
};

} /*namespace ldndc*/

#endif  /*  !MOBILE_STATE_REQUESTS_H_  */

