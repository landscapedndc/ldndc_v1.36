/*!
 * @author
 *    Steffen Klatt,
 *    Edwin Haas,
 *    David Kraus
 */

#include  "state/mbe_state.h"
#include  "state/mbe_substate.h"
#include  <scientific/soil/ld_soil.h>
#include  <scientific/hydrology/ld_vangenuchten.h>

#include  <logging/cbm_logging.h>
#include  <utils/cbm_utils.h>

ldndc::MoBiLE_State::MoBiLE_State() : cbm::object_t()
{
    wr_type = "parameter";
    cbm::init_array( this->substates_, this->full_substate_cnt(), (MoBiLE_Substate *)NULL);
}

ldndc::MoBiLE_State::~MoBiLE_State()
{
    for ( size_t  k = 0;  k < this->full_substate_cnt();  ++k)
    {
        if ( this->substates_[k])
        {
            substate_factory[k]->destruct( substates_[k]);
        }
    }
}


#include  "kernel/io-kcomm.h"
lerr_t
ldndc::MoBiLE_State::initialize(
                                cbm::io_kcomm_t *  _iokcomm)
{
    ldndc_assert( _iokcomm);
    this->set_object_id( _iokcomm->object_id());
    this->vegetation.set_input( _iokcomm->get_input_class< speciesparameters::input_class_speciesparameters_t >());

    for ( size_t  k = 0;  k < this->full_substate_cnt();  ++k)
    {
        /* allocate substate */
        MoBiLE_Substate *  s = substate_factory[k]->construct( this->object_id(), &this->vegetation);

        if ( s)
        {
            s->wr_type = wr_type;

            LOGDEBUG( "substate '",SUBSTATE_NAMES[k],"' (#",s->member_cnt(),")");
            this->substates_[k] = s;
            lerr_t  rc_state_init = s->initialize( _iokcomm);
            if ( rc_state_init || !_iokcomm->state.ok)
            {
                LOGERROR( "initializing substate failed  [substate=",substate_factory[k]->name(),"]");
                return  LDNDC_ERR_OBJECT_INIT_FAILED;
            }
            ldndc_assert( s->is_initialized());
        }
        else
        {
            LOGERROR( "failed to allocate substate  [substate=",SUBSTATE_NAMES[k],"]");
            return  LDNDC_ERR_OBJECT_INIT_FAILED;
        }
    }

    return  LDNDC_ERR_OK;
}

ldndc::MoBiLE_Substate *
ldndc::MoBiLE_State::get_substate(
        substate_type_e  _stype)
{
    if ( static_cast< size_t >( _stype) < this->full_substate_cnt())
    {
        return  this->substates_[_stype];
    }
    return  static_cast< MoBiLE_Substate * >( NULL);
}
ldndc::MoBiLE_Substate const *
ldndc::MoBiLE_State::get_substate(
        substate_type_e  _stype)
const
{
    return  const_cast< MoBiLE_State * >( this)->get_substate( _stype);
}
ldndc::MoBiLE_Substate *
ldndc::MoBiLE_State::get_substate(
        char const *  _sname)
{
    for ( size_t  s = 0;  s < this->full_substate_cnt();  ++s)
    {
        if (( this->substates_[s])
            && cbm::is_equal( this->substates_[s]->name(), _sname))
        {
            return  this->substates_[s];
        }
    }
    return  NULL;
}
ldndc::MoBiLE_Substate const *
ldndc::MoBiLE_State::get_substate(
        char const *  _sname)
const
{
    return  const_cast< MoBiLE_State * >( this)->get_substate( _sname);
}



size_t
ldndc::MoBiLE_State::full_substate_cnt()
const
{
    return  static_cast< size_t >( SUBSTATE_CNT);
}
size_t
ldndc::MoBiLE_State::substate_cnt()
const
{
    size_t  substate_n = 0;
    for ( size_t  s = 0;  s < this->full_substate_cnt();  ++s)
    {
        if ( this->substates_[s])
        {
            ++substate_n;
        }
    }
    return  substate_n;
}
size_t
ldndc::MoBiLE_State::full_substate_member_cnt()
const
{
    size_t  full_member_n = 0;
    for ( size_t  s = 0;  s < this->full_substate_cnt();  ++s)
    {
        MoBiLE_Substate *  ss = substate_factory[s]->construct( invalid_lid, NULL);
        if ( !ss)
        {
            return  invalid_t< size_t >::value;
        }

        full_member_n += ss->member_cnt();
        substate_factory[s]->destruct( ss);
    }
    return  full_member_n;
}
size_t
ldndc::MoBiLE_State::substate_member_cnt()
const
{
    size_t  member_n = 0;
    for ( size_t  s = 0;  s < this->full_substate_cnt();  ++s)
    {
        MoBiLE_Substate const *  ss =
            this->get_substate( static_cast< substate_type_e >( s));
        if ( !ss)
        {
            continue;
        }

        member_n += ss->member_cnt();
    }
    return  member_n;
}
lerr_t
ldndc::MoBiLE_State::resize_entities(
        cbm::io_kcomm_t *  _iokcomm)
{
    for ( size_t  s = 0;  s < this->full_substate_cnt();  ++s)
    {
        MoBiLE_Substate *  ss =
            this->get_substate( static_cast< substate_type_e >( s));
        if ( !ss)
        {
            continue;
        }

        lerr_t  rc_resize = ss->resize_entities( _iokcomm);
        RETURN_IF_NOT_OK(rc_resize);
    }
    return  LDNDC_ERR_OK;
}

size_t
ldndc::MoBiLE_State::alloc_size()
const
{
    size_t  bytes_n = 0;
    for ( size_t  s = 0;  s < this->full_substate_cnt();  ++s)
    {
        MoBiLE_Substate const *  ss =
            this->get_substate( static_cast< substate_type_e >( s));
        if ( !ss)
        {
            continue;
        }

        bytes_n += ss->alloc_size();
    }
    return  bytes_n;
}


#ifdef  _HAVE_SERIALIZE
#include  <cbm_rtcfg.h>
#include  "state/mbe_statecheckpoint.h"
int
ldndc::MoBiLE_State::substate_checkpoint_write_context_init(
        cbm::io_kcomm_t *  _io_kcomm, substate_checkpoint_write_context_t * _cxt)
{
    if ( !_cxt)
    {
        return  -1;
    }
    int  sink_acquire =
        state_checkpoint_acquire_sink_handle( _io_kcomm, &_cxt->sink);
    if ( sink_acquire)
    {
        return  -2;
    }
    cbm::sclock_t const *  clk = LD_RtCfg.clk;
    _cxt->record = sink_fixed_record_defaults;
    _cxt->record.meta.t.reg.year = clk->year();
    _cxt->record.meta.t.reg.month = clk->month();
    _cxt->record.meta.t.reg.day = clk->day();
    _cxt->record.meta.t.reg.subday = clk->subday();

    _cxt->client.target_id = _io_kcomm->object_id();
    return  0;
}
int
ldndc::MoBiLE_State::substate_checkpoint_write_context_deinit(
        cbm::io_kcomm_t *  _io_kcomm, substate_checkpoint_write_context_t * _cxt)
{
    if ( !_cxt)
    {
        return  -1;
    }
    state_checkpoint_release_sink_handle( _io_kcomm, &_cxt->sink);
    _cxt->record = sink_fixed_record_defaults;
    _cxt->client = sink_client_defaults;
    return  0;
}
int
ldndc::MoBiLE_State::create_checkpoint(
        cbm::io_kcomm_t *  _io_kcomm)
{
    substate_checkpoint_write_context_t  cp_cxt;
    int  rc_cxtinit =
        substate_checkpoint_write_context_init( _io_kcomm, &cp_cxt);
    if ( rc_cxtinit)
    {
        LOGERROR( "failed to initialize checkpoint context");
        substate_checkpoint_write_context_deinit( _io_kcomm, &cp_cxt);
        return  -1;
    }

    int  rc = 0;
    for ( size_t  s = 0;  s < this->full_substate_cnt();  ++s)
    {
        MoBiLE_Substate *  ss =
            this->get_substate( static_cast< substate_type_e >( s));
        if ( !ss)
        {
            continue;
        }

        int  rc_checkpoint = ss->create_checkpoint( &cp_cxt);
        if ( rc_checkpoint)
        {
            LOGERROR( "failed to create checkpoint for substate",
                    "  [substate=",ss->name(),"]");
            rc = -1;
            break;
        }
    }

    substate_checkpoint_write_context_deinit( _io_kcomm, &cp_cxt);
    return  rc;
}


int
ldndc::MoBiLE_State::substate_checkpoint_read_context_init(
        cbm::io_kcomm_t *  _io_kcomm, ldate_t const *  _ldate,
        substate_checkpoint_read_context_t * _cxt)
{
    if ( !_cxt)
    {
        return  -1;
    }
    _cxt->source = _io_kcomm->get_input_class< input_class_checkpoint_t >();
    if ( !_cxt->source)
    {
        LOGERROR( "no checkpoint input for me :-(");
        return  -2;
    }
    sink_record_meta_t  m;
    m.t.reg.year = _ldate->year();
    m.t.reg.month = _ldate->month();
    m.t.reg.day = _ldate->day();
    m.t.reg.subday = _ldate->subday();
    _cxt->timestamp = m.t.ts;

    return  0;
}
int
ldndc::MoBiLE_State::substate_checkpoint_read_context_deinit(
        cbm::io_kcomm_t *, substate_checkpoint_read_context_t *)
{
    return  0;
}
int
ldndc::MoBiLE_State::restore_checkpoint(
        cbm::io_kcomm_t *  _io_kcomm, ldate_t const *  _ldate)
{
    substate_checkpoint_read_context_t  cp_cxt;
    int  rc_cxtinit =
        substate_checkpoint_read_context_init( _io_kcomm, _ldate, &cp_cxt);
    if ( rc_cxtinit)
    {
        LOGERROR( "failed to initialize checkpoint context");
        substate_checkpoint_read_context_deinit( _io_kcomm, &cp_cxt);
        return  -1;
    }

    int  rc = 0;
    for ( size_t  s = 0;  s < this->full_substate_cnt();  ++s)
    {
        MoBiLE_Substate *  ss =
            this->get_substate( static_cast< substate_type_e >( s));
        if ( !ss)
        {
            continue;
        }

        int  rc_checkpoint = ss->restore_checkpoint( &cp_cxt);
        if ( rc_checkpoint)
        {
            LOGERROR( "failed to restore checkpoint for substate",
                    "  [substate=",ss->name(),"]");
            rc = -1;
            break;
        }
    }

    substate_checkpoint_read_context_deinit( _io_kcomm, &cp_cxt);
    return  rc;
}
#endif /* _HAVE_SERIALIZE */

/*!
 * @details
 *
 * @param[in] _p Plant species
 * @param[in] _f_area_sum area fraction, default is set to 1
 * 
 * This functions returns the drought stress factor (\f$ f_h2o \in [0,1] \f$).
 * It is determined as a sum over all layers, where for every layer the water content
 * in respect to minimal and maximal water content ((wc - wcmin)/(wcmax - wcmin)), the inverse area sum,
 * and the fraction of fine roots within the layer are factored in.
 * The smaller the value, the more do the conditions approach drought.
 */
double
ldndc::MoBiLE_State::get_fh2o( ldndc::MoBiLE_Plant const * _p,
                               double const _f_area_sum,
                               bool const _use_van_genuchten,
                               bool const _root_weighted) const
{
    double  f_h2o = 0.0;
    if (   cbm::flt_greater_zero( _p->mFrt)
        && cbm::flt_greater_zero( _f_area_sum))
    {
        substate_watercycle_t const * wc = get_substate< substate_watercycle_t >();
        substate_soilchemistry_t const * sc = get_substate< substate_soilchemistry_t >();

        if ( _use_van_genuchten)
        {
            double const cp_wp( 150.0); // must be the same unit as 1/alpha -> m
            
            if ( _root_weighted)
            {
                for ( size_t  l = 0;  l < wc->wc_sl.size();  ++l)
                {
                    if ( cbm::flt_greater_zero( _p->fFrt_sl[l]))
                    {
                        double const vg_m( cbm::bound_min(0.0, 1.0 - (1.0 / sc->vgn_sl[l])));
                        double const wcmin( ldndc::hydrology::water_content( cp_wp, sc->vga_sl[l], sc->vgn_sl[l], vg_m,
                                                                             sc->wfps_max_sl[l] * sc->poro_sl[l],
                                                                             sc->wfps_min_sl[l] * sc->poro_sl[l]));
                        f_h2o += get_fh2o_sl( _p, wc, sc, l, wcmin, _f_area_sum) * _p->fFrt_sl[l];
                    }
                    else
                    {
                        break;
                    }
                }
                f_h2o = cbm::bound( 0.0, f_h2o, 1.0);
            }
            else
            {
                double depth_sum( 1.0e-9);
                for ( size_t  l = 0;  l < wc->wc_sl.size();  ++l)
                {
                    if ( cbm::flt_greater_zero( _p->fFrt_sl[l]))
                    {
                        double const vg_m( cbm::bound_min(0.0, 1.0 - (1.0 / sc->vgn_sl[l])));
                        double const wcmin( ldndc::hydrology::water_content( cp_wp, sc->vga_sl[l], sc->vgn_sl[l], vg_m,
                                                                             sc->wfps_max_sl[l] * sc->poro_sl[l],
                                                                             sc->wfps_min_sl[l] * sc->poro_sl[l]));
                        depth_sum += sc->h_sl[l];
                        f_h2o += get_fh2o_sl( _p, wc, sc, l, wcmin, _f_area_sum) * sc->h_sl[l];
                    }
                    else
                    {
                        break;
                    }
                }
                f_h2o = cbm::bound( 0.0, f_h2o / depth_sum, 1.0);
            }
        }
        else
        {
            for ( size_t  l = 0;  l < wc->wc_sl.size();  ++l)
            {
                f_h2o += get_fh2o_sl( _p, wc, sc, l, sc->wcmin_sl[l], _f_area_sum);
            }
            f_h2o = cbm::bound( 0.0, f_h2o, 1.0);
        }
    }
    else
    {
        f_h2o = 1.0;
    }

    return f_h2o;
}


double
ldndc::MoBiLE_State::get_fh2o_sl( ldndc::MoBiLE_Plant const * _p,
                                  substate_watercycle_t const *_wc,
                                  substate_soilchemistry_t const *_sc,
                                  size_t _sl,
                                  double const &_wc_min,
                                  double const _f_area_sum) const
{
    return  cbm::bound( 0.0,
                        (_wc->wc_sl[_sl] - _wc_min) / (_f_area_sum * ( _sc->wcmax_sl[_sl] - _wc_min)),
                        1.0) * _p->fFrt_sl[_sl];
}


#ifdef  _LDNDC_HAVE_ONLINECONTROL
#include  "comm/lmessage.h"
int
ldndc::MoBiLE_State::process_request(
        lreply_t *  _reply, lrequest_t const *  _request)
{
    if ( !_request)
    {
        return  -1;
    }

    if (( _request->command == "GET")
		|| ( _request->command == "PUT"))
    {
        return  this->substate_request_putget( _reply, _request);
    }
    return  -2;
}

int
ldndc::MoBiLE_State::substate_request_putget(
        lreply_t *  _reply, lrequest_t const *  _request)
{
    MoBiLE_Substate *  ss =
        this->get_substate( _request->uri.query( "substate"));
    if ( ss)
    {
        lrequest_t  req( _request->subrequest());
		req.data.set_string( ":item:", _request->uri.fragment());
        return  ss->process_request( _reply, &req);
    }
    return  -1;
}
#endif /* _LDNDC_HAVE_ONLINECONTROL */

