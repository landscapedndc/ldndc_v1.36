/*!
 * @brief
 *    convenience header pulling all substate headers
 *
 * @author
 *    steffen klatt (created on: oct 27, 2012),
 *    edwin haas
 */

#ifndef LDNDC_SUBSTATES_H_
#define LDNDC_SUBSTATES_H_

#include  "state/mbe_substate.h"

namespace ldndc {
extern  substate_factory_base_t LDNDC_API const *  substate_factory[SUBSTATE_CNT];
}

#include  "state/substates/mbe_airchemistry.h"
namespace ldndc {
extern substate_factory_t< substate_airchemistry_t > const  substate_airchemistry_factory;
}
#include  "state/substates/mbe_microclimate.h"
namespace ldndc {
extern substate_factory_t< substate_microclimate_t > const  substate_microclimate_factory;
}
#include  "state/substates/mbe_physiology.h"
namespace ldndc {
extern substate_factory_t< substate_physiology_t > const  substate_physiology_factory;
}
#include  "state/substates/mbe_soilchemistry.h"
namespace ldndc {
extern substate_factory_t< substate_soilchemistry_t > const  substate_soilchemistry_factory;
}
#include  "state/substates/mbe_surfacebulk.h"
namespace ldndc {
extern substate_factory_t< substate_surfacebulk_t > const  substate_surfacebulk_factory;
}
#include  "state/substates/mbe_watercycle.h"
namespace ldndc {
extern substate_factory_t< substate_watercycle_t > const  substate_watercycle_factory;
}

#endif  /*  !LDNDC_SUBSTATES_H_  */

