/*!
 * @brief
 *    abstract base class for all model substates
 *    and substate factory
 *
 * @author
 *    edwin haas,
 *    steffen klatt
 */

#ifndef  LDNDC_SUBSTATE_H_
#define  LDNDC_SUBSTATE_H_

#include  "ld_legacy.h"
#include  "state/mbe_substatetypes.h"

#include  <input/ecosystemtypes.h>
#include  <cbm_object.h>

struct  lreply_t;
struct  lrequest_t;

#define  __LDNDC_SUBSTATE_CLASS(__substate__)  TOKENPASTE3(substate_,__substate__,_t)
#define  MOBILE_SUBSTATE_OBJECT(__type__,__flags__)             \
    LDNDC_OBJECT(__LDNDC_SUBSTATE_CLASS(__type__))              \
    private: static char const * const  ss_name;                \
    public: char const *  name() const { return  ss_name;}      \
    public:                                                     \
        enum                                                    \
        {                                                       \
            substate_type = (__ldndc_substate_enum_##__type__)  \
        };                                                      \
                                                                \
        static  lflags_t  flags()                               \
        {                                                       \
            return  (__flags__);                                \
        }                                                       \
                                                                \
    public:                                                     \
        __LDNDC_SUBSTATE_CLASS(__type__)();                     \
        __LDNDC_SUBSTATE_CLASS(__type__)(                       \
                lid_t const &, MoBiLE_PlantVegetation *);       \
                                                                \
        ~__LDNDC_SUBSTATE_CLASS(__type__)();                    \
                                                                \
        lerr_t  initialize(                                     \
                cbm::io_kcomm_t *,                              \
                void * = NULL);                                 \
                                                                \
    private:                                                    \
        bool  set_required_inputs_(                             \
                cbm::io_kcomm_t *);                             \
        bool  has_required_inputs_(                             \
                cbm::io_kcomm_t *) const;                       \
                                                                \
        lerr_t  resize_entities(                                \
                cbm::io_kcomm_t *);                             \
                                                                \
        /* hide these buggers for now */                        \
        __LDNDC_SUBSTATE_CLASS(__type__)(                       \
                __LDNDC_SUBSTATE_CLASS(__type__) const &);      \
        __LDNDC_SUBSTATE_CLASS(__type__) &  operator=(          \
                __LDNDC_SUBSTATE_CLASS(__type__) const &)/*;*/

#define  MOBILE_SUBSTATE_OBJECT_DEFN(__type__)													\
    LDNDC_OBJECT_DEFN(ldndc::__LDNDC_SUBSTATE_CLASS(__type__))					\
    char const * const  ldndc::__LDNDC_SUBSTATE_CLASS(__type__)::ss_name =	    \
				SUBSTATE_NAMES[__ldndc_substate_enum_##__type__];


namespace ldndc {
struct  substate_checkpoint_read_context_t;
struct  substate_checkpoint_write_context_t;
enum
{
    /*! empty flag */
    LSUB_FLAG_NONE = 0u
};

/*!
 * @brief
 *    abstract) base class for all simulation system state compartments
 */
class  LDNDC_API  MoBiLE_Substate  :  public  cbm::object_t
{
    public:
        MoBiLE_Substate();
        MoBiLE_Substate( lid_t const &);
        virtual  ~MoBiLE_Substate() = 0;

        virtual  char const *  name() const = 0;

        /*!
         * @brief
         *    assigns dynamically allocated substate
         *    class members a memory area using offset
         *    given to this function
         *
         * @param
         *    memory offset in state memory pool
         */
        virtual  lerr_t  initialize( cbm::io_kcomm_t *, void * = NULL) = 0;

        /*!
         * @brief
         *    return number of substate container entities
         */
        virtual  size_t  member_cnt() const = 0;

        /*!
         * @brief
         *    returns the size of memory a substate needs
         *    to store its dynamically allocated objects
         *    (note that this is not allowed to change during
         *    program run)
         */
        virtual  size_t  alloc_size() const = 0;

        /*!
         * @brief
         *    resize substate container entities
         */
        virtual  lerr_t  resize_entities( cbm::io_kcomm_t *) = 0;


        /*!
         * @brief
         *    substates initialization status. if initialization
         *    succeeded the state should be set by calling
         *    @fn set_initialized
         *
         * @return
         *    true, if initialization succeeded. false, if it did
         *    not succeed or initialize was not called prior to
         *    calling @fn is_initialized
         */
        bool  is_initialized() const
            { return  is_initialized_; }

#ifdef  _HAVE_SERIALIZE
        virtual  int  create_checkpoint( substate_checkpoint_write_context_t *) = 0;
        virtual  int  restore_checkpoint( substate_checkpoint_read_context_t *) = 0;
#endif /* _HAVE_SERIALIZE */
#ifdef  _LDNDC_HAVE_ONLINECONTROL
        virtual  int  process_request(
                lreply_t * /*reply*/, lrequest_t const * /*request*/)
        { return  LDNDC_ERR_OK;}
#endif /* _LDNDC_HAVE_ONLINECONTROL */

    protected:
        /*!
         * @brief
         *    sets initialization state to true (successful
         *    initialization (this has to be done by substate
         *    itself)
         *
         * @note
         *    for now this cannot be reset. currently,
         *    initialization occurs only once so this seems
         *    ok ;-)
         */
        void  set_initialized()
        {
            is_initialized_ = true;
        }

        /* hide copy construction and assignment operator */
        MoBiLE_Substate( MoBiLE_Substate const &);

        MoBiLE_Substate &  operator=( MoBiLE_Substate const &);

    public:
        static cbm::string_t wr_type;

    private:
        bool  is_initialized_;
};


class MoBiLE_PlantVegetation;
struct  LDNDC_API  substate_factory_base_t
{
    char const *  name()
    const
    {
        return  SUBSTATE_NAMES[substate_type()];
    }


    substate_factory_base_t() {}
    virtual  ~substate_factory_base_t() = 0;

    virtual  substate_type_e  substate_type() const = 0;

    virtual  size_t  memsize() const = 0;

    virtual  MoBiLE_Substate *  construct(
            lid_t const &, MoBiLE_PlantVegetation *) const = 0;
    virtual  void  destruct(
            MoBiLE_Substate *) const = 0;
};


template < class _S >
struct substate_factory_t : substate_factory_base_t
{
    substate_factory_t() 
        : substate_factory_base_t()
        { }

    substate_type_e  substate_type() const
        { return  (substate_type_e)_S::substate_type; }

    size_t  memsize() const
        { return  sizeof( _S); }

    MoBiLE_Substate *  construct(
            lid_t const &  _id, MoBiLE_PlantVegetation *  _pv)
    const
    {
// sk:off           return  static_cast< MoBiLE_Substate * >( _S::new_instance( _id, _pv));
        return  static_cast< MoBiLE_Substate * >( new _S( _id, _pv));
    }

    void  destruct( MoBiLE_Substate *  _s) const
    {
        if ( _s)
        {
// sk:off            _s->delete_instance();
            delete _s;
        }
    }
};

#define  LDNDC_INAME(__iclass__) (ldndc::__iclass__::input_class_ ## __iclass__ ## _t::iclassname())

} /*namespace ldndc*/

#include  <containers/cbm_vector.h>


#endif  /*  !LDNDC_SUBSTATE_H_  */

