# vim: ft=cmake

set( alloc_mem_size_reg  "alloc_mem_size_")

set( extents "extents")
set( extent_type "extent_type")

set( members_decl "")
set( members_construct_defn "")
set( members_resize_defn "")
set( members_scalar_write_defn "")
set( members_write_defn "")
set( rank_max "0")
set( member_cnt "0")

## by definition list is not empty, i.e. size > 1
math( EXPR last_state_item_index "${state_item_prop_list_size} - 1")

## iterate through all state items
foreach( s_item_idx RANGE ${last_state_item_index})

    list( GET state_item_prop_list ${s_item_idx} s_item_line)

    ## turn property line into list
    string( REGEX REPLACE "\ +" ";" s_item_props "${s_item_line}")

    list( GET s_item_props ${e_id} e_id_val)
    list( GET s_item_props ${e_name} e_name_val)
    list( GET s_item_props ${e_type} e_type_val)
    list( GET s_item_props ${e_size} e_size_val)
    list( GET s_item_props ${e_inival} e_inival_val)
    list( GET s_item_props ${e_unit} e_unit_val)

    s_type2type( ${e_type_val} e_ltype)

    ## update member counter
    math( EXPR member_cnt "${member_cnt} + 1")

    string( REGEX REPLACE ":" ";" e_size_val_l "${e_size_val}")
    if( "${e_size_val}" STREQUAL "-")
        ## declaration (type)
        defn_append( members_decl
                "\t\t\\\n\t${e_type_val} ${e_name_val};  /* [${e_unit_val}] */")
        defn_append( members_construct_defn
                "\tthis->${e_name_val} = ${e_inival_val};\n")
        defn_append( members_scalar_write_defn
                "\terr_count += state_checkpoint_write_member( \"${_substate}\", \"${e_id_val}\", &(_${_substate}->${e_name_val}), &scalar_size, 1, ${e_ltype}, _context);\n")
        defn_append( members_scalar_read_defn
                "\terr_count += state_checkpoint_read_member( \"${_substate}\", \"${e_id_val}\", &(_${_substate}->${e_name_val}), &scalar_size, 1, ${e_ltype}, _context);\n")

        defn_append( member_scalar_get_defn
                "\telse if ( lstring::is_equal( _m_id, \"${e_id_val}\"))\n\t{")
        defn_append( member_scalar_get_defn
                "\n\t\treturn  _sq->get_member(\n\t\t\t\"${_substate}\", \"${e_id_val}\", &(_${_substate}->${e_name_val}), &scalar_size, 1, ${e_ltype}, _reply);\n")
        defn_append( member_scalar_get_defn "\t}\n")
    else()
        list( LENGTH e_size_val_l rank)
        math( EXPR rank_minus_one "${rank} - 1")

        if( ${rank_max} LESS ${rank})
            set( rank_max "${rank}")
        endif()

        defn_append( members_decl
                "\t\t\\\n\tCBM_Vector< ${e_type_val}, ${rank} >  ${e_name_val};  /* [${e_unit_val}] */")

        set( arr_extents "")
        set( arr_extents_serial "")
        set( arr_zeroth_index "")
        set( arr_extents_check_zero "true")
        foreach( dim_j RANGE ${rank_minus_one})
            ## generate argument list for array sizes
            list( GET e_size_val_l ${dim_j} e_size_val_i)
            set( extent  "${SIZE_PREFIX}${e_size_val_i}")
            defn_append( arr_extents "${extents}[${dim_j}] = ${extent}; ")

            defn_append( arr_extents_serial "${extents}[${dim_j}] = _${_substate}->${e_name_val}.extent(${dim_j}); ")
            defn_append( arr_zeroth_index "[0]")
            defn_append( arr_extents_check_zero "&&(${extents}[${dim_j}]>${_substate}_EXTENT_ZERO)")
        endforeach()

        defn_append( members_resize_defn "\t${arr_extents}\n")
        defn_append( members_resize_defn
                "\tthis->${e_name_val}.resize_and_preserve( ${extents}, ${e_inival_val});\n\tthis->${alloc_mem_size_reg} += this->${e_name_val}.bytes();\n\n")

        if( NOT "${e_id_val}" STREQUAL "-")
            defn_append( members_write_defn "\t${arr_extents_serial}\n")
            defn_append( members_write_defn "\t${e_type_val} const *  ${e_name_val}_ptr = NULL;\n\tif ( ${arr_extents_check_zero}) { ${e_name_val}_ptr = &(_${_substate}->${e_name_val}${arr_zeroth_index}); }\n")
            defn_append( members_write_defn
                    "\terr_count += state_checkpoint_write_member(\n\t\t\"${_substate}\", \"${e_id_val}\", ${e_name_val}_ptr, ${extents}, ${rank}, ${e_ltype}, _context);\n\n")
            defn_append( members_read_defn "\t${arr_extents_serial}\n")
            defn_append( members_read_defn "\t${e_type_val} *  ${e_name_val}_ptr = NULL;\n\tif ( ${arr_extents_check_zero}) { ${e_name_val}_ptr = &(_${_substate}->${e_name_val}${arr_zeroth_index}); }\n")
            defn_append( members_read_defn
                    "\terr_count += state_checkpoint_read_member(\n\t\t\"${_substate}\", \"${e_id_val}\", ${e_name_val}_ptr, ${extents}, ${rank}, ${e_ltype}, _context);\n\n")

            defn_append( member_get_defn "\telse if ( lstring::is_equal( _m_id, \"${e_id_val}\"))\n\t{\n")
            defn_append( member_get_defn "\t\t${arr_extents_serial}\n")
            defn_append( member_get_defn "\t\t${e_type_val} *  ${e_name_val}_ptr = NULL;\n\t\tif ( ${arr_extents_check_zero}) { ${e_name_val}_ptr = &(_${_substate}->${e_name_val}${arr_zeroth_index}); }\n")
            defn_append( member_get_defn
                    "\t\treturn  _sq->get_member(\n\t\t\t\"${_substate}\", \"${e_id_val}\", ${e_name_val}_ptr, ${extents}, ${rank}, ${e_ltype}, _reply);\n")
            defn_append( member_get_defn "\t}\n")
        endif()
    endif()

endforeach( s_item_idx)

message( STATUS "largest rank for substate \"${_substate}\" is r=${rank_max}")

set( mbe_state_substate_header_g "${legacy_g_source_dir}/substate/mbe_${_substate}.h.inc")
set( mbe_state_substate_source_g "${legacy_g_source_dir}/substate/mbe_${_substate}.cpp.inc")

## header file
file( WRITE "${mbe_state_substate_header_g}"
    "#ifndef  MBE_SUBSTATE_${_substate}_H_INC_\n"
    "#define  MBE_SUBSTATE_${_substate}_H_INC_\n"
    "\n#include  \"legacy/state/mbe_statecheckpoint.h\""

    "\n\n"
    "#define  LDNDC_${_substate}_SUBSTATE_ITEMS "
    "\\\npublic: size_t alloc_size() const; "
    "\\\npublic: size_t member_cnt() const; "
    "\\\nprivate: void g_construct_(); "
    "\\\nprivate: lerr_t g_resize_( ldndc::io_kcomm_t *); "
    "\\\nprivate: size_t ${alloc_mem_size_reg}; "
    "\\\npublic:\\\n${members_decl} \\\nprivate:\n\n"

    "\n\n#endif  /*  !MBE_SUBSTATE_${_substate}_H_INC_  */"
    "\n\n"
)


## implementation file

## opening substate block
file( WRITE "${mbe_state_substate_source_g}"
    "#ifndef  MBE_SUBSTATE_${_substate}_CPP_INC_\n"
    "#define  MBE_SUBSTATE_${_substate}_CPP_INC_\n"

    "\n"
    "void\n"
    "ldndc::substate_${_substate}_t::g_construct_()"
    "\n{"
    "\n\tthis->${alloc_mem_size_reg} = 0;"
    "\n"
    "\n${members_construct_defn}"
    "\n}"

    "\nlerr_t"
    "\nldndc::substate_${_substate}_t::g_resize_( ldndc::io_kcomm_t *  _io_comm)"
    "\n{"
    "\n\tLDNDC_FIX_UNUSED(_io_comm);"
    "\n\tthis->${alloc_mem_size_reg} = 0;"
    "\n"
    "\n#define  ${_substate}_RANK_MAX  ${rank_max}"
    "\n#if  ${_substate}_RANK_MAX > 0"
    "\n\tCBM_Vector< int, 1 >::${extent_type}  ${extents}[${_substate}_RANK_MAX];"

    "\n\n"

    "\n${members_resize_defn}"

    "\n#endif"
    "\n#undef  ${_substate}_RANK_MAX"

    "\n\treturn  LDNDC_ERR_OK;"
    "\n}"

    "\nsize_t"
    "\nldndc::substate_${_substate}_t::member_cnt()"
    "\nconst"
    "\n{"
    "\n\treturn  static_cast< size_t >( ${member_cnt});"
    "\n}"
    "\n\n"
    "\nsize_t"
    "\nldndc::substate_${_substate}_t::alloc_size()"
    "\nconst"
    "\n{"
    "\n\treturn  this->${alloc_mem_size_reg};"
    "\n}"
    "\n\n#endif  /*  !MBE_SUBSTATE_${_substate}_CPP_INC_  */"
    "\n\n"
)

file( APPEND "${mbe_state_substate_source_g}"
    "\n#ifdef  _HAVE_SERIALIZE\n"
    "#ifndef  MBE_SUBSTATE_${_substate}_SERIALIZE_CPP_INC_\n"
    "#define  MBE_SUBSTATE_${_substate}_SERIALIZE_CPP_INC_\n"

## dump()
    "\n#include  \"legacy/state/mbe_statecheckpoint.h\""
    "\nstatic int  g_mobile_substate_write_members_s(\n\tldndc::substate_${_substate}_t const *  _${_substate}, ldndc::substate_checkpoint_write_context_t *  _context)"
    "\n{"
    "\n\tint  err_count = 0; int  scalar_size = 1; LDNDC_FIX_UNUSED(scalar_size);\n\n"
    "${members_scalar_write_defn}"
    "\n"
    "\n#define  ${_substate}_RANK_MAX  ${rank_max}"
    "\n#if  ${_substate}_RANK_MAX > 0"
    "\n\tCBM_Vector< int, 1 >::extent_type  ${extents}[${_substate}_RANK_MAX];"
    "\n\tstatic CBM_Vector< int, 1 >::extent_type const  ${_substate}_EXTENT_ZERO = 0;\n\n"
    "\n"
    "${members_write_defn}"
    "\n#endif  /* ${_substate}_RANK_MAX > 0 */"
    "\n\n\treturn  err_count;"
    "\n}\n"
## restore()
    "\nstatic int  g_mobile_substate_read_members_s(\n\tldndc::substate_${_substate}_t *  _${_substate}, ldndc::substate_checkpoint_read_context_t *  _context)"
    "\n{"
    "\n\tint  err_count = 0; int  scalar_size = 1; LDNDC_FIX_UNUSED(scalar_size);\n\n"
    "${members_scalar_read_defn}"
    "\n"
    "\n#define  ${_substate}_RANK_MAX  ${rank_max}"
    "\n#if  ${_substate}_RANK_MAX > 0"
    "\n\tCBM_Vector< int, 1 >::extent_type  ${extents}[${_substate}_RANK_MAX];"
    "\n\tstatic CBM_Vector< int, 1 >::extent_type const  ${_substate}_EXTENT_ZERO = 0;\n\n"
    "\n"
    "${members_read_defn}"
    "\n#endif  /* ${_substate}_RANK_MAX > 0 */"
    "\n\n\treturn  err_count;"
    "\n}\n"

    "\n\n#endif  /*  !MBE_SUBSTATE_${_substate}_SERIALIZE_CPP_INC_  */"
    "\n\n"
    "\n#endif  /*  _HAVE_SERIALIZE  */"
    "\n"
)

file( APPEND "${mbe_state_substate_source_g}"
    "#ifndef  MBE_SUBSTATE_${_substate}_REQUESTS_CPP_INC_\n"
    "#define  MBE_SUBSTATE_${_substate}_REQUESTS_CPP_INC_\n"
    "\n#ifdef  _LDNDC_HAVE_ONLINECONTROL\n"
    "\n#include  \"legacy/state/mbe_staterequests.h\"\n"

## put
    "/* TODO  implement \"g_mobile_substate_put_member_s\"*/\n"
## get
    "\nstatic int  g_mobile_substate_get_member_s("
    "\n\tldndc::state_request_t *  _sq, lreply_t *  _reply, char const *  _m_id, ldndc::substate_${_substate}_t *  _${_substate})"
    "\n{"
    "\n#define  ${_substate}_RANK_MAX  ${rank_max}"
    "\n\tint  scalar_size = 1; LDNDC_FIX_UNUSED(scalar_size);\n\n"
    "\n\tif ( 0) { /*no op*/}\n"
    "${member_scalar_get_defn}"
    "\n"
    "\n#if  ${_substate}_RANK_MAX > 0"
    "\n\tCBM_Vector< int, 1 >::extent_type  ${extents}[${_substate}_RANK_MAX];"
    "\n\tstatic CBM_Vector< int, 1 >::extent_type const  ${_substate}_EXTENT_ZERO = 0;\n\n"
    "\n"
    "\n\tif ( 0) { /*no op*/}\n"
    "${member_get_defn}"
    "\n#endif  /* ${_substate}_RANK_MAX > 0 */"
    "\n\n\treturn  -10;"
    "\n}\n"

    "\n#endif  /*  _LDNDC_HAVE_ONLINECONTROL  */"
    "\n\n"
    "\n\n#endif  /*  !MBE_SUBSTATE_${_substate}_REQUESTS_CPP_INC_  */"
    "\n"
)

