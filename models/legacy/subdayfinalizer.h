/*!
 * @brief
 *    kind of helper module to perform additional tasks
 *    after all user modules have been run
 *
 * @author
 *    steffen klatt
 *    edwin haas
 */

#ifndef  MBE_SUBDAYFINALIZER_H_
#define  MBE_SUBDAYFINALIZER_H_

#include  "mbe_legacymodel.h"

namespace ldndc {

class  LDNDC_API  SubdailyFinalizer  :  public  MBE_LegacyModel
{
    LMOD_EXPORT_MODULE_INFO(SubdailyFinalizer,"sys:*:subdailyfinalizer","house-keeping at end of subday");
    public:
        SubdailyFinalizer(
                MoBiLE_State *,
                cbm::io_kcomm_t *,
                timemode_e);

        ~SubdailyFinalizer();


        lerr_t  configure( ldndc::config_file_t const *) { return  LDNDC_ERR_OK; }

        lerr_t  initialize() { return  LDNDC_ERR_OK; }

        lerr_t  solve();

        lerr_t  finalize() { return  LDNDC_ERR_OK; }


        lerr_t  sleep() { return  LDNDC_ERR_OK; }
        lerr_t  wake() { return  LDNDC_ERR_OK; }

    private:
        input_class_setup_t const *  se_;
        input_class_soillayers_t const *  sl_;

        substate_microclimate_t &  mc_;
        substate_physiology_t &  ph_;
        substate_watercycle_t &  wc_;

        MoBiLE_PlantVegetation *  m_veg;

        double  latitude_;
};

} /*namespace ldndc*/

#endif  /*  !MBE_SUBDAYFINALIZER_H_  */

