/*!
 * @file
 *      plant attributes
 *
 * @author
 *      Steffen Klatt (created on: jan 12, 2017),
 *      David Kraus,
 *      Ruediger Grote
 */

#ifndef  MoBiLE_PLANT_H_
#define  MoBiLE_PLANT_H_

#include  "ld_legacy.h"

#include  <ld_plants.h>
#include  <string/cbm_string.h>

namespace ldndc {


/*! @details overview of plant properties
 *  ...
 */
typedef speciesparameters::speciesparameters_set_t MoBiLE_PlantParameters;
class LDNDC_API MoBiLE_PlantSettings : public LD_PlantSettings< MoBiLE_PlantParameters >
{
public:
    MoBiLE_PlantSettings( size_t /* canopy layers */,
                          char const * /*name*/,
                          char const * /*type*/,
                          char const * /*group*/,
                          MoBiLE_PlantParameters const * = NULL /*parameters*/);

    size_t  nb_ageclasses;
    size_t  nb_foliagelayers;
    size_t  nb_soillayers;
};


template < typename _T >
class MoBiLE_PlantRateAttribute
{
public:
    MoBiLE_PlantRateAttribute(){ value = daily_value = yearly_value = total_value = 0.0; };
    ~MoBiLE_PlantRateAttribute(){};

    _T value;

    _T daily(){ return daily_value; };
    _T yearly(){ return yearly_value; };
    _T total(){ return total_value; };

    void reset()
    {
        value = 0.0;
    };
    void reset_daily()
    {
        reset();
        daily_value = 0.0;
    };
    void reset_yearly()
    {
        reset_daily();
        yearly_value = 0.0;
    };
    void reset_total()
    {
        reset_yearly();
        total_value = 0.0;
    };

private:
    _T daily_value;
    _T yearly_value;
    _T total_value;

public:
    MoBiLE_PlantRateAttribute & operator+(_T const &_add)
    {
        value += _add;
        daily_value += _add;
        yearly_value += _add;
        total_value += _add;
        return *this;
    };
    MoBiLE_PlantRateAttribute & operator+=(_T const &_add)
    {
        value += _add;
        daily_value += _add;
        yearly_value += _add;
        total_value += _add;
        return *this;
    };
};


class MoBiLE_State;
class MoBiLE_PlantVegetation;
class LDNDC_API MoBiLE_Plant : public LD_Plant< MoBiLE_PlantSettings >
{
public:
    typedef MoBiLE_PlantParameters  Parameters;
    typedef MoBiLE_PlantSettings  Settings;
public:
    MoBiLE_Plant( MoBiLE_PlantSettings const *);
    ~MoBiLE_Plant();

    void switchparameters( Parameters _params){ m_parameters = _params; };

    char const *  cname() const
        { return this->name().c_str(); }
    char const *  ctype() const
        { return this->type().c_str(); }
    char const *  cgroup() const
        { return this->group().c_str(); }
    species::species_group_e  groupId() const
        { return this->parameters()->GROUP(); }

    bool  is_family( char const * /*family*/) const;

    size_t  nb_ageclasses() const;
    size_t  NB_AGECLASSES() const
        { return this->m_plantsettings.nb_ageclasses; }
    size_t  nb_foliagelayers() const;
    size_t  NB_FOLIAGELAYERS() const
        { return this->m_plantsettings.nb_foliagelayers; }
    size_t  nb_soillayers() const;
    size_t  NB_SOILLAYERS() const
        { return this->m_plantsettings.nb_soillayers; }

    size_t  slot;

    lerr_t  clear();
    lerr_t  initialize();

public: /*plant attributes*/
    
    /*! @details listing of plant properties
     *  ...
     */
    
    /* day of foliage budburst  [-] */
    int  dEmerg;
    /* yearly nitrogen fixation [g:10^3:m^-2:a^-1] */
    double  a_fix_n;
    /* crown diameter relation to breast height diameter [%] */
    double  cdr;
    /* average individual diameter at ground height [m] */
    double  dbas;
    /* average individual diameter at 1.3 m height [m] */
    double  dbh;
    /* root depth [m] */
    double  rooting_depth;

    /* carbon increase of reproductive tissue per time step. [g:10^3:m^-2] */
    double  dcBud, d_dcBud;
    /* carbon allocated to pool of free available substrat per time step. [g:10^3:m^-2] */
    double  dcFac, d_dcFac;
    /* carbon increase of foliage per time step. [g:10^3:m^-2] */
    double  dcFol, d_dcFol;
    /* carbon increase of fine roots per time step. [g:10^3:m^-2] */
    double  dcFrt, d_dcFrt;
    /* carbon increase of sapwood per time step. [g:10^3:m^-2] */
    double  dcSap, d_dcSap;

    /* nitrous oxide uptake per time step. [g:10^3:m^-2] */
    double  nox_uptake, d_nox_uptake;
    /* N2 uptake per time step. [g:10^3:m^-2] */
    double  n2_fixation, d_n2_fixation;

    /* relative state of foliage flushing. [%] */
    double  dvsFlush, dvsFlushOld;
    /* relative state of foliage senescence. [%] */
    double  dvsMort;
    /* development stage of wood growth. [%] */
    double  dvsWood;

    /* carbon loss from exudation [kgC m-2] */
    double  exsuLoss, d_exsuLoss;
    /* area fraction of species relative to total ground coverage [%] */
    double  f_area;
    /* branch fraction of total (sap)wood [%] */
    double  f_branch;
    /* reduction factor for initialization of the nitrogen and basic cation concentration relative to the maximum [%] */
    double  f_ncc;
    /* relative state of free available carbon [-] */
    double  f_fac;
    /* drought stress factor [%] */
    double  f_h2o, d_f_h2o;
    /* upper height of vegetation (i.e., average height of highest vegetation cohort) [m] */
    double  height_max;
    /* height of canopy start (from the ground) [m] */
    double  height_at_canopy_start;
    /* potential (supported by tree dimension and stand density) leaf area index [m2 m-2] */ 
    double  lai_pot;
    /* maximum (supported by foliage and bud biomass) leaf area index [m2 m-2] */
    double  lai_max;

    /* Dry weight of reproductive tissue [kgDW m-2] */
    double  mBud;
    /* Dry weight of reproduction mass at the end of the last year [kgDW m-2] */
    double  mBudStart;
    /* Dry weight of species specific core (dead) wood [kgDW m-2] */
    double  mCor;
    /* Dry weight of dead structural matter [kgDW m-2] */
    double  dw_dst;
    /* Living foliage biomass [kgDW m-2] */
    double  mFol;
    /* Dry weight of dead foliage biomass [kgDW m-2] */
    double  dw_dfol;
    /* maximum foliage mass aimed at [kgDW m-2] */
    double  mFolMax;
    /* minimum foliage mass aimed at [kgDW m-2] */
    double  mFolMin;
    /* Living fine root biomass [kgDW m-2] */
    double  mFrt;
    /* sapwood biomass [kgDW m-2] */
    double  mSap;
    /* Dry weight of living structural matter [kgDW m-2] */
    double  dw_lst;
    
    /* nitrogen concentration in reproductive tissue [kgN kgDW-1] */
    double  ncBud;
    /* nitrogen concentration in heartwood [kgN kgDW-1] */
    double  ncCor;
    /* Nitrogen concentration in living foliage [kgN kgDW-1] */
    double  ncFol;
    /* nitrogen concentration in fine roots [kgN kgDW-1] */
    double  ncFrt;
    /* nitrogen concentration in sapwood [kgN kgDW-1] */
    double  ncSap;

    /* Amount of nitrogen of dead foliage [kgN m-2] */
    double  n_dfol;
    /* Amount of nitrogen of dead structural tissue [kgN m-2] */
    double  n_dst;
    /* Amount of nitrogen of living structural tissue [kgN m-2] */
    double  n_lst;

    /* nitrogen litter input from buds or reproductive tissue [kgN m-2] */
    double  nLitBud, d_nLitBud;
    /* nitrogen litter input from foliage [kgN m-2] */
    double  nLitFol, d_nLitFol;
    /* aboveground sapwood nitrogen from events [kgN m-2] */
    double  nLitWoodAbove, d_nLitWoodAbove;

    /* nitrogen retention from senescence per day [kgN] */
    double  n_retention, d_n_retention;   /*! [g:10^3] */

    /* sapwood area (at 1.3 m height) to foliage area ratio [-] */
    double  qsfa;
    /* sapwood to foliage biomass ratio [-] */
    double  qsfm;

    /* */
    double  psi_mean;
    /* */
    double  psi_thresh;
    /* */
    double  psi_pd;
    /* */
    double  psi_start;

    /* residual respiration from reproductive tissue [kgC m-1] */
    double  rBud, d_rBud;
    /* residual respiration from foliage [kgC m-1] */
    double  rFol, d_rFol;
    /* residual respiration from fine roots [kgC m-1] */
    double  rFrt, d_rFrt;
    /* total growth respiration [kgC m-1] */
    double  rGro, d_rGro;
    /* total growth respiration within the soil [kgC m-1] */
    double  rGroBelow, d_rGroBelow;
    /* total residual respiration [kgC m-1] */
    double  rRes, d_rRes;
    /* residual respiration from sapwood [kgC m-1] */
    double  rSap, d_rSap;
    /* residual respiration from sapwood [kgC m-1] */
    double  rSapBelow, d_rSapBelow;
    /* total transport and uptake respiration [kgC m-1] */
    double  rTra, d_rTra;

    /* senescence of reproductive tissue [kgDW m-2] */
    double  sBud, d_sBud;
    /* foliage senescence per time step [kgDW m-2] */
    double  sFol, d_sFol;
    /* fine root senescence per time step [kgDW m-2] */
    double  sFrt, d_sFrt;
    /* aboveground sapwood senescence from mortality events [kgDW m-2] */
    double  sWoodAbove, d_sWoodAbove;
    MoBiLE_PlantRateAttribute<double> sWoodAbovetest;

    /* weighted growing degree days [oC] */
    double  growing_degree_days;

    /* previous day water supply modifier */
    double  fwatOld;

    /* number of trees per ha [-] */
    double  tree_number;

    /* root aerenchyme transport coefficient */
    double  root_tc;

    /* resistance of water transport through the xylem (roots + stem) [MPa s m2 mol-1] */
    double  xylem_resistance;

    /* foliage biomass per age class  [kgDW] */
    double *  mFol_na;
    /* foliage senescence per age class per time step [kgDW m-2] */
    double *  sFol_na, * d_sFol_na;

    /* layer specific carbon uptake (photosynthesis) [kgC m-2] */
    double *  carbonuptake_fl, * d_carbonuptake_fl;
    /* layer specific intercellular concentration of CO2 and under standard conditions [umol mol-1] */
    double *  co2i_fl, * co2i_std_fl, * d_co2i_fl;
    /* layer specific leaf internal O2 concentration [umol m-2] */
    double *  o2i_fl, * o2i_std_fl;
    /* CO2 compensation point at 25oC per canopy layer and under standard conditions [umol m-2] */
    double *  co2comp25_fl, * co2comp25_std_fl;
    /* layer specific MEP [umol l-1] */
    double *  mep_fl;
    /* layer specific DMADP [umol l-1] */
    double *  dmadp_fl;
    /* layer specific DXP [umol l-1] */
    double *  dxp_fl;
    /* layer specific GDP [umol l-1] */
    double *  gdp_fl;
    /* layer specific IDP [umol l-1] */
    double *  idp_fl;
    /* layer specific isoprene emission [umol m-2] */
    double *  isoprene_emission_fl, * d_isoprene_emission_fl;
    /* layer specific monoterpene emission [umol m-2] */
    double *  monoterpene_emission_fl, * d_monoterpene_emission_fl;
    /* layer specific monoterpene emission from storages [umol m-2] */
    double *  monoterpene_s_emission_fl, * d_monoterpene_s_emission_fl;
    /* layer specific ovoc emission from storages [umol m-2] */
    double *  ovoc_emission_fl, * d_ovoc_emission_fl;
    /* Michaelis-Menten constant for CO2 reaction of rubisco per canopy layer and under standard conditions [umol mol-1 ubar-1] */
    double *  kco2_fl, *  kco2_std_fl;
    /* Michaelis-Menten constant for O2 reaction of rubisco per canopy layer and under standard conditions [umol mol-1 ubar-1] */
    double *  ko2_fl, *  ko2_std_fl;
    /* leaf area index [m2 m-2] */
    double *  lai_fl;
    /* relative amount of foliage per layer [%] */
    double *  fFol_fl;
    /* deposited nh3 in the canopy [kg m-2] */
    double *  nh3_fl;
    /* deposited nh4 in the canopy [kg m-2] */
    double *  nh4_fl;
    /* deposited no3 in the canopy [kg m-2] */
    double *  no3_fl;
    /* layer specific relative conductance [%] */
    double *  relativeconductance_fl, * d_relativeconductance_fl;
    /* specific foliage area [m2 kgDW-1] */
    double *  sla_fl;
    /* layer specific ATP production [umol m-2LA s-1] */
    double *  atp_fl;
    /* layer specific ATP pool [umol kgDW-1] */
    double *  atp_pool_fl;
    /* layer specific triose phosphate production [umol m-2LA s-1] */
    double *  tp_fl;
    /* layer specific triose phosphate pool [umol kgDW-1] */
    double *  tp_pool_fl;
    /* layer specific NADPH production [umol m-2 s-1] */
    double *  nadph_fl;
    /* layer specific NADPH pool [umol kgDW-1] */
    double *  nadph_pool_fl;
    /* activity state of isoprene synthase [nmol m-2 s-1] */
    double *  isoAct_fl;
    /* activity state of monoterpene synthase [nmol m-2 s-1] */
    double *  monoAct_fl;
    /* standard emission factor of isoprene (can be calculated from isoAct_fl) [ug gDW-1 h-1] */
    double *  ef_iso_fl;
    /* standard emission factor of monoterpenes (can be calculated from monoAct_fl) [ug gDW-1 h-1] */
    double *  ef_mono_fl;    
    /* electron transport capacity at 25oC for full light adjusted leaves [umol m-2 s-1] */
    double *  jAct25_fl;
    /* potential electron transport capacity per canopy layer under current light and temperature conditions [umol m-2 s-1] */
    double *  jPot_fl, * d_jPot_fl;
    /* electron transport capacity at 25oC per canopy layer [umol m-2 s-1] */
    double *  jMax25_fl;
    /* actual electron transport capacity per canopy layer and under standard conditions [umol m-2 s-1] */
    double *  jMax_fl, * jMax_std_fl, * d_jMax_fl;
    /* dark respiration at 25oC per canopy layer [umol m-2 s-1] */
    double *  rdAct25_fl;
    /* actual dark respiration per canopy layer [umol m-2 s-1] */
    double *  rd_fl;
    /* activity state of rubisco at 25oC for full light adjusted leaves [umol m-2 s-1] */
    double *  vcAct25_fl;
    /* activity state of rubisco with light and CO2 but not temperature restrictions [umol m-2 s-1] */
    double *  vcAct_fl;
    /* activity state of rubisco at 25oC per canopy layer [umol m-2 s-1] */
    double *  vcMax25_fl;
    /* actual activity state of rubisco per canopy layer and under standard conditions [umol m-2 s-1] */
    double *  vcMax_fl, * vcMax_std_fl;

    /* Fine root length per layer [m] */
    double *  rootlength_sl;
    /* relative amount of fine roots per layer per species. [%] */
    double *  fFrt_sl;
    /* fine root nitrogen litter input from events [kgN m-2] */
    double *  nLitFrt_sl, * d_nLitFrt_sl;
    /* fine root senescence from events [kgDW m-2] */
    double *  sFrt_sl, * d_sFrt_sl;
    /* belowground sapwood senescence from mortality events [kgDW m-2] */
    double *  sWoodBelow_sl, * d_sWoodBelow_sl;
    /* belowground sapwood nitrogen from events [kgN m-2] */
    double *  nLitWoodBelow_sl, * d_nLitWoodBelow_sl;

public:

    /*! @brief Resets all plant properties to zero */
    void reset();

    /*! @brief Leaf area index [m^2:m^-2] */
    double lai() const
        { return cbm::sum( this->lai_fl, this->nb_foliagelayers()); };

    /*! @page waterlibs
     *  @section watercyle_interception_capacity Interception capacity
     *  Interception capacity \f$ I_c \f$ is given by:
     *  \f[
     *  I_c = m_{wood, above} \cdot MWWM + LAI \cdot MWFM
     *  \f]
     */
    double interception_capacity() const
        { return this->aboveground_wood() * this->parameters()->MWWM() * cbm::M2_IN_HA +
                 this->lai() * this->parameters()->MWFM(); };

    /*! @details stand volume
     *  Stand volume \f$ V [m3:ha-1] \f$ is given by:
     *  \f[
     *  V = \frac{m_{wood, stem}}{DSAP}
     *  \f]
     *  \f$ V [m3:ha-1] \f$
     */
    double stand_volume() const
        { return this->stem_wood() / (this->parameters()->DSAP() * cbm::DM3_IN_M3) * cbm::M2_IN_HA; };

    double dead_structural_matter() const
        { return ( this->mCor + this->dw_dst); };

    double living_structural_matter() const
        { return ( this->mSap + this->dw_lst); };

    double foliage_matter() const
        { return ( this->mFol + this->dw_dfol); };

    /*! @brief Aboveground total living and dead biomass [kgDW m-2] */
    double aboveground_biomass() const
        { return ( this->aboveground_structural_matter() + this->mFol + this->dw_dfol + (this->parameters()->TUBER() ? 0.0 : this->mBud)); };

    /*! @brief Aboveground structural matter [kgDW m-2] */
    double aboveground_structural_matter() const
        { return ( this->mSap + this->mCor + this->dw_lst + this->dw_dst) * ( 1.0 - this->parameters()->UGWDF()); };
    
    /*! @brief Aboveground wood biomass [kgDW m-2] */
    double aboveground_wood() const
        { return ( this->mSap + this->mCor) * ( 1.0 - this->parameters()->UGWDF()); };

    /*! @brief Belowground total living and dead biomass [kgDW m-2] */
    double belowground_biomass() const
        { return ( this->belowground_structural_matter() + this->mFrt + (this->parameters()->TUBER() ? this->mBud : 0.0)); };

    /*! @brief Belowground structural matter [kgDW m-2] */
    double belowground_structural_matter() const
        { return ( this->mSap + this->mCor + this->dw_lst + this->dw_dst) * this->parameters()->UGWDF(); };
    
    /*! @brief Belowground wood biomass [kgDW m-2] */
    double belowground_wood() const
        { return ( this->mSap + this->mCor) * this->parameters()->UGWDF(); };

    /*! @brief Stem wood biomass [kgDW m-2] */
    double stem_wood() const
        { return this->aboveground_wood() * ( 1.0 - this->f_branch); };

    /*! @brief Branch wood biomass [kgDW m-2] */
    double branch_wood() const
        { return this->aboveground_wood() * this->f_branch; };

    /*! @brief Total biomass [kgDW m-2] */
    double total_biomass() const
        { return this->mSap + this->dw_lst +
                 this->mCor + this->dw_dst +
                 this->mFol + this->dw_dfol +
                 this->mBud + this->mFrt; };

    /*! @brief Nitrogen in corewood [kgN m-2] */
    double n_cor() const
        { return this->mCor * this->ncCor; }
    double nc_cor() const
        { return this->ncCor; }
    
    /*! @brief Nitrogen in sapwood [kgN m-2] */
    double n_sap() const
        { return this->mSap * this->ncSap; }
    double nc_sap() const
        { return this->ncSap; }
    
    /*! @brief Nitrogen in fine roots [kgN m-2] */
    double n_frt() const
        { return this->mFrt * this->ncFrt; }
    double nc_frt() const
        { return this->ncFrt; }
    
    /*! @brief Nitrogen in foliage [kgN m-2] */
    double n_fol() const
        { return this->mFol * this->ncFol; }
    double nc_fol() const
        { return this->ncFol; }
    
    /*! @brief Nitrogen in buds [kgN m-2] */
    double n_bud() const
        { return this->mBud * this->ncBud; }
    double nc_bud() const
        { return this->ncBud; }
    
    /*! @brief Nitrogen concentration of living structural tissue [kgN kgDW-1] */
    double nc_lst() const
        { return cbm::flt_greater_zero( this->mSap + this->dw_lst) ? (this->n_sap() + this->n_lst) / (this->mSap + this->dw_lst): 0.0; }

    /*! @brief Nitrogen concentration of dead structural tissue [kgN kgDW-1] */
    double nc_dst() const
        { return cbm::flt_greater_zero( this->mCor + this->dw_dst) ? (this->n_cor() + this->n_dst) / (this->mCor + this->dw_dst): 0.0; }

    /*! @brief Aboveground total nitrogen [kgN m-2] */
    double aboveground_nitrogen() const
        { return ( (this->n_sap() + this->n_lst + this->n_cor() + this->n_dst) * ( 1.0 - this->parameters()->UGWDF())
                  + this->n_fol() + this->n_dfol
                  + (this->parameters()->TUBER() ? 0.0 : this->n_bud())); };

    /*! @brief Belowground total nitrogen [kgN m-2] */
    double belowground_nitrogen() const
        { return ( (this->n_sap() + this->n_lst + this->n_cor() + this->n_dst) * this->parameters()->UGWDF()
                  + this->n_frt() + (this->parameters()->TUBER() ? this->n_bud() : 0.0)); };
    
    /*! @brief Total biomass [kgDW m-2] */
    double total_nitrogen() const
        { return this->n_sap() + this->n_lst + this->n_cor() + this->n_dst + this->n_fol() + this->n_dfol + this->n_bud() + this->n_frt(); };

    /*! @brief C/N ratio of entire plant [-] */
    double cn_ratio() const
        { return cbm::flt_greater_zero( total_nitrogen()) ? total_biomass() * cbm::CCDM / total_nitrogen() : 0.0; };

    /* Carbon of reproductive tissue [kgC m-2] */
    double  c_fru() const
        { return this->mBud * cbm::CCDM; };

    /* Carbon of dead structural matter [kgC m-2] */
    double  c_dst() const
        { return (this->mCor + this->dw_dst) * cbm::CCDM; };
    
    /* Carbon of living foliage biomass [kgC m-2] */
    double  c_fol() const
        { return this->mFol * cbm::CCDM; };
    
    /* Carbon of dead foliage biomass [kgC m-2] */
    double  c_dfol() const
        { return this->dw_dfol * cbm::CCDM; };
    
    /* Carbon of living fine root biomass [kgC m-2] */
    double  c_frt() const
        { return this->mFrt * cbm::CCDM; };

    /* Carbon of living structural matter [kgC m-2] */
    double  c_lst() const
        { return (this->mSap + this->dw_lst) * cbm::CCDM; };

    /*! @brief Maximum relative amount of foliage across canopy [%] */
    double f_fol_maximum() const
    {
        double f_fol_max( 0.0);
        for ( size_t  fl = 0;  fl < this->nb_foliagelayers(); fl++)
        {
            if ( cbm::flt_greater( this->fFol_fl[fl], f_fol_max))
            {
                f_fol_max = this->fFol_fl[fl];
            }
        }
        return f_fol_max;
    };

    /*! @brief Foliage biomass per canopy layer [kgDW m-2] */
    double m_fol_fl( size_t _fl) const
        { return this->mFol * this->fFol_fl[_fl]; }

    /*!
     * @details
     * Belowground respiration is the sum of:
     *  - Belowground growth respiration
     *  - Belowground maintenance respiration of fine roots
     *  - Belowground maintenance respiration of living structural matter
     *  - Belowground maintenance respiration of fruits/storage (for tuber species)
     *  - Belowground respiration associated with transport
     */
    double belowground_respiration()
        { return rGroBelow + rFrt + rTra + rSapBelow + (parameters()->TUBER() ? rBud : 0.0); };

    double d_belowground_respiration()
        { return d_rGroBelow + d_rFrt + d_rTra + d_rSapBelow + (parameters()->TUBER() ? d_rBud : 0.0); };


public:
    /* properties */
    ldate_t  seeding_date;
    bool  is_covercrop;
    double  initial_biomass;

private:
    MoBiLE_PlantSettings  m_plantsettings;
    friend class MoBiLE_PlantVegetation;
    MoBiLE_PlantVegetation const *  m_plantvegetation;
};

class LDNDC_API MoBiLE_PlantVegetation : public LD_PlantVegetation< MoBiLE_Plant >
{
public:
    MoBiLE_PlantVegetation( LD_PlantsParametersDB const * = NULL);
    ~MoBiLE_PlantVegetation();

    MoBiLE_Plant *  new_plant( MoBiLE_PlantSettings const *);
    lerr_t  delete_plant( char const * /*plant name*/);

    bool  is_family( MoBiLE_Plant const *, char const * /*family*/) const;
    bool  is_family( char const * /*plant type*/, char const * /*family*/) const;

    void  set_input( speciesparameters::input_class_speciesparameters_t const *  _speciesparameters)
        { this->m_speciesparameters = _speciesparameters; }
    MoBiLE_PlantParameters  get_parameters( char const * /*plant type*/);

    size_t  slot_cnt();

    double  canopy_height();
    size_t  canopy_layers_used();

    double  lai();
    double  lai_fl( size_t /* foliage layer */);
    double  dw_frt();
    double  mfrt_sl( size_t /* soil layer */);
    double  rootlength_sl( size_t /* soil layer */);
    double  area_cover();

private:
    speciesparameters::input_class_speciesparameters_t const *  m_speciesparameters;
};

typedef MoBiLE_PlantVegetation::Iterator PlantIterator;
typedef MoBiLE_PlantVegetation::GroupIterator< species::crop > CropIterator;
typedef MoBiLE_PlantVegetation::GroupIterator< species::grass > GrassIterator;
typedef MoBiLE_PlantVegetation::GroupIterator< species::wood > TreeIterator;

} /* namespace ldndc */


#endif /* !PLANT_H_ */
