/*!
 * @author
 *    steffen klatt,
 *    edwin haas
 */

#include  "legacy/mbe_legacymodel.h"

lflags_t const  ldndc::TMODE_DEFAULTS = TMODE_SUBDAILY|TMODE_PRE_DAILY|TMODE_POST_DAILY|TMODE_PRE_YEARLY|TMODE_POST_YEARLY;

ldndc::module_info_t::module_info_t(
        char const *  _id, char const *  _name,
        lflags_t  _timemode, lflags_t  _mflags)
        : id( _id), name( _name), timemode( _timemode),
          mflags( _mflags)
    { }

ldndc::MBE_LegacyModel::MBE_LegacyModel(
            MoBiLE_State *  _state, timemode_e  _timemode)
        : ldndc::MBE_LegacyModelBase( _state?_state->object_id():invalid_lid),
          m_state( _state),
          m_timemode( _timemode),
          m_active(( _timemode == TMODE_NONE) ? 0 : 1)
    { }

ldndc::MBE_LegacyModel::~MBE_LegacyModel()
    { }

timemode_e
ldndc::MBE_LegacyModel::timemode() const
    { return  m_timemode; }

bool
ldndc::MBE_LegacyModel::have_legacymodel(
        char const *  _module_id) const
{
    if ( this->__m_moduleinfos)
        { return this->__m_moduleinfos->have_module( _module_id); }
    return  false;
}

lerr_t
ldndc::MBE_LegacyModel::merge_options(
        mobile_module_options_t const &  _module_options)
{
    mobile_module_options_t::const_iterator  m_option = _module_options.begin();
    for ( ; m_option != _module_options.end();  ++m_option)
    {
        mobile_module_options_t::key_type const  option_key = m_option->first;
        mobile_module_options_t::value_type const  option_value = m_option->second;

        mobile_module_options_t::key_type const  option_regkey =
                mobile_module_options_t::key_type( registered_key_prefix) + option_key;
        if ( this->options_info()->exists( option_regkey))
        {
            this->m_options.insert( option_key, option_value);
        }
#ifdef  _DEBUG
        else
        {
            KLOGERROR( "[DEBUGGING HINT] seeing unregistered option",
                  "  [",option_key,"=",option_value,"]");
            return  LDNDC_ERR_FAIL;
        }
#endif
    }
    return  LDNDC_ERR_OK;
}

cbm::string_t const *
ldndc::MBE_LegacyModel::get_option(
        mobile_module_options_t::key_type const &  _option_key)
const
{
#ifdef  _DEBUG
    mobile_module_options_t::key_type  option_key =
        mobile_module_options_t::key_type( registered_key_prefix) + _option_key;
    if ( !this->options_info()->exists( option_key))
    {
        KLOGFATAL( "querying unregistered option  [",_option_key,"]");
    }
#endif
    return  this->m_options.get( _option_key);
}
cbm::string_t const *
ldndc::MBE_LegacyModel::get_option(
            char const *  _option_key)
const
{
    return  this->get_option(
        mobile_module_options_t::key_type( _option_key));
}

lerr_t
ldndc::MBE_LegacyModel::set_option( char const *  _option_key,
                cbm::string_t const *  _option_value)
{
    ldndc_kassert( _option_key);
    lerr_t  rc = LDNDC_ERR_OK;
    if ( _option_value)
    {
        rc = this->m_options.insert( _option_key, *_option_value);
    }
    return  rc;
}

ldndc::MoBiLE_ModuleFactoryBase::~MoBiLE_ModuleFactoryBase() { }

ldndc::MoBiLE_ModuleEnvelope::MoBiLE_ModuleEnvelope(
            ldndc::MBE_LegacyModel *  _module, char const *  _id)
        : module( _module), id( _id), mbe_state( NULL),
        mbe_tmmode( TMODE_NONE), mbe_info( NULL),
            iokcomm( NULL), factory( NULL)
{ }

ldndc::MoBiLE_ModuleEnvelope::MoBiLE_ModuleEnvelope(
            ldndc::MoBiLE_ModuleEnvelope const &  _me)
        : module( _me.module), id( _me.id), mbe_state( _me.mbe_state),
        mbe_tmmode( _me.mbe_tmmode), mbe_info( _me.mbe_info),
            iokcomm( _me.iokcomm), factory( _me.factory)
{ }

