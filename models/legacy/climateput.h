/*!
 * @brief
 *    updates climate subday and day buffers
 *
 * @author
 *    steffen klatt,
 *    edwin haas
 */

#ifndef  MBE_CLIMATEPUT_H_
#define  MBE_CLIMATEPUT_H_

#include  "mbe_legacymodel.h"
#include  "input/climate/climatetypes.h"
#ifdef  _HAVE_INPUTS_CLIENT_SIDE_SYNTHESIZE_OK
#  include  "synthesizers/climate/synth-climate.h"
#endif

namespace ldndc {

class  LDNDC_API  ClimateDayBufferUpdate  :  public  MBE_LegacyModel
{
    LMOD_EXPORT_MODULE_INFO(ClimateDayBufferUpdate,"sys:climate:copy-to-state","update subday and day climate buffers");
    public:
        ClimateDayBufferUpdate(
                MoBiLE_State *,
                cbm::io_kcomm_t *,
                timemode_e);

        ~ClimateDayBufferUpdate();

        lerr_t  configure( ldndc::config_file_t const *);

        lerr_t  initialize();

        /*!
         * @brief
         *    reads stream data items for current time
         *    step and stores them in the appropriate
         *    variables.
         */
        lerr_t  solve();

        lerr_t  finalize() { return  LDNDC_ERR_OK; }

        lerr_t  sleep() { return  LDNDC_ERR_OK; }
        lerr_t  wake() { return  LDNDC_ERR_OK; }

    private:
        input_class_climate_t const *  cl_;
        substate_microclimate_t &  mc_;

#ifdef  _HAVE_INPUTS_CLIENT_SIDE_SYNTHESIZE_OK
        ltime_t  cl_time_;
        bool  allow_synthesize_;
        climate::record::item_type  rec_s_buf_[climate::record::RECORD_SIZE];
        climate::record::item_type *  rec_s_;
        climate::record::item_type  rec_d_buf_[climate::record::RECORD_SIZE];
        climate::record::item_type *  rec_d_;

        ldndc::data_synthesizer::climate::lsynth_climate_t  cl_synth_;
#endif

        void  array_to_record(
                double const *, timemode_e);

        lerr_t  copy_climate_record_to_buffers_();
};

}

#endif /* !MBE_CLIMATEPUT_H_ */

