/*!
 * @file
 * @brief
 *    see header
 *
 * @author
 *    steffen klatt,
 *    edwin haas
 */

#include  "airchemistryput.h"
#include  "state/mbe_state.h"

#include  <time/cbm_time.h>
#include  <input/airchemistry/airchemistry.h>

LMOD_MODULE_INFO(AirChemistryDayBufferUpdate,TMODE_SUBDAILY,LMOD_FLAG_SYSTEM|LMOD_FLAG_MAINTENANCE|LMOD_FLAG_INITIALIZER);
ldndc::AirChemistryDayBufferUpdate::AirChemistryDayBufferUpdate(
        MoBiLE_State *  _state,
        cbm::io_kcomm_t *  _iokcomm,
        timemode_e  _timemode)
        : MBE_LegacyModel( _state, _timemode),

        ad_( _iokcomm->get_input_class< airchemistry::input_class_airchemistry_t >()),
        ac_( *_state->get_substate< substate_airchemistry_t >())
{
}

ldndc::AirChemistryDayBufferUpdate::~AirChemistryDayBufferUpdate()
{
    // no op
}

lerr_t
ldndc::AirChemistryDayBufferUpdate::configure(
        ldndc::config_file_t const *)
{
// sk:todo  allow generating input? (default: no)
    return  LDNDC_ERR_OK;
}

lerr_t
ldndc::AirChemistryDayBufferUpdate::initialize()
{
    return  LDNDC_ERR_OK;
}

lerr_t
ldndc::AirChemistryDayBufferUpdate::solve()
{
    if ( ad_)
    {
        return  copy_air_chemistry_record_to_buffers_();
    }

    return  LDNDC_ERR_OK;
}

lerr_t
ldndc::AirChemistryDayBufferUpdate::copy_air_chemistry_record_to_buffers_()
{

    cbm::sclock_t const &  clk( this->lclock_ref());

    if ( cbm::is_valid( ad_->conc_ch4_subday( clk)))
    {
        ac_.ts_ch4_concentration = ad_->conc_ch4_subday( clk);
    }
    else
    {
        ac_.ts_ch4_concentration = ad_->average_ch4();
    }
    ac_.ts_ch4_concentration_fl = ac_.ts_ch4_concentration;


    if ( cbm::is_valid( ad_->conc_co2_subday( clk)))
    {
        ac_.ts_co2_concentration = ad_->conc_co2_subday( clk);
    }
    else
    {
        ac_.ts_co2_concentration = ad_->average_co2();
    }
    ac_.ts_co2_concentration_fl = ac_.ts_co2_concentration;


    if ( cbm::is_valid( ad_->conc_nh3_subday( clk)))
    {
        ac_.ts_nh3_concentration = ad_->conc_nh3_subday( clk);
    }
    else
    {
        ac_.ts_nh3_concentration = ad_->average_nh3();
    }
    ac_.ts_nh3_concentration_fl = ac_.ts_nh3_concentration;


    if ( cbm::is_valid( ad_->conc_no_subday( clk)))
    {
        ac_.ts_no_concentration = ad_->conc_no_subday( clk);
    }
    else
    {
        ac_.ts_no_concentration = ad_->average_no();
    }
    ac_.ts_no_concentration_fl = ac_.ts_no_concentration;


    if ( cbm::is_valid( ad_->conc_no2_subday( clk)))
    {
        ac_.ts_no2_concentration = ad_->conc_no2_subday( clk);
    }
    else
    {
        ac_.ts_no2_concentration = ad_->average_no2();
    }
    ac_.ts_no2_concentration_fl = ac_.ts_no2_concentration;


    if ( cbm::is_valid( ad_->conc_o2_subday( clk)))
    {
        ac_.ts_o2_concentration = ad_->conc_o2_subday( clk);
    }
    else
    {
        ac_.ts_o2_concentration = ad_->average_o2();
    }


    if ( cbm::is_valid( ad_->conc_o3_subday( clk)))
    {
        ac_.ts_o3_concentration = ad_->conc_o3_subday( clk);
    }
    else
    {
        ac_.ts_o3_concentration = ad_->average_o3();
    }
    ac_.ts_o3_concentration_fl = ac_.ts_o3_concentration;

// sk:later    ac_.cNH4_ts = ad_->conc_nh4( clk);
// sk:later    ac_.cNO3_ts = ad_->conc_no3( clk);

    if ( clk.is_position( TMODE_PRE_DAILY))
    {
        if ( cbm::is_valid( ad_->conc_ch4_day( clk)))
        {
            ac_.nd_ch4_concentration = ad_->conc_ch4_day( clk);
        }
        else
        {
            ac_.nd_ch4_concentration = ad_->average_ch4();
        }
        ac_.nd_ch4_concentration_fl = ac_.nd_ch4_concentration;


        if ( cbm::is_valid( ad_->conc_co2_day( clk)))
        {
            ac_.nd_co2_concentration = ad_->conc_co2_day( clk);
        }
        else
        {
            ac_.nd_co2_concentration = ad_->average_co2();
        }
        ac_.nd_co2_concentration_fl = ac_.nd_co2_concentration;


        if ( cbm::is_valid( ad_->conc_nh3_day( clk)))
        {
            ac_.nd_nh3_concentration = ad_->conc_nh3_day( clk);
        }
        else
        {
            ac_.nd_nh3_concentration = ad_->average_nh3();
        }
        ac_.nd_nh3_concentration_fl = ac_.nd_nh3_concentration;

        
        if ( cbm::is_valid( ad_->conc_no_day( clk)))
        {
            ac_.nd_no_concentration = ad_->conc_no_day( clk);
        }
        else
        {
            ac_.nd_no_concentration = ad_->average_no();
        }
        ac_.nd_no_concentration_fl = ac_.nd_no_concentration;


        if ( cbm::is_valid( ad_->conc_no2_day( clk)))
        {
            ac_.nd_no2_concentration = ad_->conc_no2_day( clk);
        }
        else
        {
            ac_.nd_no2_concentration = ad_->average_no2();
        }
        ac_.nd_no2_concentration_fl = ac_.nd_no2_concentration;


        if ( cbm::is_valid( ad_->conc_o2_day( clk)))
        {
            ac_.nd_o2_concentration = ad_->conc_o2_day( clk);
        }
        else
        {
            ac_.nd_o2_concentration = ad_->average_o2();
        }


        if ( cbm::is_valid( ad_->conc_o3_day( clk)))
        {
            ac_.nd_o3_concentration = ad_->conc_o3_day( clk);
        }
        else
        {
            ac_.nd_o3_concentration = ad_->average_o3();
        }
        ac_.nd_o3_concentration_fl = ac_.nd_o3_concentration;


        ac_.nd_nh4_dry_deposition = ad_->conc_nh4dry_day( clk);
        ac_.nd_no3_dry_deposition = ad_->conc_no3dry_day( clk);

        ac_.nd_nh4_wet_deposition = ad_->conc_nh4_day( clk);
        ac_.nd_no3_wet_deposition = ad_->conc_no3_day( clk);

    }


    return  LDNDC_ERR_OK;
}

