/*!
 * @brief
 *    common stuff for output modules
 *
 * @author
 *    steffen klatt (created on: may 24, 2016)
 */

#include  "legacy/mbe_legacyoutputmodel.h"

ldndc::MBE_LegacyOutputModel::MBE_LegacyOutputModel(
    MoBiLE_State *  _state,
    cbm::io_kcomm_t *  _io_kcomm, timemode_e  _timemode)
    : MBE_LegacyModel( _state, _timemode),
      m_sif( _io_kcomm)
    { }

ldndc::MBE_LegacyOutputModel::~MBE_LegacyOutputModel()
    { }

lerr_t
ldndc::MBE_LegacyOutputModel::set_metaflags(
    ldndc::config_file_t const *  _cf, lflags_t const &  _flags)
    { return  this->m_sif.set_metaflags( _cf, info().id, _flags); }

lflags_t
ldndc::MBE_LegacyOutputModel::get_metaflags() const
    { return  this->m_sif.get_metaflags(); }

lerr_t
ldndc::MBE_LegacyOutputModel::write_fixed_record(
    ldndc::sink_handle_t *  _sink, void **  _data)
    { return  this->m_sif.write_fixed_record( _sink, _data, this->lclock()); }
lerr_t
ldndc::MBE_LegacyOutputModel::write_fixed_zero_record(
    ldndc::sink_handle_t *  _sink)
    { return  this->m_sif.write_fixed_zero_record( _sink, this->lclock()); }

