/*!
 * @brief
 *    kind of helper module to perform additional tasks
 *    before user modules are run.
 *
 * @author
 *    Steffen Klatt
 *    Edwin Haas
 */

#include  "dayinitializer.h"
#include  "state/mbe_state.h"

#include  <input/soillayers/soillayers.h>
#include  <input/species/species.h>

#include  <scientific/meteo/ld_meteo.h>

LMOD_MODULE_INFO(DailyInitializer,TMODE_PRE_DAILY,LMOD_FLAG_SYSTEM|LMOD_FLAG_MAINTENANCE|LMOD_FLAG_INITIALIZER);


ldndc::DailyInitializer::DailyInitializer(
        MoBiLE_State *  _state,
        cbm::io_kcomm_t *_io_kcomm,
        timemode_e  _timemode)
        : MBE_LegacyModel( _state, _timemode),

        ph_( _state->get_substate_ref< substate_physiology_t >()),
        sc_( _state->get_substate_ref< substate_soilchemistry_t >()),
        wc_( _state->get_substate_ref< substate_watercycle_t >()),
        mc_( _state->get_substate_ref< substate_microclimate_t >()),

        m_veg( &_state->vegetation),
        setup( _io_kcomm->get_input_class< input_class_setup_t >()),
        soillayers( _io_kcomm->get_input_class< input_class_soillayers_t >())
{
}


ldndc::DailyInitializer::~DailyInitializer()
{
}


lerr_t
ldndc::DailyInitializer::solve()
{
    reset_state_items_();

    return  LDNDC_ERR_OK;
}



void
ldndc::DailyInitializer::reset_state_items_()
{
    /* microclimate section */
    mc_.nd_parsun_fl = 0.0;
    mc_.nd_rad_a = 0.0;
    mc_.nd_temp_a = 0.0;
    mc_.nd_shortwaveradiation_out = 0.0;
    mc_.nd_longwaveradiation_out = 0.0;
    mc_.nd_sensible_heat_out = 0.0;
    
    mc_.nd_sunlitfoliagefraction_fl = 0.0;
    mc_.nd_temp_fl = 0.0;
    mc_.nd_shortwaveradiation_fl = 0.0;
    mc_.nd_vpd_fl = 0.0;
    mc_.nd_win_fl = 0.0;

    /* physiology section */
    ph_.nd_nh3_uptake_fl = 0.0;
    ph_.nd_nox_uptake_fl = 0.0;

    if ( this->m_veg->size() > 0)
    {
        for( PlantIterator  vt = this->m_veg->begin(); vt != this->m_veg->end(); ++vt)
        {
            MoBiLE_Plant * p = *vt;

            p->d_dcFac = 0.0;
            p->d_dcFol = 0.0;
            p->d_dcBud = 0.0;
            p->d_dcSap = 0.0;
            p->d_dcFrt = 0.0;
            p->d_sFol = 0.0;
            p->d_sBud = 0.0;
            p->d_rFol = 0.0;
            p->d_rBud = 0.0;
            p->d_rSap = 0.0;
            p->d_rSapBelow = 0.0;
            p->d_rFrt = 0.0;
            p->d_rGro = 0.0;
            p->d_rGroBelow = 0.0;
            p->d_rTra = 0.0;
            p->d_rRes = 0.0;
            p->d_exsuLoss = 0.0;
            p->d_nLitFol = 0.0;
            p->d_nLitBud = 0.0;

            p->d_n_retention = 0.0;

            p->d_n2_fixation = 0.0;
            p->d_nox_uptake = 0.0;

            for ( size_t  fl = 0;  fl < setup->canopylayers();  ++fl)
            {
                p->d_relativeconductance_fl[fl] = 0.0;
                p->d_jMax_fl[fl] = 0.0;
                p->d_jPot_fl[fl] = 0.0;
                p->d_co2i_fl[fl] = 0.0;
                p->d_carbonuptake_fl[fl] = 0.0;
                p->d_isoprene_emission_fl[fl] = 0.0;
                p->d_monoterpene_emission_fl[fl] = 0.0;
                p->d_monoterpene_s_emission_fl[fl] = 0.0;
                p->d_ovoc_emission_fl[fl] = 0.0;
            }

            for ( size_t  na = 0;  na < p->nb_ageclasses();  ++na)
            {
                p->d_sFol_na[na] = 0.0;
            }

            p->d_sWoodAbove = 0.0;
            p->d_nLitWoodAbove = 0.0;

            for (size_t sl = 0; sl < soillayers->soil_layer_cnt(); ++sl)
            {
                p->d_sFrt_sl[sl] = 0.0;
                p->d_nLitFrt_sl[sl] = 0.0;
                p->d_sWoodBelow_sl[sl] = 0.0;
                p->d_nLitWoodBelow_sl[sl] = 0.0;
            }
        }
    }
}

