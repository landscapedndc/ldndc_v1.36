/*!
 * @file
 * 
 * @author
 *  David Kraus
 * @date
 *  May 6, 2014
 */

#ifndef  LD_CATIONEXCHANGECAPACITY_H_
#define  LD_CATIONEXCHANGECAPACITY_H_

#include  "ldndc.h"
#include  <ecosystemtypes.h>

namespace ldndc {

/*!
 * @brief
 * Detailed description provided @link soillibs_cationexchangecapacity here @endlink.
 *
 * @param _clay Clay content [%]
 * @param _sand Sand content [%]
 * @param _som Soil organic matter content [%]
 * @param _ph pH value [-]
 * @param _ecosystem Ecosystem type  [-]
 *
 * @return Cation exchange capacity [mol:g-1:1e-5]
 */
double LDNDC_API cation_exchange_capacity(
                                          double _clay,
                                          double _sand,
                                          double _som,
                                          double _ph,
                                          ecosystem_type_e _ecosystem);

}

#endif  /*  !LD_CATIONEXCHANGECAPACITY_H_  */

