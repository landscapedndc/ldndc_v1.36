/*!
 * @file
 * @author
 *  David Kraus
 * @date
 *  Nov, 2021
 */

#include  "soilchemistry/ld_effective_diffusion_coefficient.h"

/*!
 * @brief
 *  Returns effective diffusion coefficient depending on gas content.
 */
double
ldndc::d_eff_air_a( double const &_exp,
                    double const &_gas_content)
{
    return std::pow( _gas_content, _exp);
}

/*!
 * @brief
 *  Returns effective diffusion coefficient depending on gas content and porosity.
 */
double
ldndc::d_eff_air_b( double const &_exp_g,
                    double const &_exp_p,
                    double const &_gas_content,
                    double const &_porosity)
{
    return std::pow( _gas_content, _exp_g) / std::pow( _porosity, _exp_p);
}

/*!
 * @brief
 *  Returns effective diffusion coefficient after @cite millington:1961a
 */
double
ldndc::d_eff_air_millington_and_quirk_1961( double const &_gas_content,
                                            double const &_porosity)
{
    return std::pow( _gas_content, 10.0/3.0) / (_porosity * _porosity);
}

