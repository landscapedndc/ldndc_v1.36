/*!
 * @brief
 * 
 * @authors
 *  - Changsheng Li
 *  - Klaus Butterbach-Bahl
 *  - Ralf Kiese
 */

#include  "soilchemistry/dndc/soilchemistry-dndc.h"
#include  "soilchemistry/ld_allocatelitter.h"
#include  "soilchemistry/ld_litterheight.h"
#include  "soilchemistry/ld_transport.h"

#include  <input/event/event.h>
#include  <input/siteparameters/siteparameters.h>
#include  <input/soillayers/soillayers.h>
#include  <input/species/species.h>

#include  <constants/cbm_const.h>
#include  <math/cbm_math.h>
#include  <utils/cbm_utils.h>

#include  <cfgfile/cbm_cfgfile.h>
#include  <scientific/meteo/ld_meteo.h>

LMOD_MODULE_INFO(SoilChemistryDNDC,TMODE_POST_DAILY,LMOD_FLAG_USER);

REGISTER_OPTION(SoilChemistryDNDC, output,"Controls if any output is written  [bool] {on}");
REGISTER_OPTION(SoilChemistryDNDC, outputdaily,"Controls if daily output is written  [bool] {on}");

namespace ldndc {

const double  SoilChemistryDNDC::TNORM = 293.2;
const double  SoilChemistryDNDC::HLIM = 0.05;
const unsigned int  SoilChemistryDNDC::IMAX_S = 24;
const unsigned int  SoilChemistryDNDC::NN = 60;
const double  SoilChemistryDNDC::FRN2 = 0.9;

SoilChemistryDNDC::SoilChemistryDNDC(
                MoBiLE_State *  _state,
                cbm::io_kcomm_t *  _io,
                timemode_e  _timemode)
              : MBE_LegacyModel(
              _state,
              _timemode),

              io_kcomm( _io),

              gw_( NULL),
              sipar_( _io->get_input_class_ref< input_class_siteparameters_t >()),
              sl_( _io->get_input_class_ref< input_class_soillayers_t >()),

              sb_( _state->get_substate_ref< substate_surfacebulk_t >()),
              ac_( _state->get_substate_ref< substate_airchemistry_t >()),
              mc_( _state->get_substate_ref< substate_microclimate_t >()),
              ph_( _state->get_substate_ref< substate_physiology_t >()),
              sc_( _state->get_substate_ref< substate_soilchemistry_t >()),
              wc_( _state->get_substate_ref< substate_watercycle_t >()),

              m_veg( &_state->vegetation),

              timemode_( _timemode),
              output_writer( _state, _io, _timemode),
              output_buffer( NULL),

              m_eventfertilize( _state, _io, _timemode),
              m_eventtill( _state, _io, _timemode),

              m_PlantCounter( 0),

              d_soil_eff_( NULL),
              tdma_solver_( NULL),

              crb_l( sl_.soil_layer_cnt(), 0.0),
              crb_r( sl_.soil_layer_cnt(), 0.0),
              hdc_l( sl_.soil_layer_cnt(), 0.0),
              hdc_r( sl_.soil_layer_cnt(), 0.0),

              p_o2_sl( sl_.soil_layer_cnt(), 0.0),
              fdeni( sl_.soil_layer_cnt(), 0.0),

              day_co2( sl_.soil_layer_cnt(), 0.0),
              co2_auto_sl( sl_.soil_layer_cnt(), 0.0),
              anvf_old( sl_.soil_layer_cnt(), 0.0),
              nitrify_n2o( sl_.soil_layer_cnt(), 0.0),

              communicate_sl( sl_.soil_layer_cnt(), 0.0),
    
              factorA( sl_.soil_layer_cnt()),
              factorB( sl_.soil_layer_cnt()),
              flux_chem_no_sl( sl_.soil_layer_cnt()),
              flux_chem_n2o_sl( sl_.soil_layer_cnt())
{
    anvf_old = 0.0;
    
    day_mineral_litter = 0.0;
    day_mineral_humus = 0.0;
    day_mineral_microbes = 0.0;
    day_mineral_aorg = 0.0;
    day_mineral_humads = 0.0;

    day_no3_groundwater_access = 0.0;
    year_no3_groundwater_access = 0.0;

    this->bc_warn = true;
    this->bc_tolerance = 0.0;

    isotope.io_kcomm = this->io_kcomm;

    waterflux_sl = new double[sl_.soil_layer_cnt()];
    accumulated_waterflux_old_sl = new double[sl_.soil_layer_cnt()];
    for (size_t sl = 0; sl < sl_.soil_layer_cnt(); ++sl)
    {
        waterflux_sl[sl] = 0.0;
        accumulated_waterflux_old_sl[sl] = 0.0;
    }
}



SoilChemistryDNDC::~SoilChemistryDNDC()
{
    if ( tdma_solver_) delete  tdma_solver_;
    if ( d_soil_eff_) delete[]  d_soil_eff_;
    if ( waterflux_sl) delete[]  waterflux_sl;
    if ( accumulated_waterflux_old_sl)
        delete[]  accumulated_waterflux_old_sl;

    if ( output_buffer)
    {
        LD_Allocator->deallocate( output_buffer);
    }
}



lerr_t
SoilChemistryDNDC::configure(
        ldndc::config_file_t const *  _cf)
{
    lerr_t  rc_conf = LDNDC_ERR_FAIL;

    this->bc_warn = _cf->have_balance_check();
    this->bc_tolerance = _cf->balance_tolerance();

    this->output_buffer = NULL;

#ifdef  MoBiLE_Isotopes
    Isotopes::keybuffer_t  itemkey;
    Isotopes::keyname( itemkey, "hours");
    this->io_kcomm->get_scratch()->set( itemkey, IMAX_S);
#endif  /* MoBiLE_Isotopes */

    /* configure SoilChemistryDNDC specific output */
    if ( _cf && _cf->have_output())
    {
        /* forward option settings */
        this->output_writer.set_option( "output", this->get_option( "output"));
        this->output_writer.set_option( "outputdaily", this->get_option( "outputdaily"));

        lerr_t const  rc_owconf = this->output_writer.configure( _cf);
        if ( !this->output_writer.active())
        {
            rc_conf = LDNDC_ERR_OK;
        }
        else if ( rc_owconf == LDNDC_ERR_OK)
        {
            size_t const  record_sz = this->output_writer.max_record_size();
            if ( record_sz > 0u && cbm::is_valid( record_sz))
            {
                this->output_buffer =
                    LD_Allocator->allocate_type< ldndc_flt64_t >( record_sz);
                if ( this->output_buffer)
                {
                    lerr_t  rc_owini = this->output_writer.initialize();
                    if ( rc_owini)
                    {
                        LD_Allocator->deallocate( this->output_buffer);
                        this->output_buffer = NULL;
                    }
                    rc_conf = rc_owini ? LDNDC_ERR_FAIL : LDNDC_ERR_OK;
                }
                else
                {
                    KLOGERROR( "nomem");
                }
            }
        }
    }
    else
    {
        rc_conf = LDNDC_ERR_OK;
    }
    return  rc_conf;
}



lerr_t
SoilChemistryDNDC::initialize()
{
    this->m_eventfertilize.set_object_id( this->object_id());
    this->m_eventtill.set_object_id( this->object_id());

    size_t  N( sl_.soil_layer_cnt());

    tdma_solver_ = new solver_tridiagonal_t( N+2);

    d_soil_eff_ = new double[N];

    for ( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  sl++)
    {
        this->isotope.rewrite(sc_.urea_sl[sl],"ureainitialisation",sl);
        this->isotope.rewrite(sc_.C_lit1_sl[sl] / sipar_.RCNRVL() + sc_.C_lit2_sl[sl] / sipar_.RCNRL()
                             + sc_.C_lit3_sl[sl] / sipar_.RCNRR(),"litterinitialisation",sl);
        this->isotope.rewrite(sc_.C_aorg_sl[sl] * sipar_.RBO() / sipar_.RCNB()
                             + sc_.C_aorg_sl[sl] * (1.0 - sipar_.RBO()) / sipar_.RCNH(),"aorginitialisation",sl);
        this->isotope.rewrite(sc_.nh4_sl[sl],"nh4initialisation",sl);
        this->isotope.rewrite(sc_.N_hum_sl[sl],"humusinitialisation",sl);
        this->isotope.rewrite(sc_.C_micro1_sl[sl] / sipar_.RCNB(),"micro1initialisation",sl);
        this->isotope.rewrite(sc_.C_micro3_sl[sl] / sipar_.RCNB(),"micro3initialisation",sl);
        this->isotope.rewrite(sc_.nh3_liq_sl[sl],"nh3initialisation",sl);
        this->isotope.rewrite(sc_.clay_nh4_sl[sl],"clayinitialisation",sl);
        this->isotope.rewrite(sc_.no3_sl[sl],"no3initialisation",sl);
        this->isotope.rewrite(sc_.an_no3_sl[sl],"no3aninitialisation",sl);
        this->isotope.rewrite(sc_.no2_sl[sl],"no2initialisation",sl);
        this->isotope.rewrite(sc_.an_no2_sl[sl],"no2aninitialisation",sl);
        this->isotope.rewrite(sc_.no_sl[sl],"noinitialisation",sl);
        this->isotope.rewrite(sc_.an_no_sl[sl],"noaninitialisation",sl);
        this->isotope.rewrite(sc_.n2o_sl[sl],"n2oinitialisation",sl);
        this->isotope.rewrite(sc_.an_n2o_sl[sl],"n2oaninitialisation",sl);
    }

    /* check for groundwater input */
    cbm::source_descriptor_t  gw_source_info;
    lid_t  gw_id = io_kcomm->get_input_class_source_info< input_class_groundwater_t >( &gw_source_info);
    if ( gw_id != invalid_lid)
    {
        gw_ = io_kcomm->acquire_input< input_class_groundwater_t >( &gw_source_info);
    }
    else
    {
        gw_ = NULL;
    }

    for ( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
    {
        p_o2_sl[sl] = sc_.o2_sl[sl];
        communicate_sl[sl] = sc_.no3_sl[sl] + sc_.an_no3_sl[sl];
    }

    return  LDNDC_ERR_OK;
}



lerr_t
SoilChemistryDNDC::solve()
{
    RETURN_IF_NOT_OK( SCDNDC_step_initialize_());

    double  bc_c = 0.0, bc_n = 0.0;
    SCDNDC_balance_check( 0, &bc_c, &bc_n);
    for ( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  sl++)
    {
//        this->isotope.rewrite(ph_.nd_nh4_uptake_sl[sl],"nh4uptake",sl);
//        this->isotope.rewrite(ph_.nd_no3_uptake_sl[sl]*sc_.no3_sl[sl]/(sc_.no3_sl[sl] + sc_.an_no3_sl[sl]),"no3uptake",sl);
//        this->isotope.rewrite(ph_.nd_no3_uptake_sl[sl]*sc_.an_no3_sl[sl]/(sc_.no3_sl[sl] + sc_.an_no3_sl[sl]),"an_no3uptake",sl);
//        this->isotope.rewrite(ph_.nd_nh3_uptake_sl[sl],"nh3uptake",sl);
    }
    for ( PlantIterator vt = this->m_veg->begin(); vt != this->m_veg->end(); ++vt)
    {
        this->isotope.cumulative_write((*vt)->d_n2_fixation,"n2fixation",0);
    }

    /* act on management events */
    {
        for ( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  sl++)
        {
            this->isotope.cumulative_write(-sc_.C_lit1_sl[sl] / sipar_.RCNRVL() - sc_.C_lit2_sl[sl] / sipar_.RCNRL()
                    - sc_.C_lit3_sl[sl] / sipar_.RCNRR(),"fertlitter",sl);
            this->isotope.cumulative_write((-sc_.C_aorg_sl[sl] * sipar_.RBO() / sipar_.RCNB()
                        - sc_.C_aorg_sl[sl] * (1.0 - sipar_.RBO()) / sipar_.RCNH()),"fertNaorg",sl);
            this->isotope.cumulative_write(-sc_.nh4_sl[sl],"fertnh4",sl);
            this->isotope.cumulative_write(-sc_.urea_sl[sl],"ferturea",sl);
            this->isotope.cumulative_write(-sc_.no3_sl[sl],"fertno3",sl);
        }

        lerr_t  rc_fertilize = this->m_eventfertilize.solve();
        if ( rc_fertilize)
            { return LDNDC_ERR_FAIL; }

        for ( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  sl++)
        {
            this->isotope.cumulative_write(sc_.C_lit1_sl[sl] / sipar_.RCNRVL() + sc_.C_lit2_sl[sl] / sipar_.RCNRL()
                    + sc_.C_lit3_sl[sl] / sipar_.RCNRR(),"fertlitter",sl);
            this->isotope.cumulative_write((sc_.C_aorg_sl[sl] * sipar_.RBO() / sipar_.RCNB()
                        + sc_.C_aorg_sl[sl] * (1.0 - sipar_.RBO()) / sipar_.RCNH()),"fertNaorg",sl);
            this->isotope.cumulative_write(sc_.nh4_sl[sl],"fertnh4",sl);
            this->isotope.cumulative_write(sc_.urea_sl[sl],"ferturea",sl);
            this->isotope.cumulative_write(sc_.no3_sl[sl],"fertno3",sl);
        }

        lerr_t  rc_till = this->m_eventtill.solve();
        if ( rc_till)
            { return LDNDC_ERR_FAIL; }
    }

    SCDNDC_urea_hydrolysis();

    SCDNDC_autotrophic_respiration();
  
    SCDNDC_littering();
    this->m_PlantCounter = 0;

    SCDNDC_pertubation();

    SCDNDC_leaching();

    SCDNDC_split_aorg();

    SCDNDC_wood_decomposition();

    SCDNDC_litter_decomposition();

    SCDNDC_humus_decomposition();

    SCDNDC_microbial_dynamics();

    SCDNDC_humads_decomposition();

    SCDNDC_nh4_nh3_equilibrium();

    SCDNDC_nh3_volatilization();

    SCDNDC_nitrification();

    SCDNDC_combine_aorg();

    SCDNDC_nh4_clay_equilibrium();

    SCDNDC_calc_sl_arrays();

    for ( size_t  hr = 0;  hr < IMAX_S;  hr++)
    {
        this->isotope.hour = hr;

        SCDNDC_o2_diffusion();

        SCDNDC_anvf();

        SCDNDC_denitrification();
     
        SCDNDC_gas_diffusion();

        SCDNDC_groundwater_access();
    }

    RETURN_IF_NOT_OK( SCDNDC_step_finalize_());

    SCDNDC_write_output();

    lerr_t rc = SCDNDC_balance_check( 1, &bc_c, &bc_n);
    if ( rc) { return rc; }

    return  LDNDC_ERR_OK;
}



lerr_t
SoilChemistryDNDC::SCDNDC_step_initialize_()
{
    day_co2 = 0.0;
    nitrify_n2o = 0.0;

    // module specific variables
    day_floor_no = 0.0;
    day_floor_n2o = 0.0;
    day_floor_an_no = 0.0;
    day_floor_an_n2o = 0.0;

    day_mineral_litter = 0.0;
    day_mineral_humus = 0.0;
    day_mineral_microbes = 0.0;
    day_mineral_aorg = 0.0;
    day_mineral_humads = 0.0;

    day_no3_groundwater_access = 0.0;

    n2o_atm = 0.0;
    no_atm = 0.0;

    if ( cbm::flt_greater_zero( ac_.nd_no_concentration_fl[0]))
    {
        no_atm = (ac_.nd_no_concentration_fl[0] / (1.0e6 * cbm::VGAS) * sc_.h_sl[0] * cbm::MN);
    }

    no_0        = no_atm;  // Surface no quantity (kgN m-2)
    an_no_0     = no_atm;  // Surface an_no quantity (kgN m-2)
    n2o_0       = n2o_atm; // Surface n2o quantity (kgN m-2)
    an_n2o_0    = n2o_atm; // Surface an_n2o quantity (kgN m-2)

    for ( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
    {
        double const  m1_plus_m2( sc_.C_micro1_sl[sl] + sc_.C_micro2_sl[sl]);
        if ( m1_plus_m2 < cbm::DIFFMAX)
        {
            sc_.C_micro1_sl[sl] = 0.0;
            sc_.C_micro2_sl[sl] = 0.0;
            fdeni[sl] = 0.0;
        }
        else
        {
            // Heterotrophic microbial carbon with nitrifier and denitrifier (kgC m-2)
            sc_.C_micro1_sl[sl] = m1_plus_m2;

            // Denitrifier microbial fraction on heterotrophic microbes
            fdeni[sl] = sc_.C_micro2_sl[sl] / m1_plus_m2;
            sc_.C_micro2_sl[sl] = 0.0;
        }
    }
    
    // yearly variables
    if (lclock()->yearday() == 1u)
    {
        year_no3_groundwater_access = 0.0;

        update_litter_height( sl_, wc_, sc_);
    }

    for (size_t sl = 0; sl < sl_.soil_layer_cnt(); ++sl)
    {
        waterflux_sl[sl] = wc_.accumulated_waterflux_sl[sl] - accumulated_waterflux_old_sl[sl];
        accumulated_waterflux_old_sl[sl] = wc_.accumulated_waterflux_sl[sl];
    }

    return  LDNDC_ERR_OK;
}



/*!
 * @brief
 *    update of soilchemistry state
 */
lerr_t
SoilChemistryDNDC::SCDNDC_step_finalize_()
{
//    double  weightSum( 0.0);
//    for ( size_t  sl = 0;  sl < sl_.soil_layers_in_litter_cnt();  ++sl)
//    {
//        weightSum += (sc_.h_sl[sl] / sc_.depth_sl[sl]);
//    }

    for ( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
    {
        // Denitrifying heterotrophic microbes (kgC m-2)
        sc_.C_micro2_sl[sl] = (sc_.C_micro1_sl[sl] * sc_.anvf_sl[sl] * sipar_.DENIFRAC());
        
        // Nitrifying heterotrophic microbes (kgC m-2)
        sc_.C_micro1_sl[sl] -= sc_.C_micro2_sl[sl];                        
        
        // N in microbes (kgN m-2)
        sc_.N_micro_sl[sl] = (sc_.C_micro1_sl[sl] + sc_.C_micro2_sl[sl] + sc_.C_micro3_sl[sl]) / sipar_.RCNB();    
        
        // N in active organic material (kgN m-2)
        sc_.N_aorg_sl[sl] = (sc_.C_aorg_sl[sl] * sipar_.RBO() / sipar_.RCNB()
                             + sc_.C_aorg_sl[sl] * (1.0 - sipar_.RBO()) / sipar_.RCNH());
        
        // N in litter (kgN m-2)
        sc_.N_lit1_sl[sl] = (sc_.C_lit1_sl[sl] / sipar_.RCNRVL());
        sc_.N_lit2_sl[sl] = (sc_.C_lit2_sl[sl] / sipar_.RCNRL());
        sc_.N_lit3_sl[sl] = (sc_.C_lit3_sl[sl] / sipar_.RCNRR());

        // Soil organic matter
        sc_.som_sl[sl] = ((1.0 / cbm::CCORG) 
                          * (sc_.C_lit1_sl[sl] + sc_.C_lit2_sl[sl] + sc_.C_lit3_sl[sl]
                             + sc_.c_raw_lit_1_sl[sl] + sc_.c_raw_lit_2_sl[sl] + sc_.c_raw_lit_3_sl[sl]
                             + sc_.C_aorg_sl[sl] + sc_.C_hum_sl[sl]
                             + sc_.C_micro1_sl[sl] + sc_.C_micro2_sl[sl] + + sc_.C_micro3_sl[sl]));

        if ( sl >= sl_.soil_layers_in_litter_cnt())
        {
            // Organic fraction (-)
            double const  orgf( sc_.som_sl[sl] / (sc_.som_sl[sl] + sc_.min_sl[sl]));
            sc_.min_sl[sl] = std::max( 0.0, ( 1.0 - orgf) * sc_.dens_sl[sl] * cbm::DM3_IN_M3 * sc_.h_sl[sl]);
        }
        
        sc_.o2_sl[sl] = p_o2_sl[sl];
    }

    year_no3_groundwater_access += day_no3_groundwater_access;

    // gas production/consumption
    sc_.accumulated_no_emis  += (day_floor_no + day_floor_an_no);       // no emission
    sc_.accumulated_n2o_emis += (day_floor_n2o + day_floor_an_n2o);     // n2o emission

    sc_.accumulated_co2_emis_hetero += day_co2.sum();                   // heterotrophic co2 emission

    return  LDNDC_ERR_OK;
}



/*!
 * @brief
 * calculates the urea hydrolysis in the soil
 */
void
SoilChemistryDNDC::SCDNDC_urea_hydrolysis()
{
    for ( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
    {
        if ( cbm::flt_greater_zero( sc_.urea_sl[sl]))
        {
            /* effective soil temperature */
            double  eff_temp(( std::max( 0.0, mc_.nd_temp_sl[sl]) + 5.0) * 0.6);

            /* urease */
            double  urease( cbm::bound( 0.2, (eff_temp / 25.0 + SCDNDC_get_wfps( sl)) * 0.8, 0.9));

            /* hydrolized urea */
            double  urea_hydro( sc_.urea_sl[sl] * urease);

            sc_.nh4_sl[sl] += urea_hydro;
            sc_.urea_sl[sl] -= urea_hydro;
            
            this->isotope.rewrite(urea_hydro,"urea_hydro",sl);
        }
        else
        {
            sc_.urea_sl[sl] = 0.0;
        }
    }
}



/*!
 * @brief
 *      SCDNDC_autotrophic_respiration()
 *      Total autotrophic belowground respiration is calculated as the sum of belowground
 *      growth and maintanence respiration. The fraction of belowground from total growth
 *      respiration is determined from the contribution of fine root and belowground sapwood
 *      growth to total growth. Maintenance respiration consists of root and a fraction of
 *      sapwood maintanence respiration and the total uptake and transport respiration
 *      (assuming all nitrogen transformations to occur in the roots)
*/
void
SoilChemistryDNDC::SCDNDC_autotrophic_respiration()
{    
    double mFrtSum( 0.0);
    for ( PlantIterator vt = this->m_veg->begin(); vt != this->m_veg->end(); ++vt)
    {
        mFrtSum += (*vt)->mFrt;
    }

    if (( this->m_veg->size() > 0) && ( mFrtSum > 0.0))
    {
        double root_resp( 0.0);
        for ( PlantIterator vt = this->m_veg->begin(); vt != this->m_veg->end(); ++vt)
        {
            MoBiLE_Plant *p =(*vt);
            root_resp += p->d_belowground_respiration();
        }
        
        static double const  fts( 1.0 / (double)IMAX_S);
        for ( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  sl++)
        {
            double  root_co2_prod( 0.0);
            for ( PlantIterator vt = this->m_veg->begin(); vt != this->m_veg->end(); ++vt)
            {
                MoBiLE_Plant *p =(*vt);
                root_co2_prod += (p->fFrt_sl[sl] * p->mFrt);
            }
            root_co2_prod *= ( root_resp / mFrtSum);
            
            co2_auto_sl[sl] = ( root_co2_prod * fts);
            sc_.accumulated_co2_emis_auto += root_co2_prod;
        }
    }
    else
    {
        co2_auto_sl = 0.0;
    }
}



/*!
 * @brief
 *      Litter is distributed into very labile, labile, and resistant
 *      compartments across the soil profile according to its nitrogen content.
 */
void
SoilChemistryDNDC::SCDNDC_littering()
{

    //getting the initial amount of litter for the flux output
//    for ( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  sl++)
//    {
//        this->isotope.cumulative_write(-sc_.C_lit1_sl[sl] / sipar_.RCNRVL()- sc_.C_lit2_sl[sl] / sipar_.RCNRL()- sc_.C_lit3_sl[sl] / sipar_.RCNRR(),"litterproduction",sl);
//        this->isotope.cumulative_write(-sc_.nh4_sl[sl],"nh4production",sl);
//    }

    double add_c[CPOOL_CNT];
    double n_diff( 0.0);

    /* Raw litter fragmentation */
    
    for ( size_t  l = 0;  l < sl_.soil_layer_cnt();  l++)
    {
        double const c_raw_total( sc_.c_raw_lit_1_sl[l] + sc_.c_raw_lit_2_sl[l] + sc_.c_raw_lit_3_sl[l]);
        double const n_raw_total( sc_.n_raw_lit_1_sl[l] + sc_.n_raw_lit_2_sl[l] + sc_.n_raw_lit_3_sl[l]);
        
        if( cbm::flt_greater_zero( c_raw_total * n_raw_total))
        {
            allocate_litter( c_raw_total, n_raw_total,
                             sipar_.RCNRVL(), sipar_.RCNRL(), sipar_.RCNRR(), add_c, n_diff);
            
            sc_.C_lit1_sl[l] += add_c[CPOOL_VL];
            sc_.C_lit2_sl[l] += add_c[CPOOL_L];
            sc_.C_lit3_sl[l] += add_c[CPOOL_R];
            sc_.c_wood_sl[l] += add_c[CPOOL_B];
            sc_.nh4_sl[l] += n_diff;
            
            sc_.c_raw_lit_1_sl[l] = sc_.c_raw_lit_2_sl[l] = sc_.c_raw_lit_3_sl[l] = 0.0;
            sc_.n_raw_lit_1_sl[l] = sc_.n_raw_lit_2_sl[l] = sc_.n_raw_lit_3_sl[l] = 0.0;
        }
    }

    /* Dung and urine allocation from grazing */
    
    if (sc_.c_dung > 0.0 || sc_.n_dung > 0.0 || sc_.n_urine > 0.0)
    {
        double const  nh3_factor( cbm::bound( 0.2, mc_.nd_airtemperature / 40.0, 0.9));
        double const  nh3_emit_urine( sc_.n_urine * nh3_factor);
        double const  nh3_emit_dung( sc_.n_dung * nh3_factor);
        sc_.n_urine -= nh3_emit_urine;
        sc_.n_dung -= nh3_emit_dung;
        sc_.accumulated_nh3_emis += nh3_emit_dung + nh3_emit_urine;

        allocate_litter( sc_.c_dung, sc_.n_dung, sipar_.RCNRVL(), sipar_.RCNRL(), sipar_.RCNRR(), add_c, n_diff);
        sc_.C_lit1_sl[0] += add_c[CPOOL_VL];
        sc_.C_lit2_sl[0] += add_c[CPOOL_L];
        sc_.C_lit3_sl[0] += add_c[CPOOL_R];
        sc_.c_wood_sl[0] += add_c[CPOOL_B];

        sc_.nh4_sl[0] += n_diff;
        sc_.urea_sl[0] += sc_.n_urine;

        this->isotope.cumulative_write(n_diff,"grazing_nh4",0);
        this->isotope.cumulative_write(-n_diff,"nh4production",0);
        this->isotope.cumulative_write(sc_.n_urine,"grazing_urine",0);

        sc_.accumulated_c_fertilizer += sc_.c_dung;
        sc_.accumulated_n_fertilizer += (sc_.n_dung + sc_.n_urine);

        sc_.c_dung = 0.0;
        sc_.n_dung = 0.0;
        sc_.n_urine = 0.0;
    }


    /* Stubble fragmentation */

    if ( cbm::flt_greater_zero( sc_.c_stubble_lit1) ||
         cbm::flt_greater_zero( sc_.n_stubble_lit1))
    {
        // stubble removed after app. 90 days
        double const c_deplit1( sc_.c_stubble_lit1 * 0.025);
        double const n_deplit1( sc_.n_stubble_lit1 * 0.025);

        allocate_litter( c_deplit1, n_deplit1, sipar_.RCNRVL(), sipar_.RCNRL(), sipar_.RCNRR(), add_c, n_diff);
        sc_.C_lit1_sl[0] += add_c[CPOOL_VL];
        sc_.C_lit2_sl[0] += add_c[CPOOL_L];
        sc_.C_lit3_sl[0] += add_c[CPOOL_R];
        sc_.c_wood_sl[0] += add_c[CPOOL_B];
        sc_.nh4_sl[0] += n_diff;

        sc_.c_stubble_lit1 -= c_deplit1;
        sc_.n_stubble_lit1 -= n_deplit1;
    }
    else
    {
        sc_.c_stubble_lit1 = 0.0;
        sc_.n_stubble_lit1 = 0.0;
    }


    if ( cbm::flt_greater_zero( sc_.c_stubble_lit2) ||
         cbm::flt_greater_zero( sc_.n_stubble_lit2))
    {
        // stubble removed after app. 90 days
        double const c_deplit2( sc_.c_stubble_lit2 * 0.025);
        double const n_deplit2( sc_.n_stubble_lit2 * 0.025);

        allocate_litter( c_deplit2, n_deplit2, sipar_.RCNRVL(), sipar_.RCNRL(), sipar_.RCNRR(), add_c, n_diff);
        sc_.C_lit1_sl[0] += add_c[CPOOL_VL];
        sc_.C_lit2_sl[0] += add_c[CPOOL_L];
        sc_.C_lit3_sl[0] += add_c[CPOOL_R];
        sc_.c_wood_sl[0] += add_c[CPOOL_B];
        sc_.nh4_sl[0] += n_diff;

        sc_.c_stubble_lit2 -= c_deplit2;
        sc_.n_stubble_lit2 -= n_deplit2;
    }
    else
    {
        sc_.c_stubble_lit2 = 0.0;
        sc_.n_stubble_lit2 = 0.0;
    }


    if ( cbm::flt_greater_zero( sc_.c_stubble_lit3) ||
         cbm::flt_greater_zero( sc_.n_stubble_lit3))
    {
        // stubble removed after app. 90 days
        double const c_deplit3( sc_.c_stubble_lit3 * 0.025);
        double const n_deplit3( sc_.n_stubble_lit3 * 0.025);

        allocate_litter( c_deplit3, n_deplit3, sipar_.RCNRVL(), sipar_.RCNRL(), sipar_.RCNRR(), add_c, n_diff);
        sc_.C_lit1_sl[0] += add_c[CPOOL_VL];
        sc_.C_lit2_sl[0] += add_c[CPOOL_L];
        sc_.C_lit3_sl[0] += add_c[CPOOL_R];
        sc_.c_wood_sl[0] += add_c[CPOOL_B];
        sc_.nh4_sl[0] += n_diff;

        sc_.c_stubble_lit3 -= c_deplit3;
        sc_.n_stubble_lit3 -= n_deplit3;
    }
    else
    {
        sc_.c_stubble_lit3 = 0.0;
        sc_.n_stubble_lit3 = 0.0;
    }


//    //flux output
//    for ( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  sl++)
//    {
//        this->isotope.cumulative_write(sc_.C_lit1_sl[sl] / sipar_.RCNRVL()+ sc_.C_lit2_sl[sl] / sipar_.RCNRL()+ sc_.C_lit3_sl[sl] / sipar_.RCNRR(),"litterproduction",sl);
//        this->isotope.cumulative_write(sc_.nh4_sl[sl],"nh4production",sl);
//    }
}



/*!
 * @brief
 *
 */
void
SoilChemistryDNDC::SCDNDC_distribute_among_litter_layers(
                                                         double  _add_c[],
                                                         double  _add_nh4)
{
    // sk:TODO  balance check B3

    if ( sl_.soil_layers_in_litter_cnt() > 0)
    {
        double  weightSum( 0.0);
        for ( size_t  sl = 0;  sl < sl_.soil_layers_in_litter_cnt();  ++sl)
        {
            weightSum += (sc_.h_sl[sl] / sc_.depth_sl[sl]);
        }

        for ( size_t  sl = 0;  sl < sl_.soil_layers_in_litter_cnt();  ++sl)
        {
            double const fsl(( sc_.h_sl[sl] / sc_.depth_sl[sl]) / weightSum);
            sc_.C_lit1_sl[sl] += ( _add_c[CPOOL_VL] * fsl);
            sc_.C_lit2_sl[sl] += ( _add_c[CPOOL_L] * fsl);
            sc_.C_lit3_sl[sl] += ( _add_c[CPOOL_R] * fsl);
            sc_.nh4_sl[sl]    += ( _add_nh4 * fsl);
        }
    }
    else
    {
        // allocation of material into upper soil if no litter layers exist
        sc_.C_lit1_sl[0] += _add_c[CPOOL_VL];
        sc_.C_lit2_sl[0] += _add_c[CPOOL_L];
        sc_.C_lit3_sl[0] += _add_c[CPOOL_R];
        sc_.nh4_sl[0]    += _add_nh4;
    }
}



/*!
 * @brief
 *      CalcPertubation: transport of litter into deeper layers
 */
void
SoilChemistryDNDC::SCDNDC_pertubation()
{    
    // sk:TODO  balance check B5
    
    int  sl_max_1( (int)sl_.soil_layer_cnt()-1);
    for ( int  sl = sl_max_1;  sl >= 0;  --sl)
    {        
        double  fsl( 1.0);        // Integration correction?
        double const depth( sc_.depth_sl[sl] - sc_.h_sl[sl]);
        
        if ( depth > sipar_.PERTMAX())      // no litter transport below soil specific limit
        {
            fsl = 0.0;
            /* fsl = 0 means no leaching */
            continue;
        }
        else
        {
            // depth dependency instead decrease with layer
            if ( depth > sipar_.PSL_SC())
            {
                fsl = (sipar_.PSL_SC() / depth);
            }
        }
        
        {
            double const  leach_C( sipar_.PERTVL() * sc_.C_lit1_sl[sl] * fsl);  // transport depends on litter availability instead of nitrogen content of litter (rg)
            sc_.C_lit1_sl[sl] -= leach_C;
            this->isotope.cumulative_write(leach_C / sipar_.RCNRVL(),"perturbation",sl);
            
            if ( sl == sl_max_1)
            {
                sc_.accumulated_doc_leach += leach_C;
                sc_.accumulated_don_leach += (leach_C / sipar_.RCNRVL());
            }
            else
            {
                sc_.C_lit1_sl[sl+1] += leach_C;
            }
        }
        
        {
            double const  leach_C( sipar_.PERTL() * sc_.C_lit2_sl[sl] * fsl);
            sc_.C_lit2_sl[sl] -= leach_C;
            this->isotope.cumulative_write(leach_C / sipar_.RCNRL(),"perturbation",sl);
            
            if ( sl == sl_max_1)
            {
                sc_.accumulated_doc_leach += leach_C;
                sc_.accumulated_don_leach += (leach_C / sipar_.RCNRL());
            }
            else
            {
                sc_.C_lit2_sl[sl+1] += leach_C;
            }
        }
        
        {
            double const  leach_C( sipar_.PERTR() * sc_.C_lit3_sl[sl] * fsl);
            sc_.C_lit3_sl[sl] -= leach_C;
            this->isotope.cumulative_write(leach_C / sipar_.RCNRR(),"perturbation",sl);
            
            if ( sl == sl_max_1)
            {
                sc_.accumulated_doc_leach += leach_C;
                sc_.accumulated_don_leach += (leach_C / sipar_.RCNRR());
            }
            else
            {
                sc_.C_lit3_sl[sl+1] += leach_C;
            }
        }   
    }
    // sk:TODO  balance check B5 close
}



/*!
 * @brief
 *
 */
void
SoilChemistryDNDC::SCDNDC_nh4_clay_equilibrium()
{
    for ( size_t sl = 0; sl < sl_.soil_layer_cnt();  ++sl)
    {        
        double const  nh4_tot( sc_.clay_nh4_sl[sl] + sc_.nh4_sl[sl]);
        double const  cec( (1080.2 * sc_.clay_sl[sl] + 14.442) * 14.0 * sc_.dens_sl[sl] *sc_.h_sl[sl] * cbm::KG_IN_G);

        this->isotope.cumulative_write(sc_.nh4_sl[sl],"clay_nh4_flux",sl);
        
        sc_.clay_nh4_sl[sl] = 0.005 * cec * pow( 0.8, sc_.depth_sl[sl] * 50.0);

        if ( sc_.clay_nh4_sl[sl] > nh4_tot)
        {
            sc_.clay_nh4_sl[sl] = nh4_tot;
        }

        if ( sc_.clay_nh4_sl[sl] > 1.0/cbm::M2_IN_HA)
        {
            sc_.clay_nh4_sl[sl] = 1.0/cbm::M2_IN_HA;
        }

        sc_.nh4_sl[sl] = nh4_tot - sc_.clay_nh4_sl[sl];
        
        this->isotope.cumulative_write(-sc_.nh4_sl[sl],"clay_nh4_flux",sl);
    }
}



/*!
 * @brief
 *
 */
void
SoilChemistryDNDC::SCDNDC_wood_decomposition()
{
    double krc_w( 0.0 );
    if ( this->m_veg->size() > 0)
    {
        for ( PlantIterator vt = this->m_veg->begin(); vt != this->m_veg->end(); ++vt)
        {
            // decomposition rate of wood is defined by wood properties
            krc_w += (vt->KRC_WOOD() * (*vt)->f_area);
        }
    }
    else
    {
        krc_w = 0.00025;
    }
    
    double decomp_c_wood( krc_w * sc_.c_wood);
    double decomp_n_wood( krc_w * sc_.n_wood);
    double resp_wood( FRN2 * decomp_c_wood);
    double alloc_wood( decomp_c_wood - resp_wood);
    
    sc_.c_wood -= decomp_c_wood;
    sc_.n_wood -= decomp_n_wood;
    
    sc_.C_lit3_sl[0] += alloc_wood;
    day_co2[0] += resp_wood;

    if ( cbm::flt_greater( alloc_wood / sipar_.RCNRR(), decomp_n_wood))
    {
        // all nitrogen required for decomposition is taken from the air
        sc_.accumulated_n2_fixation += alloc_wood / sipar_.RCNRR() - decomp_n_wood;
    }
    else
    {
        sc_.nh4_sl[0] += cbm::bound_min( 0.0, decomp_n_wood - alloc_wood / sipar_.RCNRR());
        sc_.accumulated_n_mineral_sl[0] += cbm::bound_min( 0.0, decomp_n_wood - alloc_wood / sipar_.RCNRR());
    }

    for ( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
    {
        decomp_c_wood = krc_w * sc_.c_wood_sl[sl];
        decomp_n_wood = krc_w * sc_.n_wood_sl[sl];
        resp_wood = FRN2 * decomp_c_wood;
        alloc_wood = decomp_c_wood - resp_wood;
        
        sc_.c_wood_sl[sl] -= decomp_c_wood;
        sc_.n_wood_sl[sl] -= decomp_n_wood;
        
        sc_.C_lit3_sl[sl] += alloc_wood;
        day_co2[sl] += resp_wood;
        
        if ( cbm::flt_greater( alloc_wood / sipar_.RCNRR(), decomp_n_wood))
        {
            // all nitrogen required for decomposition is taken from the air
            sc_.accumulated_n2_fixation += alloc_wood / sipar_.RCNRR() - decomp_n_wood;
        }
        else
        {
            sc_.nh4_sl[sl] += cbm::bound_min( 0.0, decomp_n_wood - alloc_wood / sipar_.RCNRR());
            sc_.accumulated_n_mineral_sl[sl] += cbm::bound_min( 0.0, decomp_n_wood - alloc_wood / sipar_.RCNRR());
        }
    }
}



/*!
 * @brief
 *
 */
void 
SoilChemistryDNDC::SCDNDC_split_aorg()
{
    // Separation active organic substance into crb_l, crb_r (humus) and  hdc_l, hdc_r (humads)
    for ( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
    {
        /* labile humus, default 20% * 90% = 18% */
        crb_l[sl] = ( sc_.C_aorg_sl[sl] * sipar_.RBO() * sipar_.SRB());
        
        /* recalcitrant humus, default 20% * 10% = 2% */
        crb_r[sl] = ( sc_.C_aorg_sl[sl] * sipar_.RBO() * ( 1.0 - sipar_.SRB()));
        
        /* labile humads, default 80% * 16% = 12.8% */
        hdc_l[sl] = (( 1.0 - sipar_.RBO()) * sipar_.SHR() * sc_.C_aorg_sl[sl]);
        
        /* recalcitrant humads, default 80% * 84% = 67.2% */
        hdc_r[sl] = (( 1.0 - sipar_.RBO()) * ( 1.0 - sipar_.SHR()) * sc_.C_aorg_sl[sl]);
    }
}



/*!
 * @brief
 *
 */
void 
SoilChemistryDNDC::SCDNDC_combine_aorg()
{
    for ( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
    {
        // updating active organic material
        sc_.C_aorg_sl[sl] = crb_l[sl] + crb_r[sl] + hdc_r[sl] + hdc_l[sl];
        
        // nitrogen content of organic matter considering fixed fractions of crb and hdc
        double const naorgO(( crb_l[sl] + crb_r[sl]) / sipar_.RCNB() + (hdc_l[sl] + hdc_r[sl]) / sipar_.RCNH());
        
        // required nitrogen content after decomposition
        double const naorgN( sc_.C_aorg_sl[sl] * ( sipar_.RBO() / sipar_.RCNB() + (1.0 - sipar_.RBO()) / sipar_.RCNH()));
  
        this->isotope.cumulative_write(sc_.nh4_sl[sl],"combine_aorg_nh4flux",sl);
  
        if (naorgN > naorgO)
        {
            // nh4 pool is sufficient to supply the N deficit
            if (sc_.nh4_sl[sl] > ( naorgN - naorgO))
            {
                sc_.nh4_sl[sl] -= ( naorgN - naorgO);
            }
            // nh4 pool is not sufficient and N concentration in aorg will be
            // increased by partially transferring C into doc
            else if ( naorgN > 0.0)
            {
                sc_.doc_sl[sl]  += (sc_.C_aorg_sl[sl] * (1.0 - (naorgO + sc_.nh4_sl[sl]) / naorgN));
                sc_.C_aorg_sl[sl] *= ((naorgO + sc_.nh4_sl[sl]) / naorgN);
                sc_.nh4_sl[sl]  = 0.0;
            }
            // nh4 pool is not sufficient and there is no nitrogen in aorg
            // - all aorg will be thus transfered into doc
            else
            {
                sc_.doc_sl[sl] += sc_.C_aorg_sl[sl];
                sc_.C_aorg_sl[sl] = 0.0;
                sc_.nh4_sl[sl]  = 0.0;
            }
        }
        else
        {
            if ( naorgN < naorgO)
            {
                sc_.nh4_sl[sl] += ( naorgO-naorgN);
            }
        }
        
        this->isotope.cumulative_write(-sc_.nh4_sl[sl],"combine_aorg_nh4flux",sl);
    }
}



/*!
 * @brief
 *
 */
void
SoilChemistryDNDC::SCDNDC_denitrification()
{
    double crb( 0.0);                        // part of active organic carbon that originates from death microbes (kgC m-2)
    double fts( 1.0 / double( IMAX_S));
    
    //! Main loop across all soil layers
    for( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
    {
        // anaerobic volumetric fraction
        double const  anvf_diff( sc_.anvf_sl[sl] - anvf_old[sl]);
        
        size_t  ni( (size_t)( std::max( 1.0, sc_.h_sl[sl] / sipar_.PSL_SC())));// rg: number of iteration depends on soil layer thickness (more loops if thickness > PSL_SC)
        double const  fn( 1.0 / double(ni));

        // Iterative scheme for numerical time integration
        for( size_t  k = 0;  k < ni;  k++)
        {
            double  anvf_0( 1.0);
            
            double  trans_doc(0.0);
            double  trans_no3(0.0);
            double  trans_no2(0.0);
            double  trans_no(0.0);
            double  trans_n2o(0.0);
            
            if ( cbm::flt_equal_zero( anvf_diff))
            {
                // no op
            }
            else if( anvf_diff < 0.0)
            {
                trans_doc = sc_.an_doc_sl[sl] * fn;
                trans_no3 = sc_.an_no3_sl[sl] * fn;
                trans_no2 = sc_.an_no2_sl[sl] * fn;
                trans_no  = sc_.an_no_sl[sl] * fn;
                trans_n2o = sc_.an_n2o_sl[sl] * fn;
                
                anvf_0 = -anvf_diff / anvf_old[sl];
            }
            else
            {
                trans_doc = -sc_.doc_sl[sl] * fn;
                trans_no3 = -sc_.no3_sl[sl] * fn;
                trans_no2 = -sc_.no2_sl[sl] * fn;
                trans_no  = -sc_.no_sl[sl] * fn;
                trans_n2o = -sc_.n2o_sl[sl] * fn;
                
                anvf_0 = anvf_diff / ( 1.0 - anvf_old[sl]);
            }
            
            if ( anvf_0 < 1.0)
            {
                trans_doc *= anvf_0;
                trans_no3 *= anvf_0;
                trans_no2 *= anvf_0;
                trans_no  *= anvf_0;
                trans_n2o *= anvf_0;
            }
            
            this->isotope.subdaily_write(sc_.an_no3_sl[sl],"trans_no3",sl);
            this->isotope.subdaily_write(sc_.an_no2_sl[sl],"trans_no2",sl);
            this->isotope.subdaily_write(sc_.an_no_sl[sl],"trans_no",sl);
            this->isotope.subdaily_write(sc_.an_n2o_sl[sl],"trans_n2o",sl);

            if ( cbm::flt_equal_zero( anvf_diff))
            {
                // no op
            }
            else
            {
                sc_.an_doc_sl[sl] = cbm::bound_min( 0.0, sc_.an_doc_sl[sl] - trans_doc);
                sc_.doc_sl[sl] = cbm::bound_min( 0.0, sc_.doc_sl[sl] + trans_doc);
                
                sc_.an_no3_sl[sl] = cbm::bound_min( 0.0, sc_.an_no3_sl[sl] - trans_no3);
                sc_.no3_sl[sl] = cbm::bound_min( 0.0, sc_.no3_sl[sl] + trans_no3);
                
                sc_.an_no2_sl[sl] = cbm::bound_min( 0.0, sc_.an_no2_sl[sl] - trans_no2);
                sc_.no2_sl[sl] = cbm::bound_min( 0.0, sc_.no2_sl[sl] + trans_no2);
                
                sc_.an_no_sl[sl] = cbm::bound_min( 0.0, sc_.an_no_sl[sl] - trans_no);
                sc_.no_sl[sl] = cbm::bound_min( 0.0, sc_.no_sl[sl] + trans_no);
                
                sc_.an_n2o_sl[sl] = cbm::bound_min( 0.0, sc_.an_n2o_sl[sl] - trans_n2o);
                sc_.n2o_sl[sl] = cbm::bound_min( 0.0, sc_.n2o_sl[sl] + trans_n2o);
            }
            
            // 0.925 and 0.175 are maximum and minimum values for anaeroform parameters for equation max at sc_.anvf_sl[sl] = 0.375  //ralf
            // temperature dependend diffusion rate of between aerobic and anareobic soil fraction
            double const diffuse( cbm::bound( 0.0, 
                                               pow(((mc_.nd_temp_sl[sl] + cbm::D_IN_K) / TNORM),sipar_.TEXP()) 
                                               * ((0.925 - sc_.anvf_sl[sl]) * (0.175 + sc_.anvf_sl[sl])) * fn,
                                               0.99));
            
            /* Concept of the anaerobic baloon:
             * Transport of chemical species from anaerobic to aerobic soil environment and back
             * via the SCDNDC_diffusion(() method.
             * The SCDNDC_diffusion(() method will be called 5 times for: doc, no2, no3, no and n2o
             */
            
            SCDNDC_diffusion( sl, diffuse, sipar_.DIFF_C(), sc_.doc_sl[sl], sc_.an_doc_sl[sl]);
            SCDNDC_diffusion( sl, diffuse, sipar_.DIFF_N(), sc_.no2_sl[sl], sc_.an_no2_sl[sl]);
            SCDNDC_diffusion( sl, diffuse, sipar_.DIFF_N(), sc_.no3_sl[sl], sc_.an_no3_sl[sl]);
            SCDNDC_diffusion( sl, diffuse, sipar_.DIFF_N(), sc_.no_sl[sl], sc_.an_no_sl[sl]);
            SCDNDC_diffusion( sl, diffuse, sipar_.DIFF_N(), sc_.n2o_sl[sl], sc_.an_n2o_sl[sl]);

            this->isotope.subdaily_write(-sc_.an_no3_sl[sl],"trans_no3",sl);
            this->isotope.subdaily_write(-sc_.an_no2_sl[sl],"trans_no2",sl);
            this->isotope.subdaily_write(-sc_.an_no_sl[sl],"trans_no",sl);
            this->isotope.subdaily_write(-sc_.an_n2o_sl[sl],"trans_n2o",sl);           
        
            /* NOTE: Some differences occur between the versions 37K and 38A on the one side
             * and NOFRETETE on the other side. The NOFRETETE version diminishes N and
             * C pools weighted by demands if they are not sufficiently large
             */
            // carbon content of denitrifying microbes (kgC m-2 layer-1)
            double  deniC( fdeni[sl] * sc_.C_micro1_sl[sl]);    // denitrifier biomass
            sc_.C_micro1_sl[sl] -= deniC;                     // microbial biomass without denitrifiers (assuming the same CN ratio)
            
            // half optimum content of doc in soil solution for denitrifier activity (kgC m-2)
            double  c_K( 0.0);
            // half optimum content of nitrogen in soil solution for denitrifier activity (kgN m-2)
            double  n_K( 0.0);
            if (wc_.wc_sl[sl] > 0.0)
            {
                n_K = sipar_.DNDC_KMM_N_DENIT() * sc_.h_sl[sl];    // /ice_fact orig;   // FloE, rg: depends on water volume not soil volume
                c_K = sipar_.DNDC_KMM_C_DENIT() * sc_.h_sl[sl];    // /ice_fact orig    // FloE, rg: depends on water volume not soil volume
            }
            
            // Michaelis Menten factor for doc conversion between anaerob and aerob soil fractions.
            double  doc_MM( 0.0);
            doc_MM = (sc_.an_doc_sl[sl] / sc_.anvf_sl[sl]) / (c_K + sc_.an_doc_sl[sl] / sc_.anvf_sl[sl]);
            
            
            /* The effect of nitrogen shortages on denitrification rates depends on the
             * summarised anorganic nitrogen fractions and the optimal N content in the
             * soil solution for denitrifier activity \image html fact_n.jpg
             */
            // sum of anorganic n-compounds per layer (kgN m-2) (but see QUESTION(0.0)
            double  n_sum(( sc_.an_no3_sl[sl] + sc_.an_no2_sl[sl] + sc_.an_no_sl[sl] + sc_.an_n2o_sl[sl]) / sc_.micro2_act_sl[sl]);

            // nitrogen availability effect on denitrifier activity
            double  fact_n( n_sum / (n_K + n_sum));
            
            /* Temperature impact on denitrifier activity is assumed to be exponentially. */
            double  fact_t( cbm::bound_max( ldndc::meteo::Ft_pow(sipar_.TF_DEN1(),sipar_.TF_DEN2(),mc_.nd_temp_sl[sl]), 1.0));
            
            /* Activity of denitrifying microbes (1/day) depends on the maximum growth
             * rate of a population and the inhibition factors for temperature and
             * nitrogen shortage and the size of the anaerobic balloon
             */
            if (( fact_t > 0.0) && ( fact_n > 0.0))
            {
                sc_.micro2_act_sl[sl] += (sipar_.MUEMAX() * (2.0/(1.0/fact_t + 1.0/fact_n)) * sc_.anvf_sl[sl] ) - sc_.micro2_act_sl[sl]; // original
            }
            sc_.micro2_act_sl[sl] = cbm::bound( 0.0001, sc_.micro2_act_sl[sl], 1.0);
            
            // Denitrification process
            {
                // Amount of no3(-) converted to no2(-)
                double const  MAINTENANCE = sipar_.MNO3()*24.*fts;
                double const  MAXRGR      = sipar_.MUE_NO3()*24.*fts;
                double const  MAXYLD      = sipar_.EFF_NO3();
                double const  phk         = 1.0 - 0.99 / (1.0 + (double)exp(( sc_.ph_sl[sl] - sipar_.PHCRIT_NO3()) / sipar_.PHDELTA_NO3())); //DAUM et al.
                this->isotope.subdaily_write(sc_.an_no3_sl[sl] + deniC / sipar_.RCNB(),"deni_no3_no2",sl);
                this->isotope.subdaily_write(-deniC / sipar_.RCNB(),"deni_no3_mic",sl);
                double  f_MM( (sc_.an_no3_sl[sl] / sc_.anvf_sl[sl]) / (n_K + sc_.an_no3_sl[sl] / sc_.anvf_sl[sl]));
                sc_.accumulated_no3_denitrify_sl[sl] += sc_.an_no3_sl[sl];
                SCDNDC_conversion(sl,cbm::CNEFF,4.0*cbm::MN,n_sum,doc_MM,fn,MAINTENANCE,MAXRGR,MAXYLD,phk,f_MM,deniC,sc_.an_doc_sl,day_co2, sc_.an_no3_sl[sl], sc_.an_no2_sl[sl]);
                sc_.accumulated_no3_denitrify_sl[sl] -= sc_.an_no3_sl[sl];
                this->isotope.subdaily_write(-sc_.an_no3_sl[sl] - deniC / sipar_.RCNB(),"deni_no3_no2",sl);
                this->isotope.subdaily_write(deniC / sipar_.RCNB(),"deni_no3_mic",sl);
            }
            
            {
                // Amount of no2(-) converted to n2o
                double const  MAINTENANCE = sipar_.MNO2()*24.*fts;
                double const  MAXRGR      = sipar_.MUE_NO2()*24.*fts;
                double const  MAXYLD      = sipar_.EFF_NO2();
                double const  phk         = 1.0 - 0.99 / (1.0 + (double)exp(( sc_.ph_sl[sl] - sipar_.PHCRIT_NO2()) / sipar_.PHDELTA_NO2())); //model daum_prefect2 !!!
                this->isotope.subdaily_write(sc_.an_no2_sl[sl] + deniC / sipar_.RCNB(),"deni_no2_n2o",sl);
                this->isotope.subdaily_write(-deniC / sipar_.RCNB(),"deni_no2_mic_1",sl);
                double  f_MM( (sc_.an_no2_sl[sl] / sc_.anvf_sl[sl]) / (n_K + sc_.an_no2_sl[sl] / sc_.anvf_sl[sl]));
                sc_.accumulated_no2_denitrify_sl[sl] += sc_.an_no2_sl[sl];
                SCDNDC_conversion(sl,cbm::CNEFF,4.0*cbm::MN,n_sum,doc_MM,fn,MAINTENANCE,MAXRGR,MAXYLD,phk,f_MM,deniC,sc_.an_doc_sl,day_co2, sc_.an_no2_sl[sl], sc_.an_n2o_sl[sl]);
                sc_.accumulated_no2_denitrify_sl[sl] -= sc_.an_no2_sl[sl];
                this->isotope.subdaily_write(-sc_.an_no2_sl[sl] - deniC / sipar_.RCNB(),"deni_no2_n2o",sl);
                this->isotope.subdaily_write(deniC / sipar_.RCNB(),"deni_no2_mic_1",sl);
            }

            {
                // Amount of no2(-) converted to no
                double const  MAINTENANCE = sipar_.MNO2()*24.*fts;
                double const  MAXRGR      = sipar_.MUE_NO2()*24.*fts;
                double const  MAXYLD      = sipar_.EFF_NO2();
                double const  phk         = 1.0 - 0.99 / (1.0 + (double)exp(( sc_.ph_sl[sl] - sipar_.PHCRIT_NO3()) / sipar_.PHDELTA_NO3())); //DAUM et al.
                this->isotope.subdaily_write(sc_.an_no2_sl[sl] + deniC / sipar_.RCNB(),"deni_no2_no",sl);
                this->isotope.subdaily_write(-deniC / sipar_.RCNB(),"deni_no2_mic_2",sl);
                double  f_MM( (sc_.an_no2_sl[sl] / sc_.anvf_sl[sl]) / (n_K + sc_.an_no2_sl[sl] / sc_.anvf_sl[sl]));
                sc_.accumulated_no2_denitrify_sl[sl] += sc_.an_no2_sl[sl];
                SCDNDC_conversion(sl,cbm::CNEFF,4.0*cbm::MN,n_sum,doc_MM,fn,MAINTENANCE,MAXRGR,MAXYLD,phk,f_MM,deniC,sc_.an_doc_sl,day_co2, sc_.an_no2_sl[sl], sc_.an_no_sl[sl]);
                sc_.accumulated_no2_denitrify_sl[sl] -= sc_.an_no2_sl[sl];
                this->isotope.subdaily_write(-sc_.an_no2_sl[sl] - deniC / sipar_.RCNB(),"deni_no2_no",sl);
                this->isotope.subdaily_write(deniC / sipar_.RCNB(),"deni_no2_mic_2",sl);
            }
            
            {
                // Amount of no converted to n2o
                double const  MAINTENANCE = sipar_.MNO()*24.*fts;
                double const  MAXRGR      = sipar_.MUE_NO()*24.*fts;
                double const  MAXYLD      = sipar_.EFF_NO();
                double const  phk         = 1.0 - 0.99 / (1.0 + (double)exp(( sc_.ph_sl[sl] - sipar_.PHCRIT_NO2()) / sipar_.PHDELTA_NO2())); //model daum_prefect2 !!!
                this->isotope.subdaily_write(sc_.an_no_sl[sl] + deniC / sipar_.RCNB(),"deni_no_n2o",sl);
                this->isotope.subdaily_write(-deniC / sipar_.RCNB(),"deni_no_mic",sl);
                double  f_MM( (sc_.an_no_sl[sl] / sc_.anvf_sl[sl]) / (n_K + sc_.an_no_sl[sl] / sc_.anvf_sl[sl]));
                sc_.accumulated_no_denitrify_sl[sl] += sc_.an_no_sl[sl];
                SCDNDC_conversion(sl,cbm::CNEFF,4.0*cbm::MN,n_sum,doc_MM,fn,MAINTENANCE,MAXRGR,MAXYLD,phk,f_MM,deniC,sc_.an_doc_sl,day_co2, sc_.an_no_sl[sl], sc_.an_n2o_sl[sl]);
                sc_.accumulated_no_denitrify_sl[sl] -= sc_.an_no_sl[sl];
                this->isotope.subdaily_write(-sc_.an_no_sl[sl] - deniC / sipar_.RCNB(),"deni_no_n2o",sl);
                this->isotope.subdaily_write(deniC / sipar_.RCNB(),"deni_no_mic",sl);
            }
            
            {
                // Amount of n2o converted to n2
                double const  MAINTENANCE = sipar_.MN2O()*24.*fts;
                double const  MAXRGR      = sipar_.MUE_N2O()*24.*fts;
                double const  MAXYLD      = sipar_.EFF_N2O();
                double const  phk         = 1.0 - 0.99 / (1.0 + (double)exp(( sc_.ph_sl[sl] - sipar_.PHCRIT_N2O()) / sipar_.PHDELTA_N2O()));
                this->isotope.subdaily_write(sc_.an_n2o_sl[sl] + deniC / sipar_.RCNB(),"deni_n2o_n2",sl);
                this->isotope.subdaily_write(-deniC / sipar_.RCNB(),"deni_n2o_mic",sl);
                double  f_MM( (sc_.an_n2o_sl[sl] / sc_.anvf_sl[sl]) / (n_K + sc_.an_n2o_sl[sl] / sc_.anvf_sl[sl]));
                sc_.accumulated_n2o_denitrify_sl[sl] += sc_.an_n2o_sl[sl];
                SCDNDC_conversion(sl,cbm::CNEFF,4.0*cbm::MN,n_sum,doc_MM,fn,MAINTENANCE,MAXRGR,MAXYLD,phk,f_MM,deniC,sc_.an_doc_sl,day_co2, sc_.an_n2o_sl[sl], sc_.accumulated_n2_emis);
                sc_.accumulated_n2o_denitrify_sl[sl] -= sc_.an_n2o_sl[sl];
                this->isotope.subdaily_write(-sc_.an_n2o_sl[sl] - deniC / sipar_.RCNB(),"deni_n2o_n2",sl);
                this->isotope.subdaily_write(deniC / sipar_.RCNB(),"deni_n2o_mic",sl);
            }
            
            // Calculate the chemodenitrification for this soil layer
            SCDNDC_chemodenitrification( sl, fts);
            
            // denitrifier dynamics
            // death rate of microbes (denitrifiers) (kgC m-2)
            // 1 percent of denitrifiers stay alive whatever happens (rg)
            double const  deni_death( cbm::bound_max( sipar_.AMAXX() * fts * fn * deniC * sc_.micro2_act_sl[sl], 0.99 * deniC));
            
            sc_.an_doc_sl[sl]   += (deni_death * sc_.anvf_sl[sl]);                        // under aerobic conditions direct dissolution of biomass
            sc_.nh4_sl[sl]      += (deni_death * sc_.anvf_sl[sl] / sipar_.RCNB());        // release of nitrogen during direct dissolution
            deniC               -= deni_death;                                            // new denitifier biomass
            sc_.C_micro1_sl[sl] += deniC;                                                 // total microbes get back their share of denitrifiers
            
            this->isotope.subdaily_write(deni_death * sc_.anvf_sl[sl] / sipar_.RCNB(),"deni_death_nh4",sl);
            this->isotope.subdaily_write(deni_death * (1.0 - sc_.anvf_sl[sl]) / sipar_.RCNB(),"deni_death_aorg",sl);            
            
            // recalibration of soil organic matter fractions (rg: new)
            crb = sc_.C_aorg_sl[sl] * sipar_.RBO();                                       // initial dead microbial pool (rg)
            crb += (deni_death * (1.0 - sc_.anvf_sl[sl]));                                // death microbial biomass (same nitrogen concentration as living microbes)
            double const hdc( sc_.C_aorg_sl[sl] * (1.0 - sipar_.RBO()));
            sc_.C_aorg_sl[sl] = crb + hdc;
            
            /* correction of nitrogen content
             * The mass of crb and hdc has individually changed but is nevertheless
             * considered to contribute fixed proportions to aorg. This contradiction
             * has to be resolved by adding or substracting nitrogen to the aorg pool.
             */
            double const  naorgO( crb / sipar_.RCNB() + hdc / sipar_.RCNH());
            double const  naorgN( sc_.C_aorg_sl[sl] * sipar_.RBO() / sipar_.RCNB() + sc_.C_aorg_sl[sl] * (1.0 - sipar_.RBO()) / sipar_.RCNH());
            
            this->isotope.subdaily_write(sc_.nh4_sl[sl],"calculate_aorg_nh4",sl);
            
            if (naorgN > naorgO)
            {
                // nh4 pool is sufficient to supply the N deficit
                if (sc_.nh4_sl[sl] > naorgN - naorgO)
                {
                    sc_.nh4_sl[sl] -= (naorgN - naorgO);
                }
                // nh4 pool is not sufficient and N concentration in sc_.C_aorg_sl will be increased by partially transferring C into doc
                else if ( naorgN > 0.0)
                {
                    sc_.doc_sl[sl] += (sc_.C_aorg_sl[sl] * (1.0 - (naorgO + sc_.nh4_sl[sl]) / naorgN));
                    sc_.C_aorg_sl[sl] *= ((naorgO + sc_.nh4_sl[sl]) / naorgN);
                    sc_.nh4_sl[sl] = 0.0;
                }
                // nh4 pool is not sufficient and there is no nitrogen in aorg - all aorg will be thus transfered into doc
                else
                {
                    sc_.doc_sl[sl] += sc_.C_aorg_sl[sl];
                    sc_.C_aorg_sl[sl] = 0.0;
                    sc_.nh4_sl[sl]  = 0.0;
                }
            }
            else if ( naorgN < naorgO)
            {
                sc_.nh4_sl[sl] += ( naorgO - naorgN);
            }
            
            this->isotope.subdaily_write(-sc_.nh4_sl[sl],"calculate_aorg_nh4",sl);
            
            if (sc_.an_doc_sl[sl] < 0.0) sc_.an_doc_sl[sl] = 0.0;
            if (sc_.an_no3_sl[sl] < 0.0) sc_.an_no3_sl[sl] = 0.0;
            if (sc_.an_no2_sl[sl] < 0.0) sc_.an_no2_sl[sl] = 0.0;
        } // next k
    } // go to next layer
}



/*! SoilChemistryDNDC::SCDNDC_diffusion(
 * \brief
 *  @note
 *    This Method includes a NO oxidation, which should be moved elsewhere
 *
 */
void
SoilChemistryDNDC::SCDNDC_gas_diffusion()
{

#define  SCDNDC_FLUX_CHEM(__v__,__av__)                                            \
{                                                                                  \
flux_chem = flux_chem_##__v__[sl] * ((sc_.__av__)[sl] - (*(__av__##_pre)));        \
if ( flux_chem > (sc_.__av__)[sl])                                                 \
{                                                                                  \
flux_chem = (sc_.__av__)[sl];                                                      \
}                                                                                  \
if ((*(__av__##_pre)) < -flux_chem)                                                \
{                                                                                  \
flux_chem = -(*(__av__##_pre));                                                    \
}                                                                                  \
(sc_.__av__)[sl] -= flux_chem;                                                     \
*(__av__##_pre) += flux_chem;                                                      \
}

    for ( size_t  i = 0;  i < NN;  i++)
    {
        double *  an_no_sl_pre( &an_no_0);
        double *  no_sl_pre( &no_0);
        double *  an_n2o_sl_pre( &an_n2o_0);
        double *  n2o_sl_pre( &n2o_0);

        double  flux_chem( 0.0);

        for ( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
        {
            // no
            SCDNDC_FLUX_CHEM(no_sl, no_sl)
            this->isotope.subdaily_write(flux_chem,"no_diffusion",sl);
            SCDNDC_FLUX_CHEM(no_sl, an_no_sl)
            this->isotope.subdaily_write(flux_chem,"an_no_diffusion",sl);
            
            // n2o
            SCDNDC_FLUX_CHEM(n2o_sl, n2o_sl)
            this->isotope.subdaily_write(flux_chem,"n2o_diffusion",sl);
            SCDNDC_FLUX_CHEM(n2o_sl, an_n2o_sl)
            this->isotope.subdaily_write(flux_chem,"an_n2o_diffusion",sl);

            /* --------------- DN NO oxidation ------------------------------------------------
             * amount of no[sl] oxidized into no3
             * notes:
             *    in contrast to co2 production, no3 production causes no o2 decrease!
             */
            double no_oxidation( std::min( sc_.no_sl[sl], factorA[sl] * sc_.no_sl[sl] / (factorB[sl] + sc_.no_sl[sl])));

            if ( sc_.no2_sl[sl] < no_oxidation)
            {
                no_oxidation = sc_.no2_sl[sl];
            }
            sc_.no3_sl[sl] += no_oxidation;
            sc_.no_sl[sl] -= no_oxidation;
            this->isotope.subdaily_write(no_oxidation,"no_oxidation",sl);

            an_no_sl_pre = &sc_.an_no_sl[sl];
            no_sl_pre = &sc_.no_sl[sl];
            an_n2o_sl_pre = &sc_.an_n2o_sl[sl];
            n2o_sl_pre = &sc_.n2o_sl[sl];
        }
        SCDNDC_emission();
    }
}



/*! void SoilChemistryDNDC::SCDNDC_calc_sl_arrays()
 * \brief This method has been introduced due to computational performance reasons.
 *
 *      It pre computes some some variables which have been computed before redundantly
 *      in any loop cycle of the soil layer for loop... This was an issue as the computations
 *      included power functions which are quite costly to evaluate.
 */
void SoilChemistryDNDC::SCDNDC_calc_sl_arrays()
{
    for ( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
    {
        // air-filled porosity
        double  poro_tot( std::max(0.01, sc_.poro_sl[sl] - wc_.ice_sl[sl] / cbm::DICE));
        double  air_poro( std::max(0.01, poro_tot - wc_.wc_sl[sl]));

        double  ice_fact(( sipar_.KICE() / (sipar_.KICE() + wc_.ice_sl[sl])));

        // According to Rudolph et al. 1996
        double  soil_eff(ice_fact * pow( air_poro, sipar_.EXP1_NX()) * pow( poro_tot, sipar_.EXP2_NX())
                         * pow(((mc_.nd_temp_sl[sl] + cbm::D_IN_K) / TNORM), sipar_.TEXP()));

        soil_eff /= sc_.h_sl[sl] * sc_.h_sl[sl] * (double)NN /* FIXME incorrect: remove! --> */* (double)NN;

        // speed up pre-diffusion calcs by introducing some more sl arrays
        flux_chem_no_sl[sl] = sipar_.D_NO() *  soil_eff;
        flux_chem_n2o_sl[sl] = sipar_.D_N2O() *  soil_eff;


        // DN_no_oxidation factor pre-calc
        double  fact_t2( std::min(1.0,ldndc::meteo::Ft_pow(sipar_.TF_DEN1(),sipar_.TF_DEN2(),mc_.nd_temp_sl[sl])));

        // yet another pre-calc for DN_no_oxidation speed up - introducing constants
        factorA[sl] = 0.365 * sc_.h_sl[sl] * sc_.dens_sl[sl] * fact_t2 / cbm::M2_IN_HA;
        factorB[sl] = cbm::PPB_IN_KG / sc_.h_sl[sl] / cbm::M2_IN_HA;
    }
}



/*! void SoilChemistryDNDC::SCDNDC_emission
 * \brief Compares concentrations of no, an_no, n2o, n2 in the upper soil
 * layer with air concentration and delivers emission (day_floor_xxx)
 * when it exceeds air concentration
 *
 */
void SoilChemistryDNDC::SCDNDC_emission()
{
    // Emission of no
    day_floor_no += (no_0 - no_atm);
    day_floor_an_no += ( an_no_0 - no_atm);
    no_0 = no_atm;
    an_no_0 = no_atm;

    // Emission of n2o
    day_floor_n2o += ( n2o_0 - n2o_atm);
    day_floor_an_n2o += ( an_n2o_0 - n2o_atm);
    n2o_0 = n2o_atm;
    an_n2o_0 = n2o_atm;
}



/*!
 * @brief
 *    Calculates anaerobic volume fraction according to Li et al., 1992
 *
 */
void
SoilChemistryDNDC::SCDNDC_anvf()
{
    anvf_old = sc_.anvf_sl;
    for ( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
    {
        sc_.anvf_sl[sl] = cbm::bound( 0.01, exp( -cbm::sqr( 7.0 * p_o2_sl[sl])), 0.99);
    }
}



/*!
 * @brief
 *    calculates only the diffusion from anaerobic
 *    to aerobic soil parts and back. It is not the
 *    general soil gas diffusion!
 *
 * @note
 *    This Method is called several times with different
 *    gases as parameters.
 */
void SoilChemistryDNDC::SCDNDC_diffusion(
                                         size_t  _sl,          //!< soil layer
                                         double  _diffuse,     //!< transport coefficient from anaerobic to aerobic soil environment
                                         double  _DIFF,        //!< transport coefficient from anaerobic to aerobic soil environment
                                         double &  _state,     //!< aerobe species
                                         double &  _an_state)  //!< anaerobe species
{
    // Transport of chemical from anaerobic to aerobic soil environment and back
    // Flux of chemical from aerobic to anaerobic  soil environment (and back)
    double  trans( (cbm::HR_IN_DAY / (double)IMAX_S) * ((( _state + _an_state) * sc_.anvf_sl[_sl]) - _an_state) * _DIFF * _diffuse);
    trans = cbm::bound( -_an_state, trans, _state);

    // Update of the stocks
    _an_state += trans;
    _state -= trans;
}



/*! void SoilChemistryDNDC::SCDNDC_conversion
 * \brief SCDNDC_conversion calculates chemical conversions in the denitrification reaction chain
 *
 * This is the essential step of the denitrification.
 * The method is called several times with different chemical species and parameters in the parameter list.
 *
 * The amount of C released as CO2 during denitrification (\f$C_{resp,den}\f$) is determined
 * following (??literatur). They declare that five C atoms are respired while four N atoms are denitrified.
 * In the model this is represented by the respective mole weights of 60 g / mol for the five C atoms and
 * 56 g / mol for the four N atoms.
 * As this finding refers to the total denitrification reaction chain it is further partitioned equally to
 * the four reaction steps \n
 * \f$C_{resp,den}=\Delta N_{den} \cdot 60 / (56 \cdot 4)    \f$
 *
 * C assimilated during denitrification (\f$C_{ass,den}\f$) is calculated as a function of growth rate
 * \f$\mu N_2 O_y\f$ \n
 * \f$C_{ass,den} = \mu_{N_2 O_y } \cdot f_{pH,den} \cdot C_{den} \cdot Act_{den} \f$
 *
 * The amount of dying denitrifiers is calculated based on the same maximum dying rate, which is also used
 * for the nitrifier population \n
 * \f$ C_{dec,den} = \alpha_{macro} \cdot C_{den} \cdot Act_{den} \f$
*/
void SoilChemistryDNDC::SCDNDC_conversion(
                                          size_t  _sl,                        //!<  soil layer counter
                                          double  _CNEFF,                     //!<
                                          double  _MWN,                       //!<
                                          double  _n_sum,                     //!<
                                          double  _doc_MM,                    //!<
                                          double  _fn,                        //!<
                                          double  _MAINTENANCE,               //!<
                                          double  _MAXRGR,                    //!<
                                          double  _MAXYLD,                    //!<
                                          double  _phk,                       //!<
                                          double  _f_MM,                      //!<
                                          double &  _deniC,                   //!<
                                          lvector_t< double > &  _an_doc,     //!<
                                          lvector_t< double > &  _day_co2,    //!< soil CO2
                                          double &  _chem_from,               //!< denitrifying from
                                          double &  _chem_to)                 //!< denitrifying to
{
    double d_C(0.0);                    // Carbon required for nitrogen conversions
    double da_C(0.0);                   // Carbon required for nitrogen assimilation
    double d_chem(0.0);                 // Nitrogen flux for conversion reaction
    double da_chem(0.0);                // Nitrogen flux to denitrifyer microbial population
    double rgr(0.0);                    // Conversion reaction rate / denitrifyer population growth rate
    double fts( 1.0 / double(IMAX_S));  // subdaily time step

    // Amount of chemical converted to next chemical
    if ( !cbm::flt_greater_zero( _f_MM) || !cbm::flt_greater_zero( _doc_MM))
    {
        // if any of the MM factors = 0, there is no growth
        _f_MM = 0.0;
        _doc_MM = 0.0;
    }
    else
    {
        // else, relative growth is related to the MM factors
        rgr = cbm::HR_IN_DAY * fts * _MAXRGR  / (0.5 * (1.0 / _doc_MM + 1.0 / _f_MM));
    }

    if ( cbm::flt_greater_zero( _n_sum))
    {
        d_chem = cbm::HR_IN_DAY * fts * _phk * _deniC * sc_.micro2_act_sl[_sl] * _fn * (rgr / _MAXYLD + _MAINTENANCE  * _chem_from / _n_sum);
    }
    else
    {
        d_chem = 0.0;
    }

    // Nitrogen limitation
    if ( d_chem > _chem_from)
    {
        d_chem = _chem_from;
    }

    // Carbon limitation
    if ( _an_doc[_sl] < ( d_chem * _CNEFF / _MWN / 4.0))
    {
        // the "4.0 division" is necessary to account for stepwise (4 steps) of nitrogen reduction (NO3 -> N2)
        d_chem = _an_doc[_sl]/ ( _CNEFF / _MWN / 4.0);
    }

    d_C            = d_chem * ( _CNEFF / _MWN) / 4.0;   // Calculate carbon requirement for respiration with n transformation
    _chem_from    -= d_chem;                            // If there is insufficient C for respiration, slow down N-transformation
    _chem_to      += d_chem;                            // Else increment the recieving nitrogen species
    _an_doc[_sl]  -= d_C;                               // Subtract respiration C from an_doc[_sl]
    _day_co2[_sl] += d_C;                               // Increment soil CO2 with respiration C


    //! combined limitation of C and N availability
    da_chem = std::min( _chem_from, std::min( _an_doc[_sl] / sipar_.RCNB(),
                                             rgr * _phk * _deniC * sc_.micro2_act_sl[_sl] * _fn));

    da_C         = da_chem * sipar_.RCNB();    // Calculate associated carbon requirement for assimilation
    _chem_from   -= da_chem;
    _an_doc[_sl] -= da_C;                      // Decrease carbon use from an_doc[_sl]
    _deniC       += da_C;                      // Increment denitrifyer Carbon
}


void
SoilChemistryDNDC::SCDNDC_groundwater_access()
{
    if ( this->gw_ == NULL)
        { return ; }

    size_t const  S = this->sl_.soil_layer_cnt();
    if ( S == 0)
        { return; }

    double const  watertable = this->gw_->watertable_subday( this->lclock_ref());
    double const  C = ( 1.0 / (cbm::MN + 3.0 * cbm::MO))
            * cbm::MN * cbm::KG_IN_MG * cbm::DM3_IN_M3;
    double const  groundwater_nitrate = C * this->gw_->no3_day( this->lclock_ref());

    double  L = this->sc_.depth_sl[S-1];
    double  H = L - this->sc_.h_sl[S-1];
    for ( size_t  sl = 0;  sl < S;  ++sl)
    {
        double  no3_groundwater = groundwater_nitrate;
        size_t const  l = S - sl - 1; /*move upwards*/
        if ( watertable < H)
            { /* fully saturated */ }
        else if ( watertable < L)
        {
            /* partially saturated */
            no3_groundwater *= ( H-L)/(H-watertable);
        }
        else
            { break; /*watertable too low*/}

        no3_groundwater = no3_groundwater * ( wc_.wc_sl[l] + wc_.ice_sl[l]) * sc_.h_sl[l];
        if ( cbm::flt_greater_zero( no3_groundwater))
        {
            double const no3_diff = this->sipar_.GROUNDWATER_NUTRIENT_RENEWAL()
                / (double)IMAX_S * ( no3_groundwater - this->sc_.an_no3_sl[l]);
            this->sc_.an_no3_sl[l] += no3_diff;
            this->day_no3_groundwater_access += no3_diff;
        }

        L -= this->sc_.h_sl[l];
        H -= this->sc_.h_sl[l];
    }
}



#define  CLEAR_WATER_TABLE_CONTENT(__val_sbl__,__add_val__)      \
{                                                                \
(__add_val__) += (__val_sbl__).sum();                            \
(__val_sbl__) = 0.0;                                             \
}                                                                \



/*!
 * @brief
 *    SCDNDC_leaching()
 *    NH4, NO3 and DOC are transported downwards by water flux
 */
void SoilChemistryDNDC::SCDNDC_leaching()
{
    /* add substances from surfacebulk layers (water or snow) to first soil layer */
    if ( !cbm::flt_greater_zero(wc_.surface_water) && !cbm::flt_greater_zero(wc_.surface_ice))
    {
        this->isotope.cumulative_write(-sc_.nh4_sl[0],"nh4surfacewater",0);
        this->isotope.cumulative_write(-sc_.no3_sl[0],"no3surfacewater",0);

        CLEAR_WATER_TABLE_CONTENT(sb_.nh4_sbl, sc_.nh4_sl[0]);
        CLEAR_WATER_TABLE_CONTENT(sb_.no3_sbl, sc_.no3_sl[0]);

        this->isotope.cumulative_write(sc_.nh4_sl[0],"nh4surfacewater",0);
        this->isotope.cumulative_write(sc_.no3_sl[0],"no3surfacewater",0);
    }

    if ( sipar_.WCDNDC_HAVE_CAPILLARY_ACTION())
    {
        /* leaching within soil layers */
        {
            double const leach_fact( cbm::bound(0.0, waterflux_sl[0] / std::max( 0.001, wc_.wc_sl[0] * sc_.h_sl[0]), 0.99));
            if ( cbm::flt_greater_zero( leach_fact))
            {
                double const leach_fact_nh4( leach_fact * sipar_.RETNH4());
                double const leach_fact_no3( leach_fact * sipar_.RETNO3());
                double const leach_fact_doc( leach_fact * sipar_.RETDOC());

                LEACH_DOWN(  sc_.nh4_sl[0], sc_.nh4_sl[1], leach_fact_nh4);
                LEACH_DOWN(  sc_.no3_sl[0], sc_.no3_sl[1], leach_fact_no3);
                LEACH_DOWN(  sc_.an_no3_sl[0], sc_.an_no3_sl[1], leach_fact_no3);
                LEACH_DOWN(  sc_.doc_sl[0], sc_.doc_sl[1], leach_fact_doc);
                LEACH_DOWN(  sc_.an_doc_sl[0], sc_.an_doc_sl[1], leach_fact_doc);
            }
        }

        for( size_t sl = 1; sl < sl_.soil_layer_cnt()-1; ++sl)
        {
            size_t sl_0( sl + -1);
            size_t sl_2( sl + 1);
            double const leach_fact( cbm::bound(-0.99, waterflux_sl[sl] / std::max( 0.001, wc_.wc_sl[sl] * sc_.h_sl[sl]), 0.99));
            double const leach_fact_nh4( leach_fact * sipar_.RETNH4());
            double const leach_fact_no3( leach_fact * sipar_.RETNO3());
            double const leach_fact_doc( leach_fact * sipar_.RETDOC());

            LEACH_UP_AND_DOWN(  sc_.nh4_sl[sl_0], sc_.nh4_sl[sl], sc_.nh4_sl[sl_2], leach_fact_nh4);
            LEACH_UP_AND_DOWN(  sc_.no3_sl[sl_0], sc_.no3_sl[sl], sc_.no3_sl[sl_2], leach_fact_no3);
            LEACH_UP_AND_DOWN(  sc_.an_no3_sl[sl_0], sc_.an_no3_sl[sl], sc_.an_no3_sl[sl_2], leach_fact_no3);
            LEACH_UP_AND_DOWN(  sc_.doc_sl[sl_0], sc_.doc_sl[sl], sc_.doc_sl[sl_2], leach_fact_doc);
            LEACH_UP_AND_DOWN(  sc_.an_doc_sl[sl_0], sc_.an_doc_sl[sl], sc_.an_doc_sl[sl_2], leach_fact_doc);
        }

        {
            size_t sl_0( sl_.soil_layer_cnt()-2);
            size_t sl_1( sl_.soil_layer_cnt()-1);
            double const leach_fact( cbm::bound(-0.99, waterflux_sl[sl_1] / std::max( 0.001, wc_.wc_sl[sl_1] * sc_.h_sl[sl_1]), 0.99));
            double const leach_fact_nh4( leach_fact * sipar_.RETNH4());
            double const leach_fact_no3( leach_fact * sipar_.RETNO3());
            double const leach_fact_doc( leach_fact * sipar_.RETDOC());

            LEACH_UP_AND_DOWN(  sc_.nh4_sl[sl_0], sc_.nh4_sl[sl_1], sc_.accumulated_nh4_leach, leach_fact_nh4);
            LEACH_UP_AND_DOWN(  sc_.no3_sl[sl_0], sc_.no3_sl[sl_1], sc_.accumulated_no3_leach, leach_fact_no3);
            LEACH_UP_AND_DOWN(  sc_.an_no3_sl[sl_0], sc_.an_no3_sl[sl_1], sc_.accumulated_no3_leach, leach_fact_no3);
            LEACH_UP_AND_DOWN(  sc_.doc_sl[sl_0], sc_.doc_sl[sl_1], sc_.accumulated_doc_leach, leach_fact_doc);
            LEACH_UP_AND_DOWN(  sc_.an_doc_sl[sl_0], sc_.an_doc_sl[sl_1], sc_.accumulated_doc_leach, leach_fact_doc);
        }
    }
    else
    {
        /* leaching within soil layers */
        size_t  sl_cnt_1( sl_.soil_layer_cnt() - 1);
        for ( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
        {
            double const leach_fact( cbm::bound( 0.0, (waterflux_sl[sl] / std::max( 0.001, wc_.wc_sl[sl] * sc_.h_sl[sl])), 1.0));

            // relation of leaching to soil type
            double const leach_nh4( sc_.nh4_sl[sl] * std::min( leach_fact, sipar_.RETNH4()));
            double const leach_no3_ae( sc_.no3_sl[sl] * std::min( leach_fact, sipar_.RETNO3()));
            double const leach_no3_an( sc_.an_no3_sl[sl] * std::min( leach_fact, sipar_.RETNO3()));
            double const leach_doc_ae( sc_.doc_sl[sl] * std::min( leach_fact, sipar_.RETDOC()));
            double const leach_doc_an( sc_.an_doc_sl[sl] * std::min( leach_fact, sipar_.RETDOC()));

            /* transport one layer down */
            sc_.nh4_sl[sl]      -= leach_nh4;
            sc_.no3_sl[sl]      -= leach_no3_ae;
            sc_.an_no3_sl[sl]   -= leach_no3_an;
            sc_.doc_sl[sl]      -= leach_doc_ae;
            sc_.an_doc_sl[sl]   -= leach_doc_an;

            this->isotope.rewrite(leach_nh4,"leach_nh4",sl);
            this->isotope.rewrite(leach_no3_ae,"leach_no3_ae",sl);
            this->isotope.rewrite(leach_no3_an,"leach_no3_an",sl);

            if ( sl == sl_cnt_1)
            {
                sc_.accumulated_no3_leach += (leach_no3_ae + leach_no3_an);
                sc_.accumulated_doc_leach += (leach_doc_ae + leach_doc_an);
                sc_.accumulated_nh4_leach += leach_nh4;
            }
            else
            {
                sc_.nh4_sl[sl+1]    += leach_nh4;
                sc_.no3_sl[sl+1]    += leach_no3_ae;
                sc_.an_no3_sl[sl+1] += leach_no3_an;
                sc_.doc_sl[sl+1]    += leach_doc_ae;
                sc_.an_doc_sl[sl+1] += leach_doc_an;
            }
        }
    }
}



/*!
 * @brief
 *      SCDNDC_temp_mois calculates fact_tm, a reduction factor that is used to
 *      compensate decomposition rate for temperature and moisture.
 */
double
SoilChemistryDNDC::SCDNDC_temp_mois(
                                    size_t  _sl)
{
    //! The moisture reduction factor fact_m is calculated using a Weibull function
    //! depending on water and ice filled porespace wifps
    double const  wifps( std::max( 0.0, ( wc_.ice_sl[_sl] + wc_.wc_sl[_sl]) / sc_.poro_sl[_sl]));
    double const  fact_m( ldndc::meteo::F_weibull( wifps, sipar_.M_FACT_DEC1(), sipar_.M_FACT_DEC2(), 1.0));

    //! The temperature reduction factor fact_t is calculated using an exponential function
    double const  fact_t( exp( -sipar_.TF_DEC1() * cbm::sqr( 1.0 - ( mc_.nd_temp_sl[_sl] / sipar_.TF_DEC2()))));

    /*! The combined temperature and moisture reduction factor is calculated as
     *  harmonic mean of fact_t and fact_m representing the combined temperature and moisture factor
     */
    if ( cbm::flt_greater_zero( fact_m) && cbm::flt_greater_zero( fact_t))
    {
        return cbm::harmonic_mean2(fact_m, fact_t);
    }
    else
    {
        return 0.0;
    }
}



void 
SoilChemistryDNDC::SCDNDC_litter_decomposition()
{
    for ( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
    {        
        // combined reduction factor for temperature and moisture stress factor
        double temp_mois_fact( SCDNDC_temp_mois(sl));
        
        this->isotope.cumulative_write(-crb_l[sl]/sipar_.RCNB(),"litterdecompositionNaorg",sl);
        this->isotope.cumulative_write(-sc_.nh4_sl[sl],"litterdecompositionnh4",sl);
        
        // Decomposition of very labile litter
        SCDNDC_decomp_litter(sl, sipar_.RCNRVL(), sipar_.KRCVL(), sipar_.FDVL(), temp_mois_fact, crb_l[sl], sc_.C_lit1_sl[sl]);
        
        // Decomposition of labile litter
        SCDNDC_decomp_litter( sl, sipar_.RCNRL(), sipar_.KRCL(), sipar_.FDL(), temp_mois_fact, crb_l[sl], sc_.C_lit2_sl[sl]);
        
        // Decomposition of recalcitrant litter
        SCDNDC_decomp_litter( sl, sipar_.RCNRR(), sipar_.KRCR(), sipar_.FDR(), temp_mois_fact, crb_l[sl], sc_.C_lit3_sl[sl]);
        
        
        this->isotope.cumulative_write(crb_l[sl]/sipar_.RCNB(),"litterdecompositionNaorg",sl);
        this->isotope.cumulative_write(sc_.nh4_sl[sl],"litterdecompositionnh4",sl);
    }
}



void
SoilChemistryDNDC::SCDNDC_decomp_litter(
                                        size_t  _sl,
                                        double  _rcn,                 //!< CN ratio of the material
                                        double  _decoConstant,        //!< Decomposition constant (-)
                                        double  _microUse,            //!< Instantanious microbial use of the material that is decomposed
                                        double  _temp_mois_fact,      //!< Temperature/ moisture reduction factor.
                                        double &  _crb_l,             //!< Labile dead microbial biomass per soil layer
                                        double &  _rc)                //!< Litter quantity per soil layer
{
    // Turnover flux for the component that the function is called for
    double const  decompC_0( sc_.till_effect_sl[_sl] * _temp_mois_fact * _decoConstant * sc_.poro_sl[_sl]);

    if ( cbm::flt_greater_zero( _rc) && cbm::flt_in_range( 0.0, decompC_0, 1.0))
    {
        if ( cbm::flt_greater_zero( _rcn))
        {
            double const  decompC( _rc * decompC_0);
            _rc -= decompC;

            /* Remaining decomposed carbon is split up into doc
             * and co2 oxygen impact factor on decomposition
             */
            double const  o2_fact( SCDNDC_get_fact_o2( _sl));
            double const  d_rcb( _microUse * (decompC / _rcn) * sipar_.RCNB());
            _crb_l += d_rcb;
            sc_.doc_sl[_sl] += (( 1.0 - o2_fact) * ( decompC - d_rcb));
            day_co2[_sl] += ( o2_fact * ( decompC - d_rcb));

            double const  free_n(( decompC/_rcn) * ( 1.0 - _microUse));
            sc_.accumulated_n_mineral_sl[_sl] += free_n;
            sc_.nh4_sl[_sl] += free_n;
            day_mineral_litter += free_n;
        }
    }
}



void 
SoilChemistryDNDC::SCDNDC_humus_decomposition()
{
    for ( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
    {        
        // combined reduction factor for temperature and moisture stress factor
        double temp_mois_fact( SCDNDC_temp_mois(sl));
        
        // Turnover flux for the component that the function is called for
        double const  fact_decomp( sc_.till_effect_sl[sl] * temp_mois_fact * sipar_.KRCH() * sc_.poro_sl[sl]);
        
        /* Remaining decomposed carbon is split up into doc
         * and co2 oxygen impact factor on decomposition
         */
        double const  o2_fact( SCDNDC_get_fact_o2( sl));
        
        double const  decomp_c( sc_.C_hum_sl[sl] * fact_decomp);
        double const  decomp_n( sc_.N_hum_sl[sl] * fact_decomp);
        sc_.C_hum_sl[sl] -= decomp_c;
        sc_.N_hum_sl[sl] -= decomp_n;
        this->isotope.rewrite(decomp_n,"humusdecomposition",sl);
        
        sc_.doc_sl[sl]  += (( 1.0 - o2_fact) * decomp_c);
        day_co2[sl]     += (o2_fact * decomp_c);
        
        sc_.accumulated_n_mineral_sl[sl]  += decomp_n;
        sc_.nh4_sl[sl]          += decomp_n;
        day_mineral_humus       += decomp_n;
    }
}



void 
SoilChemistryDNDC::SCDNDC_microbial_dynamics()
{
    for ( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
    {        
        double temp_mois_fact( SCDNDC_temp_mois(sl));
        
        
        /* water content of a soil layer */
        double const  dayWater( std::max(0.001, wc_.wc_sl[sl] * sc_.h_sl[sl]));
        
        /* water availability impact factor for rapid turnover */
        double const  fact_m( ldndc::meteo::F_weibull( SCDNDC_get_wfps( sl), sipar_.M_FACT_P5(), sipar_.M_FACT_P6(), 1.0));
        
        /* dying labile microbial biomass (kgC m-2) */
        double const  dcrb_l( sc_.till_effect_sl[sl] * temp_mois_fact * sc_.poro_sl[sl] * sipar_.KCRB_L() * crb_l[sl]);
        crb_l[sl] -= dcrb_l;
        
        /* dying resistant microbial biomass (kgC m-2) */
        double const  dcrb_r( sc_.till_effect_sl[sl] * temp_mois_fact * sc_.poro_sl[sl] * sipar_.KCRB_R() * crb_r[sl]);
        crb_r[sl] -= dcrb_r;
                
        // Total carbon flux from decomposed microbial biomass
        double const  dcrb( dcrb_l + dcrb_r);
        this->isotope.rewrite(dcrb / sipar_.RCNB(),"mic_nh4_growth",sl);
        
        // Allocation decomposition-C to doc and co2
        sc_.doc_sl[sl]  += (sipar_.EFFAC() * dcrb);
        day_co2[sl]     += ((1.0 - sipar_.EFFAC()) * dcrb);
        
        // Remaining nitrogen is added to ammonium + output of net ammonification
        sc_.accumulated_n_mineral_sl[sl] += ( dcrb / sipar_.RCNB());
        sc_.nh4_sl[sl] += ( dcrb / sipar_.RCNB());
        day_mineral_aorg += ( dcrb / sipar_.RCNB());
        
        // Microbial dynamics (in IMAX_S time steps per day)
        double const  fts( 1.0 / double(IMAX_S));
        for ( size_t  ts = 0;  ts < IMAX_S;  ++ts)
        {
            this->isotope.hour = ts;

            /* nitrogen availability impact factor */
            double const n_tot( (sc_.nh4_sl[sl] + sc_.no3_sl[sl] + sc_.an_no3_sl[sl]) / dayWater);
            double fact_n( n_tot / (sipar_.DNDC_KMM_N_MIC() + n_tot));
            fact_n = cbm::bound_min( 0.1, fact_n);
            
            /*
             * 1: factor to account for microbial death dependence on doc availability
             * 2: accounts for the saturation of microbial growth with organic material accumulation
             */
            double fact_doc(( sc_.doc_sl[sl] + sc_.an_doc_sl[sl]) / (sipar_.DNDC_KMM_C_MIC() * sc_.h_sl[sl] + sc_.doc_sl[sl] + sc_.an_doc_sl[sl]));
            fact_doc = cbm::bound_min( 0.1, fact_doc);
            
            /* Combined substrate availability factor (factorial combination from Stange 2007) */
            double const fact_n_doc( 2.0 / (1.0 / fact_doc + 1.0 / fact_n));
            
            /* microbial activity */
            sc_.micro1_act_sl[sl] += (( temp_mois_fact - sc_.micro1_act_sl[sl]) * sipar_.MUEMAX() * fts); 
            
            /* Potential growth of microbes (kgC m-2) */
            double pot_growth( sc_.C_micro1_sl[sl] * sc_.micro1_act_sl[sl] * fact_n_doc * sipar_.MUEMAX() * fts);
            
            double doc_ratio(1.0);
            if (sc_.an_doc_sl[sl] + sc_.doc_sl[sl] > 0.0)
            {
                doc_ratio = sc_.doc_sl[sl] / (sc_.doc_sl[sl] + sc_.an_doc_sl[sl]);
            }
            
            if ((sc_.doc_sl[sl] + sc_.an_doc_sl[sl]) < ( pot_growth / sipar_.EFFAC()))
            {
                pot_growth = (sc_.doc_sl[sl] + sc_.an_doc_sl[sl]) * sipar_.EFFAC();
            }
            
            /* nitrogen required for potential microbial growth */
            double const n_demand( pot_growth / sipar_.RCNB());
            
            /* Nitrate uptake by microbes (kg N m-2) */
            double no3_use( std::min( n_demand, sipar_.FNO3_U() * ( sc_.no3_sl[sl] + sc_.an_no3_sl[sl])));
            
            if ( cbm::flt_greater_zero( sc_.no3_sl[sl] + sc_.an_no3_sl[sl]))
            {
                double const no3_ratio( sc_.no3_sl[sl] / ( sc_.no3_sl[sl] + sc_.an_no3_sl[sl]));
                this->isotope.subdaily_write(sc_.no3_sl[sl],"mic_nitrateuse",sl);
                this->isotope.subdaily_write(sc_.an_no3_sl[sl],"mic_an_nitrateuse",sl);                
                sc_.no3_sl[sl] = cbm::bound_min( 0.0, sc_.no3_sl[sl] - ( no3_use * no3_ratio));
                sc_.an_no3_sl[sl] = cbm::bound_min( 0.0, sc_.an_no3_sl[sl] - ( no3_use * ( 1.0 - no3_ratio)));
                this->isotope.subdaily_write(-sc_.no3_sl[sl],"mic_nitrateuse",sl);
                this->isotope.subdaily_write(-sc_.an_no3_sl[sl],"mic_an_nitrateuse",sl);   
            }
            
            /* Ammonium uptake by microbes (kgN m-2) */
            double nh4_use( 0.0);
            if ( n_demand > no3_use)
            {
                nh4_use = std::min( n_demand - no3_use, ( 1.0 - sipar_.FNO3_U()) * sc_.nh4_sl[sl]);
                this->isotope.subdaily_write(nh4_use,"mic_ammoniumuse",sl);
            }
            
            // actual microbial growth (nitrogen limited)
            double const micro_growth( ( no3_use + nh4_use) * sipar_.RCNB());
            double const microNMax( 1.0 / (1.0 / sipar_.RCNB() + sipar_.MICRRESP() * sc_.micro1_act_sl[sl]));
            
            /* Microbial respiration (kg C m-2) */
            double   micro_resp( cbm::bound_min(0.0,
                                                  sc_.C_micro1_sl[sl] * sc_.micro1_act_sl[sl] * temp_mois_fact 
                                                  * (1.5 - microNMax / sipar_.RCNB()) * sipar_.AMAXX() * fts));
            
            /* Microbial death
             *
             * Microbial turnover is calculated using the microbial pool size sc_.C_micro1_sl,
             * - corrected for inactive microbes by the term sc_.micro1_act_sl,
             * - corrected for the effect of doc abundance on microbial growth
             * - multiplied with potential microbial turnover AMAXX
             */
            
            fact_doc /= cbm::M2_IN_HA;
            fact_doc /= (fact_doc + sc_.doc_sl[sl] + sc_.an_doc_sl[sl]);
            
            double  fact_doc_rc( fact_doc);
            if (sipar_.FRC() * (sc_.C_lit1_sl[sl] + sc_.C_lit2_sl[sl]) > 1.0/cbm::M2_IN_HA)
            {
                fact_doc_rc = (fact_doc + sc_.C_micro1_sl[sl] / (sipar_.FRC() * (sc_.C_lit1_sl[sl] + sc_.C_lit2_sl[sl])));  // FloD
            }
            
            
            /* Microbial decay (kg C m-2) */
            double  micro_death( sc_.C_micro1_sl[sl] * sc_.micro1_act_sl[sl] * fact_doc_rc * sipar_.AMAXX() * fts);
            if ( micro_death > ( sc_.C_micro1_sl[sl] - micro_resp))
            {
                micro_death = cbm::bound_min( 0.0, sc_.C_micro1_sl[sl] - micro_resp);
            }
            
            this->isotope.subdaily_write(micro_resp / sipar_.RCNB(),"mic_respiration",sl);
            this->isotope.subdaily_write(micro_death / sipar_.RCNB(),"mic_death",sl); 
            
            // rapid turnover (turns no3 directly into nh4)
            double const  transR_nh4( std::min((sc_.an_no3_sl[sl]+sc_.no3_sl[sl]), 
                                               std::max(0.0, 
                                                        sipar_.FTRANS() * fact_m * sc_.micro1_act_sl[sl] * sc_.C_micro1_sl[sl] 
                                                        * (sc_.an_no3_sl[sl]+sc_.no3_sl[sl]) / (sipar_.DNDC_KMM_NO3_TRANSNH4() + (sc_.an_no3_sl[sl]+sc_.no3_sl[sl])))));
            
            // updates
            crb_l[sl] += ( micro_death * sipar_.SRB());
            crb_r[sl] += ( micro_death * ( 1.0 - sipar_.SRB()));
            sc_.C_micro1_sl[sl] += ( micro_growth - micro_death - micro_resp);
            
            
            //doc reduction due to microbial c uptake for growth and growth respiration
            sc_.doc_sl[sl] = cbm::bound_min( 0.0, sc_.doc_sl[sl] - ( micro_growth / sipar_.EFFAC()) * doc_ratio);
            sc_.an_doc_sl[sl] = cbm::bound_min( 0.0, sc_.an_doc_sl[sl] - ( micro_growth / sipar_.EFFAC()) * ( 1.0 - doc_ratio));
            
            day_co2[sl] += (micro_resp + (1.0-sipar_.EFFAC()) * micro_growth / sipar_.EFFAC()); // total respiration incl. growth respiration
            
            if ( sc_.no3_sl[sl] + sc_.an_no3_sl[sl] > 0.0)
            {
                double const no3_ratio( sc_.no3_sl[sl] / ( sc_.no3_sl[sl] + sc_.an_no3_sl[sl]));
                this->isotope.subdaily_write(sc_.no3_sl[sl],"mic_nitraterapid",sl);
                this->isotope.subdaily_write(sc_.an_no3_sl[sl],"mic_an_nitraterapid",sl);
                sc_.no3_sl[sl] = cbm::bound_min( 0.0, sc_.no3_sl[sl] - ( transR_nh4 * no3_ratio));
                sc_.an_no3_sl[sl] = cbm::bound_min( 0.0, sc_.an_no3_sl[sl] - ( transR_nh4 * (1.0 - no3_ratio)));
                this->isotope.subdaily_write(-sc_.no3_sl[sl],"mic_nitraterapid",sl);
                this->isotope.subdaily_write(-sc_.an_no3_sl[sl],"mic_an_nitraterapid",sl);
            }
            
            sc_.nh4_sl[sl] += (micro_resp / sipar_.RCNB() + transR_nh4 - nh4_use);
            
            // output
            sc_.accumulated_n_mineral_sl[sl] += ((micro_resp + micro_death) / sipar_.RCNB() + transR_nh4);
            day_mineral_microbes += (micro_resp / sipar_.RCNB() + transR_nh4);
        }
    }
}



void 
SoilChemistryDNDC::SCDNDC_humads_decomposition()
{
    for ( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
    {        
        // combined reduction factor for temperature and moisture stress factor
        double temp_mois_fact( SCDNDC_temp_mois(sl));
        // Decomposition of humads
        
        // effect of clay abundance on humus decomposition if there is clay in the soil layer
        double clayc( 0.0);
        if ( cbm::flt_greater_zero( sc_.clay_sl[sl]))
        {
            clayc = log(sipar_.FCLAY1() / sc_.clay_sl[sl]) / sipar_.FCLAY2() + 1.0;
        }
        else
        {
            sc_.clay_sl[sl] = 0.0;
        }
        
        /* The quantity of humads that decompose in one timestep depends
         * on the factors:
         *
         *    temperature,
         *    moisture and
         *    distribution into labile and resistant humads
         */
        if ( cbm::flt_greater_zero( sc_.nh4_sl[sl]))
        {
            // Decomposition of labile humads
            double const c_decomp_hdc_l( sc_.till_effect_sl[sl] * temp_mois_fact * sipar_.KHDC_L() * clayc * sc_.poro_sl[sl] * hdc_l[sl]);
            hdc_l[sl] -= c_decomp_hdc_l;
            
            // Decomposition of recalcitrant humads
            double const c_decomp_hdc_r( sc_.till_effect_sl[sl] * temp_mois_fact * sipar_.KHDC_R() * clayc * sc_.poro_sl[sl] * hdc_r[sl]);
            hdc_r[sl] -= c_decomp_hdc_r;
            
            // Total decomposition flux (Carbon)
            double const c_decomp_tot( c_decomp_hdc_l + c_decomp_hdc_r);
            double const n_decomp_tot( c_decomp_tot / sipar_.RCNH());
            this->isotope.rewrite(n_decomp_tot,"humad_decomposotion",sl);
            
            // Carbon that is transferred to CO2:
            double const fco2( sipar_.FCO2_1() + sipar_.FCO2_2() * pow(sipar_.FCO2_3() ,sc_.clay_sl[sl] * sipar_.RCEC() * -sipar_.FCO2_4()));
            
            // Humads conversion to co2 and humus
            double const cn_hum( (sc_.N_hum_sl[sl] > 0.0) ? sc_.C_hum_sl[sl]/sc_.N_hum_sl[sl] : sc_.cn_hum_sl[sl]);
            double c_trans_co2( sipar_.FCO2_HU() * fco2 / (1.0 + fco2) * c_decomp_tot);
            double c_trans_hum( c_decomp_tot - c_trans_co2);
            double n_trans_hum( c_trans_hum / cn_hum);
            
            // correction of humification if n supply not fullfilled
            double const n_avail( 0.9 * (n_decomp_tot+sc_.nh4_sl[sl]));
            if (n_trans_hum > n_avail)
            {
                c_trans_hum = (n_avail * cn_hum);
                c_trans_co2 = (c_decomp_tot - c_trans_hum);
                n_trans_hum = n_avail;
            }
            
            // update
            day_co2[sl] += c_trans_co2;
            sc_.C_hum_sl[sl] += c_trans_hum;
            sc_.N_hum_sl[sl] += n_trans_hum;
            sc_.nh4_sl[sl] += (n_decomp_tot - n_trans_hum);
            
            this->isotope.rewrite(n_trans_hum,"humad_trans",sl);
            
            // output
            sc_.accumulated_n_mineral_sl[sl] += (c_decomp_tot / sipar_.RCNH());
            day_mineral_humads += (n_decomp_tot - n_trans_hum);
        }
    }
}



/*!
 * @brief
 *      Calculates NH4-NH3 equilibrium in liquid phase
 */
void
SoilChemistryDNDC::SCDNDC_nh4_nh3_equilibrium()
{
    for ( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
    {        
        double const  exponent( 8.0 + (25.0 - mc_.nd_temp_sl[sl]) * 0.032 - sc_.ph_sl[sl]);
        double const  fsl( sipar_.PSL_SC() / std::min( 0.3, sc_.depth_sl[sl]));
        
        double const  abc( 0.5 * pow( 10.0, exponent));    
        double d_nh3 = std::max ( 0.0, std::min( sc_.nh4_sl[sl], 0.8 * ( sc_.nh4_sl[sl] - abc * sc_.nh3_liq_sl[sl]) / ( abc + 1.0) * fsl));
        
        sc_.nh3_liq_sl[sl]  += d_nh3;
        sc_.nh4_sl[sl]  -= d_nh3;
        
        this->isotope.rewrite(d_nh3,"equilibrium",sl);
        
        // soil ph change by nh4/nh3 transfer
        if ( cbm::flt_greater_zero( d_nh3)  &&  cbm::flt_greater_zero( wc_.wc_sl[sl]))
        {
            sc_.ph_sl[sl] -= (d_nh3 / (2.0 * wc_.wc_sl[sl] * sc_.h_sl[sl]));
            sc_.ph_sl[sl]  = std::min( (double)sipar_.PHMAX(), std::max( (double)sipar_.PHMIN(), sc_.ph_sl[sl]));   //ar: empirical limitation of pH decrease,
        }
        else
        {
            sc_.ph_sl[sl] -= (0.1 * (sc_.ph_sl[sl] - sc_.phi_sl[sl]) / double(IMAX_S));
        }
    }
}



/*!
 * @brief
 *      SCDNDC_nh3_volatilization calculates NH3 volatilization from soil
 */
void
SoilChemistryDNDC::SCDNDC_nh3_volatilization()
{
    for ( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
    {        
        // Volatilised ammonia depends on the fraction of nh3 that is located in air filled porosity,
        // soil temperature and the amount of ammonia in the soil.
        double  vol_nh3(( 1.0 - SCDNDC_get_wfps( sl)) * std::max( 0.0, std::min((double)sipar_.TREF(), mc_.nd_temp_sl[sl])) / sipar_.TREF() * sc_.nh3_liq_sl[sl]);
        
        // Volatilised ammonia is subtracted from the soil solution (sc_.nh3_liq_sl[sl])
        sc_.nh3_liq_sl[sl] -= vol_nh3;
        
        // Volatilised NH3 is added to min_nh3 (NOTE: min_nh3 is a dead end, leading to N-gaps!!)
        sc_.accumulated_nh3_emis += vol_nh3;
        
        this->isotope.rewrite(vol_nh3,"volatilization",sl);
    }
}



void 
SoilChemistryDNDC::SCDNDC_nitrification()
{
    double const  fts( 1.0 / double(IMAX_S));
    
    for ( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
    {
        double  autoNitrify( 0.0); 
        double  nitrify_no2( 0.0);
        
        /* water content of a soil layer (kg m-3)
         *      effect of pH and moisture on nitrification rate
         */
        double const  dayWater( std::max( 0.001, wc_.wc_sl[sl] * sc_.h_sl[sl]));
        
        /* pH factor */
        double  fact_ph( ldndc::meteo::Ft_oneill( 17.0, 7.0, 12.0, sc_.ph_sl[sl]));
        if ( cbm::flt_equal_zero( fact_ph))
        {
            autoNitrify = 0.0;
            nitrify_no2 = 0.0;
            
            return;
        }
        ldndc_kassert( cbm::flt_greater_zero( fact_ph));
        
        /* Ammonium stress factor (-) */
        double  fact_nh4( sc_.nh4_sl[sl] / ( dayWater * sipar_.DNDC_KMM_NH4_NIT() + sc_.nh4_sl[sl]));
        
        if ( cbm::flt_equal_zero( fact_nh4))
        {
            autoNitrify = 0.0;
            nitrify_no2 = 0.0;
            
            return;
        }
        ldndc_kassert( cbm::flt_greater_zero( fact_nh4));
        
        double  heteroNitrify( std::min(sipar_.NH4_DENIMAX() * sc_.nh4_sl[sl],
                                        sipar_.KNIT() * 2.0 / ( 1.0 / fact_ph + 1.0 / fact_nh4) * sc_.micro1_act_sl[sl] * sc_.C_micro1_sl[sl]));
        
        fact_ph = exp(-( cbm::sqr( sc_.ph_sl[sl] - 7.0)));
        
        autoNitrify = std::min( sipar_.NH4_DENIMAX() * sc_.nh4_sl[sl], 
                                sipar_.KNIT() * 2.0 / ( 1.0 / fact_ph + 1.0 / fact_nh4) * sc_.micro1_act_sl[sl] * sc_.C_micro3_sl[sl]);
        
        /* gross nitrification rate (kg N m-2 layer-1) */
        double  grossNitrify( autoNitrify + heteroNitrify);
        
        if ( grossNitrify > sc_.nh4_sl[sl])
        {
            grossNitrify  = sipar_.NH4_DENIMAX() * sc_.nh4_sl[sl];
            double  NitrifyRatio( autoNitrify/( autoNitrify + heteroNitrify));
            autoNitrify   = grossNitrify * NitrifyRatio;
            heteroNitrify = grossNitrify * ( 1.0 - NitrifyRatio);
        }

        /* equation after Kesik et al. 2006 (data in Fig.1 scaled to 1) (rg)*/
        double  fact_t( cbm::bound( 0.0, sipar_.TF_NUP_NO1() * exp (mc_.nd_temp_sl[sl] / sipar_.TF_NUP_NO2()), 1.0));
        double  fact_m( cbm::bound( 0.0, ldndc::meteo::F_weibull(SCDNDC_get_wfps( sl), sipar_.M_FACT_P1(),sipar_.M_FACT_P2(), 1.0), 1.0));

        fact_ph = cbm::bound( 0.0, sipar_.PH_FACT_P2() - ( sipar_.PH_FACT_P3() * sc_.ph_sl[sl]), 1.0);

        /* NO created through nitrification (kg N m-2) */
        double  nitrify_no( sipar_.KNO() * 2.0 / ( 1.0 / fact_m + 1.0 / fact_t) * fact_ph * grossNitrify);
        
        fact_t = cbm::bound( 0.0, sipar_.TF_NUP_N2O1() * exp (mc_.nd_temp_sl[sl] / sipar_.TF_NUP_N2O2()), 1.0);
        fact_m = cbm::bound( 0.0, ldndc::meteo::F_weibull(SCDNDC_get_wfps( sl), sipar_.M_FACT_P3(), sipar_.M_FACT_P4(), 1.0), 1.0);

        // MK: sc_.no_sl[sl] pH effect, FloD: = 1.3 - 0.08 * sc_.ph_sl[sl], Li et al. 1992: 7.22 * (sc_.ph_sl[sl] - 4.4) / 18.8
        fact_ph = 1.0; //no more pH effect 
        nitrify_n2o[sl] = sipar_.KN2O() * 2.0 / ( 1.0 / fact_m + 1.0 / fact_t) * fact_ph * grossNitrify;
        
        // update
        /* gross nitrification rate (kg N m-2 layer-1) */
        double  netNitrify( 0.0);
        double  n_nxo_sum( nitrify_no + nitrify_n2o[sl]);
        if ( cbm::flt_equal( n_nxo_sum, grossNitrify))
        {
            // no op
        }
        else if ( n_nxo_sum < grossNitrify)
        {
            netNitrify = grossNitrify - n_nxo_sum;
        }
        else
        {
            ldndc_kassert( cbm::flt_not_equal_zero( n_nxo_sum));
            double const  f( grossNitrify / n_nxo_sum);
            nitrify_no *= f;
            nitrify_n2o[sl] *= f;
        }
        
        sc_.nh4_sl[sl] -= grossNitrify;
        sc_.no_sl[sl]  += nitrify_no;
        sc_.n2o_sl[sl] += nitrify_n2o[sl];
        sc_.no2_sl[sl] += netNitrify;
        
        this->isotope.rewrite(nitrify_no,"nitrify_no",sl);
        this->isotope.rewrite(nitrify_n2o[sl],"nitrify_n2o",sl);
        this->isotope.rewrite(netNitrify,"netNitrify",sl);
        
        // nitrification-induced NO2 transformation
        nitrify_no2 = std::min( 0.98 * sc_.no2_sl[sl], sipar_.KNIT() * 2.0/( 1.0/fact_ph + 1.0/fact_nh4) * sc_.C_micro3_sl[sl] * sc_.micro1_act_sl[sl]);
        sc_.no2_sl[sl] -= nitrify_no2;
        sc_.no3_sl[sl] += nitrify_no2;
        this->isotope.cumulative_write(nitrify_no2,"nitrify_no2",sl);
        
        nitrify_no2 = sc_.no2_sl[sl] * sc_.no2_sl[sl] / ( sipar_.DNDC_KMM_NO2_NIT() + sc_.no2_sl[sl]);
        sc_.no2_sl[sl] -= nitrify_no2;
        sc_.no3_sl[sl] += nitrify_no2;
        this->isotope.cumulative_write(nitrify_no2,"nitrify_no2",sl);
        
        // for output only
        sc_.accumulated_n_nitrify_sl[sl] += grossNitrify;
        
        for ( unsigned int  ts = 0;  ts < IMAX_S;  ++ts)
        {
            // death of autotrophic nitrifiers
            double const  micro_death( std::min(  sc_.C_micro3_sl[sl],
                                                ( sc_.C_micro3_sl[sl] * sc_.micro1_act_sl[sl] * ( autoNitrify / ( autoNitrify + nitrify_no2) * 0.002246
                                                                                                 + nitrify_no2 / ( autoNitrify + nitrify_no2) * 0.000629))));
            
            crb_l[sl] += ( micro_death * sipar_.SRB());
            crb_r[sl] += ( micro_death * ( 1.0 - sipar_.SRB()));
            
            // microbial growth limited by N availability
            double  pot_growth(( autoNitrify * 0.075 + nitrify_no2 * 0.021) * fts);
            double  no3_use( std::min( pot_growth / sipar_.RCNB(), 0.8 * sc_.no3_sl[sl]));
            
            double  nh4_use( 0.0);
            if ( no3_use < ( pot_growth / sipar_.RCNB()))
            {
                nh4_use = std::min( pot_growth/sipar_.RCNB() - no3_use, 0.8 * sc_.nh4_sl[sl]);
            }
            pot_growth = ( nh4_use + no3_use) * sipar_.RCNB();
            
            // growth limitation limited by carbon availability
            // rg 01.06.11: in order to close the balance, carbon has to be taken from somewhere 
            // - suggestion: crb pool (needs to release nitrogen -> assumed to nh4 pool)
            double  crb_l_use( pot_growth * sipar_.SRB());
            if (crb_l_use > crb_l[sl])
            {
                crb_l_use = 0.0;
            }
            
            double  crb_r_use( pot_growth * (1.0 - sipar_.SRB()));
            if (crb_r_use > crb_r[sl])
            {
                crb_r_use = 0.0;
            }
            
            sc_.C_micro3_sl[sl] -= micro_death;
            double const  microC3_growth( crb_l_use + crb_r_use);
            if ( cbm::flt_greater_zero( microC3_growth))
            {
                crb_l[sl]   -= crb_l_use;
                crb_r[sl]   -= crb_r_use;
                // N contained in the carbon that has been taken for growth
                sc_.nh4_sl[sl] += ( microC3_growth / sipar_.RCNB());
                sc_.nh4_sl[sl] -= ( nh4_use * microC3_growth / pot_growth);
                sc_.no3_sl[sl] -= ( no3_use * microC3_growth / pot_growth);
                
                sc_.C_micro3_sl[sl] += microC3_growth;
                
                this->isotope.cumulative_write(no3_use * microC3_growth / pot_growth,"mic3_no3nh4",sl);
                this->isotope.cumulative_write((microC3_growth - micro_death) / sipar_.RCNB(),"mic3_growthanddeath",sl);
            }
        }
    }
}



/*! void SoilChemistryDNDC::SCDNDC_chemodenitrification
 * \brief  SCDNDC_chemodenitrification estimates the turnover from no2 to no
 *
 * SCDNDC_chemodenitrification estimates the turnover from no2 to no through chemical denitrification:
 * \f[ NO_{chem} = K_{chem} \cdot f_{pH} \cdot f_t \cdot NO_{2}^{-} \f]
 * with \n
 * \f$ K_{chem}\f$ reaction rate for the chemodenitrification [h-1] \n
 * \f$ f_{pH}\f$ pH-factor: calculated via the oneill function \n
 * \f$ f_{pH} = ( \frac{sipar_.PHMIN_CHEM() - sc_.ph_sl[sl]}{sipar_.PHMIN_CHEM() - sipar_.PHOPT_CHEM()}  )^{sipar_.PH_FACT_P5()}
 *                 \cdot e ^{sipar_.PH_FACT_P5() *  ( \frac{sc_.ph_sl[sl] - sipar_.PHOPT_CHEM() }{sipar_.PHMIN_CHEM() - sipar_.PHOPT_CHEM()}  )}  \f$ \n
 * \f$ f_t\f$ temperature influence factor: \f$f_t\f$ = sipar_.TF_CHEM1() * e^{sipar_.TF_CHEM2() * mc_.nd_temp_sl[sl]} \n
 *
 * with \n
 * \f$ sc_.ph_sl[sl] \f$    as pH value of soil layer \n
 * \f$ sipar_.PHMIN_CHEM() \f$    pH minimal value for O'Neill function ??? \n
 * \f$ sipar_.PHOPT_CHEM() \f$    pH optimum value for O'Neill function \n
 * \f$ sipar_.PH_FACT_P5() \f$    form parameter of O'Neill function  \n
 * \f$ sipar_.TF_CHEM1() \f$     \n
 * \f$ sipar_.TF_CHEM2()  \f$     \n
 * \f$ mc_.nd_temp_sl[sl] \f$    soil layer temperature \n
 *
*/
void SoilChemistryDNDC::SCDNDC_chemodenitrification(
                                                 size_t  _sl,         //!< soil layer
                                                 double  _fts)        //!< time step fraction
{
    double  totN_before( 0.0);
    if ( this->bc_warn)
    {
        // Total incoming nitrogen
        totN_before = sc_.no2_sl[_sl] + sc_.no_sl[_sl];
    }

    // chemodenitrification-induced NO
    double  fact_t( cbm::bound_max( sipar_.TF_CHEM1() * exp( sipar_.TF_CHEM2() * mc_.nd_temp_sl[_sl]), 1.0));
    
    // rg, adjusted to data in Kesik 2006(b), scaled to 1
    double  fact_ph( ldndc::meteo::Ft_oneill( sipar_.PHMIN_CHEM(), sipar_.PHOPT_CHEM(), sipar_.PH_FACT_P5(), sc_.ph_sl[_sl])); 
    
    /* Chemically denitrified NO (from soil N2O to soil NO) */
    double  chem_no( _fts * sipar_.KCHEM() * fact_ph * fact_t * sc_.no2_sl[_sl]);
    
    // update
    if ( sc_.no2_sl[_sl] > chem_no)
    {
        sc_.no2_sl[_sl] -= chem_no;
    }
    else
    {
        chem_no = sc_.no2_sl[_sl];
        sc_.no2_sl[_sl] = 0.0;
    }
    
    sc_.no_sl[_sl] += chem_no;    // chem_no not used anymore (did not need to be set to zero)
    
    // output
    sc_.accumulated_n_chemodenitrify_sl[_sl] += chem_no;
    
    if ( this->bc_warn) 
    {
        double const  totN_after( sc_.no2_sl[_sl] + sc_.no_sl[_sl]);
        double const  diffN( std::abs( totN_after - totN_before));
        if ( diffN > cbm::DIFFMAX)
        {
            KLOGWARN( "Nitrogen leakage in SCDNDC_chemodenitrification");
        }
    }
}



/*!
 * @brief
 *      SCDNDC_o2_diffusion is using an implicite
 *      scheme to solve the O_2 diffusion equation.
 *      The matrix inversion solver might be changed due to the OpenMP parallelization of the method.
 */
void
SoilChemistryDNDC::SCDNDC_o2_diffusion()
{
    /* time constants setting */
    static double const  fts( 1.0 / (double)IMAX_S);
    size_t const sl_count( sl_.soil_layer_cnt());    
    static double const  D_O2_rate( cbm::SEC_IN_DAY * fts * cbm::D_O2_AIR);
            

    /* upper boundary condition */    
    tdma_solver_->set_row( 0, 0.0, 1.0, 0.0, cbm::PO2);
    

    /* assemble equation system */
    for ( size_t  sl = 0;  sl < sl_count;  ++sl)
    {
        /* ice impact on o2_sl diffusion */
        double  ice_fact_loc( (mc_.nd_temp_sl[sl] < 0.0) ? sipar_.KICE() / ( sipar_.KICE() + wc_.ice_sl[sl]) : 1.0);
        
        /* air and water filled porosity */
        double  poro_tot_loc( std::max( 0.01, sc_.poro_sl[sl] - wc_.ice_sl[sl] / cbm::DICE));
        
        /* air filled porosity */
        double  air_poro_loc( cbm::bound_min( 0.01, poro_tot_loc - wc_.wc_sl[sl]));
        
        d_soil_eff_[sl] = (ice_fact_loc * D_O2_rate * pow( air_poro_loc, sipar_.EXP1_O2()) * pow( poro_tot_loc, sipar_.EXP2_O2())
                           * pow((( mc_.nd_temp_sl[sl] + cbm::D_IN_K) / TNORM), sipar_.TEXP()));

        double const o2_consumption( cbm::bound_min( 0.0, 
                                                      ((day_co2[sl] * fts)+co2_auto_sl[sl]) * ( cbm::VGAS / ( sc_.h_sl[sl] * air_poro_loc * cbm::MC))));
        
        double  m( -d_soil_eff_[sl] / ( sc_.h_sl[sl] * sc_.h_sl[sl]));
        tdma_solver_->set_row( sl+1, m, 1.0 - ( 2.0 * m), m, cbm::bound_min(0.0, p_o2_sl[sl]-o2_consumption));
    }
    
    
    /* lower boundary conditions */
    tdma_solver_->set_row( sl_count+1, 0.0, 1.0, 0.0, tdma_solver_->b[sl_count]);
    
    
    /* solve */
    tdma_solver_->solve_in_place();
    
        
    /* check for too high o2 concentrations */
    for ( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  sl++)
    {
        p_o2_sl[sl] = cbm::bound_max( tdma_solver_->x(sl+1), cbm::PO2);
    }
}



double
SoilChemistryDNDC::SCDNDC_get_fact_o2(size_t _sl)
{
    return( p_o2_sl[_sl] / (sipar_.DNDC_KMM_O2_DECOMP() + p_o2_sl[_sl]));
}



/*!
 * @brief
 */
double
SoilChemistryDNDC::SCDNDC_get_wfps(
                                   size_t _sl)
{
    return wc_.wc_sl[_sl] / sc_.poro_sl[_sl];
}



lerr_t
SoilChemistryDNDC::SCDNDC_balance_check(
                                        unsigned int  _bc_stage,
                                        double *  _c,
                                        double *  _n)
{
    if ( !this->bc_warn)
    {
        return  LDNDC_ERR_OK;
    }

    double const tot_c( - sc_.accumulated_c_fertilizer + sc_.accumulated_doc_leach + day_co2.sum() + SCDNDC_get_c_tot());
    double const tot_n( - sc_.accumulated_n_fertilizer - sc_.accumulated_n2_fixation - day_no3_groundwater_access
                       + sc_.accumulated_no3_leach + sc_.accumulated_nh4_leach + sc_.accumulated_don_leach
                       + day_floor_no + day_floor_an_no
                       + day_floor_n2o + day_floor_an_n2o
                       + sc_.accumulated_n2_emis + sc_.accumulated_nh3_emis
                       + SCDNDC_get_n_tot());

    if ( _bc_stage == 0)
    {
        *_c = tot_c;
        *_n = tot_n;
    }
    else if ( _bc_stage == 1)
    {
        double const  c_difference( std::abs(*_c - tot_c) );
        if ( c_difference > this->bc_tolerance)
        {
            KLOGERROR( "C-leakage:  before=", (*_c)*cbm::M2_IN_HA,
                      "\tafter=", tot_c*cbm::M2_IN_HA, "\t difference=", c_difference*cbm::M2_IN_HA);
        }

        double const  n_difference = std::abs( *_n - tot_n);

        if ( n_difference > this->bc_tolerance)
        {

            KLOGERROR( "N-leakage:  before=", (*_n)*cbm::M2_IN_HA,
                      "\tafter=", tot_n*cbm::M2_IN_HA, "\t difference=", n_difference*cbm::M2_IN_HA);
        }
    }
    else
    {
        KLOGFATAL( "[BUG]");
        return LDNDC_ERR_FAIL;
    }

    return  LDNDC_ERR_OK;
}



double
SoilChemistryDNDC::SCDNDC_get_c_tot()
{
    double c_tot(  sc_.c_wood_sl.sum() + sc_.c_wood
                 + sc_.c_stubble_lit1 + sc_.c_stubble_lit2 + sc_.c_stubble_lit3);

    for (size_t sl = 0; sl < sl_.soil_layer_cnt(); sl++)
    {
        c_tot += (  sc_.doc_sl[sl] + sc_.an_doc_sl[sl]
                  + sc_.C_aorg_sl[sl] + sc_.C_micro1_sl[sl] + sc_.C_micro2_sl[sl] + sc_.C_micro3_sl[sl]
                  + sc_.C_lit1_sl[sl] + sc_.C_lit2_sl[sl] + sc_.C_lit3_sl[sl]
                  + sc_.C_hum_sl[sl]
                  + sc_.c_raw_lit_1_sl[sl] + sc_.c_raw_lit_2_sl[sl] + sc_.c_raw_lit_3_sl[sl]);
    }

    for (size_t sbl = 0; sbl < sb_.surfacebulk_layer_cnt(); ++sbl)
    {
        c_tot += sb_.ch4_sbl[sbl] + sb_.doc_sbl[sbl];
    }

    return c_tot;
}



double
SoilChemistryDNDC::SCDNDC_get_n_tot()
{
    double n_tot(  sc_.n_wood_sl.sum() + sc_.n_wood
                 + sc_.n_stubble_lit1 + sc_.n_stubble_lit2 + sc_.n_stubble_lit3);

    for (size_t sl = 0; sl < sl_.soil_layer_cnt(); sl++)
    {
        n_tot += ( sc_.nh4_sl[sl] + sc_.clay_nh4_sl[sl] + sc_.don_sl[sl]
                  + sc_.nh3_liq_sl[sl] + sc_.nh3_gas_sl[sl] + sc_.urea_sl[sl]
                  + sc_.no3_sl[sl] + sc_.an_no3_sl[sl]
                  + sc_.no2_sl[sl] + sc_.an_no2_sl[sl]
                  + sc_.no_sl[sl] + sc_.an_no_sl[sl]
                  + sc_.n2o_sl[sl] + sc_.an_n2o_sl[sl]
                  + (sc_.C_micro1_sl[sl] + sc_.C_micro2_sl[sl] + sc_.C_micro3_sl[sl]) / sipar_.RCNB()
                  + (sc_.C_aorg_sl[sl] * sipar_.RBO() / sipar_.RCNB() + sc_.C_aorg_sl[sl] * (1.0 - sipar_.RBO()) / sipar_.RCNH())
                  + sc_.C_lit1_sl[sl] / sipar_.RCNRVL()
                  + sc_.C_lit2_sl[sl] / sipar_.RCNRL()
                  + sc_.C_lit3_sl[sl] / sipar_.RCNRR()
                  + sc_.N_hum_sl[sl]
                  + sc_.n_raw_lit_1_sl[sl] + sc_.n_raw_lit_2_sl[sl] + sc_.n_raw_lit_3_sl[sl]);
    }

    for (size_t sbl = 0; sbl < sb_.surfacebulk_layer_cnt(); ++sbl)
    {
        n_tot += sb_.nh4_sbl[sbl] + sb_.nh3_sbl[sbl] + sb_.urea_sbl[sbl] + sb_.no3_sbl[sbl] + sb_.don_sbl[sbl];
    }
    
    return n_tot;
}


lerr_t
SoilChemistryDNDC::finalize()
{
    if ( this->output_buffer)
    {
        lerr_t  rc_owfin = this->output_writer.finalize();
        RETURN_IF_NOT_OK(rc_owfin);
    }

    return  LDNDC_ERR_OK;
}


static int  _CountEventPlant( void const *, size_t, void *  _counter)
{
    int *  counter = static_cast< int * >( _counter);
    if ( !counter)
        { return -1; }

    *counter += 1;
    return 0;
}

lerr_t
SoilChemistryDNDC::register_ports( cbm::io_kcomm_t *  _io_kcomm)
{
    lerr_t  rc = LDNDC_ERR_OK;
    
    rc = this->m_eventfertilize.register_ports( _io_kcomm);
    if ( rc)
        { return LDNDC_ERR_FAIL; }

    rc = this->m_eventtill.register_ports( _io_kcomm);
    if ( rc)
        { return LDNDC_ERR_FAIL; }

    CBM_Callback  cb_plant;
    cb_plant.fn = &_CountEventPlant;
    cb_plant.data = &this->m_PlantCounter;
    this->m_PlantHandle = _io_kcomm->subscribe_event( "plant", cb_plant);
    if ( !CBM_HandleOk(this->m_PlantHandle))
        { return  LDNDC_ERR_FAIL; }

    return rc;
}
lerr_t
SoilChemistryDNDC::unregister_ports( cbm::io_kcomm_t *  _io_kcomm)
{
    this->m_eventfertilize.unregister_ports( _io_kcomm);
    this->m_eventtill.unregister_ports( _io_kcomm);

    _io_kcomm->unsubscribe_event( this->m_PlantHandle);

    return LDNDC_ERR_OK;
}

} /*namespace ldndc*/

