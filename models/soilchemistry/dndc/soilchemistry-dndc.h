/*!
 * @brief
 *    generalization of the bio-geochemical models ForestDNDC and DNDC.
 *
 *    @n
 *    the class SoilChemistryDNDC is a specialization of substate_soilchemistry_t. It holds the
 *    definition of the SoilChemistryDNDC module dealing with soil microbial and bio-geo-chemistry
 *    processes originating from the agricultural DNDC and PnET-N-DNDC Model (Li 2000, Stange 2001).
 *    @n
 *
 *    The SoilChemistryDNDC module calculates daily dynamics of mineralization (decomposition,
 *    ammonification), denitrification, and nitrification as well as transport processes of carbon
 *    and nitrogen.
 */

#ifndef  LM_SOILCHEMISTRY_DNDC_H_
#define  LM_SOILCHEMISTRY_DNDC_H_

#include  "mbe_legacymodel.h"
#include  "state/mbe_state.h"
#include  "ld_shared.h"
#include  "ld_isotopes.h"
#include  "soilchemistry/dndc/output-soilchemistry-dndc.h"

#include  "soilchemistry/ld_enhanced_efficiency_nitrogen_fertilizers.h"

#include  "eventhandler/fertilize/fertilize.h"
#include  "eventhandler/till/till.h"

#include  <input/event/events.h>

class  solver_tridiagonal_t;

namespace ldndc {
class  LDNDC_API  SoilChemistryDNDC  :  public  MBE_LegacyModel
{
    /***  module specific constants  ***/

    /*! number of time steps per day */
    static const unsigned int  IMAX_S;
    
    /*! number of iterations in diffusion calculations */
    static const unsigned int  NN;
    
    /*! reference temperature [K] */
    static const double  TNORM;
    
    /*! soil depth down to which aboveground litter is distributed [m] */
    static const double  HLIM;
    
    /*! fraction of decomposed black carbon lost by respiration due to N2 fixation */
    static const double  FRN2;

    LMOD_EXPORT_MODULE_INFO(SoilChemistryDNDC,"soilchemistry:dndc","Soilchemistry DNDC");
    public:
        SoilChemistryDNDC(
                MoBiLE_State *,
                cbm::io_kcomm_t *,
                timemode_e);

        ~SoilChemistryDNDC();

        lerr_t  configure( ldndc::config_file_t const *);
        lerr_t  initialize();

        lerr_t  register_ports( cbm::io_kcomm_t *);
        lerr_t  unregister_ports( cbm::io_kcomm_t *);

        lerr_t  solve();
        lerr_t  finalize();

        lerr_t  sleep() { return  LDNDC_ERR_OK; }
        lerr_t  wake() { return  LDNDC_ERR_OK; }

    private:
        cbm::io_kcomm_t *  io_kcomm;

        input_class_groundwater_t const *  gw_;
        input_class_siteparameters_t const &  sipar_;
        input_class_soillayers_t const &  sl_;

        substate_surfacebulk_t &  sb_;
        substate_airchemistry_t const &  ac_;
        substate_microclimate_t const &  mc_;
        substate_physiology_t const &  ph_;
        substate_soilchemistry_t &  sc_;
        substate_watercycle_t &  wc_;

        MoBiLE_PlantVegetation *  m_veg;

        timemode_e const timemode_;

        OutputSoilchemistryDNDC  output_writer;
        ldndc_flt64_t *  output_buffer;

        EventHandlerFertilize  m_eventfertilize;
        EventHandlerTill  m_eventtill;

        CBM_Handle  m_PlantHandle;
        int  m_PlantCounter;

    private:
        /**/
        double *  d_soil_eff_;

        solver_tridiagonal_t *  tdma_solver_;

        double *waterflux_sl;
        double *accumulated_waterflux_old_sl;

        lvector_t< double >  crb_l;             // very labile active organic carbon
        lvector_t< double >  crb_r;             // labile active organic carbon
        lvector_t< double >  hdc_l;             // recalcitrant active organic carbon
        lvector_t< double >  hdc_r;             // very recalcitrant active organic carbon

        lvector_t< double >  p_o2_sl;           //!< oxygen partial pressure [bar]
        lvector_t< double >  fdeni;             //!< fraction denitrifying microbes of total microbial biomass per soil layer (-)
        lvector_t< double >  day_co2;           //!< co2 produced by decomposition per soil layer (kgC m-2)
        lvector_t< double >  co2_auto_sl;
        lvector_t< double >  anvf_old;          //!< anaerobi volume fraction of last time step
        lvector_t< double >  nitrify_n2o;       //!< n2o production by nitrification

        lvector_t< double >  communicate_sl;

        lvector_t< double >  factorA;           //!<
        lvector_t< double >  factorB;           //!<
        lvector_t< double >  flux_chem_no_sl;   //!<
        lvector_t< double >  flux_chem_n2o_sl;  //!<

        double  no_0;                           //!< no-N per soil layer (kgN m-2)
        double  n2o_0;                          //!< no-N per soil layer (kgN m-2)
        double  an_no_0;                        //!< no in anaerobic balloon per soil layer (kgN m-2)
        double  an_n2o_0;                       //!< n2o in anaerobic balloon per soil layer (kgN m-2)

        double day_floor_no;                    //!< NO emission   (kgN m-2)
        double day_floor_n2o;                   //!< N2O emission (kgN m-2)
        double day_floor_an_no;                 //!< an_no emission (kgN m-2)
        double day_floor_an_n2o;                //!< an_n2o emission (kgN m-2)

        double day_no3_groundwater_access;      //!< daily no3 exchange with groundwater (kgN m-2)
        double year_no3_groundwater_access;     //!< yearly no3 exchange with groundwater (kgN m-2)

        double n2o_atm;                         //!< mass of n2o in atmospheric layer (kgN m-2)
        double no_atm;                          //!< mass of no in atmospheric layer (kgN m-2)

        double day_mineral_litter;
        double day_mineral_humus;
        double day_mineral_microbes;
        double day_mineral_aorg;
        double day_mineral_humads;

        lerr_t  SCDNDC_step_initialize_();
        lerr_t  SCDNDC_step_finalize_();




        /*!
         * @brief
         */
        lerr_t
        SCDNDC_write_output();
        lerr_t
        SCDNDC_write_output_daily();


        //! \fn void UreaHydrolysis()
        void SCDNDC_urea_hydrolysis();

        //! \fn void AutotrophicRespiration()
        void SCDNDC_autotrophic_respiration();

        //! \fn void Littering()
        void SCDNDC_littering();

        //! \fn void Pertubation()
        void SCDNDC_pertubation();

        /*!
         * @brief
         *    Used to be part of calcLittering. Distributes litter among the litter layers
         *     according to the weighted height of the layers.
         *
         * @param
         *    c pools
         * @param
         *    n diff
         * @param
         *    bc litter
         * @param
         *    litter
         */
         void SCDNDC_distribute_among_litter_layers(
                double  _add_c[], 
                double  _n_diff);

        //! \fn void NH4_ClayEquilibrium()
        void SCDNDC_nh4_clay_equilibrium();

        //! \fn void Calc_sl_arrays()
        void SCDNDC_calc_sl_arrays();
    
        //! \fn void SplitAorg()    
        void SCDNDC_split_aorg();
    
        //! \fn void CombineAorg()    
        void SCDNDC_combine_aorg();
    
        //! \fn void LitterDecomposition()    
        void SCDNDC_litter_decomposition();
        
        //! \fn void HumusDecomposition()    
        void SCDNDC_humus_decomposition();
        
        //! \fn void MicrobialDynamics()    
        void SCDNDC_microbial_dynamics();
    
        //! \fn void HumadsDecomposition()    
        void SCDNDC_humads_decomposition();
        
        //! \fn void NH4NH3Dynamics()    
        void SCDNDC_nh4_nh3_dynamics();

        //! \fn void Nitrification()    
        void SCDNDC_nitrification();

        //! \fn void Denitrification()
        void SCDNDC_denitrification();

        //! \fn void Diffusion()
        void SCDNDC_gas_diffusion();

        //! \fn void Leaching()
        void SCDNDC_groundwater_access();

        //! \fn void Leaching()
        void SCDNDC_leaching();

        //! \fn void O2_Diffusion()
        void SCDNDC_o2_diffusion();

        //! \fn void Anvf()
        void SCDNDC_anvf();

        //! \fn double DC_temp_mois
        double SCDNDC_temp_mois(
                size_t);

        //! \fn void DC_decomp_litter
        void SCDNDC_decomp_litter(
                size_t sl,
                double rcn,                 //!< CN ratio of the material
                double decoConstant,        //!< Decomposition constant (-)
                double microUse,            //!< Instantanious microbial use of the material that is decomposed
                double temp_mois_fact,      //!< Temperature/ moisture reduction factor.
                double &crb_l,              //!< Labile dead microbial biomass per soil layer (kgC m-2)
                double &rc);                //!< Litter quantity per soil layer

        //! \fn void WoodDecomposition
        void SCDNDC_wood_decomposition();
        
        //!    \fn void NH4_NH3_Equilibrium_
        void SCDNDC_nh4_nh3_equilibrium();

        //! \fn void NH3_Volatilization_
        void SCDNDC_nh3_volatilization();

        //! \fn void DN_diffusion
        void SCDNDC_diffusion(
                size_t,
                double,
                double,
                double &,
                double &);

        //! \fn void DN_conversion
        void SCDNDC_conversion(
                size_t sl,
                double CNEFF,
                double MWN,
                double n_sum,
                double doc_MM,
                double fn,
                double MAINTENANCE,
                double MAXRGR,
                double MAXYLD,
                double phk,
                double f_MM,
                double &deniC,
                lvector_t< double > &  an_doc,
                lvector_t< double > &  day_co2,
                double &chem_from,
                double &chem_to);

        //! \fn void DN_chemodenitrification
        void SCDNDC_chemodenitrification(
                size_t sl,
                double fts);

        //! \fn void DN_emission()
        void SCDNDC_emission();

        double SCDNDC_get_fact_o2(size_t /* soil layer */);

        double SCDNDC_get_wfps(
                               size_t /* soil layer */);
    
        /* balance check switch */
        bool  bc_warn;
        double  bc_tolerance;

        lerr_t  SCDNDC_balance_check(
                unsigned int /*stage*/, double * /*carbon*/, double * /*nitrogen*/);
        double SCDNDC_get_c_tot();
        double SCDNDC_get_n_tot();
        
        Isotopes  isotope;
};

} /*namespace ldndc*/

#endif    /*  LM_SOILCHEMISTRY_DNDC_H_  */

