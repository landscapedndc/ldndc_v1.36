/*!
 * @brief
 *
 * @author
 *    david kraus
 */

#ifndef  LM_OUTPUT_SOILCHEMISTRYDNDC_DAILY_H_
#define  LM_OUTPUT_SOILCHEMISTRYDNDC_DAILY_H_

#include  "mbe_legacyoutputmodel.h"
#include  "soilchemistry/dndc/output-soilchemistry-dndc-base.h"

#define  LMOD_OUTPUT_MODULE_NAME  OutputSoilchemistryDNDCDaily
#define  LMOD_OUTPUT_MODULE_ID    "output:soilchemistry:dndc"
#define  LMOD_OUTPUT_MODULE_DESC  "SoilchemistryDNDC Daily Output"
namespace ldndc {
class  LDNDC_API  LMOD_OUTPUT_MODULE_NAME  :  public  soilchemistrydndc_writer_t
{
    LMOD_EXPORT_MODULE_INFO(LMOD_OUTPUT_MODULE_NAME,LMOD_OUTPUT_MODULE_ID,LMOD_OUTPUT_MODULE_DESC);
    public:
        LMOD_OUTPUT_MODULE_NAME(
                MoBiLE_State *,
                cbm::io_kcomm_t *,
                timemode_e  _timemode);

        ~LMOD_OUTPUT_MODULE_NAME();


        lerr_t  configure( ldndc::config_file_t const *);
        lerr_t  initialize();
        lerr_t  solve();
        lerr_t  finalize();

        lerr_t  wake() { return  LDNDC_ERR_OK; }
        lerr_t  sleep() { return  LDNDC_ERR_OK; }


        lerr_t  write_record( ldndc_flt64_t *);
        size_t  record_size() const;

    private:
        cbm::io_kcomm_t *  io_kcomm;

    private:
        ldndc::sink_handle_t  snk_;
};

} /*namespace ldndc*/

#undef  LMOD_OUTPUT_MODULE_NAME
#undef  LMOD_OUTPUT_MODULE_ID
#undef  LMOD_OUTPUT_MODULE_DESC

#endif  /*  !LM_OUTPUT_SOILCHEMISTRYDNDC_DAILY_H_  */

