/*!
 * @file
 *
 * @author
 *    david kraus
 */

#ifndef  LM_OUTPUT_SOILCHEMISTRYDNDC_H_
#define  LM_OUTPUT_SOILCHEMISTRYDNDC_H_

#include  "mbe_legacymodel.h"
#include  "soilchemistry/dndc/output-soilchemistry-dndc-daily.h"

#define  LMOD_OUTPUT_MODULE_BASE  MBE_LegacyModel
#define  LMOD_OUTPUT_MODULE_NAME  OutputSoilchemistryDNDC
#define  LMOD_OUTPUT_MODULE_ID    "output:soilchemistry:dndc"
#define  LMOD_OUTPUT_MODULE_DESC  "SoilchemistryDNDC Output"
namespace ldndc {
class  LDNDC_API  LMOD_OUTPUT_MODULE_NAME  :  public  LMOD_OUTPUT_MODULE_BASE
{
    LMOD_EXPORT_MODULE_INFO(LMOD_OUTPUT_MODULE_NAME,LMOD_OUTPUT_MODULE_ID,LMOD_OUTPUT_MODULE_DESC);
    enum  writer_id_e
    {
        WRITER_DAILY,
        /* total number of writers */
        WRITER_CNT
    };
    static char const * const  WRITER_OPTIONKEY[WRITER_CNT];
    public:

        LMOD_OUTPUT_MODULE_NAME(
                MoBiLE_State *,
               cbm::io_kcomm_t *,
                timemode_e  _timemode);

        ~LMOD_OUTPUT_MODULE_NAME();


        lerr_t  configure( ldndc::config_file_t const *);
        lerr_t  initialize();
        lerr_t  solve();
        lerr_t  finalize();

        lerr_t  wake() { return  LDNDC_ERR_OK; }
        lerr_t  sleep() { return  LDNDC_ERR_OK; }


        bool  write_daily() const { return  this->output_writer_daily.active(); }
        lerr_t  write_daily(
                ldndc_flt64_t * /*data*/);

        size_t  max_record_size() const;

    private:
        OutputSoilchemistryDNDCDaily  output_writer_daily;
        soilchemistrydndc_writer_t *  writers[WRITER_CNT];

        lerr_t  configure_writer(
            soilchemistrydndc_writer_t * /*writer*/, ldndc::config_file_t const * /*configuration*/,
            char const * /*writer option key*/);
};

} /*namespace ldndc*/

#undef  LMOD_OUTPUT_MODULE_BASE
#undef  LMOD_OUTPUT_MODULE_NAME
#undef  LMOD_OUTPUT_MODULE_ID
#undef  LMOD_OUTPUT_MODULE_DESC

#endif  /*  !LM_OUTPUT_SOILCHEMISTRYDNDC_H_  */

