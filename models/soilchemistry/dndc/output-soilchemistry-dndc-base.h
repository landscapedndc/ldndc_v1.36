/*!
 * @file
 *
 * @author
 *    david kraus
 */

#ifndef  LM_OUTPUT_SOILCHEMISTRYDNDC_BASE_H_
#define  LM_OUTPUT_SOILCHEMISTRYDNDC_BASE_H_

#include  "mbe_legacyoutputmodel.h"

namespace ldndc {

class  LDNDC_API  soilchemistrydndc_writer_t  :  public  MBE_LegacyOutputModel
{
    public:
        soilchemistrydndc_writer_t(
            MoBiLE_State * _state,cbm::io_kcomm_t * _io, timemode_e _timemode)
            : MBE_LegacyOutputModel( _state, _io, _timemode)
        { }
        virtual  ~soilchemistrydndc_writer_t() = 0;

        virtual size_t  record_size() const = 0;
};

} /*namespace ldndc*/

#endif  /*  !LM_OUTPUT_SOILCHEMISTRYDNDC_BASE_H_  */

