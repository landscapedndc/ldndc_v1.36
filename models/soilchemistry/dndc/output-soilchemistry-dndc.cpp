/*!
 * @file
 * @author
 *  - David Kraus
 * @date
 *  oct 20, 2014 
 */

#include  "soilchemistry/dndc/output-soilchemistry-dndc.h"

#define  LMOD_OUTPUT_MODULE_BASE  MBE_LegacyModel
#define  LMOD_OUTPUT_MODULE_NAME  OutputSoilchemistryDNDC
#define  LMOD_OUTPUT_MODULE_TIMEMODE  TMODE_POST_DAILY

LMOD_MODULE_INFO(LMOD_OUTPUT_MODULE_NAME,LMOD_OUTPUT_MODULE_TIMEMODE,LMOD_FLAG_USER);

REGISTER_OPTION(OutputSoilchemistryDNDC, output,"Controls if any output is written  [bool] {on}");
REGISTER_OPTION(OutputSoilchemistryDNDC, outputdaily,"Controls if daily output is written  [bool] {on}");

namespace ldndc {

soilchemistrydndc_writer_t::~soilchemistrydndc_writer_t() {}

char const * const  LMOD_OUTPUT_MODULE_NAME::WRITER_OPTIONKEY[WRITER_CNT] =
{
    "outputdaily"
};

LMOD_OUTPUT_MODULE_NAME::LMOD_OUTPUT_MODULE_NAME(
        MoBiLE_State *  _state,
       cbm::io_kcomm_t *  _io,
        timemode_e  _timemode)
        : LMOD_OUTPUT_MODULE_BASE( _state, _timemode),
          output_writer_daily( _state, _io, _timemode)
{
    this->writers[WRITER_DAILY] = &this->output_writer_daily;
}


LMOD_OUTPUT_MODULE_NAME::~LMOD_OUTPUT_MODULE_NAME()
{
}


size_t
LMOD_OUTPUT_MODULE_NAME::max_record_size()
const
{
    size_t  max_record_sz = 0;
    for ( int  w = 0;  w < WRITER_CNT;  ++w)
    {
        if ( this->writers[w] && this->writers[w]->active())
        {
            size_t const  record_size =
                this->writers[w]->record_size();
            if ( cbm::is_valid( record_size))
            {
                max_record_sz = std::max( record_size, max_record_sz);
            }
        }
    }

    return  max_record_sz;
}

lerr_t
LMOD_OUTPUT_MODULE_NAME::configure_writer(
            soilchemistrydndc_writer_t *  _module, ldndc::config_file_t const *  _config,
            char const *  _optionkey)
{
    lerr_t  rc_conf = LDNDC_ERR_OK;

    bool  output_on /*global switches*/;
    CF_LMOD_QUERY( _config, "output", output_on, true);
    output_on = output_on && _config->have_output();
    output_on = output_on && this->get_option< bool >( "output", true);

    bool  have_output_config /*configuration switches*/,
        have_output /*model setup switches*/;
    CF_LMOD_QUERY( _config, _optionkey, have_output_config, true);

    have_output = this->get_option< bool >( _optionkey, true);
    if ( output_on && have_output_config && have_output)
    {
        rc_conf = _module->configure( _config);
    }
    else
    {
        _module->set_active_off();
    }
    return  rc_conf;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::configure(
        ldndc::config_file_t const *  _cf)
{
    lerr_t  rc_conf = LDNDC_ERR_OK;
    bool  have_active_writer = false;
    
    for ( int  w = 0;  w < WRITER_CNT;  ++w)
    {
        if ( this->writers[w])
        {
            lerr_t const  rc_owconf = this->configure_writer(
                    this->writers[w], _cf, WRITER_OPTIONKEY[w]);
            if ( rc_owconf)
            {
                rc_conf = rc_owconf;
            }
            if ( this->writers[w]->active())
            {
                have_active_writer = true;
            }
        }
    }
    if ( !have_active_writer)
    {
        this->set_active_off();
    }

    return  rc_conf;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::initialize()
{
    lerr_t  rc_ini = LDNDC_ERR_OK;

    for ( int  w = 0;  w < WRITER_CNT;  ++w)
    {
        if ( this->writers[w] && this->writers[w]->active())
        {
            lerr_t const  rc_owini =
                this->writers[w]->initialize();
            if ( rc_owini)
            {
                rc_ini = rc_owini;
            }
        }
    }

    return  rc_ini;
}

lerr_t
LMOD_OUTPUT_MODULE_NAME::solve()
{
    KLOGFATAL( "do not use solve(), we rock with write_record()");
    return  LDNDC_ERR_FAIL;

}

lerr_t
LMOD_OUTPUT_MODULE_NAME::write_daily( ldndc_flt64_t *  _data)
{
    if ( output_writer_daily.active())
    {
        return  output_writer_daily.write_record( _data);
    }
    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::finalize()
{
    for ( int  w = 0;  w < WRITER_CNT;  ++w)
    {
        if ( this->writers[w] && this->writers[w]->active())
        {
            this->writers[w]->finalize();
        }
    }

    return  LDNDC_ERR_OK;
}

} /*namespace ldndc*/

#undef  LMOD_OUTPUT_MODULE_BASE
#undef  LMOD_OUTPUT_MODULE_NAME
#undef  LMOD_OUTPUT_MODULE_TIMEMODE

#undef  OutputSoilchemistryDNDCDaily_Rank
#undef  OutputSoilchemistryDNDCDaily_Datasize

