/*!
 * @file
 * david kraus (created on: oct 20, 2014)
 * 
 */

#include  "soilchemistry/dndc/soilchemistry-dndc.h"

#include  <constants/cbm_const.h>

namespace ldndc {

lerr_t
SoilChemistryDNDC::SCDNDC_write_output()
{
    if ( !this->output_buffer)
    {
        return  LDNDC_ERR_OK;
    }


    if ( timemode_ == TMODE_SUBDAILY &&
            ( ! lclock()->is_position( TMODE_POST_DAILY)))
    {
        return  LDNDC_ERR_OK;
    }

    if ( this->output_writer.write_daily())
    {
        lerr_t const  rc_out = this->SCDNDC_write_output_daily();
        if ( rc_out)
        {
            return  LDNDC_ERR_FAIL;
        }
    }

    return  LDNDC_ERR_OK;    
}

lerr_t
SoilChemistryDNDC::SCDNDC_write_output_daily()
{
#define  SCDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(__val__)  LDNDC_OUTPUT_SET_COLUMN_((__val__),=,this->output_buffer,1)

    LDNDC_OUTPUT_SET_COLUMN_INDEX(0);

    SCDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( day_no3_groundwater_access * cbm::M2_IN_HA);
    SCDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( year_no3_groundwater_access * cbm::M2_IN_HA);

    return  this->output_writer.write_daily( this->output_buffer);
}

} /*namespace ldndc*/

