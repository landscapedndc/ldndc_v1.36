/*!
 * @file
 *    
 *
 * @author
 *      david kraus (created on: oct 20, 2014) 
 */

#include  "soilchemistry/dndc/output-soilchemistry-dndc-daily.h"

#define  LMOD_OUTPUT_MODULE_NAME  OutputSoilchemistryDNDCDaily
#define  LMOD_OUTPUT_MODULE_TIMEMODE  TMODE_POST_DAILY

#define  sinkidentifier_SoilchemistryDNDCDaily "soilchemistrydndcdaily"

LMOD_MODULE_INFO(LMOD_OUTPUT_MODULE_NAME,LMOD_OUTPUT_MODULE_TIMEMODE,LMOD_FLAG_USER|LMOD_FLAG_OUTPUT);
namespace ldndc {



ldndc_string_t const  OutputSoilchemistryDNDCDaily_Ids[] =
{
    "day_no3_groundwater",
    "year_no3_groundwater",
    "day_denitrify_no3_growth",
    "day_denitrify_no3_maintenance",
    "day_denitrify_no2_growth",
    "day_denitrify_no2_maintenance",
    "day_denitrify_no_growth",
    "day_denitrify_no_maintenance",
    "day_denitrify_n2o_growth",
    "day_denitrify_n2o_maintenance"
};



ldndc_string_t const  OutputSoilchemistryDNDCDaily_Header[] =
{
    "day_no3_groundwater",
    "year_no3_groundwater",
    "day_denitrify_no3_growth",
    "day_denitrify_no3_maintenance",
    "day_denitrify_no2_growth",
    "day_denitrify_no2_maintenance",
    "day_denitrify_no_growth",
    "day_denitrify_no_maintenance",
    "day_denitrify_n2o_growth",
    "day_denitrify_n2o_maintenance"
};


#define  OutputSoilchemistryDNDCDaily_Datasize  (sizeof( OutputSoilchemistryDNDCDaily_Header) / sizeof( OutputSoilchemistryDNDCDaily_Header[0]))
ldndc_output_size_t const  OutputSoilchemistryDNDCDaily_Sizes[] =
{
    OutputSoilchemistryDNDCDaily_Datasize,

    OutputSoilchemistryDNDCDaily_Datasize /*total size*/
};



ldndc_output_size_t const *  OutputSoilchemistryDNDCDaily_EntitySizes = NULL;

#define  OutputSoilchemistryDNDCDaily_Rank  ((ldndc_output_rank_t)(sizeof( OutputSoilchemistryDNDCDaily_Sizes) / sizeof( OutputSoilchemistryDNDCDaily_Sizes[0])) - 1)
atomic_datatype_t const  OutputSoilchemistryDNDCDaily_Types[] =
{
    LDNDC_FLOAT64
};



LMOD_OUTPUT_MODULE_NAME::LMOD_OUTPUT_MODULE_NAME(
                MoBiLE_State *  _state,cbm::io_kcomm_t *  _io, timemode_e  _timemode)
                : soilchemistrydndc_writer_t( _state, _io, _timemode),
                io_kcomm( _io)
{
}



LMOD_OUTPUT_MODULE_NAME::~LMOD_OUTPUT_MODULE_NAME()
{
}



size_t
LMOD_OUTPUT_MODULE_NAME::record_size()
const
{
    return  OutputSoilchemistryDNDCDaily_Datasize;
}



lerr_t
LMOD_OUTPUT_MODULE_NAME::configure(
        ldndc::config_file_t const *  _cf)
{
    if ( _cf && !_cf->have_output())
    {
        return  LDNDC_ERR_OK;
    }

    lerr_t  rc_setflags =
    this->set_metaflags( _cf, RM_DEFAULT_DAILY);
    if ( rc_setflags)
    { return  LDNDC_ERR_FAIL; }

    snk_ = io_kcomm->sink_handle_acquire( sinkidentifier_SoilchemistryDNDCDaily);
    if ( snk_.status() == LDNDC_ERR_OK)
    {
        lerr_t  rc_layout = LDNDC_OUTPUT_DEFINE_SINK_LAYOUT_DEFAULTS(
                    snk_, OutputSoilchemistryDNDCDaily);
        RETURN_IF_NOT_OK(rc_layout);
    }
    else if ( snk_.status() != LDNDC_ERR_ATTRIBUTE_NOT_FOUND)
    {
        KLOGERROR( "sink status bad  [sink=", sinkidentifier_SoilchemistryDNDCDaily, "]");
        return  LDNDC_ERR_FAIL;
    }
    
    return  LDNDC_ERR_OK;
}



lerr_t
LMOD_OUTPUT_MODULE_NAME::initialize()
{
    return  LDNDC_ERR_OK;
}



lerr_t
LMOD_OUTPUT_MODULE_NAME::solve()
{
    KLOGFATAL( "do not use solve(), we rock with write()");
    return  LDNDC_ERR_FAIL;

}



lerr_t
LMOD_OUTPUT_MODULE_NAME::write_record( ldndc_flt64_t *  _data)
{
    if ( !_data || !snk_.is_acquired())
    {
        /* we assume there is nothing to dump ... */
        return  LDNDC_ERR_OK;
    }

    /* output */
    void *data[] = { _data};

    lerr_t  rc_write =
    this->write_fixed_record( &this->snk_, data);
    if ( rc_write)
    { return  LDNDC_ERR_FAIL; }

    return LDNDC_ERR_OK;
}



lerr_t
LMOD_OUTPUT_MODULE_NAME::finalize()
{
    return  io_kcomm->sink_handle_release( &snk_);
}

} /*namespace ldndc*/

#undef  LMOD_OUTPUT_MODULE_NAME
#undef  LMOD_OUTPUT_MODULE_TIMEMODE

#undef  OutputSoilchemistryDNDCDaily_Rank
#undef  OutputSoilchemistryDNDCDaily_Datasize

