/*!
 * @file
 * @author
 *  David Kraus
 * @date
 *  Oct, 2015
 */


#include  "soilchemistry/ld_litterheight.h"

#include  <input/soillayers/soillayers.h>
#include  <constants/cbm_const.h>
#include  <logging/cbm_logging.h>

/*!
 * @page soillibs
 * @section soillibs_litter_height Height of litter layer
 * @authors
 *  - Ruediger Grote
 *  - David Kraus
 *
 * Update litter height depending on soil organic matter and bulk density.
 */
lerr_t
ldndc::update_litter_height(
                            soillayers::input_class_soillayers_t const &  _soillayers,
                            substate_watercycle_t &  _wc,
                            substate_soilchemistry_t &  _sc)
{
    double h_cum( 0.0);
    for( size_t  sl = 0;  sl < _soillayers.soil_layer_cnt();  ++sl)
    {
        if (sl < _soillayers.soil_layers_in_litter_cnt())
        {
            if ( cbm::flt_greater_zero( _sc.dens_sl[sl] * _sc.fcorg_sl[sl]) && cbm::flt_greater_zero( _sc.som_sl[sl]))
            {
                // soil organic matter increases --> inc sl-height
                double const h_sl_0( cbm::CCORG * _sc.som_sl[sl] / ( cbm::DM3_IN_M3 * _sc.dens_sl[sl] * _sc.fcorg_sl[sl]));
                _wc.wc_sl[sl] *= _sc.h_sl[sl] / h_sl_0;
                _sc.h_sl[sl] = h_sl_0;
            }

            if ( !cbm::flt_greater_zero( _sc.som_sl[sl]))
            {
                _sc.h_sl[sl] = 0.0;
                _wc.wc_sl[sl] = 0.0;

                LOGERROR( "Soil litter layer height vanished. Illegal situation!");
                return  LDNDC_ERR_FAIL;
            }
        }

        h_cum += _sc.h_sl[sl];
        _sc.depth_sl[sl] = h_cum;
    }

    return  LDNDC_ERR_OK;
}


lerr_t
ldndc::update_soil_height(
                          soillayers::input_class_soillayers_t const &  _soillayers,
                          substate_watercycle_t &  ,
                          substate_soilchemistry_t &  _sc)
{
    double h_sum_old( 0.0);
    double h_sum_target( 0.0);
    double som_05_inv_sum( 0.0);
    for( size_t  sl = 0;  sl < _soillayers.soil_layer_cnt();  ++sl)
    {
        double const soil_dry_mass( _sc.min_sl[sl] + _sc.som_sl[sl]);               // [kg:m-2]
        double const h_target( soil_dry_mass / (_sc.dens_sl[sl] * cbm::DM3_IN_M3)); // [m]

        h_sum_old += _sc.h_sl[sl];
        h_sum_target += h_target;
        if ( cbm::flt_greater( _sc.h_sl[sl], 0.05))
        {
            som_05_inv_sum += 1.0 / _sc.som_sl[sl];
        }
    }

    double const delta_h_sum( h_sum_target - h_sum_old);
    for( size_t  sl = 0;  sl < _soillayers.soil_layer_cnt();  ++sl)
    {
        if ( cbm::flt_greater( _sc.h_sl[sl], 0.05))
        {
            double const som_05_inv( 1.0 / _sc.som_sl[sl]);
            double const delta_h( som_05_inv / som_05_inv_sum * delta_h_sum);
            _sc.h_sl[sl] += delta_h;
        }
    }

    return  LDNDC_ERR_OK;
}


/*!
 * @brief
 * First floor defined by:
 * - litter height
 * - ecosystem type
 */
bool
ldndc::forest_floor(
                    size_t _sl,
                    soillayers::input_class_soillayers_t const &_soillayers,
                    const ldndc::site::input_class_site_t &_site)
{
    if ( _sl < _soillayers.soil_layers_in_litter_cnt())
    {
        if ( (_site.soil_use_history() == ECOSYS_FOREST) ||
             (_site.soil_use_history() == ECOSYS_FOREST_NATURAL))
        {
            return true;
        }
    }

    return false;
}


bool
ldndc::forest_floor(
                    double _depth,  // [m]
                    soillayers::input_class_soillayers_t const &_soillayers,
                    const ldndc::site::input_class_site_t &_site)
{
    if ( cbm::flt_less_equal( _depth, _soillayers.litterheight() * cbm::M_IN_MM))
    {
        if ( (_site.soil_use_history() == ECOSYS_FOREST) ||
             (_site.soil_use_history() == ECOSYS_FOREST_NATURAL))
        {
            return true;
        }
    }

    return false;
}
