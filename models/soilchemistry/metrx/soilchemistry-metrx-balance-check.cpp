/*!
 * @file
 * David Kraus (created on: may 12, 2014)
 *
 */

#include  "soilchemistry/metrx/soilchemistry-metrx.h"
#include  <constants/lconstants-conv.h>

namespace ldndc {

/*!
 * @brief
 *
 */
double
SoilChemistryMeTrX:: MeTrX_get_n_tot()
{
    double n_tot(  n_algae + n_dead_algae + sc_.n_wood
                 + sc_.n_stubble_lit1 + sc_.n_stubble_lit2 + sc_.n_stubble_lit3
                 + sc_.n_raw_lit_1_above + sc_.n_raw_lit_2_above + sc_.n_raw_lit_3_above);

    for (size_t sl = 0; sl < sl_.soil_layer_cnt(); sl++)
    {
        n_tot += (  sc_.n_wood_sl[sl]
                  + nh4_ae_sl[sl] + nh4_an_sl[sl] + sc_.clay_nh4_sl[sl] + sc_.coated_nh4_sl[sl]
                  + sc_.nh3_liq_sl[sl] + sc_.nh3_gas_sl[sl] + sc_.urea_sl[sl]
                  + no3_ae_sl[sl] + no3_an_sl[sl]
                  + sc_.no2_sl[sl] + sc_.an_no2_sl[sl]
                  + don_ae_sl[sl] + don_an_sl[sl]
                  + no_gas_sl[sl] + an_no_gas_sl[sl] + no_liq_sl[sl] + an_no_liq_sl[sl]
                  + n2o_gas_sl[sl] + an_n2o_gas_sl[sl] + n2o_liq_sl[sl] + an_n2o_liq_sl[sl]
                  + sc_.N_lit1_sl[sl] + sc_.N_lit2_sl[sl] + sc_.N_lit3_sl[sl]
                  + sc_.n_raw_lit_1_sl[sl] + sc_.n_raw_lit_2_sl[sl] + sc_.n_raw_lit_3_sl[sl]
                  + n_micro_1_sl[sl] + n_micro_2_sl[sl] + n_micro_3_sl[sl]
                  + sc_.N_aorg_sl[sl] + n_microbial_necromass_sl[sl]
                  + n_humus_1_sl[sl] + n_humus_2_sl[sl] + n_humus_3_sl[sl]);
    }

    for (size_t sbl = 0; sbl < sb_.surfacebulk_layer_cnt(); ++sbl)
    {
        n_tot +=  sb_.nh4_sbl[sbl] + sb_.coated_nh4_sbl[sbl]
                + sb_.no3_sbl[sbl] + sb_.don_sbl[sbl]
                + sb_.nh3_sbl[sbl] + sb_.urea_sbl[sbl]
                + sb_.n2o_sbl[sbl] + sb_.no_sbl[sbl];
    }

    n_tot += nh3_fl.sum();

    return n_tot;
}



/*!
 * @brief
 *
 */
double
SoilChemistryMeTrX::MeTrX_get_c_tot()
{
    double c_tot(  c_algae + c_dead_algae + sc_.c_wood
                 + sc_.c_stubble_lit1 + sc_.c_stubble_lit2 + sc_.c_stubble_lit3
                 + sc_.c_raw_lit_1_above + sc_.c_raw_lit_2_above + sc_.c_raw_lit_3_above);

    for (size_t sl = 0; sl < sl_.soil_layer_cnt(); sl++)
    {
        c_tot += (  sc_.c_wood_sl[sl]
                  + sc_.doc_sl[sl] + sc_.an_doc_sl[sl]
                  + ae_acetate_sl[sl] + an_acetate_sl[sl]
                  + sc_.C_micro1_sl[sl] + sc_.C_micro2_sl[sl] + sc_.C_micro3_sl[sl]
                  + sc_.C_aorg_sl[sl] + c_microbial_necromass_sl[sl]
                  + c_humus_1_sl[sl] + c_humus_2_sl[sl] + c_humus_3_sl[sl]
                  + sc_.C_lit1_sl[sl] + sc_.C_lit2_sl[sl] + sc_.C_lit3_sl[sl]
                  + sc_.c_raw_lit_1_sl[sl] + sc_.c_raw_lit_2_sl[sl] + sc_.c_raw_lit_3_sl[sl]
                  + ch4_gas_sl[sl] + ch4_liq_sl[sl]
                  + co2_gas_sl[sl] + co2_liq_sl[sl]);
    }

    for (size_t sbl = 0; sbl < sb_.surfacebulk_layer_cnt(); ++sbl)
    {
        c_tot += sb_.ch4_sbl[sbl] + sb_.co2_sbl[sbl] + sb_.doc_sbl[sbl];
    }

    return c_tot;
}



/*!
 * @brief
 *
 */
lerr_t
SoilChemistryMeTrX::MeTrX_balance_check(
                                        unsigned int _i)
{
    double const tot_c(  MeTrX_get_c_tot()
                       + sc_.accumulated_doc_leach + subdaily_ch4_leach.sum() + subdaily_co2_leach.sum()
                       + subdaily_co2_soil.sum() + subdaily_co2_plant.sum() + subdaily_co2_bubbling.sum() + subdaily_co2_water.sum()
                       + subdaily_ch4_soil.sum() + subdaily_ch4_plant.sum() + subdaily_ch4_bubbling.sum() + subdaily_ch4_water.sum()
                       - sc_.accumulated_c_fertilizer - sc_.accumulated_c_livestock_grazing - day_c_fix_algae
                       - co2_auto_sl.sum());

    double const tot_n(  MeTrX_get_n_tot()
                       + sc_.accumulated_don_leach + sc_.accumulated_no3_leach + sc_.accumulated_nh4_leach
                       + day_leach_no + day_leach_n2o + day_leach_nh3 + day_leach_urea
                       + subdaily_no_soil.sum() + subdaily_no_plant.sum() + subdaily_no_bubbling.sum() + subdaily_no_water.sum()
                       + subdaily_nh3_soil.sum() + subdaily_nh3_plant.sum() + subdaily_nh3_bubbling.sum() + subdaily_nh3_water.sum()
                       + subdaily_n2o_soil.sum() + subdaily_n2o_plant.sum() + subdaily_n2o_bubbling.sum() + subdaily_n2o_water.sum()
                       + accumulated_n_no3_n2_denitrification_sl.sum()
                       + accumulated_n_no2_n2_denitrification_sl.sum()
                       + accumulated_n_no_n2_denitrification_sl.sum()
                       + accumulated_n_n2o_n2_denitrification_sl.sum()
                       - sc_.accumulated_n_fertilizer - sc_.accumulated_n_livestock_grazing - day_n_fix_algae);
    
    if ( _i == 1)
    {
        tot_c_balance_1 = tot_c;
        tot_n_balance_1 = tot_n;
    }
    else if ( _i == 2)
    {
        double const c_difference( std::abs(tot_c_balance_1 - tot_c));
        if ( cbm::flt_greater( c_difference, 1.0e-10))
        {
            KLOGERROR( name(),": C-leakage \tC before: ", tot_c_balance_1,
                       "\t C after: ", tot_c, ".\t difference: ", c_difference*cbm::M2_IN_HA, "[kg C ha-1]");
            return LDNDC_ERR_OK;
        }

        double const n_difference( std::abs(tot_n_balance_1 - tot_n));
        if ( cbm::flt_greater( n_difference, 1.0e-10))
        {

            KLOGERROR( name(),": N-leakage \tN before: ", tot_n_balance_1,
                       "\t N after: ", tot_n, ".\t difference: ", n_difference*cbm::M2_IN_HA, "[kg N ha-1]");
            return LDNDC_ERR_OK;
        }
    }
    return LDNDC_ERR_OK;
}



/*
 * @brief
 *      checks all kind of non-positive numbers (including nan)
 */
#define  MeTrX_log_invalid(__var__,__index__,__name__)        \
if (!((__var__) >= 0.0))                                      \
{                                                             \
    if ((__var__) >= (-1.0e-20))                              \
    {                                                         \
        (__var__) = 0.0;                                      \
    }                                                         \
    else                                                      \
    {                                                         \
        ++n_err;                                              \
        KLOGERROR( "Illegal value at ", _position, ": ",      \
        __name__, "[",__index__, "] = ", __var__);            \
    }                                                         \
}



/*
 * @brief 
 *      helping function for debugging
 *      checks complete state for all kind of non-positive numbers (including nan)
 */
lerr_t
SoilChemistryMeTrX::MeTrX_check_for_negative_value(
                                                   char const *  _position)
{
    int  n_err = 0;
    for (size_t sbl = 0; sbl < sb_.surfacebulk_layer_cnt(); ++sbl)
    {
#define  MeTrX_log_invalid_sb(__var__)                      \
MeTrX_log_invalid(this->sb_.__var__[sbl],sbl,#__var__)
        MeTrX_log_invalid_sb( ch4_sbl)
        MeTrX_log_invalid_sb( co2_sbl)
        MeTrX_log_invalid_sb( doc_sbl)
        MeTrX_log_invalid_sb( nh4_sbl)
        MeTrX_log_invalid_sb( nh3_sbl)
        MeTrX_log_invalid_sb( n2o_sbl)
        MeTrX_log_invalid_sb( no_sbl)
        MeTrX_log_invalid_sb( urea_sbl)
        MeTrX_log_invalid_sb( no3_sbl)
        MeTrX_log_invalid_sb( don_sbl)
        MeTrX_log_invalid_sb( o2_sbl)
        MeTrX_log_invalid_sb( so4_sbl)
    }

#define  MeTrX_log_invalid_metrx(__var__)                   \
MeTrX_log_invalid( __var__,-99,#__var__)
    
    MeTrX_log_invalid_metrx( sc_.c_raw_lit_1_above)
    MeTrX_log_invalid_metrx( sc_.c_raw_lit_2_above)
    MeTrX_log_invalid_metrx( sc_.c_raw_lit_3_above)

    MeTrX_log_invalid_metrx( sc_.n_raw_lit_1_above)
    MeTrX_log_invalid_metrx( sc_.n_raw_lit_2_above)
    MeTrX_log_invalid_metrx( sc_.n_raw_lit_3_above)
    
    MeTrX_log_invalid_metrx( sc_.c_stubble_lit1)
    MeTrX_log_invalid_metrx( sc_.c_stubble_lit2)
    MeTrX_log_invalid_metrx( sc_.c_stubble_lit3)

    MeTrX_log_invalid_metrx( sc_.n_stubble_lit1)
    MeTrX_log_invalid_metrx( sc_.n_stubble_lit2)
    MeTrX_log_invalid_metrx( sc_.n_stubble_lit3)

    MeTrX_log_invalid_metrx( c_algae)
    MeTrX_log_invalid_metrx( n_algae)

    MeTrX_log_invalid_metrx( sc_.c_wood)
    MeTrX_log_invalid_metrx( sc_.n_wood)

    MeTrX_log_invalid_metrx( c_dead_algae)
    MeTrX_log_invalid_metrx( n_dead_algae)

    for( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
    {
#define  MeTrX_log_invalid_metrx_sl(__var__)                   \
MeTrX_log_invalid(this->__var__[sl],sl,#__var__)
        MeTrX_log_invalid_metrx_sl( sc_.N_lit1_sl)
        MeTrX_log_invalid_metrx_sl( sc_.N_lit2_sl)
        MeTrX_log_invalid_metrx_sl( sc_.N_lit3_sl)
        MeTrX_log_invalid_metrx_sl( o2_gas_sl)
        MeTrX_log_invalid_metrx_sl( o2_liq_sl)
        MeTrX_log_invalid_metrx_sl( c_humus_1_sl)
        MeTrX_log_invalid_metrx_sl( c_humus_2_sl)
        MeTrX_log_invalid_metrx_sl( c_humus_3_sl)
        MeTrX_log_invalid_metrx_sl( ae_acetate_sl)
        MeTrX_log_invalid_metrx_sl( an_acetate_sl)
        MeTrX_log_invalid_metrx_sl( no3_ae_sl)
        MeTrX_log_invalid_metrx_sl( no3_an_sl)
        MeTrX_log_invalid_metrx_sl( no_liq_sl)
        MeTrX_log_invalid_metrx_sl( no_gas_sl)
        MeTrX_log_invalid_metrx_sl( an_no_liq_sl)
        MeTrX_log_invalid_metrx_sl( an_no_gas_sl)
        MeTrX_log_invalid_metrx_sl( n2o_liq_sl)
        MeTrX_log_invalid_metrx_sl( n2o_gas_sl)
        MeTrX_log_invalid_metrx_sl( an_n2o_liq_sl)
        MeTrX_log_invalid_metrx_sl( an_n2o_gas_sl)
        MeTrX_log_invalid_metrx_sl( n_micro_1_sl)
        MeTrX_log_invalid_metrx_sl( n_micro_2_sl)
        MeTrX_log_invalid_metrx_sl( n_micro_3_sl)
        MeTrX_log_invalid_metrx_sl( n_humus_1_sl)
        MeTrX_log_invalid_metrx_sl( n_humus_3_sl)
        MeTrX_log_invalid_metrx_sl( sc_.c_raw_lit_1_sl)
        MeTrX_log_invalid_metrx_sl( sc_.c_raw_lit_2_sl)
        MeTrX_log_invalid_metrx_sl( sc_.c_raw_lit_3_sl)
        MeTrX_log_invalid_metrx_sl( sc_.n_raw_lit_1_sl)
        MeTrX_log_invalid_metrx_sl( sc_.n_raw_lit_2_sl)
        MeTrX_log_invalid_metrx_sl( sc_.n_raw_lit_3_sl)
        MeTrX_log_invalid_metrx_sl( sc_.N_aorg_sl)
        MeTrX_log_invalid_metrx_sl( sc_.C_aorg_sl)
        MeTrX_log_invalid_metrx_sl( sc_.C_micro1_sl)
        MeTrX_log_invalid_metrx_sl( sc_.C_micro2_sl)
        MeTrX_log_invalid_metrx_sl( sc_.C_micro3_sl)
        MeTrX_log_invalid_metrx_sl( c_microbial_necromass_sl)
        MeTrX_log_invalid_metrx_sl( n_microbial_necromass_sl)
        MeTrX_log_invalid_metrx_sl( nh4_ae_sl)
        MeTrX_log_invalid_metrx_sl( nh4_an_sl)
        MeTrX_log_invalid_metrx_sl( don_ae_sl)
        MeTrX_log_invalid_metrx_sl( don_an_sl)
        MeTrX_log_invalid_metrx_sl( sc_.doc_sl)
        MeTrX_log_invalid_metrx_sl( sc_.an_doc_sl)
        MeTrX_log_invalid_metrx_sl( sc_.C_lit1_sl)
        MeTrX_log_invalid_metrx_sl( sc_.C_lit2_sl)
        MeTrX_log_invalid_metrx_sl( sc_.C_lit3_sl)
        MeTrX_log_invalid_metrx_sl( sc_.no2_sl)
        MeTrX_log_invalid_metrx_sl( sc_.an_no2_sl)
    }

    return  n_err ? LDNDC_ERR_FAIL : LDNDC_ERR_OK;
}

} /*namespace ldndc*/

