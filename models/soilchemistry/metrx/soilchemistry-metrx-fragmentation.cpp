/*!
 * @file
 *      david kraus (created on: may 12, 2014)
 *
 */

#include  "soilchemistry/metrx/soilchemistry-metrx.h"

namespace ldndc {

/*!
 * @page metrx
 * @section metrx_fragmentation Fragmentation
 *
 * Incoming litter from, e.g., plants, algae, animals is fragmentated to
 * 'soil organic' litter (no more distinguishable from soil organic matter)
 *
 *
 * @image html metrx_litter_flow_chart.png "Litter flow" width=500
 * @image latex metrx_litter_flow_chart.png "Litter flow"
 */
lerr_t
SoilChemistryMeTrX::MeTrX_fragmentation()
{
    /***************/
    /* wood litter */
    /***************/

    /* aboveground wood litter */
    if ( cbm::flt_greater_zero( sc_.c_wood) ||
         cbm::flt_greater_zero( sc_.n_wood))
    {
        double const frag_c_wood( K_FRAG_WOOD_ABOVE * MeTrX_get_fact_tm_decomp( 0) * sc_.c_wood);
        double const frag_n_wood( K_FRAG_WOOD_ABOVE * MeTrX_get_fact_tm_decomp( 0) * sc_.n_wood);

        sc_.c_wood -= frag_c_wood;
        sc_.n_wood -= frag_n_wood;

        sc_.c_raw_lit_2_above += 0.5 * frag_c_wood;
        sc_.c_raw_lit_3_above += 0.5 * frag_c_wood;

        sc_.n_raw_lit_2_above += 0.5 * frag_n_wood;
        sc_.n_raw_lit_3_above += 0.5 * frag_n_wood;

        if ( spinup_.spinup_stage( this->lclock()))
        {
            spinup_.exit_wood += frag_c_wood;
        }
    }


    /* belowground wood litter */
    for( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
    {
        if ( cbm::flt_greater_zero( sc_.c_wood_sl[sl]) ||
             cbm::flt_greater_zero( sc_.n_wood_sl[sl]))
        {
            double const frag_c_wood( K_FRAG_WOOD_BELOW * MeTrX_get_fact_tm_decomp( sl) * sc_.c_wood_sl[sl]);
            double const frag_n_wood( K_FRAG_WOOD_BELOW * MeTrX_get_fact_tm_decomp( sl) * sc_.n_wood_sl[sl]);

            sc_.c_wood_sl[sl] -= frag_c_wood;
            sc_.n_wood_sl[sl] -= frag_n_wood;

            sc_.c_raw_lit_2_sl[sl] += 0.5 * frag_c_wood;
            sc_.c_raw_lit_3_sl[sl] += 0.5 * frag_c_wood;

            sc_.n_raw_lit_2_sl[sl] += 0.5 * frag_n_wood;
            sc_.n_raw_lit_3_sl[sl] += 0.5 * frag_n_wood;
        }
    }


    /******************/
    /* stubble litter */
    /******************/

    if ( cbm::flt_greater_zero( sc_.c_stubble_lit1+sc_.c_stubble_lit2+sc_.c_stubble_lit3))
    {
        double const c_senescence_lit1( K_FRAG_STUBBLE * sc_.c_stubble_lit1);
        double const c_senescence_lit2( K_FRAG_STUBBLE * sc_.c_stubble_lit2);
        double const c_senescence_lit3( K_FRAG_STUBBLE * sc_.c_stubble_lit3);

        double const n_senescence_lit1( K_FRAG_STUBBLE * sc_.n_stubble_lit1);
        double const n_senescence_lit2( K_FRAG_STUBBLE * sc_.n_stubble_lit2);
        double const n_senescence_lit3( K_FRAG_STUBBLE * sc_.n_stubble_lit3);

        sc_.c_stubble_lit1 -= c_senescence_lit1;
        sc_.c_stubble_lit2 -= c_senescence_lit2;
        sc_.c_stubble_lit3 -= c_senescence_lit3;

        sc_.n_stubble_lit1 -= n_senescence_lit1;
        sc_.n_stubble_lit2 -= n_senescence_lit2;
        sc_.n_stubble_lit3 -= n_senescence_lit3;

        sc_.c_raw_lit_1_above += c_senescence_lit1;
        sc_.c_raw_lit_2_above += c_senescence_lit2;
        sc_.c_raw_lit_3_above += c_senescence_lit3;

        sc_.n_raw_lit_1_above += n_senescence_lit1;
        sc_.n_raw_lit_2_above += n_senescence_lit2;
        sc_.n_raw_lit_3_above += n_senescence_lit3;
    }


    /**************/
    /* raw litter */
    /**************/

    /* aboveground */
    if ( cbm::flt_greater_zero( sc_.c_raw_lit_1_above+sc_.c_raw_lit_2_above+sc_.c_raw_lit_3_above))
    {
        double const c_senescence_lit1( K_FRAG_LITTER_ABOVE * sc_.c_raw_lit_1_above);
        double const c_senescence_lit2( K_FRAG_LITTER_ABOVE * sc_.c_raw_lit_2_above);
        double const c_senescence_lit3( K_FRAG_LITTER_ABOVE * sc_.c_raw_lit_3_above);

        double const n_senescence_lit1( K_FRAG_LITTER_ABOVE * sc_.n_raw_lit_1_above);
        double const n_senescence_lit2( K_FRAG_LITTER_ABOVE * sc_.n_raw_lit_2_above);
        double const n_senescence_lit3( K_FRAG_LITTER_ABOVE * sc_.n_raw_lit_3_above);

        sc_.c_raw_lit_1_above -= c_senescence_lit1;
        sc_.c_raw_lit_2_above -= c_senescence_lit2;
        sc_.c_raw_lit_3_above -= c_senescence_lit3;

        sc_.n_raw_lit_1_above -= n_senescence_lit1;
        sc_.n_raw_lit_2_above -= n_senescence_lit2;
        sc_.n_raw_lit_3_above -= n_senescence_lit3;

        MeTrX_litter_distribution( n_senescence_lit1, n_senescence_lit2, n_senescence_lit3,
                                   c_senescence_lit1, c_senescence_lit2, c_senescence_lit3,
                                   accumulated_n_aboveground_raw_litter_fragmentation_sl);
    }

    /* belowground */
    for( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
    {
        if ( cbm::flt_greater_zero( sc_.c_raw_lit_1_sl[sl]))
        {
            double const fact_decomp( K_FRAG_LITTER_BELOW * MeTrX_get_fact_tm_decomp( sl));
            double const c_decomp_lit1( fact_decomp * sc_.c_raw_lit_1_sl[sl]);
            double const c_decomp_lit2( fact_decomp * sc_.c_raw_lit_2_sl[sl]);
            double const c_decomp_lit3( fact_decomp * sc_.c_raw_lit_3_sl[sl]);

            double const n_decomp_lit1( fact_decomp * sc_.n_raw_lit_1_sl[sl]);
            double const n_decomp_lit2( fact_decomp * sc_.n_raw_lit_2_sl[sl]);
            double const n_decomp_lit3( fact_decomp * sc_.n_raw_lit_3_sl[sl]);

            sc_.c_raw_lit_1_sl[sl] -= c_decomp_lit1;
            sc_.c_raw_lit_2_sl[sl] -= c_decomp_lit2;
            sc_.c_raw_lit_3_sl[sl] -= c_decomp_lit3;
            
            sc_.n_raw_lit_1_sl[sl] -= n_decomp_lit1;
            sc_.n_raw_lit_2_sl[sl] -= n_decomp_lit2;
            sc_.n_raw_lit_3_sl[sl] -= n_decomp_lit3;

            sc_.C_lit1_sl[sl] += c_decomp_lit1;
            sc_.C_lit2_sl[sl] += c_decomp_lit2;
            sc_.C_lit3_sl[sl] += c_decomp_lit3;

            sc_.N_lit1_sl[sl] += n_decomp_lit1;
            sc_.N_lit2_sl[sl] += n_decomp_lit2;
            sc_.N_lit3_sl[sl] += n_decomp_lit3;

            accumulated_n_belowground_raw_litter_fragmentation_sl[sl] += n_decomp_lit1 + n_decomp_lit2 + n_decomp_lit3;
        }
    }


    /****************/
    /* algae litter */
    /****************/

    if ( cbm::flt_greater_zero( c_dead_algae) ||
         cbm::flt_greater_zero( n_dead_algae))
    {
        double const fact_decomp( K_FRAG_ALGAE * MeTrX_get_fact_tm_decomp( 0));
        double const c_decomp_algae( fact_decomp * c_dead_algae);
        double const n_decomp_algae( fact_decomp * n_dead_algae);

        c_dead_algae -= c_decomp_algae;
        n_dead_algae -= n_decomp_algae;

        MeTrX_litter_distribution( 0.25 * n_decomp_algae, 0.7 * n_decomp_algae, 0.05 * n_decomp_algae,
                                   0.25 * c_decomp_algae, 0.7 * c_decomp_algae, 0.05 * c_decomp_algae,
                                   accumulated_n_litter_from_algae_sl);

        dC_litter_algae += c_decomp_algae;

        if ( !cbm::flt_greater_zero( c_dead_algae))
        {
            c_dead_algae = 0.0;
        }
        if ( !cbm::flt_greater_zero( n_dead_algae))
        {
            n_dead_algae = 0.0;
        }
    }


    /******************/
    /* dung and urine */
    /******************/

    if ( cbm::flt_greater_zero( sc_.c_dung) ||
         cbm::flt_greater_zero( sc_.n_dung) ||
         cbm::flt_greater_zero( sc_.n_urine))
    {
        MeTrX_litter_distribution( 0.25 * sc_.n_dung, 0.7 * sc_.n_dung, 0.05 * sc_.n_dung,
                                   0.25 * sc_.c_dung, 0.7 * sc_.c_dung, 0.05 * sc_.c_dung,
                                   accumulated_n_litter_from_dung_sl);
        
        for( size_t  sl = 0;  sl <= SL_SURFACE_DISTRIBUTION;  ++sl)
        {
            sc_.urea_sl[sl] += sc_.n_urine * SL_SURFACE_DISTRIBUTION_INVERSE;
            accumulated_n_urea_from_dung_sl[sl] += sc_.n_urine * SL_SURFACE_DISTRIBUTION_INVERSE;
        }

        sc_.accumulated_c_livestock_grazing += sc_.c_dung;
        sc_.accumulated_n_livestock_grazing += sc_.n_dung + sc_.n_urine;

        sc_.c_dung = 0.0;
        sc_.n_dung = 0.0;
        sc_.n_urine = 0.0;
    }

    return  LDNDC_ERR_OK;
}


/*!
 * @brief
 *  Adds carbon and nitrogen litter to soil litter pools
 */
void
SoilChemistryMeTrX::MeTrX_litter_distribution(
                                              double const &_add_N_sol,
                                              double const &_add_N_cel,
                                              double const &_add_N_lig,
                                              double const &_add_C_sol,
                                              double const &_add_C_cel,
                                              double const &_add_C_lig,
                                              CBM_Vector< double > &accumulated_litter_sl)
{
    for (size_t sl = 0; sl <= SL_SURFACE_DISTRIBUTION; ++sl)
    {
        sc_.C_lit1_sl[sl] += _add_C_sol * SL_SURFACE_DISTRIBUTION_INVERSE;
        sc_.C_lit2_sl[sl] += _add_C_cel * SL_SURFACE_DISTRIBUTION_INVERSE;
        sc_.C_lit3_sl[sl] += _add_C_lig * SL_SURFACE_DISTRIBUTION_INVERSE;

        sc_.N_lit1_sl[sl] += _add_N_sol * SL_SURFACE_DISTRIBUTION_INVERSE;
        sc_.N_lit2_sl[sl] += _add_N_cel * SL_SURFACE_DISTRIBUTION_INVERSE;
        sc_.N_lit3_sl[sl] += _add_N_lig * SL_SURFACE_DISTRIBUTION_INVERSE;

        accumulated_litter_sl[sl] += (_add_N_sol + _add_N_cel + _add_N_lig) * SL_SURFACE_DISTRIBUTION_INVERSE;
    }
}

} /*namespace ldndc*/
