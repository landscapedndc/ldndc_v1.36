/*!
 * @file
 *  David Kraus (created on: may 12, 2014)
 *
 */

#include  "soilchemistry/metrx/soilchemistry-metrx.h"

#include  <constants/lconstants-conv.h>
#include  <constants/lconstants-chem.h>


namespace ldndc {



/*!
 * @brief
 *    Allocation of synthetic fertilizer
 *
 */
lerr_t
SoilChemistryMeTrX::MeTrX_fertilize()
{
    EventAttributes const *  ev_fert = NULL;
    while (( ev_fert = this->m_FertilizeEvents.pop()) != NULL)
    {
        double new_fertilizer_n( -sc_.accumulated_n_fertilizer);
        //
        cbm::string_t const fertilizer = ev_fert->get( "/type", "?");
        //
        double const  f_depth = ev_fert->get( "/depth", 0.0);

        // amount of fertilizer [kg:ha-1]
        double const n_amount( ev_fert->get( "/amount", 0.0));

        // amount of nitrification inhibitor [kg:ha-1]
        double const ni_amount( ev_fert->get( "/ni_amount", 0.0));

        // amount of urease inhibitor [kg:ha-1]
        double const ui_amount( ev_fert->get( "/ui_amount", 0.0));

        if ( have_water_table && cbm::flt_equal_zero( f_depth))
        {
            double const n_amount_sbl( n_amount / (cbm::M2_IN_HA * sb_.surfacebulk_layer_cnt()));
            double const ni_amount_sbl( ni_amount / (cbm::M2_IN_HA * sb_.surfacebulk_layer_cnt()));
            double const ui_amount_sbl( ui_amount / (cbm::M2_IN_HA * sb_.surfacebulk_layer_cnt()));

            for (size_t sbl = 0; sbl < sb_.surfacebulk_layer_cnt(); ++sbl)
            {
                if ( fertilizer == "nh4no3" )
                {
                    sb_.nh4_sbl[sbl] += 0.5 * n_amount_sbl;
                    sb_.no3_sbl[sbl] += 0.5 * n_amount_sbl;
                    accumulated_n_nh4_fertilization_sbl[sbl] += 0.5 * n_amount_sbl;
                    accumulated_n_no3_fertilization_sbl[sbl] += 0.5 * n_amount_sbl;
                    sc_.accumulated_n_fertilizer += n_amount_sbl;
                }
                else if ( fertilizer == "nh3" )
                {
                    sb_.nh3_sbl[sbl] += n_amount_sbl;
                    accumulated_n_nh3_fertilization_sbl[sbl] += n_amount_sbl;
                    sc_.accumulated_n_fertilizer += n_amount_sbl;
                }
                else if ( fertilizer == "no3" )
                {
                    sb_.no3_sbl[sbl] += n_amount_sbl;
                    accumulated_n_no3_fertilization_sbl[sbl] += n_amount_sbl;
                    sc_.accumulated_n_fertilizer += n_amount_sbl;
                }
                else if (( fertilizer == "nh4" ) || ( fertilizer == "nh4hco3" ) || ( fertilizer == "nh4hpo4" ))
                {
                    sb_.nh4_sbl[sbl] += n_amount_sbl;
                    accumulated_n_nh4_fertilization_sbl[sbl] += n_amount_sbl;
                    sc_.accumulated_n_fertilizer += n_amount_sbl;
                }
                else if ( fertilizer == "nh4so4" )
                {
                    sb_.so4_sbl[sbl] += n_amount_sbl * (cbm::MS / cbm::MN);

                    sb_.nh4_sbl[sbl] += n_amount_sbl;
                    accumulated_n_nh4_fertilization_sbl[sbl] += n_amount_sbl;
                    sc_.accumulated_n_fertilizer += n_amount_sbl;
                }
                else if ( fertilizer == "so4" )
                {
                    sb_.so4_sbl[sbl] += n_amount_sbl;
                }
                else if ( fertilizer == "urea" )
                {
                    sb_.urea_sbl[sbl] += n_amount_sbl;
                    accumulated_n_urea_fertilization_sbl[sbl] += n_amount_sbl;
                    sc_.accumulated_n_fertilizer += n_amount_sbl;
                }
                /* controled release nitrogen fertilizer */
                else if ( fertilizer == "crnf" )
                {
                    crnf_.add_crnf( n_amount_sbl);

                    sb_.coated_nh4_sbl[sbl] += n_amount_sbl;
                    sc_.accumulated_n_fertilizer += n_amount_sbl;
                }
                else
                {
                    KLOGWARN( "Unknown fertilizer type \"",fertilizer,"\". Ignoring event!");
                }

                sb_.ni_sbl[sbl] += ni_amount_sbl;
                sb_.ui_sbl[sbl] += ui_amount_sbl;
                crnf_.add_nitrification_inhibitior( ni_amount_sbl);
            }
        }
        else
        {
            //l_0 not allowed to be deeper than last but one soil layer
            size_t l_0( sl_.soil_layer_cnt()-2);
            //find first layer (from top) having at least depth <depth(fert)>
            for ( size_t  l = 0;  l < sl_.soil_layer_cnt()-1;  ++l)
            {
                if ( cbm::flt_greater_equal( sc_.depth_sl[l], f_depth))
                {
                    l_0 = l;
                    break;
                }
            }
            //l_1 must at least one layer below l_0
            size_t l_1( l_0 + 1);
            for ( size_t  l = l_0 + 1;  l < sl_.soil_layer_cnt();  ++l)
            {
                /* distribute at least among 4cm soil profile */
                if ( cbm::flt_greater_equal( sc_.depth_sl[l], f_depth + 0.04))
                {
                    l_1 = l;
                    break;
                }
            }

            /* check if we found soil layer for given fertilizing depth */
            if ( l_0 == sl_.soil_layer_cnt())
            {
                KLOGERROR( "Fertilizing depth below deepest strata.",
                          " Failed to fulfill your request :-(",
                          " [type=", fertilizer,",depth=", f_depth,"]");
                return  LDNDC_ERR_FAIL;
            }

            double const n_amount_per_layer = (n_amount / cbm::M2_IN_HA) / (double)( l_1 - l_0);
            double const ni_amount_per_layer = (ni_amount / cbm::M2_IN_HA) / (double)( l_1 - l_0);
            double const ui_amount_per_layer = (ui_amount / cbm::M2_IN_HA) / (double)( l_1 - l_0);

            for ( size_t  sl = l_0;  sl < l_1;  ++sl)
            {
                if ( fertilizer == "nh4no3" )
                {
                    nh4_ae_sl[sl] += 0.5 * n_amount_per_layer * (1.0 - sc_.anvf_sl[sl]);
                    nh4_an_sl[sl] += 0.5 * n_amount_per_layer * sc_.anvf_sl[sl];
                    no3_ae_sl[sl] += 0.5 * n_amount_per_layer * (1.0 - sc_.anvf_sl[sl]);
                    no3_an_sl[sl] += 0.5 * n_amount_per_layer * sc_.anvf_sl[sl];
                    accumulated_n_nh4_fertilization_sl[sl] += 0.5 * n_amount_per_layer;
                    accumulated_n_no3_fertilization_sl[sl] += 0.5 * n_amount_per_layer;
                    sc_.accumulated_n_fertilizer += n_amount_per_layer;
                }
                else if ( fertilizer == "nh3" )
                {
                    sc_.nh3_liq_sl[sl] += n_amount_per_layer;
                    accumulated_n_nh3_fertilization_sl[sl] += n_amount_per_layer;
                    sc_.accumulated_n_fertilizer += n_amount_per_layer;
                }
                else if ( fertilizer == "no3" )
                {
                    no3_ae_sl[sl] += n_amount_per_layer * (1.0 - sc_.anvf_sl[sl]);
                    no3_an_sl[sl] += n_amount_per_layer * sc_.anvf_sl[sl];
                    accumulated_n_no3_fertilization_sl[sl] += n_amount_per_layer;
                    sc_.accumulated_n_fertilizer += n_amount_per_layer;
                }
                else if (( fertilizer == "nh4" ) || ( fertilizer == "nh4hco3" ) || ( fertilizer == "nh4hpo4" ))
                {
                    nh4_ae_sl[sl] += n_amount_per_layer * (1.0 - sc_.anvf_sl[sl]);
                    nh4_an_sl[sl] += n_amount_per_layer * sc_.anvf_sl[sl];
                    accumulated_n_nh4_fertilization_sl[sl] += n_amount_per_layer;
                    sc_.accumulated_n_fertilizer += n_amount_per_layer;
                }
                else if ( fertilizer == "nh4so4" )
                {
                    sc_.so4_sl[sl] += n_amount_per_layer * (cbm::MS / cbm::MN);

                    nh4_ae_sl[sl] += n_amount_per_layer * (1.0 - sc_.anvf_sl[sl]);
                    nh4_an_sl[sl] += n_amount_per_layer * sc_.anvf_sl[sl];
                    accumulated_n_nh4_fertilization_sl[sl] += n_amount_per_layer;
                    sc_.accumulated_n_fertilizer += n_amount_per_layer;
                }
                else if ( fertilizer == "so4" )
                {
                    sc_.so4_sl[sl] += n_amount_per_layer;
                }
                else if ( fertilizer == "urea" )
                {
                    sc_.urea_sl[sl] += n_amount_per_layer;
                    accumulated_n_urea_fertilization_sl[sl] += n_amount_per_layer;
                    sc_.accumulated_n_fertilizer += n_amount_per_layer;
                }
                /* controled release nitrogen fertilizer */
                else if ( fertilizer == "crnf" )
                {
                    crnf_.add_crnf( n_amount_per_layer);

                    sc_.coated_nh4_sl[sl] += n_amount_per_layer;
                    accumulated_n_nh4_fertilization_sl[sl] += n_amount_per_layer;
                    sc_.accumulated_n_fertilizer += n_amount_per_layer;
                }
                else
                {
                    KLOGWARN( "Unknown fertilizer type \"",fertilizer,"\". Skipping event!");
                    break;
                }

                sc_.ni_sl[sl] += ni_amount_per_layer;
                sc_.ui_sl[sl] += ui_amount_per_layer;
                crnf_.add_nitrification_inhibitior( ni_amount_per_layer);
            }
        }

        cbm::state_scratch_t *  mcom = io_kcomm->get_scratch();

        new_fertilizer_n += sc_.accumulated_n_fertilizer;
        double old_fertilizer_n( 0.0);
        mcom->get( "fertilize:amount", &old_fertilizer_n, 0.0);
        mcom->set( "fertilize:amount", old_fertilizer_n + (new_fertilizer_n * cbm::M2_IN_HA));
    }

    return  LDNDC_ERR_OK;
}



/*!
 * @brief
 *    Allocation of organic fertilizer
 */
lerr_t
SoilChemistryMeTrX::MeTrX_manure()
{
    EventAttributes const *  ev_manure = NULL;
    while (( ev_manure = this->m_ManureEvents.pop()) != NULL)
    {
        double new_manure_n( -sc_.accumulated_n_fertilizer);
        double new_manure_c( -sc_.accumulated_c_fertilizer);

        double  me_c = ev_manure->get( "/carbon", -1.0);
        if ( cbm::flt_less( me_c, 0.0))
        {
            KLOGERROR( "manure event: carbon and nitrogen amount must be greater 0",
                      "  [carbon=",me_c,"]");
            return  LDNDC_ERR_FAIL;
        }
        double  me_cn = ev_manure->get( "/carbon-nitrogen-ratio", -1.0);
        if ( cbm::flt_less( me_cn, 0.0))
        {
            KLOGERROR( "manure event: carbon/nitrogen ratio must be greater 0",
                      "  [carbon/nitrogen ratio=",me_cn,"]");
            return  LDNDC_ERR_FAIL;
        }

        double add_c( me_c * cbm::HA_IN_M2);
        double add_n( add_c / me_cn);

        sc_.accumulated_c_fertilizer += add_c;
        sc_.accumulated_n_fertilizer += add_n;

        double add_n_nh4( 0.0);
        double add_n_no3( 0.0);
        double add_n_don( 0.0);
        double add_n_urea( 0.0);

        double add_c_doc( 0.0);

        double add_c_aorg( 0.0);
        double add_n_aorg( 0.0);

        double add_c_lit_1( 0.0);
        double add_c_lit_2( 0.0);
        double add_c_lit_3( 0.0);

        double add_n_lit_1( 0.0);
        double add_n_lit_2( 0.0);
        double add_n_lit_3( 0.0);

        double  ph_manure = ev_manure->get( "/ph", -1.0);

        cbm::string_t const &  manure = ev_manure->get( "/type", "-");
        if ( manure == "slurry" )
        {
            if ( !cbm::flt_greater_zero( ph_manure))
            {
                ph_manure = 7.0;
            }

            double const  avail_n = ev_manure->get( "/nitrogen-available", sipar_.FRAC_LABILE_N_SLURRY());
            if ( !cbm::flt_greater_zero( avail_n))
            {
                KLOGERROR( "manure event (",manure,"): fraction of available nitrogen must be at least 0",
                          "  [available nitrogen=",avail_n,"]");
                return  LDNDC_ERR_FAIL;
            }
            double const  avail_c = ev_manure->get( "/carbon-available", sipar_.FRAC_LABILE_C_SLURRY());
            if ( !cbm::flt_greater_zero( avail_c))
            {
                KLOGERROR( "manure event (",manure,"): fraction of available carbon must be at least 0",
                          "  [available carbon=",avail_c,"]");
                return  LDNDC_ERR_FAIL;
            }

            double const  liq_n = avail_n * add_n;
            double const  f_urea = ev_manure->get( "/urea-fraction", sipar_.LIQUREA_SLURRY());
            add_n_urea = f_urea * liq_n;
            double const  f_nh4 = ev_manure->get( "/nh4-fraction", sipar_.LIQNH4_SLURRY());
            add_n_nh4 = f_nh4 * liq_n;
            double const  f_don = ev_manure->get( "/don-fraction", sipar_.LIQDON_SLURRY());
            add_n_don = f_don * liq_n;
            double const  f_no3 = ev_manure->get( "/no3-fraction", sipar_.LIQNO3_SLURRY());
            add_n_no3 = f_no3 * liq_n;

            if (cbm::flt_not_equal_zero( add_n_urea + add_n_nh4 + add_n_don + add_n_no3 - liq_n))
            {
                KLOGERROR( "manure event (",manure,"): fractions of liquid nitrogen do not add up to 1.0!",
                          " check UREA fraction, NH4 fraction, DON fraction and NO3 fraction");
                return  LDNDC_ERR_FAIL;
            }

            add_c_doc = avail_c * add_c;
            
            double const add_c_res( add_c - add_c_doc);
            double const add_n_res( add_n - add_n_don - add_n_nh4 - add_n_no3 - add_n_urea);
            
            double const f_lignin = ev_manure->get( "/lignin-fraction", 0.2); // Bhogal et al. (2011), Levi-Minzi et al. (1986), Probert et al. (2005)
            double const frac_c_lit_3( f_lignin);
            double const f_cellulose = ev_manure->get( "/cellulose-fraction", 0.6); // Bhogal et al. (2011), Levi-Minzi et al. (1986)
            double const frac_c_lit_2( f_cellulose);
            double const frac_c_lit_1( 1.0 - frac_c_lit_2 - frac_c_lit_3);

            if (( f_lignin + f_cellulose) > 1.0)
            {
                KLOGERROR( "manure event (",manure,"): fractions of litter do not add up to 1.0!",
                          " check lignin fraction and cellulose fraction");
                return  LDNDC_ERR_FAIL;
            }

            add_c_lit_1 += ( frac_c_lit_1 * add_c_res);
            add_c_lit_2 += ( frac_c_lit_2 * add_c_res);
            add_c_lit_3 += ( frac_c_lit_3 * add_c_res);
            
            add_n_lit_1 += ( frac_c_lit_1 * add_n_res);
            add_n_lit_2 += ( frac_c_lit_2 * add_n_res);
            add_n_lit_3 += ( frac_c_lit_3 * add_n_res);
        }
        else if ( manure == "green" )
        {
            add_c_lit_1 = 0.2 * add_c;
            add_c_lit_2 = 0.7 * add_c;
            add_c_lit_3 = 0.1 * add_c;
            
            add_n_lit_1 = 0.2 * add_n;
            add_n_lit_2 = 0.7 * add_n;
            add_n_lit_3 = 0.1 * add_n;
        }
        else if ( manure == "straw" )
        {
            add_c_lit_1 = 0.2 * add_c;
            add_c_lit_2 = 0.7 * add_c;
            add_c_lit_3 = 0.1 * add_c;
            
            add_n_lit_1 = 0.2 * add_n;
            add_n_lit_2 = 0.7 * add_n;
            add_n_lit_3 = 0.1 * add_n;
        }
        else if ( manure == "farmyard" )
        {
            double const  avail_n = ev_manure->get( "/nitrogen-available", sipar_.FRAC_LABILE_N_FARMYARD());
            if ( !cbm::flt_greater_zero( avail_n))
            {
                KLOGERROR( "manure event (",manure,"): fraction of available nitrogen must be at least 0",
                          "  [available nitrogen=",avail_n,"]");
                return  LDNDC_ERR_FAIL;
            }
            double const  avail_c = ev_manure->get( "/carbon-available", sipar_.FRAC_LABILE_C_FARMYARD());
            if ( !cbm::flt_greater_zero( avail_c))
            {
                KLOGERROR( "manure event (",manure,"): fraction of available carbon must be at least 0",
                          "  [available carbon=",avail_c,"]");
                return  LDNDC_ERR_FAIL;
            }

            double const  liq_n = avail_n * add_n;
            double const  f_urea = ev_manure->get( "/urea-fraction", sipar_.LIQUREA_FARMYARD());
            add_n_urea = f_urea * liq_n;
            double const  f_nh4 = ev_manure->get( "/nh4-fraction", sipar_.LIQNH4_FARMYARD());
            add_n_nh4 = f_nh4 * liq_n;
            double const  f_don = ev_manure->get( "/don-fraction", sipar_.LIQDON_FARMYARD());
            add_n_don = f_don * liq_n;
            double const  f_no3 = ev_manure->get( "/no3-fraction", sipar_.LIQNO3_FARMYARD());
            add_n_no3 = f_no3 * liq_n;

            if (cbm::flt_not_equal_zero( add_n_urea + add_n_nh4 + add_n_don + add_n_no3 - liq_n))
            {
                KLOGERROR( "manure event (",manure,"): fractions of liquid nitrogen do not add up to 1.0!",
                          " check UREA fraction, NH4 fraction, DON fraction and NO3 fraction");
                return  LDNDC_ERR_FAIL;
            }
            
            add_c_doc = avail_c * add_c;
            
            double const add_c_res( add_c - add_c_doc);
            double const add_n_res( add_n - add_n_don - add_n_nh4 - add_n_no3 - add_n_urea);
            
            double const f_lignin = ev_manure->get( "/lignin-fraction", 0.2); // Bhogal et al. (2011), Levi-Minzi et al. (1986), Probert et al. (2005)
            double const frac_c_lit_3( f_lignin);
            double const f_cellulose = ev_manure->get( "/cellulose-fraction", 0.6); // Bhogal et al. (2011), Levi-Minzi et al. (1986)
            double const frac_c_lit_2( f_cellulose);
            double const frac_c_lit_1( 1.0 - frac_c_lit_2 - frac_c_lit_3);
            
            if (( f_lignin + f_cellulose) > 1.0)
            {
                KLOGERROR( "manure event (",manure,"): fractions of litter do not add up to 1.0!",
                          " check lignin fraction and cellulose fraction");
                return  LDNDC_ERR_FAIL;
            }

            add_c_lit_1 += ( frac_c_lit_1 * add_c_res);
            add_c_lit_2 += ( frac_c_lit_2 * add_c_res);
            add_c_lit_3 += ( frac_c_lit_3 * add_c_res);
            
            add_n_lit_1 += ( frac_c_lit_1 * add_n_res);
            add_n_lit_2 += ( frac_c_lit_2 * add_n_res);
            add_n_lit_3 += ( frac_c_lit_3 * add_n_res);
        }
        else if ( manure == "compost" )
        {
            double const  avail_n = ev_manure->get( "/nitrogen-available", sipar_.FRAC_LABILE_N_COMPOST());
            if ( avail_n < 0.0)
            {
                KLOGERROR( "manure event (",manure,"): fraction of available nitrogen must be at least 0",
                          "  [available nitrogen=",avail_n,"]");
                return  LDNDC_ERR_FAIL;
            }
            double const  avail_c = ev_manure->get( "/carbon-available", sipar_.FRAC_LABILE_C_COMPOST());
            if ( avail_c < 0.0)
            {
                KLOGERROR( "manure event (",manure,"): fraction of available carbon must be at least 0",
                          "  [available carbon=",avail_c,"]");
                return  LDNDC_ERR_FAIL;
            }

            double const  liq_n = avail_n * add_n;
            double const  f_urea = ev_manure->get( "/urea-fraction", sipar_.LIQUREA_COMPOST());
            add_n_urea = f_urea * liq_n;
            double const  f_nh4 = ev_manure->get( "/nh4-fraction", sipar_.LIQNH4_COMPOST());
            add_n_nh4 = f_nh4 * liq_n;
            double const  f_don = ev_manure->get( "/don-fraction", sipar_.LIQDON_COMPOST());
            add_n_don = f_don * liq_n;
            double const  f_no3 = ev_manure->get( "/no3-fraction", sipar_.LIQNO3_COMPOST());
            add_n_no3 = f_no3 * liq_n;

            if (cbm::flt_not_equal_zero( add_n_urea+add_n_nh4+add_n_don+add_n_no3-liq_n))
            {
                KLOGERROR( "manure event (",manure,"): fractions of liquid nitrogen do not add up to 1.0!",
                          " check UREA fraction, NH4 fraction, DON fraction and NO3 fraction");
                return  LDNDC_ERR_FAIL;
            }

            add_c_doc = ( avail_c * add_c);

            double const add_c_res( add_c - add_c_doc);
            double const add_n_res( add_n - add_n_don - add_n_nh4 - add_n_no3 - add_n_urea);

            double const f_lignin = ev_manure->get( "/lignin-fraction", 0.8);
            double const frac_c_lit_3( f_lignin);
            double const f_cellulose = ev_manure->get( "/cellulose-fraction", 0.1);
            double const frac_c_lit_2( f_cellulose);
            double const frac_c_lit_1( 1.0 - frac_c_lit_2 - frac_c_lit_3);

            if (( f_lignin + f_cellulose) > 1.0)
            {
                KLOGERROR( "manure event (",manure,"): fractions of litter do not add up to 1.0!",
                          " check lignin fraction and cellulose fraction");
                return  LDNDC_ERR_FAIL;
            }

            add_c_lit_1 += ( frac_c_lit_1 * add_c_res);
            add_c_lit_2 += ( frac_c_lit_2 * add_c_res);
            add_c_lit_3 += ( frac_c_lit_3 * add_c_res);

            add_n_lit_1 += ( frac_c_lit_1 * add_n_res);
            add_n_lit_2 += ( frac_c_lit_2 * add_n_res);
            add_n_lit_3 += ( frac_c_lit_3 * add_n_res);
        }
        else
        {
            KLOGWARN( "Unknown manure type \"",manure,"\". Skipping event!");
            break;
        }

        double const  depth( ev_manure->get( "/depth", 0.0));

        double sl_max( 0);
        double h_max( sc_.h_sl[0]);

        for ( size_t  sl = 1;  sl < sl_.soil_layer_cnt();  ++sl)
        {
            if ( cbm::flt_greater_equal( depth, sc_.depth_sl[sl]))
            {
                h_max += sc_.h_sl[sl];
                sl_max += 1;
            }
            else{ break; }
        }

        for ( size_t  sl = 0;  sl <= sl_max;  ++sl)
        {
            double const layer_fraction( sc_.h_sl[sl] / h_max);
            if ( cbm::flt_greater_zero( ph_manure))
            {
                sc_.ph_sl[sl] = ph_manure;
            }
            double const layer_fraction_ae( (1.0 - sc_.anvf_sl[sl]) * layer_fraction);
            double const layer_fraction_an( sc_.anvf_sl[sl] * layer_fraction);

            don_ae_sl[sl] += add_n_don * layer_fraction_ae;
            don_an_sl[sl] += add_n_don * layer_fraction_an;
            nh4_ae_sl[sl] += add_n_nh4 * layer_fraction_ae;
            nh4_an_sl[sl] += add_n_nh4 * layer_fraction_an;
            no3_ae_sl[sl] += add_n_no3 * layer_fraction_ae;
            no3_an_sl[sl] += add_n_no3 * layer_fraction_an;
            sc_.urea_sl[sl] += add_n_urea * layer_fraction;

            sc_.doc_sl[sl]    += add_c_doc * layer_fraction_ae;
            sc_.an_doc_sl[sl] += add_c_doc * layer_fraction_an;

            sc_.C_aorg_sl[sl] += add_c_aorg * layer_fraction;
            sc_.N_aorg_sl[sl] += add_n_aorg * layer_fraction;

            sc_.C_lit1_sl[sl] += add_c_lit_1 * layer_fraction;
            sc_.C_lit2_sl[sl] += add_c_lit_2 * layer_fraction;
            sc_.C_lit3_sl[sl] += add_c_lit_3 * layer_fraction;

            sc_.N_lit1_sl[sl] += add_n_lit_1 * layer_fraction;
            sc_.N_lit2_sl[sl] += add_n_lit_2 * layer_fraction;
            sc_.N_lit3_sl[sl] += add_n_lit_3 * layer_fraction;

            accumulated_n_nh4_fertilization_sl[sl] += add_n_nh4 * layer_fraction;
            accumulated_n_no3_fertilization_sl[sl] += add_n_no3 * layer_fraction;
            accumulated_n_don_fertilization_sl[sl] += add_n_don * layer_fraction;
            accumulated_n_urea_fertilization_sl[sl] += add_n_urea * layer_fraction;
            accumulated_n_litter_fertilization_sl[sl] += (add_n_lit_1 + add_n_lit_2 + add_n_lit_3) * layer_fraction;
            accumulated_n_aorg_fertilization_sl[sl] += add_n_aorg * layer_fraction;
        }

        cbm::state_scratch_t *  mcom = io_kcomm->get_scratch();

        new_manure_n += sc_.accumulated_n_fertilizer;
        double old_manure_n( 0.0);
        mcom->get( "manure:amountN", &old_manure_n, 0.0);
        mcom->set( "manure:amountN", old_manure_n + (new_manure_n * cbm::M2_IN_HA));

        new_manure_c += sc_.accumulated_c_fertilizer;
        double old_manure_c( 0.0);
        mcom->get( "manure:amountC", &old_manure_c, 0.0);
        mcom->set( "manure:amountC", old_manure_c + (new_manure_c * cbm::M2_IN_HA));
    }

    return  LDNDC_ERR_OK;
}



/*!
 * @brief
 *    tilling event increases decomposition (sc_.till_effect_sl).
 *    stubble are incorporated to soil litter pools.
 *    all soil pools as well as water are distributed homogeneously.
 */
lerr_t
SoilChemistryMeTrX::MeTrX_till()
{
    if ( cbm::flt_greater( sc_.till_effect_sl[0], 1.01))
    {
        for( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
        {
            sc_.till_effect_sl[sl] = cbm::bound_min( 1.0, sc_.till_effect_sl[sl] * (1.0 - (sipar_.METRX_TILL_STIMULATION_2() / this->lclock()->time_resolution())));
        }
    }
    else if ( cbm::flt_greater( sc_.till_effect_sl[0], 1.0))
    {
        for( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
        {
            sc_.till_effect_sl[sl] = 1.0;
        }
    }


    if ( this->m_TillEvents.is_empty())
    {
        return  LDNDC_ERR_OK;
    }

    while ( this->m_TillEvents)
    {
        EventAttributes  ev_till = this->m_TillEvents.pop();

        /***************************/
        /* tilling depth and layer */
        /***************************/

        double  till_depth( ev_till.get( "/depth", 0.0));
        if ( till_depth > 0.5)
        {
            KLOGINFO( "tilling depth greater than 0.5m, reset to 0.5m [depth=",till_depth, "m,time=",lclock_ref().to_string(),"]");
            till_depth = 0.5;
        }

        unsigned int till_layer( SL_SURFACE_DISTRIBUTION);
        if ( cbm::flt_greater_zero( till_depth))
        {
            for( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
            {
                if ( sc_.depth_sl[sl] <= till_depth)
                {
                    sc_.till_effect_sl[sl] = sipar_.METRX_TILL_STIMULATION_1();
                    till_layer = std::max( SL_SURFACE_DISTRIBUTION, sl);
                }
                else { break; }
            }
        }

        /**************************************************/
        /* stubble, root and surface litter incorporation */
        /**************************************************/

        sc_.C_lit1_sl[0] += sc_.c_stubble_lit1 + sc_.c_raw_lit_1_sl.sum();
        sc_.C_lit2_sl[0] += sc_.c_stubble_lit2 + sc_.c_raw_lit_2_sl.sum();
        sc_.C_lit3_sl[0] += sc_.c_stubble_lit3 + sc_.c_raw_lit_3_sl.sum();

        sc_.N_lit1_sl[0] += sc_.n_stubble_lit1 + sc_.n_raw_lit_1_sl.sum();
        sc_.N_lit2_sl[0] += sc_.n_stubble_lit2 + sc_.n_raw_lit_2_sl.sum();
        sc_.N_lit3_sl[0] += sc_.n_stubble_lit3 + sc_.n_raw_lit_3_sl.sum();

        accumulated_n_surface_litter_incorporation_via_tilling_sl[0] += sc_.n_stubble_lit1 + sc_.n_stubble_lit2 + sc_.n_stubble_lit3 ;
        accumulated_n_litter_tilling_sl[0] += sc_.n_raw_lit_1_sl.sum() + sc_.n_raw_lit_2_sl.sum() + sc_.n_raw_lit_3_sl.sum();

        sc_.c_stubble_lit1 = sc_.c_stubble_lit2 = sc_.c_stubble_lit3 = 0.0;
        sc_.n_stubble_lit1 = sc_.n_stubble_lit2 = sc_.n_stubble_lit3 = 0.0;

        for( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
         {
             accumulated_n_litter_tilling_sl[sl] -= sc_.N_lit1_sl[sl] + sc_.N_lit2_sl[sl] + sc_.N_lit3_sl[sl] + sc_.n_raw_lit_1_sl[sl] + sc_.n_raw_lit_2_sl[sl] + sc_.n_raw_lit_3_sl[sl];
             accumulated_n_aorg_tilling_sl[sl] -= sc_.N_aorg_sl[sl];
             accumulated_n_humus_1_tilling_sl[sl] -= n_humus_1_sl[sl];
             accumulated_n_humus_2_tilling_sl[sl] -= n_humus_2_sl[sl];
             accumulated_n_humus_3_tilling_sl[sl] -= n_humus_3_sl[sl];
             accumulated_n_no3_tilling_sl[sl] -= no3_ae_sl[sl] + no3_an_sl[sl];
             accumulated_n_no2_tilling_sl[sl] -= sc_.no2_sl[sl] + sc_.an_no2_sl[sl];
             accumulated_n_no_tilling_sl[sl] -= no_gas_sl[sl] + no_liq_sl[sl] + an_no_gas_sl[sl] + an_no_liq_sl[sl];
             accumulated_n_n2o_tilling_sl[sl] -= n2o_gas_sl[sl] + n2o_liq_sl[sl] + an_n2o_gas_sl[sl] + an_n2o_liq_sl[sl];
             accumulated_n_urea_tilling_sl[sl] -= sc_.urea_sl[sl];
             accumulated_n_don_tilling_sl[sl] -= don_ae_sl[sl] + don_an_sl[sl];
             accumulated_n_nh4_tilling_sl[sl] -= nh4_ae_sl[sl] + nh4_an_sl[sl] + sc_.clay_nh4_sl[sl];
             accumulated_n_nh3_tilling_sl[sl] -= sc_.nh3_gas_sl[sl] + sc_.nh3_liq_sl[sl];
             accumulated_n_microbes_tilling_sl[sl] -= n_micro_1_sl[sl] + n_micro_2_sl[sl] + n_micro_3_sl[sl];
         }

         sc_.c_raw_lit_1_sl = sc_.c_raw_lit_2_sl = sc_.c_raw_lit_3_sl = 0.0;
         sc_.n_raw_lit_1_sl = sc_.n_raw_lit_2_sl = sc_.n_raw_lit_3_sl = 0.0;

        /****************************************/
        /* rearranging soil pools after tilling */
        /****************************************/

        lvector_t< double >  pool_buf[] =
        {
            sc_.fe2_sl, sc_.fe3_sl,
            sc_.C_lit1_sl, sc_.C_lit2_sl, sc_.C_lit3_sl,
            sc_.N_lit1_sl, sc_.N_lit2_sl, sc_.N_lit3_sl,
            sc_.C_aorg_sl, sc_.N_aorg_sl,
            sc_.C_micro1_sl, sc_.C_micro2_sl, sc_.C_micro3_sl,
            n_micro_1_sl, n_micro_2_sl, n_micro_3_sl,
            c_humus_1_sl, c_humus_2_sl, c_humus_3_sl,
            n_humus_1_sl, n_humus_2_sl, n_humus_3_sl,
            sc_.doc_sl, sc_.an_doc_sl,
            ae_acetate_sl, an_acetate_sl,
            no3_ae_sl, no3_an_sl,
            sc_.no2_sl, sc_.an_no2_sl,
            don_ae_sl, don_an_sl,
            nh4_ae_sl, nh4_an_sl, sc_.clay_nh4_sl,
            sc_.urea_sl, sc_.nh3_gas_sl, sc_.nh3_liq_sl,
            no_gas_sl, no_liq_sl, an_no_gas_sl, an_no_liq_sl,
            n2o_gas_sl, n2o_liq_sl, an_n2o_gas_sl, an_n2o_liq_sl
        };

        size_t const  pools_cnt( sizeof( pool_buf) / sizeof(lvector_t< double >));
        for ( size_t  k = 0;  k < pools_cnt;  ++k)
        {
            // summing up pool down to tilling depth
            double  pool_sum( pool_buf[k].sum( till_layer) / sc_.depth_sl[till_layer-1]);
            for ( size_t  l = 0;  l < till_layer;  l++)
            {
                pool_buf[k][l] = pool_sum * sc_.h_sl[l];
            }
        }

        for( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
        {
            accumulated_n_litter_tilling_sl[sl] += sc_.N_lit1_sl[sl] + sc_.N_lit2_sl[sl] + sc_.N_lit3_sl[sl] + sc_.n_raw_lit_1_sl[sl] + sc_.n_raw_lit_2_sl[sl] + sc_.n_raw_lit_3_sl[sl];
            accumulated_n_aorg_tilling_sl[sl] += sc_.N_aorg_sl[sl];
            accumulated_n_humus_1_tilling_sl[sl] += n_humus_1_sl[sl];
            accumulated_n_humus_2_tilling_sl[sl] += n_humus_2_sl[sl];
            accumulated_n_humus_3_tilling_sl[sl] += n_humus_3_sl[sl];
            accumulated_n_no3_tilling_sl[sl] += no3_ae_sl[sl] + no3_an_sl[sl];
            accumulated_n_no2_tilling_sl[sl] += sc_.no2_sl[sl] + sc_.an_no2_sl[sl];
            accumulated_n_no_tilling_sl[sl] += no_gas_sl[sl] + no_liq_sl[sl] + an_no_gas_sl[sl] + an_no_liq_sl[sl];
            accumulated_n_n2o_tilling_sl[sl] += n2o_gas_sl[sl] + n2o_liq_sl[sl] + an_n2o_gas_sl[sl] + an_n2o_liq_sl[sl];
            accumulated_n_urea_tilling_sl[sl] += sc_.urea_sl[sl];
            accumulated_n_don_tilling_sl[sl] += don_ae_sl[sl] + don_an_sl[sl];
            accumulated_n_nh4_tilling_sl[sl] += nh4_ae_sl[sl] + nh4_an_sl[sl] + sc_.clay_nh4_sl[sl];
            accumulated_n_nh3_tilling_sl[sl] += sc_.nh3_gas_sl[sl] + sc_.nh3_liq_sl[sl];
            accumulated_n_microbes_tilling_sl[sl] += n_micro_1_sl[sl] + n_micro_2_sl[sl] + n_micro_3_sl[sl];
        }
    }

    return  LDNDC_ERR_OK;
}

} /*namespace ldndc*/
