/*!
 * @file
 * @author
 *  David Kraus
 */

#include  "soilchemistry/metrx/output-soilchemistry-metrx-subdaily.h"

LMOD_MODULE_INFO(OutputSoilchemistryMeTrXSubdaily,TMODE_SUBDAILY,LMOD_FLAG_USER|LMOD_FLAG_OUTPUT);
namespace ldndc {

/*!
 * @page metrx
 * @subsection metrx_output_subdaily Subdaily output
 * entity name | decription | unit
 * ----------- | ---------- | ----
 * N\_nh4 | Ammonium | [kgNm-2]
 * N\_no3 | Nitrate | [kgNm-2]
 * C\_doc | Dissolved organic carbon | [kgCm-2]
 * sC\_ch4\_ebul | CH4 emissions via ebullition | [kgCm-2]
 * sC\_ch4\_plant | CH4 emissions via plant mediated diffusion | [kgCm-2]
 * sC\_ch4\_soil | CH4 emissions via soil surface | [kgCm-2]
 * sC\_ch4\_water | CH4 emissions via surface water table | [kgCm-2]
 * sC\_ch4\_leach | CH4 leaching | [kgCm-2]
 * sC\_ch4\_prod | CH4 production | [kgCm-2]
 * sC\_ch4\_ox | CH4 oxidation | [kgCm-2]
 * sC\_acetate\_prod | Acetate production | [kgCm-2]
 * sC\_doc\_prod | DOC production | [kgCm-2]
 * sN\_no3\_denit | NO3 denitrification | [kgNm-2]
 * sN\_n2o\_ebul | N2O emissions via ebullition | [kgNm-2]
 * sN\_n2o\_plant | N2O emissions via plant mediated diffusion | [kgNm-2]
 * sN\_n2o\_soil | N2O emissions via soil surface | [kgNm-2]
 * sN\_n2o\_water | N2O emissions via surface water table | [kgNm-2]
 * sN\_no\_ebul | NO emissions via ebullition | [kgNm-2]
 * sN\_no\_plant | NO emissions via plant mediated diffusion | [kgNm-2]
 * sN\_no\_soil | NO emissions via soil surface | [kgNm-2]
 * sN\_no\_water | NO emissions via surface water table | [kgNm-2]
 * sN\_nh3\_ebul | NH3 emissions via ebullition | [kgNm-2]
 * sN\_nh3\_plant | NH3 emissions via plant mediated diffusion | [kgNm-2]
 * sN\_nh3\_soil | NH3 emissions via soil surface | [kgNm-2]
 * sN\_nh3\_water | NH3 emissions via surface water table | [kgNm-2]
 * sO\_plant\_o2\_cons | Oxygen consumption by roots | [kgOm-2]
 * sO\_plant\_o2\_prod | Oxygen release by roots | [kgOm-2]
 * sO\_algae\_o2\_prod | Oxygen release by algae | [kgOm-2]
 * ph\_watertable | Mean pH value in the surface water table | [-]
 * ph\_soil\_surface | Mean pH value in the soil  | [-]
 * anvf | Anaerobic volume fraction | [%]
 */
ldndc_string_t const  OutputSoilchemistryMeTrXSubdaily_Ids[] =
{
    "C_mic_1[kgCm-2]",
    "C_mic_2[kgCm-2]",
    "C_mic_3[kgCm-2]",
    "C_aorg[kgCm-2]",
    "N_nh4[kgNm-2]",
    "N_no3_ae[kgNm-2]",
    "N_no3_an[kgNm-2]",
    "C_doc_ae[kgCm-2]",
    "C_doc_an[kgCm-2]",
    "C_acetate_ae[kgCm-2]",
    "C_acetate_an[kgCm-2]",
    
    "sC_ch4_ebul[kgCm-2]",
    "sC_ch4_plant[kgCm-2]",
    "sC_ch4_soil[kgCm-2]",
    "sC_ch4_water[kgCm-2]",
    "sC_ch4_leach[kgCm-2]",
    "sC_ch4_prod[kgCm-2]",
    "sC_ch4_ox[kgCm-2]",
    
    "sC_acetate_prod[kgCm-2]",
    "sC_doc_prod[kgCm-2]",
    
    "sN_no2_nit[kgNm-2]",
    "sN_no3_denit[kgNm-2]",
    
    "sN_n2o_ebul[kgNm-2]",
    "sN_n2o_plant[kgNm-2]",
    "sN_n2o_soil[kgNm-2]",
    "sN_n2o_water[kgNm-2]",
    "sN_no_ebul[kgNm-2]",
    "sN_no_plant[kgNm-2]",
    "sN_no_soil[kgNm-2]",
    "sN_no_water[kgNm-2]",
    "sN_nh3_ebul[kgNm-2]",
    "sN_nh3_plant[kgNm-2]",
    "sN_nh3_soil[kgNm-2]",
    "sN_nh3_water[kgNm-2]",
    
    "sO_plant_o2_cons[kgOm-2]",
    "sO_plant_o2_prod[kgOm-2]",
    "sO_algae_o2_prod[kgOm-2]",
    
    "ph_watertable[-]",
    "ph_soil_surface[-]",
    "anvf[%]"
};


ldndc_string_t const *  OutputSoilchemistryMeTrXSubdaily_Header = OutputSoilchemistryMeTrXSubdaily_Ids;


#define  OutputSoilchemistryMeTrXSubdaily_Datasize  (sizeof( OutputSoilchemistryMeTrXSubdaily_Ids) / sizeof( OutputSoilchemistryMeTrXSubdaily_Ids[0]))
ldndc_output_size_t const  OutputSoilchemistryMeTrXSubdaily_Sizes[] =
{
    OutputSoilchemistryMeTrXSubdaily_Datasize,
    OutputSoilchemistryMeTrXSubdaily_Datasize /*total size*/
};


ldndc_output_size_t const *  OutputSoilchemistryMeTrXSubdaily_EntitySizes = NULL;

#define  OutputSoilchemistryMeTrXSubdaily_Rank  ((ldndc_output_rank_t)(sizeof( OutputSoilchemistryMeTrXSubdaily_Sizes) / sizeof( OutputSoilchemistryMeTrXSubdaily_Sizes[0])) - 1)
atomic_datatype_t const  OutputSoilchemistryMeTrXSubdaily_Types[] =
{
    LDNDC_FLOAT64
};



OutputSoilchemistryMeTrXSubdaily::OutputSoilchemistryMeTrXSubdaily(
        MoBiLE_State *  _state,cbm::io_kcomm_t *  _io_kcomm, timemode_e  _timemode)
        : MBE_LegacyOutputModel( _state, _io_kcomm, _timemode),
        m_iokcomm( _io_kcomm),
        accumulated_n_no2_no3_nitrification( 0.0),
        accumulated_n_no3_no2_denitrification( 0.0)
{ }



OutputSoilchemistryMeTrXSubdaily::~OutputSoilchemistryMeTrXSubdaily()
{ }



size_t
OutputSoilchemistryMeTrXSubdaily::record_size()
const
{
    return  OutputSoilchemistryMeTrXSubdaily_Datasize;
}



lerr_t
OutputSoilchemistryMeTrXSubdaily::configure(
                ldndc::config_file_t const *  _cf)
{
    if ( _cf && !_cf->have_output())
    {
        return  LDNDC_ERR_OK;
    }

    lerr_t  rc_setflags =
        this->set_metaflags( _cf, RM_DEFAULT_SUBDAILY);
    if ( rc_setflags)
        { return  LDNDC_ERR_FAIL; }


    m_sink = m_iokcomm->sink_handle_acquire( "metrxsubdaily");
    if ( m_sink.status() == LDNDC_ERR_OK)
    {
        lerr_t  rc_layout = LDNDC_OUTPUT_DEFINE_SINK_LAYOUT_DEFAULTS(
            this->m_sink,OutputSoilchemistryMeTrXSubdaily);
        RETURN_IF_NOT_OK(rc_layout);
    }
    else
    {
        this->set_active_off();
    }

    return  LDNDC_ERR_OK;
}



lerr_t
OutputSoilchemistryMeTrXSubdaily::initialize()
{
    return  LDNDC_ERR_OK;
}



lerr_t
OutputSoilchemistryMeTrXSubdaily::solve()
{
    KLOGDEBUG( "metrx record size=", record_size());
    KLOGFATAL( "do not use solve(), we rock with write_results()");
    return  LDNDC_ERR_FAIL;

}



lerr_t
OutputSoilchemistryMeTrXSubdaily::write_results(
        ldndc_flt64_t *  _data)
{
    if ( !_data || !m_sink.is_acquired())
    {
        /* we assume there is nothing to dump ... */
        return  LDNDC_ERR_OK;
    }

    /* output */
    void *  data[] = { _data};
    lerr_t  rc_write =
        this->write_fixed_record( &this->m_sink, data);
    if ( rc_write)
        { return  LDNDC_ERR_FAIL; }
    return  LDNDC_ERR_OK;
}



lerr_t
OutputSoilchemistryMeTrXSubdaily::finalize()
{
    return  m_iokcomm->sink_handle_release( &m_sink);
}

} /*namespace ldndc*/

#undef  OutputSoilchemistryMeTrXSubdaily_Rank
#undef  OutputSoilchemistryMeTrXSubdaily_Datasize

