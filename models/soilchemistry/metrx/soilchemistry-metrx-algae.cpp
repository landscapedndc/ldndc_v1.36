/*!
 * @file
 *      david kraus (created on: may 12, 2014)
 *
 */

#include  "soilchemistry/metrx/soilchemistry-metrx.h"
#include  <constants/lconstants-conv.h>

namespace ldndc {

/*!
 * @page metrx
 * @section algae Algae growth
 */
void
SoilChemistryMeTrX::MeTrX_algae_dynamics()
{
    /*!
     * @page metrx
     *  Algae growth is only considered if option "algae" is set true.
     */
    if ( !have_algae)
    {
        return;
    }

    /*!
     * @page metrx
     *  Algae growth only takes place if ponding water table exists and
     *  incoming shortwave radiation directly above soil (below canopy)
     *  is greater zero.
     */
    if ( have_water_table)
    {
        double const par( (mc_.parsun_fl[0] > 0.0) ?
                           mc_.parsun_fl[0] :
                           mc_.shortwaveradiation_in * cbm::FPAR);
        if ( cbm::flt_greater_zero( par))
        {
            /*!
             * @page metrx
             *  Algae growth rate depends on:
             *  - Nitrogen availability: \n
             *    \f[
             *    \phi_N = (1.0 - \Phi_{METRX\_F\_N\_ALGAE}) +
             *    \Phi_{METRX\_F\_N\_ALGAE} \cdot \frac{N}{\Phi_{METRX\_KMM\_N\_ALGAE} + N }
             *    \f]
             *  - Temperature: \n
             *    \f[
             *    \phi_T = \left\{ \begin{array}{cccc}
             *    0.0 & T \le 15.0 \\
             *    \frac{T - 15.0}{15.0} & 15.0 < T \le 30.0 \\
             *    1.0 - \frac{T - 30.0}{15.0} & 30.0 < T \le 45.0 \\
             *    0.0 & 45.0 > T
             *    \end{array} \right. \label{eq2}
             *    \f]
             *  - Photosynthetic radiation: \n
             *    \f[
             *    \phi_P = 1.0 - e^{\frac{-P}{100.0}}
             *    \f]
             */
            double const n_tot( sb_.nh4_sbl.sum() + sb_.nh3_sbl.sum() + sb_.no3_sbl.sum());
            double const fact_n( (1.0 - sipar_.METRX_F_N_ALGAE()) + sipar_.METRX_F_N_ALGAE() * (n_tot / (sipar_.METRX_KMM_N_ALGAE() * h_wl + n_tot)));
            double const fact_t( (*mc_temp_atm > 30.0) ?
                                 cbm::bound(0.0, 1.0 - (*mc_temp_atm - 30.0) / 15.0, 1.0) :
                                 cbm::bound(0.0, (*mc_temp_atm - 15.0) / 15.0, 1.0));
            double const fact_par( cbm::bound(0.0, 1.0 - std::exp(-par / 200.0), 1.0));

            /*!
             * @page metrx
             *  Algae growth rate is given by: \n
             *  \f[
             *  \mu_g = \Phi_{METRX\_MUEMAX\_C\_ALGAE} \cdot min(\phi_N, \phi_T, \phi_P)
             *  \f]
             */
            double const c_growth( sipar_.METRX_MUEMAX_C_ALGAE() * 200.0 * std::min( fact_par, std::min(fact_n, fact_t)));

            /*!
             * @page metrx
             *  Algae growth changes pH value of the ponding water table: \n
             *  \f[
             *  \Delta pH = 3.0 \cdot \phi_N \cdot \phi_T \cdot \phi_P
             *  \f]
             */
            ph_delta_pab_wl = 3.0 * cbm::bound_max( c_growth / (2.0 * cbm::HA_IN_M2), 1.0);

            size_t help( sb_.surfacebulk_layer_cnt() * (sb_.surfacebulk_layer_cnt()+1) * 0.5);
            for (size_t sbl = 0; sbl < sb_.surfacebulk_layer_cnt(); ++sbl)
            {
                double const c_growth_sbl( c_growth * (sbl+1) / help);
                double const n_growth_sbl( c_growth_sbl / sipar_.METRX_CN_ALGAE());
                double const o2_prod_sbl( c_growth_sbl / MC_MO2_RATIO);

                c_algae += c_growth_sbl;
                sb_.o2_sbl[sbl] += o2_prod_sbl;
                day_c_fix_algae += c_growth_sbl;

                double const n_avail( sb_.nh3_sbl[sbl] + sb_.nh4_sbl[sbl] + sb_.no3_sbl[sbl]);
                double const n_up( cbm::bound_max( fact_n * n_growth_sbl, n_avail));
                double const n_fix( n_growth_sbl - n_up);

                n_algae += n_fix;
                day_n_fix_algae += n_fix;
                accumulated_n_to_living_plant_and_algae_from_extern_sl[0] += n_fix;

                if ( cbm::flt_greater_zero( n_avail) &&
                     cbm::flt_greater( n_avail, n_up))
                {
                    double const n_up_nh3( sb_.nh3_sbl[sbl] / n_avail * n_up);
                    if ( cbm::flt_greater_zero( n_up_nh3))
                    {
                        sb_.nh3_sbl[sbl] -= n_up_nh3;
                        n_algae += n_up_nh3;
                        accumulated_n_algae_nh3_uptake_sbl[0] += n_up_nh3;
                    }

                    double const n_up_nh4( sb_.nh4_sbl[sbl] / n_avail * n_up);
                    if ( cbm::flt_greater_zero( n_up_nh4))
                    {
                        sb_.nh4_sbl[sbl] -= n_up_nh4;
                        n_algae += n_up_nh4;
                        accumulated_n_algae_nh4_uptake_sbl[0] += n_up_nh4;
                    }

                    double const n_up_no3( sb_.no3_sbl[sbl] / n_avail * n_up);
                    if ( cbm::flt_greater_zero( n_up_no3))
                    {
                        sb_.no3_sbl[sbl] -= n_up_no3;
                        n_algae += n_up_no3;
                        accumulated_n_algae_no3_uptake_sbl[0] += n_up_no3;
                    }
                }
                else
                {
                    n_algae += n_up;
                    day_n_fix_algae += n_up;
                }
            }
        }
    }


    /*!
     * @page metrx
     *  Algae turnover: \n
     *  \f[
     *  \mu_d = m_A \cdot \Phi_{METRX\_AMAX\_ALGAE}
     *  \f]
     */
    if ( cbm::flt_greater_zero( c_algae))
    {
        double const c_algae_decay( A_MAX_ALGAE * c_algae);
        c_algae -= c_algae_decay;
        c_dead_algae += c_algae_decay;
    }
    else
    {
        c_algae = 0.0;
    }

    if ( cbm::flt_greater_zero( n_algae))
    {
        double const n_algae_decay( A_MAX_ALGAE * n_algae);
        n_algae -= n_algae_decay;
        n_dead_algae += n_algae_decay;
    }
    else
    {
        n_algae = 0.0;
    }
}

} /*namespace ldndc*/
