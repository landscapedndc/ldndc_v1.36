/*!
 * @file
 * @author
 *  David Kraus
 * @date
 *  May 12, 2014
 */

#include  "soilchemistry/metrx/soilchemistry-metrx.h"

#include  <constants/lconstants-chem.h>
#include  <scientific/meteo/ld_meteo.h>

namespace ldndc {


/*!
 * @brief
 * Denitrification of NO3 to N2 via NO2, NO and N2O
 */
void
SoilChemistryMeTrX::MeTrX_denitrification()
{
    for( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
    {
        /*!
         * @page metrx
         * @section metrx_denitrification Denitrification
         * @subsection metrx_microbial_denitrification Microbial denitrification
         * Denitrification is calculated as four-step process including the nitrogen species
         * \f$ NO_3^-, NO_2^-, NO \f$ and \f$ N_2O \f$:
         *
         * \f[
         * NO_3^- \rightarrow NO_2^- \rightarrow NO \rightarrow N_2O \rightarrow N_2
         * \f]
         *
         */
        double const wfpf_eff( MeTrX_get_wfps_eff( sl, 1.0));
        double const afpf_eff( 1.0 - wfpf_eff);

        double const n_denit_part( wfpf_eff * (an_no_liq_sl[sl] + an_n2o_liq_sl[sl] + sc_.an_no2_sl[sl]) +
                                   afpf_eff * (an_no_gas_sl[sl] + an_n2o_gas_sl[sl]));
        double const n_denit_tot( wfpf_eff * no3_an_sl[sl] + n_denit_part);
        double const c_tot( sc_.an_doc_sl[sl] + an_acetate_sl[sl]);

        /*!
         * @page metrx
         * @subsubsection metrx_microbial_denitrification_growth Denitrifier growth
         * All denitrification steps are calculated based on actively denitrifying microbial biomass.
         * The associated activity coefficient of denitrifying microbes \f$ a_{mic} \f$ is given by a harmonic mean
         * of a soil temperature (\f$ \phi_{T}\f$) and a soil water (\f$ \phi_{wfps}\f$) depending response coefficient.
         *
         * \f[
         * a_{mic} = \frac{2}{\frac{1}{\phi_{T}} + \frac{1}{\phi_{wfps}}}
         * \f]
         * with \f$ \phi_{T}\f$ given by:
         * \f[
         * \phi_{T} = e^{-METRX\_F\_MIC\_T\_EXP\_1 \cdot \left( \frac{1 - T}{METRX\_F\_MIC\_T\_EXP\_2} \right)^2}
         * \f]
         *
         * @image html metrx_denitrification_on_temperature.png "Response function depending on temperature" width=900
         * @image latex metrx_denitrification_on_temperature.png "Response function depending on temperature"
         *
         * and with \f$ \phi_{wfps}\f$ given by:
         *
         *  \f[
         *  \phi_{wfps} = 1 - \frac{1}{1 + e^{(wfps - METRX\_F\_MIC\_M\_WEIBULL\_1) \cdot METRX\_F\_MIC\_M\_WEIBULL\_2}}
         *  \f]
         *
         * @image html metrx_denitrification_on_soilwater.png "Response function depending on soilwater" width=900
         * @image latex metrx_denitrification_on_soilwater.png "Response function depending on soilwater"
         */
        double const activity_an( MeTrX_get_fact_tm_mic( sl) * sc_.anvf_sl[sl]);
        double const active_microbes( sc_.C_micro2_sl[sl] * activity_an);

        /*!
         * @page metrx
         * Microbial growth depends on carbon and nitrogen availability:
         *
         * \f{eqnarray*}{
         * \phi_{C} &=& \frac{DOC}{DOC + METRX\_KMM\_C\_DENIT} \\
         * \phi_{N} &=& \frac{N_{total}}{N_{total} + METRX\_KMM\_N\_DENIT} \\
         * \phi_{C,N} &=& \frac{2}{\frac{1}{\phi_{C}} + \frac{1}{\phi_{N}}}
         * \f}
         *
         */
        double const k_mm_c( MeTrX_get_k_mm_x_sl(sl, sc_.anvf_sl[sl] * sipar_.METRX_KMM_C_DENIT()));
        double const k_mm_n( MeTrX_get_k_mm_x_sl(sl, sc_.anvf_sl[sl] * sipar_.METRX_KMM_N_DENIT()));

        // Bacteria and fungi both produce N2O via heterotrophic denitrification.
        // Fungi, however, lack the nosZ gene and cannot denitrify N2O to N2.
        // Think of Mic-CN dependency of N2O reduction since fungi have on average a higher CN-ratio
        double const cn_ratio( sc_.C_micro2_sl[sl] / n_micro_2_sl[sl]);

        double const fact_c( MeTrX_get_fact_mm( c_tot, k_mm_c));
        day_denit_factor_c_sl[sl] += fact_c;

        double const fact_n_denit_part( MeTrX_get_fact_mm( n_denit_part, k_mm_n));
        double const fact_n_denit_tot( MeTrX_get_fact_mm( n_denit_tot, k_mm_n));
        day_denit_factor_n_sl[sl] += fact_n_denit_tot;

        /******************************************/
        /* Denitrification and microbial C growth */
        /******************************************/

        if ( cbm::flt_greater_zero( fact_n_denit_tot) &&
             cbm::flt_greater_zero( fact_c))
        {
            /*!
             * @page metrx
             * Microbial enzymes relevant for denitrification are:
             * - narG and napA: (NO3 -> NO2)
             * - nirK and nirS: (NO2 -> NO)
             * - cnorB and qnorB: (NO -> N2O)
             * - nosZ (N2O -> N2)
             *
             * Heterotrophic denitrification decreases with increasing pH (Kool et al. 2010).
             * Low pH inhibits all enzymes but especially nosZ
             * The general pH influence on microbial denitrifier growth influencing all enzymes is given by:
             *
             * \f[
             * \phi_{pH} = 1 - \frac{1}{1 + e^{\frac{ph - METRX\_F\_DENIT\_PH\_1}{METRX\_F\_DENIT\_PH\_2}}}
             * \f]
             *
             * @image html metrx_denitrification_on_ph.png "Response function of denitrification depending on pH" width=900
             * @image latex metrx_denitrification_on_ph.png "Response function of denitrification depending on pH"
             *
             */
            double const fact_ph_denit_all( 1.0 - 1.0 / (1.0 + std::exp(( sc_.ph_sl[sl] - sipar_.METRX_F_DENIT_PH_1()) / sipar_.METRX_F_DENIT_PH_2())));

            /*!
             * @page metrx
             * The pH factor specifically for nosZ influencing denitrification of N2O is given by:
             *
             * \f[
             * \phi_{pH,nosZ} = 1 - \frac{1}{1 + e^{\frac{ph - METRX\_F\_DENIT\_N2O\_PH\_1}{METRX\_F\_DENIT\_N2O\_PH\_2}}}
             * \f]
             *
             * @image html metrx_denitrification_of_n2o_on_ph.png "Response function of denitrification of N2O depending on pH" width=900
             * @image latex metrx_denitrification_of_n2o_on_ph.png "Response function of denitrification of N2O depending on pH"
             */
            double const fact_ph_denit_n2o( 1.0 - 1.0 / (1.0 + std::exp(( sc_.ph_sl[sl] - sipar_.METRX_F_DENIT_N2O_PH_1()) / sipar_.METRX_F_DENIT_N2O_PH_2())));

            /*!
             * @page metrx
             * Total microbial carbon demand includes assimilation and dissimilation:
             *
             * \f{eqnarray*}{
             * C_{demand} &=& c_{mic} \mu_{mic} a_{mic} \frac{1.0}{Y_{denit}} \phi_{C,N} \phi_{pH} \\
             * C_{demand, assi} &=& C_{demand} Y_{denit} \\
             * C_{demand, diss} &=& C_{demand} (1 - Y_{denit})
             * \f}
             *
             * The denitrification efficiency is constant:
             *
             * \f[
             * Y_{denit} = METRX\_MIC\_EFF\_ANAEROBIC\_RESPIRATION
             * \f]
             *
             */
            double const c_use_tot( cbm::bound_max( active_microbes * MUE_MAX_C_MICRO_2 /
                                                    sipar_.METRX_MIC_EFF_ANAEROBIC_RESPIRATION() *
                                                    cbm::harmonic_mean2(fact_n_denit_tot, fact_c) * fact_ph_denit_all,
                                                    0.99 * c_tot));

            //carbon partly assimilated to biomass and partly dissimilated to CO2
            double c_demand_assi( sipar_.METRX_MIC_EFF_ANAEROBIC_RESPIRATION() * c_use_tot);
            double c_demand_diss( c_use_tot - c_demand_assi);

            sc_.C_micro2_sl[sl] += c_demand_assi;
            day_assi_c_mic_2 += c_demand_assi;

            co2_gas_sl[sl] += c_demand_diss;
            co2_hetero_sl[sl] += c_demand_diss;
            day_co2_prod_mic_2 += c_demand_diss;

            //relative use of DOC and acetate
            double const doc_demand( cbm::bound_max( sc_.an_doc_sl[sl] / c_tot * c_use_tot,
                                                     sc_.an_doc_sl[sl]));
            double const acetate_demand( cbm::bound_max( c_use_tot - doc_demand,
                                                         an_acetate_sl[sl]));
            sc_.an_doc_sl[sl] -= doc_demand;
            an_acetate_sl[sl] -= acetate_demand;

            /*!
             * @page metrx
             * @subsubsection metrx_microbial_denitrification_n_use Denitrifier nitrogen use
             * Denitrification of \f$ N_x \f$ depends on
             * their relative abundance using associated scaling factors:
             * \f{eqnarray*}{
             * \psi_{NO_3^-} &=& \frac{NO_3^-}{NO_3^- + NO_2^- + NO + N_2O} \\
             * \psi_{NO_2^-} &=& (1- \psi_{NO_3^-}) \cdot \frac{NO_2^-}{NO_2^- + NO + N_2O} \\
             * \psi_{NO}     &=& (1- \psi_{NO_3^-}) \cdot \frac{NO}{NO_2^- + NO + N_2O} \\
             * \psi_{N_2O}   &=& (1- \psi_{NO_3^-}) \cdot \frac{N_2O}{NO_2^- + NO + N_2O} \\
             * \f}
             */
            double const frac_no3( wfpf_eff * no3_an_sl[sl] / n_denit_tot * (1.0 - fact_n_denit_part));
            double frac_no2( 0.0);
            double frac_no_liq( 0.0);
            double frac_no_gas( 0.0);
            double frac_n2o_liq( 0.0);
            double frac_n2o_gas( 0.0);
            if ( cbm::flt_greater_zero( n_denit_part))
            {
                double const frac_no3_inv( 1.0 - frac_no3);
                frac_no2 = frac_no3_inv * wfpf_eff * sc_.an_no2_sl[sl] / n_denit_part;
                frac_no_liq = frac_no3_inv * wfpf_eff * an_no_liq_sl[sl] / n_denit_part;
                frac_no_gas = frac_no3_inv * afpf_eff * an_no_gas_sl[sl] / n_denit_part;
                frac_n2o_liq = frac_no3_inv * wfpf_eff * an_n2o_liq_sl[sl] / n_denit_part;
                frac_n2o_gas = frac_no3_inv * afpf_eff * an_n2o_gas_sl[sl] / n_denit_part;
            }

            /*!
             * @page metrx
             * \f$ N_x \f$ can be denitrified a sinlge step or multiple steps before it is released from the denitrifying microbial organism
             * to the soil environment. The fraction of \f$ N_x \f$, which is completely denitrified to \f$ N_2 \f$ within the same organism
             * depends on the soil anaerobic volume (assuming denitrification enzymes are more developped under anaerobic conditions):
             *
             * \f[
             * \phi_{anvf,1} = METRX\_F\_DENIT\_N2\_1 \cdot METRX\_F\_DENIT\_N2\_2 + V_{an} \cdot METRX\_F\_DENIT\_N2\_2
             * \f]
             *
             * @image html metrx_denitrification_to_N2_on_anvf.png "Response function of denitrification depending on pH" width=900
             * @image latex metrx_denitrification_to_N2_on_anvf.png "Response function of denitrification depending on pH"
             *
             */
            double const fact_anvf_n2( METRX_F_DENIT_N2_MIN + sc_.anvf_sl[sl] * METRX_F_DENIT_N2_DELTA);
            double const fact_anvf_no( pow( 1.0 - sc_.anvf_sl[sl], sipar_.METRX_F_DENIT_NO()));

            /*!
             * @page metrx
             * Stoichiometry between dissimilative nitrogen and carbon demand is based on succinate (\f$ CH_{1.8}O_{0.5}N_{0.2} \f$) (Kampschreur et al. 2012):
             *
             * - \f$ C_4H_4O_4 + 3.23 NO_3 + 1.64 H + 0.36 NH_4 \rightarrow 1.8 CH_{1.8}O_{0.5}N_{0.2} + 3.23 NO_2 + 2.2 CO_2 + 1.92 H_2O \f$
             * - \f$ C_4H_4O_4 + 6.45 NO_2 + 8.09 H + 0.36 NH_4 \rightarrow 1.8 CH_{1.8}O_{0.5}N_{0.2} + 6.45 NO   + 2.2 CO_2 + 5.15 H_2O \f$
             * - \f$ C_4H_4O_4 + 6.45 NO   + 1.64 H + 0.36 NH_4 \rightarrow 1.8 CH_{1.8}O_{0.5}N_{0.2} + 3.23 NO   + 2.2 CO_2 + 1.92 H_2O \f$
             *
             */
            double const CN_NO3_NO2( 6.45 / 8.0 * cbm::MN / cbm::MC);   // 8C for 6.45N
            double const CN_NO3_NO( 6.45 / 12.0 * cbm::MN / cbm::MC);   // (8+4)C for 6.45N
            double const CN_NO3_N2O( 6.45 / 16.0 * cbm::MN / cbm::MC);  // (8+4+4)C for 6.45N
            double const CN_NO3_N2( 6.45 / 20.0 * cbm::MN / cbm::MC);   // (8+4+4+4)C for 6.45N

            double const CN_NO2_NO( 6.45 / 4.0 * cbm::MN / cbm::MC);
            double const CN_NO2_N2O( 6.45 / 8.0 * cbm::MN / cbm::MC);
            double const CN_NO2_N2( 6.45 / 16.0 * cbm::MN / cbm::MC);

            double const CN_NO_N2O( 6.45 / 4.0 * cbm::MN / cbm::MC);
            double const CN_NO_N2( 6.45 / 8.0 * cbm::MN / cbm::MC);

            double const CN_N2O_N2( 6.45 / 4.0 * cbm::MN / cbm::MC);

            /*!
             * @page metrx
             * @paragraph metrx_microbial_denitrification_no3 Denitrification of NO3
             *
             * Currently, it is assumed that after denitrification of \f$ NO_3^- \f$,
             * \f$ NO_2^- \f$ is always released to the environment:
             *
             * \f{eqnarray*}{
             * \frac{d NO_3^-}{dt} &=& \psi_{NO_3^-} \cdot C_{demand} \cdot \Xi_{CN, NO_3^-} \\
             * \Xi_{CN, NO_3^-}    &=& \xi_{CN, NO_3^- \rightarrow NO_2^-} \\
             * \frac{d NO_3^-}{dt} &=& \frac{d NO_{3, \rightarrow NO_2^-}^-}{dt}
             * \f}
             *
             */
            double c_denit_no3( frac_no3 * c_use_tot);
            double const c_denit_no3_n2( 0.0);// fact_anvf_n2 * denit_no3);
            double const c_denit_no3_no2( c_denit_no3);// fact_anvf_no * (denit_no3 - denit_no3_n2));
            double const c_denit_no3_no( 0.0);// fact_anvf_no * (denit_no3 - denit_no3_no2 - denit_no3_n2));
            double const c_denit_no3_n2o( 0.0);

            double n_denit_no3_n2( c_denit_no3_n2 * CN_NO3_N2);
            double n_denit_no3_no2( c_denit_no3_no2 * CN_NO3_NO2);
            double n_denit_no3_no( c_denit_no3_no * CN_NO3_NO);
            double n_denit_no3_n2o( c_denit_no3_n2o * CN_NO3_N2O);
            double n_denit_no3( n_denit_no3_n2 + n_denit_no3_no2 + n_denit_no3_no + n_denit_no3_n2o);
            if ( cbm::flt_greater( n_denit_no3, 0.99 * no3_an_sl[sl]))
            {
                double const scale( 0.99 * no3_an_sl[sl] / n_denit_no3);
                n_denit_no3_n2 *= scale;
                n_denit_no3_no2 *= scale;
                n_denit_no3_no *= scale;
                n_denit_no3_n2o *= scale;
                n_denit_no3 *= scale;
            }

            /*!
             * @page metrx
             * @paragraph metrx_microbial_denitrification_no2 Denitrification of NO2
             *
             * Denitrified \f$ NO_2^- \f$ is partly transferred to \f$ NO, N_2O \f$ and \f$ N_2 \f$ depending on the anaerobicity of the soil.
             * The associated carbon demand is given by:
             *
             * \f{eqnarray*}{
             * \frac{d C_{NO_2^-}}{dt} &=& \psi_{NO_2^-} \frac{C_{denit}}{dt} \\
             * \frac{d C_{NO_2^- \rightarrow N_2}}{dt}  &=& \frac{d C_{NO_2^-}}{dt} \cdot \phi_{anvf,1} \cdot \phi_{pH, nosZ} \\
             * \frac{d C_{NO_2^- \rightarrow NO}}{dt}   &=& \frac{d C_{NO_2^-}}{dt} \cdot (1-\phi_{anvf,1}) \cdot \phi_{anvf,2} \\
             * \frac{d C_{NO_2^- \rightarrow N_2O}}{dt} &=& \frac{d C_{NO_2^-}}{dt} - \frac{d C_{NO_2^- \rightarrow N_2}}{dt} - \frac{d C_{NO_2^- \rightarrow NO}}{dt}
             * \f}
             *
             * The stoichiometry between carbon and nitrogen is given by:
             *
             * \f{eqnarray*}{
             * \frac{d NO_{2, NO_2^- \rightarrow N_2}^-}{dt}  &=& \xi_{CN, NO_2^- \rightarrow N_2} \frac{d C_{NO_2^- \rightarrow N_2}}{dt} \\
             * \frac{d NO_{2, NO_2^- \rightarrow NO}^-}{dt}   &=& \xi_{CN, NO_2^- \rightarrow NO} \frac{d C_{NO_2^- \rightarrow NO}}{dt} \\
             * \frac{d NO_{2, NO_2^- \rightarrow N_2O}^-}{dt} &=& \xi_{CN, NO_2^- \rightarrow N_2O} \frac{d C_{NO_2^- \rightarrow N_2O}}{dt}
             * \f}
             */
            double const c_denit_no2( frac_no2 * c_use_tot);
            double const c_denit_no2_no( fact_anvf_no * cbm::bound_min(0.0, 1.0 - fact_anvf_n2) * c_denit_no2);
            double const c_denit_no2_n2( fact_anvf_n2 * fact_ph_denit_n2o * c_denit_no2);
            double const c_denit_no2_n2o( c_denit_no2 - c_denit_no2_n2 - c_denit_no2_no);

            double n_denit_no2_n2( c_denit_no2_n2 * CN_NO2_N2);
            double n_denit_no2_no( c_denit_no2_no * CN_NO2_NO);
            double n_denit_no2_n2o( c_denit_no2_n2o * CN_NO2_N2O);
            double n_denit_no2( n_denit_no2_n2 + n_denit_no2_no + n_denit_no2_n2o);
            if ( cbm::flt_greater( n_denit_no2, 0.99 * sc_.an_no2_sl[sl]))
            {
                double const scale( 0.99 * sc_.an_no2_sl[sl] / n_denit_no2);
                n_denit_no2_n2 *= scale;
                n_denit_no2_no *= scale;
                n_denit_no2_n2o *= scale;
                n_denit_no2 *= scale;
            }

            /*!
             * @page metrx
             * @paragraph metrx_microbial_denitrification_no Denitrification of NO
             *
             * Denitrified \f$ NO \f$ is partly transferred to \f$ N_2O \f$ and \f$ N_2 \f$ depending on the anaerobicity of the soil.
             * The associated carbon demand is given by:
             *
             * \f{eqnarray*}{
             * \frac{d C_{NO}}{dt} &=& \psi_{NO} \frac{C_{denit}}{dt} \\
             * \frac{d C_{NO \rightarrow N_2}}{dt}  &=& \frac{d C_{NO}}{dt} \cdot \phi_{anvf,1} \cdot \phi_{pH, nosZ} \\
             * \frac{d C_{NO \rightarrow N_2O}}{dt} &=& \frac{d C_{NO}}{dt} - \frac{d C_{NO_2^- \rightarrow N_2}}{dt}
             * \f}
             *
             * The stoichiometry between carbon and nitrogen is given by:
             *
             * \f{eqnarray*}{
             * \frac{d NO_{NO \rightarrow N_2}}{dt}  &=& \xi_{CN, NO \rightarrow N_2} \frac{d C_{NO \rightarrow N_2}}{dt} \\
             * \frac{d NO_{NO \rightarrow N_2O}}{dt} &=& \xi_{CN, NO \rightarrow N_2O} \frac{d C_{NO \rightarrow N_2O}}{dt}
             * \f}
             */
            double const c_denit_no_liq( frac_no_liq * c_use_tot);
            double const c_denit_no_liq_n2( fact_anvf_n2 * fact_ph_denit_n2o * c_denit_no_liq);
            double const c_denit_no_liq_n2o( c_denit_no_liq - c_denit_no_liq_n2);

            double n_denit_no_liq_n2( c_denit_no_liq_n2 * CN_NO_N2);
            double n_denit_no_liq_n2o( c_denit_no_liq_n2o * CN_NO_N2O);
            double n_denit_no_liq( n_denit_no_liq_n2 + n_denit_no_liq_n2o);
            if ( cbm::flt_greater( n_denit_no_liq, 0.99 * an_no_liq_sl[sl]))
            {
                double const scale( 0.99 * an_no_liq_sl[sl] / n_denit_no_liq);
                n_denit_no_liq_n2 *= scale;
                n_denit_no_liq_n2o *= scale;
                n_denit_no_liq *= scale;
            }

            double const c_denit_no_gas( frac_no_gas * c_use_tot);
            double const c_denit_no_gas_n2( fact_anvf_n2 * fact_ph_denit_n2o * c_denit_no_gas);
            double const c_denit_no_gas_n2o( c_denit_no_gas - c_denit_no_gas_n2);

            double n_denit_no_gas_n2( c_denit_no_gas_n2 * CN_NO_N2);
            double n_denit_no_gas_n2o( c_denit_no_gas_n2o * CN_NO_N2O);
            double n_denit_no_gas( n_denit_no_gas_n2 + n_denit_no_gas_n2o);
            if ( cbm::flt_greater( n_denit_no_gas, 0.99 * an_no_gas_sl[sl]))
            {
                double const scale( 0.99 * an_no_gas_sl[sl] / n_denit_no_gas);
                n_denit_no_gas_n2 *= scale;
                n_denit_no_gas_n2o *= scale;
                n_denit_no_gas *= scale;
            }

            /*!
             * @page metrx
             * @paragraph metrx_microbial_denitrification_n2o Denitrification of N2O
             *
             * The associated carbon demand is given by:
             *
             * \f{eqnarray*}{
             * \frac{d C_{N_2O}}{dt} &=& \psi_{N_2O} \frac{C_{denit}}{dt} \\
             * \f}
             *
             * The stoichiometry between carbon and nitrogen is given by:
             *
             * \f{eqnarray*}{
             * \frac{d N_2O}{dt}  &=& \xi_{CN, N_2O} \frac{d C_{N_2O}}{dt} \\
             * \f}
             */
            double const c_denit_n2o_liq( frac_n2o_liq * c_use_tot);
            double n_denit_n2o_liq( c_denit_n2o_liq * CN_N2O_N2);
            if ( cbm::flt_greater( n_denit_n2o_liq, 0.99 * an_n2o_liq_sl[sl]))
            {
                double const scale( 0.99 * an_n2o_liq_sl[sl] / n_denit_n2o_liq);
                n_denit_n2o_liq *= scale;
            }

            double const c_denit_n2o_gas( frac_n2o_gas * c_use_tot);
            double n_denit_n2o_gas( c_denit_n2o_gas * CN_N2O_N2);
            if ( cbm::flt_greater( n_denit_n2o_gas, 0.99 * an_n2o_gas_sl[sl]))
            {
                double const scale( 0.99 * an_n2o_gas_sl[sl] / n_denit_n2o_gas);
                n_denit_n2o_gas *= scale;
            }

            //dot_metrx_nitrogen no3_sl -> no2_sl [label="denit."];
            //dot_metrx_nitrogen no2_sl -> no_sl [label="denit."];
            //dot_metrx_nitrogen no_sl -> n2o_sl [label="denit."];
            //dot_metrx_nitrogen n2o_sl -> n2_sl [label="denit."];
            no3_an_sl[sl] -= n_denit_no3;
            sc_.an_no2_sl[sl] -= (n_denit_no2 - n_denit_no3_no2);
            an_no_liq_sl[sl]  -= n_denit_no_liq;
            an_no_gas_sl[sl]  -= (n_denit_no_gas - n_denit_no3_no - n_denit_no2_no);
            an_n2o_liq_sl[sl] -= (n_denit_n2o_liq - n_denit_no_liq_n2o);
            an_n2o_gas_sl[sl] -= (n_denit_n2o_gas - n_denit_no3_n2o - n_denit_no2_n2o - n_denit_no_gas_n2o);
            n2_gas_sl[sl] += n_denit_no2_n2 + n_denit_no_gas_n2 + n_denit_no_liq_n2 + n_denit_n2o_gas + n_denit_n2o_liq;

            day_denit_no3_no2 += n_denit_no3_no2;
            day_denit_no2_no += n_denit_no2_no;
            day_denit_no2_n2o += n_denit_no2_n2o;
            day_denit_no2_n2 += n_denit_no2_n2;
            day_denit_no_n2o += (n_denit_no_gas_n2o + n_denit_no_liq_n2o);
            day_denit_no_n2 += (n_denit_no_gas_n2 + n_denit_no_liq_n2);
            day_denit_n2o_n2 += (n_denit_n2o_gas + n_denit_n2o_liq);

            accumulated_n_no3_no2_denitrification_sl[sl] += n_denit_no3_no2;
            accumulated_n_no3_no_denitrification_sl[sl] += n_denit_no3_no;
            accumulated_n_no3_n2o_denitrification_sl[sl] += n_denit_no3_n2o;
            accumulated_n_no3_n2_denitrification_sl[sl] += n_denit_no3_n2;

            accumulated_n_no2_no_denitrification_sl[sl] += n_denit_no2_no;
            accumulated_n_no2_n2o_denitrification_sl[sl] += n_denit_no2_n2o;
            accumulated_n_no2_n2_denitrification_sl[sl] += n_denit_no2_n2;

            accumulated_n_no_n2o_denitrification_sl[sl] += n_denit_no_gas_n2o + n_denit_no_liq_n2o;
            accumulated_n_no_n2_denitrification_sl[sl] += n_denit_no_gas_n2 + n_denit_no_liq_n2;

            accumulated_n_n2o_n2_denitrification_sl[sl] += n_denit_n2o_gas + n_denit_n2o_liq;
        }

        /******************/
        /* N assimilation */
        /********cd **********/

        double const n_assi_tot( nh4_an_sl[sl] + no3_an_sl[sl] + don_an_sl[sl]);
        if ( cbm::flt_greater_zero( n_assi_tot))
        {
            double const fact_n_assi( MeTrX_get_fact_mm( n_assi_tot, MeTrX_get_k_mm_x_sl( sl, sc_.anvf_sl[sl] * sipar_.METRX_KMM_N_MIC())));
            double const pot_n_growth( MeTrX_get_n_growth( sc_.C_micro2_sl[sl],
                                                           n_micro_2_sl[sl],
                                                           activity_an,
                                                           fact_n_assi,
                                                           cn_opt_mic_sl[sl]));

            MeTrX_execute_n_assimilation( pot_n_growth,
                                          sl,
                                          false,
                                          n_micro_2_sl[sl],
                                          day_min_n_mic_2,
                                          day_assi_n_mic_2);
        }

        /*******************/
        /* Microbial decay */
        /*******************/

        double const micro_c_decay_max( MeTrX_get_micro_c_decay_max( sc_.C_micro2_sl[sl]));
        if ( cbm::flt_greater_zero( micro_c_decay_max))
        {
            double const ka( sipar_.METRX_KA_C_MIC() / (sc_.h_sl[sl] * sc_.anvf_sl[sl]));
            double const decay_scale( active_microbes * A_MAX_MICRO_2);
            double const micro_death_c( cbm::bound_max( decay_scale * 1.0 / (1.0 + c_tot * ka),
                                                        micro_c_decay_max));
            double const micro_death_n( micro_death_c / cn_ratio);
            MeTrX_dead_microbial_biomass_allocation( sl,
                                                     micro_death_c,
                                                     micro_death_n,
                                                     sc_.C_micro2_sl[sl],
                                                     n_micro_2_sl[sl]);
        }
    }

    /*!
     * @page metrx
     * @subsection metrx_chemodenitrification Chemodenitrification
     *
     */
    // At low pH (<4.5) N2O can be produced via chemical decomposition of hydroxylamine (NH2OH),
    // nitroxyl hydride (HNO) or NO2−, which are intermediate products of ammonia oxidation (Zumft 1997; Wrageet al. 2001).
    for( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
    {
        double const no2_total( sc_.no2_sl[sl] + sc_.an_no2_sl[sl]);
        if ( !cbm::flt_greater_zero( no2_total))
        {
            continue;
        }

        //add water dependency (Venterea et al., 2005)
        /*!
         * @page metrx
         *  Temperature dependency of chemodenitrification:
         *  @image html metrx_chemodenitrification_on_temperature.png "Response function depending on temperature" width=300
         *  @image latex metrx_chemodenitrification_on_temperature.png "Response function depending on temperature"
         */
        double const fact_temp( sipar_.METRX_F_CHEMODENIT_T_1() *
                                std::exp(-sipar_.METRX_F_CHEMODENIT_T_2() / (cbm::RGAS * (mc_temp_sl[sl] + cbm::D_IN_K))));

        /*!
         * @page metrx
         *  pH dependency of chemodenitrification:
         *  @image html metrx_chemodenitrification_on_ph.png "Response function depending on pH" width=300
         *  @image latex metrx_chemodenitrification_on_ph.png "Response function depending on pH"
         */
        double const fact_ph( 1.0 - 1.0 / (1.0 + std::exp(-sipar_.METRX_F_CHEMODENIT_PH_1() * (sc_.ph_sl[sl] - sipar_.METRX_F_CHEMODENIT_PH_2()))));

        double const k_chemodenit_scale( KR_DENIT_CHEMO * fact_temp * fact_ph);
        double const chemodenit_no_ae( k_chemodenit_scale * sc_.no2_sl[sl]);
        double const chemodenit_no_an( k_chemodenit_scale * sc_.an_no2_sl[sl]);
        double const chemodenit_no_tot( chemodenit_no_ae + chemodenit_no_an);

        //dot_metrx_nitrogen no2_sl -> no_sl [label="chemodenit."];
        sc_.no2_sl[sl] -= chemodenit_no_ae;
        sc_.an_no2_sl[sl] -= chemodenit_no_an;

        no_liq_sl[sl] += chemodenit_no_ae;
        an_no_liq_sl[sl] += chemodenit_no_an;

        // output
        sc_.accumulated_n_chemodenitrify_sl[sl] += chemodenit_no_tot;
        accumulated_n_no2_chemodenitrification_sl[sl] += chemodenit_no_tot;
        day_chemodenit_no2_no += chemodenit_no_tot;
    }
}



void
SoilChemistryMeTrX::MeTrX_chemodenitrification()
{

}

} /*namespace ldndc*/

