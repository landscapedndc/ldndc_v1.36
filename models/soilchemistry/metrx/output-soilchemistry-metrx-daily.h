/*!
 * @file
 *
 * @author
 *    david kraus
 */

#ifndef  LM_OUTPUT_SOILCHEMISTRYMETRX_DAILY_H_
#define  LM_OUTPUT_SOILCHEMISTRYMETRX_DAILY_H_

#include  "mbe_legacyoutputmodel.h"

#define  LMOD_OUTPUT_MODULE_ID    "output:soilchemistry-metrx:daily"
#define  LMOD_OUTPUT_MODULE_DESC  "MeTrX Daily Output"
namespace ldndc {
class  LDNDC_API  OutputSoilchemistryMeTrXDaily  :  public  MBE_LegacyOutputModel
{
    LMOD_EXPORT_MODULE_INFO(OutputSoilchemistryMeTrXDaily,LMOD_OUTPUT_MODULE_ID,LMOD_OUTPUT_MODULE_DESC);
    public:
        OutputSoilchemistryMeTrXDaily(
                MoBiLE_State *,
                cbm::io_kcomm_t *,
                timemode_e  _timemode);

        ~OutputSoilchemistryMeTrXDaily();


        lerr_t  configure( ldndc::config_file_t const *);
        lerr_t  initialize();
        lerr_t  solve();
        lerr_t  finalize();

        lerr_t  wake() { return  LDNDC_ERR_OK; }
        lerr_t  sleep() { return  LDNDC_ERR_OK; }

        lerr_t  write_results( ldndc_flt64_t *);
    
        size_t  record_size() const;

    private:
        cbm::io_kcomm_t *  m_iokcomm;
        ldndc::sink_handle_t  m_sink;
};

} /*namespace ldndc*/

#undef  LMOD_OUTPUT_MODULE_ID
#undef  LMOD_OUTPUT_MODULE_DESC

#endif  /*  !LM_OUTPUT_SOILCHEMISTRYMETRX_DAILY_H_  */

