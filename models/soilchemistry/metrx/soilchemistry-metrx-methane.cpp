/*!
 * @file
 *    David Kraus (created on: may 12, 2014)
 *
 */

#include  "soilchemistry/metrx/soilchemistry-metrx.h"

namespace ldndc {


/*!
 * @page metrx
 * @section metrx_methane_production Methane production
 *  @image html metrx_ch4_flow_chart.png "Scheme of methane related processes" width=400
 *  @image latex metrx_ch4_flow_chart.png "Scheme of methane related processes"
 *
 */
void SoilChemistryMeTrX::MeTrX_ch4_production()
{
    for( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
    {
        /*!
         * @page metrx
         *  Methane production takes first place after large fraction of total
         *  iron has been reduced:
         *  \f[
         *  Fe^{3+} < METRX\_FRAC\_FE\_CH4\_PROD \cdot (Fe^{3+} + Fe^{2+})
         *  \f]
         */
        if ( cbm::flt_less( sc_.fe3_sl[sl],
                            sipar_.METRX_FRAC_FE_CH4_PROD() * (sc_.fe2_sl[sl] + sc_.fe3_sl[sl])))
        {
            /*!
             * @page metrx
             *  Methane production depends on temperature and pH:
             *  \f{eqnarray*}{
             *  \phi_{T} &=& METRX\_F\_CH4\_PRODUCTION\_T\_EXP\_1 \\ && \left ( 1- \frac{1}{METRX\_F\_CH4\_PRODUCTION\_T\_EXP\_2} \right )^2 \\
             *  \phi_{pH} &=& \frac{1}{1 + \left( \frac{|pH - 7|}{2} \right)^{3.4}} \\
             *  \f}
             *  @image html metrx_ch4_production_on_ph.png "pH dependency of methane production" width=500
             *  @image latex metrx_ch4_production_on_ph.png "pH dependency of methane production"
             *
             *  @image html metrx_ch4_production_on_temperature.png "Temperature dependency of methane production" width=500
             *  @image latex metrx_ch4_production_on_temperature.png "Temperature dependency of methane production"
             */
            double const fact_no3( 1.0 - no3_an_sl[sl] / (no3_an_sl[sl] + (NO3_MOLAR_MAX_FE_RED * sc_.h_sl[sl] * M_N_SCALE)));
            double const fact_ph( 1.0 / (1.0 + pow(std::abs(sc_.ph_sl[sl] - sipar_.METRX_F_CH4_PRODUCTION_PH_3()) / 
                                                   sipar_.METRX_F_CH4_PRODUCTION_PH_1(), sipar_.METRX_F_CH4_PRODUCTION_PH_2())));
            double const fact_t( std::exp( -sipar_.METRX_F_CH4_PRODUCTION_T_EXP_1() * pow( 1.0 - mc_temp_sl[sl] / sipar_.METRX_F_CH4_PRODUCTION_T_EXP_2(), 2.0)));
            double const fact_t_no3_ph( fact_t * fact_no3 * fact_ph);

            if ( cbm::flt_greater_zero( an_acetate_sl[sl]))
            {
                /*!
                 * @page metrx
                 *  The stoecheometric formula for acetate methanogenesis is given by:
                 *  \f[
                 *  CH_3COOH -> CO_2 + CH_4
                 *  \f]
                 *  Acetoclastic methanogenesis is given by:
                 *  \f[
                 *  \frac{d CH_4}{d t} = METRX\_MUEMAX\_C\_CH4\_PROD \cdot \phi_{CH_3COOH} \cdot \phi_T \cdot \phi_{pH} \cdot \phi_{O_2}
                 *  \f]
                 *  with \f$ \phi_{CH_3COOH}\f$ given by:
                 *  \f[
                 *  \phi_{CH_3COOH} = \frac{CH_3COOH}{METRX\_KMM\_AC\_CH4\_PROD + CH_3COOH}
                 *  \f]
                 */
                double const k_mm( MeTrX_get_k_mm_x_sl( sl, sc_.anvf_sl[sl] * sipar_.METRX_KMM_AC_CH4_PROD()));
                double const fact_ac( MeTrX_get_fact_mm( an_acetate_sl[sl], k_mm));
                double const ac_ch4( std::min( METRX_MUEMAX_C_CH4_PROD * sc_.h_sl[sl] * fact_ac * fact_t_no3_ph,
                                               0.45 * an_acetate_sl[sl]));

                ch4_liq_sl[sl] += ac_ch4;
                co2_liq_sl[sl] += ac_ch4;
                an_acetate_sl[sl] -= (ac_ch4 + ac_ch4);

                co2_hetero_sl[sl] += ac_ch4;
                day_co2_prod_ch4_prod += ac_ch4;
                day_ch4_production_acetate_sl[sl] += ac_ch4;
                subdaily_ch4_prod[subdaily_time_step_] += ac_ch4;
            }
            else
            {
                an_acetate_sl[sl] = 0.0;
            }

            if ( cbm::flt_greater_zero( h2_sl[sl]))
            {
                /*!
                 * @page metrx
                 *  The stoecheometric formula for hydrogenotrophic methanogenesis is given by:
                 *  \f[
                 *  4 H_2 + CO_2 -> 2 H_2O + CH_4
                 *  \f]
                 *  Hydrogenotrophic methanogenesis is given by:
                 *  \f[
                 *  \frac{\partial CH_4}{\partial t} = METRX\_MUEMAX\_H2\_CH4\_PROD \cdot \phi_{H_2} \\
                 \cdot \phi_T \cdot \phi_{pH} \cdot \phi_{O_2}
                 *  \f]
                 */
                double const k_mm( MeTrX_get_k_mm_x_sl( sl, sc_.anvf_sl[sl] * sipar_.METRX_KMM_H2_CH4_PROD()));
                double const fact_h2( MeTrX_get_fact_mm( h2_sl[sl], k_mm));
                double const h2_ch4( std::min( METRX_MUEMAX_H2_CH4_PROD * sc_.h_sl[sl] * fact_h2 * fact_t_no3_ph,
                                               0.9 * std::min(h2_sl[sl] * H2_KG_CH4_KG_CONVERSION, co2_liq_sl[sl])));

                ch4_liq_sl[sl] += h2_ch4;
                co2_liq_sl[sl] -= h2_ch4;
                h2_sl[sl] -= (h2_ch4 / H2_KG_CH4_KG_CONVERSION);

                co2_hetero_sl[sl] -= h2_ch4;
                day_ch4_production_hydrogen_sl[sl] += h2_ch4;
                subdaily_ch4_prod[subdaily_time_step_] += h2_ch4;
            }
            else
            {
                h2_sl[sl] = 0.0;
            }
        }
    }
}



/*!
 * @page metrx
 * @section metrx_methane_oxidation Methane oxidation
 *  The stoecheometry of methanotrophy is given by:
 *  \f[
 *  CH_4 + (2-\alpha) O_2 \rightarrow (1-\alpha) CO_2 + (2-\alpha) H_2O + \alpha CH_2O
 *  \f]
 *
 */
void
SoilChemistryMeTrX::MeTrX_ch4_oxidation()
{
    for( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
    {
        double const aevf( 1.0 - sc_.anvf_sl[sl]);
        double const ch4_tot( ch4_gas_sl[sl] + ch4_liq_sl[sl]);
        double const o2_tot( o2_gas_sl[sl] + o2_liq_sl[sl]);
        if ( !cbm::flt_greater_zero( o2_tot) ||
             !cbm::flt_greater_zero( ch4_tot))
        {
            continue;
        }

        /*!
         * @page metrx
         *  Methane oxidation depends on nitrogen, oxygen and methane concentration as well as temperature:
         *  \f{eqnarray*}{
         *  \phi_{N} &=& (1- METRX\_F\_N\_CH4\_OXIDATION) \\ && METRX\_F\_N\_CH4\_OXIDATION \frac{N}{METRX\_KMM\_N\_CH4\_OX + N}\\
         *  \phi_{O_2} &=& \frac{O_2}{METRX\_K\_O2\_CH4\_OX \cdot O_2} \\
         *  \phi_{CH_4} &=& \frac{CH_4}{METRX\_KMM\_CH4\_CH4\_OX + CH_4} \\
         *  \phi_{T} &=& METRX\_F\_CH4\_OXIDATION\_T\_EXP\_1 \\ && \left ( 1- \frac{1}{METRX\_F\_CH4\_OXIDATION\_T\_EXP\_2} \right )^2 \\
         *  \f}
         *
         *  @image html metrx_ch4_oxidation_on_temperature.png "Temperature dependency of methane oxidation" width=500
         *  @image latex metrx_ch4_oxidation_on_temperature.png "Temperature dependency of methane oxidation"
         */
        double const n_tot( nh4_ae_sl[sl] + nh4_an_sl[sl] + don_ae_sl[sl] + don_an_sl[sl] + no3_ae_sl[sl] + no3_an_sl[sl]);
        double const fact_n( (1.0 -sipar_.METRX_F_N_CH4_OXIDATION())
                            + sipar_.METRX_F_N_CH4_OXIDATION() * n_tot / (MeTrX_get_k_mm_x_sl( sl, aevf * sipar_.METRX_KMM_N_CH4_OX()) + n_tot));

        double const fact_t( std::exp( -sipar_.METRX_F_CH4_OXIDATION_T_EXP_1() * pow( 1.0 - mc_temp_sl[sl] / sipar_.METRX_F_CH4_OXIDATION_T_EXP_2(), 2.0)));
        double const fact_o2( o2_tot / (MeTrX_get_k_mm_x_sl( sl, sipar_.METRX_KMM_O2_CH4_OX()) + o2_tot));
        double const fact_ch4( ch4_tot / (MeTrX_get_k_mm_x_sl( sl, aevf * sipar_.METRX_KMM_CH4_CH4_OX()) + ch4_tot));

        /*!
         * @page metrx
         *  Methane oxidation is given by:
         *  \f[
         *  \frac{d CH_4}{d t} = - METRX\_MUEMAX\_C\_CH4\_OX \cdot \phi_{N} \cdot \phi_{O_2} \cdot \phi_{CH_4} \cdot \phi_{T}
         *  \f]
         *  */
        double const ch4_oxidation_max( cbm::bound_max( ch4_tot, o2_tot / O2_CONVERSION_FOR_CH4_OX));
        double const ch4_oxidation( cbm::bound_max( METRX_MUEMAX_C_CH4_OX * sc_.h_sl[sl] * fact_o2 * fact_ch4 * fact_n * fact_t,
                                                    ch4_oxidation_max));
        double const ch4_oxidation_gas( ch4_gas_sl[sl] / ch4_tot * ch4_oxidation);
        double const ch4_oxidation_liq( ch4_liq_sl[sl] / ch4_tot * ch4_oxidation);

        double const o2_oxidation( ch4_oxidation * O2_CONVERSION_FOR_CH4_OX);
        double const co2_production( ch4_oxidation * (1.0 - sipar_.METRX_MIC_EFF_METHANE_OXIDATION()));
        double const mic_production( ch4_oxidation - co2_production);

        o2_gas_sl[sl] = cbm::bound_min( 0.0, o2_gas_sl[sl] - o2_oxidation * o2_gas_sl[sl] / o2_tot);
        o2_liq_sl[sl] = cbm::bound_min( 0.0, o2_liq_sl[sl] - o2_oxidation * o2_liq_sl[sl] / o2_tot);
        ch4_liq_sl[sl] = cbm::bound_min( 0.0, ch4_liq_sl[sl] - ch4_oxidation_liq);
        ch4_gas_sl[sl] = cbm::bound_min( 0.0, ch4_gas_sl[sl] - ch4_oxidation_gas);
        co2_gas_sl[sl] += co2_production;
        sc_.C_aorg_sl[sl] += mic_production;

        subdaily_ch4_ox[subdaily_time_step_] += ch4_oxidation;
        day_ch4_oxidation_sl[sl] += ch4_oxidation;
        day_co2_prod_ch4_cons += co2_production;
        co2_hetero_sl[sl] += co2_production;
    }
}


    
} /*namespace ldndc*/

