/*!
 * @file
 *
 * @author
 *      David Kraus (created on: may 12, 2014)
 *
 */

#include  "soilchemistry/metrx/soilchemistry-metrx.h"

namespace ldndc {


/*!
 * @page metrx
 * @section som Soil organic matter turnover
 *  Turnover of soil organic matter (SOM) includes
 *  - Decomposition to inorganic carbon dioxide (\f$ CO_2 \f$)
 *  - Decomposition to dissolved organic carbon (DOC)
 *  - Redistribution (humification) of SOM within different pools (e.g., from younger to older humus pools)
 *
 * Dissolved organic carbon is distinguished between the aerob and the anaerob soil 
 * and facilitates microbial metabolism (e.g., nitrification, denitrification, fermentation, ...).
 * During fermentation and synthrophic metabolism, anaerob DOC can be further metabolized to acetate and molecular hydrogen, 
 * which serves methanogenic microbes as substrate.
 * Decomposed nitrogen is always transferred to the dissolved organic nitrogen pool (DON) 
 * from where it is subsequently redistributed depending on pool specific target CN ratios.
 *
 * @image html metrx_c_flow_chart.png "Scheme of Soil organic carbon turnover" width=500
 * @image latex metrx_c_flow_chart.png "Scheme of Soil organic carbon turnover"
 *
 */
void
SoilChemistryMeTrX::MeTrX_soil_organic_matter_turnover()
{
    /*!
     * @page metrx
     * @subsection turnover_aom Turnover of microbial necromass
     * Turnover of microbial necromass involves:
     * - Decomposition (transfer to dissolved organic carbon pool)
     * - Humification (transfer to humus pools)
     */
    for( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
    {
        double const n_tot( nh4_ae_sl[sl] + nh4_an_sl[sl] +
                            no3_ae_sl[sl] + no3_an_sl[sl] +
                            don_ae_sl[sl] + don_an_sl[sl]);
        double const fact_n( n_tot / (MeTrX_get_k_mm_x_sl( sl, sipar_.METRX_KMM_N_MIC()) + n_tot));
        double const cn_target( sipar_.METRX_CN_MIC_MAX() -
                               (sipar_.METRX_CN_MIC_MAX() - sipar_.METRX_CN_MIC_MIN()) * fact_n);
        cn_opt_mic_sl[sl] += MUE_MAX_C_MICRO_1 * (cn_target - cn_opt_mic_sl[sl]);

        /*!
         * @page metrx
         * Decomposition of inactive microbial carbon \f$ m_{imc} \f$ and
         * nitrogen \f$ m_{imn} \f$ depends on soil temperature, soil moisture and soil anaerobicity:
         * \f{eqnarray*}{
         * \Delta m_{imc \rightarrow doc_{ae}} = K\_DC\_AORG \cdot m_{imc} \cdot \phi_{tm} \cdot \phi_{till} \cdot (1 - anvf) \\
         * \Delta m_{imc \rightarrow doc_{an}} = K\_DC\_AORG \cdot m_{imc} \cdot \phi_{tm} \cdot \phi_{till} \cdot anvf \\
         * \Delta m_{imn} = \frac{\Delta m_{imc \rightarrow doc_{ae}} + \Delta m_{imc \rightarrow doc_{ae}}}{CN_{im}} \\
         * \f}
         *
         * Temperature moisture factor
         * @image html metrx_decomposition_on_temperature_soilwater.png "Response function of decomposition of inactive microbes depending on temperature and moisture" width=500
         * @image latex metrx_decomposition_on_temperature_soilwater.png "Response function of decomposition of inactive microbes depending on temperature and moisture"
         *
         * Humification of inactive microbial carbon is given by:
         * \f{eqnarray*}{
         * \Delta m_{imc \rightarrow hum (young)} = METRX\_KR\_HU\_AORG\_HUM\_0 \cdot m_{imc} \\
         * \Delta m_{imc \rightarrow hum (old, high CN)} = METRX\_KR\_HU\_AORG\_HUM\_1 \cdot m_{imc} \\
         * \f}
         */
        double const fact_tm( MeTrX_get_fact_tm_decomp( sl));
        double const active_aorg( fact_tm * sc_.till_effect_sl[sl] *  sc_.C_aorg_sl[sl]);

        double const decomp_aorg_ae( K_DC_AORG * (1.0 - sc_.anvf_sl[sl]) * active_aorg);
        double const decomp_aorg_an( K_DC_AORG * sc_.anvf_sl[sl] * active_aorg);

        double const humify_aorg_hum_1( K_HU_AORG_HUM1 * active_aorg);
        double const humify_aorg_hum_2( K_HU_AORG_HUM2 * active_aorg);

        double const n_change( cbm::flt_greater_zero( sc_.N_aorg_sl[sl]) ?
                               cbm::bound( 0.0, sc_.N_aorg_sl[sl] - sc_.C_aorg_sl[sl] / cn_opt_mic_sl[sl], 0.9 * sc_.N_aorg_sl[sl]) :
                               0.0);
        double const mineral_n( N_MINERALISATION_FRACTION * n_change);

        /*************/
        /* integrate */
        /*************/

        sc_.C_aorg_sl[sl] -= (decomp_aorg_ae + decomp_aorg_an + humify_aorg_hum_1 + humify_aorg_hum_2);
        sc_.doc_sl[sl] += decomp_aorg_ae;
        sc_.an_doc_sl[sl] += decomp_aorg_an;

        //dot_metrx_nitrogen N_aorg_sl -> don_sl
        //dot_metrx_nitrogen N_aorg_sl -> nh4_sl
        sc_.N_aorg_sl[sl] -= n_change;
        don_ae_sl[sl] += (n_change - mineral_n) * (1.0 - sc_.anvf_sl[sl]);
        don_an_sl[sl] += (n_change - mineral_n) * sc_.anvf_sl[sl];
        nh4_ae_sl[sl] += mineral_n * (1.0 - sc_.anvf_sl[sl]);
        nh4_an_sl[sl] += mineral_n * sc_.anvf_sl[sl];

        c_humus_1_sl[sl] += humify_aorg_hum_1;
        c_humus_2_sl[sl] += humify_aorg_hum_2;


        /*********/
        /* write */
        /*********/

        accumulated_n_aorg_don_dissolve_sl[sl] += n_change - mineral_n;
        accumulated_n_aorg_nh4_mineral_sl[sl] += mineral_n;

        day_c_humify_mic_hum_1_sl[sl] += humify_aorg_hum_1;
        day_c_humify_mic_hum_2_sl[sl] += humify_aorg_hum_2;

        double const doc_prod_tot( decomp_aorg_ae + decomp_aorg_an);
        day_doc_prod_decomp_aorg += doc_prod_tot;
        day_doc_prod_sl[sl] += doc_prod_tot;

        day_min_n_aorg += mineral_n;
        sc_.accumulated_n_mineral_sl[sl] += mineral_n;

        /**********/
        /* others */
        /**********/

        //equilibrium spinup of humus pools
        if ( spinup_.spinup_stage( this->lclock()))
        {
            spinup_.humify_c_to_humus_1_sl[sl] += humify_aorg_hum_1;
            spinup_.humify_c_to_humus_2_sl[sl] += humify_aorg_hum_2;
        }
    }

    /*!
     * @page metrx
     * @subsection turnover_som Turnover of soil organic matter and plant debris
     * Turnover of SOM and plant debris includes:
     * - Decomposition (transfer to dissolved organic carbon pool)
     * - Humification (transfer to humus pools)
     *
     * Turnover rates depend on:
     * - Climatic factors (temperature, moisture)
     * - Soil related factors (clay content, pH, aerobicity)
     * - Chemical composition (litter type, CN ratio)
     * - Management (tilling)
     *
     * Depending on the litter or humus pool the general turnover rate is given by:
     * \f[
     * \frac{dm_x}{dt} = K_x \cdot m_x \cdot \Pi_i \phi_{i}
     * \f]
     * with \f$ \Pi_i \phi_{i} \f$ being the multiplicative combination of
     * pool specific environmental reduction factors \f$ \phi_{i} \f$ (e.g., temperature, moisture,...)
     * determining turnover.
     */
    /*!
     * @todo
     * Consider cellulose and lignin activity:
     * Divergent responses of carbon-degrading enzyme activities to litter alterations:
     * Implications for soil respiration DOI: 10.22541/au.166627305.57736558/v1
     */
    for( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
    {
        /*********/
        /* solve */
        /*********/

        /*!
         * @page metrx
         * The influence of temperature and moisture is given by:
         * \f[
         * \phi_{t,m} = \frac{2}{\frac{1}{\phi_{t}} + \frac{1}{\phi_{m}}}
         * \f]
         *  @image html metrx_decomposition_on_temperature_soilwater.png "Response function of decomposition depending on temperature and moisture" width=900
         *  @image latex metrx_decomposition_on_temperature_soilwater.png "Response function of decomposition depending on temperature and moisture"
         */
        double const fact_tm( MeTrX_get_fact_tm_decomp( sl));

        // chemical composition factors (lignin concentration + CN ratio)
        double const c_lit_tot( sc_.C_lit1_sl[sl] + sc_.C_lit2_sl[sl] + sc_.C_lit3_sl[sl]);
        double const c_sol_tot( sc_.doc_sl[sl] + sc_.an_doc_sl[sl] + ae_acetate_sl[sl] + an_acetate_sl[sl]);

        double const n_lit_tot( sc_.N_lit1_sl[sl] + sc_.N_lit2_sl[sl] + sc_.N_lit3_sl[sl]);
        double const n_sol_tot( don_ae_sl[sl] + don_an_sl[sl] + nh4_ae_sl[sl] + nh4_an_sl[sl] + no3_ae_sl[sl] + no3_an_sl[sl]);
        double const n_lit_sol( n_lit_tot + n_sol_tot);

        /*!
         * @page metrx
         * The influence of the lignin content of the litter is given by:
         * \f[
         * \phi_{lig} = e^{-METRX\_BETA\_LITTER\_TYPE \cdot \frac{c_{lig}}{c_{tot}}}
         * \f]
         */
        double const fact_lc( cbm::flt_greater_zero( c_lit_tot) ?
                              std::exp( -sipar_.METRX_BETA_LITTER_TYPE() * sc_.C_lit3_sl[sl] / c_lit_tot)
                              : 0.0);

        /*!
         * @page metrx
         * The influence of litter quality (C/N ratio) is given by:
         * \f[
         * \phi_{litter} = 1 - METRX\_KR\_REDUCTION\_CN \cdot \frac{C_{lit}}{N_{lit}}
         * \f]
         */
        double const fact_cn( cbm::flt_greater_zero( n_lit_sol) ?
                              std::max( 1.0 - (sipar_.METRX_KR_REDUCTION_CN() * (c_lit_tot + c_sol_tot) / n_lit_sol), 0.1)
                              : 0.0);

        /*!
         * @page metrx
         * The influence of clay content is given by
         * \f[
         * \phi_{clay} = METRX\_F\_DECOMP\_CLAY\_1 + (1- METRX\_F\_DECOMP\_CLAY\_1) \cdot e^{-METRX\_F\_DECOMP\_CLAY\_2 \cdot c_{clay}}
         * \f]
         *
         *  @image html metrx_decomposition_on_clay.png "Response function of decomposition depending on clay" width=500
         *  @image latex metrx_decomposition_on_clay.png "Response function of decomposition depending on clay"
         */
        double const fact_clay( sipar_.METRX_F_DECOMP_CLAY_1() +
                                (1.0 - sipar_.METRX_F_DECOMP_CLAY_1()) * std::exp(-sipar_.METRX_F_DECOMP_CLAY_2() * sc_.clay_sl[sl]));

        /*!
         * @page metrx
         * The influence of O2 availability is given by the anaerobic volume fraction (\f$ anvf \f$).
         * The carbon flux from each litter and humus pool due to decomposition is distinguished by \f$ anvf \f$:
         * \f{eqnarray*}{
         * \frac{dm_x}{dt} &=& \frac{dm_{x, anvf}}{dt} + \frac{dm_{x, aevf}}{dt} \\
         * \frac{dm_{x, anvf}}{dt} &=& K_x \cdot m_x \cdot \Pi_j \phi_{j} \cdot METRX\_KR\_REDUCTION\_ANVF \cdot anvf \\
         * \frac{dm_{x, aevf}}{dt} &=& K_x \cdot m_x \cdot \Pi_j \phi_{j} \cdot (1 - anvf)
         * \f}
         */
        double const fact_aevr( (1.0 - sc_.anvf_sl[sl]));
        double const fact_anvr( sipar_.METRX_KR_REDUCTION_ANVF() * sc_.anvf_sl[sl]);

        /*!
         * @page metrx
         * The influence of pH is given by:
         * \f[
         * \phi_{pH} = \frac{1}{1 + e^{-METRX\_F\_DECOMP\_PH\_1 \cdot (pH - METRX\_F\_DECOMP\_PH\_2)}}
         * \f]
         *
         *  @image html metrx_decomposition_on_ph.png "Response function of decomposition depending on pH" width=500
         *  @image latex metrx_decomposition_on_ph.png "Response function of decomposition depending on pH"
         */
        double const fact_ph( 1.0 / (1.0 + std::exp(-sipar_.METRX_F_DECOMP_PH_1() * (sc_.ph_sl[sl] - sipar_.METRX_F_DECOMP_PH_2()))));

        // compile factors
        // litter type, litter CN ratio and litter lignin ratio only effects litter decomposition
        // clay content only effects humus decomposition
        double const fact_ft_tm_ph( std::max( freeze_thaw_fact_sl[sl],
                                              fact_tm * fact_ph));

        double const fact_ft_tm_ph_litter( fact_ft_tm_ph * litter_type_fact_sl[sl] * fact_cn * fact_lc);
        double const fact_till_ft_tm_ph_litter_ae( fact_ft_tm_ph_litter * fact_aevr * sc_.till_effect_sl[sl]);
        double const fact_till_ft_tm_ph_litter_an( fact_ft_tm_ph_litter * fact_anvr * sc_.till_effect_sl[sl]);

        double const fact_ft_tm_ph_clay( fact_ft_tm_ph * fact_clay);
        double const fact_till_ft_tm_ph_clay_ae( fact_ft_tm_ph_clay * fact_aevr * sc_.till_effect_sl[sl]);
        double const fact_till_ft_tm_ph_clay_an( fact_ft_tm_ph_clay * fact_anvr * sc_.till_effect_sl[sl]);


        /*!
         * @page metrx
         * <b> Overview of reduction factors for decomposition of litter and humus pools </b>
         * Pool | Parameter | tilling | Temp. Moisture | pH | litter type | litter C/N | clay | O2
         * ---- | --------- | ------- | -------------- | -- | ----------- | -----------| ---- | --
         * Solutes   | METRX_KR_DC_SOL   | yes | yes | yes | yes | yes | no  | yes
         * Cellulose | METRX_KR_DC_CEL   | yes | yes | yes | yes | yes | no  | yes
         * Lignin    | METRX_KR_DC_CEL   | yes | yes | yes | yes | yes | no  | yes
         * Humus 1   | METRX_KR_DC_HUM1 | yes | yes | yes | no  | no  | yes | yes
         * Humus 2   | METRX_KR_DC_HUM2 | yes | yes | yes | no  | no  | yes | yes
         * Humus 3   | METRX_KR_DC_HUM3 | yes | yes | yes | no  | no  | yes | yes
         */
        double const fact_decomp_sol_ae( K_DC_SOL * fact_till_ft_tm_ph_litter_ae);
        double const fact_decomp_sol_an( K_DC_SOL * fact_till_ft_tm_ph_litter_an);

        double const fact_decomp_cel_ae( K_DC_CEL * fact_till_ft_tm_ph_litter_ae);
        double const fact_decomp_cel_an( K_DC_CEL * fact_till_ft_tm_ph_litter_an);

        double const fact_decomp_lig( K_DC_LIG * fact_till_ft_tm_ph_litter_ae);

        double const fact_decomp_hum_1_ae( K_DC_HUM1 * fact_till_ft_tm_ph_clay_ae);
        double const fact_decomp_hum_1_an( K_DC_HUM1 * fact_till_ft_tm_ph_clay_an);

        double const cn_factor( 1.0 - 0.99 / (1.0 + std::exp(-0.5 * (cn_hum_2_sl[sl] - 30.0))));
        double const fact_decomp_hum_2_ae( K_DC_HUM2 * fact_till_ft_tm_ph_clay_ae);
        double const fact_decomp_hum_2_an( K_DC_HUM2 * fact_till_ft_tm_ph_clay_an * cn_factor);

        double const fact_decomp_hum_3_ae( K_DC_HUM3 * fact_till_ft_tm_ph_clay_ae);
        double const fact_decomp_hum_3_an( K_DC_HUM3 * fact_till_ft_tm_ph_clay_an);

        /************************/
        /* Litter decomposition */
        /************************/

        double const decomp_c_sol_ae( fact_decomp_sol_ae * sc_.C_lit1_sl[sl]);
        double const decomp_c_sol_an( fact_decomp_sol_an * sc_.C_lit1_sl[sl]);
        double const decomp_c_sol_tot( decomp_c_sol_ae + decomp_c_sol_an);

        double const decomp_c_cel_ae( fact_decomp_cel_ae * sc_.C_lit2_sl[sl]);
        double const decomp_c_cel_an( fact_decomp_cel_an * sc_.C_lit2_sl[sl]);
        double const decomp_c_cel_tot( decomp_c_cel_ae + decomp_c_cel_an);

        double const decomp_c_lig_ae( fact_decomp_lig * sc_.C_lit3_sl[sl]);

        double const decomp_n_sol_ae( fact_decomp_sol_ae * sc_.N_lit1_sl[sl]);
        double const decomp_n_sol_an( fact_decomp_sol_an * sc_.N_lit1_sl[sl]);
        double const decomp_n_sol_tot( decomp_n_sol_ae + decomp_n_sol_an);

        double const decomp_n_cel_ae( fact_decomp_cel_ae * sc_.N_lit2_sl[sl]);
        double const decomp_n_cel_an( fact_decomp_cel_an * sc_.N_lit2_sl[sl]);
        double const decomp_n_cel_tot( decomp_n_cel_ae + decomp_n_cel_an);

        double const decomp_n_lig_ae( fact_decomp_lig * sc_.N_lit3_sl[sl]);


        /***********************/
        /* Humus decomposition */
        /***********************/

        double const decomp_c_hum_1_ae( fact_decomp_hum_1_ae * c_humus_1_sl[sl]);
        double const decomp_c_hum_1_an( fact_decomp_hum_1_an * c_humus_1_sl[sl]);
        double const decomp_c_hum_1_tot( decomp_c_hum_1_ae + decomp_c_hum_1_an);

        double const decomp_c_hum_2_ae( fact_decomp_hum_2_ae * c_humus_2_sl[sl]);
        double const decomp_c_hum_2_an( fact_decomp_hum_2_an * c_humus_2_sl[sl]);
        double const decomp_c_hum_2_tot( decomp_c_hum_2_ae + decomp_c_hum_2_an);

        double const decomp_c_hum_3_ae( fact_decomp_hum_3_ae * c_humus_3_sl[sl]);
        double const decomp_c_hum_3_an( fact_decomp_hum_3_an * c_humus_3_sl[sl]);
        double const decomp_c_hum_3_tot( decomp_c_hum_3_ae + decomp_c_hum_3_an);

        double const decomp_n_hum_1_ae( fact_decomp_hum_1_ae * n_humus_1_sl[sl]);
        double const decomp_n_hum_1_an( fact_decomp_hum_1_an * n_humus_1_sl[sl]);
        double const decomp_n_hum_1_tot( decomp_n_hum_1_ae + decomp_n_hum_1_an);

        double const decomp_n_hum_2_ae( fact_decomp_hum_2_ae * n_humus_2_sl[sl]);
        double const decomp_n_hum_2_an( fact_decomp_hum_2_an * n_humus_2_sl[sl]);
        double const decomp_n_hum_2_tot( decomp_n_hum_2_ae + decomp_n_hum_2_an);

        double const decomp_n_hum_3_ae( fact_decomp_hum_3_ae * n_humus_3_sl[sl]);
        double const decomp_n_hum_3_an( fact_decomp_hum_3_an * n_humus_3_sl[sl]);
        double const decomp_n_hum_3_tot( decomp_n_hum_3_ae + decomp_n_hum_3_an);


        /*****************/
        /* Litter update */
        /*****************/

        sc_.C_lit1_sl[sl] -= decomp_c_sol_tot;
        sc_.C_lit2_sl[sl] -= decomp_c_cel_tot;
        sc_.C_lit3_sl[sl] -= decomp_c_lig_ae;

        sc_.N_lit1_sl[sl] -= decomp_n_sol_tot;
        sc_.N_lit2_sl[sl] -= decomp_n_cel_tot;
        sc_.N_lit3_sl[sl] -= decomp_n_lig_ae;

        day_decomp_c_lit_1 += decomp_c_sol_tot;                                                    // Output
        day_decomp_c_lit_2 += decomp_c_cel_tot;                                                    // Output
        day_decomp_c_lit_3 += decomp_c_lig_ae;                                                     // Output

        day_decomp_n_lit_1 += decomp_n_sol_tot;                                                    // Output
        day_decomp_n_lit_2 += decomp_n_cel_tot;                                                    // Output
        day_decomp_n_lit_3 += decomp_n_lig_ae;                                                     // Output


        /****************/
        /* Humus update */
        /****************/

        c_humus_1_sl[sl] -= decomp_c_hum_1_tot;
        c_humus_2_sl[sl] -= decomp_c_hum_2_tot;
        c_humus_3_sl[sl] -= decomp_c_hum_3_tot;

        n_humus_1_sl[sl] -= decomp_n_hum_1_tot;
        n_humus_2_sl[sl] -= decomp_n_hum_2_tot;
        n_humus_3_sl[sl] -= decomp_n_hum_3_tot;

        day_decomp_c_hum_1_sl[sl] += decomp_c_hum_1_tot;                                           // Output
        day_decomp_c_hum_2_sl[sl] += decomp_c_hum_2_tot;                                           // Output
        day_decomp_c_hum_3_sl[sl] += decomp_c_hum_3_tot;                                           // Output

        day_decomp_n_hum_1 += decomp_n_hum_1_tot;                                                  // Output
        day_decomp_n_hum_2 += decomp_n_hum_2_tot;                                                  // Output
        day_decomp_n_hum_3 += decomp_n_hum_3_tot;                                                  // Output


        /*!
         * @page metrx
         * Decomposed carbon is added to DOC:
         * \f{eqnarray*}{
         * \frac{dDOC_{ae}}{dt} &=& -\frac{dC_{x, aevf}}{dt} \\
         * \frac{dDOC_{an}}{dt} &=& -\frac{dC_{x, anvf}}{dt}
         * \f}
         */
        double const c_decomp_litter_ae( decomp_c_sol_ae + decomp_c_cel_ae + decomp_c_lig_ae);
        double const c_decomp_humus_ae( decomp_c_hum_1_ae + decomp_c_hum_2_ae + decomp_c_hum_3_ae);

        double const c_decomp_litter_an( decomp_c_sol_an + decomp_c_cel_an);
        double const c_decomp_humus_an( decomp_c_hum_1_an + decomp_c_hum_2_an + decomp_c_hum_3_an);

        double c_decomp_tot_ae( c_decomp_litter_ae + c_decomp_humus_ae);
        double const c_decomp_tot_an( c_decomp_litter_an + c_decomp_humus_an);

        double const frac_litter_ae( c_decomp_litter_ae / c_decomp_tot_ae);
        double const frac_humus_ae( 1.0 - frac_litter_ae);

        double const frac_litter_an( c_decomp_litter_an / c_decomp_tot_an);
        double const frac_humus_an( 1.0 - frac_litter_an);

        sc_.doc_sl[sl] += c_decomp_tot_ae;
        sc_.an_doc_sl[sl] += c_decomp_tot_an;

        double const doc_prod_tot( c_decomp_tot_ae + c_decomp_tot_an);
        day_doc_prod_sl[sl] += doc_prod_tot;
        subdaily_doc_prod[subdaily_time_step_] += doc_prod_tot;

        day_doc_prod_decomp_litter += ((c_decomp_tot_ae * frac_litter_ae) + (c_decomp_tot_an * frac_litter_an));
        day_doc_prod_decomp_humus += ((c_decomp_tot_ae * frac_humus_ae) + (c_decomp_tot_an * frac_humus_an));

        /*!
         * @page metrx
         * Decomposed nitrogen is partly mineralized and partly added to DON:
         * \f{eqnarray*}{
         * \frac{dNH_4}{dt} &=& -0.5 \frac{dN_{x}}{dt} \\
         * \frac{dDON}{dt} &=& -0.5 \frac{dN_{x}}{dt}
         * \f}
         */
        double const decomp_n_ae_litter( decomp_n_sol_ae + decomp_n_cel_ae + decomp_n_lig_ae );
        double const decomp_n_an_litter( decomp_n_sol_an + decomp_n_cel_an);

        double const decomp_n_litter( decomp_n_ae_litter + decomp_n_an_litter);
        double const decomp_n_humus_1( decomp_n_hum_1_ae + decomp_n_hum_1_an);
        double const decomp_n_humus_2( decomp_n_hum_2_ae + decomp_n_hum_2_an);
        double const decomp_n_humus_3( decomp_n_hum_3_ae + decomp_n_hum_3_an);

        double const mineral_n_litter( N_MINERALISATION_FRACTION * decomp_n_litter);
        double const mineral_n_humus_1( N_MINERALISATION_FRACTION * decomp_n_humus_1);
        double const mineral_n_humus_2( N_MINERALISATION_FRACTION * decomp_n_humus_2);
        double const mineral_n_humus_3( N_MINERALISATION_FRACTION * decomp_n_humus_3);

        double const add_don(  decomp_n_litter + decomp_n_humus_1 + decomp_n_humus_2 + decomp_n_humus_3
                             - mineral_n_litter - mineral_n_humus_1 - mineral_n_humus_2 - mineral_n_humus_3);
        double const add_nh4( mineral_n_litter + mineral_n_humus_1 + mineral_n_humus_2 + mineral_n_humus_3);
        don_ae_sl[sl] += add_don * (1.0 - sc_.anvf_sl[sl]);
        don_an_sl[sl] += add_don * sc_.anvf_sl[sl];
        nh4_ae_sl[sl] += add_nh4 * (1.0 - sc_.anvf_sl[sl]);
        nh4_an_sl[sl] += add_nh4 * sc_.anvf_sl[sl];

        //dot_metrx_nitrogen N_lit_sl -> don_sl [label="decomp."];
        //dot_metrx_nitrogen N_lit_sl -> nh4_sl [label="decomp."];
        accumulated_n_humus_1_don_dissolve_sl[sl] += decomp_n_humus_1 - mineral_n_humus_1;
        accumulated_n_humus_2_don_dissolve_sl[sl] += decomp_n_humus_2 - mineral_n_humus_2;
        accumulated_n_humus_3_don_dissolve_sl[sl] += decomp_n_humus_3 - mineral_n_humus_3;
        accumulated_n_humus_1_nh4_mineral_sl[sl] += mineral_n_humus_1;
        accumulated_n_humus_2_nh4_mineral_sl[sl] += mineral_n_humus_2;
        accumulated_n_humus_3_nh4_mineral_sl[sl] += mineral_n_humus_3;

        //dot_metrx_nitrogen n_humus_sl -> don_sl [label="decomp."];
        //dot_metrx_nitrogen n_humus_sl -> nh4_sl [label="decomp."];
        accumulated_n_litter_don_dissolve_sl[sl] += decomp_n_litter - mineral_n_litter;
        accumulated_n_litter_nh4_mineral_sl[sl] += mineral_n_litter;

        day_min_n_decomp += mineral_n_litter + mineral_n_humus_1 + mineral_n_humus_2 + mineral_n_humus_3;
        sc_.accumulated_n_mineral_sl[sl] += mineral_n_litter + mineral_n_humus_1 + mineral_n_humus_2 + mineral_n_humus_3;


        /******************************/
        /* Litter / soil Humification */
        /******************************/

        /* C-humification */

        double const humify_acetate_ae( K_HU_DOC * ae_acetate_sl[sl]);
        double const humify_acetate_an( K_HU_DOC * an_acetate_sl[sl]);

        double const humify_doc_ae( K_HU_DOC * sc_.doc_sl[sl]);
        double const humify_doc_an( K_HU_DOC * sc_.an_doc_sl[sl]);

        double const humify_c_sol( K_HU_SOL * sc_.C_lit1_sl[sl]);
        double const humify_c_cel( K_HU_CEL * sc_.C_lit2_sl[sl]);
        double const humify_c_lig( K_HU_LIG * sc_.C_lit3_sl[sl]);

        double const humify_c_lig_hum_1( (1.0 - sipar_.METRX_LIG_HUMIFICATION()) * humify_c_lig);
        double const humify_c_lig_hum_2( sipar_.METRX_LIG_HUMIFICATION() * humify_c_lig);

        double const humify_c_hum_1( K_HU_HUM1 * c_humus_1_sl[sl]);
        double const humify_c_hum_2( K_HU_HUM2 * c_humus_2_sl[sl] * cn_factor);

        ae_acetate_sl[sl] -= humify_acetate_ae;
        an_acetate_sl[sl] -= humify_acetate_an;

        sc_.doc_sl[sl] -= humify_doc_ae;
        sc_.an_doc_sl[sl] -= humify_doc_an;

        sc_.C_lit1_sl[sl] -= humify_c_sol;
        sc_.C_lit2_sl[sl] -= humify_c_cel;
        sc_.C_lit3_sl[sl] -= humify_c_lig;

        c_humus_1_sl[sl] -= (humify_c_hum_1 - humify_acetate_ae - humify_acetate_an - humify_doc_ae - humify_doc_an - humify_c_sol - humify_c_cel - humify_c_lig_hum_1);
        c_humus_2_sl[sl] -= (humify_c_hum_2 - humify_c_hum_1 - humify_c_lig_hum_2);
        c_humus_3_sl[sl] += humify_c_hum_2;

        day_c_humify_doc_hum_1_sl[sl] += humify_acetate_ae + humify_acetate_an + humify_doc_ae + humify_doc_an;
        day_c_humify_sol_hum_1_sl[sl] += humify_c_sol;
        day_c_humify_cel_hum_1_sl[sl] += humify_c_cel;
        day_c_humify_lig_hum_1_sl[sl] += humify_c_lig_hum_1;
        day_c_humify_lig_hum_2_sl[sl] += humify_c_lig_hum_2;
        day_c_humify_hum_1_hum_2_sl[sl] += humify_c_hum_1;
        day_c_humify_hum_2_hum_3_sl[sl] += humify_c_hum_2;


        /* N-humification */

        double const humify_n_sol( K_HU_SOL * sc_.N_lit1_sl[sl]);
        double const humify_n_cel( K_HU_CEL * sc_.N_lit2_sl[sl]);
        double const humify_n_lig( K_HU_LIG * sc_.N_lit3_sl[sl]);

        sc_.N_lit1_sl[sl] -= humify_n_sol;
        sc_.N_lit2_sl[sl] -= humify_n_cel;
        sc_.N_lit3_sl[sl] -= humify_n_lig;
        n_humus_1_sl[sl] += (humify_n_sol + humify_n_cel + humify_n_lig);
        accumulated_n_litter_humus_1_humify_sl[sl] += (humify_n_sol + humify_n_cel + humify_n_lig);

        double n_demand_hum_1( (c_humus_1_sl[sl] / cn_hum_1_sl[sl]) - n_humus_1_sl[sl]);
        double n_demand_hum_2( (c_humus_2_sl[sl] / cn_hum_2_sl[sl]) - n_humus_2_sl[sl]);
        double n_demand_hum_3( (c_humus_3_sl[sl] / cn_hum_3_sl[sl]) - n_humus_3_sl[sl]);

        //dot_metrx_nitrogen n_humus_sl -> don_sl [label="CN decomp."];
        //dot_metrx_nitrogen n_humus_sl -> nh4_sl [label="CN decomp."];
        if ( cbm::flt_less( n_demand_hum_1, 0.0))
        {
            double const n_transfer( -K_DC_HUM1 * n_demand_hum_1);
            double const n_mineral( N_MINERALISATION_FRACTION * n_transfer);
            n_humus_1_sl[sl] -= n_transfer;
            don_ae_sl[sl] += (n_transfer - n_mineral) * (1.0 - sc_.anvf_sl[sl]);
            don_an_sl[sl] += (n_transfer - n_mineral) * sc_.anvf_sl[sl];
            nh4_ae_sl[sl] += n_mineral * (1.0 - sc_.anvf_sl[sl]);
            nh4_an_sl[sl] += n_mineral * sc_.anvf_sl[sl];
            day_min_n_decomp += n_mineral;
            sc_.accumulated_n_mineral_sl[sl] += n_mineral;
            accumulated_n_humus_1_don_dissolve_sl[sl] += n_transfer - n_mineral;
            accumulated_n_humus_1_nh4_mineral_sl[sl] += n_mineral;
            n_demand_hum_1 = 0.0;
        }

        if ( cbm::flt_less( n_demand_hum_2, 0.0))
        {
            double const n_transfer( -K_DC_HUM2 * n_demand_hum_2);
            double const n_mineral( N_MINERALISATION_FRACTION * n_transfer);
            n_humus_2_sl[sl] -= n_transfer;
            don_ae_sl[sl] += (n_transfer - n_mineral) * (1.0 - sc_.anvf_sl[sl]);
            don_an_sl[sl] += (n_transfer - n_mineral) * sc_.anvf_sl[sl];
            nh4_ae_sl[sl] += n_mineral * (1.0 - sc_.anvf_sl[sl]);
            nh4_an_sl[sl] += n_mineral * sc_.anvf_sl[sl];
            day_min_n_decomp += n_mineral;
            sc_.accumulated_n_mineral_sl[sl] += n_mineral;
            accumulated_n_humus_2_don_dissolve_sl[sl] += n_transfer - n_mineral;
            accumulated_n_humus_2_nh4_mineral_sl[sl] += n_mineral;
            n_demand_hum_2 = 0.0;
        }

        if ( cbm::flt_less( n_demand_hum_3, 0.0))
        {
            double const n_transfer( -K_DC_HUM3 * n_demand_hum_3);
            double const n_mineral( N_MINERALISATION_FRACTION * n_transfer);
            n_humus_3_sl[sl] -= n_transfer;
            don_ae_sl[sl] += (n_transfer - n_mineral) * (1.0 - sc_.anvf_sl[sl]);
            don_an_sl[sl] += (n_transfer - n_mineral) * sc_.anvf_sl[sl];
            nh4_ae_sl[sl] += n_mineral * (1.0 - sc_.anvf_sl[sl]);
            nh4_an_sl[sl] += n_mineral * sc_.anvf_sl[sl];
            day_min_n_decomp += n_mineral;
            sc_.accumulated_n_mineral_sl[sl] += n_mineral;
            accumulated_n_humus_3_don_dissolve_sl[sl] += n_transfer - n_mineral;
            accumulated_n_humus_3_nh4_mineral_sl[sl] += n_mineral;
            n_demand_hum_3 = 0.0;
        }


        double const don_tot( don_ae_sl[sl] + don_an_sl[sl]);
        double const n_demand_tot( n_demand_hum_1 + n_demand_hum_2 + n_demand_hum_3);
        if ( cbm::flt_greater_zero( n_demand_tot))
        {
            if ( cbm::flt_greater( don_tot, n_demand_tot))
            {
                double const don_ae_use( don_ae_sl[sl] / don_tot * n_demand_tot);
                double const don_an_use( n_demand_tot - don_ae_use);
                don_ae_sl[sl] -= don_ae_use;
                don_an_sl[sl] -= don_an_use;

                n_humus_1_sl[sl] += n_demand_hum_1;
                n_humus_2_sl[sl] += n_demand_hum_2;
                n_humus_3_sl[sl] += n_demand_hum_3;

                //dot_metrx_nitrogen don_sl -> n_humus_sl [label="humify"];
                accumulated_n_don_humus_1_humify_sl[sl] += n_demand_hum_1;
                accumulated_n_don_humus_2_humify_sl[sl] += n_demand_hum_2;
                accumulated_n_don_humus_3_humify_sl[sl] += n_demand_hum_3;
            }
            else
            {
                don_ae_sl[sl] = 0.0;
                don_an_sl[sl] = 0.0;

                double const n_humus_1_avail( n_demand_hum_1 / n_demand_tot * don_tot);
                double const n_humus_2_avail( n_demand_hum_2 / n_demand_tot * don_tot);
                double const n_humus_3_avail( n_demand_hum_3 / n_demand_tot * don_tot);
                n_humus_1_sl[sl] += n_humus_1_avail;
                n_humus_2_sl[sl] += n_humus_2_avail;
                n_humus_3_sl[sl] += n_humus_3_avail;

                //dot_metrx_nitrogen don_sl -> n_humus_sl [label="humify"];
                accumulated_n_don_humus_1_humify_sl[sl] += n_humus_1_avail;
                accumulated_n_don_humus_2_humify_sl[sl] += n_humus_2_avail;
                accumulated_n_don_humus_3_humify_sl[sl] += n_humus_3_avail;
            }
        }

        /**********/
        /* others */
        /**********/

        //equilibrium spinup of humus pools
        if ( spinup_.spinup_stage( this->lclock()))
        {
            spinup_.decompose_hum_1[sl] += fact_decomp_hum_1_ae+fact_decomp_hum_1_an;
            spinup_.decompose_hum_2[sl] += fact_decomp_hum_2_ae+fact_decomp_hum_2_an;
            spinup_.decompose_hum_3[sl] += fact_decomp_hum_3_ae+fact_decomp_hum_3_an;

            spinup_.humify_humus_1_to_humus_2_sl[sl] += K_HU_HUM1;
            spinup_.humify_humus_2_to_humus_3_sl[sl] += K_HU_HUM2 * cn_factor;

            spinup_.humify_c_to_humus_1_sl[sl] += humify_doc_ae + humify_doc_an + humify_c_sol + humify_c_cel + humify_c_lig_hum_1;
            spinup_.humify_c_to_humus_2_sl[sl] += humify_c_lig_hum_2;
        }
    }
}


} /*namespace ldndc*/
