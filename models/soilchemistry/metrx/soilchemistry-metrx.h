
#ifndef  LM_SOILCHEMISTRYMETRX_H_
#define  LM_SOILCHEMISTRYMETRX_H_

#include  "mbe_legacymodel.h"
#include  "state/mbe_state.h"
#include  "ld_shared.h"

#include  "models/soilchemistry/metrx/output-soilchemistry-metrx.h"
#include  "models/soilchemistry/metrx/soilchemistry-metrx.h.inc"
#include  "models/soilchemistry/ld_transport.h"
#include  "models/soilchemistry/ld_enhanced_efficiency_nitrogen_fertilizers.h"

#include  "eventhandler/fertilize/fertilize.h"
#include  "eventhandler/till/till.h"

#include  <math/lmath-tdma.h>
#include  <logging/cbm_logging.h>

namespace ldndc {

extern cbm::logger_t *  SoilChemistryMeTrXLogger;

/*!
 * @brief
 *  Biogeochemical model MeTrx
 */
class  LDNDC_API  SoilChemistryMeTrX  :  public  MBE_LegacyModel
{
    /***  module specific constants  ***/

    /*! Reference temperature [K] */
    static const double  TNORM;

    /*! Fractionation factor determining the share of decomposed nitrogen from different sources being mineralized to NH4 [-] */
    static const double  N_MINERALISATION_FRACTION;

    LMOD_EXPORT_MODULE_INFO(SoilChemistryMeTrX,"soilchemistry:metrx","Soilchemistry MeTrX");
public:
    SoilChemistryMeTrX(
                       MoBiLE_State *,
                       cbm::io_kcomm_t *,
                       timemode_e);

    ~SoilChemistryMeTrX();

    lerr_t  configure( ldndc::config_file_t const *);
    lerr_t  register_ports( cbm::io_kcomm_t *);
    lerr_t  initialize();

    lerr_t  solve();
    lerr_t  integrate();

    lerr_t  unregister_ports( cbm::io_kcomm_t *);
    lerr_t  finalize();

    lerr_t  sleep() { return  LDNDC_ERR_OK; }
    lerr_t  wake() { return  LDNDC_ERR_OK; }

private:
    cbm::io_kcomm_t *  io_kcomm;

    input_class_groundwater_t const *  gw_;
    input_class_siteparameters_t const &  sipar_;
    input_class_soillayers_t const &  sl_;
    input_class_setup_t const &  se_;

    substate_surfacebulk_t &  sb_;
    substate_airchemistry_t &  ac_;

    substate_microclimate_t const &  mc_;
    substate_physiology_t &  ph_;
    substate_soilchemistry_t &  sc_;
    substate_watercycle_t &  wc_;

    MoBiLE_PlantVegetation *  m_veg;

    SubscribedEvent<LD_EventHandlerQueue>  m_FertilizeEvents;
    SubscribedEvent<LD_EventHandlerQueue>  m_ManureEvents;
    SubscribedEvent<LD_EventHandlerQueue>  m_PlantEvents;

    EventQueue  m_TillEvents;
    CBM_Handle  m_TillHandle;

    Transport transport_;
    ControlledReleaseNitrogenFertilizer crnf_;
    
    timemode_e const timemode_;

    OutputSoilchemistryMeTrX  output_writer_;
    ldndc_flt64_t *  output_data_;

    unsigned int const TOTAL_TIME_STEPS;
    unsigned int const INTERNAL_TIME_STEPS;
    double const FTS_TOT_;

    /* Molar weights scaled from [g mol-1] to [kg mol-1] */

    double const M_C_SCALE;                                         // Molar weigth of C [kg mol-1]
    double const M_2C_SCALE;                                        // Molar weigth of C [kg mol-1]
    double const M_N_SCALE;                                         // Molar weigth of N [kg mol-1]
    double const M_2H_SCALE;                                        // Molar weigth of H2 [kg mol-1]
    double const M_2N_SCALE;                                        // Molar weigth of N2 [kg mol-1]
    double const M_O_SCALE;                                         // Molar weigth of O2 [kg mol-1]
    double const M_2O_SCALE;                                        // Molar weight of SO4 in [kg mol-1]
    double const M_FE_SCALE;                                        // Molar weight of Fe in [kg mol-1]

    double const N_O2_RATIO_NITRIFICATION;
    double const ACETATE_RATIO_FERM_SYN;                            // Ratio of acetate production from glucose via fermentative/syntrophic pathway.    [-]

    double const H2_MOL_CH4_KG_CONVERSION;                          // Converts from mol H2 consumed for CH4 production to kg CO2 needed
    double const H2_KG_CH4_KG_CONVERSION;                           // Converts from kg H2 consumed for CH4 production to kg CO2 needed
    double const H2_KG_CH4_MOL_CONVERSION;                          // Converts from kg H2 consumed for CH4 production to mol CO2 needed

    double const FE3_ACETATE_REDUCTION_RATIO;                       // CHCOO(-) + 8Fe(3+) + 4 H2O -> 2 HCO3(-) + 8 Fe(2+) + 9 H(+)    (Brock Biology of Microorganisms)
    double const FE3_HYDROGEN_REDUCTION_RATIO;                      // H2 + 2 Fe(3+) + 4 H+ -> 2 Fe2+ + 6 H2O

    double const O2_CONVERSION_FOR_CH4_OX;

    double const NO3_MOLAR_MAX_FE_RED;                              // NO3-threshold concentration. No Fe3-reduction possible for no3 > NO3_MOLAR_MAX_FE_RED.    [mol m-3]
    double const FE3_MOLAR_MAX_CH4_PROD;                            // Fe3-threshold concentration. No CH4-production possible for fe3 > FE3_MOLAR_MAX_CH4_PROD.    [mol m-3]
    double const O2_FE2_RATIO;                                      // 2 Fe(2+) + 0.5 O2 + 2 H(+) -> 2 Fe(3+) + H2O

    double const MICRO_C_MIN;

    double const MC_MO2_RATIO;                                      // Factor that transforms O2-consumption to CO2-production
    double const RGAS_M_O2_BAR_PA;                                  // Conversion factor for anaerobic volume calculation.

    double const K_DC_AORG;
    double const K_HU_AORG_HUM1;
    double const K_HU_AORG_HUM2;

    double const K_DC_SOL;
    double const K_HU_SOL;
    double const K_HU_DOC;

    double const K_DC_CEL;
    double const K_HU_CEL;

    double const K_DC_LIG;
    double const K_HU_LIG;

    double const K_DC_HUM1;
    double const K_HU_HUM1;

    double const K_DC_HUM2;
    double const K_HU_HUM2;

    double const K_DC_HUM3;
    double const KR_DENIT_CHEMO;

    double const K_FRAG_STUBBLE;
    double const K_FRAG_ALGAE;
    double const K_FRAG_LITTER_ABOVE;
    double const K_FRAG_LITTER_BELOW;
    double const K_FRAG_WOOD_ABOVE;
    double const K_FRAG_WOOD_BELOW;

    double const KMM_O2_ROOT_RESPIRATION;

    double const MUE_MAX_C_MICRO_1;                                    // Maximal microbial growth rate for mic_1.    [kg C timestep-1]
    double const MUE_MAX_C_MICRO_2;                                    // Maximal microbial growth rate for mic_2.    [kg C timestep-1]
    double const MUE_MAX_C_MICRO_3;                                    // Maximal microbial growth rate for mic_3.    [kg C timestep-1]

    double const METRX_MUEMAX_C_CH4_PROD;
    double const METRX_MUEMAX_H2_CH4_PROD;
    double const METRX_MUEMAX_C_CH4_OX;

    double const METRX_MUEMAX_C_FE_RED;
    double const METRX_KR_OX_FE;

    double const MUE_MAX_N_ASSI;

    double const PH_SURFACE_WATER;
    double const PH_MAX;

    double const A_MAX_ALGAE;
    double const A_MAX_MICRO_1;
    double const A_MAX_MICRO_2;
    double const A_MAX_MICRO_3;

    double const METRX_F_DENIT_N2_MIN;
    double const METRX_F_DENIT_N2_DELTA;
    
    double const D_NO_WATER_SCALE;
    double const D_N2O_WATER_SCALE;
    double const D_O2_WATER_SCALE;
    double const D_CO2_WATER_SCALE;
    double const D_CH4_WATER_SCALE;
    double const D_NH3_WATER_SCALE;
    double const D_NH4_WATER_SCALE;
    double const D_NO3_WATER_SCALE;
    double const D_NO2_WATER_SCALE;
    double const D_DOC_WATER_SCALE;

    double const D_O2_AIR_SCALE;
    double const D_N2_AIR_SCALE;
    double const D_N2O_AIR_SCALE;
    double const D_NO_AIR_SCALE;
    double const D_CH4_AIR_SCALE;
    double const D_CO2_AIR_SCALE;
    double const D_NH3_AIR_SCALE;

    double const METRX_V_EBULLITION;
    double const GROUNDWATER_NUTRIENT_ACCESS_RATE;

    size_t SL_ATMOSPHERIC_CONTACT;                                  // number of soil layers that are in direct contact with atmosphere
    size_t SL_SURFACE_DISTRIBUTION;                                 // number of soil layers for surface matter allocation to soil domaine
    double SL_SURFACE_DISTRIBUTION_INVERSE;                         // inverse of SL_SURFACE_DISTRIBUTION

    //wind speed
    double const *mc_wind;

    // soil temperature
    double const *mc_temp_atm;
    lvector_t< double > const &mc_temp_sl;

    // water flux
    double *waterflux_sl;
    double *accumulated_waterflux_old_sl;

    // infiltration flux
    double infiltration;
    double accumulated_infiltration_old;

private:

    PublishedField<double>  StubbleCarbon;

    /*****/
    /*   */
    /*****/

    bool have_algae;
    bool have_dry_wet;
    bool have_freeze_thaw;
    bool have_surface_bulk;
    bool have_canopy_transport;
    bool have_river_connection;
    bool have_no_litter_height_change;
    cbm::string_t d_eff_method;
    double spinup_delta_c;

    cbm::string_t anvf_method;

    bool have_plant_diffusion;
    bool have_water_table;                                                  // True for timespan with surface water

    double h_wl;                                                            // heigth of each watertable layer [m]

    double c_algae;
    double c_dead_algae;
    double n_algae;
    double n_dead_algae;

    double *o2_concentration;
    lvector_t< double > ts_co2_concentration_fl;
    lvector_t< double > ts_ch4_concentration_fl;
    lvector_t< double > ts_no_concentration_fl;
    lvector_t< double > nh3_fl;

    /*********************/
    /* balance variables */
    /*********************/

    double tot_c_balance_1;
    double tot_n_balance_1;

    /************************/
    /* daily rate variables */
    /************************/

    // Groundwater access
    double day_no3_groundwater_access;

    // Mineralisation
    double day_min_n_decomp;
    double day_min_n_aorg;
    double day_min_n_mic_1;
    double day_min_n_mic_2;
    double day_min_n_mic_3;

    // Assimilation
    double day_assi_n_mic_1;
    double day_assi_n_mic_2;
    double day_assi_n_mic_3;
    double day_assi_c_mic_1;
    double day_assi_c_mic_2;
    double day_assi_c_mic_3;

    // Nitrification
    double day_nit_nh4_no2;
    double day_nit_no2_no;
    double day_nit_no2_n2o;

    // Denitrification
    double day_denit_no3_no2;
    double day_denit_no2_no;
    double day_denit_no2_n2o;
    double day_denit_no2_n2;
    double day_denit_no_n2o;
    double day_denit_no_n2;
    double day_denit_n2o_n2;
    double day_chemodenit_no2_no;

    // Respiration / Fermentation / Methanogenesis
    double day_co2_prod_mic_1_growth;
    double day_co2_prod_mic_1_maintenance;
    double day_co2_prod_mic_2;
    double day_co2_prod_mic_3_acetate_prod;
    double day_co2_prod_mic_3_acetate_cons;
    double day_co2_prod_ch4_prod;
    double day_co2_prod_ch4_cons;

    // Leaching
    double day_leach_o2;
    double day_leach_n2o;
    double day_leach_n2;
    double day_leach_no;
    double day_leach_nh3;
    double day_leach_urea;

    // litter
    double accumulated_c_litter_above_old;
    double accumulated_c_litter_below_old;

    double dC_root_exsudates;
    double dC_litter_algae;

    // Decomposition
    double day_decomp_c_lit_1;
    double day_decomp_c_lit_2;
    double day_decomp_c_lit_3;
    double day_decomp_n_lit_1;
    double day_decomp_n_lit_2;
    double day_decomp_n_lit_3;
    lvector_t< double > day_decomp_c_hum_1_sl;
    lvector_t< double > day_decomp_c_hum_2_sl;
    lvector_t< double > day_decomp_c_hum_3_sl;
    lvector_t< double > day_decay_c_mic_sl;
    double day_decomp_n_hum_1;
    double day_decomp_n_hum_2;
    double day_decomp_n_hum_3;

    // Humification
    lvector_t< double > day_c_humify_doc_hum_1_sl;
    lvector_t< double > day_c_humify_sol_hum_1_sl;
    lvector_t< double > day_c_humify_cel_hum_1_sl;
    lvector_t< double > day_c_humify_lig_hum_1_sl;
    lvector_t< double > day_c_humify_lig_hum_2_sl;
    lvector_t< double > day_c_humify_mic_hum_1_sl;
    lvector_t< double > day_c_humify_mic_hum_2_sl;
    lvector_t< double > day_c_humify_hum_1_hum_2_sl;
    lvector_t< double > day_c_humify_hum_2_hum_3_sl;

    // Algae
    double day_n_fix_algae;
    double day_c_fix_algae;

    // DOC production
    double day_doc_prod_decomp_litter;      // daily doc production by litter decomposition
    double day_doc_prod_decomp_humus;       // daily doc production by humus decomposition
    double day_doc_prod_decomp_aorg;        // daily doc production by active organic material decomposition


    /*************************/
    /* Yearly rate variables */
    /*************************/

    // Groundwater access
    double year_no3_groundwater_access;

    // Humification
    lvector_t< double > year_c_humify_doc_hum_1_sl;
    lvector_t< double > year_c_humify_sol_hum_1_sl;
    lvector_t< double > year_c_humify_cel_hum_1_sl;
    lvector_t< double > year_c_humify_lig_hum_1_sl;
    lvector_t< double > year_c_humify_lig_hum_2_sl;
    lvector_t< double > year_c_humify_mic_hum_1_sl;
    lvector_t< double > year_c_humify_mic_hum_2_sl;
    lvector_t< double > year_c_humify_hum_1_hum_2_sl;
    lvector_t< double > year_c_humify_hum_2_hum_3_sl;

    lvector_t< double > year_c_decomp_hum_1_sl;
    lvector_t< double > year_c_decomp_hum_2_sl;
    lvector_t< double > year_c_decomp_hum_3_sl;

    lvector_t< double > year_doc_prod_sl;
    lvector_t< double > year_decay_c_mic_sl;
    lvector_t< double > year_co2_hetero_sl;

    // Decomposition
    double year_decomp_c_lit_1;
    double year_decomp_c_lit_2;
    double year_decomp_c_lit_3;
    double year_decomp_n_lit_1;
    double year_decomp_n_lit_2;
    double year_decomp_n_lit_3;
    double year_decomp_n_hum_1;
    double year_decomp_n_hum_2;
    double year_decomp_n_hum_3;

    // Methane
    double year_ch4_ox;

    // Algae
    double year_c_fix_algae;
    double year_n_fix_algae;

    // litter
    double accumulated_c_litter_above_last_year;
    double accumulated_c_litter_below_last_year;


    /*****************************/
    /* Litter C- and N-fractions */
    /*****************************/

    enum atm_eq_liq{ nh3_atm_eq_liq, o2_atm_eq_liq, co2_atm_eq_liq, ch4_atm_eq_liq, n2o_atm_eq_liq, no_atm_eq_liq };

    size_t subdaily_time_step_;

    // Respiration
    lvector_t< double >  co2_auto_sl;
    lvector_t< double >  co2_hetero_sl;

    lvector_t< double >  cn_hum_1_sl;
    lvector_t< double >  cn_hum_2_sl;
    lvector_t< double >  cn_hum_3_sl;

    lvector_t< double >  freeze_thaw_fact_sl;
    lvector_t< double >  dry_wet_fact_sl;
    lvector_t< double >  litter_type_fact_sl;

    // Iron, sulfate, ...
    lvector_t< double >  day_acetate_cons_fe3_sl;
    lvector_t< double >  day_h2_c_eq_cons_fe3_sl;
    lvector_t< double >  day_fe2_oxidation_sl;

    // Nitrification
    lvector_t< double >  day_nitrify_n2o_sl;
    lvector_t< double >  day_nit_no2_no3_sl;

    // Microbes
    lvector_t< double >  cn_opt_mic_sl;

    // Pool variables
    lvector_t< double >  h2_sl;

    lvector_t< double >  o2_liq_sl;
    lvector_t< double >  o2_gas_sl;

    lvector_t< double >  ae_acetate_sl;
    lvector_t< double >  an_acetate_sl;

    lvector_t< double >  ch4_liq_sl;
    lvector_t< double >  ch4_gas_sl;
    lvector_t< double >  co2_liq_sl;
    lvector_t< double >  co2_gas_sl;

    lvector_t< double >  nh4_ae_sl;
    lvector_t< double >  nh4_an_sl;

    lvector_t< double >  don_ae_sl;
    lvector_t< double >  don_an_sl;

    lvector_t< double >  no3_ae_sl;
    lvector_t< double >  no3_an_sl;

    lvector_t< double >  n2o_liq_sl;
    lvector_t< double >  n2o_gas_sl;
    lvector_t< double >  an_n2o_liq_sl;
    lvector_t< double >  an_n2o_gas_sl;
    lvector_t< double >  n2_gas_sl;

    lvector_t< double >  no_liq_sl;
    lvector_t< double >  no_gas_sl;
    lvector_t< double >  an_no_liq_sl;
    lvector_t< double >  an_no_gas_sl;

    lvector_t< double >  c_microbial_necromass_sl;                                        // carbon content of microbial necromass
    lvector_t< double >  n_microbial_necromass_sl;                                        // nitrogen content of microbial necromass

    lvector_t< double >  c_humus_1_sl;                                                    // carbon content of humus_0 pool
    lvector_t< double >  c_humus_2_sl;                                                    // carbon content of humus_1 pool
    lvector_t< double >  c_humus_3_sl;                                                    // carbon content of humus_2 pool

    lvector_t< double >  n_humus_1_sl;                                                    // nitrogen content of humus_0 pool
    lvector_t< double >  n_humus_2_sl;                                                    // nitrogen content of humus_1 pool
    lvector_t< double >  n_humus_3_sl;                                                    // nitrogen content of humus_2 pool

    lvector_t< double >  n_micro_1_sl;                                                    // nitrogen content of micro_1 pool
    lvector_t< double >  n_micro_2_sl;                                                    // nitrogen content of micro_2 pool
    lvector_t< double >  n_micro_3_sl;                                                    // nitrogen content of micro_3 pool


    // Diffusion
    lvector_t< double >  D_eff_air_fl;                                                    // Effective gas diffusion coefficient in atmosphere.
    lvector_t< double >  D_eff_air_sl;                                                    // Effective gas diffusion coefficient in soils after WARRICK.
    lvector_t< double >  D_eff_water_sl;
    lvector_t< double >  D_eff_dailyturbation_sl;
    lvector_t< double >  D_eff_plant_sl;                                                  // Effective O2 diffusion coefficient in plants

    lvector_t< double >  interface_air_sl;                                                // interface of air content between two soil layers
    lvector_t< double >  interface_delta_x_air_sl;                                        // interface of air content between two soil layers divided by distance
    lvector_t< double >  interface_delta_x_water_sl;                                      // interface of water content between two soil layers divided by distance
    lvector_t< double >  interface_delta_x_soil_sl;

    lvector_t< double >  delta_x;                                                         // interface of water content between two soil layers
    lvector_t< double >  fact_uc_xl;                                                      // unit conversion factor used for diffusion calculations

    lvector_t< double >  pore_connectivity_sl;                                            // connectivity of pores
    
    // Soil environment
    double  ph_delta_pab_wl;
    double  ph_delta_urea_wl;

    lvector_t< double >  ph_delta_urea_sl;

    lvector_t< double >  soil_gas_advection_sl;                                           // soil gas advection [m:s-1]
    lvector_t< double >  v_water_sl;                                                      // water volume of soillayer [m3]
    lvector_t< double >  v_air_sl;                                                        // air volume of soillayer [m3]
    lvector_t< double >  v_ice_sl;                                                        // ice volume of soillayer [m3]

    lvector_t< double >  communicate_sl;
    lvector_t< double >  day_denit_factor_c_sl;
    lvector_t< double >  day_denit_factor_n_sl;

    // Methanogenesis
    lvector_t< double >  day_ch4_production_hydrogen_sl;
    lvector_t< double >  day_ch4_production_acetate_sl;
    lvector_t< double >  day_ch4_oxidation_sl;                                            // Layerspecific daily CH4-Oxidation in soil.    [kg C-CH4 day-1]

    lvector_t< double >  day_ch4_bubbling_wl;                                             // Layerspecific daily CH4-bubbling in surface water.    [kg C-CH4 day-1]
    lvector_t< double >  day_ch4_bubbling_sl;                                             // Layerspecific daily CH4-bubbling in soil.    [kg C-CH4 day-1]

    lvector_t< double >  subdaily_ch4_bubbling;                                           // Subdaily CH4 emission rate via bubbling.    [kg C m-2 timestep-1]
    lvector_t< double >  subdaily_ch4_plant;                                              // Subdaily CH4 emission rate via plant diffusion.    [kg C m-2 timestep-1]
    lvector_t< double >  subdaily_ch4_soil;                                               // Subdaily CH4 emission rate via soil diffusion.    [kg C m-2 timestep-1]
    lvector_t< double >  subdaily_ch4_water;                                              // Subdaily CH4 emission rate via water table diffusion.    [kg C m-2 timestep-1]

    lvector_t< double >  subdaily_ch4_leach;                                              // Subdaily CH4 leaching rate.    [kg C m-2 timestep-1]
    lvector_t< double >  subdaily_ch4_prod;                                               // Subdaily CH4 production rate.    [kg C m-2 timestep-1]
    lvector_t< double >  subdaily_ch4_ox;                                                 // Subdaily CH4 oxidation rate.    [kg C m-2 timestep-1]
    lvector_t< double >  subdaily_ch4_content;                                            // Subdaily CH4 oxidation rate.    [kg C m-2 timestep-1]

    lvector_t< double >  subdaily_co2_bubbling;                                           // Subdaily CH4 emission rate via bubbling.    [kg C m-2 timestep-1]
    lvector_t< double >  subdaily_co2_plant;                                              // Subdaily CH4 emission rate via plant diffusion.    [kg C m-2 timestep-1]
    lvector_t< double >  subdaily_co2_soil;                                               // Subdaily CH4 emission rate via soil diffusion.    [kg C m-2 timestep-1]
    lvector_t< double >  subdaily_co2_water;                                              // Subdaily CH4 emission rate via water table diffusion.    [kg C m-2 timestep-1]
    lvector_t< double >  subdaily_co2_leach;                                              // Subdaily CH4 leaching rate.    [kg C m-2 timestep-1]

    // Others
    lvector_t< double >  subdaily_acetate_prod;
    lvector_t< double >  subdaily_doc_prod;

    lvector_t< double >  subdaily_o2_bubbling;
    lvector_t< double >  subdaily_o2_plant;
    lvector_t< double >  subdaily_o2_soil;
    lvector_t< double >  subdaily_o2_water;

    lvector_t< double >  subdaily_n2o_bubbling;
    lvector_t< double >  subdaily_n2o_plant;
    lvector_t< double >  subdaily_n2o_soil;
    lvector_t< double >  subdaily_n2o_water;

    lvector_t< double >  subdaily_no_bubbling;
    lvector_t< double >  subdaily_no_plant;
    lvector_t< double >  subdaily_no_soil;
    lvector_t< double >  subdaily_no_water;

    lvector_t< double >  subdaily_nh3_bubbling;
    lvector_t< double >  subdaily_nh3_plant;
    lvector_t< double >  subdaily_nh3_soil;
    lvector_t< double >  subdaily_nh3_water;

    lvector_t< double >  subdaily_plant_o2_cons;
    lvector_t< double >  subdaily_flood_o2_conc;
    lvector_t< double >  subdaily_algae_o2_prod;

    lvector_t< double >  day_doc_prod_sl;
    lvector_t< double >  day_acetate_prod_sl;
    lvector_t< double >  day_h2_c_eq_prod_sl;

    lvector_t< double >  plant_o2_consumption_sl;                                        // O2 consumption of oxygen per time step.    [kg O2 m-2]

    lvector_t< double >  accumulated_n_nh4_throughfall_sl;
    lvector_t< double >  accumulated_n_no3_throughfall_sl;
    lvector_t< double >  accumulated_n_nh4_throughfall_sbl;
    lvector_t< double >  accumulated_n_no3_throughfall_sbl;
    double accumulated_n_nh4_throughfall_old;
    double accumulated_n_no3_throughfall_old;

    // N by planting and fixation
    lvector_t< double >  accumulated_n_to_living_plant_and_algae_from_extern_sl;

    lvector_t< double >  accumulated_n_algae_nh4_uptake_sbl;
    lvector_t< double >  accumulated_n_algae_no3_uptake_sbl;
    lvector_t< double >  accumulated_n_algae_nh3_uptake_sbl;

    lvector_t< double >  accumulated_n_litter_from_plants_below_rawlitter_sl;
    lvector_t< double >  accumulated_n_litter_from_plants_below_wood_sl;
    double  accumulated_n_litter_from_plants_above_rawlitter;
    double  accumulated_n_litter_from_plants_above_wood;
    double  accumulated_n_litter_from_plants_above_stubble;
    lvector_t< double >  accumulated_n_aboveground_raw_litter_fragmentation_sl;
    lvector_t< double >  accumulated_n_belowground_raw_litter_fragmentation_sl;
    lvector_t< double >  accumulated_n_litter_from_algae_sl;
    lvector_t< double >  accumulated_n_litter_from_dung_sl;
    lvector_t< double >  accumulated_n_urea_from_dung_sl;

    double accumulated_n_export_harvest_cutting_grazing;
    double accumulated_n_export_harvest_old;
    double accumulated_c_litter_wood_above_old;

    lvector_t< double >  accumulated_n_nh4_fertilization_sl;
    lvector_t< double >  accumulated_n_no3_fertilization_sl;
    lvector_t< double >  accumulated_n_nh3_fertilization_sl;
    lvector_t< double >  accumulated_n_don_fertilization_sl;
    lvector_t< double >  accumulated_n_urea_fertilization_sl;
    lvector_t< double >  accumulated_n_litter_fertilization_sl;
    lvector_t< double >  accumulated_n_aorg_fertilization_sl;
    lvector_t< double >  accumulated_n_nh4_fertilization_sbl;
    lvector_t< double >  accumulated_n_no3_fertilization_sbl;
    lvector_t< double >  accumulated_n_nh3_fertilization_sbl;
    lvector_t< double >  accumulated_n_urea_fertilization_sbl;

    lvector_t< double >  accumulated_n_surface_litter_incorporation_via_tilling_sl;
    lvector_t< double >  accumulated_n_litter_tilling_sl;
    lvector_t< double >  accumulated_n_aorg_tilling_sl;
    lvector_t< double >  accumulated_n_humus_1_tilling_sl;
    lvector_t< double >  accumulated_n_humus_2_tilling_sl;
    lvector_t< double >  accumulated_n_humus_3_tilling_sl;
    lvector_t< double >  accumulated_n_no3_tilling_sl;
    lvector_t< double >  accumulated_n_no2_tilling_sl;
    lvector_t< double >  accumulated_n_no_tilling_sl;
    lvector_t< double >  accumulated_n_n2o_tilling_sl;
    lvector_t< double >  accumulated_n_urea_tilling_sl;
    lvector_t< double >  accumulated_n_don_tilling_sl;
    lvector_t< double >  accumulated_n_nh4_tilling_sl;
    lvector_t< double >  accumulated_n_nh3_tilling_sl;
    lvector_t< double >  accumulated_n_microbes_tilling_sl;

    lvector_t< double >  accumulated_n_humus_1_spinup_sl;
    lvector_t< double >  accumulated_n_humus_2_spinup_sl;
    lvector_t< double >  accumulated_n_humus_3_spinup_sl;
    lvector_t< double >  accumulated_n_surface_litter_spinup_sl;
    lvector_t< double >  accumulated_n_litter_spinup_sl;

    lvector_t< double >  accumulated_n_urea_nh4_hydrolysis_sl;
    lvector_t< double >  accumulated_n_urea_nh4_hydrolysis_sbl;

    lvector_t< double >  accumulated_n_nh4_assimilation_sl;
    lvector_t< double >  accumulated_n_no3_assimilation_sl;
    lvector_t< double >  accumulated_n_don_assimilation_sl;

    lvector_t< double >  accumulated_n_nh4_nh3_conversion_sl;
    lvector_t< double >  accumulated_n_nh3_nh4_conversion_sl;
    lvector_t< double >  accumulated_n_nh4_nh3_conversion_sbl;
    lvector_t< double >  accumulated_n_nh3_nh4_conversion_sbl;

    lvector_t< double >  accumulated_n_micro_leaching_sl;
    lvector_t< double >  accumulated_n_aorg_leaching_sl;
    lvector_t< double >  accumulated_n_litter_leaching_sl;
    lvector_t< double >  accumulated_n_humus_1_leaching_sl;
    lvector_t< double >  accumulated_n_humus_2_leaching_sl;
    lvector_t< double >  accumulated_n_humus_3_leaching_sl;
    lvector_t< double >  accumulated_n_nh4_leaching_sl;
    lvector_t< double >  accumulated_n_no3_leaching_sl;
    lvector_t< double >  accumulated_n_nh3_leaching_sl;
    lvector_t< double >  accumulated_n_don_leaching_sl;
    lvector_t< double >  accumulated_n_urea_leaching_sl;
    lvector_t< double >  accumulated_n_no_leaching_sl;
    lvector_t< double >  accumulated_n_n2o_leaching_sl;

    lvector_t< double >  accumulated_n_urea_liq_diffusion_sl;
    lvector_t< double >  accumulated_n_nh3_liq_diffusion_sl;
    lvector_t< double >  accumulated_n_nh4_liq_diffusion_sl;
    lvector_t< double >  accumulated_n_no3_liq_diffusion_sl;

    lvector_t< double >  accumulated_n_nh4_infiltration_phys_sl;
    lvector_t< double >  accumulated_n_nh3_infiltration_phys_sl;
    lvector_t< double >  accumulated_n_urea_infiltration_phys_sl;
    lvector_t< double >  accumulated_n_no3_infiltration_phys_sl;
    lvector_t< double >  accumulated_n_don_infiltration_phys_sl;
    lvector_t< double >  accumulated_n_aorg_infiltration_phys_sl;

    lvector_t< double >  accumulated_n_nh4_infiltration_leach_sl;
    lvector_t< double >  accumulated_n_nh3_infiltration_leach_sl;
    lvector_t< double >  accumulated_n_urea_infiltration_leach_sl;
    lvector_t< double >  accumulated_n_no3_infiltration_leach_sl;
    lvector_t< double >  accumulated_n_don_infiltration_leach_sl;
    lvector_t< double >  accumulated_n_no_infiltration_leach_sl;
    lvector_t< double >  accumulated_n_n2o_infiltration_leach_sl;

    lvector_t< double >  accumulated_n_nh4_infiltration_liqdif_sl;
    lvector_t< double >  accumulated_n_nh3_infiltration_liqdif_sl;
    lvector_t< double >  accumulated_n_urea_infiltration_liqdif_sl;
    lvector_t< double >  accumulated_n_no3_infiltration_liqdif_sl;

    lvector_t< double >  accumulated_n_nh3_bubbling_sl;
    lvector_t< double >  accumulated_n_no_bubbling_sl;
    lvector_t< double >  accumulated_n_n2o_bubbling_sl;
    lvector_t< double >  accumulated_n_nh3_bubbling_sbl;
    lvector_t< double >  accumulated_n_no_bubbling_sbl;
    lvector_t< double >  accumulated_n_n2o_bubbling_sbl;

    lvector_t< double >  accumulated_n_nh3_dissolution_sbl;
    lvector_t< double >  accumulated_n_no_dissolution_sbl;
    lvector_t< double >  accumulated_n_n2o_dissolution_sbl;

    lvector_t< double >  accumulated_n_nh3_phys_diffusion_sl;
    lvector_t< double >  accumulated_n_no_phys_diffusion_sl;
    lvector_t< double >  accumulated_n_n2o_phys_diffusion_sl;
    lvector_t< double >  accumulated_n_nh3_gas_diffusion_sl;
    lvector_t< double >  accumulated_n_no_gas_diffusion_sl;
    lvector_t< double >  accumulated_n_n2o_gas_diffusion_sl;

    lvector_t< double >  accumulated_n_micro_perturbation_sl;
    lvector_t< double >  accumulated_n_aorg_perturbation_sl;
    lvector_t< double >  accumulated_n_litter_perturbation_sl;
    lvector_t< double >  accumulated_n_humus_1_perturbation_sl;
    lvector_t< double >  accumulated_n_humus_2_perturbation_sl;
    lvector_t< double >  accumulated_n_humus_3_perturbation_sl;

    lvector_t< double >  accumulated_n_nh4_no2_nitrification_sl;
    lvector_t< double >  accumulated_n_nh4_no_nitrification_sl;
    lvector_t< double >  accumulated_n_nh4_n2o_nitrification_sl;
    lvector_t< double >  accumulated_n_no2_no3_nitrification_sl;

    lvector_t< double >  accumulated_n_no3_no2_denitrification_sl;
    lvector_t< double >  accumulated_n_no3_no_denitrification_sl;
    lvector_t< double >  accumulated_n_no3_n2o_denitrification_sl;
    lvector_t< double >  accumulated_n_no3_n2_denitrification_sl;
    lvector_t< double >  accumulated_n_no2_no_denitrification_sl;
    lvector_t< double >  accumulated_n_no2_n2o_denitrification_sl;
    lvector_t< double >  accumulated_n_no2_n2_denitrification_sl;
    lvector_t< double >  accumulated_n_no_n2o_denitrification_sl;
    lvector_t< double >  accumulated_n_no_n2_denitrification_sl;
    lvector_t< double >  accumulated_n_n2o_n2_denitrification_sl;

    lvector_t< double >  accumulated_n_no2_chemodenitrification_sl;

    lvector_t< double >  accumulated_n_mic_naorg_decay_sl;
    lvector_t< double >  accumulated_n_mic_nh4_mineral_sl;
    lvector_t< double >  accumulated_n_mic_don_dissolve_sl;

    lvector_t< double >  accumulated_n_aorg_nh4_mineral_sl;
    lvector_t< double >  accumulated_n_aorg_don_dissolve_sl;

    lvector_t< double >  accumulated_n_humus_1_don_dissolve_sl;
    lvector_t< double >  accumulated_n_humus_1_nh4_mineral_sl;
    lvector_t< double >  accumulated_n_humus_2_don_dissolve_sl;
    lvector_t< double >  accumulated_n_humus_2_nh4_mineral_sl;
    lvector_t< double >  accumulated_n_humus_3_don_dissolve_sl;
    lvector_t< double >  accumulated_n_humus_3_nh4_mineral_sl;

    lvector_t< double >  accumulated_n_litter_don_dissolve_sl;
    lvector_t< double >  accumulated_n_litter_nh4_mineral_sl;

    lvector_t< double >  accumulated_n_don_humus_1_humify_sl;
    lvector_t< double >  accumulated_n_don_humus_2_humify_sl;
    lvector_t< double >  accumulated_n_don_humus_3_humify_sl;
    lvector_t< double >  accumulated_n_litter_humus_1_humify_sl;
    
    lvector_t< double >  accumulated_c_root_exsudates_old_sl;

#ifdef  METRX_ANVF_TYPES
    lvector_t< double >  anvf_pnet_sl;
    lvector_t< double >  anvf_dndccan_sl;
    lvector_t< double >  anvf_nloss_sl;
#endif

    class SpinUp
    {
        public:

            SpinUp( size_t _y, size_t _sl_cnt) : spinup_years_( 0),
                                                 executed_months_( 0),
                                                 execute_( false),
                                                 humify_c_to_humus_1_sl( _sl_cnt, 0.0),
                                                 humify_c_to_humus_2_sl( _sl_cnt, 0.0),
                                                 humify_humus_1_to_humus_2_sl( _sl_cnt, 0.0),
                                                 humify_humus_2_to_humus_3_sl( _sl_cnt, 0.0),
                                                 decompose_hum_1( _sl_cnt, 0.0),
                                                 decompose_hum_2( _sl_cnt, 0.0),
                                                 decompose_hum_3( _sl_cnt, 0.0),
                                                 c_humus_1_temp_sl( _sl_cnt, 0.0),
                                                 c_humus_2_temp_sl( _sl_cnt, 0.0),
                                                 c_humus_3_temp_sl( _sl_cnt, 0.0),
                                                 cn_humus_2_temp_sl( _sl_cnt, 0.0),
                                                 cn_humus_3_temp_sl( _sl_cnt, 0.0),
                                                 exit_wood( 0.0),
                                                 enter_wood( 0.0)
            {
                this->set_spinup_years( _y);
            }

            ~SpinUp()
            {}

        public:

            void reset()
            {
                humify_c_to_humus_1_sl = 0.0;
                humify_c_to_humus_2_sl = 0.0;
                humify_humus_1_to_humus_2_sl = 0.0;
                humify_humus_2_to_humus_3_sl = 0.0;
                decompose_hum_1 = 0.0;
                decompose_hum_2 = 0.0;
                decompose_hum_3 = 0.0;
                exit_wood = 0.0;
                enter_wood = 0.0;
            }

            inline size_t spinup_months() const
            {
                return this->spinup_years_ * 12;
            }

            inline size_t spinup_years() const
            {
                return this->spinup_years_;
            }

            lerr_t stop_spinup()
            {
                execute_ = false;
                return LDNDC_ERR_OK;
            }

            void set_spinup_years( size_t _y)
            {
                this->spinup_years_ = _y;
                if ( this->spinup_years_ > 0)
                {
                    this->execute_ = true;
                }
            }

            size_t  simulated_years( cbm::sclock_t const *) const;

            bool  spinup_stage( cbm::sclock_t const *) const;

            bool  execute_spinup( cbm::sclock_t const *);


            size_t spinup_years_;
            size_t executed_months_;

        private:

            bool execute_;

        public:

            lvector_t< double >  humify_c_to_humus_1_sl;
            lvector_t< double >  humify_c_to_humus_2_sl;

            lvector_t< double >  humify_humus_1_to_humus_2_sl;
            lvector_t< double >  humify_humus_2_to_humus_3_sl;

            lvector_t< double >  decompose_hum_1;
            lvector_t< double >  decompose_hum_2;
            lvector_t< double >  decompose_hum_3;

            lvector_t< double >  c_humus_1_temp_sl;
            lvector_t< double >  c_humus_2_temp_sl;
            lvector_t< double >  c_humus_3_temp_sl;

            lvector_t< double >  cn_humus_2_temp_sl;
            lvector_t< double >  cn_humus_3_temp_sl;

            double  exit_wood;
            double  enter_wood;
    };

    SpinUp spinup_;


    /*****************/
    /***  methods  ***/
    /*****************/

    /*!
     * @param[in] None
     * @param[out] None
     * @return error code
     */
    lerr_t
    MeTrX_write_output();

    /*!
     * @param[in] None
     * @param[out] None
     * @return error code
     */
    lerr_t
    MeTrX_write_output_daily();

    /*!
     * @param[in] None
     * @param[out] None
     * @return error code
     */
    lerr_t
    MeTrX_write_output_yearly();

    /*!
     * @param[in] None
     * @param[out] None
     * @return error code
     */
    lerr_t
    MeTrX_write_output_layer_daily();

    /*!
     * @param[in] None
     * @param[out] None
     * @return error code
     */
    lerr_t
    MeTrX_write_output_layer_yearly();

    /*!
     * @param[in] None
     * @param[out] None
     * @return error code
     */
    lerr_t
    MeTrX_write_output_fluxes();

    /*!
     * @param[in] None
     * @param[out] None
     * @return error code
     */
    lerr_t
    MeTrX_write_output_pools();

    /*!
     * @param[in] None
     * @param[out] None
     * @return error code
     */
    lerr_t
    MeTrX_write_output_subdaily();

    /*!
     * @param[in] None
     * @param[out] None
     * @return error code
     */
    lerr_t
    MeTrX_write_rates();

    /*!
     * @param[in] None
     * @param[out] None
     * @return error code
     */
    lerr_t
    MeTrX_fertilize();

    /*!
     * @param[in] None
     * @param[out] None
     * @return error code
     */
    lerr_t
    MeTrX_manure();

    /*!
     * @param[in] None
     * @param[out] None
     * @return error code
     */
    lerr_t
    MeTrX_till();

    /*!
     * @param[in] None
     * @param[out] None
     * @return void
     */
    void
    MeTrX_reset();

    /*!
     * @param[in] None
     * @param[out] None
     * @return void
     */
    void
    MeTrX_reset_subdaily();

    /*!
     * @param[in] None
     * @param[out] None
     * @return void
     */
    void
    MeTrX_reset_daily();

    /*!
     * @param[in] None
     * @param[out] None
     * @return void
     */
    void
    MeTrX_reset_yearly();

    /*!
     * @param[in] None
     * @param[out] None
     * @return error code
     */
    lerr_t
    MeTrX_update();

    /*!
     * @brief
     *      iterates over complete soilchemistry state and checks
     *      for negative values
     */
    lerr_t
    MeTrX_check_for_negative_value(
                                   char const * /*name*/);

    /*!
     * @param[in] None
     * @param[out] None
     * @return void
     */
    lerr_t
    MeTrX_spinup();

    /*!
     * @brief
     */
    lerr_t
    MeTrX_balance_check(
                        unsigned int);

    /*!
     * @param[in] None
     * @param[out] None
     * @return error code
     */
    lerr_t
    MeTrX_physics();

    lerr_t
    MeTrX_advection();

    /*!
     * @param[in] None
     * @param[out] None
     * @return void
     */
    void
    MeTrX_leaching();

    /*!
     * @param[in] None
     * @param[out] None
     * @return void
     */
    void
    MeTrX_groundwater_access();

    /*!
     * @brief
     */
    lerr_t
    MeTrX_plant_respiration( size_t);
    
    double
    MeTrX_get_wfps(
                   size_t /* soil layer */);

    /*!
     * @brief
     */
    double
    MeTrX_get_wfps_eff(
                       unsigned int,
                       double const &);

    /*!
     * @param[in] None
     * @param[out] None
     * @return error code
     */
    lerr_t
    MeTrX_freeze_thaw();

    /*!
     * @param[in] None
     * @param[out] None
     * @return error code
     */
    lerr_t
    MeTrX_dry_wet();

    /*!
     * @param[in] None
     * @param[out] None
     * @return error code
     */
    lerr_t
    MeTrX_fragmentation();

    /*!
     * @brief
     */
    void
    MeTrX_litter_distribution(
                              double const &,
                              double const &,
                              double const &,
                              double const &,
                              double const &,
                              double const &,
                              CBM_Vector< double > &accumulated_litter_sl);

    /*!
     * @param[in] None
     * @param[out] None
     * @return error code
     */
    lerr_t
    MeTrX_gas_diffusion();

    void
    MeTrX_liq_diffusion_scale( double &_out, lvector_t< double > &_scale);

    /*!
     * @param[in] None
     * @param[out] None
     * @return void
     */
    void
    MeTrX_liq_diffusion();

    /*!
     * @param[in] None
     * @param[out] None
     * @return error code
     */
    lerr_t
    MeTrX_pertubation();

    /*!
     * @param[in] None
     * @param[out] None
     * @return error code
     */
    lerr_t
    MeTrX_metabolism();

    /*!
     * @param[in] None
     * @param[out] None
     * @return error code
     */
    lerr_t
    MeTrX_dissolution();

    void
    MeTrX_ebullition_transfer(
                              double &,
                              double &,
                              double ,
                              double );

    /*!
     * @param[in] None
     * @param[out] None
     * @return void
     */
    void
    MeTrX_ebullition();

    /*!
     * @param[in] None
     * @param[out] None
     * @return void
     */
    void
    MeTrX_iron_reduction();

    /*!
     * @param[in] None
     * @param[out] None
     * @return void
     */
    void
    MeTrX_iron_oxidation();

    /*!
     * @param[in] None
     * @param[out] None
     * @return void
     */
    void
    MeTrX_algae_dynamics();

    /*!
     * @param[in] None
     * @param[out] None
     * @return void
     */
    void
    MeTrX_ch4_production();

    /*!
     * @param[in] None
     * @param[out] None
     * @return void
     */
    void
    MeTrX_ch4_oxidation();

    /*!
     * @param[in] None
     * @param[out] None
     * @return void
     */
    void
    MeTrX_microbial_dynamics();

    /*!
     * @param[in] None
     * @param[out] None
     * @return void
     */
    void
    MeTrX_soil_organic_matter_turnover();

    /*!
     * @param[in] None
     * @param[out] None
     * @return error code
     */
    lerr_t
    MeTrX_fermentation();

    /*!
     * @brief
     */
    double
    MeTrX_get_mineral_efficiency(
                                 double,
                                 double);

    /*!
     * @brief
     */
    double
    MeTrX_get_micro_c_decay_max(
                                double &);

    /*!
     * @brief
     */
    double
    MeTrX_get_micro_n_decay_max(
                                double &);

    /*!
     * @brief
     */
    void
    MeTrX_dead_microbial_biomass_allocation(
                                            size_t /* soil layer */,
                                            double const & /* carbon transfer amount*/,
                                            double const & /* nitrogen transfer amount*/,
                                            double &,
                                            double &);

    /*!
     * @brief
     */
    void
    MeTrX_execute_c_growth(
                           size_t const &,
                           double ,
                           double const &,
                           double &,
                           double &);

    /*!
     * @brief
     */
    void
    MeTrX_execute_co2_prod(
                           size_t const &,
                           double const &,
                           double &,
                           double &);

    /*!
     * @brief
     */
    void
    MeTrX_execute_n_assimilation(
                                 double const &,
                                 size_t const &,
                                 bool const &,
                                 double &,
                                 double &,
                                 double &);

    /*!
     * @brief
     */
    double
    MeTrX_get_n_growth(
                       double const &,
                       double const &,
                       double const &,
                       double const &,
                       double const &);

    /*!
     * @param[in] None
     * @param[out] None
     * @return void
     */
    void
    MeTrX_urea_hydrolysis();

    /*!
     * @param[in] None
     * @param[out] None
     * @return void
     */
    void
    MeTrX_pH_calculation();

    /*!
     * @brief
     * Transforms nitrogen from slow release nitrogen fertilizer to nh4
     * @param[in] None
     * @param[out] None
     * @return void
     */
    lerr_t
    MeTrX_nitrogen_fertilizer_release();

    /*!
     * @param[in] None
     * @param[out] None
     * @return void
     */
    void
    MeTrX_nitrification();

    /*!
     * @param[in] None
     * @param[out] None
     * @return void
     */
    void
    MeTrX_denitrification();

    /*!
     * @param[in] None
     * @param[out] None
     * @return void
     */
    void
    MeTrX_chemodenitrification();

    /*!
     * @param[in] None
     * @param[out] None
     * @return void
     */
    void
    MeTrX_clay_nh4_equilibrium();

    /*!
     * @param[in] None
     * @param[out] None
     * @return void
     */
    void
    MeTrX_nh3_nh4_equilibrium();

    /*!
     * @param[in] None
     * @param[out] None
     * @return void
     */
    void
    MeTrX_anaerobic_volume();

    /*!
     * @brief
     */
    double
    MeTrX_get_ph_sl(
                 size_t /* soil layer */);

    /*!
     * @param[in] None
     * @param[out] None
     * @return double
     */
    double
    MeTrX_get_ph_wl();

    /*!
     * @brief
     */
    double
    MeTrX_get_fact_ph(
                      size_t,
                      double,
                      double);

    /*!
     * @param[in] _sl soil layer
     * @param[out] None
     * @return requested factor
     */
    double
    MeTrX_get_fact_tm_decomp( size_t const &);
    double
    MeTrX_get_fact_tm_mic( size_t const &);

    /*!
     * @brief
     */
    double
    MeTrX_get_fact_mm(
                      double const &,
                      double const &);

    /*!
     * @brief
     */
    double
    MeTrX_get_k_mm_x_sl(
                        size_t const &,
                        double const &);

    /*!
     * @param[in] None
     * @param[out] None
     * @return Total soil associated carbon
     */
    double
    MeTrX_get_c_tot();

    /*!
     * @param[in] None
     * @param[out] None
     * @return Total soil associated nitrogen
     */
    double
    MeTrX_get_n_tot();

    /*!
     * @brief
     */
    double
    MeTrX_get_atm_eq_liq(
                         atm_eq_liq const &,
                         double const &);

    /*!
     * @param[in] None
     * @param[out] None
     * @return error code
     */
    lerr_t
    MeTrX_receive_state();

    /*!
     * @param[in] None
     * @param[out] None
     * @return error code
     */
    lerr_t
    MeTrX_send_state();

    /*!
     * @param[in] _henry None
     * @param[in,out] _gas gaseous species
     * @param[in,out] _liq dissolved substance
     * @return error code
     */
    lerr_t
    MeTrX_dissolution_soil( double const &,
                            double &,
                            double &);
};

} /*namespace ldndc*/

#endif    /*  LM_SOILCHEMISTRYMETRX_H_  */
