/*!
 * @file
 * @author
 * - David Kraus (created on: may 12, 2014)
 *
 */

#include  "soilchemistry/metrx/soilchemistry-metrx.h"
#include  <input/soillayers/soillayers.h>
#include  <constants/cbm_const.h>

namespace ldndc {

lerr_t
SoilChemistryMeTrX::MeTrX_write_output()
{
    if ( !output_data_ || !this->output_writer_.active())
    {
        return LDNDC_ERR_OK;
    }

    if ( output_writer_.write_subdaily())
    {
        lerr_t  rc_out = MeTrX_write_output_subdaily();
        if ( rc_out){ return  LDNDC_ERR_FAIL; }
    }

    if ( output_writer_.write_fluxes())
    {
        lerr_t  rc_out = MeTrX_write_output_fluxes();
        if ( rc_out){ return  LDNDC_ERR_FAIL; }
    }

    if ( lclock()->is_position( TMODE_POST_DAILY))
    {
        if ( output_writer_.write_pools())
        {
            lerr_t  rc_out = MeTrX_write_output_pools();
            if ( rc_out){ return  LDNDC_ERR_FAIL; }
        }

        if ( output_writer_.write_daily())
        {
            lerr_t  rc_out = MeTrX_write_output_daily();
            if ( rc_out){ return  LDNDC_ERR_FAIL; }
        }

        if ( output_writer_.write_layer_daily())
        {
            lerr_t  rc_out = MeTrX_write_output_layer_daily();
            if ( rc_out){ return  LDNDC_ERR_FAIL; }
        }
    }

    if ( lclock()->is_position( TMODE_POST_YEARLY))
    {
        if ( output_writer_.write_yearly())
        {
            lerr_t  rc_out = MeTrX_write_output_yearly();
            if ( rc_out){ return  LDNDC_ERR_FAIL; }
        }

        if ( output_writer_.write_layer_yearly())
        {
            lerr_t  rc_out = MeTrX_write_output_layer_yearly();
            if ( rc_out){ return  LDNDC_ERR_FAIL; }
        }
    }

    return  LDNDC_ERR_OK;
}

lerr_t
SoilChemistryMeTrX::MeTrX_write_output_daily()
{
#define  METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(__val__)  LDNDC_OUTPUT_SET_COLUMN_((__val__),=,output_data_,1)
    LDNDC_OUTPUT_SET_COLUMN_INDEX(0);

    double const clit1( sc_.C_lit1_sl.sum() + sc_.c_raw_lit_1_sl.sum());
    double const clit2( sc_.C_lit2_sl.sum() + sc_.c_raw_lit_2_sl.sum());
    double const clit3( sc_.C_lit3_sl.sum() + sc_.c_raw_lit_3_sl.sum());
    double const nlit1( sc_.N_lit1_sl.sum() + sc_.n_raw_lit_1_sl.sum());
    double const nlit2( sc_.N_lit2_sl.sum() + sc_.n_raw_lit_2_sl.sum());
    double const nlit3( sc_.N_lit3_sl.sum() + sc_.n_raw_lit_3_sl.sum());

    double const cmic1( sc_.C_micro1_sl.sum());
    double const cmic2( sc_.C_micro2_sl.sum());
    double const cmic3( sc_.C_micro3_sl.sum());
    double const nmic1( n_micro_1_sl.sum());
    double const nmic2( n_micro_2_sl.sum());
    double const nmic3( n_micro_3_sl.sum());

    double const cstub( sc_.c_stubble_lit1+sc_.c_stubble_lit2+sc_.c_stubble_lit3);
    double const nstub( sc_.n_stubble_lit1+sc_.n_stubble_lit2+sc_.n_stubble_lit3);

    double const chum1( c_humus_1_sl.sum());
    double const chum2( c_humus_2_sl.sum());
    double const chum3( c_humus_3_sl.sum());
    double const nhum1( n_humus_1_sl.sum());
    double const nhum2( n_humus_2_sl.sum());
    double const nhum3( n_humus_3_sl.sum());

    double const caorg( sc_.C_aorg_sl.sum());
    double const naorg( sc_.N_aorg_sl.sum());

    double const cwood( sc_.c_wood_sl.sum()+sc_.c_wood);
    double const nwood( sc_.n_wood_sl.sum()+sc_.n_wood);

    double const n_algae_tot( n_algae + n_dead_algae);
    double const don_sum( don_ae_sl.sum()+don_an_sl.sum()+sb_.don_sbl.sum());
    double const nh4_sum( nh4_ae_sl.sum()+nh4_an_sl.sum()+sb_.nh4_sbl.sum());
    double const nh4_clay_sum( sc_.clay_nh4_sl.sum());
    double const coated_nh4_sum( sc_.coated_nh4_sl.sum());
    double const urea_sum( sc_.urea_sl.sum()+sb_.urea_sbl.sum());
    double const no2_sum( sc_.no2_sl.sum());
    double const an_no2_sum( sc_.an_no2_sl.sum());
    double const no3_sum( no3_ae_sl.sum());
    double const an_no3_sum( no3_an_sl.sum()+sb_.no3_sbl.sum());
    double const n2o_sum( n2o_gas_sl.sum()+n2o_liq_sl.sum());
    double const an_n2o_sum( an_n2o_gas_sl.sum()+an_n2o_liq_sl.sum());
    double const no_sum( no_gas_sl.sum()+no_liq_sl.sum());
    double const an_no_sum( an_no_gas_sl.sum()+an_no_liq_sl.sum());
    double const nh3_gas_sum( sc_.nh3_gas_sl.sum());
    double const nh3_liq_sum( sc_.nh3_liq_sl.sum()+sb_.nh3_sbl.sum());

    double const nsol( nh4_sum + nh4_clay_sum + don_sum
                      + nh3_gas_sum + nh3_liq_sum + urea_sum
                      + no3_sum + an_no3_sum + no2_sum + an_no2_sum
                      + n2o_sum + an_n2o_sum + no_sum + an_no_sum);

    double const c_algae_tot( c_algae + c_dead_algae);
    double const doc_sum( sc_.doc_sl.sum());
    double const an_doc_sum( sc_.an_doc_sl.sum());
    double const acetate_sum( ae_acetate_sl.sum() + an_acetate_sl.sum());
    double const ch4_gas_sum( ch4_gas_sl.sum());
    double const ch4_liq_sum( ch4_liq_sl.sum());

    double const csol( doc_sum + an_doc_sum + acetate_sum + ch4_gas_sum + ch4_liq_sum);

    double soc20( 0.0);
    double soc40( 0.0);
    double totn20( 0.0);
    double totn40( 0.0);
    double soilmass20( 0.0);
    double soilmass40( 0.0);
    double anvf_avg( 0.0);
#ifdef  METRX_ANVF_TYPES
    double anvf_avg_pnet( 0.0);
    double anvf_avg_dndc_can( 0.0);
    double anvf_avg_nloss( 0.0);
#endif
    double pore_connect_avg( 0.0);
    double d_eff_avg( 0.0);
    double wfps_avg( 0.0);
    double air_avg( 0.0);
    double root_conduct_avg( 0.0);
    double o2_sum( 0.0);
    double depth_sum( 0.0);

    double nitrification_inhibition( 0.0);
    double urease_inhibition( 0.0);

    double mineral_efficiency_scale_sum( 0.0);
    double mineral_efficiency( 0.0);
    for( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
    {
        double const c_mic_sum( sc_.C_micro1_sl[sl] + sc_.C_micro2_sl[sl] + sc_.C_micro3_sl[sl]);
        double const n_mic_sum( n_micro_1_sl[sl] + n_micro_2_sl[sl] + n_micro_3_sl[sl]);
        mineral_efficiency += MeTrX_get_mineral_efficiency( c_mic_sum / n_mic_sum, cn_opt_mic_sl[sl]) * c_mic_sum;
        mineral_efficiency_scale_sum += c_mic_sum;

        anvf_avg += sc_.anvf_sl[sl] * sc_.h_sl[sl];
#ifdef  METRX_ANVF_TYPES
        anvf_avg_pnet  += anvf_pnet_sl[sl] * sc_.h_sl[sl];
        anvf_avg_dndc_can  += anvf_dndccan_sl[sl] * sc_.h_sl[sl];
        anvf_avg_nloss  += anvf_nloss_sl[sl] * sc_.h_sl[sl];
#endif
        pore_connect_avg += pore_connectivity_sl[sl] * sc_.h_sl[sl];
        d_eff_avg += D_eff_air_sl[sl] * sc_.h_sl[sl];
        wfps_avg += MeTrX_get_wfps( sl) * sc_.h_sl[sl];
        air_avg += (v_air_sl[sl] / sc_.h_sl[sl]) * sc_.h_sl[sl];
        root_conduct_avg += D_eff_plant_sl[sl] * sc_.h_sl[sl];
        o2_sum += o2_gas_sl[sl] + o2_liq_sl[sl];
        depth_sum += sc_.h_sl[sl];

        if ( cbm::flt_less_equal( sc_.depth_sl[sl], 0.2))
        {
            soc20 += (sc_.C_aorg_sl[sl] +c_humus_1_sl[sl]+c_humus_2_sl[sl]+c_humus_3_sl[sl]
                      +sc_.C_micro1_sl[sl]+sc_.C_micro2_sl[sl]+sc_.C_micro3_sl[sl]
                      +sc_.C_lit1_sl[sl]+sc_.C_lit2_sl[sl]+sc_.C_lit3_sl[sl]);
            totn20 += (sc_.N_aorg_sl[sl] +n_humus_1_sl[sl]+n_humus_2_sl[sl]+n_humus_3_sl[sl]
                       +sc_.N_micro_sl[sl] +sc_.N_lit1_sl[sl]+sc_.N_lit2_sl[sl]+sc_.N_lit3_sl[sl]);
            soilmass20 += (sc_.dens_sl[sl] * cbm::DM3_IN_M3 * sc_.h_sl[sl]);
        }
        
        if ( cbm::flt_less_equal( sc_.depth_sl[sl], 0.4))
        {
            soc40 += (sc_.C_aorg_sl[sl] +c_humus_1_sl[sl]+c_humus_2_sl[sl]+c_humus_3_sl[sl]
                      +sc_.C_micro1_sl[sl]+sc_.C_micro2_sl[sl]+sc_.C_micro3_sl[sl]
                      +sc_.C_lit1_sl[sl]+sc_.C_lit2_sl[sl]+sc_.C_lit3_sl[sl]);
            totn40 += (sc_.N_aorg_sl[sl] +n_humus_1_sl[sl]+n_humus_2_sl[sl]+n_humus_3_sl[sl]
                       +sc_.N_micro_sl[sl] +sc_.N_lit1_sl[sl]+sc_.N_lit2_sl[sl]+sc_.N_lit3_sl[sl]);
            soilmass40 += (sc_.dens_sl[sl] * cbm::DM3_IN_M3 * sc_.h_sl[sl]);
        }
    }

    int sl_counter( 0);
    for( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
    {
        double const ni_inhibition( crnf_.get_nitrification_inhibition( sc_.ni_sl[sl]));
        if ( cbm::flt_greater_zero( ni_inhibition))
        {
            sl_counter += 1;
            nitrification_inhibition += ni_inhibition;
        }
    }

    if ( sl_counter > 0)
    {
        nitrification_inhibition /= sl_counter;
        urease_inhibition /= sl_counter;
    }

    double const litter_height( (sl_.soil_layers_in_litter_cnt() > 0) ? sc_.depth_sl[sl_.soil_layers_in_litter_cnt()-1] : 0.0);
    anvf_avg /= depth_sum;
#ifdef  METRX_ANVF_TYPES
    anvf_avg_pnet /= depth_sum;
    anvf_avg_dndc_can /= depth_sum;
    anvf_avg_nloss /= depth_sum;
#endif
    pore_connect_avg /= depth_sum;
    d_eff_avg /= depth_sum;
    wfps_avg /= depth_sum;
    air_avg /= depth_sum;
    root_conduct_avg /= depth_sum;

    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( anvf_avg);
#ifdef  METRX_ANVF_TYPES
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( anvf_avg_pnet);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( anvf_avg_dndc_can);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( anvf_avg_nloss);
#endif
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( pore_connect_avg);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( d_eff_avg);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( wfps_avg);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( air_avg);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( root_conduct_avg);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( o2_sum * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( crnf_.get_crnf_teff_mean());
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( crnf_.get_crnf_release_fraction_mean());
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( nitrification_inhibition);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( urease_inhibition);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( mineral_efficiency / (mineral_efficiency_scale_sum * sl_.soil_layer_cnt()));
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( don_sum * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( nh4_sum * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( nh4_clay_sum * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( coated_nh4_sum * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( urea_sum * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( no2_sum * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( an_no2_sum * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( no3_sum * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( an_no3_sum * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( n2o_sum * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( an_n2o_sum * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( no_sum * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( an_no_sum * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( nh3_gas_sum * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( nh3_liq_sum * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( doc_sum * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( an_doc_sum * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( acetate_sum * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( ch4_gas_sum * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( ch4_liq_sum * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( caorg * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( naorg * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( cmic1 * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( cmic2 * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( cmic3 * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( nmic1 * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( nmic2 * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( nmic3 * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( chum1 * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( chum2 * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( chum3 * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( nhum1 * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( nhum2 * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( nhum3 * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( clit1 * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( clit2 * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( clit3 * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( nlit1 * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( nlit2 * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( nlit3 * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( c_algae_tot * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( n_algae_tot * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( cstub * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( nstub * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( (cwood+caorg+cmic1+cmic2+cmic3+chum1+chum2+chum3+clit1+clit2+clit3+c_algae+cstub+csol) * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( (nwood+naorg+nmic1+nmic2+nmic3+nhum1+nhum2+nhum3+nlit1+nlit2+nlit3+n_algae+nstub+nsol) * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( soc20 / soilmass20);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( soc40 / soilmass40);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( totn20 / soilmass20);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( totn40 / soilmass40);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( sc_.fe2_sl.sum() * cbm::M2_IN_HA/FE3_ACETATE_REDUCTION_RATIO);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( sc_.fe3_sl.sum() * cbm::M2_IN_HA/FE3_ACETATE_REDUCTION_RATIO);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( sc_.till_effect_sl.sum() / sl_.soil_layer_cnt());
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( freeze_thaw_fact_sl.sum());
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( litter_height);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( MeTrX_get_ph_wl());
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( sc_.ph_sl[0]);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( day_no3_groundwater_access * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( (day_assi_n_mic_1+day_assi_n_mic_2+day_assi_n_mic_3) * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( day_nit_nh4_no2 * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( day_nit_no2_no3_sl.sum() * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( day_nit_no2_no * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( day_nit_no2_n2o * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( day_denit_no3_no2 * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( day_denit_no2_no * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( day_denit_no2_n2o * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( day_denit_no2_n2 * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( day_denit_no_n2o * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( day_denit_no_n2 * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( day_denit_n2o_n2 * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( day_chemodenit_no2_no * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( day_ch4_oxidation_sl.sum() * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( day_ch4_production_hydrogen_sl.sum() * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( day_ch4_production_acetate_sl.sum() * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( subdaily_ch4_plant.sum() * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( subdaily_ch4_soil.sum() * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( subdaily_ch4_water.sum() * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( subdaily_ch4_bubbling.sum() * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( subdaily_ch4_leach.sum() * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( subdaily_o2_plant.sum() * cbm::M2_IN_HA);

    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( (sc_.accumulated_c_litter_stubble + sc_.accumulated_c_litter_wood_above - accumulated_c_litter_above_old) * cbm::M2_IN_HA);
    accumulated_c_litter_above_old = sc_.accumulated_c_litter_stubble + sc_.accumulated_c_litter_wood_above;

    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( (sc_.accumulated_c_litter_below_sl.sum() + sc_.accumulated_c_litter_wood_below_sl.sum() - accumulated_c_litter_below_old) * cbm::M2_IN_HA);
    accumulated_c_litter_below_old = sc_.accumulated_c_litter_below_sl.sum() + sc_.accumulated_c_litter_wood_below_sl.sum();

    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( day_c_fix_algae * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( day_n_fix_algae * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( (day_decomp_c_lit_1+day_decomp_c_lit_2+day_decomp_c_lit_3) * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( day_decay_c_mic_sl.sum()*cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( day_co2_prod_mic_1_growth * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( day_co2_prod_mic_1_maintenance * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( day_co2_prod_mic_2 * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( day_co2_prod_mic_3_acetate_prod * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( day_co2_prod_mic_3_acetate_cons * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( day_co2_prod_ch4_prod * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( day_co2_prod_ch4_cons * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( day_h2_c_eq_prod_sl.sum() * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( day_acetate_prod_sl.sum() * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( day_acetate_cons_fe3_sl.sum() * cbm::M2_IN_HA/FE3_ACETATE_REDUCTION_RATIO);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( day_h2_c_eq_cons_fe3_sl.sum() * cbm::M2_IN_HA/FE3_ACETATE_REDUCTION_RATIO);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( day_doc_prod_decomp_litter * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( day_doc_prod_decomp_humus * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( day_doc_prod_decomp_aorg * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( dC_root_exsudates * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( day_doc_prod_sl.sum() * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( day_c_humify_mic_hum_2_sl.sum() * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( day_c_humify_hum_1_hum_2_sl.sum() * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( day_c_humify_hum_2_hum_3_sl.sum() * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( (day_leach_n2o+day_leach_no+day_leach_nh3+day_leach_urea) * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( (subdaily_ch4_leach.sum()) * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( year_no3_groundwater_access * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( day_min_n_decomp * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( day_min_n_aorg * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( day_min_n_mic_1 * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( day_min_n_mic_2 * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( day_min_n_mic_3 * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( day_assi_c_mic_1 * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( day_assi_c_mic_2 * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( day_assi_c_mic_3 * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( year_decomp_c_lit_1 * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( year_decomp_c_lit_2 * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( year_decomp_c_lit_3 * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( year_decomp_n_lit_1 * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( year_decomp_n_lit_2 * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( year_decomp_n_lit_3 * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( year_c_decomp_hum_1_sl.sum() * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( year_c_decomp_hum_2_sl.sum() * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( year_c_decomp_hum_3_sl.sum() * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( year_decomp_n_hum_1 * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( year_decomp_n_hum_2 * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( year_decomp_n_hum_3 * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( year_c_humify_doc_hum_1_sl.sum() * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( year_c_humify_sol_hum_1_sl.sum() * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( year_c_humify_cel_hum_1_sl.sum() * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( year_c_humify_lig_hum_1_sl.sum() * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( year_c_humify_mic_hum_1_sl.sum() * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( year_c_humify_lig_hum_2_sl.sum() * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( year_c_humify_mic_hum_2_sl.sum() * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( year_c_humify_hum_1_hum_2_sl.sum() * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( year_c_humify_hum_2_hum_3_sl.sum() * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( year_c_fix_algae * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( year_n_fix_algae * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( (sc_.accumulated_c_litter_stubble + sc_.accumulated_c_litter_above + sc_.accumulated_c_litter_wood_above - accumulated_c_litter_above_last_year) * cbm::M2_IN_HA);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( (sc_.accumulated_c_litter_wood_below_sl.sum() + sc_.accumulated_c_litter_below_sl.sum() - accumulated_c_litter_below_last_year) * cbm::M2_IN_HA);

    return  output_writer_.write_daily( output_data_);
}



lerr_t
SoilChemistryMeTrX::MeTrX_write_output_yearly()
{
    double surface_litter( c_algae + c_dead_algae + sc_.c_stubble_lit1 + sc_.c_stubble_lit2 + sc_.c_stubble_lit3);
    double soc20( 0.0);
    double c_hum_1_20( 0.0);
    double c_hum_2_20( 0.0);
    double c_hum_3_20( 0.0);
    double c_litter_20( 0.0);
    double c_aorg_20( 0.0);
    for( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
    {
        if ( cbm::flt_less_equal( sc_.depth_sl[sl], 0.2))
        {
            soc20 += sc_.C_aorg_sl[sl] + c_humus_1_sl[sl] + c_humus_2_sl[sl] + c_humus_3_sl[sl]
                   + sc_.C_micro1_sl[sl] + sc_.C_micro2_sl[sl] + sc_.C_micro3_sl[sl]
                   + sc_.C_lit1_sl[sl] + sc_.C_lit2_sl[sl] + sc_.C_lit3_sl[sl];
            c_hum_1_20 += c_humus_1_sl[sl];
            c_hum_2_20 += c_humus_2_sl[sl];
            c_hum_3_20 += c_humus_3_sl[sl];
            c_litter_20 += sc_.C_lit1_sl[sl] + sc_.C_lit2_sl[sl] + sc_.C_lit3_sl[sl];
            c_aorg_20 += sc_.C_aorg_sl[sl];
        }
        else
        {
            break;
        }
    }

#define  METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(__val__)  LDNDC_OUTPUT_SET_COLUMN_((__val__),=,output_data_,1)
    LDNDC_OUTPUT_SET_COLUMN_INDEX(0);

    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( surface_litter);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( soc20);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( c_hum_1_20);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( c_hum_2_20);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( c_hum_3_20);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( c_litter_20);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( c_aorg_20);

    return  output_writer_.write_yearly( output_data_);
}



lerr_t
SoilChemistryMeTrX::MeTrX_write_output_layer_daily()
{
    size_t fl_cnt( m_veg->canopy_layers_used());
    size_t sbl_cnt( sb_.surfacebulk_layer_cnt());

    for ( size_t  fl = fl_cnt;  fl > 0;  --fl)
    {
        size_t l( fl-1);
        LDNDC_OUTPUT_SET_COLUMN_INDEX(0);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( 0.0); //depth
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( 0.0); //extension
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( 0.0); //pore_connectivity
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( D_eff_air_fl[l]); //air permeability
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( 0.0); //aerenchym_permeability
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( 0.0); //water_content
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( 0.0); //air_content
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( 0.0); //porosity
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( 0.0); //fe2
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( 0.0); //fe3
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( ts_co2_concentration_fl[l]*cbm::M2_IN_HA);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( ts_ch4_concentration_fl[l]*cbm::M2_IN_HA);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( 0.0); //ch4 bubbling
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( 0.0); //ch4 production
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( 0.0); //ch4 oxidation
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( 0.0); //doc production
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( 0.0); //acetate production
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( 0.0); //mic decay
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( 0.0); //doc
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( 0.0); //acetate
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( 0.0); //no3
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( 0.0); //nh4
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( 0.0); //urea
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( nh3_fl[l]*cbm::M2_IN_HA);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( 0.0); //o2
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( ts_no_concentration_fl[l]*cbm::M2_IN_HA);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( 0.0); //n2o
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( 0.0); //c_humus_0
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( 0.0); //c_humus_1
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( 0.0); //c_humus_2
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( 0.0); //no3_cons_denit
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( 0.0); //denit_factor_c
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( 0.0); //denit_factor_n
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( 0.0); //no3_prod_nit
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( 0.0); //no3_leaching

        lerr_t  rc_write = output_writer_.write_layer_daily( -static_cast< int >( fl+sbl_cnt+1), output_data_);
        if ( rc_write)
        {
            return  LDNDC_ERR_FAIL;
        }
    }

    for (size_t sbl = sbl_cnt; sbl > 0; --sbl)
    {
        double depth_sbl( sbl_cnt * 0.01 - 0.5 * 0.01 - (sbl-1) * 0.01);
        size_t l( sbl-1);
        LDNDC_OUTPUT_SET_COLUMN_INDEX(0);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( depth_sbl); //depth
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( 0.0); //extension
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( 0.0); //pore_connectivity
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( 0.0); //air permeability
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( 0.0); //aerenchym_permeability
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( 0.0); //water_content
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( 0.0); //air_content
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( 0.0); //porosity
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( 0.0); //fe2
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( 0.0); //fe3
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( sb_.co2_sbl[l]*cbm::M2_IN_HA);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( sb_.ch4_sbl[l]*cbm::M2_IN_HA);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( day_ch4_bubbling_wl[l]*cbm::M2_IN_HA);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( 0.0); //ch4 production
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( 0.0); //ch4 oxidation
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( 0.0); //doc production
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( 0.0); //acetate production
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( 0.0); //mic decay
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( sb_.doc_sbl[l]*cbm::M2_IN_HA);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( 0.0); //acetate
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( sb_.no3_sbl[l]*cbm::M2_IN_HA);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( sb_.nh4_sbl[l]*cbm::M2_IN_HA);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( sb_.urea_sbl[l]*cbm::M2_IN_HA);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( sb_.nh3_sbl[l]*cbm::M2_IN_HA);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( sb_.o2_sbl[l]*cbm::M2_IN_HA);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( 0.0); //no
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( 0.0); //n2o
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( 0.0); //c_humus_0
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( 0.0); //c_humus_1
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( 0.0); //c_humus_2
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( 0.0); //no3_cons_denit
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( 0.0); //denit_factor_c
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( 0.0); //denit_factor_n
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( 0.0); //no3_prod_nit
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( 0.0); //no3_leaching

        lerr_t  rc_write = output_writer_.write_layer_daily( -static_cast< int >( sbl), output_data_);
        if ( rc_write)
        {
            return  LDNDC_ERR_FAIL;
        }
    }

    for ( size_t  l = 0;  l < sl_.soil_layer_cnt();  ++l)
    {
        LDNDC_OUTPUT_SET_COLUMN_INDEX(0);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( -sc_.depth_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( sc_.h_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( pore_connectivity_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( D_eff_air_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( D_eff_plant_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( wc_.wc_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( v_air_sl[l]/sc_.h_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( sc_.poro_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( sc_.fe2_sl[l]*cbm::M2_IN_HA/FE3_ACETATE_REDUCTION_RATIO);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( sc_.fe3_sl[l]*cbm::M2_IN_HA/FE3_ACETATE_REDUCTION_RATIO);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( (co2_liq_sl[l]+co2_gas_sl[l])*cbm::M2_IN_HA);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( (ch4_liq_sl[l]+ch4_gas_sl[l])*cbm::M2_IN_HA);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( day_ch4_bubbling_sl[l]*cbm::M2_IN_HA);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( (day_ch4_production_acetate_sl[l]+day_ch4_production_hydrogen_sl[l])*cbm::M2_IN_HA);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( day_ch4_oxidation_sl[l]*cbm::M2_IN_HA);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( day_doc_prod_sl[l]*cbm::M2_IN_HA);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( day_acetate_prod_sl[l]*cbm::M2_IN_HA);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( day_decay_c_mic_sl[l]*cbm::M2_IN_HA);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( (sc_.doc_sl[l]+sc_.an_doc_sl[l])*cbm::M2_IN_HA);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( (ae_acetate_sl[l]+an_acetate_sl[l])*cbm::M2_IN_HA);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( (no3_ae_sl[l]+no3_an_sl[l])*cbm::M2_IN_HA);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( (nh4_ae_sl[l]+nh4_an_sl[l]+sc_.clay_nh4_sl[l])*cbm::M2_IN_HA);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( sc_.urea_sl[l]*cbm::M2_IN_HA);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( (sc_.nh3_gas_sl[l]+sc_.nh3_liq_sl[l])*cbm::M2_IN_HA);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( (o2_gas_sl[l]+o2_liq_sl[l])*cbm::M2_IN_HA);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( (no_liq_sl[l]+no_gas_sl[l]+an_no_liq_sl[l]+an_no_gas_sl[l])*cbm::M2_IN_HA);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( (n2o_liq_sl[l]+n2o_gas_sl[l]+an_n2o_liq_sl[l]+an_n2o_gas_sl[l])*cbm::M2_IN_HA);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( c_humus_1_sl[l]*cbm::M2_IN_HA);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( c_humus_2_sl[l]*cbm::M2_IN_HA);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( c_humus_3_sl[l]*cbm::M2_IN_HA);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( (accumulated_n_no3_no2_denitrification_sl[l]
                                                -output_writer_.output_writer_layer_daily.accumulated_n_no3_no2_denitrification_sl[l]) * cbm::M2_IN_HA);
        output_writer_.output_writer_layer_daily.accumulated_n_no3_no2_denitrification_sl[l] = accumulated_n_no3_no2_denitrification_sl[l];
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( day_denit_factor_c_sl[l]*FTS_TOT_);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( day_denit_factor_n_sl[l]*FTS_TOT_);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( day_nit_no2_no3_sl[l]*cbm::M2_IN_HA);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( (accumulated_n_no3_leaching_sl[l]
                                                -output_writer_.output_writer_layer_daily.accumulated_n_no3_leaching_sl[l]) * cbm::M2_IN_HA);
        output_writer_.output_writer_layer_daily.accumulated_n_no3_leaching_sl[l] = accumulated_n_no3_leaching_sl[l];

        lerr_t  rc_write = output_writer_.write_layer_daily( l, output_data_);
        if ( rc_write)
        {
            return  LDNDC_ERR_FAIL;
        }
    }

    return  LDNDC_ERR_OK;
}



lerr_t
SoilChemistryMeTrX::MeTrX_write_output_layer_yearly()
{
    for ( size_t  l = 0;  l < sl_.soil_layer_cnt();  ++l)
    {
        LDNDC_OUTPUT_SET_COLUMN_INDEX(0);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( -sc_.depth_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( c_humus_1_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( c_humus_2_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( c_humus_3_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( sc_.C_aorg_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( sc_.C_micro1_sl[l]+sc_.C_micro2_sl[l]+sc_.C_micro3_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( year_c_humify_doc_hum_1_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( year_c_humify_sol_hum_1_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( year_c_humify_cel_hum_1_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( year_c_humify_lig_hum_1_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( year_c_humify_lig_hum_2_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( year_c_humify_mic_hum_1_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( year_c_humify_mic_hum_2_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( year_c_humify_hum_1_hum_2_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( year_c_humify_hum_2_hum_3_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( year_c_decomp_hum_1_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( year_c_decomp_hum_2_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( year_c_decomp_hum_3_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( year_doc_prod_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( year_decay_c_mic_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( year_co2_hetero_sl[l]);

        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( accumulated_n_no3_no2_denitrification_sl[l] - output_writer_.output_writer_layer_yearly.accumulated_n_no3_no2_denitrification_sl[l]);
        output_writer_.output_writer_layer_yearly.accumulated_n_no3_no2_denitrification_sl[l] = accumulated_n_no3_no2_denitrification_sl[l];

        lerr_t  rc_write = output_writer_.write_layer_yearly( l, output_data_);
        if ( rc_write)
        {
            return  LDNDC_ERR_FAIL;
        }
    }

    return  LDNDC_ERR_OK;
}



lerr_t
SoilChemistryMeTrX::MeTrX_write_output_pools()
{
    for ( size_t  l = 0;  l < sl_.soil_layer_cnt();  ++l)
    {
        LDNDC_OUTPUT_SET_COLUMN_INDEX(0);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( -sc_.depth_sl[l]);

        //pools that exist for each soil layer
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( sc_.urea_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( sc_.N_lit1_sl[l] + sc_.N_lit2_sl[l] + sc_.N_lit3_sl[l] + sc_.n_wood_sl[l] + sc_.n_raw_lit_1_sl[l] + sc_.n_raw_lit_2_sl[l] + sc_.n_raw_lit_3_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( sc_.N_aorg_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( n_humus_1_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( n_humus_2_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( n_humus_3_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( n_micro_1_sl[l] + n_micro_2_sl[l] + n_micro_3_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( sc_.nh3_liq_sl[l] + sc_.nh3_gas_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( nh4_ae_sl[l] + nh4_an_sl[l] + sc_.clay_nh4_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( don_ae_sl[l] + don_an_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( no3_ae_sl[l] + no3_an_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( sc_.no2_sl[l] + sc_.an_no2_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( no_gas_sl[l] + an_no_gas_sl[l] + no_liq_sl[l] + an_no_liq_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( n2o_gas_sl[l] + an_n2o_gas_sl[l] + n2o_liq_sl[l] + an_n2o_liq_sl[l]);

        //surface pools
        if( l == 0 )
        {
          double nplant(0.0);
          for ( PlantIterator vt = this->m_veg->begin(); vt != this->m_veg->end(); ++vt)
          {
              nplant += (*vt)->total_nitrogen();
          }

          METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( sb_.nh4_sbl.sum() );
          METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( sb_.nh3_sbl.sum() );
          METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( sb_.n2o_sbl.sum() );
          METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( sb_.no_sbl.sum() );
          METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( sb_.urea_sbl.sum() );
          METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( sb_.no3_sbl.sum() );
          //METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( sb_.don_sbl.sum() );
          METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( nplant );
          METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( sc_.n_wood + sc_.n_stubble_lit1 + sc_.n_stubble_lit2 + sc_.n_stubble_lit3 +
                                                 sc_.n_raw_lit_1_above + sc_.n_raw_lit_2_above + sc_.n_raw_lit_3_above);
        }
        else
        {
          METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( 0 );
          METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( 0 );
          METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( 0 );
          METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( 0 );
          METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( 0 );
          METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( 0 );
          METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( 0 );
          METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( 0 );
          //METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( 0 );
          //METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( 0 );
        }

        lerr_t  rc_write = output_writer_.write_pools( l, output_data_);
        if ( rc_write)
        {
            return  LDNDC_ERR_FAIL;
        }
    }

    return  LDNDC_ERR_OK;
}



lerr_t
SoilChemistryMeTrX::MeTrX_write_output_fluxes()
{
#define  METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH(__val__)              \
{                                                                               \
double const delta_val( __val__ - output_writer_.output_writer_fluxes.__val__); \
LDNDC_OUTPUT_SET_COLUMN_((delta_val),=,output_data_,1);                         \
output_writer_.output_writer_fluxes.__val__ = __val__;                          \
}                                                                               \

#define  METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH_2(__val1__,__val2__)    \
{                                                                                 \
double const delta_val( __val1__ - output_writer_.output_writer_fluxes.__val2__); \
LDNDC_OUTPUT_SET_COLUMN_((delta_val),=,output_data_,1);                           \
output_writer_.output_writer_fluxes.__val2__ = __val1__;                          \
}                                                                                 \

    for ( size_t  l = 0;  l < sl_.soil_layer_cnt();  ++l)
    {
        LDNDC_OUTPUT_SET_COLUMN_INDEX(0);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( -sc_.depth_sl[l]);

        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_nh4_throughfall_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_no3_throughfall_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_nh4_throughfall_sbl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_no3_throughfall_sbl[l]);

        //N fluxes in plant/algae
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_to_living_plant_and_algae_from_extern_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH_2( ph_.accumulated_nh4_uptake_sl[l], accumulated_n_plant_nh4_uptake_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH_2( ph_.accumulated_no3_uptake_sl[l], accumulated_n_plant_no3_uptake_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH_2( ph_.accumulated_nh3_uptake_sl[l], accumulated_n_plant_nh3_uptake_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH_2( ph_.accumulated_don_uptake_sl[l], accumulated_n_plant_don_uptake_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_algae_nh4_uptake_sbl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_algae_no3_uptake_sbl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_algae_nh3_uptake_sbl[l]);

        //N fluxes from plant to raw litter, stubble and wood
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_litter_from_plants_below_rawlitter_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_litter_from_plants_below_wood_sl[l]);
        if ( l == 0)
        {
            METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_litter_from_plants_above_rawlitter);
            METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_litter_from_plants_above_wood);
            METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_litter_from_plants_above_stubble);
        }
        else
        {
            LDNDC_OUTPUT_SET_COLUMN_(0.0,=,output_data_,1);
            LDNDC_OUTPUT_SET_COLUMN_(0.0,=,output_data_,1);
            LDNDC_OUTPUT_SET_COLUMN_(0.0,=,output_data_,1);
        }

        //N fluxes from raw litter to soil litter
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_aboveground_raw_litter_fragmentation_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_belowground_raw_litter_fragmentation_sl[l]);

        //N fluxes from grazer and algae
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_litter_from_algae_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_litter_from_dung_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_urea_from_dung_sl[l]);

        //N exported via harvesting, cutting and grazing
        if ( l == 0)
        {
            METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_export_harvest_cutting_grazing);
        }
        else
        {
            LDNDC_OUTPUT_SET_COLUMN_(0.0,=,output_data_,1);
        }

        //N fluxes from fertilization and manuring
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_nh4_fertilization_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_no3_fertilization_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_nh3_fertilization_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_don_fertilization_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_urea_fertilization_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_litter_fertilization_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_aorg_fertilization_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_nh4_fertilization_sbl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_no3_fertilization_sbl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_nh3_fertilization_sbl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_urea_fertilization_sbl[l]);

        //N redistribution during tilling
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_surface_litter_incorporation_via_tilling_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_litter_tilling_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_aorg_tilling_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_humus_1_tilling_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_humus_2_tilling_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_humus_3_tilling_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_no3_tilling_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_no2_tilling_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_no_tilling_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_n2o_tilling_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_urea_tilling_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_don_tilling_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_nh4_tilling_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_nh3_tilling_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_microbes_tilling_sl[l]);

        //N redistribution during spin up
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_humus_1_spinup_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_humus_2_spinup_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_humus_3_spinup_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_surface_litter_spinup_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_litter_spinup_sl[l]);

        //N fluxes due to water movement
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_micro_leaching_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_aorg_leaching_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_litter_leaching_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_humus_1_leaching_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_humus_2_leaching_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_humus_3_leaching_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_nh4_leaching_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_no3_leaching_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_nh3_leaching_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_don_leaching_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_urea_leaching_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_no_leaching_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_n2o_leaching_sl[l]);

        //N fluxes via liquid diffusion
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_urea_liq_diffusion_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_nh3_liq_diffusion_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_nh4_liq_diffusion_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_no3_liq_diffusion_sl[l]);

        // incoming N from leaching from surface water
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_nh4_infiltration_phys_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_nh3_infiltration_phys_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_urea_infiltration_phys_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_no3_infiltration_phys_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_don_infiltration_phys_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_aorg_infiltration_phys_sl[l]);

        //incoming N fluxes from disappearing water table
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_nh4_infiltration_leach_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_nh3_infiltration_leach_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_urea_infiltration_leach_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_no3_infiltration_leach_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_don_infiltration_leach_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_no_infiltration_leach_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_n2o_infiltration_leach_sl[l]);

        //incoming N flux via liquid diffusion from surface water
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_nh4_infiltration_liqdif_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_nh3_infiltration_liqdif_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_urea_infiltration_liqdif_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_no3_infiltration_liqdif_sl[l]);

        //ebullition fluxes
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_nh3_bubbling_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_no_bubbling_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_n2o_bubbling_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_nh3_bubbling_sbl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_no_bubbling_sbl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_n2o_bubbling_sbl[l]);

        //N fluxes due to dissolution between atmosphere and surface water
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_nh3_dissolution_sbl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_no_dissolution_sbl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_n2o_dissolution_sbl[l]);

        //N fluxes from urea to nh4
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_urea_nh4_hydrolysis_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_urea_nh4_hydrolysis_sbl[l]);

        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_nh4_assimilation_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_no3_assimilation_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_don_assimilation_sl[l]);

        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_nh4_nh3_conversion_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_nh3_nh4_conversion_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_nh4_nh3_conversion_sbl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_nh3_nh4_conversion_sbl[l]);

        //gaseous diffusion
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_nh3_phys_diffusion_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_no_phys_diffusion_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_n2o_phys_diffusion_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_nh3_gas_diffusion_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_no_gas_diffusion_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_n2o_gas_diffusion_sl[l]);

        //perturbation
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_micro_perturbation_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_aorg_perturbation_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_litter_perturbation_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_humus_1_perturbation_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_humus_2_perturbation_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_humus_3_perturbation_sl[l]);

        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_nh4_no2_nitrification_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_nh4_no_nitrification_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_nh4_n2o_nitrification_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_no2_no3_nitrification_sl[l]);

        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_no3_no2_denitrification_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_no3_no_denitrification_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_no3_n2o_denitrification_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_no3_n2_denitrification_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_no2_no_denitrification_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_no2_n2o_denitrification_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_no2_n2_denitrification_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_no_n2o_denitrification_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_no_n2_denitrification_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_n2o_n2_denitrification_sl[l]);

        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_no2_chemodenitrification_sl[l]);

        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_mic_naorg_decay_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_mic_nh4_mineral_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_mic_don_dissolve_sl[l]);

        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_aorg_nh4_mineral_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_aorg_don_dissolve_sl[l]);

        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_humus_1_don_dissolve_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_humus_1_nh4_mineral_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_humus_2_don_dissolve_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_humus_2_nh4_mineral_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_humus_3_don_dissolve_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_humus_3_nh4_mineral_sl[l]);

        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_litter_don_dissolve_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_litter_nh4_mineral_sl[l]);

        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_don_humus_1_humify_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_don_humus_2_humify_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_don_humus_3_humify_sl[l]);
        METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT_AND_SWITCH( accumulated_n_litter_humus_1_humify_sl[l]);

        lerr_t  rc_write = output_writer_.write_fluxes( l, output_data_);
        if ( rc_write)
        {
            return  LDNDC_ERR_FAIL;
        }
    }

    return  LDNDC_ERR_OK;
}



lerr_t
SoilChemistryMeTrX::MeTrX_write_output_subdaily()
{
    size_t s( lclock()->subday()-1);
    LDNDC_OUTPUT_SET_COLUMN_INDEX(0);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( sc_.C_micro1_sl.sum());
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( sc_.C_micro2_sl.sum());
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( sc_.C_micro3_sl.sum());
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( sc_.C_aorg_sl.sum());
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( nh4_ae_sl.sum()+nh4_an_sl.sum()+sc_.clay_nh4_sl.sum());
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( no3_ae_sl.sum());
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( no3_an_sl.sum());
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( sc_.doc_sl.sum());
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( sc_.an_doc_sl.sum());
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( ae_acetate_sl.sum());
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( an_acetate_sl.sum());
    
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( subdaily_ch4_bubbling[s]);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( subdaily_ch4_plant[s]);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( subdaily_ch4_soil[s]);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( subdaily_ch4_water[s]);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( subdaily_ch4_leach[s]);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( subdaily_ch4_prod[s]);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( subdaily_ch4_ox[s]);
    
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( subdaily_acetate_prod[s]);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( subdaily_doc_prod[s]);

    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( accumulated_n_no2_no3_nitrification_sl.sum() - output_writer_.output_writer_subdaily.accumulated_n_no2_no3_nitrification);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( accumulated_n_no3_no2_denitrification_sl.sum() - output_writer_.output_writer_subdaily.accumulated_n_no3_no2_denitrification);

    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( subdaily_n2o_bubbling[s]);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( subdaily_n2o_plant[s]);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( subdaily_n2o_soil[s]);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( subdaily_n2o_water[s]);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( subdaily_no_bubbling[s]);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( subdaily_no_plant[s]);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( subdaily_no_soil[s]);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( subdaily_no_water[s]);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( subdaily_nh3_bubbling[s]);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( subdaily_nh3_plant[s]);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( subdaily_nh3_soil[s]);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( subdaily_nh3_water[s]);
    
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( subdaily_plant_o2_cons[s]);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( subdaily_o2_plant[s]);
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( subdaily_algae_o2_prod[s]);
    
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( MeTrX_get_ph_wl());
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( sc_.ph_sl[0]);
    double anvf_mean( sc_.anvf_sl.sum() / sl_.soil_layer_cnt());
    METRX_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( anvf_mean);

    lerr_t  rc_write = output_writer_.write_subdaily( output_data_);
    if ( rc_write)
    {
        return  LDNDC_ERR_FAIL;
    }

    output_writer_.output_writer_subdaily.accumulated_n_no2_no3_nitrification = accumulated_n_no2_no3_nitrification_sl.sum();
    output_writer_.output_writer_subdaily.accumulated_n_no3_no2_denitrification = accumulated_n_no3_no2_denitrification_sl.sum();

    return  LDNDC_ERR_OK;
}

} /*namespace ldndc*/
