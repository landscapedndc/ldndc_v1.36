/*!
 * @file
 *
 * @author
 *      David Kraus (created on: may 12, 2014)
 *
 */

#include  "soilchemistry/metrx/soilchemistry-metrx.h"

namespace ldndc {



/*!
 * @page metrx
 * @section mic_dynamics Microbial dynamics
 *
 */
void
SoilChemistryMeTrX::MeTrX_microbial_dynamics()
{
    for( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
    {
        double const c_tot_old( sc_.doc_sl[sl] + ae_acetate_sl[sl]);
        double const n_tot_old( nh4_ae_sl[sl] + no3_ae_sl[sl] + don_ae_sl[sl]);
        double const o2_tot( o2_gas_sl[sl] + o2_liq_sl[sl]);
        double const aevf( 1.0 - sc_.anvf_sl[sl]);

        /*!
         * @page metrx
         * Microbial growth is given by:
         * \f[
         * \frac{c_{mic}}{dt} = c_{mic} a_{mic} \mu_{mic} \phi_{DOC}
         * \f]
         * \f$ c_{mic}: \f$ Microbial biomass \n
         * \f$ a_{mic}: \f$ Microbial activity \n
         * \f$ \mu_{mic}: \f$ Potential microbial growth rate \n
         * \f$ \phi_{DOC}: \f$ Microbial growth dependency on DOC \n
         * \n
         * The activity coefficient of microbes \f$ a_{mic} \f$ is given by a harmonic mean
         * of a soil temperature (\f$ \phi_{T}\f$) and a soil water (\f$ \phi_{wfps}\f$) depending response coefficient.
         *
         * \f[
         * a_{mic} = \frac{\phi_{T} + \phi_{wfps}}{\frac{1}{\phi_{T}} + \frac{1}{\phi_{wfps}}}
         * \f]
         * with \f$ \phi_{T}\f$ given by:
         * \f[
         * \phi_{T} = e^{-METRX\_F\_MIC\_T\_EXP\_1 \cdot \left( \frac{1 - T}{METRX\_F\_MIC\_T\_EXP\_2} \right)^2}
         * \f]
         *
         * @image html metrx_denitrification_on_temperature.png "Response function depending on temperature" width=900
         * @image latex metrx_denitrification_on_temperature.png "Response function depending on temperature"
         *
         * and with \f$ \phi_{wfps}\f$ given by:
         *
         * \f[
         * \phi_{wfps} = 1 - \frac{1}{1 + e^{(wfps - METRX\_F\_MIC\_M\_WEIBULL\_1) \cdot METRX\_F\_MIC\_M\_WEIBULL\_2}}
         * \f]
         *
         * @image html metrx_denitrification_on_soilwater.png "Response function depending on soilwater" width=900
         * @image latex metrx_denitrification_on_soilwater.png "Response function depending on soilwater"
         * \n
         */
        double const activity_ae( MeTrX_get_fact_tm_mic( sl));
        double const active_microbes( sc_.C_micro1_sl[sl] * activity_ae);

        if ( cbm::flt_greater_zero( c_tot_old * o2_tot))
        {
            /*!
             * @page metrx
             * The potential microbial growth rate is given by the parameter: \f$ MUE\_MAX\_C\_MICRO\_1 \f$.
             *
             * The dependency of microbial growth on DOC is given by:
             *
             * \f[
             * \phi_{DOC} = \frac{DOC}{DOC + METRX\_KMM\_C\_MIC}
             * \f]
             *
             * @image html metrx_microbial_growth_on_doc.png "Microbial growth dependency on DOC" width=500
             * @image latex metrx_microbial_growth_on_doc.png "Microbial growth dependency on DOC"
             * \n
             */
            double const fact_c( c_tot_old / (MeTrX_get_k_mm_x_sl( sl, aevf * sipar_.METRX_KMM_C_MIC()) + c_tot_old));
            double const c_demand_tot( cbm::bound_max( active_microbes * MUE_MAX_C_MICRO_1 * fact_c /
                                                       sipar_.METRX_MIC_EFF_AEROBIC_RESPIRATION(),
                                                       c_tot_old));
            double const c_demand_diss( cbm::bound_max( c_demand_tot * (1.0 - sipar_.METRX_MIC_EFF_AEROBIC_RESPIRATION()),
                                                        MC_MO2_RATIO * o2_tot));
            double const c_demand_assi( c_demand_tot - c_demand_diss);
            
            double const o2_cons_tot( c_demand_diss / MC_MO2_RATIO);
            double const o2_gas_cons( o2_cons_tot * o2_gas_sl[sl] / o2_tot);
            double const o2_liq_cons( o2_cons_tot - o2_gas_cons);
            o2_gas_sl[sl] = cbm::bound_min( 0.0, o2_gas_sl[sl] - o2_gas_cons);
            o2_liq_sl[sl] = cbm::bound_min( 0.0, o2_liq_sl[sl] - o2_liq_cons);
            
            double const doc_cons( sc_.doc_sl[sl] / c_tot_old * c_demand_tot);
            double const acetate_cons( c_demand_tot - doc_cons);
            sc_.doc_sl[sl] = cbm::bound_min( 0.0, sc_.doc_sl[sl] - doc_cons);
            ae_acetate_sl[sl] = cbm::bound_min( 0.0, ae_acetate_sl[sl] - acetate_cons);
            
            sc_.C_micro1_sl[sl] += c_demand_assi;
            day_assi_c_mic_1 += c_demand_assi;
            
            co2_hetero_sl[sl] += c_demand_diss;
            co2_gas_sl[sl] += c_demand_diss;
            day_co2_prod_mic_1_growth += c_demand_diss;
        }

        if ( cbm::flt_greater_zero( n_tot_old))
        {
            double const fact_n( n_tot_old / (MeTrX_get_k_mm_x_sl( sl, aevf * sipar_.METRX_KMM_N_MIC()) + n_tot_old));
            double const pot_n_growth( MeTrX_get_n_growth( sc_.C_micro1_sl[sl],
                                                           n_micro_1_sl[sl],
                                                           activity_ae,
                                                           fact_n,
                                                           cn_opt_mic_sl[sl]));
            
            MeTrX_execute_n_assimilation( pot_n_growth,
                                          sl,
                                          true,
                                          n_micro_1_sl[sl],
                                          day_min_n_mic_1,
                                          day_assi_n_mic_1);
        }

        /*!
         * @page metrx
         * Microbial loss of biomass via maintenance respiration \f$ \frac{c_{mic,r}}{dt} \f$
         * and death are calculated after @cite blagodatsky:1998:
         *
         * \f{eqnarray*}{
         * \frac{c_{mic,r}}{dt} &=& c_{mic} a_{mic} a_{max} (1 - Y_r) \\
         * \frac{c_{mic,d}}{dt} &=& c_{mic} a_{mic} a_{max} \frac{1}{1 + k_a \cdot DOC}
         * \f}
         *
         * \f$ c_{mic}: \f$ Microbial biomass \n
         * \f$ c_{mic,r}: \f$ Microbial biomass subject to respiration \n
         * \f$ c_{mic,d}: \f$ Microbial biomass subject to death \n
         * \f$ a_{mic}: \f$ Microbial activity \n
         * \f$ a_{max}: \f$ Maximum microbial death rate \n
         * \n
         */
        double const micro_c_decay_max( MeTrX_get_micro_c_decay_max( sc_.C_micro1_sl[sl]));
        if ( cbm::flt_greater_zero( micro_c_decay_max * n_micro_1_sl[sl]))
        {
            double const ka( sipar_.METRX_KA_C_MIC() / (sc_.h_sl[sl] * aevf));
            double const cn_ratio( sc_.C_micro1_sl[sl] / n_micro_1_sl[sl]);
            double const decay_scale( active_microbes * A_MAX_MICRO_1);
            double const micro_mineral_eff( MeTrX_get_mineral_efficiency( cn_ratio, cn_opt_mic_sl[sl]));

            double micro_resp_c( decay_scale * (1.0 - micro_mineral_eff));
            double micro_death_c( decay_scale * 1.0 / (1.0 + c_tot_old * ka));
            double const micro_decay_c( micro_resp_c + micro_death_c);
            if ( cbm::flt_greater_equal( micro_decay_c ,micro_c_decay_max))
            {
                double const corr( micro_c_decay_max / micro_decay_c);
                micro_resp_c *= corr;
                micro_death_c *= corr;
            }

            double const micro_decay_n( (micro_resp_c + micro_death_c) / cn_ratio);
            MeTrX_execute_co2_prod(sl, micro_resp_c, sc_.C_micro1_sl[sl], day_co2_prod_mic_1_maintenance);
            MeTrX_dead_microbial_biomass_allocation( sl,
                                                     micro_death_c,
                                                     micro_decay_n,
                                                     sc_.C_micro1_sl[sl],
                                                     n_micro_1_sl[sl]);
        }
    }
}


} /*namespace ldndc*/
