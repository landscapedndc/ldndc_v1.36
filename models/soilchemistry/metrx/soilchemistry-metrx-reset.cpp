/*!
 * @file
 *      David Kraus (created on: may 20, 2015)
 *
 */

#include  "soilchemistry/metrx/soilchemistry-metrx.h"

#include  <input/soillayers/soillayers.h>
#include  <constants/lconstants-plant.h>

namespace ldndc {

/*!
 * @brief
 *
 */
void
SoilChemistryMeTrX::MeTrX_reset()
{
    co2_auto_sl = 0.0;
    co2_hetero_sl = 0.0;

    if ( timemode_ == TMODE_SUBDAILY)
    {
        if ( lclock()->is_position( TMODE_PRE_DAILY))
        {
            MeTrX_reset_subdaily();
            MeTrX_reset_daily();
        }

        if ( lclock()->is_position( TMODE_PRE_YEARLY))
        {
            MeTrX_reset_yearly();
        }
    }
    else
    {
        MeTrX_reset_subdaily();
        MeTrX_reset_daily();

        if ( lclock()->yearday() == 1)
        {
            MeTrX_reset_yearly();
        }
    }
}



/*!
 * @brief
 *
 */
void
SoilChemistryMeTrX::MeTrX_reset_subdaily()
{
    for (size_t ts = 0; ts < TOTAL_TIME_STEPS; ts++)
    {
        subdaily_co2_bubbling[ts] = 0.0;
        subdaily_co2_plant[ts] = 0.0;
        subdaily_co2_soil[ts] = 0.0;
        subdaily_co2_water[ts] = 0.0;
        subdaily_co2_leach[ts] = 0.0;

        subdaily_ch4_bubbling[ts] = 0.0;
        subdaily_ch4_plant[ts] = 0.0;
        subdaily_ch4_soil[ts] = 0.0;
        subdaily_ch4_water[ts] = 0.0;
        subdaily_ch4_leach[ts] = 0.0;
        subdaily_ch4_prod[ts] = 0.0;
        subdaily_ch4_ox[ts] = 0.0;
        subdaily_acetate_prod[ts] = 0.0;
        subdaily_doc_prod[ts] = 0.0;
        subdaily_plant_o2_cons[ts] = 0.0;
        subdaily_flood_o2_conc[ts] = 0.0;
        subdaily_algae_o2_prod[ts] = 0.0;
        subdaily_o2_bubbling[ts] = 0.0;
        subdaily_o2_plant[ts] = 0.0;
        subdaily_o2_soil[ts] = 0.0;
        subdaily_o2_water[ts] = 0.0;
        subdaily_n2o_bubbling[ts] = 0.0;
        subdaily_n2o_plant[ts] = 0.0;
        subdaily_n2o_soil[ts] = 0.0;
        subdaily_n2o_water[ts] = 0.0;
        subdaily_no_bubbling[ts] = 0.0;
        subdaily_no_plant[ts] = 0.0;
        subdaily_no_soil[ts] = 0.0;
        subdaily_no_water[ts] = 0.0;
        subdaily_nh3_bubbling[ts] = 0.0;
        subdaily_nh3_plant[ts] = 0.0;
        subdaily_nh3_soil[ts] = 0.0;
        subdaily_nh3_water[ts] = 0.0;
    }
}



/*!
 * @brief
 *
 */
void
SoilChemistryMeTrX::MeTrX_reset_daily()
{
    /***************/
    /* Water layer */
    /***************/

    for (size_t wl = 0; wl < sb_.surfacebulk_layer_cnt(); wl++)
    {
        day_ch4_bubbling_wl[wl] = 0.0;
    }

    /**************/
    /* Soil layer */
    /**************/

    for (size_t sl = 0; sl < sl_.soil_layer_cnt(); sl++)
    {
        plant_o2_consumption_sl[sl] = 0.0;

        day_nitrify_n2o_sl[sl] = 0.0;
        day_nit_no2_no3_sl[sl] = 0.0;

        day_acetate_cons_fe3_sl[sl] = 0.0;
        day_h2_c_eq_cons_fe3_sl[sl] = 0.0;
        day_fe2_oxidation_sl[sl] = 0.0;

        day_acetate_prod_sl[sl] = 0.0;

        day_ch4_oxidation_sl[sl] = 0.0;
        day_ch4_bubbling_sl[sl] = 0.0;

        day_h2_c_eq_prod_sl[sl] = 0.0;
        day_ch4_production_acetate_sl[sl] = 0.0;
        day_ch4_production_hydrogen_sl[sl] = 0.0;

        day_doc_prod_sl[sl] = 0.0;
        day_denit_factor_c_sl[sl] = 0.0;
        day_denit_factor_n_sl[sl] = 0.0;
    }

    // Groundwater access
    day_no3_groundwater_access = 0.0;

    // Mineralisation
    day_min_n_decomp = 0.0;
    day_min_n_aorg = 0.0;
    day_min_n_mic_1 = 0.0;
    day_min_n_mic_2 = 0.0;
    day_min_n_mic_3 = 0.0;

    // Assimilation
    day_assi_n_mic_1 = 0.0;
    day_assi_n_mic_2 = 0.0;
    day_assi_n_mic_3 = 0.0;
    day_assi_c_mic_1 = 0.0;
    day_assi_c_mic_2 = 0.0;
    day_assi_c_mic_3 = 0.0;

    // Nitrification
    day_nit_nh4_no2 = 0.0;
    day_nit_no2_no = 0.0;
    day_nit_no2_n2o = 0.0;
    day_nit_no2_no3_sl = 0.0;

    // Denitrification
    day_denit_no3_no2 = 0.0;
    day_denit_no2_no = 0.0;
    day_denit_no2_n2o = 0.0;
    day_denit_no2_n2 = 0.0;
    day_denit_no_n2o = 0.0;
    day_denit_no_n2 = 0.0;
    day_denit_n2o_n2 = 0.0;
    day_chemodenit_no2_no = 0.0;

    // Respiration / Fermentation / Methanogenesis
    day_co2_prod_mic_1_growth = 0.0;
    day_co2_prod_mic_1_maintenance = 0.0;
    day_co2_prod_mic_2 = 0.0;
    day_co2_prod_mic_3_acetate_prod = 0.0;
    day_co2_prod_mic_3_acetate_cons = 0.0;
    day_co2_prod_ch4_prod = 0.0;
    day_co2_prod_ch4_cons = 0.0;

    // Leaching
    day_leach_o2 = 0.0;
    day_leach_n2o = 0.0;
    day_leach_n2 = 0.0;
    day_leach_no = 0.0;
    day_leach_nh3 = 0.0;
    day_leach_urea = 0.0;

    // litter
    dC_root_exsudates = 0.0;
    dC_litter_algae = 0.0;

    // Decomposition
    day_decomp_c_lit_1 = 0.0;
    day_decomp_c_lit_2 = 0.0;
    day_decomp_c_lit_3 = 0.0;
    day_decomp_n_lit_1 = 0.0;
    day_decomp_n_lit_2 = 0.0;
    day_decomp_n_lit_3 = 0.0;
    day_decomp_c_hum_1_sl = 0.0;
    day_decomp_c_hum_2_sl = 0.0;
    day_decomp_c_hum_3_sl = 0.0;
    day_decay_c_mic_sl = 0.0;
    day_decomp_n_hum_1 = 0.0;
    day_decomp_n_hum_2 = 0.0;
    day_decomp_n_hum_3 = 0.0;

    // Humification
    day_c_humify_doc_hum_1_sl = 0.0;
    day_c_humify_sol_hum_1_sl = 0.0;
    day_c_humify_cel_hum_1_sl = 0.0;
    day_c_humify_lig_hum_1_sl = 0.0;
    day_c_humify_lig_hum_2_sl = 0.0;
    day_c_humify_mic_hum_1_sl = 0.0;
    day_c_humify_mic_hum_2_sl = 0.0;
    day_c_humify_hum_1_hum_2_sl = 0.0;
    day_c_humify_hum_2_hum_3_sl = 0.0;

    // Algae
    day_n_fix_algae = 0.0;
    day_c_fix_algae = 0.0;

    // DOC production
    day_doc_prod_decomp_litter = 0.0;
    day_doc_prod_decomp_humus = 0.0;
    day_doc_prod_decomp_aorg = 0.0;
}



/*!
 * @brief
 *
 */
void
SoilChemistryMeTrX::MeTrX_reset_yearly()
{
    // Groundwater access
    year_no3_groundwater_access = 0.0;

    // Humification
    year_c_humify_doc_hum_1_sl = 0.0;
    year_c_humify_sol_hum_1_sl = 0.0;
    year_c_humify_cel_hum_1_sl = 0.0;
    year_c_humify_lig_hum_1_sl = 0.0;
    year_c_humify_lig_hum_2_sl = 0.0;
    year_c_humify_mic_hum_1_sl = 0.0;
    year_c_humify_mic_hum_2_sl = 0.0;
    year_c_humify_hum_1_hum_2_sl = 0.0;
    year_c_humify_hum_2_hum_3_sl = 0.0;
    year_c_decomp_hum_1_sl = 0.0;
    year_c_decomp_hum_2_sl = 0.0;
    year_c_decomp_hum_3_sl = 0.0;
    year_doc_prod_sl = 0.0;
    year_decay_c_mic_sl = 0.0;
    year_co2_hetero_sl = 0.0;

    // Decomposition
    year_decomp_c_lit_1 = 0.0;
    year_decomp_c_lit_2 = 0.0;
    year_decomp_c_lit_3 = 0.0;
    year_decomp_n_lit_1 = 0.0;
    year_decomp_n_lit_2 = 0.0;
    year_decomp_n_lit_3 = 0.0;
    year_decomp_n_hum_1 = 0.0;
    year_decomp_n_hum_2 = 0.0;
    year_decomp_n_hum_3 = 0.0;

    // Methane
    year_ch4_ox = 0.0;

    // Algae
    year_c_fix_algae = 0.0;
    year_n_fix_algae = 0.0;

    // litter
    accumulated_c_litter_above_last_year = sc_.accumulated_c_litter_stubble + sc_.accumulated_c_litter_wood_above;
    accumulated_c_litter_below_last_year = sc_.accumulated_c_litter_wood_below_sl.sum() + sc_.accumulated_c_litter_below_sl.sum();
}

} /*namespace ldndc*/

