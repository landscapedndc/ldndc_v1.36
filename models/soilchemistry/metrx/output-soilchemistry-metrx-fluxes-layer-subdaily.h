/*!
 * @file
 *
 * @author
 *    david kraus
 */

#ifndef  LM_OUTPUT_SOILCHEMISTRYMETRX_FLUXES_LAYER_SUBDAILY_H_
#define  LM_OUTPUT_SOILCHEMISTRYMETRX_FLUXES_LAYER_SUBDAILY_H_

#include  "mbe_legacyoutputmodel.h"

#define  LMOD_OUTPUT_MODULE_ID    "output:soilchemistry-metrx-fluxes-layer:subdaily"
#define  LMOD_OUTPUT_MODULE_DESC  "SoilchemistryMeTrX Fluxes Output"
namespace ldndc {
class  LDNDC_API  OutputSoilchemistryMeTrXFluxes  :  public  MBE_LegacyOutputModel
{
    LMOD_EXPORT_MODULE_INFO(OutputSoilchemistryMeTrXFluxes,LMOD_OUTPUT_MODULE_ID,LMOD_OUTPUT_MODULE_DESC);
    public:
        OutputSoilchemistryMeTrXFluxes(
                MoBiLE_State *,
                cbm::io_kcomm_t *,
                timemode_e  _timemode);

        ~OutputSoilchemistryMeTrXFluxes();


        lerr_t  configure( ldndc::config_file_t const *);
        lerr_t  initialize();
        lerr_t  solve();
        lerr_t  finalize();

        lerr_t  wake() { return  LDNDC_ERR_OK; }
        lerr_t  sleep() { return  LDNDC_ERR_OK; }


        lerr_t  write_results(
                unsigned int /* layer index */,
                ldndc_flt64_t *);

        size_t  record_size() const;

    private:
        cbm::io_kcomm_t *  m_iokcomm;
        ldndc::sink_handle_t  m_sink;

        input_class_soillayers_t const &  sl_;
        substate_surfacebulk_t &  sb_;

    public:

        lvector_t< double >  accumulated_n_nh4_throughfall_sl;
        lvector_t< double >  accumulated_n_no3_throughfall_sl;
        lvector_t< double >  accumulated_n_nh4_throughfall_sbl;
        lvector_t< double >  accumulated_n_no3_throughfall_sbl;

        lvector_t< double >  accumulated_n_to_living_plant_and_algae_from_extern_sl;

        lvector_t< double >  accumulated_n_plant_nh4_uptake_sl;
        lvector_t< double >  accumulated_n_plant_no3_uptake_sl;
        lvector_t< double >  accumulated_n_plant_nh3_uptake_sl;
        lvector_t< double >  accumulated_n_plant_don_uptake_sl;
        lvector_t< double >  accumulated_n_algae_nh4_uptake_sbl;
        lvector_t< double >  accumulated_n_algae_no3_uptake_sbl;
        lvector_t< double >  accumulated_n_algae_nh3_uptake_sbl;


        lvector_t< double >  accumulated_n_litter_from_plants_below_rawlitter_sl;
        lvector_t< double >  accumulated_n_litter_from_plants_below_wood_sl;
        double  accumulated_n_litter_from_plants_above_rawlitter;
        double  accumulated_n_litter_from_plants_above_wood;
        double  accumulated_n_litter_from_plants_above_stubble;
        lvector_t< double >  accumulated_n_aboveground_raw_litter_fragmentation_sl;
        lvector_t< double >  accumulated_n_belowground_raw_litter_fragmentation_sl;
        lvector_t< double >  accumulated_n_litter_from_algae_sl;
        lvector_t< double >  accumulated_n_litter_from_dung_sl;
        lvector_t< double >  accumulated_n_urea_from_dung_sl;

        double  accumulated_n_export_harvest_cutting_grazing;

        lvector_t< double >  accumulated_n_nh4_fertilization_sl;
        lvector_t< double >  accumulated_n_no3_fertilization_sl;
        lvector_t< double >  accumulated_n_nh3_fertilization_sl;
        lvector_t< double >  accumulated_n_don_fertilization_sl;
        lvector_t< double >  accumulated_n_urea_fertilization_sl;
        lvector_t< double >  accumulated_n_litter_fertilization_sl;
        lvector_t< double >  accumulated_n_aorg_fertilization_sl;
        lvector_t< double >  accumulated_n_nh4_fertilization_sbl;
        lvector_t< double >  accumulated_n_no3_fertilization_sbl;
        lvector_t< double >  accumulated_n_nh3_fertilization_sbl;
        lvector_t< double >  accumulated_n_urea_fertilization_sbl;

        lvector_t< double >  accumulated_n_surface_litter_incorporation_via_tilling_sl;
        lvector_t< double >  accumulated_n_litter_tilling_sl;
        lvector_t< double >  accumulated_n_aorg_tilling_sl;
        lvector_t< double >  accumulated_n_humus_1_tilling_sl;
        lvector_t< double >  accumulated_n_humus_2_tilling_sl;
        lvector_t< double >  accumulated_n_humus_3_tilling_sl;
        lvector_t< double >  accumulated_n_no3_tilling_sl;
        lvector_t< double >  accumulated_n_no2_tilling_sl;
        lvector_t< double >  accumulated_n_no_tilling_sl;
        lvector_t< double >  accumulated_n_n2o_tilling_sl;
        lvector_t< double >  accumulated_n_urea_tilling_sl;
        lvector_t< double >  accumulated_n_don_tilling_sl;
        lvector_t< double >  accumulated_n_nh4_tilling_sl;
        lvector_t< double >  accumulated_n_nh3_tilling_sl;
        lvector_t< double >  accumulated_n_microbes_tilling_sl;

        lvector_t< double >  accumulated_n_humus_1_spinup_sl;
        lvector_t< double >  accumulated_n_humus_2_spinup_sl;
        lvector_t< double >  accumulated_n_humus_3_spinup_sl;
        lvector_t< double >  accumulated_n_surface_litter_spinup_sl;
        lvector_t< double >  accumulated_n_litter_spinup_sl;

        lvector_t< double >  accumulated_n_urea_nh4_hydrolysis_sl;
        lvector_t< double >  accumulated_n_urea_nh4_hydrolysis_sbl;

        lvector_t< double >  accumulated_n_nh4_assimilation_sl;
        lvector_t< double >  accumulated_n_no3_assimilation_sl;
        lvector_t< double >  accumulated_n_don_assimilation_sl;

        lvector_t< double >  accumulated_n_nh4_nh3_conversion_sl;
        lvector_t< double >  accumulated_n_nh3_nh4_conversion_sl;
        lvector_t< double >  accumulated_n_nh4_nh3_conversion_sbl;
        lvector_t< double >  accumulated_n_nh3_nh4_conversion_sbl;

        lvector_t< double >  accumulated_n_micro_leaching_sl;
        lvector_t< double >  accumulated_n_aorg_leaching_sl;
        lvector_t< double >  accumulated_n_litter_leaching_sl;
        lvector_t< double >  accumulated_n_humus_1_leaching_sl;
        lvector_t< double >  accumulated_n_humus_2_leaching_sl;
        lvector_t< double >  accumulated_n_humus_3_leaching_sl;
        lvector_t< double >  accumulated_n_nh4_leaching_sl;
        lvector_t< double >  accumulated_n_no3_leaching_sl;
        lvector_t< double >  accumulated_n_nh3_leaching_sl;
        lvector_t< double >  accumulated_n_don_leaching_sl;
        lvector_t< double >  accumulated_n_urea_leaching_sl;
        lvector_t< double >  accumulated_n_no_leaching_sl;
        lvector_t< double >  accumulated_n_n2o_leaching_sl;

        lvector_t< double >  accumulated_n_urea_liq_diffusion_sl;
        lvector_t< double >  accumulated_n_nh3_liq_diffusion_sl;
        lvector_t< double >  accumulated_n_nh4_liq_diffusion_sl;
        lvector_t< double >  accumulated_n_no3_liq_diffusion_sl;

        lvector_t< double >  accumulated_n_nh4_infiltration_phys_sl;
        lvector_t< double >  accumulated_n_nh3_infiltration_phys_sl;
        lvector_t< double >  accumulated_n_urea_infiltration_phys_sl;
        lvector_t< double >  accumulated_n_no3_infiltration_phys_sl;
        lvector_t< double >  accumulated_n_don_infiltration_phys_sl;
        lvector_t< double >  accumulated_n_aorg_infiltration_phys_sl;

        lvector_t< double >  accumulated_n_nh4_infiltration_leach_sl;
        lvector_t< double >  accumulated_n_nh3_infiltration_leach_sl;
        lvector_t< double >  accumulated_n_urea_infiltration_leach_sl;
        lvector_t< double >  accumulated_n_no3_infiltration_leach_sl;
        lvector_t< double >  accumulated_n_don_infiltration_leach_sl;
        lvector_t< double >  accumulated_n_no_infiltration_leach_sl;
        lvector_t< double >  accumulated_n_n2o_infiltration_leach_sl;

        lvector_t< double >  accumulated_n_nh4_infiltration_liqdif_sl;
        lvector_t< double >  accumulated_n_nh3_infiltration_liqdif_sl;
        lvector_t< double >  accumulated_n_urea_infiltration_liqdif_sl;
        lvector_t< double >  accumulated_n_no3_infiltration_liqdif_sl;

        lvector_t< double >  accumulated_n_nh3_bubbling_sl;
        lvector_t< double >  accumulated_n_no_bubbling_sl;
        lvector_t< double >  accumulated_n_n2o_bubbling_sl;
        lvector_t< double >  accumulated_n_nh3_bubbling_sbl;
        lvector_t< double >  accumulated_n_no_bubbling_sbl;
        lvector_t< double >  accumulated_n_n2o_bubbling_sbl;

        lvector_t< double >  accumulated_n_nh3_dissolution_sbl;
        lvector_t< double >  accumulated_n_no_dissolution_sbl;
        lvector_t< double >  accumulated_n_n2o_dissolution_sbl;

        lvector_t< double >  accumulated_n_nh3_phys_diffusion_sl;
        lvector_t< double >  accumulated_n_no_phys_diffusion_sl;
        lvector_t< double >  accumulated_n_n2o_phys_diffusion_sl;
        lvector_t< double >  accumulated_n_nh3_gas_diffusion_sl;
        lvector_t< double >  accumulated_n_no_gas_diffusion_sl;
        lvector_t< double >  accumulated_n_n2o_gas_diffusion_sl;

        lvector_t< double >  accumulated_n_micro_perturbation_sl;
        lvector_t< double >  accumulated_n_aorg_perturbation_sl;
        lvector_t< double >  accumulated_n_litter_perturbation_sl;
        lvector_t< double >  accumulated_n_humus_1_perturbation_sl;
        lvector_t< double >  accumulated_n_humus_2_perturbation_sl;
        lvector_t< double >  accumulated_n_humus_3_perturbation_sl;

        lvector_t< double >  accumulated_n_nh4_no2_nitrification_sl;
        lvector_t< double >  accumulated_n_nh4_no_nitrification_sl;
        lvector_t< double >  accumulated_n_nh4_n2o_nitrification_sl;
        lvector_t< double >  accumulated_n_no2_no3_nitrification_sl;

        lvector_t< double >  accumulated_n_no3_no2_denitrification_sl;
        lvector_t< double >  accumulated_n_no3_no_denitrification_sl;
        lvector_t< double >  accumulated_n_no3_n2o_denitrification_sl;
        lvector_t< double >  accumulated_n_no3_n2_denitrification_sl;
        lvector_t< double >  accumulated_n_no2_no_denitrification_sl;
        lvector_t< double >  accumulated_n_no2_n2o_denitrification_sl;
        lvector_t< double >  accumulated_n_no2_n2_denitrification_sl;
        lvector_t< double >  accumulated_n_no_n2o_denitrification_sl;
        lvector_t< double >  accumulated_n_no_n2_denitrification_sl;
        lvector_t< double >  accumulated_n_n2o_n2_denitrification_sl;

        lvector_t< double >  accumulated_n_no2_chemodenitrification_sl;

        lvector_t< double >  accumulated_n_mic_naorg_decay_sl;
        lvector_t< double >  accumulated_n_mic_nh4_mineral_sl;
        lvector_t< double >  accumulated_n_mic_don_dissolve_sl;

        lvector_t< double >  accumulated_n_aorg_nh4_mineral_sl;
        lvector_t< double >  accumulated_n_aorg_don_dissolve_sl;

        lvector_t< double >  accumulated_n_humus_1_don_dissolve_sl;
        lvector_t< double >  accumulated_n_humus_1_nh4_mineral_sl;
        lvector_t< double >  accumulated_n_humus_2_don_dissolve_sl;
        lvector_t< double >  accumulated_n_humus_2_nh4_mineral_sl;
        lvector_t< double >  accumulated_n_humus_3_don_dissolve_sl;
        lvector_t< double >  accumulated_n_humus_3_nh4_mineral_sl;

        lvector_t< double >  accumulated_n_litter_don_dissolve_sl;
        lvector_t< double >  accumulated_n_litter_nh4_mineral_sl;

        lvector_t< double >  accumulated_n_don_humus_1_humify_sl;
        lvector_t< double >  accumulated_n_don_humus_2_humify_sl;
        lvector_t< double >  accumulated_n_don_humus_3_humify_sl;
        lvector_t< double >  accumulated_n_litter_humus_1_humify_sl;
};

} /*namespace ldndc*/

#undef  LMOD_OUTPUT_MODULE_ID
#undef  LMOD_OUTPUT_MODULE_DESC

#endif  /*  !LM_OUTPUT_SOILCHEMISTRYMETRX_FLUXES_LAYER_SUBDAILY_H_  */
