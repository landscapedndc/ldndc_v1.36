/*!
 * @file
 * @author
 *  David Kraus
 */

#include  "soilchemistry/metrx/output-soilchemistry-metrx.h"

#define  LMOD_OUTPUT_MODULE_BASE  MBE_LegacyModel
#define  LMOD_OUTPUT_MODULE_NAME  OutputSoilchemistryMeTrX
#define  LMOD_OUTPUT_MODULE_TIMEMODE  TMODE_POST_DAILY

LMOD_MODULE_INFO(LMOD_OUTPUT_MODULE_NAME,LMOD_OUTPUT_MODULE_TIMEMODE,LMOD_FLAG_USER|LMOD_FLAG_OUTPUT);

REGISTER_OPTION(OutputSoilchemistryMeTrX, output,"Controls if any output is written  [bool] {on}");
REGISTER_OPTION(OutputSoilchemistryMeTrX, outputdaily,"Controls if daily output is written  [bool] {on}");
REGISTER_OPTION(OutputSoilchemistryMeTrX, outputyearly,"Controls if yearly output is written  [bool] {on}");
REGISTER_OPTION(OutputSoilchemistryMeTrX, outputlayerdaily,"Controls if daily layer output is written  [bool] {on}");
REGISTER_OPTION(OutputSoilchemistryMeTrX, outputlayeryearly,"Controls if yearly layer output is written  [bool] {on}");
REGISTER_OPTION(OutputSoilchemistryMeTrX, outputsubdaily,"Controls if subdaily output is written  [bool] {on}");
REGISTER_OPTION(OutputSoilchemistryMeTrX, outputlayersubdaily,"Controls if subdaily layer output is written  [bool] {on}");
REGISTER_OPTION(OutputSoilchemistryMeTrX, outputfluxes,"Controls if fluxes output is written  [bool] {on}");
REGISTER_OPTION(OutputSoilchemistryMeTrX, outputpools,"Controls if pools output is written  [bool] {on}");

namespace ldndc {

LMOD_OUTPUT_MODULE_NAME::LMOD_OUTPUT_MODULE_NAME(
        MoBiLE_State *  _state,
       cbm::io_kcomm_t *  _io,
        timemode_e  _timemode)
        : LMOD_OUTPUT_MODULE_BASE( _state, _timemode),
          output_writer_daily( _state, _io, _timemode),
          output_writer_yearly( _state, _io, _timemode),
          output_writer_layer_daily( _state, _io, _timemode),
          output_writer_layer_yearly( _state, _io, _timemode),
          output_writer_fluxes( _state, _io, _timemode),
          output_writer_pools( _state, _io, _timemode),
          output_writer_subdaily( _state, _io, _timemode)
{}


LMOD_OUTPUT_MODULE_NAME::~LMOD_OUTPUT_MODULE_NAME()
{}



size_t
LMOD_OUTPUT_MODULE_NAME::record_size(
        writer_id_e  _writer_id)
const
{
    size_t  record_sz = invalid_size;

    switch ( _writer_id)
    {
        case  WRITER_DAILY:
            if ( output_writer_daily.active())
            {
                record_sz = output_writer_daily.record_size();
            }
            else
            {
                record_sz = 0;
            }
            break;
        case  WRITER_YEARLY:
            if ( output_writer_yearly.active())
            {
                record_sz = output_writer_yearly.record_size();
            }
            else
            {
                record_sz = 0;
            }
            break;
        case  WRITER_DAILY_LAYER:
            if ( output_writer_layer_daily.active())
            {
                record_sz = output_writer_layer_daily.record_size();
            }
            else
            {
                record_sz = 0;
            }
            break;
        case  WRITER_YEARLY_LAYER:
            if ( output_writer_layer_yearly.active())
            {
                record_sz = output_writer_layer_yearly.record_size();
            }
            else
            {
                record_sz = 0;
            }
            break;
        case  WRITER_FLUXES:
            if ( output_writer_fluxes.active())
            {
                record_sz = output_writer_fluxes.record_size();
            }
            else
            {
                record_sz = 0;
            }
            break;
        case  WRITER_POOLS:
            if ( output_writer_pools.active())
            {
                record_sz = output_writer_pools.record_size();
            }
            else
            {
                record_sz = 0;
            }
            break;
        case  WRITER_SUBDAILY:
            if ( output_writer_subdaily.active())
            {
                record_sz = output_writer_subdaily.record_size();
            }
            else
            {
                record_sz = 0;
            }
            break;
        default:
            cbm::invalidate( record_sz);
    }

    return  record_sz;
}
size_t
LMOD_OUTPUT_MODULE_NAME::max_record_size()
const
{
    size_t  max_record_sz = 0;
    for ( int  w = 0;  w < WRITER_CNT;  ++w)
    {
        size_t const  record_sz = this->record_size( (writer_id_e)w);
        if ( cbm::is_valid( record_sz))
        {
            max_record_sz = std::max( record_sz, max_record_sz);
        }
    }

    return  max_record_sz;
}

lerr_t
LMOD_OUTPUT_MODULE_NAME::configure_output(
                                          MBE_LegacyOutputModel *  _module,
                                          ldndc::config_file_t const *  _config,
                                          char const *  _optionkey)
{
    lerr_t  rc_conf = LDNDC_ERR_OK;

    /*configuration switches*/
    bool  have_output_config;
    CF_LMOD_QUERY( _config, _optionkey, have_output_config, true);

    /*model setup switches*/
    bool have_output = this->get_option< bool >( _optionkey, true);


    if ( have_output_config && have_output)
    {
        rc_conf = _module->configure( _config);
    }
    else
    {
        _module->set_active_off();
    }
    return  rc_conf;
}



lerr_t
LMOD_OUTPUT_MODULE_NAME::configure(
                                   ldndc::config_file_t const *  _cf)
{
    bool  output_on /*global switch (configuration, setup)*/;
    CF_LMOD_QUERY( _cf, "output", output_on, true);
    output_on = output_on && this->get_option< bool >( "output", true);

    lerr_t  rc_conf = LDNDC_ERR_OK;
    if ( output_on)
    {
        if ( !rc_conf)
        {
            rc_conf = this->configure_output( &this->output_writer_daily, _cf, "outputdaily");
        }
        if ( !rc_conf)
        {
            rc_conf = this->configure_output( &this->output_writer_yearly, _cf, "outputyearly");
        }
        if ( !rc_conf)
        {
            rc_conf = this->configure_output( &this->output_writer_layer_daily, _cf, "outputlayerdaily");
        }
        if ( !rc_conf)
        {
            rc_conf = this->configure_output( &this->output_writer_layer_yearly, _cf, "outputlayeryearly");
        }
        if ( !rc_conf)
        {
            rc_conf = this->configure_output( &this->output_writer_fluxes, _cf, "outputfluxes");
        }
        if ( !rc_conf)
        {
            rc_conf = this->configure_output( &this->output_writer_pools, _cf, "outputpools");
        }
        if ( !rc_conf)
        {
            rc_conf = this->configure_output( &this->output_writer_subdaily, _cf, "outputsubdaily");
        }
    }
    else
    {
        this->output_writer_daily.set_active_off();
        this->output_writer_yearly.set_active_off();
        this->output_writer_layer_daily.set_active_off();
        this->output_writer_layer_yearly.set_active_off();
        this->output_writer_fluxes.set_active_off();
        this->output_writer_pools.set_active_off();
        this->output_writer_subdaily.set_active_off();
    }

    /* deactivate output writer manager, when no output module
     * is active */
    if ( !( this->output_writer_daily.active() ||
            this->output_writer_yearly.active() ||
            this->output_writer_layer_daily.active() ||
            this->output_writer_layer_yearly.active() ||
            this->output_writer_fluxes.active() ||
            this->output_writer_pools.active() ||
            this->output_writer_subdaily.active()))
    {
        this->set_active_off();
    }

    return  rc_conf;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::initialize()
{
    lerr_t  rc_ini = LDNDC_ERR_OK;

    if ( output_writer_daily.active())
    {
        rc_ini = output_writer_daily.initialize();
    }
    if ( output_writer_yearly.active())
    {
        rc_ini = output_writer_yearly.initialize();
    }
    if ( !rc_ini && output_writer_layer_daily.active())
    {
        rc_ini = output_writer_layer_daily.initialize();
    }
    if ( !rc_ini && output_writer_layer_yearly.active())
    {
        rc_ini = output_writer_layer_yearly.initialize();
    }
    if ( !rc_ini && output_writer_fluxes.active())
    {
        rc_ini = output_writer_fluxes.initialize();
    }
    if ( !rc_ini && output_writer_pools.active())
    {
        rc_ini = output_writer_pools.initialize();
    }
    if ( !rc_ini && output_writer_subdaily.active())
    {
        rc_ini = output_writer_subdaily.initialize();
    }
    return  rc_ini;
}

lerr_t
LMOD_OUTPUT_MODULE_NAME::solve()
{
    KLOGFATAL( "do not use solve(), we rock with write()");
    return  LDNDC_ERR_FAIL;

}

lerr_t
LMOD_OUTPUT_MODULE_NAME::write_daily(
                                     ldndc_flt64_t *  _data)
{
    if ( output_writer_daily.active())
    {
        return  output_writer_daily.write_results( _data);
    }
    return  LDNDC_ERR_OK;
}

lerr_t
LMOD_OUTPUT_MODULE_NAME::write_yearly(
                                     ldndc_flt64_t *  _data)
{
    if ( output_writer_yearly.active())
    {
        return  output_writer_yearly.write_results( _data);
    }
    return  LDNDC_ERR_OK;
}

lerr_t
LMOD_OUTPUT_MODULE_NAME::write_layer_daily(
                                           unsigned int  _layer, 
                                           ldndc_flt64_t *  _data)
{
    if ( output_writer_layer_daily.active())
    {
        return  output_writer_layer_daily.write_results( _layer, _data);
    }
    return  LDNDC_ERR_OK;
}

lerr_t
LMOD_OUTPUT_MODULE_NAME::write_layer_yearly(
                                           unsigned int  _layer,
                                           ldndc_flt64_t *  _data)
{
    if ( output_writer_layer_yearly.active())
    {
        return  output_writer_layer_yearly.write_results( _layer, _data);
    }
    return  LDNDC_ERR_OK;
}

lerr_t
LMOD_OUTPUT_MODULE_NAME::write_fluxes(
                                      unsigned int  _layer,
                                      ldndc_flt64_t *  _data)
{
    if ( output_writer_fluxes.active())
    {
        return  output_writer_fluxes.write_results( _layer, _data);
    }
    return  LDNDC_ERR_OK;
}

lerr_t
LMOD_OUTPUT_MODULE_NAME::write_pools(
                                      unsigned int  _layer,
                                      ldndc_flt64_t *  _data)
{
    if ( output_writer_pools.active())
    {
        return  output_writer_pools.write_results( _layer, _data);
    }
    return  LDNDC_ERR_OK;
}

lerr_t
LMOD_OUTPUT_MODULE_NAME::write_subdaily(
                                        ldndc_flt64_t *  _data)
{
    if ( output_writer_subdaily.active())
    {
        return  output_writer_subdaily.write_results( _data);
    }
    return  LDNDC_ERR_OK;
}

lerr_t
LMOD_OUTPUT_MODULE_NAME::finalize()
{
    if ( output_writer_daily.active())
    {
        output_writer_daily.finalize();
    }
    if ( output_writer_yearly.active())
    {
        output_writer_yearly.finalize();
    }
    if ( output_writer_layer_daily.active())
    {
        output_writer_layer_daily.finalize();
    }
    if ( output_writer_layer_yearly.active())
    {
        output_writer_layer_yearly.finalize();
    }
    if ( output_writer_fluxes.active())
    {
        output_writer_fluxes.finalize();
    }
    if ( output_writer_pools.active())
    {
        output_writer_pools.finalize();
    }
    if ( output_writer_subdaily.active())
    {
        output_writer_subdaily.finalize();
    }
    return  LDNDC_ERR_OK;
}

} /*namespace ldndc*/

#undef  LMOD_OUTPUT_MODULE_BASE
#undef  LMOD_OUTPUT_MODULE_NAME
#undef  LMOD_OUTPUT_MODULE_TIMEMODE

#undef  OutputSoilchemistryMeTrXDaily_Rank
#undef  OutputSoilchemistryMeTrXDaily_Datasize

