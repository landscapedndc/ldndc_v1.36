/*!
 * @file
 * @author
 *  David Kraus
 */

#include  "soilchemistry/metrx/output-soilchemistry-metrx-fluxes-layer-subdaily.h"

LMOD_MODULE_INFO(OutputSoilchemistryMeTrXFluxes,TMODE_SUBDAILY,LMOD_FLAG_USER|LMOD_FLAG_OUTPUT);
namespace ldndc {

ldndc_string_t const  OutputSoilchemistryMeTrXFluxes_Ids[] =
{
    "level",

    "nh4_throughfall",
    "no3_throughfall",
    "nh4_throughfall_sbl",
    "no3_throughfall_sbl",

    //N fluxes in plant/algae
    "n_to_living_plant_and_algae_from_extern",
    "plant_nh4_uptake",
    "plant_no3_uptake",
    "plant_nh3_uptake",
    "plant_don_uptake",
    "algae_nh4_uptake_sbl",
    "algae_no3_uptake_sbl",
    "algae_nh3_uptake_sbl",

    //N fluxes from plant
    "litter_from_plants_to_raw_litter_below",
    "litter_from_plants_to_wood_litter_below",
    "litter_from_plants_to_raw_litter_above",
    "litter_from_plants_to_wood_litter_above",
    "litter_from_plants_to_stubble_litter",

    //N fluxes from raw to soil litter
    "litter_from_raw_litter_to_soil_litter_above",
    "litter_from_raw_litter_to_soil_litter_below",

    //N fluxes from grazer and algae
    "litter_from_algae",
    "litter_from_dung",
    "urea_from_dung",

    //N export via harvest, cutting and grazing
    "n_export_harvest_cutting_grazing",

    //N fluxes from fertilization and manuring
    "nh4_fertilization",
    "no3_fertilization",
    "nh3_fertilization",
    "don_fertilization",
    "urea_fertilization",
    "n_litter_fertilization",
    "n_aorg_fertilization",
    "nh4_fertilization_sbl",
    "no3_fertilization_sbl",
    "nh3_fertilization_sbl",
    "urea_fertilization_sbl",

    //N redistribution during tilling
    "surface_litter_incorporation_via_tilling",
    "litter_tilling",
    "naorg_tilling",
    "humus_1_tilling",
    "humus_2_tilling",
    "humus_3_tilling",
    "no3_tilling",
    "no2_tilling",
    "no_tilling",
    "n2o_tilling",
    "urea_tilling",
    "don_tilling",
    "nh4_tilling",
    "nh3_tilling",
    "nmicrobes_tilling",

    //N redistribution during spin up
    "humus_1_spinup",
    "humus_2_spinup",
    "humus_3_spinup",
    "surface_litter_spinup",
    "litter_spinup",

    //N fluxes due to water movement
    "nmicro_leaching",
    "naorg_leaching",
    "litter_leaching",
    "humus_1_leaching",
    "humus_2_leaching",
    "humus_3_leaching",
    "nh4_leaching",
    "no3_leaching",
    "nh3_leaching",
    "don_leaching",
    "urea_leaching",
    "no_leaching",
    "n2o_leaching",

    //liquid diffusion
    "urea_liqdiffusion",
    "nh3_liqdiffusion",
    "nh4_liqdiffusion",
    "no3_liqdiffusion",

    //fluxes due to disappearing surface water
    "nh4_infiltration_phys",
    "nh3_infiltration_phys",
    "urea_infiltration_phys",
    "no3_infiltration_phys",
    "don_infiltration_phys",
    "naorg_infiltration_phys",

    //leaching in from surface water
    "nh4_infiltration_leach",
    "nh3_infiltration_leach",
    "urea_infiltration_leach",
    "no3_infiltration_leach",
    "don_infiltration_leach",
    "no_infiltration_leach",
    "n2o_infiltration_leach",

    //in/outflow via liquid diffusion between surface water and soil
    "nh4_infiltration_liqdif",
    "nh3_infiltration_liqdif",
    "urea_infiltration_liqdif",
    "no3_infiltration_liqdif",

    //fluxes due to ebullition
    "nh3_bubbling",
    "no_bubbling",
    "n2o_bubbling",
    "nh3_bubbling_sbl",
    "no_bubbling_sbl",
    "n2o_bubbling_sbl",

    //N fluxes due to dissolution between atmosphere and soil system
    "nh3_dissolution_sbl",
    "no_dissolution_sbl",
    "n2o_dissolution_sbl",

    //N fluxes from urea to nh4
    "urea_nh4_hydrolysis",
    "urea_nh4_hydrolysis_sbl",

    "nh4_assimilation",
    "no3_assimilation",
    "don_assimilation",

    "nh4_nh3_conversion",
    "nh3_nh4_conversion",
    "nh4_nh3_conversion_sbl",
    "nh3_nh4_conversion_sbl",

    //gaseous diffusion
    "nh3_phys_diffusion",
    "no_phys_diffusion",
    "n2o_phys_diffusion",
    "nh3_gas_diffusion",
    "no_gas_diffusion",
    "n2o_gas_diffusion",

    "nmicro_perturbation",
    "naorg_perturbation",
    "litter_perturbation",
    "humus_1_perturbation",
    "humus_2_perturbation",
    "humus_3_perturbation",

    "nh4_no2_nitrification",
    "nh4_no_nitrification",
    "nh4_n2o_nitrification",
    "no2_no3_nitrification",

    "no3_no2_denitrification",
    "no3_no_denitrification",
    "no3_n2o_denitrification",
    "no3_n2_denitrification",
    "no2_no_denitrification",
    "no2_n2o_denitrification",
    "no2_n2_denitrification",
    "no_n2o_denitrification",
    "no_n2_denitrification",
    "n2o_n2_denitrification",

    "no2_no_chemodenitrification",

    "nmic_naorg_decay",
    "nmic_nh4_mineral",
    "nmic_don_dissolve",

    "naorg_nh4_mineral",
    "naorg_don_dissolve",

    "humus_1_don_dissolve",
    "humus_1_nh4_mineral",
    "humus_2_don_dissolve",
    "humus_2_nh4_mineral",
    "humus_3_don_dissolve",
    "humus_3_nh4_mineral",

    "litter_don_dissolve",
    "litter_nh4_mineral",

    "don_humus_1_humify",
    "don_humus_2_humify",
    "don_humus_3_humify",
    "litter_humus_1_humify"
};

ldndc_string_t const *  OutputSoilchemistryMeTrXFluxes_Header =
    OutputSoilchemistryMeTrXFluxes_Ids;


#define  OutputSoilchemistryMeTrXFluxes_Datasize  (sizeof( OutputSoilchemistryMeTrXFluxes_Ids) / sizeof( OutputSoilchemistryMeTrXFluxes_Ids[0]))
ldndc_output_size_t const  OutputSoilchemistryMeTrXFluxes_Sizes[] =
{
    OutputSoilchemistryMeTrXFluxes_Datasize,

    OutputSoilchemistryMeTrXFluxes_Datasize /*total size*/
};



ldndc_output_size_t const *  OutputSoilchemistryMeTrXFluxes_EntitySizes = NULL;

#define  OutputSoilchemistryMeTrXFluxes_Rank  ((ldndc_output_rank_t)(sizeof( OutputSoilchemistryMeTrXFluxes_Sizes) / sizeof( OutputSoilchemistryMeTrXFluxes_Sizes[0])) - 1)
atomic_datatype_t const  OutputSoilchemistryMeTrXFluxes_Types[] =
{
    LDNDC_FLOAT64
};



OutputSoilchemistryMeTrXFluxes::OutputSoilchemistryMeTrXFluxes(
                                                               MoBiLE_State *  _state,
                                                               cbm::io_kcomm_t *  _io_kcomm,
                                                               timemode_e  _timemode)
                                    : MBE_LegacyOutputModel( _state, _io_kcomm, _timemode),
                                    m_iokcomm( _io_kcomm),
                                    sl_( _io_kcomm->get_input_class_ref< input_class_soillayers_t >()),
                                    sb_( _state->get_substate_ref< substate_surfacebulk_t >()),

                                    accumulated_n_nh4_throughfall_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_no3_throughfall_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_nh4_throughfall_sbl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_no3_throughfall_sbl( sl_.soil_layer_cnt(), 0.0),

                                    accumulated_n_to_living_plant_and_algae_from_extern_sl( sl_.soil_layer_cnt(), 0.0),

                                    accumulated_n_plant_nh4_uptake_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_plant_no3_uptake_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_plant_nh3_uptake_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_plant_don_uptake_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_algae_nh4_uptake_sbl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_algae_no3_uptake_sbl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_algae_nh3_uptake_sbl( sl_.soil_layer_cnt(), 0.0),

                                    accumulated_n_litter_from_plants_below_rawlitter_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_litter_from_plants_below_wood_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_litter_from_plants_above_rawlitter( 0.0),
                                    accumulated_n_litter_from_plants_above_wood( 0.0),
                                    accumulated_n_litter_from_plants_above_stubble( 0.0),
                                    accumulated_n_aboveground_raw_litter_fragmentation_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_belowground_raw_litter_fragmentation_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_litter_from_algae_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_litter_from_dung_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_urea_from_dung_sl( sl_.soil_layer_cnt(), 0.0),

                                    accumulated_n_export_harvest_cutting_grazing( 0.0),

                                    accumulated_n_nh4_fertilization_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_no3_fertilization_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_nh3_fertilization_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_don_fertilization_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_urea_fertilization_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_litter_fertilization_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_aorg_fertilization_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_nh4_fertilization_sbl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_no3_fertilization_sbl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_nh3_fertilization_sbl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_urea_fertilization_sbl( sl_.soil_layer_cnt(), 0.0),

                                    accumulated_n_surface_litter_incorporation_via_tilling_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_litter_tilling_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_aorg_tilling_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_humus_1_tilling_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_humus_2_tilling_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_humus_3_tilling_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_no3_tilling_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_no2_tilling_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_no_tilling_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_n2o_tilling_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_urea_tilling_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_don_tilling_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_nh4_tilling_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_nh3_tilling_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_microbes_tilling_sl( sl_.soil_layer_cnt(), 0.0),

                                    accumulated_n_humus_1_spinup_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_humus_2_spinup_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_humus_3_spinup_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_surface_litter_spinup_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_litter_spinup_sl( sl_.soil_layer_cnt(), 0.0),

                                    accumulated_n_urea_nh4_hydrolysis_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_urea_nh4_hydrolysis_sbl( sl_.soil_layer_cnt(), 0.0),

                                    accumulated_n_nh4_assimilation_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_no3_assimilation_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_don_assimilation_sl( sl_.soil_layer_cnt(), 0.0),

                                    accumulated_n_nh4_nh3_conversion_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_nh3_nh4_conversion_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_nh4_nh3_conversion_sbl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_nh3_nh4_conversion_sbl( sl_.soil_layer_cnt(), 0.0),

                                    accumulated_n_micro_leaching_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_aorg_leaching_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_litter_leaching_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_humus_1_leaching_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_humus_2_leaching_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_humus_3_leaching_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_nh4_leaching_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_no3_leaching_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_nh3_leaching_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_don_leaching_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_urea_leaching_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_no_leaching_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_n2o_leaching_sl( sl_.soil_layer_cnt(), 0.0),

                                    accumulated_n_urea_liq_diffusion_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_nh3_liq_diffusion_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_nh4_liq_diffusion_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_no3_liq_diffusion_sl( sl_.soil_layer_cnt(), 0.0),

                                    accumulated_n_nh4_infiltration_phys_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_nh3_infiltration_phys_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_urea_infiltration_phys_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_no3_infiltration_phys_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_don_infiltration_phys_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_aorg_infiltration_phys_sl( sl_.soil_layer_cnt(), 0.0),

                                    accumulated_n_nh4_infiltration_leach_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_nh3_infiltration_leach_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_urea_infiltration_leach_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_no3_infiltration_leach_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_don_infiltration_leach_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_no_infiltration_leach_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_n2o_infiltration_leach_sl( sl_.soil_layer_cnt(), 0.0),

                                    accumulated_n_nh4_infiltration_liqdif_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_nh3_infiltration_liqdif_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_urea_infiltration_liqdif_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_no3_infiltration_liqdif_sl( sl_.soil_layer_cnt(), 0.0),

                                    accumulated_n_nh3_bubbling_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_no_bubbling_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_n2o_bubbling_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_nh3_bubbling_sbl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_no_bubbling_sbl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_n2o_bubbling_sbl( sl_.soil_layer_cnt(), 0.0),

                                    accumulated_n_nh3_dissolution_sbl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_no_dissolution_sbl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_n2o_dissolution_sbl( sl_.soil_layer_cnt(), 0.0),

                                    accumulated_n_nh3_phys_diffusion_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_no_phys_diffusion_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_n2o_phys_diffusion_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_nh3_gas_diffusion_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_no_gas_diffusion_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_n2o_gas_diffusion_sl( sl_.soil_layer_cnt(), 0.0),

                                    accumulated_n_micro_perturbation_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_aorg_perturbation_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_litter_perturbation_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_humus_1_perturbation_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_humus_2_perturbation_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_humus_3_perturbation_sl( sl_.soil_layer_cnt(), 0.0),

                                    accumulated_n_nh4_no2_nitrification_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_nh4_no_nitrification_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_nh4_n2o_nitrification_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_no2_no3_nitrification_sl( sl_.soil_layer_cnt(), 0.0),

                                    accumulated_n_no3_no2_denitrification_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_no3_no_denitrification_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_no3_n2o_denitrification_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_no3_n2_denitrification_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_no2_no_denitrification_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_no2_n2o_denitrification_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_no2_n2_denitrification_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_no_n2o_denitrification_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_no_n2_denitrification_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_n2o_n2_denitrification_sl( sl_.soil_layer_cnt(), 0.0),

                                    accumulated_n_no2_chemodenitrification_sl( sl_.soil_layer_cnt(), 0.0),

                                    accumulated_n_mic_naorg_decay_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_mic_nh4_mineral_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_mic_don_dissolve_sl( sl_.soil_layer_cnt(), 0.0),

                                    accumulated_n_aorg_nh4_mineral_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_aorg_don_dissolve_sl( sl_.soil_layer_cnt(), 0.0),

                                    accumulated_n_humus_1_don_dissolve_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_humus_1_nh4_mineral_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_humus_2_don_dissolve_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_humus_2_nh4_mineral_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_humus_3_don_dissolve_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_humus_3_nh4_mineral_sl( sl_.soil_layer_cnt(), 0.0),

                                    accumulated_n_litter_don_dissolve_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_litter_nh4_mineral_sl( sl_.soil_layer_cnt(), 0.0),

                                    accumulated_n_don_humus_1_humify_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_don_humus_2_humify_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_don_humus_3_humify_sl( sl_.soil_layer_cnt(), 0.0),
                                    accumulated_n_litter_humus_1_humify_sl( sl_.soil_layer_cnt(), 0.0)
{ }

OutputSoilchemistryMeTrXFluxes::~OutputSoilchemistryMeTrXFluxes()
{ }


size_t
OutputSoilchemistryMeTrXFluxes::record_size()
const
{
    return  OutputSoilchemistryMeTrXFluxes_Datasize;
}



lerr_t
OutputSoilchemistryMeTrXFluxes::configure(
                                          ldndc::config_file_t const *  _cf)
{
    if ( _cf && !_cf->have_output())
    {
        return  LDNDC_ERR_OK;
    }

    lerr_t  rc_setflags = set_metaflags( _cf, RM_DEFAULT_LAYERSUBDAILY);
    if ( rc_setflags){ return  LDNDC_ERR_FAIL; }

    m_sink = m_iokcomm->sink_handle_acquire( "metrxfluxes");
    if ( m_sink.status() == LDNDC_ERR_OK)
    {
        lerr_t  rc_layout = LDNDC_OUTPUT_DEFINE_SINK_LAYOUT_DEFAULTS( m_sink, OutputSoilchemistryMeTrXFluxes);
        RETURN_IF_NOT_OK(rc_layout);
    }
    else
    {
        this->set_active_off();
    }

    return  LDNDC_ERR_OK;
}



lerr_t
OutputSoilchemistryMeTrXFluxes::initialize()
{
    return  LDNDC_ERR_OK;
}



lerr_t
OutputSoilchemistryMeTrXFluxes::solve()
{
    KLOGDEBUG( "metrx record size=", record_size());
    KLOGFATAL( "do not use solve(), we rock with write_results()");
    return  LDNDC_ERR_FAIL;

}



lerr_t
OutputSoilchemistryMeTrXFluxes::write_results(
        unsigned int  _layer, ldndc_flt64_t *  _data)
{
    if ( !_data || !m_sink.is_acquired())
    {
        /* we assume there is nothing to dump ... */
        return  LDNDC_ERR_OK;
    }

    /* output */
    void *  data[] = { _data};
    this->set_layernumber( -static_cast< int >( _layer)-1);
    lerr_t  rc_write =
        this->write_fixed_record( &this->m_sink, data);
    if ( rc_write)
        { return  LDNDC_ERR_FAIL; }
    return  LDNDC_ERR_OK;
}



lerr_t
OutputSoilchemistryMeTrXFluxes::finalize()
{
    return  m_iokcomm->sink_handle_release( &m_sink);
}

} /*namespace ldndc*/

#undef  OutputSoilchemistryMeTrXFluxes_Rank
#undef  OutputSoilchemistryMeTrXFluxes_Datasize
