/*!
 * @file
 * @author
 *  David Kraus (created on: may 12, 2014)
 * 
 */

#include  "soilchemistry/metrx/soilchemistry-metrx.h"
#include  <constants/lconstants-chem.h>
#include  <scientific/meteo/ld_meteo.h>

namespace ldndc {

/*!
 * @page metrx
 * @section metrx_nitrification Nitrification
 * Nitrification is modeled as a two-stage process:
 * \f[
 * NH_4^+ \rightarrow NO_2^- \rightarrow NO_3^-
 * \f] 
 * depending on microbial biomass.
 */
void
SoilChemistryMeTrX::MeTrX_nitrification()
{
    for( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
    {
        double const aevf( 1.0 - sc_.anvf_sl[sl]);
        double const o2_tot( o2_gas_sl[sl] + o2_liq_sl[sl]);

        // NH3 → NH2OH/HNO → NO2− → NO3−
        // NH3 → NH2OH/HNO → NO2− (amoA gene)
        /*!
         * @page metrx
         * @subsection metrx_nitrification_nh4 NH4 nitrification
         *
         * The first step of Nitrification is given by:
         * \f[
         * \frac{d NH_4^+}{dt} = -c_{mic} a_{mic} \mu_{mic} \phi_{NH_4^+} \phi_{O_2} \phi_{ph} \phi_{ni}
         * \f]
         * \f$ c_{mic}: \f$ Microbial biomass \n
         * \f$ a_{mic}: \f$ Microbial activity \n
         * \f$ \mu_{mic}: \f$ Microbial growth rate \n
         * \f$ \phi_{NH_4^+}: \f$ Microbial growth dependency on DOC \n
         * \f$ \phi_{O_2}: \f$ Microbial growth dependency on O2 \n
         * \f$ \phi_{ph}: \f$ Microbial growth dependency on NH4 \n
         * \f$ \phi_{ni}: \f$ Effect of nitrification inhibitor \n
         * \n
         * The activity coefficient of nitrifier \f$ a_{mic} \f$ is given by a harmonic mean
         * of a soil temperature (\f$ \phi_{T}\f$) and a soil water (\f$ \phi_{wfps}\f$) depending response coefficient.
         *
         * \f[
         * a_{mic} = \frac{\phi_{T} + \phi_{wfps}}{\frac{1}{\phi_{T}} + \frac{1}{\phi_{wfps}}}
         * \f]
         * with \f$ \phi_{T}\f$ given by:
         * \f[
         * \phi_{T} = e^{-METRX\_F\_MIC\_T\_EXP\_1 \cdot \left( \frac{1 - T}{METRX\_F\_MIC\_T\_EXP\_2} \right)^2}
         * \f]
         *
         * @image html metrx_denitrification_on_temperature.png "Response function depending on temperature" width=900
         * @image latex metrx_denitrification_on_temperature.png "Response function depending on temperature"
         *
         * and with \f$ \phi_{wfps}\f$ given by:
         * \f[
         * \phi_{wfps} = 1 - \frac{1}{1 + e^{(wfps - METRX\_F\_MIC\_M\_WEIBULL\_1) \cdot METRX\_F\_MIC\_M\_WEIBULL\_2}}
         * \f]
         *
         * @image html metrx_denitrification_on_soilwater.png "Response function depending on soilwater" width=900
         * @image latex metrx_denitrification_on_soilwater.png "Response function depending on soilwater"
         * \n
         */
        double const active_microbes( sc_.C_micro1_sl[sl] * MeTrX_get_fact_tm_mic( sl) * sipar_.METRX_F_NIT_FRAC_MIC());

        /*!
         * @page metrx
         * The influence of \f$ NH_4^+ \f$ on nitrification of \f$ NH_4^+ \f$ is given by:
         * \f[
         * \phi_{NH_4^+} = \frac{NH_4^+}{NH_4^+ + METRX\_KMM\_NH4\_NIT}
         * \f]
         *
         * @image html metrx_nh4_nitrification_on_nh4.png "Nitrification of NH4 depending on NH4" width=500
         * @image latex metrx_nh4_nitrification_on_nh4.png "Nitrification of NH4 depending on NH4"
         * \n
         */
        double const fact_nh4( nh4_ae_sl[sl] / (MeTrX_get_k_mm_x_sl( sl, aevf * sipar_.METRX_KMM_NH4_NIT()) + nh4_ae_sl[sl]));

        /*!
         * The influence of \f$ O_2 \f$ on nitrification of \f$ NH_4^+ \f$ is given by:
         * \f[
         * \phi_{O_2} = \frac{O_2}{O_2 + METRX\_KMM\_O2\_NIT}
         * \f]
         *
         * @image html metrx_nh4_nitrification_on_o2.png "Nitrification of NH4 depending on O2" width=500
         * @image latex metrx_nh4_nitrification_on_o2.png "Nitrification of NH4 depending on O2"
         * \n
         */
        double const fact_o2( o2_tot / (MeTrX_get_k_mm_x_sl( sl, sipar_.METRX_KMM_O2_NIT()) + o2_tot));

        /*!
         * Nitrification is slightly negatively correlated with pH @cite booth:2005a. The influence of pH on
         * nitrification of \f$ NH_4^+ \f$ is given by:
         * \f[
         * \phi_{ph} = 1 - \frac{1}{1 + e^{METRX\_F\_NIT\_PH\_1 \cdot (pH - METRX\_F\_NIT\_PH\_2)}}
         * \f]
         *
         * @page metrx
         * @image html metrx_nh4_nitrification_on_ph.png "Nitrification of NH4 depending on pH" width=900
         * @image latex metrx_nh4_nitrification_on_ph.png "Nitrification of NH4 depending on pH"
         * \n
         */
        double const fact_ph( 1.0 - 1.0 / (1.0 + std::exp(-sipar_.METRX_F_NIT_PH_1() * (sc_.ph_sl[sl] - sipar_.METRX_F_NIT_PH_2()))));

        /*!
         * @page metrx
         * The influence of nitrification inhibitor is given by @ref soillibs_enhancedefficiencynitrogenfertilizers.
         */
        crnf_.update_nitrification_inhibition( mc_temp_sl[sl] * FTS_TOT_, sc_.ni_sl.sum(), sc_.ni_sl[sl]);
        double const fact_ni( cbm::bound_min( 0.0, 1.0 - crnf_.get_nitrification_inhibition( sc_.ni_sl[sl])));

        if ( cbm::flt_greater_zero( fact_nh4 * o2_tot * fact_ni))
        {
            /*!
             * @page metrx
             * @subsubsection metrx_nitrification_no_n2o Production of NO and N2O during nitrification
             *
             *  During the first step of nitrification a certain amount of N ist lost in form of \f$ NO \f$ and \f$ N_2O \f$.
             *  The relevant processes are a mix of:
             *  - chemical decomposition of the metabolite hydroxylamine (NH2OH) to NO
             *  - nitrifier denitrification (denitrification within the nitrifying microbe)
             *
             *  Reported production of \f$ NO \f$ during nitrification:
             *  - 0.1 - 10% of gross NH4 oxidation (Ludwig et al., 2001)
             *  - 0.6 - 2.5% of gross NH4 oxidation (Garrido et al. 2002)
             *
             *  All \f$ N_2O \f$ produced in connection with nitrification was initially \f$ NO \f$.
             *  Emissions of \f$ NO \f$ and \f$ N_2O \f$ during nitrification are influenced by soil temperature and moisture,
             *  0.03% at 5oC and 40% WFPS to 0.12% at 25oC and 60% WFPS (Chen et al. 2010)
             *
             * \f[
             * \frac{d (NO+N_2O)}{dt} = -\frac{d NH_4^+}{dt} \phi_{NO+N2O, T, wfps}
             * \f]
             *
             * The associated factor \f$ \phi_{NO+N2O, T, wfps} \f$ is given by a harmonic mean
             * of a soil temperature (\f$ \phi_{NO+N2O, T} \f$) and a soil water (\f$ \phi_{NO+N2O, wfps} \f$) depending response coefficient:
             *
             * \f[
             * \phi_{NO+N2O, T, wfps} = \frac{2.0}{\frac{1.0}{\phi_{NO+N2O, T}} + \frac{1.0}{\phi_{NO+N2O, wfps}}}
             * \f]
             *
             * with \f$ \phi_{NO+N2O, T}\f$ given by:
             *
             * \f[
             * \phi_{NO+N2O, T} = METRX\_F\_NIT\_NO\_N2O\_T\_EXP\_1 \cdot e^{\frac{T}{METRX\_F\_NIT\_NO\_N2O\_T\_EXP\_2}}
             * \f]
             *
             * @image html metrx_no_n2o_prod_nitrification_on_temperature.png "NO and N2O production during nitrification depending on soil temperature" width=900
             * @image latex metrx_no_n2o_prod_nitrification_on_temperature.png "NO and N2O production during nitrification depending on soil temperature"
             *
             * and with \f$ \phi_{NO+N2O, wfps}\f$ given by:
             *
             * \f[
             *  \phi_{NO+N2O, wfps} = 1 - \frac{1}{1 + e^{(wfps - METRX\_F\_NIT\_NO\_N2O\_M\_WEIBULL\_1) \cdot METRX\_F\_NIT\_NO\_N2O\_M\_WEIBULL\_2}}
             * \f]
             *
             * @image html metrx_no_n2o_prod_nitrification_on_soilwater.png "NO and N2O production during nitrification depending on soil water" width=900
             * @image latex metrx_no_n2o_prod_nitrification_on_soilwater.png "NO and N2O production during nitrification depending on soil water"
             *
             */
            double const fact_t_on_no_n2o( sipar_.METRX_F_NIT_NO_N2O_T_EXP_1() * std::exp( mc_temp_sl[sl] / sipar_.METRX_F_NIT_NO_N2O_T_EXP_2()));
            double const fact_m_on_no_n2o( ldndc::meteo::F_weibull( MeTrX_get_wfps( sl),
                                                                    sipar_.METRX_F_NIT_NO_N2O_M_WEIBULL_1(),
                                                                    sipar_.METRX_F_NIT_NO_N2O_M_WEIBULL_2(),
                                                                    1.0));

            double const fact_k_tm_on_no_n2o( sipar_.METRX_KF_NIT_NO_N2O() * cbm::harmonic_mean2( fact_t_on_no_n2o, fact_m_on_no_n2o));
            
            /*!
             * @page metrx
             *
             * The fraction of NO depends on soil water:
             *
             * \f[
             * \frac{d (NO)}{dt} = \frac{d (NO+N_2O)}{dt} \cdot \phi_{NO, wfps}
             * \f]
             *
             * with
             *
             * \f[
             * \phi_{NO, wfps} = 1 - \frac{1}{ 1 + e^{METRX\_F\_NIT\_NO\_M\_EXP\_1 \cdot (wfps - METRX\_F\_NIT\_NO\_M\_EXP\_2)}}
             * \f]
             *
             * @image html metrx_no_prod_nitrification_on_soilwater.png "NO production during nitrification depending on soil water" width=900
             * @image latex metrx_no_prod_nitrification_on_soilwater.png "NO production during nitrification depending on soil water"
             *
             * No pH effect of ammonia oxidation on N2O emissions observed (Booth, Stark and Rastetter 2005).
             */
            double const fact_m_on_no( 1.0 - 1.0 / (1.0 + std::exp( -sipar_.METRX_F_NIT_NO_M_EXP_1() *
                                                                    (MeTrX_get_wfps( sl) - sipar_.METRX_F_NIT_NO_M_EXP_2()))));

            double const fact_k_tm_on_no( cbm::bound_min( 0.0, fact_k_tm_on_no_n2o * fact_m_on_no));
            double const fact_k_tm_on_n2o( cbm::bound_min( 0.0, fact_k_tm_on_no_n2o * (1.0 - fact_m_on_no)));

            //maximum possible nitrification rate
            double const nitrify_nh4_max( cbm::flt_greater( nh4_ae_sl[sl], o2_tot * N_O2_RATIO_NITRIFICATION) ?
                                          cbm::bound_max( sipar_.METRX_NITRIFY_MAX() * sc_.h_sl[sl] * aevf,
                                                          o2_tot * N_O2_RATIO_NITRIFICATION) :
                                          cbm::bound_max( sipar_.METRX_NITRIFY_MAX() * sc_.h_sl[sl] * aevf,
                                                          nh4_ae_sl[sl]));
            double const nitrify_nh4_tot( cbm::bound_max( active_microbes * MUE_MAX_C_MICRO_1 * fact_nh4 * fact_ph * fact_o2 * fact_ni,
                                                          nitrify_nh4_max));

            double const nitrify_nh4_no( fact_k_tm_on_no * nitrify_nh4_tot);
            double const nitrify_nh4_n2o( fact_k_tm_on_n2o * nitrify_nh4_tot);
            double const nitrify_nh4_no2( nitrify_nh4_tot - nitrify_nh4_no - nitrify_nh4_n2o);

            //dot_metrx_nitrogen nh4_sl -> no2_sl [label="nit."];
            nh4_ae_sl[sl] -= nitrify_nh4_tot;
            sc_.no2_sl[sl] += nitrify_nh4_no2;

            //dot_metrx_nitrogen nh4_sl -> no_sl [label="nit."];
            //dot_metrx_nitrogen nh4_sl -> n2o_sl [label="nit."];
            no_gas_sl[sl] += nitrify_nh4_no;
            n2o_gas_sl[sl] += nitrify_nh4_n2o;

            day_nit_no2_no += nitrify_nh4_no;                                                  // Output
            day_nit_no2_n2o += nitrify_nh4_n2o;                                                // Output
            day_nit_nh4_no2 += nitrify_nh4_no2;                                                // Output

            accumulated_n_nh4_no2_nitrification_sl[sl] += nitrify_nh4_no2;
            accumulated_n_nh4_no_nitrification_sl[sl] += nitrify_nh4_no;
            accumulated_n_nh4_n2o_nitrification_sl[sl] += nitrify_nh4_n2o;

            double const o2_cons_tot( nitrify_nh4_tot / N_O2_RATIO_NITRIFICATION);
            double const o2_gas_cons( o2_cons_tot * o2_gas_sl[sl] / o2_tot);
            double const o2_liq_cons( o2_cons_tot * o2_liq_sl[sl] / o2_tot);
            o2_gas_sl[sl] = cbm::bound_min( 0.0, o2_gas_sl[sl] - o2_gas_cons);
            o2_liq_sl[sl] = cbm::bound_min( 0.0, o2_liq_sl[sl] - o2_liq_cons);
        }

        /*!
         * @page metrx
         * @subsection metrx_nitrification_no2 NO2 nitrification
         * The second step of nitrification is calculated independent of microbial biomass:
         * \f[
         * \frac{d NO_2^-}{dt} = \frac{NO_2^-}{METRX\_KMM\_NO2\_NIT + NO_2^-}
         * \f]
         *
         * @image html metrx_no2_nitrification_on_no2.png "Nitrification of NO2 depending on NO2" width=500
         * @image latex metrx_no2_nitrification_on_no2.png "Nitrification of NO2 depending on NO2"
         */
        double const fact_no2( sc_.no2_sl[sl] / (MeTrX_get_k_mm_x_sl( sl, aevf * sipar_.METRX_KMM_NO2_NIT()) + sc_.no2_sl[sl]));
        if ( cbm::flt_greater_zero( fact_no2))
        {
            double const no2_avail( 0.9 * sc_.no2_sl[sl]);
            double const nitrify_no2_no3( fact_no2 * no2_avail);

            //dot_metrx_nitrogen no2_sl -> no3_sl [label="nit."];
            sc_.no2_sl[sl] -= nitrify_no2_no3;
            no3_ae_sl[sl] += nitrify_no2_no3;

            day_nit_no2_no3_sl[sl] += nitrify_no2_no3;                                          // Output
            accumulated_n_no2_no3_nitrification_sl[sl] += nitrify_no2_no3;                      // Output
            sc_.accumulated_n_nitrify_sl[sl] += nitrify_no2_no3;                                // Output
        }
    }
}

} /*namespace ldndc*/

