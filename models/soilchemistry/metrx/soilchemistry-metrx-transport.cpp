/*!
 * @file
 *
 * @author
 *   - David Kraus (created on: may 12, 2014)
 *
 */

#include  "soilchemistry/metrx/soilchemistry-metrx.h"
#include  "soilchemistry/ld_transport.h"
#include  <scientific/soil/ld_soil.h>
#include  <scientific/hydrology/ld_vangenuchten.h>
#include  <constants/cbm_const.h>

namespace ldndc {

/*!
 * @brief
 *      Downwards transport of solutes, litter and humus
 *      with percolation water
 */
void 
SoilChemistryMeTrX::MeTrX_leaching()
{
    /*************************/
    /* Water table transport */
    /*************************/

    if ( have_water_table &&
         cbm::flt_greater_zero( infiltration))
    {
        ldndc_kassert( h_wl > 0.0);

        accumulated_n_nh4_infiltration_leach_sl[0] -= nh4_an_sl[0] + sc_.coated_nh4_sl[0];
        accumulated_n_nh3_infiltration_leach_sl[0] -= sc_.nh3_liq_sl[0];
        accumulated_n_no_infiltration_leach_sl[0] -= an_no_liq_sl[0];
        accumulated_n_n2o_infiltration_leach_sl[0] -= an_n2o_liq_sl[0];
        accumulated_n_urea_infiltration_leach_sl[0] -= sc_.urea_sl[0];
        accumulated_n_no3_infiltration_leach_sl[0] -= no3_an_sl[0];
        accumulated_n_don_infiltration_leach_sl[0] -= don_an_sl[0];

        double const leach_fact_sbl( cbm::bound_max( infiltration / h_wl, 0.99));
        LEACH_DOWN( sb_.ch4_sbl[0], ch4_liq_sl[0], leach_fact_sbl);
        LEACH_DOWN( sb_.o2_sbl[0], o2_liq_sl[0], leach_fact_sbl);
        LEACH_DOWN( sb_.nh4_sbl[0], nh4_an_sl[0], leach_fact_sbl);
        LEACH_DOWN( sb_.coated_nh4_sbl[0], sc_.coated_nh4_sl[0], leach_fact_sbl);
        LEACH_DOWN( sb_.nh3_sbl[0], sc_.nh3_liq_sl[0], leach_fact_sbl);
        LEACH_DOWN( sb_.no_sbl[0], an_no_liq_sl[0], leach_fact_sbl);
        LEACH_DOWN( sb_.n2o_sbl[0], an_n2o_liq_sl[0], leach_fact_sbl);
        LEACH_DOWN( sb_.urea_sbl[0], sc_.urea_sl[0], leach_fact_sbl);
        LEACH_DOWN( sb_.no3_sbl[0], no3_an_sl[0], leach_fact_sbl);
        LEACH_DOWN( sb_.don_sbl[0], don_an_sl[0], leach_fact_sbl);
        LEACH_DOWN( sb_.doc_sbl[0], sc_.doc_sl[0], leach_fact_sbl);
        LEACH_DOWN( sb_.co2_sbl[0], co2_liq_sl[0], leach_fact_sbl);
        LEACH_DOWN( sb_.so4_sbl[0], sc_.so4_sl[0], leach_fact_sbl);
        LEACH_DOWN( sb_.ni_sbl[0], sc_.ni_sl[0], leach_fact_sbl);

        accumulated_n_nh4_infiltration_leach_sl[0] += nh4_an_sl[0] + sc_.coated_nh4_sl[0];
        accumulated_n_nh3_infiltration_leach_sl[0] += sc_.nh3_liq_sl[0];
        accumulated_n_no_infiltration_leach_sl[0] += an_no_liq_sl[0];
        accumulated_n_n2o_infiltration_leach_sl[0] += an_n2o_liq_sl[0];
        accumulated_n_urea_infiltration_leach_sl[0] += sc_.urea_sl[0];
        accumulated_n_no3_infiltration_leach_sl[0] += no3_an_sl[0];
        accumulated_n_don_infiltration_leach_sl[0] += don_an_sl[0];

        for (size_t sbl = 1; sbl < sb_.surfacebulk_layer_cnt(); ++sbl)
        {
            size_t const sbl_plus( sbl - 1);
            LEACH_DOWN( sb_.ch4_sbl[sbl], sb_.ch4_sbl[sbl_plus], leach_fact_sbl);
            LEACH_DOWN( sb_.o2_sbl[sbl], sb_.o2_sbl[sbl_plus], leach_fact_sbl);
            LEACH_DOWN( sb_.nh4_sbl[sbl], sb_.nh4_sbl[sbl_plus], leach_fact_sbl);
            LEACH_DOWN( sb_.coated_nh4_sbl[sbl], sb_.coated_nh4_sbl[sbl_plus], leach_fact_sbl);
            LEACH_DOWN( sb_.nh3_sbl[sbl], sb_.nh3_sbl[sbl_plus], leach_fact_sbl);
            LEACH_DOWN( sb_.no_sbl[sbl], sb_.no_sbl[sbl_plus], leach_fact_sbl);
            LEACH_DOWN( sb_.n2o_sbl[sbl], sb_.n2o_sbl[sbl_plus], leach_fact_sbl);
            LEACH_DOWN( sb_.urea_sbl[sbl], sb_.urea_sbl[sbl_plus], leach_fact_sbl);
            LEACH_DOWN( sb_.no3_sbl[sbl], sb_.no3_sbl[sbl_plus], leach_fact_sbl);
            LEACH_DOWN( sb_.don_sbl[sbl], sb_.don_sbl[sbl_plus], leach_fact_sbl);
            LEACH_DOWN( sb_.doc_sbl[sbl], sb_.doc_sbl[sbl_plus], leach_fact_sbl);
            LEACH_DOWN( sb_.co2_sbl[sbl], sb_.co2_sbl[sbl_plus], leach_fact_sbl);
            LEACH_DOWN( sb_.so4_sbl[sbl], sb_.so4_sbl[sbl_plus], leach_fact_sbl);
            LEACH_DOWN( sb_.ni_sbl[sbl], sb_.ni_sbl[sbl_plus], leach_fact_sbl);
        }
    }

    /************************/
    /* Soil water transport */
    /************************/

    if ( cbm::flt_greater_zero( waterflux_sl[0]) &&
         cbm::flt_greater_zero( v_water_sl[0]))
    {
        size_t const sl( 0);
        size_t const sl_plus( 1);

        double const leach_fact( cbm::bound_min( 0.0, waterflux_sl[sl] / v_water_sl[sl]));

        double const leach_fact_nh4( cbm::bound_max( leach_fact * sipar_.RETNH4(), 0.9999));
        double const leach_fact_no3( cbm::bound_max( leach_fact * sipar_.RETNO3(), 0.9999));
        double const leach_fact_doc( cbm::bound_max( leach_fact * sipar_.RETDOC(), 0.9999));
        double const leach_fact_mic( cbm::bound_max( leach_fact * sipar_.METRX_RET_MICROBES(), 0.9999));
        double const leach_fact_lit( cbm::bound_max( leach_fact * sipar_.METRX_RET_LITTER(), 0.9999));
        double const leach_fact_hum( cbm::bound_max( leach_fact * sipar_.METRX_RET_HUMUS(), 0.9999));
        double const leach_fact_dissolved_gases( cbm::bound_max( leach_fact, 0.9999));

        accumulated_n_aorg_leaching_sl[sl] += sc_.N_aorg_sl[sl] * leach_fact_mic;
        LEACH_DOWN( sc_.C_aorg_sl[sl], sc_.C_aorg_sl[sl_plus], leach_fact_mic);
        LEACH_DOWN( sc_.N_aorg_sl[sl], sc_.N_aorg_sl[sl_plus], leach_fact_mic);

        transport_.leach_down_epsilon( sc_.C_micro1_sl[sl], sc_.C_micro1_sl[sl_plus], leach_fact_mic, MeTrX_get_micro_c_decay_max( sc_.C_micro1_sl[sl]));
        transport_.leach_down_epsilon( sc_.C_micro2_sl[sl], sc_.C_micro2_sl[sl_plus], leach_fact_mic, MeTrX_get_micro_c_decay_max( sc_.C_micro2_sl[sl]));
        transport_.leach_down_epsilon( sc_.C_micro3_sl[sl], sc_.C_micro3_sl[sl_plus], leach_fact_mic, MeTrX_get_micro_c_decay_max( sc_.C_micro3_sl[sl]));

        if( n_micro_1_sl[sl] > MeTrX_get_micro_n_decay_max( n_micro_1_sl[sl]) )
        {
          accumulated_n_micro_leaching_sl[sl] += ( n_micro_1_sl[sl] - MeTrX_get_micro_n_decay_max( n_micro_1_sl[sl]) ) * leach_fact_mic;
        }
        if( n_micro_2_sl[sl] > MeTrX_get_micro_n_decay_max( n_micro_2_sl[sl]) )
        {
          accumulated_n_micro_leaching_sl[sl] += ( n_micro_2_sl[sl] - MeTrX_get_micro_n_decay_max( n_micro_2_sl[sl]) ) * leach_fact_mic;
        }
        if( n_micro_3_sl[sl] > MeTrX_get_micro_n_decay_max( n_micro_3_sl[sl]) )
        {
          accumulated_n_micro_leaching_sl[sl] += ( n_micro_3_sl[sl] - MeTrX_get_micro_n_decay_max( n_micro_3_sl[sl]) ) * leach_fact_mic;
        }
        transport_.leach_down_epsilon( n_micro_1_sl[sl], n_micro_1_sl[sl_plus], leach_fact_mic, MeTrX_get_micro_n_decay_max( n_micro_1_sl[sl]));
        transport_.leach_down_epsilon( n_micro_2_sl[sl], n_micro_2_sl[sl_plus], leach_fact_mic, MeTrX_get_micro_n_decay_max( n_micro_2_sl[sl]));
        transport_.leach_down_epsilon( n_micro_3_sl[sl], n_micro_3_sl[sl_plus], leach_fact_mic, MeTrX_get_micro_n_decay_max( n_micro_3_sl[sl]));

        LEACH_DOWN( sc_.C_lit1_sl[sl], sc_.C_lit1_sl[sl_plus], leach_fact_lit);
        LEACH_DOWN( sc_.C_lit2_sl[sl], sc_.C_lit2_sl[sl_plus], leach_fact_lit);
        LEACH_DOWN( sc_.C_lit3_sl[sl], sc_.C_lit3_sl[sl_plus], leach_fact_lit);

        accumulated_n_litter_leaching_sl[sl] += (sc_.N_lit1_sl[sl] + sc_.N_lit2_sl[sl] + sc_.N_lit3_sl[sl]) * leach_fact_lit;
        LEACH_DOWN( sc_.N_lit1_sl[sl], sc_.N_lit1_sl[sl_plus], leach_fact_lit);
        LEACH_DOWN( sc_.N_lit2_sl[sl], sc_.N_lit2_sl[sl_plus], leach_fact_lit);
        LEACH_DOWN( sc_.N_lit3_sl[sl], sc_.N_lit3_sl[sl_plus], leach_fact_lit);

        LEACH_DOWN( c_humus_1_sl[sl], c_humus_1_sl[sl_plus], leach_fact_hum);
        LEACH_DOWN( c_humus_2_sl[sl], c_humus_2_sl[sl_plus], leach_fact_hum);
        LEACH_DOWN( c_humus_3_sl[sl], c_humus_3_sl[sl_plus], leach_fact_hum);

        accumulated_n_humus_1_leaching_sl[sl] += n_humus_1_sl[sl] * leach_fact_hum;
        accumulated_n_humus_2_leaching_sl[sl] += n_humus_2_sl[sl] * leach_fact_hum;
        accumulated_n_humus_3_leaching_sl[sl] += n_humus_3_sl[sl] * leach_fact_hum;
        LEACH_DOWN( n_humus_1_sl[sl], n_humus_1_sl[sl_plus], leach_fact_hum);
        LEACH_DOWN( n_humus_2_sl[sl], n_humus_2_sl[sl_plus], leach_fact_hum);
        LEACH_DOWN( n_humus_3_sl[sl], n_humus_3_sl[sl_plus], leach_fact_hum);

        accumulated_n_nh4_leaching_sl[sl] += (nh4_ae_sl[sl] + nh4_an_sl[sl] + sc_.coated_nh4_sl[sl]) * leach_fact_nh4;
        accumulated_n_nh3_leaching_sl[sl] += sc_.nh3_liq_sl[sl] * leach_fact_doc;
        accumulated_n_urea_leaching_sl[sl] += sc_.urea_sl[sl] * leach_fact_doc;
        LEACH_DOWN( nh4_ae_sl[sl], nh4_ae_sl[sl_plus], leach_fact_nh4);
        LEACH_DOWN( nh4_an_sl[sl], nh4_an_sl[sl_plus], leach_fact_nh4);
        LEACH_DOWN( sc_.coated_nh4_sl[sl], sc_.coated_nh4_sl[sl_plus], leach_fact_nh4);
        LEACH_DOWN( sc_.nh3_liq_sl[sl], sc_.nh3_liq_sl[sl_plus], leach_fact_doc);
        LEACH_DOWN( sc_.urea_sl[sl], sc_.urea_sl[sl_plus], leach_fact_doc);

        accumulated_n_no3_leaching_sl[sl] += (no3_ae_sl[sl] + no3_an_sl[sl]) * leach_fact_no3;
        LEACH_DOWN( no3_ae_sl[sl], no3_ae_sl[sl_plus], leach_fact_no3);
        LEACH_DOWN( no3_an_sl[sl], no3_an_sl[sl_plus], leach_fact_no3);

        accumulated_n_don_leaching_sl[sl] += (don_ae_sl[sl] + don_an_sl[sl]) * leach_fact_doc;
        LEACH_DOWN( don_ae_sl[sl], don_ae_sl[sl_plus], leach_fact_doc);
        LEACH_DOWN( don_an_sl[sl], don_an_sl[sl_plus], leach_fact_doc);
        LEACH_DOWN( sc_.doc_sl[sl], sc_.doc_sl[sl_plus], leach_fact_doc);
        LEACH_DOWN( sc_.an_doc_sl[sl], sc_.an_doc_sl[sl_plus], leach_fact_doc);
        LEACH_DOWN( ae_acetate_sl[sl], ae_acetate_sl[sl_plus], leach_fact_doc);
        LEACH_DOWN( an_acetate_sl[sl], an_acetate_sl[sl_plus], leach_fact_doc);

        accumulated_n_no_leaching_sl[sl] += (no_liq_sl[sl]+an_no_liq_sl[sl]) * leach_fact_dissolved_gases;
        accumulated_n_n2o_leaching_sl[sl] += (n2o_liq_sl[sl]+an_n2o_liq_sl[sl]) * leach_fact_dissolved_gases;
        LEACH_DOWN( no_liq_sl[sl], no_liq_sl[sl_plus], leach_fact_dissolved_gases);
        LEACH_DOWN( an_no_liq_sl[sl], an_no_liq_sl[sl_plus], leach_fact_dissolved_gases);
        LEACH_DOWN( n2o_liq_sl[sl], n2o_liq_sl[sl_plus], leach_fact_dissolved_gases);
        LEACH_DOWN( an_n2o_liq_sl[sl], an_n2o_liq_sl[sl_plus], leach_fact_dissolved_gases);

        LEACH_DOWN( ch4_liq_sl[sl], ch4_liq_sl[sl_plus], leach_fact_dissolved_gases);
        LEACH_DOWN( co2_liq_sl[sl], co2_liq_sl[sl_plus], leach_fact_dissolved_gases);
        LEACH_DOWN( o2_liq_sl[sl], o2_liq_sl[sl_plus], leach_fact_dissolved_gases);
        
        LEACH_DOWN( sc_.so4_sl[sl], sc_.so4_sl[sl_plus], leach_fact_no3);
        LEACH_DOWN( sc_.ni_sl[sl], sc_.ni_sl[sl_plus], leach_fact_doc);
    }

    for( size_t sl = 1; sl < sl_.soil_layer_cnt()-1; ++sl)
    {
        if ( !cbm::flt_equal_zero( waterflux_sl[sl]) &&
              cbm::flt_greater_zero( v_water_sl[sl]))
        {
            size_t sl_minus( sl - 1);
            size_t sl_plus( sl + 1);

            double const leach_fact( waterflux_sl[sl] / v_water_sl[sl]);

            double const leach_fact_nh4( cbm::bound(-0.9999, leach_fact * sipar_.RETNH4(), 0.9999));
            double const leach_fact_no3( cbm::bound(-0.9999, leach_fact * sipar_.RETNO3(), 0.9999));
            double const leach_fact_doc( cbm::bound(-0.9999, leach_fact * sipar_.RETDOC(), 0.9999));
            double const leach_fact_mic( cbm::bound(-0.9999, leach_fact * sipar_.METRX_RET_MICROBES(), 0.9999));
            double const leach_fact_lit( cbm::bound(-0.9999, leach_fact * sipar_.METRX_RET_LITTER(), 0.9999));
            double const leach_fact_hum( cbm::bound(-0.9999, leach_fact * sipar_.METRX_RET_HUMUS(), 0.9999));
            double const leach_fact_dissolved_gases( cbm::bound(-0.9999, leach_fact, 0.9999));

            accumulated_n_aorg_leaching_sl[sl] += sc_.N_aorg_sl[sl] * leach_fact_mic;
            transport_.leach_up_and_down( sc_.C_aorg_sl[sl_minus], sc_.C_aorg_sl[sl], sc_.C_aorg_sl[sl_plus], leach_fact_mic);
            transport_.leach_up_and_down( sc_.N_aorg_sl[sl_minus], sc_.N_aorg_sl[sl], sc_.N_aorg_sl[sl_plus], leach_fact_mic);

            transport_.leach_up_and_down_epsilon(  sc_.C_micro1_sl[sl_minus], sc_.C_micro1_sl[sl], sc_.C_micro1_sl[sl_plus], leach_fact_mic, MeTrX_get_micro_c_decay_max( sc_.C_micro1_sl[sl]));
            transport_.leach_up_and_down_epsilon(  sc_.C_micro2_sl[sl_minus], sc_.C_micro2_sl[sl], sc_.C_micro2_sl[sl_plus], leach_fact_mic, MeTrX_get_micro_c_decay_max( sc_.C_micro2_sl[sl]));
            transport_.leach_up_and_down_epsilon(  sc_.C_micro3_sl[sl_minus], sc_.C_micro3_sl[sl], sc_.C_micro3_sl[sl_plus], leach_fact_mic, MeTrX_get_micro_c_decay_max( sc_.C_micro3_sl[sl]));

            if( n_micro_1_sl[sl] > MeTrX_get_micro_n_decay_max( n_micro_1_sl[sl]) )
            {
              accumulated_n_micro_leaching_sl[sl] += ( n_micro_1_sl[sl] - MeTrX_get_micro_n_decay_max( n_micro_1_sl[sl]) ) * leach_fact_mic;
            }
            if( n_micro_2_sl[sl] > MeTrX_get_micro_n_decay_max( n_micro_2_sl[sl]) )
            {
              accumulated_n_micro_leaching_sl[sl] += ( n_micro_2_sl[sl] - MeTrX_get_micro_n_decay_max( n_micro_2_sl[sl]) ) * leach_fact_mic;
            }
            if( n_micro_3_sl[sl] > MeTrX_get_micro_n_decay_max( n_micro_3_sl[sl]) )
            {
              accumulated_n_micro_leaching_sl[sl] += ( n_micro_3_sl[sl] - MeTrX_get_micro_n_decay_max( n_micro_3_sl[sl]) ) * leach_fact_mic;
            }
            transport_.leach_up_and_down_epsilon(  n_micro_1_sl[sl_minus], n_micro_1_sl[sl], n_micro_1_sl[sl_plus], leach_fact_mic, MeTrX_get_micro_n_decay_max( n_micro_1_sl[sl]));
            transport_.leach_up_and_down_epsilon(  n_micro_2_sl[sl_minus], n_micro_2_sl[sl], n_micro_2_sl[sl_plus], leach_fact_mic, MeTrX_get_micro_n_decay_max( n_micro_2_sl[sl]));
            transport_.leach_up_and_down_epsilon(  n_micro_3_sl[sl_minus], n_micro_3_sl[sl], n_micro_3_sl[sl_plus], leach_fact_mic, MeTrX_get_micro_n_decay_max( n_micro_3_sl[sl]));

            transport_.leach_up_and_down(  sc_.C_lit1_sl[sl_minus], sc_.C_lit1_sl[sl], sc_.C_lit1_sl[sl_plus], leach_fact_lit);
            transport_.leach_up_and_down(  sc_.C_lit2_sl[sl_minus], sc_.C_lit2_sl[sl], sc_.C_lit2_sl[sl_plus], leach_fact_lit);
            transport_.leach_up_and_down(  sc_.C_lit3_sl[sl_minus], sc_.C_lit3_sl[sl], sc_.C_lit3_sl[sl_plus], leach_fact_lit);

            accumulated_n_litter_leaching_sl[sl] += (sc_.N_lit1_sl[sl]+sc_.N_lit2_sl[sl]+sc_.N_lit3_sl[sl]) * leach_fact_lit;
            transport_.leach_up_and_down(  sc_.N_lit1_sl[sl_minus], sc_.N_lit1_sl[sl], sc_.N_lit1_sl[sl_plus], leach_fact_lit);
            transport_.leach_up_and_down(  sc_.N_lit2_sl[sl_minus], sc_.N_lit2_sl[sl], sc_.N_lit2_sl[sl_plus], leach_fact_lit);
            transport_.leach_up_and_down(  sc_.N_lit3_sl[sl_minus], sc_.N_lit3_sl[sl], sc_.N_lit3_sl[sl_plus], leach_fact_lit);

            transport_.leach_up_and_down(  c_humus_1_sl[sl_minus], c_humus_1_sl[sl], c_humus_1_sl[sl_plus], leach_fact_hum);
            transport_.leach_up_and_down(  c_humus_2_sl[sl_minus], c_humus_2_sl[sl], c_humus_2_sl[sl_plus], leach_fact_hum);
            transport_.leach_up_and_down(  c_humus_3_sl[sl_minus], c_humus_3_sl[sl], c_humus_3_sl[sl_plus], leach_fact_hum);

            accumulated_n_humus_1_leaching_sl[sl] += n_humus_1_sl[sl] * leach_fact_hum;
            accumulated_n_humus_2_leaching_sl[sl] += n_humus_2_sl[sl] * leach_fact_hum;
            accumulated_n_humus_3_leaching_sl[sl] += n_humus_3_sl[sl] * leach_fact_hum;
            transport_.leach_up_and_down(  n_humus_1_sl[sl_minus], n_humus_1_sl[sl], n_humus_1_sl[sl_plus], leach_fact_hum);
            transport_.leach_up_and_down(  n_humus_2_sl[sl_minus], n_humus_2_sl[sl], n_humus_2_sl[sl_plus], leach_fact_hum);
            transport_.leach_up_and_down(  n_humus_3_sl[sl_minus], n_humus_3_sl[sl], n_humus_3_sl[sl_plus], leach_fact_hum);

            accumulated_n_nh4_leaching_sl[sl] += (nh4_ae_sl[sl] + nh4_an_sl[sl] + sc_.coated_nh4_sl[sl]) * leach_fact_nh4;
            accumulated_n_nh3_leaching_sl[sl] += sc_.nh3_liq_sl[sl] * leach_fact_doc;
            accumulated_n_urea_leaching_sl[sl] += sc_.urea_sl[sl] * leach_fact_doc;
            transport_.leach_up_and_down(  nh4_ae_sl[sl_minus], nh4_ae_sl[sl], nh4_ae_sl[sl_plus], leach_fact_nh4);
            transport_.leach_up_and_down(  nh4_an_sl[sl_minus], nh4_an_sl[sl], nh4_an_sl[sl_plus], leach_fact_nh4);
            transport_.leach_up_and_down(  sc_.coated_nh4_sl[sl_minus], sc_.coated_nh4_sl[sl], sc_.coated_nh4_sl[sl_plus], leach_fact_nh4);
            transport_.leach_up_and_down(  sc_.nh3_liq_sl[sl_minus], sc_.nh3_liq_sl[sl], sc_.nh3_liq_sl[sl_plus], leach_fact_doc);
            transport_.leach_up_and_down(  sc_.urea_sl[sl_minus], sc_.urea_sl[sl], sc_.urea_sl[sl_plus], leach_fact_doc);

            accumulated_n_no3_leaching_sl[sl] += (no3_ae_sl[sl]+no3_an_sl[sl]) * leach_fact_no3;
            transport_.leach_up_and_down(  no3_ae_sl[sl_minus], no3_ae_sl[sl], no3_ae_sl[sl_plus], leach_fact_no3);
            transport_.leach_up_and_down(  no3_an_sl[sl_minus], no3_an_sl[sl], no3_an_sl[sl_plus], leach_fact_no3);

            accumulated_n_don_leaching_sl[sl] += (don_ae_sl[sl] + don_an_sl[sl]) * leach_fact_doc;
            transport_.leach_up_and_down(  don_ae_sl[sl_minus], don_ae_sl[sl], don_ae_sl[sl_plus], leach_fact_doc);
            transport_.leach_up_and_down(  don_an_sl[sl_minus], don_an_sl[sl], don_an_sl[sl_plus], leach_fact_doc);
            transport_.leach_up_and_down(  sc_.doc_sl[sl_minus], sc_.doc_sl[sl], sc_.doc_sl[sl_plus], leach_fact_doc);
            transport_.leach_up_and_down(  sc_.an_doc_sl[sl_minus], sc_.an_doc_sl[sl], sc_.an_doc_sl[sl_plus], leach_fact_doc);
            transport_.leach_up_and_down(  ae_acetate_sl[sl_minus], ae_acetate_sl[sl], ae_acetate_sl[sl_plus], leach_fact_doc);
            transport_.leach_up_and_down(  an_acetate_sl[sl_minus], an_acetate_sl[sl], an_acetate_sl[sl_plus], leach_fact_doc);

            accumulated_n_no_leaching_sl[sl] += (no_liq_sl[sl]+an_no_liq_sl[sl]) * leach_fact_dissolved_gases;
            accumulated_n_n2o_leaching_sl[sl] += (n2o_liq_sl[sl]+an_n2o_liq_sl[sl]) * leach_fact_dissolved_gases;
            transport_.leach_up_and_down(  no_liq_sl[sl_minus], no_liq_sl[sl], no_liq_sl[sl_plus], leach_fact_dissolved_gases);
            transport_.leach_up_and_down(  an_no_liq_sl[sl_minus], an_no_liq_sl[sl], an_no_liq_sl[sl_plus], leach_fact_dissolved_gases);
            transport_.leach_up_and_down(  n2o_liq_sl[sl_minus], n2o_liq_sl[sl], n2o_liq_sl[sl_plus], leach_fact_dissolved_gases);
            transport_.leach_up_and_down(  an_n2o_liq_sl[sl_minus], an_n2o_liq_sl[sl], an_n2o_liq_sl[sl_plus], leach_fact_dissolved_gases);

            transport_.leach_up_and_down(  ch4_liq_sl[sl_minus], ch4_liq_sl[sl], ch4_liq_sl[sl_plus], leach_fact_dissolved_gases);
            transport_.leach_up_and_down(  co2_liq_sl[sl_minus], co2_liq_sl[sl], co2_liq_sl[sl_plus], leach_fact_dissolved_gases);
            transport_.leach_up_and_down(  o2_liq_sl[sl_minus], o2_liq_sl[sl], o2_liq_sl[sl_plus], leach_fact_dissolved_gases);

            transport_.leach_up_and_down(  sc_.so4_sl[sl_minus], sc_.so4_sl[sl], sc_.so4_sl[sl_plus], leach_fact_no3);
            transport_.leach_up_and_down(  sc_.ni_sl[sl_minus], sc_.ni_sl[sl], sc_.ni_sl[sl_plus], leach_fact_doc);
        }
    }

    size_t sl( sl_.soil_layer_cnt()-1);

    if ( !cbm::flt_equal_zero( waterflux_sl[sl]) &&
          cbm::flt_greater_zero( v_water_sl[sl]))
    {
        double dummy_out( 0.0); //dummy for substances that are not balanced (no accumulated leaching variable exists)
        size_t sl_minus( sl_.soil_layer_cnt()-2);

        double const leach_fact( waterflux_sl[sl] / v_water_sl[sl]);

        double const leach_fact_nh4( cbm::bound(-0.9999, leach_fact * sipar_.RETNH4(), 0.9999));
        double const leach_fact_no3( cbm::bound(-0.9999, leach_fact * sipar_.RETNO3(), 0.9999));
        double const leach_fact_doc( cbm::bound(-0.9999, leach_fact * sipar_.RETDOC(), 0.9999));
        double const leach_fact_mic( cbm::bound(-0.9999, leach_fact * sipar_.METRX_RET_MICROBES(), 0.9999));
        double const leach_fact_lit( cbm::bound(-0.9999, leach_fact * sipar_.METRX_RET_LITTER(), 0.9999));
        double const leach_fact_hum( cbm::bound(-0.9999, leach_fact * sipar_.METRX_RET_HUMUS(), 0.9999));
        double const leach_fact_dissolved_gases( cbm::bound(-0.9999, leach_fact, 0.9999));

        accumulated_n_aorg_leaching_sl[sl] += sc_.N_aorg_sl[sl] * leach_fact_mic;
        transport_.leach_up_and_down(  sc_.C_aorg_sl[sl_minus], sc_.C_aorg_sl[sl], sc_.accumulated_doc_leach, leach_fact_mic);
        transport_.leach_up_and_down(  sc_.N_aorg_sl[sl_minus], sc_.N_aorg_sl[sl], sc_.accumulated_don_leach, leach_fact_mic);

        transport_.leach_up_and_down_epsilon(  sc_.C_micro1_sl[sl_minus], sc_.C_micro1_sl[sl], sc_.accumulated_doc_leach, leach_fact_mic, MeTrX_get_micro_c_decay_max( sc_.C_micro1_sl[sl]));
        transport_.leach_up_and_down_epsilon(  sc_.C_micro2_sl[sl_minus], sc_.C_micro2_sl[sl], sc_.accumulated_doc_leach, leach_fact_mic, MeTrX_get_micro_c_decay_max( sc_.C_micro2_sl[sl]));
        transport_.leach_up_and_down_epsilon(  sc_.C_micro3_sl[sl_minus], sc_.C_micro3_sl[sl], sc_.accumulated_doc_leach, leach_fact_mic, MeTrX_get_micro_c_decay_max( sc_.C_micro3_sl[sl]));

        if( n_micro_1_sl[sl] > MeTrX_get_micro_n_decay_max( n_micro_1_sl[sl]) )
        {
          accumulated_n_micro_leaching_sl[sl] += ( n_micro_1_sl[sl] - MeTrX_get_micro_n_decay_max( n_micro_1_sl[sl]) ) * leach_fact_mic;
        }
        if( n_micro_2_sl[sl] > MeTrX_get_micro_n_decay_max( n_micro_2_sl[sl]) )
        {
          accumulated_n_micro_leaching_sl[sl] += ( n_micro_2_sl[sl] - MeTrX_get_micro_n_decay_max( n_micro_2_sl[sl]) ) * leach_fact_mic;
        }
        if( n_micro_3_sl[sl] > MeTrX_get_micro_n_decay_max( n_micro_3_sl[sl]) )
        {
          accumulated_n_micro_leaching_sl[sl] += ( n_micro_3_sl[sl] - MeTrX_get_micro_n_decay_max( n_micro_3_sl[sl]) ) * leach_fact_mic;
        }
        transport_.leach_up_and_down_epsilon(  n_micro_1_sl[sl_minus], n_micro_1_sl[sl], sc_.accumulated_don_leach, leach_fact_mic, MeTrX_get_micro_n_decay_max( n_micro_1_sl[sl]));
        transport_.leach_up_and_down_epsilon(  n_micro_2_sl[sl_minus], n_micro_2_sl[sl], sc_.accumulated_don_leach, leach_fact_mic, MeTrX_get_micro_n_decay_max( n_micro_2_sl[sl]));
        transport_.leach_up_and_down_epsilon(  n_micro_3_sl[sl_minus], n_micro_3_sl[sl], sc_.accumulated_don_leach, leach_fact_mic, MeTrX_get_micro_n_decay_max( n_micro_3_sl[sl]));

        transport_.leach_up_and_down(  sc_.C_lit1_sl[sl_minus], sc_.C_lit1_sl[sl], sc_.accumulated_doc_leach, leach_fact_lit);
        transport_.leach_up_and_down(  sc_.C_lit2_sl[sl_minus], sc_.C_lit2_sl[sl], sc_.accumulated_doc_leach, leach_fact_lit);
        transport_.leach_up_and_down(  sc_.C_lit3_sl[sl_minus], sc_.C_lit3_sl[sl], sc_.accumulated_doc_leach, leach_fact_lit);

        accumulated_n_litter_leaching_sl[sl] += (sc_.N_lit1_sl[sl]+sc_.N_lit2_sl[sl]+sc_.N_lit3_sl[sl]) * leach_fact_lit;
        transport_.leach_up_and_down(  sc_.N_lit1_sl[sl_minus], sc_.N_lit1_sl[sl], sc_.accumulated_don_leach, leach_fact_lit);
        transport_.leach_up_and_down(  sc_.N_lit2_sl[sl_minus], sc_.N_lit2_sl[sl], sc_.accumulated_don_leach, leach_fact_lit);
        transport_.leach_up_and_down(  sc_.N_lit3_sl[sl_minus], sc_.N_lit3_sl[sl], sc_.accumulated_don_leach, leach_fact_lit);

        transport_.leach_up_and_down(  c_humus_1_sl[sl_minus], c_humus_1_sl[sl], sc_.accumulated_doc_leach, leach_fact_hum);
        transport_.leach_up_and_down(  c_humus_2_sl[sl_minus], c_humus_2_sl[sl], sc_.accumulated_doc_leach, leach_fact_hum);
        transport_.leach_up_and_down(  c_humus_3_sl[sl_minus], c_humus_3_sl[sl], sc_.accumulated_doc_leach, leach_fact_hum);

        accumulated_n_humus_1_leaching_sl[sl] += n_humus_1_sl[sl] * leach_fact_hum;
        accumulated_n_humus_2_leaching_sl[sl] += n_humus_2_sl[sl] * leach_fact_hum;
        accumulated_n_humus_3_leaching_sl[sl] += n_humus_3_sl[sl] * leach_fact_hum;
        transport_.leach_up_and_down(  n_humus_1_sl[sl_minus], n_humus_1_sl[sl], sc_.accumulated_don_leach, leach_fact_hum);
        transport_.leach_up_and_down(  n_humus_2_sl[sl_minus], n_humus_2_sl[sl], sc_.accumulated_don_leach, leach_fact_hum);
        transport_.leach_up_and_down(  n_humus_2_sl[sl_minus], n_humus_2_sl[sl], sc_.accumulated_don_leach, leach_fact_hum);

        accumulated_n_nh4_leaching_sl[sl] += (nh4_ae_sl[sl] + nh4_an_sl[sl] + sc_.coated_nh4_sl[sl]) * leach_fact_nh4;
        accumulated_n_nh3_leaching_sl[sl] += sc_.nh3_liq_sl[sl] * leach_fact_doc;
        accumulated_n_urea_leaching_sl[sl] += sc_.urea_sl[sl] * leach_fact_doc;
        transport_.leach_up_and_down(  nh4_ae_sl[sl_minus], nh4_ae_sl[sl], sc_.accumulated_nh4_leach, leach_fact_nh4);
        transport_.leach_up_and_down(  nh4_an_sl[sl_minus], nh4_an_sl[sl], sc_.accumulated_nh4_leach, leach_fact_nh4);
        transport_.leach_up_and_down(  sc_.coated_nh4_sl[sl_minus], sc_.coated_nh4_sl[sl], sc_.accumulated_nh4_leach, leach_fact_nh4);
        transport_.leach_up_and_down(  sc_.nh3_liq_sl[sl_minus], sc_.nh3_liq_sl[sl], day_leach_nh3, leach_fact_doc);
        transport_.leach_up_and_down(  sc_.urea_sl[sl_minus], sc_.urea_sl[sl], day_leach_urea, leach_fact_doc);

        accumulated_n_no3_leaching_sl[sl] += (no3_ae_sl[sl]+no3_an_sl[sl]) * leach_fact_no3;
        transport_.leach_up_and_down(  no3_ae_sl[sl_minus], no3_ae_sl[sl], sc_.accumulated_no3_leach, leach_fact_no3);
        transport_.leach_up_and_down(  no3_an_sl[sl_minus], no3_an_sl[sl], sc_.accumulated_no3_leach, leach_fact_no3);

        accumulated_n_don_leaching_sl[sl] += (don_ae_sl[sl] + don_an_sl[sl]) * leach_fact_doc;
        transport_.leach_up_and_down(  don_ae_sl[sl_minus], don_ae_sl[sl], sc_.accumulated_don_leach, leach_fact_doc);
        transport_.leach_up_and_down(  don_an_sl[sl_minus], don_an_sl[sl], sc_.accumulated_don_leach, leach_fact_doc);
        transport_.leach_up_and_down(  sc_.doc_sl[sl_minus], sc_.doc_sl[sl], sc_.accumulated_doc_leach, leach_fact_doc);
        transport_.leach_up_and_down(  sc_.an_doc_sl[sl_minus], sc_.an_doc_sl[sl], sc_.accumulated_doc_leach, leach_fact_doc);
        transport_.leach_up_and_down(  ae_acetate_sl[sl_minus], ae_acetate_sl[sl], sc_.accumulated_doc_leach, leach_fact_doc);
        transport_.leach_up_and_down(  an_acetate_sl[sl_minus], an_acetate_sl[sl], sc_.accumulated_doc_leach, leach_fact_doc);

        accumulated_n_no_leaching_sl[sl] += (no_liq_sl[sl]+an_no_liq_sl[sl]) * leach_fact_dissolved_gases;
        accumulated_n_n2o_leaching_sl[sl] += (n2o_liq_sl[sl]+an_n2o_liq_sl[sl]) * leach_fact_dissolved_gases;
        transport_.leach_up_and_down(  no_liq_sl[sl_minus], no_liq_sl[sl], day_leach_no, leach_fact_dissolved_gases);
        transport_.leach_up_and_down(  an_no_liq_sl[sl_minus], an_no_liq_sl[sl], day_leach_no, leach_fact_dissolved_gases);
        transport_.leach_up_and_down(  n2o_liq_sl[sl_minus], n2o_liq_sl[sl], day_leach_n2o, leach_fact_dissolved_gases);
        transport_.leach_up_and_down(  an_n2o_liq_sl[sl_minus], an_n2o_liq_sl[sl], day_leach_n2o, leach_fact_dissolved_gases);
        transport_.leach_up_and_down(  o2_liq_sl[sl_minus], o2_liq_sl[sl], day_leach_o2, leach_fact_dissolved_gases);

        transport_.leach_up_and_down(  ch4_liq_sl[sl_minus], ch4_liq_sl[sl], subdaily_ch4_leach[subdaily_time_step_], leach_fact_dissolved_gases);
        transport_.leach_up_and_down(  co2_liq_sl[sl_minus], co2_liq_sl[sl], subdaily_co2_leach[subdaily_time_step_], leach_fact_dissolved_gases);

        transport_.leach_up_and_down(  sc_.so4_sl[sl_minus], sc_.so4_sl[sl], sc_.accumulated_so4_leach, leach_fact_no3);
        transport_.leach_up_and_down(  sc_.ni_sl[sl_minus], sc_.ni_sl[sl], dummy_out, leach_fact_doc);
    }
}



/*!
 * @brief
 *
 */
#define DISSOLUTION_ATMOSPHERE(__val_eq__,__val__,__out__)        \
{                                                                 \
double const transfer( ((__val_eq__) - (__val__)) * FTS_TOT_);    \
(__out__) -= transfer;                                            \
(__val__) += transfer;                                            \
}                                                                 \



/*!
 * @brief
 *
 */
lerr_t
SoilChemistryMeTrX::MeTrX_dissolution_soil( double const &_henry,
                                            double &_val_gas,
                                            double &_val_liq)
{
    double const val_tot( _val_gas + _val_liq);
    if ( cbm::flt_greater_zero( val_tot))
    {
        _val_liq = _henry / (1.0 + _henry) * val_tot;
        _val_gas = val_tot - _val_liq;
    }
    return LDNDC_ERR_OK;
}



/*!
 * @brief
 *      - exchange of soil no3 <-> groundwater no3
 *      - only calculated in case of valid groundwater input
 */
void SoilChemistryMeTrX::MeTrX_groundwater_access()
{
    if ( gw_ != NULL)
    {
        for ( size_t sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
        {
            if ( sc_.depth_sl[sl] > gw_->watertable_subday( lclock_ref()))
            {
                double const no3_gw( gw_->no3_day(lclock_ref())                                             //given in (mg no3 L-1)
                                    * cbm::MN / (cbm::MN + 3.0 * cbm::MO) * cbm::KG_IN_MG                   //converts no3 from (mg no3) -> (kg n-no3)
                                    * (wc_.wc_sl[sl] + wc_.ice_sl[sl]) * sc_.h_sl[sl] * cbm::DM3_IN_M3);    //converts water (m3) -> (L)

                if ( cbm::flt_greater_zero( no3_gw))
                {
                    double const no3_diff( GROUNDWATER_NUTRIENT_ACCESS_RATE * (no3_gw - no3_an_sl[sl]));
                    no3_an_sl[sl] += no3_diff;
                    day_no3_groundwater_access += no3_diff;
                    sc_.accumulated_no3_leach -= no3_diff;
                    accumulated_n_no3_leaching_sl[sl] -= no3_diff;
                }
            }
        }

        if ( have_river_connection)
        {
            if ( have_water_table)
            {
                for (size_t sbl = 0; sbl < sb_.surfacebulk_layer_cnt(); ++sbl)
                {
                    DISSOLUTION_ATMOSPHERE( MeTrX_get_atm_eq_liq( o2_atm_eq_liq, h_wl),  sb_.o2_sbl[sbl],  subdaily_o2_water[subdaily_time_step_]);
                    DISSOLUTION_ATMOSPHERE( MeTrX_get_atm_eq_liq( ch4_atm_eq_liq, h_wl), sb_.ch4_sbl[sbl], subdaily_ch4_water[subdaily_time_step_]);
                    DISSOLUTION_ATMOSPHERE( MeTrX_get_atm_eq_liq( co2_atm_eq_liq, h_wl), sb_.co2_sbl[sbl], subdaily_co2_water[subdaily_time_step_]);
                    DISSOLUTION_ATMOSPHERE( MeTrX_get_atm_eq_liq( nh3_atm_eq_liq, h_wl), sb_.nh3_sbl[sbl], subdaily_nh3_water[subdaily_time_step_]);
                    DISSOLUTION_ATMOSPHERE( MeTrX_get_atm_eq_liq( no_atm_eq_liq, h_wl),  sb_.no_sbl[sbl],  subdaily_no_water[subdaily_time_step_]);
                    DISSOLUTION_ATMOSPHERE( MeTrX_get_atm_eq_liq( n2o_atm_eq_liq, h_wl), sb_.n2o_sbl[sbl], subdaily_n2o_water[subdaily_time_step_]);
                }
            }
        }
    }
}



/*!
 * @page metrx
 * @section MeTrX_dissolution Dissolution
 *  Phase equilibrium betwenn dissolved and gaseous state is calculated throughout the soil profile
 *  as well as for the top layer of the surface water table (if existing).
 */
lerr_t
SoilChemistryMeTrX::MeTrX_dissolution()
{
    /************************************************************/
    /* Dissolution process between atmosphere and surface water */
    /************************************************************/

    /*!
     * @page metrx
     *  For the following species the phase equilibrium betwenn dissolved and gaseousstate is calculated:
     *   - \f$ O_2 \f$
     *   - \f$ CH_4 \f$
     *   - \f$ CO_2 \f$
     *   - \f$ NH_3 \f$
     *   - \f$ NO \f$
     *   - \f$ N_2O \f$
     */
    if ( have_water_table)
    {
        size_t const  w( sb_.surfacebulk_layer_cnt() - 1);

        accumulated_n_nh3_dissolution_sbl[0] -= sb_.nh3_sbl[w];
        accumulated_n_no_dissolution_sbl[0] -= sb_.no_sbl[w];
        accumulated_n_n2o_dissolution_sbl[0] -= sb_.n2o_sbl[w];

        DISSOLUTION_ATMOSPHERE( MeTrX_get_atm_eq_liq( o2_atm_eq_liq, h_wl),  sb_.o2_sbl[w],  subdaily_o2_water[subdaily_time_step_]);
        DISSOLUTION_ATMOSPHERE( MeTrX_get_atm_eq_liq( ch4_atm_eq_liq, h_wl), sb_.ch4_sbl[w], subdaily_ch4_water[subdaily_time_step_]);
        DISSOLUTION_ATMOSPHERE( MeTrX_get_atm_eq_liq( co2_atm_eq_liq, h_wl), sb_.co2_sbl[w], subdaily_co2_water[subdaily_time_step_]);
        DISSOLUTION_ATMOSPHERE( MeTrX_get_atm_eq_liq( nh3_atm_eq_liq, h_wl), sb_.nh3_sbl[w], subdaily_nh3_water[subdaily_time_step_]);
        DISSOLUTION_ATMOSPHERE( MeTrX_get_atm_eq_liq( no_atm_eq_liq, h_wl),  sb_.no_sbl[w],  subdaily_no_water[subdaily_time_step_]);
        DISSOLUTION_ATMOSPHERE( MeTrX_get_atm_eq_liq( n2o_atm_eq_liq, h_wl), sb_.n2o_sbl[w], subdaily_n2o_water[subdaily_time_step_]);

        accumulated_n_nh3_dissolution_sbl[0] += sb_.nh3_sbl[w];
        accumulated_n_no_dissolution_sbl[0] += sb_.no_sbl[w];
        accumulated_n_n2o_dissolution_sbl[0] += sb_.n2o_sbl[w];
    }


    /****************************************/
    /* Dissolution processes in soil matrix */
    /****************************************/

    /*!
     * @page metrx
     *  For the equilibrium between the partial pressure in the gas pahse \f$ p_{i,gas} \f$ and the concentrations in the dissolved species \f$ i \f$, the Henry law is applied:
     *  \f[
     *  c_{i,liq} = H_{i} p_{i,gas}
     *  \f]
     */
    for( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
    {
        //decrease water/air ratio for top soil layer approaching zero in order to support surface volatilization
        double const ratio_water_air( v_water_sl[sl] / v_air_sl[sl]);
        double const temp( mc_temp_sl[sl] + cbm::D_IN_K);
        double const fact_uc( cbm::RGAS * temp * ratio_water_air);

        double h_x_scale( get_henry_coefficient( mc_temp_sl[sl], henry_o2) * fact_uc);
        MeTrX_dissolution_soil( h_x_scale, o2_gas_sl[sl], o2_liq_sl[sl]);

        h_x_scale = get_henry_coefficient( mc_temp_sl[sl], henry_n2o) * fact_uc;
        MeTrX_dissolution_soil( h_x_scale, n2o_gas_sl[sl], n2o_liq_sl[sl]);
        MeTrX_dissolution_soil( h_x_scale, an_n2o_gas_sl[sl], an_n2o_liq_sl[sl]);

        h_x_scale = get_henry_coefficient( mc_temp_sl[sl], henry_no) * fact_uc;
        MeTrX_dissolution_soil( h_x_scale, no_gas_sl[sl], no_liq_sl[sl]);
        MeTrX_dissolution_soil( h_x_scale, an_no_gas_sl[sl], an_no_liq_sl[sl]);

        h_x_scale = get_henry_coefficient( mc_temp_sl[sl], henry_ch4) * fact_uc;
        MeTrX_dissolution_soil( h_x_scale, ch4_gas_sl[sl], ch4_liq_sl[sl]);

        h_x_scale = get_henry_coefficient( mc_temp_sl[sl], henry_co2) * fact_uc;
        MeTrX_dissolution_soil( h_x_scale, co2_gas_sl[sl], co2_liq_sl[sl]);

        h_x_scale = get_henry_coefficient( mc_temp_sl[sl], henry_nh3) * fact_uc;
        MeTrX_dissolution_soil( h_x_scale, sc_.nh3_gas_sl[sl], sc_.nh3_liq_sl[sl]);
    }

    return LDNDC_ERR_OK;
}



/*!
 * @page metrx
 * @section MeTrX_gas_diffusion Diffusion of gaseous species
 *   Gaseous diffusion is calculated for:
 *    - \f$ O_2 \f$
 *    - \f$ N_2O \f$ (separately for aerobic and anaerobic species)
 *    - \f$ NO \f$ (separately for aerobic and anaerobic species)
 *    - \f$ CH_4 \f$
 *    - \f$ CO_2 \f$
 *    - \f$ NH_3 \f$
 *
 */
lerr_t
SoilChemistryMeTrX::MeTrX_gas_diffusion()
{
    /*************/
    /* Diffusion */
    /*************/

    double dirichlet_n2o( 0.0);
    double dirichlet_an_n2o( 0.0);
    double dirichlet_no( 0.0);
    double dirichlet_an_no( 0.0);
    double dirichlet_nh3( 0.0);

    double const RGAS_temp_atm( cbm::RGAS * (mc_temp_sl[0] + cbm::D_IN_K));
    size_t fl_cnt( m_veg->canopy_layers_used());
    /*!
     * @page metrx
     *  For the bottom, a Neuman no-flow boundary condition is used and for the top a Dirichlet boundary condition is used.
     */
    if ( have_water_table)
    {
        /* N2 */
        double dummy( 0.0);
        double atmos_value( (cbm::PN2 * cbm::BAR_IN_PA) / RGAS_temp_atm * M_2N_SCALE);
        transport_.gas_diffusion_soil(have_plant_diffusion, D_N2_AIR_SCALE, D_eff_air_sl, n2_gas_sl, v_air_sl, interface_delta_x_air_sl,
                                      dummy, dummy, dummy,
                                      dirichlet, neuman,
                                      std::min(n2_gas_sl[0] / (v_air_sl[0]), n2_gas_sl[1] / (v_air_sl[1])), atmos_value);

        /* O2 */
        atmos_value = ((*o2_concentration) * cbm::BAR_IN_PA) / RGAS_temp_atm * M_2O_SCALE;
        transport_.gas_diffusion_soil(have_plant_diffusion, D_O2_AIR_SCALE, D_eff_air_sl, o2_gas_sl, v_air_sl, interface_delta_x_air_sl,
                                      subdaily_o2_soil[subdaily_time_step_], day_leach_o2, subdaily_o2_plant[subdaily_time_step_],
                                      dirichlet, neuman,
                                      std::min(o2_gas_sl[0] / (v_air_sl[0]), o2_gas_sl[1] / (v_air_sl[1])), atmos_value);

        /* N2O */
        atmos_value = ((cbm::PN2O * cbm::BAR_IN_PA) / RGAS_temp_atm * M_2N_SCALE);
        dirichlet_n2o = std::min(n2o_gas_sl[0] / (v_air_sl[0]), n2o_gas_sl[1] / (v_air_sl[1]));
        dirichlet_an_n2o = std::min(an_n2o_gas_sl[0] / (v_air_sl[0]), an_n2o_gas_sl[1] / (v_air_sl[1]));
        transport_.gas_diffusion_soil(have_plant_diffusion, D_N2O_AIR_SCALE, D_eff_air_sl, n2o_gas_sl, v_air_sl, interface_delta_x_air_sl,
                                      subdaily_n2o_soil[subdaily_time_step_], day_leach_n2o, subdaily_n2o_plant[subdaily_time_step_],
                                      dirichlet, neuman, dirichlet_n2o, atmos_value);
        transport_.gas_diffusion_soil(have_plant_diffusion, D_N2O_AIR_SCALE, D_eff_air_sl, an_n2o_gas_sl, v_air_sl, interface_delta_x_air_sl,
                                      subdaily_n2o_soil[subdaily_time_step_], day_leach_n2o, subdaily_n2o_plant[subdaily_time_step_],
                                      dirichlet, neuman, dirichlet_an_n2o, atmos_value);

        /* NO */
        atmos_value = (ts_no_concentration_fl[0] * cbm::PPM * cbm::BAR_IN_PA / RGAS_temp_atm * M_N_SCALE);
        dirichlet_no = std::min(no_gas_sl[0] / (v_air_sl[0]), no_gas_sl[1] / (v_air_sl[1]));
        dirichlet_an_no = std::min(an_no_gas_sl[0] / (v_air_sl[0]), an_no_gas_sl[1] / (v_air_sl[1]));
        transport_.gas_diffusion_soil(have_plant_diffusion, D_NO_AIR_SCALE, D_eff_air_sl, no_gas_sl, v_air_sl, interface_delta_x_air_sl,
                                      subdaily_no_soil[subdaily_time_step_], day_leach_no, subdaily_no_plant[subdaily_time_step_],
                                      dirichlet, neuman, dirichlet_no, atmos_value);
        transport_.gas_diffusion_soil(have_plant_diffusion, D_NO_AIR_SCALE, D_eff_air_sl, an_no_gas_sl, v_air_sl, interface_delta_x_air_sl,
                                      subdaily_no_soil[subdaily_time_step_], day_leach_no, subdaily_no_plant[subdaily_time_step_],
                                      dirichlet, neuman, dirichlet_an_no, atmos_value);

        /* CH4 */
        atmos_value = (ts_ch4_concentration_fl[0] * cbm::PPM * cbm::BAR_IN_PA / RGAS_temp_atm * M_C_SCALE);
        transport_.gas_diffusion_soil(have_plant_diffusion, D_CH4_AIR_SCALE, D_eff_air_sl, ch4_gas_sl, v_air_sl, interface_delta_x_air_sl,
                                      subdaily_ch4_soil[subdaily_time_step_], subdaily_ch4_leach[subdaily_time_step_], subdaily_ch4_plant[subdaily_time_step_],
                                      dirichlet, neuman,
                                      std::min(ch4_gas_sl[0] / (v_air_sl[0]), ch4_gas_sl[1] / (v_air_sl[1])), atmos_value);

        /* CO2 */
        atmos_value = (ts_co2_concentration_fl[0] * cbm::PPM * cbm::BAR_IN_PA / RGAS_temp_atm * M_C_SCALE);
        transport_.gas_diffusion_soil(have_plant_diffusion, D_CH4_AIR_SCALE, D_eff_air_sl, co2_gas_sl, v_air_sl, interface_delta_x_air_sl,
                                      subdaily_co2_soil[subdaily_time_step_], subdaily_co2_leach[subdaily_time_step_], subdaily_co2_plant[subdaily_time_step_],
                                      dirichlet, neuman,
                                      std::min(co2_gas_sl[0] / (v_air_sl[0]), co2_gas_sl[1] / (v_air_sl[1])), atmos_value);

        /* NH3 */
        atmos_value = 0.0; //potential nh3 concentrations in the canopy from airchemistry input are negelcted on purpose since this is violating N-balance
        dirichlet_nh3 = std::min(sc_.nh3_gas_sl[0] / (v_air_sl[0]), sc_.nh3_gas_sl[1] / (v_air_sl[1]));
        transport_.gas_diffusion_soil(have_plant_diffusion, D_NH3_AIR_SCALE, D_eff_air_sl, sc_.nh3_gas_sl, v_air_sl, interface_delta_x_air_sl,
                                      subdaily_nh3_soil[subdaily_time_step_], day_leach_nh3, subdaily_nh3_plant[subdaily_time_step_],
                                      dirichlet, neuman,
                                      std::min(sc_.nh3_gas_sl[0] / (v_air_sl[0]), sc_.nh3_gas_sl[1] / (v_air_sl[1])), atmos_value);
    }
    else
    {
        /* N2 */
        double dummy( 0.0);
        double atmos_value( (cbm::PN2 * cbm::BAR_IN_PA) / RGAS_temp_atm * M_2N_SCALE);
        transport_.gas_diffusion_soil(have_plant_diffusion, D_N2_AIR_SCALE, D_eff_air_sl, n2_gas_sl, v_air_sl, interface_delta_x_air_sl,
                                      dummy, dummy, dummy,
                                      dirichlet, neuman, atmos_value, atmos_value);

        /* O2 */
        atmos_value = ((*o2_concentration) * cbm::BAR_IN_PA) / RGAS_temp_atm * M_2O_SCALE;
        transport_.gas_diffusion_soil(have_plant_diffusion, D_O2_AIR_SCALE, D_eff_air_sl, o2_gas_sl, v_air_sl, interface_delta_x_air_sl,
                                      subdaily_o2_soil[subdaily_time_step_], day_leach_o2, subdaily_o2_plant[subdaily_time_step_],
                                      dirichlet, neuman, atmos_value, atmos_value);

        /* N2O */
        atmos_value = ((cbm::PN2O * cbm::BAR_IN_PA) / RGAS_temp_atm * M_2N_SCALE);
        dirichlet_n2o = atmos_value;
        transport_.gas_diffusion_soil(have_plant_diffusion, D_N2O_AIR_SCALE, D_eff_air_sl, n2o_gas_sl, v_air_sl, interface_delta_x_air_sl,
                                      subdaily_n2o_soil[subdaily_time_step_], day_leach_n2o, subdaily_n2o_plant[subdaily_time_step_],
                                      dirichlet, neuman, dirichlet_n2o, atmos_value);
        transport_.gas_diffusion_soil(have_plant_diffusion, D_N2O_AIR_SCALE, D_eff_air_sl, an_n2o_gas_sl, v_air_sl, interface_delta_x_air_sl,
                                      subdaily_n2o_soil[subdaily_time_step_], day_leach_n2o, subdaily_n2o_plant[subdaily_time_step_],
                                      dirichlet, neuman, dirichlet_n2o, atmos_value);

        /* NO */
        atmos_value = (ts_no_concentration_fl[0] * cbm::PPM * cbm::BAR_IN_PA / RGAS_temp_atm * M_N_SCALE);
        dirichlet_no = atmos_value;
        transport_.gas_diffusion_soil(have_plant_diffusion, D_NO_AIR_SCALE, D_eff_air_sl, no_gas_sl, v_air_sl, interface_delta_x_air_sl,
                                      subdaily_no_soil[subdaily_time_step_], day_leach_no, subdaily_no_plant[subdaily_time_step_],
                                      dirichlet, neuman, dirichlet_no, atmos_value);
        transport_.gas_diffusion_soil(have_plant_diffusion, D_NO_AIR_SCALE, D_eff_air_sl, an_no_gas_sl, v_air_sl, interface_delta_x_air_sl,
                                      subdaily_no_soil[subdaily_time_step_], day_leach_no, subdaily_no_plant[subdaily_time_step_],
                                      dirichlet, neuman, dirichlet_no, atmos_value);

        /* CH4 */
        atmos_value = (ts_ch4_concentration_fl[0] * cbm::PPM * cbm::BAR_IN_PA / RGAS_temp_atm * M_C_SCALE);
        transport_.gas_diffusion_soil(have_plant_diffusion, D_CH4_AIR_SCALE, D_eff_air_sl, ch4_gas_sl, v_air_sl, interface_delta_x_air_sl,
                                      subdaily_ch4_soil[subdaily_time_step_], subdaily_ch4_leach[subdaily_time_step_], subdaily_ch4_plant[subdaily_time_step_],
                                      dirichlet, neuman, atmos_value, atmos_value);

        /* CO2 */
        atmos_value = (ts_co2_concentration_fl[0] * cbm::PPM * cbm::BAR_IN_PA / RGAS_temp_atm * M_C_SCALE);
        transport_.gas_diffusion_soil(have_plant_diffusion, D_CO2_AIR_SCALE, D_eff_air_sl, co2_gas_sl, v_air_sl, interface_delta_x_air_sl,
                                      subdaily_co2_soil[subdaily_time_step_], subdaily_co2_leach[subdaily_time_step_], subdaily_co2_plant[subdaily_time_step_],
                                      dirichlet, neuman, atmos_value, atmos_value);

        int ts_total( 1);
        double const nh3_sum( sc_.nh3_gas_sl.sum()+sc_.nh3_liq_sl.sum());
        if ( cbm::flt_greater( nh3_sum, 0.1 * cbm::HA_IN_M2))
        {
            if ( cbm::flt_greater( nh3_sum, 10.0 * cbm::HA_IN_M2))
            {
                ts_total = static_cast<int>( ceil( 3600.0 * 24.0 / TOTAL_TIME_STEPS));
            }
            else if ( cbm::flt_greater( nh3_sum, 1.0 * cbm::HA_IN_M2))
            {
                ts_total = static_cast<int>( ceil( 360.0 * 24.0 / TOTAL_TIME_STEPS));
            }
            else
            {
                ts_total = static_cast<int>( ceil( 60.0 * 24.0 / TOTAL_TIME_STEPS));
            }
        }

        for ( int ts = 1; ts <= ts_total; ++ts)
        {
            for( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
            {
                //decrease water/air ratio for top soil layer approaching zero in order to support surface volatilization
                double const ratio_water_air( v_water_sl[sl] / v_air_sl[sl]);
                double const temp( mc_temp_sl[sl] + cbm::D_IN_K);
                double const fact_uc( cbm::RGAS * temp * ratio_water_air);

                double h_x_scale( get_henry_coefficient( mc_temp_sl[sl], henry_nh3) * fact_uc);
                MeTrX_dissolution_soil( h_x_scale, sc_.nh3_gas_sl[sl], sc_.nh3_liq_sl[sl]);
            }

            /* NH3 */
            atmos_value = 0.0; //potential nh3 concentrations in the canopy from airchemistry input are neglected on purpose since this is violating the N-balance
            dirichlet_nh3 = atmos_value;
            transport_.gas_diffusion_soil_atm( fl_cnt, have_plant_diffusion, D_NH3_AIR_SCALE/ts_total, nh3_fl, sc_.nh3_gas_sl,
                                               subdaily_nh3_soil[subdaily_time_step_], day_leach_nh3, subdaily_nh3_plant[subdaily_time_step_],
                                               atmos_value, atmos_value);
        }
    }

    if ( output_writer_.write_fluxes())
    {
        /* N2O */
        transport_.calculate_soil_fluxes( sl_.soil_layer_cnt(), D_N2O_AIR_SCALE, dirichlet_n2o, dirichlet, v_air_sl, interface_delta_x_air_sl, D_eff_air_sl, n2o_gas_sl, communicate_sl);
        for (size_t sl = 0; sl < sl_.soil_layer_cnt(); sl++)
        {
            accumulated_n_n2o_gas_diffusion_sl[sl] += communicate_sl[sl];
        }
        transport_.calculate_soil_fluxes( sl_.soil_layer_cnt(), D_N2O_AIR_SCALE, dirichlet_n2o, dirichlet, v_air_sl, interface_delta_x_air_sl, D_eff_air_sl, an_n2o_gas_sl, communicate_sl);
        for (size_t sl = 0; sl < sl_.soil_layer_cnt(); sl++)
        {
            accumulated_n_n2o_gas_diffusion_sl[sl] += communicate_sl[sl];
        }

        /* NO */
        transport_.calculate_soil_fluxes( sl_.soil_layer_cnt(), D_NO_AIR_SCALE, dirichlet_no, dirichlet, v_air_sl, interface_delta_x_air_sl, D_eff_air_sl, no_gas_sl, communicate_sl);
        for (size_t sl = 0; sl < sl_.soil_layer_cnt(); sl++)
        {
            accumulated_n_no_gas_diffusion_sl[sl] += communicate_sl[sl];
        }

        transport_.calculate_soil_fluxes( sl_.soil_layer_cnt(), D_NO_AIR_SCALE, dirichlet_no, dirichlet, v_air_sl, interface_delta_x_air_sl, D_eff_air_sl, an_no_gas_sl, communicate_sl);
        for (size_t sl = 0; sl < sl_.soil_layer_cnt(); sl++)
        {
            accumulated_n_no_gas_diffusion_sl[sl] += communicate_sl[sl];
        }

        /* NH3 */
        transport_.calculate_soil_fluxes( sl_.soil_layer_cnt(), D_NH3_AIR_SCALE, dirichlet_nh3, dirichlet, v_air_sl, interface_delta_x_air_sl, D_eff_air_sl, sc_.nh3_gas_sl, communicate_sl);
        for (size_t sl = 0; sl < sl_.soil_layer_cnt(); sl++)
        {
            accumulated_n_nh3_gas_diffusion_sl[sl] += communicate_sl[sl];
        }
    }

    return LDNDC_ERR_OK;
}



lerr_t
SoilChemistryMeTrX::MeTrX_pertubation()
{
    //Pertubation is only calculated once per day since it is a very slow process
    if ( lclock()->subday() == 1)
    {
        size_t sl_max_1( sl_.soil_layer_cnt()-1);
        double nmicro1_lowest_sl(n_micro_1_sl[sl_max_1]);
        double nmicro2_lowest_sl(n_micro_2_sl[sl_max_1]);
        double nmicro3_lowest_sl(n_micro_3_sl[sl_max_1]);
        double naorg_lowest_sl(sc_.N_aorg_sl[sl_max_1]);
        double nlit1_lowest_sl(sc_.N_lit1_sl[sl_max_1]);
        double nlit2_lowest_sl(sc_.N_lit2_sl[sl_max_1]);
        double nlit3_lowest_sl(sc_.N_lit3_sl[sl_max_1]);
        double nhum1_lowest_sl(n_humus_1_sl[sl_max_1]);
        double nhum2_lowest_sl(n_humus_2_sl[sl_max_1]);
        double nhum3_lowest_sl(n_humus_3_sl[sl_max_1]);

        lerr_t rc = transport_.pertubation(1.0, D_eff_dailyturbation_sl, sc_.C_micro1_sl, sc_.h_sl, interface_delta_x_soil_sl);
        if ( rc){ return rc; }
        rc = transport_.pertubation(1.0, D_eff_dailyturbation_sl, n_micro_1_sl, sc_.h_sl, interface_delta_x_soil_sl);
        if ( rc){ return rc; }

        rc = transport_.pertubation(1.0, D_eff_dailyturbation_sl, sc_.C_micro2_sl, sc_.h_sl, interface_delta_x_soil_sl);
        if ( rc){ return rc; }
        rc = transport_.pertubation(1.0, D_eff_dailyturbation_sl, n_micro_2_sl, sc_.h_sl, interface_delta_x_soil_sl);
        if ( rc){ return rc; }

        rc = transport_.pertubation(1.0, D_eff_dailyturbation_sl, sc_.C_micro3_sl, sc_.h_sl, interface_delta_x_soil_sl);
        if ( rc){ return rc; }
        rc = transport_.pertubation(1.0, D_eff_dailyturbation_sl, n_micro_3_sl, sc_.h_sl, interface_delta_x_soil_sl);
        if ( rc){ return rc; }

        rc = transport_.pertubation(1.0, D_eff_dailyturbation_sl, sc_.C_aorg_sl, sc_.h_sl, interface_delta_x_soil_sl);
        if ( rc){ return rc; }
        rc = transport_.pertubation(1.0, D_eff_dailyturbation_sl, sc_.N_aorg_sl, sc_.h_sl, interface_delta_x_soil_sl);
        if ( rc){ return rc; }

        rc = transport_.pertubation(1.0, D_eff_dailyturbation_sl, sc_.C_lit1_sl, sc_.h_sl, interface_delta_x_soil_sl);
        if ( rc){ return rc; }
        rc = transport_.pertubation(1.0, D_eff_dailyturbation_sl, sc_.N_lit1_sl, sc_.h_sl, interface_delta_x_soil_sl);
        if ( rc){ return rc; }

        rc = transport_.pertubation(1.0, D_eff_dailyturbation_sl, sc_.C_lit2_sl, sc_.h_sl, interface_delta_x_soil_sl);
        if ( rc){ return rc; }
        rc = transport_.pertubation(1.0, D_eff_dailyturbation_sl, sc_.N_lit2_sl, sc_.h_sl, interface_delta_x_soil_sl);
        if ( rc){ return rc; }

        rc = transport_.pertubation(1.0, D_eff_dailyturbation_sl, sc_.C_lit3_sl, sc_.h_sl, interface_delta_x_soil_sl);
        if ( rc){ return rc; }
        rc = transport_.pertubation(1.0, D_eff_dailyturbation_sl, sc_.N_lit3_sl, sc_.h_sl, interface_delta_x_soil_sl);
        if ( rc){ return rc; }

        rc = transport_.pertubation(1.0, D_eff_dailyturbation_sl, c_humus_1_sl, sc_.h_sl, interface_delta_x_soil_sl);
        if ( rc){ return rc; }
        rc = transport_.pertubation(1.0, D_eff_dailyturbation_sl, n_humus_1_sl, sc_.h_sl, interface_delta_x_soil_sl);
        if ( rc){ return rc; }

        rc = transport_.pertubation(1.0, D_eff_dailyturbation_sl, c_humus_2_sl, sc_.h_sl, interface_delta_x_soil_sl);
        if ( rc){ return rc; }
        rc = transport_.pertubation(1.0, D_eff_dailyturbation_sl, n_humus_2_sl, sc_.h_sl, interface_delta_x_soil_sl);
        if ( rc){ return rc; }

        rc = transport_.pertubation(1.0, D_eff_dailyturbation_sl, c_humus_3_sl, sc_.h_sl, interface_delta_x_soil_sl);
        if ( rc){ return rc; }
        rc = transport_.pertubation(1.0, D_eff_dailyturbation_sl, n_humus_3_sl, sc_.h_sl, interface_delta_x_soil_sl);
        if ( rc){ return rc; }

        if ( output_writer_.write_fluxes())
        {
            /* nmicro */
            transport_.calculate_soil_perturbation_fluxes( sl_.soil_layer_cnt(), 1.0, sc_.h_sl, interface_delta_x_soil_sl, D_eff_dailyturbation_sl, n_micro_1_sl, nmicro1_lowest_sl, communicate_sl);
            for (size_t sl = 0; sl < sl_.soil_layer_cnt(); sl++)
            {
                accumulated_n_micro_perturbation_sl[sl] += communicate_sl[sl];
            }
            transport_.calculate_soil_perturbation_fluxes( sl_.soil_layer_cnt(), 1.0, sc_.h_sl, interface_delta_x_soil_sl, D_eff_dailyturbation_sl, n_micro_2_sl, nmicro2_lowest_sl, communicate_sl);
            for (size_t sl = 0; sl < sl_.soil_layer_cnt(); sl++)
            {
                accumulated_n_micro_perturbation_sl[sl] += communicate_sl[sl];
            }
            transport_.calculate_soil_perturbation_fluxes( sl_.soil_layer_cnt(), 1.0, sc_.h_sl, interface_delta_x_soil_sl, D_eff_dailyturbation_sl, n_micro_3_sl, nmicro3_lowest_sl, communicate_sl);
            for (size_t sl = 0; sl < sl_.soil_layer_cnt(); sl++)
            {
                accumulated_n_micro_perturbation_sl[sl] += communicate_sl[sl];
            }

            /* naorg */
            transport_.calculate_soil_perturbation_fluxes( sl_.soil_layer_cnt(), 1.0, sc_.h_sl, interface_delta_x_soil_sl, D_eff_dailyturbation_sl, sc_.N_aorg_sl, naorg_lowest_sl, communicate_sl);
            for (size_t sl = 0; sl < sl_.soil_layer_cnt(); sl++)
            {
                accumulated_n_aorg_perturbation_sl[sl] += communicate_sl[sl];
            }

            /*litter*/
            transport_.calculate_soil_perturbation_fluxes( sl_.soil_layer_cnt(), 1.0, sc_.h_sl, interface_delta_x_soil_sl, D_eff_dailyturbation_sl, sc_.N_lit1_sl, nlit1_lowest_sl, communicate_sl);
            for (size_t sl = 0; sl < sl_.soil_layer_cnt(); sl++)
            {
                accumulated_n_litter_perturbation_sl[sl] += communicate_sl[sl];
            }
            transport_.calculate_soil_perturbation_fluxes( sl_.soil_layer_cnt(), 1.0, sc_.h_sl, interface_delta_x_soil_sl, D_eff_dailyturbation_sl, sc_.N_lit2_sl, nlit2_lowest_sl, communicate_sl);
            for (size_t sl = 0; sl < sl_.soil_layer_cnt(); sl++)
            {
                accumulated_n_litter_perturbation_sl[sl] += communicate_sl[sl];
            }
            transport_.calculate_soil_perturbation_fluxes( sl_.soil_layer_cnt(), 1.0, sc_.h_sl, interface_delta_x_soil_sl, D_eff_dailyturbation_sl, sc_.N_lit3_sl, nlit3_lowest_sl, communicate_sl);
            for (size_t sl = 0; sl < sl_.soil_layer_cnt(); sl++)
            {
                accumulated_n_litter_perturbation_sl[sl] += communicate_sl[sl];
            }

            /*humus*/
            transport_.calculate_soil_perturbation_fluxes( sl_.soil_layer_cnt(), 1.0, sc_.h_sl, interface_delta_x_soil_sl, D_eff_dailyturbation_sl, n_humus_1_sl, nhum1_lowest_sl, communicate_sl);
            for (size_t sl = 0; sl < sl_.soil_layer_cnt(); sl++)
            {
                accumulated_n_humus_1_perturbation_sl[sl] += communicate_sl[sl];
            }
            transport_.calculate_soil_perturbation_fluxes( sl_.soil_layer_cnt(), 1.0, sc_.h_sl, interface_delta_x_soil_sl, D_eff_dailyturbation_sl, n_humus_2_sl, nhum2_lowest_sl, communicate_sl);
            for (size_t sl = 0; sl < sl_.soil_layer_cnt(); sl++)
            {
                accumulated_n_humus_2_perturbation_sl[sl] += communicate_sl[sl];
            }
            transport_.calculate_soil_perturbation_fluxes( sl_.soil_layer_cnt(), 1.0, sc_.h_sl, interface_delta_x_soil_sl, D_eff_dailyturbation_sl, n_humus_3_sl, nhum3_lowest_sl, communicate_sl);
            for (size_t sl = 0; sl < sl_.soil_layer_cnt(); sl++)
            {
                accumulated_n_humus_3_perturbation_sl[sl] += communicate_sl[sl];
            }
        }
    }
    return LDNDC_ERR_OK;
}



/*!
 * @page metrx
 * @section MeTrX_liq_diffusion Diffusion of dissolved species
 *   Diffusion of dissolved species is calculated for:
 *    - \f$ O_2 \f$
 *    - \f$ CH_4 \f$
 *    - \f$ CO_2 \f$
 *    - \f$ NH_3 \f$
 *    - Urea
 *    - \f$ NH_4 \f$
 *    - \f$ NO_3 \f$
 *    - DOC
 *
 */
void
SoilChemistryMeTrX::MeTrX_liq_diffusion()
{
    size_t sl_max_1( sl_.soil_layer_cnt()-1);
    size_t sl_max_2( sl_.soil_layer_cnt()-2);

    double urea_lowest_sl(sc_.urea_sl[sl_max_1]);
    double nh3_liq_lowest_sl(sc_.nh3_liq_sl[sl_max_1]);
    double nh4_lowest_sl(nh4_ae_sl[sl_max_1]);
    double no3_lowest_sl(no3_ae_sl[sl_max_1]);
    double an_no3_lowest_sl(no3_an_sl[sl_max_1]);

    double o2_out( 0.0);
    if ( cbm::flt_greater( o2_liq_sl.sum() + sb_.o2_sbl.sum(), 1.0e-5))
    {
        transport_.liq_diffusion( D_O2_WATER_SCALE, D_eff_water_sl, sb_.o2_sbl, o2_liq_sl, v_water_sl, interface_delta_x_water_sl,
                                  o2_out, neuman, dirichlet,
                                  std::min(o2_liq_sl[sl_max_1] / v_water_sl[sl_max_1],
                                           o2_liq_sl[sl_max_2] / v_water_sl[sl_max_2]));
        MeTrX_liq_diffusion_scale( o2_out, o2_liq_sl);
    }

    double ch4_out( 0.0);
    if ( cbm::flt_greater( ch4_liq_sl.sum() + sb_.ch4_sbl.sum(), 1.0e-5))
    {
        transport_.liq_diffusion( D_CH4_WATER_SCALE, D_eff_water_sl, sb_.ch4_sbl, ch4_liq_sl, v_water_sl, interface_delta_x_water_sl,
                                  ch4_out, neuman, dirichlet,
                                  std::min(ch4_liq_sl[sl_max_1] / v_water_sl[sl_max_1],
                                           ch4_liq_sl[sl_max_2] / v_water_sl[sl_max_2]));
        MeTrX_liq_diffusion_scale( ch4_out, ch4_liq_sl);
    }

    double co2_out( 0.0);
    if ( cbm::flt_greater( co2_liq_sl.sum() + sb_.co2_sbl.sum(), 1.0e-5))
    {
        transport_.liq_diffusion( D_CO2_WATER_SCALE, D_eff_water_sl, sb_.co2_sbl, co2_liq_sl, v_water_sl, interface_delta_x_water_sl,
                                  co2_out, neuman, dirichlet,
                                  std::min(co2_liq_sl[sl_max_1] / v_water_sl[sl_max_1],
                                           co2_liq_sl[sl_max_2] / v_water_sl[sl_max_2]));
        MeTrX_liq_diffusion_scale( co2_out, co2_liq_sl);
    }

    double urea_out( 0.0);
    if ( cbm::flt_greater( sc_.urea_sl.sum() + sb_.urea_sbl.sum(), 1.0e-5))
    {
        transport_.liq_diffusion( D_NH3_WATER_SCALE, D_eff_water_sl, sb_.urea_sbl, sc_.urea_sl, v_water_sl, interface_delta_x_water_sl,
                                  urea_out, neuman, dirichlet,
                                  std::min(sc_.urea_sl[sl_max_1] / v_water_sl[sl_max_1],
                                           sc_.urea_sl[sl_max_2] / v_water_sl[sl_max_2]));
        MeTrX_liq_diffusion_scale( urea_out, sc_.urea_sl);
    }

    double nh3_out( 0.0);
    if ( cbm::flt_greater( sc_.nh3_liq_sl.sum() + sb_.nh3_sbl.sum(), 1.0e-5))
    {
        transport_.liq_diffusion( D_NH3_WATER_SCALE, D_eff_water_sl, sb_.nh3_sbl, sc_.nh3_liq_sl, v_water_sl, interface_delta_x_water_sl,
                                  nh3_out, neuman, dirichlet,
                                  std::min(sc_.nh3_liq_sl[sl_max_1] / v_water_sl[sl_max_1],
                                           sc_.nh3_liq_sl[sl_max_2] / v_water_sl[sl_max_2]));
        MeTrX_liq_diffusion_scale( nh3_out, sc_.nh3_liq_sl);
    }

    double nh4_out( 0.0);
    if ( cbm::flt_greater( nh4_ae_sl.sum() + sb_.nh4_sbl.sum(), 1.0e-5))
    {
        transport_.liq_diffusion( D_NH4_WATER_SCALE, D_eff_water_sl, sb_.nh4_sbl, nh4_ae_sl, v_water_sl, interface_delta_x_water_sl,
                                  nh4_out, neuman, dirichlet,
                                  std::min(nh4_ae_sl[sl_max_1] / v_water_sl[sl_max_1],
                                           nh4_ae_sl[sl_max_2] / v_water_sl[sl_max_2]));
        MeTrX_liq_diffusion_scale( nh4_out, nh4_ae_sl);
    }

    double coated_nh4_out( 0.0);
    if ( cbm::flt_greater( sc_.coated_nh4_sl.sum() + sb_.coated_nh4_sbl.sum(), 1.0e-5))
    {
        transport_.liq_diffusion( D_NH4_WATER_SCALE, D_eff_water_sl, sb_.coated_nh4_sbl, sc_.coated_nh4_sl, v_water_sl, interface_delta_x_water_sl,
                                  coated_nh4_out, neuman, dirichlet,
                                  std::min(sc_.coated_nh4_sl[sl_max_1] / v_water_sl[sl_max_1],
                                           sc_.coated_nh4_sl[sl_max_2] / v_water_sl[sl_max_2]));
        MeTrX_liq_diffusion_scale( coated_nh4_out, sc_.coated_nh4_sl);
    }

    double no3_ae_out( 0.0);
    if ( cbm::flt_greater( no3_ae_sl.sum() + sb_.no3_sbl.sum(), 1.0e-5))
    {
        transport_.liq_diffusion( D_NO3_WATER_SCALE, D_eff_water_sl, sb_.no3_sbl, no3_ae_sl, v_water_sl, interface_delta_x_water_sl,
                                  no3_ae_out, neuman, dirichlet,
                                  std::min(no3_ae_sl[sl_max_1] / v_water_sl[sl_max_1],
                                           no3_ae_sl[sl_max_2] / v_water_sl[sl_max_2]));
        MeTrX_liq_diffusion_scale( no3_ae_out, no3_ae_sl);
    }

    //dummy variable for soil water flux calculation (below), needed due to no3 and an_no3 both exchanging with no3_sbl
    double no3_an_out( 0.0);
    lvector_t< double > no3_sbl_dum( sb_.surfacebulk_layer_cnt(), 0.0);
    if ( cbm::flt_greater( no3_an_sl.sum() + sb_.no3_sbl.sum(), 1.0e-5))
    {
        no3_sbl_dum[0] += sb_.no3_sbl[0];
        transport_.liq_diffusion( D_NO3_WATER_SCALE, D_eff_water_sl, sb_.no3_sbl, no3_an_sl, v_water_sl, interface_delta_x_water_sl,
                                  no3_an_out, neuman, dirichlet,
                                  std::min(no3_an_sl[sl_max_1] / v_water_sl[sl_max_1],
                                           no3_an_sl[sl_max_2] / v_water_sl[sl_max_2]));
        MeTrX_liq_diffusion_scale( no3_an_out, no3_an_sl);
    }

    double doc_out( 0.0);
    if ( cbm::flt_greater( sc_.an_doc_sl.sum() + sb_.doc_sbl.sum(), 1.0e-5))
    {
        transport_.liq_diffusion( D_DOC_WATER_SCALE, D_eff_water_sl, sb_.doc_sbl, sc_.an_doc_sl, v_water_sl, interface_delta_x_water_sl,
                                  doc_out, neuman, dirichlet,
                                  std::min(sc_.an_doc_sl[sl_max_1] / v_water_sl[sl_max_1],
                                           sc_.an_doc_sl[sl_max_2] / v_water_sl[sl_max_2]));
        MeTrX_liq_diffusion_scale( doc_out, sc_.an_doc_sl);
    }

    double so4_out( 0.0);
    if ( cbm::flt_greater( sc_.so4_sl.sum() + sb_.so4_sbl.sum(), 1.0e-5))
    {
        transport_.liq_diffusion( D_DOC_WATER_SCALE, D_eff_water_sl, sb_.so4_sbl, sc_.so4_sl, v_water_sl, interface_delta_x_water_sl,
                                  so4_out, neuman, dirichlet,
                                  std::min(sc_.so4_sl[sl_max_1] / v_water_sl[sl_max_1],
                                           sc_.so4_sl[sl_max_2] / v_water_sl[sl_max_2]));
        MeTrX_liq_diffusion_scale( so4_out, sc_.so4_sl);
    }

    if ( cbm::flt_greater( sc_.ni_sl.sum() + sb_.ni_sbl.sum(), 1.0e-5))
    {
        double ni_out( 0.0);
        transport_.liq_diffusion( D_DOC_WATER_SCALE, D_eff_water_sl, sb_.ni_sbl, sc_.ni_sl, v_water_sl, interface_delta_x_water_sl,
                                  ni_out, neuman, dirichlet,
                                  std::min(sc_.ni_sl[sl_max_1] / v_water_sl[sl_max_1],
                                           sc_.ni_sl[sl_max_2] / v_water_sl[sl_max_2]));
        MeTrX_liq_diffusion_scale( ni_out, sc_.ni_sl);
    }

    /*!
     * @page metrx
     * The amount of dissolved matter that leaves the simulated domain is added to the leaching output.
     */
    day_leach_o2 += o2_out;
    subdaily_ch4_leach[subdaily_time_step_] += ch4_out;
    subdaily_co2_leach[subdaily_time_step_] += co2_out;
    day_leach_urea += urea_out;
    day_leach_nh3 += nh3_out;
    sc_.accumulated_nh4_leach += nh4_out + coated_nh4_out;
    sc_.accumulated_no3_leach += no3_ae_out + no3_an_out;
    sc_.accumulated_doc_leach += doc_out;
    sc_.accumulated_so4_leach += so4_out;
    
    // calculate fluxes of N species due to liquid diffusion
    if ( output_writer_.write_fluxes())
    {
       /* urea */
       transport_.calculate_soil_water_fluxes(sl_.soil_layer_cnt(), D_NH3_WATER_SCALE, v_water_sl, interface_delta_x_water_sl,
                                              D_eff_water_sl, sb_.urea_sbl, sc_.urea_sl, urea_lowest_sl, communicate_sl);
       for (size_t sl = 0; sl < sl_.soil_layer_cnt(); sl++)
       {
           accumulated_n_urea_liq_diffusion_sl[sl] += communicate_sl[sl+1];
       }
       accumulated_n_urea_infiltration_liqdif_sl[0] += communicate_sl[0];

       /* NH3 liquid */
       transport_.calculate_soil_water_fluxes(sl_.soil_layer_cnt(), D_NH3_WATER_SCALE, v_water_sl, interface_delta_x_water_sl,
                                              D_eff_water_sl, sb_.nh3_sbl, sc_.nh3_liq_sl, nh3_liq_lowest_sl, communicate_sl);
       for (size_t sl = 0; sl < sl_.soil_layer_cnt(); sl++)
       {
           accumulated_n_nh3_liq_diffusion_sl[sl] += communicate_sl[sl+1];
       }
       accumulated_n_nh3_infiltration_liqdif_sl[0] += communicate_sl[0];

       /* NH4 */
       transport_.calculate_soil_water_fluxes(sl_.soil_layer_cnt(), D_NH4_WATER_SCALE, v_water_sl, interface_delta_x_water_sl,
                                              D_eff_water_sl, sb_.nh4_sbl, nh4_ae_sl, nh4_lowest_sl, communicate_sl);
       for (size_t sl = 0; sl < sl_.soil_layer_cnt(); sl++)
       {
           accumulated_n_nh4_liq_diffusion_sl[sl] += communicate_sl[sl+1];
       }
       accumulated_n_nh4_infiltration_liqdif_sl[0] += communicate_sl[0];

       /* NO3 aerobic */
       transport_.calculate_soil_water_fluxes(sl_.soil_layer_cnt(), D_NO3_WATER_SCALE, v_water_sl, interface_delta_x_water_sl,
                                              D_eff_water_sl, no3_sbl_dum, no3_ae_sl, no3_lowest_sl, communicate_sl);
       for (size_t sl = 0; sl < sl_.soil_layer_cnt(); sl++)
       {
           accumulated_n_no3_liq_diffusion_sl[sl] += communicate_sl[sl+1];
       }
       accumulated_n_no3_infiltration_liqdif_sl[0] += communicate_sl[0];

       /* NO3 anerobic */
       transport_.calculate_soil_water_fluxes(sl_.soil_layer_cnt(), D_NO3_WATER_SCALE, v_water_sl, interface_delta_x_water_sl,
                                              D_eff_water_sl, sb_.no3_sbl, no3_an_sl, an_no3_lowest_sl, communicate_sl);
       for (size_t sl = 0; sl < sl_.soil_layer_cnt(); sl++)
       {
           accumulated_n_no3_liq_diffusion_sl[sl] += communicate_sl[sl+1];
       }
       accumulated_n_no3_infiltration_liqdif_sl[0] += communicate_sl[0];
    }
}



/*!
 * @page metrx
 * If there is a calculated inflow of matter, there is a following correction of mass.
 * The mass is scaled down throughout the whole profile by the amount of calculated mass inflow.
 */
void
SoilChemistryMeTrX::MeTrX_liq_diffusion_scale( double &_out, lvector_t< double > &_scale)
{
    if ( cbm::flt_less( _out, 0.0))
    {
        _scale *= 1.0 + _out / _scale.sum();
        _out = 0.0;
    }
}



void
SoilChemistryMeTrX::MeTrX_ebullition_transfer(
                                              double &_from,
                                              double &_to,
                                              double _p,
                                              double _p_static)
{
    if ( cbm::flt_greater( _p, _p_static))
    {
        double const ebullition_fraction( (1.0 - _p_static / _p) * METRX_V_EBULLITION);
        double const trans_x( _from * cbm::bound_max( ebullition_fraction, 0.9));

        _from -= trans_x;
        _to += trans_x;
    }
}



/*!
 * @page metrx
 * @section MeTrX_ebullition Ebullition
 * Ebullition is calculated in the soil as well as in the water table.
 * Considered substances are CH4 and CO2.
 * Ebullition occurs as soon as total pressure (CH4+CO2) exceeds static pressure.
 */
void
SoilChemistryMeTrX::MeTrX_ebullition()
{
    double ch4_ebullition( 0.0);
    double co2_ebullition( 0.0);
    double o2_ebullition( 0.0);
    double nh3_ebullition( 0.0);
    double no_ebullition( 0.0);
    double n2o_ebullition( 0.0);
    double n2_ebullition( 0.0);

    for ( int sl = sl_.soil_layer_cnt()-1; sl >=0; sl--)
    {
        if ( cbm::flt_greater( MeTrX_get_wfps( sl), 0.9))
        {
            /* hydrostatic pressure (Pa) */
            double const p_static(cbm::BAR_IN_PA + (wc_.surface_water + sc_.depth_sl[sl]) * cbm::PA_IN_MH2O);
            /* conversion factor from kg to Pa (note: air volume decreased artifically in order to enhance ebullition) */
            double const fact_uc( cbm::RGAS * (mc_temp_sl[sl] + cbm::D_IN_K) / (0.3 * v_air_sl[sl]));
            
            double const p_ch4( ch4_gas_sl[sl] / M_C_SCALE * fact_uc);
            double const p_co2( co2_gas_sl[sl] / M_C_SCALE * fact_uc);
            double const p_o2( o2_gas_sl[sl] / M_2O_SCALE * fact_uc);
            double const p_nh3( sc_.nh3_gas_sl[sl] / M_N_SCALE * fact_uc);
            double const p_an_no( an_no_gas_sl[sl] / M_N_SCALE * fact_uc);
            double const p_an_n2o( an_n2o_gas_sl[sl] / M_2N_SCALE * fact_uc);
            double const p_n2( n2_gas_sl[sl] / M_2N_SCALE * fact_uc);
            
            MeTrX_ebullition_transfer( ch4_gas_sl[sl], ch4_ebullition, p_ch4, p_static);
            MeTrX_ebullition_transfer( co2_gas_sl[sl], co2_ebullition, p_co2, p_static);
            MeTrX_ebullition_transfer( o2_gas_sl[sl], o2_ebullition, p_o2, p_static);
            MeTrX_ebullition_transfer( sc_.nh3_gas_sl[sl], nh3_ebullition, p_nh3, p_static);
            MeTrX_ebullition_transfer( an_no_gas_sl[sl], no_ebullition, p_an_no, p_static);
            MeTrX_ebullition_transfer( an_n2o_gas_sl[sl], n2o_ebullition, p_an_n2o, p_static);
            MeTrX_ebullition_transfer( n2_gas_sl[sl], n2_ebullition, p_n2, p_static);
            
            accumulated_n_nh3_bubbling_sl[sl] += nh3_ebullition;
            accumulated_n_no_bubbling_sl[sl] += no_ebullition;
            accumulated_n_n2o_bubbling_sl[sl] += n2o_ebullition;
        }
        
        if ( sl >= 0)
        {
            ch4_gas_sl[sl] += ch4_ebullition;
            co2_gas_sl[sl] += co2_ebullition;
            o2_gas_sl[sl] += o2_ebullition;
            sc_.nh3_gas_sl[sl] += nh3_ebullition;
            an_no_gas_sl[sl] += no_ebullition;
            an_n2o_gas_sl[sl] += n2o_ebullition;
            n2_gas_sl[sl] += n2_ebullition;

            ch4_ebullition = 0.0;
            co2_ebullition = 0.0;
            o2_ebullition = 0.0;
            nh3_ebullition = 0.0;
            no_ebullition = 0.0;
            n2o_ebullition = 0.0;
            n2_ebullition = 0.0;
        }
        else
        {
            subdaily_ch4_bubbling[subdaily_time_step_] += ch4_ebullition;
            subdaily_co2_bubbling[subdaily_time_step_] += co2_ebullition;
            subdaily_o2_bubbling[subdaily_time_step_] += o2_ebullition;
            subdaily_nh3_bubbling[subdaily_time_step_] += nh3_ebullition;
            subdaily_no_bubbling[subdaily_time_step_] += no_ebullition;
            subdaily_n2o_bubbling[subdaily_time_step_] += n2o_ebullition;
            //n2 no balanced here on purpose
        }
    }

    if ( have_water_table)
    {
        for (size_t sbl = 0; sbl < sb_.surfacebulk_layer_cnt(); sbl++)
        {
            double const p_static(cbm::BAR_IN_PA + (wc_.surface_water - ((sbl*h_wl) + (0.5*h_wl))) * cbm::PA_IN_MH2O);

            double const p_ch4( sb_.ch4_sbl[sbl] / (M_C_SCALE * get_henry_coefficient( *mc_temp_atm, henry_ch4) * h_wl));
            double const p_co2( sb_.co2_sbl[sbl] / (M_C_SCALE * get_henry_coefficient( *mc_temp_atm, henry_co2) * h_wl));
            double const p_o2( sb_.o2_sbl[sbl] / (M_2O_SCALE * get_henry_coefficient( *mc_temp_atm, henry_o2) * h_wl));
            double const p_nh3( sb_.nh3_sbl[sbl] / (M_N_SCALE * get_henry_coefficient( *mc_temp_atm, henry_nh3) * h_wl));
            double const p_no( sb_.no_sbl[sbl] / (M_N_SCALE * get_henry_coefficient( *mc_temp_atm, henry_no) * h_wl));
            double const p_n2o( sb_.n2o_sbl[sbl] / (M_2N_SCALE * get_henry_coefficient( *mc_temp_atm, henry_n2o) * h_wl));

            MeTrX_ebullition_transfer( sb_.ch4_sbl[sbl], subdaily_ch4_bubbling[subdaily_time_step_], p_ch4, p_static);
            MeTrX_ebullition_transfer( sb_.co2_sbl[sbl], subdaily_co2_bubbling[subdaily_time_step_], p_co2, p_static);
            MeTrX_ebullition_transfer( sb_.o2_sbl[sbl], subdaily_o2_bubbling[subdaily_time_step_], p_o2, p_static);
            MeTrX_ebullition_transfer( sb_.nh3_sbl[sbl], subdaily_nh3_bubbling[subdaily_time_step_], p_nh3, p_static);
            MeTrX_ebullition_transfer( sb_.no_sbl[sbl], subdaily_no_bubbling[subdaily_time_step_], p_no, p_static);
            MeTrX_ebullition_transfer( sb_.n2o_sbl[sbl], subdaily_n2o_bubbling[subdaily_time_step_], p_n2o, p_static);

            accumulated_n_nh3_bubbling_sbl[0] += subdaily_nh3_bubbling[subdaily_time_step_];
            accumulated_n_no_bubbling_sbl[0] += subdaily_no_bubbling[subdaily_time_step_];
            accumulated_n_n2o_bubbling_sbl[0] += subdaily_n2o_bubbling[subdaily_time_step_];
        }
    }
}



/*!
 * @brief
 *
 */
#define CHANGE_ANVF(__val_1__,__val_2__,__trans__)       \
{                                                        \
(__val_1__) += (__trans__);                              \
(__val_2__) -= (__trans__);                              \
}                                                        \



/*!
 * @brief
 *
 */
#define DIFFUSE_ANVF(__val_1__,__val_2__,__ratio__,__diffuse__)              \
{                                                                            \
double const val_tot( (__val_1__) + (__val_2__));                            \
if ( cbm::flt_greater_zero( val_tot))                                      \
{                                                                            \
double const trans( ((__ratio__) * val_tot - (__val_2__)) * __diffuse__);    \
(__val_1__) -= trans;                                                        \
(__val_2__) += trans;                                                        \
}                                                                            \
}                                                                            \



/*!
 * @page metrx
 * @section MeTrX_anaerobic_volume Anaerobic volume
 * MeTrx considers for a subset of carbon and nitrogen species a horizontal (within one soil layer)
 * differentiation between aerobic and anaerobic parts of the soil. The differentiation is based on
 * the oxygen partial pressure, which in turn is mainly determined by the water profile, respiration
 * processes and gaseous diffusion of oxygen.
 *
 *  @image html metrx_anvf_profile.png "Scheme of soil profile anaerobicity" width=500
 *  @image latex metrx_anvf_profile.png "Scheme of soil profile anaerobicity" width=10cm
 */
void
SoilChemistryMeTrX::MeTrX_anaerobic_volume()
{
    for( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
    {
        /*!
         * @page metrx
         * The anaerobic volume \f$ V_{an} \f$ within one soil layer is given by:
         * \f[
         *  V_{an} = e^{-(7 p_{O_2})^2}
         * \f]
         * wherein \f$ p_{O_2} \f$ refers to the oxygen partial pressure in bar.
         *
         *  @image html metrx_anaerobic_volume_fraction_on_o2.png "Size of the anaerobic volume fraction depending on partial  pressure of oxygen" width=900
         *  @image latex metrx_anaerobic_volume_fraction_on_o2.png "Size of the anaerobic volume fraction depending on partial  pressure of oxygen" width=8cm
         */
        double const anvf_old( sc_.anvf_sl[sl]);
        double const p_o2( o2_gas_sl[sl] * RGAS_M_O2_BAR_PA * (mc_temp_sl[sl] + cbm::D_IN_K) / v_air_sl[sl]);
        if ( anvf_method == "default")
        {
            sc_.anvf_sl[sl] = cbm::bound( 0.001,
                                          exp( -std::pow(sipar_.METRX_F_ANVF_2() * p_o2, sipar_.METRX_F_ANVF_1())),
                                          0.999);
        }
        else
        {
            sc_.anvf_sl[sl] = cbm::bound( 0.001,
                                          sc_.anvf_sl[sl] - 
                                         (sc_.anvf_sl[sl] - exp( -std::pow(sipar_.METRX_F_ANVF_2() * p_o2, sipar_.METRX_F_ANVF_1()))) * FTS_TOT_,
                                          0.999);
        }

#ifdef  METRX_ANVF_TYPES
        double const pnet_dndc_a( 1.0);
        double const pnet_dndc_b( 1.0);
        anvf_pnet_sl[sl] = cbm::bound( 0.001,
                                      pnet_dndc_a * (1.0 - (pnet_dndc_b * p_o2 / cbm::PO2)),
                                      0.999);
        
        double const o2_dndccan( 0.0);
        anvf_dndccan_sl[sl] = cbm::bound( 0.001,
                                         1000 * ((0.816 - 0.01475 * std::log(1.0 / o2_dndccan + 0.0)) / 2.3026),
                                         0.999);
        
        double const a_nloss( 1.5e-6);
        double const alpha( 1.26);
        double const pressure_head( ldndc::hydrology::capillary_pressure( wc_.wc_sl[sl] / sc_.poro_sl[sl],
                                                                          sc_.vga_sl[sl], sc_.vgn_sl[sl], sc_.vgm_sl[sl],
                                                                          sc_.wfps_max_sl[sl], sc_.wfps_min_sl[sl]) / (cbm::G * cbm::DWAT)); // [m]

        double const r( 14.84 / pressure_head * 1.0e-6);   // simple relationship of radius depending on suction head (Warrick Soil Physics) [m]
        
        double const V( 20.0e-6);
        double const beta( 0.6);
        
        double const o2( (o2_gas_sl[sl] / M_2O_SCALE) / v_air_sl[sl]); // o2 concentration in the pore air [mol:m-3]
        double const gamma( 0.6);
        
        double const phi( v_water_sl[sl] / sc_.h_sl[sl]);   //soil water content [0-1]
        double const xi( cbm::DAIR / cbm::DWAT);
        double const epsilon( v_air_sl[sl] / (sc_.h_sl[sl] * sc_.poro_sl[sl]));   //air filled porosity [0-1]
        double const delta( 0.85);
        
        anvf_nloss_sl[sl] = cbm::bound( 0.001,
                                       std::exp(-a_nloss * std::pow(r, -alpha) * std::pow(V, -beta) * std::pow(o2, gamma) * std::pow(phi + xi * epsilon, delta)),
                                       0.999);
#endif

        /*!
         * @page metrx
         *  A change of the size of the anaerobic volume induces redistribution of
         *  the aerobic and anerobic part of carbon and nitrogen species.
         *
         *  Redistribution occurs by:
         *  - Relative change of the size of the anaerobic volume
         *  - Active transport between aerobic and anaerobic volume
         *
         *  @image html metrx_anvf_aggregate.png "Scheme of (an-)aerobic soil fractions" width=500
         *  @image latex metrx_anvf_aggregate.png "Scheme of (an-)aerobic soil fractions" width=8cm
         *
         *  The relative change of a species \f$ c_x \f$ only takes place if the share between aerobic and anaerobic volume changes:
         *  \f[
         *  \frac{\partial c_{x,an}}{\partial t} = \frac{c_{x,an}}{V_{an}} \frac{\partial V_{an}}{\partial t}
         *  \f]
         *  \f[
         *  \frac{\partial c_{x,ae}}{\partial t} = -\frac{\partial c_{x,an}}{\partial t}
         *  \f]
         */
        double const anvf_diff( (sc_.anvf_sl[sl] - anvf_old));
        if( cbm::flt_greater_equal_zero( anvf_diff))
        {
            double const anvf_ratio( anvf_diff / (1.0 - anvf_old));
            CHANGE_ANVF(sc_.an_doc_sl[sl], sc_.doc_sl[sl], sc_.doc_sl[sl] * anvf_ratio)
            CHANGE_ANVF(an_acetate_sl[sl], ae_acetate_sl[sl], ae_acetate_sl[sl] * anvf_ratio)
            
            CHANGE_ANVF(nh4_an_sl[sl], nh4_ae_sl[sl], nh4_ae_sl[sl] * anvf_ratio)
            CHANGE_ANVF(don_an_sl[sl], don_ae_sl[sl], don_ae_sl[sl] * anvf_ratio)
            CHANGE_ANVF(no3_an_sl[sl], no3_ae_sl[sl], no3_ae_sl[sl] * anvf_ratio)
            CHANGE_ANVF(sc_.an_no2_sl[sl], sc_.no2_sl[sl], sc_.no2_sl[sl] * anvf_ratio)
            CHANGE_ANVF(an_no_gas_sl[sl], no_gas_sl[sl], no_gas_sl[sl] * anvf_ratio)
            CHANGE_ANVF(an_no_liq_sl[sl], no_liq_sl[sl], no_liq_sl[sl] * anvf_ratio)
            CHANGE_ANVF(an_n2o_gas_sl[sl], n2o_gas_sl[sl], n2o_gas_sl[sl] * anvf_ratio)
            CHANGE_ANVF(an_n2o_liq_sl[sl], n2o_liq_sl[sl], n2o_liq_sl[sl] * anvf_ratio)
        }
        else
        {
            double const anvf_ratio( -anvf_diff / anvf_old);
            CHANGE_ANVF(sc_.doc_sl[sl], sc_.an_doc_sl[sl], sc_.an_doc_sl[sl] * anvf_ratio)
            CHANGE_ANVF(ae_acetate_sl[sl], an_acetate_sl[sl], an_acetate_sl[sl] * anvf_ratio)

            CHANGE_ANVF(nh4_ae_sl[sl], nh4_an_sl[sl], nh4_an_sl[sl] * anvf_ratio)
            CHANGE_ANVF(don_ae_sl[sl], don_an_sl[sl], don_an_sl[sl] * anvf_ratio)
            CHANGE_ANVF(no3_ae_sl[sl], no3_an_sl[sl], no3_an_sl[sl] * anvf_ratio)
            CHANGE_ANVF(sc_.no2_sl[sl], sc_.an_no2_sl[sl], sc_.an_no2_sl[sl] * anvf_ratio)
            CHANGE_ANVF(no_gas_sl[sl], an_no_gas_sl[sl], an_no_gas_sl[sl] * anvf_ratio)
            CHANGE_ANVF(no_liq_sl[sl], an_no_liq_sl[sl], an_no_liq_sl[sl] * anvf_ratio)
            CHANGE_ANVF(n2o_gas_sl[sl], an_n2o_gas_sl[sl], an_n2o_gas_sl[sl] * anvf_ratio)
            CHANGE_ANVF(n2o_liq_sl[sl], an_n2o_liq_sl[sl], an_n2o_liq_sl[sl] * anvf_ratio)
        }

        /*!
         * @page metrx
         *  The active transport of a species \f$ c_x \f$ is given by:
         *  \f[
         *  \frac{\partial c_{x,an}}{\partial t} = T^{\ast} \left ( c_{x,an} - c_{x,tot}  \frac{V_{an}}{V_{tot}} \right )
         *  \f]
         *  \f[
         *  \frac{\partial c_{x,ae}}{\partial t} = -\frac{\partial c_{x,an}}{\partial t}
         *  \f]
         *  The transport coefficient \f$ T^{\ast} \f$ is determined by the two parameters:
         *  - \f$ METRX\_KR\_ANVF\_DIFF\_GAS \f$
         *  - \f$ METRX\_KR\_ANVF\_DIFF\_LIQ \f$
         *
         *  for gaseous and dissolved species, respectively.
         */
        double const fact_anvf( (1.0 - 3.5 * cbm::sqr( sc_.anvf_sl[sl] - 0.5)) * FTS_TOT_);
        double const fact_anvf_gas( fact_anvf * sipar_.METRX_KR_ANVF_DIFF_GAS());
        double const fact_anvf_liq( fact_anvf * sipar_.METRX_KR_ANVF_DIFF_LIQ());

        DIFFUSE_ANVF(sc_.doc_sl[sl], sc_.an_doc_sl[sl], sc_.anvf_sl[sl], fact_anvf_liq);
        DIFFUSE_ANVF(ae_acetate_sl[sl], an_acetate_sl[sl], sc_.anvf_sl[sl], fact_anvf_liq);

        DIFFUSE_ANVF(nh4_ae_sl[sl], nh4_an_sl[sl], sc_.anvf_sl[sl], fact_anvf_liq);
        DIFFUSE_ANVF(don_ae_sl[sl], don_an_sl[sl], sc_.anvf_sl[sl], fact_anvf_liq);
        DIFFUSE_ANVF(no3_ae_sl[sl], no3_an_sl[sl], sc_.anvf_sl[sl], fact_anvf_liq);
        DIFFUSE_ANVF(sc_.no2_sl[sl], sc_.an_no2_sl[sl], sc_.anvf_sl[sl], fact_anvf_liq);
        DIFFUSE_ANVF(no_gas_sl[sl], an_no_gas_sl[sl], sc_.anvf_sl[sl], fact_anvf_gas);
        DIFFUSE_ANVF(no_liq_sl[sl], an_no_liq_sl[sl], sc_.anvf_sl[sl], fact_anvf_liq);
        DIFFUSE_ANVF(n2o_gas_sl[sl], an_n2o_gas_sl[sl], sc_.anvf_sl[sl], fact_anvf_gas);
        DIFFUSE_ANVF(n2o_liq_sl[sl], an_n2o_liq_sl[sl], sc_.anvf_sl[sl], fact_anvf_liq);
    }
}

} /*namespace ldndc*/
