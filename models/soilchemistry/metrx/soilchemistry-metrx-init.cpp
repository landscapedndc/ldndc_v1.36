/*!
 * @file
 * @author
 *  - David Kraus
 *
 * @date
 *  May 12, 2014
 */

/*!
 * @page metrx
 * @tableofcontents
 * @section metrx_guide User guide
 *  MeTrx simulates carbon and nitrogen cycle of soils. Focus lies on the production and consumption
 *  of the greenhouse gases \f$ CO_2, CH_4 \f$ and \f$ N_2O \f$. Therewith related outputs include
 *  leaching of \f$ NO_3 \f$ and emissions of \f$ NH_3 \f$.
 *  Depending on the ecosystem of interest, several model options should be set
 *  distinguishing predominantly between agricultural rice production systems
 *  and other ecosystem tpyes (see: @ref metrxoptions).
 *  Microbial processes are especially relevant in the upper soil layers. Choose a spatial discretization of
 *  at maximum 0.5 cm for the upper 1cm soil depth. For a soil depth greater 1cm and smaller 10 cm,
 *  a spatal discretization of 1-2 cm per soil layer is recommended.
 *
 * @subsection metrx_guide_structure Model structure
 * MeTrx requires further models for:
 * - watercycle (e.g., soil water content)
 * - plant growth (e.g., litter production / nitrogen uptake)
 *
 * @subsection metrx_guide_parameters Model parameters
 * MeTrx includes the following siteparameters:
 * - GROUNDWATER_NUTRIENT_RENEWAL
 * - METRX_AMAX
 * - METRX_AMAX_ALGAE
 * - METRX_BETA_LITTER_TYPE
 * - METRX_BIOSYNTH_EFF
 * - METRX_CN_ALGAE
 * - METRX_CN_FRAC_HUM3
 * - METRX_CN_MIC_MIN
 * - METRX_D_EFF_REDUCTION
 * - METRX_FE_REDUCTION
 * - METRX_FRAC_FE_CH4_PROD
 * - METRX_F_CH4_OXIDATION_T_EXP_1
 * - METRX_F_CH4_PRODUCTION_T_EXP_1
 * - METRX_F_CHEMODENIT_PH_1
 * - METRX_F_CHEMODENIT_PH_2
 * - METRX_F_CHEMODENIT_T_1
 * - METRX_F_CHEMODENIT_T_2
 * - METRX_F_DECOMP_CLAY_1
 * - METRX_F_DECOMP_M_WEIBULL_1
 * - METRX_F_DECOMP_M_WEIBULL_2
 * - METRX_F_DECOMP_T_EXP_1
 * - METRX_F_DECOMP_T_EXP_2
 * - METRX_F_DENIT_M_WEIBULL_1
 * - METRX_F_DENIT_M_WEIBULL_2
 * - METRX_F_DENIT_PH_1
 * - METRX_F_DENIT_PH_2
 * - METRX_F_DENIT_N2O_PH_1
 * - METRX_F_DENIT_N2O_PH_2
 * - METRX_F_DENIT_N2_1
 * - METRX_F_DENIT_N2_2
 * - METRX_F_DENIT_NO
 * - METRX_F_NIT_NO_N2O_M_WEIBULL_1
 * - METRX_F_NIT_NO_N2O_M_WEIBULL_2
 * - METRX_F_NIT_NO_M_EXP_1
 * - METRX_F_NIT_NO_M_EXP_2
 * - METRX_F_NIT_NO_N2O_T_EXP_1
 * - METRX_F_NIT_NO_N2O_T_EXP_2
 * - METRX_F_NIT_PH_1
 * - METRX_F_NIT_PH_2
 * - METRX_F_N_ALGAE
 * - METRX_F_N_CH4_OXIDATION
 * - METRX_KF_NIT_NO_N2O
 * - METRX_KMM_AC_CH4_PROD
 * - METRX_KMM_AC_FE_RED
 * - METRX_KMM_CH4_CH4_OX
 * - METRX_KMM_C_DENIT
 * - METRX_KMM_C_MIC
 * - METRX_KMM_H2_CH4_PROD
 * - METRX_KMM_H2_FERM
 * - METRX_KMM_H2_FE_RED
 * - METRX_KMM_NH4_NIT
 * - METRX_KMM_NO2_NIT
 * - METRX_KMM_N_DENIT
 * - METRX_KMM_N_MIC
 * - METRX_KMM_O2_CH4_OX
 * - METRX_KMM_O2_FE_OX
 * - METRX_KMM_O2_NIT
 * - METRX_KMM_PH_INCREASE_FROM_UREA
 * - METRX_KR_ANVF_DIFF_GAS
 * - METRX_KR_ANVF_DIFF_LIQ
 * - METRX_KR_DC_ALGAE
 * - METRX_KR_DC_AORG
 * - METRX_KR_DC_SOL
 * - METRX_KR_DC_CEL
 * - METRX_KR_DC_LIG
 * - METRX_KR_DC_HUM1
 * - METRX_KR_DC_HUM2
 * - METRX_KR_DC_HUM3
 * - METRX_KR_DC_RAW_LITTER
 * - METRX_KR_DC_WOOD
 * - METRX_KR_DENIT_CHEMO
 * - METRX_KR_FRAG_ABOVE
 * - METRX_KR_HU_AORG_HUM1
 * - METRX_KR_HU_AORG_HUM2
 * - METRX_KR_HU_CEL
 * - METRX_KR_HU_DOC
 * - METRX_KR_HU_HUM1
 * - METRX_KR_HU_HUM2
 * - METRX_KR_HU_LIG
 * - METRX_KR_HU_SOL
 * - METRX_KR_OX_FE
 * - METRX_KR_REDUCTION_ANVF
 * - METRX_KR_REDUCTION_CN
 * - METRX_KR_REDUCTION_CONIFEROUS
 * - METRX_K_FE_FE_RED
 * - METRX_K_O2_CH4_PROD
 * - METRX_K_O2_FE_RED
 * - METRX_LIG_HUMIFICATION
 * - METRX_MIC_EFF_AEROBIC_RESPIRATION
 * - METRX_MIC_EFF_ANAEROBIC_RESPIRATION
 * - METRX_MIC_EFF_METHANE_OXIDATION
 * - METRX_MUEMAX_C_ALGAE
 * - METRX_MUEMAX_C_CH4_OX
 * - METRX_MUEMAX_C_CH4_PROD
 * - METRX_MUEMAX_C_DENIT
 * - METRX_MUEMAX_C_FERM
 * - METRX_MUEMAX_C_FE_RED
 * - METRX_MUEMAX_C_NIT
 * - METRX_MUEMAX_H2_CH4_PROD
 * - METRX_MUEMAX_N_ASSI
 * - METRX_NITRIFY_MAX
 * - METRX_RET_HUMUS
 * - METRX_RET_LITTER
 * - METRX_RET_MICROBES
 * - METRX_TILL_STIMULATION_1
 * - METRX_TILL_STIMULATION_2
 * - METRX_V_EBULLITION
 * - RETDOC
 * - RETNH4
 * - RETNO3
 * - TEXP
 *
 * @subsubsection metrx_guide_nx_emissions_calibration Calibration of N trace gases
 * The calibration of nitrogen trace gas emissions primarily addresses microbial metabolism,
 * anaerobic conditions, and transport. The processes of decomposition and humification of SOM
 * should be excluded or managed carefully in order to avoid compromising the overall stability
 * of SOM by inadvertently inducing unrealistically high losses or gains.
 *
 * Nitrification
 * - METRX_MUEMAX_C_NIT
 * - METRX_KMM_N_MIC
 * - METRX_KMM_C_MIC
 * - METRX_KF_NIT_NO_N2O
 * - METRX_F_NIT_NO_N2O_M_WEIBULL_1
 * - METRX_F_NIT_NO_M_EXP_1
 * - METRX_F_NIT_NO_M_EXP_2
 * - METRX_F_NIT_NO_N2O_T_EXP_1
 * - METRX_F_NIT_NO_N2O_T_EXP_2
 * - METRX_F_NIT_PH_1
 * - METRX_F_NIT_PH_2
 *
 * Denitrification
 * - METRX_MUEMAX_C_DENIT
 * - METRX_KMM_C_DENIT
 * - METRX_KMM_N_DENIT
 * - METRX_MIC_EFF_ANAEROBIC_RESPIRATION
 * - METRX_F_DENIT_PH_1 (relevant for low pH only)
 * - METRX_F_DENIT_PH_2 (relevant for low pH only)
 * - METRX_F_DENIT_N2O_PH_1 (relevant for low pH only)
 * - METRX_F_DENIT_N2O_PH_2 (relevant for low pH only)
 * - METRX_F_DENIT_M_WEIBULL_1
 * - METRX_F_DENIT_N2_1
 * - METRX_F_DENIT_N2_2
 * - METRX_F_DENIT_NO
 *
 * Microbial N assimilation
 * - METRX_MUEMAX_N_ASSI
 *
 * Microbial turnover (decay constants)
 * - A_MAX_MICRO_1
 * - METRX_KA_C_MIC
 *
 * Transport between aerobic/anaerobic microsites
 * - METRX_KR_ANVF_DIFF_LIQ
 * - METRX_KR_ANVF_DIFF_GAS
 *
 * Anaerobicity
 * - METRX_F_ANVF_1
 * - METRX_F_ANVF_2
 *
 * Determining ratio between CO2 and DOC production from microbial turnover
 * - METRX_BIOSYNTH_EFF
 * - METRX_CN_MIC_MIN
 * - METRX_CN_MIC_MAX
 * - METRX_MIC_EFF_AEROBIC_RESPIRATION
 *
 * Decomposition/humification
 * - METRX_KR_HU_DOC
 * - METRX_KR_DC_AORG
 * - METRX_KR_REDUCTION_ANVF
 *
 */

#include  "soilchemistry/metrx/soilchemistry-metrx.h"
#include  <input/groundwater/groundwater.h>
#include  <constants/cbm_const.h>
#include  <string/cbm_string.h>

namespace ldndc {

cbm::logger_t *  SoilChemistryMeTrXLogger = NULL;

const double  SoilChemistryMeTrX::TNORM = 293.2;
const double  SoilChemistryMeTrX::N_MINERALISATION_FRACTION = 0.5;


/*!
 * @brief
 *
 */
SoilChemistryMeTrX::SoilChemistryMeTrX(
                                       MoBiLE_State *  _state,
                                       cbm::io_kcomm_t *  _io,
                                       timemode_e  _timemode)
                                     : MBE_LegacyModel(
                                      _state,
                                      _timemode),
io_kcomm( _io),

gw_( NULL),
sipar_( _io->get_input_class_ref< input_class_siteparameters_t >()),
sl_( _io->get_input_class_ref< input_class_soillayers_t >()),
se_( _io->get_input_class_ref< input_class_setup_t >()),
sb_( _state->get_substate_ref< substate_surfacebulk_t >()),
ac_( _state->get_substate_ref< substate_airchemistry_t >()),
mc_( _state->get_substate_ref< substate_microclimate_t >()),
ph_( _state->get_substate_ref< substate_physiology_t >()),
sc_( _state->get_substate_ref< substate_soilchemistry_t >()),
wc_( _state->get_substate_ref< substate_watercycle_t >()),

m_veg( &_state->vegetation),

transport_( _state, _io),
crnf_( sipar_.EEF_CONTROLLED_RELEASE_Q10(),
       sipar_.EEF_CONTROLLED_RELEASE_LAG_PERIOD(),
       sipar_.EEF_CONTROLLED_RELEASE_T80(),
       sipar_.EEF_CONTROLLED_RELEASE_WFPS_FACTOR(),
       sipar_.EEF_MAXIMUM_NITRIFICATION_INHIBITION(),
       sipar_.EEF_NITRIFICATION_INHIBITION_STABILITY_1(),
       sipar_.EEF_NITRIFICATION_INHIBITION_STABILITY_2(),
       sipar_.EEF_NITRIFICATION_INHIBITION_DILUTION()),
timemode_( _timemode),

output_writer_( _state, _io, _timemode),
output_data_( NULL),

TOTAL_TIME_STEPS(    std::max( 24, int(LD_RtCfg.clk->time_resolution()))),
INTERNAL_TIME_STEPS( (_timemode == TMODE_SUBDAILY) ? std::max( 1,  24 / int(LD_RtCfg.clk->time_resolution())) : 24),

FTS_TOT_(1.0 / (double)TOTAL_TIME_STEPS),                                   // timestep-scaling for all !!DAILY!! variables

M_C_SCALE( cbm::MC * cbm::KG_IN_G),
M_2C_SCALE( 2.0 * M_C_SCALE),
M_N_SCALE( cbm::MN * cbm::KG_IN_G),
M_2H_SCALE( 2.0 * cbm::MH * cbm::KG_IN_G),
M_2N_SCALE( 2.0 * cbm::MN * cbm::KG_IN_G),
M_O_SCALE( cbm::MO * cbm::KG_IN_G),
M_2O_SCALE( 2.0 * cbm::MO * cbm::KG_IN_G),
M_FE_SCALE( cbm::MFE * cbm::KG_IN_G),

N_O2_RATIO_NITRIFICATION( 2.0 / 3.0 * M_N_SCALE / M_2O_SCALE),
ACETATE_RATIO_FERM_SYN( 2.0 / 3.0),

H2_MOL_CH4_KG_CONVERSION( M_C_SCALE / 4.0),                                 // 4 H2 + CO2 -> CH4 + 2 H2O
H2_KG_CH4_KG_CONVERSION( M_C_SCALE / (4.0 * M_2H_SCALE)),                   // 4 H2 + CO2 -> CH4 + 2 H2O
H2_KG_CH4_MOL_CONVERSION( 1.0 / (4.0 * M_2H_SCALE)),                        // 4 H2 + CO2 -> CH4 + 2 H2O

FE3_ACETATE_REDUCTION_RATIO( 4.0 * M_FE_SCALE / M_C_SCALE),                 // CHCOO(-) + 8Fe(3+) + 4 H2O -> 2 HCO3(-) + 8 Fe(2+) + 9 H(+) (Brock Biology of Microorganisms)
FE3_HYDROGEN_REDUCTION_RATIO( M_FE_SCALE / M_2H_SCALE),                     // H2 + 2 Fe(3+) + 4 H+ -> 2 Fe2+ + 6 H2O

O2_CONVERSION_FOR_CH4_OX( (2.0 - sipar_.METRX_MIC_EFF_METHANE_OXIDATION()) * M_2O_SCALE / M_C_SCALE),

NO3_MOLAR_MAX_FE_RED( 1.0e-4),
FE3_MOLAR_MAX_CH4_PROD( 8.0e-3),
O2_FE2_RATIO( 0.25 * M_2O_SCALE / M_FE_SCALE),                              // 2 Fe(2+) + 0.5 O2 + 2 H(+) -> 2 Fe(3+) + H2O

MICRO_C_MIN( 1.0e-9),                                                       // Minimum amount of microbes.    [kg m-3]

MC_MO2_RATIO( cbm::MC / (2.0 * cbm::MO)),

RGAS_M_O2_BAR_PA( cbm::RGAS / M_2O_SCALE * cbm::PA_IN_BAR),

K_DC_AORG( sipar_.METRX_KR_DC_AORG() * FTS_TOT_),
K_HU_AORG_HUM1( sipar_.METRX_KR_HU_AORG_HUM1() * FTS_TOT_),
K_HU_AORG_HUM2( sipar_.METRX_KR_HU_AORG_HUM2() * FTS_TOT_),

K_DC_SOL( sipar_.METRX_KR_DC_SOL() * FTS_TOT_),
K_HU_SOL( sipar_.METRX_KR_HU_SOL() * FTS_TOT_),
K_HU_DOC( sipar_.METRX_KR_HU_DOC() * FTS_TOT_),

K_DC_CEL( sipar_.METRX_KR_DC_CEL() * FTS_TOT_),
K_HU_CEL( sipar_.METRX_KR_HU_CEL() * FTS_TOT_),

K_DC_LIG( sipar_.METRX_KR_DC_LIG() * FTS_TOT_),
K_HU_LIG( sipar_.METRX_KR_HU_LIG() * FTS_TOT_),

K_DC_HUM1( sipar_.METRX_KR_DC_HUM1() * FTS_TOT_),
K_HU_HUM1( sipar_.METRX_KR_HU_HUM1() * FTS_TOT_),

K_DC_HUM2( sipar_.METRX_KR_DC_HUM2() * FTS_TOT_),
K_HU_HUM2( sipar_.METRX_KR_HU_HUM2() * FTS_TOT_),

K_DC_HUM3( sipar_.METRX_KR_DC_HUM3() * FTS_TOT_),

KR_DENIT_CHEMO( sipar_.METRX_KR_DENIT_CHEMO() * FTS_TOT_),

K_FRAG_STUBBLE( sipar_.METRX_KR_FRAG_ABOVE() * FTS_TOT_),
K_FRAG_ALGAE( sipar_.METRX_KR_DC_ALGAE() * FTS_TOT_),
K_FRAG_LITTER_ABOVE( 0.2 * sipar_.METRX_KR_DC_RAW_LITTER() * FTS_TOT_),
K_FRAG_LITTER_BELOW( sipar_.METRX_KR_DC_RAW_LITTER() * FTS_TOT_),
K_FRAG_WOOD_ABOVE( 0.2 * sipar_.METRX_KR_DC_WOOD() * FTS_TOT_),
K_FRAG_WOOD_BELOW( sipar_.METRX_KR_DC_WOOD() * FTS_TOT_),
KMM_O2_ROOT_RESPIRATION( std::min( sipar_.METRX_KMM_O2_CH4_OX(), 
                                   std::min( sipar_.METRX_KMM_O2_FE_OX(),
                                             sipar_.METRX_KMM_O2_NIT()))),

MUE_MAX_C_MICRO_1( sipar_.METRX_MUEMAX_C_MIC() * FTS_TOT_),
MUE_MAX_C_MICRO_2( sipar_.METRX_MUEMAX_C_DENIT() * FTS_TOT_),
MUE_MAX_C_MICRO_3( sipar_.METRX_MUEMAX_C_FERM() * FTS_TOT_),

METRX_MUEMAX_C_CH4_PROD( sipar_.METRX_MUEMAX_C_CH4_PROD() * FTS_TOT_),
METRX_MUEMAX_H2_CH4_PROD( sipar_.METRX_MUEMAX_H2_CH4_PROD() * FTS_TOT_),
METRX_MUEMAX_C_CH4_OX( sipar_.METRX_MUEMAX_C_CH4_OX() * FTS_TOT_),

METRX_MUEMAX_C_FE_RED( sipar_.METRX_MUEMAX_C_FE_RED() * FTS_TOT_),
METRX_KR_OX_FE( sipar_.METRX_KR_OX_FE() * FTS_TOT_),

MUE_MAX_N_ASSI( sipar_.METRX_MUEMAX_N_ASSI() * FTS_TOT_),

PH_SURFACE_WATER( 7.0),
PH_MAX( 10.0),

A_MAX_ALGAE( sipar_.METRX_AMAX_ALGAE() * FTS_TOT_),
A_MAX_MICRO_1( sipar_.METRX_AMAX() * FTS_TOT_),
A_MAX_MICRO_2( A_MAX_MICRO_1 * MUE_MAX_C_MICRO_2 / MUE_MAX_C_MICRO_1),       // A_MAX for mic2 is decreased according to MUE_MAX
A_MAX_MICRO_3( A_MAX_MICRO_1 * MUE_MAX_C_MICRO_3 / MUE_MAX_C_MICRO_1),       // A_MAX for mic3 is decreased according to MUE_MAX

METRX_F_DENIT_N2_MIN( sipar_.METRX_F_DENIT_N2_1() * sipar_.METRX_F_DENIT_N2_2()),
METRX_F_DENIT_N2_DELTA( cbm::bound_min(0.0, sipar_.METRX_F_DENIT_N2_2() - METRX_F_DENIT_N2_MIN)),

/* Diffusion Coefficients */

D_NO_WATER_SCALE( cbm::D_NO_WATER * cbm::SEC_IN_DAY * FTS_TOT_),
D_N2O_WATER_SCALE( cbm::D_N2O_WATER * cbm::SEC_IN_DAY * FTS_TOT_),
D_O2_WATER_SCALE( cbm::D_O2_WATER * cbm::SEC_IN_DAY * FTS_TOT_),
D_CO2_WATER_SCALE( cbm::D_CO2_WATER * cbm::SEC_IN_DAY * FTS_TOT_),
D_CH4_WATER_SCALE( cbm::D_CH4_WATER * cbm::SEC_IN_DAY * FTS_TOT_),

D_NH3_WATER_SCALE( cbm::D_NH4_WATER * cbm::SEC_IN_DAY * FTS_TOT_),
D_NH4_WATER_SCALE( cbm::D_NH4_WATER * cbm::SEC_IN_DAY * FTS_TOT_),
D_NO3_WATER_SCALE( cbm::D_NO3_WATER * cbm::SEC_IN_DAY * FTS_TOT_),
D_NO2_WATER_SCALE( cbm::D_NO2_WATER * cbm::SEC_IN_DAY * FTS_TOT_),
D_DOC_WATER_SCALE( cbm::D_DOC_WATER * cbm::SEC_IN_DAY * FTS_TOT_),

D_O2_AIR_SCALE( cbm::D_O2_AIR * cbm::SEC_IN_DAY * FTS_TOT_),
D_N2_AIR_SCALE( cbm::D_N2_AIR * cbm::SEC_IN_DAY * FTS_TOT_),
D_N2O_AIR_SCALE( cbm::D_N2O_AIR * cbm::SEC_IN_DAY * FTS_TOT_),
D_NO_AIR_SCALE( cbm::D_NO_AIR * cbm::SEC_IN_DAY * FTS_TOT_),
D_CH4_AIR_SCALE( cbm::D_CH4_AIR * cbm::SEC_IN_DAY * FTS_TOT_),
D_CO2_AIR_SCALE( cbm::D_CO2_AIR * cbm::SEC_IN_DAY * FTS_TOT_),
D_NH3_AIR_SCALE( cbm::D_NH3_AIR * cbm::SEC_IN_DAY * FTS_TOT_),

METRX_V_EBULLITION( sipar_.METRX_V_EBULLITION() * FTS_TOT_),
GROUNDWATER_NUTRIENT_ACCESS_RATE( sipar_.GROUNDWATER_NUTRIENT_RENEWAL() * FTS_TOT_),

/* Soil temperature */

mc_wind( (_timemode == TMODE_SUBDAILY) ? &mc_.ts_windspeed : &mc_.nd_windspeed),
mc_temp_atm( (_timemode == TMODE_SUBDAILY) ? &mc_.ts_airtemperature : &mc_.nd_airtemperature),
mc_temp_sl( (_timemode == TMODE_SUBDAILY) ? mc_.temp_sl : mc_.nd_temp_sl),

/* Algae */
c_algae( 0.0),
c_dead_algae( 0.0),
n_algae( 0.0),
n_dead_algae( 0.0),

nh3_fl( se_.canopylayers(), 0.0),

accumulated_c_litter_above_old( 0.0),
accumulated_c_litter_below_old( 0.0),

day_decomp_c_hum_1_sl( sl_.soil_layer_cnt(), 0.0),
day_decomp_c_hum_2_sl( sl_.soil_layer_cnt(), 0.0),
day_decomp_c_hum_3_sl( sl_.soil_layer_cnt(), 0.0),
day_decay_c_mic_sl( sl_.soil_layer_cnt(), 0.0),

day_c_humify_doc_hum_1_sl( sl_.soil_layer_cnt(), 0.0),
day_c_humify_sol_hum_1_sl( sl_.soil_layer_cnt(), 0.0),
day_c_humify_cel_hum_1_sl( sl_.soil_layer_cnt(), 0.0),
day_c_humify_lig_hum_1_sl( sl_.soil_layer_cnt(), 0.0),
day_c_humify_lig_hum_2_sl( sl_.soil_layer_cnt(), 0.0),
day_c_humify_mic_hum_1_sl( sl_.soil_layer_cnt(), 0.0),
day_c_humify_mic_hum_2_sl( sl_.soil_layer_cnt(), 0.0),
day_c_humify_hum_1_hum_2_sl( sl_.soil_layer_cnt(), 0.0),
day_c_humify_hum_2_hum_3_sl( sl_.soil_layer_cnt(), 0.0),

year_c_humify_doc_hum_1_sl( sl_.soil_layer_cnt(), 0.0),
year_c_humify_sol_hum_1_sl( sl_.soil_layer_cnt(), 0.0),
year_c_humify_cel_hum_1_sl( sl_.soil_layer_cnt(), 0.0),
year_c_humify_lig_hum_1_sl( sl_.soil_layer_cnt(), 0.0),
year_c_humify_lig_hum_2_sl( sl_.soil_layer_cnt(), 0.0),
year_c_humify_mic_hum_1_sl( sl_.soil_layer_cnt(), 0.0),
year_c_humify_mic_hum_2_sl( sl_.soil_layer_cnt(), 0.0),
year_c_humify_hum_1_hum_2_sl( sl_.soil_layer_cnt(), 0.0),
year_c_humify_hum_2_hum_3_sl( sl_.soil_layer_cnt(), 0.0),
year_c_decomp_hum_1_sl( sl_.soil_layer_cnt(), 0.0),
year_c_decomp_hum_2_sl( sl_.soil_layer_cnt(), 0.0),
year_c_decomp_hum_3_sl( sl_.soil_layer_cnt(), 0.0),
year_doc_prod_sl( sl_.soil_layer_cnt(), 0.0),
year_decay_c_mic_sl( sl_.soil_layer_cnt(), 0.0),
year_co2_hetero_sl( sl_.soil_layer_cnt(), 0.0),

co2_auto_sl( sl_.soil_layer_cnt(), 0.0),
co2_hetero_sl( sl_.soil_layer_cnt(), 0.0),
cn_hum_1_sl( sl_.soil_layer_cnt(), 0.0),
cn_hum_2_sl( sl_.soil_layer_cnt(), 0.0),
cn_hum_3_sl( sl_.soil_layer_cnt(), 0.0),
freeze_thaw_fact_sl( sl_.soil_layer_cnt(), 0.0),
dry_wet_fact_sl( sl_.soil_layer_cnt(), 1.0),
litter_type_fact_sl( sl_.soil_layer_cnt(), 1.0),
day_acetate_cons_fe3_sl( sl_.soil_layer_cnt(), 0.0),
day_h2_c_eq_cons_fe3_sl( sl_.soil_layer_cnt(), 0.0),
day_fe2_oxidation_sl( sl_.soil_layer_cnt(), 0.0),
day_nitrify_n2o_sl( sl_.soil_layer_cnt(), 0.0),
day_nit_no2_no3_sl( sl_.soil_layer_cnt(), 0.0),
cn_opt_mic_sl( sl_.soil_layer_cnt(), 0.0),
h2_sl( sl_.soil_layer_cnt(), 0.0),
o2_liq_sl( sl_.soil_layer_cnt(), 0.0),
o2_gas_sl( sl_.soil_layer_cnt(), 0.0),
ae_acetate_sl( sl_.soil_layer_cnt(), 0.0),
an_acetate_sl( sl_.soil_layer_cnt(), 0.0),
ch4_liq_sl( sl_.soil_layer_cnt(), 0.0),
ch4_gas_sl( sl_.soil_layer_cnt(), 0.0),
co2_liq_sl( sl_.soil_layer_cnt(), 0.0),
co2_gas_sl( sl_.soil_layer_cnt(), 0.0),
nh4_ae_sl( sl_.soil_layer_cnt(), 0.0),
nh4_an_sl( sl_.soil_layer_cnt(), 0.0),
don_ae_sl( sl_.soil_layer_cnt(), 0.0),
don_an_sl( sl_.soil_layer_cnt(), 0.0),
no3_ae_sl( sl_.soil_layer_cnt(), 0.0),
no3_an_sl( sl_.soil_layer_cnt(), 0.0),
n2o_liq_sl( sl_.soil_layer_cnt(), 0.0),
n2o_gas_sl( sl_.soil_layer_cnt(), 0.0),
an_n2o_liq_sl( sl_.soil_layer_cnt(), 0.0),
an_n2o_gas_sl( sl_.soil_layer_cnt(), 0.0),
n2_gas_sl( sl_.soil_layer_cnt(), 0.0),
no_liq_sl( sl_.soil_layer_cnt(), 0.0),
no_gas_sl( sl_.soil_layer_cnt(), 0.0),
an_no_liq_sl( sl_.soil_layer_cnt(), 0.0),
an_no_gas_sl( sl_.soil_layer_cnt(), 0.0),
c_microbial_necromass_sl( sl_.soil_layer_cnt(), 0.0),
n_microbial_necromass_sl( sl_.soil_layer_cnt(), 0.0),
c_humus_1_sl( sl_.soil_layer_cnt(), 0.0),
c_humus_2_sl( sl_.soil_layer_cnt(), 0.0),
c_humus_3_sl( sl_.soil_layer_cnt(), 0.0),
n_humus_1_sl( sl_.soil_layer_cnt(), 0.0),
n_humus_2_sl( sl_.soil_layer_cnt(), 0.0),
n_humus_3_sl( sl_.soil_layer_cnt(), 0.0),
n_micro_1_sl( sl_.soil_layer_cnt(), 0.0),
n_micro_2_sl( sl_.soil_layer_cnt(), 0.0),
n_micro_3_sl( sl_.soil_layer_cnt(), 0.0),
D_eff_air_fl( se_.canopylayers(), 5.0),
D_eff_air_sl( sl_.soil_layer_cnt(), 0.0),
D_eff_water_sl( sl_.soil_layer_cnt(), 0.0),
D_eff_dailyturbation_sl( sl_.soil_layer_cnt(), 0.0),
D_eff_plant_sl( sl_.soil_layer_cnt(), 0.0),
interface_air_sl( sl_.soil_layer_cnt(), 0.0),
interface_delta_x_air_sl( sl_.soil_layer_cnt(), 0.0),
interface_delta_x_water_sl( sl_.soil_layer_cnt(), 0.0),
interface_delta_x_soil_sl( sl_.soil_layer_cnt(), 0.0),
delta_x( sl_.soil_layer_cnt(), 0.0),
fact_uc_xl( sl_.soil_layer_cnt()+se_.canopylayers()+sb_.surfacebulk_layer_cnt()+1, 0.0),
pore_connectivity_sl( sl_.soil_layer_cnt(), 0.0),
ph_delta_pab_wl( 0.0),
ph_delta_urea_wl( 0.0),
ph_delta_urea_sl( sl_.soil_layer_cnt(), 0.0),
soil_gas_advection_sl( sl_.soil_layer_cnt(), 0.0),
v_water_sl( sl_.soil_layer_cnt(), 0.0),
v_air_sl( sl_.soil_layer_cnt(), 0.0),
v_ice_sl( sl_.soil_layer_cnt(), 0.0),
communicate_sl( sl_.soil_layer_cnt()+1, 0.0),
day_denit_factor_c_sl( sl_.soil_layer_cnt(), 0.0),
day_denit_factor_n_sl( sl_.soil_layer_cnt(), 0.0),
day_ch4_production_hydrogen_sl( sl_.soil_layer_cnt(), 0.0),
day_ch4_production_acetate_sl( sl_.soil_layer_cnt(), 0.0),
day_ch4_oxidation_sl( sl_.soil_layer_cnt(), 0.0),
day_ch4_bubbling_wl( sb_.surfacebulk_layer_cnt(), 0.0),
day_ch4_bubbling_sl( sl_.soil_layer_cnt(), 0.0),
subdaily_ch4_bubbling( TOTAL_TIME_STEPS, 0.0),
subdaily_ch4_plant( TOTAL_TIME_STEPS, 0.0),
subdaily_ch4_soil( TOTAL_TIME_STEPS, 0.0),
subdaily_ch4_water( TOTAL_TIME_STEPS, 0.0),
subdaily_ch4_leach( TOTAL_TIME_STEPS, 0.0),
subdaily_ch4_prod( TOTAL_TIME_STEPS, 0.0),
subdaily_ch4_ox( TOTAL_TIME_STEPS, 0.0),
subdaily_ch4_content( TOTAL_TIME_STEPS, 0.0),
subdaily_co2_bubbling( TOTAL_TIME_STEPS, 0.0),
subdaily_co2_plant( TOTAL_TIME_STEPS, 0.0),
subdaily_co2_soil( TOTAL_TIME_STEPS, 0.0),
subdaily_co2_water( TOTAL_TIME_STEPS, 0.0),
subdaily_co2_leach( TOTAL_TIME_STEPS, 0.0),
subdaily_acetate_prod( TOTAL_TIME_STEPS, 0.0),
subdaily_doc_prod( TOTAL_TIME_STEPS, 0.0),
subdaily_o2_bubbling( TOTAL_TIME_STEPS, 0.0),
subdaily_o2_plant( TOTAL_TIME_STEPS, 0.0),
subdaily_o2_soil( TOTAL_TIME_STEPS, 0.0),
subdaily_o2_water( TOTAL_TIME_STEPS, 0.0),
subdaily_n2o_bubbling( TOTAL_TIME_STEPS, 0.0),
subdaily_n2o_plant( TOTAL_TIME_STEPS, 0.0),
subdaily_n2o_soil( TOTAL_TIME_STEPS, 0.0),
subdaily_n2o_water( TOTAL_TIME_STEPS, 0.0),
subdaily_no_bubbling( TOTAL_TIME_STEPS, 0.0),
subdaily_no_plant( TOTAL_TIME_STEPS, 0.0),
subdaily_no_soil( TOTAL_TIME_STEPS, 0.0),
subdaily_no_water( TOTAL_TIME_STEPS, 0.0),
subdaily_nh3_bubbling( TOTAL_TIME_STEPS, 0.0),
subdaily_nh3_plant( TOTAL_TIME_STEPS, 0.0),
subdaily_nh3_soil( TOTAL_TIME_STEPS, 0.0),
subdaily_nh3_water( TOTAL_TIME_STEPS, 0.0),
subdaily_plant_o2_cons( TOTAL_TIME_STEPS, 0.0),
subdaily_flood_o2_conc( TOTAL_TIME_STEPS, 0.0),
subdaily_algae_o2_prod( TOTAL_TIME_STEPS, 0.0),
day_doc_prod_sl( sl_.soil_layer_cnt(), 0.0),
day_acetate_prod_sl( sl_.soil_layer_cnt(), 0.0),
day_h2_c_eq_prod_sl( sl_.soil_layer_cnt(), 0.0),

plant_o2_consumption_sl( sl_.soil_layer_cnt(), 0.0),


accumulated_n_nh4_throughfall_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_no3_throughfall_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_nh4_throughfall_sbl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_no3_throughfall_sbl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_nh4_throughfall_old( 0.0),
accumulated_n_no3_throughfall_old( 0.0),

accumulated_n_to_living_plant_and_algae_from_extern_sl( sl_.soil_layer_cnt(), 0.0),

accumulated_n_algae_nh4_uptake_sbl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_algae_no3_uptake_sbl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_algae_nh3_uptake_sbl( sl_.soil_layer_cnt(), 0.0),

accumulated_n_litter_from_plants_below_rawlitter_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_litter_from_plants_below_wood_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_litter_from_plants_above_rawlitter( 0.0),
accumulated_n_litter_from_plants_above_wood( 0.0),
accumulated_n_litter_from_plants_above_stubble( 0.0),
accumulated_n_aboveground_raw_litter_fragmentation_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_belowground_raw_litter_fragmentation_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_litter_from_algae_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_litter_from_dung_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_urea_from_dung_sl( sl_.soil_layer_cnt(), 0.0),

accumulated_n_export_harvest_cutting_grazing( 0.0),
accumulated_n_export_harvest_old(0.0),
accumulated_c_litter_wood_above_old(0.0),

accumulated_n_nh4_fertilization_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_no3_fertilization_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_nh3_fertilization_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_don_fertilization_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_urea_fertilization_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_litter_fertilization_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_aorg_fertilization_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_nh4_fertilization_sbl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_no3_fertilization_sbl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_nh3_fertilization_sbl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_urea_fertilization_sbl( sl_.soil_layer_cnt(), 0.0),

accumulated_n_surface_litter_incorporation_via_tilling_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_litter_tilling_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_aorg_tilling_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_humus_1_tilling_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_humus_2_tilling_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_humus_3_tilling_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_no3_tilling_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_no2_tilling_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_no_tilling_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_n2o_tilling_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_urea_tilling_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_don_tilling_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_nh4_tilling_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_nh3_tilling_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_microbes_tilling_sl( sl_.soil_layer_cnt(), 0.0),

accumulated_n_humus_1_spinup_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_humus_2_spinup_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_humus_3_spinup_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_surface_litter_spinup_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_litter_spinup_sl( sl_.soil_layer_cnt(), 0.0),

accumulated_n_urea_nh4_hydrolysis_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_urea_nh4_hydrolysis_sbl( sl_.soil_layer_cnt(), 0.0),

accumulated_n_nh4_assimilation_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_no3_assimilation_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_don_assimilation_sl( sl_.soil_layer_cnt(), 0.0),

accumulated_n_nh4_nh3_conversion_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_nh3_nh4_conversion_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_nh4_nh3_conversion_sbl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_nh3_nh4_conversion_sbl( sl_.soil_layer_cnt(), 0.0),

accumulated_n_micro_leaching_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_aorg_leaching_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_litter_leaching_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_humus_1_leaching_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_humus_2_leaching_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_humus_3_leaching_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_nh4_leaching_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_no3_leaching_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_nh3_leaching_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_don_leaching_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_urea_leaching_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_no_leaching_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_n2o_leaching_sl( sl_.soil_layer_cnt(), 0.0),

accumulated_n_urea_liq_diffusion_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_nh3_liq_diffusion_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_nh4_liq_diffusion_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_no3_liq_diffusion_sl( sl_.soil_layer_cnt(), 0.0),

accumulated_n_nh4_infiltration_phys_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_nh3_infiltration_phys_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_urea_infiltration_phys_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_no3_infiltration_phys_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_don_infiltration_phys_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_aorg_infiltration_phys_sl( sl_.soil_layer_cnt(), 0.0),

accumulated_n_nh4_infiltration_leach_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_nh3_infiltration_leach_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_urea_infiltration_leach_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_no3_infiltration_leach_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_don_infiltration_leach_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_no_infiltration_leach_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_n2o_infiltration_leach_sl( sl_.soil_layer_cnt(), 0.0),

accumulated_n_nh4_infiltration_liqdif_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_nh3_infiltration_liqdif_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_urea_infiltration_liqdif_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_no3_infiltration_liqdif_sl( sl_.soil_layer_cnt(), 0.0),

accumulated_n_nh3_bubbling_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_no_bubbling_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_n2o_bubbling_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_nh3_bubbling_sbl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_no_bubbling_sbl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_n2o_bubbling_sbl( sl_.soil_layer_cnt(), 0.0),

accumulated_n_nh3_dissolution_sbl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_no_dissolution_sbl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_n2o_dissolution_sbl( sl_.soil_layer_cnt(), 0.0),

accumulated_n_nh3_phys_diffusion_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_no_phys_diffusion_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_n2o_phys_diffusion_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_nh3_gas_diffusion_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_no_gas_diffusion_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_n2o_gas_diffusion_sl( sl_.soil_layer_cnt(), 0.0),

accumulated_n_micro_perturbation_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_aorg_perturbation_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_litter_perturbation_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_humus_1_perturbation_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_humus_2_perturbation_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_humus_3_perturbation_sl( sl_.soil_layer_cnt(), 0.0),

accumulated_n_nh4_no2_nitrification_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_nh4_no_nitrification_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_nh4_n2o_nitrification_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_no2_no3_nitrification_sl( sl_.soil_layer_cnt(), 0.0),

accumulated_n_no3_no2_denitrification_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_no3_no_denitrification_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_no3_n2o_denitrification_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_no3_n2_denitrification_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_no2_no_denitrification_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_no2_n2o_denitrification_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_no2_n2_denitrification_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_no_n2o_denitrification_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_no_n2_denitrification_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_n2o_n2_denitrification_sl( sl_.soil_layer_cnt(), 0.0),

accumulated_n_no2_chemodenitrification_sl( sl_.soil_layer_cnt(), 0.0),

accumulated_n_mic_naorg_decay_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_mic_nh4_mineral_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_mic_don_dissolve_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_aorg_nh4_mineral_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_aorg_don_dissolve_sl( sl_.soil_layer_cnt(), 0.0),

accumulated_n_humus_1_don_dissolve_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_humus_1_nh4_mineral_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_humus_2_don_dissolve_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_humus_2_nh4_mineral_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_humus_3_don_dissolve_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_humus_3_nh4_mineral_sl( sl_.soil_layer_cnt(), 0.0),

accumulated_n_litter_don_dissolve_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_litter_nh4_mineral_sl( sl_.soil_layer_cnt(), 0.0),

accumulated_n_don_humus_1_humify_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_don_humus_2_humify_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_don_humus_3_humify_sl( sl_.soil_layer_cnt(), 0.0),
accumulated_n_litter_humus_1_humify_sl( sl_.soil_layer_cnt(), 0.0),

accumulated_c_root_exsudates_old_sl( sl_.soil_layer_cnt(), 0.0),

#ifdef  METRX_ANVF_TYPES
anvf_pnet_sl( sl_.soil_layer_cnt(), 0.0),
anvf_dndccan_sl( sl_.soil_layer_cnt(), 0.0),
anvf_nloss_sl( sl_.soil_layer_cnt(), 0.0),
#endif

spinup_( 0, sl_.soil_layer_cnt())
{
    if ( !SoilChemistryMeTrXLogger)
    {
        SoilChemistryMeTrXLogger = CBM_RegisteredLoggers.new_logger( "SoilChemistryMeTrX");
    }

    MeTrX_reset_subdaily();
    MeTrX_reset_daily();
    MeTrX_reset_yearly();

    if ( timemode_ == TMODE_SUBDAILY)
    {
        o2_concentration = &ac_.ts_o2_concentration;
        ts_co2_concentration_fl.reference( ac_.ts_co2_concentration_fl);
        ts_ch4_concentration_fl.reference( ac_.ts_ch4_concentration_fl);
        ts_no_concentration_fl.reference( ac_.ts_no_concentration_fl);
    }
    else
    {
        o2_concentration = &ac_.nd_o2_concentration;
        ts_co2_concentration_fl.reference( ac_.nd_co2_concentration_fl);
        ts_ch4_concentration_fl.reference( ac_.nd_ch4_concentration_fl);
        ts_no_concentration_fl.reference( ac_.nd_no_concentration_fl);
    }

    this->have_algae = false;
    this->have_dry_wet = false;
    this->have_freeze_thaw = false;
    this->have_surface_bulk = false;
    this->have_canopy_transport = false;
    this->have_river_connection = false;

    have_water_table = false;
    have_plant_diffusion = false;

    infiltration = 0.0;
    accumulated_infiltration_old = 0.0;

    waterflux_sl = new double[sl_.soil_layer_cnt()];
    accumulated_waterflux_old_sl = new double[sl_.soil_layer_cnt()];
    for( size_t sl = 0; sl < sl_.soil_layer_cnt(); ++sl)
    {
        waterflux_sl[sl] = 0.0;
        accumulated_waterflux_old_sl[sl] = 0.0;
    }

    //Initialize specific soil layers with uppest (sl=0) soil layer
    SL_ATMOSPHERIC_CONTACT = 0;
    SL_SURFACE_DISTRIBUTION = 0;

    if ( sl_.soil_layers_in_litter_cnt() > 0)
    {
        SL_SURFACE_DISTRIBUTION = sl_.soil_layers_in_litter_cnt() - 1;
    }

    for( size_t sl = 0; sl < sl_.soil_layer_cnt(); ++sl)
    {
        if ( cbm::flt_greater_equal( 0.02, sc_.depth_sl[sl]))
        {
            if ( sl > SL_SURFACE_DISTRIBUTION)
            {
                SL_SURFACE_DISTRIBUTION = sl;
            }

        }
        if ( cbm::flt_greater_equal( 0.01, sc_.depth_sl[sl]))
        {
            SL_ATMOSPHERIC_CONTACT = sl;
        }
    }

    SL_SURFACE_DISTRIBUTION_INVERSE = 1.0 / ((double)SL_SURFACE_DISTRIBUTION + 1.0);

    io_kcomm->get_scratch()->set(
                                 "producer.fertilize.timemode", (int)_timemode);
    io_kcomm->get_scratch()->set(
                                 "producer.manure.timemode", (int)_timemode);
}



/*!
 * @brief
 *
 */
SoilChemistryMeTrX::~SoilChemistryMeTrX()
{
    if ( output_data_)
    {
        LD_Allocator->deallocate( output_data_);
    }

    delete[] waterflux_sl;
    delete[] accumulated_waterflux_old_sl;
}



/*!
 * @brief
 *
 */
lerr_t
SoilChemistryMeTrX::configure(
                              ldndc::config_file_t const *  _cf)
{
    std::string  loggerstream = get_option< char const * >( "loggerstream", "null");
    loggerstream = cbm::format_expand( loggerstream);
    SoilChemistryMeTrXLogger->initialize( loggerstream.c_str(), _cf->log_level());

    /*!
     * @page metrx
     * @section metrx_options Model options
     * Available model options:
     * Default options are marked with bold letters.
     *  - Algae (default: "algae" = \b no / yes)
     *    Set to \c yes for agricultural rice production ecosystems
     *    for which growth of algae should be considered.
     */
    have_algae = get_option< bool >( "algae", true);
    if( have_algae)
    {
        KLOGINFO_TO( SoilChemistryMeTrXLogger, "consider algae: ", cbm::n2s( have_algae));
    }
    
    /*!
     * @page metrx
     *  - Drywet (default: "drywet" = \b no / yes)
     *    Under construction.
     */
    have_dry_wet = get_option< bool >( "drywet", false);
    if ( have_dry_wet)
    {
        KLOGINFO_TO( SoilChemistryMeTrXLogger, "consider drywet: ", cbm::n2s( have_dry_wet));
    }
    
    /*!
     * @page metrx
     *  - Surface bulk, e.g., water table (default: "surfacebulk" = \b yes / no)
     *    Automatically includes additional surface layers in case surface water table builds up.
     */
    have_surface_bulk = get_option< bool >( "surfacebulk", true);
    if ( !have_surface_bulk)
    {
        KLOGINFO_TO( SoilChemistryMeTrXLogger, "consider surface bulk: ", cbm::n2s( have_surface_bulk));
    }
    
    /*!
     * @page metrx
     *  - Freeze thaw (default: "freezethaw" = \b no / yes)
     *    Under construction.
     */
    have_freeze_thaw = get_option< bool >( "freezethaw", false);
    if ( have_freeze_thaw)
    {
        KLOGINFO_TO( SoilChemistryMeTrXLogger, "consider freezethaw: ", cbm::n2s( have_freeze_thaw));
    }
    
    /*!
     * @page metrx
     *  - Canopy transport (default: "canopytransport" = \b no / yes)
     *    Considers diffusive transport of \f$ NH_3 \f$ through the canopy.
     */
    have_canopy_transport = get_option< bool >( "canopytransport", false);
    if ( have_canopy_transport)
    {
        KLOGINFO_TO( SoilChemistryMeTrXLogger, "consider canopytransport: ", cbm::n2s( have_canopy_transport));
    }
    
    /*!
     * @page metrx
     *  - River connection (default: "riverconnection" = \b no / yes)
     *    Considers adjacent river connection: Dissolved constituents of surface water are in equlibrium with the atmoshpere.
     */
    have_river_connection = get_option< bool >( "riverconnection", false);
    if ( have_river_connection)
    {
        KLOGINFO_TO( SoilChemistryMeTrXLogger, "consider riverconnection: ", cbm::n2s( have_river_connection));
    }
    
    /*!
     * @page metrx
     *  - Change litter height (default: "nochangelitterheight" = \b no / yes )
     *    Spatial discretization of litter height changes depending on litter input from vegetation.
     */
    have_no_litter_height_change = get_option< bool >( "nochangelitterheight", false);
    if ( have_no_litter_height_change)
    {
        KLOGINFO_TO( SoilChemistryMeTrXLogger, "have litter height change: ", cbm::n2s( have_no_litter_height_change));
    }
    
    /*!
     * @page metrx
     *  - Effective diffusion coefficient (default: "effectivediffusion" = \b parameter / millington_and_quirk_1961 )
     *    Diffusion coefficients in the air phase are reduced due to soil tortuosity
     */
    d_eff_method = get_option< char const * >( "effectivediffusion", "parameter");
    if ( ! (d_eff_method == "parameter"))
    {
        KLOGINFO_TO( SoilChemistryMeTrXLogger, "effective diffusion coefficient: ", d_eff_method);
    }
    
    /*!
     * @page metrx
     *  - Spin up years (default: "spinupyears" = \b 2 / any integer number)
     *    During spin up years humus pools are scaled in order to balance decomposition with humification.
     */
    spinup_.set_spinup_years( get_option< int >( "spinupyears", 2));
    KLOGINFO_TO( SoilChemistryMeTrXLogger, "consider: ", cbm::n2s( spinup_.spinup_years()), " spinup years");

    /*!
     * @page metrx
     *  - Spin up carbon lost/build-up rate (default: "spinupdeltac" = \b 0 / any floating point number)
     *    During spin up years humus pools are assumed to have an annual lost/build-up rate.
     */
    spinup_delta_c = get_option< double >( "spinupdeltac", 0.0) * cbm::HA_IN_M2;
    if ( cbm::flt_greater( spinup_delta_c, 0.0))
    {
        KLOGINFO_TO( SoilChemistryMeTrXLogger, "consider: ", cbm::n2s( spinup_delta_c * cbm::M2_IN_HA), " [kg C ha-1] of annual build-up of soil organic carbon");
    }
    else if ( cbm::flt_less( spinup_delta_c, 0.0))
    {
        KLOGINFO_TO( SoilChemistryMeTrXLogger, "consider: ", cbm::n2s( -spinup_delta_c * cbm::M2_IN_HA), " [kg C ha-1] of annual loss of soil organic carbon");
    }

    /*!
     * @page metrx
     */
    anvf_method = get_option< char const * >( "anvf", "default");
    if ( ! (anvf_method == "default"))
    {
        KLOGINFO_TO( SoilChemistryMeTrXLogger, "Anaerobic volume calculation: ", anvf_method);
    }

    if ( _cf && _cf->have_output())
    {
        /*!
         * @page metrx
         * MeTrx includes several model-specifc output options:
         *  - metrxdaily
         *  - metrxyearly
         *  - metrxlayerdaily
         *  - metrxlayeryearly
         *  - metrxsubdaily
         *  - metrxfluxes
         *
         *  In order to include a MeTrx specific output, add the according attribute to the sinks section in the project file.
         *  Example:
         *
         * \code{.xml}
         * <sinks sinkprefix="output/metrx_" >
         *     <metrxdaily  sink="metrx-daily.txt" format="txt" />
         * </sinks>
         * \endcode
         */
        output_writer_.set_option( "output", get_option( "output"));
        output_writer_.set_option( "outputdaily", get_option( "outputdaily"));
        output_writer_.set_option( "outputyearly", get_option( "outputyearly"));
        output_writer_.set_option( "outputlayerdaily", get_option( "outputlayerdaily"));
        output_writer_.set_option( "outputlayeryearly", get_option( "outputlayeryearly"));
        output_writer_.set_option( "outputsubdaily", get_option( "outputsubdaily"));
        output_writer_.set_option( "outputfluxes", get_option( "outputfluxes"));

        lerr_t  rc_output_writer_configure = output_writer_.configure( _cf);
        RETURN_IF_NOT_OK(rc_output_writer_configure);

        size_t  record_sz = output_writer_.max_record_size();
        if ( record_sz > 0u && this->output_writer_.active())
        {
            output_data_ = LD_Allocator->allocate_type< ldndc_flt64_t >( output_writer_.max_record_size());

            if ( !output_data_)
            {
                return  LDNDC_ERR_OBJECT_CONFIG_FAILED;
            }

            lerr_t  rc_owini = output_writer_.initialize();
            if ( rc_owini && output_data_)
            {
                LD_Allocator->deallocate( output_data_);
                output_data_ = NULL;
            }
            RETURN_IF_NOT_OK(rc_owini);
        }
        else
        {
            output_data_ = NULL;
        }
    }
    else
    {
        output_data_ = NULL;
    }

    return  LDNDC_ERR_OK;
}



/*!
 * @page metrx
 * @section metrx_initialisation Model initialisation
 */
lerr_t
SoilChemistryMeTrX::initialize()
{
    if ( cbm::flt_less( METRX_F_DENIT_N2_DELTA, 0.0))
    {
        LOGINFO("Condition not fullfilled: METRX_F_DENIT_N2_MAX > METRX_F_DENIT_N2_MIN");
        return LDNDC_ERR_FAIL;
    }

    site::input_class_site_t const *s_site( io_kcomm->get_input_class< site::input_class_site_t >());

    cbm::source_descriptor_t  gw_source_info;
    lid_t  gw_id = io_kcomm->get_input_class_source_info< input_class_groundwater_t >( &gw_source_info);
    if ( gw_id != invalid_lid)
    {
        gw_ = io_kcomm->acquire_input< input_class_groundwater_t >( &gw_source_info);
    }
    else
    {
        gw_ = NULL;
    }

    //fraction of litter that is considered as raw litter
    double const frac_raw_litter( 0.1);

    size_t sl_st0( 0); //first soil layer of stratum
    for ( size_t  st = 0;  st < sl_.strata_cnt();  ++st)
    {
        site::iclass_site_stratum_t  stratum;
        lerr_t  rc_stratum = sl_.stratum( st, &stratum);
        RETURN_IF_NOT_OK(rc_stratum);

        size_t  sl_st1 = sl_st0 + stratum.split;
        for ( size_t  sl = sl_st0;  sl < sl_st1;  sl++)
        {
            if ( cbm::flt_greater_equal( sc_.phi_sl[sl], PH_MAX))
            {
                LOGWARN("pH value above maximum allowed value. ph give: ",sc_.phi_sl[sl],
                        " maximum allowed value: ", PH_MAX,". ph value changed to: ",0.9 * PH_MAX);
                sc_.phi_sl[sl] = 0.9 * PH_MAX;
            }

            /* calculation of organic and mineral soil fractions */
            double const frac_org( sc_.fcorg_sl[sl] / cbm::CCORG);                       // layer fraction of organic carbon related to bulk_mass.    [-]
            double const mass_soil( sc_.dens_sl[sl] * cbm::DM3_IN_M3 * sc_.h_sl[sl]);    // mass of total soil (bulk + stones) [kg m-2 layer-1]
            double const layer_soc_tot( sc_.fcorg_sl[sl] * mass_soil);                   // total soil organic carbon per layer.    [kg C m-2 layer-1]

            sc_.min_sl[sl] = ((1.0 - frac_org) * mass_soil);                             // soil layer mass of mineral material.    [kg]
            sc_.som_sl[sl] = (frac_org * mass_soil);                                     // soil layer mass of organic material.    [kg]

            /*!
             * @page metrx
             * @subsection metrx_initialisation_cn_ratio C/N ratio
             *  The allocation of soil organic matter to various humus pools is primarily influenced
             *  by the C/N ratio within each pool, while adhering to the overall constraint
             *  of maintaining the soil's overarching C/N ratio.
             *
             *  The target C/N ratio of all mineral associated organic matter pools
             *  depend on the overall soil C/N ratio.
             *  Humus pool 1 representes non-protected organic matter.
             *  Humus pool 2 and 3 represent "old" and "very old" protected soil organic matter, respectively:
             *
             *  \f{eqnarray*}{
             *  C/N_{hum,1} &=& C/N_{soil} \\
             *  C/N_{hum,2} &=& 1.5 \cdot C/N_{soil} \\
             *  C/N_{hum,3} &=& METRX\_CN\_FRAC\_HUM3 \cdot C/N_{soil} \\
             *  \f}
             */
            cn_hum_1_sl[sl] = sc_.cnr_sl[sl];
            cn_hum_2_sl[sl] = 1.5 * sc_.cnr_sl[sl];
            if ( sipar_.METRX_CN_FRAC_HUM3() > 0.1)
            {
                cn_hum_3_sl[sl] = sipar_.METRX_CN_FRAC_HUM3() * sc_.cnr_sl[sl];
            }
            else
            {
                //increasing CN ratio of soil reduces recalcitrant old humus pool
                double const cn_hum_3_scale( cbm::bound( 0.0, 0.8 - 8.0 / sc_.cnr_sl[sl], 0.8));
                cn_hum_3_sl[sl] = (0.9 - cn_hum_3_scale) * sc_.cnr_sl[sl];
            }

            /* CN-ratios of litter pools */
            cn_opt_mic_sl[sl] = 0.5 * (sipar_.METRX_CN_MIC_MIN() + sipar_.METRX_CN_MIC_MAX());
            double cn_lit_1( 3.0 * std::min(sc_.cnr_sl[sl], 25.0));
            double cn_lit_2( 4.0 * std::min(sc_.cnr_sl[sl], 25.0));
            double cn_lit_3( 6.0 * std::min(sc_.cnr_sl[sl], 25.0));

            /* ice, water and oxygen */
            v_water_sl[sl] = wc_.wc_sl[sl] * sc_.h_sl[sl];
            v_ice_sl[sl] = wc_.ice_sl[sl] * sc_.h_sl[sl];
            v_air_sl[sl] = (sc_.h_sl[sl] * sc_.poro_sl[sl]) - v_water_sl[sl] - v_ice_sl[sl];

            o2_gas_sl[sl] = (sc_.o2_sl[sl] * v_air_sl[sl] / (RGAS_M_O2_BAR_PA * (mc_temp_sl[sl] + cbm::D_IN_K)));

            double const p_o2( o2_gas_sl[sl] * RGAS_M_O2_BAR_PA * (mc_temp_sl[sl] + cbm::D_IN_K) / v_air_sl[sl]);
            sc_.anvf_sl[sl] = cbm::bound( 0.001, exp(-cbm::sqr(7.0 * p_o2)), 0.999);

            /* reduction of total iron content by assumed fraction of active iron */
            double const fe_tot( (sc_.fe2_sl[sl] + sc_.fe3_sl[sl]) * sipar_.METRX_FE_REDUCTION());
            sc_.fe2_sl[sl] = fe_tot * sc_.anvf_sl[sl];
            sc_.fe3_sl[sl] = fe_tot - sc_.fe2_sl[sl];

            /* tilling factor */
            sc_.till_effect_sl[sl] = 1.0;

            /*!
             * @page metrx
             * @subsection metrx_initialisation_pool_distribution Pool distribution
             */

            double frac_c_doc( 0.0);
            if ( cbm::is_invalid( sc_.doc_sl[sl] + sc_.an_doc_sl[sl]))
            {
                if ( (s_site->soil_use_history() == ECOSYS_WETLAND))
                {
                    frac_c_doc = 1.0e-5 * sc_.fcorg_sl[sl];
                }
                else
                {
                    frac_c_doc = 1.0e-4 * sc_.fcorg_sl[sl];
                }
            }
            else
            {
                frac_c_doc = (sc_.doc_sl[sl] + sc_.an_doc_sl[sl]) / layer_soc_tot;
            }

            double frac_litter( stratum.pom_init);
            if ( cbm::is_invalid( frac_litter))
            {
                if ( (s_site->soil_use_history() == ECOSYS_WETLAND))
                {
                    frac_litter = 0.002 * (1.0 - 1.0 / std::exp( sc_.fcorg_sl[sl]));
                }
                else
                {
                    frac_litter = 0.02 * (1.0 - 1.0 / std::exp( 5.0 * sc_.fcorg_sl[sl]));
                }
            }

            double frac_c_sol( frac_litter * 0.2);
            double frac_c_cel( frac_litter * 0.7);
            double frac_c_lig( frac_litter * 0.1);

            double frac_aorg( stratum.aorg_init);
            if ( cbm::is_invalid( frac_aorg))
            {
                if ( (s_site->soil_use_history() == ECOSYS_WETLAND))
                {
                    frac_aorg = 0.01 * (1.0 - 1.0 / std::exp( 2.0 * sc_.fcorg_sl[sl]));
                }
                else
                {
                    frac_aorg = 0.1 * (1.0 - 1.0 / std::exp( 10.0 * sc_.fcorg_sl[sl]));
                }
            }

            /*!
             * @page metrx
             * Active organic material is assigned to microbial necromass (15%) and
             * mineral associated but non-protected organic matter (humus pool 1: 85%).
             */
            double frac_c_aorg( 0.15 * frac_aorg);
            double frac_c_hum_1( 0.85 * frac_aorg);

            double frac_c_mic_1( 1.0 / 3.0 * 1.0e2 * cbm::HA_IN_M2 * sc_.som_sl[sl] / sc_.som_sl.sum());
            double frac_c_mic_2( 1.0 / 3.0 * 1.0e2 * cbm::HA_IN_M2 * sc_.som_sl[sl] / sc_.som_sl.sum());
            double frac_c_mic_3( 1.0 / 3.0 * 1.0e2 * cbm::HA_IN_M2 * sc_.som_sl[sl] / sc_.som_sl.sum());
            
            double frac_c_hum( 1.0 - frac_c_sol - frac_c_cel - frac_c_lig
                               - frac_c_aorg - frac_c_mic_1 - frac_c_mic_2 - frac_c_mic_3
                               - frac_c_doc - frac_c_hum_1);
            double frac_c_hum_2( 0.5 * frac_c_hum);
            double frac_c_hum_3( 0.5 * frac_c_hum);

            /************************************************************/
            /* Initialize fixed amounts for gaseous and mineral N-pools */
            /************************************************************/

            n2o_gas_sl[sl] = 0.0;
            an_n2o_gas_sl[sl] = 0.0;

            no_gas_sl[sl] = 0.0;
            an_no_gas_sl[sl] = 0.0;

            n2_gas_sl[sl] = (cbm::PN2 * cbm::BAR_IN_PA * v_air_sl[sl]) /
                            (cbm::RGAS * (mc_temp_sl[sl] + cbm::D_IN_K)) * M_2N_SCALE;

            /***************************************************************/
            /* Iteratively Solution of other C-, and N-pool initialization */
            /***************************************************************/

            unsigned int max_it( 0);
            for (;;)
            {
                double const increment( 1.0e-4);
                double const cnProb( 1.0 /   (frac_c_hum_1 / cn_hum_1_sl[sl]
                                            + frac_c_hum_2 / cn_hum_2_sl[sl]
                                            + frac_c_hum_3 / cn_hum_3_sl[sl]
                                            + frac_c_sol / cn_lit_1
                                            + frac_c_cel / cn_lit_2
                                            + frac_c_lig / cn_lit_3
                                            + frac_c_aorg / cn_opt_mic_sl[sl]
                                            + frac_c_mic_1 / cn_opt_mic_sl[sl]
                                            + frac_c_mic_2 / cn_opt_mic_sl[sl]
                                            + frac_c_mic_3 / cn_opt_mic_sl[sl]));
                
                double const residuum( std::abs(cnProb - sc_.cnr_sl[sl]));
                if (residuum < 0.1)
                {
                    break;
                }
                else if (cnProb < sc_.cnr_sl[sl])
                {
                    if (frac_c_hum_3 > 1.0e-3)
                    {
                        frac_c_hum_2 += increment;
                        frac_c_hum_3 -= increment;
                    }
                    else
                    {
                        if (frac_c_hum_2 > 1.0e-3)
                        {
                            frac_c_lig += increment;
                            frac_c_cel += increment;
                            frac_c_hum_2 -= (increment+increment);
                        }
                        else
                        {
                            if (frac_c_hum_1 > 1.0e-3 || frac_c_aorg > 1.0e-3)
                            {
                                if (frac_c_hum_1 > 1.0e-3)
                                {
                                    frac_c_lig += increment;
                                    frac_c_cel += increment;
                                    frac_c_hum_1 -= (increment+increment);
                                }
                                if (frac_c_aorg > 1.0e-3)
                                {
                                    frac_c_lig += increment;
                                    frac_c_cel += increment;
                                    frac_c_hum_1 -= (increment+increment);
                                }
                            }
                            else
                            {
                                KLOGERROR("could not initialize soil pools in layer ", sl ," in metrx: too small humus pools initialized");
                                KLOGERROR("cnProb: ", cnProb, "  cn site: ",  sc_.cnr_sl[sl]);
                                return LDNDC_ERR_RUNTIME_ERROR;
                            }
                        }
                    }
                }
                else if (cnProb > sc_.cnr_sl[sl])
                {
                    if (frac_c_hum_2 > 1.0e-3)
                    {
                        frac_c_hum_2 -= increment;
                        frac_c_hum_3 += increment;
                    }
                    else
                    {
                        double incr_c_hum_3( 0.0);
                        if ( frac_c_hum_1 > 1.0e-3)
                        {
                            frac_c_hum_1 -= increment;
                            incr_c_hum_3 += increment;
                        }
                        if ( frac_c_lig > 1.0e-3)
                        {
                            frac_c_lig -= increment;
                            incr_c_hum_3 += increment;
                        }
                        if ( frac_c_cel > 1.0e-3)
                        {
                            frac_c_cel -= increment;
                            incr_c_hum_3 += increment;
                        }
                        if ( frac_c_sol > 1.0e-3)
                        {
                            frac_c_sol -= increment;
                            incr_c_hum_3 += increment;
                        }
                        frac_c_hum_3 += incr_c_hum_3;
                    }
                }
                
                max_it += 1;
                if (max_it > 1e6)
                {
                    KLOGERROR("could not initialize soil pools in metrx: max_it reached in layer: ", sl);
                    KLOGERROR("cnProb: ", cnProb, "  cn site: ",  sc_.cnr_sl[sl]);
                    return LDNDC_ERR_RUNTIME_ERROR;
                }
            }

            /****************************************/
            /* Updating of C-, and N-pool-variables */
            /****************************************/

            sc_.C_lit1_sl[sl] = (1.0 - frac_raw_litter) * frac_c_sol * layer_soc_tot;
            sc_.C_lit2_sl[sl] = (1.0 - frac_raw_litter) * frac_c_cel * layer_soc_tot;
            sc_.C_lit3_sl[sl] = (1.0 - frac_raw_litter) * frac_c_lig * layer_soc_tot;

            sc_.N_lit1_sl[sl] = (1.0 - frac_raw_litter) * sc_.C_lit1_sl[sl]/cn_lit_1;
            sc_.N_lit2_sl[sl] = (1.0 - frac_raw_litter) * sc_.C_lit2_sl[sl]/cn_lit_2;
            sc_.N_lit3_sl[sl] = (1.0 - frac_raw_litter) * sc_.C_lit3_sl[sl]/cn_lit_3;

            sc_.C_aorg_sl[sl] = frac_c_aorg * layer_soc_tot;
            sc_.N_aorg_sl[sl] = sc_.C_aorg_sl[sl] / cn_opt_mic_sl[sl];

            sc_.C_micro1_sl[sl] = frac_c_mic_1 * layer_soc_tot;
            sc_.C_micro2_sl[sl] = frac_c_mic_2 * layer_soc_tot;
            sc_.C_micro3_sl[sl] = frac_c_mic_3 * layer_soc_tot;

            n_micro_1_sl[sl] = sc_.C_micro1_sl[sl] / cn_opt_mic_sl[sl];
            n_micro_2_sl[sl] = sc_.C_micro2_sl[sl] / cn_opt_mic_sl[sl];
            n_micro_3_sl[sl] = sc_.C_micro3_sl[sl] / cn_opt_mic_sl[sl];
            sc_.N_micro_sl[sl] = n_micro_1_sl[sl] + n_micro_2_sl[sl] + n_micro_3_sl[sl];

            c_humus_1_sl[sl] = frac_c_hum_1 * layer_soc_tot;
            c_humus_2_sl[sl] = frac_c_hum_2 * layer_soc_tot;
            c_humus_3_sl[sl] = frac_c_hum_3 * layer_soc_tot;
            sc_.C_hum_sl[sl] = c_humus_1_sl[sl] + c_humus_2_sl[sl] + c_humus_3_sl[sl];

            n_humus_1_sl[sl] = c_humus_1_sl[sl] / cn_hum_1_sl[sl];
            n_humus_2_sl[sl] = c_humus_2_sl[sl] / cn_hum_2_sl[sl];
            n_humus_3_sl[sl] = c_humus_3_sl[sl] / cn_hum_3_sl[sl];
            sc_.N_hum_sl[sl] = n_humus_1_sl[sl] + n_humus_2_sl[sl] + n_humus_3_sl[sl];
        }
        sl_st0 = sl_st1;
    }

    double const scale( frac_raw_litter / (1.0 -frac_raw_litter));
    sc_.c_raw_lit_1_sl[0] = scale * sc_.C_lit1_sl.sum();
    sc_.c_raw_lit_2_sl[0] = scale * sc_.C_lit2_sl.sum();
    sc_.c_raw_lit_3_sl[0] = scale * sc_.C_lit3_sl.sum();

    sc_.n_raw_lit_1_sl[0] = scale * sc_.N_lit1_sl.sum();
    sc_.n_raw_lit_2_sl[0] = scale * sc_.N_lit2_sl.sum();
    sc_.n_raw_lit_3_sl[0] = scale * sc_.N_lit3_sl.sum();

    return  LDNDC_ERR_OK;
}



/*!
 * @brief
 *
 */
lerr_t
SoilChemistryMeTrX::finalize()
{
    if ( output_data_)
    {
        lerr_t  rc_owfin = output_writer_.finalize();
        RETURN_IF_NOT_OK(rc_owfin);
    }

    return  LDNDC_ERR_OK;
}


static int  _QueueEventTill( void const *  _msg, size_t  _msg_sz, void *  _queue)
{
    ldndc::EventQueue *  queue =
        static_cast< ldndc::EventQueue * >( _queue);
    if ( !queue)
        { return -1; }

    ldndc::EventAttributes  till_event(
                "till", (char const *)_msg, _msg_sz);
    queue->push( till_event);
    return 0;
}



lerr_t
SoilChemistryMeTrX::register_ports( cbm::io_kcomm_t *  _io_kcomm)
{
    lerr_t  rc = LDNDC_ERR_OK;

    StubbleCarbon.publish( "StubbleCarbon", "kg/m2", _io_kcomm);

    int rc_fert = m_FertilizeEvents.subscribe( "fertilize", _io_kcomm);
    if ( rc_fert != LD_PortOk)
    { return  LDNDC_ERR_FAIL; }

    int rc_manure = m_ManureEvents.subscribe( "manure", _io_kcomm);
    if ( rc_manure != LD_PortOk)
    { return  LDNDC_ERR_FAIL; }

    int rc_plant = m_PlantEvents.subscribe( "plant", _io_kcomm);
    if ( rc_plant != LD_PortOk)
    { return  LDNDC_ERR_FAIL; }

    CBM_Callback  cb_till;
    cb_till.fn = &_QueueEventTill;
    cb_till.data = &this->m_TillEvents;
    this->m_TillHandle = _io_kcomm->subscribe_event( "till", cb_till);
    if ( !CBM_HandleOk(this->m_TillHandle))
    { return  LDNDC_ERR_FAIL; }

    return rc;
}



lerr_t
SoilChemistryMeTrX::unregister_ports( cbm::io_kcomm_t *  _io_kcomm)
{
    StubbleCarbon.unpublish();

    m_FertilizeEvents.unsubscribe();
    m_ManureEvents.unsubscribe();
    m_PlantEvents.unsubscribe();

    _io_kcomm->unsubscribe_event( this->m_TillHandle);

    return LDNDC_ERR_OK;
}

} /*namespace ldndc*/
