/*!
 * @file
 * @author
 *  David Kraus (created on: may 12, 2014)
 *
 */

#include  "soilchemistry/metrx/soilchemistry-metrx.h"

namespace ldndc {

/*!
 * @page metrx
 * @section metrx_iron_reduction Iron reduction
 *  The stoecheometry of iron reduction via aceate is given by::
 *  \f[
 *  CHCOO^- + 8 Fe^{3+} + 4 H_2O \rightarrow 2 HCO_3^- + 8 Fe^{2+} + 9 H^+
 *  \f]
 *
 *  The stoecheometry of iron reduction via aceate is given by::
 *  \f[
 *  H_2 + 2 Fe^{3+} + 4 H^+ \rightarrow 2 Fe^{2+} + 6 H_2O
 *  \f]
 */
void SoilChemistryMeTrX::MeTrX_iron_reduction()
{
    for( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
    {
        if ( cbm::flt_greater_zero( sc_.fe3_sl[sl]))
        {
            /******************/
            /* Factor setting */
            /******************/

            /*!
             * @page metrx
             * Iron reduction via acetate and hydrogen depends on the amount of Fe3+ and
             * the amount of either aceatet or hydrogen each modeled using a Michaelis Menten kinetic.
             * Further influencing factors are temperature, NO3 and O2:
             *  \f[
             *  \frac{dFe^{3+}}{dt} = - METRX\_MUEMAX\_C\_FE\_RED \cdot \phi_{T} \cdot \phi_{NO3} \cdot \phi_{O2} \cdot \phi_{Fe3+} \cdot \phi_{AC/H2}
             *  \f]
             */
            double const fact_t( std::exp( -sipar_.METRX_F_DECOMP_T_EXP_1() *
                                           pow( 1.0 - mc_temp_sl[sl] / sipar_.METRX_F_DECOMP_T_EXP_2(),
                                                2.0)));
            
            /*!
             * @page metrx
             * The dependy on NO3 is given by:
             *
             *  \f[
             *  \phi_{NO3} = 1 - \frac{NO3}{NO3 + NO3\_MOLAR\_MAX\_FE\_RED}
             *  \f]
             */
            double const fact_no3( 1.0 - no3_an_sl[sl] / (no3_an_sl[sl] + (NO3_MOLAR_MAX_FE_RED * sc_.h_sl[sl] * M_N_SCALE)));
            double const fact_fe3_fe2( sc_.fe3_sl[sl] / (MeTrX_get_k_mm_x_sl( sl, sc_.anvf_sl[sl] * sipar_.METRX_K_FE_FE_RED())));
            double const fact_all_fe2_scale( fact_t * fact_no3 * fact_fe3_fe2);

            /******************/
            /* Iron reduction */
            /******************/


            double const fact_ac( an_acetate_sl[sl] / (MeTrX_get_k_mm_x_sl( sl, sc_.anvf_sl[sl] * sipar_.METRX_KMM_AC_FE_RED()) + an_acetate_sl[sl]));
            double acetate_co2( std::min( METRX_MUEMAX_C_FE_RED * fact_ac * fact_all_fe2_scale * sc_.h_sl[sl], 0.9 * an_acetate_sl[sl]));
            double const iron_reduction_acetate( acetate_co2 * FE3_ACETATE_REDUCTION_RATIO);

            double const fact_doc( sc_.an_doc_sl[sl] / (MeTrX_get_k_mm_x_sl( sl, sc_.anvf_sl[sl] * sipar_.METRX_KMM_AC_FE_RED()) + sc_.an_doc_sl[sl]));
            double doc_co2( std::min( METRX_MUEMAX_C_FE_RED * fact_doc * fact_all_fe2_scale * sc_.h_sl[sl], 0.9 * sc_.an_doc_sl[sl]));
            double const iron_reduction_doc( doc_co2 * FE3_ACETATE_REDUCTION_RATIO);


            double const fact_h2( h2_sl[sl] / (MeTrX_get_k_mm_x_sl( sl, sc_.anvf_sl[sl] * sipar_.METRX_KMM_H2_FE_RED()) + h2_sl[sl]));
            double h2_h2o( std::min( METRX_MUEMAX_C_FE_RED * fact_h2 * fact_all_fe2_scale * M_2H_SCALE / M_2C_SCALE * sc_.h_sl[sl], 0.9 * h2_sl[sl]));
            double const iron_reduction_h2( h2_h2o * FE3_HYDROGEN_REDUCTION_RATIO);

            double iron_reduction_tot( iron_reduction_acetate + iron_reduction_doc + iron_reduction_h2);
            double const iron_reduction_max( 0.9 * sc_.fe3_sl[sl]);
            if ( iron_reduction_tot > iron_reduction_max)
            {
                double const correction( iron_reduction_max / iron_reduction_tot);
                iron_reduction_tot *= correction;
                acetate_co2 *= correction;
                doc_co2 *= correction;
                h2_h2o *= correction;
            }


            /**********/
            /* Update */
            /**********/

            sc_.fe3_sl[sl] -= iron_reduction_tot;
            sc_.fe2_sl[sl] += iron_reduction_tot;

            an_acetate_sl[sl] -= acetate_co2;
            sc_.an_doc_sl[sl] -= doc_co2;
            co2_liq_sl[sl] += acetate_co2 + doc_co2;
            h2_sl[sl] -= h2_h2o;

            co2_hetero_sl[sl] += acetate_co2 + doc_co2;
            day_co2_prod_mic_3_acetate_cons += acetate_co2 + doc_co2;
            day_acetate_cons_fe3_sl[sl] += acetate_co2;
            day_h2_c_eq_cons_fe3_sl[sl] += (h2_h2o * H2_KG_CH4_KG_CONVERSION);
        }
    }
}



/*!
 * @page metrx
 * @section metrx_iron_oxidation Iron oxidation
 *  The stoecheometry of iron oxidation is given by::
 *  \f[
 *  4 Fe^{2+} + O_2 + 10 H_2O \rightarrow 4 Fe(OH)_3 + 8 H^+
 *  \f]
 */
void
SoilChemistryMeTrX::MeTrX_iron_oxidation()
{
    for( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
    {
        if ( cbm::flt_greater_zero( sc_.fe2_sl[sl]))
        {
            double const o2_tot( o2_gas_sl[sl] + o2_liq_sl[sl]);

            // Fe(2+) rapidly oxidizes nonbiologically to Fe(3+) under neutral pH and oxic conditions.
            // The anaerobic volume fraction determines which part of Fe(2+) might get into contact with O2.
            // For low pH values Fe(2+) is nonbiologically stable under oxic conditions.
            // However there are acidophile bacteria, which then biologically oxidize Fe(2+) (pH dependencies not implemented)
            double const aevf( 1.0 - sc_.anvf_sl[sl]);
            if ( cbm::flt_greater( aevf * (sc_.fe2_sl[sl] + sc_.fe3_sl[sl]), sc_.fe3_sl[sl]) &&
                 cbm::flt_greater_zero( o2_tot))
            {
                /*!
                 * @page metrx
                 *  \f[
                 *  \frac{dFe^{2+}}{dt} = - METRX\_KR\_OX\_FE \cdot Fe^{2+} \cdot \phi_{O2}
                 *  \f]
                 *
                 * The dependy on O2 is given by:
                 *
                 *  \f[
                 *  \phi_{O2} = \frac{O_2}{O_2 + METRX\_KMM\_O2\_FE\_OX}
                 *  \f]
                 */
                double const fe2_ox_max( std::min( sc_.fe2_sl[sl], o2_tot / O2_FE2_RATIO));
                double const fact_o2( o2_tot / (o2_tot + MeTrX_get_k_mm_x_sl( sl, sipar_.METRX_KMM_O2_FE_OX())));
                double const fe2_ox( cbm::bound_max( METRX_KR_OX_FE  * aevf * sc_.fe2_sl[sl] * fact_o2, fe2_ox_max));

                sc_.fe2_sl[sl] = cbm::bound_min( 0.0, sc_.fe2_sl[sl] - fe2_ox);
                sc_.fe3_sl[sl] += fe2_ox;

                o2_gas_sl[sl] = cbm::bound_min( 0.0, o2_gas_sl[sl] - fe2_ox * O2_FE2_RATIO * o2_gas_sl[sl] / o2_tot);
                o2_liq_sl[sl] = cbm::bound_min( 0.0, o2_liq_sl[sl] - fe2_ox * O2_FE2_RATIO * o2_liq_sl[sl] / o2_tot);

                // Output
                day_fe2_oxidation_sl[sl] += fe2_ox;
            }
        }
    }
}

} /*namespace ldndc*/

