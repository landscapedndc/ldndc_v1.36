/*!
 * @file
 * @author
 *  David Kraus
 */

#include  "soilchemistry/metrx/output-soilchemistry-metrx-layer-daily.h"

LMOD_MODULE_INFO(OutputSoilchemistryMeTrXLayerDaily,TMODE_POST_DAILY,LMOD_FLAG_USER|LMOD_FLAG_OUTPUT);
namespace ldndc {


/*!
 * @page metrx
 * @subsection metrx_output_layer_daily MeTrx layer output (daily)
 * entity name | decription | unit
 * ----------- | ---------- | ----
 * anvf | Anaerobic volume fraction averaged over the complete soil profile | [-]
 * level | ... | [...]
 * extension | ... | [...]
 * pore_connectivity | ... | [-]
 * permeability | ... | [...]
 * aerenchym_permeability | ... | [...]
 * water_content | ... | [...]
 * air_content | ... | [...]
 * porosity | ... | [...]
 * fe2 | ... | [...]
 * fe3 | ... | [...]
 * C_co2 | ... | [...]
 * C_ch4 | ... | [...]
 * dC_ch4_bubbling | ... | [...]
 * dC_ch4_production | ... | [...]
 * dC_ch4_oxidation | ... | [...]
 * dC_doc_production | ... | [...]
 * dC_acetate_production | ... | [...]
 * dC_microbial_death | ... | [...]
 * C_doc | ... | [...]
 * C_acetate | ... | [...]
 * N_no3 | ... | [...]
 * N_nh4 | ... | [...]
 * N_urea | ... | [...]
 * N_nh3 | ... | [...]
 * o2 | ... | [...]
 * N_no | ... | [...]
 * N_n2o | ... | [...]
 * C_humus_1 | ... | [...]
 * C_humus_2 | ... | [...]
 * C_humus_3 | ... | [...]
 * dN_no3_cons_denit | ... | [...]
 * denit_factor_c | ... | [-]
 * denit_factor_n | ... | [-]
 * dN_no3_prod_nit | ... | [kgNha-1]
 * dN_no3_transport | Downward transport of NO3 | [kgNha-1]
 */
ldndc_string_t const  OutputSoilchemistryMeTrXLayerDaily_Ids[] =
{
    "level",
    "extension",
    "pore_connectivity[-]",
    "permeability[-]",
    "aerenchym_permeability[-]",
    "water_content[%]",
    "air_content[%]",
    "porosity[%]",
    "fe2",
    "fe3",
    "C_co2",
    "C_ch4",
    "dC_ch4_bubbling",
    "dC_ch4_production",
    "dC_ch4_oxidation",
    "dC_doc_production",
    "dC_acetate_production",
    "dC_microbial_death",
    "C_doc",
    "C_acetate",
    "N_no3",
    "N_nh4",
    "N_urea",
    "N_nh3",
    "o2",
    "N_no",
    "N_n2o",
    "C_humus_1",
    "C_humus_2",
    "C_humus_3",
    "dN_no3_cons_denit",
    "denit_factor_c",
    "denit_factor_n",
    "dN_no3_prod_nit",
    "dN_no3_transport[kgNm-2]"
};

ldndc_string_t const *  OutputSoilchemistryMeTrXLayerDaily_Header =
    OutputSoilchemistryMeTrXLayerDaily_Ids;


#define  OutputSoilchemistryMeTrXLayerDaily_Datasize  (sizeof( OutputSoilchemistryMeTrXLayerDaily_Ids) / sizeof( OutputSoilchemistryMeTrXLayerDaily_Ids[0]))
ldndc_output_size_t const  OutputSoilchemistryMeTrXLayerDaily_Sizes[] =
{
    OutputSoilchemistryMeTrXLayerDaily_Datasize,

    OutputSoilchemistryMeTrXLayerDaily_Datasize /*total size*/
};



ldndc_output_size_t const *  OutputSoilchemistryMeTrXLayerDaily_EntitySizes = NULL;

#define  OutputSoilchemistryMeTrXLayerDaily_Rank  ((ldndc_output_rank_t)(sizeof( OutputSoilchemistryMeTrXLayerDaily_Sizes) / sizeof( OutputSoilchemistryMeTrXLayerDaily_Sizes[0])) - 1)
atomic_datatype_t const  OutputSoilchemistryMeTrXLayerDaily_Types[] =
{
    LDNDC_FLOAT64
};



OutputSoilchemistryMeTrXLayerDaily::OutputSoilchemistryMeTrXLayerDaily(
                                       MoBiLE_State *  _state,
                                       cbm::io_kcomm_t *  _io_kcomm,
                                       timemode_e  _timemode)
                                        : MBE_LegacyOutputModel( _state, _io_kcomm, _timemode),
                                        m_iokcomm( _io_kcomm),
                                        sl_( _io_kcomm->get_input_class_ref< input_class_soillayers_t >()),
                                        accumulated_n_no3_no2_denitrification_sl( sl_.soil_layer_cnt(), 0.0),
                                        accumulated_n_no3_leaching_sl( sl_.soil_layer_cnt(), 0.0)
{ }

OutputSoilchemistryMeTrXLayerDaily::~OutputSoilchemistryMeTrXLayerDaily()
{ }


size_t
OutputSoilchemistryMeTrXLayerDaily::record_size()
const
{
    return  OutputSoilchemistryMeTrXLayerDaily_Datasize;
}



lerr_t
OutputSoilchemistryMeTrXLayerDaily::configure(
        ldndc::config_file_t const *  _cf)
{
    if ( _cf && !_cf->have_output())
    {
        return  LDNDC_ERR_OK;
    }

    lerr_t  rc_setflags =
        this->set_metaflags( _cf, RM_DEFAULT_LAYERDAILY);
    if ( rc_setflags)
        { return  LDNDC_ERR_FAIL; }

    m_sink = m_iokcomm->sink_handle_acquire( "metrxlayerdaily");
    if ( m_sink.status() == LDNDC_ERR_OK)
    {
        lerr_t  rc_layout = LDNDC_OUTPUT_DEFINE_SINK_LAYOUT_DEFAULTS(
            this->m_sink,OutputSoilchemistryMeTrXLayerDaily);
        RETURN_IF_NOT_OK(rc_layout);
    }
    else
    {
        this->set_active_off();
    }
    
    return  LDNDC_ERR_OK;
}



lerr_t
OutputSoilchemistryMeTrXLayerDaily::initialize()
{
    return  LDNDC_ERR_OK;
}



lerr_t
OutputSoilchemistryMeTrXLayerDaily::solve()
{
    KLOGDEBUG( "metrx record size=", record_size());
    KLOGFATAL( "do not use solve(), we rock with write_results()");
    return  LDNDC_ERR_FAIL;

}



lerr_t
OutputSoilchemistryMeTrXLayerDaily::write_results(
        unsigned int  _layer, ldndc_flt64_t *  _data)
{
    if ( !_data || !m_sink.is_acquired())
    {
        /* we assume there is nothing to dump ... */
        return  LDNDC_ERR_OK;
    }

    /* output */
    void *  data[] = { _data};
    this->set_layernumber( -static_cast< int >( _layer)-1);
    lerr_t  rc_write =
        this->write_fixed_record( &this->m_sink, data);
    if ( rc_write)
        { return  LDNDC_ERR_FAIL; }
    return  LDNDC_ERR_OK;
}



lerr_t
OutputSoilchemistryMeTrXLayerDaily::finalize()
{
    return  m_iokcomm->sink_handle_release( &m_sink);
}

} /*namespace ldndc*/

#undef  OutputSoilchemistryMeTrXLayerDaily_Rank
#undef  OutputSoilchemistryMeTrXLayerDaily_Datasize

