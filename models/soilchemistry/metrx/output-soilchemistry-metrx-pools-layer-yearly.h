/*!
 * @brief
 *
 * @author
 *    david kraus
 */

#ifndef  LM_OUTPUT_SOILCHEMISTRYMETRX_POOLS_LAYER_DAILY_H_
#define  LM_OUTPUT_SOILCHEMISTRYMETRX_POOLS_LAYER_DAILY_H_

#include  "mbe_legacyoutputmodel.h"

#define  LMOD_OUTPUT_MODULE_ID    "output:soilchemistry-metrx-pools-layer:daily"
#define  LMOD_OUTPUT_MODULE_DESC  "SoilchemistryMeTrX Pools Layer Output"
namespace ldndc {
class  LDNDC_API  OutputSoilchemistryMeTrXPools  :  public  MBE_LegacyOutputModel
{
    LMOD_EXPORT_MODULE_INFO(OutputSoilchemistryMeTrXPools,LMOD_OUTPUT_MODULE_ID,LMOD_OUTPUT_MODULE_DESC);
    public:
        OutputSoilchemistryMeTrXPools(
                MoBiLE_State *,
                cbm::io_kcomm_t *,
                timemode_e  _timemode);

        ~OutputSoilchemistryMeTrXPools();


        lerr_t  configure( ldndc::config_file_t const *);
        lerr_t  initialize();
        lerr_t  solve();
        lerr_t  finalize();

        lerr_t  wake() { return  LDNDC_ERR_OK; }
        lerr_t  sleep() { return  LDNDC_ERR_OK; }


        lerr_t  write_results(
                unsigned int /* layer index */,
                ldndc_flt64_t *);
    
        size_t  record_size() const;

    private:
        cbm::io_kcomm_t *  m_iokcomm;
        ldndc::sink_handle_t  m_sink;

        input_class_soillayers_t const &  sl_;
        substate_surfacebulk_t &  sb_;

};

} /*namespace ldndc*/

#undef  LMOD_OUTPUT_MODULE_ID
#undef  LMOD_OUTPUT_MODULE_DESC

#endif  /*  !LM_OUTPUT_SOILCHEMISTRYMETRX_POOLS_LAYER_DAILY_H_  */

