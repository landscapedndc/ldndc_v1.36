/*!
 * @file
 *
 * @author
 *    david kraus
 */

#ifndef  LM_OUTPUT_SOILCHEMISTRYMETRX_H_
#define  LM_OUTPUT_SOILCHEMISTRYMETRX_H_

#include  "mbe_legacymodel.h"

#include  "soilchemistry/metrx/output-soilchemistry-metrx-daily.h"
#include  "soilchemistry/metrx/output-soilchemistry-metrx-yearly.h"
#include  "soilchemistry/metrx/output-soilchemistry-metrx-layer-daily.h"
#include  "soilchemistry/metrx/output-soilchemistry-metrx-layer-yearly.h"
#include  "soilchemistry/metrx/output-soilchemistry-metrx-fluxes-layer-subdaily.h"
#include  "soilchemistry/metrx/output-soilchemistry-metrx-pools-layer-daily.h"
#include  "soilchemistry/metrx/output-soilchemistry-metrx-subdaily.h"

#define  LMOD_OUTPUT_MODULE_BASE  MBE_LegacyModel
#define  LMOD_OUTPUT_MODULE_NAME  OutputSoilchemistryMeTrX
#define  LMOD_OUTPUT_MODULE_ID    "output:soilchemistry:metrx"
#define  LMOD_OUTPUT_MODULE_DESC  "SoilchemistryMETRX Output"
namespace ldndc {
class  LDNDC_API  LMOD_OUTPUT_MODULE_NAME  :  public  LMOD_OUTPUT_MODULE_BASE
{
    LMOD_EXPORT_MODULE_INFO(LMOD_OUTPUT_MODULE_NAME,LMOD_OUTPUT_MODULE_ID,LMOD_OUTPUT_MODULE_DESC);
    public:
        enum  writer_id_e
        {
            WRITER_DAILY,
            WRITER_YEARLY,
            WRITER_DAILY_LAYER,
            WRITER_YEARLY_LAYER,
            WRITER_FLUXES,
            WRITER_POOLS,
            WRITER_SUBDAILY,
            WRITER_CNT
        };

        LMOD_OUTPUT_MODULE_NAME(
                MoBiLE_State *,
                cbm::io_kcomm_t *,
                timemode_e  _timemode);

        ~LMOD_OUTPUT_MODULE_NAME();


        lerr_t  configure( 
                          ldndc::config_file_t const *);

        lerr_t  initialize();

        lerr_t  solve();

        lerr_t  finalize();

        lerr_t  wake() { return  LDNDC_ERR_OK; }
    
        lerr_t  sleep() { return  LDNDC_ERR_OK; }


        bool  write_daily() const { return  this->output_writer_daily.active(); }
        lerr_t  write_daily( ldndc_flt64_t * /*data*/);

        bool  write_yearly() const { return  this->output_writer_yearly.active(); }
        lerr_t  write_yearly( ldndc_flt64_t * /*data*/);
    
        bool  write_layer_daily() const { return  this->output_writer_layer_daily.active(); }
        lerr_t  write_layer_daily( unsigned int /*layer index*/, ldndc_flt64_t * /*data*/);

        bool  write_layer_yearly() const { return  this->output_writer_layer_yearly.active(); }
        lerr_t  write_layer_yearly( unsigned int /*layer index*/, ldndc_flt64_t * /*data*/);
    
        bool  write_fluxes() const { return  this->output_writer_fluxes.active(); }
        lerr_t  write_fluxes( unsigned int /*layer index*/, ldndc_flt64_t * /*data*/);

        bool  write_pools() const { return  this->output_writer_pools.active(); }
        lerr_t  write_pools( unsigned int /*layer index*/, ldndc_flt64_t * /*data*/);

        bool  write_subdaily() const { return  this->output_writer_subdaily.active(); }
        lerr_t  write_subdaily( ldndc_flt64_t * /*data*/);    


        size_t  record_size( writer_id_e /*which writer*/) const;
        size_t  max_record_size() const;

    private:
        OutputSoilchemistryMeTrXDaily  output_writer_daily;
        OutputSoilchemistryMeTrXYearly  output_writer_yearly;
    public:
        OutputSoilchemistryMeTrXLayerDaily  output_writer_layer_daily;
        OutputSoilchemistryMeTrXLayerYearly  output_writer_layer_yearly;
        OutputSoilchemistryMeTrXFluxes  output_writer_fluxes;
        OutputSoilchemistryMeTrXPools  output_writer_pools;
        OutputSoilchemistryMeTrXSubdaily  output_writer_subdaily;
    private:

        lerr_t configure_output(
            MBE_LegacyOutputModel * /*output module*/, ldndc::config_file_t const * /*configuration*/,
            char const * /*option key*/);
};

} /*namespace ldndc*/

#undef  LMOD_OUTPUT_MODULE_BASE
#undef  LMOD_OUTPUT_MODULE_NAME
#undef  LMOD_OUTPUT_MODULE_ID
#undef  LMOD_OUTPUT_MODULE_DESC

#endif  /*  !LM_OUTPUT_SOILCHEMISTRYMETRX_H_  */

