/*!
 * @file
 * @author
 *     David Kraus (created on: may 12, 2014)
 * 
 */

#include  "soilchemistry/metrx/soilchemistry-metrx.h"

namespace ldndc {

/*!
 * @page metrx
 * @section fermentation Fermentation and synthrophy
 *  Under anaerobic conditions acetate and hydrogen are produced, which
 *  may serve as substrate for iron reducing and methanogenic bacteria.
 *
 */
lerr_t
SoilChemistryMeTrX::MeTrX_fermentation()
{
    for( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
    {
        /*!
         * @page metrx
         *  Temperature dependency of microbial activity:
         *  @image html metrx_fermentation_on_temperature.png "Response function depending on temperature" width=300
         *  @image latex metrx_fermentation_on_temperature.png "Response function depending on temperature"
         */
        double const activity_an( MeTrX_get_fact_tm_mic( sl));
        double const active_microbes( sc_.C_micro3_sl[sl] * activity_an);
        double const cn_ratio( sc_.C_micro3_sl[sl] / n_micro_3_sl[sl]);
        double const c_tot( sc_.an_doc_sl[sl] + an_acetate_sl[sl]);
        double const n_tot( no3_an_sl[sl] + sc_.an_no2_sl[sl] + nh4_an_sl[sl] + don_an_sl[sl]);
        double const k_mm_c( MeTrX_get_k_mm_x_sl( sl, sc_.anvf_sl[sl] * sipar_.METRX_KMM_C_MIC()));
        double const k_mm_n( MeTrX_get_k_mm_x_sl( sl, sc_.anvf_sl[sl] * sipar_.METRX_KMM_N_MIC()));

        /******************************/
        /* Fermentation and Syntrophy */
        /******************************/

        if ( cbm::flt_greater_zero( sc_.an_doc_sl[sl]))
        {
            // Only 19% of C is used for assimilation assuming a four times higher C-need for dissimilation
            double const fact_c( MeTrX_get_fact_mm( c_tot, k_mm_c));
            double const c_change_assi_an( cbm::bound_max( active_microbes * MUE_MAX_C_MICRO_3 * fact_c,
                                                           0.19 * sc_.an_doc_sl[sl]));
            double const c_change_diss_an( c_change_assi_an * 4.0);
            
            // Distinguish between acetate production via homoacetogenic and fermentative/syntrophic pathway.    [kg H2 hr-1]
            double const fact_h2( h2_sl[sl] / (MeTrX_get_k_mm_x_sl( sl, sc_.anvf_sl[sl] * sipar_.METRX_KMM_H2_FERM()) + h2_sl[sl]));
            double const acetate_prod_homoacet( fact_h2 * c_change_diss_an);
            double const acetate_prod_ferm_syn( ACETATE_RATIO_FERM_SYN * (c_change_diss_an - acetate_prod_homoacet));
            double const acetate_prod_tot( acetate_prod_homoacet + acetate_prod_ferm_syn);
            
            double const co2_prod_tot( c_change_diss_an - acetate_prod_homoacet - acetate_prod_ferm_syn);
            double const h2_c_eq_prod( 0.5 * co2_prod_tot);
            double const h2_prod_tot( h2_c_eq_prod / H2_KG_CH4_KG_CONVERSION);
            
            sc_.C_micro3_sl[sl] += c_change_assi_an;
            an_acetate_sl[sl] += acetate_prod_tot;
            h2_sl[sl] += h2_prod_tot;
            sc_.an_doc_sl[sl] -= c_change_assi_an + acetate_prod_tot + co2_prod_tot;

            /**********/
            /* Output */
            /**********/

            day_assi_c_mic_3 += c_change_assi_an;
            subdaily_acetate_prod[subdaily_time_step_] += acetate_prod_tot;
            day_acetate_prod_sl[sl] += acetate_prod_tot;
            co2_hetero_sl[sl] += co2_prod_tot;
            co2_gas_sl[sl] += co2_prod_tot;
            day_co2_prod_mic_3_acetate_prod += co2_prod_tot;
            day_h2_c_eq_prod_sl[sl] += h2_c_eq_prod;
        }

        /**********************************/
        /* Microbial N-Dynamics and Death */
        /**********************************/

        double const fact_n( MeTrX_get_fact_mm( n_tot, k_mm_n));
        double const pot_n_growth( MeTrX_get_n_growth( sc_.C_micro3_sl[sl],
                                                       n_micro_3_sl[sl],
                                                       activity_an,
                                                       fact_n,
                                                       cn_opt_mic_sl[sl]));

        MeTrX_execute_n_assimilation( pot_n_growth,
                                      sl,
                                      false,
                                      n_micro_3_sl[sl],
                                      day_min_n_mic_3,
                                      day_assi_n_mic_3);

        double const micro_c_decay_max( MeTrX_get_micro_c_decay_max( sc_.C_micro3_sl[sl]));
        if ( cbm::flt_greater_zero( micro_c_decay_max))
        {
            double const ka( sipar_.METRX_KA_C_MIC() / (sc_.h_sl[sl] * sc_.anvf_sl[sl]));
            double const decay_scale( active_microbes * A_MAX_MICRO_3);
            double const micro_death_c( cbm::bound_max( decay_scale * 1.0 / (1.0 + c_tot * ka),
                                                        micro_c_decay_max));
            double const micro_death_n( micro_death_c / cn_ratio);
            MeTrX_dead_microbial_biomass_allocation( sl,
                                                     micro_death_c,
                                                     micro_death_n,
                                                     sc_.C_micro3_sl[sl],
                                                     n_micro_3_sl[sl]);
        }
    }

    return LDNDC_ERR_OK;
}

} /*namespace ldndc*/

