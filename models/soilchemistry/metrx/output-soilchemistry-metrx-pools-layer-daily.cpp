/*!
 * @file
 * @author
 *  David Kraus
 */

#include  "soilchemistry/metrx/output-soilchemistry-metrx-pools-layer-daily.h"

LMOD_MODULE_INFO(OutputSoilchemistryMeTrXPools,TMODE_POST_DAILY,LMOD_FLAG_USER|LMOD_FLAG_OUTPUT);
namespace ldndc {

ldndc_string_t const  OutputSoilchemistryMeTrXPools_Ids[] =
{
    "level",
    "urea",
    "litter",
    "naorg",
    "humus1",
    "humus2",
    "humus3",
    "microbes",
    "nh3",
    "nh4",
    "don",
    "no3",
    "no2",
    "no",
    "n2o",
    //"naorg_sbl",
    "nh4_sbl",
    "nh3_sbl",
    "n2o_sbl",
    "no_sbl",
    "urea_sbl",
    "no3_sbl",
    //"don_sbl"
    "plants",
    "litter_surface"
};

ldndc_string_t const *  OutputSoilchemistryMeTrXPools_Header =
    OutputSoilchemistryMeTrXPools_Ids;


#define  OutputSoilchemistryMeTrXPools_Datasize  (sizeof( OutputSoilchemistryMeTrXPools_Ids) / sizeof( OutputSoilchemistryMeTrXPools_Ids[0]))
ldndc_output_size_t const  OutputSoilchemistryMeTrXPools_Sizes[] =
{
    OutputSoilchemistryMeTrXPools_Datasize,

    OutputSoilchemistryMeTrXPools_Datasize /*total size*/
};



ldndc_output_size_t const *  OutputSoilchemistryMeTrXPools_EntitySizes = NULL;

#define  OutputSoilchemistryMeTrXPools_Rank  ((ldndc_output_rank_t)(sizeof( OutputSoilchemistryMeTrXPools_Sizes) / sizeof( OutputSoilchemistryMeTrXPools_Sizes[0])) - 1)
atomic_datatype_t const  OutputSoilchemistryMeTrXPools_Types[] =
{
    LDNDC_FLOAT64
};



OutputSoilchemistryMeTrXPools::OutputSoilchemistryMeTrXPools(
                                                               MoBiLE_State *  _state,
                                                               cbm::io_kcomm_t *  _io_kcomm,
                                                               timemode_e  _timemode)
                                    : MBE_LegacyOutputModel( _state, _io_kcomm, _timemode),
                                    m_iokcomm( _io_kcomm),
                                    sl_( _io_kcomm->get_input_class_ref< input_class_soillayers_t >()),
                                    sb_( _state->get_substate_ref< substate_surfacebulk_t >())
{ }

OutputSoilchemistryMeTrXPools::~OutputSoilchemistryMeTrXPools()
{ }


size_t
OutputSoilchemistryMeTrXPools::record_size()
const
{
    return  OutputSoilchemistryMeTrXPools_Datasize;
}



lerr_t
OutputSoilchemistryMeTrXPools::configure(
                                          ldndc::config_file_t const *  _cf)
{
    if ( _cf && !_cf->have_output())
    {
        return  LDNDC_ERR_OK;
    }

    lerr_t  rc_setflags = set_metaflags( _cf, RM_DEFAULT_LAYERDAILY);
    if ( rc_setflags){ return  LDNDC_ERR_FAIL; }

    m_sink = m_iokcomm->sink_handle_acquire( "metrxpools");
    if ( m_sink.status() == LDNDC_ERR_OK)
    {
        lerr_t  rc_layout = LDNDC_OUTPUT_DEFINE_SINK_LAYOUT_DEFAULTS( m_sink, OutputSoilchemistryMeTrXPools);
        RETURN_IF_NOT_OK(rc_layout);
    }
    else
    {
        this->set_active_off();
    }

    return  LDNDC_ERR_OK;
}



lerr_t
OutputSoilchemistryMeTrXPools::initialize()
{
    return  LDNDC_ERR_OK;
}



lerr_t
OutputSoilchemistryMeTrXPools::solve()
{
    KLOGDEBUG( "metrx record size=", record_size());
    KLOGFATAL( "do not use solve(), we rock with write_results()");
    return  LDNDC_ERR_FAIL;

}



lerr_t
OutputSoilchemistryMeTrXPools::write_results(
        unsigned int  _layer, ldndc_flt64_t *  _data)
{
    if ( !_data || !m_sink.is_acquired())
    {
        /* we assume there is nothing to dump ... */
        return  LDNDC_ERR_OK;
    }

    /* output */
    void *  data[] = { _data};
    this->set_layernumber( -static_cast< int >( _layer)-1);
    lerr_t  rc_write =
        this->write_fixed_record( &this->m_sink, data);
    if ( rc_write)
        { return  LDNDC_ERR_FAIL; }
    return  LDNDC_ERR_OK;
}



lerr_t
OutputSoilchemistryMeTrXPools::finalize()
{
    return  m_iokcomm->sink_handle_release( &m_sink);
}

} /*namespace ldndc*/

#undef  OutputSoilchemistryMeTrXPools_Rank
#undef  OutputSoilchemistryMeTrXPools_Datasize
