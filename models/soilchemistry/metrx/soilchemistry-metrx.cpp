/*!
 * @file
 * @author
 *    - David Kraus
 * @date
 *  May 12, 2014
 */

#include  "soilchemistry/metrx/soilchemistry-metrx.h"
#include  "soilchemistry/ld_litterheight.h"
#include  "soilchemistry/ld_cation_exchange_capacity.h"
#include  "soilchemistry/ld_effective_diffusion_coefficient.h"

#include  <input/siteparameters/siteparameters.h>
#include  <input/soillayers/soillayers.h>
#include  <input/species/species.h>

#include  <math/cbm_math.h>
#include  <constants/cbm_const.h>

#include  <scientific/meteo/ld_meteo.h>
#include  <scientific/soil/ld_soil.h>

LMOD_MODULE_INFO(SoilChemistryMeTrX,TMODE_SUBDAILY|TMODE_POST_DAILY,LMOD_FLAG_USER);

REGISTER_OPTION(SoilChemistryMeTrX, algae, "Algae description text  [bool]");
REGISTER_OPTION(SoilChemistryMeTrX, drywet,"  [bool]");
REGISTER_OPTION(SoilChemistryMeTrX, surfacebulk,"  [bool]");
REGISTER_OPTION(SoilChemistryMeTrX, freezethaw,"  [bool]");
REGISTER_OPTION(SoilChemistryMeTrX, canopytransport,"  [bool]");
REGISTER_OPTION(SoilChemistryMeTrX, riverconnection,"  [bool]");
REGISTER_OPTION(SoilChemistryMeTrX, nochangelitterheight,"  [bool]");
REGISTER_OPTION(SoilChemistryMeTrX, effectivediffusion,"  [char]");
REGISTER_OPTION(SoilChemistryMeTrX, spinupyears,"  [int]");
REGISTER_OPTION(SoilChemistryMeTrX, spinupdeltac,"  [float]");
REGISTER_OPTION(SoilChemistryMeTrX, anvf,"  [char]");

REGISTER_OPTION(SoilChemistryMeTrX, output,"Controls if any output is written  [bool] {on}");
REGISTER_OPTION(SoilChemistryMeTrX, outputdaily,"Controls if daily output is written  [bool] {on}");
REGISTER_OPTION(SoilChemistryMeTrX, outputyearly,"Controls if yearly output is written  [bool] {on}");
REGISTER_OPTION(SoilChemistryMeTrX, outputlayerdaily,"Controls if daily layer output is written [bool] {on}");
REGISTER_OPTION(SoilChemistryMeTrX, outputlayeryearly,"Controls if yearly layer output is written [bool] {on}");
REGISTER_OPTION(SoilChemistryMeTrX, outputsubdaily,"Controls if subdaily output is written  [bool] {on}");
REGISTER_OPTION(SoilChemistryMeTrX, outputlayersubdaily,"Controls if subdaily layer output is written [bool] {on}");
REGISTER_OPTION(SoilChemistryMeTrX, outputfluxes,"Controls if fluxes output is written [bool] {on}");
REGISTER_OPTION(SoilChemistryMeTrX, outputpools,"Controls if pools output is written [bool] {on}");

REGISTER_OPTION(SoilChemistryMeTrX, loggerstream, "Stream for sending SoilChemistryMeTrX logging data");

namespace ldndc {



/*!
 * @details
 *  Kicks off computation for one time step.
 */
lerr_t
SoilChemistryMeTrX::solve()
{
    if ( (lclock()->subday() == 1) || (timemode_ != TMODE_SUBDAILY))
    {
        subdaily_time_step_ = 0;
    }

    lerr_t rc = MeTrX_check_for_negative_value( "entry");
    if ( rc) return LDNDC_ERR_FAIL;

    MeTrX_reset();

    rc = MeTrX_update();
    if ( rc) return LDNDC_ERR_FAIL;

    rc = MeTrX_spinup();
    if ( rc) return LDNDC_ERR_FAIL;

    rc = MeTrX_balance_check( 1);
    if ( rc) return LDNDC_ERR_FAIL;

    rc = MeTrX_physics();
    if ( rc) return LDNDC_ERR_FAIL;

    rc = MeTrX_fertilize();
    if ( rc) return LDNDC_ERR_FAIL;

    rc = MeTrX_manure();
    if ( rc) return LDNDC_ERR_FAIL;

    rc = MeTrX_till();
    if ( rc) return LDNDC_ERR_FAIL;

    MeTrX_leaching();

    for (size_t ts = 0; ts < INTERNAL_TIME_STEPS; ++ts)
    {
        MeTrX_dissolution();
        MeTrX_groundwater_access();
        MeTrX_ebullition();
        MeTrX_advection();
        MeTrX_gas_diffusion();
        MeTrX_liq_diffusion();

        MeTrX_anaerobic_volume();

        MeTrX_algae_dynamics();

        lerr_t rc_meta = MeTrX_metabolism();
        if (rc_meta) { return rc_meta; }

        lerr_t rc_pertub = MeTrX_pertubation();
        if (rc_pertub) { return rc_pertub; }

        MeTrX_nitrogen_fertilizer_release();
        MeTrX_freeze_thaw();
        MeTrX_dry_wet();

        MeTrX_plant_respiration(ts);
        MeTrX_fragmentation();
        MeTrX_soil_organic_matter_turnover();
        MeTrX_microbial_dynamics();

        MeTrX_nitrification();
        MeTrX_denitrification();
        MeTrX_chemodenitrification();
        MeTrX_fermentation();

        MeTrX_urea_hydrolysis();
        MeTrX_clay_nh4_equilibrium();
        MeTrX_nh3_nh4_equilibrium();

        MeTrX_pH_calculation();

        MeTrX_iron_reduction();
        MeTrX_iron_oxidation();

        MeTrX_ch4_production();
        MeTrX_ch4_oxidation();

        subdaily_time_step_ += 1;
    }

    rc = MeTrX_balance_check( 2);
    if ( rc) return LDNDC_ERR_FAIL;

    MeTrX_write_rates();
    MeTrX_write_output();

    rc = MeTrX_check_for_negative_value( "exit");
    if ( rc) return LDNDC_ERR_FAIL;

    return  LDNDC_ERR_OK;
}



/*!
 * @brief
 *
 */
lerr_t
SoilChemistryMeTrX::MeTrX_metabolism()
{
    for( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
    {
        double const metabolism_rate( 0.1 * FTS_TOT_);

        double const mic_12( sc_.C_micro1_sl[sl] + sc_.C_micro2_sl[sl]);
        double const mic_23( sc_.C_micro2_sl[sl] + sc_.C_micro3_sl[sl]);

        if ( cbm::flt_greater_zero( mic_12))
        {
            double const fact_mic_12( sc_.anvf_sl[sl]);
            double trans_12( (fact_mic_12 * mic_12 - sc_.C_micro2_sl[sl]) * metabolism_rate);
            if ( cbm::flt_greater_zero( trans_12))
            {
                double const trans_max( cbm::bound_min( 0.0, sc_.C_micro1_sl[sl] - MICRO_C_MIN));
                trans_12 = cbm::bound_max( trans_12, trans_max);
                sc_.C_micro1_sl[sl] -= trans_12;
                sc_.C_micro2_sl[sl] += trans_12;
            }
            else
            {
                double const trans_max( cbm::bound_min( 0.0, sc_.C_micro2_sl[sl] - MICRO_C_MIN));
                trans_12 = cbm::bound_max( -trans_12, trans_max);
                sc_.C_micro2_sl[sl] -= trans_12;
                sc_.C_micro1_sl[sl] += trans_12;
            }
        }

        if ( cbm::flt_greater_zero( mic_23))
        {
            double const fact_mic_23( 1.0 - no3_an_sl[sl] / (no3_an_sl[sl] + (NO3_MOLAR_MAX_FE_RED * sc_.h_sl[sl] * M_N_SCALE)));
            double trans_23( (fact_mic_23 * mic_23 - sc_.C_micro3_sl[sl]) * metabolism_rate);
            if ( cbm::flt_greater_zero( trans_23))
            {
                double const trans_max( cbm::bound_min( 0.0, sc_.C_micro2_sl[sl] - MICRO_C_MIN));
                trans_23 = cbm::bound_max( trans_23, trans_max);
                sc_.C_micro2_sl[sl] -= trans_23;
                sc_.C_micro3_sl[sl] += trans_23;
            }
            else
            {
                double const trans_max( cbm::bound_min( 0.0, sc_.C_micro3_sl[sl] - MICRO_C_MIN));
                trans_23 = cbm::bound_max( -trans_23, trans_max);
                sc_.C_micro3_sl[sl] -= trans_23;
                sc_.C_micro2_sl[sl] += trans_23;
            }
        }
    }
    return  LDNDC_ERR_OK;
}



/*!
 * @brief
 *
 */
lerr_t
SoilChemistryMeTrX::integrate()
{
    StubbleCarbon.send( sc_.c_stubble_lit1 + sc_.c_stubble_lit2 + sc_.c_stubble_lit3);

    return  LDNDC_ERR_OK;
}



/*!
 * @brief
 *
 */
#define UPDATE_STATE(__state_old_1__, __state_old_2__, __state_new__)            \
{                                                                                \
{                                                                                \
double const state_sum( __state_old_1__ + __state_old_2__);                      \
if (cbm::flt_greater_zero( state_sum))                                         \
{                                                                                \
double const state_new_1( __state_old_1__ / state_sum * __state_new__);          \
double const state_new_2( __state_new__ - state_new_1);                          \
__state_old_1__ = state_new_1;                                                   \
__state_old_2__ = state_new_2;                                                   \
}                                                                                \
else                                                                             \
{                                                                                \
__state_old_1__ = 0.5*__state_new__;                                             \
__state_old_2__ = 0.5*__state_new__;                                             \
}                                                                                \
}                                                                                \
}                                                                                \



/*!
 * @brief
 *
 */
#define CHECK_FOR_ZERO(__val__)                                                  \
{                                                                                \
{                                                                                \
if ( !cbm::flt_greater_zero( __val__))                                         \
{                                                                                \
    __val__ = 0.0;                                                               \
}                                                                                \
}                                                                                \
}



/*!
 * @brief
 *
 */
size_t
SoilChemistryMeTrX::SpinUp::simulated_years( cbm::sclock_t const * _clk) const
{
    return static_cast<unsigned int>( _clk->seconds() / (cbm::SEC_IN_DAY * 366.1));
}



/*!
 * @brief
 *
 */
bool
SoilChemistryMeTrX::SpinUp::spinup_stage( cbm::sclock_t const * _clk) const
{
    return this->simulated_years( _clk) < this->spinup_years();
}


/*!
 * @brief
 *
 */
bool
SoilChemistryMeTrX::SpinUp::execute_spinup( cbm::sclock_t const *  _clk)
{
    if ( !execute_)
    {
        return false;
    }
    else if ( (_clk->day() == _clk->days_in_month()) &&
              (_clk->subday() == 1) &&
              (_clk->seconds() > 4 * 24 * 3600))
    {
        return true;
    }
    else
    {
        return false;
    }
}



/*!
 * @brief
 *
 */
lerr_t
SoilChemistryMeTrX::MeTrX_spinup()
{
    if ( spinup_.execute_spinup( lclock()))
    {
        for( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
        {
          accumulated_n_litter_spinup_sl[sl] -= sc_.n_wood_sl[sl];
        }

        /* WOOD */
        double c_wood(  sc_.c_wood);
        double n_wood(  sc_.n_wood);
        if ( cbm::flt_greater_zero( spinup_.exit_wood) &&
             cbm::flt_equal_zero( (spinup_.executed_months_+1) % 12))
        {
            double const fraction( 0.5);
            double const c_wood_temp( cbm::bound( 0.5, spinup_.enter_wood / spinup_.exit_wood, 1.5) * sc_.c_wood);
            sc_.c_wood = c_wood_temp * fraction + c_wood * (1.0 - fraction);

            double const n_demand_wood( cbm::flt_greater_zero( n_wood) ?
                                        sc_.c_wood / (c_wood / n_wood)
                                        : 0.0);
            sc_.n_wood = n_demand_wood;

            for( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
            {
                double c_wood_sl(  sc_.c_wood_sl[sl]);
                double n_wood_sl(  sc_.n_wood_sl[sl]);

                double const c_wood_sl_temp( cbm::bound( 0.5, spinup_.enter_wood / spinup_.exit_wood, 1.5) * sc_.c_wood_sl[sl]);
                sc_.c_wood_sl[sl] = c_wood_sl_temp * 0.5 + c_wood_sl * 0.5;

                double const n_demand_wood_sl( cbm::flt_greater_zero( n_wood_sl) ?
                                           sc_.c_wood_sl[sl] / (c_wood_sl / n_wood_sl)
                                           : 0.0);
                sc_.n_wood_sl[sl] = n_demand_wood_sl;
            }
        }

        double scale_depth_sum( 0.0);
        for( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
        {
            scale_depth_sum += std::exp(-10.0 * sc_.depth_sl[sl]) * sc_.h_sl[sl];
            accumulated_n_litter_spinup_sl[sl] += sc_.n_wood_sl[sl];
        }

        /* HUMUS */
        for( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
        {
            for ( int i = 0; i < 10; ++i)
            {
                double c_hum_1( c_humus_1_sl[sl]);
                double n_hum_1( n_humus_1_sl[sl]);
                double c_hum_2( c_humus_2_sl[sl]);
                double n_hum_2( n_humus_2_sl[sl]);
                double c_hum_3( c_humus_3_sl[sl]);
                double n_hum_3( n_humus_3_sl[sl]);

                double const c_tot( c_hum_1 + c_hum_2 + c_hum_3);
                double const n_tot( n_hum_1 + n_hum_2 + n_hum_3);

                double const scale_depth( std::exp(-10.0 * sc_.depth_sl[sl]) * sc_.h_sl[sl]);
                double const scale_hum( sipar_.METRX_KR_DC_HUM1() / sipar_.METRX_KR_DC_HUM2());

                int spinup_steps_per_year( 12);
                double const delta_c_hum_1( scale_hum * c_hum_1 / (scale_hum * c_hum_1 + c_hum_2) * scale_depth / scale_depth_sum 
                                            * spinup_delta_c / spinup_steps_per_year);
                double const delta_c_hum_2( c_hum_2             / (scale_hum * c_hum_1 + c_hum_2) * scale_depth / scale_depth_sum
                                            * spinup_delta_c / spinup_steps_per_year);

                double const c_humus_1_tmp( cbm::bound_min( -1.0e-9, spinup_.humify_c_to_humus_1_sl[sl] - delta_c_hum_1)
                                           / (spinup_.decompose_hum_1[sl] + spinup_.humify_humus_1_to_humus_2_sl[sl]));

                double const c_humus_2_tmp( cbm::bound_min( -1.0e-9, spinup_.humify_c_to_humus_2_sl[sl]
                                                            + spinup_.humify_humus_1_to_humus_2_sl[sl] * c_humus_1_sl[sl]
                                                            - delta_c_hum_2)
                                           / (spinup_.decompose_hum_2[sl] + spinup_.humify_humus_2_to_humus_3_sl[sl]));

                //change less with time
                double const fraction( 0.1);

                //use upper limit for humus pool 1
                c_humus_1_sl[sl] = cbm::bound_max( c_humus_1_tmp * fraction + c_hum_1 * (1.0 - fraction), 0.2 * c_tot);
                c_humus_2_sl[sl] = cbm::bound_max( c_humus_2_tmp * fraction + c_hum_2 * (1.0 - fraction), 0.7 * c_tot);
                c_humus_3_sl[sl] = c_tot - c_humus_1_sl[sl] -c_humus_2_sl[sl];

                double const n_demand_humus_1( c_humus_1_sl[sl] / cn_hum_1_sl[sl]);
                double const n_demand_humus_2( c_humus_2_sl[sl] / cn_hum_2_sl[sl]);
                double const n_demand_humus_3( c_humus_3_sl[sl] / cn_hum_3_sl[sl]);
                double const n_demand_tot( n_demand_humus_1 + n_demand_humus_2 + n_demand_humus_3);

                accumulated_n_humus_1_spinup_sl[sl] -= n_humus_1_sl[sl];
                accumulated_n_humus_2_spinup_sl[sl] -= n_humus_2_sl[sl];
                accumulated_n_humus_3_spinup_sl[sl] -= n_humus_3_sl[sl];

                n_humus_1_sl[sl] = n_demand_humus_1 / n_demand_tot * n_tot;
                n_humus_2_sl[sl] = n_demand_humus_2 / n_demand_tot * n_tot;
                n_humus_3_sl[sl] = n_demand_humus_3 / n_demand_tot * n_tot;

                accumulated_n_humus_1_spinup_sl[sl] += n_humus_1_sl[sl];
                accumulated_n_humus_2_spinup_sl[sl] += n_humus_2_sl[sl];
                accumulated_n_humus_3_spinup_sl[sl] += n_humus_3_sl[sl];

                //adapt only target cn of recalcitrant pools
                cn_hum_2_sl[sl] = c_humus_2_sl[sl] / n_humus_2_sl[sl];
                cn_hum_3_sl[sl] = c_humus_3_sl[sl] / n_humus_3_sl[sl];
            }

            spinup_.cn_humus_2_temp_sl[sl] += cn_hum_2_sl[sl];
            spinup_.cn_humus_3_temp_sl[sl] += cn_hum_3_sl[sl];

            spinup_.c_humus_1_temp_sl[sl] += c_humus_1_sl[sl];
            spinup_.c_humus_2_temp_sl[sl] += c_humus_2_sl[sl];
            spinup_.c_humus_3_temp_sl[sl] += c_humus_3_sl[sl];
        }

        spinup_.executed_months_ += 1;

        if ( spinup_.executed_months_ == spinup_.spinup_months())
        {
            double const c_hum_tot( c_humus_1_sl.sum() + c_humus_2_sl.sum() + c_humus_3_sl.sum());
            double const c_hum_1_acc_tot(  spinup_.c_humus_1_temp_sl.sum());
            double const c_hum_2_acc_tot(  spinup_.c_humus_2_temp_sl.sum());
            double const c_hum_3_acc_tot(  spinup_.c_humus_3_temp_sl.sum());
            for( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
            {
                double const c_hum_acc_tot(  c_hum_1_acc_tot + c_hum_2_acc_tot + c_hum_3_acc_tot);

                double const c_hum_1_tot( c_hum_1_acc_tot / c_hum_acc_tot * c_hum_tot);
                double const c_hum_2_tot( c_hum_2_acc_tot / c_hum_acc_tot * c_hum_tot);
                double const c_hum_3_tot( c_hum_3_acc_tot / c_hum_acc_tot * c_hum_tot);

                c_humus_1_sl[sl] = spinup_.c_humus_1_temp_sl[sl] / c_hum_1_acc_tot * c_hum_1_tot;
                c_humus_2_sl[sl] = spinup_.c_humus_2_temp_sl[sl] / c_hum_2_acc_tot * c_hum_2_tot;
                c_humus_3_sl[sl] = spinup_.c_humus_3_temp_sl[sl] / c_hum_3_acc_tot * c_hum_3_tot;
            }

            double const c_difference( c_hum_tot - (c_humus_1_sl.sum() + c_humus_2_sl.sum() + c_humus_3_sl.sum()));
            if ( cbm::flt_greater( c_difference, 1.0e-10))
            {
                KLOGERROR( name(),": C-leakage during spinup: ", c_difference*cbm::M2_IN_HA, "[kg C ha-1]");
            }

            double const n_old_total( n_humus_1_sl.sum() + n_humus_2_sl.sum() + n_humus_3_sl.sum());
            double n_new_total( 0.0);
            for( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
            {
                cn_hum_2_sl[sl] = spinup_.cn_humus_2_temp_sl[sl] / spinup_.executed_months_;
                cn_hum_3_sl[sl] = spinup_.cn_humus_3_temp_sl[sl] / spinup_.executed_months_;

                n_new_total += c_humus_1_sl[sl] / cn_hum_1_sl[sl];
                n_new_total += c_humus_2_sl[sl] / cn_hum_2_sl[sl];
                n_new_total += c_humus_3_sl[sl] / cn_hum_3_sl[sl];
            }

            double const scale( n_old_total / n_new_total);
            for( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
            {
                accumulated_n_humus_1_spinup_sl[sl] -= n_humus_1_sl[sl];
                accumulated_n_humus_2_spinup_sl[sl] -= n_humus_2_sl[sl];
                accumulated_n_humus_3_spinup_sl[sl] -= n_humus_3_sl[sl];

                n_humus_1_sl[sl] = c_humus_1_sl[sl] / cn_hum_1_sl[sl] * scale;
                n_humus_2_sl[sl] = c_humus_2_sl[sl] / cn_hum_2_sl[sl] * scale;
                n_humus_3_sl[sl] = c_humus_3_sl[sl] / cn_hum_3_sl[sl] * scale;

                accumulated_n_humus_1_spinup_sl[sl] += n_humus_1_sl[sl];
                accumulated_n_humus_2_spinup_sl[sl] += n_humus_2_sl[sl];
                accumulated_n_humus_3_spinup_sl[sl] += n_humus_3_sl[sl];
            }

            double const n_difference( n_old_total - (n_humus_1_sl.sum() + n_humus_2_sl.sum() + n_humus_3_sl.sum()));
            if ( cbm::flt_greater( c_difference, 1.0e-10))
            {
                KLOGERROR( name(),": N-leakage during spinup: ", n_difference*cbm::M2_IN_HA, "[kg N ha-1]");
            }

            spinup_.stop_spinup();
        }

        spinup_.reset();
    }

    return LDNDC_ERR_OK;
}



/*!
 * @brief
 *
 */
lerr_t
SoilChemistryMeTrX::MeTrX_update()
{
    lerr_t rc = MeTrX_receive_state();
    if ( rc)
    {
        return LDNDC_ERR_FAIL;
    }

    /* update module specific variables from state */
    for( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
    {
        /* use liq/gas distribution from previous time step */
        UPDATE_STATE( no_liq_sl[sl], no_gas_sl[sl], sc_.no_sl[sl]);
        UPDATE_STATE( an_no_liq_sl[sl], an_no_gas_sl[sl], sc_.an_no_sl[sl]);
        UPDATE_STATE( n2o_liq_sl[sl], n2o_gas_sl[sl], sc_.n2o_sl[sl]);
        UPDATE_STATE( an_n2o_liq_sl[sl], an_n2o_gas_sl[sl], sc_.an_n2o_sl[sl]);

        CHECK_FOR_ZERO( sc_.C_lit1_sl[sl]);
        CHECK_FOR_ZERO( sc_.C_lit2_sl[sl]);
        CHECK_FOR_ZERO( sc_.C_lit3_sl[sl]);
        CHECK_FOR_ZERO( sc_.N_lit1_sl[sl]);
        CHECK_FOR_ZERO( sc_.N_lit2_sl[sl]);
        CHECK_FOR_ZERO( sc_.N_lit3_sl[sl]);
    }


    /***************************/
    /* Vertical discretization */
    /***************************/
    
    if ( !have_no_litter_height_change)
    {
        lerr_t rc_litter = update_litter_height( sl_, wc_, sc_);
        if ( rc_litter){ return LDNDC_ERR_FAIL; }

        //lerr_t rc_soil = update_soil_height( sl_, wc_, sc_);
        //if ( rc_soil){ return LDNDC_ERR_FAIL; }
    }

    /* update module specific layer height dependent distance from midpoints of neighbour layers */
    for( size_t  sl = 0;  sl < sl_.soil_layer_cnt()-1;  ++sl)
    {
        delta_x[sl] = cbm::arithmetic_mean2( sc_.h_sl[sl], sc_.h_sl[sl+1]);
    }
    delta_x[sl_.soil_layer_cnt()-1] = sc_.h_sl[sl_.soil_layer_cnt()-1];


    /****************/
    /* Water fluxes */
    /****************/

    infiltration = wc_.accumulated_infiltration - accumulated_infiltration_old;
    accumulated_infiltration_old = wc_.accumulated_infiltration;

    for( size_t sl = 0; sl < sl_.soil_layer_cnt(); ++sl)
    {
        waterflux_sl[sl] = wc_.accumulated_waterflux_sl[sl] - accumulated_waterflux_old_sl[sl];
        accumulated_waterflux_old_sl[sl] = wc_.accumulated_waterflux_sl[sl];
    }


    /***********************/
    /* Nitrogen deposition */
    /***********************/

    if ( cbm::flt_greater_zero( wc_.surface_water) || cbm::flt_greater_zero( wc_.surface_ice))
    {
        accumulated_n_nh4_throughfall_sbl[0] += ph_.accumulated_nh4_throughfall - accumulated_n_nh4_throughfall_old;
        accumulated_n_no3_throughfall_sbl[0] += ph_.accumulated_no3_throughfall - accumulated_n_no3_throughfall_old;
    }
    else
    {
        accumulated_n_nh4_throughfall_sl[0] += ph_.accumulated_nh4_throughfall - accumulated_n_nh4_throughfall_old;
        accumulated_n_no3_throughfall_sl[0] += ph_.accumulated_no3_throughfall - accumulated_n_no3_throughfall_old;
    }
    accumulated_n_nh4_throughfall_old = ph_.accumulated_nh4_throughfall;
    accumulated_n_no3_throughfall_old = ph_.accumulated_no3_throughfall;


    /******************************/
    /* N import/export vegetation */
    /******************************/

    // export by harvest, cutting
    accumulated_n_export_harvest_cutting_grazing += ph_.accumulated_n_export_harvest -
                                                    accumulated_n_export_harvest_old;
    accumulated_n_export_harvest_old = ph_.accumulated_n_export_harvest;

    // import by planting
    EventAttributes const *  ev_plant = NULL;
    while (( ev_plant = this->m_PlantEvents.pop()) != NULL)
    {
        MoBiLE_Plant *  vt = m_veg->get_plant( ev_plant->get( "/name", "?"));
        accumulated_n_to_living_plant_and_algae_from_extern_sl[0] += vt->total_nitrogen();
    }


    /**************************/
    /* litter from vegetation */
    /**************************/

    // nitrogen
    accumulated_n_litter_from_plants_above_wood = sc_.accumulated_n_litter_wood_above;
    accumulated_n_litter_from_plants_above_rawlitter = sc_.accumulated_n_litter_above;
    accumulated_n_litter_from_plants_above_stubble = sc_.accumulated_n_litter_stubble;
    for ( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
    {
        accumulated_n_litter_from_plants_below_rawlitter_sl[sl] = sc_.accumulated_n_litter_below_sl[sl];
        accumulated_n_litter_from_plants_below_wood_sl[sl] = sc_.accumulated_n_litter_wood_below_sl[sl];
    }

    // carbon
    double const delta_c_wood( sc_.accumulated_c_litter_wood_above - accumulated_c_litter_wood_above_old);
    if (   cbm::flt_greater_zero( delta_c_wood))
    {
        if ( spinup_.spinup_stage( this->lclock()))
        {
            spinup_.enter_wood += delta_c_wood;
        }
    }
    accumulated_c_litter_wood_above_old = sc_.accumulated_c_litter_wood_above;


    /*******************/
    /* root exsudation */
    /*******************/

    double const delta_c_exsudation( sc_.accumulated_c_root_exsudates_sl.sum() -
                                     accumulated_c_root_exsudates_old_sl.sum());
    if ( cbm::flt_greater_zero( delta_c_exsudation))
    {
        dC_root_exsudates += delta_c_exsudation;
        subdaily_doc_prod[subdaily_time_step_] += delta_c_exsudation;
        for( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
        {
            double const delta_c_exsudation_sl( sc_.accumulated_c_root_exsudates_sl[sl] - accumulated_c_root_exsudates_old_sl[sl]);
            day_doc_prod_sl[sl] += delta_c_exsudation_sl;
            accumulated_c_root_exsudates_old_sl[sl] = sc_.accumulated_c_root_exsudates_sl[sl];
        }
    }


    /***************/
    /* litter type */
    /***************/

    double bio_coniferous( 0.0);
    double bio_deciduous( 0.0);
    for ( PlantIterator vt = this->m_veg->begin(); vt != this->m_veg->end(); ++vt)
    {
        MoBiLE_Plant *p( *vt);
        if ((*p)->DECIDUOUS())
        {
            bio_deciduous += p->aboveground_biomass();
        }
        else
        {
            bio_coniferous += p->aboveground_biomass();
        }
    }

    double const bio_tot( bio_deciduous + bio_coniferous);
    if ( cbm::flt_greater_zero( bio_tot))
    {
        double const frac_deciduous( bio_deciduous / bio_tot);
        double const frac_coniferous( 1.0 - frac_deciduous);
        for (size_t sl = 0; sl <= SL_SURFACE_DISTRIBUTION; ++sl)
        {
            litter_type_fact_sl[sl] = (frac_deciduous + sipar_.METRX_KR_REDUCTION_CONIFEROUS() * frac_coniferous);
        }
    }

    return LDNDC_ERR_OK;
}



/*!
 * @brief
 *
 */
lerr_t
SoilChemistryMeTrX::MeTrX_write_rates()
{
    sc_.accumulated_n2_emis = 0.0;
    for( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
    {
        sc_.no_sl[sl] = (no_gas_sl[sl]+no_liq_sl[sl]);
        sc_.an_no_sl[sl] = (an_no_gas_sl[sl]+an_no_liq_sl[sl]);

        sc_.n2o_sl[sl] = (n2o_gas_sl[sl]+n2o_liq_sl[sl]);
        sc_.an_n2o_sl[sl] = (an_n2o_gas_sl[sl]+an_n2o_liq_sl[sl]);

        sc_.C_hum_sl[sl] = (c_humus_1_sl[sl] + c_humus_2_sl[sl] + c_humus_3_sl[sl]);
        sc_.N_hum_sl[sl] = (n_humus_1_sl[sl] + n_humus_2_sl[sl] + n_humus_3_sl[sl]);

        sc_.N_micro_sl[sl] = (n_micro_1_sl[sl] + n_micro_2_sl[sl] + n_micro_3_sl[sl]);
        sc_.som_sl[sl]  = ((sc_.C_lit1_sl[sl] + sc_.C_lit2_sl[sl] + sc_.C_lit3_sl[sl]
                            + c_humus_1_sl[sl] + c_humus_2_sl[sl] + c_humus_3_sl[sl]
                            + sc_.C_aorg_sl[sl] + sc_.C_micro1_sl[sl] + sc_.C_micro2_sl[sl] + sc_.C_micro3_sl[sl]) / cbm::CCORG);

        sc_.o2_sl[sl] = (o2_gas_sl[sl] * RGAS_M_O2_BAR_PA * (mc_temp_sl[sl] + cbm::D_IN_K) / v_air_sl[sl]);

        sc_.accumulated_no3_denitrify_sl[sl] = accumulated_n_no3_no2_denitrification_sl[sl];
        sc_.accumulated_n2_emis += accumulated_n_no3_n2_denitrification_sl[sl]
                                 + accumulated_n_no2_n2_denitrification_sl[sl]
                                 + accumulated_n_no_n2_denitrification_sl[sl]
                                 + accumulated_n_n2o_n2_denitrification_sl[sl];
    }


    size_t const ts_end( (timemode_ == TMODE_SUBDAILY) ? lclock()->subday() * INTERNAL_TIME_STEPS : INTERNAL_TIME_STEPS);
    size_t const ts_start( ts_end - INTERNAL_TIME_STEPS);
    for ( size_t ts = ts_start; ts < ts_end; ++ts)
    {
        sc_.accumulated_co2_emis += subdaily_co2_soil[ts] + subdaily_co2_plant[ts] + subdaily_co2_bubbling[ts] + subdaily_co2_water[ts];
        sc_.accumulated_ch4_emis += subdaily_ch4_soil[ts] + subdaily_ch4_plant[ts] + subdaily_ch4_bubbling[ts] + subdaily_ch4_water[ts];
        sc_.accumulated_n2o_emis += subdaily_n2o_soil[ts] + subdaily_n2o_plant[ts] + subdaily_n2o_bubbling[ts] + subdaily_n2o_water[ts];
        sc_.accumulated_no_emis  += subdaily_no_soil[ts]  + subdaily_no_plant[ts]  + subdaily_no_bubbling[ts]  + subdaily_no_water[ts];
        sc_.accumulated_nh3_emis += subdaily_nh3_soil[ts] + subdaily_nh3_plant[ts] + subdaily_nh3_bubbling[ts] + subdaily_nh3_water[ts];

        sc_.accumulated_ch4_leach += subdaily_ch4_leach[ts];
    }

    sc_.accumulated_co2_emis_auto += co2_auto_sl.sum();
    sc_.accumulated_co2_emis_hetero += co2_hetero_sl.sum();


    if (timemode_ == TMODE_SUBDAILY)
    {
        for ( PlantIterator vt = this->m_veg->begin(); vt != this->m_veg->end(); ++vt)
        {
            accumulated_n_to_living_plant_and_algae_from_extern_sl[0] += (*vt)->n2_fixation;
        }
    }
    else
    {
        for ( PlantIterator vt = this->m_veg->begin(); vt != this->m_veg->end(); ++vt)
        {
            accumulated_n_to_living_plant_and_algae_from_extern_sl[0] += (*vt)->d_n2_fixation;
        }
    }

    if ( timemode_ != TMODE_SUBDAILY || lclock()->is_position( TMODE_POST_DAILY))
    {
        /*************************/
        /* Yearly rate variables */
        /*************************/

        year_c_fix_algae += day_c_fix_algae;
        year_n_fix_algae += day_n_fix_algae;
        sc_.accumulated_c_fix_algae += day_c_fix_algae;
        sc_.accumulated_n_fix_algae += day_n_fix_algae;

        // Groundwater access
        year_no3_groundwater_access += day_no3_groundwater_access;

        // Humification
        for ( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
        {
            year_c_humify_doc_hum_1_sl[sl] += day_c_humify_doc_hum_1_sl[sl];
            year_c_humify_sol_hum_1_sl[sl] += day_c_humify_sol_hum_1_sl[sl];
            year_c_humify_cel_hum_1_sl[sl] += day_c_humify_cel_hum_1_sl[sl];
            year_c_humify_lig_hum_1_sl[sl] += day_c_humify_lig_hum_1_sl[sl];
            year_c_humify_lig_hum_2_sl[sl] += day_c_humify_lig_hum_2_sl[sl];
            year_c_humify_mic_hum_1_sl[sl] += day_c_humify_mic_hum_1_sl[sl];
            year_c_humify_mic_hum_2_sl[sl] += day_c_humify_mic_hum_2_sl[sl];
            year_c_humify_hum_1_hum_2_sl[sl] += day_c_humify_hum_1_hum_2_sl[sl];
            year_c_humify_hum_2_hum_3_sl[sl] += day_c_humify_hum_2_hum_3_sl[sl];
            year_c_decomp_hum_1_sl[sl] += day_decomp_c_hum_1_sl[sl];
            year_c_decomp_hum_2_sl[sl] += day_decomp_c_hum_2_sl[sl];
            year_c_decomp_hum_3_sl[sl] += day_decomp_c_hum_3_sl[sl];

            year_doc_prod_sl[sl] += day_doc_prod_sl[sl];
            year_decay_c_mic_sl[sl] += day_decay_c_mic_sl[sl];
            year_co2_hetero_sl[sl] += co2_hetero_sl[sl];
        }

        // Decomposition
        year_decomp_c_lit_1 += day_decomp_c_lit_1;
        year_decomp_c_lit_2 += day_decomp_c_lit_2;
        year_decomp_c_lit_3 += day_decomp_c_lit_3;
        year_decomp_n_lit_1 += day_decomp_n_lit_1;
        year_decomp_n_lit_2 += day_decomp_n_lit_2;
        year_decomp_n_lit_3 += day_decomp_n_lit_3;
        year_decomp_n_hum_1 += day_decomp_n_hum_1;
        year_decomp_n_hum_2 += day_decomp_n_hum_2;
        year_decomp_n_hum_3 += day_decomp_n_hum_3;

        // Methane
        year_ch4_ox += day_ch4_oxidation_sl.sum();
    }

    MeTrX_send_state();

    return LDNDC_ERR_OK;
}



/*!
 * @brief
 */
lerr_t
SoilChemistryMeTrX::MeTrX_receive_state()
{
    for ( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
    {
        if ( cbm::flt_greater_zero( sc_.nh4_sl[sl]))
        {
            double const nh4_old( nh4_ae_sl[sl] + nh4_an_sl[sl]);
            if ( cbm::flt_greater_zero( nh4_old))
            {
                nh4_ae_sl[sl] = cbm::bound_min( 0.0, nh4_ae_sl[sl] / nh4_old * sc_.nh4_sl[sl]);
                nh4_an_sl[sl] = cbm::bound_min( 0.0, sc_.nh4_sl[sl] - nh4_ae_sl[sl]);
            }
            else
            {
                nh4_ae_sl[sl] = (1.0 - sc_.anvf_sl[sl]) * sc_.nh4_sl[sl];
                nh4_an_sl[sl] = sc_.anvf_sl[sl] * sc_.nh4_sl[sl];
            }
        }
        else
        {
            nh4_ae_sl[sl] = 0.0;
            nh4_an_sl[sl] = 0.0;
        }

        if ( cbm::flt_greater_zero( sc_.don_sl[sl]))
        {
            double const don_old( don_ae_sl[sl] + don_an_sl[sl]);
            if ( cbm::flt_greater_zero( don_old))
            {
                don_ae_sl[sl] = cbm::bound_min( 0.0, don_ae_sl[sl] / don_old * sc_.don_sl[sl]);
                don_an_sl[sl] = cbm::bound_min( 0.0, sc_.don_sl[sl] - don_ae_sl[sl]);
            }
            else
            {
                don_ae_sl[sl] = (1.0 - sc_.anvf_sl[sl]) * sc_.don_sl[sl];
                don_an_sl[sl] = sc_.anvf_sl[sl] * sc_.don_sl[sl];
            }
        }
        else
        {
            don_ae_sl[sl] = 0.0;
            don_an_sl[sl] = 0.0;
        }

        double no3_tot( sc_.no3_sl[sl] + sc_.an_no3_sl[sl]);
        if ( cbm::flt_greater_zero( no3_tot))
        {
            double const no3_old( no3_ae_sl[sl] + no3_an_sl[sl]);
            if ( cbm::flt_greater_zero( no3_old))
            {
                no3_ae_sl[sl] = cbm::bound_min( 0.0, no3_ae_sl[sl] / no3_old * no3_tot);
                no3_an_sl[sl] = cbm::bound_min( 0.0, no3_tot - no3_ae_sl[sl]);
            }
            else
            {
                no3_ae_sl[sl] = (1.0 - sc_.anvf_sl[sl]) * no3_tot;
                no3_an_sl[sl] = sc_.anvf_sl[sl] * no3_tot;
            }
        }
        else
        {
            no3_ae_sl[sl] = 0.0;
            no3_an_sl[sl] = 0.0;
        }
    }

    return LDNDC_ERR_OK;
}



/*!
 * @brief
 */
lerr_t
SoilChemistryMeTrX::MeTrX_send_state()
{
    for( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
    {
        sc_.nh4_sl[sl] = nh4_ae_sl[sl] + nh4_an_sl[sl];
        sc_.don_sl[sl] = don_ae_sl[sl] + don_an_sl[sl];
        sc_.no3_sl[sl] = no3_ae_sl[sl] + no3_an_sl[sl];
        sc_.an_no3_sl[sl] = 0.0;
    }

    return LDNDC_ERR_OK;
}



/*!
 * @brief
 */
lerr_t
SoilChemistryMeTrX::MeTrX_freeze_thaw()
{
    if ( have_freeze_thaw)
    {
        for( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
        {
            if ( cbm::flt_greater_zero( v_ice_sl[sl]) &&
                 cbm::flt_greater( mc_temp_sl[sl], -0.5))
            {
                freeze_thaw_fact_sl[sl] = 1.0;
            }
            else if (freeze_thaw_fact_sl[sl] > 0.1)
            {
                freeze_thaw_fact_sl[sl] -= 0.3 * FTS_TOT_ * freeze_thaw_fact_sl[sl];
            }
            else
            {
                freeze_thaw_fact_sl[sl] = 0.0;
            }
        }
    }

    return LDNDC_ERR_OK;
}



/*!
 * @brief
 */
double
SoilChemistryMeTrX::MeTrX_get_wfps(
                                   size_t _sl)
{
    return wc_.wc_sl[_sl] / sc_.poro_sl[_sl];
}



/*!
 * @brief
 */
lerr_t
SoilChemistryMeTrX::MeTrX_dry_wet()
{
    if ( have_dry_wet)
    {
        for( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
        {
            if ( MeTrX_get_wfps( sl) < sc_.wcmin_sl[sl] / sc_.poro_sl[sl]
                && cbm::flt_greater(dry_wet_fact_sl[sl], 0.0))
            {
                dry_wet_fact_sl[sl] -= (0.01 * dry_wet_fact_sl[sl] * FTS_TOT_);
            }
            else if ( cbm::flt_greater(1.0, dry_wet_fact_sl[sl]))
            {
                dry_wet_fact_sl[sl] += 0.05 * FTS_TOT_;
            }
            else
            {
                dry_wet_fact_sl[sl] = 1.0;
            }
        }
    }

    return LDNDC_ERR_OK;
}



/*!
 * @page metrx
 * @section metrx_nitrogen_fertilizer_release Nitrogen fertilizer release
 */
lerr_t
SoilChemistryMeTrX::MeTrX_nitrogen_fertilizer_release()
{
    double const coated_nh4_sbl_sum( sb_.coated_nh4_sbl.sum());
    double const coated_nh4_sl_sum( sc_.coated_nh4_sl.sum());
    double const coated_nh4_sum( coated_nh4_sbl_sum + coated_nh4_sl_sum);
    if ( !cbm::flt_greater_zero( coated_nh4_sum))
    { return LDNDC_ERR_OK; }
    
    if ( cbm::flt_greater_zero( coated_nh4_sbl_sum))
    {
        for (size_t sbl = 0; sbl < sb_.surfacebulk_layer_cnt(); ++sbl)
        {
            crnf_.update_crnf( 1.0, mc_temp_sl[0], FTS_TOT_, sb_.nh4_sbl[sbl] / coated_nh4_sum);
        }
    }

    if ( cbm::flt_greater_zero( coated_nh4_sl_sum))
    {
        for( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
        {
            crnf_.update_crnf( MeTrX_get_wfps( sl), mc_temp_sl[sl], FTS_TOT_, sc_.coated_nh4_sl[sl] / coated_nh4_sum);
        }
    }

    double coated_nh4_release( crnf_.get_crnf_release( coated_nh4_sum));
    if ( !cbm::flt_greater_zero( coated_nh4_release))
    { return LDNDC_ERR_OK; }
    
    double const scale( coated_nh4_release / coated_nh4_sum);
    
    if ( cbm::flt_greater( scale, 0.99))
    {
        if ( cbm::flt_greater_zero( coated_nh4_sbl_sum))
        {
            for (size_t sbl = 0; sbl < sb_.surfacebulk_layer_cnt(); ++sbl)
            {
                sb_.nh4_sbl[sbl] += sb_.coated_nh4_sbl[sbl];
                sb_.coated_nh4_sbl[sbl] = 0.0;
            }
        }
        
        if ( cbm::flt_greater_zero( coated_nh4_sl_sum))
        {
            for( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
            {
                nh4_ae_sl[sl] += sc_.coated_nh4_sl[sl] * (1.0 - sc_.anvf_sl[sl]);
                nh4_an_sl[sl] += sc_.coated_nh4_sl[sl] * sc_.anvf_sl[sl];
                sc_.coated_nh4_sl[sl] = 0.0;
            }
        }
    }
    else
    {
        if ( cbm::flt_greater_zero( coated_nh4_sbl_sum))
        {
            for (size_t sbl = 0; sbl < sb_.surfacebulk_layer_cnt(); ++sbl)
            {
                if ( cbm::flt_greater_zero( sb_.coated_nh4_sbl[sbl]))
                {
                    double const n_transform( scale * sb_.coated_nh4_sbl[sbl]);
                    sb_.coated_nh4_sbl[sbl] -= n_transform;
                    sb_.nh4_sbl[sbl] += n_transform;
                }
            }
        }
        
        if ( cbm::flt_greater_zero( coated_nh4_sl_sum))
        {
            for( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
            {
                if ( cbm::flt_greater_zero( sc_.coated_nh4_sl[sl]))
                {
                    double const n_transform( scale * sc_.coated_nh4_sl[sl]);
                    sc_.coated_nh4_sl[sl] -= n_transform;
                    nh4_ae_sl[sl] += n_transform * (1.0 - sc_.anvf_sl[sl]);
                    nh4_an_sl[sl] += n_transform * sc_.anvf_sl[sl];
                }
            }
        }
    }

    return LDNDC_ERR_OK;
}



/*!
 * @brief
 *      Updates o2 and co2 concentrations in soil layers.
 *      There is no o2 mass conservation guaranteed because vegetation modules calculate
 *      autotrophic respiration without oxygen limitation. When oxygen demand in soil layers
 *      is not fullfilled o2_deff stores deficit and increases oxygen demand in following layers.
 */
lerr_t
SoilChemistryMeTrX::MeTrX_plant_respiration(size_t _ts)
{
    if ( this->m_veg->size() == 0)
    {
        return LDNDC_ERR_OK;
    }

    if ( _ts == 0)
    {
        double r_tot( 0.0);
        double mFrtSum( 0.0);
        for ( PlantIterator vt = this->m_veg->begin(); vt != this->m_veg->end(); ++vt)
        {
            MoBiLE_Plant *p = (*vt);
            mFrtSum += p->mFrt;
            r_tot += (timemode_ == TMODE_SUBDAILY) ? p->belowground_respiration() : p->d_belowground_respiration();
        }


        if ( cbm::flt_greater_zero( mFrtSum) &&
             cbm::flt_greater_zero( r_tot))
        {
            double const root_resp_ratio( r_tot / mFrtSum);
            for (size_t sl = 0; sl < sl_.soil_layer_cnt(); ++sl)
            {
                double root_co2_prod( 0.0);
                for ( PlantIterator vt = this->m_veg->begin(); vt != this->m_veg->end(); ++vt)
                {
                    MoBiLE_Plant *p = (*vt);
                    root_co2_prod += (p->fFrt_sl[sl] * p->mFrt);
                }

                double const o2_cons_tot( root_co2_prod * root_resp_ratio / MC_MO2_RATIO);
                plant_o2_consumption_sl[sl] = (o2_cons_tot / INTERNAL_TIME_STEPS);
            }
        }
        else
        {
            for (size_t sl = 0; sl < sl_.soil_layer_cnt(); ++sl)
            {
                plant_o2_consumption_sl = 0.0;
            }
        }
    }

    double o2_deff( 0.0);
    for( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
    {
        double const o2_tot( o2_gas_sl[sl] + o2_liq_sl[sl]);
        double const fact_o2( o2_tot / (KMM_O2_ROOT_RESPIRATION * sc_.h_sl[sl] + o2_tot));

        double const o2_avail( fact_o2 * o2_tot);
        if ( cbm::flt_greater_zero( o2_avail))
        {
            double const o2_demand( plant_o2_consumption_sl[sl] + o2_deff);
            double o2_cons( o2_demand);
            if ( cbm::flt_greater( o2_cons, o2_avail))
            {
                o2_cons = o2_avail;
                o2_deff = o2_demand - o2_avail;
            }
            else
            {
                o2_deff = 0.0;
            }

            double const co2_prod( plant_o2_consumption_sl[sl] * MC_MO2_RATIO);
            co2_gas_sl[sl] += co2_prod;
            co2_auto_sl[sl] += co2_prod;

            double const o2_gas_cons( o2_cons * o2_gas_sl[sl] / (o2_gas_sl[sl] + o2_liq_sl[sl]));
            double const o2_liq_cons( o2_cons * o2_liq_sl[sl] / (o2_gas_sl[sl] + o2_liq_sl[sl]));
            o2_gas_sl[sl] = cbm::bound_min( 0.0, o2_gas_sl[sl] - o2_gas_cons);
            o2_liq_sl[sl] = cbm::bound_min( 0.0, o2_liq_sl[sl] - o2_liq_cons);
        }
    }

    return LDNDC_ERR_OK;
}



/*!
 * @brief
 * Scales Michaelis-Menten constant according to spatial discretization
 */
double
SoilChemistryMeTrX::MeTrX_get_k_mm_x_sl(
                                        size_t const &_sl,
                                        double const &_k_mm)
{
    return (_k_mm * sc_.h_sl[_sl]);
}



/*!
 * @brief
 * All kind of different response functions
 */
double
SoilChemistryMeTrX::MeTrX_get_fact_tm_decomp( size_t const &_sl)
{
    double const fact_t( std::exp( -sipar_.METRX_F_DECOMP_T_EXP_1() *
                                   pow(1.0 - mc_temp_sl[_sl] / sipar_.METRX_F_DECOMP_T_EXP_2(), 2.0)));
    double const fact_m( ldndc::meteo::F_weibull( MeTrX_get_wfps_eff( _sl, 0.0),
                                                  sipar_.METRX_F_DECOMP_M_WEIBULL_1(),
                                                  sipar_.METRX_F_DECOMP_M_WEIBULL_2(),
                                                  1.0));
    return cbm::harmonic_mean2( fact_t, fact_m);
}



/*!
 * @brief
 *
 */
double
SoilChemistryMeTrX::MeTrX_get_fact_tm_mic( size_t const &_sl)
{
    double const fact_t( std::exp( -sipar_.METRX_F_MIC_T_EXP_1() *
                                   pow(1.0 - mc_temp_sl[_sl] / sipar_.METRX_F_MIC_T_EXP_2(), 2.0)));
    double const fact_m( ldndc::meteo::F_weibull( MeTrX_get_wfps_eff( _sl, 1.0),
                                                  sipar_.METRX_F_MIC_M_WEIBULL_1(),
                                                  sipar_.METRX_F_MIC_M_WEIBULL_2(),
                                                  1.0));
    return cbm::harmonic_mean2( fact_t, fact_m);
}



/*!
 * @brief
 *
 */
double
SoilChemistryMeTrX::MeTrX_get_fact_mm(
                                      double const &_conc,
                                      double const &_k)
{
    return (_conc / (_k + _conc));
}



/*!
 * @brief
 *
 */
double
SoilChemistryMeTrX::MeTrX_get_wfps_eff(
                                       unsigned int _sl,
                                       double const &_frac_wc_min)
{
    return cbm::bound(0.001, (wc_.wc_sl[_sl] - (_frac_wc_min*sc_.wcmin_sl[_sl])) / (sc_.wcmax_sl[_sl] - (_frac_wc_min*sc_.wcmin_sl[_sl])), 0.999);
}



/*!
 * @brief
 *
 */
void
SoilChemistryMeTrX::MeTrX_execute_co2_prod(
                                           size_t const &_sl,
                                           double const &_co2_prod,
                                           double &_c_source,
                                           double &_day_co2)
{
    if ( cbm::flt_greater_zero( o2_gas_sl[_sl]))
    {
        double const pot_c_resp( 0.9 * o2_gas_sl[_sl] * MC_MO2_RATIO);
        if ( cbm::flt_greater_equal( _co2_prod, pot_c_resp))
        {
            double const o2_cons( pot_c_resp / MC_MO2_RATIO);
            o2_gas_sl[_sl] -= o2_cons;
            _c_source -= pot_c_resp;

            // output
            _day_co2 += pot_c_resp;
            co2_gas_sl[_sl] += pot_c_resp;
            co2_hetero_sl[_sl] += pot_c_resp;
        }
        else if ( cbm::flt_greater_zero( pot_c_resp))
        {
            double const o2_cons_fact( _co2_prod / pot_c_resp);
            o2_gas_sl[_sl] -= (o2_gas_sl[_sl] * o2_cons_fact);
            _c_source -= _co2_prod;

            // output
            _day_co2 += _co2_prod;
            co2_gas_sl[_sl] += _co2_prod;
            co2_hetero_sl[_sl] += _co2_prod;
        }
    }
}



/*!
 * @brief
 *
 */
double
SoilChemistryMeTrX::MeTrX_get_n_growth(
                                       double const &_c_mic,
                                       double const &_n_mic,
                                       double const &_mic_act,
                                       double const &_fact_n,
                                       double const &_cn_opt)
{
    return (_c_mic * std::min(_mic_act * MUE_MAX_N_ASSI * _fact_n, 1.0) * (1.0/_cn_opt - _n_mic/_c_mic));
}



/*!
 * @brief
 *
 */
void
SoilChemistryMeTrX::MeTrX_dead_microbial_biomass_allocation(
                                                            size_t _sl,
                                                            double const &_c_trans,
                                                            double const &_n_trans,
                                                            double &_c_source,
                                                            double &_n_source)
{
    //dot_metrx_nitrogen n_micro_sl -> N_aorg_sl
    _c_source -= _c_trans;
    _n_source -= _n_trans;

    sc_.C_aorg_sl[_sl] += _c_trans;
    sc_.N_aorg_sl[_sl] += _n_trans;

    day_decay_c_mic_sl[_sl] += _c_trans;
    accumulated_n_mic_naorg_decay_sl[_sl] += _n_trans;
}



/*!
 * @brief
 *
 */
double
SoilChemistryMeTrX::MeTrX_get_mineral_efficiency(
                                                 double _cn,
                                                 double _cn_opt)
{
    if (_cn < 15)
    {
        return ((1.0/_cn_opt > 1.0/_cn) ?
                std::max(sipar_.METRX_BIOSYNTH_EFF() - (1.0/_cn_opt - 1.0/_cn), 0.0)
                : sipar_.METRX_BIOSYNTH_EFF());
    }
    else
    {
        return 0.0;
    }
}



/*!
 * @brief
 *
 */
double
SoilChemistryMeTrX::MeTrX_get_micro_c_decay_max(
                                                double &_c_micro)
{
    return cbm::bound_min( 0.0, _c_micro - MICRO_C_MIN);
}



/*!
 * @brief
 *
 */
double
SoilChemistryMeTrX::MeTrX_get_micro_n_decay_max(
                                                double &_n_micro)
{
    return cbm::bound_min( 0.0, _n_micro - MICRO_C_MIN / sipar_.METRX_CN_MIC_MIN());
}



/*!
 * @brief
 *
 */
void
SoilChemistryMeTrX::MeTrX_execute_n_assimilation(
                                                 double const &_pot_n_growth,
                                                 size_t const &_sl,
                                                 bool const &_aerob,
                                                 double &_n_mic_x,
                                                 double &_day_n_min,
                                                 double &_day_n_assi)
{
    //dot_metrx_nitrogen nh4_sl -> n_micro_sl [label="assi."];
    //dot_metrx_nitrogen don_sl -> n_micro_sl [label="assi."];
    //dot_metrx_nitrogen no3_sl -> n_micro_sl [label="assi."];
    //dot_metrx_nitrogen no2_sl -> n_micro_sl [label="assi."];
    if ( cbm::flt_greater_zero( _pot_n_growth))
    {
        /* Calculation of currently available N for assimilation */
        if ( _aerob)
        {
            double const n_avail_assi_ae( nh4_ae_sl[_sl] + no3_ae_sl[_sl] + don_ae_sl[_sl]);
            if ( cbm::flt_greater_zero( n_avail_assi_ae))
            {
                double const use_factor( std::min( _pot_n_growth / n_avail_assi_ae, 0.9));
                double const nh4_use( nh4_ae_sl[_sl] * use_factor);
                double const no3_use( no3_ae_sl[_sl] * use_factor);
                double const don_use( don_ae_sl[_sl] * use_factor);

                nh4_ae_sl[_sl] -= nh4_use;
                no3_ae_sl[_sl] -= no3_use;
                don_ae_sl[_sl] -= don_use;

                double const n_assi( nh4_use + no3_use + don_use);
                _n_mic_x += n_assi;

                // output
                _day_n_assi += n_assi;
                accumulated_n_nh4_assimilation_sl[_sl] += nh4_use;
                accumulated_n_no3_assimilation_sl[_sl] += no3_use;
                accumulated_n_don_assimilation_sl[_sl] += don_use;
            }
        }
        else
        {
            double const n_avail_assi_an( nh4_an_sl[_sl] + no3_an_sl[_sl] + don_an_sl[_sl]);
            if ( cbm::flt_greater_zero( n_avail_assi_an))
            {
                double const use_factor( std::min( _pot_n_growth / n_avail_assi_an, 0.9));
                double const an_nh4_use( nh4_an_sl[_sl] * use_factor);
                double const an_no3_use( no3_an_sl[_sl] * use_factor);
                double const an_don_use( don_an_sl[_sl] * use_factor);

                nh4_an_sl[_sl] -= an_nh4_use;
                no3_an_sl[_sl] -= an_no3_use;
                don_an_sl[_sl] -= an_don_use;

                double const n_assi( an_nh4_use + an_no3_use + an_don_use);
                _n_mic_x += n_assi;

                // output
                _day_n_assi += n_assi;
                accumulated_n_nh4_assimilation_sl[_sl] += an_nh4_use;
                accumulated_n_no3_assimilation_sl[_sl] += an_no3_use;
                accumulated_n_don_assimilation_sl[_sl] += an_don_use;
            }
        }
    }
    else
    {
        //dot_metrx_nitrogen n_micro_sl -> nh4_sl [label="mineral."];
        //dot_metrx_nitrogen n_micro_sl -> don_sl [label="mineral."];
        /* If microbial N is higher than current cn_opt suggests N-surplus is assumed to be mineralised to nh4 and released to soil */
        /* Note: _pot_n_growth < 0.0 */
        double const n_free( -1.0 * _pot_n_growth);
        if ( cbm::flt_greater_zero( n_free))
        {
            double const mineral_n( N_MINERALISATION_FRACTION * n_free);
            nh4_ae_sl[_sl] += mineral_n * (1.0 - sc_.anvf_sl[_sl]);
            nh4_an_sl[_sl] += mineral_n * sc_.anvf_sl[_sl];
            don_ae_sl[_sl] += (n_free - mineral_n) * (1.0 - sc_.anvf_sl[_sl]);
            don_an_sl[_sl] += (n_free - mineral_n) * sc_.anvf_sl[_sl];
            _n_mic_x -= n_free;

            // output
            accumulated_n_mic_don_dissolve_sl[_sl] += n_free - mineral_n;
            accumulated_n_mic_nh4_mineral_sl[_sl] += mineral_n;
            sc_.accumulated_n_mineral_sl[_sl] += mineral_n;
            _day_n_min += mineral_n;
        }
    }
}



/*!
 * @brief
 *
 */
double
SoilChemistryMeTrX::MeTrX_get_fact_ph(
                                      size_t _sl,
                                      double _ph_crit,
                                      double _ph_delta)
{
    return (1.0 - (1.0 / (1.0 + exp((sc_.ph_sl[_sl] - _ph_crit) / _ph_delta))));
}



/*!
 * @brief
 *
 */
lerr_t
SoilChemistryMeTrX::MeTrX_physics()
{
    /***************/
    /* Water table */
    /***************/
    
    if ( (wc_.surface_water > 0.01) &&
        (sb_.surfacebulk_layer_cnt() > 0))
    {
        have_water_table = true;
        
        // for simplicity of calculations changing watertable
        // alters heigth of single waterlayer and not number of layers
        h_wl = (wc_.surface_water / (double)sb_.surfacebulk_layer_cnt());
    }
    else
    {
        have_water_table = false;
        h_wl = 0.0;
    }
    
    
    if ( have_water_table == false)
    {
        double const clear_ch4( sb_.ch4_sbl.sum());
        sb_.ch4_sbl = 0.0;
        double const clear_o2( sb_.o2_sbl.sum());
        sb_.o2_sbl = 0.0;
        double const clear_nh4( sb_.nh4_sbl.sum());
        sb_.nh4_sbl = 0.0;
        double const clear_nh3( sb_.nh3_sbl.sum());
        sb_.nh3_sbl = 0.0;
        double const clear_urea( sb_.urea_sbl.sum());
        sb_.urea_sbl = 0.0;
        double const clear_no3( sb_.no3_sbl.sum());
        sb_.no3_sbl = 0.0;
        double const clear_don( sb_.don_sbl.sum());
        sb_.don_sbl = 0.0;
        double const clear_doc( sb_.doc_sbl.sum());
        sb_.doc_sbl = 0.0;
        double const clear_co2( sb_.co2_sbl.sum());
        sb_.co2_sbl = 0.0;
        double const clear_so4( sb_.so4_sbl.sum());
        sb_.so4_sbl = 0.0;

        for( size_t  sl = 0;  sl <= SL_SURFACE_DISTRIBUTION;  ++sl)
        {
            ch4_liq_sl[sl] += clear_ch4 * SL_SURFACE_DISTRIBUTION_INVERSE;
            o2_liq_sl[sl] += clear_o2 * SL_SURFACE_DISTRIBUTION_INVERSE;
            nh4_ae_sl[sl] += clear_nh4 * SL_SURFACE_DISTRIBUTION_INVERSE;
            sc_.nh3_liq_sl[sl] += clear_nh3 * SL_SURFACE_DISTRIBUTION_INVERSE;
            sc_.urea_sl[sl] += clear_urea * SL_SURFACE_DISTRIBUTION_INVERSE;
            no3_ae_sl[sl] += clear_no3 * SL_SURFACE_DISTRIBUTION_INVERSE;
            don_ae_sl[sl] += clear_don * SL_SURFACE_DISTRIBUTION_INVERSE;
            sc_.doc_sl[sl] += clear_doc * SL_SURFACE_DISTRIBUTION_INVERSE;
            co2_liq_sl[sl] += clear_co2 * SL_SURFACE_DISTRIBUTION_INVERSE;
            sc_.so4_sl[sl] += clear_so4 * SL_SURFACE_DISTRIBUTION_INVERSE;
            
            accumulated_n_nh4_infiltration_phys_sl[sl] += clear_nh4 * SL_SURFACE_DISTRIBUTION_INVERSE;
            accumulated_n_nh3_infiltration_phys_sl[sl] += clear_nh3 * SL_SURFACE_DISTRIBUTION_INVERSE;
            accumulated_n_urea_infiltration_phys_sl[sl] += clear_urea * SL_SURFACE_DISTRIBUTION_INVERSE;
            accumulated_n_no3_infiltration_phys_sl[sl] += clear_no3 * SL_SURFACE_DISTRIBUTION_INVERSE;
            accumulated_n_don_infiltration_phys_sl[sl] += clear_don * SL_SURFACE_DISTRIBUTION_INVERSE;
        }
    }

    /*****************************/
    /* Water-, Ice-, Air-Content */
    /*****************************/

    for (size_t sl = 0; sl < sl_.soil_layer_cnt(); ++sl)
    {
        double const v_pores( sc_.h_sl[sl] * sc_.poro_sl[sl]);
        v_water_sl[sl] = cbm::bound_min( 0.01 * v_pores, wc_.wc_sl[sl] * sc_.h_sl[sl]);
        v_air_sl[sl] = cbm::bound_min( 0.05 * v_pores, v_pores - v_water_sl[sl] - v_ice_sl[sl]);
        v_ice_sl[sl] = cbm::bound_min( 0.0, wc_.ice_sl[sl] * sc_.h_sl[sl]);

        pore_connectivity_sl[sl] = cbm::bound(
                          0.001,
                          1.0 - exp( -sipar_.METRX_F_CONNECTIVITY_2() *
                                    ((v_air_sl[sl] / sc_.h_sl[sl]) - sipar_.METRX_F_CONNECTIVITY_1())),
                          0.999);
    }

    size_t sl_max( sl_.soil_layer_cnt()-1);
    for (size_t sl = 0; sl < sl_max; ++sl)
    {
        interface_air_sl[sl] = cbm::harmonic_mean2( v_air_sl[sl]/sc_.h_sl[sl], v_air_sl[sl+1]/sc_.h_sl[sl+1]);
        interface_delta_x_air_sl[sl] = interface_air_sl[sl] / delta_x[sl];
        interface_delta_x_water_sl[sl] = cbm::harmonic_mean2( v_water_sl[sl]/sc_.h_sl[sl], v_water_sl[sl+1]/sc_.h_sl[sl+1]) / delta_x[sl];
        interface_delta_x_soil_sl[sl] = 1.0;
    }
    interface_air_sl[sl_max] = v_air_sl[sl_max]/sc_.h_sl[sl_max];
    interface_delta_x_air_sl[sl_max] = interface_air_sl[sl_max] / delta_x[sl_max];
    interface_delta_x_water_sl[sl_max] = v_water_sl[sl_max]/sc_.h_sl[sl_max] / delta_x[sl_max];
    interface_delta_x_soil_sl[sl_max] = 1.0;


    double interface_delta_x_atm_soil( 0.0);
    double interface_delta_x_fl_soil( 0.0);
    double interface_delta_x_sbl_soil( 0.0);

    if ( have_water_table)
    {
        interface_delta_x_sbl_soil = (cbm::harmonic_mean2( 1.0, v_water_sl[0]/sc_.h_sl[0]) / cbm::arithmetic_mean2( h_wl, sc_.h_sl[0]));
    }
    else
    {
        /* Set atmospheric Dirichlet boundary condition at ground level */
        interface_delta_x_atm_soil = (cbm::harmonic_mean2( 1.0, v_air_sl[0]/sc_.h_sl[0]) / (0.5 * sc_.h_sl[0]));
        interface_delta_x_fl_soil  = (cbm::harmonic_mean2( 1.0, v_air_sl[0]/sc_.h_sl[0]) / cbm::arithmetic_mean2( ph_.h_fl[0], sc_.h_sl[0]));
    }



    /************************************/
    /* Effective Diffusion Coefficients */
    /************************************/

    /* Plant mediated diffusion */

    for ( PlantIterator vt = this->m_veg->begin(); vt != this->m_veg->end(); ++vt)
    {
        MoBiLE_Plant *p = *vt;
        if ( cbm::flt_greater_zero( p->root_tc))
        {
            for( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
            {
                D_eff_plant_sl[sl] = (p->root_tc * p->fFrt_sl[sl]);
            }
            have_plant_diffusion = true;
        }
        else
        {
            for( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
            {
                D_eff_plant_sl[sl] = 0.0;
            }
            have_plant_diffusion = false;
        }
    }


    /* Soil diffusion */

    /* Effective diffusion coefficients relates to the tortuosity of the soil matrix.
     * Formular is taken from Lai, S.H., J.M. Tiedje, and A.E. Erickson. 1976.
     * In situ measurement of gas diffusion coefficient in soils, Soil Sci. Soc. Am. J., 40:3–6
     *      tortuosity = theta^(7/3)
     *      theta refers to soil gas content: volume_gas / volume_soil
     *
     * Stone content is added to air volume in order to derive characteristic gas content for soil matrix
     * only, i.e., excluding the stone fraction of the soil. Stone fractions are implicitly incorporated into
     * the gaseous diffusion  by the reduction of the diffusive interface area between soil layers
     */
    if ( d_eff_method == "parameter")
    {
        for (size_t sl = 0; sl < sl_.soil_layer_cnt(); ++sl)
        {
            double const air_content( v_air_sl[sl] / sc_.h_sl[sl]);
            double const d_eff( d_eff_air_a( 7.0/3.0, air_content)
                                * pow((mc_temp_sl[sl] + cbm::D_IN_K) / TNORM, sipar_.TEXP()))    ;
            
            /* Empirical reduction of gaseous diffsuion */
            D_eff_air_sl[sl] = cbm::bound_max( sipar_.METRX_D_EFF_REDUCTION() * d_eff * sc_.till_effect_sl[sl], 1.0);
        }
    }
    else if ( d_eff_method == "millington_and_quirk_1961")
    {
        for (size_t sl = 0; sl < sl_.soil_layer_cnt(); ++sl)
        {
            double const air_content( v_air_sl[sl] / sc_.h_sl[sl]);
            double const d_eff( d_eff_air_millington_and_quirk_1961( air_content, sc_.poro_sl[sl])
                                * pow((mc_temp_sl[sl] + cbm::D_IN_K) / TNORM, sipar_.TEXP()))    ;
            
            /* Empirical reduction of gaseous diffsuion */
            D_eff_air_sl[sl] = cbm::bound_max( sipar_.METRX_D_EFF_REDUCTION() * d_eff * sc_.till_effect_sl[sl], 1.0);
        }
    }
    else
    {
        KLOGERROR("invalid effective diffusion approach: [", d_eff_method,"]");
        return LDNDC_ERR_FAIL;
    }

    for (size_t sl = 0; sl < sl_.soil_layer_cnt(); ++sl)
    {
        /* Preliminary set to any value */
        D_eff_water_sl[sl] = pow( MeTrX_get_wfps( sl), 2.0);
        D_eff_dailyturbation_sl[sl] = std::exp( - sc_.depth_sl[sl] / 0.28) * 5.0e-4 / 365.0;
    }

    /*******************************/
    /* Update transport properties */
    /*******************************/

    transport_.update_diffusion( D_eff_plant_sl, D_eff_air_sl,
                                 v_air_sl,
                                 soil_gas_advection_sl,
                                 interface_delta_x_air_sl,
                                 interface_delta_x_atm_soil,
                                 interface_delta_x_fl_soil,
                                 interface_delta_x_sbl_soil,
                                 h_wl,
                                 have_water_table);
    transport_.update_advection( interface_air_sl);

    double const d_wind_scale( 1.0 + (*mc_wind) / ((*mc_wind) + 5.0) * 2.0);
    transport_.wind_stimulation = d_wind_scale;

    for ( std::map< std::string , double>::iterator it = sc_.n_subdivision.begin();
          it != sc_.n_subdivision.end(); )
    {
        it->second -= 0.02 * FTS_TOT_;
        if ( !cbm::flt_greater_zero( it->second))
        {
            sc_.n_subdivision.erase( it++);
        }
        else{ ++it; }
    }

    return LDNDC_ERR_OK;
}



lerr_t
SoilChemistryMeTrX::MeTrX_advection()
{
    size_t const sub_steps( 30);
    double const permeability( 1.0e-11);

    double k_gas( permeability / cbm::VISCOSITY_AIR * cbm::SEC_IN_DAY * FTS_TOT_ / sub_steps);
    for ( size_t s = 0; s < sub_steps; ++s)
    {
        bool end_it( true);
        for ( size_t sl = sl_.soil_layer_cnt()-1; sl > 0; --sl)
        {
            double fact_uc_down( cbm::RGAS * (mc_temp_sl[sl] + cbm::D_IN_K) / v_air_sl[sl] * cbm::PA_IN_BAR);
            double fact_uc_up( cbm::RGAS * (mc_temp_sl[sl-1] + cbm::D_IN_K) / v_air_sl[sl-1] * cbm::PA_IN_BAR);
            double p_down( (o2_gas_sl[sl] / M_2O_SCALE + n2_gas_sl[sl] / M_2N_SCALE) * fact_uc_down);
            double p_up( (o2_gas_sl[sl-1] / M_2O_SCALE + n2_gas_sl[sl-1] / M_2N_SCALE) * fact_uc_up);

            if ( cbm::flt_greater( p_down, p_up))
            {
                end_it = false;
                double const flux( cbm::bound_max( 1.0 / sc_.h_sl[sl] * k_gas * (1.0 - D_eff_air_sl[sl]) * (p_down - p_up) / (0.5 * (sc_.h_sl[sl] + sc_.h_sl[sl-1])), 0.1));

                double const trans_o2(  flux * o2_gas_sl[sl]);
                double const trans_n2( flux * n2_gas_sl[sl]);
                double const trans_nh3( flux * sc_.nh3_gas_sl[sl]);
                double const trans_n2o( flux * n2o_gas_sl[sl]);
                double const trans_an_n2o( flux * an_n2o_gas_sl[sl]);
                double const trans_no( flux * no_gas_sl[sl]);
                double const trans_an_no( flux * an_no_gas_sl[sl]);
                double const trans_ch4( flux * ch4_gas_sl[sl]);
                double const trans_co2( flux * co2_gas_sl[sl]);

                o2_gas_sl[sl] -= trans_o2;
                n2_gas_sl[sl] -= trans_n2;
                sc_.nh3_gas_sl[sl] -= trans_nh3;
                n2o_gas_sl[sl] -= trans_n2o;
                an_n2o_gas_sl[sl] -= trans_an_n2o;
                no_gas_sl[sl] -= trans_no;
                an_no_gas_sl[sl] -= trans_an_no;
                ch4_gas_sl[sl] -= trans_ch4;
                co2_gas_sl[sl] -= trans_co2;

                o2_gas_sl[sl-1] += trans_o2;
                n2_gas_sl[sl-1] += trans_n2;
                sc_.nh3_gas_sl[sl-1] += trans_nh3;
                n2o_gas_sl[sl-1] += trans_n2o;
                an_n2o_gas_sl[sl-1] += trans_an_n2o;
                no_gas_sl[sl-1] += trans_no;
                an_no_gas_sl[sl-1] += trans_an_no;
                ch4_gas_sl[sl-1] += trans_ch4;
                co2_gas_sl[sl-1] += trans_co2;

                accumulated_n_n2o_phys_diffusion_sl[sl] += trans_n2o + trans_an_n2o;
                accumulated_n_no_phys_diffusion_sl[sl] += trans_no + trans_an_no;
                accumulated_n_nh3_phys_diffusion_sl[sl] += trans_nh3;
            }
        }

        if ( !have_water_table)
        {
            double fact_uc( cbm::RGAS * (mc_temp_sl[0] + cbm::D_IN_K) / v_air_sl[0] * cbm::PA_IN_BAR);
            double p_down( (o2_gas_sl[0] / M_2O_SCALE + n2_gas_sl[0] / M_2N_SCALE) * fact_uc);
            double p_up( (*o2_concentration) + cbm::PN2);

            if ( cbm::flt_greater( p_down, p_up))
            {
                end_it = false;
                double const flux( cbm::bound_max( 1.0 / sc_.h_sl[0] * k_gas * (1.0 - D_eff_air_sl[0]) * (p_down - p_up) / (0.5 * sc_.h_sl[0]), 0.1));

                double const trans_o2( flux * o2_gas_sl[0]);
                double const trans_n2( flux * n2_gas_sl[0]);
                double const trans_nh3( flux * sc_.nh3_gas_sl[0]);
                double const trans_n2o( flux * n2o_gas_sl[0]);
                double const trans_an_n2o( flux * an_n2o_gas_sl[0]);
                double const trans_no( flux * no_gas_sl[0]);
                double const trans_an_no( flux * an_no_gas_sl[0]);
                double const trans_ch4( flux * ch4_gas_sl[0]);
                double const trans_co2( flux * co2_gas_sl[0]);

                o2_gas_sl[0] -= trans_o2;
                n2_gas_sl[0] -= trans_n2;
                sc_.nh3_gas_sl[0] -= trans_nh3;
                n2o_gas_sl[0] -= trans_n2o;
                an_n2o_gas_sl[0] -= trans_an_n2o;
                no_gas_sl[0] -= trans_no;
                an_no_gas_sl[0] -= trans_an_no;
                ch4_gas_sl[0] -= trans_ch4;
                co2_gas_sl[0] -= trans_co2;

                subdaily_o2_soil[subdaily_time_step_] += trans_o2;
                subdaily_nh3_soil[subdaily_time_step_] += trans_nh3;
                subdaily_n2o_soil[subdaily_time_step_] += trans_n2o + trans_an_n2o;
                subdaily_no_soil[subdaily_time_step_] += trans_no + trans_an_no;
                subdaily_ch4_soil[subdaily_time_step_] += trans_ch4;
                subdaily_co2_soil[subdaily_time_step_] += trans_co2;

                accumulated_n_n2o_phys_diffusion_sl[0] += trans_n2o + trans_an_n2o;
                accumulated_n_no_phys_diffusion_sl[0] += trans_no + trans_an_no;
                accumulated_n_nh3_phys_diffusion_sl[0] += trans_nh3;
            }
        }

        if ( end_it)
        {
            break;
        }
    }

    return LDNDC_ERR_OK;
}



/*!
 * @brief
 *  Returns dynamic pH-value of given soil layer.
 */
double
SoilChemistryMeTrX::MeTrX_get_ph_sl(
                                 size_t _sl)
{
    return cbm::bound( sc_.phi_sl[_sl], sc_.phi_sl[_sl] + ph_delta_urea_sl[_sl], PH_MAX);
}


double
SoilChemistryMeTrX::MeTrX_get_ph_wl()
{
    if ( cbm::flt_greater_zero( wc_.surface_water))
    {
        return cbm::bound( PH_SURFACE_WATER, PH_SURFACE_WATER + ph_delta_pab_wl + ph_delta_urea_wl, PH_MAX);
    }
    else
    {
        return 7.0;
    }
}



/*!
 * @page metrx
 * @section metrx_urea_hydrolysis Urea hydrolysis
 * Urea from fertilizer or manure is dissolved to ammonium in soil and/or in the surface water table depending on temperature:
 * \f{eqnarray*}{
 * \frac{d NH_4^+}{dT}  = N_{urea} \phi_T \\
 * \frac{d N_{urea}}{dT}  = - \frac{d NH_4^+}{dT}
 * \f}
 * Depending on the rate of urea hydrolysis, the pH value from of the respective soil layer or water table layer is increased:
 * \f[
 * pH = pH_i + \frac{\frac{d N_{urea}}{dT}}{METRX\_KMM\_PH\_INCREASE\_FROM\_UREA}
 * \f]
 * with \f$ p_i \f$ representing the basi pH value that has been defined as model input.
 */
void
SoilChemistryMeTrX::MeTrX_urea_hydrolysis()
{
    if ( have_water_table)
    {
        double nh4_prod_sum( 0.0);
        for (size_t wl = 0; wl < sb_.surfacebulk_layer_cnt(); wl++)
        {
            double const fact_t( 0.25 * exp(0.0693 * mc_temp_sl[0]));
            double const nh4_prod( sb_.urea_sbl[wl]
                          * cbm::bound( 0.0,
                                        1.0 - exp( -fact_t * 0.02),
                                        0.9));
            sb_.urea_sbl[wl] -= nh4_prod;
            sb_.nh4_sbl[wl] += nh4_prod;
            accumulated_n_urea_nh4_hydrolysis_sbl[0] += nh4_prod;
            nh4_prod_sum += nh4_prod;
        }
        ph_delta_urea_wl = cbm::bound( 0.0, nh4_prod_sum / (sipar_.METRX_KMM_PH_INCREASE_FROM_UREA() * wc_.surface_water), PH_MAX-PH_SURFACE_WATER);
    }

    for ( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
    {
        if ( cbm::flt_greater_zero( sc_.urea_sl[sl]))
        {
            double const fact_t( 0.25 * exp(0.0693 * mc_temp_sl[sl]));
            double const nh4_prod( sc_.urea_sl[sl]
                          * cbm::bound( 0.0,
                                        1.0 - exp( -fact_t * 0.02),
                                        0.9));

            sc_.urea_sl[sl] -= nh4_prod;
            nh4_ae_sl[sl] += nh4_prod * (1.0 - sc_.anvf_sl[sl]);
            nh4_an_sl[sl] += nh4_prod * sc_.anvf_sl[sl];
            accumulated_n_urea_nh4_hydrolysis_sl[sl] += nh4_prod;
            ph_delta_urea_sl[sl] = cbm::bound( 0.0, nh4_prod / (sipar_.METRX_KMM_PH_INCREASE_FROM_UREA() * sc_.h_sl[sl]), PH_MAX-sc_.phi_sl[sl]);
        }
        else
        {
            sc_.urea_sl[sl] = 0.0;
            ph_delta_urea_sl[sl] = 0.0;
        }
    }
}



void
SoilChemistryMeTrX::MeTrX_pH_calculation()
{
    for ( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
    {
        sc_.ph_sl[sl] = MeTrX_get_ph_sl( sl);
    }
}



/*!
 * @page metrx
 * @section metrx_clay_nh4_equilibrium Ammonium adsorption
 * Ammonium is partly adsorped at the soil surface depending on clay content.
 */
void
SoilChemistryMeTrX::MeTrX_clay_nh4_equilibrium()
{
    for( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
    {
        /* partitioning coefficient */
        double const k_nh4( 1.0);

        /* total mount of n-nh4 [1e3:g] */
        double const nh4_sol_old( nh4_ae_sl[sl] + nh4_an_sl[sl]);
        double const nh4_total( nh4_sol_old + sc_.clay_nh4_sl[sl]);

        /* soil mass [1e3:g] */
        double const m_soil( sc_.get_soil_mass( sl));

        /* soil organic matter content [%] */
        double const som_percentage( sc_.som_sl[sl] / m_soil * 100.0);

        /* cation exchange capacity [1e-2:mol] */
        double const cec( cation_exchange_capacity(
                                                   sc_.clay_sl[sl] * 100.0,
                                                   sc_.sand_sl[sl] * 100.0,
                                                   som_percentage,
                                                   sc_.ph_sl[sl],
                                                   ECOSYS_GRASSLAND) * m_soil);

        /* n-h4 exchange capacity [kg] */
        double const nh4_capacity( cec * cbm::MN * 1.0e-5);

        /* nh4 per soil mass [1e-5:mol:g] */
        double const nh4_total_percentage( nh4_total * 1.0e5 / (cbm::MN * m_soil));

        /* adsorbed nh4 percentage [%] */
        double const nh4_clay_percentage( k_nh4 * nh4_total_percentage);

        /* adsorbed nh4  */
        double const nh4_clay( cbm::bound_max( nh4_clay_percentage * 1.0e-2 * nh4_capacity, nh4_total));

        /* nh4 - clay_nh4 exchange */
        double const delta_nh4( (nh4_clay - sc_.clay_nh4_sl[sl]) * FTS_TOT_);

        sc_.clay_nh4_sl[sl] += delta_nh4;
        double const nh4_sol_new( nh4_total - sc_.clay_nh4_sl[sl]);
        if ( cbm::flt_greater_zero( nh4_sol_old))
        {
            nh4_ae_sl[sl] = nh4_ae_sl[sl] / nh4_sol_old * nh4_sol_new;
            nh4_an_sl[sl] = nh4_sol_new - nh4_ae_sl[sl];
        }
        else
        {
            nh4_ae_sl[sl] = nh4_sol_new * (1.0 - sc_.anvf_sl[sl]);
            nh4_an_sl[sl] = nh4_sol_new * sc_.anvf_sl[sl];
        }
    }
}



/*!
 * @brief
 *
 */
void
SoilChemistryMeTrX::MeTrX_nh3_nh4_equilibrium()
{
    if ( have_water_table)
    {
        double ph( MeTrX_get_ph_wl());
        for ( size_t wl = 0; wl < sb_.surfacebulk_layer_cnt(); wl++)
        {
            double const k_a( pow( 10.0,(-2728.3 / (mc_temp_sl[0] + cbm::D_IN_K) - 0.094219)));  // Sadeghi et al. (1988) (for 5 < ph < 10)

            double const n_tot( sb_.nh4_sbl[wl] + sb_.nh3_sbl[wl]);
            double const frac_nh3( cbm::bound_max( 1.0 / (1.0 + (pow(10.0, - ph) / k_a)), 0.99));

            double const delta_nh3( frac_nh3 * n_tot - sb_.nh3_sbl[wl]);
            if( cbm::flt_greater( delta_nh3, 0.0))
            {
                sb_.nh3_sbl[wl] += delta_nh3;
                sb_.nh4_sbl[wl] = cbm::bound_min( 0.0, sb_.nh4_sbl[wl] - delta_nh3);
                accumulated_n_nh4_nh3_conversion_sbl[wl] += delta_nh3;
            }
            else if( cbm::flt_less( delta_nh3, 0.0))
            {
                sb_.nh3_sbl[wl] = cbm::bound_min( 0.0, sb_.nh3_sbl[wl] + delta_nh3);
                sb_.nh4_sbl[wl] -= delta_nh3;
                accumulated_n_nh3_nh4_conversion_sbl[wl] -= delta_nh3;
            }
        }
    }


    for( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
    {
        // Sadeghi et al. (1988) (for 5 < ph < 10)
        double const k_a( pow( 10.0,(-2728.3 / (mc_temp_sl[sl] + cbm::D_IN_K) - 0.094219)));
        double const nh4_tot( nh4_ae_sl[sl] + nh4_an_sl[sl]);
        double const n_tot( nh4_tot + sc_.nh3_liq_sl[sl]);
        double const frac_nh3( cbm::bound_max( 1.0 / (1.0 + (pow(10.0, - sc_.ph_sl[sl]) / k_a)), 0.99));

        double const delta_nh3( frac_nh3 * n_tot - sc_.nh3_liq_sl[sl]);
        if ( cbm::flt_greater( delta_nh3, 0.0))
        {
            sc_.nh3_liq_sl[sl] += delta_nh3;
            double const delta_nh4_ae( nh4_ae_sl[sl] / nh4_tot * delta_nh3);
            double const delta_nh4_an( delta_nh3 - delta_nh4_ae);
            nh4_ae_sl[sl] = cbm::bound_min( 0.0, nh4_ae_sl[sl] - delta_nh4_ae);
            nh4_an_sl[sl] = cbm::bound_min( 0.0, nh4_an_sl[sl] - delta_nh4_an);
            accumulated_n_nh4_nh3_conversion_sl[sl] += delta_nh3;
        }
        else if ( cbm::flt_less( delta_nh3, 0.0))
        {
            sc_.nh3_liq_sl[sl] = cbm::bound_min( 0.0, sc_.nh3_liq_sl[sl] + delta_nh3);
            nh4_ae_sl[sl] -= delta_nh3 * (1.0 - sc_.anvf_sl[sl]);
            nh4_an_sl[sl] -= delta_nh3 * sc_.anvf_sl[sl];
            accumulated_n_nh3_nh4_conversion_sl[sl] -= delta_nh3;
        }
    }
}



/*!
 * @brief
 *
 */
double
SoilChemistryMeTrX::MeTrX_get_atm_eq_liq(
                                         atm_eq_liq const &_atm_eq_liq,
                                         double const &_vol)
{
    switch (_atm_eq_liq)
    {
        case nh3_atm_eq_liq:
        {
            return nh3_fl[0] * get_henry_coefficient(*mc_temp_atm, henry_nh3) * cbm::BAR_IN_PA * cbm::PPM * M_N_SCALE * _vol;
        }
        case o2_atm_eq_liq:
        {
            return ( cbm::PO2 * get_henry_coefficient(*mc_temp_atm, henry_o2) * cbm::BAR_IN_PA * M_2O_SCALE * _vol);
        }
        case co2_atm_eq_liq:
        {
            return ts_co2_concentration_fl[0] * get_henry_coefficient(*mc_temp_atm, henry_co2) * cbm::BAR_IN_PA * cbm::PPM * M_C_SCALE * _vol;
        }
        case ch4_atm_eq_liq:
        {
            return ts_ch4_concentration_fl[0] * get_henry_coefficient(*mc_temp_atm, henry_ch4) * cbm::BAR_IN_PA * cbm::PPM * M_C_SCALE * _vol;
        }
        case n2o_atm_eq_liq:
        {
            return cbm::PN2O * get_henry_coefficient(*mc_temp_atm, henry_n2o) * cbm::BAR_IN_PA * M_2N_SCALE * _vol;
        }
        case no_atm_eq_liq:
        {
            return ts_no_concentration_fl[0] * get_henry_coefficient(*mc_temp_atm, henry_no) * cbm::BAR_IN_PA * M_N_SCALE * _vol;
        }
        default:
        {
            KLOGFATAL("atm_eq_liq type not supported");
            return invalid_dbl;
        }
    }
}


} /*namespace ldndc*/
