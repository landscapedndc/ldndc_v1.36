/*!
 * @file
 * @author
 * - David Kraus
 */

#include  "soilchemistry/metrx/output-soilchemistry-metrx-daily.h"


LMOD_MODULE_INFO(OutputSoilchemistryMeTrXDaily,TMODE_POST_DAILY,LMOD_FLAG_USER|LMOD_FLAG_OUTPUT);
namespace ldndc {

/*!
 * @page metrx
 * @section metrx_output MeTrx Output
 * @subsection metrx_output_daily MeTrx output (daily)
 * entity name | decription | unit
 * ----------- | ---------- | ----
 * anvf | Anaerobic volume fraction averaged over the complete soil profile | [-]
 * pore_connectivity | Pore connectivity averaged over the complete soil profile | [-]
 * permeability | Permeability averaged over the complete soil profile | [-]
 * wfps | Water filled pore space averaged over the complete soil profile | [-]
 * air | Water filled pore space averaged over the complete soil profile | [-]
 * root_conductivity | Root conductivity averaged over the complete soil profile | [m2s-1]
 * o2 | Sum of oygen mass  over the complete soil profile | [kgha-1]
 * t_eff_crnf | ... | [oC]
 * n_release_fraction_crnf | ... | [-]
 * nitrification_inhibition | Inhibition of nitrification | [-]
 * urease_inhibition | Inhibition of urease hydrolysis | [-]
 * carbon_use_efficiency | Carbon use efficiency | [-]
 * N_don | Sum of dissolved organic nitrogen mass  over the complete soil profile | [kgha-1]
 * N_nh4 | Sum of ammonium mass  over the complete soil profile | [kgha-1]
 * N_nh4_clay | Sum of ammonium adsorbed to clay minerals mass  over the complete soil profile | [kgha-1]
 * N_urea | Sum of urea mass  over the complete soil profile | [kgha-1]
 * N_no2_ae | Sum of aerobic NO2 mass  over the complete soil profile | [kgha-1]
 * N_no2_an | Sum of anaerobic NO2 mass  over the complete soil profile | [kgha-1]
 * N_no3_ae | Sum of aerobic NO3 mass  over the complete soil profile | [kgha-1]
 * N_no3_an | Sum of anaerobic NO3 mass  over the complete soil profile | [kgha-1]
 * N_n2o_ae | Sum of aerobic N2O mass  over the complete soil profile | [kgha-1]
 * N_n2o_an | Sum of anaerobic N2O mass  over the complete soil profile | [kgha-1]
 * N_no_ae | Sum of aerobic NO mass  over the complete soil profile | [kgha-1]
 * N_no_an | Sum of anaerobic NO mass  over the complete soil profile | [kgha-1]
 * N_nh3_gas | Sum of gaseous ammonia mass  over the complete soil profile | [kgha-1]
 * N_nh3_liq | Sum of dissolved ammonia mass  over the complete soil profile | [kgha-1]
 * C_doc_ae | Dissolved organic carbon (aerobic soil volume) | [kgCha-1]
 * C_doc_an | Dissolved organic carbon (anaerobic soil volume) | [kgCha-1]
 * C_acetate | Acetate | [kgCha-1]
 * C_ch4_gas | Methane in gas phase | [kgCha-1]
 * C_ch4_liq | Dissolved methane | [kgCha-1]
 * C_aorg | Amount of carbon in active organic material pool | [kgCha-1]
 * N_aorg | Amount of carbon in active organic material pool | [kgNha-1]
 * C_micro_1 | Amount of carbon in microbial pool 1 | [kgCha-1]
 * C_micro_2 | Amount of carbon in microbial pool 2 | [kgCha-1]
 * C_micro_3 | Amount of carbon in microbial pool 3 | [kgCha-1]
 * N_micro_1 | Amount of nitrogen in microbial pool 1 | [kgNha-1]
 * N_micro_2 | Amount of nitrogen in microbial pool 2 | [kgNha-1]
 * N_micro_3 | Amount of nitrogen in microbial pool 3 | [kgNha-1]
 * C_humus_1 | Amount of carbon in humus pool 1 | [kgCha-1]
 * C_humus_2 | Amount of carbon in humus pool 2 | [kgCha-1]
 * C_humus_3 | Amount of carbon in humus pool 3 | [kgCha-1]
 * N_humus_1 | Amount of nitrogen in humus pool 1 | [kgNha-1]
 * N_humus_2 | Amount of nitrogen in humus pool 2 | [kgNha-1]
 * N_humus_3 | Amount of nitrogen in humus pool 3 | [kgNha-1]
 * C_litter_soil_1 | Amount of carbon in soil litter pool 1 | [kgCha-1]
 * C_litter_soil_2 | Amount of carbon in soil litter pool 2 | [kgCha-1]
 * C_litter_soil_3 | Amount of carbon in soil litter pool 3 | [kgCha-1]
 * N_litter_soil_1 | Amount of nitrogen in soil litter pool 1 | [kgNha-1]
 * N_litter_soil_2 | Amount of nitrogen in soil litter pool 2 | [kgNha-1]
 * N_litter_soil_3 | Amount of nitrogen in soil litter pool 3 | [kgNha-1]
 * C_algae | Amount of carbon in algae pool | [kgCha-1]
 * N_algae | Amount of nitrogen in algae pool | [kgNha-1]
 * C_litter_stubble | Amount of nitrogen in stubble pool | [kgCha-1]
 * N_litter_stubble | Amount of nitrogen in stubble pool | [kgNha-1]
 * C_total | Total soil organic carbon | [kgCha-1]
 * N_total | Total soil organic nitrogen | [kgNha-1]
 * soc_20cm | Soil organic carbon content in the top 20 cm soil horizon | [%]
 * soc_40cm | Soil organic carbon content in the top 40 cm soil horizon | [%]
 * totn_20cm | Total nitrogen content in the top 20 cm soil horizon | [%]
 * totn_40cm | Total nitrogen content in the top 40 cm soil horizon | [%]
 * fe2_tot | ... | []
 * fe3_tot | ... | []
 * till_fact | ... | []
 * freeze_thaw_fact | ... | []
 * litter_height | ... | [m]
 * ph_watertable | ... | []
 * ph_soil_surface | ... | []
 * dN_no3_groundwater | ... | []
 * dN_assi | ... | []
 * dN_nit_nh4_no2 | ... | []
 * dN_nit_no2_no3 | ... | []
 * dN_nit_no2_no | ... | []
 * dN_nit_no2_n2o | ... | []
 * dN_denit_no3_no2 | ... | []
 * dN_denit_no2_no | ... | []
 * dN_denit_no2_n2o | ... | []
 * dN_denit_no2_n2 | ... | []
 * dN_denit_no_n2o | ... | []
 * dN_denit_no_n2 | ... | []
 * dN_denit_n2o_n2 | ... | []
 * dN_chemodenit_no2_no | ... | []
 * dC_ch4_oxidation | ... | [kgCha-1]
 * dC_ch4_production_hydrogen | ... | [kgCha-1]
 * dC_ch4_production_acetate | ... | [kgCha-1]
 * dC_floor_ch4_plant_diffusion | ... | [kgCha-1]
 * dC_floor_ch4_soil_diffusion | ... | [kgCha-1]
 * dC_floor_ch4_water_diffusion | ... | [kgCha-1]
 * dC_floor_ch4_bubbling | ... | [kgCha-1]
 * dC_leach_ch4 | ... | [kgCha-1]
 * dO_floor_o2_plant_diffusion | ... | []
 * dC_litter_above | ... | [kgCha-1]
 * dC_litter_below | ... | [kgCha-1]
 * dC_fix_algae | ... | [kgCha-1]
 * dN_fix_algae | ... | []
 * dC_decomp_litter | ... | [kg:ha-1]
 * dC_co2_prod_mic_1_growth | ... | [kgCha-1]
 * dC_co2_prod_mic_1_maintenance | ... | [kgCha-1]
 * dC_co2_prod_mic_2 | ... | [kgCha-1]
 * dC_co2_prod_mic_3_acetate_prod | ... | [kgCha-1]
 * dC_co2_prod_mic_3_acetate_cons | ... | [kgCha-1]
 * dC_co2_prod_ch4_prod | ... | [kgCha-1]
 * dC_co2_prod_ch4_cons | ... | [kgCha-1]
 * dH_hydrogen_prod | ... | []
 * dC_acetate_prod | ... | [kgCha-1]
 * dC_acetate_cons_fe3 | ... | [kgCha-1]
 * dH_hydrogen_cons_fe3 | ... | []
 * dC_doc_prod_litter | ... | [kgCha-1]
 * dC_doc_prod_humus | ... | [kgCha-1]
 * dC_doc_prod_aorg | ... | [kgCha-1]
 * dC_doc_prod_plant | ... | [kgCha-1]
 * dC_doc_prod_total | ... | [kgCha-1]
 * dC_humify_mic_hum_2 | ... | [kgCha-1]
 * dC_humify_hum_1_hum_2 | ... | [kgCha-1]
 * dC_humify_hum_2_hum_3 | ... | [kgCha-1]
 * dN_leach | ... | [kgNha-1]
 * dC_leach | ... | [kgCha-1]
 * aN_no3_groundwater | ... | [kgNha-1]
 * aN_assi | ... | [kgNha-1]
 * aN_min_decomp | ... | [kgNha-1]
 * aN_min_aorg | ... | [kgNha-1]
 * aN_min_mic_1 | ... | [kgNha-1]
 * aN_min_mic_2 | ... | [kgNha-1]
 * aN_min_mic_3 | ... | [kgNha-1]
 * aC_mic_1_growth | ... | [kgCha-1]
 * aC_mic_2_growth | ... | [kgCha-1]
 * aC_mic_3_growth | ... | [kgCha-1]
 * aN_nit_nh4_no2 | ... | [kgNha-1]
 * aN_nit_no2_no3 | ... | [kgNha-1]
 * aN_nit_no2_no | ... | [kgNha-1]
 * aN_nit_no2_n2o | ... | [kgNha-1]
 * aN_denit_no3_no2 | ... | [kgNha-1]
 * aN_denit_no2_no | ... | [kgNha-1]
 * aN_denit_no2_n2o | ... | [kgNha-1]
 * aN_denit_no2_n2 | ... | [kgNha-1]
 * aN_denit_n2o_n2 | ... | [kgNha-1]
 * aN_chemodenit_no2_no | ... | [kgNha-1]
 * aC_decomp_lit_1 | ... | [kgCha-1]
 * aC_decomp_lit_2 | ... | [kgCha-1]
 * aC_decomp_lit_3 | ... | [kgCha-1]
 * aN_decomp_lit_1 | ... | [kgNha-1]
 * aN_decomp_lit_2 | ... | [kgNha-1]
 * aN_decomp_lit_3 | ... | [kgNha-1]
 * aC_decomp_hum_1 | ... | [kgCha-1]
 * aC_decomp_hum_2 | ... | [kgCha-1]
 * aC_decomp_hum_3 | ... | [kgCha-1]
 * aN_decomp_hum_1 | ... | [kgNha-1]
 * aN_decomp_hum_2 | ... | [kgNha-1]
 * aN_decomp_hum_3 | ... | [kgNha-1]
 * aC_humify_doc_hum_1 | ... | [kgCha-1]
 * aC_humify_sol_hum_1 | ... | [kgCha-1]
 * aC_humify_cel_hum_1 | ... | [kgCha-1]
 * aC_humify_lig_hum_1 | ... | [kgCha-1]
 * aC_humify_mic_hum_1 | ... | [kgCha-1]
 * aC_humify_lig_hum_2 | ... | [kgCha-1]
 * aC_humify_mic_hum_2 | ... | [kgCha-1]
 * aC_humify_hum_1_hum_2 | ... | [kgCha-1]
 * aC_humify_hum_2_hum_3 | ... | [kgCha-1]
 * aC_fix_algae | ... | [kgCha-1]
 * aN_fix_algae | ... | [kgNha-1]
 * aC_litter_above | ... | [kgCha-1]
 * aC_litter_belo | ... | [kgNha-1]
 */
ldndc_string_t const  OutputSoilchemistryMeTrXDaily_Ids[] =
{
    "anvf[-]",
#ifdef  METRX_ANVF_TYPES
    "anvf_pnet[-]",
    "anvf_dndc_can[-]",
    "anvf_nloss[-]",
#endif
    "pore_connectivity[-]",
    "permeability",
    "wfps[%]",
    "air_content[%]",
    "root_conductivity[m2s-1]",
    "o2[kgha-1]",
    "t_eff_crnf",
    "n_release_fraction_crnf",
    "nitrification_inhibition[-]",
    "urease_inhibition[-]",
    "carbon_use_efficiency[-]",
    "N_don[kgNha-1]",
    "N_nh4[kgNha-1]",
    "N_nh4_clay[kgNha-1]",
    "N_nh4_coated[kgNha-1]",
    "N_urea[kgNha-1]",
    "N_no2_ae[kgNha-1]",
    "N_no2_an[kgNha-1]",
    "N_no3_ae[kgNha-1]",
    "N_no3_an[kgNha-1]",
    "N_n2o_ae[kgNha-1]",
    "N_n2o_an[kgNha-1]",
    "N_no_ae[kgNha-1]",
    "N_no_an[kgNha-1]",
    "N_nh3_gas[kgNha-1]",
    "N_nh3_liq[kgNha-1]",
    "C_doc_ae[kgCha-1]",
    "C_doc_an[kgCha-1]",
    "C_acetate[kgCha-1]",
    "C_ch4_gas[kgCha-1]",
    "C_ch4_liq[kgCha-1]",
    "C_aorg[kgCha-1]",
    "N_aorg[kgCha-1]",
    "C_micro_1[kgCha-1]",
    "C_micro_2[kgCha-1]",
    "C_micro_3[kgCha-1]",
    "N_micro_1[kgNha-1]",
    "N_micro_2[kgNha-1]",
    "N_micro_3[kgNha-1]",
    "C_humus_1[kgCha-1]",
    "C_humus_2[kgCha-1]",
    "C_humus_3[kgCha-1]",
    "N_humus_1[kgNha-1]",
    "N_humus_2[kgNha-1]",
    "N_humus_3[kgNha-1]",
    "C_litter_soil_1[kgCha-1]",
    "C_litter_soil_2[kgCha-1]",
    "C_litter_soil_3[kgCha-1]",
    "N_litter_soil_1[kgNha-1]",
    "N_litter_soil_2[kgNha-1]",
    "N_litter_soil_3[kgNha-1]",
    "C_algae[kgCha-1]",
    "N_algae[kgNha-1]",
    "C_litter_stubble[kgCha-1]",
    "N_litter_stubble[kgNha-1]",
    "C_total[kgCha-1]",
    "N_total[kgNha-1]",
    "soc_20cm[%]",
    "soc_40cm[%]",
    "totn_20cm[%]",
    "totn_40cm[%]",
    "fe2_tot",
    "fe3_tot",
    "till_fact",
    "freeze_thaw_fact",
    "litter_height[m]",
    "ph_watertable",
    "ph_soil_surface",
    "dN_no3_groundwater[kgNha-1]",
    "dN_assi[kgNha-1]",
    "dN_nit_nh4_no2[kgNha-1]",
    "dN_nit_no2_no3[kgNha-1]",
    "dN_nit_no2_no[kgNha-1]",
    "dN_nit_no2_n2o[kgNha-1]",
    "dN_denit_no3_no2[kgNha-1]",
    "dN_denit_no2_no[kgNha-1]",
    "dN_denit_no2_n2o[kgNha-1]",
    "dN_denit_no2_n2[kgNha-1]",
    "dN_denit_no_n2o[kgNha-1]",
    "dN_denit_no_n2[kgNha-1]",
    "dN_denit_n2o_n2[kgNha-1]",
    "dN_chemodenit_no2_no[kgNha-1]",
    "dC_ch4_oxidation",
    "dC_ch4_production_hydrogen",
    "dC_ch4_production_acetate",
    "dC_floor_ch4_plant_diffusion",
    "dC_floor_ch4_soil_diffusion",
    "dC_floor_ch4_water_diffusion",
    "dC_floor_ch4_bubbling",
    "dC_leach_ch4",
    "dO_floor_o2_plant_diffusion",
    "dC_litter_above",
    "dC_litter_below",
    "dC_fix_algae",
    "dN_fix_algae",
    "dC_decomp_litter[kgCha-1]",
    "dC_mic_death[kgCha-1]",
    "dC_co2_prod_mic_1_growth",
    "dC_co2_prod_mic_1_maintenance",
    "dC_co2_prod_mic_2",
    "dC_co2_prod_mic_3_acetate_prod",
    "dC_co2_prod_mic_3_acetate_cons",
    "dC_co2_prod_ch4_prod",
    "dC_co2_prod_ch4_cons",
    "dC_co2_prod_decomp",
    "dC_acetate_prod",
    "dC_acetate_cons_fe3",
    "dH_hydrogen_cons_fe3",
    "dC_doc_prod_litter",
    "dC_doc_prod_humus",
    "dC_doc_prod_aorg",
    "dC_doc_prod_plant",
    "dC_doc_prod_total",
    "dC_humify_mic_hum_2",
    "dC_humify_hum_1_hum_2",
    "dC_humify_hum_2_hum_3",
    "dN_leach",
    "dC_leach",
    "aN_no3_groundwater",
    "dN_min_decomp",
    "dN_min_aorg",
    "dN_min_mic_1",
    "dN_min_mic_2",
    "dN_min_mic_3",
    "dC_mic_1_growth",
    "dC_mic_2_growth",
    "dC_mic_3_growth",
    "aC_decomp_lit_1[kgCha-1]",
    "aC_decomp_lit_2[kgCha-1]",
    "aC_decomp_lit_3[kgCha-1]",
    "aN_decomp_lit_1[kgNha-1]",
    "aN_decomp_lit_2[kgNha-1]",
    "aN_decomp_lit_3[kgNha-1]",
    "aC_decomp_hum_1[kgCha-1]",
    "aC_decomp_hum_2[kgCha-1]",
    "aC_decomp_hum_3[kgCha-1]",
    "aN_decomp_hum_1[kgCha-1]",
    "aN_decomp_hum_2[kgCha-1]",
    "aN_decomp_hum_3[kgCha-1]",
    "aC_humify_doc_hum_1[kgCha-1]",
    "aC_humify_sol_hum_1[kgCha-1]",
    "aC_humify_cel_hum_1[kgCha-1]",
    "aC_humify_lig_hum_1[kgCha-1]",
    "aC_humify_mic_hum_1[kgCha-1]",
    "aC_humify_lig_hum_2[kgCha-1]",
    "aC_humify_mic_hum_2[kgCha-1]",
    "aC_humify_hum_1_hum_2[kgCha-1]",
    "aC_humify_hum_2_hum_3[kgCha-1]",
    "aC_fix_algae[kgCha-1]",
    "aN_fix_algae[kgNha-1]",
    "aC_litter_above[kgCha-1]",
    "aC_litter_below[kgNha-1]",
};



ldndc_string_t const *  OutputSoilchemistryMeTrXDaily_Header =
    OutputSoilchemistryMeTrXDaily_Ids;



#define  OutputSoilchemistryMeTrXDaily_Datasize  (sizeof( OutputSoilchemistryMeTrXDaily_Ids) / sizeof( OutputSoilchemistryMeTrXDaily_Ids[0]))
ldndc_output_size_t const  OutputSoilchemistryMeTrXDaily_Sizes[] =
{
    OutputSoilchemistryMeTrXDaily_Datasize,

    OutputSoilchemistryMeTrXDaily_Datasize /*total size*/
};



ldndc_output_size_t const *  OutputSoilchemistryMeTrXDaily_EntitySizes = NULL;

#define  OutputSoilchemistryMeTrXDaily_Rank  ((ldndc_output_rank_t)(sizeof( OutputSoilchemistryMeTrXDaily_Sizes) / sizeof( OutputSoilchemistryMeTrXDaily_Sizes[0])) - 1)
atomic_datatype_t const  OutputSoilchemistryMeTrXDaily_Types[] =
{
    LDNDC_FLOAT64
};



OutputSoilchemistryMeTrXDaily::OutputSoilchemistryMeTrXDaily(
    MoBiLE_State *  _state,cbm::io_kcomm_t *  _io_kcomm, timemode_e  _timemode)
        : MBE_LegacyOutputModel( _state, _io_kcomm, _timemode),
        m_iokcomm( _io_kcomm)
    { }



OutputSoilchemistryMeTrXDaily::~OutputSoilchemistryMeTrXDaily()
    { }



size_t
OutputSoilchemistryMeTrXDaily::record_size()
const
{
    return  OutputSoilchemistryMeTrXDaily_Datasize;
}



lerr_t
OutputSoilchemistryMeTrXDaily::configure(
                                   ldndc::config_file_t const *  _cf)
{
    if ( _cf && !_cf->have_output())
    {
        return  LDNDC_ERR_OK;
    }

    lerr_t  rc_setflags =
        this->set_metaflags( _cf, RM_DEFAULT_DAILY);
    if ( rc_setflags)
        { return  LDNDC_ERR_FAIL; }

    m_sink = m_iokcomm->sink_handle_acquire( "metrxdaily");
    if ( m_sink.status() == LDNDC_ERR_OK)
    {
        lerr_t  rc_layout = LDNDC_OUTPUT_DEFINE_SINK_LAYOUT_DEFAULTS(
                this->m_sink, OutputSoilchemistryMeTrXDaily);
        RETURN_IF_NOT_OK(rc_layout);
    }
    else
    {
        this->set_active_off();
    }
    
    return  LDNDC_ERR_OK;
}



lerr_t
OutputSoilchemistryMeTrXDaily::initialize()
{
    return  LDNDC_ERR_OK;
}



lerr_t
OutputSoilchemistryMeTrXDaily::solve()
{
    KLOGDEBUG( "metrx record size=", record_size());
    KLOGFATAL( "do not use solve(), we rock with write_results()");
    return  LDNDC_ERR_FAIL;

}



lerr_t
OutputSoilchemistryMeTrXDaily::write_results(
                               ldndc_flt64_t *  _data)
{
    if ( !_data || !m_sink.is_acquired())
    {
        /* we assume there is nothing to dump ... */
        return  LDNDC_ERR_OK;
    }

    /* output */
    void *  data[] = { _data};
    lerr_t  rc_write = write_fixed_record( &this->m_sink, data);
    if ( rc_write)
    { return LDNDC_ERR_FAIL; }
    return  LDNDC_ERR_OK;
}



lerr_t
OutputSoilchemistryMeTrXDaily::finalize()
{
    return  m_iokcomm->sink_handle_release( &m_sink);
}

} /*namespace ldndc*/

#undef  OutputSoilchemistryMeTrXDaily_Rank
#undef  OutputSoilchemistryMeTrXDaily_Datasize

