/*!
 * @file
 * @author
 *  David Kraus
 */

#include  "soilchemistry/metrx/output-soilchemistry-metrx-yearly.h"

LMOD_MODULE_INFO(OutputSoilchemistryMeTrXYearly,TMODE_POST_DAILY,LMOD_FLAG_USER|LMOD_FLAG_OUTPUT);
namespace ldndc {

/*!
 * @page metrx
 * @subsection metrx_output_yearly Yearly output
 * entity name | decription | unit
 * ----------- | ---------- | ----
 * C\_surface | Surface C-litter | [kgCm-2]
 * C\_soil\_20cm | Total soil carbon in 20 cm topsoil | [kgCm-2]
 * C\_humus\_1\_20cm | Carbon in humus pool 1 in 20 cm topsoil | [kgCm-2]
 * C\_humus\_2\_20cm | Carbon in humus pool 1 in 20 cm topsoil | [kgCm-2]
 * C\_humus\_3\_20cm | Carbon in humus pool 1 in 20 cm topsoil | [kgCm-2]
 * C\_litter\_20cm | Carbon in litter pools in 20 cm topsoil | [kgCm-2]
 * C\_aorg\_20cm | Carbon aorg pool in 20 cm topsoil | [kgCm-2]
 */
ldndc_string_t const  OutputSoilchemistryMeTrXYearly_Ids[] =
{
    "C_surface[kgCm-2]",
    "C_soil_20cm[kgCm-2]",
    "C_humus_1_20cm[kgCm-2]",
    "C_humus_2_20cm[kgCm-2]",
    "C_humus_3_20cm[kgCm-2]",
    "C_litter_20cm[kgCm-2]",
    "C_aorg_20cm[kgCm-2]",
};

ldndc_string_t const *  OutputSoilchemistryMeTrXYearly_Header =
    OutputSoilchemistryMeTrXYearly_Ids;


#define  OutputSoilchemistryMeTrXYearly_Datasize  (sizeof( OutputSoilchemistryMeTrXYearly_Ids) / sizeof( OutputSoilchemistryMeTrXYearly_Ids[0]))
ldndc_output_size_t const  OutputSoilchemistryMeTrXYearly_Sizes[] =
{
    OutputSoilchemistryMeTrXYearly_Datasize,

    OutputSoilchemistryMeTrXYearly_Datasize /*total size*/
};



ldndc_output_size_t const *  OutputSoilchemistryMeTrXYearly_EntitySizes = NULL;

#define  OutputSoilchemistryMeTrXYearly_Rank  ((ldndc_output_rank_t)(sizeof( OutputSoilchemistryMeTrXYearly_Sizes) / sizeof( OutputSoilchemistryMeTrXYearly_Sizes[0])) - 1)
atomic_datatype_t const  OutputSoilchemistryMeTrXYearly_Types[] =
{
    LDNDC_FLOAT64
};



OutputSoilchemistryMeTrXYearly::OutputSoilchemistryMeTrXYearly(
                                                               MoBiLE_State *  _state,
                                                               cbm::io_kcomm_t *  _io_kcomm,
                                                               timemode_e  _timemode)
                                                : MBE_LegacyOutputModel( _state, _io_kcomm, _timemode),
                                                m_iokcomm( _io_kcomm),
                                                sl_( _io_kcomm->get_input_class_ref< input_class_soillayers_t >()),
                                                accumulated_n_no3_no2_denitrification_sl( sl_.soil_layer_cnt(), 0.0)
{ }

OutputSoilchemistryMeTrXYearly::~OutputSoilchemistryMeTrXYearly()
{ }


size_t
OutputSoilchemistryMeTrXYearly::record_size()
const
{
    return  OutputSoilchemistryMeTrXYearly_Datasize;
}



lerr_t
OutputSoilchemistryMeTrXYearly::configure(
        ldndc::config_file_t const *  _cf)
{
    if ( _cf && !_cf->have_output())
    {
        return  LDNDC_ERR_OK;
    }

    lerr_t  rc_setflags = set_metaflags( _cf, RM_DEFAULT_LAYERDAILY);
    if ( rc_setflags)
    { return  LDNDC_ERR_FAIL; }

    m_sink = m_iokcomm->sink_handle_acquire( "metrxyearly");
    if ( m_sink.status() == LDNDC_ERR_OK)
    {
        lerr_t  rc_layout = LDNDC_OUTPUT_DEFINE_SINK_LAYOUT_DEFAULTS( m_sink,OutputSoilchemistryMeTrXYearly);
        RETURN_IF_NOT_OK(rc_layout);
    }
    else
    {
        this->set_active_off();
    }
    
    return  LDNDC_ERR_OK;
}



lerr_t
OutputSoilchemistryMeTrXYearly::initialize()
{
    return  LDNDC_ERR_OK;
}



lerr_t
OutputSoilchemistryMeTrXYearly::solve()
{
    KLOGDEBUG( "metrx record size=", record_size());
    KLOGFATAL( "do not use solve(), we rock with write_results()");
    return  LDNDC_ERR_FAIL;

}



lerr_t
OutputSoilchemistryMeTrXYearly::write_results( ldndc_flt64_t *  _data)
{
    if ( !_data || !m_sink.is_acquired())
    {
        /* we assume there is nothing to dump ... */
        return  LDNDC_ERR_OK;
    }

    /* output */
    void *  data[] = { _data};
    lerr_t  rc_write = write_fixed_record( &this->m_sink, data);
    if ( rc_write)
    { return  LDNDC_ERR_FAIL; }
    return  LDNDC_ERR_OK;
}



lerr_t
OutputSoilchemistryMeTrXYearly::finalize()
{
    return  m_iokcomm->sink_handle_release( &m_sink);
}

} /*namespace ldndc*/

#undef  OutputSoilchemistryMeTrXYearly_Rank
#undef  OutputSoilchemistryMeTrXYearly_Datasize

