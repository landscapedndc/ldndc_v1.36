import os

import numpy as np
import math

from mpl_toolkits.mplot3d import axes3d
import matplotlib.pyplot as plt

import xml.etree.ElementTree as xml

script_dir = os.path.dirname(os.path.abspath(__file__))

parameterfile = script_dir+'/../../../../../resources/parameters-site.xml'
xmlparameters = xml.parse( parameterfile)
xmlparameters = xmlparameters.getroot()
xmlparameters = xmlparameters.find( 'siteparameters')

def get_par( _name, _xml, _type='mean') :
    for elem in _xml :
        if elem.attrib.get( 'name').lower() == _name.lower() :
            if _type == 'mean':
                return float(elem.attrib.get( 'value', ''))
            elif _type == 'min':
                return float(elem.attrib.get( 'min', ''))
            elif _type == 'max':
                return float(elem.attrib.get( 'max', ''))

    return None

def moisture_no_nit( _x, _p1, _p2) :
    return 1.0 - 1.0 / (1.0 + math.exp(-_p1*(_x-_p2)))
    #return  math.exp( -_p1 * (1.0 - (_x / _p2))**2.0)
def weibull( _x, _p1, _p2, _p3) :
    return  1.0 - ( _p3 / ( 1.0 + math.exp((_x - _p1) * _p2)))
def exponential( _x, _p1, _p2) :
    return _p1 * math.exp( _x / _p2)
def temp( _x, _p1, _p2) :
    return math.exp(-_p1 * (1.0 - _x / _p2)**2.0)
def oneill( _x, _p_max, _p_opt, _exp) :
    d0 = _p_max - _x
    d1 = _p_max - _p_opt
    return  (d0 / d1)**_exp * math.exp( _exp * ( d1 - d0) / d1)
def logisitc( _x, _p1, _p2) :
    return 1.0 / (1.0 + math.exp(-_p1 * (_x-_p2)))
def q10( _x, _xref, _q10) :
    return _q10**((_x - _xref)/10.0)
def michaelis_menten( _x, _k_mm) :
    return _x / (_x + _k_mm)


##################################################################################################
# Microbial processes depending on temperature
p1 = { 'mean': get_par('METRX_F_MIC_T_EXP_1', xmlparameters),
       'min': get_par('METRX_F_MIC_T_EXP_1', xmlparameters, 'min'),
       'max': get_par('METRX_F_MIC_T_EXP_1', xmlparameters, 'max')}
p2 = { 'mean': get_par('METRX_F_MIC_T_EXP_2', xmlparameters),
       'min': get_par('METRX_F_MIC_T_EXP_2', xmlparameters, 'min'),
       'max': get_par('METRX_F_MIC_T_EXP_2', xmlparameters, 'max')}

fig, ax = plt.subplots( 1, 3, figsize=(15, 4))
temp_range = np.arange(-10, 45, 0.5)

temp_response = [temp( x, p1['mean'], p2['mean']) for x in temp_range]
ax[0].plot(temp_range, temp_response)
ax[0].set_title( f"METRX_F_MIC_T_EXP_1: {p1['mean']}\nMETRX_F_MIC_T_EXP_2: {p2['mean']}", fontsize=8)

for t in ['mean', 'min', 'max']:
    temp_response = [temp( x, p1[t], p2['mean']) for x in temp_range]
    ax[1].plot(temp_range, temp_response)
ax[1].set_title( f"METRX_F_MIC_T_EXP_1: [{p1['min']}, {p1['max']}]\nMETRX_F_MIC_T_EXP_2: {p2['mean']}", fontsize=8)
for t in ['mean', 'min', 'max']:
    temp_response = [temp( x, p1['mean'], p2[t]) for x in temp_range]
    ax[2].plot(temp_range, temp_response)
ax[2].set_title( f"METRX_F_MIC_T_EXP_1: {p1['mean']}\nMETRX_F_MIC_T_EXP_2: [{p2['min']}, {p2['max']}]", fontsize=8)

for i in range(3):
    ax[i].set_xlabel('T [oC]')
    ax[i].set_ylabel('$\phi_{T,Mic}$ [-]')


plt.savefig(script_dir+"/../figures/metrx_microbial_activity_on_temperature.png")
plt.close()

##################################################################################################
# Denitrification depending on soilwater
p1 = { 'mean': get_par('METRX_F_DENIT_M_WEIBULL_1', xmlparameters),
       'min': get_par('METRX_F_DENIT_M_WEIBULL_1', xmlparameters, 'min'),
       'max': get_par('METRX_F_DENIT_M_WEIBULL_1', xmlparameters, 'max')}
p2 = { 'mean': get_par('METRX_F_DENIT_M_WEIBULL_2', xmlparameters),
       'min': get_par('METRX_F_DENIT_M_WEIBULL_2', xmlparameters, 'min'),
       'max': get_par('METRX_F_DENIT_M_WEIBULL_2', xmlparameters, 'max')}
p3 = 1.0
fig, ax = plt.subplots( 1, 3, figsize=(15, 4))
wfps_range = np.arange(0, 1, 0.05)

wfps_response = [weibull( x, p1['mean'], p2['mean'], p3) for x in wfps_range]
ax[0].plot(wfps_range, wfps_response)
ax[0].set_title( f"METRX_F_DENIT_M_WEIBULL_1: {p1['mean']}\nMETRX_F_DENIT_M_WEIBULL_2: {p2['mean']}", fontsize=8)

for t in ['mean', 'min', 'max']:
    wfps_response = [weibull( x, p1[t], p2['mean'], p3) for x in wfps_range]
    ax[1].plot(wfps_range, wfps_response)
ax[1].set_title( f"METRX_F_DENIT_M_WEIBULL_1: [{p1['min']}, {p1['max']}]\nMETRX_F_DENIT_M_WEIBULL_2: {p2['mean']}", fontsize=8)
for t in ['mean', 'min', 'max']:
    wfps_response = [weibull( x, p1['mean'], p2[t], p3) for x in wfps_range]
    ax[2].plot(wfps_range, wfps_response)
ax[2].set_title( f"METRX_F_DENIT_M_WEIBULL_1: {p1['mean']}\nMETRX_F_DENIT_M_WEIBULL_2: [{p2['min']}, {p2['max']}]", fontsize=8)


for i in range(3):
    ax[i].set_xlabel('wfps [-]')
    ax[i].set_ylabel('$\phi_{wfps,Denit}$ [-]')
plt.savefig(script_dir+"/../figures/metrx_denitrification_on_soilwater.png")
plt.close()

##################################################################################################
# Denitrification depending on anvf
p1 = { 'mean': get_par('METRX_F_DENIT_N2_1', xmlparameters),
       'min': get_par('METRX_F_DENIT_N2_1', xmlparameters, 'min'),
       'max': get_par('METRX_F_DENIT_N2_1', xmlparameters, 'max')}
p2 = { 'mean': get_par('METRX_F_DENIT_N2_2', xmlparameters),
       'min': get_par('METRX_F_DENIT_N2_2', xmlparameters, 'min'),
       'max': get_par('METRX_F_DENIT_N2_2', xmlparameters, 'max')}

fig, ax = plt.subplots( 1, 3, figsize=(15, 4))
anvf_range = np.arange(0, 1, 0.01)

anvf_response = [p1['mean']*p2['mean'] + i * (p2['mean'] - p1['mean']*p2['mean']) for i in anvf_range]
ax[0].plot(anvf_range, anvf_response)
ax[0].set_title( f"METRX_F_DENIT_N2_1: {p1['mean']}\nMETRX_F_DENIT_N2_2: {p2['mean']}", fontsize=8)

for t in ['mean', 'min', 'max']:
    anvf_response = [p1[t]*p2['mean'] + i * (p2['mean'] - p1[t]*p2['mean']) for i in anvf_range]
    ax[1].plot(anvf_range, anvf_response)
ax[1].set_title( f"METRX_F_DENIT_N2_1: [{p1['min']}, {p1['max']}]\nMETRX_F_DENIT_N2_2: {p2['mean']}", fontsize=8)
for t in ['mean', 'min', 'max']:
    anvf_response = [p1['mean']*p2[t] + i * (p2[t] - p1['mean']*p2[t]) for i in anvf_range]
    ax[2].plot(anvf_range, anvf_response)
ax[2].set_title( f"METRX_F_DENIT_N2_1: {p1['mean']}\METRX_F_DENIT_N2_2: [{p2['min']}, {p2['max']}]", fontsize=8)

for i in range(3):
    ax[i].set_xlabel('anvf [-]')
    ax[i].set_ylabel('response [-]')
    ax[i].set_ylim(-0.01, 1.0)
plt.savefig(script_dir+"/../figures/metrx_denitrification_to_N2_on_anvf.png")

##################################################################################################
# Denitrification depending on pH
p1 = { 'mean': get_par('METRX_F_DENIT_PH_1', xmlparameters),
       'min': get_par('METRX_F_DENIT_PH_1', xmlparameters, 'min'),
       'max': get_par('METRX_F_DENIT_PH_1', xmlparameters, 'max')}
p2 = { 'mean': get_par('METRX_F_DENIT_PH_2', xmlparameters),
       'min': get_par('METRX_F_DENIT_PH_2', xmlparameters, 'min'),
       'max': get_par('METRX_F_DENIT_PH_2', xmlparameters, 'max')}

fig, ax = plt.subplots( 1, 3, figsize=(15, 4))
ph_range = np.arange(0, 14, 0.1)

ph_response = [1.0-1.0/(1.0+math.exp((i-p1['mean'])/p2['mean'])) for i in ph_range]
ax[0].plot(ph_range, ph_response)
ax[0].set_title( f"METRX_F_DENIT_PH_1: {p1['mean']}\nMETRX_F_DENIT_PH_2: {p2['mean']}", fontsize=8)

for t in ['mean', 'min', 'max']:
    ph_response = [1.0-1.0/(1.0+math.exp((i-p1[t])/p2['mean'])) for i in ph_range]
    ax[1].plot(ph_range, ph_response)
ax[1].set_title( f"METRX_F_DENIT_PH_1: [{p1['min']}, {p1['max']}]\nMETRX_F_DENIT_PH_2: {p2['mean']}", fontsize=8)
for t in ['mean', 'min', 'max']:
    ph_response = [1.0-1.0/(1.0+math.exp((i-p1['mean'])/p2[t])) for i in ph_range]
    ax[2].plot(ph_range, ph_response)
ax[2].set_title( f"METRX_F_DENIT_PH_1: {p1['mean']}\nMETRX_F_DENIT_PH_2: [{p2['min']}, {p2['max']}]", fontsize=8)

for i in range(3):
    ax[i].set_xlabel('pH [-]')
    ax[i].set_ylabel('response [-]')
plt.savefig(script_dir+"/../figures/metrx_denitrification_on_ph.png")

##################################################################################################
# Denitrification of N2O depending on pH
p1 = { 'mean': get_par('METRX_F_DENIT_N2O_PH_1', xmlparameters),
       'min': get_par('METRX_F_DENIT_N2O_PH_1', xmlparameters, 'min'),
       'max': get_par('METRX_F_DENIT_N2O_PH_1', xmlparameters, 'max')}
p2 = { 'mean': get_par('METRX_F_DENIT_N2O_PH_2', xmlparameters),
       'min': get_par('METRX_F_DENIT_N2O_PH_2', xmlparameters, 'min'),
       'max': get_par('METRX_F_DENIT_N2O_PH_2', xmlparameters, 'max')}

fig, ax = plt.subplots( 1, 3, figsize=(15, 4))
ph_range = np.arange(0, 14, 0.1)

ph_response = [1.0-1.0/(1.0+math.exp((i-p1['mean'])/p2['mean'])) for i in ph_range]
ax[0].plot(ph_range, ph_response)
ax[0].set_title( f"METRX_F_DENIT_N2O_PH_1: {p1['mean']}\nMETRX_F_DENIT_N2O_PH_2: {p2['mean']}", fontsize=8)

for t in ['mean', 'min', 'max']:
    ph_response = [1.0-1.0/(1.0+math.exp((i-p1[t])/p2['mean'])) for i in ph_range]
    ax[1].plot(ph_range, ph_response)
ax[1].set_title( f"METRX_F_DENIT_N2O_PH_1: [{p1['min']}, {p1['max']}]\nMETRX_F_DENIT_N2O_PH_2: {p2['mean']}", fontsize=8)
for t in ['mean', 'min', 'max']:
    ph_response = [1.0-1.0/(1.0+math.exp((i-p1['mean'])/p2[t])) for i in ph_range]
    ax[2].plot(ph_range, ph_response)
ax[2].set_title( f"METRX_F_DENIT_N2O_PH_1: {p1['mean']}\nMETRX_F_DENIT_N2O_PH_2: [{p2['min']}, {p2['max']}]", fontsize=8)

for i in range(3):
    ax[i].set_xlabel('pH [-]')
    ax[i].set_ylabel('response [-]')
plt.savefig(script_dir+"/../figures/metrx_denitrification_of_n2o_on_ph.png")


##################################################################################################
# Chemodenitrification depending on pH
p1 = get_par('METRX_F_CHEMODENIT_PH_1', xmlparameters)
p2 = get_par('METRX_F_CHEMODENIT_PH_2', xmlparameters)
fig = plt.figure( figsize=(5, 4))
ax = fig.add_subplot(1,1,1)
ph = np.arange(0, 14, 0.1)
ph_response = [1.0-1.0/(1.0+math.exp(-p1*(i-p2))) for i in ph]
ax.plot(ph, ph_response)
ax.set_xlabel('pH [-]')
ax.set_ylabel('response [-]')
plt.savefig(script_dir+"/../figures/metrx_chemodenitrification_on_ph.png")

##################################################################################################
# Chemodenitrification depending on temperature
p1 = get_par('METRX_F_CHEMODENIT_T_1', xmlparameters)
p2 = get_par('METRX_F_CHEMODENIT_T_2', xmlparameters)
fig = plt.figure( figsize=(5, 4))
ax = fig.add_subplot(1,1,1)
t_range = np.arange(0, 50, 1.0)
temp_response = [p1 * math.exp(-p2/(8.314 * (i+273.15))) for i in t_range]
ax.plot(t_range, temp_response)
ax.set_ylim(0, 2.0)
ax.set_xlabel('T [oC]')
ax.set_ylabel('response [-]')
plt.savefig(script_dir+"/../figures/metrx_chemodenitrification_on_temperature.png")

##################################################################################################
# Decomposition depending on clay content
p1 = get_par('METRX_F_DECOMP_CLAY_1', xmlparameters)
p2 = get_par('METRX_F_DECOMP_CLAY_2', xmlparameters)
fig = plt.figure( figsize=(5, 4))
ax = fig.add_subplot(1,1,1)
clay_range = np.arange(0, 1.05, 0.05)
clay_response = [p1 + (1.0 - p1) * math.exp(-p2 * x) for x in clay_range]
ax.plot(clay_range, clay_response)
ax.set_xlabel('clay [-]')
ax.set_ylabel('$\phi_{clay}$ [-]')
ax.set_ylim(0, 1.0)
ax.set_xlim(0, 1.0)
plt.savefig(script_dir+"/../figures/metrx_decomposition_on_clay.png")
plt.close()

##################################################################################################
# Decomposition depending on temperature and soilwater
fig, ax = plt.subplots( 2, 3, figsize=(15, 8))
t1 = { 'mean': get_par('METRX_F_DECOMP_T_EXP_1', xmlparameters),
       'min': get_par('METRX_F_DECOMP_T_EXP_1', xmlparameters, 'min'),
       'max': get_par('METRX_F_DECOMP_T_EXP_1', xmlparameters, 'max')}
t2 = { 'mean': get_par('METRX_F_DECOMP_T_EXP_2', xmlparameters),
       'min': get_par('METRX_F_DECOMP_T_EXP_2', xmlparameters, 'min'),
       'max': get_par('METRX_F_DECOMP_T_EXP_2', xmlparameters, 'max')}

w1 = { 'mean': get_par('METRX_F_DECOMP_M_WEIBULL_1', xmlparameters),
       'min': get_par('METRX_F_DECOMP_M_WEIBULL_1', xmlparameters, 'min'),
       'max': get_par('METRX_F_DECOMP_M_WEIBULL_1', xmlparameters, 'max')}
w2 = { 'mean': get_par('METRX_F_DECOMP_M_WEIBULL_2', xmlparameters),
       'min': get_par('METRX_F_DECOMP_M_WEIBULL_2', xmlparameters, 'min'),
       'max': get_par('METRX_F_DECOMP_M_WEIBULL_2', xmlparameters, 'max')}
w3 = 1

temp_range = np.arange(-10, 50, 1)
wfps_range = np.arange(0, 1.1, 0.01)

temp_response = [temp( x, t1['mean'], t2['mean']) for x in temp_range]
ax[0][0].plot(temp_range, temp_response)
ax[0][0].set_title( f"METRX_F_DECOMP_T_EXP_1: {t1['mean']}\nMETRX_F_DECOMP_T_EXP_2: {t2['mean']}", fontsize=8)

for t in ['mean', 'min', 'max']:
    temp_response = [temp( x, t1[t], t2['mean']) for x in temp_range]
    ax[0][1].plot(temp_range, temp_response)
ax[0][1].set_title( f"METRX_F_DECOMP_T_EXP_1: [{t1['min']}, {t1['max']}]\nMETRX_F_DECOMP_T_EXP_2: {t2['mean']}", fontsize=8)
for t in ['mean', 'min', 'max']:
    temp_response = [temp( x, t1['mean'], t2[t]) for x in temp_range]
    ax[0][2].plot(temp_range, temp_response)
ax[0][2].set_title( f"METRX_F_DECOMP_T_EXP_1: {t1['mean']}\nMETRX_F_DECOMP_T_EXP_2: [{t2['min']}, {t2['max']}]", fontsize=8)

for i in range(3):
    ax[0][i].set_xlabel('T [oC]')
    ax[0][i].set_ylabel('$\phi_{T,Decomp}$ [-]')

wfps_response = [weibull( x, w1['mean'], w2['mean'], w3) for x in wfps_range]
ax[1][0].plot(wfps_range, wfps_response)
ax[1][0].set_title( f"METRX_F_DECOMP_M_WEIBULL_1: {w1['mean']}\nMETRX_F_DECOMP_M_WEIBULL_2: {w2['mean']}", fontsize=8)

for t in ['mean', 'min', 'max']:
    wfps_response = [weibull( x, w1[t], w2['mean'], w3) for x in wfps_range]
    ax[1][1].plot(wfps_range, wfps_response)
ax[1][1].set_title( f"METRX_F_DECOMP_M_WEIBULL_1: [{w1['min']}, {w1['max']}]\nMETRX_F_DECOMP_M_WEIBULL_2: {w2['mean']}", fontsize=8)
for t in ['mean', 'min', 'max']:
    wfps_response = [weibull( x, w1['mean'], w2[t], w3) for x in wfps_range]
    ax[1][2].plot(wfps_range, wfps_response)
ax[1][2].set_title( f"METRX_F_DECOMP_M_WEIBULL_1: {w1['mean']}\nMETRX_F_DECOMP_M_WEIBULL_2: [{w2['min']}, {w2['max']}]", fontsize=8)

for i in range(3):
    ax[1][i].set_xlabel('wfps [-]')
    ax[1][i].set_ylabel('$\phi_{wfps,Decomp}$ [-]')

for x in range(3):
    ax[1][x].set_xlim(-0.01, 1.01)
    for y in range(2):
        ax[y][x].set_ylim(-0.01, 1.01)

plt.subplots_adjust(top = 0.92, bottom=0.08, hspace=0.4, wspace=0.2)
plt.savefig(script_dir+"/../figures/metrx_decomposition_on_temperature_soilwater.png")
plt.close()

##################################################################################################
# Decomposition depending on pH
fig = plt.figure( figsize=(5, 4))
ax = fig.add_subplot(1,1,1)
ph = np.arange(0, 14, 0.1)
p1 = get_par('METRX_F_DECOMP_PH_1', xmlparameters)
p2 = get_par('METRX_F_DECOMP_PH_2', xmlparameters)
ph_response = [logisitc(i, p1, p2) for i in ph]
ax.plot(ph, ph_response)
ax.set_xlabel('pH [-]')
ax.set_ylabel('$\phi_{pH}$ [-]')
plt.savefig(script_dir+"/../figures/metrx_decomposition_on_ph.png")

##################################################################################################
# NO and N2O during nitrification depending on temperature
p1 = { 'mean': get_par('METRX_F_NIT_NO_N2O_T_EXP_1', xmlparameters),
       'min': get_par('METRX_F_NIT_NO_N2O_T_EXP_1', xmlparameters, 'min'),
       'max': get_par('METRX_F_NIT_NO_N2O_T_EXP_1', xmlparameters, 'max')}
p2 = { 'mean': get_par('METRX_F_NIT_NO_N2O_T_EXP_2', xmlparameters),
       'min': get_par('METRX_F_NIT_NO_N2O_T_EXP_2', xmlparameters, 'min'),
       'max': get_par('METRX_F_NIT_NO_N2O_T_EXP_2', xmlparameters, 'max')}

fig, ax = plt.subplots( 1, 3, figsize=(15, 4))
temp_range = np.arange(-10, 45, 0.5)

temp_response = [exponential(x, p1['mean'], p2['mean']) for x in temp_range]
ax[0].plot(temp_range, temp_response)
ax[0].set_title( f"METRX_F_NIT_NO_N2O_T_EXP_1: {p1['mean']}\nMETRX_F_NIT_NO_N2O_T_EXP_2: {p2['mean']}", fontsize=8)

for t in ['mean', 'min', 'max']:
    temp_response = [exponential(x, p1[t], p2['mean']) for x in temp_range]
    ax[1].plot(temp_range, temp_response)
ax[1].set_title( f"METRX_F_NIT_NO_N2O_T_EXP_1: [{p1['min']}, {p1['max']}]\nMETRX_F_NIT_NO_N2O_T_EXP_2: {p2['mean']}", fontsize=8)
for t in ['mean', 'min', 'max']:
    temp_response = [exponential(x, p1['mean'], p2[t]) for x in temp_range]
    ax[2].plot(temp_range, temp_response)
ax[2].set_title( f"METRX_F_NIT_NO_N2O_T_EXP_1: {p1['mean']}\nMETRX_F_NIT_NO_N2O_T_EXP_2: [{p2['min']}, {p2['max']}]", fontsize=8)

for i in range(3):
    ax[i].set_xlabel('T [oC]')
    ax[i].set_ylabel('$\phi_{T}$ [-]')
    ax[i].set_ylim(0, 2.0)

plt.savefig(script_dir+"/../figures/metrx_no_n2o_prod_nitrification_on_temperature.png")
plt.close()

##################################################################################################
# NO and N2O during nitrification depending on water
p1 = { 'mean': get_par('METRX_F_NIT_NO_N2O_M_WEIBULL_1', xmlparameters),
       'min': get_par('METRX_F_NIT_NO_N2O_M_WEIBULL_1', xmlparameters, 'min'),
       'max': get_par('METRX_F_NIT_NO_N2O_M_WEIBULL_1', xmlparameters, 'max')}
p2 = { 'mean': get_par('METRX_F_NIT_NO_N2O_M_WEIBULL_2', xmlparameters),
       'min': get_par('METRX_F_NIT_NO_N2O_M_WEIBULL_2', xmlparameters, 'min'),
       'max': get_par('METRX_F_NIT_NO_N2O_M_WEIBULL_2', xmlparameters, 'max')}
p3 = 1.0
fig, ax = plt.subplots( 1, 3, figsize=(15, 4))
wfps_range = np.arange(0, 1, 0.05)

wfps_response = [weibull( x, p1['mean'], p2['mean'], p3) for x in wfps_range]
ax[0].plot(wfps_range, wfps_response)
ax[0].set_title( f"METRX_F_NIT_NO_N2O_M_WEIBULL_1: {p1['mean']}\nMETRX_F_NIT_NO_N2O_M_WEIBULL_2: {p2['mean']}", fontsize=8)

for t in ['mean', 'min', 'max']:
    wfps_response = [weibull( x, p1[t], p2['mean'], p3) for x in wfps_range]
    ax[1].plot(wfps_range, wfps_response)
ax[1].set_title( f"METRX_F_NIT_NO_N2O_M_WEIBULL_1: [{p1['min']}, {p1['max']}]\nMETRX_F_NIT_NO_N2O_M_WEIBULL_2: {p2['mean']}", fontsize=8)
for t in ['mean', 'min', 'max']:
    wfps_response = [weibull( x, p1['mean'], p2[t], p3) for x in wfps_range]
    ax[2].plot(wfps_range, wfps_response)
ax[2].set_title( f"METRX_F_NIT_NO_N2O_M_WEIBULL_1: {p1['mean']}\nMETRX_F_NIT_NO_N2O_M_WEIBULL_2: [{p2['min']}, {p2['max']}]", fontsize=8)

for i in range(3):
    ax[i].set_xlabel('wfps [-]')
    ax[i].set_ylabel('$\phi_{wfps}$ [-]')
plt.savefig(script_dir+"/../figures/metrx_no_n2o_prod_nitrification_on_soilwater.png")
plt.close()

##################################################################################################
# NO during nitrification depending on water
p1 = { 'mean': get_par('METRX_F_NIT_NO_M_EXP_1', xmlparameters),
       'min': get_par('METRX_F_NIT_NO_M_EXP_1', xmlparameters, 'min'),
       'max': get_par('METRX_F_NIT_NO_M_EXP_1', xmlparameters, 'max')}
p2 = { 'mean': get_par('METRX_F_NIT_NO_M_EXP_2', xmlparameters),
       'min': get_par('METRX_F_NIT_NO_M_EXP_2', xmlparameters, 'min'),
       'max': get_par('METRX_F_NIT_NO_M_EXP_2', xmlparameters, 'max')}
p3 = 1.0
fig, ax = plt.subplots( 1, 3, figsize=(15, 4))
wfps_range = np.arange(0, 1, 0.05)

wfps_response = [moisture_no_nit(x, p1['mean'], p2['mean']) for x in wfps_range]
ax[0].plot(wfps_range, wfps_response)
ax[0].set_title( f"METRX_F_NIT_NO_M_EXP_1: {p1['mean']}\nMETRX_F_NIT_NO_M_EXP_2: {p2['mean']}", fontsize=8)

for t in ['mean', 'min', 'max']:
    wfps_response = [moisture_no_nit(x, p1[t], p2['mean']) for x in wfps_range]
    ax[1].plot(wfps_range, wfps_response)
ax[1].set_title( f"METRX_F_NIT_NO_M_EXP_1: [{p1['min']}, {p1['max']}]\nMETRX_F_NIT_NO_M_EXP_2: {p2['mean']}", fontsize=8)
for t in ['mean', 'min', 'max']:
    wfps_response = [moisture_no_nit(x, p1['mean'], p2[t]) for x in wfps_range]
    ax[2].plot(wfps_range, wfps_response)
ax[2].set_title( f"METRX_F_NIT_NO_M_EXP_1: {p1['mean']}\nMETRX_F_NIT_NO_M_EXP_2: [{p2['min']}, {p2['max']}]", fontsize=8)

for i in range(3):
    ax[i].set_xlabel('wfps [-]')
    ax[i].set_ylabel('$\phi_{wfps}$ [-]')
plt.savefig(script_dir+"/../figures/metrx_no_prod_nitrification_on_soilwater.png")
plt.close()

##################################################################################################
# Nitrification on O2
p1 = get_par('METRX_KMM_O2_NIT', xmlparameters)*1.0e3
fig = plt.figure( figsize=(5, 4))
ax = fig.add_subplot(1,1,1)
doc = np.arange(0, 100.0, 1.0e-1)
doc_response = [michaelis_menten( i, p1) for i in doc]
ax.plot(doc, doc_response)
ax.set_xlabel('O2 [g:m-3]')
ax.set_ylabel('response [-]')
plt.savefig(script_dir+"/../figures/metrx_nh4_nitrification_on_o2.png")

##################################################################################################
# Nitrification on NH4
p1 = get_par('METRX_KMM_NH4_NIT', xmlparameters)*1.0e3
fig = plt.figure( figsize=(5, 4))
ax = fig.add_subplot(1,1,1)
nh4 = np.arange(0, 10.0, 1.0e-1)
nh4_response = [michaelis_menten( i, p1) for i in nh4]
ax.plot(nh4, nh4_response)
ax.set_xlabel('N-NH4 [g:m-3]')
ax.set_ylabel('response [-]')
plt.savefig(script_dir+"/../figures/metrx_nh4_nitrification_on_nh4.png")

##################################################################################################
# Nitrification on NO2
p1 = get_par('METRX_KMM_NO2_NIT', xmlparameters)*1.0e3
fig = plt.figure( figsize=(5, 4))
ax = fig.add_subplot(1,1,1)
no2 = np.arange(0, 10.0, 1.0e-1)
no2_response = [michaelis_menten( i, p1) for i in no2]
ax.plot(no2, no2_response)
ax.set_xlabel('NO2 [g:m-3]')
ax.set_ylabel('response [-]')
plt.savefig(script_dir+"/../figures/metrx_no2_nitrification_on_no2.png")

##################################################################################################
# Denitrification of N2O depending on pH
p1 = { 'mean': get_par('METRX_F_NIT_PH_1', xmlparameters),
       'min': get_par('METRX_F_NIT_PH_1', xmlparameters, 'min'),
       'max': get_par('METRX_F_NIT_PH_1', xmlparameters, 'max')}
p2 = { 'mean': get_par('METRX_F_NIT_PH_2', xmlparameters),
       'min': get_par('METRX_F_NIT_PH_2', xmlparameters, 'min'),
       'max': get_par('METRX_F_NIT_PH_2', xmlparameters, 'max')}

fig, ax = plt.subplots( 1, 3, figsize=(15, 4))
ph_range = np.arange(0, 14, 0.1)

ph_response = [1.0-1.0/(1.0+math.exp(-p1['mean']*(i-p2['mean']))) for i in ph]
ax[0].plot(ph_range, ph_response)
ax[0].set_title( f"METRX_F_NIT_PH_1: {p1['mean']}\nMETRX_F_NIT_PH_2: {p2['mean']}", fontsize=8)

for t in ['mean', 'min', 'max']:
    ph_response = [1.0-1.0/(1.0+math.exp(-p1[t]*(i-p2['mean']))) for i in ph]
    ax[1].plot(ph_range, ph_response)
ax[1].set_title( f"METRX_F_NIT_PH_1: [{p1['min']}, {p1['max']}]\nMETRX_F_NIT_PH_2: {p2['mean']}", fontsize=8)
for t in ['mean', 'min', 'max']:
    ph_response = [1.0-1.0/(1.0+math.exp(-p1['mean']*(i-p2[t]))) for i in ph]
    ax[2].plot(ph_range, ph_response)
ax[2].set_title( f"METRX_F_NIT_PH_1: {p1['mean']}\nMETRX_F_NIT_PH_2: [{p2['min']}, {p2['max']}]", fontsize=8)

for i in range(3):
    ax[i].set_xlabel('pH [-]')
    ax[i].set_ylabel('response [-]')
plt.savefig(script_dir+"/../figures/metrx_nh4_nitrification_on_ph.png")
plt.close()

##################################################################################################
# Mic. growth depending on DOC
p1 = get_par('METRX_KMM_C_MIC', xmlparameters)*1.0e3
fig = plt.figure( figsize=(5, 4))
ax = fig.add_subplot(1,1,1)
doc = np.arange(0, 10.0, 1.0e-1)
doc_response = [michaelis_menten( i, p1) for i in doc]
ax.plot(doc, doc_response)
ax.set_xlabel('DOC [g:m-3]')
ax.set_ylabel('response [-]')
plt.savefig(script_dir+"/../figures/metrx_microbial_growth_on_doc.png")

##################################################################################################
# CH4 production on ph
p1 = 2
p2 = 3.4
p3 = 7
ph_response2 = [1.0 / (1.0 + (abs(i - p3) / p1)**p2) for i in ph]
ax.plot(ph, ph_response)
ax.plot(ph, ph_response2)
ax.set_xlabel('pH [-]')
ax.set_ylabel('response [-]')
ax.set_xlim(0, 14.0)
plt.savefig(script_dir+"/../figures/metrx_ch4_production_on_ph.png")
plt.close()

##################################################################################################
# CH4 production on temperature
fig = plt.figure( figsize=(5, 4))
ax = fig.add_subplot(1,1,1)
p1 = get_par('METRX_F_CH4_PRODUCTION_T_EXP_1', xmlparameters)
p2 = get_par('METRX_F_CH4_PRODUCTION_T_EXP_2', xmlparameters)
t_range = np.arange(-10, 40, 1)
t_response = [temp(i, p1, p2) for i in t_range]
ax.plot(t_range, t_response)
ax.set_ylim([0.0, 2.05])
ax.set_xlabel('T [-]')
ax.set_ylabel('$\phi_{T}$ [-]')
plt.savefig(script_dir+"/../figures/metrx_ch4_production_on_temperature.png")

##################################################################################################
# CH4 oxidation on temperature
p1 = get_par('METRX_F_CH4_OXIDATION_T_EXP_1', xmlparameters)
p2 = get_par('METRX_F_CH4_OXIDATION_T_EXP_2', xmlparameters)
t_range = np.arange(-10, 40, 1)
t_response = [temp(i, p1, p2) for i in t_range]
ax.plot(t_range, t_response)
ax.set_xlabel('T [-]')
ax.set_ylabel('$\phi_{T}$ [-]')
plt.savefig(script_dir+"/../figures/metrx_ch4_oxidation_on_temperature.png")

##################################################################################################
#Fermentation on temperature
fig = plt.figure( figsize=(5, 4))
ax = fig.add_subplot(1,1,1)
p1 = 4.6
p2 = 30.0
t_range = np.arange(0, 40, 1.0)
temp_response = [p1**((i-p2)/10.0) for i in t_range]
ax.plot(t_range, temp_response)
ax.set_ylim(0, 2.0)
ax.set_xlabel('T [oC]')
ax.set_ylabel('response [-]')
plt.savefig(script_dir+"/../figures/metrx_fermentation_on_temperature.png")

##################################################################################################
#Anaerobic volume fraction
p1 = { 'mean': get_par('METRX_F_ANVF_1', xmlparameters),
       'min': get_par('METRX_F_ANVF_1', xmlparameters, 'min'),
       'max': get_par('METRX_F_ANVF_1', xmlparameters, 'max')}
p2 = { 'mean': get_par('METRX_F_ANVF_2', xmlparameters),
       'min': get_par('METRX_F_ANVF_2', xmlparameters, 'min'),
       'max': get_par('METRX_F_ANVF_2', xmlparameters, 'max')}

fig, ax = plt.subplots( 1, 3, figsize=(15, 4))
o2_range = np.arange(0, 0.3, 0.001)

anvf = [math.exp(-(p2['mean']*i)**p1['mean']) for i in o2_range]
ax[0].plot(o2_range, anvf)
ax[0].set_title( f"METRX_F_ANVF_1: {p1['mean']}\nMETRX_F_DENIT_N2O_PH_2: {p2['mean']}", fontsize=8)

for t in ['mean', 'min', 'max']:
    anvf = [math.exp(-(p2['mean']*i)**p1[t]) for i in o2_range]
    ax[1].plot(o2_range, anvf)
ax[1].set_title( f"METRX_F_ANVF_1: [{p1['min']}, {p1['max']}]\nMETRX_F_DENIT_N2O_PH_2: {p2['mean']}", fontsize=8)
for t in ['mean', 'min', 'max']:
    anvf = [math.exp(-(p2[t]*i)**p1['mean']) for i in o2_range]
    ax[2].plot(o2_range, anvf)
ax[2].set_title( f"METRX_F_ANVF_1: {p1['mean']}\nMETRX_F_DENIT_N2O_PH_2: [{p2['min']}, {p2['max']}]", fontsize=8)

for i in range(3):
    ax[i].set_xlabel('O$_2$ [bar]')
    ax[i].set_ylabel('anvf [-]')
    ax[i].set_ylim([0.0, 1.05])
plt.subplots_adjust(top = 0.85, bottom=0.15, hspace=0.4, wspace=0.2)
plt.savefig(script_dir+"/../figures/metrx_anaerobic_volume_fraction_on_o2.png")

##################################################################################################
#Pore connectivity on air content
p1 = get_par('METRX_F_CONNECTIVITY_1', xmlparameters)
p2 = get_par('METRX_F_CONNECTIVITY_2', xmlparameters)
fig = plt.figure( figsize=(5, 4))
plt.rcParams.update({'font.size': 13})
plt.gcf().subplots_adjust(bottom=0.15)
plt.gcf().subplots_adjust(left=0.15)
ax = fig.add_subplot(1,1,1)
v_air = np.arange(0, 1.0, 0.01)
p_connect = [max( 0.0, 1.0 - math.exp(-p2*(i-p1))) for i in v_air]
ax.plot(v_air, p_connect)
ax.set_xlabel('air content [-]')
ax.set_ylabel('connectivity [-]')
ax.set_ylim([0.0, 1.05])
plt.savefig(script_dir+"/../figures/metrx_pore_connectivity_on_air_content.png")
