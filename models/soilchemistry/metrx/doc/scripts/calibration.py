import os

import pandas as pd
import numpy as np

import xml.etree.ElementTree as xml

script_dir = os.path.dirname(os.path.abspath(__file__))

parameterfile = script_dir+'/../../../../../resources/parameters-site.xml'
xmlparameters = xml.parse( parameterfile)
xmlparameters = xmlparameters.getroot()
xmlparameters = xmlparameters.find( 'siteparameters')

def get_par( _name, _xml, _type='mean') :
    try:
        for elem in _xml :
            if elem.attrib.get( 'name').lower() == _name.lower() :
                if _type == 'mean':
                    return float(elem.attrib.get( 'value', ''))
                elif _type == 'min':
                    return float(elem.attrib.get( 'min', ''))
                elif _type == 'max':
                    return float(elem.attrib.get( 'max', ''))
    except:
        pass
    return None


df = pd.DataFrame( columns={"name": [], "mean": [], "min": [], "max": [], "function": [], "calibration_target": [] })


for p_name in ['METRX_F_MIC_T_EXP_1', 'METRX_F_MIC_T_EXP_2']:
    f_name = 'metrx_microbial_activity_on_temperature'
    p = { 'mean': get_par(p_name, xmlparameters), 'min': get_par(p_name, xmlparameters, 'min'), 'max': get_par(p_name, xmlparameters, 'max')}
    df = pd.concat( [df, pd.DataFrame({ "name": [p_name], "mean": [p["mean"]], "min": [p['min']], "max": [p['max']], "function": [f_name], "calibration_target": ["N2O"] })], ignore_index=True)
for p_name in ['METRX_F_MIC_M_WEIBULL_1', 'METRX_F_MIC_M_WEIBULL_2']:
    f_name = 'metrx_microbial_activity_on_soilwater'
    p = { 'mean': get_par(p_name, xmlparameters), 'min': get_par(p_name, xmlparameters, 'min'), 'max': get_par(p_name, xmlparameters, 'max')}
    df = pd.concat( [df, pd.DataFrame({ "name": [p_name], "mean": [p["mean"]], "min": [p['min']], "max": [p['max']], "function": [f_name], "calibration_target": ["N2O"] })], ignore_index=True)


for p_name in ['METRX_MUEMAX_C_DENIT']:
    f_name = 'metrx_denitrification_growth'
    p = { 'mean': get_par(p_name, xmlparameters), 'min': get_par(p_name, xmlparameters, 'min'), 'max': get_par(p_name, xmlparameters, 'max')}
    df = pd.concat( [df, pd.DataFrame({ "name": [p_name], "mean": [p["mean"]], "min": [p['min']], "max": [p['max']], "function": [f_name], "calibration_target": ["N2O"] })], ignore_index=True)
for p_name in ['METRX_KMM_C_DENIT']:
    f_name = 'metrx_denitrification_on_c'
    p = { 'mean': get_par(p_name, xmlparameters), 'min': get_par(p_name, xmlparameters, 'min'), 'max': get_par(p_name, xmlparameters, 'max')}
    df = pd.concat( [df, pd.DataFrame({ "name": [p_name], "mean": [p["mean"]], "min": [p['min']], "max": [p['max']], "function": [f_name], "calibration_target": ["N2O"] })], ignore_index=True)
for p_name in ['METRX_KMM_N_DENIT']:
    f_name = 'metrx_denitrification_on_n'
    p = { 'mean': get_par(p_name, xmlparameters), 'min': get_par(p_name, xmlparameters, 'min'), 'max': get_par(p_name, xmlparameters, 'max')}
    df = pd.concat( [df, pd.DataFrame({ "name": [p_name], "mean": [p["mean"]], "min": [p['min']], "max": [p['max']], "function": [f_name], "calibration_target": ["N2O"] })], ignore_index=True)
for p_name in ['METRX_F_DENIT_M_WEIBULL_1', 'METRX_F_DENIT_M_WEIBULL_2']:
    f_name = 'metrx_denitrification_on_soilwater'
    p = { 'mean': get_par(p_name, xmlparameters), 'min': get_par(p_name, xmlparameters, 'min'), 'max': get_par(p_name, xmlparameters, 'max')}
    df = pd.concat( [df, pd.DataFrame({ "name": [p_name], "mean": [p["mean"]], "min": [p['min']], "max": [p['max']], "function": [f_name], "calibration_target": ["N2O"] })], ignore_index=True)
for p_name in ['METRX_F_DENIT_NO']:
    f_name = 'metrx_denitrification_to_NO_on_anvf'
    p = { 'mean': get_par(p_name, xmlparameters), 'min': get_par(p_name, xmlparameters, 'min'), 'max': get_par(p_name, xmlparameters, 'max')}
    df = pd.concat( [df, pd.DataFrame({ "name": [p_name], "mean": [p["mean"]], "min": [p['min']], "max": [p['max']], "function": [f_name], "calibration_target": ["N2O"] })], ignore_index=True)
for p_name in ['METRX_F_DENIT_N2_1', 'METRX_F_DENIT_N2_2']:
    f_name = 'metrx_denitrification_to_N2_on_anvf'
    p = { 'mean': get_par(p_name, xmlparameters), 'min': get_par(p_name, xmlparameters, 'min'), 'max': get_par(p_name, xmlparameters, 'max')}
    df = pd.concat( [df, pd.DataFrame({ "name": [p_name], "mean": [p["mean"]], "min": [p['min']], "max": [p['max']], "function": [f_name], "calibration_target": ["N2O"] })], ignore_index=True)
for p_name in ['METRX_F_DENIT_PH_1', 'METRX_F_DENIT_PH_2']:
    f_name = 'metrx_denitrification_on_ph'
    p = { 'mean': get_par(p_name, xmlparameters), 'min': get_par(p_name, xmlparameters, 'min'), 'max': get_par(p_name, xmlparameters, 'max')}
    df = pd.concat( [df, pd.DataFrame({ "name": [p_name], "mean": [p["mean"]], "min": [p['min']], "max": [p['max']], "function": [f_name], "calibration_target": ["N2O"] })], ignore_index=True)
for p_name in ['METRX_F_DENIT_N2O_PH_1', 'METRX_F_DENIT_N2O_PH_2']:
    f_name = 'metrx_denitrification_of_n2o_on_ph'
    p = { 'mean': get_par(p_name, xmlparameters), 'min': get_par(p_name, xmlparameters, 'min'), 'max': get_par(p_name, xmlparameters, 'max')}
    df = pd.concat( [df, pd.DataFrame({ "name": [p_name], "mean": [p["mean"]], "min": [p['min']], "max": [p['max']], "function": [f_name], "calibration_target": ["N2O"] })], ignore_index=True)


for p_name in ['METRX_F_NIT_FRAC_MIC']:
    f_name = 'metrx_nitrification_on_microbes'
    p = { 'mean': get_par(p_name, xmlparameters), 'min': get_par(p_name, xmlparameters, 'min'), 'max': get_par(p_name, xmlparameters, 'max')}
    df = pd.concat( [df, pd.DataFrame({ "name": [p_name], "mean": [p["mean"]], "min": [p['min']], "max": [p['max']], "function": [f_name], "calibration_target": ["N2O"] })], ignore_index=True)
for p_name in ['METRX_F_NIT_NO_N2O_T_EXP_1', 'METRX_F_NIT_NO_N2O_T_EXP_2']:
    f_name = 'metrx_nitrification_no_n2o_on_temperature'
    p = { 'mean': get_par(p_name, xmlparameters), 'min': get_par(p_name, xmlparameters, 'min'), 'max': get_par(p_name, xmlparameters, 'max')}
    df = pd.concat( [df, pd.DataFrame({ "name": [p_name], "mean": [p["mean"]], "min": [p['min']], "max": [p['max']], "function": [f_name], "calibration_target": ["N2O"] })], ignore_index=True)
for p_name in ['METRX_F_NIT_NO_N2O_M_WEIBULL_1', 'METRX_F_NIT_NO_N2O_M_WEIBULL_2']:
    f_name = 'metrx_nitrification_no_n2o_on_soilwater'
    p = { 'mean': get_par(p_name, xmlparameters), 'min': get_par(p_name, xmlparameters, 'min'), 'max': get_par(p_name, xmlparameters, 'max')}
    df = pd.concat( [df, pd.DataFrame({ "name": [p_name], "mean": [p["mean"]], "min": [p['min']], "max": [p['max']], "function": [f_name], "calibration_target": ["N2O"] })], ignore_index=True)
for p_name in ['METRX_F_NIT_NO_M_EXP_1', 'METRX_F_NIT_NO_M_EXP_2']:
    f_name = 'metrx_nitrification_no_on_temperature'
    p = { 'mean': get_par(p_name, xmlparameters), 'min': get_par(p_name, xmlparameters, 'min'), 'max': get_par(p_name, xmlparameters, 'max')}
    df = pd.concat( [df, pd.DataFrame({ "name": [p_name], "mean": [p["mean"]], "min": [p['min']], "max": [p['max']], "function": [f_name], "calibration_target": ["N2O"] })], ignore_index=True)
for p_name in ['METRX_KMM_O2_NIT']:
    f_name = 'metrx_nitrification_on_o2'
    p = { 'mean': get_par(p_name, xmlparameters), 'min': get_par(p_name, xmlparameters, 'min'), 'max': get_par(p_name, xmlparameters, 'max')}
    df = pd.concat( [df, pd.DataFrame({ "name": [p_name], "mean": [p["mean"]], "min": [p['min']], "max": [p['max']], "function": [f_name], "calibration_target": ["N2O"] })], ignore_index=True)
for p_name in ['METRX_KMM_NH4_NIT']:
    f_name = 'metrx_nitrification_on_nh4'
    p = { 'mean': get_par(p_name, xmlparameters), 'min': get_par(p_name, xmlparameters, 'min'), 'max': get_par(p_name, xmlparameters, 'max')}
    df = pd.concat( [df, pd.DataFrame({ "name": [p_name], "mean": [p["mean"]], "min": [p['min']], "max": [p['max']], "function": [f_name], "calibration_target": ["N2O"] })], ignore_index=True)
for p_name in ['METRX_KMM_NO2_NIT']:
    f_name = 'metrx_nitrification_on_no2'
    p = { 'mean': get_par(p_name, xmlparameters), 'min': get_par(p_name, xmlparameters, 'min'), 'max': get_par(p_name, xmlparameters, 'max')}
    df = pd.concat( [df, pd.DataFrame({ "name": [p_name], "mean": [p["mean"]], "min": [p['min']], "max": [p['max']], "function": [f_name], "calibration_target": ["N2O"] })], ignore_index=True)
for p_name in ['METRX_F_NIT_PH_1', 'METRX_F_NIT_PH_2']:
    f_name = 'metrx_nitrification_on_ph'
    p = { 'mean': get_par(p_name, xmlparameters), 'min': get_par(p_name, xmlparameters, 'min'), 'max': get_par(p_name, xmlparameters, 'max')}
    df = pd.concat( [df, pd.DataFrame({ "name": [p_name], "mean": [p["mean"]], "min": [p['min']], "max": [p['max']], "function": [f_name], "calibration_target": ["N2O"] })], ignore_index=True)


for p_name in ['METRX_F_ANVF_1', 'METRX_F_ANVF_2']:
    f_name = 'metrx_anaerobic_volume_fraction_on_o2'
    p = { 'mean': get_par(p_name, xmlparameters), 'min': get_par(p_name, xmlparameters, 'min'), 'max': get_par(p_name, xmlparameters, 'max')}
    df = pd.concat( [df, pd.DataFrame({ "name": [p_name], "mean": [p["mean"]], "min": [p['min']], "max": [p['max']], "function": [f_name], "calibration_target": ["N2O"] })], ignore_index=True)

df.to_csv(script_dir+"/calibration.txt", sep="\t", index=False, na_rep="None")