/*!
 * @file
 * @author
 *  David Kraus
 */

#include  "soilchemistry/metrx/output-soilchemistry-metrx-layer-yearly.h"

LMOD_MODULE_INFO(OutputSoilchemistryMeTrXLayerYearly,TMODE_POST_DAILY,LMOD_FLAG_USER|LMOD_FLAG_OUTPUT);
namespace ldndc {

ldndc_string_t const  OutputSoilchemistryMeTrXLayerYearly_Ids[] =
{
    "level",
    "c_humus_1[kgCm-2]",
    "c_humus_2[kgCm-2]",
    "c_humus_3[kgCm-2]",
    "c_aorg[kgCm-2]",
    "c_mic[kgCm-2]",
    "aC_humify_doc_hum_1[kgCm-2]",
    "aC_humify_sol_hum_1[kgCm-2]",
    "aC_humify_cel_hum_1[kgCm-2]",
    "aC_humify_lig_hum_1[kgCm-2]",
    "aC_humify_lig_hum_2[kgCm-2]",
    "aC_humify_mic_hum_1[kgCm-2]",
    "aC_humify_mic_hum_2[kgCm-2]",
    "aC_humify_hum_1_hum_2[kgCm-2]",
    "aC_humify_hum_2_hum_3[kgCm-2]",
    "aC_decomp_hum_1[kgCm-2]",
    "aC_decomp_hum_2[kgCm-2]",
    "aC_decomp_hum_3[kgCm-2]",
    "aC_doc_prod[kgCm-2]",
    "aC_mic_death[kgCm-2]",
    "aC_co2_hetero[kgCm-2]",
    "no3_cons_denit[kgNm-2]"
};

ldndc_string_t const *  OutputSoilchemistryMeTrXLayerYearly_Header =
    OutputSoilchemistryMeTrXLayerYearly_Ids;


#define  OutputSoilchemistryMeTrXLayerYearly_Datasize  (sizeof( OutputSoilchemistryMeTrXLayerYearly_Ids) / sizeof( OutputSoilchemistryMeTrXLayerYearly_Ids[0]))
ldndc_output_size_t const  OutputSoilchemistryMeTrXLayerYearly_Sizes[] =
{
    OutputSoilchemistryMeTrXLayerYearly_Datasize,

    OutputSoilchemistryMeTrXLayerYearly_Datasize /*total size*/
};



ldndc_output_size_t const *  OutputSoilchemistryMeTrXLayerYearly_EntitySizes = NULL;

#define  OutputSoilchemistryMeTrXLayerYearly_Rank  ((ldndc_output_rank_t)(sizeof( OutputSoilchemistryMeTrXLayerYearly_Sizes) / sizeof( OutputSoilchemistryMeTrXLayerYearly_Sizes[0])) - 1)
atomic_datatype_t const  OutputSoilchemistryMeTrXLayerYearly_Types[] =
{
    LDNDC_FLOAT64
};



OutputSoilchemistryMeTrXLayerYearly::OutputSoilchemistryMeTrXLayerYearly(
                                                                       MoBiLE_State *  _state,
                                                                       cbm::io_kcomm_t *  _io_kcomm,
                                                                       timemode_e  _timemode)
                                                                        : MBE_LegacyOutputModel( _state, _io_kcomm, _timemode),
                                                                        m_iokcomm( _io_kcomm),
                                                                        sl_( _io_kcomm->get_input_class_ref< input_class_soillayers_t >()),
                                                                        accumulated_n_no3_no2_denitrification_sl( sl_.soil_layer_cnt(), 0.0)
{ }

OutputSoilchemistryMeTrXLayerYearly::~OutputSoilchemistryMeTrXLayerYearly()
{ }


size_t
OutputSoilchemistryMeTrXLayerYearly::record_size()
const
{
    return  OutputSoilchemistryMeTrXLayerYearly_Datasize;
}



lerr_t
OutputSoilchemistryMeTrXLayerYearly::configure(
        ldndc::config_file_t const *  _cf)
{
    if ( _cf && !_cf->have_output())
    {
        return  LDNDC_ERR_OK;
    }

    lerr_t  rc_setflags =
        this->set_metaflags( _cf, RM_DEFAULT_LAYERDAILY);
    if ( rc_setflags)
        { return  LDNDC_ERR_FAIL; }

    m_sink = m_iokcomm->sink_handle_acquire( "metrxlayeryearly");
    if ( m_sink.status() == LDNDC_ERR_OK)
    {
        lerr_t  rc_layout = LDNDC_OUTPUT_DEFINE_SINK_LAYOUT_DEFAULTS(
            this->m_sink,OutputSoilchemistryMeTrXLayerYearly);
        RETURN_IF_NOT_OK(rc_layout);
    }
    else
    {
        this->set_active_off();
    }
    
    return  LDNDC_ERR_OK;
}



lerr_t
OutputSoilchemistryMeTrXLayerYearly::initialize()
{
    return  LDNDC_ERR_OK;
}



lerr_t
OutputSoilchemistryMeTrXLayerYearly::solve()
{
    KLOGDEBUG( "metrx record size=", record_size());
    KLOGFATAL( "do not use solve(), we rock with write_results()");
    return  LDNDC_ERR_FAIL;

}



lerr_t
OutputSoilchemistryMeTrXLayerYearly::write_results(
        unsigned int  _layer, ldndc_flt64_t *  _data)
{
    if ( !_data || !m_sink.is_acquired())
    {
        /* we assume there is nothing to dump ... */
        return  LDNDC_ERR_OK;
    }

    /* output */
    void *  data[] = { _data};
    this->set_layernumber( -static_cast< int >( _layer)-1);
    lerr_t  rc_write =
        this->write_fixed_record( &this->m_sink, data);
    if ( rc_write)
        { return  LDNDC_ERR_FAIL; }
    return  LDNDC_ERR_OK;
}



lerr_t
OutputSoilchemistryMeTrXLayerYearly::finalize()
{
    return  m_iokcomm->sink_handle_release( &m_sink);
}

} /*namespace ldndc*/

#undef  OutputSoilchemistryMeTrXLayerYearly_Rank
#undef  OutputSoilchemistryMeTrXLayerYearly_Datasize

