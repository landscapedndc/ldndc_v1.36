/*!
 * @brief
 * 
 * Calculation is based on the sapwood area demand for foliage.
 * Sapwood volume is assumed to equal a cone within the canopy and across the rooted soil, 
 * and a column between the ground and canopy start.
 * 
 * @author
 *    david kraus (created on: may 6, 2014)
 */

#include  "soilchemistry/ld_sapwoodfraction.h"
#include  <math/cbm_math.h>

double 
ldndc::sap_wood_fraction(
                  double _lai_max,
                  double _qsfa,
                  double _height_min,
                  double _height_max,
                  double _depth)
{
    /* sapwood area (assumed to be in equilibrium with foliage demand) */
    double const  sapArea( _lai_max * _qsfa);
    
    /* estimated aboveground sapwood biomass */
    double const  msap_a( sapArea * _height_min + sapArea/3.0 * (_height_max - _height_min));
    
    /* estimated belowground sapwood biomass */
    double const  msap_b( sapArea / 3.0 * _depth);
    
    if ( cbm::flt_greater_zero( msap_a + msap_b))
    {
        return  msap_b / ( msap_a + msap_b);
    }
    
    return  0.0;
}

