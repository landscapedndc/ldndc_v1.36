/*!
 * @brief
 * @author
 *  David Kraus
 * @date
 *  Dec, 2017
 */

#include  "soilchemistry/ld_cation_exchange_capacity.h"
#include  <math/cbm_math.h>


/*!
 * @page soillibs
 * @section soillibs_cationexchangecapacity Cation exchange capacity
 * @authors
 *  - Khaledian @cite khaledian:2017a
 *  - David Kraus (implementation into LandscapeDNDC)
 *
 *  Cation exchange capacity (CEC) describes the capacity of soils to hold
 *  positively charged ions such as:
 *  - Calcium \f$(Ca^{2+})\f$
 *  - Magnesium \f$(Mg^{2+})\f$
 *  - Potassium \f$(K^{+})\f$
 *  - Sodium \f$(Na^{+})\f$
 *  - Hydrogen \f$(H^{+})\f$
 *  - Aluminium \f$(Al^{3+})\f$
 *  - Iron \f$(Fe^{2+})\f$
 *  - Manganese \f$(Mn^{2+})\f$
 *  - Zinc \f$(Zn^{2+})\f$
 *
 *  E.g., clay and soil organic matter (SOM) are negatively charged and are major
 *  contributors to total soil CEC. The CEC of SOM depends on the soil pH value
 *  (increasing with pH). The CEC of soil can be expressed in mol positive charge per
 *  soil mass \f$\left ( \frac{cmol_c}{kg} \right) \f$. Sandy soil with low SOM
 *  have low CEC \f$ \left( < 3 \frac{cmol_c}{kg} \right) \f$), while clayey and
 *  SOM-enriched soils have high CEC \f$ \left( > 20 \frac{cmol_c}{kg} \right )\f$.
 */
double 
ldndc::cation_exchange_capacity(
                                double _clay,
                                double _sand,
                                double _som,
                                double _ph,
                                ecosystem_type_e _ecosystem)
{
    double const silt( cbm::bound(0.0, 100.0 - _clay - _sand, 100.0));
    double const cec_max( 50.0);
    
    /*!
     * @page soillibs
     *  The ecosystem type specific pedotransfer functions of soil CEC are
     *  taken from @cite khaledian:2017a.
     *
     *  - Arable ecosystems \f$ 41.025 + 1.091 * clay + 0.512 * silt + 0.159 * sand - 10.087 * ph + 0.494 * som \f$
     *  - Grassland ecosystems \f$ -64.880 + 1.365 * clay + 0.992 * silt + 0.864 * sand - 2.453 * ph + 0.282 * som \f$
     *  - Forest ecosystems \f$ 10.973 + 0.427 * clay - 0.379 * silt - 0.436 * sand + 4.313 * ph + 6.963e-2 * som \f$
     */
    switch ( _ecosystem)
    {
        case  ECOSYS_ARABLE:
            return cbm::bound( 0.0, 41.025 + 1.091 * _clay + 0.512 * silt + 0.159 * _sand - 10.087 * _ph + 0.494 * _som, cec_max);

        case  ECOSYS_GRASSLAND:
            return cbm::bound( 0.0, -64.880 + 1.365 * _clay + 0.992 * silt + 0.864 * _sand - 2.453 * _ph + 0.282 * _som, cec_max);

        case  ECOSYS_FOREST:
            return cbm::bound( 0.0, 10.973 + 0.427 * _clay - 0.379 * silt - 0.436 * _sand + 4.313 * _ph + 6.963e-2 * _som, cec_max);

        default:
            return cbm::bound( 0.0, 27.842 + 0.706 * _clay - 0.092 * silt - 0.169 * _sand - 2.277 * _ph + 0.278 * _som, cec_max);
    }
}

