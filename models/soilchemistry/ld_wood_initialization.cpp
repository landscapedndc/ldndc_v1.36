/*!
 * @file
 * @author
 *    David Kraus
 * @date
 *    Nov, 2017)
 */

#include  "soilchemistry/ld_wood_initialization.h"
#include  <constants/cbm_const.h>

/*!
 * @brief
 *  Initialization of aboveground and belowground wood debris,
 *  derived from existing vegetation.
 */
lerr_t
ldndc::wood_initialization(
                           soillayers::input_class_soillayers_t const &  _sl,
                           MoBiLE_PlantVegetation & _m_veg,
                           substate_soilchemistry_t &  _sc,
                           ecosystem_type_e _ecosystemtype)
{
    for ( PlantIterator vt = _m_veg.begin(); vt != _m_veg.end(); ++vt)
    {
        MoBiLE_Plant *p = *vt;
        /*!
         * @todo
         *    Find a logic relation to stand conditions or initialize with a selected value.
         *    The current assumption of 10% of standing wood biomass is very crude
         */
        double const add_c_wood( ((_ecosystemtype == ECOSYS_FOREST_NATURAL) ? 0.3 : 0.1) * ( p->mSap + p->mCor) * cbm::CCDM);

        //add 80% aboveground
        _sc.c_wood += 0.8 * add_c_wood;
        _sc.n_wood += 0.8 * add_c_wood / cbm::CCDM * p->ncCor;

        //add 20% belowground
        for ( size_t  sl = 0;  sl < _sl.soil_layer_cnt();  ++sl)
        {
            _sc.c_wood_sl[sl] += 0.2 * p->fFrt_sl[sl] * add_c_wood;
            _sc.n_wood_sl[sl] += 0.2 * p->fFrt_sl[sl] * add_c_wood / cbm::CCDM * p->ncCor;
        }
    }

    return  LDNDC_ERR_OK;
}

