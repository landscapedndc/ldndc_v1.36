/*!
 * @file
 *
 * @author
 *    david kraus (created on: oct 23, 2015)
 */

#ifndef  LM_SOILCHEMISTRY_TRANSPORT_H_
#define  LM_SOILCHEMISTRY_TRANSPORT_H_

#include  "state/mbe_state.h"

namespace ldndc {


enum boundary_condition{ dirichlet, neuman };


/*!
 * @brief
 *
 */
class LDNDC_API Transport
{
public:

    Transport( MoBiLE_State *, cbm::io_kcomm_t *);

    ~Transport();

    substate_soilchemistry_t const & sc_;
    substate_surfacebulk_t &  sb_;
    substate_physiology_t const &  ph_;
    input_class_setup_t const & se_;
    input_class_soillayers_t const & sl_;

private:

    solver_tridiagonal_t  tdma_solver_;

    //effective diffusion coefficient for plant mediated diffusion per soil layer
    lvector_t< double >  d_eff_plant_sl_;

    //
    lvector_t< double >  d_eff_air_sl_;

    //effective gas diffusion coefficient in atmosphere.
    lvector_t< double >  d_eff_air_fl_;

    //air volume per soil layer
    lvector_t< double >  v_air_sl_;

    //asdvection velocity
    lvector_t< double >  soil_gas_advection_sl_;

    //
    lvector_t< double >  interface_air_sl_;

    //
    lvector_t< double >  interface_delta_x_air_sl_;

    //unit conversion factor used for diffusion calculations
    lvector_t< double >  fact_uc_xl_;

    //interface between surface and upper soil layer
    double interface_delta_x_atm_soil_;

    //
    double interface_delta_x_fl_soil_;

    double interface_delta_x_sbl_soil_;
public:
    //
    bool have_water_table_;

    //
    double h_wl_;

public:

    double wind_stimulation;

    /*!
     * @brief
     *
     */
    void
    update_diffusion(
           lvector_t< double > const &,
           lvector_t< double > const &,
           lvector_t< double > const &,
           lvector_t< double > const &,
           lvector_t< double > const &,
           double ,
           double ,
           double ,
           double ,
           bool );

    void
    update_advection(
           lvector_t< double > const &);

    /*!
     * @brief
     *
     */
    void
    gas_diffusion_soil(
                       bool const & /* enable/disable plant diffusion */,
                       double const & /* diffusion coefficient */,
                       lvector_t< double > & /* effective diffusion coefficient */,
                       lvector_t< double > & /* substance */,
                       lvector_t< double > & /* bulk */,
                       lvector_t< double > & /* interface */,
                       double & /* out/in-flow via surface */,
                       double & /* out/in-flow via soil ground */,
                       double & /* out/in-flow via plant */,
                       boundary_condition const & /* upper boundary condition*/,
                       boundary_condition const & /* lower boundary condition*/,
                       double const & /* dirichlet boundary condition*/,
                       double const & /* atmospheric concentration */);

    void
    gas_diffusion_soil_atm(
                           size_t const _fl_cnt,
                           bool const & /*_have_plant_diff*/,
                           double const &_D_x,
                           lvector_t< double > &_value_fl,
                           lvector_t< double > &_value_sl,
                           double &_out_floor,
                           double &_out_ground,
                           double & /*_out_plant*/,
                           double const &_dirichlet,
                           double const &/*_atmos*/);

    lerr_t
    pertubation(
                double const &_D_x,
                lvector_t< double > &_D_eff_bulk_sl,
                lvector_t< double > &_value_sl,
                lvector_t< double > &_bulk,
                lvector_t< double > &_interface_delta_x_sl);

    void
    liq_diffusion(
                         double const &_D_x,
                         lvector_t< double > &_D_eff_bulk_sl,
                         lvector_t< double > &_value_wl,
                         lvector_t< double > &_value_sl,
                         lvector_t< double > &_bulk,
                         lvector_t< double > &_interface_delta_x_sl,
                         double &_out,
                         boundary_condition const &_upper_bc,
                         boundary_condition const &_lower_bc,
                         double const &_dirichlet);

    void
    calculate_soil_fluxes(
                          size_t /* number of soil layers */,
                          double /* diffusion coefficient */,
                          double /* dirichlet boundary condition value */,
                          boundary_condition const & /* boundary condition */,
                          lvector_t< double > const & /* bulk volume */,
                          lvector_t< double > const & /* effective diffusion coefficient */,
                          lvector_t< double > const & /* interface / delta x */,
                          lvector_t< double > const & /* considered state variable */,
                          lvector_t< double > & /* calculated flux output */);

    void
    calculate_soil_water_fluxes(
                          size_t /* number of soil layers */,
                          double /* diffusion coefficient */,
                          lvector_t< double > const & /* bulk volume */,
                          lvector_t< double > const & /* effective diffusion coefficient */,
                          lvector_t< double > const & /* interface / delta x */,
                          lvector_t< double > const &_val_wl /* considered state variable in water layers */,
                          lvector_t< double > const & /* considered state variable */,
                          double const & /*t-1 value of state variable in lowest sublayer*/,
                          lvector_t< double > & /* calculated flux output */);

    void
    calculate_soil_perturbation_fluxes(
                          size_t /* number of soil layers */,
                          double /* diffusion coefficient */,
                          lvector_t< double > const & /* bulk volume */,
                          lvector_t< double > const & /* effective diffusion coefficient */,
                          lvector_t< double > const & /* interface / delta x */,
                          lvector_t< double > const & /* considered state variable */,
                          double const & /*t-1 value of state variable in lowest sublayer*/,
                          lvector_t< double > & /* calculated flux output */);

    void
    diffusion_assemble_soil_water_matrix(
                                         double const &_D_x,
                                         lvector_t< double > &_D_eff_bulk_sl,
                                         lvector_t< double > &_source_wl,
                                         lvector_t< double > &_source_sl,
                                         lvector_t< double > &_bulk_sl,
                                         lvector_t< double > &_interface_delta_x_sl,
                                         double &_out,
                                         double const &_dirichlet);

    lerr_t
    leach_up_and_down( double &,
                       double &,
                       double &,
                       double);

private:

    /*!
     * @brief
     *
     */
    void
    add_plant_diffusion(
                        boundary_condition const & /* upper boundary condition */,
                        double const & /* atmospheric concentration */);

    void
    diffusion_assemble_soil_matrix(
                                   double const & /* diffusion coefficient */,
                                   lvector_t< double > & /* effective diffusion coefficient */,
                                   lvector_t< double > & /* substance */,
                                   lvector_t< double > & /* bulk */,
                                   double const &  /* interface of upper soil layer */,
                                   lvector_t< double > & /* interface */,
                                   double & /* out/in flow */,
                                   boundary_condition const & /* upper boundary condition*/,
                                   boundary_condition const & /* lower boundary condition*/,
                                   double const & /* dirichlet boundary condition*/);

    void
    diffusion_assemble_soil_atm_matrix(
                                       size_t const ,
                                       double const &,
                                       lvector_t< double > &,
                                       lvector_t< double > &,
                                       lvector_t< double > &,
                                       lvector_t< double > &,
                                       lvector_t< double > &,
                                       double &,
                                       double const &);


    /*!
     * @brief
     */
    void
    diffusion_end_soil(
                       unsigned int,
                       lvector_t< double > &,
                       double &);

    /*!
     * @brief
     */
    void
    diffusion_end_soil_atm(
                           size_t const _fl_cnt,
                           lvector_t< double > &,
                           lvector_t< double > &,
                           double &);

    /*!
     * @brief
     */
    void
    diffusion_end_soil_water(
                             lvector_t< double > &,
                             lvector_t< double > &,
                             double &);

    /*!
     * @brief
     */
    double
    get_out_floor(
                  double const & /*diffusion constant*/,
                  lvector_t< double > &/*effective diffusion constant*/);


    /*!
     * @brief
     */
    double
    get_out_plant(
                  boundary_condition const &,
                  double const &);

public:

    /*!
     * @brief
     */
    void
    leach_down_epsilon(
                       double & _val_mid,
                       double & _val_down,
                       double const &_transport_factor,
                       double const &_epsilon);

    /*!
     * @brief
     */
    void
    leach_up_and_down_epsilon(
                              double & _val_up,
                              double & _val_mid,
                              double & _val_down,
                              double const &_transport_factor,
                              double const &_epsilon);
};



/*!
 * @brief
 *      transports substances downwards
 */
#define LEACH_DOWN(__val_mid__,__val_down__,__transport_fact__)     \
{                                                                   \
if ( cbm::flt_greater_zero( (__val_mid__)))                       \
{                                                                   \
double const leach_value( (__val_mid__) * (__transport_fact__));    \
(__val_mid__) -= (leach_value);                                     \
(__val_down__) += (leach_value);                                    \
}                                                                   \
else                                                                \
{                                                                   \
(__val_mid__) = 0.0;                                                \
}                                                                   \
}                                                                   \



/*!
 * @brief
 *      transports substances upwards and downwards
 */
#define LEACH_UP_AND_DOWN(__val_up__,__val_mid__,__val_down__,__transport_fact__)   \
{                                                                                   \
if ( cbm::flt_greater_zero( (__val_mid__)))                                         \
{                                                                                   \
if ( cbm::flt_greater_zero( (__transport_fact__)))                                  \
{                                                                                   \
double const leach_value( (__val_mid__) * (__transport_fact__));                    \
(__val_mid__) -= (leach_value);                                                     \
(__val_down__) += (leach_value);                                                    \
}                                                                                   \
else                                                                                \
{                                                                                   \
double const leach_value( (__val_mid__) * (__transport_fact__) * (-1));             \
(__val_mid__) -= (leach_value);                                                     \
(__val_up__) += (leach_value);                                                      \
}                                                                                   \
}                                                                                   \
else                                                                                \
{                                                                                   \
(__val_mid__) = 0.0;                                                                \
}                                                                                   \
}                                                                                   \

} /*namespace ldndc*/

#endif  /*  !LM_SOILCHEMISTRY_TRANSPORT_H_  */
