/*!
 * @brief
 * 
 * @author
 *    david kraus (created on: may 6, 2014)
 */

#include  "soilchemistry/ld_allocatelitter.h"

#include  <logging/cbm_logging.h>
#include  <math/cbm_math.h>

lerr_t
ldndc::allocate_litter(
                double  _c, double  _n,
                double  _RCNRVL, double  _RCNRL, double  _RCNRR,
                double  _C[],
                double &  _n_diff) 
{
    ldndc_assert( _n >= 0.0);
    
    if (cbm::flt_equal_zero(_n))
    {
        _C[CPOOL_VL] = 0.0;
        _C[CPOOL_L] = 0.0;
        _C[CPOOL_R] = 0.0;
        _C[CPOOL_B] = _c;
        _n_diff = 0.0;
    }
    else
    {
        ldndc_assert(( _RCNRVL > 0.0) && ( _RCNRL > 0.0) && ( _RCNRR > 0.0));
        double const  cn( _c / _n);
        
        if ( cn < _RCNRVL)
        {
            /* cn \in (0,rL) */
            _C[CPOOL_VL] = _c;
            _C[CPOOL_L] = 0.0;
            _C[CPOOL_R] = 0.0;
            _C[CPOOL_B] = 0.0;
            _n_diff = ( _n - _c/_RCNRVL);
        }
        else if (( cn >= _RCNRVL) && ( cn < _RCNRL))
        {
            /* cn \in [rL,r) */
            double  rr_l_r( 2.0 * _RCNRR * _RCNRL / ( _RCNRR + _RCNRL));
            double  addrr( _c * ( rr_l_r - cn) / ( rr_l_r - _RCNRVL) * ( _RCNRVL / cn));
            _C[CPOOL_VL] = ( 1.25 * addrr - _c * 0.25);
            _C[CPOOL_L] = ( _c - addrr);
            _C[CPOOL_R] = (( _c - addrr) * 0.25);
            _C[CPOOL_B] = 0.0;
            _n_diff = 0.0;
        }
        else if (( cn >= _RCNRL) && ( cn <= _RCNRR))
        {
            /* cn \in [r,rR] */
            double  rr_vl_l( 2.0 * _RCNRVL * _RCNRL / ( _RCNRVL + _RCNRL));
            double  addrr( _c * ( rr_vl_l - cn) / (rr_vl_l - _RCNRR) * ( _RCNRR / cn));
            _C[CPOOL_VL] = (( _c - addrr) * 0.25);
            _C[CPOOL_L] = ( _c - addrr);
            _C[CPOOL_R] = ( _c - ( _C[0] + _C[1]));
            _C[CPOOL_B] = 0.0;
            _n_diff = 0.0;
        }
        else
        {
            /* cn \in (rR,\inf) */
            _C[CPOOL_VL] = 0.0;
            _C[CPOOL_L] = 0.0;
            _C[CPOOL_R] = _n * _RCNRR;
            _C[CPOOL_B] = _c - _n * _RCNRR;
            _n_diff = 0.0;
        }    
    }
    
    return  LDNDC_ERR_OK;
}
