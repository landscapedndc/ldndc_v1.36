/*!
 * @file
 * 
 * @author
 *    david kraus (created on: may 6, 2014)
 */

#ifndef  LD_SAPWOODFRACTION_H_
#define  LD_SAPWOODFRACTION_H_

#include  "ld_modelsconfig.h"

namespace ldndc {

/*!
 * @brief sap_wood_fraction
 * The calculation of the fraction of sapwood in coarse roots is based on the
 * idealized volume fraction and assumes that the stem in the crown as well as the
 * coarse roots are shaped as cones, and that core wood reaches a third of the cone
 *
 */
double LDNDC_API sap_wood_fraction(
            double _lai_max, double _qsfa,
            double _height_min, double _height_max,
            double _depth);
}

#endif  /*  !LD_SAPWOODFRACTION_H_  */

