/*!
 * @file
 * 
 * @author
 *  David Kraus
 * @date
 *  Sep, 2021
 */

#ifndef  LD_ENHANCEDEFFICIENCYNITROGENFERTILIZERS_H_
#define  LD_ENHANCEDEFFICIENCYNITROGENFERTILIZERS_H_

#include  "ldndc.h"
#include  "state/mbe_state.h"

namespace ldndc {

class LDNDC_API ControlledReleaseNitrogenFertilizer
{
    static const double T_REF;

public:
    ControlledReleaseNitrogenFertilizer( double /* controlled release q10 */,
                                         double /* controlled release lag period */,
                                         double /* controlled release t80 */,
                                         double /* controlled release wfps factor */,
                                         double /* maximum nitrification inhibition */,
                                         double /* nitrification inhibition stability_1 */,
                                         double /* nitrification inhibition stability_2 */,
                                         double /* nitrification inhibition dilution */);
    ~ControlledReleaseNitrogenFertilizer();

    /**********************/
    /* controlled release */
    /**********************/

    double const EEF_CONTROLLED_RELEASE_Q10;
    double const EEF_CONTROLLED_RELEASE_LAG_PERIOD;
    double const EEF_CONTROLLED_RELEASE_T80;
    double const EEF_CONTROLLED_RELEASE_WFPS_FACTOR;

    struct crnf_fertilizer
    {
        crnf_fertilizer( double /* amount */);
        ~crnf_fertilizer();
        
        double n_crnf;
        double n_crnf_start;

        //
        double t_eff_crnf;
    };

    std::vector< crnf_fertilizer > crnf;

public:
    lerr_t
    add_crnf( double /* amount */);

    lerr_t
    update_crnf( double /* water filled pore space */,
                 double /* temperature */,
                 double /* scaling due to time resolution */,
                 double /* scaling due to vertical distribution */);

    double
    get_crnf_teff_mean();

    double
    get_crnf_release( double /* amount */);

    double
    get_crnf_release_fraction( double);

    double
    get_crnf_release_fraction_mean();

    /****************************/
    /* nitrification inhibition */
    /****************************/
public:
    lerr_t
    add_nitrification_inhibitior( double /* amount */);

    lerr_t
    update_nitrification_inhibition( double   /* temperature */,
                                     double   /* total soil inhibitor */,
                                     double & /* layer specific inhibitor */);

    double
    get_nitrification_inhibition( double /* layer specific inhibitor */);

    double
    get_ni_stability();

    double
    get_ni_dilution( double /* layer specific inhibitor */);

private:

    double const EEF_MAXIMUM_NITRIFICATION_INHIBITION;
    double const EEF_NITRIFICATION_INHIBITION_STABILITY_1;
    double const EEF_NITRIFICATION_INHIBITION_STABILITY_2;
    double const EEF_NITRIFICATION_INHIBITION_DILUTION;

    double cumulative_truncated_soil_temperature;   // [oC]
    double ni_start;                                // [kg N m-2];
};

}

#endif  /*  !LD_ENHANCEDEFFICIENCYNITROGENFERTILIZERS_H_  */

