

#ifndef  LM_SOILCHEMISTRY_SIMPLICITY_H_
#define  LM_SOILCHEMISTRY_SIMPLICITY_H_

#include  "mbe_legacymodel.h"
#include  "state/mbe_state.h"

#include  "eventhandler/fertilize/fertilize.h"
#include  "eventhandler/till/till.h"

#include  <input/event/events.h>

namespace ldndc {

class  LDNDC_API  SoilChemistrySimplicity  :  public  MBE_LegacyModel
{
    LMOD_EXPORT_MODULE_INFO(SoilChemistrySimplicity,"soilchemistry:simplicity","Soilchemistry SIMPLICITY");
    public:
        SoilChemistrySimplicity( MoBiLE_State *,
                cbm::io_kcomm_t *, timemode_e);

        ~SoilChemistrySimplicity();

        lerr_t  configure( ldndc::config_file_t const *);
        lerr_t  initialize();

        lerr_t  register_ports( cbm::io_kcomm_t *);
        lerr_t  unregister_ports( cbm::io_kcomm_t *);

        lerr_t  solve();
        lerr_t  finalize() { return  LDNDC_ERR_OK; }

        lerr_t  sleep() { return  LDNDC_ERR_OK; }
        lerr_t  wake() { return  LDNDC_ERR_OK; }

    private:
        substate_soilchemistry_t &  m_sc;
        EventHandlerFertilize  m_eventfertilize;
};

} /*namespace ldndc*/

#endif /* !LM_SOILCHEMISTRY_SIMPLICITY_H_ */

