/*!
 * @brief
 * 
 */

#include  "soilchemistry/simplicity/soilchemistry-simplicity.h"

#include  <constants/cbm_const.h>

LMOD_MODULE_INFO(SoilChemistrySimplicity,TMODE_SUBDAILY|TMODE_POST_DAILY,LMOD_FLAG_USER);
namespace ldndc {


SoilChemistrySimplicity::SoilChemistrySimplicity(
        MoBiLE_State *  _state, cbm::io_kcomm_t *  _io,
        timemode_e  _timemode) : MBE_LegacyModel(
            _state, _timemode),

        m_sc( *_state->get_substate< substate_soilchemistry_t >()),
        m_eventfertilize( _state, _io, _timemode)
{}


SoilChemistrySimplicity::~SoilChemistrySimplicity()
{}


lerr_t
SoilChemistrySimplicity::configure(
        ldndc::config_file_t const *)
{
    this->m_eventfertilize.set_object_id( this->object_id());

    return  LDNDC_ERR_OK;
}


lerr_t
SoilChemistrySimplicity::initialize()
{
    m_sc.nh4_sl = 0.0;
    m_sc.don_sl = 0.0;
    m_sc.no3_sl = 0.0;
    m_sc.an_no3_sl = 0.0;

    return  LDNDC_ERR_OK;
}



lerr_t
SoilChemistrySimplicity::register_ports( cbm::io_kcomm_t *  _io_kcomm)
{
    lerr_t rc = this->m_eventfertilize.register_ports( _io_kcomm);
    if ( rc)
    {
        return LDNDC_ERR_FAIL;
    }

    return rc;
}



lerr_t
SoilChemistrySimplicity::unregister_ports( cbm::io_kcomm_t *  _io_kcomm)
{
    lerr_t rc = this->m_eventfertilize.unregister_ports( _io_kcomm);
    if ( rc)
    {
        return LDNDC_ERR_FAIL;
    }

    return LDNDC_ERR_OK;
}



/*!
 * @brief
 *    kicks off computation for one time step
 */
lerr_t
SoilChemistrySimplicity::solve()
{
    //fertilize (needs to be removed as soon as soilchemistry modules are in the game)
    bool have_surfacewater( false);
    lerr_t rc_fert = m_eventfertilize.event_fertilize( have_surfacewater);
    if ( rc_fert)
    { return  LDNDC_ERR_FAIL;}

   // KLOGERROR("m_sc.urea_sl.sum()  ", m_sc.urea_sl.sum() );
    if ( m_sc.urea_sl.sum() > 0.0)
    {
        m_sc.nh4_sl[0] += m_sc.urea_sl.sum();
        m_sc.urea_sl = 0.0;
    }

    //0.8 soil supply
    m_sc.don_sl = 0.0;
    m_sc.don_sl[0] = 0.8 * cbm::HA_IN_M2;

    return  LDNDC_ERR_OK;
}


} /*namespace ldndc*/

