/*!
 * @file
 * 
 * @author
 *    david kraus (created on: may 6, 2014)
 */

#ifndef  LD_ALLOCATELITTER_H_
#define  LD_ALLOCATELITTER_H_

#include  "ld_modelsconfig.h"

namespace ldndc {

/*!
 * @brief
 *    Order of carbon pools in array passed to allocate_litter
 */
enum
{
    /*! very labile */
    CPOOL_VL,
    /*! labile */
    CPOOL_L,
    /*! recalcitrant */
    CPOOL_R,
    /*! wood */
    CPOOL_B,
    
    CPOOL_CNT
};



/*!
 * @brief
 *     calculates the amount of C which is added to the very labile, labile and
 *     recalcitrant decomposition pool
 *
 *     The needed information in this function are the amount of carbon in
 *    litter @p _c and the corresponding CN ratio (determined from nitrogen @p _n).
 *
 *     four cases:
 *     If the CN ratio is lower than the CN ratio of the very labile litter pool,
 *    all litter-C goes to the very labile decomposition pool.
 *
 *    If the CN ratio is between RCNRVL and the harmonic mean of RCNRVL and RCNRR,
 *     an increasing CN ratio decreases @p C_0 and increases @p C_1 and @p C_2.
 *
 *     If thc CN ratio is between the harmonic mean of RCNRVL and RCNRR and RCNRR,
 *    @p C_1 and @p C_2 decrease and only @p C_0 increases.
 *
 *     If CN ratio is higher than RCNRR, all Litter-C goes to @p C_2.
 *
 * @param  _c
 *    Carbon in litter flux
 * @param  _n
 *    Nitrogen in litter flux
 * @param  _C
 *    Organic carbon that is added to the
 *     - very labile,
 *     - labile,
 *     - recalcitrant
 *    decomposition pool (kgC ha-1)
 * @param  _n_diff
 *
 * @note
 *    rr_vl_l
 *        harmonic mean of RCNRL and RCNRVL; used to calculate weights how addC is partitioned into C1,C2 and C3
 *    rr_l_r
 *        harmonic mean of RCNRL and RCNRR
 */
lerr_t LDNDC_API allocate_litter(
                                 double  _c, double  _n,
                                 double  _RCNRVL, double  _RCNRL, double  _RCNRR,
                                 double  _C[],
                                 double &  _n_diff);
}

#endif  /*  !LD_ALLOCATELITTER_H_  */

