/*!
 * @brief
 *
 * @author
 *    David Kraus (created on: oct 23, 2015)
 */

#include  "ld_transport.h"


/*!
 * @brief
 *
 */
Transport::Transport(MoBiLE_State *_state, cbm::io_kcomm_t *_io) :
                        sc_( _state->get_substate_ref< substate_soilchemistry_t >()),
                        sb_( _state->get_substate_ref< substate_surfacebulk_t >()),
                        ph_( _state->get_substate_ref< substate_physiology_t >()),
                        se_( _io->get_input_class_ref< input_class_setup_t >()),
                        sl_( _io->get_input_class_ref< input_class_soillayers_t >()),
                        tdma_solver_( solver_tridiagonal_t(sl_.soil_layer_cnt()+se_.canopylayers()+sb_.surfacebulk_layer_cnt()+1)),
                        d_eff_plant_sl_( sl_.soil_layer_cnt(), 0.0),
                        d_eff_air_sl_( sl_.soil_layer_cnt(), 0.0),
                        d_eff_air_fl_( se_.canopylayers(), 5.0),
                        v_air_sl_( sl_.soil_layer_cnt(), 0.0),
                        soil_gas_advection_sl_( sl_.soil_layer_cnt(), 0.0),
                        interface_air_sl_( sl_.soil_layer_cnt(), 0.0),
                        interface_delta_x_air_sl_( sl_.soil_layer_cnt(), 0.0),
                        fact_uc_xl_( sl_.soil_layer_cnt()+se_.canopylayers()+sb_.surfacebulk_layer_cnt()+1, 0.0),
                        interface_delta_x_atm_soil_( 0.0),
                        interface_delta_x_fl_soil_( 0.0),
                        interface_delta_x_sbl_soil_( 0.0),
                        have_water_table_( false),
                        h_wl_( 0.0),
                        wind_stimulation( 1.0)
{}



/*!
 * @brief
 *
 */
Transport::~Transport()
{}



/*!
 * @brief
 *
 */
void
Transport::update_diffusion(
                  lvector_t< double > const &_d_eff_plant_sl,
                  lvector_t< double > const &_d_eff_air_sl,
                  lvector_t< double > const &_v_air_sl,
                  lvector_t< double > const &_soil_gas_advection_sl,
                  lvector_t< double > const &_interface_delta_x_air_sl,
                  double _interface_delta_x_atm_soil,
                  double _interface_delta_x_fl_soil,
                  double _interface_delta_x_sbl_soil,
                  double _h_wl,
                  bool _have_water_table)
{
    for( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
    {
        d_eff_plant_sl_[sl] = _d_eff_plant_sl[sl];
        d_eff_air_sl_[sl] = _d_eff_air_sl[sl];
        interface_delta_x_air_sl_[sl] = _interface_delta_x_air_sl[sl];
        v_air_sl_[sl] = _v_air_sl[sl];
        soil_gas_advection_sl_[sl] = _soil_gas_advection_sl[sl];
    }

    interface_delta_x_atm_soil_ = _interface_delta_x_atm_soil;
    interface_delta_x_fl_soil_ = _interface_delta_x_fl_soil;
    interface_delta_x_sbl_soil_ = _interface_delta_x_sbl_soil;

    have_water_table_ = _have_water_table;
    h_wl_ = _h_wl;
}



/*!
 * @brief
 *
 */
void
Transport::update_advection(
                            lvector_t< double > const &_interface_air_sl)
{
    for( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
    {
        interface_air_sl_[sl] = _interface_air_sl[sl];
    }
}



/*!
 * @brief
 *
 */
void
Transport::add_plant_diffusion(
                               boundary_condition const &_upper_bc,
                               double const &_atmos)
{
    unsigned int xl( (_upper_bc == dirichlet) ? 1 : 0);
    for( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
    {
        double const eta( d_eff_plant_sl_[sl] / v_air_sl_[sl]);

        tdma_solver_.a1[xl] += eta;
        tdma_solver_.b[xl] += (eta * _atmos);
        xl++;
    }
}



/*!
 * @brief
 *
 */
void
Transport::gas_diffusion_soil_atm(
                                  size_t const _fl_cnt,
                                  bool const & /*_have_plant_diff*/,
                                  double const &_D_x,
                                  lvector_t< double > &_value_fl,
                                  lvector_t< double > &_value_sl,
                                  double &_out_floor,
                                  double &_out_ground,
                                  double & /*_out_plant*/,
                                  double const &_dirichlet,
                                  double const &/*_atmos*/)
{
    double out_floor_ground( 0.0);        // helper variable for differentiation inbetween plant floor and ground diffusion

    /* Assemble diffusion matrix coefficients */
    diffusion_assemble_soil_atm_matrix(_fl_cnt, _D_x, d_eff_air_sl_, _value_fl, _value_sl, interface_delta_x_air_sl_, v_air_sl_, out_floor_ground, _dirichlet);

    tdma_solver_.solve_in_place(NULL, sl_.soil_layer_cnt()+_fl_cnt+1);


    /* Reconversion of units and input/output determination */

    double out_floor( _D_x * d_eff_air_fl_[_fl_cnt-1] * (tdma_solver_.x(1) - tdma_solver_.x(0)) / ph_.h_fl[_fl_cnt-1]);

    diffusion_end_soil_atm(_fl_cnt, _value_fl, _value_sl, out_floor_ground);

    _out_floor += out_floor;
    _out_ground += (out_floor_ground - out_floor);        // all excess gas is assumed to be exchanged with floor
}



/*!
 * @brief
 *
 */
void
Transport::gas_diffusion_soil(
                              bool const &_have_plant_diff,
                              double const &_D_x,
                              lvector_t< double > &_D_eff_bulk_sl,
                              lvector_t< double > &_value_sl,
                              lvector_t< double > &_bulk,
                              lvector_t< double > &_interface_delta_x_sl,
                              double &_out_floor,
                              double &_out_ground,
                              double &_out_plant,
                              boundary_condition const &_upper_bc,
                              boundary_condition const &_lower_bc,
                              double const &_dirichlet,
                              double const &_atmos)
{
    double out_floor_ground( 0.0);  // helper variable distinguishing plant, floor and ground diffusion

    /* Assemble diffusion matrix coefficients */
    diffusion_assemble_soil_matrix( _D_x, _D_eff_bulk_sl, _value_sl, _bulk,
                                    interface_delta_x_atm_soil_, _interface_delta_x_sl,
                                    out_floor_ground, _upper_bc, _lower_bc, _dirichlet);

    double out_plant( 0.0);
    if ( _have_plant_diff)
    {
        add_plant_diffusion( _upper_bc, _atmos);
        tdma_solver_.solve_in_place(NULL, sl_.soil_layer_cnt()+1);
        out_plant = get_out_plant(_upper_bc, _atmos);
        _out_plant += out_plant;
    }
    else
    {
        tdma_solver_.solve_in_place(NULL, sl_.soil_layer_cnt()+1);
    }

    diffusion_end_soil((_upper_bc == dirichlet) ? 1 : 0, _value_sl, out_floor_ground);
    double const out_floor( get_out_floor( _D_x, _D_eff_bulk_sl));

    _out_floor += out_floor;
    _out_ground += (out_floor_ground - out_plant - out_floor);    // all excess gas exchanged with ground
}



/*!
 * @brief
 */
void
Transport::leach_down_epsilon(
                              double & _val_mid,
                              double & _val_down,
                              double const &_transport_factor,
                              double const &_epsilon)
{
    if ( cbm::flt_greater_zero( _val_mid - _epsilon))
    {
        double const leach_value( (_val_mid - _epsilon) * _transport_factor);
        _val_mid -= leach_value;
        _val_down += leach_value;
    }
}



/*!
 * @brief
 */
void
Transport::leach_up_and_down_epsilon(
                                     double & _val_up,
                                     double & _val_mid,
                                     double & _val_down,
                                     double const &_transport_factor,
                                     double const &_epsilon)
{
    if ( cbm::flt_greater_zero( _val_mid - _epsilon))
    {
        if ( cbm::flt_greater_zero( _transport_factor))
        {
            double const leach_value( (_val_mid - _epsilon) * _transport_factor);
            _val_mid -= leach_value;
            _val_down += leach_value;
        }
        else
        {
            double const leach_value( (_val_mid - _epsilon) * _transport_factor * (-1.0));
            _val_mid -= leach_value;
            _val_up += leach_value;
        }
    }
}



/*!
 * @brief
 *
 */
#define FACT_UC_SOIL                            \
{                                               \
fact_uc_xl_[xl] = _bulk_sl[sl];                 \
_out += _source_sl[sl];                         \
}                                               \

#define FACT_UC_WATER                           \
{                                               \
fact_uc_xl_[xl] = h_wl_;                        \
_out += _source_wl[wl];                         \
}

#define FACT_UC_ATM                             \
{                                               \
fact_uc_xl_[xl] = ph_.h_fl[fl];                 \
_out += _source_fl[fl];                         \
}


#define D_X_0 ( _D_x * _D_eff_bulk_sl[sl])
#define D_X_1 ( cbm::harmonic_mean2(_D_x * _D_eff_bulk_sl[sl-1], D_X_0))
#define D_X_2 ( cbm::harmonic_mean2(D_X_0, _D_x * _D_eff_bulk_sl[sl+1]))
#define D_X_ATM_0 (_D_x * d_eff_air_fl_[fl])
#define D_X_ATM_1 ( cbm::harmonic_mean2(_D_x * d_eff_air_fl_[fl+1], D_X_ATM_0))
#define D_X_ATM_2 ( cbm::harmonic_mean2(D_X_ATM_0, _D_x * d_eff_air_fl_[fl-1]))
#define D_X_ATM_SOIL ( cbm::harmonic_mean2(_D_x * d_eff_air_fl_[0], _D_x * _D_eff_bulk_sl[0]))
#define D_X_SBL ( _D_x * wind_stimulation)
#define D_X_SBL_SOIL ( cbm::harmonic_mean2(_D_x * wind_stimulation, _D_x * _D_eff_bulk_sl[0]))


#define INCREMENT_SOIL(__sl__,__xl__)    \
{                                        \
(__sl__) ++;                             \
(__xl__) ++;                             \
}                                        \

#define INCREMENT_WATER(__wl__,__xl__)   \
{                                        \
(__wl__) --;                             \
(__xl__) ++;                             \
}                                        \

#define INCREMENT_ATM(__fl__,__xl__)     \
{                                        \
(__fl__) --;                             \
(__xl__) ++;                             \
}                                        \

#define RIGHT_HAND_SIDE_SOIL ( _source_sl[sl] / fact_uc_xl_[xl])
#define RIGHT_HAND_SIDE_WATER ( _source_wl[wl] / fact_uc_xl_[xl])
#define RIGHT_HAND_SIDE_ATM ( _source_fl[fl] / fact_uc_xl_[xl])

#define ASSEMBLE_INNER_SOIL_MATRIX                                                            \
{                                                                                             \
{                                                                                             \
FACT_UC_SOIL;                                                                                 \
double tau_e( D_X_1 * _interface_delta_x_sl[sl-1]);                                           \
double tau_w( D_X_2 * _interface_delta_x_sl[sl]);                                             \
\
while( sl < sl_.soil_layer_cnt()-2)                                                           \
{                                                                                             \
double const tau_E( tau_e / _bulk_sl[sl]);                                                   \
double const tau_W( tau_w / _bulk_sl[sl]);                                                   \
tdma_solver_.set_row( xl, -tau_E, (1.0 + tau_E + tau_W), -tau_W, RIGHT_HAND_SIDE_SOIL);   \
INCREMENT_SOIL( sl,xl);                                                                       \
FACT_UC_SOIL;                                                                                 \
tau_e = tau_w;                                                                                \
tau_w = ( D_X_2 * _interface_delta_x_sl[sl]);                                                 \
}                                                                                             \
{                                                                                             \
double const tau_E( tau_e / _bulk_sl[sl]);                                                   \
double const tau_W( tau_w / _bulk_sl[sl]);                                                   \
tdma_solver_.set_row( xl, -tau_E, (1.0 + tau_E + tau_W), -tau_W, RIGHT_HAND_SIDE_SOIL);   \
INCREMENT_SOIL( sl,xl);                                                                       \
FACT_UC_SOIL;                                                                                 \
}                                                                                             \
}                                                                                             \
}                                                                                             \


#define ASSEMBLE_INNER_SOIL_DIFFUSION_ADVECTION_MATRIX                                        \
{                                                                                             \
{                                                                                             \
FACT_UC_SOIL;                                                                                 \
double tau_e( D_X_1 * _interface_delta_x_sl[sl-1]);                                           \
double tau_w( D_X_2 * _interface_delta_x_sl[sl]);                                             \
\
double beta_e( D_X_1 * _interface_delta_x_sl[sl-1]);                                           \
double beta_w( D_X_2 * _interface_delta_x_sl[sl]);                                             \
\
while( sl < sl_.soil_layer_cnt()-2)                                                           \
{                                                                                             \
double const tau_E( tau_e / _bulk_sl[sl]);                                                   \
double const tau_W( tau_w / _bulk_sl[sl]);                                                   \
tdma_solver_.set_row( xl, -tau_E, (1.0 + tau_E + tau_W), -tau_W, RIGHT_HAND_SIDE_SOIL);   \
INCREMENT_SOIL( sl,xl);                                                                       \
FACT_UC_SOIL;                                                                                 \
tau_e = tau_w;                                                                                \
tau_w = ( D_X_2 * _interface_delta_x_sl[sl]);                                                 \
}                                                                                             \
{                                                                                             \
double const tau_E( tau_e / _bulk_sl[sl]);                                                   \
double const tau_W( tau_w / _bulk_sl[sl]);                                                   \
tdma_solver_.set_row( xl, -tau_E, (1.0 + tau_E + tau_W), -tau_W, RIGHT_HAND_SIDE_SOIL);   \
INCREMENT_SOIL( sl,xl);                                                                       \
FACT_UC_SOIL;                                                                                 \
}                                                                                             \
}                                                                                             \
}                                                                                             \


/*!
 * @brief
 *
 */
void
Transport::diffusion_assemble_soil_matrix(
                                          double const &_D_x,
                                          lvector_t< double > &_D_eff_bulk_sl,
                                          lvector_t< double > &_source_sl,
                                          lvector_t< double > &_bulk_sl,
                                          double const &_interface_delta_x_0,
                                          lvector_t< double > &_interface_delta_x_sl,
                                          double &_out,
                                          boundary_condition const &_upper_bc,
                                          boundary_condition const &_lower_bc,
                                          double const &_dirichlet)
{
    ldndc_kassert( sl_.soil_layer_cnt() > 1);
    size_t sl( 0);
    size_t xl( 0);

    if ( _upper_bc == dirichlet)
    {
        /* Additional layer for dirichlet boundary condition */
        tdma_solver_.set_row( xl, 0.0, 1.0, 0.0, _dirichlet);
        xl++;

        /* Uppest soil layer is no boundary */
        FACT_UC_SOIL;
        double const tau_1( D_X_0 * _interface_delta_x_0 / _bulk_sl[sl]);
        double const tau_2( D_X_2 * _interface_delta_x_sl[sl] / _bulk_sl[sl]);
        tdma_solver_.set_row( xl, -tau_1, (1.0 + tau_1 + tau_2), -tau_2, RIGHT_HAND_SIDE_SOIL);
        INCREMENT_SOIL(sl, xl);
    }
    else
    {
        /* Uppest soil layer for no-flow boundary condition */
        FACT_UC_SOIL;
        double const tau_2( D_X_2 * _interface_delta_x_sl[sl] / _bulk_sl[sl]);
        tdma_solver_.set_row( xl, 0.0, (1.0 + tau_2), -tau_2, RIGHT_HAND_SIDE_SOIL);
        INCREMENT_SOIL(sl, xl);
    }

    /* Inner soil layers */
    ASSEMBLE_INNER_SOIL_MATRIX;


    /* Lower boundary condition */
    if ( _lower_bc == dirichlet)
    {
        /* minimum is taken to prevent inflow from below domaine */
        tdma_solver_.set_row( xl, 0.0, 1.0, 0.0, std::min(_source_sl[sl], _source_sl[sl-1]) / fact_uc_xl_[xl]);
    }
    else
    {
        /* always set to no-flow neuman boundary condition */
        double const tau_1( D_X_1 * _interface_delta_x_sl[sl-1] / _bulk_sl[sl]);
        tdma_solver_.set_row( xl, -tau_1, (1.0 + tau_1), 0.0, RIGHT_HAND_SIDE_SOIL);
    }
}



/*!
 * @brief
 *
 */
void
Transport::diffusion_end_soil(
                              unsigned int _xl,
                              lvector_t< double > &_value_sl,
                              double &_out_soil)
{
    for( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
    {
        _value_sl[sl] = (tdma_solver_.x(_xl) * fact_uc_xl_[_xl]);
        _out_soil -= _value_sl[sl];
        _xl++;
    }
}



/*!
 * @brief
 *
 */
void
Transport::diffusion_assemble_soil_atm_matrix(
                                              size_t const _fl_cnt,
                                              double const &_D_x,
                                              lvector_t< double > &_D_eff_bulk_sl,
                                              lvector_t< double > &_source_fl,
                                              lvector_t< double > &_source_sl,
                                              lvector_t< double > &_interface_delta_x_sl,
                                              lvector_t< double > &_bulk_sl,
                                              double &_out,
                                              double const &_dirichlet)
{
    ldndc_kassert( sl_.soil_layer_cnt() > 1);
    size_t fl( _fl_cnt);
    size_t sl( 0);
    size_t xl( 0);

    /* Additional layer for dirichlet boundary condition */
    tdma_solver_.set_row( xl, 0.0, 1.0, 0.0, _dirichlet);
    INCREMENT_ATM( fl, xl);

    /* Uppest atmospheric layer */
    {
        FACT_UC_ATM;
        double const tau_1( D_X_ATM_0 / (ph_.h_fl[fl] * ph_.h_fl[fl]));
        double const tau_2( D_X_ATM_2 / ((0.5 * (ph_.h_fl[fl-1] + ph_.h_fl[fl])) * ph_.h_fl[fl]));
        tdma_solver_.set_row( xl, -tau_1, (1.0 + tau_1 + tau_2), -tau_2, RIGHT_HAND_SIDE_ATM);
        INCREMENT_ATM( fl, xl);
    }

    /* Inner atmospheric layer */
    for ( size_t  l = _fl_cnt-2;  l > 0;  --l)
    {
        FACT_UC_ATM;
        double const tau_1( D_X_ATM_1 / (0.5 * (ph_.h_fl[fl+1] + ph_.h_fl[fl]) * ph_.h_fl[fl]));
        double const tau_2( D_X_ATM_2 / (0.5 * (ph_.h_fl[fl-1] + ph_.h_fl[fl]) * ph_.h_fl[fl]));
        tdma_solver_.set_row( xl, -tau_1, (1.0 + tau_1 + tau_2), -tau_2, RIGHT_HAND_SIDE_ATM);
        INCREMENT_ATM( fl, xl);
    }

    /* Lowest atmospheric layer */
    {
        FACT_UC_ATM;
        double const tau_1( D_X_ATM_1 / (0.5 * (ph_.h_fl[fl+1] + ph_.h_fl[fl]) * ph_.h_fl[fl]));
        double const tau_2( D_X_ATM_SOIL * interface_delta_x_fl_soil_ / ph_.h_fl[fl]);
        tdma_solver_.set_row( xl, -tau_1, (1.0 + tau_1 + tau_2), -tau_2, RIGHT_HAND_SIDE_ATM);
        INCREMENT_ATM(fl, xl);
    }

    /* Uppest soil layer */
    {
        FACT_UC_SOIL
        double const tau_1( D_X_ATM_SOIL * interface_delta_x_fl_soil_ / _bulk_sl[sl]);
        double const tau_2( D_X_2 * _interface_delta_x_sl[sl] / _bulk_sl[sl]);
        tdma_solver_.set_row( xl, -tau_1, (1.0 + tau_1 + tau_2), -tau_2, RIGHT_HAND_SIDE_SOIL);
        INCREMENT_SOIL(sl, xl);
    }

    /* Inner soil layers */
    ASSEMBLE_INNER_SOIL_MATRIX;

    /* Lower boundary condition */
    double const tau_1( D_X_1 * _interface_delta_x_sl[sl-1] / v_air_sl_[sl]);
    tdma_solver_.set_row( xl, -tau_1, (1.0 + tau_1), 0.0, RIGHT_HAND_SIDE_SOIL);
}



/*!
 * @brief
 *
 */
void
Transport::diffusion_end_soil_atm(
                                  size_t const _fl_cnt,
                                  lvector_t< double > &_value_fl,
                                  lvector_t< double > &_value_sl,
                                  double &_out)
{
    unsigned int xl( 1);

    for ( size_t  fl = _fl_cnt;  fl > 0;  --fl)
    {
        _value_fl[fl-1] = (tdma_solver_.x(xl) * fact_uc_xl_[xl]);
        _out -= _value_fl[fl-1];
        xl++;
    }

    diffusion_end_soil(xl, _value_sl, _out);
}



/*!
 * @brief
 *
 */
void
Transport::diffusion_end_soil_water(
                                    lvector_t< double > &_value_wl,
                                    lvector_t< double > &_value_sl,
                                    double &_out)
{
    unsigned int xl( 0);

    for ( size_t  wl = sb_.surfacebulk_layer_cnt();  wl > 0;  --wl)
    {
        _value_wl[wl-1] = (tdma_solver_.x(xl) * fact_uc_xl_[xl]);
        _out -= _value_wl[wl-1];
        xl++;
    }

    diffusion_end_soil(xl, _value_sl, _out);
}


/*!
 * @brief
 *
 */
double
Transport::get_out_floor(
                         double const &_D_x,
                         lvector_t< double > &_D_eff_bulk_sl)
{
    return ( _D_x * _D_eff_bulk_sl[0] * interface_delta_x_atm_soil_ * (tdma_solver_.x(1) - tdma_solver_.x(0)));
}


/*!
 * @brief
 *
 */
double
Transport::get_out_plant(
                         boundary_condition const &_upper_bc,
                         double const &_atmos)
{
    double out_plant_total( 0.0);
    unsigned int xl( (_upper_bc == dirichlet) ? 1 : 0);
    for( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
    {
        double const out_plant_sl( d_eff_plant_sl_[sl] * (tdma_solver_.x(xl) - _atmos));    // amount of gas exchange via plant diffusion.    [kg]
        out_plant_total += out_plant_sl;
        xl++;
    }

    return ( out_plant_total);
}



/*!
 * @brief
 *
 */
lerr_t
Transport::pertubation(
                       double const &_D_x,
                       lvector_t< double > &_D_eff_bulk_sl,
                       lvector_t< double > &_value_sl,
                       lvector_t< double > &_bulk,
                       lvector_t< double > &_interface_delta_x_sl)
{
    double before( 0.0);

    /* Assemble diffusion matrix coefficients */
    diffusion_assemble_soil_matrix( _D_x, _D_eff_bulk_sl, _value_sl, _bulk,
                                   invalid_dbl, _interface_delta_x_sl,
                                   before, neuman, dirichlet, 0.0);

    /* Solve equation system */
    tdma_solver_.solve_in_place( NULL, sl_.soil_layer_cnt());

    double after( 0.0);
    for( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
    {
        _value_sl[sl] = (tdma_solver_.x( sl) * fact_uc_xl_[sl]);
        after += _value_sl[sl];
    }

    //distribute difference across soil
    //ensuring that no mass is lost/gained across domain boundaries
    if ( cbm::flt_greater_zero( after))
    {
        double const diff( before - after);
        if ( cbm::flt_greater_zero( diff))
        {
            for( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
            {
                double const diff_sl( _value_sl[sl] / after * diff);
                _value_sl[sl] += diff_sl;
            }
        }
        else if ( cbm::flt_less( diff, 0.0))
        {
            for( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
            {
                double const diff_sl( _value_sl[sl] / after * diff);
                _value_sl[sl] = cbm::bound_min( 0.0, _value_sl[sl] + diff_sl);
            }
        }
    }
    else if ( cbm::flt_equal_zero( after))
    {
        if ( cbm::flt_greater_zero( before))
        {
            double add( before / sl_.soil_layer_cnt());
            for( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
            {
                _value_sl[sl] = add;
            }
        }
    }
    else
    {
        LOGERROR("Negative state after pertubation: ", after);
        return LDNDC_ERR_FAIL;
    }

    return LDNDC_ERR_OK;
}



/*!
 * @brief
 *
 */
void
Transport::liq_diffusion(
                         double const &_D_x,
                         lvector_t< double > &_D_eff_bulk_sl,
                         lvector_t< double > &_value_wl,
                         lvector_t< double > &_value_sl,
                         lvector_t< double > &_bulk,
                         lvector_t< double > &_interface_delta_x_sl,
                         double &_out,
                         boundary_condition const &_upper_bc,
                         boundary_condition const &_lower_bc,
                         double const &_dirichlet)
{
    if ( have_water_table_)
    {
        /* Assemble diffusion matrix coefficients */
        diffusion_assemble_soil_water_matrix( _D_x, _D_eff_bulk_sl, _value_wl, _value_sl, _bulk, _interface_delta_x_sl, _out, _dirichlet);

        /* Solve equation system */
        tdma_solver_.solve_in_place( NULL, sl_.soil_layer_cnt()+sb_.surfacebulk_layer_cnt());

        /* Reconversion of units and input/output determination */
        diffusion_end_soil_water(_value_wl, _value_sl, _out);
    }
    else
    {
        /* Assemble diffusion matrix coefficients */
        diffusion_assemble_soil_matrix( _D_x, _D_eff_bulk_sl, _value_sl, _bulk,
                                       invalid_dbl, _interface_delta_x_sl,
                                       _out, _upper_bc, _lower_bc, _dirichlet);

        /* Solve equation system */
        tdma_solver_.solve_in_place(NULL, sl_.soil_layer_cnt());

        /* Reconversion of units and input/output determination */
        diffusion_end_soil(0, _value_sl, _out);    //set fix to neuman for correct reconversion (details see function)
    }
}



/*!
 * @brief
 *
 */
void
Transport::diffusion_assemble_soil_water_matrix(
                                                double const &_D_x,
                                                lvector_t< double > &_D_eff_bulk_sl,
                                                lvector_t< double > &_source_wl,
                                                lvector_t< double > &_source_sl,
                                                lvector_t< double > &_bulk_sl,
                                                lvector_t< double > &_interface_delta_x_sl,
                                                double &_out,
                                                double const &_dirichlet)
{
    ldndc_kassert( sb_.surfacebulk_layer_cnt() > 1);
    ldndc_kassert( h_wl_ > 0.0);

    size_t xl( 0);
    size_t wl( sb_.surfacebulk_layer_cnt()-1);
    size_t sl( 0);

    /* Constants for all water layers */

    double const tau_water( D_X_SBL / (h_wl_ * h_wl_));
    double const tau_water_double( 1.0 + tau_water + tau_water);
    double const tau_water_soil( D_X_SBL_SOIL * interface_delta_x_sbl_soil_ / h_wl_);
    double const tau_soil_water( D_X_SBL_SOIL * interface_delta_x_sbl_soil_ / _bulk_sl[sl]);
    double const tau_soil( D_X_2 * _interface_delta_x_sl[sl] / _bulk_sl[sl]);

    /* Uppest waterlayer layer */
    {
        FACT_UC_WATER;
        tdma_solver_.set_row( xl, 0.0, (1.0 + tau_water), -tau_water, RIGHT_HAND_SIDE_WATER);
        INCREMENT_WATER( wl, xl);
    }

    /* Inner water layer discretization */
    for ( size_t  l = sb_.surfacebulk_layer_cnt()-2;  l > 0;  --l)
    {
        FACT_UC_WATER;
        tdma_solver_.set_row( xl, -tau_water, tau_water_double, -tau_water, RIGHT_HAND_SIDE_WATER);
        INCREMENT_WATER( wl, xl);
    }

    /* Lowest water layer */
    {
        FACT_UC_WATER;
        tdma_solver_.set_row( xl, -tau_water, (1.0 + tau_water + tau_water_soil), -tau_water_soil, RIGHT_HAND_SIDE_WATER);
        INCREMENT_WATER( wl, xl);
    }

    /* Uppest soil layer */
    {
        FACT_UC_SOIL
        tdma_solver_.set_row( xl, -tau_soil_water, (1.0 + tau_soil_water + tau_soil), -tau_soil, RIGHT_HAND_SIDE_SOIL);
        INCREMENT_SOIL( sl, xl);
    }

    /* Inner soil layers */
    ASSEMBLE_INNER_SOIL_MATRIX;

    /* Lower boundary condition */
    tdma_solver_.set_row( xl, 0.0, 1.0, 0.0, _dirichlet);
}



/*!
 * @author
 *      david kraus
 *
 * @brief
 *      calculates diffusive gas fluxes between soil layers
 *      and upper/lower soil layer domaine boundaries.
 *      calculated value for _flux[i] refers to
 *      flux from soil layer [i] to soil layer [i-1]
 *      _flux[0] refers to soil flux from soil domaine into atmosphere
 *      _flux[_sl_max-1] refers to soil flux into soil domaine from belowground
 *
 */
void
Transport::calculate_soil_fluxes(
                                 size_t _sl_max /* number of soil layers */,
                                 double _D_x /* diffusion constant */,
                                 double _dirichlet /* value of dirichlet boundary condition */,
                                 boundary_condition const &_upper_bc /* type of upper boundary condition */,
                                 lvector_t< double > const &_bulk_sl /* volume of bulk */,
                                 lvector_t< double > const &_interface_delta_x_sl /* soil layer interface divided by delta x */,
                                 lvector_t< double > const &_D_eff_bulk_sl /* effective diffusion constant */,
                                 lvector_t< double > const &_val /* considered value */,
                                 lvector_t< double > &_flux /* calculated flux */)
{
    /* calculate inner soil layer fluxes */
    for (size_t sl = 1; sl < _sl_max; ++sl)
    {
        _flux[sl] = D_X_1 * _interface_delta_x_sl[sl-1] * (_val[sl]/_bulk_sl[sl] - _val[sl-1]/_bulk_sl[sl-1]);
    }

    if ( _upper_bc == dirichlet)
    {
        size_t sl( 0);
        _flux[sl] = D_X_0 * interface_delta_x_atm_soil_ * (_val[sl]/_bulk_sl[sl] - _dirichlet);
        _flux[_sl_max] = 0.0;
    }
    else
    {
        size_t sl( 0);
        _flux[sl] = 0.0;
        _flux[_sl_max] = _flux[_sl_max-1];
    }
}


/*!
 * @author
 *      andrew smerald
 *
 * @brief
 *      calculates diffusive fluxes of dissolved substances between soil layers
 *      includes upper/lower soil layer domain boundaries.
 *      positive fluxes refer to downwards transport
 *      _flux[0] refers to flux into top soil layer (from surface water) - still to do
 *      flux [i+1] corresponds to flux from soil layer [i] to [i+1]
 *      _flux[_sl_max] refers to flux out of bottom soil layer
 */
void
Transport::calculate_soil_water_fluxes(
                                 size_t _sl_max /* number of soil layers */,
                                 double _D_x /* diffusion constant */,
                                 lvector_t< double > const &_bulk_sl /* volume of bulk */,
                                 lvector_t< double > const &_interface_delta_x_sl /* soil layer interface divided by delta x */,
                                 lvector_t< double > const &_D_eff_bulk_sl /* effective diffusion constant */,
                                 lvector_t< double > const &_val_wl /* considered values in water layers */,
                                 lvector_t< double > const &_val /* considered values in soil layers */,
                                 double const &_val_lowest_sl /*t-1 value in lowest sublayer*/,
                                 lvector_t< double > &_flux /* calculated flux */)
{
    /* calculate inner soil-layer fluxes */
    for (size_t sl = 0; sl < _sl_max-1; ++sl)
    {
        _flux[sl+1] = D_X_2 * _interface_delta_x_sl[sl] * ( _val[sl]/_bulk_sl[sl] - _val[sl+1]/_bulk_sl[sl+1]);
    }

    //lowest soil-layer value at t is set to the minimum of the two lowest soil-layer values at t-1
    //the flux out of bottom soil layer is set so as to respect this condition
    size_t sl( _sl_max-1);
    _flux[sl+1] = _flux[sl] + _val_lowest_sl - _val[sl];

    if ( have_water_table_)
    {
      //liquid diffusion occurs from surface water into first soil layer
      _flux[0] = D_X_SBL_SOIL * interface_delta_x_sbl_soil_ * (_val_wl[0]/h_wl_ - _val[0]/_bulk_sl[0]);
    }
    else
    {
      //no surface water = no liquid diffusion into first soil layer
      _flux[0] = 0.0;
    }
}

/*!
 * @author
 *      andrew smerald
 *
 * @brief
 *      calculates fluxes between soil layers due to perturbation
 *      positive fluxes refer to downwards transport
 *      flux [i+1] corresponds to flux from soil layer [i] to [i+1]
 *      _flux[_sl_max] refers to flux out of bottom soil layer
 */
void
Transport::calculate_soil_perturbation_fluxes(
                                 size_t _sl_max /* number of soil layers */,
                                 double _D_x /* diffusion constant */,
                                 lvector_t< double > const &_bulk_sl /* volume of bulk */,
                                 lvector_t< double > const &_interface_delta_x_sl /* soil layer interface divided by delta x */,
                                 lvector_t< double > const &_D_eff_bulk_sl /* effective diffusion constant */,
                                 lvector_t< double > const &_val /* considered value */,
                                 double const &_val_lowest_sl /*t-1 value in lowest sublayer*/,
                                 lvector_t< double > &_flux /* calculated flux */)
{
    for (size_t sl = 0; sl < _sl_max-1; ++sl)
    {
        _flux[sl] = D_X_2 * _interface_delta_x_sl[sl] * ( _val[sl]/_bulk_sl[sl] - _val[sl+1]/_bulk_sl[sl+1]);
    }
    size_t sl( _sl_max-1);
    _flux[sl] = _flux[sl-1] + _val_lowest_sl - _val[sl];
}



lerr_t
Transport::leach_up_and_down( double &_val_up,
                              double &_val_mid,
                              double &_val_down,
                              double _transport_factor)
{
    if ( cbm::flt_greater_zero( _val_mid))
    {
        if ( cbm::flt_greater_zero( _transport_factor))
        {
            double const leach_value( _val_mid * _transport_factor);
            _val_mid -= leach_value;
            _val_down += leach_value;
        }
        else if ( cbm::flt_greater_zero( -_transport_factor))
        {
            double const leach_value( -_val_mid * _transport_factor);
            _val_mid -= leach_value;
            _val_up += leach_value;

        }
        else
        {
            return LDNDC_ERR_OK;
        }
    }
    else
    {
        _val_mid = 0.0;
    }

    return LDNDC_ERR_OK;
}
