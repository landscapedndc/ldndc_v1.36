/*!
 * @file
 * @author
 *    David Kraus (created on: feb 2, 2016)
 */

#ifndef  LD_LITTERHEIGHT_H_
#define  LD_LITTERHEIGHT_H_

#include  "state/mbe_state.h"
#include  <input/soillayers/soillayers.h>

namespace ldndc
{

/*!
 * @brief
 * Detailed description provided @link soillibs_litter_height here @endlink.
 *
 * @param _sl Soil layers class
 * @param _wc Watercycle state
 * @param _sc Soilchemistry state
 *
 * @return Error code
 */
lerr_t
update_litter_height(
                     soillayers::input_class_soillayers_t const &,
                     substate_watercycle_t &,
                     substate_soilchemistry_t &);

lerr_t
update_soil_height(
                   soillayers::input_class_soillayers_t const &,
                   substate_watercycle_t &,
                   substate_soilchemistry_t &);

/*!
 * @brief
 */
bool
forest_floor(
             size_t /* soil layer */,
             soillayers::input_class_soillayers_t const &,
             const ldndc::site::input_class_site_t &);


/*!
 * @brief
 */
bool
forest_floor(
             double /* soil depth */,
             soillayers::input_class_soillayers_t const &,
             const ldndc::site::input_class_site_t &);

}
#endif  /*  !LD_LITTERHEIGHT_H_  */
