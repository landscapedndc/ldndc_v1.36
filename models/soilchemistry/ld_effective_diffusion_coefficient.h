/*!
 * @file
 * 
 * @author
 *  David Kraus
 * @date
 *  Nov, 2021
 */

#ifndef  LD_EFFECTIVEDIFFUSIONCOEFFICIENT_H_
#define  LD_EFFECTIVEDIFFUSIONCOEFFICIENT_H_

#include  "ldndc.h"

namespace ldndc {

/*!
 * @param[in] _exp Exponent [-]
 * @param[in] _gas_content Gas content [m^3:m^-3]
 * @return Effective diffusion coefficient [-]
 */
double d_eff_air_a( double const & /* exponent */,
                    double const & /* gas content */);

/*!
 * @param[in] _exp_g Exponent for gas content [-]
 * @param[in] _exp_p Exponent for porosity [-]
 * @param[in] _gas_content Gas content [m^3:m^-3]
 * @param[in] _porosity Porosity [m^3:m^-3]
 * @return Effective diffusion coefficient [-]
 */
double d_eff_air_b( double const & /* exponent g */,
                    double const & /* exponent p */,
                    double const & /* gas content */,
                    double const & /* porosity */);

/*!
 * @param[in] _gas_content Gas content [m^3:m^-3]
 * @param[in] _porosity Porosity [m^3:m^-3]
 * @return Effective diffusion coefficient [-]
 */
double d_eff_air_millington_and_quirk_1961( double const & /* gas content */,
                                            double const & /* porosity */);

}

#endif  /*  !LD_EFFECTIVEDIFFUSIONCOEFFICIENT_H_  */

