/*!
 * @file
 * @author
 *    David Kraus
 * @date
 *    Nov, 2017)
 */

#ifndef  LD_WOOD_INITIALIZATION_H_
#define  LD_WOOD_INITIALIZATION_H_

#include  "state/mbe_state.h"
#include  <input/soillayers/soillayers.h>

namespace ldndc {

/*!
 * @brief
 *  Initialization of aboveground and belowground wood debris,
 *  derived from existing vegetation.
 */
lerr_t
wood_initialization(
                     soillayers::input_class_soillayers_t const &,
                     MoBiLE_PlantVegetation &,
                     substate_soilchemistry_t &,
                     ecosystem_type_e);

}

#endif  /*  !LD_WOOD_INITIALIZATION_H_  */
