/*!
 * @file
 * @author
 *  David Kraus
 * @date
 *  Sep, 2021
 */

#include  "soilchemistry/ld_enhanced_efficiency_nitrogen_fertilizers.h"


double const ldndc::ControlledReleaseNitrogenFertilizer::T_REF = 23.0;

/*!
 * @page soillibs
 * @section soillibs_enhancedefficiencynitrogenfertilizers Enhanced efficiency nitrogen fertilizers
 * @authors
 *  - David Kraus (implementation into LandscapeDNDC)
 */
ldndc::ControlledReleaseNitrogenFertilizer::ControlledReleaseNitrogenFertilizer(
                                                            double _eef_controlled_release_q10,
                                                            double _eef_controlled_release_lag_period,
                                                            double _eef_controlled_release_t80,
                                                            double _eef_controlled_release_wfps_factor,
                                                            double _eef_maximum_nitrification_inhibition,
                                                            double _eef_nitrification_inhibition_stability_1,
                                                            double _eef_nitrification_inhibition_stability_2,
                                                            double _eef_nitrification_inhibition_dilution):
                                                            EEF_CONTROLLED_RELEASE_Q10( _eef_controlled_release_q10),
                                                            EEF_CONTROLLED_RELEASE_LAG_PERIOD( _eef_controlled_release_lag_period),
                                                            EEF_CONTROLLED_RELEASE_T80( _eef_controlled_release_t80),
                                                            EEF_CONTROLLED_RELEASE_WFPS_FACTOR( _eef_controlled_release_wfps_factor),
                                                            EEF_MAXIMUM_NITRIFICATION_INHIBITION( _eef_maximum_nitrification_inhibition),
                                                            EEF_NITRIFICATION_INHIBITION_STABILITY_1( _eef_nitrification_inhibition_stability_1),
                                                            EEF_NITRIFICATION_INHIBITION_STABILITY_2( _eef_nitrification_inhibition_stability_2),
                                                            EEF_NITRIFICATION_INHIBITION_DILUTION( _eef_nitrification_inhibition_dilution),
                                                            cumulative_truncated_soil_temperature( 0.0),
                                                            ni_start( 0.0)
{}
ldndc::ControlledReleaseNitrogenFertilizer::~ControlledReleaseNitrogenFertilizer()
{}



/*!
 * @page soillibs
 * @subsection soillibs_enhancedefficiencynitrogenfertilizers_crnf Controlled-release nitrogen fertilizer
 */
ldndc::ControlledReleaseNitrogenFertilizer::crnf_fertilizer::crnf_fertilizer( double _amount)
{
    n_crnf = _amount;
    n_crnf_start = _amount;
    t_eff_crnf = 0.0;
}
ldndc::ControlledReleaseNitrogenFertilizer::crnf_fertilizer::~crnf_fertilizer()
{}


lerr_t
ldndc::ControlledReleaseNitrogenFertilizer::add_crnf( double _amount)
{
    crnf_fertilizer add_crnf( _amount);
    crnf.push_back( add_crnf);
    return LDNDC_ERR_OK;
}


lerr_t
ldndc::ControlledReleaseNitrogenFertilizer::update_crnf( double _water_filled_pore_space,
                                                         double _temperature,
                                                         double _scale_time,
                                                         double _scale_vertical_distribution)
{
    for ( size_t i = 0; i < crnf.size(); ++i)
    {
        crnf[i].t_eff_crnf += (EEF_CONTROLLED_RELEASE_WFPS_FACTOR + (1.0 - EEF_CONTROLLED_RELEASE_WFPS_FACTOR) * _water_filled_pore_space) *
                              std::pow( EEF_CONTROLLED_RELEASE_Q10, (_temperature - T_REF) / 10.0) * _scale_time * _scale_vertical_distribution;
    }
    return LDNDC_ERR_OK;
}


double
ldndc::ControlledReleaseNitrogenFertilizer::get_crnf_teff_mean()
{
    if ( crnf.size() > 0)
    {
        double t_eff_crnf_avg( 0.0);
        double n_crnf_sum( 0.0);
        for ( size_t i = 0; i < crnf.size(); ++i)
        {
            t_eff_crnf_avg += crnf[i].t_eff_crnf * crnf[i].n_crnf;
            n_crnf_sum += crnf[i].n_crnf;
        }
        if ( cbm::flt_greater_zero( n_crnf_sum))
        {
            return t_eff_crnf_avg / n_crnf_sum;
        }
    }
    return 0.0;
}


double
ldndc::ControlledReleaseNitrogenFertilizer::get_crnf_release( double _amount)
{
    double n_release_sum( 0.0);
    for ( size_t i = 0; i < crnf.size(); ++i)
    {
        double const n_release_fraction( get_crnf_release_fraction( crnf[i].t_eff_crnf));
        if ( cbm::flt_less( n_release_fraction, 0.99))
        {
            double const n_release( cbm::bound_min( 0.0,
                                                    (n_release_fraction * crnf[i].n_crnf_start) -    //potential release
                                                    (crnf[i].n_crnf_start - crnf[i].n_crnf)));       //actual release
            n_release_sum += n_release;
            crnf[i].n_crnf -= n_release;
        }
        else
        {
            n_release_sum += crnf[i].n_crnf;
            crnf[i].n_crnf = 0.0;
        }
    }

    std::vector< crnf_fertilizer >::iterator it = crnf.begin();
    while ( it != crnf.end())
    {
        if ( !cbm::flt_greater_zero( it->n_crnf))
        {
            it = crnf.erase( it);
        }
        else
        {
            ++it;
        }
    }

    return cbm::bound_max( n_release_sum, _amount);
}


/*!
 * @page soillibs
 * The cumulative fraction of N release [0-1] is given by:
 */
double
ldndc::ControlledReleaseNitrogenFertilizer::get_crnf_release_fraction( double _t_eff_crnf)
{
    double const mu_crf( (std::log( -std::log( 0.8)) - 1.0) / (std::exp( 1.0) * (EEF_CONTROLLED_RELEASE_LAG_PERIOD - EEF_CONTROLLED_RELEASE_T80)));
    return cbm::bound( 0.0,
                       std::exp( -std::exp( mu_crf * std::exp(1.0) * (EEF_CONTROLLED_RELEASE_LAG_PERIOD - _t_eff_crnf) + 1.0)),
                       1.0);
}


double
ldndc::ControlledReleaseNitrogenFertilizer::get_crnf_release_fraction_mean()
{
    if ( crnf.size() > 0)
    {
        double n_release_fraction_avg( 0.0);
        double n_crnf_sum( 0.0);
        for ( size_t i = 0; i < crnf.size(); ++i)
        {
            n_release_fraction_avg += get_crnf_release_fraction( crnf[i].t_eff_crnf) * crnf[i].n_crnf;
            n_crnf_sum += crnf[i].n_crnf;
        }
        if ( cbm::flt_greater_zero( n_crnf_sum))
        {
            return n_release_fraction_avg / n_crnf_sum;
        }
    }
    return 0.0;
}



/*!
 * @page soillibs
 * @subsection soillibs_enhancedefficiencynitrogenfertilizers_ni Nitrification inhibition
 */
lerr_t
ldndc::ControlledReleaseNitrogenFertilizer::add_nitrification_inhibitior( double _amount)
{
    //if old ni is still available
    if ( cbm::flt_greater_zero( ni_start))
    {
        double const fraction_old_ni( get_ni_stability() * ni_start / (ni_start + _amount));
        double const fraction_new_ni( 1.0 - fraction_old_ni);
        cumulative_truncated_soil_temperature *= fraction_old_ni;
        ni_start = fraction_old_ni * ni_start +
                   fraction_new_ni * _amount;
    }
    else
    {
        cumulative_truncated_soil_temperature = 0.0;
        ni_start = _amount;
    }
    return LDNDC_ERR_OK;
}


lerr_t
ldndc::ControlledReleaseNitrogenFertilizer::update_nitrification_inhibition( double _temperature,
                                                                             double _ni_sum,
                                                                             double &_ni_sl)
{
    //weight temperature increase with ni fraction
    if ( cbm::flt_greater_zero( _ni_sl * _ni_sum))
    {
        cumulative_truncated_soil_temperature += _ni_sl / _ni_sum * _temperature;
    }

    //delete ni locally if inhibition is over
    double const nitrification_inhibition( get_nitrification_inhibition( _ni_sl));
    if ( cbm::flt_less( nitrification_inhibition, 0.01))
    {
        _ni_sl = 0.0;
    }

    //delete ni setting as soon as ni has been reached zero
    if ( cbm::flt_equal_zero( _ni_sum))
    {
        cumulative_truncated_soil_temperature = 0.0;
        ni_start = 0.0;
    }

    return LDNDC_ERR_OK;
}


/*!
 * @page soillibs
 *
 * \f[
 * 1 - (1 - \beta_3) \cdot min \left( e^{- \beta_1 T_{cum} + \beta_2 cwf + \beta_0}, 1\right)
 * \f]
 */
double
ldndc::ControlledReleaseNitrogenFertilizer::get_nitrification_inhibition( double _ni_sl)
{
    //inhibition = 1 -> full inhibition
    //inhibition = 0 -> no inhibition
    if ( cbm::flt_greater_zero( _ni_sl * ni_start))
    {
        double const f_stability( get_ni_stability());
        double const f_dilution( get_ni_dilution( _ni_sl));

        return EEF_MAXIMUM_NITRIFICATION_INHIBITION * cbm::bound( 0.0, f_stability * f_dilution, 1.0);
    }
    else
    {
        return 0.0;
    }
}


double
ldndc::ControlledReleaseNitrogenFertilizer::get_ni_stability()
{
    //stability = 1 -> 100% active/stable
    //stability = 0 -> inactive
    if ( cbm::flt_greater_zero( cumulative_truncated_soil_temperature))
    {
        return std::exp( -(EEF_NITRIFICATION_INHIBITION_STABILITY_1 +
                           EEF_NITRIFICATION_INHIBITION_STABILITY_2 * cumulative_truncated_soil_temperature));
    }
    else
    {
        return 1.0;
    }
}


double
ldndc::ControlledReleaseNitrogenFertilizer::get_ni_dilution(double _ni_sl)
{
    //dilution = 1 -> 100% active
    //dilution = 0 -> completely diluted / not present
    if ( cbm::flt_greater_zero( ni_start))
    {
        return EEF_NITRIFICATION_INHIBITION_DILUTION * _ni_sl / ni_start;
    }
    else
    {
        return 0.0;
    }
}
