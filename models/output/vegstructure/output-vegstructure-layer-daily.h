/*!
 * @brief
 *    dumping modified vegetation structure related items (layers)
 *
 * @author
 *    steffen klatt,
 *    edwin haas
 */

#ifndef  LM_OUTPUT_VEGSTRUCTURE_LAYER_DAILY_H_
#define  LM_OUTPUT_VEGSTRUCTURE_LAYER_DAILY_H_

#include  "mbe_legacyoutputmodel.h"
#include  "state/mbe_state.h"

#define  LMOD_OUTPUT_MODULE_BASE  MBE_LegacyOutputModel
#define  LMOD_OUTPUT_MODULE_NAME  OutputVegstructureLayerDaily
#define  LMOD_OUTPUT_MODULE_ID    "output:vegstructure-layer:daily"
#define  LMOD_OUTPUT_MODULE_DESC  "Vegetation Structure Daily Output (Layers)"
namespace ldndc {
class  LDNDC_API  LMOD_OUTPUT_MODULE_NAME  :  public  LMOD_OUTPUT_MODULE_BASE
{
    LMOD_EXPORT_MODULE_INFO(LMOD_OUTPUT_MODULE_NAME,LMOD_OUTPUT_MODULE_ID,LMOD_OUTPUT_MODULE_DESC);
    public:
        LMOD_OUTPUT_MODULE_NAME(
                MoBiLE_State *,
                cbm::io_kcomm_t *,
                timemode_e  _timemode);

        ~LMOD_OUTPUT_MODULE_NAME();


        lerr_t  configure( ldndc::config_file_t const *);

        lerr_t  initialize();

        lerr_t  solve();

        lerr_t  finalize();

        lerr_t  wake() { return  LDNDC_ERR_OK; }
        lerr_t  sleep() { return  LDNDC_ERR_OK; }

    private:
        cbm::io_kcomm_t *  io_kcomm;

        input_class_soillayers_t const *  soillayers_;
        input_class_setup_t const *  setup_;
        substate_soilchemistry_t const *  sc;
        substate_physiology_t const *  ph;

        MoBiLE_PlantVegetation *  m_veg;
    
    private:
        lerr_t  sink_handle_acquire_();
        ldndc::sink_handle_t  m_sink;

        lerr_t  m_writerecord( ldndc_flt64_t * /*buffer*/);
};
} /*namespace ldndc*/


#undef  LMOD_OUTPUT_MODULE_BASE
#undef  LMOD_OUTPUT_MODULE_NAME
#undef  LMOD_OUTPUT_MODULE_ID
#undef  LMOD_OUTPUT_MODULE_DESC


#endif  /*  !LM_OUTPUT_VEGSTRUCTURE_LAYER_DAILY_H_  */

