/*!
 * @file
 * @author
 *  - Edwin Haas
 *  - Ruediger Grote
 *  - David Kraus
 */

#include  "output/vegstructure/output-vegstructure-daily.h"

#include  "constants/lconstants-math.h"
#include  "constants/lconstants-conv.h"
#include  "utils/lutils.h"

#define  LMOD_OUTPUT_MODULE_BASE  MBE_LegacyOutputModel
#define  LMOD_OUTPUT_MODULE_NAME  OutputVegstructureDaily
#define  LMOD_OUTPUT_MODULE_TIMEMODE  TMODE_POST_DAILY

LMOD_MODULE_INFO(LMOD_OUTPUT_MODULE_NAME,LMOD_OUTPUT_MODULE_TIMEMODE,LMOD_FLAG_USER|LMOD_FLAG_OUTPUT);
namespace ldndc {

#define  OutputVegstructureDaily_Item__species  "species"


/*!
 * @page vegetationstructureoutput
 * @section vegetationstructureoutputdaily Vegetation structure output (daily)
 *
 * <b>xml-based module selection in project's setup:</b>
 * \n\code{.xml}< module id="output:vegstructure:daily" />\endcode\n
 *
 * entity name | decription | unit
 * ----------- | ---------- | ----
 * area\_cover | Area cover | [-]
 * area\_fraction | Area fraction | [-]
 * n\_tree | Number of plants | [-]
 * vol\_tree | Volume of plants |  [m3ha-1]
 * h\_max | maximal Height of plants | [m]
 * h\_min | minimal Height of plants | [m]
 * diam\_ground | Stem diameter at ground | [m]
 * diam\_breast | Stem diameter at breast height | [m]
 * depth | Depth | [m]
 * sla | Specific leaf area | [m2 kg-1]
 * lai | Leaf area index | [-]
 * crown\_diam\_ratio | Crown diameter ratio | [-]
 * sap\_fol\_ratio | Sapwood foliage ratio | [-]
 * branch\_fraction | Branch fraction | [-]
 * DW\_stem\_wood | Stem wood | [kg m-2]
 * DW\_branch\_wood | Branch wood | [kg m-2]
 * DW\_root\_wood | Root wood | [kg m-2]
 */
ldndc_string_t const  OutputVegstructureDaily_Ids[] =
{
#ifdef  OutputVegstructureDaily_Item__species
    OutputVegstructureDaily_Item__species,
#endif
    "area_cover",
    "area_fraction",
    "n_tree",
    "vol_tree",
    "h_max",
    "h_min",
    "diam_ground",
    "diam_breast",
    "depth",
    "sla",
    "lai",
    "crown_diam_ratio",
    "sap_fol_ratio",
    "branch_fraction",
    "DW_stem_wood",
    "DW_branch_wood",
    "DW_root_wood"
};
ldndc_string_t const  OutputVegstructureDaily_Header[] =
{
#ifdef  OutputVegstructureDaily_Item__species
    OutputVegstructureDaily_Item__species,
#endif
    "area_cover[-]",
    "area_fraction[-]",
    "n_tree[ha-1]",
    "vol_tree[m3ha-1]",
    "h_max[m]",
    "h_min[m]",
    "diam_ground[m]",
    "diam_breast[m]",
    "depth[m]",
    "sla[m2kg-1]",
    "lai[-]",
    "crown_diam_ratio[-]",
    "sap_fol_ratio[-]",
    "branch_fraction[-]",
    "DW_stem_wood[kgm-2]",
    "DW_branch_wood[kgm-2]",
    "DW_root_wood[kgm-2]"
};
#define  OutputVegstructureDaily_Datasize  (sizeof( OutputVegstructureDaily_Header) / sizeof( OutputVegstructureDaily_Header[0]))
ldndc_output_size_t const  OutputVegstructureDaily_Sizes[] =
{
#ifdef  OutputVegstructureDaily_Item__species
    1,
    OutputVegstructureDaily_Datasize - 1,
#else
    OutputVegstructureDaily_Datasize,
#endif
    OutputVegstructureDaily_Datasize /*total size*/
};
ldndc_output_size_t const *  OutputVegstructureDaily_EntitySizes = NULL;
#define  OutputVegstructureDaily_Rank  ((ldndc_output_rank_t)(sizeof( OutputVegstructureDaily_Sizes) / sizeof( OutputVegstructureDaily_Sizes[0])) - 1)
atomic_datatype_t const  OutputVegstructureDaily_Types[] =
{
#ifdef  OutputVegstructureDaily_Item__species
    LDNDC_STRING,
#endif
    LDNDC_FLOAT64
};


LMOD_OUTPUT_MODULE_NAME::LMOD_OUTPUT_MODULE_NAME(
        MoBiLE_State *  _state,
        cbm::io_kcomm_t *  _io_kcomm,
        timemode_e  _timemode)
        : LMOD_OUTPUT_MODULE_BASE( _state, _io_kcomm, _timemode),

          io_kcomm( _io_kcomm),

          phys( _state->get_substate< substate_physiology_t >()),
          m_veg( &_state->vegetation)
{
}


LMOD_OUTPUT_MODULE_NAME::~LMOD_OUTPUT_MODULE_NAME()
{
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::configure(
                ldndc::config_file_t const *  _cf)
{
    lerr_t  rc_setflags =
        this->set_metaflags( _cf, RM_DEFAULT_DAILY);
    if ( rc_setflags)
        { return  LDNDC_ERR_FAIL; }

    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::initialize()
{
    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::sink_handle_acquire_(
        sink_handle_t &  _snk,
        int  _index)
{
    int  sink_index = _index;
    if ( !this->separate_concurrent())
        { sink_index = 0; }
    _snk = this->io_kcomm->sink_handle_acquire( "vegstructuredaily", sink_index);
    if ( _snk.status() == LDNDC_ERR_OK)
    {
        lerr_t  rc_layout = LDNDC_OUTPUT_DEFINE_SINK_LAYOUT_DEFAULTS(
            _snk,OutputVegstructureDaily);
        RETURN_IF_NOT_OK(rc_layout);
    }
    else
    {
        KLOGERROR( "sink status bad  [sink=","vegstructuredaily","]");
        return  _snk.status();
    }

    return  LDNDC_ERR_OK;
}
lerr_t
LMOD_OUTPUT_MODULE_NAME::acquire_sinks_if_needed_()
{
    size_t const  c = this->m_veg->size();
    if ( c == 0)
    {
        return  LDNDC_ERR_OK;
    }
    else if ( c > 1)
    {
        if ( !this->m_sinksum.is_acquired())
        {
            lerr_t  rc_snk_avg = this->sink_handle_acquire_( this->m_sinksum, 0);
            RETURN_IF_NOT_OK(rc_snk_avg);
        }
    }

    size_t const  C = this->m_veg->slot_cnt()-1;
    if ( cbm::is_invalid( C) || ( this->m_sinks.size() > C))
    {
        return  LDNDC_ERR_OK;
    }

    /* recruit more sinks */
    for ( size_t  s = this->m_sinks.size();  s <= C;  ++s)
    {
        ldndc::sink_handle_t  snk;
        lerr_t  rc_snk = this->sink_handle_acquire_( snk, s+1);
        RETURN_IF_NOT_OK(rc_snk);

        this->m_sinks.push_back( snk);
    }

    return  LDNDC_ERR_OK;
}

lerr_t
LMOD_OUTPUT_MODULE_NAME::solve()
{
    lerr_t  rc_sinks = this->acquire_sinks_if_needed_();
    RETURN_IF_NOT_OK(rc_sinks);
    if ( this->m_sinks.size() == 0)
    {
        return  LDNDC_ERR_OK;
    }

    size_t const  c = this->m_veg->size();

    dump_info_t  vs_di;
    this->set_dump_info( &vs_di);

    ldndc_flt64_t  data_flt64_a[OutputVegstructureDaily_Datasize];
    cbm::mem_set( data_flt64_a, OutputVegstructureDaily_Datasize, 0.0);
    for ( PlantIterator  vt = this->m_veg->begin(); vt != this->m_veg->end(); ++vt)
    {
#ifdef  OutputVegstructureDaily_Item__species
        ldndc_string_t  data_string[1];
        cbm::as_strcpy( &data_string[0], (*vt)->cname());
#endif
        vs_di.plant = *vt;
        ldndc_flt64_t  data_flt64_0[OutputVegstructureDaily_Datasize];
        lerr_t  rc_dump = this->m_writerecord( &vs_di, data_flt64_0, data_flt64_a);
        RETURN_IF_NOT_OK(rc_dump);

#ifdef  OutputVegstructureDaily_Item__species
        void *  data[] = { data_string, data_flt64_0};
#else
        void *  data[] = { data_flt64_0};
#endif
        lerr_t  rc_write =
            this->write_fixed_record( &this->m_sinks[(*vt)->slot], data);
        if ( rc_write)
            { return  LDNDC_ERR_FAIL; }
    }

    if ( this->m_sinksum.is_acquired())
    {
        if ( c > 0)
        {
#ifdef  OutputVegstructureDaily_Item__species
            ldndc_string_t  data_string[1];
            cbm::as_strcpy( &data_string[0], COLUMN_NAME_ALLSPECIES);
            void *  data_a[] = { data_string, data_flt64_a};
#else
            void *  data_a[] = { data_flt64_a};
#endif
            lerr_t  rc_write =
                this->write_fixed_record( &this->m_sinksum, data_a);
            if ( rc_write)
                { return  LDNDC_ERR_FAIL; }
        }
    }

    return  LDNDC_ERR_OK;
}

void
LMOD_OUTPUT_MODULE_NAME::set_dump_info( dump_info_t *  _di)
const
{
    _di->n_grasses = this->m_veg->size_by_group( SPECIES_GROUP_GRASS);
    _di->f_vt_sum_grass = 0.0;
    for ( GrassIterator  g = this->m_veg->groupbegin< species::grass >();
            g != this->m_veg->groupend< species::grass >();  ++g)
    {
        _di->f_vt_sum_grass += (*g)->f_area;
    }

    _di->n_trees = this->m_veg->size_by_group( SPECIES_GROUP_WOOD);
    _di->f_vt_sum_tree = 0.0;
    for ( TreeIterator  w = this->m_veg->groupbegin< species::wood >();
            w != this->m_veg->groupend< species::wood >();  ++w)
    {
        _di->f_vt_sum_tree += (*w)->f_area;
    }
}

lerr_t
LMOD_OUTPUT_MODULE_NAME::finalize()
{
    for ( size_t  s = 0;  s < this->m_sinks.size();  ++s)
    {
        if ( this->m_sinks[s].is_acquired())
        {
            this->io_kcomm->sink_handle_release( &this->m_sinks[s]);
        }
    }
    this->m_sinks.clear();

    if ( this->m_sinksum.is_acquired())
    {
        this->io_kcomm->sink_handle_release( &this->m_sinksum);
    }
    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::m_writerecord(
                                       dump_info_t const *  _di,
                                       ldndc_flt64_t *  _buf,
                                       ldndc_flt64_t *  _buf_avg)
{
    /* calculate averaging factors */

    double const  f_s_tree = (_di->f_vt_sum_tree > 0.0) ?
                             (_di->plant->f_area / _di->f_vt_sum_tree) : 0.0;


    /* begin record */

    LDNDC_OUTPUT_SET_COLUMN_INDEX(0);

    double area_cover( 0.0);
    for ( PlantIterator vt = this->m_veg->begin(); vt != this->m_veg->end();  ++vt)
    {
        area_cover += (*vt)-> f_area;
    }
    area_cover = cbm::bound_max( area_cover, 1.0);
    LDNDC_OUTPUT_SET_COLUMN_AVG_SET(area_cover);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(area_cover);

    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_di->plant->f_area);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_di->plant->f_area);

    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_di->plant->tree_number);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_di->plant->tree_number);

    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_di->plant->stand_volume());
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_di->plant->stand_volume());

    LDNDC_OUTPUT_SET_COLUMN_AVG_MAX(_di->plant->height_max);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_di->plant->height_max);

    if ( _buf_avg)
    {
        if ( cbm::flt_equal_zero( LDNDC_OUTPUT_GET_CURRENT_COLUMN_VALUE(_buf_avg)))
        {
            LDNDC_OUTPUT_SET_COLUMN_AVG_SET(_di->plant->height_at_canopy_start);
        }
        else
        {
            LDNDC_OUTPUT_SET_COLUMN_AVG_MIN(_di->plant->height_at_canopy_start);
        }
    }
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_di->plant->height_at_canopy_start);

    if ( _di->plant->groupId() == SPECIES_GROUP_WOOD)
    {
        LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_di->plant->dbas * f_s_tree);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_di->plant->dbas);

        LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_di->plant->dbh * f_s_tree);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_di->plant->dbh);
    }
    else
    {
        LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(0.0);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_di->plant->dbas);

        LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(0.0);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_di->plant->dbh);
    }


    LDNDC_OUTPUT_SET_COLUMN_AVG_MAX(_di->plant->rooting_depth);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_di->plant->rooting_depth);


    double const sla( cbm::sum( _di->plant->sla_fl, _di->plant->nb_foliagelayers()));
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM( sla);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( sla);

    double const lai( _di->plant->lai());
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(lai);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(lai);

    if ( _di->plant->groupId() == SPECIES_GROUP_WOOD)
    {
        LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_di->plant->cdr * f_s_tree);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_di->plant->cdr);

        LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_di->plant->qsfm * f_s_tree);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_di->plant->qsfm);

        LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_di->plant->f_branch * f_s_tree);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_di->plant->f_branch);

        double const  fSC = _di->plant->mSap + _di->plant->mCor + _di->plant->mBud;

        double const  stemWodAvg = fSC * ( 1.0 - (*_di->plant)->UGWDF()) * ( 1.0 - _di->plant->f_branch);
        LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(stemWodAvg);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(stemWodAvg);

        double const  braWodAvg = fSC * ( 1.0 - (*_di->plant)->UGWDF()) * _di->plant->f_branch;
        LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(braWodAvg);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(braWodAvg);

        double const  rootWodAvg = fSC * (*_di->plant)->UGWDF();
        LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(rootWodAvg);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(rootWodAvg);
    }
    else
    {
        LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(0.0);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( _di->plant->cdr);

        LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(0.0);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( _di->plant->qsfm);

        LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(0.0);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( _di->plant->f_branch);

        double const  fSC = _di->plant->mSap + _di->plant->mCor;

        double const  stemWodAvg = fSC * ( 1.0 - (*_di->plant)->UGWDF()) * ( 1.0 - _di->plant->f_branch);
        LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(0.0);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(stemWodAvg);

        double const  braWodAvg = fSC * ( 1.0 - (*_di->plant)->UGWDF()) * _di->plant->f_branch;
        LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(0.0);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(braWodAvg);

        double const  rootWodAvg = fSC * (*_di->plant)->UGWDF();
        LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(0.0);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(rootWodAvg);
    }

    return  LDNDC_ERR_OK;
}

#undef  LMOD_OUTPUT_MODULE_BASE
#undef  LMOD_OUTPUT_MODULE_NAME

#undef  OutputVegstructureDaily_Rank
#undef  OutputVegstructureDaily_Datasize

} /*namespace ldndc*/

