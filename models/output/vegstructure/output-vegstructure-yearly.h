/*!
 * @brief
 *    dumping modified vegetation structure related items
 *
 * @author
 *    steffen klatt,
 *    edwin haas
 */

#ifndef  LM_OUTPUT_VEGSTRUCTURE_YEARLY_H_NEW_
#define  LM_OUTPUT_VEGSTRUCTURE_YEARLY_H_NEW_

#include  "mbe_legacyoutputmodel.h"
#include  "state/mbe_state.h"

#define  LMOD_OUTPUT_MODULE_BASE  MBE_LegacyOutputModel
#define  LMOD_OUTPUT_MODULE_NAME  OutputVegstructureYearly
#define  LMOD_OUTPUT_MODULE_ID    "output:vegstructure:yearly"
#define  LMOD_OUTPUT_MODULE_DESC  "Vegetation Structure Yearly Output"
namespace ldndc {
class  substate_physiology_t;
class  LDNDC_API  LMOD_OUTPUT_MODULE_NAME  :  public  LMOD_OUTPUT_MODULE_BASE
{
    LMOD_EXPORT_MODULE_INFO(LMOD_OUTPUT_MODULE_NAME,LMOD_OUTPUT_MODULE_ID,LMOD_OUTPUT_MODULE_DESC);
    public:
        LMOD_OUTPUT_MODULE_NAME(
                MoBiLE_State *,
                cbm::io_kcomm_t *,
                timemode_e  _timemode);

        ~LMOD_OUTPUT_MODULE_NAME();


        lerr_t  configure( ldndc::config_file_t const *);

        lerr_t  initialize();

        lerr_t  solve();

        lerr_t  finalize();

        lerr_t  wake() { return  LDNDC_ERR_OK; }
        lerr_t  sleep() { return  LDNDC_ERR_OK; }

    private:
        lerr_t  sink_handle_acquire_(
                sink_handle_t & /*sink handle*/, int /*index*/);

    private:
        cbm::io_kcomm_t *  io_kcomm;

        substate_physiology_t const *  phys;
        MoBiLE_PlantVegetation *  m_veg;

    private:
        lerr_t  acquire_sinks_if_needed_();
        size_t  max_sink_index_() const;
        std::vector< ldndc::sink_handle_t >  m_sinks;
        ldndc::sink_handle_t  m_sinksum;

        lerr_t  m_writerecord( MoBiLE_Plant *, ldndc_flt64_t * /*buffer*/,
                    ldndc_flt64_t * /*average buffer*/);
};
} /*namespace ldndc*/


#undef  LMOD_OUTPUT_MODULE_BASE
#undef  LMOD_OUTPUT_MODULE_NAME
#undef  LMOD_OUTPUT_MODULE_ID
#undef  LMOD_OUTPUT_MODULE_DESC


#endif  /*  !LM_OUTPUT_VEGSTRUCTURE_YEARLY_H_NEW_  */

