/*!
 * @file
 * @author
 *  - Carolin Boos
 *  - David Kraus
 */

#include  "output/vegstructure/output-vegstructure-horizons-daily.h"
#include  "utils/lutils.h"

#define  LMOD_OUTPUT_MODULE_BASE  MBE_LegacyOutputModel
#define  LMOD_OUTPUT_MODULE_NAME  OutputVegstructureHorizonsDaily
#define  LMOD_OUTPUT_MODULE_TIMEMODE  TMODE_POST_DAILY

LMOD_MODULE_INFO(LMOD_OUTPUT_MODULE_NAME,TMODE_POST_DAILY,LMOD_FLAG_USER|LMOD_FLAG_OUTPUT);

namespace ldndc
{

/*!
 * @page vegetationstructureoutput
 * @section vegetationstructureoutputhorizonsdaily Vegetation structure output (horizons,daily)
 *
 * <b>xml-based module selection in project's setup:</b>
 * \n\code{.xml}< module id="output:vegstructure-horizons:daily" />\endcode\n
 *
 * entity name | decription | unit
 * ----------- | ---------- | ----
 * lai\_canopy\_top | Leaf area index at canopy top | [m2m-2]
 * lai\_canopy\_middle | Leaf area index at canopy middle | [m2m-2]
 * lai\_canopy\_bottom | Leaf area index at canopy bottom | [m2m-2]
 * root\_length\_density\_10cm | Root length density in 10 cm soil depth | [mm-3]
 * root\_length\_density\_20cm | Root length density in 20 cm soil depth | [mm-3]
 * root\_length\_density\_40cm | Root length density in 40 cm soil depth | [mm-3]
 * root\_length\_density\_60cm | Root length density in 60 cm soil depth | [mm-3]
 * root\_length\_density\_80cm | Root length density in 80 cm soil depth | [mm-3]
 * root\_length\_density\_120cm | Root length density in 120 cm soil depth | [mm-3]
 * root\_length\_density\_150cm | Root length density in 150 cm soil depth | [mm-3]
 */
ldndc_string_t const  OutputVegstructureHorizonsDaily_Ids[] =
{
    "lai_canopy_top",
    "lai_canopy_middle",
    "lai_canopy_bottom",
    
    "root_length_density_10cm",
    "root_length_density_20cm",
    "root_length_density_40cm",
    "root_length_density_60cm",
    "root_length_density_80cm",
    "root_length_density_120cm",
    "root_length_density_150cm"
};
ldndc_string_t const  OutputVegstructureHorizonsDaily_Header[] =
{
    "lai_canopy_top[m2m-2]",
    "lai_canopy_middle[m2m-2]",
    "lai_canopy_bottom[m2m-2]",
    
    "root_length_density_10cm[mm-3]",
    "root_length_density_20cm[mm-3]",
    "root_length_density_40cm[mm-3]",
    "root_length_density_60cm[mm-3]",
    "root_length_density_80cm[mm-3]",
    "root_length_density_120cm[mm-3]",
    "root_length_density_150cm[mm-3]"
};


#define  OutputVegstructureHorizonsDaily_Datasize  (sizeof( OutputVegstructureHorizonsDaily_Header) / sizeof( OutputVegstructureHorizonsDaily_Header[0]))
ldndc_output_size_t const  OutputVegstructureHorizonsDaily_Sizes[] =
{
    OutputVegstructureHorizonsDaily_Datasize,

    OutputVegstructureHorizonsDaily_Datasize /*total size*/
};


ldndc_output_size_t const *  OutputVegstructureHorizonsDaily_EntitySizes = NULL;
#define  OutputVegstructureHorizonsDaily_Rank  ((ldndc_output_rank_t)(sizeof( OutputVegstructureHorizonsDaily_Sizes) / sizeof( OutputVegstructureHorizonsDaily_Sizes[0])) - 1)
atomic_datatype_t const  OutputVegstructureHorizonsDaily_Types[] =
{
    LDNDC_FLOAT64
};


LMOD_OUTPUT_MODULE_NAME::LMOD_OUTPUT_MODULE_NAME(
        MoBiLE_State *  _state,
        cbm::io_kcomm_t *  _io_kcomm,
        timemode_e  _timemode)
        : LMOD_OUTPUT_MODULE_BASE( _state, _io_kcomm, _timemode),

          io_kcomm( _io_kcomm),

          soillayers_( _io_kcomm->get_input_class< input_class_soillayers_t >()),
          setup_( _io_kcomm->get_input_class< input_class_setup_t >()),
          wc_( _state->get_substate_ref< substate_watercycle_t >()),
          mc_( _state->get_substate_ref< substate_microclimate_t >()),
          sc_( _state->get_substate_ref< substate_soilchemistry_t >()),
          m_veg( &_state->vegetation)
{
}


LMOD_OUTPUT_MODULE_NAME::~LMOD_OUTPUT_MODULE_NAME()
{}


lerr_t
LMOD_OUTPUT_MODULE_NAME::configure(
                ldndc::config_file_t const *  _cf)
{
    lerr_t  rc_setflags = this->set_metaflags( _cf);
    if ( rc_setflags)
        { return  LDNDC_ERR_FAIL; }
    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::initialize()
{
    this->m_sink = this->io_kcomm->sink_handle_acquire( "vegstructurehorizonsdaily");
    if ( this->m_sink.status() == LDNDC_ERR_OK)
    {
        lerr_t  rc_layout = LDNDC_OUTPUT_DEFINE_SINK_LAYOUT_DEFAULTS(
            this->m_sink,OutputVegstructureHorizonsDaily);
        RETURN_IF_NOT_OK(rc_layout);
    }
    else
    {
        KLOGERROR( "sink status bad  [sink=","vegstructurehorizonsdaily","]");
        return  this->m_sink.status();
    }

    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::solve()
{
    ldndc_flt64_t  data_flt64_0[OutputVegstructureHorizonsDaily_Datasize];
        lerr_t  rc_dump = dump_0( data_flt64_0);
    RETURN_IF_NOT_OK(rc_dump);

    void *  data[] = { data_flt64_0};
    lerr_t  rc_write =
        this->write_fixed_record( &this->m_sink, data);
    if ( rc_write)
        { return  LDNDC_ERR_FAIL; }

    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::finalize()
{
    this->io_kcomm->sink_handle_release( &this->m_sink);
    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::dump_0(
                                ldndc_flt64_t *  _buf)
{
    size_t fl_cnt( m_veg->canopy_layers_used());

    int fl_top( fl_cnt-1);
    int fl_middle( fl_top / 2);
    int fl_bottom( 0);

    static const double  D[] = { 0.1, 0.2, 0.4, 0.6, 0.8, 1.2, 1.5};
    static const size_t  D_SIZE = sizeof(D) / sizeof( double);
    static const double  val_ini = ldndc::invalid_t< double >::value;
    double  root_length_at[D_SIZE] = { val_ini, val_ini, val_ini, val_ini, val_ini, val_ini, val_ini};


    size_t  d( 0);
    
    for ( size_t  sl = 0;  ( d < D_SIZE) && ( sl < soillayers_->soil_layer_cnt());  ++sl)
    {
        if ( sc_.depth_sl[sl] >= ( D[d] - 1.0e-06)) // search for the first layer, which ends below the wanted output layer
        {
            root_length_at[d] = m_veg->rootlength_sl( sl) /  sc_.h_sl[sl]; // all units are m
            //~ std::cout << sc_.depth_sl[sl] << " " << sc_.h_sl[sl] << std::endl;
            ++d;
        }
    }

    LDNDC_OUTPUT_SET_COLUMN_INDEX(0);

    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( m_veg->lai_fl( fl_top));
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( m_veg->lai_fl( fl_middle));
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( m_veg->lai_fl( fl_bottom));

    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(root_length_at[0]);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(root_length_at[1]);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(root_length_at[2]);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(root_length_at[3]);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(root_length_at[4]);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(root_length_at[5]);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(root_length_at[6]);

    return  LDNDC_ERR_OK;
}


#undef  LMOD_OUTPUT_MODULE_BASE
#undef  LMOD_OUTPUT_MODULE_NAME

#undef  OutputVegstructureHorizonsDaily_Rank
#undef  OutputVegstructureHorizonsDaily_Datasize

} /*namespace ldndc*/

