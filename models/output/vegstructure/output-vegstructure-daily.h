/*!
 * @brief
 *    dumping modified vegetation structure related items
 *
 * @author
 *    steffen klatt,
 *    edwin haas
 */

#ifndef  LM_OUTPUT_VEGSTRUCTURE_DAILY_H_NEW_
#define  LM_OUTPUT_VEGSTRUCTURE_DAILY_H_NEW_

#include  "mbe_legacyoutputmodel.h"
#include  "state/mbe_state.h"

#define  LMOD_OUTPUT_MODULE_BASE  MBE_LegacyOutputModel
#define  LMOD_OUTPUT_MODULE_NAME  OutputVegstructureDaily
#define  LMOD_OUTPUT_MODULE_ID    "output:vegstructure:daily"
#define  LMOD_OUTPUT_MODULE_DESC  "Vegetation Structure Daily Output"
namespace ldndc {
class  substate_physiology_t;
class  LDNDC_API  LMOD_OUTPUT_MODULE_NAME  :  public  LMOD_OUTPUT_MODULE_BASE
{
    LMOD_EXPORT_MODULE_INFO(LMOD_OUTPUT_MODULE_NAME,LMOD_OUTPUT_MODULE_ID,LMOD_OUTPUT_MODULE_DESC);
    public:
        LMOD_OUTPUT_MODULE_NAME(
                MoBiLE_State *,
                cbm::io_kcomm_t *,
                timemode_e  _timemode);

        ~LMOD_OUTPUT_MODULE_NAME();


        lerr_t  configure( ldndc::config_file_t const *);

        lerr_t  initialize();

        lerr_t  solve();

        lerr_t  finalize();

        lerr_t  wake() { return  LDNDC_ERR_OK; }
        lerr_t  sleep() { return  LDNDC_ERR_OK; }

    private:
        lerr_t  sink_handle_acquire_(
                sink_handle_t & /*sink handle*/, int /*index*/);
        lerr_t  acquire_sinks_if_needed_();
        std::vector< ldndc::sink_handle_t >  m_sinks;
        ldndc::sink_handle_t  m_sinksum;

    private:
        cbm::io_kcomm_t *  io_kcomm;

        substate_physiology_t const *  phys;
        MoBiLE_PlantVegetation *  m_veg;

    private:
        struct  dump_info_t
        {
            MoBiLE_Plant *  plant;
            size_t  n_trees;
            double  f_vt_sum_tree;
            size_t  n_grasses;
            double  f_vt_sum_grass;
        };
        void set_dump_info(  dump_info_t *) const;
        lerr_t  m_writerecord( dump_info_t const *,
                ldndc_flt64_t * /*buffer*/, ldndc_flt64_t * /*average buffer*/);
};
} /*namespace ldndc*/


#undef  LMOD_OUTPUT_MODULE_BASE
#undef  LMOD_OUTPUT_MODULE_NAME
#undef  LMOD_OUTPUT_MODULE_ID
#undef  LMOD_OUTPUT_MODULE_DESC


#endif  /*  !LM_OUTPUT_VEGSTRUCTURE_DAILY_H_NEW_  */

