/*!
 * @file
 * @author
 *  - Edwin Haas
 *  - Ruediger Grote
 *  - David Kraus
 */

#include  "output/vegstructure/output-vegstructure-layer-daily.h"
#include  "utils/lutils.h"

#define  LMOD_OUTPUT_MODULE_BASE  MBE_LegacyOutputModel
#define  LMOD_OUTPUT_MODULE_NAME  OutputVegstructureLayerDaily
#define  LMOD_OUTPUT_MODULE_TIMEMODE  TMODE_POST_DAILY

LMOD_MODULE_INFO(LMOD_OUTPUT_MODULE_NAME,LMOD_OUTPUT_MODULE_TIMEMODE,LMOD_FLAG_USER|LMOD_FLAG_OUTPUT);
namespace ldndc {


/*!
 * @page vegetationstructureoutput
 * @section vegetationstructureoutputlayerdaily Vegetation structure output (layer,daily)
 *
 * <b>xml-based module selection in project's setup:</b>
 * \n\code{.xml}< module id="output:vegstructure-layer:daily" />\endcode\n
 *
 * entity name | decription | unit
 * ----------- | ---------- | ----
 * level | Level height | [m]
 * extension | Height | [m]
 * lai | Leaf area index | [-]
 * sla | Specific leaf area | [m2 kg-1]
 * fol\_fraction | Foliage fraction | [0-1]
 * frt\_fraction | Fine roots fraction | [0-1]
 */
ldndc_string_t const  OutputVegstructureLayerDaily_Ids[] =
{
    "level",
    "extension",
    "lai",
    "sla", 
    "fol_fraction", 
    "frt_fraction"
};
ldndc_string_t const  OutputVegstructureLayerDaily_Header[] =
{
    "level[m]",
    "extension[m]",
    "lai[m2m-2]",
    "sla[m2kg-1]", 
    "fol_fraction[-]",
    "frt_fraction[-]"
};
#define  OutputVegstructureLayerDaily_Datasize  (sizeof( OutputVegstructureLayerDaily_Header) / sizeof( OutputVegstructureLayerDaily_Header[0]))
ldndc_output_size_t const  OutputVegstructureLayerDaily_Sizes[] =
{
    OutputVegstructureLayerDaily_Datasize,

    OutputVegstructureLayerDaily_Datasize /*total size*/
};
ldndc_output_size_t const *  OutputVegstructureLayerDaily_EntitySizes = NULL;
#define  OutputVegstructureLayerDaily_Rank  ((ldndc_output_rank_t)(sizeof( OutputVegstructureLayerDaily_Sizes) / sizeof( OutputVegstructureLayerDaily_Sizes[0])) - 1)
atomic_datatype_t const  OutputVegstructureLayerDaily_Types[] =
{
    LDNDC_FLOAT64
};


LMOD_OUTPUT_MODULE_NAME::LMOD_OUTPUT_MODULE_NAME(
        MoBiLE_State *  _state,
        cbm::io_kcomm_t *  _io_kcomm,
        timemode_e  _timemode)
    : LMOD_OUTPUT_MODULE_BASE( _state, _io_kcomm, _timemode),

          io_kcomm( _io_kcomm),

          soillayers_( _io_kcomm->get_input_class< input_class_soillayers_t >()),
          setup_( _io_kcomm->get_input_class< input_class_setup_t >()),
          sc( _state->get_substate< substate_soilchemistry_t >()),
          ph( _state->get_substate< substate_physiology_t >()),

          m_veg( &_state->vegetation)
{
}


LMOD_OUTPUT_MODULE_NAME::~LMOD_OUTPUT_MODULE_NAME()
{
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::configure(
                ldndc::config_file_t const *  _cf)
{
    lerr_t  rc_setflags =
        this->set_metaflags( _cf, RM_DEFAULT_LAYERDAILY);
    if ( rc_setflags)
        { return  LDNDC_ERR_FAIL; }

    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::initialize()
{
    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::sink_handle_acquire_()
{
    size_t const  c = this->m_veg->size();
    if ( !this->m_sink.is_acquired() && ( c > 0))
    {
        this->m_sink = this->io_kcomm->sink_handle_acquire( "vegstructurelayerdaily");
        if ( this->m_sink.status() == LDNDC_ERR_OK)
        {
            lerr_t  rc_layout = LDNDC_OUTPUT_DEFINE_SINK_LAYOUT_DEFAULTS(
                this->m_sink,OutputVegstructureLayerDaily);
            RETURN_IF_NOT_OK(rc_layout);
        }
        else
        {
            KLOGERROR( "sink status bad  [sink=","vegstructurelayerdaily","]");
            return  this->m_sink.status();
        }
    }

    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::solve()
{
    lerr_t  rc_sinks = this->sink_handle_acquire_();
    RETURN_IF_NOT_OK(rc_sinks);

    if ( !this->m_sink.is_acquired())
        { return  LDNDC_ERR_OK; }

    size_t const  c = this->m_veg->size();

    if ( c == 0)
    {
        return  LDNDC_ERR_OK;
    }

    ldndc_flt64_t  data_flt64_0[OutputVegstructureLayerDaily_Datasize];
    lerr_t  rc_dump = this->m_writerecord( data_flt64_0);
    if ( rc_dump)
        { return  LDNDC_ERR_FAIL; }

    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::finalize()
{
    if ( this->m_sink.is_acquired())
    {
        this->io_kcomm->sink_handle_release( &this->m_sink);
    }
    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::m_writerecord( ldndc_flt64_t *  _buf)
{
    size_t const  CF = std::min( m_veg->canopy_layers_used(), this->ph->h_fl.size());
    size_t const  F = ( CF == 0) ? 0 : ( CF - 1);
    size_t const  CS = this->soillayers_->soil_layer_cnt();
    
    // - above ground
    double  h = ( CF == 0) ? 0.0 : this->ph->h_fl.sum(CF);
    for ( size_t  l = 0;  l < CF;  ++l)
    {
        size_t const  fl = F-l;

        double  lai_fl = 0.0;
        double  sla_fl = 0.0;
        double  fFol_fl = 0.0;
        
        for ( PlantIterator  vt = this->m_veg->begin(); vt != this->m_veg->end(); ++vt)
        {
            lai_fl += (*vt)->sla_fl[fl];
            sla_fl += (*vt)->sla_fl[fl];
            fFol_fl += (*vt)->fFol_fl[fl];
        }
        
        LDNDC_OUTPUT_SET_COLUMN_INDEX(0);
        
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(h);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->ph->h_fl[fl]);

        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(lai_fl);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(sla_fl);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(fFol_fl);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(ldndc::invalid_t< ldndc_flt64_t >::value);
        
        h -= this->ph->h_fl[fl];

        void *  data_above[] = { _buf};
        this->set_layernumber( fl+1);
        lerr_t  rc_write =
            this->write_fixed_record( &this->m_sink, data_above);
        if ( rc_write)
            { return  LDNDC_ERR_FAIL; }
    }
    
    // - below ground
    for ( size_t  sl = 0;  sl < CS;  ++sl)
    {
        double  fFrt_sl = 0.0;
        for ( PlantIterator  vt = this->m_veg->begin(); vt != this->m_veg->end(); ++vt)
        {
            fFrt_sl += (*vt)->fFrt_sl[sl];
        }
        
        LDNDC_OUTPUT_SET_COLUMN_INDEX(0);
        
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(-this->sc->depth_sl[sl]);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->sc->h_sl[sl]);
        
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(ldndc::invalid_t< ldndc_flt64_t >::value);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(ldndc::invalid_t< ldndc_flt64_t >::value);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(ldndc::invalid_t< ldndc_flt64_t >::value);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(fFrt_sl);
        
        void *  data_below[] = { _buf};
        this->set_layernumber( -static_cast< int >( sl) - 1);
        lerr_t  rc_write =
            this->write_fixed_record( &this->m_sink, data_below);
        if ( rc_write)
            { return  LDNDC_ERR_FAIL; }
    }
    
    return  LDNDC_ERR_OK;
}

#undef  LMOD_OUTPUT_MODULE_BASE
#undef  LMOD_OUTPUT_MODULE_NAME

#undef  OutputVegstructureLayerDaily_Rank
#undef  OutputVegstructureLayerDaily_Datasize

} /*namespace ldndc*/

