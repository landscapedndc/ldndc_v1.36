/*!
 * @file
 * @author
 *  - Edwin Haas
 *  - Ruediger Grote
 *  - David Kraus
 */

#include  "output/vegstructure/output-vegstructure-yearly.h"

#include  "constants/lconstants-math.h"
#include  "constants/lconstants-conv.h"
#include  "utils/lutils.h"

#define  LMOD_OUTPUT_MODULE_BASE  MBE_LegacyOutputModel
#define  LMOD_OUTPUT_MODULE_NAME  OutputVegstructureYearly
#define  LMOD_OUTPUT_MODULE_TIMEMODE  TMODE_POST_YEARLY

LMOD_MODULE_INFO(LMOD_OUTPUT_MODULE_NAME,LMOD_OUTPUT_MODULE_TIMEMODE,LMOD_FLAG_USER|LMOD_FLAG_OUTPUT);
namespace ldndc {

#define  OutputVegstructureYearly_Item__species  "species"

/*!
 * @page vegetationstructureoutput
 * @section vegetationstructureoutputyearly Vegetation structure output (yearly)
 *
 * <b>xml-based module selection in project's setup:</b>
 * \n\code{.xml}< module id="output:vegstructure:yearly" />\endcode\n
 *
 * entity name | decription | unit
 * ----------- | ---------- | ----
 * area\_cover | Area cover | [-]
 * area\_fraction | Area fraction | [-]
 * n\_tree | Number of plants | [-]
 * vol\_tree | Volume of plants |  [m3ha-1]
 * h\_max | maximal Height of plants | [m]
 * h\_min | minimal Height of plants | [m]
 * diam\_ground | Stem diameter at ground | [m]
 * diam\_breast | Stem diameter at breast height | [m]
 * depth | Depth | [m]
 * lai | Leaf area index | [-]
 * crown\_diam\_ratio | Crown diameter ratio | [-]
 * sap\_fol\_ratio | Sapwood foliage ratio | [-]
 * branch\_fraction | Branch fraction | [-]
 * DW\_stem\_wood | Stem wood | [kg m-2]
 * DW\_branch\_wood | Branch wood | [kg m-2]
 * DW\_root\_wood | Root wood | [kg m-2]
 */
ldndc_string_t const  OutputVegstructureYearly_Ids[] =
{
#ifdef  OutputVegstructureYearly_Item__species
    OutputVegstructureYearly_Item__species,
#endif
    "area_cover", 
    "area_fraction", 
    "n_tree", 
    "vol_tree",
    "h_max", 
    "h_min", 
    "diam_ground", 
    "diam_breast", 
    "depth", 
    "lai",
    "crown_diam_ratio", 
    "sap_fol_ratio",
    "branch_fraction",
    "stem_wood", 
    "branch_wood", 
    "root_wood"
};
ldndc_string_t const  OutputVegstructureYearly_Header[] =
{
#ifdef  OutputVegstructureYearly_Item__species
    OutputVegstructureYearly_Item__species,
#endif    
    "area_cover", 
    "area_fraction", 
    "n_tree", 
    "vol_tree[m3ha-1]",
    "h_max[m]", 
    "h_min[m]", 
    "diam_ground[m]", 
    "diam_breast[m]", 
    "depth[m]", 
    "lai",
    "crown_diam_ratio", 
    "sap_fol_ratio",
    "branch_fraction",
    "stem_wood[kgm-2]", 
    "branch_wood[kgm-2]", 
    "root_wood[kgm-2]"
};
#define  OutputVegstructureYearly_Datasize  (sizeof( OutputVegstructureYearly_Header) / sizeof( OutputVegstructureYearly_Header[0]))
ldndc_output_size_t const  OutputVegstructureYearly_Sizes[] =
{
#ifdef  OutputVegstructureYearly_Item__species
    1,
    OutputVegstructureYearly_Datasize - 1,
#else
    OutputVegstructureYearly_Datasize,
#endif
    OutputVegstructureYearly_Datasize /*total size*/
};
ldndc_output_size_t const *  OutputVegstructureYearly_EntitySizes = NULL;
#define  OutputVegstructureYearly_Rank  ((ldndc_output_rank_t)(sizeof( OutputVegstructureYearly_Sizes) / sizeof( OutputVegstructureYearly_Sizes[0])) - 1)
atomic_datatype_t const  OutputVegstructureYearly_Types[] =
{
#ifdef  OutputVegstructureYearly_Item__species
    LDNDC_STRING,
#endif
    LDNDC_FLOAT64
};


LMOD_OUTPUT_MODULE_NAME::LMOD_OUTPUT_MODULE_NAME(
        MoBiLE_State *  _state,
        cbm::io_kcomm_t *  _io_kcomm,
        timemode_e  _timemode)
        : LMOD_OUTPUT_MODULE_BASE( _state, _io_kcomm, _timemode),

          io_kcomm( _io_kcomm),

          phys( _state->get_substate< substate_physiology_t >()),
          m_veg( &_state->vegetation)
{
}


LMOD_OUTPUT_MODULE_NAME::~LMOD_OUTPUT_MODULE_NAME()
{
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::configure(
                ldndc::config_file_t const *  _cf)
{
    lerr_t  rc_setflags =
        this->set_metaflags( _cf, RM_DEFAULT_YEARLY);
    if ( rc_setflags)
        { return  LDNDC_ERR_FAIL; }

    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::initialize()
{
    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::sink_handle_acquire_(
        sink_handle_t &  _snk,
        int  _index)
{
    int  sink_index = _index;
    if ( !this->separate_concurrent())
        { sink_index = 0; }
    _snk = this->io_kcomm->sink_handle_acquire( "vegstructureyearly", sink_index);
    if ( _snk.status() == LDNDC_ERR_OK)
    {
        lerr_t  rc_layout = LDNDC_OUTPUT_DEFINE_SINK_LAYOUT_DEFAULTS(
            _snk,OutputVegstructureYearly);
        RETURN_IF_NOT_OK(rc_layout);
    }
    else
    {
        KLOGERROR( "sink status bad  [sink=","vegstructureyearly","]");
        return  _snk.status();
    }

    return  LDNDC_ERR_OK;
}
lerr_t
LMOD_OUTPUT_MODULE_NAME::acquire_sinks_if_needed_()
{
    size_t const  c = this->m_veg->size();
    if ( c == 0)
    {
        return  LDNDC_ERR_OK;
    }
    else if ( c > 1)
    {
        if ( !this->m_sinksum.is_acquired())
        {
            lerr_t  rc_snk_avg = this->sink_handle_acquire_( this->m_sinksum, 0);
            RETURN_IF_NOT_OK(rc_snk_avg);
        }
    }

    size_t const  C = this->m_veg->slot_cnt()-1;
    if ( cbm::is_invalid( C) || ( this->m_sinks.size() > C))
    {
        return  LDNDC_ERR_OK;
    }

    /* recruit more sinks */
    for ( size_t  s = this->m_sinks.size();  s <= C;  ++s)
    {
        ldndc::sink_handle_t  snk;
        lerr_t  rc_snk = this->sink_handle_acquire_( snk, s+1);
        RETURN_IF_NOT_OK(rc_snk);

        this->m_sinks.push_back( snk);
    }

    return  LDNDC_ERR_OK;
}

lerr_t
LMOD_OUTPUT_MODULE_NAME::solve()
{
    lerr_t  rc_sinks = this->acquire_sinks_if_needed_();
    RETURN_IF_NOT_OK(rc_sinks);
    if ( this->m_sinks.size() == 0)
    {
        return  LDNDC_ERR_OK;
    }

    size_t const  c = this->m_veg->size();

    ldndc_flt64_t  data_flt64_a[OutputVegstructureYearly_Datasize];
    cbm::mem_set( data_flt64_a, OutputVegstructureYearly_Datasize, 0.0);
    for ( PlantIterator  vt = this->m_veg->begin(); vt != this->m_veg->end(); ++vt)
    {
#ifdef  OutputVegstructureYearly_Item__species
        ldndc_string_t  data_string[1];
        cbm::as_strcpy( &data_string[0], (*vt)->cname());
#endif
        ldndc_flt64_t  data_flt64_0[OutputVegstructureYearly_Datasize];
        lerr_t  rc_dump = this->m_writerecord( *vt, data_flt64_0, data_flt64_a);
        RETURN_IF_NOT_OK(rc_dump);

#ifdef  OutputVegstructureYearly_Item__species
        void *  data[] = { data_string, data_flt64_0};
#else
        void *  data[] = { data_flt64_0};
#endif
        lerr_t  rc_write =
            this->write_fixed_record( &this->m_sinks[(*vt)->slot], data);
        if ( rc_write)
            { return  LDNDC_ERR_FAIL; }
    }

    if ( this->m_sinksum.is_acquired())
    {
        if ( c > 0)
        {
#ifdef  OutputVegstructureYearly_Item__species
            ldndc_string_t  data_string[1];
            cbm::as_strcpy( &data_string[0], COLUMN_NAME_ALLSPECIES);
            void *  data_a[] = { data_string, data_flt64_a};
#else
            void *  data_a[] = { data_flt64_a};
#endif
            lerr_t  rc_write =
                this->write_fixed_record( &this->m_sinksum, data_a);
            if ( rc_write)
                { return  LDNDC_ERR_FAIL; }
        }
    }
    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::finalize()
{
    for ( size_t  s = 0;  s < this->m_sinks.size();  ++s)
    {
        if ( this->m_sinks[s].is_acquired())
        {
            this->io_kcomm->sink_handle_release( &this->m_sinks[s]);
        }
    }
    this->m_sinks.clear();

    if ( this->m_sinksum.is_acquired())
    {
        this->io_kcomm->sink_handle_release( &this->m_sinksum);
    }
    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::m_writerecord(
                                       MoBiLE_Plant *   _s,
                                       ldndc_flt64_t *  _buf,
                                       ldndc_flt64_t *  _buf_avg)
{
    /* calculate averaging factors */

    double f_area_tree( 0.0);
    for ( TreeIterator w = m_veg->groupbegin< species::wood >(); w != m_veg->groupend< species::wood >();  ++w)
    {
        f_area_tree += (*w)->f_area;
    }
    double f_area_all( 0.0);
    for( PlantIterator  vt = m_veg->begin(); vt != m_veg->end(); ++vt)
    {
        f_area_all += (*vt)->f_area;
    }
    double const f_s_tree( (f_area_tree > 0.0) ? _s->f_area / f_area_tree : 0.0);
    double const f_s_all( (f_area_all > 0.0) ? _s->f_area / f_area_all : 0.0);


    /* begin record */

    LDNDC_OUTPUT_SET_COLUMN_INDEX(0);

    double area_cover( 0.0);
    for ( PlantIterator vt = this->m_veg->begin(); vt != this->m_veg->end();  ++vt)
    {
        area_cover += (*vt)-> f_area;
    }
    area_cover = cbm::bound_max( area_cover, 1.0);
    
    LDNDC_OUTPUT_SET_COLUMN_AVG_SET(area_cover);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(area_cover);

    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_s->f_area);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_s->f_area);

    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_s->tree_number);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_s->tree_number);

    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_s->stand_volume());
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_s->stand_volume());

    LDNDC_OUTPUT_SET_COLUMN_AVG_MAX(_s->height_max);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_s->height_max);

    if ( _buf_avg)
    {
        if ( cbm::flt_equal_zero( LDNDC_OUTPUT_GET_CURRENT_COLUMN_VALUE(_buf_avg)))
        {
            LDNDC_OUTPUT_SET_COLUMN_AVG_SET(_s->height_at_canopy_start);
        }
        else
        {
            LDNDC_OUTPUT_SET_COLUMN_AVG_MIN(_s->height_at_canopy_start);
        }
    }
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_s->height_at_canopy_start);

    if ( _s->groupId() == SPECIES_GROUP_WOOD)
    {
        LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_s->dbas * f_s_tree);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_s->dbas);

        LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_s->dbh * f_s_tree);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_s->dbh);
    }
    else
    {
        LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(0.0);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_s->dbas);

        LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(0.0);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_s->dbh);
    }
    LDNDC_OUTPUT_SET_COLUMN_AVG_MAX(_s->rooting_depth);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_s->rooting_depth);

    double const lai( cbm::sum( _s->lai_fl, _s->nb_foliagelayers()));
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(lai * f_s_all);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(lai);

    if ( _s->groupId() == SPECIES_GROUP_WOOD)
    {
        LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_s->cdr * f_s_tree);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_s->cdr);

        LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_s->qsfm * f_s_tree);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_s->qsfm);

        LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_s->f_branch * f_s_tree);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_s->f_branch);

        double const  fSC = _s->mSap + _s->mCor + _s->mBud;

        double const  stemWodAvg = fSC * ( 1.0 - (*_s)->UGWDF()) * ( 1.0 - _s->f_branch);
        LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(stemWodAvg);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(stemWodAvg);

        double const  braWodAvg = fSC * ( 1.0 - (*_s)->UGWDF()) * _s->f_branch;
        LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(braWodAvg);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(braWodAvg);

        double const  rootWodAvg = fSC * (*_s)->UGWDF();
        LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(rootWodAvg);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(rootWodAvg);
    }
    else
    {
        LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(0.0);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( _s->cdr);

        LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(0.0);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( _s->qsfm);

        LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(0.0);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( _s->f_branch);

        double const  fSC = _s->mSap + _s->mCor;

        double const  stemWodAvg = fSC * ( 1.0 - (*_s)->UGWDF()) * ( 1.0 - _s->f_branch);
        LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(0.0);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(stemWodAvg);

        double const  braWodAvg = fSC * ( 1.0 - (*_s)->UGWDF()) * _s->f_branch;
        LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(0.0);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(braWodAvg);

        double const  rootWodAvg = fSC * (*_s)->UGWDF();
        LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(0.0);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(rootWodAvg);
    }

    return  LDNDC_ERR_OK;
}

#undef  LMOD_OUTPUT_MODULE_BASE
#undef  LMOD_OUTPUT_MODULE_NAME

#undef  OutputVegstructureYearly_Rank
#undef  OutputVegstructureYearly_Datasize

} /*namespace ldndc*/

