/*!
 * @file
 * @author
 *  - Edwin Haas
 *  - Ruediger Grote
 */

#include  "output/physiology/output-physiology-layer-daily.h"
#include  <utils/cbm_utils.h>

#define  LMOD_OUTPUT_MODULE_BASE  MBE_LegacyOutputModel
#define  LMOD_OUTPUT_MODULE_NAME  OutputPhysiologyLayerDaily
#define  LMOD_OUTPUT_MODULE_TIMEMODE  TMODE_POST_DAILY

LMOD_MODULE_INFO(LMOD_OUTPUT_MODULE_NAME,LMOD_OUTPUT_MODULE_TIMEMODE,LMOD_FLAG_USER|LMOD_FLAG_OUTPUT);
namespace ldndc {


/*!
 * @page physiologyoutput
 * @section physiologyoutputlayerdaily Physiology output (layer/daily)
 *
 * <b>xml-based module selection in project's setup:</b>
 * \n\code{.xml}< module id="output:physiology-layer:daily" />\endcode\n
 *
 * entity name | decription | unit
 * ----------- | ---------- | ----
 * layer | Canopy/soil layer | [-]
 * level | Level height | [m]
 * extension | Height | [m]
 * DW\_fol/DW\_frt | Foliage (living and dead) / fine roots biomass | [kgDW m-2]
 * lai | Leaf area index | [-]
 * dN\_upt\_nh3 | Nitrogen uptake | [kgN]
 * dN\_upt\_nox | Nitrogen gasses uptake | [kgN]
 * dC\_co2\_upt | Carbon dioxide uptake | [kgN]
 * vc\_act\_25 | Activity state of rubisco at 25 degrees celsius  | [umol m-2 s-1]
 * vc\_max | Actual activity state of rubisco | [umol m-2 s-1]
 * j\_max | Maximum rate of electron transport | [umol m-2 s-1]
 * j\_pot | Potential rate of electron transport | [umol m-2 s-1]
 * ci | Intercellular concentration of CO2  | [umol m-2 s-1]
 * iso\_act | Activity state of isoprene synthase | [nmol m-2 s-1]
 * mono\_act | Activity state of monoterpene synthase  | [nmol m-2 s-1]
 * iso\_emis | Isoprene emission from plants | [umol m-2 s-1]
 * mono\_emis | Monoterpene emission from plants | [umol m-2 s-1]
 * rootlength | root length | [m]
 */
ldndc_string_t const  OutputPhysiologyLayerDaily_Ids[] =
{
    "level",
    "extension",
    "DW_fol_frt",
    "lai",
    "dN_nh3_nh4_upt",
    "dN_nox_no3_upt",
    "dC_co2_upt",
    "vc_act_25",
    "vc_max",
    "j_max",
    "j_pot",
    "ci",
    "iso_act",
    "mono_act",
    "iso_emis",
    "mono_emis",
    "rootlength"
};
ldndc_string_t const  OutputPhysiologyLayerDaily_Header[] =
{
    "level[m]",
    "extension[m]",
    "DW_fol_frt[kgDWm-2]",
    "lai[-]",
    "dN_nh3_nh4_upt[kgN]",
    "dN_nox_no3_upt[kgN]",
    "dC_co2_upt[kgC]",
    "vc_act_25[umol:m-2:s-1]",
    "vc_max[umol:m-2:s-1]",
    "j_max[umol:m-2:s-1]",
    "j_pot[umol:m-2:s-1]",
    "ci[umol:m-2:s-1]",
    "iso_act[nmol:m-2:s-1]",
    "mono_act[nmol:m-2:s-1]",
    "iso_emis[umol:m-2]",
    "mono_emis[umol:m-2]",
    "rootlength[m]"
};
#define  OutputPhysiologyLayerDaily_Datasize  (sizeof( OutputPhysiologyLayerDaily_Header) / sizeof( OutputPhysiologyLayerDaily_Header[0]))
ldndc_output_size_t const  OutputPhysiologyLayerDaily_Sizes[] =
{
    OutputPhysiologyLayerDaily_Datasize,

    OutputPhysiologyLayerDaily_Datasize /*total size*/
};
ldndc_output_size_t const *  OutputPhysiologyLayerDaily_EntitySizes = NULL;
#define  OutputPhysiologyLayerDaily_Rank  ((ldndc_output_rank_t)(sizeof( OutputPhysiologyLayerDaily_Sizes) / sizeof( OutputPhysiologyLayerDaily_Sizes[0])) - 1)
atomic_datatype_t const  OutputPhysiologyLayerDaily_Types[] =
{
    LDNDC_FLOAT64
};


LMOD_OUTPUT_MODULE_NAME::LMOD_OUTPUT_MODULE_NAME(
        MoBiLE_State *  _state,
        cbm::io_kcomm_t *  _io_kcomm,
        timemode_e  _timemode)
    : LMOD_OUTPUT_MODULE_BASE( _state, _io_kcomm, _timemode),

          io_kcomm( _io_kcomm),

          soillayers_( _io_kcomm->get_input_class< input_class_soillayers_t >()),
          setup_( _io_kcomm->get_input_class< input_class_setup_t >()),
          ph( _state->get_substate< substate_physiology_t >()),
          sc( _state->get_substate< substate_soilchemistry_t >()),

          m_veg( &_state->vegetation)
{
    this->rc.foliage_all = 1;
    this->rc.soil_all = 1;

    this->acc.nh4_uptake_sl = new double[this->soillayers_->soil_layer_cnt()];
    this->acc.no3_uptake_sl = new double[this->soillayers_->soil_layer_cnt()];
}


LMOD_OUTPUT_MODULE_NAME::~LMOD_OUTPUT_MODULE_NAME()
{
    delete[] this->acc.nh4_uptake_sl;
    delete[] this->acc.no3_uptake_sl;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::configure(
                ldndc::config_file_t const *  _cf)
{
    lerr_t  rc_setflags =
        this->set_metaflags( _cf, RM_DEFAULT_LAYERDAILY);
    if ( rc_setflags)
        { return  LDNDC_ERR_FAIL; }

    bool  rc_option;

    /* 0 = output is first, mid, and last of all foliage layers; 1 = all layers */
    CF_LMOD_QUERY(_cf, "write_foliage_layers_all", rc_option, true);
    this->rc.foliage_all = rc_option;
    /* 0 = output is first, mid, and last of all soil and litter layers; 1 = all layers */
    CF_LMOD_QUERY(_cf, "write_soil_layers_all", rc_option, true);
    this->rc.soil_all = rc_option;

    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::initialize()
{
    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::sink_handle_acquire_()
{
    size_t const  c = this->m_veg->size();
    if ( !this->m_sink.is_acquired() && ( c > 0))
    {
        this->m_sink = this->io_kcomm->sink_handle_acquire( "physiologylayerdaily");
        if ( this->m_sink.status() == LDNDC_ERR_OK)
        {
            lerr_t  rc_layout = LDNDC_OUTPUT_DEFINE_SINK_LAYOUT_DEFAULTS(
                this->m_sink,OutputPhysiologyLayerDaily);
            RETURN_IF_NOT_OK(rc_layout);
        }
        else
        {
            KLOGERROR( "sink status bad  [sink=","physiologylayerdaily","]");
            return  this->m_sink.status();
        }
    }

    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::solve()
{
    lerr_t  rc_sinks = this->sink_handle_acquire_();
    RETURN_IF_NOT_OK(rc_sinks);

    if ( !this->m_sink.is_acquired())
        { return  LDNDC_ERR_OK; }

    size_t const  c = this->m_veg->size();

    if ( c == 0)
    {
        return  LDNDC_ERR_OK;
    }

    ldndc_flt64_t  data_flt64_0[OutputPhysiologyLayerDaily_Datasize];

    lerr_t  rc_dump = this->m_writerecord( data_flt64_0);
    if ( rc_dump)
    { return  LDNDC_ERR_FAIL; }

    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::finalize()
{
    if ( this->m_sink.is_acquired())
    {
        this->io_kcomm->sink_handle_release( &this->m_sink);
    }
    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::m_writerecord( ldndc_flt64_t *  _buf)
{
    size_t const  CF = std::min( m_veg->canopy_layers_used(), this->ph->h_fl.size());
    size_t const  F = ( CF == 0) ? 0 : ( CF - 1);
    size_t const  CS = this->soillayers_->soil_layer_cnt();
    size_t const  CL = this->soillayers_->soil_layers_in_litter_cnt();
    size_t const  S = ( CS == 0) ? 0 : ( CS - 1);

    size_t const  F_MID = static_cast< size_t >( 0.5 * CF);
    size_t const  S_MID = static_cast< size_t >( 0.5 * CS + CL);

    // - above ground
    double  h = ( CF == 0) ? 0.0 : this->ph->h_fl.sum(CF);
    for ( size_t  l = 0;  l < CF;  ++l)
    {
        size_t const  fl = F-l;

        double  vcAct25 = 0.0;
        double  vcMax = 0.0;
        double  jMax = 0.0;
        double  jPot = 0.0;
        double  ci = 0.0;
        double  cUpt = 0.0;
        double  isoAct = 0.0;
        double  monoAct = 0.0;
        double  isoEmi = 0.0;
        double  monoEmi = 0.0;
        double  lai = 0.0;

        for ( PlantIterator vt = this->m_veg->begin(); vt != this->m_veg->end(); ++vt)
        {
            MoBiLE_Plant *p = *vt;
            vcAct25 += p->vcAct25_fl[fl] * p->lai_fl[fl];
            vcMax += p->vcMax_fl[fl] * p->lai_fl[fl];
            jMax += p->d_jMax_fl[fl] * p->lai_fl[fl];
            jPot += p->d_jPot_fl[fl] * p->lai_fl[fl];
            ci += p->d_co2i_fl[fl] * p->lai_fl[fl];
            isoAct += p->isoAct_fl[fl] * p->lai_fl[fl];
            monoAct += p->monoAct_fl[fl] * p->lai_fl[fl];
            isoEmi += p->d_isoprene_emission_fl[fl];
            monoEmi += p->d_monoterpene_emission_fl[fl];
            cUpt += p->d_carbonuptake_fl[fl];
            lai += p->lai_fl[fl];
        }

        if ( lai > 0.0)
        {
            vcAct25 /= lai;
            vcMax /= lai;
            jMax /= lai;
            jPot /= lai;
            ci /= lai;
            isoAct /= lai;
            monoAct /= lai;
        }

        if (( this->rc.foliage_all) || ( fl == 0) || ( fl == F_MID) || ( fl == F))
        {
            LDNDC_OUTPUT_SET_COLUMN_INDEX(0);

            LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(h);
            LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->ph->h_fl[fl]);

            double  m_fol_fl( 0.0);
            double  rootlength_fl( 0.0);
            for ( PlantIterator vt = this->m_veg->begin(); vt != this->m_veg->end(); ++vt)
            {
                m_fol_fl += (*vt)->m_fol_fl(fl);
            }
            LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(m_fol_fl);
            LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(lai);
            LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->ph->nd_nh3_uptake_fl[fl]);
            LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->ph->nd_nox_uptake_fl[fl]);
            LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(cUpt);
            LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(vcAct25);
            LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(vcMax);
            LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(jMax );
            LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(jPot );
            LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(ci);
            LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(isoAct);
            LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(monoAct);
            LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(isoEmi);
            LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(monoEmi);
            LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(rootlength_fl);

            void *  data_above[] = { _buf};
            this->set_layernumber( fl+1);
            lerr_t  rc_write =
                this->write_fixed_record( &this->m_sink, data_above);
            if ( rc_write)
                { return  LDNDC_ERR_FAIL; }
        }

        h -= this->ph->h_fl[fl];
    }


    // - below ground
    cbm::mem_set( _buf, OutputPhysiologyLayerDaily_Datasize, ldndc::invalid_t< ldndc_flt64_t >::value);
    for ( size_t  sl = 0;  sl < CS;  ++sl)
    {
        this->acc.nh4_uptake_sl[sl] = cbm::bound_min( 0.0,
                                           ph->accumulated_nh4_uptake_sl[sl] - this->acc.nh4_uptake_sl[sl]);
        this->acc.no3_uptake_sl[sl] = cbm::bound_min( 0.0,
                                           ph->accumulated_no3_uptake_sl[sl] - this->acc.no3_uptake_sl[sl]);

        if (( this->rc.soil_all) || (( sl == 0) || ( sl == CL) || ( sl == S_MID) || ( sl == S)))
        {
            
            LDNDC_OUTPUT_SET_COLUMN_INDEX(0);

            LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(-this->sc->depth_sl[sl]);
            LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->sc->h_sl[sl]);

            LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(ph->mfrt_sl(m_veg, sl));
            LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(ldndc::invalid_t< ldndc_flt64_t >::value);
            LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->acc.nh4_uptake_sl[sl]);
            LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->acc.no3_uptake_sl[sl]);
            
            LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(ldndc::invalid_t< ldndc_flt64_t >::value);
            LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(ldndc::invalid_t< ldndc_flt64_t >::value);
            LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(ldndc::invalid_t< ldndc_flt64_t >::value);
            LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(ldndc::invalid_t< ldndc_flt64_t >::value);
            LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(ldndc::invalid_t< ldndc_flt64_t >::value);
            LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(ldndc::invalid_t< ldndc_flt64_t >::value);
            LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(ldndc::invalid_t< ldndc_flt64_t >::value);
            LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(ldndc::invalid_t< ldndc_flt64_t >::value);
            LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(ldndc::invalid_t< ldndc_flt64_t >::value);
            LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(ldndc::invalid_t< ldndc_flt64_t >::value);
            LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(ph->rootlength_sl(m_veg, sl));

            void *  data_below[] = { _buf};
            this->set_layernumber( -static_cast< int >( sl) - 1);
            lerr_t  rc_write =
                this->write_fixed_record( &this->m_sink, data_below);
            if ( rc_write)
                { return  LDNDC_ERR_FAIL; }
            
            this->acc.nh4_uptake_sl[sl] = ph->accumulated_nh4_uptake_sl[sl];
            this->acc.no3_uptake_sl[sl] = ph->accumulated_no3_uptake_sl[sl];
        }
    }


    return  LDNDC_ERR_OK;
}

#undef  LMOD_OUTPUT_MODULE_BASE
#undef  LMOD_OUTPUT_MODULE_NAME

#undef  OutputPhysiologyLayerDaily_Rank
#undef  OutputPhysiologyLayerDaily_Datasize

} /*namespace ldndc*/

