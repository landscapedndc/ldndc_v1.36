/*!
 * @file
 * @author
 *  - Edwin Haas
 *  - Ruediger Grote
 */

#include  "output/physiology/output-physiology-daily.h"

#include  <utils/cbm_utils.h>
#include  <math/cbm_math.h>

#define  LMOD_OUTPUT_MODULE_BASE  MBE_LegacyOutputModel
#define  LMOD_OUTPUT_MODULE_NAME  OutputPhysiologyDaily
#define  LMOD_OUTPUT_MODULE_TIMEMODE  TMODE_POST_DAILY

LMOD_MODULE_INFO(LMOD_OUTPUT_MODULE_NAME,LMOD_OUTPUT_MODULE_TIMEMODE,LMOD_FLAG_USER|LMOD_FLAG_OUTPUT);
namespace ldndc {

#define  OutputPhysiologyDaily_Item__species
#define  OutputPhysiologyDaily_Item__dEmerg
#define  OutputPhysiologyDaily_Item__tCum
#define  OutputPhysiologyDaily_Item__dvsflush
#define  OutputPhysiologyDaily_Item__dvsmort
#define  OutputPhysiologyDaily_Item__uptDON
#define  OutputPhysiologyDaily_Item__uptNH4
#define  OutputPhysiologyDaily_Item__uptNO3
#define  OutputPhysiologyDaily_Item__uptNH3
#define  OutputPhysiologyDaily_Item__uptNOx
#define  OutputPhysiologyDaily_Item__fixN2
#define  OutputPhysiologyDaily_Item__cumfixN2
#define  OutputPhysiologyDaily_Item__dcFol
#define  OutputPhysiologyDaily_Item__dcFru
#define  OutputPhysiologyDaily_Item__dcFrt
#define  OutputPhysiologyDaily_Item__dcLst
#define  OutputPhysiologyDaily_Item__dcFac
#define  OutputPhysiologyDaily_Item__rFol
#define  OutputPhysiologyDaily_Item__rFru
#define  OutputPhysiologyDaily_Item__rFrt
#define  OutputPhysiologyDaily_Item__rLst
#define  OutputPhysiologyDaily_Item__rRes
#define  OutputPhysiologyDaily_Item__rTra
#define  OutputPhysiologyDaily_Item__rGro
#define  OutputPhysiologyDaily_Item__cUpt
#define  OutputPhysiologyDaily_Item__fFac
#define  OutputPhysiologyDaily_Item__mFol
#define  OutputPhysiologyDaily_Item__mFru
#define  OutputPhysiologyDaily_Item__mStemFru
#define  OutputPhysiologyDaily_Item__mFrt
#define  OutputPhysiologyDaily_Item__dw_lst
#define  OutputPhysiologyDaily_Item__dw_dst
#define  OutputPhysiologyDaily_Item__mBelow
#define  OutputPhysiologyDaily_Item__mAbove
#define  OutputPhysiologyDaily_Item__lai
#define  OutputPhysiologyDaily_Item__sla
#define  OutputPhysiologyDaily_Item__nRet
#define  OutputPhysiologyDaily_Item__ncFol
#define  OutputPhysiologyDaily_Item__ncFru
#define  OutputPhysiologyDaily_Item__ncFrt
#define  OutputPhysiologyDaily_Item__ncLst
#define  OutputPhysiologyDaily_Item__ncCor
#define  OutputPhysiologyDaily_Item__ntot
#define  OutputPhysiologyDaily_Item__sFol
#define  OutputPhysiologyDaily_Item__sFru
#define  OutputPhysiologyDaily_Item__sFrt
#define  OutputPhysiologyDaily_Item__sWoodBelow
#define  OutputPhysiologyDaily_Item__sWoodAbove
#define  OutputPhysiologyDaily_Item__exsuLoss
#define  OutputPhysiologyDaily_Item__nLitFol
#define  OutputPhysiologyDaily_Item__nLitFru
#define  OutputPhysiologyDaily_Item__nLitFrt
#define  OutputPhysiologyDaily_Item__nLitWoodBelow
#define  OutputPhysiologyDaily_Item__nLitWoodAbove
#define  OutputPhysiologyDaily_Item__droughtRel
#define  OutputPhysiologyDaily_Item__conductRel
#define  OutputPhysiologyDaily_Item__vcact25
#define  OutputPhysiologyDaily_Item__isoAct
#define  OutputPhysiologyDaily_Item__monoAct
#define  OutputPhysiologyDaily_Item__isoEmi
#define  OutputPhysiologyDaily_Item__monoEmi
#define  OutputPhysiologyDaily_Item__monosEmi
#define  OutputPhysiologyDaily_Item__ovocEmi
#define  OutputPhysiologyDaily_Item__throu_nh4
#define  OutputPhysiologyDaily_Item__throu_no3


/*!
 * @page physiologyoutput
 * @section physiologyoutputdaily Physiology output (daily)
 *
 * <b>xml-based module selection in project's setup:</b>
 * \n\code{.xml}< module id="output:physiology:daily" />\endcode\n
 *
 * entity name | decription | unit
 * ----------- | ---------- | ----
 * species  | Abbreviated name of species | [-]
 * day\_emergence | Day of emergence | [-]
 * gdd | Accumulated growing degree days | [oC]
 * dvs\_flush | Relative state of foliage flushing in forest simulations | [-]
 * dvs\_mort | Relative state of foliage senescence | [-]
 * dN\_don\_upt | Plant uptake of soil organic nitrogen | [kgN m-2 ts-1]
 * dN\_nh4\_upt | Plant uptake of soil ammonium |  [kgN m-2 ts-1]
 * dN\_no3\_upt | Plant uptake of soil nitrate | [kgN m-2 d-1]
 * dN\_nh3\_upt | Plant uptake of soil ammonia | [kgN m-2 d-1]
 * dN\_nox\_upt | Uptake of NOx | [kgN m-2 d-1]
 * dN\_n2\_fix | Nitrogen fixation by plants | [kgN m-2  d-1]
 * dC\_fol\_grow | Daily increase of foliages carbon | [kgC m-2  d-1]
 * dC\_fru\_grow | Daily increase of fruit carbon | [kgC m-2  d-1]
 * dC\_frt\_grow | Daily increase of (fine) roots carbon | [kgC m-2  d-1]
 * dC\_lst\_grow | Daily increase of living structural matter (carbon) | [kgC m-2  d-1]
 * dC\_fac\_grow | Daily carbon allocated to pool available substrate | [kgC m-2  d-1]
 * dC\_fol\_resp | Foliar respiration | [kgC m-2  d-1]
 * dC\_fru\_resp | Fruit respiration | [kgC m-2  d-1]
 * dC\_frt\_resp  |  Fine roots respiration | [kgC m-2  d-1]
 * dC\_lst\_resp  |  Living structural matter respiration | [kgC m-2  d-1]
 * dC\_maintenance\_resp | Total residual respiration | [kgC m-2  d-1]
 * dC\_transport\_resp | Total transport and uptake respiration | [kgC m-2  d-1]
 * dC\_growth\_resp  | Total growth respiration | [kgC m-2  d-1]
 * dC\_co2\_upt | Carbon uptake-Photosynthesis rate | [kgC m-2  d-1]
 * f\_Fac  | Relative state of free available carbon | [-]
 * DW\_fol | Living foliar biomass | [kgDW m-2]
 * DW\_dfol | Dead foliar biomass | [kgDW m-2]
 * DW\_fru | Fruit biomass | [kgDW m-2]
 * DW\_stemfru | Stem and fruit biomass | [kgDW m-2]
 * DW\_frt | Fine roots biomass | [kgDW m-2]
 * DW\_lst  | Living structural matter (e.g., young wood) biomass | [kgDW m-2]
 * DW\_dst  | Dead structural matter (e.g., old wood) biomass | [kgDW m-2]
 * DW\_above  | Total aboveground biomass | [kgDW m-2]
 * DW\_below  | Total belowground  biomass | [kgDW m-2]
 * lai | Leaf area index | [m2 m-2]
 * sla | Specifc leaf area | [m-2 kg-1]
 * n\_ret | Retention of nitrogen | [kgN m-2]
 * NC\_fol | Nitrogen content in the foliage | [kgN kgDW-1]
 * NC\_frt | Nitrogen content in the fine roots | [kgN kgDW-1]
 * NC\_fru | Nitrogen content in the fruit | [kgN kgDW-1]
 * NC\_lst | Nitrogen content in  living structural matter (e.g., young wood)  | [kgN kgDW-1]
 * NC\_dst | Nitrogen content in  dead structural matter (e.g., old wood)  | [kgN kgDW-1]
 * N\_total | Total plant nitrogen | [kgN m-2]
 * dDW\_fol\_sen | Foliage senescence | [kgDW m-2 d-1]
 * dDW\_fru\_sen | Fruit senescence | [kgDW m-2 d-1]
 * dDW\_frt\_sen | Fine roots senescence | [kgDW m-2 d-1]
 * dDW\_lst\_sen | Living structural matter (e.g., young wood)  senescence | [kgDW m-2 d-1]
 * dDW\_dst\_sen | Dead structural matter (e.g., old wood)  senescence | [kgDW m-2 d-1]
 * dC\_exsudates | Root exsudates losses | [kgC m-2 d-1]
 * dN\_lit\_fol | Nitrogen in the foliage-litter | [kgN m-2 d-1]
 * dN\_lit\_fru | Nitrogen in the Fruit-litter | [kgN m-2 d-1]
 * dN\_lit\_frt | Nitrogen in the fine roots-litter | [kgN m-2 d-1]
 * dN\_lit\_ldst_below | Nitrogen in aboveground living and dead structural matter | [kgN m-2 d-1]
 * dN\_lit\_ldst_above | Nitrogen in belowground living and dead structural matter | [kgN m-2 d-1]
 * drought\_rel | Species specific drought stress factor | [-]
 * conduct\_rel | Relative stomatal conductance | [-]
 * vc\_act\_25 | Activity state of rubisco at 25 degrees celsius | [umol m-2 d-1]
 * iso\_act |  Activity state of isoprene synthase | [umol m-2 d-1]
 * d\_mono\_act |   Activity state of monoterpene synthase | [umol m-2 d-1]
 * d\_iso\_emis | Isoprene emission from plants | [umol m-2 d-1]
 * d\_mono\_emis | Monoterpene emission from plants | [umol m-2 d-1]
 * d\_mono\_storage_emis | Monoterpene emission from plant storage | [umol:m-2]
 * d\_ovoc\_emis | OVOC emission from plant storage | [umol:m-2]
 * dN\_nh4\_throughf | Ammonium deposition through rainfall | [kgN L-1]
 * dN\_no3\_throughf | Nitrate deposition throug rainfall | [kgN L-1]
 */
ldndc_string_t const  OutputPhysiologyDaily_Ids[] =
{
#ifdef  OutputPhysiologyDaily_Item__species
    "species",
#endif
#ifdef  OutputPhysiologyDaily_Item__dEmerg
    "day_emergence",
#endif
#ifdef  OutputPhysiologyDaily_Item__tCum
    "gdd",
#endif
#ifdef  OutputPhysiologyDaily_Item__dvsflush
    "dvs_flush",
#endif
#ifdef  OutputPhysiologyDaily_Item__dvsmort
    "dvs_mort",
#endif
#ifdef  OutputPhysiologyDaily_Item__uptDON
    "dN_don_upt",
#endif
#ifdef  OutputPhysiologyDaily_Item__uptNH4
    "dN_nh4_upt",
#endif
#ifdef  OutputPhysiologyDaily_Item__uptNO3
    "dN_no3_upt",
#endif
#ifdef  OutputPhysiologyDaily_Item__uptNH3
    "dN_nh3_upt",
#endif
#ifdef  OutputPhysiologyDaily_Item__uptNOx
    "dN_nox_upt",
#endif
#ifdef  OutputPhysiologyDaily_Item__fixN2
    "dN_n2_fix",
#endif
#ifdef  OutputPhysiologyDaily_Item__cumfixN2
    "N2_fix",
#endif
#ifdef  OutputPhysiologyDaily_Item__dcFol
    "dC_fol_grow",
#endif
#ifdef  OutputPhysiologyDaily_Item__dcFru
    "dC_fru_grow",
#endif
#ifdef  OutputPhysiologyDaily_Item__dcFrt
    "dC_frt_grow",
#endif
#ifdef  OutputPhysiologyDaily_Item__dcLst
    "dC_lst_grow",
#endif
#ifdef  OutputPhysiologyDaily_Item__dcFac
    "dC_fac_grow",
#endif
#ifdef  OutputPhysiologyDaily_Item__rFol
    "dC_fol_resp",
#endif
#ifdef  OutputPhysiologyDaily_Item__rFru
    "dC_fru_resp",
#endif
#ifdef  OutputPhysiologyDaily_Item__rFrt
    "dC_frt_resp",
#endif
#ifdef  OutputPhysiologyDaily_Item__rLst
    "dC_lst_resp",
#endif
#ifdef  OutputPhysiologyDaily_Item__rRes
    "dC_maintenance_resp",
#endif
#ifdef  OutputPhysiologyDaily_Item__rTra
    "dC_transport_resp",
#endif
#ifdef  OutputPhysiologyDaily_Item__rGro
    "dC_growth_resp",
#endif
#ifdef  OutputPhysiologyDaily_Item__cUpt
    "dC_co2_upt",
#endif
#ifdef  OutputPhysiologyDaily_Item__fFac
    "f_fac",
#endif
#ifdef  OutputPhysiologyDaily_Item__mFol
    "DW_fol",
    "DW_dfol",
#endif
#ifdef  OutputPhysiologyDaily_Item__mFru
    "DW_fru",
#endif
#ifdef  OutputPhysiologyDaily_Item__mStemFru
    "DW_stemfru",
#endif
#ifdef  OutputPhysiologyDaily_Item__mFrt
    "DW_frt",
#endif
#ifdef  OutputPhysiologyDaily_Item__dw_lst
    "DW_lst",
#endif
#ifdef  OutputPhysiologyDaily_Item__dw_dst
    "DW_dst",
#endif
#ifdef  OutputPhysiologyDaily_Item__mBelow
    "DW_below",
#endif
#ifdef  OutputPhysiologyDaily_Item__mAbove
    "DW_above",
#endif
#ifdef  OutputPhysiologyDaily_Item__lai
    "lai",
#endif
#ifdef  OutputPhysiologyDaily_Item__sla
    "specific_leaf_area",
#endif
#ifdef  OutputPhysiologyDaily_Item__nRet
    "N_ret",
#endif
#ifdef  OutputPhysiologyDaily_Item__ncFol
    "NC_fol",
#endif
#ifdef  OutputPhysiologyDaily_Item__ncFru
    "NC_fru",
#endif
#ifdef  OutputPhysiologyDaily_Item__ncFrt
    "NC_frt",
#endif
#ifdef  OutputPhysiologyDaily_Item__ncLst
    "NC_lst",
#endif
#ifdef  OutputPhysiologyDaily_Item__ncCor
    "NC_dst",
#endif
#ifdef  OutputPhysiologyDaily_Item__ntot
    "N_total",
#endif
#ifdef  OutputPhysiologyDaily_Item__sFol
    "dDW_fol_sen",
#endif
#ifdef  OutputPhysiologyDaily_Item__sFru
    "dDW_fru_sen",
#endif
#ifdef  OutputPhysiologyDaily_Item__sFrt
    "dDW_frt_sen",
#endif
#ifdef  OutputPhysiologyDaily_Item__sWoodBelow
    "dDW_lst_sen_below",
#endif
#ifdef  OutputPhysiologyDaily_Item__sWoodAbove
    "dDW_lst_sen_above",
#endif
#ifdef  OutputPhysiologyDaily_Item__exsuLoss
    "dC_exsudates",
#endif
#ifdef  OutputPhysiologyDaily_Item__nLitFol
    "dN_lit_fol",
#endif
#ifdef  OutputPhysiologyDaily_Item__nLitFru
    "dN_lit_fru",
#endif
#ifdef  OutputPhysiologyDaily_Item__nLitFrt
    "dN_lit_frt",
#endif
#ifdef  OutputPhysiologyDaily_Item__nLitWoodBelow
    "dN_lit_ldst_below",
#endif
#ifdef  OutputPhysiologyDaily_Item__nLitWoodAbove
    "dN_lit_ldst_above",
#endif
#ifdef  OutputPhysiologyDaily_Item__droughtRel
    "drought_rel",
#endif
#ifdef  OutputPhysiologyDaily_Item__conductRel
    "conduct_rel",
#endif
#ifdef  OutputPhysiologyDaily_Item__vcact25
    "vc_act_25",
#endif
#ifdef  OutputPhysiologyDaily_Item__isoAct
    "iso_act",
#endif
#ifdef  OutputPhysiologyDaily_Item__monoAct
    "mono_act",
#endif
#ifdef  OutputPhysiologyDaily_Item__isoEmi
    "iso_emis",
#endif
#ifdef  OutputPhysiologyDaily_Item__monoEmi
    "mono_emis",
#endif
#ifdef  OutputPhysiologyDaily_Item__monosEmi
    "mono_storage_emis",
#endif
#ifdef  OutputPhysiologyDaily_Item__ovocEmi
    "ovoc_emis",
#endif    
#ifdef  OutputPhysiologyDaily_Item__throu_nh4
    "dN_nh4_throughf",
#endif
#ifdef  OutputPhysiologyDaily_Item__throu_no3
    "dN_no3_throughf",
#endif
};

ldndc_string_t const  OutputPhysiologyDaily_Header[] =
{
#ifdef  OutputPhysiologyDaily_Item__species
    "species",
#endif
#ifdef  OutputPhysiologyDaily_Item__dEmerg
    "day_emergence",
#endif
#ifdef  OutputPhysiologyDaily_Item__tCum
    "gdd[oC]",
#endif
#ifdef  OutputPhysiologyDaily_Item__dvsflush
    "dvs_flush[-]",
#endif
#ifdef  OutputPhysiologyDaily_Item__dvsmort
    "dvs_mort[-]",
#endif
#ifdef  OutputPhysiologyDaily_Item__uptDON
    "dN_don_upt[kgNm-2]",
#endif
#ifdef  OutputPhysiologyDaily_Item__uptNH4
    "dN_nh4_upt[kgNm-2]",
#endif
#ifdef  OutputPhysiologyDaily_Item__uptNO3
    "dN_no3_upt[kgNm-2]",
#endif
#ifdef  OutputPhysiologyDaily_Item__uptNH3
    "dN_nh3_upt[kgNm-2]",
#endif
#ifdef  OutputPhysiologyDaily_Item__uptNOx
    "dN_nox_upt[kgNm-2]",
#endif
#ifdef  OutputPhysiologyDaily_Item__fixN2
    "dN_n2_fix[kgNm-2]",
#endif
#ifdef  OutputPhysiologyDaily_Item__cumfixN2
    "N2_fix[kgNm-2]",
#endif
#ifdef  OutputPhysiologyDaily_Item__dcFol
    "dC_fol_grow[kgCm-2]",
#endif
#ifdef  OutputPhysiologyDaily_Item__dcFru
    "dC_fru_grow[kgCm-2]",
#endif
#ifdef  OutputPhysiologyDaily_Item__dcFrt
    "dC_frt_grow[kgCm-2]",
#endif
#ifdef  OutputPhysiologyDaily_Item__dcLst
    "dC_lst_grow[kgCm-2]",
#endif
#ifdef  OutputPhysiologyDaily_Item__dcFac
    "dC_fac_grow[kgCm-2]",
#endif
#ifdef  OutputPhysiologyDaily_Item__rFol
    "dC_fol_resp[kgCm-2]",
#endif
#ifdef  OutputPhysiologyDaily_Item__rFru
    "dC_fru_resp[kgCm-2]",
#endif
#ifdef  OutputPhysiologyDaily_Item__rFrt
    "dC_frt_resp[kgCm-2]",
#endif
#ifdef  OutputPhysiologyDaily_Item__rLst
    "dC_lst_resp[kgCm-2]",
#endif
#ifdef  OutputPhysiologyDaily_Item__rRes
    "dC_maintenance_resp[kgCm-2]",
#endif
#ifdef  OutputPhysiologyDaily_Item__rTra
    "dC_transport_resp[kgCm-2]",
#endif
#ifdef  OutputPhysiologyDaily_Item__rGro
    "dC_growth_resp[kgCm-2]",
#endif
#ifdef  OutputPhysiologyDaily_Item__cUpt
    "dC_co2_upt[kgCm-2]",
#endif
#ifdef  OutputPhysiologyDaily_Item__fFac
    "f_fac",
#endif
#ifdef  OutputPhysiologyDaily_Item__mFol
    "DW_fol[kgDWm-2]",
    "DW_dfol[kgDWm-2]",
#endif
#ifdef  OutputPhysiologyDaily_Item__mFru
    "DW_fru[kgDWm-2]",
#endif
#ifdef  OutputPhysiologyDaily_Item__mStemFru
    "DW_stemfru[kgDWm-2]",
#endif
#ifdef  OutputPhysiologyDaily_Item__mFrt
    "DW_frt[kgDWm-2]",
#endif
#ifdef  OutputPhysiologyDaily_Item__dw_lst
    "DW_lst[kgDWm-2]",
#endif
#ifdef  OutputPhysiologyDaily_Item__dw_dst
    "DW_dst[kgDWm-2]",
#endif
#ifdef  OutputPhysiologyDaily_Item__mBelow
    "DW_below[kgDWm-2]",
#endif
#ifdef  OutputPhysiologyDaily_Item__mAbove
    "DW_above[kgDWm-2]",
#endif
#ifdef  OutputPhysiologyDaily_Item__lai
    "lai",
#endif
#ifdef  OutputPhysiologyDaily_Item__sla
    "specific_leaf_area[m2kg-1]",
#endif
#ifdef  OutputPhysiologyDaily_Item__nRet
    "N_ret[kgNm-2]",
#endif
#ifdef  OutputPhysiologyDaily_Item__ncFol
    "NC_fol[gNgDW-1]",
#endif
#ifdef  OutputPhysiologyDaily_Item__ncFru
    "NC_fru[gNgDW-1]",
#endif
#ifdef  OutputPhysiologyDaily_Item__ncFrt
    "NC_frt[gNgDW-1]",
#endif
#ifdef  OutputPhysiologyDaily_Item__ncLst
    "NC_lst[gNgDW-1]",
#endif
#ifdef  OutputPhysiologyDaily_Item__ncCor
    "NC_dst[gNgDW-1]",
#endif
#ifdef  OutputPhysiologyDaily_Item__ntot
    "N_total[kgNm-2]",
#endif
#ifdef  OutputPhysiologyDaily_Item__sFol
    "dDW_fol_sen[kgDWm-2]",
#endif
#ifdef  OutputPhysiologyDaily_Item__sFru
    "dDW_fru_sen[kgDWm-2]",
#endif
#ifdef  OutputPhysiologyDaily_Item__sFrt
    "dDW_frt_sen[kgDWm-2]",
#endif
#ifdef  OutputPhysiologyDaily_Item__sWoodBelow
    "dDW_lst_sen_below[kgDWm-2]",
#endif
#ifdef  OutputPhysiologyDaily_Item__sWoodAbove
    "dDW_lst_sen_above[kgDWm-2]",
#endif
#ifdef  OutputPhysiologyDaily_Item__exsuLoss
    "dC_exsudates[kgCm-2]",
#endif
#ifdef  OutputPhysiologyDaily_Item__nLitFol
    "dN_lit_fol[kgNm-2]",
#endif
#ifdef  OutputPhysiologyDaily_Item__nLitFru
    "dN_lit_fru[kgNm-2]",
#endif
#ifdef  OutputPhysiologyDaily_Item__nLitFrt
    "dN_lit_frt[kgNm-2]",
#endif
#ifdef  OutputPhysiologyDaily_Item__nLitWoodBelow
    "dN_lit_ldst_below[kgNm-2]",
#endif
#ifdef  OutputPhysiologyDaily_Item__nLitWoodAbove
    "dN_lit_ldst_above[kgNm-2]",
#endif
#ifdef  OutputPhysiologyDaily_Item__droughtRel
    "drought_rel[-]",
#endif
#ifdef  OutputPhysiologyDaily_Item__conductRel
    "conduct_rel[-]",
#endif
#ifdef  OutputPhysiologyDaily_Item__vcact25
    "vc_act_25[umol:m-2:s-1]",
#endif
#ifdef  OutputPhysiologyDaily_Item__isoAct
    "iso_act[nmol:m-2:s-1]",
#endif
#ifdef  OutputPhysiologyDaily_Item__monoAct
    "mono_act[nmol:m-2:s-1]",
#endif
#ifdef  OutputPhysiologyDaily_Item__isoEmi
    "d_iso_emis[umol:m-2]",
#endif
#ifdef  OutputPhysiologyDaily_Item__monoEmi
    "d_mono_emis[umol:m-2]",
#endif
#ifdef  OutputPhysiologyDaily_Item__monosEmi
    "d_mono_storage_emis[umol:m-2]",
#endif
#ifdef  OutputPhysiologyDaily_Item__ovocEmi
    "d_ovoc_emis[umol:m-2]",
#endif
#ifdef  OutputPhysiologyDaily_Item__throu_nh4
    "dN_nh4_throughf[kgN:L-1]",
#endif
#ifdef  OutputPhysiologyDaily_Item__throu_no3
    "dN_no3_throughf[kgN:L-1]",
#endif
};
#define  OutputPhysiologyDaily_Datasize  (sizeof( OutputPhysiologyDaily_Header) / sizeof( OutputPhysiologyDaily_Header[0]))
ldndc_output_size_t const  OutputPhysiologyDaily_Sizes[] =
{
#ifdef  OutputPhysiologyDaily_Item__species
    1,
    OutputPhysiologyDaily_Datasize - 1,
#else
    OutputPhysiologyDaily_Datasize,
#endif
    OutputPhysiologyDaily_Datasize /*total size*/
};
ldndc_output_size_t const *  OutputPhysiologyDaily_EntitySizes = NULL;
#define  OutputPhysiologyDaily_Rank  ((ldndc_output_rank_t)(sizeof( OutputPhysiologyDaily_Sizes) / sizeof( OutputPhysiologyDaily_Sizes[0])) - 1)
atomic_datatype_t const  OutputPhysiologyDaily_Types[] =
{
#ifdef  OutputPhysiologyDaily_Item__species
    LDNDC_STRING,
#endif
    LDNDC_FLOAT64
};


LMOD_OUTPUT_MODULE_NAME::LMOD_OUTPUT_MODULE_NAME(
        MoBiLE_State *  _state,
        cbm::io_kcomm_t *  _io_kcomm,
        timemode_e  _timemode)
        : LMOD_OUTPUT_MODULE_BASE( _state, _io_kcomm, _timemode),

          io_kcomm( _io_kcomm),

          phys( _state->get_substate< substate_physiology_t >()),
          water( _state->get_substate< substate_watercycle_t >()),
          m_veg( &_state->vegetation),
          soillayers( _io_kcomm->get_input_class< input_class_soillayers_t >())
{
    this->push_accumulated_outputs();
}


LMOD_OUTPUT_MODULE_NAME::~LMOD_OUTPUT_MODULE_NAME()
{
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::configure(
                ldndc::config_file_t const *  _cf)
{
    lerr_t  rc_setflags = this->set_metaflags( _cf);
    if ( rc_setflags)
        { return  LDNDC_ERR_FAIL; }
    return  LDNDC_ERR_OK;
}

lerr_t
LMOD_OUTPUT_MODULE_NAME::initialize()
{
    this->push_accumulated_outputs();
    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::sink_handle_acquire_(
        sink_handle_t &  _snk,
        int  _index)
{
    int  sink_index = _index;
    if ( !this->separate_concurrent())
        { sink_index = 0; }
    _snk = this->io_kcomm->sink_handle_acquire( "physiologydaily", sink_index);
    if ( _snk.status() == LDNDC_ERR_OK)
    {
        lerr_t  rc_layout = LDNDC_OUTPUT_DEFINE_SINK_LAYOUT_DEFAULTS(
            _snk,OutputPhysiologyDaily);
        RETURN_IF_NOT_OK(rc_layout);
    }
    else
    {
        KLOGERROR( "sink status bad  [sink=","physiologydaily","]");
        return  _snk.status();
    }

    return  LDNDC_ERR_OK;
}
lerr_t
LMOD_OUTPUT_MODULE_NAME::acquire_sinks_if_needed_()
{
    size_t const  c = this->m_veg->size();
    if ( c == 0)
    {
        return  LDNDC_ERR_OK;
    }
    else if ( c > 1)
    {
        if ( !this->snk_sum_.is_acquired())
        {
            lerr_t  rc_snk_avg = this->sink_handle_acquire_( this->snk_sum_, 0);
            RETURN_IF_NOT_OK(rc_snk_avg);
        }
    }

    size_t const  C = this->m_veg->slot_cnt()-1;
    if ( cbm::is_invalid( C) || ( this->snks_.size() > C))
    {
        return  LDNDC_ERR_OK;
    }

    /* recruit more sinks */
    for ( size_t  s = this->snks_.size();  s <= C;  ++s)
    {
        ldndc::sink_handle_t  snk;
        lerr_t  rc_snk = this->sink_handle_acquire_( snk, s+1);
        RETURN_IF_NOT_OK(rc_snk);

        this->snks_.push_back( snk);
    }

    return  LDNDC_ERR_OK;
}

lerr_t
LMOD_OUTPUT_MODULE_NAME::solve()
{
    this->pop_accumulated_outputs();

    lerr_t  rc_sinks = this->acquire_sinks_if_needed_();
    RETURN_IF_NOT_OK(rc_sinks);
    if ( this->snks_.size() == 0)
    {
        return  LDNDC_ERR_OK;
    }

    size_t const  c = this->m_veg->size();

    ldndc_flt64_t  data_flt64_a[OutputPhysiologyDaily_Datasize];
    cbm::mem_set( data_flt64_a, OutputPhysiologyDaily_Datasize, 0.0);
    for ( PlantIterator vt = this->m_veg->begin(); vt != this->m_veg->end(); ++vt)
    {
#ifdef  OutputPhysiologyDaily_Item__species
        ldndc_string_t  data_string[1];
        cbm::as_strcpy( &data_string[0], (*vt)->cname());
#endif
        ldndc_flt64_t  data_flt64_0[OutputPhysiologyDaily_Datasize];
        lerr_t  rc_dump = this->m_writerecord( *vt, data_flt64_0, data_flt64_a);
        RETURN_IF_NOT_OK(rc_dump);

#ifdef  OutputPhysiologyDaily_Item__species
        void *  data[] = { data_string, data_flt64_0};
#else
        void *  data[] = { data_flt64_0};
#endif
        lerr_t  rc_write =
            this->write_fixed_record( &this->snks_[(*vt)->slot], data);
        if ( rc_write)
            { return  LDNDC_ERR_FAIL; }
    }

    if ( this->snk_sum_.is_acquired())
    {
        if ( c > 0)
        {
#ifdef  OutputPhysiologyDaily_Item__species
            ldndc_string_t  data_string[1];
            cbm::as_strcpy( &data_string[0], COLUMN_NAME_ALLSPECIES);
            void *  data_a[] = { data_string, data_flt64_a};
#else
            void *  data_a[] = { data_flt64_a};
#endif
            lerr_t  rc_write =
                this->write_fixed_record( &this->snk_sum_, data_a);
            if ( rc_write)
                { return  LDNDC_ERR_FAIL; }
        }
    }
    this->push_accumulated_outputs();

    return  LDNDC_ERR_OK;
}

void
LMOD_OUTPUT_MODULE_NAME::pop_accumulated_outputs()
{
    this->acc.nh3_uptake =
        this->phys->accumulated_nh3_uptake_sl.sum() - this->acc.nh3_uptake;
    this->acc.nh4_uptake =
        this->phys->accumulated_nh4_uptake_sl.sum() - this->acc.nh4_uptake;
    this->acc.no3_uptake =
        this->phys->accumulated_no3_uptake_sl.sum() - this->acc.no3_uptake;
    this->acc.don_uptake =
        this->phys->accumulated_don_uptake_sl.sum() - this->acc.don_uptake;

    this->acc.nh4_throughfall =
        this->phys->accumulated_nh4_throughfall - this->acc.nh4_throughfall;
    this->acc.no3_throughfall =
        this->phys->accumulated_no3_throughfall - this->acc.no3_throughfall;
}
void
LMOD_OUTPUT_MODULE_NAME::push_accumulated_outputs()
{
    this->acc.nh3_uptake = this->phys->accumulated_nh3_uptake_sl.sum();
    this->acc.nh4_uptake = this->phys->accumulated_nh4_uptake_sl.sum();
    this->acc.no3_uptake = this->phys->accumulated_no3_uptake_sl.sum();
    this->acc.don_uptake = this->phys->accumulated_don_uptake_sl.sum();

    this->acc.nh4_throughfall = this->phys->accumulated_nh4_throughfall;
    this->acc.no3_throughfall = this->phys->accumulated_no3_throughfall;
}

lerr_t
LMOD_OUTPUT_MODULE_NAME::finalize()
{
    for ( size_t  s = 0;  s < this->snks_.size();  ++s)
    {
        if ( this->snks_[s].is_acquired())
        {
            this->io_kcomm->sink_handle_release( &this->snks_[s]);
        }
    }
    this->snks_.clear();

    if ( this->snk_sum_.is_acquired())
    {
        this->io_kcomm->sink_handle_release( &this->snk_sum_);
    }
    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::m_writerecord( MoBiLE_Plant * _vt,
        ldndc_flt64_t *  _buf, ldndc_flt64_t * _buf_avg)
{
    foliage_sums_t  fs;
    this->foliage_sums_( _vt, &fs);

    LDNDC_OUTPUT_SET_COLUMN_INDEX(0);

#ifdef  OutputPhysiologyDaily_Item__dEmerg
    LDNDC_OUTPUT_SET_COLUMN_AVG_MAX(_vt->dEmerg);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_vt->dEmerg);
#endif
#ifdef  OutputPhysiologyDaily_Item__tCum
    LDNDC_OUTPUT_SET_COLUMN_AVG_MAX(_vt->growing_degree_days);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_vt->growing_degree_days);
#endif
#ifdef  OutputPhysiologyDaily_Item__dvsflush
    LDNDC_OUTPUT_SET_COLUMN_AVG_MAX(_vt->dvsFlush);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_vt->dvsFlush);
#endif
#ifdef  OutputPhysiologyDaily_Item__dvsmort
    LDNDC_OUTPUT_SET_COLUMN_AVG_MAX(_vt->dvsMort);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_vt->dvsMort);
#endif
#ifdef  OutputPhysiologyDaily_Item__uptDON
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(this->acc.don_uptake);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->acc.don_uptake);
#endif
#ifdef  OutputPhysiologyDaily_Item__uptNH4
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(this->acc.nh4_uptake);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->acc.nh4_uptake);
#endif
#ifdef  OutputPhysiologyDaily_Item__uptNO3
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(this->acc.no3_uptake);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->acc.no3_uptake);
#endif
#ifdef  OutputPhysiologyDaily_Item__uptNH3
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(this->acc.nh3_uptake);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->acc.nh3_uptake);
#endif
#ifdef  OutputPhysiologyDaily_Item__uptNOx
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_vt->d_nox_uptake);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_vt->d_nox_uptake);
#endif
#ifdef  OutputPhysiologyDaily_Item__fixN2
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_vt->d_n2_fixation);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_vt->d_n2_fixation);
#endif
#ifdef  OutputPhysiologyDaily_Item__cumfixN2
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_vt->a_fix_n);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_vt->a_fix_n);
#endif
#ifdef  OutputPhysiologyDaily_Item__dcFol
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_vt->d_dcFol);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_vt->d_dcFol);
#endif
#ifdef  OutputPhysiologyDaily_Item__dcFru
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_vt->d_dcBud);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_vt->d_dcBud);
#endif
#ifdef  OutputPhysiologyDaily_Item__dcFrt
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_vt->d_dcFrt);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_vt->d_dcFrt);
#endif
#ifdef  OutputPhysiologyDaily_Item__dcLst
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_vt->d_dcSap);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_vt->d_dcSap);
#endif
#ifdef  OutputPhysiologyDaily_Item__dcFac
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_vt->d_dcFac);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_vt->d_dcFac);
#endif
#ifdef  OutputPhysiologyDaily_Item__rFol
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_vt->d_rFol);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_vt->d_rFol);
#endif
#ifdef  OutputPhysiologyDaily_Item__rFru
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_vt->d_rBud);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_vt->d_rBud);
#endif
#ifdef  OutputPhysiologyDaily_Item__rFrt
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_vt->d_rFrt);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_vt->d_rFrt);
#endif
#ifdef  OutputPhysiologyDaily_Item__rLst
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_vt->d_rSap);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_vt->d_rSap);
#endif
#ifdef  OutputPhysiologyDaily_Item__rRes
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_vt->d_rRes);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_vt->d_rRes);
#endif
#ifdef  OutputPhysiologyDaily_Item__rTra
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_vt->d_rTra);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_vt->d_rTra);
#endif
#ifdef  OutputPhysiologyDaily_Item__rGro
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_vt->d_rGro);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_vt->d_rGro);
#endif
#ifdef  OutputPhysiologyDaily_Item__cUpt
    double const nd_cUpt_vt( cbm::sum(_vt->d_carbonuptake_fl, _vt->nb_foliagelayers()));
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(nd_cUpt_vt);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(nd_cUpt_vt);
#endif
#ifdef  OutputPhysiologyDaily_Item__fFac
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_vt->f_fac * _vt->f_area);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_vt->f_fac);
#endif
#ifdef  OutputPhysiologyDaily_Item__mFol
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_vt->mFol);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_vt->mFol);
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_vt->dw_dfol);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_vt->dw_dfol);
#endif
#ifdef  OutputPhysiologyDaily_Item__mFru
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_vt->mBud);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_vt->mBud);
#endif
#ifdef  OutputPhysiologyDaily_Item__mStemFru
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_vt->aboveground_biomass()-_vt->mFol);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_vt->aboveground_biomass()-_vt->mFol);
#endif
#ifdef  OutputPhysiologyDaily_Item__mFrt
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_vt->mFrt);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_vt->mFrt);
#endif
#ifdef  OutputPhysiologyDaily_Item__dw_lst
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_vt->living_structural_matter());
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_vt->living_structural_matter());
#endif
#ifdef  OutputPhysiologyDaily_Item__dw_dst
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_vt->dead_structural_matter());
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_vt->dead_structural_matter());
#endif
#ifdef  OutputPhysiologyDaily_Item__mBelow
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_vt->belowground_biomass());
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_vt->belowground_biomass());
#endif
#ifdef  OutputPhysiologyDaily_Item__mAbove
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM( _vt->aboveground_biomass());
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_vt->aboveground_biomass());
#endif
#ifdef  OutputPhysiologyDaily_Item__lai
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(fs.lai);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(fs.lai);
#endif
#ifdef  OutputPhysiologyDaily_Item__sla
//     double const  sla_vt( this->vegstruct->sla_vtfl[_vt].mean());
//     LDNDC_OUTPUT_SET_COLUMN_AVG_SUM( sla_vt);
//     LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( sla_vt);
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM( fs.sla);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( fs.sla);
#endif
#ifdef  OutputPhysiologyDaily_Item__nRet
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_vt->d_n_retention);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_vt->d_n_retention);
#endif
#ifdef  OutputPhysiologyDaily_Item__ncFol
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_vt->ncFol * _vt->f_area);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_vt->ncFol);
#endif
#ifdef  OutputPhysiologyDaily_Item__ncFru
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_vt->ncBud * _vt->f_area);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_vt->ncBud);
#endif
#ifdef  OutputPhysiologyDaily_Item__ncFrt
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_vt->ncFrt * _vt->f_area);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_vt->ncFrt);
#endif
#ifdef  OutputPhysiologyDaily_Item__ncLst
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_vt->nc_lst() * _vt->f_area);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_vt->nc_lst());
#endif
#ifdef  OutputPhysiologyDaily_Item__ncCor
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_vt->nc_dst() * _vt->f_area);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_vt->nc_dst());
#endif
#ifdef  OutputPhysiologyDaily_Item__ntot
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_vt->total_nitrogen());
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_vt->total_nitrogen());
#endif
#ifdef  OutputPhysiologyDaily_Item__sFol
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_vt->d_sFol);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_vt->d_sFol);
#endif
#ifdef  OutputPhysiologyDaily_Item__sFru
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_vt->d_sBud);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_vt->d_sBud);
#endif
#ifdef  OutputPhysiologyDaily_Item__sFrt
    double const d_sFrt_sl_sum( cbm::sum( _vt->d_sFrt_sl, soillayers->soil_layer_cnt()));
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(d_sFrt_sl_sum);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(d_sFrt_sl_sum);
#endif
#ifdef  OutputPhysiologyDaily_Item__sWoodBelow
    double const d_sWoodBelow_sl_sum( cbm::sum( _vt->d_sWoodBelow_sl, soillayers->soil_layer_cnt()));
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(d_sWoodBelow_sl_sum);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(d_sWoodBelow_sl_sum);
#endif
#ifdef  OutputPhysiologyDaily_Item__sWoodAbove
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_vt->d_sWoodAbove);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_vt->d_sWoodAbove);
#endif
#ifdef  OutputPhysiologyDaily_Item__exsuLoss
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_vt->d_exsuLoss);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_vt->d_exsuLoss);
#endif
#ifdef  OutputPhysiologyDaily_Item__nLitFol
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_vt->d_nLitFol);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_vt->d_nLitFol);
#endif
#ifdef  OutputPhysiologyDaily_Item__nLitFru
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_vt->d_nLitBud);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_vt->d_nLitBud);
#endif
#ifdef  OutputPhysiologyDaily_Item__nLitFrt
    double const d_nLitFrt_sl_sum( cbm::sum( _vt->d_nLitFrt_sl, soillayers->soil_layer_cnt()));
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(d_nLitFrt_sl_sum);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(d_nLitFrt_sl_sum);
#endif
#ifdef  OutputPhysiologyDaily_Item__nLitWoodBelow
    double const d_nLitWoodBelow_sl_sum( cbm::sum( _vt->d_nLitWoodBelow_sl, soillayers->soil_layer_cnt()));
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(d_nLitWoodBelow_sl_sum);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(d_nLitWoodBelow_sl_sum);
#endif
#ifdef  OutputPhysiologyDaily_Item__nLitWoodAbove
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_vt->d_nLitWoodAbove);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_vt->d_nLitWoodAbove);
#endif
#ifdef  OutputPhysiologyDaily_Item__droughtRel
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_vt->f_h2o * _vt->f_area);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_vt->f_h2o);
#endif
#ifdef  OutputPhysiologyDaily_Item__conductRel
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(fs.conductRel);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(fs.conductRel);
#endif
#ifdef  OutputPhysiologyDaily_Item__vcact25
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(fs.vcAct25);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(fs.vcAct25);
#endif
#ifdef  OutputPhysiologyDaily_Item__isoAct
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(fs.isoAct);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(fs.isoAct);
#endif
#ifdef  OutputPhysiologyDaily_Item__monoAct
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(fs.monoAct);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(fs.monoAct);
#endif
#ifdef  OutputPhysiologyDaily_Item__isoEmi
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(fs.iso_emi_sum);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(fs.iso_emi_sum);
#endif
#ifdef  OutputPhysiologyDaily_Item__monoEmi
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(fs.mono_emi_sum);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(fs.mono_emi_sum);
#endif
#ifdef  OutputPhysiologyDaily_Item__monosEmi
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(fs.mono_s_emi_sum);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(fs.mono_s_emi_sum);
#endif
#ifdef  OutputPhysiologyDaily_Item__ovocEmi
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(fs.ovoc_emi_sum);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(fs.ovoc_emi_sum);
#endif    
#ifdef  OutputPhysiologyDaily_Item__throu_nh4
    LDNDC_OUTPUT_SET_COLUMN_AVG_SET(ldndc::invalid_t< ldndc_flt64_t >::value);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->acc.nh4_throughfall);
#endif
#ifdef  OutputPhysiologyDaily_Item__throu_no3
    LDNDC_OUTPUT_SET_COLUMN_AVG_SET(ldndc::invalid_t< ldndc_flt64_t >::value);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->acc.no3_throughfall);
#endif

    return  LDNDC_ERR_OK;
}
} 

namespace ldndc {
void
LMOD_OUTPUT_MODULE_NAME::foliage_sums_(
        MoBiLE_Plant *  _vt, foliage_sums_t *  _fs)
{
    _fs->vcAct25        = 0.0;
    _fs->isoAct         = 0.0;
    _fs->monoAct        = 0.0;
    _fs->conductRel     = 0.0;
    _fs->sla            = 0.0;
    _fs->iso_emi_sum    = 0.0;
    _fs->mono_emi_sum   = 0.0;
    _fs->mono_s_emi_sum = 0.0;
    _fs->ovoc_emi_sum   = 0.0;

    size_t const  nb_foliagelayers = _vt->nb_foliagelayers();
    _fs->lai = cbm::sum( _vt->lai_fl, nb_foliagelayers);
    if ( cbm::flt_greater_zero( _fs->lai))
    {
        for ( size_t  fl = 0;  fl < nb_foliagelayers;  ++fl)
        {
            _fs->conductRel     += _vt->d_relativeconductance_fl[fl] * _vt->fFol_fl[fl];
            _fs->vcAct25        += _vt->vcAct25_fl[fl] * _vt->fFol_fl[fl];
            _fs->isoAct         += _vt->isoAct_fl[fl]  * _vt->fFol_fl[fl];
            _fs->monoAct        += _vt->monoAct_fl[fl] * _vt->fFol_fl[fl];
            _fs->iso_emi_sum    += _vt->d_isoprene_emission_fl[fl];
            _fs->mono_emi_sum   += _vt->d_monoterpene_emission_fl[fl];
            _fs->mono_s_emi_sum += _vt->d_monoterpene_s_emission_fl[fl];
            _fs->ovoc_emi_sum   += _vt->d_ovoc_emission_fl[fl];
        }
    }

    if ( cbm::flt_greater_zero( _vt->mFol))
    {
        for ( size_t  fl = 0;  fl < nb_foliagelayers;  ++fl)
        {
            _fs->sla += _vt->sla_fl[fl] * _vt->m_fol_fl(fl);
        }

        _fs->sla /= _vt->mFol;
    }
}


#undef  LMOD_OUTPUT_MODULE_BASE
#undef  LMOD_OUTPUT_MODULE_NAME

#undef  OutputPhysiologyDaily_Rank
#undef  OutputPhysiologyDaily_Datasize

} /*namespace ldndc*/

