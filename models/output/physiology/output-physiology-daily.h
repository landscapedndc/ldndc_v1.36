/*!
 * @brief
 *    dumping modified physiology related items
 *
 * @author
 *    steffen klatt,
 *    edwin haas,
 */

#ifndef  LM_OUTPUT_PHYSIOLOGY_DAILY_H_
#define  LM_OUTPUT_PHYSIOLOGY_DAILY_H_

#include  "mbe_legacyoutputmodel.h"
#include  "state/mbe_state.h"

#include  <vector>

// off #ifdef  LDNDC_OUTPUT_HAVE_DUMMY_LINES
// off #  undef  LDNDC_OUTPUT_HAVE_DUMMY_LINES
// off #endif

#define  LMOD_OUTPUT_MODULE_BASE  MBE_LegacyOutputModel
#define  LMOD_OUTPUT_MODULE_NAME  OutputPhysiologyDaily
#define  LMOD_OUTPUT_MODULE_ID    "output:physiology:daily"
#define  LMOD_OUTPUT_MODULE_DESC  "Physiology Daily Output"
namespace ldndc {
class  substate_physiology_t;
class  substate_watercycle_t;
class  LDNDC_API  LMOD_OUTPUT_MODULE_NAME  :  public  LMOD_OUTPUT_MODULE_BASE
{
    LMOD_EXPORT_MODULE_INFO(LMOD_OUTPUT_MODULE_NAME,LMOD_OUTPUT_MODULE_ID,LMOD_OUTPUT_MODULE_DESC);
    public:
        LMOD_OUTPUT_MODULE_NAME(
                MoBiLE_State *,
                cbm::io_kcomm_t *,
                timemode_e  _timemode);

        ~LMOD_OUTPUT_MODULE_NAME();


        lerr_t  configure( ldndc::config_file_t const *);

        lerr_t  initialize();

        lerr_t  solve();

        lerr_t  finalize();

        lerr_t  wake() { return  LDNDC_ERR_OK; }
        lerr_t  sleep() { return  LDNDC_ERR_OK; }

    private:
        lerr_t  sink_handle_acquire_(
                sink_handle_t & /*sink handle*/, int /*index*/);

    private:
        cbm::io_kcomm_t *  io_kcomm;

        substate_physiology_t const *  phys;
        substate_watercycle_t const *  water;

        MoBiLE_PlantVegetation *  m_veg;
        input_class_soillayers_t const *  soillayers;

    private:
        lerr_t  acquire_sinks_if_needed_();
        std::vector< ldndc::sink_handle_t >  snks_;
        ldndc::sink_handle_t  snk_sum_;

    private:
        typedef  struct
        {
            ldndc_flt64_t  vcAct25;
            ldndc_flt64_t  isoAct;
            ldndc_flt64_t  monoAct;
            ldndc_flt64_t  conductRel;
            ldndc_flt64_t  sla;
            ldndc_flt64_t  iso_emi_sum;
            ldndc_flt64_t  mono_emi_sum;            
            ldndc_flt64_t  mono_s_emi_sum;
            ldndc_flt64_t  ovoc_emi_sum;
          
            ldndc_flt64_t  lai;
        }  foliage_sums_t;

        void  foliage_sums_(
                MoBiLE_Plant *, foliage_sums_t * /*buffer*/);
        typedef  struct
        {
            ldndc_flt64_t  sum;
            ldndc_flt64_t  sum_inv;
        }  myc_sum_t;
        void  myc_sum_( myc_sum_t *);

        lerr_t  m_writerecord( MoBiLE_Plant *,
                ldndc_flt64_t * /*buffer*/, ldndc_flt64_t * /*average buffer*/);

        struct  output_physiology_daily_acc_t
        {
            double  nh3_uptake;
            double  nh4_uptake;
            double  no3_uptake;
            double  don_uptake;

            double  nh4_throughfall;
            double  no3_throughfall;
        };
        output_physiology_daily_acc_t  acc;
        void  pop_accumulated_outputs();
        void  push_accumulated_outputs();
};
} /*namespace ldndc*/


#undef  LMOD_OUTPUT_MODULE_BASE
#undef  LMOD_OUTPUT_MODULE_NAME
#undef  LMOD_OUTPUT_MODULE_ID
#undef  LMOD_OUTPUT_MODULE_DESC


#endif  /*  !LM_OUTPUT_PHYSIOLOGY_DAILY_H_  */

