/*!
 * @brief
 *    dumping modified physiology related items (layers)
 *
 * @author
 *    steffen klatt,
 *    edwin haas
 */

#ifndef  LM_OUTPUT_PHYSIOLOGY_LAYER_DAILY_H_
#define  LM_OUTPUT_PHYSIOLOGY_LAYER_DAILY_H_

#include  "mbe_legacyoutputmodel.h"
#include  "state/mbe_state.h"

#define  LMOD_OUTPUT_MODULE_BASE  MBE_LegacyOutputModel
#define  LMOD_OUTPUT_MODULE_NAME  OutputPhysiologyLayerDaily
#define  LMOD_OUTPUT_MODULE_ID    "output:physiology-layer:daily"
#define  LMOD_OUTPUT_MODULE_DESC  "Physiology Daily Output (Layers)"
namespace ldndc {
class  substate_physiology_t;
class  substate_soilchemistry_t;
class  LDNDC_API  LMOD_OUTPUT_MODULE_NAME  :  public  LMOD_OUTPUT_MODULE_BASE
{
    LMOD_EXPORT_MODULE_INFO(LMOD_OUTPUT_MODULE_NAME,LMOD_OUTPUT_MODULE_ID,LMOD_OUTPUT_MODULE_DESC);
    public:
        LMOD_OUTPUT_MODULE_NAME(
                MoBiLE_State *,
                cbm::io_kcomm_t *,
                timemode_e  _timemode);

        ~LMOD_OUTPUT_MODULE_NAME();


        lerr_t  configure( ldndc::config_file_t const *);

        lerr_t  initialize();

        lerr_t  solve();

        lerr_t  finalize();

        lerr_t  wake() { return  LDNDC_ERR_OK; }
        lerr_t  sleep() { return  LDNDC_ERR_OK; }

    private:
        cbm::io_kcomm_t *  io_kcomm;

        input_class_soillayers_t const *  soillayers_;
        input_class_setup_t const *  setup_;
        substate_physiology_t const *  ph;
        substate_soilchemistry_t const *  sc;

        MoBiLE_PlantVegetation *  m_veg;

    private:
        lerr_t  sink_handle_acquire_();
        ldndc::sink_handle_t  m_sink;
        struct  conf_t
        {
            bool  foliage_all:1;
            bool  soil_all:1;
        };
        struct conf_t  rc;

        struct  output_physiology_layer_daily_acc_t
        {
            double * nh4_uptake_sl;
            double * no3_uptake_sl;
        };
        output_physiology_layer_daily_acc_t  acc;

        lerr_t  m_writerecord( ldndc_flt64_t * /*buffer*/);
};
} /*namespace ldndc*/


#undef  LMOD_OUTPUT_MODULE_BASE
#undef  LMOD_OUTPUT_MODULE_NAME
#undef  LMOD_OUTPUT_MODULE_ID
#undef  LMOD_OUTPUT_MODULE_DESC


#endif  /*  !LM_OUTPUT_PHYSIOLOGY_LAYER_DAILY_H_  */

