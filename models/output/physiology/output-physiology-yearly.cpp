/*!
 * @file
 * @author
 *  - Edwin Haas
 *  - Ruediger Grote
 *  - David Kraus
 */

#include  "output/physiology/output-physiology-yearly.h"
#include  "constants/lconstants-math.h"
#include  "constants/lconstants-conv.h"
#include  "utils/lutils.h"

#define  LMOD_OUTPUT_MODULE_BASE  MBE_LegacyOutputModel
#define  LMOD_OUTPUT_MODULE_NAME  OutputPhysiologyYearly
#define  LMOD_OUTPUT_MODULE_TIMEMODE  TMODE_POST_YEARLY

LMOD_MODULE_INFO(LMOD_OUTPUT_MODULE_NAME,LMOD_OUTPUT_MODULE_TIMEMODE,LMOD_FLAG_USER|LMOD_FLAG_OUTPUT);
namespace ldndc {

#define  OutputPhysiologyYearly_Item__species  "species"

/*!
 * @page physiologyoutput
 * @section physiology_output_yearly Physiology output (yearly)
 *
 * <b>xml-based module selection in project's setup:</b>
 * \n\code{.xml}< module id="output:physiology:yearly" />\endcode\n
 *
 * entity name | decription | unit
 * ----------- | ---------- | ----
 * aN\_n2\_fix | Annual nitrogen fixation | [kgN m-2]
 * DW\_fol | Foliage biomass | [kgDW m-2]
 * DW\_frt | Fine roots biomass | [kgDW m-2]
 * DW\_lst | Living structural biomass | [kgDW m-2]
 * DW\_dst | Dead structural  biomass | [kgDW m-2]
 * DW\_fru | Fruit (grain, storage) biomass | [kgDW m-2]
 * DW\_above | Aboveground biomass | [kgDW m-2]
 * DW\_below | Belowground biomass | [kgDW m-2]
 * NC\_fol | Nitrogen content in the foliage | [kgN kgDW-1]
 * NC\_frt | Nitrogen content in the fine roots | [kgN kgDW-1]
 * NC\_fru | Nitrogen content in the fruit | [kgN kgDW-1]
 * NC\_lst | Nitrogen content in  living structural matter (e.g., young wood)  | [kgN kgDW-1]
 * NC\_dst | Nitrogen content in  dead structural matter (e.g., old wood)  | [kgN kgDW-1]
 * N\_total | Total plant nitrogen | [kgN m-2]
 */

ldndc_string_t const  OutputPhysiologyYearly_Ids[] =
{
#ifdef  OutputPhysiologyYearly_Item__species
    OutputPhysiologyYearly_Item__species,
#endif

    "aN_n2_fix",
    "DW_fol",
    "DW_frt",
    "DW_lst",
    "DW_dst",
    "DW_fru",
    "DW_above",
    "DW_below",
    "NC_fol",
    "NC_frt",
    "NC_fru",
    "NC_lst",
    "NC_dst",
    "N_total"
};
ldndc_string_t const  OutputPhysiologyYearly_Header[] =
{
#ifdef  OutputPhysiologyYearly_Item__species
    OutputPhysiologyYearly_Item__species,
#endif

    "aN_n2_fix[kgNm-2]",
    "DW_fol[kgDWm-2]",
    "DW_frt[kgDWm-2]",
    "DW_lst[kgDWm-2]",
    "DW_dst[kgDWm-2]",
    "DW_fru[kgDWm-2]",
    "DW_above[kgDWm-2]",
    "DW_below[kgDWm-2]",
    "NC_fol[gNgDW-1]",
    "NC_frt[gNgDW-1]",
    "NC_fru[gNgDW-1]",
    "NC_lst[gNgDW-1]",
    "NC_dst[gNgDW-1]",
    "N_total[kgNm-2]"
};
#define  OutputPhysiologyYearly_Datasize  (sizeof( OutputPhysiologyYearly_Header) / sizeof( OutputPhysiologyYearly_Header[0]))
ldndc_output_size_t const  OutputPhysiologyYearly_Sizes[] =
{
#ifdef  OutputPhysiologyYearly_Item__species
    1,
    OutputPhysiologyYearly_Datasize - 1,
#else
    OutputPhysiologyYearly_Datasize,
#endif
    OutputPhysiologyYearly_Datasize /*total size*/
};
ldndc_output_size_t const *  OutputPhysiologyYearly_EntitySizes = NULL;
#define  OutputPhysiologyYearly_Rank  ((ldndc_output_rank_t)(sizeof( OutputPhysiologyYearly_Sizes) / sizeof( OutputPhysiologyYearly_Sizes[0])) - 1)
atomic_datatype_t const  OutputPhysiologyYearly_Types[] =
{
#ifdef  OutputPhysiologyYearly_Item__species
    LDNDC_STRING,
#endif
    LDNDC_FLOAT64
};


LMOD_OUTPUT_MODULE_NAME::LMOD_OUTPUT_MODULE_NAME(
        MoBiLE_State *  _state,
        cbm::io_kcomm_t *  _io_kcomm,
        timemode_e  _timemode)
        : LMOD_OUTPUT_MODULE_BASE( _state, _io_kcomm, _timemode),
          io_kcomm( _io_kcomm),
          phys( _state->get_substate< substate_physiology_t >()),
          m_veg( &_state->vegetation)
{
}


LMOD_OUTPUT_MODULE_NAME::~LMOD_OUTPUT_MODULE_NAME()
{
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::configure(
                ldndc::config_file_t const *  _cf)
{
    lerr_t  rc_setflags = set_metaflags( _cf, RM_DEFAULT_YEARLY);
    if ( rc_setflags)
    { return  LDNDC_ERR_FAIL; }

    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::initialize()
{
    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::sink_handle_acquire_(
        sink_handle_t &  _snk,
        int  _index)
{
    int  sink_index = _index;
    if ( !separate_concurrent())
        { sink_index = 0; }
    _snk = io_kcomm->sink_handle_acquire( "physiologyyearly", sink_index);
    if ( _snk.status() == LDNDC_ERR_OK)
    {
        lerr_t  rc_layout = LDNDC_OUTPUT_DEFINE_SINK_LAYOUT_DEFAULTS(
            _snk,OutputPhysiologyYearly);
        RETURN_IF_NOT_OK(rc_layout);
    }
    else
    {
        KLOGERROR( "sink status bad  [sink=","physiologyyearly","]");
        return  _snk.status();
    }

    return  LDNDC_ERR_OK;
}
lerr_t
LMOD_OUTPUT_MODULE_NAME::acquire_sinks_if_needed_()
{
    size_t const  c = m_veg->size();
    if ( c == 0)
    {
        return  LDNDC_ERR_OK;
    }
    else if ( c > 1)
    {
        if ( !m_sinksum.is_acquired())
        {
            lerr_t  rc_snk_avg = sink_handle_acquire_( m_sinksum, 0);
            RETURN_IF_NOT_OK(rc_snk_avg);
        }
    }

    size_t const  C = m_veg->slot_cnt()-1;
    if ( cbm::is_invalid( C) || ( m_sinks.size() > C))
    {
        return  LDNDC_ERR_OK;
    }

    /* recruit more sinks */
    for ( size_t  s = m_sinks.size();  s <= C;  ++s)
    {
        ldndc::sink_handle_t  snk;
        lerr_t  rc_snk = sink_handle_acquire_( snk, s+1);
        RETURN_IF_NOT_OK(rc_snk);

        m_sinks.push_back( snk);
    }

    return  LDNDC_ERR_OK;
}

lerr_t
LMOD_OUTPUT_MODULE_NAME::solve()
{
    lerr_t  rc_sinks = acquire_sinks_if_needed_();
    RETURN_IF_NOT_OK( rc_sinks);
    if ( m_sinks.size() == 0)
    {
        return  LDNDC_ERR_OK;
    }

    size_t const  c = m_veg->size();

    ldndc_flt64_t  data_flt64_a[OutputPhysiologyYearly_Datasize];
    cbm::mem_set( data_flt64_a, OutputPhysiologyYearly_Datasize, 0.0);


    for ( PlantIterator  vt = m_veg->begin(); vt != m_veg->end(); ++vt)
    {
#ifdef  OutputPhysiologyYearly_Item__species
        ldndc_string_t  data_string[1];
        cbm::as_strcpy( &data_string[0], (*vt)->cname());
#endif
        ldndc_flt64_t  data_flt64_0[OutputPhysiologyYearly_Datasize];
        lerr_t  rc_dump = m_writerecord( *vt, data_flt64_0, data_flt64_a);
        RETURN_IF_NOT_OK(rc_dump);

#ifdef  OutputPhysiologyYearly_Item__species
        void *  data[] = { data_string, data_flt64_0};
#else
        void *  data[] = { data_flt64_0};
#endif
        lerr_t  rc_write = write_fixed_record( &m_sinks[(*vt)->slot], data);
        if ( rc_write)
        {
            return  LDNDC_ERR_FAIL;
        }
    }

    if ( m_sinksum.is_acquired())
    {
        if ( c > 0)
        {
#ifdef  OutputPhysiologyYearly_Item__species
            ldndc_string_t  data_string[1];
            cbm::as_strcpy( &data_string[0], COLUMN_NAME_ALLSPECIES);
            void *  data_a[] = { data_string, data_flt64_a};
#else
            void *  data_a[] = { data_flt64_a};
#endif
            lerr_t  rc_write = write_fixed_record( &m_sinksum, data_a);
            if ( rc_write)
            { return  LDNDC_ERR_FAIL; }
        }
    }
    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::finalize()
{
    for ( size_t  s = 0;  s < m_sinks.size();  ++s)
    {
        if ( m_sinks[s].is_acquired())
        {
            io_kcomm->sink_handle_release( &m_sinks[s]);
        }
    }
    m_sinks.clear();

    if ( m_sinksum.is_acquired())
    {
        io_kcomm->sink_handle_release( &m_sinksum);
    }
    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::m_writerecord( MoBiLE_Plant *  _s,
        ldndc_flt64_t *  _buf, ldndc_flt64_t *  _buf_avg)
{
    LDNDC_OUTPUT_SET_COLUMN_INDEX(0);

    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_s->a_fix_n);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_s->a_fix_n);
    
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_s->mFol);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_s->mFol);

    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_s->mFrt);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_s->mFrt);

    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_s->living_structural_matter());
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_s->living_structural_matter());

    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_s->dead_structural_matter());
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_s->dead_structural_matter());

    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_s->mBud);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_s->mBud);

    if ( (*_s)->TUBER())
    {
        LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_s->foliage_matter() + _s->aboveground_wood());
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_s->foliage_matter() + _s->aboveground_wood());

        LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_s->mBud + _s->mFrt + _s->belowground_wood());
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_s->mBud + _s->mFrt + _s->belowground_wood());
    }
    else
    {
        LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_s->mBud + _s->foliage_matter() + _s->aboveground_wood());
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_s->mBud + _s->foliage_matter() + _s->aboveground_wood());

        LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_s->mFrt + _s->belowground_wood());
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_s->mFrt + _s->belowground_wood());
    }

    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_s->ncFol * _s->f_area);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_s->ncFol);

    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_s->ncFrt * _s->f_area);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_s->ncFrt);

    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_s->ncBud * _s->f_area);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_s->ncBud);

    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_s->ncSap * _s->f_area);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_s->ncSap);

    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_s->ncCor * _s->f_area);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_s->ncCor);

    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_s->total_nitrogen());
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_s->total_nitrogen());

    return  LDNDC_ERR_OK;
}

#undef  LMOD_OUTPUT_MODULE_BASE
#undef  LMOD_OUTPUT_MODULE_NAME

#undef  OutputPhysiologyYearly_Rank
#undef  OutputPhysiologyYearly_Datasize

} /*namespace ldndc*/

