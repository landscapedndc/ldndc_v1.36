/*!
 * @file
 * @author
 * - Edwin Haas,
 * - Ruediger Grote,
 * - Felix Wiß
 * @date
 *  may 12, 2015)
 */

#include  "output/physiology/output-physiology-subdaily.h"

#include  "utils/cbm_utils.h"
#include  <constants/cbm_const.h>
#include  <math/cbm_math.h>

#define  LMOD_OUTPUT_MODULE_BASE  MBE_LegacyOutputModel
#define  LMOD_OUTPUT_MODULE_NAME  OutputPhysiologySubdaily
#define  LMOD_OUTPUT_MODULE_TIMEMODE  TMODE_SUBDAILY

LMOD_MODULE_INFO(LMOD_OUTPUT_MODULE_NAME,LMOD_OUTPUT_MODULE_TIMEMODE,LMOD_FLAG_USER|LMOD_FLAG_OUTPUT);
namespace ldndc {

#define  OutputPhysiologySubdaily_Item__species
#define  OutputPhysiologySubdaily_Item__dEmerg
#define  OutputPhysiologySubdaily_Item__tCum
#define  OutputPhysiologySubdaily_Item__dvsflush
#define  OutputPhysiologySubdaily_Item__dvsmort
#define  OutputPhysiologySubdaily_Item__uptDON
#define  OutputPhysiologySubdaily_Item__uptNH4
#define  OutputPhysiologySubdaily_Item__uptNO3
#define  OutputPhysiologySubdaily_Item__uptNH3
#define  OutputPhysiologySubdaily_Item__uptNOx
#define  OutputPhysiologySubdaily_Item__fixN2
#define  OutputPhysiologySubdaily_Item__dcFol
#define  OutputPhysiologySubdaily_Item__dcFru
#define  OutputPhysiologySubdaily_Item__dcFrt
#define  OutputPhysiologySubdaily_Item__dcLst
#define  OutputPhysiologySubdaily_Item__dcFac
#define  OutputPhysiologySubdaily_Item__rFol
#define  OutputPhysiologySubdaily_Item__rFru
#define  OutputPhysiologySubdaily_Item__rFrt
#define  OutputPhysiologySubdaily_Item__rLst
#define  OutputPhysiologySubdaily_Item__rRes
#define  OutputPhysiologySubdaily_Item__rTra
#define  OutputPhysiologySubdaily_Item__rGro
#define  OutputPhysiologySubdaily_Item__cUpt
#define  OutputPhysiologySubdaily_Item__fFac
#define  OutputPhysiologySubdaily_Item__mFol
#define  OutputPhysiologySubdaily_Item__mFru
#define  OutputPhysiologySubdaily_Item__mFrt
#define  OutputPhysiologySubdaily_Item__mLst
#define  OutputPhysiologySubdaily_Item__mDst
#define  OutputPhysiologySubdaily_Item__mBelow
#define  OutputPhysiologySubdaily_Item__mAbove
#define  OutputPhysiologySubdaily_Item__lai
#define  OutputPhysiologySubdaily_Item__sla
#define  OutputPhysiologySubdaily_Item__nRet
#define  OutputPhysiologySubdaily_Item__ncFol
#define  OutputPhysiologySubdaily_Item__ncFru
#define  OutputPhysiologySubdaily_Item__ncFrt
#define  OutputPhysiologySubdaily_Item__ncLst
#define  OutputPhysiologySubdaily_Item__ncDst
#define  OutputPhysiologySubdaily_Item__ntot
#define  OutputPhysiologySubdaily_Item__sFol
#define  OutputPhysiologySubdaily_Item__sFru
#define  OutputPhysiologySubdaily_Item__sFrt
#define  OutputPhysiologySubdaily_Item__sWood
#define  OutputPhysiologySubdaily_Item__exsuLoss
#define  OutputPhysiologySubdaily_Item__nLitFol
#define  OutputPhysiologySubdaily_Item__nLitFru
#define  OutputPhysiologySubdaily_Item__nLitFrt
#define  OutputPhysiologySubdaily_Item__nLitWood
#define  OutputPhysiologySubdaily_Item__droughtRel
#define  OutputPhysiologySubdaily_Item__conductRel
#define  OutputPhysiologySubdaily_Item__vcact25
#define  OutputPhysiologySubdaily_Item__isoAct
#define  OutputPhysiologySubdaily_Item__monoAct
#define  OutputPhysiologySubdaily_Item__isoEmi
#define  OutputPhysiologySubdaily_Item__monoEmi
#define  OutputPhysiologySubdaily_Item__monosEmi
#define  OutputPhysiologySubdaily_Item__ovocEmi
#define  OutputPhysiologySubdaily_Item__psi

/*!
 * @page physiologyoutput
 * @section physiologyoutputsubdaily Physiology output (subdaily)
 *
 * <b>xml-based module selection in project's setup:</b>
 * \n\code{.xml}< module id="output:physiology:subdaily" />\endcode\n
 *
 * entity name | decription | unit
 * ----------- | ---------- | ----
 * day\_emerg     | Day of emergence | [day]
 * temp\_cum    | Growing degree days | [gdd]
 * dvs\_flush  | Relative state of foliage flushing in forest simulations | [-]
 * dvs\_mort  | Relative state of foliage senescence | [-]
 * dN\_don\_upt     | Plant uptake of soil organic nitrogen | [kgN m-2 ts-1]
 * dN\_nh4\_upt | Plant uptake of soil ammonium | [kgN m-2 ts-1]
 * dN\_no3\_upt | Plant uptake of soil nitrate | [kgN m-2 ts-1]
 * dN\_nh3\_upt | Plant uptake of soil ammonia | [kgN m-2 ts-1]
 * dN\_nox\_upt | Uptake of NOx | [kgN m-2 ts-1]
 * dN\_n2\_fix     | Nitrogen fixation by plants | [kgN m-2 ts-1]
 * dC\_fol\_grow | Increase of foliages carbon | [kgC m-2 ts-1]
 * dC\_frt\_grow | Increase of (fine) roots carbon | [kgC m-2 ts-1]
 * dC\_lst\_grow | Increase of living structural tissue carbon | [kgC m-2 ts-1]
 * dC\_fru\_grow | Increase of fruit (buds) carbon | [kgC m-2 ts-1]
 * dC\_fac\_grow | Carbon allocated to pool available substrate | [kgC m-2 ts-1]
 * dC\_fol\_resp | Foliar respiration | [kgC m-2 ts-1]
 * dC\_frt\_resp | Fine roots respiration | [kgC m-2 ts-1]
 * dC\_lst\_resp | Young living structural tissue respiration | [kgC m-2 ts-1]
 * dC\_fru\_resp | Fruit (buds) respiration | [kgC m-2 ts-1]
 * dC\_maintenance\_resp  | Total residual respiration | [kgC m-2 ts-1]
 * dC\_transport\_resp | Total transport and uptake respiration | [kgC m-2 ts-1]
 * dC\_growth\_resp | Total growth respiration | [kgC m-2 ts-1]
 * dC\_co2\_upt | Carbon uptake-Photosynthesis rate | [kgC m-2 ts-1]
 * f\_Fac | Relative state of free available carbon | [-]
 * DW\_fol    | Foliar mass | [kgDW m-2]
 * DW\_frt | Fine roots mass | [kgDW m-2]
 * DW\_lst | Living structural tissue mass | [kgDW m-2]
 * DW\_dst | Dead structural tissue mass | [kgDW m-2]
 * DW\_fru | Fruit (buds) mass | [kgDW m-2]
 * lai | Leaf area index | [-]
 * sla | Leaf area index | [m-2 kg-1]
 * n\_ret | Retention of nitrogen | [kgN m-2]
 * NC\_fol    | Nitrogen content in the foliage | [kgN kgDW-1]
 * NC\_frt | Nitrogen content in the fine roots | [kgN kgDW-1]
 * NC\_lst | Nitrogen content in the living structural tissue | [kgN kgDW-1]
 * NC\_dst | Nitrogen content in the dead structural tissue | [kgN kgDW-1]
 * NC\_fru | Nitrogen content in the fruit | [kgN kgDW-1]
 * dC\_fol\_sen | Foliage senescence | [kgDW m-2 ts-1]
 * dC\_frt\_sen | Fine roots senescence | [kgDW m-2 ts-1]
 * dC\_lst\_sen | Living structural tissue senescence | [kgDW m-2 ts-1]
 * dC\_fru\_sen | Fruit (buds) senescence | [kgDW m-2 ts-1]
 * dC\_exsudates | Root exsudates losses | [kgC m-2 ts-1]
 * N\_lit\_fol | Nitrogen in the foliage-litter | [kgN m-2]
 * N\_lit\_frt | Nitrogen in the fine roots-litter | [kgN m-2]
 * N\_lit\_lst | Nitrogen in the living structural tissue-litter | [kgN m-2]
 * N\_lit\_fru | Nitrogen in the fruit-litter | [kgN m-2]
 * drought\_rel | Species specific drought stress factor | [-]
 * conduct\_rel | Conductance rel | [-]
 * vc\_act\_25 | Activity state of rubisco at 25 degrees celsius | [umol m-2 s-1]
 * iso\_act |  Activity state of isoprene synthase | [umol m-2 s-1]
 * mono\_act  |   Activity state of monoterpene synthase | [umol m-2 s-1]
 * iso\_emis  | Isoprene emission from plants | [umol m-2 s-1]
 * mono\_emis | Monoterpene emission from plants | [umol m-2 s-1]
 * dN\_nh4\_throughf | Ammonium deposition through rainfall | [kgN L-1]
 * dN\_no3\_throughf | Nitrate deposition throug rainfall | [kgN L-1]
 */

ldndc_string_t const  OutputPhysiologySubdaily_Ids[] =
{
#ifdef  OutputPhysiologySubdaily_Item__species
    "species",
#endif
#ifdef  OutputPhysiologySubdaily_Item__dEmerg
    "day_emerg",
#endif
#ifdef  OutputPhysiologySubdaily_Item__tCum
    "temp_cum",
#endif
#ifdef  OutputPhysiologySubdaily_Item__dvsflush
    "dvs_flush",
#endif
#ifdef  OutputPhysiologySubdaily_Item__dvsmort
    "dvs_mort",
#endif
#ifdef  OutputPhysiologySubdaily_Item__uptDON
    "dN_don_upt",
#endif
#ifdef  OutputPhysiologySubdaily_Item__uptNH4
    "dN_nh4_upt",
#endif
#ifdef  OutputPhysiologySubdaily_Item__uptNO3
    "dN_no3_upt",
#endif
#ifdef  OutputPhysiologySubdaily_Item__uptNH3
    "dN_nh3_upt",
#endif
#ifdef  OutputPhysiologySubdaily_Item__uptNOx
    "dN_nox_upt",
#endif
#ifdef  OutputPhysiologySubdaily_Item__fixN2
    "dN_n2_fix",
#endif
#ifdef  OutputPhysiologySubdaily_Item__dcFol
    "dC_fol_grow",
#endif
#ifdef  OutputPhysiologySubdaily_Item__dcFru
    "dC_fru_grow",
#endif
#ifdef  OutputPhysiologySubdaily_Item__dcFrt
    "dC_frt_grow",
#endif
#ifdef  OutputPhysiologySubdaily_Item__dcLst
    "dC_lst_grow",
#endif
#ifdef  OutputPhysiologySubdaily_Item__dcFac
    "dC_fac_grow",
#endif
#ifdef  OutputPhysiologySubdaily_Item__rFol
    "dC_fol_resp",
#endif
#ifdef  OutputPhysiologySubdaily_Item__rFru
    "dC_fru_resp",
#endif
#ifdef  OutputPhysiologySubdaily_Item__rFrt
    "dC_frt_resp",
#endif
#ifdef  OutputPhysiologySubdaily_Item__rLst
    "dC_lst_resp",
#endif
#ifdef  OutputPhysiologySubdaily_Item__rRes
    "dC_maintenance_resp",
#endif
#ifdef  OutputPhysiologySubdaily_Item__rTra
    "dC_transport_resp",
#endif
#ifdef  OutputPhysiologySubdaily_Item__rGro
    "dC_growth_resp",
#endif
#ifdef  OutputPhysiologySubdaily_Item__cUpt
    "dC_co2_upt",
#endif
#ifdef  OutputPhysiologySubdaily_Item__fFac
    "f_fac",
#endif
#ifdef  OutputPhysiologySubdaily_Item__mFol
    "DW_fol",
#endif
#ifdef  OutputPhysiologySubdaily_Item__mFru
    "DW_fru",
#endif
#ifdef  OutputPhysiologySubdaily_Item__mFrt
    "DW_frt",
#endif
#ifdef  OutputPhysiologySubdaily_Item__mLst
    "DW_lst",
#endif
#ifdef  OutputPhysiologySubdaily_Item__mDst
    "DW_dst",
#endif
#ifdef  OutputPhysiologySubdaily_Item__mBelow
    "DW_below",
#endif
#ifdef  OutputPhysiologySubdaily_Item__mAbove
    "DW_above",
#endif
#ifdef  OutputPhysiologySubdaily_Item__lai
    "lai",
#endif
#ifdef  OutputPhysiologySubdaily_Item__sla
    "specific_leaf_area",
#endif
#ifdef  OutputPhysiologySubdaily_Item__nRet
    "N_ret",
#endif
#ifdef  OutputPhysiologySubdaily_Item__ncFol
    "NC_fol",
#endif
#ifdef  OutputPhysiologySubdaily_Item__ncFru
    "NC_fru",
#endif
#ifdef  OutputPhysiologySubdaily_Item__ncFrt
    "NC_frt",
#endif
#ifdef  OutputPhysiologySubdaily_Item__ncLst
    "NC_lst",
#endif
#ifdef  OutputPhysiologySubdaily_Item__ncDst
    "NC_dst",
#endif
#ifdef  OutputPhysiologySubdaily_Item__ntot
    "N_total",
#endif
#ifdef  OutputPhysiologySubdaily_Item__sFol
    "dDW_fol_sen",
#endif
#ifdef  OutputPhysiologySubdaily_Item__sFru
    "dDW_fru_sen",
#endif
#ifdef  OutputPhysiologySubdaily_Item__sFrt
    "dDW_frt_sen",
#endif
#ifdef  OutputPhysiologySubdaily_Item__sWood
    "dDW_lst_sen",
#endif
#ifdef  OutputPhysiologySubdaily_Item__exsuLoss
    "dC_exsudates",
#endif
#ifdef  OutputPhysiologySubdaily_Item__nLitFol
    "N_lit_fol",
#endif
#ifdef  OutputPhysiologySubdaily_Item__nLitFru
    "N_lit_fru",
#endif
#ifdef  OutputPhysiologySubdaily_Item__nLitFrt
    "N_lit_frt",
#endif
#ifdef  OutputPhysiologySubdaily_Item__nLitWood
    "N_lit_lst",
#endif
#ifdef  OutputPhysiologySubdaily_Item__droughtRel
    "drought_rel",
#endif
#ifdef  OutputPhysiologySubdaily_Item__conductRel
    "conduct_rel",
#endif
#ifdef  OutputPhysiologySubdaily_Item__vcact25
    "vc_act_25",
#endif
#ifdef  OutputPhysiologySubdaily_Item__isoAct
    "iso_act",
#endif
#ifdef  OutputPhysiologySubdaily_Item__monoAct
    "mono_act",
#endif
#ifdef  OutputPhysiologySubdaily_Item__isoEmi
    "iso_emis",
#endif
#ifdef  OutputPhysiologySubdaily_Item__monoEmi
    "mono_emis",
#endif
#ifdef  OutputPhysiologySubdaily_Item__monosEmi
    "mono_storage_emis",
#endif
#ifdef  OutputPhysiologySubdaily_Item__ovocEmi
    "ovoc_emis",
#endif
#ifdef  OutputPhysiologySubdaily_Item__psi
    "psi_pd",
    "psi_mean",
#endif
};

ldndc_string_t const  OutputPhysiologySubdaily_Header[] =
{
#ifdef  OutputPhysiologySubdaily_Item__species
    "species",
#endif
#ifdef  OutputPhysiologySubdaily_Item__dEmerg
    "day_emerg[day]",
#endif
#ifdef  OutputPhysiologySubdaily_Item__tCum
    "temp_cum[gdd]",
#endif
#ifdef  OutputPhysiologySubdaily_Item__dvsflush
    "dvs_flush[-]",
#endif
#ifdef  OutputPhysiologySubdaily_Item__dvsmort
    "dvs_mort[-]",
#endif
#ifdef  OutputPhysiologySubdaily_Item__uptDON
    "dN_don_upt[kgNm-2]",
#endif
#ifdef  OutputPhysiologySubdaily_Item__uptNH4
    "dN_nh4_upt[kgNm-2]",
#endif
#ifdef  OutputPhysiologySubdaily_Item__uptNO3
    "dN_no3_upt[kgNm-2]",
#endif
#ifdef  OutputPhysiologySubdaily_Item__uptNH3
    "dN_nh3_upt[kgNm-2]",
#endif
#ifdef  OutputPhysiologySubdaily_Item__uptNOx
    "dN_nox_upt[kgNm-2]",
#endif
#ifdef  OutputPhysiologySubdaily_Item__fixN2
    "dN_n2_fix[kgNm-2]",
#endif
#ifdef  OutputPhysiologySubdaily_Item__dcFol
    "dC_fol_grow[kgCm-2]",
#endif
#ifdef  OutputPhysiologySubdaily_Item__dcFru
    "dC_fru_grow[kgCm-2]",
#endif
#ifdef  OutputPhysiologySubdaily_Item__dcFrt
    "dC_frt_grow[kgCm-2]",
#endif
#ifdef  OutputPhysiologySubdaily_Item__dcLst
    "dC_lst_grow[kgCm-2]",
#endif
#ifdef  OutputPhysiologySubdaily_Item__dcFac
    "dC_fac_grow[kgCm-2]",
#endif
#ifdef  OutputPhysiologySubdaily_Item__rFol
    "dC_fol_resp[kgCm-2]",
#endif
#ifdef  OutputPhysiologySubdaily_Item__rFru
    "dC_fru_resp[kgCm-2]",
#endif
#ifdef  OutputPhysiologySubdaily_Item__rFrt
    "dC_frt_resp[kgCm-2]",
#endif
#ifdef  OutputPhysiologySubdaily_Item__rLst
    "dC_lst_resp[kgCm-2]",
#endif
#ifdef  OutputPhysiologySubdaily_Item__rRes
    "dC_maintenance_resp[kgCm-2]",
#endif
#ifdef  OutputPhysiologySubdaily_Item__rTra
    "dC_transport_resp[kgCm-2]",
#endif
#ifdef  OutputPhysiologySubdaily_Item__rGro
    "dC_growth_resp[kgCm-2]",
#endif
#ifdef  OutputPhysiologySubdaily_Item__cUpt
    "dC_co2_upt[kgCm-2]",
#endif
#ifdef  OutputPhysiologySubdaily_Item__fFac
    "f_fac",
#endif
#ifdef  OutputPhysiologySubdaily_Item__mFol
    "DW_fol[kgDWm-2]",
#endif
#ifdef  OutputPhysiologySubdaily_Item__mFru
    "DW_fru[kgDWm-2]",
#endif
#ifdef  OutputPhysiologySubdaily_Item__mFrt
    "DW_frt[kgDWm-2]",
#endif
#ifdef  OutputPhysiologySubdaily_Item__mLst
    "DW_lst[kgDWm-2]",
#endif
#ifdef  OutputPhysiologySubdaily_Item__mDst
    "DW_dst[kgDWm-2]",
#endif
#ifdef  OutputPhysiologySubdaily_Item__mBelow
    "DW_below[kgDWm-2]",
#endif
#ifdef  OutputPhysiologySubdaily_Item__mAbove
    "DW_above[kgDWm-2]",
#endif
#ifdef  OutputPhysiologySubdaily_Item__lai
    "lai",
#endif
#ifdef  OutputPhysiologySubdaily_Item__sla
    "specific_leaf_area[m2kg-1]",
#endif
#ifdef  OutputPhysiologySubdaily_Item__nRet
    "N_ret[kgNm-2]",
#endif
#ifdef  OutputPhysiologySubdaily_Item__ncFol
    "NC_fol[gNgDW-1]",
#endif
#ifdef  OutputPhysiologySubdaily_Item__ncFru
    "NC_fru[gNgDW-1]",
#endif
#ifdef  OutputPhysiologySubdaily_Item__ncFrt
    "NC_frt[gNgDW-1]",
#endif
#ifdef  OutputPhysiologySubdaily_Item__ncLst
    "NC_lst[gNgDW-1]",
#endif
#ifdef  OutputPhysiologySubdaily_Item__ncDst
    "NC_dst[gNgDW-1]",
#endif
#ifdef  OutputPhysiologySubdaily_Item__ntot
    "N_total[kgNm-2]",
#endif
#ifdef  OutputPhysiologySubdaily_Item__sFol
    "dDW_fol_sen[kgDWm-2]",
#endif
#ifdef  OutputPhysiologySubdaily_Item__sFru
    "dDW_fru_sen[kgDWm-2]",
#endif
#ifdef  OutputPhysiologySubdaily_Item__sFrt
    "dDW_frt_sen[kgDWm-2]",
#endif
#ifdef  OutputPhysiologySubdaily_Item__sWood
    "dDW_lst_sen[kgDWm-2]",
#endif
#ifdef  OutputPhysiologySubdaily_Item__exsuLoss
    "dC_exsudates[kgCm-2]",
#endif
#ifdef  OutputPhysiologySubdaily_Item__nLitFol
    "N_lit_fol[kgNm-2]",
#endif
#ifdef  OutputPhysiologySubdaily_Item__nLitFru
    "N_lit_fru[kgNm-2]",
#endif
#ifdef  OutputPhysiologySubdaily_Item__nLitFrt
    "N_lit_frt[kgNm-2]",
#endif
#ifdef  OutputPhysiologySubdaily_Item__nLitWood
    "N_lit_lst[kgNm-2]",
#endif
#ifdef  OutputPhysiologySubdaily_Item__droughtRel
    "drought_rel",
#endif
#ifdef  OutputPhysiologySubdaily_Item__conductRel
    "conduct_rel",
#endif
#ifdef  OutputPhysiologySubdaily_Item__vcact25
    "vc_act_25[umol_m-2_s-1]",
#endif
#ifdef  OutputPhysiologySubdaily_Item__isoAct
    "iso_act[nmol_m-2_s-1]",
#endif
#ifdef  OutputPhysiologySubdaily_Item__monoAct
    "mono_act[nmol_m-2_s-1]",
#endif
#ifdef  OutputPhysiologySubdaily_Item__isoEmi
    "iso_emis[umol_m-2]",
#endif
#ifdef  OutputPhysiologySubdaily_Item__monoEmi
    "mono_emis[umol_m-2]",
#endif
#ifdef  OutputPhysiologySubdaily_Item__monosEmi
    "mono_storage_emis[umol_m-2]",
#endif
#ifdef  OutputPhysiologySubdaily_Item__ovocEmi
    "ovoc_emis[umol_m-2]",
#endif
#ifdef  OutputPhysiologySubdaily_Item__psi
    "psi_pd",
    "psi_mean",
#endif
};

#define  OutputPhysiologySubdaily_Datasize  (sizeof( OutputPhysiologySubdaily_Header) / sizeof( OutputPhysiologySubdaily_Header[0]))
ldndc_output_size_t const  OutputPhysiologySubdaily_Sizes[] =
{
#ifdef  OutputPhysiologySubdaily_Item__species
    1,
    OutputPhysiologySubdaily_Datasize - 1,
#else
    OutputPhysiologySubdaily_Datasize,
#endif
    OutputPhysiologySubdaily_Datasize /*total size*/
};

ldndc_output_size_t const *  OutputPhysiologySubdaily_EntitySizes = NULL;
#define  OutputPhysiologySubdaily_Rank  ((ldndc_output_rank_t)(sizeof( OutputPhysiologySubdaily_Sizes) / sizeof( OutputPhysiologySubdaily_Sizes[0])) - 1)
atomic_datatype_t const  OutputPhysiologySubdaily_Types[] =
{
#ifdef  OutputPhysiologySubdaily_Item__species
    LDNDC_STRING,
#endif
    LDNDC_FLOAT64
};

LMOD_OUTPUT_MODULE_NAME::LMOD_OUTPUT_MODULE_NAME(
        MoBiLE_State *  _state,
        cbm::io_kcomm_t *  _io_kcomm,
        timemode_e  _timemode)
        : LMOD_OUTPUT_MODULE_BASE( _state, _io_kcomm, _timemode),

          io_kcomm( _io_kcomm),

          phys( _state->get_substate< substate_physiology_t >()),
          water( _state->get_substate< substate_watercycle_t >()),
          m_veg( &_state->vegetation),
          soillayers( _io_kcomm->get_input_class< input_class_soillayers_t >())
{
    this->push_accumulated_outputs();
}


LMOD_OUTPUT_MODULE_NAME::~LMOD_OUTPUT_MODULE_NAME()
{
}

lerr_t
LMOD_OUTPUT_MODULE_NAME::configure(
                ldndc::config_file_t const *  _cf)
{
    lerr_t  rc_setflags = set_metaflags( _cf);
    if ( rc_setflags)
        { return  LDNDC_ERR_FAIL; }

    bool  bool_option = false;
    CF_LMOD_QUERY(_cf, "single_files", bool_option, false);

    cbm::state_scratch_t * mcom = io_kcomm->get_scratch();
    mcom->set("physiologysubdaily.singlefiles", bool_option);

    return  LDNDC_ERR_OK;
}

lerr_t
LMOD_OUTPUT_MODULE_NAME::initialize()
{
    this->push_accumulated_outputs();

    return  LDNDC_ERR_OK;
}



lerr_t
LMOD_OUTPUT_MODULE_NAME::sink_handle_acquire_(
        sink_handle_t &  _snk, int  _index)
{
    int  sink_index = _index;
    if ( !separate_concurrent())
        { sink_index = 0; }
    _snk = io_kcomm->sink_handle_acquire( "physiologysubdaily", sink_index);
    if ( _snk.status() == LDNDC_ERR_OK)
    {
        lerr_t  rc_layout = LDNDC_OUTPUT_DEFINE_SINK_LAYOUT_DEFAULTS(
                _snk,OutputPhysiologySubdaily);
        RETURN_IF_NOT_OK(rc_layout);
    }
    else
    {
        KLOGERROR( "sink status bad  [sink=","physiologysubdaily","]");
        return  _snk.status();
    }

    return  LDNDC_ERR_OK;
}
lerr_t
LMOD_OUTPUT_MODULE_NAME::acquire_sinks_if_needed_()
{
    size_t const  c = m_veg->size();
    if ( c == 0)
    {
        return  LDNDC_ERR_OK;
    }
    else if ( c > 1)
    {
        if ( !snk_sum_.is_acquired())
        {
            lerr_t  rc_snk_avg = sink_handle_acquire_( snk_sum_, 0);
            RETURN_IF_NOT_OK(rc_snk_avg);
        }
    }

    size_t const  C = m_veg->slot_cnt()-1;
    if ( cbm::is_invalid( C) || ( snks_.size() > C))
    {
        return  LDNDC_ERR_OK;
    }

    /* recruit more sinks */
    for ( size_t  s = snks_.size();  s <= C;  ++s)
    {
        ldndc::sink_handle_t  snk;
        lerr_t  rc_snk = sink_handle_acquire_( snk, s+1);
        RETURN_IF_NOT_OK(rc_snk);

        snks_.push_back( snk);
    }

    return  LDNDC_ERR_OK;
}

lerr_t
LMOD_OUTPUT_MODULE_NAME::solve()
{
    this->pop_accumulated_outputs();
    
    lerr_t  rc_sinks = acquire_sinks_if_needed_();
    RETURN_IF_NOT_OK(rc_sinks);
    if ( snks_.size() == 0)
    {
        return  LDNDC_ERR_OK;
    }

    size_t const  c = m_veg->size();

    ldndc_flt64_t  data_flt64_a[OutputPhysiologySubdaily_Datasize];
    cbm::mem_set( data_flt64_a, OutputPhysiologySubdaily_Datasize, 0.0);
    for ( PlantIterator vt = m_veg->begin(); vt != m_veg->end(); ++vt)
    {
#ifdef  OutputPhysiologySubdaily_Item__species
        ldndc_string_t  data_string[1];
        cbm::as_strcpy( &data_string[0], (*vt)->cname());
#endif


        ldndc_flt64_t  data_flt64_0[OutputPhysiologySubdaily_Datasize];
        lerr_t  rc_dump = m_writerecord( *vt, data_flt64_0, data_flt64_a);
        RETURN_IF_NOT_OK(rc_dump);

#ifdef  OutputPhysiologySubdaily_Item__species
        void *  data[] = { data_string, data_flt64_0};
#else
        void *  data[] = { data_flt64_0};
#endif
        lerr_t  rc_write =
            write_fixed_record( &snks_[(*vt)->slot], data);
        if ( rc_write)
            { return  LDNDC_ERR_FAIL; }
    }

    if ( snk_sum_.is_acquired())
    {
        if ( c > 0)
        {
#ifdef  OutputPhysiologySubdaily_Item__species
            ldndc_string_t  data_string[1];
            cbm::as_strcpy( &data_string[0], COLUMN_NAME_ALLSPECIES);
            void *  data_a[] = { data_string, data_flt64_a};
#else
            void *  data_a[] = { data_flt64_a};
#endif
            lerr_t  rc_write =
                write_fixed_record( &snk_sum_, data_a);
            if ( rc_write)
                { return  LDNDC_ERR_FAIL; }
        }
    }
    
    this->push_accumulated_outputs();
    
    return  LDNDC_ERR_OK;
}


void
LMOD_OUTPUT_MODULE_NAME::pop_accumulated_outputs()
{
    this->acc.nh3_uptake =
        this->phys->accumulated_nh3_uptake_sl.sum() - this->acc.nh3_uptake;
    this->acc.nh4_uptake =
        this->phys->accumulated_nh4_uptake_sl.sum() - this->acc.nh4_uptake;
    this->acc.no3_uptake =
        this->phys->accumulated_no3_uptake_sl.sum() - this->acc.no3_uptake;
    this->acc.don_uptake =
        this->phys->accumulated_don_uptake_sl.sum() - this->acc.don_uptake;
}
void
LMOD_OUTPUT_MODULE_NAME::push_accumulated_outputs()
{
    this->acc.nh3_uptake = this->phys->accumulated_nh3_uptake_sl.sum();
    this->acc.nh4_uptake = this->phys->accumulated_nh4_uptake_sl.sum();
    this->acc.no3_uptake = this->phys->accumulated_no3_uptake_sl.sum();
    this->acc.don_uptake = this->phys->accumulated_don_uptake_sl.sum();
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::finalize()
{
    for ( size_t  s = 0;  s < snks_.size();  ++s)
    {
        if ( snks_[s].is_acquired())
        {
            io_kcomm->sink_handle_release( &snks_[s]);
        }
    }
    snks_.clear();

    if ( snk_sum_.is_acquired())
    {
        io_kcomm->sink_handle_release( &snk_sum_);
    }
    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::m_writerecord( MoBiLE_Plant * _vt,
        ldndc_flt64_t *  _buf, ldndc_flt64_t * _buf_avg)
{
    foliage_sums_t  fs;
    foliage_sums_( _vt, &fs);


    LDNDC_OUTPUT_SET_COLUMN_INDEX(0);

#ifdef  OutputPhysiologySubdaily_Item__dEmerg
    LDNDC_OUTPUT_SET_COLUMN_AVG_MAX(_vt->dEmerg);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_vt->dEmerg);
#endif
#ifdef  OutputPhysiologySubdaily_Item__tCum
    LDNDC_OUTPUT_SET_COLUMN_AVG_MAX(_vt->growing_degree_days);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_vt->growing_degree_days);
#endif
#ifdef  OutputPhysiologySubdaily_Item__dvsflush
    LDNDC_OUTPUT_SET_COLUMN_AVG_MAX(_vt->dvsFlush);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_vt->dvsFlush);
#endif
#ifdef  OutputPhysiologySubdaily_Item__dvsmort
    LDNDC_OUTPUT_SET_COLUMN_AVG_MAX(_vt->dvsMort);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_vt->dvsMort);
#endif
#ifdef  OutputPhysiologySubdaily_Item__uptDON
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(this->acc.don_uptake);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->acc.don_uptake);
#endif
#ifdef  OutputPhysiologySubdaily_Item__uptNH4
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(this->acc.nh4_uptake);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->acc.nh4_uptake);
#endif
#ifdef  OutputPhysiologySubdaily_Item__uptNO3
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(this->acc.no3_uptake);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->acc.no3_uptake);
#endif
#ifdef  OutputPhysiologySubdaily_Item__uptNH3
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(this->acc.nh3_uptake);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->acc.nh3_uptake);
#endif
#ifdef  OutputPhysiologySubdaily_Item__uptNOx
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_vt->nox_uptake);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_vt->nox_uptake);
#endif
#ifdef  OutputPhysiologySubdaily_Item__fixN2
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_vt->n2_fixation);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_vt->n2_fixation);
#endif
#ifdef  OutputPhysiologySubdaily_Item__dcFol
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_vt->dcFol);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_vt->dcFol);
#endif
#ifdef  OutputPhysiologySubdaily_Item__dcFru
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_vt->dcBud);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_vt->dcBud);
#endif
#ifdef  OutputPhysiologySubdaily_Item__dcFrt
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_vt->dcFrt);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_vt->dcFrt);
#endif
#ifdef  OutputPhysiologySubdaily_Item__dcLst
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_vt->dcSap);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_vt->dcSap);
#endif
#ifdef  OutputPhysiologySubdaily_Item__dcFac
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_vt->dcFac);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_vt->dcFac);
#endif
#ifdef  OutputPhysiologySubdaily_Item__rFol
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_vt->rFol);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_vt->rFol);
#endif
#ifdef  OutputPhysiologySubdaily_Item__rFru
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_vt->rBud);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_vt->rBud);
#endif
#ifdef  OutputPhysiologySubdaily_Item__rFrt
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_vt->rFrt);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_vt->rFrt);
#endif
#ifdef  OutputPhysiologySubdaily_Item__rLst
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_vt->rSap);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_vt->rSap);
#endif
#ifdef  OutputPhysiologySubdaily_Item__rRes
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_vt->rRes);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_vt->rRes);
#endif
#ifdef  OutputPhysiologySubdaily_Item__rTra
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_vt->rTra);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_vt->rTra);
#endif
#ifdef  OutputPhysiologySubdaily_Item__rGro
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_vt->rGro);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_vt->rGro);
#endif
#ifdef  OutputPhysiologySubdaily_Item__cUpt
    double const cUpt_vt( cbm::sum( _vt->carbonuptake_fl, _vt->nb_foliagelayers()));
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(cUpt_vt);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(cUpt_vt);
#endif
#ifdef  OutputPhysiologySubdaily_Item__fFac
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_vt->f_fac * _vt->f_area);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_vt->f_fac);
#endif
#ifdef  OutputPhysiologySubdaily_Item__mFol
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_vt->foliage_matter());
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_vt->foliage_matter());
#endif
#ifdef  OutputPhysiologySubdaily_Item__mFru
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_vt->mBud);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_vt->mBud);
#endif
#ifdef  OutputPhysiologySubdaily_Item__mFrt
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_vt->mFrt);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_vt->mFrt);
#endif
#ifdef  OutputPhysiologySubdaily_Item__mLst
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_vt->living_structural_matter());
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_vt->living_structural_matter());
#endif
#ifdef  OutputPhysiologySubdaily_Item__mDst
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_vt->dead_structural_matter());
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_vt->dead_structural_matter());
#endif
#ifdef  OutputPhysiologySubdaily_Item__mBelow
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_vt->belowground_biomass());
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_vt->belowground_biomass());
#endif
#ifdef  OutputPhysiologySubdaily_Item__mAbove
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_vt->aboveground_biomass());
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_vt->aboveground_biomass());
#endif
#ifdef  OutputPhysiologySubdaily_Item__lai
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(fs.lai);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(fs.lai);
#endif
#ifdef  OutputPhysiologySubdaily_Item__sla
//     double const  sla_vt( vs->sla_vtfl[_vt].sum());
//     LDNDC_OUTPUT_SET_COLUMN_AVG_SUM( sla_vt);
//     LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( sla_vt);
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM( fs.sla);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( fs.sla);    
#endif
#ifdef  OutputPhysiologySubdaily_Item__nRet
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_vt->n_retention);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_vt->n_retention);
#endif
#ifdef  OutputPhysiologySubdaily_Item__ncFol
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_vt->ncFol * _vt->f_area);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_vt->ncFol);
#endif
#ifdef  OutputPhysiologySubdaily_Item__ncFru
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_vt->ncBud * _vt->f_area);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_vt->ncBud);
#endif
#ifdef  OutputPhysiologySubdaily_Item__ncFrt
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_vt->ncFrt * _vt->f_area);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_vt->ncFrt);
#endif
#ifdef  OutputPhysiologySubdaily_Item__ncLst
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_vt->ncSap * _vt->f_area);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_vt->ncSap);
#endif
#ifdef  OutputPhysiologySubdaily_Item__ncDst
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_vt->ncCor * _vt->f_area);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_vt->ncCor);
#endif
#ifdef  OutputPhysiologySubdaily_Item__ntot
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_vt->total_nitrogen());
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_vt->total_nitrogen());
#endif
#ifdef  OutputPhysiologySubdaily_Item__sFol
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_vt->sFol);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_vt->sFol);
#endif
#ifdef  OutputPhysiologySubdaily_Item__sFru
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_vt->sBud);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_vt->sBud);
#endif
#ifdef  OutputPhysiologySubdaily_Item__sFrt
    double const sFrt_sl_sum( cbm::sum( _vt->sFrt_sl, soillayers->soil_layer_cnt()));
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(sFrt_sl_sum);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(sFrt_sl_sum);
#endif
#ifdef  OutputPhysiologySubdaily_Item__sWood
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_vt->sWoodAbove);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_vt->sWoodAbove);
#endif
#ifdef  OutputPhysiologySubdaily_Item__exsuLoss
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_vt->exsuLoss);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_vt->exsuLoss);
#endif
#ifdef  OutputPhysiologySubdaily_Item__nLitFol
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_vt->nLitFol);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_vt->nLitFol);
#endif
#ifdef  OutputPhysiologySubdaily_Item__nLitFru
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_vt->nLitBud);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_vt->nLitBud);
#endif
#ifdef  OutputPhysiologySubdaily_Item__nLitFrt
    double const nLitFrt_sl_sum( cbm::sum( _vt->nLitFrt_sl, soillayers->soil_layer_cnt()));
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(nLitFrt_sl_sum);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(nLitFrt_sl_sum);
#endif
#ifdef  OutputPhysiologySubdaily_Item__nLitWood
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_vt->nLitWoodAbove);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_vt->nLitWoodAbove);
#endif
#ifdef  OutputPhysiologySubdaily_Item__droughtRel
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_vt->f_h2o * _vt->f_area);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_vt->f_h2o);
#endif
#ifdef  OutputPhysiologySubdaily_Item__conductRel
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(fs.conductRel);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(fs.conductRel);
#endif
#ifdef  OutputPhysiologySubdaily_Item__vcact25
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(fs.vcAct25);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(fs.vcAct25);
#endif
#ifdef  OutputPhysiologySubdaily_Item__isoAct
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(fs.isoAct);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(fs.isoAct);
#endif
#ifdef  OutputPhysiologySubdaily_Item__monoAct
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(fs.monoAct);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(fs.monoAct);
#endif
#ifdef  OutputPhysiologySubdaily_Item__isoEmi
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(fs.iso_emi_sum);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(fs.iso_emi_sum);
#endif
#ifdef  OutputPhysiologySubdaily_Item__monoEmi
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(fs.mono_emi_sum);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(fs.mono_emi_sum);
#endif
#ifdef  OutputPhysiologySubdaily_Item__monosEmi
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(fs.mono_s_emi_sum);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(fs.mono_s_emi_sum);
#endif
#ifdef  OutputPhysiologySubdaily_Item__ovocEmi
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(fs.ovoc_emi_sum);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(fs.ovoc_emi_sum);
#endif
#ifdef  OutputPhysiologySubdaily_Item__psi
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_vt->psi_pd);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_vt->psi_pd);
    
    LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(_vt->psi_mean);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_vt->psi_mean);
#endif
    return  LDNDC_ERR_OK;

}
} 

namespace ldndc {
void
LMOD_OUTPUT_MODULE_NAME::foliage_sums_( MoBiLE_Plant *  _vt, foliage_sums_t *  _fs)
{
    _fs->vcAct25        = 0.0;
    _fs->isoAct         = 0.0;
    _fs->monoAct        = 0.0;
    _fs->conductRel     = 0.0;
    _fs->sla            = 0.0;
    _fs->iso_emi_sum    = 0.0;
    _fs->mono_emi_sum   = 0.0;
    _fs->mono_s_emi_sum = 0.0;
    _fs->ovoc_emi_sum   = 0.0;

    size_t const  nb_foliagelayers = _vt->nb_foliagelayers();
    _fs->lai = cbm::sum( _vt->lai_fl, nb_foliagelayers);
    if (_fs->lai > 0.0)
    {
        for ( size_t  fl = 0;  fl < nb_foliagelayers;  ++fl)
        {
            _fs->conductRel     += _vt->relativeconductance_fl[fl] * _vt->fFol_fl[fl];
            _fs->isoAct         += _vt->isoAct_fl[fl]  * _vt->fFol_fl[fl];
            _fs->monoAct        += _vt->monoAct_fl[fl] * _vt->fFol_fl[fl];
            _fs->vcAct25        += _vt->vcAct25_fl[fl] * _vt->fFol_fl[fl];
            _fs->sla            += _vt->sla_fl[fl] * _vt->fFol_fl[fl];
            _fs->iso_emi_sum    += _vt->isoprene_emission_fl[fl];
            _fs->mono_emi_sum   += _vt->monoterpene_emission_fl[fl];
            _fs->mono_s_emi_sum += _vt->monoterpene_s_emission_fl[fl];
            _fs->ovoc_emi_sum   += _vt->ovoc_emission_fl[fl];
        }
    }
}


#undef  LMOD_OUTPUT_MODULE_BASE
#undef  LMOD_OUTPUT_MODULE_NAME

#undef  OutputPhysiologySubdaily_Rank
#undef  OutputPhysiologySubdaily_Datasize

} /*namespace ldndc*/

