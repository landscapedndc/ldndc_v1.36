/*!
 * @brief
 *
 * @author
 *  David Kraus
 */

#include  "output/ecosystem/output-ecosystem-yearly.h"

#include  "constants/lconstants-plant.h"
#include  "constants/lconstants-conv.h"

#define  LMOD_OUTPUT_MODULE_BASE  MBE_LegacyOutputModel
#define  LMOD_OUTPUT_MODULE_NAME  OutputEcosystemYearly

LMOD_MODULE_INFO(LMOD_OUTPUT_MODULE_NAME,TMODE_POST_DAILY,LMOD_FLAG_USER|LMOD_FLAG_OUTPUT);
namespace ldndc {


/*!
 * @page ecosystemoutput
 * @section ecosystemoutputoutputyearly Ecosystem output (yearly)
 *
 * entity name | decription | unit
 * ----------- | ---------- | ----
 * aC\_gpp | Annual gross primary productivity |  [kgCha-1]
 * aC\_ter | Annual total ecosystem respiration |  [kgCha-1]
 * aC\_fertilize | Annual carbon input from fertilization |  [kgCha-1]
 * aC\_export_harvest | Annual carbon exported from field by harvest and cutting |  [kgCha-1]
 * aC\_leach | Annual carbon leached out |  [kgCha-1]
 * C\_total | Total ecosystem carbon |  [kgCha-1]
 * C\_soil | Total soil carbon |  [kgCha-1]
 * C\_wood\_above | Total carbon in aboveground dead wood |  [kgCha-1]
 * C\_wood\_below | Total carbon in belowground dead wood |  [kgCha-1]
 * C\_stand\_above | Total carbon in aboveground  wood |  [kgCha-1]
 * C\_stand\_below | Total carbon in belowground  wood  |  [kgCha-1]
 * aN\_fertilize | Annual amont of nitrogen fertilizer |  [kgNha-1]
 * aN\_export\_harvest | Annual nitrogen exported from field by harvest and cutting |  [kgNha-1]
 * aN\_deposition | Annual nitrogen input by deposition |  [kgNha-1]
 * aN\_fixation | Annual nitrogen fixation  |  [kgNha-1]
 * aN\_emis | Annual nitrogen loss by gaseous emissions |  [kgNha-1]
 * aN\_leach | Annual nitrogen loss by leaching |  [kgNha-1]
 * N\_total | Total ecosystem nitrogen |  [kgNha-1]
 * N\_soil | Total soil nitrogen |  [kgNha-1]
 * N\_wood\_above | Total nitrogen in aboveground dead wood |  [kgNha-1]
 * N\_wood\_below | Total nitrogen in belowground dead wood |  [kgNha-1]
 * N\_stand\_above | Total nitrogen in aboveground  wood |  [kgNha-1]
 * N\_stand\_below | Total nitrogen in belowground  wood  |  [kgNha-1]
 */


#define  OutputEcosystemYearly_Item__GPP_C
#define  OutputEcosystemYearly_Item__TER_C
#define  OutputEcosystemYearly_Item__FERT_C
#define  OutputEcosystemYearly_Item__HARVEST_C
#define  OutputEcosystemYearly_Item__LEACH_C

#define  OutputEcosystemYearly_Item__TOTAL_C
#define  OutputEcosystemYearly_Item__SOIL_C
#define  OutputEcosystemYearly_Item__DEADWOOD_ABOVE_C
#define  OutputEcosystemYearly_Item__DEADWOOD_BELOW_C
#define  OutputEcosystemYearly_Item__VEGETATION_ABOVE_C
#define  OutputEcosystemYearly_Item__VEGETATION_BELOW_C

#define  OutputEcosystemYearly_Item__FERT_N
#define  OutputEcosystemYearly_Item__HARVEST_N
#define  OutputEcosystemYearly_Item__DEPOSIT_N
#define  OutputEcosystemYearly_Item__FIX_N
#define  OutputEcosystemYearly_Item__EMIS_N
#define  OutputEcosystemYearly_Item__LEACH_N
#define  OutputEcosystemYearly_Item__TOTAL_N
#define  OutputEcosystemYearly_Item__SOIL_N
#define  OutputEcosystemYearly_Item__DEADWOOD_ABOVE_N
#define  OutputEcosystemYearly_Item__DEADWOOD_BELOW_N
#define  OutputEcosystemYearly_Item__VEGETATION_ABOVE_N
#define  OutputEcosystemYearly_Item__VEGETATION_BELOW_N



ldndc_string_t const  OutputEcosystemYearly_Ids[] =
{

#ifdef  OutputEcosystemYearly_Item__GPP_C
    "aC_gpp",
#endif
#ifdef  OutputEcosystemYearly_Item__TER_C
    "aC_ter",
#endif
#ifdef  OutputEcosystemYearly_Item__FERT_C
    "aC_fertilize",
#endif
#ifdef  OutputEcosystemYearly_Item__HARVEST_C
    "aC_export_harvest",
#endif
#ifdef  OutputEcosystemYearly_Item__LEACH_C
    "aC_leach",
#endif
#ifdef  OutputEcosystemYearly_Item__TOTAL_C
    "C_total",
#endif
#ifdef  OutputEcosystemYearly_Item__SOIL_C
    "C_soil",
#endif
#ifdef  OutputEcosystemYearly_Item__DEADWOOD_ABOVE_C
    "C_deadwood_above",
#endif
#ifdef  OutputEcosystemYearly_Item__DEADWOOD_BELOW_C
    "C_deadwood_below",
#endif
#ifdef  OutputEcosystemYearly_Item__VEGETATION_ABOVE_C
    "C_vegetation_above",
#endif
#ifdef  OutputEcosystemYearly_Item__VEGETATION_BELOW_C
    "C_vegetation_below",
#endif
#ifdef  OutputEcosystemYearly_Item__FERT_N
    "aN_fertilize",
#endif
#ifdef  OutputEcosystemYearly_Item__HARVEST_N
    "aN_export_harvest",
#endif
#ifdef  OutputEcosystemYearly_Item__DEPOSIT_N
    "aN_deposition",
#endif
#ifdef  OutputEcosystemYearly_Item__FIX_N
    "aN_fixation",
#endif
#ifdef  OutputEcosystemYearly_Item__EMIS_N
    "aN_emis",
#endif
#ifdef  OutputEcosystemYearly_Item__LEACH_N
    "aN_leach",
#endif
#ifdef  OutputEcosystemYearly_Item__TOTAL_N
    "N_total",
#endif
#ifdef  OutputEcosystemYearly_Item__SOIL_N
    "N_soil",
#endif
#ifdef  OutputEcosystemYearly_Item__DEADWOOD_ABOVE_N
    "N_deadwood_above",
#endif
#ifdef  OutputEcosystemYearly_Item__DEADWOOD_BELOW_N
    "N_deadwood_below",
#endif
#ifdef  OutputEcosystemYearly_Item__VEGETATION_ABOVE_N
    "N_vegetation_above",
#endif
#ifdef  OutputEcosystemYearly_Item__VEGETATION_BELOW_N
    "N_vegetation_below",
#endif
};



ldndc_string_t const  OutputEcosystemYearly_Header[] =
{

#ifdef  OutputEcosystemYearly_Item__GPP_C
    "aC_gpp[kgCha-1]",
#endif
#ifdef  OutputEcosystemYearly_Item__TER_C
    "aC_ter[kgCha-1]",
#endif
#ifdef  OutputEcosystemYearly_Item__FERT_C
    "aC_fertilize[kgCha-1]",
#endif
#ifdef  OutputEcosystemYearly_Item__HARVEST_C
    "aC_export_harvest[kgCha-1]",
#endif
#ifdef  OutputEcosystemYearly_Item__LEACH_C
    "aC_leach[kgCha-1]",
#endif
#ifdef  OutputEcosystemYearly_Item__TOTAL_C
    "C_total[kgCha-1]",
#endif
#ifdef  OutputEcosystemYearly_Item__SOIL_C
    "C_soil[kgCha-1]",
#endif
#ifdef  OutputEcosystemYearly_Item__DEADWOOD_ABOVE_C
    "C_wood_above[kgCha-1]",
#endif
#ifdef  OutputEcosystemYearly_Item__DEADWOOD_BELOW_C
    "C_wood_below[kgCha-1]",
#endif
#ifdef  OutputEcosystemYearly_Item__VEGETATION_ABOVE_C
    "C_stand_above[kgCha-1]",
#endif
#ifdef  OutputEcosystemYearly_Item__VEGETATION_BELOW_C
    "C_stand_below[kgCha-1]",
#endif
#ifdef  OutputEcosystemYearly_Item__FERT_N
    "aN_fertilize[kgNha-1]",
#endif
#ifdef  OutputEcosystemYearly_Item__HARVEST_N
    "aN_export_harvest[kgNha-1]",
#endif
#ifdef  OutputEcosystemYearly_Item__DEPOSIT_N
    "aN_deposition[kgNha-1]",
#endif
#ifdef  OutputEcosystemYearly_Item__FIX_N
    "aN_fixation[kgNha-1]",
#endif
#ifdef  OutputEcosystemYearly_Item__EMIS_N
    "aN_emis[kgNha-1]",
#endif
#ifdef  OutputEcosystemYearly_Item__LEACH_N
    "aN_leach[kgNha-1]",
#endif
#ifdef  OutputEcosystemYearly_Item__TOTAL_N
    "N_total[kgNha-1]",
#endif
#ifdef  OutputEcosystemYearly_Item__SOIL_N
    "N_soil[kgNha-1]",
#endif
#ifdef  OutputEcosystemYearly_Item__DEADWOOD_ABOVE_N
    "N_wood_above[kgNha-1]",
#endif
#ifdef  OutputEcosystemYearly_Item__DEADWOOD_BELOW_N
    "N_wood_below[kgNha-1]",
#endif
#ifdef  OutputEcosystemYearly_Item__VEGETATION_ABOVE_N
    "N_stand_above[kgNha-1]",
#endif
#ifdef  OutputEcosystemYearly_Item__VEGETATION_BELOW_N
    "N_stand_below[kgNha-1]",
#endif
};

#define  OutputEcosystemYearly_Datasize  (sizeof( OutputEcosystemYearly_Header) / sizeof( OutputEcosystemYearly_Header[0]))
ldndc_output_size_t const  OutputEcosystemYearly_Sizes[] =
{
    OutputEcosystemYearly_Datasize,

    OutputEcosystemYearly_Datasize /*total size*/
};
ldndc_output_size_t const *  OutputEcosystemYearly_EntitySizes = NULL;
#define  OutputEcosystemYearly_Rank  ((ldndc_output_rank_t)(sizeof( OutputEcosystemYearly_Sizes) / sizeof( OutputEcosystemYearly_Sizes[0])) - 1)
atomic_datatype_t const  OutputEcosystemYearly_Types[] =
{
    LDNDC_FLOAT64
};


LMOD_OUTPUT_MODULE_NAME::LMOD_OUTPUT_MODULE_NAME(
        MoBiLE_State *  _state,
        cbm::io_kcomm_t *  _io_kcomm,
        timemode_e  _timemode)
        : LMOD_OUTPUT_MODULE_BASE( _state, _io_kcomm, _timemode),

        io_kcomm( _io_kcomm),
        sl( _io_kcomm->get_input_class_ref< input_class_soillayers_t >()),
        phys( _state->get_substate< substate_physiology_t >()),
        soilchem( _state->get_substate< substate_soilchemistry_t >()),
        m_veg( &_state->vegetation)
{
    acc_c_outputs.gpp = acc_c_outputs.gpp_old = 0.0;
    acc_c_outputs.ter = acc_c_outputs.ter_old = 0.0;
    acc_c_outputs.accumulated_co2_hetero_old = 0.0;
    acc_c_outputs.c_fertilize_old = 0.0;
    acc_c_outputs.c_harvest_old = 0.0;
    acc_c_outputs.c_leach_old = 0.0;

    acc_n_outputs.n_deposition_old = 0.0;
    acc_n_outputs.n_fertilize_old = 0.0;
    acc_n_outputs.n2_fixation_plant = acc_n_outputs.n2_fixation_soil_old = 0.0;
    acc_n_outputs.emis_old = 0.0;
    acc_n_outputs.leach_old = 0.0;
}


LMOD_OUTPUT_MODULE_NAME::~LMOD_OUTPUT_MODULE_NAME()
{
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::configure(
          ldndc::config_file_t const *  _cf)
{
    lerr_t  rc_setflags =
        set_metaflags( _cf, RM_DEFAULT_YEARLY);
    if ( rc_setflags)
        { return  LDNDC_ERR_FAIL; }

    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::initialize()
{
    m_sink = io_kcomm->sink_handle_acquire( "ecosystemyearly");
    if ( m_sink.status() == LDNDC_ERR_OK)
    {
        lerr_t  rc_layout = LDNDC_OUTPUT_DEFINE_SINK_LAYOUT_DEFAULTS(
            m_sink,OutputEcosystemYearly);
        RETURN_IF_NOT_OK(rc_layout);
    }
    else
    {
        KLOGERROR( "sink status bad  [sink=","ecosystemyearly","]");
        return  m_sink.status();
    }

    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::solve()
{
    accumulate_outputs();

    if ( lclock()->is_position( TMODE_POST_YEARLY))
    {
        ldndc_flt64_t  data_flt64_0[OutputEcosystemYearly_Datasize];
        lerr_t  rc_dump = dump_0( data_flt64_0);
        RETURN_IF_NOT_OK(rc_dump);

        void *  data[] = { data_flt64_0};
        lerr_t  rc_write = write_fixed_record( &m_sink, data);
        if ( rc_write){ return  LDNDC_ERR_FAIL; }

        update_outputs();
    }

    return  LDNDC_ERR_OK;
}


void
LMOD_OUTPUT_MODULE_NAME::accumulate_outputs()
{
    for ( PlantIterator s = m_veg->begin(); s != m_veg->end(); ++s)
    {
        acc_c_outputs.gpp += cbm::sum( (*s)->d_carbonuptake_fl, (*s)->nb_foliagelayers());
        acc_c_outputs.ter += (*s)->d_rTra + (*s)->d_rRes + (*s)->d_rGro;
    }

    acc_c_outputs.ter += soilchem->accumulated_co2_emis_hetero - acc_c_outputs.accumulated_co2_hetero_old;
    acc_c_outputs.accumulated_co2_hetero_old = soilchem->accumulated_co2_emis_hetero;

    for ( PlantIterator s = m_veg->begin(); s != m_veg->end(); ++s)
    {
        acc_n_outputs.n2_fixation_plant += (*s)->d_n2_fixation;
    }
}


void
LMOD_OUTPUT_MODULE_NAME::update_outputs()
{
    acc_c_outputs.gpp_old = acc_c_outputs.gpp;
    acc_c_outputs.ter_old = acc_c_outputs.ter;
    acc_c_outputs.c_fertilize_old = soilchem->accumulated_c_fertilizer;
    acc_c_outputs.c_leach_old = soilchem->accumulated_ch4_leach + soilchem->accumulated_doc_leach;
    acc_c_outputs.c_harvest_old = phys->accumulated_c_export_harvest;

    acc_n_outputs.n_deposition_old = phys->accumulated_no3_throughfall + phys->accumulated_nh4_throughfall;
    acc_n_outputs.n_fertilize_old = soilchem->accumulated_n_fertilizer;
    acc_n_outputs.n2_fixation_plant = 0.0;
    acc_n_outputs.n2_fixation_soil_old = soilchem->accumulated_n_fix_algae + soilchem->accumulated_n2_fixation;
    acc_n_outputs.emis_old = soilchem->accumulated_no_emis + soilchem->accumulated_n2o_emis + soilchem->accumulated_n2_emis + soilchem->accumulated_nh3_emis;
    acc_n_outputs.leach_old = soilchem->accumulated_don_leach + soilchem->accumulated_no3_leach + soilchem->accumulated_nh4_leach;
    acc_n_outputs.n_harvest_old = phys->accumulated_n_export_harvest;
}

lerr_t
LMOD_OUTPUT_MODULE_NAME::finalize()
{
    io_kcomm->sink_handle_release( &m_sink);
    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::dump_0(
        ldndc_flt64_t *  _buf)
{
    /*! conversion from kg m-2 to kg ha-1 */
    static const double  p( cbm::M2_IN_HA);

    double aC_fertilize( soilchem->accumulated_c_fertilizer - acc_c_outputs.c_fertilize_old);

    LDNDC_OUTPUT_SET_COLUMN_INDEX(0);

#ifdef OutputEcosystemYearly_Item__GPP_C
    {
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT (p * (acc_c_outputs.gpp - acc_c_outputs.gpp_old));
    }
#endif

#ifdef OutputEcosystemYearly_Item__TER_C
    {
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT (p * (acc_c_outputs.ter - acc_c_outputs.ter_old));
    }
#endif

#ifdef OutputEcosystemYearly_Item__FERT_C
    {
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT (p * aC_fertilize);
    }
#endif


#ifdef OutputEcosystemYearly_Item__HARVEST_C
    double aC_harvest( phys->accumulated_c_export_harvest - acc_c_outputs.c_harvest_old);
    {
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT (p * aC_harvest);
    }
#endif

#ifdef OutputEcosystemYearly_Item__LEACH_C
    double aC_leach( soilchem->accumulated_ch4_leach + soilchem->accumulated_doc_leach - acc_c_outputs.c_leach_old);
    {
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT (p * aC_leach);
    }
#endif

    double const wood_above_c( soilchem->c_wood);
    double const wood_below_c( soilchem->c_wood_sl.sum());

    double const wood_above_n( soilchem->n_wood);
    double const wood_below_n( soilchem->n_wood_sl.sum());

    double soil_c( 0.0);
    double soil_n( 0.0);

    for ( size_t  l = 0;  l < sl.soil_layer_cnt();  ++l)
    {
        double const  microC_sl( soilchem->C_micro1_sl[l] + soilchem->C_micro2_sl[l] + soilchem->C_micro3_sl[l]);
        double const  solC_sl( soilchem->doc_sl[l] + soilchem->an_doc_sl[l]);
        double const  litC_sl( soilchem->C_lit1_sl[l] + soilchem->C_lit2_sl[l] + soilchem->C_lit3_sl[l]
                             + soilchem->c_raw_lit_1_sl[l] + soilchem->c_raw_lit_2_sl[l] + soilchem->c_raw_lit_3_sl[l]);
        soil_c += microC_sl + solC_sl + litC_sl + soilchem->C_aorg_sl[l] + soilchem->C_hum_sl[l];


        double const  solN_sl( soilchem->nh4_sl[l] + soilchem->don_sl[l]
                              + soilchem->nh3_liq_sl[l] + soilchem->urea_sl[l]
                              + soilchem->no3_sl[l] + soilchem->an_no3_sl[l]);
        double const  litN_sl( soilchem->N_lit1_sl[l]+soilchem->N_lit2_sl[l]+soilchem->N_lit3_sl[l]
                             + soilchem->n_raw_lit_1_sl[l] + soilchem->n_raw_lit_2_sl[l] + soilchem->n_raw_lit_3_sl[l]);
        soil_n += soilchem->N_micro_sl[l] + solN_sl + litN_sl + soilchem->N_aorg_sl[l] + soilchem->N_hum_sl[l];
    }

    double stand_above_c( 0.0);
    double stand_below_c( 0.0);

    double stand_above_n( 0.0);
    double stand_below_n( 0.0);
    for ( PlantIterator s = m_veg->begin(); s != m_veg->end(); ++s)
    {
        stand_above_c += (*s)->aboveground_biomass() * cbm::CCDM;
        stand_below_c += (*s)->belowground_biomass() * cbm::CCDM;

        stand_above_n += (*s)->aboveground_nitrogen();
        stand_below_n += (*s)->belowground_nitrogen();
    }

    double const total_c( wood_above_c + wood_below_c + soil_c + stand_above_c + stand_below_c);
    double const total_n( wood_above_n + wood_below_n + soil_n + stand_above_n + stand_below_n);


#ifdef  OutputEcosystemYearly_Item__TOTAL_C
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT (p * total_c);
#endif
#ifdef  OutputEcosystemYearly_Item__SOIL_C
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT (p * soil_c);
#endif
#ifdef  OutputEcosystemYearly_Item__DEADWOOD_ABOVE_C
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT (p * wood_above_c);
#endif
#ifdef  OutputEcosystemYearly_Item__DEADWOOD_BELOW_C
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT (p * wood_below_c);
#endif
#ifdef  OutputEcosystemYearly_Item__VEGETATION_ABOVE_C
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT (p * stand_above_c);
#endif
#ifdef  OutputEcosystemYearly_Item__VEGETATION_BELOW_C
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT (p * stand_below_c);
#endif




#ifdef  OutputEcosystemYearly_Item__FERT_N
    double aN_fertilize( soilchem->accumulated_n_fertilizer - acc_n_outputs.n_fertilize_old);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT (p * aN_fertilize);
#endif
#ifdef  OutputEcosystemYearly_Item__HARVEST_N
    double aN_harvest( phys->accumulated_n_export_harvest - acc_n_outputs.n_harvest_old);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT (p * aN_harvest);
#endif
#ifdef  OutputEcosystemYearly_Item__DEPOSIT_N
    double aN_deposit( phys->accumulated_nh4_throughfall + phys->accumulated_no3_throughfall - acc_n_outputs.n_deposition_old);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT (p * aN_deposit);
#endif
#ifdef  OutputEcosystemYearly_Item__FIX_N
    double aN_fixation( acc_n_outputs.n2_fixation_plant + soilchem->accumulated_n_fix_algae + soilchem->accumulated_n2_fixation - acc_n_outputs.n2_fixation_soil_old);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT (p * aN_fixation);
#endif
#ifdef  OutputEcosystemYearly_Item__EMIS_N
    double aN_emis( soilchem->accumulated_no_emis + soilchem->accumulated_n2o_emis + soilchem->accumulated_n2_emis + soilchem->accumulated_nh3_emis - acc_n_outputs.emis_old);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT (p * aN_emis);
#endif
#ifdef  OutputEcosystemYearly_Item__LEACH_N
    double aN_leach( soilchem->accumulated_don_leach + soilchem->accumulated_no3_leach + soilchem->accumulated_nh4_leach - acc_n_outputs.leach_old);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT (p * aN_leach);
#endif








#ifdef  OutputEcosystemYearly_Item__TOTAL_N
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT (p * total_n);
#endif
#ifdef  OutputEcosystemYearly_Item__SOIL_N
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT (p * soil_n);
#endif
#ifdef  OutputEcosystemYearly_Item__DEADWOOD_ABOVE_N
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT (p * wood_above_n);
#endif
#ifdef  OutputEcosystemYearly_Item__DEADWOOD_BELOW_N
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT (p * wood_below_n);
#endif
#ifdef  OutputEcosystemYearly_Item__VEGETATION_ABOVE_N
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT (p * stand_above_n);
#endif
#ifdef  OutputEcosystemYearly_Item__VEGETATION_BELOW_N
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT (p * stand_below_n);
#endif

    return  LDNDC_ERR_OK;
}


#undef  LMOD_OUTPUT_MODULE_BASE
#undef  LMOD_OUTPUT_MODULE_NAME

#undef  OutputEcosystemYearly_Rank
#undef  OutputEcosystemYearly_Datasize

} /*namespace ldndc*/

