/*!
 * @brief
 *    dumping ecosystem variables
 *
 * @author
 *    David Kraus
 */

#include  "output/ecosystem/output-ecosystem-daily.h"

#include  "constants/lconstants-plant.h"
#include  "constants/lconstants-conv.h"

#define  LMOD_OUTPUT_MODULE_BASE  MBE_LegacyOutputModel
#define  LMOD_OUTPUT_MODULE_NAME  OutputEcosystemDaily

LMOD_MODULE_INFO(LMOD_OUTPUT_MODULE_NAME,TMODE_POST_DAILY,LMOD_FLAG_USER|LMOD_FLAG_OUTPUT);
namespace ldndc {


#define  OutputEcosystemDaily_Item__GPP
#define  OutputEcosystemDaily_Item__TER
#define  OutputEcosystemDaily_Item__NEE
#define  OutputEcosystemDaily_Item__TOTAL_C
#define  OutputEcosystemDaily_Item__SOIL_C
#define  OutputEcosystemDaily_Item__DEADWOOD_ABOVE_C
#define  OutputEcosystemDaily_Item__DEADWOOD_BELOW_C
#define  OutputEcosystemDaily_Item__VEGETATION_ABOVE_C
#define  OutputEcosystemDaily_Item__VEGETATION_BELOW_C
#define  OutputEcosystemDaily_Item__TOTAL_N
#define  OutputEcosystemDaily_Item__SOIL_N
#define  OutputEcosystemDaily_Item__DEADWOOD_ABOVE_N
#define  OutputEcosystemDaily_Item__DEADWOOD_BELOW_N
#define  OutputEcosystemDaily_Item__VEGETATION_ABOVE_N
#define  OutputEcosystemDaily_Item__VEGETATION_BELOW_N


/*!
 * @page ecosystemoutput
 * @section ecosystemoutputoutputdaily Ecosystem output (daily)
 *
 * entity name | decription | unit
 * ----------- | ---------- | ----
 * dC\_gpp | Daily gross primary productivity |  [kgCha-1]
 * dC\_ter | Daily total ecosystem respiration |  [kgCha-1]
 * dC\_nee | Daily total net ecosystem exchange  |  [kgCha-1]
 * aC\_fertilize | Annual carbon input from fertilization |  [kgCha-1]
 * C\_total | Total ecosystem carbon |  [kgCha-1]
 * C\_soil | Total soil carbon |  [kgCha-1]
 * C\_wood\_above | Total carbon in aboveground dead wood |  [kgCha-1]
 * C\_wood\_below | Total carbon in belowground dead wood |  [kgCha-1]
 * C\_stand\_above | Total carbon in aboveground  wood |  [kgCha-1]
 * C\_stand\_below | Total carbon in belowground  wood  |  [kgCha-1]
 * N\_total | Total ecosystem nitrogen |  [kgNha-1]
 * N\_soil | Total soil nitrogen |  [kgNha-1]
 * N\_wood\_above | Total nitrogen in aboveground dead wood |  [kgNha-1]
 * N\_wood\_below | Total nitrogen in belowground dead wood |  [kgNha-1]
 * N\_stand\_above | Total nitrogen in aboveground  wood |  [kgNha-1]
 * N\_stand\_below | Total nitrogen in belowground  wood  |  [kgNha-1]
 */
ldndc_string_t const  OutputEcosystemDaily_Ids[] =
{

#ifdef  OutputEcosystemDaily_Item__GPP
    "GPP",
#endif
#ifdef  OutputEcosystemDaily_Item__TER
    "TER",
#endif
#ifdef  OutputEcosystemDaily_Item__NEE
    "NEE",
#endif
#ifdef  OutputEcosystemDaily_Item__TOTAL_C
    "C_total",
#endif
#ifdef  OutputEcosystemDaily_Item__SOIL_C
    "C_soil",
#endif
#ifdef  OutputEcosystemDaily_Item__DEADWOOD_ABOVE_C
    "C_deadwood_above",
#endif
#ifdef  OutputEcosystemDaily_Item__DEADWOOD_BELOW_C
    "C_deadwood_below",
#endif
#ifdef  OutputEcosystemDaily_Item__VEGETATION_ABOVE_C
    "C_vegetation_above",
#endif
#ifdef  OutputEcosystemDaily_Item__VEGETATION_BELOW_C
    "C_vegetation_below",
#endif
#ifdef  OutputEcosystemDaily_Item__TOTAL_N
    "N_total",
#endif
#ifdef  OutputEcosystemDaily_Item__SOIL_N
    "N_soil",
#endif
#ifdef  OutputEcosystemDaily_Item__DEADWOOD_ABOVE_N
    "N_deadwood_above",
#endif
#ifdef  OutputEcosystemDaily_Item__DEADWOOD_BELOW_N
    "N_deadwood_below",
#endif
#ifdef  OutputEcosystemDaily_Item__VEGETATION_ABOVE_N
    "N_vegetation_above",
#endif
#ifdef  OutputEcosystemDaily_Item__VEGETATION_BELOW_N
    "N_vegetation_below",
#endif
};

ldndc_string_t const  OutputEcosystemDaily_Header[] =
{

#ifdef  OutputEcosystemDaily_Item__GPP
    "dC_GPP[kgCha-1]",
#endif
#ifdef  OutputEcosystemDaily_Item__TER
    "dC_TER[kgCha-1]",
#endif
#ifdef  OutputEcosystemDaily_Item__NEE
    "dC_NEE[kgCha-1]",
#endif
#ifdef  OutputEcosystemDaily_Item__TOTAL_C
    "C_total[kgCha-1]",
#endif
#ifdef  OutputEcosystemDaily_Item__SOIL_C
    "C_soil[kgCha-1]",
#endif
#ifdef  OutputEcosystemDaily_Item__DEADWOOD_ABOVE_C
    "C_wood_above[kgCha-1]",
#endif
#ifdef  OutputEcosystemDaily_Item__DEADWOOD_BELOW_C
    "C_wood_below[kgCha-1]",
#endif
#ifdef  OutputEcosystemDaily_Item__VEGETATION_ABOVE_C
    "C_stand_above[kgCha-1]",
#endif
#ifdef  OutputEcosystemDaily_Item__VEGETATION_BELOW_C
    "C_stand_below[kgCha-1]",
#endif
#ifdef  OutputEcosystemDaily_Item__TOTAL_N
    "N_total[kgNha-1]",
#endif
#ifdef  OutputEcosystemDaily_Item__SOIL_N
    "N_soil[kgNha-1]",
#endif
#ifdef  OutputEcosystemDaily_Item__DEADWOOD_ABOVE_N
    "N_wood_above[kgNha-1]",
#endif
#ifdef  OutputEcosystemDaily_Item__DEADWOOD_BELOW_N
    "N_wood_below[kgNha-1]",
#endif
#ifdef  OutputEcosystemDaily_Item__VEGETATION_ABOVE_N
    "N_stand_above[kgNha-1]",
#endif
#ifdef  OutputEcosystemDaily_Item__VEGETATION_BELOW_N
    "N_stand_below[kgNha-1]",
#endif
};

#define  OutputEcosystemDaily_Datasize  (sizeof( OutputEcosystemDaily_Header) / sizeof( OutputEcosystemDaily_Header[0]))
ldndc_output_size_t const  OutputEcosystemDaily_Sizes[] =
{
    OutputEcosystemDaily_Datasize,

    OutputEcosystemDaily_Datasize /*total size*/
};
ldndc_output_size_t const *  OutputEcosystemDaily_EntitySizes = NULL;
#define  OutputEcosystemDaily_Rank  ((ldndc_output_rank_t)(sizeof( OutputEcosystemDaily_Sizes) / sizeof( OutputEcosystemDaily_Sizes[0])) - 1)
atomic_datatype_t const  OutputEcosystemDaily_Types[] =
{
    LDNDC_FLOAT64
};


LMOD_OUTPUT_MODULE_NAME::LMOD_OUTPUT_MODULE_NAME(
          MoBiLE_State *  _state,
          cbm::io_kcomm_t *  _io_kcomm,
          timemode_e  _timemode)
          : LMOD_OUTPUT_MODULE_BASE( _state, _io_kcomm, _timemode),

          io_kcomm( _io_kcomm),
          sl( _io_kcomm->get_input_class_ref< input_class_soillayers_t >()),
          phys( _state->get_substate< substate_physiology_t >()),
          soilchem( _state->get_substate< substate_soilchemistry_t >()),
          m_veg( &_state->vegetation)
{
    this->push_accumulated_outputs();
}


LMOD_OUTPUT_MODULE_NAME::~LMOD_OUTPUT_MODULE_NAME()
{
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::configure(
                ldndc::config_file_t const *  _cf)
{
    lerr_t  rc_setflags = this->set_metaflags( _cf);
    if ( rc_setflags)
        { return  rc_setflags; }
    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::initialize()
{
    this->m_sink = this->io_kcomm->sink_handle_acquire( "ecosystemdaily");
    if ( this->m_sink.status() == LDNDC_ERR_OK)
    {
        lerr_t  rc_layout = LDNDC_OUTPUT_DEFINE_SINK_LAYOUT_DEFAULTS(
            this->m_sink,OutputEcosystemDaily);
        RETURN_IF_NOT_OK(rc_layout);
    }
    else
    {
        KLOGERROR( "sink status bad  [sink=","ecosystemdaily","]");
        return  this->m_sink.status();
    }

    this->push_accumulated_outputs();

    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::solve()
{
    this->pop_accumulated_outputs();

    ldndc_flt64_t  data_flt64_0[OutputEcosystemDaily_Datasize];
    lerr_t  rc_dump = m_writerecord( data_flt64_0);
    RETURN_IF_NOT_OK(rc_dump);

    void *  data[] = { data_flt64_0};
    lerr_t  rc_write = write_fixed_record( &this->m_sink, data);
    if ( rc_write)
        { return  LDNDC_ERR_FAIL; }

    this->push_accumulated_outputs();

    return  LDNDC_ERR_OK;
}

void
LMOD_OUTPUT_MODULE_NAME::pop_accumulated_outputs()
{
    this->acc_outputs.co2_hetero =
        this->soilchem->accumulated_co2_emis_hetero - this->acc_outputs.co2_hetero;
}
void
LMOD_OUTPUT_MODULE_NAME::push_accumulated_outputs()
{
    this->acc_outputs.co2_hetero =
        this->soilchem->accumulated_co2_emis_hetero;
}

lerr_t
LMOD_OUTPUT_MODULE_NAME::finalize()
{
    this->io_kcomm->sink_handle_release( &this->m_sink);
    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::m_writerecord( ldndc_flt64_t *  _buf)
{
    /*! conversion from kg m-2 to kg ha-1 */
    static const double  p( cbm::M2_IN_HA);


    LDNDC_OUTPUT_SET_COLUMN_INDEX(0);

#ifdef OutputEcosystemDaily_Item__GPP
    {
        double  carbonuptake = 0;
        for ( PlantIterator s = this->m_veg->begin(); s != this->m_veg->end(); ++s)
        {
            carbonuptake += cbm::sum( (*s)->d_carbonuptake_fl, (*s)->nb_foliagelayers());
        }
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT (p *  carbonuptake);
    }
#endif
    
#ifdef OutputEcosystemDaily_Item__TER 
    {
        double  respiration = 0;
        for ( PlantIterator s = this->m_veg->begin(); s != this->m_veg->end(); ++s)
        {
             respiration += (*s)->d_rTra + (*s)->d_rRes + (*s)->d_rGro;
        }
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT (p * ( this->acc_outputs.co2_hetero + respiration));
    }
#endif

#ifdef OutputEcosystemDaily_Item__NEE
    {
        double  respiration = 0;
        double  carbonuptake = 0;
        for ( PlantIterator s = this->m_veg->begin(); s != this->m_veg->end(); ++s)
        {
             respiration += (*s)->d_rTra + (*s)->d_rRes + (*s)->d_rGro;
             carbonuptake += cbm::sum( (*s)->d_carbonuptake_fl, (*s)->nb_foliagelayers());
        }
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT (p * ( carbonuptake - ( this->acc_outputs.co2_hetero + respiration)));
    }
#endif

    double const wood_above_c( soilchem->c_wood);
    double const wood_below_c( soilchem->c_wood_sl.sum());

    double const wood_above_n( soilchem->n_wood);
    double const wood_below_n( soilchem->n_wood_sl.sum());

    double soil_c( 0.0);
    double soil_n( 0.0);

    for ( size_t  l = 0;  l < sl.soil_layer_cnt();  ++l)
    {
        double const  microC_sl( soilchem->C_micro1_sl[l] + soilchem->C_micro2_sl[l] + soilchem->C_micro3_sl[l]);
        double const  solC_sl( soilchem->doc_sl[l] + soilchem->an_doc_sl[l]);
        double const  litC_sl( soilchem->C_lit1_sl[l] + soilchem->C_lit2_sl[l] + soilchem->C_lit3_sl[l]
                             + soilchem->c_raw_lit_1_sl[l] + soilchem->c_raw_lit_2_sl[l] + soilchem->c_raw_lit_3_sl[l]);
        soil_c += microC_sl + solC_sl + litC_sl + soilchem->C_aorg_sl[l] + soilchem->C_hum_sl[l];
        
        
        double const  solN_sl(  soilchem->nh4_sl[l] + soilchem->don_sl[l]
                              + soilchem->nh3_liq_sl[l] + soilchem->urea_sl[l]
                              + soilchem->no3_sl[l] + soilchem->an_no3_sl[l]);
        double const  litN_sl( soilchem->N_lit1_sl[l]+soilchem->N_lit2_sl[l]+soilchem->N_lit3_sl[l]
                             + soilchem->n_raw_lit_1_sl[l] + soilchem->n_raw_lit_2_sl[l] + soilchem->n_raw_lit_3_sl[l]);
        soil_n += soilchem->N_micro_sl[l] + solN_sl + litN_sl + soilchem->N_aorg_sl[l] + soilchem->N_hum_sl[l];
    }

    double stand_above_c( 0.0);
    double stand_below_c( 0.0);

    double stand_above_n( 0.0);
    double stand_below_n( 0.0);
    for ( PlantIterator s = this->m_veg->begin(); s != this->m_veg->end(); ++s)
    {
        stand_above_c += (*s)->aboveground_biomass() * cbm::CCDM;
        stand_below_c += (*s)->belowground_biomass() * cbm::CCDM;

        stand_above_n += (*s)->aboveground_nitrogen();
        stand_below_n += (*s)->belowground_nitrogen();
    }

    double const total_c( wood_above_c + wood_below_c + soil_c + stand_above_c + stand_below_c);
    double const total_n( wood_above_n + wood_below_n + soil_n + stand_above_n + stand_below_n);

#ifdef  OutputEcosystemDaily_Item__TOTAL_C
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT (p * total_c);
#endif
#ifdef  OutputEcosystemDaily_Item__SOIL_C
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT (p * soil_c);
#endif
#ifdef  OutputEcosystemDaily_Item__DEADWOOD_ABOVE_C
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT (p * wood_above_c);
#endif
#ifdef  OutputEcosystemDaily_Item__DEADWOOD_BELOW_C
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT (p * wood_below_c);
#endif
#ifdef  OutputEcosystemDaily_Item__VEGETATION_ABOVE_C
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT (p * stand_above_c);
#endif
#ifdef  OutputEcosystemDaily_Item__VEGETATION_BELOW_C
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT (p * stand_below_c);
#endif


#ifdef  OutputEcosystemDaily_Item__TOTAL_N
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT (p * total_n);
#endif
#ifdef  OutputEcosystemDaily_Item__SOIL_N
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT (p * soil_n);
#endif
#ifdef  OutputEcosystemDaily_Item__DEADWOOD_ABOVE_N
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT (p * wood_above_n);
#endif
#ifdef  OutputEcosystemDaily_Item__DEADWOOD_BELOW_N
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT (p * wood_below_n);
#endif
#ifdef  OutputEcosystemDaily_Item__VEGETATION_ABOVE_N
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT (p * stand_above_n);
#endif
#ifdef  OutputEcosystemDaily_Item__VEGETATION_BELOW_N
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT (p * stand_below_n);
#endif

    return  LDNDC_ERR_OK;
}


#undef  LMOD_OUTPUT_MODULE_BASE
#undef  LMOD_OUTPUT_MODULE_NAME

#undef  OutputEcosystemDaily_Rank
#undef  OutputEcosystemDaily_Datasize

} /*namespace ldndc*/

