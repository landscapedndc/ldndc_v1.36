/*!
 * @brief
 *
 * @author
 *    David Kraus
 */

#ifndef  LM_OUTPUT_ECOSYSTEM_YEARLY_H_NEW_
#define  LM_OUTPUT_ECOSYSTEM_YEARLY_H_NEW_

#include  "mbe_legacyoutputmodel.h"
#include  "state/mbe_state.h"

#define  LMOD_OUTPUT_MODULE_BASE  MBE_LegacyOutputModel
#define  LMOD_OUTPUT_MODULE_NAME  OutputEcosystemYearly
#define  LMOD_OUTPUT_MODULE_ID    "output:ecosystem:yearly"
#define  LMOD_OUTPUT_MODULE_DESC  "Ecosystem Yearly Output"
namespace ldndc {

struct  ec_yearly_accumulated_outputs_t
{
    double  gpp, gpp_old;
    double  ter, ter_old;
    double  accumulated_co2_hetero_old;

    double  c_fertilize_old;
    double  c_harvest_old;
    double  c_leach_old;
};



struct  en_yearly_accumulated_outputs_t
{
    double  n_deposition_old;
    double  n_fertilize_old;
    double  n2_fixation_plant;
    double  n2_fixation_soil_old;
    double  emis_old;
    double  n_harvest_old;
    double  leach_old;
};

class  LDNDC_API  LMOD_OUTPUT_MODULE_NAME  :  public  LMOD_OUTPUT_MODULE_BASE
{
    LMOD_EXPORT_MODULE_INFO(LMOD_OUTPUT_MODULE_NAME,LMOD_OUTPUT_MODULE_ID,LMOD_OUTPUT_MODULE_DESC);
    public:
        LMOD_OUTPUT_MODULE_NAME(
                MoBiLE_State *,
                cbm::io_kcomm_t *,
                timemode_e  _timemode);

        ~LMOD_OUTPUT_MODULE_NAME();


        lerr_t  configure( ldndc::config_file_t const *);

        lerr_t  initialize();

        lerr_t  solve();

        lerr_t  finalize();

        lerr_t  wake() { return  LDNDC_ERR_OK; }
        lerr_t  sleep() { return  LDNDC_ERR_OK; }

    private:
        lerr_t  dump_0(
                ldndc_flt64_t *);

    private:
        cbm::io_kcomm_t *  io_kcomm;

        input_class_soillayers_t const &  sl;
        substate_physiology_t const *  phys;
        substate_soilchemistry_t const *  soilchem;
        MoBiLE_PlantVegetation *  m_veg;

    private:
        ldndc::sink_handle_t  m_sink;
        ec_yearly_accumulated_outputs_t  acc_c_outputs;
        en_yearly_accumulated_outputs_t  acc_n_outputs;

        void accumulate_outputs();
        void update_outputs();

};
} /*namespace ldndc*/


#undef  LMOD_OUTPUT_MODULE_BASE
#undef  LMOD_OUTPUT_MODULE_NAME
#undef  LMOD_OUTPUT_MODULE_ID
#undef  LMOD_OUTPUT_MODULE_DESC


#endif  /*  !LM_OUTPUT_ECOSYSTEM_YEARLY_H_NEW_  */

