/*!
 * @brief
 *    dumping ecosystem variables
 *
 * @author
 *    David Kraus
 */

#ifndef  LM_OUTPUT_ECOSYSTEM_DAILY_H_
#define  LM_OUTPUT_ECOSYSTEM_DAILY_H_

#include  "mbe_legacyoutputmodel.h"
#include  "state/mbe_state.h"

#define  LMOD_OUTPUT_MODULE_BASE  MBE_LegacyOutputModel
#define  LMOD_OUTPUT_MODULE_NAME  OutputEcosystemDaily
#define  LMOD_OUTPUT_MODULE_ID    "output:ecosystem:daily"
#define  LMOD_OUTPUT_MODULE_DESC  "Ecosystem Daily Output"
namespace ldndc {

struct  ec_daily_accumulated_outputs_t
{
    double  co2_hetero;
};
class  LDNDC_API  LMOD_OUTPUT_MODULE_NAME  :  public  LMOD_OUTPUT_MODULE_BASE
{
    LMOD_EXPORT_MODULE_INFO(LMOD_OUTPUT_MODULE_NAME,LMOD_OUTPUT_MODULE_ID,LMOD_OUTPUT_MODULE_DESC);
    public:
        LMOD_OUTPUT_MODULE_NAME(
                MoBiLE_State *,
                cbm::io_kcomm_t *,
                timemode_e  _timemode);

        ~LMOD_OUTPUT_MODULE_NAME();


        lerr_t  configure( ldndc::config_file_t const *);

        lerr_t  initialize();
        lerr_t  solve();
        lerr_t  finalize();

        lerr_t  wake() { return  LDNDC_ERR_OK; }
        lerr_t  sleep() { return  LDNDC_ERR_OK; }

    private:
        lerr_t  m_writerecord( ldndc_flt64_t *);

    private:
        cbm::io_kcomm_t *  io_kcomm;

        input_class_soillayers_t const &  sl;
        substate_physiology_t const *  phys;
        substate_soilchemistry_t const *  soilchem;
        MoBiLE_PlantVegetation *  m_veg;

    private:
        ldndc::sink_handle_t  m_sink;
        ec_daily_accumulated_outputs_t  acc_outputs;
        void  pop_accumulated_outputs();
        void  push_accumulated_outputs();
};
} /*namespace ldndc*/


#undef  LMOD_OUTPUT_MODULE_BASE
#undef  LMOD_OUTPUT_MODULE_NAME
#undef  LMOD_OUTPUT_MODULE_ID
#undef  LMOD_OUTPUT_MODULE_DESC


#endif  /*  !LM_OUTPUT_ECOSYSTEM_DAILY_H_  */

