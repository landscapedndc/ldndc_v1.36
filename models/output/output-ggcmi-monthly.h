/*!
 * @brief
 *    monthly outputs needed for ggcmi project
 *
 * @author
 *    Andrew Smerald
 */

#ifndef  LM_OUTPUT_GGCMI_MONTHLY_H_NEW_
#define  LM_OUTPUT_GGCMI_MONTHLY_H_NEW_

#include  "mbe_legacyoutputmodel.h"
#include  "state/mbe_state.h"

#define  LMOD_OUTPUT_MODULE_BASE  MBE_LegacyOutputModel
#define  LMOD_OUTPUT_MODULE_NAME  OutputGgcmiMonthly
#define  LMOD_OUTPUT_MODULE_ID    "output:ggcmi:monthly"
#define  LMOD_OUTPUT_MODULE_DESC  "GGCMI Monthly Output"
namespace ldndc {
class  substate_microclimate_t;
class  substate_physiology_t;
class  substate_watercycle_t;
class  LDNDC_API  LMOD_OUTPUT_MODULE_NAME  :  public  LMOD_OUTPUT_MODULE_BASE
{
    LMOD_EXPORT_MODULE_INFO(LMOD_OUTPUT_MODULE_NAME,LMOD_OUTPUT_MODULE_ID,LMOD_OUTPUT_MODULE_DESC);
    public:
        LMOD_OUTPUT_MODULE_NAME(
                MoBiLE_State *,
                cbm::io_kcomm_t *,
                timemode_e  _timemode);

        ~LMOD_OUTPUT_MODULE_NAME();


        lerr_t  configure( ldndc::config_file_t const *);

        lerr_t  initialize();

        lerr_t  solve();

        lerr_t  finalize();

        lerr_t  wake() { return  LDNDC_ERR_OK; }
        lerr_t  sleep() { return  LDNDC_ERR_OK; }

    private:
        lerr_t  dump_0(
                ldndc_flt64_t *);

    private:
        cbm::io_kcomm_t *  io_kcomm;

        input_class_soillayers_t const *  soillayers_in;
        substate_soilchemistry_t const *  soilchem;
        substate_watercycle_t const *  water;

    private:
        ldndc::sink_handle_t  m_sink;

        double soil_water_acc;
        int day_counter;
        int month_counter;
        int dmonth_counter;
        void soil_water_calculator();
        void reset_monthly();
};
} /*namespace ldndc*/


#undef  LMOD_OUTPUT_MODULE_BASE
#undef  LMOD_OUTPUT_MODULE_NAME
#undef  LMOD_OUTPUT_MODULE_ID
#undef  LMOD_OUTPUT_MODULE_DESC


#endif  /*  !LM_OUTPUT_SOILCHEMISTRY_DAILY_H_  */
