/*!
 * @brief
 *    output sink test module
 *
 * @author
 *    steffen klatt (created on: nov 03, 2013),
 *    edwin haas
 */

#ifndef  LM_OUTPUT_SINKTEST_H_
#define  LM_OUTPUT_SINKTEST_H_

#include  "mbe_legacyoutputmodel.h"
#include  "state/mbe_state.h"
#include  "io/sink-handle.h"


#define  LMOD_OUTPUT_MODULE_BASE  MBE_LegacyModel
#define  LMOD_OUTPUT_MODULE_NAME  output_sink_test_module_t
#define  LMOD_OUTPUT_MODULE_ID    "output:sinktest:daily"
#define  LMOD_OUTPUT_MODULE_DESC  "module to test sink functionality"
namespace ldndc {
class  LMOD_OUTPUT_MODULE_NAME  :  public  LMOD_OUTPUT_MODULE_BASE
{
    LMOD_EXPORT_MODULE_INFO(LMOD_OUTPUT_MODULE_NAME,LMOD_OUTPUT_MODULE_ID,LMOD_OUTPUT_MODULE_DESC);
    public:
        LMOD_OUTPUT_MODULE_NAME(
                MoBiLE_State *,
                cbm::io_kcomm_t *,
                timemode_e);

        ~LMOD_OUTPUT_MODULE_NAME();


        lerr_t  configure( ldndc::config_file_t const *);

        lerr_t  initialize();

        lerr_t  solve();

        lerr_t  finalize();

        lerr_t  wake() { return  LDNDC_ERR_OK; }
        lerr_t  sleep() { return  LDNDC_ERR_OK; }

    private:
        cbm::io_kcomm_t *  io_comm_;
        sink_handle_t  sink_0a_;
};
} /*namespace ldndc*/


#undef  LMOD_OUTPUT_MODULE_BASE
#undef  LMOD_OUTPUT_MODULE_NAME
#undef  LMOD_OUTPUT_MODULE_ID
#undef  LMOD_OUTPUT_MODULE_DESC


#endif  /*  !LM_OUTPUT_SINKTEST_H_  */

