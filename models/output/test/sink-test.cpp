/*!
 * @brief
 *    output sink test module
 *
 * @author
 *    steffen klatt (created on: nov 03, 2013),
 *    edwin haas
 */

#include  "output/test/sink-test.h"

#include  <string/lstring-basic.h>
#include  <rtconf.h>

#define  LMOD_OUTPUT_MODULE_BASE  MBE_LegacyModel
#define  LMOD_OUTPUT_MODULE_NAME  output_sink_test_module_t
#define  LMOD_OUTPUT_MODULE_TIMEMODE  TMODE_POST_DAILY

LMOD_MODULE_INFO(LMOD_OUTPUT_MODULE_NAME,LMOD_OUTPUT_MODULE_TIMEMODE,LMOD_FLAG_USER|LMOD_FLAG_OUTPUT);
namespace ldndc {

ldndc_output_size_t const  output_sink_test_module_Sizes[] =
{
    1 /*lid_t*/,
    2 /*flt64*/, 1 /*string*/, 1 /*int32*/,

    4 /*total size*/
};
ldndc_output_size_t const *  output_sink_test_module_EntitySizes = NULL;
#define  output_sink_test_module_Rank  ((ldndc_output_rank_t)(sizeof( output_sink_test_module_Sizes) / sizeof( output_sink_test_module_Sizes[0])) - 1)
atomic_datatype_t const  output_sink_test_module_Types[] =
{
    LDNDC_ID, LDNDC_FLOAT64, LDNDC_STRING, LDNDC_INT32
};
ldndc_string_t const  output_sink_test_module_Header[] =
{
    "identifier",
    "h11_flt64", "h12_flt64",
    "h13_string",
    "h14_int32"
};
#define  output_sink_test_module_Datasize  (sizeof( output_sink_test_module_Header) / sizeof( output_sink_test_module_Header[0]))


LMOD_OUTPUT_MODULE_NAME::LMOD_OUTPUT_MODULE_NAME(
        MoBiLE_State *  _state,
        cbm::io_kcomm_t *  _io,
                timemode_e  _timemode)
                : LMOD_OUTPUT_MODULE_BASE( _state, _timemode),

          io_comm_( _io)
{
}


LMOD_OUTPUT_MODULE_NAME::~LMOD_OUTPUT_MODULE_NAME()
{
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::configure(
                ldndc::config_file_t const *  /*_cf*/)
{
    return  LDNDC_ERR_OK;
}

lerr_t
LMOD_OUTPUT_MODULE_NAME::initialize()
{
    this->sink_0a_ = this->io_comm_->sink_handle_acquire( "nullsink");
    if ( this->sink_0a_.status() == LDNDC_ERR_OK)
    {
        lerr_t  rc_layout = LDNDC_OUTPUT_DEFINE_SINK_LAYOUT_NOIDS(this->sink_0a_,output_sink_test_module,RM_CLIENTID|RM_DATETIME);
        RETURN_IF_NOT_OK(rc_layout);
    }
    else
    {
        KLOGERROR( "sink status bad");
        return  this->sink_0a_.status();
    }

    return  LDNDC_ERR_OK;
}

lerr_t
LMOD_OUTPUT_MODULE_NAME::solve()
{
    lid_t  data_lid[1];
    data_lid[0] = this->object_id();

    ldndc_flt64_t  data_flt64[2];
    for ( int  k = 0;  k < output_sink_test_module_Sizes[1];  ++k)
    {
        data_flt64[k] = 1.0 / ((ldndc_flt64_t)(this->lclock()->yearday() + k + 1));
    }
    ldndc_string_t  data_string[1];
    for ( int  k = 0;  k < output_sink_test_module_Sizes[2];  ++k)
    {
        cbm::as_strcpy( &data_string[k], "sinkme");
    }
    ldndc_int32_t  data_int32[1];
    for ( int  k = 0;  k < output_sink_test_module_Sizes[3];  ++k)
    {
        data_int32[k] = LD_RtCfg.rc.pid;
    }

    void *  data_0a[output_sink_test_module_Rank] = { data_lid, data_flt64, data_string, data_int32};

    ldndc::sink_fixed_record_t  rec;
    rec.meta.t.reg.year = this->lclock()->year();
    rec.meta.t.reg.julianday = this->lclock()->yearday();
    sink_client_t  client;
    client.target_id = this->object_id();

    rec.data = data_0a;

    lerr_t  rc_write = this->sink_0a_.write_fixed_record( &client, &rec);
    RETURN_IF_NOT_OK(rc_write);


        return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::finalize()
{
    this->io_comm_->sink_handle_release( &this->sink_0a_);

    return  LDNDC_ERR_OK;
}


#undef  LMOD_OUTPUT_MODULE_BASE
#undef  LMOD_OUTPUT_MODULE_NAME
#undef  LMOD_OUTPUT_MODULE_TIMEMODE

#undef  output_sink_test_module_Rank
#undef  output_sink_test_module_Datasize

} /*namespace ldndc*/

