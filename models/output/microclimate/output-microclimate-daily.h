/*!
 * @brief
 *    dumping modified micro climate related items
 *
 * @author
 *    steffen klatt,
 *    edwin haas
 */

#ifndef  LM_OUTPUT_MICROCLIMATE_DAILY_H_
#define  LM_OUTPUT_MICROCLIMATE_DAILY_H_

#include  "mbe_legacyoutputmodel.h"
#include  "state/mbe_state.h"

#define  LMOD_OUTPUT_MODULE_BASE  MBE_LegacyOutputModel
#define  LMOD_OUTPUT_MODULE_NAME  OutputMicroclimateDaily
#define  LMOD_OUTPUT_MODULE_ID    "output:microclimate:daily"
#define  LMOD_OUTPUT_MODULE_DESC  "Microclimate Daily Output"
namespace ldndc {
class  substate_soilchemistry_t;
class  LDNDC_API  LMOD_OUTPUT_MODULE_NAME  :  public  LMOD_OUTPUT_MODULE_BASE
{
    LMOD_EXPORT_MODULE_INFO(LMOD_OUTPUT_MODULE_NAME,LMOD_OUTPUT_MODULE_ID,LMOD_OUTPUT_MODULE_DESC);
    public:
        LMOD_OUTPUT_MODULE_NAME(
                MoBiLE_State *,
                cbm::io_kcomm_t *,
                timemode_e  _timemode);

        ~LMOD_OUTPUT_MODULE_NAME();


        lerr_t  configure( ldndc::config_file_t const *);

        lerr_t  initialize();

        lerr_t  solve();

        lerr_t  finalize();

        lerr_t  wake() { return  LDNDC_ERR_OK; }
        lerr_t  sleep() { return  LDNDC_ERR_OK; }

    private:
        lerr_t  dump_0(
                ldndc_flt64_t *);

    private:
        cbm::io_kcomm_t *  io_kcomm;

        input_class_soillayers_t const *  soillayers_;
        input_class_setup_t const *  setup_;
        substate_watercycle_t const &  wc_;
        substate_microclimate_t const &  mc_;
        substate_surfacebulk_t const &  sbl_;
        substate_soilchemistry_t const &  sc_;
        MoBiLE_PlantVegetation *  m_veg;

    private:
        ldndc::sink_handle_t  m_sink;
    
        double accumulated_evapotranspiration;
    
//    double accumulated_transpiration;
//    double accumulated_interceptionevaporation;
//    double accumulated_soilevaporation;
//    double accumulated_surfacewaterevaporation;
};
} /*namespace ldndc*/


#undef  LMOD_OUTPUT_MODULE_BASE
#undef  LMOD_OUTPUT_MODULE_NAME
#undef  LMOD_OUTPUT_MODULE_ID
#undef  LMOD_OUTPUT_MODULE_DESC


#endif  /*  !LM_OUTPUT_MICROCLIMATE_DAILY_H_  */

