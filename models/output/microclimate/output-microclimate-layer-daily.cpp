/*!
 * @file
 * @author
 *  - Edwin Haas
 *  - Ruediger Grote
 */

#include  "output/microclimate/output-microclimate-layer-daily.h"

#define  LMOD_OUTPUT_MODULE_BASE  MBE_LegacyOutputModel
#define  LMOD_OUTPUT_MODULE_NAME  OutputMicroclimateLayerDaily

LMOD_MODULE_INFO(LMOD_OUTPUT_MODULE_NAME,TMODE_POST_DAILY,LMOD_FLAG_USER|LMOD_FLAG_OUTPUT);

/*!
 * @page microclimateoutput
 * @section microclimateoutputlayerdaily Microclimate output (layer/daily)
 *
 * <b>xml-based module selection in project's setup:</b>
 * \n\code{.xml}< module id="output:microclimate-layer:daily" />\endcode\n
 *
 * entity name | decription | unit
 * ----------- | ---------- | ----
 * level | Canopy/soil layer | [-]
 * extension | extension | [m]
 * temp\_avg | Temperature | [oC]
 * temp\_max | Maximum temperature | [oC]
 * temp\_min | Minimum temperature | [oC]
 * sw\_rad | Radiation | [Wm-2]
 * vpd | Vapour pressure defficit | [bar]
 * windspeed | Wind speed | [ms-1]
 * sun\_fraction | Sun | [-]
 */
namespace ldndc {


ldndc_string_t const  OutputMicroclimateLayerDaily_Ids[] =
{
    "level",
    "extension",
    "temp_avg",
    "temp_max", 
    "temp_min",
    "sw_rad",
    "vpd",
    "windspeed",
    "sun_fraction"
};
ldndc_string_t const  OutputMicroclimateLayerDaily_Header[] =
{
    "level[m]",
    "extension[m]",
    "temp_avg[oC]",
    "temp_max[oC]", 
    "temp_min[oC]",
    "sw_rad[W m-2]", 
    "vpd[bar]",
    "windspeed[m s-1]",
    "sun_fraction[-]"
};
#define  OutputMicroclimateLayerDaily_Datasize  (sizeof( OutputMicroclimateLayerDaily_Header) / sizeof( OutputMicroclimateLayerDaily_Header[0]))
ldndc_output_size_t const  OutputMicroclimateLayerDaily_Sizes[] =
{
    OutputMicroclimateLayerDaily_Datasize,

    OutputMicroclimateLayerDaily_Datasize /*total size*/
};
ldndc_output_size_t const *  OutputMicroclimateLayerDaily_EntitySizes = NULL;
#define  OutputMicroclimateLayerDaily_Rank  ((ldndc_output_rank_t)(sizeof( OutputMicroclimateLayerDaily_Sizes) / sizeof( OutputMicroclimateLayerDaily_Sizes[0])) - 1)
atomic_datatype_t const  OutputMicroclimateLayerDaily_Types[] =
{
    LDNDC_FLOAT64
};


LMOD_OUTPUT_MODULE_NAME::LMOD_OUTPUT_MODULE_NAME(
        MoBiLE_State *  _state,
        cbm::io_kcomm_t *  _io_kcomm,
        timemode_e  _timemode)
    : LMOD_OUTPUT_MODULE_BASE( _state, _io_kcomm, _timemode),

          io_kcomm( _io_kcomm),
          m_veg( &_state->vegetation),
          soillayers_in( _io_kcomm->get_input_class< input_class_soillayers_t >()),
          setup_( _io_kcomm->get_input_class< input_class_setup_t >()),
          mc( _state->get_substate< substate_microclimate_t >()),
          sc( _state->get_substate< substate_soilchemistry_t >()),
          ph( _state->get_substate< substate_physiology_t >())
{
}


LMOD_OUTPUT_MODULE_NAME::~LMOD_OUTPUT_MODULE_NAME()
{
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::configure(
                ldndc::config_file_t const *  _cf)
{
    lerr_t  rc_setflags =
        this->set_metaflags( _cf, RM_DEFAULT_LAYERDAILY);
    if ( rc_setflags)
        { return  LDNDC_ERR_FAIL; }

    this->f_sel_ = 1; /* 0 = output is first, mid, and last of all foliage layers; 1 = all layers */
    this->s_sel_ = 1; /* 0 = output is first, mid, and last of all soil and litter layers; 1 = all layers */

    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::initialize()
{
    this->m_sink = this->io_kcomm->sink_handle_acquire( "microclimatelayerdaily");
    if ( this->m_sink.status() == LDNDC_ERR_OK)
    {
        lerr_t  rc_layout = LDNDC_OUTPUT_DEFINE_SINK_LAYOUT_DEFAULTS(
            this->m_sink,OutputMicroclimateLayerDaily);
        RETURN_IF_NOT_OK(rc_layout);
    }
    else
    {
        KLOGERROR( "sink status bad  [sink=","microclimatelayerdaily","]");
        return  this->m_sink.status();
    }

    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::solve()
{
    ldndc_flt64_t  data_flt64_0[OutputMicroclimateLayerDaily_Datasize];
    lerr_t  rc_dump = this->dump_0( data_flt64_0);
    if ( rc_dump)
        { return  LDNDC_ERR_FAIL; }

    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::finalize()
{
    this->io_kcomm->sink_handle_release( &this->m_sink);
    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::dump_0( ldndc_flt64_t *  _buf)
{
    double height_at_canopy_start_max( 0.0);
    for( PlantIterator  vt = this->m_veg->begin(); vt != this->m_veg->end(); ++vt)
    {
        MoBiLE_Plant *p = *vt;
        if ( p->height_at_canopy_start > height_at_canopy_start_max)
        {
            height_at_canopy_start_max = p->height_at_canopy_start;
        }
    }

    bool const  h_0 = cbm::flt_greater_zero( this->ph->h_fl[0]);
    size_t const  F_MID = ( h_0) ? (( 0.5 * ( m_veg->canopy_height() + height_at_canopy_start_max)) / this->ph->h_fl[0]) : 0;
    size_t const  F_1 = ( h_0) ? ( height_at_canopy_start_max / this->ph->h_fl[0]) : 0;
    size_t const  CF = std::min( m_veg->canopy_layers_used(), this->ph->h_fl.size());
    ldndc_kassert( CF <= this->ph->h_fl.size());
    size_t const  F = ( CF == 0) ? 0 : ( CF - 1);
    
    {
        // above canopy
        cbm::mem_set( _buf, OutputMicroclimateLayerDaily_Datasize, ldndc::invalid_t< ldndc_flt64_t >::value);
        LDNDC_OUTPUT_SET_COLUMN_INDEX(0);

        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(((CF==0) ? 0.0 : this->ph->h_fl.sum(CF)));
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(0.0);

        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->mc->nd_airtemperature);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->mc->nd_maximumairtemperature);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->mc->nd_minimumairtemperature);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->mc->nd_shortwaveradiation_in);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->mc->nd_watervaporsaturationdeficit);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->mc->nd_windspeed);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(0.0);
        
        void *  data_above_canopy[] = { _buf};
        this->set_layernumber( CF + 1);
        lerr_t  rc_write =
            this->write_fixed_record( &this->m_sink, data_above_canopy);
        if ( rc_write)
            { return  LDNDC_ERR_FAIL; }
    }
    
    // within canopy    
    double  h = ( CF==0) ? 0.0 : this->ph->h_fl.sum(CF);
    for ( size_t  l = 0;  l < CF;  ++l)
    {
        size_t const  fl = F-l;
        
        if (( this->f_sel_ == 1) || (( fl == F_1) || ( fl == F_MID) || ( fl == F)))
        {
            LDNDC_OUTPUT_SET_COLUMN_INDEX(0);
            
            LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(h);
            LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->ph->h_fl[fl]);
            
            LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->mc->nd_temp_fl[fl]);
            LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->mc->nd_tMax_fl[fl]);
            LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->mc->nd_tMin_fl[fl]);
            LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->mc->nd_shortwaveradiation_fl[fl]);
            LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->mc->nd_vpd_fl[fl]);
            LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->mc->nd_win_fl[fl]);
            LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->mc->nd_sunlitfoliagefraction_fl[fl]);
            
            void *  data_within_canopy[] = { _buf};
            this->set_layernumber( fl+1);
            lerr_t  rc_write =
                this->write_fixed_record( &this->m_sink, data_within_canopy);
            if ( rc_write)
                { return  LDNDC_ERR_FAIL; }
        }
        
        h -= this->ph->h_fl[fl];
    }
    
    {
        // below canopy
        cbm::mem_set( _buf, OutputMicroclimateLayerDaily_Datasize, ldndc::invalid_t< ldndc_flt64_t >::value);
        LDNDC_OUTPUT_SET_COLUMN_INDEX(0);
        
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(0.0);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(0.0);
        
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->mc->nd_temp_a);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(ldndc::invalid_t< ldndc_flt64_t >::value);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(ldndc::invalid_t< ldndc_flt64_t >::value);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->mc->nd_rad_a);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(ldndc::invalid_t< ldndc_flt64_t >::value);
        
        void *  data_below_canopy[] = { _buf};
        this->set_layernumber( 0);
        lerr_t  rc_write =
            this->write_fixed_record( &this->m_sink, data_below_canopy);
        if ( rc_write)
            { return  LDNDC_ERR_FAIL; }
    }
    
    // within soil
    size_t const  CS = this->soillayers_in->soil_layer_cnt();
    size_t const  S = ( CS == 0) ? 0 : ( CS - 1);
    size_t const  CL = this->soillayers_in->soil_layers_in_litter_cnt();
    size_t const  S_MID = static_cast< size_t >( 0.5 * ( CS + CL));
    
    cbm::mem_set( _buf, OutputMicroclimateLayerDaily_Datasize, ldndc::invalid_t< ldndc_flt64_t >::value);
    for ( size_t  sl = 0;  sl < CS;  ++sl)
    {        
        if (( this->s_sel_ == 1) || (( sl == 0) || ( sl == CL) || ( sl == S_MID) || ( sl == S)))
        {
            LDNDC_OUTPUT_SET_COLUMN_INDEX(0);
            
            LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(-this->sc->depth_sl[sl]);
            LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->sc->h_sl[sl]);
            
            LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->mc->nd_temp_sl[sl]);
            
            void *  data_within_soil[] = { _buf};
            this->set_layernumber( -( static_cast< int >( sl+1)));
            lerr_t  rc_write =
                this->write_fixed_record( &this->m_sink, data_within_soil);
            if ( rc_write)
                { return  LDNDC_ERR_FAIL; }
        }
    }
    
    return  LDNDC_ERR_OK;
}


#undef  LMOD_OUTPUT_MODULE_BASE
#undef  LMOD_OUTPUT_MODULE_NAME

#undef  OutputMicroclimateLayerDaily_Rank
#undef  OutputMicroclimateLayerDaily_Datasize

} /*namespace ldndc*/

