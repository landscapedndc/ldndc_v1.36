/*!
 * @file
 * @author
 *    David Kraus
 */

#include  "output/microclimate/output-microclimate-subdaily.h"
#include  "scientific/meteo/ld_meteo.h"

#define  LMOD_OUTPUT_MODULE_BASE  MBE_LegacyOutputModel
#define  LMOD_OUTPUT_MODULE_NAME  OutputMicroclimateSubdaily
#define  LMOD_OUTPUT_MODULE_TIMEMODE  TMODE_SUBDAILY

LMOD_MODULE_INFO(LMOD_OUTPUT_MODULE_NAME,LMOD_OUTPUT_MODULE_TIMEMODE,LMOD_FLAG_USER|LMOD_FLAG_OUTPUT);

/*!
 * @page microclimateoutput
 * @section microclimateoutputsubdaily Microclimate output (subdaily)
 *
 * <b>xml-based module selection in project's setup:</b>
 * \n\code{.xml}< module id="output:microclimate:subdaily" />\endcode\n
 *
 * entity name | decription | unit
 * ----------- | ---------- | ----
 * sw\_rad\_above\_canopy\_in | Incoming short wave radiation above canopy (directed downwards) | [Wm-2]
 * sw\_rad\_above\_canopy\_out | Outgoing short wave radiation above canopy (directed upwards) | [Wm-2]
 * lw\_rad\_above\_canopy\_in | Incoming long wave radiation above canopy (directed downwards) | [Wm-2]
 * lw\_rad\_above\_canopy\_out | Outgoing long wave radiation above canopy (directed upwards) | [Wm-2]
 * latent\_heat\_out\_top | Latent heat exchange at top of canopy (directed upwards) | [Wm-2]
 * latent\_heat\_out\_bottom | Latent heat exchange at botom of soil profile (directed downwards) | [Wm-2]
 * sensible\_heat\_out\_top | Sensible heat exchange at top of canopy (directed upwards) | [Wm-2]
 * sensible\_heat\_out\_bottom | Sensible heat exchange at botom of soil profile (directed downwards) | [Wm-2]
 * energy | Energy content of total ecosystem | [MJm-2]
 * temp\_above\_canopy_avg | Mean temperature above canopy | [oC]
 * temp\_above\_canopy_min | Minimum temperature above canopy | [oC]
 * temp\_above\_canopy_max | Maximum temperature above canopy | [oC]
 * temp\_canopy\_top | Temperature at top of canopy | [oC]
 * temp\_canopy\_middle | Temperature at middle of canopy | [oC]
 * temp\_canopy\_bottom | Temperature at bottom of canopy | [oC]
 * temp\_soil\_surface | Temperature at soil surface | [oC]
 * temp\_5cm | Temperature at 5 cm depth | [oC]
 * temp\_10cm | Temperature at 10 cm depth | [oC]
 * temp\_15cm | Temperature at 15 cm depth | [oC]
 * temp\_20cm | Temperature at 20 cm depth | [oC]
 * temp\_30cm | Temperature at 30 cm depth | [oC]
 * temp\_50cm | Temperature at 50 cm depth | [oC]
 * temp\_100cm | Temperature at 100 cm depth | [oC]
 */
namespace ldndc
{

ldndc_string_t const  OutputMicroclimateSubdaily_Ids[] =
{
    "sw_rad_above_canopy_in",
    "sw_rad_above_canopy_out",
    "lw_rad_above_canopy_in",
    "lw_rad_above_canopy_out",
    "latent_heat_out_top",
    "latent_heat_out_bottom",
    "sensible_heat_out_top",
    "sensible_heat_out_bottom",
    "energy",
    "temp_above_canopy_avg",
    "temp_above_canopy_min",
    "temp_above_canopy_max",
    "temp_canopy_top",
    "temp_canopy_middle",
    "temp_canopy_bottom",
    "temp_surfacewater",
    "temp_soil_surface",
    "temp_5cm",
    "temp_10cm",
    "temp_15cm",
    "temp_20cm",
    "temp_30cm",
    "temp_50cm",
    "temp_100cm",
    "vpd"
};

ldndc_string_t const  OutputMicroclimateSubdaily_Header[] =
{
    "sw_radiation_above_canopy_in[Wm-2]",
    "sw_radiation_above_canopy_out[Wm-2]",
    "lw_radiation_above_canopy_in[Wm-2]",
    "lw_radiation_above_canopy_out[Wm-2]",
    "latent_heat_out_top[Wm-2]",
    "latent_heat_out_bottom[Wm-2]",
    "sensible_heat_out_top[Wm-2]",
    "sensible_heat_out_bottom[Wm-2]",
    "energy[Jm-2]",
    "temp_above_canopy_avg[oC]",
    "temp_above_canopy_min[oC]",
    "temp_above_canopy_max[oC]",
    "temp_canopy_top[oC]",
    "temp_canopy_middle[oC]",
    "temp_canopy_bottom[oC]",
    "temp_surfacewater[oC]",
    "temp_soil_surface[oC]",
    "temp_5cm[oC]",
    "temp_10cm[oC]",
    "temp_15cm[oC]",
    "temp_20cm[oC]",
    "temp_30cm[oC]",
    "temp_50cm[oC]",
    "temp_100cm[oC]",
    "vpd"
};


#define  OutputMicroclimateSubdaily_Datasize  (sizeof( OutputMicroclimateSubdaily_Header) / sizeof( OutputMicroclimateSubdaily_Header[0]))
ldndc_output_size_t const  OutputMicroclimateSubdaily_Sizes[] =
{
    OutputMicroclimateSubdaily_Datasize,

    OutputMicroclimateSubdaily_Datasize /*total size*/
};


ldndc_output_size_t const *  OutputMicroclimateSubdaily_EntitySizes = NULL;
#define  OutputMicroclimateSubdaily_Rank  ((ldndc_output_rank_t)(sizeof( OutputMicroclimateSubdaily_Sizes) / sizeof( OutputMicroclimateSubdaily_Sizes[0])) - 1)
atomic_datatype_t const  OutputMicroclimateSubdaily_Types[] =
{
    LDNDC_FLOAT64
};


LMOD_OUTPUT_MODULE_NAME::LMOD_OUTPUT_MODULE_NAME(
        MoBiLE_State *  _state,
        cbm::io_kcomm_t *  _io_kcomm,
        timemode_e  _timemode)
        : LMOD_OUTPUT_MODULE_BASE( _state, _io_kcomm, _timemode),

          io_kcomm( _io_kcomm),

          soillayers_( _io_kcomm->get_input_class< input_class_soillayers_t >()),
          setup_( _io_kcomm->get_input_class< input_class_setup_t >()),
          wc_( _state->get_substate_ref< substate_watercycle_t >()),
          mc_( _state->get_substate_ref< substate_microclimate_t >()),
          sbl_( _state->get_substate_ref< substate_surfacebulk_t >()),
          sc_( _state->get_substate_ref< substate_soilchemistry_t >()),
          m_veg( &_state->vegetation)
{
    accumulated_evapotranspiration = 0.0;
}


LMOD_OUTPUT_MODULE_NAME::~LMOD_OUTPUT_MODULE_NAME()
{}


lerr_t
LMOD_OUTPUT_MODULE_NAME::configure(
                ldndc::config_file_t const *  _cf)
{
    lerr_t  rc_setflags = this->set_metaflags( _cf);
    if ( rc_setflags)
        { return  LDNDC_ERR_FAIL; }
    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::initialize()
{
    this->m_sink = this->io_kcomm->sink_handle_acquire( "microclimatesubdaily");
    if ( this->m_sink.status() == LDNDC_ERR_OK)
    {
        lerr_t  rc_layout = LDNDC_OUTPUT_DEFINE_SINK_LAYOUT_DEFAULTS(
            this->m_sink,OutputMicroclimateSubdaily);
        RETURN_IF_NOT_OK(rc_layout);
    }
    else
    {
        KLOGERROR( "sink status bad  [sink=","microclimatesubdaily","]");
        return  this->m_sink.status();
    }

    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::solve()
{
    ldndc_flt64_t  data_flt64_0[OutputMicroclimateSubdaily_Datasize];
        lerr_t  rc_dump = dump_0( data_flt64_0);
    RETURN_IF_NOT_OK(rc_dump);

    void *  data[] = { data_flt64_0};
    lerr_t  rc_write =
        this->write_fixed_record( &this->m_sink, data);
    if ( rc_write)
        { return  LDNDC_ERR_FAIL; }

    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::finalize()
{
    this->io_kcomm->sink_handle_release( &this->m_sink);
    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::dump_0(
                                ldndc_flt64_t *  _buf)
{
    size_t const  CS = this->soillayers_->soil_layer_cnt();

    static const size_t  sizeD = 7;
    static const double  D[sizeD] = { 0.05, 0.1, 0.15, 0.2, 0.3, 0.5, 1.0};
    static const double  iv = ldndc::invalid_t< double >::value;
    double  soil_temp_at[sizeD] = { iv, /*iv,*/ iv, iv};
    for ( size_t i = 0;  i < sizeD;  ++i)
    {
        soil_temp_at[i] = ldndc::invalid_t< ldndc_flt64_t >::value;
    }

    // soil temperature at specified depth
    size_t  d = 0;
    double  h = 0.0;
    for ( size_t  sl = 0;  ( d < sizeD) && ( sl < CS);  ++sl)
    {
        h += this->sc_.h_sl[sl];
        if ( h >= ( D[d] - 1.0e-06))
        {
            soil_temp_at[d] = this->mc_.temp_sl[sl];
            ++d;
        }
    }

    double vpd_avg( 0.0);
    double lai_sum( 0.0);
    for ( size_t fl = 0; fl < setup_->canopylayers(); ++fl)
    {
        double const lai_fl( m_veg->lai_fl( fl));
        lai_sum += lai_fl;
        vpd_avg += mc_.vpd_fl[fl] * lai_fl;
    }
    vpd_avg /= lai_sum;

    double temp_surfacewater( invalid_dbl);
    if ( cbm::flt_greater( wc_.surface_water + wc_.surface_ice, 0.01))
    {
        temp_surfacewater = 0.0;
        for (size_t l = 0; l < sbl_.surfacebulk_layer_cnt(); ++l)
        {
            temp_surfacewater += sbl_.temp_sbl[l];
        }
        temp_surfacewater /= sbl_.surfacebulk_layer_cnt();
        temp_surfacewater = sbl_.temp_sbl[0];
    }
    
    double const accumulated_evapotranspiration_new =  wc_.accumulated_transpiration_sl.sum()
                                                     + wc_.accumulated_interceptionevaporation
                                                     + wc_.accumulated_soilevaporation
                                                     + wc_.accumulated_surfacewaterevaporation;
    
    double evapo( (accumulated_evapotranspiration_new - accumulated_evapotranspiration) * cbm::MM_IN_M * cbm::LAMBDA * cbm::J_IN_MJ / cbm::SEC_IN_DAY);

    size_t fl_cnt( m_veg->canopy_layers_used());

    int fl_top( fl_cnt-1);
    int fl_middle( fl_top / 2);
    int fl_bottom( 0);
    
    LDNDC_OUTPUT_SET_COLUMN_INDEX(0);

    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(mc_.shortwaveradiation_in);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(mc_.shortwaveradiation_out);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(mc_.longwaveradiation_in);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(mc_.longwaveradiation_out);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(evapo);   //latent_heat_out_top
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(0.0);     //latent_heat_out_bottom
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(mc_.sensible_heat_out);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(0.0);     //sensible_heat_out_bottom
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(mc_.heat_content*1.0e-6); //from Joule to Megajoule
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->mc_.ts_airtemperature);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->mc_.ts_minimumairtemperature);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->mc_.ts_maximumairtemperature);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->mc_.temp_fl[fl_top]);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->mc_.temp_fl[fl_middle]);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->mc_.temp_fl[fl_bottom]);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(temp_surfacewater);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->mc_.temp_a);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(soil_temp_at[0]);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(soil_temp_at[1]);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(soil_temp_at[2]);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(soil_temp_at[3]);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(soil_temp_at[4]);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(soil_temp_at[5]);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(soil_temp_at[6]);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(vpd_avg);

    accumulated_evapotranspiration = accumulated_evapotranspiration_new;

    return  LDNDC_ERR_OK;
}

#undef  LMOD_OUTPUT_MODULE_BASE
#undef  LMOD_OUTPUT_MODULE_NAME

#undef  OutputMicroclimateSubdaily_Rank
#undef  OutputMicroclimateSubdaily_Datasize

} /*namespace ldndc*/
