/*!
 * @brief
 *    seasonal ggcmi output
 *    accumulate daily values over the growing season
 *    assumes only a single crop is planted (standard set-up for ggcmi)
 *    not very efficient (too many if statements in solve function), but I don't think it appreciably slows down ldndc
 *
 * @author
 *    Andrew Smerald
 */

#include  "output/output-ggcmi-seasonal.h"

#include  "constants/lconstants-plant.h"
#include  "constants/lconstants-conv.h"

#define  LMOD_OUTPUT_MODULE_BASE  MBE_LegacyOutputModel
#define  LMOD_OUTPUT_MODULE_NAME  OutputGgcmiSeasonal

LMOD_MODULE_INFO(LMOD_OUTPUT_MODULE_NAME,TMODE_POST_DAILY,LMOD_FLAG_USER|LMOD_FLAG_OUTPUT);
namespace ldndc {

#define  OutputGgcmiSeasonal_Item__dvs
#define  OutputGgcmiSeasonal_Item__gdd
#define  OutputGgcmiSeasonal_Item__plantyear
#define  OutputGgcmiSeasonal_Item__plantyearday
#define  OutputGgcmiSeasonal_Item__anthesisday_from_planting
#define  OutputGgcmiSeasonal_Item__harvestday_from_planting

#define  OutputGgcmiSeasonal_Item__CH4
#define  OutputGgcmiSeasonal_Item__CEMIS

#define  OutputGgcmiSeasonal_Item__NLEACH
#define  OutputGgcmiSeasonal_Item__N2O
#define  OutputGgcmiSeasonal_Item__N2
#define  OutputGgcmiSeasonal_Item__NOUT
#define  OutputGgcmiSeasonal_Item__NIN
#define  OutputGgcmiSeasonal_Item__NUPT

#define  OutputGgcmiSeasonal_Item_AET // actual evapo-transpiration
#define  OutputGgcmiSeasonal_Item_transp //transpiration
#define  OutputGgcmiSeasonal_Item_evap /*soil and surface water evaporation*/
#define  OutputGgcmiSeasonal_Item_runoff /*surface runoff*/
#define  OutputGgcmiSeasonal_Item_irri /*irrigation*/

/*!
 * @page ggcmioutput
 * @section ggcmioutputseasonal Seasonal GGCMI output
 *
 * entity name | decription | unit
 * ----------- | ---------- | ----
 * dvs | fractional maturity | -
 * planting_year | year of planting | -
 * planting_day | day of year of planting | -
 * anthesisday_from_planting | number of days after plarnting of flowering (anthesis) | -
 * harvestday_from_planting | number of days after plarnting of harvesting | -
 * dC_ch4_emis | Seasonal methane flux | [kgCha-1]
 * dC_emis | Seasonal flux of CH4 and CO2 | [kgCha-1]
 * dN_leach | Seasonal leaching of nitrate, ammonium and dissolved organic nitrogen |  [kgNha-1]
 * dN_n2o_emis | Seasonal nitrous oxide flux | [kgNha-1]
 * dN_n2_emis | Seasonal molecular nitrogen flux | [kgNha-1]
 * dN_out | Loss of reactive nitrogen due to leaching (NO3,NH4,DoN) and emission (N20, NO, N2, NH3)  | [kgNha-1]
 * dN_in| N input from deposition (NH4, NO3) and N2 fixation | [kgNha-1]
 * dN_up | Seasonal nitrogen uptake by plants from the litter layer and mineral soil | [kgNha-1]
 * AET| Seasonal sum of evaporation (soil, surface and from interception) and transpiration | [mm]
 * transp | Seasonal transpiration | [mm]
 * evap | Seasonal evaporation from soil and surface water (not from interception) | [mm]
 * runoff | Seasonal lateral run-off | [mm]
 * irri | Seasonal addition of irrigation water | [mm]
 */


ldndc_string_t const  OutputGgcmiSeasonal_Ids[] =
{
#ifdef  OutputGgcmiSeasonal_Item__dvs
    "dvs",
#endif
#ifdef  OutputGgcmiSeasonal_Item__gdd
    "acc_gdd",
#endif
#ifdef  OutputGgcmiSeasonal_Item__plantyear
    "planting_year",
#endif
#ifdef  OutputGgcmiSeasonal_Item__plantyearday
    "planting_day",
#endif
#ifdef  OutputGgcmiSeasonal_Item__anthesisday_from_planting
    "anthesis_day",
#endif
#ifdef  OutputGgcmiSeasonal_Item__harvestday_from_planting
    "harvest_day",
#endif
#ifdef  OutputGgcmiSeasonal_Item__CH4
    "dC_ch4_emis",
#endif
#ifdef  OutputGgcmiSeasonal_Item__CEMIS
    "dC_emis",
#endif
#ifdef  OutputGgcmiSeasonal_Item__NLEACH
    "dN_leach",
#endif
#ifdef  OutputGgcmiSeasonal_Item__N2O
    "dN_n2o_emis",
#endif
#ifdef  OutputGgcmiSeasonal_Item__N2
    "dN_n2_emis",
#endif
#ifdef  OutputGgcmiSeasonal_Item__NOUT
    "dN_out",
#endif
#ifdef  OutputGgcmiSeasonal_Item__NIN
    "dN_in",
#endif
#ifdef  OutputGgcmiSeasonal_Item__NUPT
    "dN_up",
#endif
#ifdef  OutputGgcmiSeasonal_Item_AET
    "AET",
#endif
#ifdef  OutputGgcmiSeasonal_Item_transp
    "transp",
#endif
#ifdef  OutputGgcmiSeasonal_Item_evap
    "evap",
#endif
#ifdef  OutputGgcmiSeasonal_Item_runoff
    "runoff",
#endif
#ifdef  OutputGgcmiSeasonal_Item_irri
    "irri",
#endif
};

ldndc_string_t const  OutputGgcmiSeasonal_Header[] =
{
#ifdef  OutputGgcmiSeasonal_Item__dvs
    "dvs",
#endif
#ifdef  OutputGgcmiSeasonal_Item__gdd
    "acc_gdd",
#endif
#ifdef  OutputGgcmiSeasonal_Item__plantyear
    "planting_year",
#endif
#ifdef  OutputGgcmiSeasonal_Item__plantyearday
    "planting_day",
#endif
#ifdef  OutputGgcmiSeasonal_Item__anthesisday_from_planting
    "anthesis_day",
#endif
#ifdef  OutputGgcmiSeasonal_Item__harvestday_from_planting
    "harvest_day",
#endif
#ifdef  OutputGgcmiSeasonal_Item__CH4
    "dC_ch4_emis[kgCha-1]",
#endif
#ifdef  OutputGgcmiSeasonal_Item__CEMIS
    "dC_emis[kgCha-1]",
#endif
#ifdef  OutputGgcmiSeasonal_Item__NLEACH
    "dN_leach[kgNha-1]",
#endif
#ifdef  OutputGgcmiSeasonal_Item__N2O
    "dN_n2o_emis[kgNha-1]",
#endif
#ifdef  OutputGgcmiSeasonal_Item__N2
    "dN_n2_emis[kgNha-1]",
#endif
#ifdef  OutputGgcmiSeasonal_Item__NOUT
    "dN_out[kgNha-1]",
#endif
#ifdef  OutputGgcmiSeasonal_Item__NIN
    "dN_in[kgNha-1]",
#endif
#ifdef  OutputGgcmiSeasonal_Item__NUPT
    "dN_up[kgNha-1]",
#endif
#ifdef  OutputGgcmiSeasonal_Item_AET
    "AET[mm]",
#endif
#ifdef  OutputGgcmiSeasonal_Item_transp
    "transp[mm]",
#endif
#ifdef  OutputGgcmiSeasonal_Item_evap
    "evap[mm]",
#endif
#ifdef  OutputGgcmiSeasonal_Item_runoff
    "runoff[mm]",
#endif
#ifdef  OutputGgcmiSeasonal_Item_irri
    "irri[mm]",
#endif
};

#define  OutputGgcmiSeasonal_Datasize  (sizeof( OutputGgcmiSeasonal_Header) / sizeof( OutputGgcmiSeasonal_Header[0]))
ldndc_output_size_t const  OutputGgcmiSeasonal_Sizes[] =
{
    OutputGgcmiSeasonal_Datasize,

    OutputGgcmiSeasonal_Datasize /*total size*/
};
ldndc_output_size_t const *  OutputGgcmiSeasonal_EntitySizes = NULL;
#define  OutputGgcmiSeasonal_Rank  ((ldndc_output_rank_t)(sizeof( OutputGgcmiSeasonal_Sizes) / sizeof( OutputGgcmiSeasonal_Sizes[0])) - 1)
atomic_datatype_t const  OutputGgcmiSeasonal_Types[] =
{
    LDNDC_FLOAT64
};


LMOD_OUTPUT_MODULE_NAME::LMOD_OUTPUT_MODULE_NAME(
          MoBiLE_State *  _state,
          cbm::io_kcomm_t *  _io_kcomm,
          timemode_e  _timemode)
          : LMOD_OUTPUT_MODULE_BASE( _state, _io_kcomm, _timemode),

          io_kcomm( _io_kcomm),

          soillayers_in( _io_kcomm->get_input_class< input_class_soillayers_t >()),

          phys( _state->get_substate< substate_physiology_t >()),
          soilchem( _state->get_substate< substate_soilchemistry_t >()),
          water( _state->get_substate< substate_watercycle_t >()),
          m_veg( &_state->vegetation)
{
    push_accumulated_outputs();
    plant_flag = m_veg->size();
    dplant_flag = 0;
    planting_year = -1;
    planting_yearday = -1;
    anthesis_yearday = -1;
    harvest_yearday = -1;

    dvs_val = 0.0;
    gdd_val = 0.0;
}


LMOD_OUTPUT_MODULE_NAME::~LMOD_OUTPUT_MODULE_NAME()
{
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::configure(
                ldndc::config_file_t const *  _cf)
{
    lerr_t  rc_setflags =
        set_metaflags( _cf, RM_DEFAULT_DAILY);
    if ( rc_setflags)
        { return  LDNDC_ERR_FAIL; }

    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::initialize()
{
    m_sink = io_kcomm->sink_handle_acquire( "ggcmiseasonal");
    if ( m_sink.status() == LDNDC_ERR_OK)
    {
        lerr_t  rc_layout = LDNDC_OUTPUT_DEFINE_SINK_LAYOUT_DEFAULTS(
            m_sink,OutputGgcmiSeasonal);
        RETURN_IF_NOT_OK(rc_layout);
    }
    else
    {
        KLOGERROR( "sink status bad  [sink=","ggcmiseasonal","]");
        return  m_sink.status();
    }

    push_accumulated_outputs();
    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::solve()
{
    ldndc_flt64_t  data_flt64_0[OutputGgcmiSeasonal_Datasize];
    //update daily values of fluxes
    pop_accumulated_outputs();
    //detects changes in plant flag
    dplant_flag = m_veg->size() - plant_flag;
    //flag for presence/absence of plant
    plant_flag = m_veg->size();

    //detect planting event and reset accumulators
    if(dplant_flag == 1)
    {
      planting_yearday = lclock_ref().yearday();
      planting_year = lclock_ref().year();
      reset_seasonal_outputs();
    }

    //accumulate daily fluxes into a seasonal output
    if(plant_flag == 1)
    {
      update_seasonal_outputs();

      double dvs_test_val = (*m_veg->begin())->dvsFlush;
      if(dvs_test_val > dvs_val)
      {
        dvs_val = dvs_test_val;
      }
      double gdd_test_val = (*m_veg->begin())->growing_degree_days;
      if(gdd_test_val > gdd_val)
      {
        gdd_val = gdd_test_val;
      }
      //inefficient way to determine anthesis date
      if( gdd_val  <  (*(*m_veg->begin()))->GDD_FLOWERING() )
      {
        anthesis_yearday = lclock_ref().yearday();
      }
    }

    //at harvest write seasonal flux output
    if(dplant_flag == -1)
    {
      harvest_yearday = lclock_ref().yearday();

      lerr_t  rc_dump = dump_0( data_flt64_0);
      if ( rc_dump)
        { return  LDNDC_ERR_FAIL; }

      void *  data[] = { data_flt64_0};
      lerr_t  rc_write =
      write_fixed_record( &m_sink, data);
      if ( rc_write)
        { return  LDNDC_ERR_FAIL; }
    }

    //reset daily accumulation
    push_accumulated_outputs();
    return  LDNDC_ERR_OK;
}


void
LMOD_OUTPUT_MODULE_NAME::pop_accumulated_outputs()
{
    acc.nh3_uptake = phys->accumulated_nh3_uptake_sl.sum() - acc.nh3_uptake;
    acc.nh4_uptake = phys->accumulated_nh4_uptake_sl.sum() - acc.nh4_uptake;
    acc.no3_uptake = phys->accumulated_no3_uptake_sl.sum() - acc.no3_uptake;
    acc.don_uptake = phys->accumulated_don_uptake_sl.sum() - acc.don_uptake;
    
    acc.nh4_throughfall =
               phys->accumulated_nh4_throughfall - acc.nh4_throughfall;
    acc.no3_throughfall =
               phys->accumulated_no3_throughfall - acc.no3_throughfall;

    acc.don_leach =
               soilchem->accumulated_don_leach - acc.don_leach;
    acc.no3_leach =
               soilchem->accumulated_no3_leach - acc.no3_leach;
    acc.nh4_leach =
               soilchem->accumulated_nh4_leach - acc.nh4_leach;

    acc.ch4_emis =
               soilchem->accumulated_ch4_emis - acc.ch4_emis;
    acc.co2_emis_auto =
               soilchem->accumulated_co2_emis_auto - acc.co2_emis_auto;
    acc.co2_emis_hetero =
               soilchem->accumulated_co2_emis_hetero - acc.co2_emis_hetero;
    acc.no_emis =
               soilchem->accumulated_no_emis - acc.no_emis;
    acc.n2o_emis =
               soilchem->accumulated_n2o_emis - acc.n2o_emis;
    acc.n2_emis =
               soilchem->accumulated_n2_emis - acc.n2_emis;
    acc.nh3_emis =
               soilchem->accumulated_nh3_emis - acc.nh3_emis;

    acc.n2_fix_algae =
               soilchem->accumulated_n_fix_algae - acc.n2_fix_algae;
    acc.n2_fix_bio =
               soilchem->accumulated_n2_fixation - acc.n2_fix_bio;

    acc.transpiration =
               water->accumulated_transpiration_sl.sum() - acc.transpiration;
    acc.interceptionevaporation =
               water->accumulated_interceptionevaporation - acc.interceptionevaporation;
    acc.soilevaporation =
               water->accumulated_soilevaporation - acc.soilevaporation;
    acc.surfacewaterevaporation =
               water->accumulated_surfacewaterevaporation - acc.surfacewaterevaporation;
    acc.runoff =
               water->accumulated_runoff - acc.runoff;
    acc.percolation =
               water->accumulated_percolation - acc.percolation;
    acc.irrigation =
               water->accumulated_irrigation_ggcmi + water->accumulated_irrigation + water->accumulated_irrigation_automatic + water->accumulated_irrigation_reservoir_withdrawal - acc.irrigation;
}


void
LMOD_OUTPUT_MODULE_NAME::push_accumulated_outputs()
{
    acc.nh3_uptake = phys->accumulated_nh3_uptake_sl.sum();
    acc.nh4_uptake = phys->accumulated_nh4_uptake_sl.sum();
    acc.no3_uptake = phys->accumulated_no3_uptake_sl.sum();
    acc.don_uptake = phys->accumulated_don_uptake_sl.sum();

    acc.nh4_throughfall = phys->accumulated_nh4_throughfall;
    acc.no3_throughfall = phys->accumulated_no3_throughfall;

    acc.don_leach = soilchem->accumulated_don_leach;
    acc.no3_leach = soilchem->accumulated_no3_leach;
    acc.nh4_leach = soilchem->accumulated_nh4_leach;

    acc.ch4_emis = soilchem->accumulated_ch4_emis;
    acc.co2_emis_auto = soilchem->accumulated_co2_emis_auto;
    acc.co2_emis_hetero = soilchem->accumulated_co2_emis_hetero;
    acc.no_emis = soilchem->accumulated_no_emis;
    acc.n2o_emis = soilchem->accumulated_n2o_emis;
    acc.n2_emis = soilchem->accumulated_n2_emis;
    acc.nh3_emis = soilchem->accumulated_nh3_emis;

    acc.n2_fix_algae = soilchem->accumulated_n_fix_algae;
    acc.n2_fix_bio = soilchem->accumulated_n2_fixation;

    acc.transpiration = water->accumulated_transpiration_sl.sum();
    acc.interceptionevaporation = water->accumulated_interceptionevaporation;
    acc.soilevaporation = water->accumulated_soilevaporation;
    acc.surfacewaterevaporation = water->accumulated_surfacewaterevaporation;
    acc.runoff = water->accumulated_runoff;
    acc.percolation = water->accumulated_percolation;
    acc.irrigation = water->accumulated_irrigation_ggcmi + water->accumulated_irrigation + water->accumulated_irrigation_automatic + water->accumulated_irrigation_reservoir_withdrawal;
}

void
LMOD_OUTPUT_MODULE_NAME::reset_seasonal_outputs()
{
    acc_seas.nh3_uptake = 0.0;
    acc_seas.nh4_uptake = 0.0;
    acc_seas.no3_uptake = 0.0;
    acc_seas.don_uptake = 0.0;
    
    acc_seas.nh4_throughfall = 0.0;
    acc_seas.no3_throughfall = 0.0;

    acc_seas.don_leach = 0.0;
    acc_seas.no3_leach = 0.0;
    acc_seas.nh4_leach = 0.0;

    acc_seas.ch4_emis = 0.0;
    acc_seas.co2_emis_auto = 0.0;
    acc_seas.co2_emis_hetero = 0.0;
    acc_seas.no_emis = 0.0;
    acc_seas.n2o_emis = 0.0;
    acc_seas.n2_emis = 0.0;
    acc_seas.nh3_emis = 0.0;

    acc_seas.n2_fix_algae = 0.0;
    acc_seas.n2_fix_bio = 0.0;
    acc_seas.n2_fix_plant = 0.0;

    acc_seas.transpiration = 0.0;
    acc_seas.interceptionevaporation = 0.0;
    acc_seas.soilevaporation = 0.0;
    acc_seas.surfacewaterevaporation = 0.0;
    acc_seas.runoff = 0.0;
    acc_seas.percolation = 0.0;
    acc_seas.irrigation = 0.0;

    dvs_val = 0.0;
    gdd_val = 0.0;
}




void
LMOD_OUTPUT_MODULE_NAME::update_seasonal_outputs()
{
    acc_seas.nh3_uptake = acc.nh3_uptake;
    acc_seas.nh4_uptake = acc.nh4_uptake;
    acc_seas.no3_uptake = acc.no3_uptake;
    acc_seas.don_uptake = acc.don_uptake;
    
    acc_seas.nh4_throughfall += acc.nh4_throughfall;
    acc_seas.no3_throughfall += acc.no3_throughfall;

    acc_seas.don_leach += acc.don_leach;
    acc_seas.no3_leach += acc.no3_leach;
    acc_seas.nh4_leach += acc.nh4_leach;

    acc_seas.ch4_emis += acc.ch4_emis;
    acc_seas.co2_emis_auto += acc.co2_emis_auto;
    acc_seas.co2_emis_hetero += acc.co2_emis_hetero;
    acc_seas.no_emis += acc.no_emis;
    acc_seas.n2o_emis += acc.n2o_emis;
    acc_seas.n2_emis += acc.n2_emis;
    acc_seas.nh3_emis += acc.nh3_emis;

    acc_seas.n2_fix_algae += acc.n2_fix_algae;
    acc_seas.n2_fix_bio += acc.n2_fix_bio;
    acc_seas.n2_fix_plant += (*m_veg->begin())->d_n2_fixation;

    acc_seas.transpiration += acc.transpiration;
    acc_seas.interceptionevaporation += acc.interceptionevaporation;
    acc_seas.soilevaporation += acc.soilevaporation;
    acc_seas.surfacewaterevaporation += acc.surfacewaterevaporation;
    acc_seas.runoff += acc.runoff;
    acc_seas.percolation += acc.percolation;
    acc_seas.irrigation += acc.irrigation;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::finalize()
{
    io_kcomm->sink_handle_release( &m_sink);
    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::dump_0(
        ldndc_flt64_t *  _buf)
{

//determine number of days between planting and anthesis
int anthesisday_from_planting = 0;
if(anthesis_yearday > planting_yearday)
{
  anthesisday_from_planting = anthesis_yearday - planting_yearday;
}
else
{
  anthesisday_from_planting = 365 + anthesis_yearday - planting_yearday;
}

int harvestday_from_planting = 0;
if(harvest_yearday > planting_yearday)
{
  harvestday_from_planting = harvest_yearday - planting_yearday;
}
else
{
  harvestday_from_planting = 365 + harvest_yearday - planting_yearday;
}


    LDNDC_OUTPUT_SET_COLUMN_INDEX(0);

#ifdef  OutputGgcmiSeasonal_Item__dvs
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( dvs_val );
#endif
#ifdef  OutputGgcmiSeasonal_Item__gdd
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( gdd_val );
#endif
#ifdef  OutputGgcmiSeasonal_Item__plantyear
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(planting_year);
#endif
#ifdef  OutputGgcmiSeasonal_Item__plantyearday
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(planting_yearday);
#endif
#ifdef  OutputGgcmiSeasonal_Item__anthesisday_from_planting
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( anthesisday_from_planting);
#endif
#ifdef  OutputGgcmiSeasonal_Item__harvestday_from_planting
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(harvestday_from_planting);
#endif
#ifdef  OutputGgcmiSeasonal_Item__CH4
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(cbm::M2_IN_HA * acc_seas.ch4_emis);
#endif
#ifdef  OutputGgcmiSeasonal_Item__CEMIS
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(cbm::M2_IN_HA * ( acc_seas.co2_emis_auto + acc_seas.co2_emis_hetero + acc_seas.ch4_emis));
#endif
#ifdef  OutputGgcmiSeasonal_Item__NLEACH
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(cbm::M2_IN_HA * ( acc_seas.don_leach + acc_seas.no3_leach + acc_seas.nh4_leach));
#endif
#ifdef  OutputGgcmiSeasonal_Item__N2O
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(cbm::M2_IN_HA * acc_seas.n2o_emis);
#endif
#ifdef  OutputGgcmiSeasonal_Item__N2
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(cbm::M2_IN_HA * acc_seas.n2_emis);
#endif
#ifdef  OutputGgcmiSeasonal_Item__NOUT
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(cbm::M2_IN_HA * ( acc_seas.don_leach + acc_seas.no3_leach + acc_seas.nh4_leach + acc_seas.nh3_emis + acc_seas.n2o_emis + acc_seas.no_emis + acc_seas.n2_emis));
#endif
#ifdef  OutputGgcmiSeasonal_Item__NIN
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(cbm::M2_IN_HA * ( acc_seas.n2_fix_plant + acc_seas.n2_fix_algae + acc_seas.n2_fix_bio + acc_seas.nh4_throughfall + acc_seas.no3_throughfall));
#endif
#ifdef  OutputGgcmiSeasonal_Item__NUPT
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(cbm::M2_IN_HA * (acc_seas.nh3_uptake + acc_seas.nh4_uptake + acc_seas.no3_uptake + acc_seas.don_uptake));
#endif
#ifdef  OutputGgcmiSeasonal_Item_AET
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(cbm::MM_IN_M * ( acc_seas.transpiration + acc_seas.interceptionevaporation + acc_seas.soilevaporation + acc_seas.surfacewaterevaporation));
#endif
#ifdef  OutputGgcmiSeasonal_Item_transp
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(cbm::MM_IN_M * acc_seas.transpiration);
#endif
#ifdef  OutputGgcmiSeasonal_Item_evap
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(cbm::MM_IN_M * ( acc_seas.soilevaporation + acc_seas.surfacewaterevaporation));
#endif
#ifdef  OutputGgcmiSeasonal_Item_runoff
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(cbm::MM_IN_M * ( acc_seas.runoff + acc_seas.percolation ));
#endif
#ifdef  OutputGgcmiSeasonal_Item_irri
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(cbm::MM_IN_M * acc_seas.irrigation);
#endif
    return  LDNDC_ERR_OK;
}


#undef  LMOD_OUTPUT_MODULE_BASE
#undef  LMOD_OUTPUT_MODULE_NAME

#undef  OutputGgcmiSeasonal_Rank
#undef  OutputGgcmiSeasonal_Datasize

} /*namespace ldndc*/
