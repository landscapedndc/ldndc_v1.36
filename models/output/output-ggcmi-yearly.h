/*!
 * @brief
 *    yearly ggcmi related output
 *
 * @author
 *    Andrew Smerald
 */

#ifndef  LM_OUTPUT_GGCMI_YEARLY_H_NEW_
#define  LM_OUTPUT_GGCMI_YEARLY_H_NEW_

#include  "mbe_legacyoutputmodel.h"
#include  "state/mbe_state.h"

#define  LMOD_OUTPUT_MODULE_BASE  MBE_LegacyOutputModel
#define  LMOD_OUTPUT_MODULE_NAME  OutputGgcmiYearly
#define  LMOD_OUTPUT_MODULE_ID    "output:ggcmi:yearly"
#define  LMOD_OUTPUT_MODULE_DESC  "GGCMI Yearly Output"
namespace ldndc {
class  substate_physiology_t;
class  substate_watercycle_t;
class  LDNDC_API  LMOD_OUTPUT_MODULE_NAME  :  public  LMOD_OUTPUT_MODULE_BASE
{
    LMOD_EXPORT_MODULE_INFO(LMOD_OUTPUT_MODULE_NAME,LMOD_OUTPUT_MODULE_ID,LMOD_OUTPUT_MODULE_DESC);
    public:
        LMOD_OUTPUT_MODULE_NAME(
                MoBiLE_State *,
                cbm::io_kcomm_t *,
                timemode_e  _timemode);

        ~LMOD_OUTPUT_MODULE_NAME();


        lerr_t  configure( ldndc::config_file_t const *);

        lerr_t  initialize();

        lerr_t  solve();

        lerr_t  finalize();

        lerr_t  wake() { return  LDNDC_ERR_OK; }
        lerr_t  sleep() { return  LDNDC_ERR_OK; }

    private:
        lerr_t  dump_0(
                ldndc_flt64_t *);

    private:
        cbm::io_kcomm_t *  io_kcomm;

        input_class_soillayers_t const *  soillayers_in;

        substate_physiology_t const *  phys;
        substate_soilchemistry_t const *  soilchem;
        substate_watercycle_t const *  water;
        MoBiLE_PlantVegetation *  m_veg;

    private:
        ldndc::sink_handle_t  m_sink;

        struct  output_ggcmi_yearly_acc_t
        {
          double  nh4_throughfall;
          double  no3_throughfall;

          double  don_leach;
          double  no3_leach;
          double  nh4_leach;

          double  ch4_emis;
          double  co2_emis_auto;
          double  co2_emis_hetero;
          double  no_emis;
          double  n2o_emis;
          double  n2_emis;
          double  nh3_emis;
          double  n2_fix_plant;

          double  throughfall;
          double  irrigation;
          double  transpiration;
          double  interceptionevaporation;
          double  soilevaporation;
          double  surfacewaterevaporation;
          double  runoff;
          double  percolation;

          double n_uptake;
          double n_exp_harvest;
          double n_fert;
          double c_fert;

          double c_litter_above;
          double c_litter_below;
          double n_litter_above;
          double n_litter_below;

          double ch4_leach;
          double doc_leach;

          double n_fix_algae;
          double c_fix_algae;
          double n_fix_bio;
        };
        output_ggcmi_yearly_acc_t  acc;
        void  pop_accumulated_outputs();
        void  push_accumulated_outputs();
        void  accumulate_daily_outputs();
        void  update_daily_outputs();

};
} /*namespace ldndc*/


#undef  LMOD_OUTPUT_MODULE_BASE
#undef  LMOD_OUTPUT_MODULE_NAME
#undef  LMOD_OUTPUT_MODULE_ID
#undef  LMOD_OUTPUT_MODULE_DESC


#endif  /*  !LM_OUTPUT_SOILCHEMISTRY_YEARLY_H_NEW_  */
