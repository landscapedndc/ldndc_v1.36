/*!
 * @brief
 *    monthly output of average soilwater above wilting point over the course of the month
 *
 * @author
 *    Andrew Smerald
 */

#include  "output/output-ggcmi-monthly.h"

#include  "constants/lconstants-plant.h"
#include  "constants/lconstants-conv.h"

#define  LMOD_OUTPUT_MODULE_BASE  MBE_LegacyOutputModel
#define  LMOD_OUTPUT_MODULE_NAME  OutputGgcmiMonthly

LMOD_MODULE_INFO(LMOD_OUTPUT_MODULE_NAME,TMODE_PRE_DAILY,LMOD_FLAG_USER|LMOD_FLAG_OUTPUT);
namespace ldndc {

#define  OutputGgcmiMonthly_Item_availsoilwater /*soil water above wilting point*/

/*!
 * @page ggcmioutput
 * @section ggcmioutputmonthly Monthly GGCMI output
 *
 * entity name | decription | unit
 * ----------- | ---------- | ----
 * soilwater | Total water in soil above wilting point | [mm]
 */


ldndc_string_t const  OutputGgcmiMonthly_Ids[] =
{
#ifdef  OutputGgcmiMonthly_Item_availsoilwater
    "soilwater",
#endif
};

ldndc_string_t const  OutputGgcmiMonthly_Header[] =
{
#ifdef  OutputGgcmiMonthly_Item_availsoilwater
    "soilwater[mm]",
#endif
};

#define  OutputGgcmiMonthly_Datasize  (sizeof( OutputGgcmiMonthly_Header) / sizeof( OutputGgcmiMonthly_Header[0]))
ldndc_output_size_t const  OutputGgcmiMonthly_Sizes[] =
{
    OutputGgcmiMonthly_Datasize,

    OutputGgcmiMonthly_Datasize /*total size*/
};
ldndc_output_size_t const *  OutputGgcmiMonthly_EntitySizes = NULL;
#define  OutputGgcmiMonthly_Rank  ((ldndc_output_rank_t)(sizeof( OutputGgcmiMonthly_Sizes) / sizeof( OutputGgcmiMonthly_Sizes[0])) - 1)
atomic_datatype_t const  OutputGgcmiMonthly_Types[] =
{
    LDNDC_FLOAT64
};


LMOD_OUTPUT_MODULE_NAME::LMOD_OUTPUT_MODULE_NAME(
          MoBiLE_State *  _state,
          cbm::io_kcomm_t *  _io_kcomm,
          timemode_e  _timemode)
          : LMOD_OUTPUT_MODULE_BASE( _state, _io_kcomm, _timemode),

          io_kcomm( _io_kcomm),

          soillayers_in( _io_kcomm->get_input_class< input_class_soillayers_t >()),
          soilchem( _state->get_substate< substate_soilchemistry_t >()),
          water( _state->get_substate< substate_watercycle_t >())
{
    soil_water_acc = 0;
    day_counter = -1;
    month_counter = lclock_ref().month();
    dmonth_counter = 0;
}


LMOD_OUTPUT_MODULE_NAME::~LMOD_OUTPUT_MODULE_NAME()
{
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::configure(
                ldndc::config_file_t const *  _cf)
{
    lerr_t  rc_setflags =
        set_metaflags( _cf, RM_DEFAULT_DAILY);
    if ( rc_setflags)
        { return  LDNDC_ERR_FAIL; }

    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::initialize()
{
    m_sink = io_kcomm->sink_handle_acquire( "ggcmimonthly");
    if ( m_sink.status() == LDNDC_ERR_OK)
    {
        lerr_t  rc_layout = LDNDC_OUTPUT_DEFINE_SINK_LAYOUT_DEFAULTS(
            m_sink,OutputGgcmiMonthly);
        RETURN_IF_NOT_OK(rc_layout);
    }
    else
    {
        KLOGERROR( "sink status bad  [sink=","ggcmimonthly","]");
        return  m_sink.status();
    }

    //push_accumulated_outputs();
    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::solve()
{
    ldndc_flt64_t  data_flt64_0[OutputGgcmiMonthly_Datasize];

    day_counter += 1;
    dmonth_counter = lclock_ref().month() - month_counter;

    soil_water_calculator();

    if( dmonth_counter != 0 )
    {
      lerr_t  rc_dump = dump_0( data_flt64_0);

      if ( rc_dump)
          { return  LDNDC_ERR_FAIL; }

      void *  data[] = { data_flt64_0};
      lerr_t  rc_write =
          write_fixed_record( &m_sink, data);
      if ( rc_write)
          { return  LDNDC_ERR_FAIL; }

      reset_monthly();
    }

    month_counter = lclock_ref().month();


    return  LDNDC_ERR_OK;
}



lerr_t
LMOD_OUTPUT_MODULE_NAME::finalize()
{
    io_kcomm->sink_handle_release( &m_sink);
    return  LDNDC_ERR_OK;
}

void
LMOD_OUTPUT_MODULE_NAME::soil_water_calculator()
{
  for ( size_t  sl = 0;  sl < soillayers_in->soil_layer_cnt();  ++sl)
  {
      soil_water_acc += ( water->wc_sl[sl] - soilchem->wcmin_sl[sl]) * soilchem->h_sl[sl];
  }
}

void
LMOD_OUTPUT_MODULE_NAME::reset_monthly()
{
  day_counter = 0;
  soil_water_acc = 0;
}

lerr_t
LMOD_OUTPUT_MODULE_NAME::dump_0(
        ldndc_flt64_t *  _buf)
{

    LDNDC_OUTPUT_SET_COLUMN_INDEX(0);


#ifdef  OutputGgcmiMonthly_Item_availsoilwater
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(cbm::MM_IN_M * soil_water_acc/day_counter);
#endif
    return  LDNDC_ERR_OK;
}


#undef  LMOD_OUTPUT_MODULE_BASE
#undef  LMOD_OUTPUT_MODULE_NAME

#undef  OutputGgcmiMonthly_Rank
#undef  OutputGgcmiMonthly_Datasize

} /*namespace ldndc*/
