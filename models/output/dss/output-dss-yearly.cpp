/*!
 * @brief
 *
 * @author
 *  David Kraus
 */

#include  "output/dss/output-dss-yearly.h"

#include  "constants/lconstants-plant.h"
#include  "constants/lconstants-conv.h"

#define  LMOD_OUTPUT_MODULE_BASE  MBE_LegacyOutputModel
#define  LMOD_OUTPUT_MODULE_NAME  OutputDSSYearly

LMOD_MODULE_INFO(LMOD_OUTPUT_MODULE_NAME,TMODE_POST_DAILY,LMOD_FLAG_USER|LMOD_FLAG_OUTPUT);
namespace ldndc {


/*!
 * @page dssoutput
 * @section dssoutputyearly DSS output (yearly)
 *
 * entity name | decription | unit
 * ----------- | ---------- | ----
 * aDW\_fru | Annual dry weight of exported fruits from field | [kgDWha-1]
 * aDW\_yield | Annual dry weight yield exported from field | [kgDWha-1]
 * aN\_yield | Annual nitrogen yield exported from field | [kgNha-1]
 * aC\_soil\_change | | [kgCha-1]
 * nitrogen\_use\_efficiency_total || [-]
 * nitrogen\_use\_efficiency_plant || [-]
 * aCH4\_emis || [kgCha-1]
 * aN2O\_emis || [kgNha-1]
 * aNH3\_emis || [kgNha-1]
 * aNO3\_leach || [kgNha-1]
 * awater\_percol || [Lha-1]
 */

ldndc_string_t const  OutputDSSYearly_Ids[] =
{
    "aDW_fru",
    "aDW_yield",
    "aN_yield",
    "aN_fert",
    "aC_soil_change",
    "nitrogen_use_efficiency_total",
    "nitrogen_use_efficiency_plant",
    "aCH4_emis",
    "aN2O_emis",
    "aNH3_emis",
    "aNO3_leach",
    "aWater_percol",
    "aWater_prec",
    "temperature"
};



ldndc_string_t const  OutputDSSYearly_Header[] =
{
    "aDW_fru[kgDWha-1]",
    "aDW_yield[kgDWha-1]",
    "aN_yield[kgNha-1]",
    "aN_fert[kgNha-1]",
    "aC_soil_change[kgCha-1]",
    "nitrogen_use_efficiency_total[-]",
    "nitrogen_use_efficiency_plant[-]",
    "aCH4_emis[kgCha-1]",
    "aN2O_emis[kgNha-1]",
    "aNH3_emis[kgNha-1]",
    "aNO3_leach[kgNha-1]",
    "aWater_percol[Lha-1]",
    "aWater_prec[mm]",
    "temperature[oC]"
};

#define  OutputDSSYearly_Datasize  (sizeof( OutputDSSYearly_Header) / sizeof( OutputDSSYearly_Header[0]))
ldndc_output_size_t const  OutputDSSYearly_Sizes[] =
{
    OutputDSSYearly_Datasize,
    OutputDSSYearly_Datasize /*total size*/
};
ldndc_output_size_t const *  OutputDSSYearly_EntitySizes = NULL;
#define  OutputDSSYearly_Rank  ((ldndc_output_rank_t)(sizeof( OutputDSSYearly_Sizes) / sizeof( OutputDSSYearly_Sizes[0])) - 1)
atomic_datatype_t const  OutputDSSYearly_Types[] =
{
    LDNDC_FLOAT64
};


LMOD_OUTPUT_MODULE_NAME::LMOD_OUTPUT_MODULE_NAME(
        MoBiLE_State *  _state,
        cbm::io_kcomm_t *  _io_kcomm,
        timemode_e  _timemode)
        : LMOD_OUTPUT_MODULE_BASE( _state, _io_kcomm, _timemode),

        io_kcomm( _io_kcomm),
        sl( _io_kcomm->get_input_class_ref< input_class_soillayers_t >()),
        phys( _state->get_substate< substate_physiology_t >()),
        soilchem( _state->get_substate< substate_soilchemistry_t >()),
        water( _state->get_substate< substate_watercycle_t >()),
        microclimate( _state->get_substate< substate_microclimate_t >()),
        climate_( io_kcomm->get_input_class_ref< input_class_climate_t >())
{
}


LMOD_OUTPUT_MODULE_NAME::~LMOD_OUTPUT_MODULE_NAME()
{
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::configure(
          ldndc::config_file_t const *  _cf)
{
    lerr_t  rc_setflags =
        set_metaflags( _cf, RM_DEFAULT_YEARLY);
    if ( rc_setflags)
        { return  LDNDC_ERR_FAIL; }

    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::initialize()
{
    m_sink = io_kcomm->sink_handle_acquire( "dssyearly");
    if ( m_sink.status() == LDNDC_ERR_OK)
    {
        lerr_t  rc_layout = LDNDC_OUTPUT_DEFINE_SINK_LAYOUT_DEFAULTS(
            m_sink,OutputDSSYearly);
        RETURN_IF_NOT_OK(rc_layout);
    }
    else
    {
        KLOGERROR( "sink status bad  [sink=","dssyearly","]");
        return  m_sink.status();
    }

    outputs.precipitation = 0.0;
    outputs.annual_average_temperature = climate_.annual_temperature_average();
    update_outputs();
    
    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::solve()
{
    accumulate_outputs();

    if ( lclock()->is_position( TMODE_POST_YEARLY))
    {
        ldndc_flt64_t  data_flt64_0[OutputDSSYearly_Datasize];
        lerr_t  rc_dump = dump_0( data_flt64_0);
        RETURN_IF_NOT_OK(rc_dump);

        void *  data[] = { data_flt64_0};
        lerr_t  rc_write = write_fixed_record( &m_sink, data);
        if ( rc_write){ return  LDNDC_ERR_FAIL; }

        update_outputs();
    }

    return  LDNDC_ERR_OK;
}


void
LMOD_OUTPUT_MODULE_NAME::accumulate_outputs()
{
    outputs.precipitation += microclimate->nd_precipitation;
    
    double ts_fraction( 1.0 / (lclock()->days_in_year()));
    outputs.annual_average_temperature = (1.0 - ts_fraction) * outputs.annual_average_temperature + ts_fraction * microclimate->nd_temp_sl[sl.soil_layer_cnt()-1];
}


void
LMOD_OUTPUT_MODULE_NAME::update_outputs()
{
    outputs.c_fru_export_harvest_old = phys->accumulated_c_fru_export_harvest;
    outputs.c_export_harvest_old = phys->accumulated_c_export_harvest;
    outputs.n_export_harvest_old = phys->accumulated_n_export_harvest;
 
    outputs.c_net_flux_old = soilchem->get_accumulated_c_net_flux();
    
    outputs.n_loss_old = soilchem->accumulated_don_leach
                        + soilchem->accumulated_no3_leach
                        + soilchem->accumulated_nh4_leach
                        + soilchem->accumulated_no_emis
                        + soilchem->accumulated_n2o_emis
                        + soilchem->accumulated_n2_emis
                        + soilchem->accumulated_nh3_emis;

    outputs.n_fert_old = soilchem->accumulated_n_fertilizer;
    
    outputs.ch4_emis_old = soilchem->accumulated_ch4_emis;
    outputs.n2o_emis_old = soilchem->accumulated_n2o_emis;
    outputs.nh3_emis_old = soilchem->accumulated_nh3_emis;
    outputs.no3_leach_old = soilchem->accumulated_no3_leach;
    
    outputs.water_percol_old = water->accumulated_percolation;
//    outputs.water_prec_old = water->accumulated_precipitation;
}

lerr_t
LMOD_OUTPUT_MODULE_NAME::finalize()
{
    io_kcomm->sink_handle_release( &m_sink);
    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::dump_0(
        ldndc_flt64_t *  _buf)
{
    /*! conversion from kg m-2 to kg ha-1 */
    static const double  p( cbm::M2_IN_HA);

    double const dw_fru( (phys->accumulated_c_fru_export_harvest - outputs.c_fru_export_harvest_old) / cbm::CCDM);
    double const dw_yield( (phys->accumulated_c_export_harvest - outputs.c_export_harvest_old) / cbm::CCDM);
    double const n_yield( phys->accumulated_n_export_harvest - outputs.n_export_harvest_old);
    
    double const c_poolchange(  soilchem->get_accumulated_c_net_flux() - outputs.c_net_flux_old);
    double const n_loss(  soilchem->accumulated_don_leach
                        + soilchem->accumulated_no3_leach
                        + soilchem->accumulated_nh4_leach
                        + soilchem->accumulated_no_emis
                        + soilchem->accumulated_n2o_emis
                        + soilchem->accumulated_n2_emis
                        + soilchem->accumulated_nh3_emis - outputs.n_loss_old);
    double const n_fert(  soilchem->accumulated_n_fertilizer - outputs.n_fert_old);
    
    LDNDC_OUTPUT_SET_COLUMN_INDEX(0);

    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT ( p * dw_fru);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT ( p * dw_yield);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT ( p * n_yield);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT ( p * n_fert);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT ( p * c_poolchange);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT ( cbm::flt_greater_zero( n_fert) ? n_loss / n_fert : -99.99);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT ( cbm::flt_greater_zero( n_fert) ? n_yield / n_fert : -99.99);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT ( p * (soilchem->accumulated_ch4_emis - outputs.ch4_emis_old));
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT ( p * (soilchem->accumulated_n2o_emis - outputs.n2o_emis_old));
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT ( p * (soilchem->accumulated_nh3_emis - outputs.nh3_emis_old));
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT ( p * (soilchem->accumulated_no3_leach - outputs.no3_leach_old));
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT ( p * (water->accumulated_percolation - outputs.water_percol_old) * cbm::MM_IN_M);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT ( (outputs.precipitation) * cbm::MM_IN_M);
//    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT ( (water->accumulated_precipitation - outputs.water_prec_old) * cbm::MM_IN_M);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT ( outputs.annual_average_temperature);

    outputs.precipitation = 0.0;
    
    return  LDNDC_ERR_OK;
}


#undef  LMOD_OUTPUT_MODULE_BASE
#undef  LMOD_OUTPUT_MODULE_NAME

#undef  OutputDSSYearly_Rank
#undef  OutputDSSYearly_Datasize

} /*namespace ldndc*/

