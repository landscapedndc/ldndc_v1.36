/*!
 * @brief
 *
 * @author
 *    David Kraus
 */

#ifndef  LM_OUTPUT_DSS_YEARLY_H_
#define  LM_OUTPUT_DSS_YEARLY_H_

#include  "mbe_legacyoutputmodel.h"
#include  "state/mbe_state.h"

#define  LMOD_OUTPUT_MODULE_BASE  MBE_LegacyOutputModel
#define  LMOD_OUTPUT_MODULE_NAME  OutputDSSYearly
#define  LMOD_OUTPUT_MODULE_ID    "output:dss:yearly"
#define  LMOD_OUTPUT_MODULE_DESC  "DSS Yearly Output"
namespace ldndc {

struct  yearly_accumulated_outputs_t
{
    double c_net_flux_old;

    double c_fru_export_harvest_old;

    double c_export_harvest_old;
    double n_export_harvest_old;

    double n_loss_old;
    double n_fert_old;

    double ch4_emis_old;
    double n2o_emis_old;
    double nh3_emis_old;
    double no3_leach_old;

    double water_percol_old;
    double water_prec_old;
    double precipitation;
    double annual_average_temperature;
};


class  LDNDC_API  LMOD_OUTPUT_MODULE_NAME  :  public  LMOD_OUTPUT_MODULE_BASE
{
    LMOD_EXPORT_MODULE_INFO(LMOD_OUTPUT_MODULE_NAME,LMOD_OUTPUT_MODULE_ID,LMOD_OUTPUT_MODULE_DESC);
    public:
        LMOD_OUTPUT_MODULE_NAME(
                MoBiLE_State *,
                cbm::io_kcomm_t *,
                timemode_e  _timemode);

        ~LMOD_OUTPUT_MODULE_NAME();


        lerr_t  configure( ldndc::config_file_t const *);

        lerr_t  initialize();

        lerr_t  solve();

        lerr_t  finalize();

        lerr_t  wake() { return  LDNDC_ERR_OK; }
        lerr_t  sleep() { return  LDNDC_ERR_OK; }

    private:
        lerr_t  dump_0(
                ldndc_flt64_t *);

    private:
        cbm::io_kcomm_t *  io_kcomm;

        input_class_soillayers_t const &  sl;
        substate_physiology_t const *  phys;
        substate_soilchemistry_t const *  soilchem;
        substate_watercycle_t const *  water;
        substate_microclimate_t const *  microclimate;
        input_class_climate_t const &  climate_;

    private:
        ldndc::sink_handle_t  m_sink;
        yearly_accumulated_outputs_t  outputs;

        void accumulate_outputs();
        void update_outputs();

};
} /*namespace ldndc*/


#undef  LMOD_OUTPUT_MODULE_BASE
#undef  LMOD_OUTPUT_MODULE_NAME
#undef  LMOD_OUTPUT_MODULE_ID
#undef  LMOD_OUTPUT_MODULE_DESC


#endif  /*  !LM_OUTPUT_DSS_YEARLY_H_  */

