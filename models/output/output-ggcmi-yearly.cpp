/*!
 * @brief
 *    gathers together outputs useful for yearly C and N inventories
 *
 * @author
 *    Andrew Smerald
 */

#include  "output/output-ggcmi-yearly.h"

#include  "constants/lconstants-plant.h"
#include  "constants/lconstants-conv.h"

#define  LMOD_OUTPUT_MODULE_BASE  MBE_LegacyOutputModel
#define  LMOD_OUTPUT_MODULE_NAME  OutputGgcmiYearly

LMOD_MODULE_INFO(LMOD_OUTPUT_MODULE_NAME,TMODE_POST_DAILY,LMOD_FLAG_USER|LMOD_FLAG_OUTPUT);
namespace ldndc {

//nitrogen
//emissions
#define  OutputGgcmiYearly_Item__N2
#define  OutputGgcmiYearly_Item__NH3
#define  OutputGgcmiYearly_Item__N2O
#define  OutputGgcmiYearly_Item__NO
//leaching
#define  OutputGgcmiYearly_Item__NO3
#define  OutputGgcmiYearly_Item__DON
#define  OutputGgcmiYearly_Item__NH4
//exported with harvest
#define  OutputGgcmiYearly_Item__NEXP_HARVEST
//inputs to soil
#define  OutputGgcmiYearly_Item__NDEP
#define  OutputGgcmiYearly_Item__NFIX
#define  OutputGgcmiYearly_Item__NFERT
//additional information about soil-plant flows
#define  OutputGgcmiYearly_Item__NUPT //plant uptake
#define  OutputGgcmiYearly_Item__NLIT_ABV //returned to soil via roots (exudation and decay)
#define  OutputGgcmiYearly_Item__NLIT_BLW //returned to soil from above-ground plant

//carbon
//emissions
#define  OutputGgcmiYearly_Item__CO2_HETERO
#define  OutputGgcmiYearly_Item__CO2_AUTO
#define  OutputGgcmiYearly_Item__CH4
//leaching
#define  OutputGgcmiYearly_Item__CLEACH
//inputs to soil
#define  OutputGgcmiYearly_Item__CFERT
#define  OutputGgcmiYearly_Item__CLIT_ABV
#define  OutputGgcmiYearly_Item__CLIT_BLW
#define  OutputGgcmiYearly_Item__CFIX

//water
#define  OutputGgcmiYearly_Item_throughf /*rainfall that reaches surface*/
#define  OutputGgcmiYearly_Item_intercep // interception evaporation
#define  OutputGgcmiYearly_Item_transp //transpiration
#define  OutputGgcmiYearly_Item_evap /*soil and surface water evaporation*/
#define  OutputGgcmiYearly_Item_runoff /*surface runoff*/
#define  OutputGgcmiYearly_Item_perc /*percolation out of bottom soil layer*/
#define  OutputGgcmiYearly_Item_irri /*irrigation*/

/*!
 * @page ggcmioutput
 * @section ggcmioutputyearly Yearly GGCMI output
 *
 * entity name | decription | unit
 * ----------- | ---------- | ----
 * dN_n2_emis | Yearly N2 flux | [kgNha-1]
 * dN_nh3_emis | Yearly NH3 flux | [kgNha-1]
 * dN_n2o_emis | Yearly N2O flux | [kgNha-1]
 * dN_no_emis | Yearly NO flux | [kgNha-1]
 *
 * dN_no3_leach | Yearly leaching of NO3 |  [kgNha-1]
 * dN_don_leach | Yearly leaching of dissolved organic nitrogen |  [kgNha-1]
 * dN_nh4_leach | Yearly leaching of NH4 |  [kgNha-1]
 *
 * dN_harvest_export | Yearly N export via harvest (grain + exported straw + exported roots) |  [kgNha-1]
 *
 * dN_dep| N input from deposition (NH4, NO3) | [kgNha-1]
 * dN_fix | Yearly nitrogen fixation by plants | [kgNha-1]
 * dN_fert | Yearly nitrogen input via fertiliser and manure | [kgNha-1]
 *
 * dN_up | Yearly nitrogen uptake by plants from the litter layer and mineral soil | [kgNha-1]
 * dN_litter_above | Yearly return of N to soil from above-ground plant litter | [kgNha-1]
 * dN_litter_below | Yearly return of N to soil from roots (decay and exudation) | [kgNha-1]
 *
 * dC_co2_hetero_emis | Yearly flux of CO2 from heterotrophic respiration | [kgCha-1]
 * dC_co2_auto_emis | Yearly flux of CO2 from autotrophic respiration associated with roots | [kgCha-1]
 * dC_ch4_emis | Yearly methane flux | [kgCha-1]
 *
 * dC_leach | Yearly leaching of DOC and CH4 out of soil |  [kgCha-1]
 *
 * dC_fert | Yearly carbon input via manure | [kgCha-1]
 * dC_litter_above | Yearly addition of C to soil from above-ground plant litter | [kgCha-1]
 * dC_litter_below | Yearly addition of C to soil from roots (decay and exudation) | [kgCha-1]
 * dC_fix | Yearly fixation of C by algae | [kgCha-1]

 * throughf | Yearly throughfall (water that reaches surface of soil) | [mm]
 * intercep | Yearly evaporation from interception | [mm]
 * transp | Yearly transpiration | [mm]
 * evap | Yearly evaporation from soil and surface water (not from interception) | [mm]
 * runoff | Yearly lateral run-off | [mm]
 * perc | Yearly percolation out of bottom soil layer | [mm]
 * irri | Yearly addition of irrigation water | [mm]
 */

ldndc_string_t const  OutputGgcmiYearly_Ids[] =
{
    #ifdef  OutputGgcmiYearly_Item__N2
        "dN_n2_emis",
    #endif
    #ifdef  OutputGgcmiYearly_Item__NH3
        "dN_nh3_emis",
    #endif
    #ifdef  OutputGgcmiYearly_Item__N2O
        "dN_n2o_emis",
    #endif
    #ifdef  OutputGgcmiYearly_Item__NO
        "dN_no_emis",
    #endif
    #ifdef  OutputGgcmiYearly_Item__NO3
        "dN_no3_leach",
    #endif
    #ifdef  OutputGgcmiYearly_Item__DON
        "dN_don_leach",
    #endif
    #ifdef  OutputGgcmiYearly_Item__NH4
        "dN_nh4_leach",
    #endif
    #ifdef  OutputGgcmiYearly_Item__NEXP_HARVEST
        "dN_harvest_export",
    #endif
    #ifdef  OutputGgcmiYearly_Item__NDEP
        "dN_dep",
    #endif
    #ifdef  OutputGgcmiYearly_Item__NFIX
        "dN_fix",
    #endif
    #ifdef  OutputGgcmiYearly_Item__NFERT
        "dN_fert",
    #endif
    #ifdef  OutputGgcmiYearly_Item__NUPT
        "dN_up",
    #endif
    #ifdef  OutputGgcmiYearly_Item__NLIT_ABV
        "dN_litter_above",
    #endif
    #ifdef  OutputGgcmiYearly_Item__NLIT_BLW
        "dN_litter_below",
    #endif

    #ifdef  OutputGgcmiYearly_Item__CO2_HETERO
        "dC_co2_hetero_emis",
    #endif
    #ifdef  OutputGgcmiYearly_Item__CO2_AUTO
        "dC_co2_auto_emis",
    #endif
    #ifdef  OutputGgcmiYearly_Item__CH4
        "dC_ch4_emis",
    #endif
    #ifdef  OutputGgcmiYearly_Item__CLEACH
        "dC_leach",
    #endif
    #ifdef  OutputGgcmiYearly_Item__CFERT
        "dC_fert",
    #endif
    #ifdef  OutputGgcmiYearly_Item__CLIT_ABV
        "dC_litter_above",
    #endif
    #ifdef  OutputGgcmiYearly_Item__CLIT_BLW
        "dC_litter_below",
    #endif
    #ifdef  OutputGgcmiYearly_Item__CFIX
        "dC_fix",
    #endif

    #ifdef  OutputGgcmiYearly_Item_throughf
        "throughfall",
    #endif
    #ifdef  OutputGgcmiYearly_Item_intercep
        "intercep",
    #endif
    #ifdef  OutputGgcmiYearly_Item_transp
        "transp",
    #endif
    #ifdef  OutputGgcmiYearly_Item_evap
        "evap",
    #endif
    #ifdef  OutputGgcmiYearly_Item_runoff
        "runoff",
    #endif
    #ifdef  OutputGgcmiYearly_Item_perc
        "perc",
    #endif
    #ifdef  OutputGgcmiYearly_Item_irri
        "irri",
    #endif
};
ldndc_string_t const  OutputGgcmiYearly_Header[] =
{
  #ifdef  OutputGgcmiYearly_Item__N2
      "dN_n2_emis[kgNha-1]",
  #endif
  #ifdef  OutputGgcmiYearly_Item__NH3
      "dN_nh3_emis[kgNha-1]",
  #endif
  #ifdef  OutputGgcmiYearly_Item__N2O
      "dN_n2o_emis[kgNha-1]",
  #endif
  #ifdef  OutputGgcmiYearly_Item__NO
      "dN_no_emis[kgNha-1]",
  #endif
  #ifdef  OutputGgcmiYearly_Item__NO3
      "dN_no3_leach[kgNha-1]",
  #endif
  #ifdef  OutputGgcmiYearly_Item__DON
      "dN_don_leach[kgNha-1]",
  #endif
  #ifdef  OutputGgcmiYearly_Item__NH4
      "dN_nh4_leach[kgNha-1]",
  #endif
  #ifdef  OutputGgcmiYearly_Item__NEXP_HARVEST
      "dN_harvest_export[kgNha-1]",
  #endif
  #ifdef  OutputGgcmiYearly_Item__NDEP
      "dN_dep[kgNha-1]",
  #endif
  #ifdef  OutputGgcmiYearly_Item__NFIX
      "dN_fix[kgNha-1]",
  #endif
  #ifdef  OutputGgcmiYearly_Item__NFERT
      "dN_fert[kgNha-1]",
  #endif
  #ifdef  OutputGgcmiYearly_Item__NUPT
      "dN_up[kgNha-1]",
  #endif
  #ifdef  OutputGgcmiYearly_Item__NLIT_ABV
      "dN_litter_above[kgNha-1]",
  #endif
  #ifdef  OutputGgcmiYearly_Item__NLIT_BLW
      "dN_litter_below[kgNha-1]",
  #endif

  #ifdef  OutputGgcmiYearly_Item__CO2_HETERO
      "dC_co2_hetero_emis[kgCha-1]",
  #endif
  #ifdef  OutputGgcmiYearly_Item__CO2_AUTO
      "dC_co2_auto_emis[kgCha-1]",
  #endif
  #ifdef  OutputGgcmiYearly_Item__CH4
      "dC_ch4_emis[kgCha-1]",
  #endif
  #ifdef  OutputGgcmiYearly_Item__CLEACH
      "dC_leach[kgCha-1]",
  #endif
  #ifdef  OutputGgcmiYearly_Item__CFERT
      "dC_fert[kgCha-1]",
  #endif
  #ifdef  OutputGgcmiYearly_Item__CLIT_ABV
      "dC_litter_above[kgCha-1]",
  #endif
  #ifdef  OutputGgcmiYearly_Item__CLIT_BLW
      "dC_litter_below[kgCha-1]",
  #endif
  #ifdef  OutputGgcmiYearly_Item__CFIX
      "dC_fix[kgCha-1]",
  #endif

  #ifdef  OutputGgcmiYearly_Item_throughf
      "throughfall[mm]",
  #endif
  #ifdef  OutputGgcmiYearly_Item_intercep
      "intercep[mm]",
  #endif
  #ifdef  OutputGgcmiYearly_Item_transp
      "transp[mm]",
  #endif
  #ifdef  OutputGgcmiYearly_Item_evap
      "evap[mm]",
  #endif
  #ifdef  OutputGgcmiYearly_Item_runoff
      "runoff[mm]",
  #endif
  #ifdef  OutputGgcmiYearly_Item_perc
      "perc[mm]",
  #endif
  #ifdef  OutputGgcmiYearly_Item_irri
      "irri[mm]",
  #endif
};
#define  OutputGgcmiYearly_Datasize  (sizeof( OutputGgcmiYearly_Header) / sizeof( OutputGgcmiYearly_Header[0]))
ldndc_output_size_t const  OutputGgcmiYearly_Sizes[] =
{
    OutputGgcmiYearly_Datasize,

    OutputGgcmiYearly_Datasize /*total size*/
};
ldndc_output_size_t const *  OutputGgcmiYearly_EntitySizes = NULL;
#define  OutputGgcmiYearly_Rank  ((ldndc_output_rank_t)(sizeof( OutputGgcmiYearly_Sizes) / sizeof( OutputGgcmiYearly_Sizes[0])) - 1)
atomic_datatype_t const  OutputGgcmiYearly_Types[] =
{
    LDNDC_FLOAT64
};


LMOD_OUTPUT_MODULE_NAME::LMOD_OUTPUT_MODULE_NAME(
        MoBiLE_State *  _state,
        cbm::io_kcomm_t *  _io_kcomm,
        timemode_e  _timemode)
        : LMOD_OUTPUT_MODULE_BASE( _state, _io_kcomm, _timemode),

          io_kcomm( _io_kcomm),

          soillayers_in( _io_kcomm->get_input_class< input_class_soillayers_t >()),

          phys( _state->get_substate< substate_physiology_t >()),
          soilchem( _state->get_substate< substate_soilchemistry_t >()),
          water( _state->get_substate< substate_watercycle_t >()),
          m_veg( &_state->vegetation)
{
  acc.n2_fix_plant = 0.0;
}


LMOD_OUTPUT_MODULE_NAME::~LMOD_OUTPUT_MODULE_NAME()
{
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::configure(
          ldndc::config_file_t const *  _cf)
{
    lerr_t  rc_setflags =
        set_metaflags( _cf, RM_DEFAULT_YEARLY);
    if ( rc_setflags)
        { return  LDNDC_ERR_FAIL; }

    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::initialize()
{
    m_sink = io_kcomm->sink_handle_acquire( "ggcmiyearly");
    if ( m_sink.status() == LDNDC_ERR_OK)
    {
        lerr_t  rc_layout = LDNDC_OUTPUT_DEFINE_SINK_LAYOUT_DEFAULTS(
            m_sink,OutputGgcmiYearly);
        RETURN_IF_NOT_OK(rc_layout);
    }
    else
    {
        KLOGERROR( "sink status bad  [sink=","ggcmiyearly","]");
        return  m_sink.status();
    }

    push_accumulated_outputs();

    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::solve()
{
    accumulate_daily_outputs();

    if ( lclock()->is_position( TMODE_POST_YEARLY))
    {
      ldndc_flt64_t  data_flt64_0[OutputGgcmiYearly_Datasize];

      pop_accumulated_outputs();
      lerr_t  rc_dump = dump_0( data_flt64_0);
      push_accumulated_outputs();

      if ( rc_dump)
          { return  LDNDC_ERR_FAIL; }

      void *  data[] = { data_flt64_0};
      write_fixed_record( &m_sink, data);

      update_daily_outputs();
    }

    return  LDNDC_ERR_OK;
}

void
LMOD_OUTPUT_MODULE_NAME::pop_accumulated_outputs()
{
  acc.nh4_throughfall =
             phys->accumulated_nh4_throughfall - acc.nh4_throughfall;
  acc.no3_throughfall =
             phys->accumulated_no3_throughfall - acc.no3_throughfall;

  acc.don_leach =
             soilchem->accumulated_don_leach - acc.don_leach;
  acc.no3_leach =
             soilchem->accumulated_no3_leach - acc.no3_leach;
  acc.nh4_leach =
             soilchem->accumulated_nh4_leach - acc.nh4_leach;

  acc.ch4_emis =
             soilchem->accumulated_ch4_emis - acc.ch4_emis;
  acc.co2_emis_auto =
             soilchem->accumulated_co2_emis_auto - acc.co2_emis_auto;
  acc.co2_emis_hetero =
             soilchem->accumulated_co2_emis_hetero - acc.co2_emis_hetero;
  acc.no_emis =
             soilchem->accumulated_no_emis - acc.no_emis;
  acc.n2o_emis =
             soilchem->accumulated_n2o_emis - acc.n2o_emis;
  acc.n2_emis =
             soilchem->accumulated_n2_emis - acc.n2_emis;
  acc.nh3_emis =
             soilchem->accumulated_nh3_emis - acc.nh3_emis;

  acc.throughfall =
             water->accumulated_throughfall - acc.throughfall;
  acc.transpiration =
             water->accumulated_transpiration_sl.sum() - acc.transpiration;
  acc.interceptionevaporation =
             water->accumulated_interceptionevaporation - acc.interceptionevaporation;
  acc.soilevaporation =
             water->accumulated_soilevaporation - acc.soilevaporation;
  acc.surfacewaterevaporation =
             water->accumulated_surfacewaterevaporation - acc.surfacewaterevaporation;
  acc.runoff =
             water->accumulated_runoff - acc.runoff;
  acc.percolation =
             water->accumulated_percolation - acc.percolation;
  acc.irrigation =
             water->accumulated_irrigation_ggcmi + water->accumulated_irrigation_automatic - acc.irrigation;

  acc.n_uptake = (phys->accumulated_no3_uptake_sl.sum() +
                  phys->accumulated_nh4_uptake_sl.sum() +
                  phys->accumulated_don_uptake_sl.sum()) - acc.n_uptake;

  acc.n_exp_harvest = phys->accumulated_n_export_harvest - acc.n_exp_harvest;
  acc.n_fert = soilchem->accumulated_n_fertilizer - acc.n_fert;
  acc.c_fert = soilchem->accumulated_c_fertilizer - acc.c_fert;

  acc.c_litter_above = soilchem->accumulated_c_litter_stubble + soilchem->accumulated_c_litter_wood_above - acc.c_litter_above;
  acc.n_litter_above = soilchem->accumulated_n_litter_stubble + soilchem->accumulated_n_litter_wood_above - acc.n_litter_above;
  acc.c_litter_below = soilchem->accumulated_c_litter_wood_below_sl.sum() + soilchem->accumulated_c_litter_below_sl.sum() - acc.c_litter_below;
  acc.n_litter_below = soilchem->accumulated_n_litter_wood_below_sl.sum() + soilchem->accumulated_n_litter_below_sl.sum() - acc.n_litter_below;

  acc.ch4_leach = soilchem->accumulated_ch4_leach - acc.ch4_leach;
  acc.doc_leach = soilchem->accumulated_doc_leach - acc.doc_leach;

  acc.c_fix_algae = soilchem->accumulated_c_fix_algae - acc.c_fix_algae;
  acc.n_fix_algae = soilchem->accumulated_n_fix_algae - acc.n_fix_algae;
  acc.n_fix_bio = soilchem->accumulated_n2_fixation - acc.n_fix_bio;
}



void
LMOD_OUTPUT_MODULE_NAME::push_accumulated_outputs()
{
  acc.nh4_throughfall = phys->accumulated_nh4_throughfall;
  acc.no3_throughfall = phys->accumulated_no3_throughfall;

  acc.don_leach = soilchem->accumulated_don_leach;
  acc.no3_leach = soilchem->accumulated_no3_leach;
  acc.nh4_leach = soilchem->accumulated_nh4_leach;

  acc.ch4_emis = soilchem->accumulated_ch4_emis;
  acc.co2_emis_auto = soilchem->accumulated_co2_emis_auto;
  acc.co2_emis_hetero = soilchem->accumulated_co2_emis_hetero;
  acc.no_emis = soilchem->accumulated_no_emis;
  acc.n2o_emis = soilchem->accumulated_n2o_emis;
  acc.n2_emis = soilchem->accumulated_n2_emis;
  acc.nh3_emis = soilchem->accumulated_nh3_emis;

  acc.throughfall = water->accumulated_throughfall;
  acc.transpiration = water->accumulated_transpiration_sl.sum();
  acc.interceptionevaporation = water->accumulated_interceptionevaporation;
  acc.soilevaporation = water->accumulated_soilevaporation;
  acc.surfacewaterevaporation = water->accumulated_surfacewaterevaporation;
  acc.runoff = water->accumulated_runoff;
  acc.percolation = water->accumulated_percolation;
  acc.irrigation = water->accumulated_irrigation_ggcmi + water->accumulated_irrigation_automatic;

  acc.n_uptake = (phys->accumulated_no3_uptake_sl.sum() +
                  phys->accumulated_nh4_uptake_sl.sum() +
                  phys->accumulated_don_uptake_sl.sum());
  acc.n_exp_harvest = phys->accumulated_n_export_harvest;
  acc.n_fert = soilchem->accumulated_n_fertilizer;
  acc.c_fert = soilchem->accumulated_c_fertilizer;

  acc.c_litter_above = soilchem->accumulated_c_litter_stubble + soilchem->accumulated_c_litter_wood_above;
  acc.n_litter_above = soilchem->accumulated_n_litter_stubble + soilchem->accumulated_n_litter_wood_above;

  acc.c_litter_below = soilchem->accumulated_c_litter_below_sl.sum() + soilchem->accumulated_c_litter_wood_below_sl.sum();
  acc.n_litter_below = soilchem->accumulated_n_litter_below_sl.sum() + soilchem->accumulated_n_litter_wood_below_sl.sum();

  acc.ch4_leach = soilchem->accumulated_ch4_leach;
  acc.doc_leach = soilchem->accumulated_doc_leach;

  acc.c_fix_algae = soilchem->accumulated_c_fix_algae;
  acc.n_fix_algae = soilchem->accumulated_n_fix_algae;
  acc.n_fix_bio = soilchem->accumulated_n2_fixation;
}

void
LMOD_OUTPUT_MODULE_NAME::accumulate_daily_outputs()
{
    for ( PlantIterator s = m_veg->begin(); s != m_veg->end(); ++s)
    {
        acc.n2_fix_plant += (*s)->d_n2_fixation;
    }
}

void
LMOD_OUTPUT_MODULE_NAME::update_daily_outputs()
{
    acc.n2_fix_plant = 0.0;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::finalize()
{
    io_kcomm->sink_handle_release( &m_sink);
    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::dump_0(
        ldndc_flt64_t *  _buf)
{

    LDNDC_OUTPUT_SET_COLUMN_INDEX(0);

    #ifdef  OutputGgcmiYearly_Item__N2
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(cbm::M2_IN_HA * acc.n2_emis);
    #endif
    #ifdef  OutputGgcmiYearly_Item__NH3
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(cbm::M2_IN_HA * acc.nh3_emis);
    #endif
    #ifdef  OutputGgcmiYearly_Item__N2O
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(cbm::M2_IN_HA * acc.n2o_emis);
    #endif
    #ifdef  OutputGgcmiYearly_Item__NO
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(cbm::M2_IN_HA * acc.no_emis);
    #endif
    #ifdef  OutputGgcmiYearly_Item__NO3
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(cbm::M2_IN_HA * acc.no3_leach );
    #endif
    #ifdef  OutputGgcmiYearly_Item__DON
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(cbm::M2_IN_HA * acc.don_leach );
    #endif
    #ifdef  OutputGgcmiYearly_Item__NH4
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(cbm::M2_IN_HA * acc.nh4_leach );
    #endif
    #ifdef  OutputGgcmiYearly_Item__NEXP_HARVEST
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(cbm::M2_IN_HA * acc.n_exp_harvest );
    #endif
    #ifdef  OutputGgcmiYearly_Item__NDEP
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(cbm::M2_IN_HA * ( acc.nh4_throughfall + acc.no3_throughfall ) );
    #endif
    #ifdef  OutputGgcmiYearly_Item__NFIX
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(cbm::M2_IN_HA * (acc.n2_fix_plant + acc.n_fix_bio + acc.n_fix_algae) );
    #endif
    #ifdef  OutputGgcmiYearly_Item__NFERT
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(cbm::M2_IN_HA * acc.n_fert );
    #endif
    #ifdef  OutputGgcmiYearly_Item__NUPT
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(cbm::M2_IN_HA * acc.n_uptake);
    #endif
    #ifdef  OutputGgcmiYearly_Item__NLIT_ABV
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(cbm::M2_IN_HA * acc.n_litter_above );
    #endif
    #ifdef  OutputGgcmiYearly_Item__NLIT_BLW
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(cbm::M2_IN_HA * acc.n_litter_below );
    #endif

    #ifdef  OutputGgcmiYearly_Item__CO2_HETERO
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(cbm::M2_IN_HA * acc.co2_emis_hetero );
    #endif
    #ifdef  OutputGgcmiYearly_Item__CO2_AUTO
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(cbm::M2_IN_HA * acc.co2_emis_auto );
    #endif
    #ifdef  OutputGgcmiYearly_Item__CH4
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(cbm::M2_IN_HA * acc.ch4_emis);
    #endif
    #ifdef  OutputGgcmiYearly_Item__CLEACH
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(cbm::M2_IN_HA * (acc.doc_leach + acc.ch4_leach) );
    #endif
    #ifdef  OutputGgcmiYearly_Item__CFERT
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(cbm::M2_IN_HA * acc.c_fert );
    #endif
    #ifdef  OutputGgcmiYearly_Item__CLIT_ABV
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(cbm::M2_IN_HA * acc.c_litter_above );
    #endif
    #ifdef  OutputGgcmiYearly_Item__CLIT_BLW
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(cbm::M2_IN_HA * acc.c_litter_below );
    #endif
    #ifdef  OutputGgcmiYearly_Item__CFIX
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(cbm::M2_IN_HA * acc.c_fix_algae );
    #endif

    #ifdef  OutputGgcmiYearly_Item_throughf
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(cbm::MM_IN_M * acc.throughfall );
    #endif
    #ifdef  OutputGgcmiYearly_Item_intercep
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(cbm::MM_IN_M * acc.interceptionevaporation );
    #endif
    #ifdef  OutputGgcmiYearly_Item_transp
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(cbm::MM_IN_M * acc.transpiration);
    #endif
    #ifdef  OutputGgcmiYearly_Item_evap
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(cbm::MM_IN_M * ( acc.soilevaporation + acc.surfacewaterevaporation));
    #endif
    #ifdef  OutputGgcmiYearly_Item_runoff
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(cbm::MM_IN_M * acc.runoff );
    #endif
    #ifdef  OutputGgcmiYearly_Item_perc
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(cbm::MM_IN_M * acc.percolation );
    #endif
    #ifdef  OutputGgcmiYearly_Item_irri
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(cbm::MM_IN_M * acc.irrigation);
    #endif


    return  LDNDC_ERR_OK;
}


#undef  LMOD_OUTPUT_MODULE_BASE
#undef  LMOD_OUTPUT_MODULE_NAME

#undef  OutputGgcmiYearly_Rank
#undef  OutputGgcmiYearly_Datasize

} /*namespace ldndc*/
