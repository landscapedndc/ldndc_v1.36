/*!
 * @brief
 *    daily outputs for ggcmi project
 *    for testing purposes (generates too much data to use for long global runs)
 *
 * @author
 *    Andrew Smerald
 */

#include  "output/output-ggcmi-daily.h"

#include  "constants/lconstants-plant.h"
#include  "constants/lconstants-conv.h"

#define  LMOD_OUTPUT_MODULE_BASE  MBE_LegacyOutputModel
#define  LMOD_OUTPUT_MODULE_NAME  OutputGgcmiDaily

LMOD_MODULE_INFO(LMOD_OUTPUT_MODULE_NAME,TMODE_POST_DAILY,LMOD_FLAG_USER|LMOD_FLAG_OUTPUT);
namespace ldndc {

#define  OutputGgcmiDaily_Item_crop_flag
#define  OutputGgcmiDaily_Item__dvs

#define  OutputGgcmiDaily_Item__CH4
#define  OutputGgcmiDaily_Item__CEMIS
#define  OutputGgcmiDaily_Item__NLEACH
#define  OutputGgcmiDaily_Item__N2O
#define  OutputGgcmiDaily_Item__N2
#define  OutputGgcmiDaily_Item__NOUT
#define  OutputGgcmiDaily_Item__NIN
#define  OutputGgcmiDaily_Item__NUPT

#define  OutputGgcmiDaily_Item_AET // actual evapo-transpiration
#define  OutputGgcmiDaily_Item_transp //transpiration
#define  OutputGgcmiDaily_Item_evap /*soil and surface water evaporation*/
#define  OutputGgcmiDaily_Item_runoff /*surface runoff*/
#define  OutputGgcmiDaily_Item_irri /*irrigation*/
#define  OutputGgcmiDaily_Item_availsoilwater /*soil water above wilting point*/

/*!
 * @page ggcmioutput
 * @section ggcmioutputdaily Daily GGCMI output
 *
 * entity name | decription | unit
 * ----------- | ---------- | ----
 * crop_flag | no crop  = 0, crop = 1 | -
 * dvs | crop maturity (no crop = -1) | -
 * dC_ch4_emis | Daily methane flux | [kgCha-1]
 * dC_emis | Daily flux of CH4 and CO2 | [kgCha-1]
 * dN_leach | Daily leaching of nitrate, ammonium and dissolved organic nitrogen |  [kgNha-1]
 * dN_n2o_emis | Daily nitrous oxide flux | [kgNha-1]
 * dN_n2_emis | Daily molecular nitrogen flux | [kgNha-1]
 * dN_out | Loss of reactive nitrogen due to leaching (NO3,NH4,DoN) and emission (N20, NO, N2, NH3)  | [kgNha-1]
 * dN_in| N input from deposition (NH4, NO3) and N2 fixation | [kgNha-1]
 * dN_up | Daily nitrogen uptake by plants from the litter layer and mineral soil | [kgNha-1]
 * AET| Daily sum of evaporation (soil, surface and from interception) and transpiration | [mm]
 * transp | daily transpiration | [mm]
 * evap | Daily evaporation from soil and surface water (not from interception) | [mm]
 * runoff | Daily lateral run-off | [mm]
 * irri | Daily addition of irrigation water | [mm]
 * soilwater | Total water in soil above wilting point | [mm]
 */


ldndc_string_t const  OutputGgcmiDaily_Ids[] =
{
#ifdef  OutputGgcmiDaily_Item_crop_flag
    "crop_flag",
#endif
#ifdef  OutputGgcmiDaily_Item__dvs
    "dvs",
#endif
#ifdef  OutputGgcmiDaily_Item__CH4
    "dC_ch4_emis",
#endif
#ifdef  OutputGgcmiDaily_Item__CEMIS
    "dC_emis",
#endif
#ifdef  OutputGgcmiDaily_Item__NLEACH
    "dN_leach",
#endif
#ifdef  OutputGgcmiDaily_Item__N2O
    "dN_n2o_emis",
#endif
#ifdef  OutputGgcmiDaily_Item__N2
    "dN_n2_emis",
#endif
#ifdef  OutputGgcmiDaily_Item__NOUT
    "dN_out",
#endif
#ifdef  OutputGgcmiDaily_Item__NIN
    "dN_in",
#endif
#ifdef  OutputGgcmiDaily_Item__NUPT
    "dN_up",
#endif
#ifdef  OutputGgcmiDaily_Item_AET
    "AET",
#endif
#ifdef  OutputGgcmiDaily_Item_transp
    "transp",
#endif
#ifdef  OutputGgcmiDaily_Item_evap
    "evap",
#endif
#ifdef  OutputGgcmiDaily_Item_runoff
    "runoff",
#endif
#ifdef  OutputGgcmiDaily_Item_irri
    "irri",
#endif
#ifdef  OutputGgcmiDaily_Item_availsoilwater
    "soilwater",
#endif
};

ldndc_string_t const  OutputGgcmiDaily_Header[] =
{
#ifdef  OutputGgcmiDaily_Item_crop_flag
    "crop_flag",
#endif
#ifdef  OutputGgcmiDaily_Item__dvs
    "dvs",
#endif
#ifdef  OutputGgcmiDaily_Item__CH4
    "dC_ch4_emis[kgCha-1]",
#endif
#ifdef  OutputGgcmiDaily_Item__CEMIS
    "dC_emis[kgCha-1]",
#endif
#ifdef  OutputGgcmiDaily_Item__NLEACH
    "dN_leach[kgNha-1]",
#endif
#ifdef  OutputGgcmiDaily_Item__N2O
    "dN_n2o_emis[kgNha-1]",
#endif
#ifdef  OutputGgcmiDaily_Item__N2
    "dN_n2_emis[kgNha-1]",
#endif
#ifdef  OutputGgcmiDaily_Item__NOUT
    "dN_out[kgNha-1]",
#endif
#ifdef  OutputGgcmiDaily_Item__NIN
    "dN_in[kgNha-1]",
#endif
#ifdef  OutputGgcmiDaily_Item__NUPT
    "dN_up[kgNha-1]",
#endif
#ifdef  OutputGgcmiDaily_Item_AET
    "AET[mm]",
#endif
#ifdef  OutputGgcmiDaily_Item_transp
    "transp[mm]",
#endif
#ifdef  OutputGgcmiDaily_Item_evap
    "evap[mm]",
#endif
#ifdef  OutputGgcmiDaily_Item_runoff
    "runoff[mm]",
#endif
#ifdef  OutputGgcmiDaily_Item_irri
    "irri[mm]",
#endif
#ifdef  OutputGgcmiDaily_Item_availsoilwater
    "soilwater[mm]",
#endif
};

#define  OutputGgcmiDaily_Datasize  (sizeof( OutputGgcmiDaily_Header) / sizeof( OutputGgcmiDaily_Header[0]))
ldndc_output_size_t const  OutputGgcmiDaily_Sizes[] =
{
    OutputGgcmiDaily_Datasize,

    OutputGgcmiDaily_Datasize /*total size*/
};
ldndc_output_size_t const *  OutputGgcmiDaily_EntitySizes = NULL;
#define  OutputGgcmiDaily_Rank  ((ldndc_output_rank_t)(sizeof( OutputGgcmiDaily_Sizes) / sizeof( OutputGgcmiDaily_Sizes[0])) - 1)
atomic_datatype_t const  OutputGgcmiDaily_Types[] =
{
    LDNDC_FLOAT64
};


LMOD_OUTPUT_MODULE_NAME::LMOD_OUTPUT_MODULE_NAME(
          MoBiLE_State *  _state,
          cbm::io_kcomm_t *  _io_kcomm,
          timemode_e  _timemode)
          : LMOD_OUTPUT_MODULE_BASE( _state, _io_kcomm, _timemode),

          io_kcomm( _io_kcomm),

          soillayers_in( _io_kcomm->get_input_class< input_class_soillayers_t >()),

          phys( _state->get_substate< substate_physiology_t >()),
          soilchem( _state->get_substate< substate_soilchemistry_t >()),
          water( _state->get_substate< substate_watercycle_t >()),
          m_veg( &_state->vegetation)
{
    push_accumulated_outputs();
}


LMOD_OUTPUT_MODULE_NAME::~LMOD_OUTPUT_MODULE_NAME()
{
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::configure(
                ldndc::config_file_t const *  _cf)
{
    lerr_t  rc_setflags =
        set_metaflags( _cf, RM_DEFAULT_DAILY);
    if ( rc_setflags)
        { return  LDNDC_ERR_FAIL; }

    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::initialize()
{
    m_sink = io_kcomm->sink_handle_acquire( "ggcmidaily");
    if ( m_sink.status() == LDNDC_ERR_OK)
    {
        lerr_t  rc_layout = LDNDC_OUTPUT_DEFINE_SINK_LAYOUT_DEFAULTS(
            m_sink,OutputGgcmiDaily);
        RETURN_IF_NOT_OK(rc_layout);
    }
    else
    {
        KLOGERROR( "sink status bad  [sink=","ggcmidaily","]");
        return  m_sink.status();
    }

    push_accumulated_outputs();
    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::solve()
{
    ldndc_flt64_t  data_flt64_0[OutputGgcmiDaily_Datasize];

    pop_accumulated_outputs();
    lerr_t  rc_dump = dump_0( data_flt64_0);
    push_accumulated_outputs();

    if ( rc_dump)
        { return  LDNDC_ERR_FAIL; }

    void *  data[] = { data_flt64_0};
    lerr_t  rc_write =
        write_fixed_record( &m_sink, data);
    if ( rc_write)
        { return  LDNDC_ERR_FAIL; }

    return  LDNDC_ERR_OK;
}


void
LMOD_OUTPUT_MODULE_NAME::pop_accumulated_outputs()
{
    acc.nh4_uptake = phys->accumulated_nh4_uptake_sl.sum() - acc.nh4_uptake;
    acc.no3_uptake = phys->accumulated_no3_uptake_sl.sum() - acc.no3_uptake;
    acc.don_uptake = phys->accumulated_don_uptake_sl.sum() - acc.don_uptake;

    acc.nh4_throughfall =
               phys->accumulated_nh4_throughfall - acc.nh4_throughfall;
    acc.no3_throughfall =
               phys->accumulated_no3_throughfall - acc.no3_throughfall;

    acc.don_leach =
               soilchem->accumulated_don_leach - acc.don_leach;
    acc.no3_leach =
               soilchem->accumulated_no3_leach - acc.no3_leach;
    acc.nh4_leach =
               soilchem->accumulated_nh4_leach - acc.nh4_leach;

    acc.ch4_emis =
               soilchem->accumulated_ch4_emis - acc.ch4_emis;
    acc.co2_emis_auto =
               soilchem->accumulated_co2_emis_auto - acc.co2_emis_auto;
    acc.co2_emis_hetero =
               soilchem->accumulated_co2_emis_hetero - acc.co2_emis_hetero;
    acc.no_emis =
               soilchem->accumulated_no_emis - acc.no_emis;
    acc.n2o_emis =
               soilchem->accumulated_n2o_emis - acc.n2o_emis;
    acc.n2_emis =
               soilchem->accumulated_n2_emis - acc.n2_emis;
    acc.nh3_emis =
               soilchem->accumulated_nh3_emis - acc.nh3_emis;

    acc.n2_fix_algae =
               soilchem->accumulated_n_fix_algae - acc.n2_fix_algae;
    acc.n2_fix_bio =
               soilchem->accumulated_n2_fixation - acc.n2_fix_bio;

    acc.transpiration =
               water->accumulated_transpiration_sl.sum() - acc.transpiration;
    acc.interceptionevaporation =
               water->accumulated_interceptionevaporation - acc.interceptionevaporation;
    acc.soilevaporation =
               water->accumulated_soilevaporation - acc.soilevaporation;
    acc.surfacewaterevaporation =
               water->accumulated_surfacewaterevaporation - acc.surfacewaterevaporation;
    acc.runoff =
               water->accumulated_runoff - acc.runoff;
    acc.irrigation =
               water->accumulated_irrigation_ggcmi - acc.irrigation;
}


void
LMOD_OUTPUT_MODULE_NAME::push_accumulated_outputs()
{
    acc.nh4_uptake = phys->accumulated_nh4_uptake_sl.sum();
    acc.no3_uptake = phys->accumulated_no3_uptake_sl.sum();
    acc.don_uptake = phys->accumulated_don_uptake_sl.sum();

    acc.nh4_throughfall = phys->accumulated_nh4_throughfall;
    acc.no3_throughfall = phys->accumulated_no3_throughfall;

    acc.don_leach = soilchem->accumulated_don_leach;
    acc.no3_leach = soilchem->accumulated_no3_leach;
    acc.nh4_leach = soilchem->accumulated_nh4_leach;

    acc.ch4_emis = soilchem->accumulated_ch4_emis;
    acc.co2_emis_auto = soilchem->accumulated_co2_emis_auto;
    acc.co2_emis_hetero = soilchem->accumulated_co2_emis_hetero;
    acc.no_emis = soilchem->accumulated_no_emis;
    acc.n2o_emis = soilchem->accumulated_n2o_emis;
    acc.n2_emis = soilchem->accumulated_n2_emis;
    acc.nh3_emis = soilchem->accumulated_nh3_emis;

    acc.n2_fix_algae = soilchem->accumulated_n_fix_algae;
    acc.n2_fix_bio = soilchem->accumulated_n2_fixation;

    acc.transpiration = water->accumulated_transpiration_sl.sum();
    acc.interceptionevaporation = water->accumulated_interceptionevaporation;
    acc.soilevaporation = water->accumulated_soilevaporation;
    acc.surfacewaterevaporation = water->accumulated_surfacewaterevaporation;
    acc.runoff = water->accumulated_runoff;
    acc.irrigation = water->accumulated_irrigation_ggcmi;
}

lerr_t
LMOD_OUTPUT_MODULE_NAME::finalize()
{
    io_kcomm->sink_handle_release( &m_sink);
    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::dump_0(
        ldndc_flt64_t *  _buf)
{
    //calculate soil water above wilting point
    double soilwater_avail( 0.0);
    for ( size_t  sl = 0;  sl < soillayers_in->soil_layer_cnt();  ++sl)
    {
        soilwater_avail += ( water->wc_sl[sl] - soilchem->wcmin_sl[sl]) * soilchem->h_sl[sl];
    }

    LDNDC_OUTPUT_SET_COLUMN_INDEX(0);

#ifdef  OutputGgcmiDaily_Item_crop_flag
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( m_veg->size() );
#endif
#ifdef  OutputGgcmiDaily_Item__dvs
    if (m_veg->size() == 1)
      {LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( (*m_veg->begin())->dvsFlush );}
    else
      {LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( -1 );}
#endif
#ifdef  OutputGgcmiDaily_Item__CH4
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(cbm::M2_IN_HA * acc.ch4_emis);
#endif
#ifdef  OutputGgcmiDaily_Item__CEMIS
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(cbm::M2_IN_HA * ( acc.co2_emis_auto + acc.co2_emis_hetero + acc.ch4_emis));
#endif
#ifdef  OutputGgcmiDaily_Item__NLEACH
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(cbm::M2_IN_HA * ( acc.don_leach + acc.no3_leach + acc.nh4_leach));
#endif
#ifdef  OutputGgcmiDaily_Item__N2O
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(cbm::M2_IN_HA * acc.n2o_emis);
#endif
#ifdef  OutputGgcmiDaily_Item__N2
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(cbm::M2_IN_HA * acc.n2_emis);
#endif
#ifdef  OutputGgcmiDaily_Item__NOUT
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(cbm::M2_IN_HA * ( acc.don_leach + acc.no3_leach + acc.nh4_leach + acc.nh3_emis + acc.n2o_emis + acc.no_emis + acc.n2_emis));
#endif
#ifdef  OutputGgcmiDaily_Item__NIN
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(cbm::M2_IN_HA * ( acc.n2_fix_algae + acc.n2_fix_bio + acc.nh4_throughfall + acc.no3_throughfall));
#endif
#ifdef  OutputGgcmiDaily_Item__NUPT
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(cbm::M2_IN_HA * (acc.nh4_uptake + acc.no3_uptake + acc.don_uptake));
#endif
#ifdef  OutputGgcmiDaily_Item_AET
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(cbm::MM_IN_M * ( acc.transpiration + acc.interceptionevaporation + acc.soilevaporation + acc.surfacewaterevaporation));
#endif
#ifdef  OutputGgcmiDaily_Item_transp
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(cbm::MM_IN_M * acc.transpiration);
#endif
#ifdef  OutputGgcmiDaily_Item_evap
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(cbm::MM_IN_M * ( acc.soilevaporation + acc.surfacewaterevaporation));
#endif
#ifdef  OutputGgcmiDaily_Item_runoff
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(cbm::MM_IN_M * ( acc.runoff + acc.percolation ));
#endif
#ifdef  OutputGgcmiDaily_Item_irri
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(cbm::MM_IN_M * acc.irrigation);
#endif
#ifdef  OutputGgcmiDaily_Item_availsoilwater
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(cbm::MM_IN_M * soilwater_avail);
#endif
    return  LDNDC_ERR_OK;
}


#undef  LMOD_OUTPUT_MODULE_BASE
#undef  LMOD_OUTPUT_MODULE_NAME

#undef  OutputGgcmiDaily_Rank
#undef  OutputGgcmiDaily_Datasize

} /*namespace ldndc*/
