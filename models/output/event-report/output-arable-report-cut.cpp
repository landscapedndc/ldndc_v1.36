/*!
 * @brief
 *
 * @author
 *  - Steffen Klatt (created on: aug 27, 2012),
 *  - Edwin Haas
 *  - David Kraus
 */

#include  "output/event-report/output-arable-report-cut.h"

#include  "constants/lconstants-conv.h"
#include  "constants/lconstants-phys.h"

#define  LMOD_OUTPUT_MODULE_BASE  MBE_LegacyOutputModel
#define  LMOD_OUTPUT_MODULE_NAME  OutputArableReportCut
#define  LMOD_OUTPUT_MODULE_TIMEMODE  TMODE_SUBDAILY|TMODE_POST_DAILY

#ifdef  LDNDC_OUTPUT_HAVE_DUMMY_LINES
//#  define  LDNDC_OUTPUT_CUT_HAVE_DUMMY_LINES  1
#endif

LMOD_MODULE_INFO(LMOD_OUTPUT_MODULE_NAME,LMOD_OUTPUT_MODULE_TIMEMODE,LMOD_FLAG_USER|LMOD_FLAG_OUTPUT);

/*!
 * @page managementoutput
 * @section cuttingoutput Cutting output
 *
 * entity name | decription | unit
 * ----------- | ---------- | ----
 * dC\_fru\_export | Daily carbon of plant fruits exported from field due to cutting | [KgCha-1]
 * dC\_fol\_export | Daily carbon of plant foliage exported from field due to cutting | [KgCha-1]
 * dC\_dfol\_export | Daily carbon of dead plant foliage exported from field due to cutting | [KgCha-1]
 * dC\_lst\_export | Daily carbon of plant living structural tissue exported from field due to cutting | [KgCha-1]
 * dC\_dst\_export | Daily carbon of plant dead structural tissue exported from field due to cutting | [KgCha-1]
 * dC\_above\_export | Daily carbon of plant above ground exported from field due to cutting | [KgCha-1]
 * dC\_frt\_export | Daily carbon of plant fine roots exported from field due to cutting | [KgCha-1]
 * dC\_fru\_remain | Daily carbon of plant fruits remaining at the field after to cutting | [KgCha-1]
 * dC\_fol\_remain | Daily carbon of plant foliage remaining at the field after to cutting | [KgCha-1]
 * dC\_lst\_remain | Daily carbon of plant living structural tissue remaining at the field after to cutting | [KgCha-1]
 * dC\_dst\_remain | Daily carbon of plant dead structural tissue remaining at the field after to cutting | [KgCha-1]
 * dC\_frt\_remain | Daily carbon of plant fine roots remaining at the field after to cutting | [KgCha-1]
 * dC\_fru\_litter | Daily carbon of plant fruits allocated to soil due to cutting | [KgCha-1]
 * dC\_fol\_litter | Daily carbon of plant foliage allocated to soil due to cutting | [KgCha-1]
 * dC\_lst\_litter | Daily carbon of plant living structural tissue allocated to soil due to cutting | [KgCha-1]
 * dC\_dst\_litter | Daily carbon of plant dead structural tissue allocated to soil due to cutting | [KgCha-1]
 * dC\_frt\_litter | Daily carbon of plant fine roots allocated to soil due to cutting | [KgCha-1]
 * dN\_fru\_export | Daily nitrogen of plant fruits exported from field due to cutting | [KgNha-1]
 * dN\_fol\_export | Daily nitrogen of plant foliage exported from field due to cutting | [KgNha-1]
 * dN\_lst\_export | Daily nitrogen of plant living structural tissue exported from field due to cutting | [KgNha-1]
 * dN\_dst\_export | Daily nitrogen of plant dead structural tissue exported from field due to cutting | [KgNha-1]
 * dN\_above\_export | Daily nitrogen of plant above ground exported from field due to cutting | [KgNha-1]
 * dN\_frt\_export | Daily nitrogen of plant fineroots exported from field due to cutting | [KgNha-1]
 * dN\_fru\_remain | Daily nitrogen of plant fruits remaining at the field after to cutting | [KgNha-1]
 * dN\_fol\_remain | Daily nitrogen of plant foliage remaining at the field after to cutting | [KgNha-1]
 * dN\_lst\_remain | Daily nitrogen of plant living structural tissue remaining at the field after to cutting | [KgNha-1]
 * dN\_dst\_remain | Daily nitrogen of plant dead structural tissue remaining at the field after to cutting | [KgNha-1]
 * dN\_frt\_remain | Daily nitrogen of plant fine roots remaining at the field after to cutting | [KgNha-1]
 * dN\_fru\_litter | Daily nitrogen of plant fruits allocated to soil due to cutting | [KgNha-1]
 * dN\_fol\_litter | Daily nitrogen of plant foliage allocated to soil due to cutting | [KgNha-1]
 * dN\_lst\_litter | Daily nitrogen of plant living structural tissue allocated to soil due to cutting | [KgNha-1]
 * dN\_dst\_litter | Daily nitrogen of plant dead structural tissue allocated to soil due to cutting | [KgNha-1]
 * dN\_frt\_litter | Daily nitrogen of plant fineroots allocated to soil due to cutting | [KgNha-1]
 */
namespace ldndc {

/* CUT */
ldndc_string_t const  OutputArableReportCut_Ids[] =
{
    "dC_fru_export", "dC_fol_export", "dC_dfol_export", "dC_lst_export", "dC_dst_export", "dC_above_export", "dC_frt_export",
    "dC_fru_remain", "dC_fol_remain", "dC_dfol_remain", "dC_lst_remain", "dC_dst_remain", "dC_above_remain", "dC_frt_remain",
    "dC_fru_litter", "dC_fol_litter", "dC_dfol_litter", "dC_lst_litter", "dC_dst_litter", "dC_above_litter", "dC_frt_litter",
    "dN_fru_export", "dN_fol_export", "dN_dfol_export", "dN_lst_export", "dN_dst_export", "dN_above_export", "dN_frt_export",
    "dN_fru_remain", "dN_fol_remain", "dN_dfol_remain", "dN_lst_remain", "dN_dst_remain", "dN_above_remain", "dN_frt_remain",
    "dN_fru_litter", "dN_fol_litter", "dN_dfol_litter", "dN_lst_litter", "dN_dst_litter", "dN_above_litter", "dN_frt_litter"
};
ldndc_string_t const  OutputArableReportCut_Header[] =
{
    "dC_fru_export[kgCha-1]", "dC_fol_export[kgCha-1]", "dC_dfol_export[kgCha-1]", "dC_lst_export[kgCha-1]", "dC_dst_export[kgCha-1]", "dC_above_export[kgCha-1]", "dC_frt_export[kgCha-1]",
    "dC_fru_remain[kgCha-1]", "dC_fol_remain[kgCha-1]", "dC_dfol_remain[kgCha-1]", "dC_lst_remain[kgCha-1]", "dC_dst_remain[kgCha-1]", "dC_above_remain[kgCha-1]", "dC_frt_remain[kgCha-1]",
    "dC_fru_litter[kgCha-1]", "dC_fol_litter[kgCha-1]", "dC_dfol_litter[kgCha-1]", "dC_lst_litter[kgCha-1]", "dC_dst_litter[kgCha-1]", "dC_above_litter[kgCha-1]", "dC_frt_litter[kgCha-1]",
    "dN_fru_export[kgNha-1]", "dN_fol_export[kgNha-1]", "dN_dfol_export[kgNha-1]", "dN_lst_export[kgNha-1]", "dN_dst_export[kgNha-1]", "dN_above_export[kgNha-1]", "dN_frt_export[kgNha-1]",
    "dN_fru_remain[kgNha-1]", "dN_fol_remain[kgNha-1]", "dN_dfol_remain[kgNha-1]", "dN_lst_remain[kgNha-1]", "dN_dst_remain[kgNha-1]", "dN_above_remain[kgNha-1]", "dN_frt_remain[kgNha-1]",
    "dN_fru_litter[kgNha-1]", "dN_fol_litter[kgNha-1]", "dN_dfol_litter[kgNha-1]", "dN_lst_litter[kgNha-1]", "dN_dst_litter[kgNha-1]", "dN_above_litter[kgNha-1]", "dN_frt_litter[kgNha-1]"
};
#define  OutputArableReportCut_Datasize  (sizeof( OutputArableReportCut_Header) / sizeof( OutputArableReportCut_Header[0]))
ldndc_output_size_t const  OutputArableReportCut_Sizes[] =
{
    OutputArableReportCut_Datasize,

    OutputArableReportCut_Datasize /*total size*/
};
ldndc_output_size_t const *  OutputArableReportCut_EntitySizes = NULL;
#define  OutputArableReportCut_Rank  ((ldndc_output_rank_t)(sizeof( OutputArableReportCut_Sizes) / sizeof( OutputArableReportCut_Sizes[0])) - 1)
atomic_datatype_t const  OutputArableReportCut_Types[] =
{
    LDNDC_FLOAT64
};

LMOD_OUTPUT_MODULE_NAME::LMOD_OUTPUT_MODULE_NAME(
        MoBiLE_State *  _state,
        cbm::io_kcomm_t *  _io_kcomm,
        timemode_e  _timemode)
    : LMOD_OUTPUT_MODULE_BASE( _state, _io_kcomm, _timemode),

    io_kcomm( _io_kcomm),
    m_veg( &_state->vegetation)
{
}


LMOD_OUTPUT_MODULE_NAME::~LMOD_OUTPUT_MODULE_NAME()
{
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::configure(
                ldndc::config_file_t const *  _cf)
{
    lerr_t  rc_setflags = this->set_metaflags( _cf);
    if ( rc_setflags)
        { return  rc_setflags; }

    int  producer_cut_timemode = TMODE_NONE;
    this->io_kcomm->get_scratch()->get( "producer.cut.timemode",
            &producer_cut_timemode, producer_cut_timemode);
    if ( producer_cut_timemode == TMODE_NONE)
    {
        KLOGWARN( "nobody sends me data :( going to sleep.");
        this->set_active_off();
        this->sleep();
    }
    else if ( this->timemode() != producer_cut_timemode)
    {
        timemode_e  tm =
            static_cast< timemode_e >( producer_cut_timemode);
        KLOGERROR( "oh no, i am in temporal disagreement with my producer :o",
                "  please select appropriate timemode",
                "  [expecting=",ldndc::time::timemode_name( tm),"]");
        return  LDNDC_ERR_FAIL;
    }
    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::initialize()
{
    this->m_sink = this->io_kcomm->sink_handle_acquire( "arablereportcut");
    if ( this->m_sink.status() == LDNDC_ERR_OK)
    {
        lerr_t  rc_layout = LDNDC_OUTPUT_DEFINE_SINK_LAYOUT_DEFAULTS(
                    this->m_sink,OutputArableReportCut);
        RETURN_IF_NOT_OK(rc_layout);
    }
    else
    {
        KLOGERROR( "sink status bad  [sink=","arablereportcut","]");
        return  this->m_sink.status();
    }

    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::solve()
{
    lerr_t  rc_dump = LDNDC_ERR_OK;
    if ( this->m_veg->size() > 0)
    {
        ldndc_flt64_t  data_flt64_0[OutputArableReportCut_Datasize];
        rc_dump = this->dump_cut_( data_flt64_0);
    }

    return  rc_dump;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::finalize()
{
    this->io_kcomm->sink_handle_release( &this->m_sink);
    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::dump_cut_( ldndc_flt64_t *  _buf)
{
    cbm::state_scratch_t *  mcom = this->io_kcomm->get_scratch();
    if ( !mcom->variable_exists( "cut:c_fru_export"))
    {
        /* we assume that all others are missing too */
        return  LDNDC_ERR_OK;
    }

    double  c_fru_export = 0.0,   c_fru_litter = 0.0,   c_fru_remain = 0.0;
    double  c_fol_export = 0.0,   c_fol_litter = 0.0,   c_fol_remain = 0.0;
    double  c_dfol_export = 0.0,  c_dfol_litter = 0.0,  c_dfol_remain = 0.0;
    double  c_lst_export = 0.0,   c_lst_litter = 0.0,   c_lst_remain = 0.0;
    double  c_dst_export = 0.0,   c_dst_litter = 0.0,   c_dst_remain = 0.0;
    double  c_above_export = 0.0, c_above_litter = 0.0, c_above_remain = 0.0;
    double  c_frt_export = 0.0,   c_frt_litter = 0.0,   c_frt_remain = 0.0;

    double  n_fru_export = 0.0,   n_fru_litter = 0.0,   n_fru_remain = 0.0;
    double  n_fol_export = 0.0,   n_fol_litter = 0.0,   n_fol_remain = 0.0;
    double  n_dfol_export = 0.0,  n_dfol_litter = 0.0,  n_dfol_remain = 0.0;
    double  n_lst_export = 0.0,   n_lst_litter = 0.0,   n_lst_remain = 0.0;
    double  n_dst_export = 0.0,   n_dst_litter = 0.0,   n_dst_remain = 0.0;
    double  n_above_export = 0.0, n_above_litter = 0.0, n_above_remain = 0.0;
    double  n_frt_export = 0.0,   n_frt_litter = 0.0,   n_frt_remain = 0.0;

    LDNDC_OUTPUT_SET_COLUMN_INDEX(0);

    /* carbon */
    mcom->get_and_remove( "cut:c_fru_export", &c_fru_export, 0.0);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( c_fru_export * cbm::M2_IN_HA);

    mcom->get_and_remove( "cut:c_fol_export", &c_fol_export, 0.0);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( c_fol_export * cbm::M2_IN_HA);

    mcom->get_and_remove( "cut:c_dfol_export", &c_dfol_export, 0.0);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( c_dfol_export * cbm::M2_IN_HA);

    mcom->get_and_remove( "cut:c_lst_export", &c_lst_export, 0.0);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( c_lst_export * cbm::M2_IN_HA);

    mcom->get_and_remove( "cut:c_dst_export", &c_dst_export, 0.0);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( c_dst_export * cbm::M2_IN_HA);

    mcom->get_and_remove( "cut:c_above_export", &c_above_export, 0.0);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( c_above_export * cbm::M2_IN_HA);

    mcom->get_and_remove( "cut:c_frt_export", &c_frt_export, 0.0);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( c_frt_export * cbm::M2_IN_HA);


    mcom->get_and_remove( "cut:c_fru_remain", &c_fru_remain, 0.0);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( c_fru_remain * cbm::M2_IN_HA);

    mcom->get_and_remove( "cut:c_fol_remain", &c_fol_remain, 0.0);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( c_fol_remain * cbm::M2_IN_HA);

    mcom->get_and_remove( "cut:c_dfol_remain", &c_dfol_remain, 0.0);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( c_dfol_remain * cbm::M2_IN_HA);

    mcom->get_and_remove( "cut:c_lst_remain", &c_lst_remain, 0.0);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( c_lst_remain * cbm::M2_IN_HA);

    mcom->get_and_remove( "cut:c_dst_remain", &c_dst_remain, 0.0);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( c_dst_remain * cbm::M2_IN_HA);

    mcom->get_and_remove( "cut:c_above_remain", &c_above_remain, 0.0);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( c_above_remain * cbm::M2_IN_HA);

    mcom->get_and_remove( "cut:c_frt_remain", &c_frt_remain, 0.0);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( c_frt_remain * cbm::M2_IN_HA);

    
    mcom->get_and_remove( "cut:c_fru_litter", &c_fru_litter, 0.0);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( c_fru_litter * cbm::M2_IN_HA);

    mcom->get_and_remove( "cut:c_fol_litter", &c_fol_litter, 0.0);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( c_fol_litter * cbm::M2_IN_HA);

    mcom->get_and_remove( "cut:c_dfol_litter", &c_dfol_litter, 0.0);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( c_dfol_litter * cbm::M2_IN_HA);

    mcom->get_and_remove( "cut:c_lst_litter", &c_lst_litter, 0.0);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( c_lst_litter * cbm::M2_IN_HA);

    mcom->get_and_remove( "cut:c_dst_litter", &c_dst_litter, 0.0);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( c_dst_litter * cbm::M2_IN_HA);

    mcom->get_and_remove( "cut:c_above_litter", &c_above_litter, 0.0);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( c_above_litter * cbm::M2_IN_HA);

    mcom->get_and_remove( "cut:c_frt_litter", &c_frt_litter, 0.0);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( c_frt_litter * cbm::M2_IN_HA);


    /* nitrogen */
    mcom->get_and_remove( "cut:n_fru_export", &n_fru_export, 0.0);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( n_fru_export * cbm::M2_IN_HA);

    mcom->get_and_remove( "cut:n_fol_export", &n_fol_export, 0.0);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( n_fol_export * cbm::M2_IN_HA);

    mcom->get_and_remove( "cut:n_dfol_export", &n_dfol_export, 0.0);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( n_dfol_export * cbm::M2_IN_HA);

    mcom->get_and_remove( "cut:n_lst_export", &n_lst_export, 0.0);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( n_lst_export * cbm::M2_IN_HA);

    mcom->get_and_remove( "cut:n_dst_export", &n_dst_export, 0.0);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( n_dst_export * cbm::M2_IN_HA);

    mcom->get_and_remove( "cut:n_above_export", &n_above_export, 0.0);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( n_above_export * cbm::M2_IN_HA);

    mcom->get_and_remove( "cut:n_frt_export", &n_frt_export, 0.0);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( n_frt_export * cbm::M2_IN_HA);

    
    mcom->get_and_remove( "cut:n_fru_remain", &n_fru_remain, 0.0);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( n_fru_remain * cbm::M2_IN_HA);

    mcom->get_and_remove( "cut:n_fol_remain", &n_fol_remain, 0.0);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( n_fol_remain * cbm::M2_IN_HA);

    mcom->get_and_remove( "cut:n_dfol_remain", &n_dfol_remain, 0.0);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( n_dfol_remain * cbm::M2_IN_HA);

    mcom->get_and_remove( "cut:n_lst_remain", &n_lst_remain, 0.0);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( n_lst_remain * cbm::M2_IN_HA);

    mcom->get_and_remove( "cut:n_dst_remain", &n_dst_remain, 0.0);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( n_dst_remain * cbm::M2_IN_HA);

    mcom->get_and_remove( "cut:n_above_remain", &n_above_remain, 0.0);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( n_above_remain * cbm::M2_IN_HA);

    mcom->get_and_remove( "cut:n_frt_remain", &n_frt_remain, 0.0);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( n_frt_remain * cbm::M2_IN_HA);


    mcom->get_and_remove( "cut:n_fru_litter", &n_fru_litter, 0.0);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( n_fru_litter * cbm::M2_IN_HA);

    mcom->get_and_remove( "cut:n_fol_litter", &n_fol_litter, 0.0);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( n_fol_litter * cbm::M2_IN_HA);

    mcom->get_and_remove( "cut:n_dfol_litter", &n_dfol_litter, 0.0);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( n_dfol_litter * cbm::M2_IN_HA);

    mcom->get_and_remove( "cut:n_lst_litter", &n_lst_litter, 0.0);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( n_lst_litter * cbm::M2_IN_HA);

    mcom->get_and_remove( "cut:n_dst_litter", &n_dst_litter, 0.0);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( n_dst_litter * cbm::M2_IN_HA);

    mcom->get_and_remove( "cut:n_above_litter", &n_above_litter, 0.0);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( n_above_litter * cbm::M2_IN_HA);

    mcom->get_and_remove( "cut:n_frt_litter", &n_frt_litter, 0.0);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( n_frt_litter * cbm::M2_IN_HA);

    void *  data[] = { _buf};
    this->write_fixed_record( &this->m_sink, data);

    return  LDNDC_ERR_OK;
}


#undef  LMOD_OUTPUT_MODULE_TIMEMODE
#undef  LMOD_OUTPUT_MODULE_BASE
#undef  LMOD_OUTPUT_MODULE_NAME

#undef  OutputArableReportCut_Rank
#undef  OutputArableReportCut_Datasize

} /*namespace ldndc*/
