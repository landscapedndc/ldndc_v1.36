/*!
 * @brief
 *
 * @author
 *  David Kraus
 *  Steffen Klatt
 */

#include  "output/event-report/output-event-report-throw.h"

#include  "constants/lconstants-conv.h"
#include  "constants/lconstants-phys.h"

#define  LMOD_OUTPUT_MODULE_BASE  MBE_LegacyOutputModel
#define  LMOD_OUTPUT_MODULE_NAME  OutputEventReportThrow
#define  LMOD_OUTPUT_MODULE_TIMEMODE  TMODE_SUBDAILY|TMODE_POST_DAILY

LMOD_MODULE_INFO(LMOD_OUTPUT_MODULE_NAME,LMOD_OUTPUT_MODULE_TIMEMODE,LMOD_FLAG_USER|LMOD_FLAG_OUTPUT);
namespace ldndc {

/* Throw */
ldndc_string_t const  OutputEventReportThrow_Ids[] =
{
    "species",
    "dDW_fol",
    "dDW_fru",
    "dDW_frt",
    "dDW_sap_below",
    "dDW_sap_above",
    "dDW_cor_below",
    "dDW_cor_above",
    "dN_fol",
    "dN_fru",
    "dN_frt",
    "dN_sap_below",
    "dN_sap_above",
    "dN_cor_below",
    "dN_cor_above"
};

ldndc_string_t const  OutputEventReportThrow_Header[] =
{
    "species",
    "dDW_fol[kgDWm-2]",
    "dDW_fru[kgDWm-2]",
    "dDW_frt[kgDWm-2]",
    "dDW_sap_below[kgDWm-2]",
    "dDW_sap_above[kgDWm-2]",
    "dDW_cor_below[kgDWm-2]",
    "dDW_cor_above[kgDWm-2]",

    "dN_fol[kgNm-2]",
    "dN_fru[kgNm-2]",
    "dN_frt[kgNm-2]",
    "dN_sap_below[kgNm-2]",
    "dN_sap_above[kgNm-2]",
    "dN_cor_below[kgNm-2]",
    "dN_cor_above[kgNm-2]"
};

    
#define  OutputEventReportThrow_Datasize  (sizeof( OutputEventReportThrow_Header) / sizeof( OutputEventReportThrow_Header[0]))
ldndc_output_size_t const  OutputEventReportThrow_Sizes[] =
{
    1,
    OutputEventReportThrow_Datasize - 1/*species name*/,

    OutputEventReportThrow_Datasize /*total size*/
};
ldndc_output_size_t const *  OutputEventReportThrow_EntitySizes = NULL;
#define  OutputEventReportThrow_Rank  ((ldndc_output_rank_t)(sizeof( OutputEventReportThrow_Sizes) / sizeof( OutputEventReportThrow_Sizes[0])) - 1)
atomic_datatype_t const  OutputEventReportThrow_Types[] =
{
    LDNDC_STRING,
    LDNDC_FLOAT64
};

LMOD_OUTPUT_MODULE_NAME::LMOD_OUTPUT_MODULE_NAME(
        MoBiLE_State *  _state,
        cbm::io_kcomm_t *  _io_kcomm,
        timemode_e  _timemode)
        : LMOD_OUTPUT_MODULE_BASE( _state, _io_kcomm, _timemode),

        io_kcomm( _io_kcomm),
        m_veg( &_state->vegetation)
{
}


LMOD_OUTPUT_MODULE_NAME::~LMOD_OUTPUT_MODULE_NAME()
{
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::configure(
            ldndc::config_file_t const *  _cf)
{
    lerr_t  rc_setflags = this->set_metaflags( _cf);
    return  rc_setflags;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::initialize()
{
    this->m_sink = this->io_kcomm->sink_handle_acquire( "eventreportthrow");
    if ( this->m_sink.status() == LDNDC_ERR_OK)
    {
        lerr_t  rc_layout = LDNDC_OUTPUT_DEFINE_SINK_LAYOUT_DEFAULTS(
                    this->m_sink,OutputEventReportThrow);
        RETURN_IF_NOT_OK(rc_layout);
    }
    else
    {
        KLOGERROR( "sink status bad  [sink=","eventreportthrow","]");
        return  this->m_sink.status();
    }

    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::solve()
{
    lerr_t  rc_dump = LDNDC_ERR_OK;
    if ( this->m_veg->size() > 0)
    {
        ldndc_flt64_t ldndc_flt64_0[OutputEventReportThrow_Datasize];

        while ( this->m_ThrowEvents)
        {
            EventAttributes  ev_throw = this->m_ThrowEvents.pop();
            char const *  species_name = ev_throw.get( "/name", "?");

            if ( cbm::is_equal_i( species_name, "*"))
            {
                for ( PlantIterator vt = this->m_veg->begin(); vt != this->m_veg->end(); ++vt)
                {
                    rc_dump = this->dump_throw_( (*vt)->cname(), ldndc_flt64_0);
                }
            }
            else
            {
                rc_dump = this->dump_throw_( species_name, ldndc_flt64_0);
            }
        }
    }

    return  rc_dump;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::finalize()
{
    this->io_kcomm->sink_handle_release( &this->m_sink);
    return  LDNDC_ERR_OK;
}





lerr_t
LMOD_OUTPUT_MODULE_NAME::dump_throw_(
                                     char const * _species_name,
                                     ldndc_flt64_t *  _buf)
{
    cbm::state_scratch_t *  mcom = this->io_kcomm->get_scratch();
    std::string  mcom_key;

    LDNDC_OUTPUT_SET_COLUMN_INDEX(0);

    double  val = 0.0;

    cbm::state_scratch_t::make_item_key(  &mcom_key, "throw:%s:m_fol", _species_name);
    mcom->get_and_remove( mcom_key.c_str(), &val, 0.0);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( val);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "throw:%s:m_fru", _species_name);
    mcom->get_and_remove( mcom_key.c_str(), &val, 0.0);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( val);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "throw:%s:m_frt", _species_name);
    mcom->get_and_remove( mcom_key.c_str(), &val, 0.0);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( val);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "throw:%s:m_sap_below", _species_name);
    mcom->get_and_remove( mcom_key.c_str(), &val, 0.0);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( val);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "throw:%s:m_sap_above", _species_name);
    mcom->get_and_remove( mcom_key.c_str(), &val, 0.0);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( val);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "throw:%s:m_cor_below", _species_name);
    mcom->get_and_remove( mcom_key.c_str(), &val, 0.0);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( val);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "throw:%s:m_cor_above", _species_name);
    mcom->get_and_remove( mcom_key.c_str(), &val, 0.0);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( val);



    cbm::state_scratch_t::make_item_key(  &mcom_key, "throw:%s:n_fol", _species_name);
    mcom->get_and_remove( mcom_key.c_str(), &val, 0.0);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( val);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "throw:%s:n_fru", _species_name);
    mcom->get_and_remove( mcom_key.c_str(), &val, 0.0);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( val);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "throw:%s:n_frt", _species_name);
    mcom->get_and_remove( mcom_key.c_str(), &val, 0.0);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( val);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "throw:%s:n_sap_below", _species_name);
    mcom->get_and_remove( mcom_key.c_str(), &val, 0.0);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( val);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "throw:%s:n_sap_above", _species_name);
    mcom->get_and_remove( mcom_key.c_str(), &val, 0.0);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( val);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "throw:%s:n_cor_below", _species_name);
    mcom->get_and_remove( mcom_key.c_str(), &val, 0.0);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( val);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "throw:%s:n_cor_above", _species_name);
    mcom->get_and_remove( mcom_key.c_str(), &val, 0.0);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( val);

    /* species name */
    ldndc_string_t  data_string[1];
    cbm::as_strcpy( &data_string[0], _species_name);

    void *  data[] = { data_string, _buf};

    lerr_t rc_write =
    this->write_fixed_record( &this->m_sink, data);
    if ( rc_write)
    { return LDNDC_ERR_FAIL; }

    return  LDNDC_ERR_OK;
}


static int  _QueueEventThrow( void const *  _msg, size_t  _msg_sz, void *  _queue)
{
    ldndc::EventQueue *  queue = static_cast< ldndc::EventQueue * >( _queue);
    if ( !queue)
    { return -1; }

    ldndc::EventAttributes  throw_event( "throw", (char const *)_msg, _msg_sz);
    queue->push( throw_event);
    return 0;
}

lerr_t
OutputEventReportThrow::register_ports( cbm::io_kcomm_t *  _io_kcomm)
{
    CBM_Callback  cb_throw;
    cb_throw.fn = &_QueueEventThrow;
    cb_throw.data = &this->m_ThrowEvents;
    this->m_ThrowHandle = _io_kcomm->subscribe_event( "throw", cb_throw);

    if ( !CBM_HandleOk(this->m_ThrowHandle))
    { return  LDNDC_ERR_FAIL; }

    return  LDNDC_ERR_OK;
}

lerr_t
OutputEventReportThrow::unregister_ports( cbm::io_kcomm_t *  _io_kcomm)
{
    _io_kcomm->unsubscribe_event( this->m_ThrowHandle);

    return  LDNDC_ERR_OK;
}

#undef  LMOD_OUTPUT_MODULE_TIMEMODE
#undef  LMOD_OUTPUT_MODULE_BASE
#undef  LMOD_OUTPUT_MODULE_NAME

#undef  OutputEventReportThrow_Rank
#undef  OutputEventReportThrow_Datasize

} /*namespace ldndc*/

