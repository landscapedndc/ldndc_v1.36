/*!
 * @brief
 *
 * @author
 *    steffen klatt (created on: aug 27, 2012),
 *    edwin haas
 */

#include  "output/event-report/output-arable-report-harvest.h"

#include  "constants/lconstants-conv.h"
#include  "constants/lconstants-phys.h"

#define  LMOD_OUTPUT_MODULE_BASE  MBE_LegacyOutputModel
#define  LMOD_OUTPUT_MODULE_NAME  OutputArableReportHarvest
#define  LMOD_OUTPUT_MODULE_TIMEMODE  TMODE_SUBDAILY|TMODE_POST_DAILY

#ifdef  LDNDC_OUTPUT_HAVE_DUMMY_LINES
//#  define  LDNDC_OUTPUT_HARVEST_HAVE_DUMMY_LINES  1
#endif

LMOD_MODULE_INFO(LMOD_OUTPUT_MODULE_NAME,LMOD_OUTPUT_MODULE_TIMEMODE,LMOD_FLAG_USER|LMOD_FLAG_OUTPUT);

/*!
 * @page managementoutput
 * @section harvestoutput Harvest output
 *
 * entity name | decription | unit
 * ----------- | ---------- | ----
 * dC\_fru | Carbon of fruit (yield) at harvest | [KgCha-1]
 * dC\_fru\_export | Carbon of fruit (yield) exported after harvest | [KgCha-1]
 * dC\_straw | Carbon of straw at harvest | [KgCha-1]
 * dC\_straw\_export | Carbon of straw exported after harvest | [KgCha-1]
 * dC\_rootlitter | Carbon of roots at harvest | [KgCha-1]
 * dN\_fru | Nitrogen of fruit (yield) at harvest | [KgCha-1]
 * dN\_fru\_export | Nitrogen of fruit (yield) exported after harvest | [KgNha-1]
 * dN\_straw | Nitrogen of straw at harvest | [KgNha-1]
 * dN\_straw\_export | Nitrogen of straw exported after harvest | [KgNha-1]
 * dN\_rootlitter | Nitrogen of roots at harvest | [KgNha-1]
 */

namespace ldndc {

/* HARVEST */
ldndc_string_t const  OutputArableReportHarvest_Ids[] =
{
    "species", "days_on_field", "temp_cum", "dvs_flush",
    "dC_fru", "dC_fru_export",
    "dC_fol", "dC_fol_export",
    "dC_frt", "dC_frt_export",
    "dC_lst_above", "dC_lst_above_export",
    "dC_lst_below", "dC_lst_below_export",
    "dC_dst_above", "dC_dst_above_export",
    "dC_dst_below", "dC_dst_below_export",
    "dC_straw", "dC_straw_export", "dC_stubble",

    "dN_fru", "dN_fru_export",
    "dN_fol", "dN_fol_export",
    "dN_frt", "dN_frt_export",
    "dN_lst_above", "dN_lst_above_export",
    "dN_lst_below", "dN_lst_below_export",
    "dN_dst_above", "dN_dst_above_export",
    "dN_dst_below", "dN_dst_below_export",
    "dN_straw", "dN_straw_export", "dN_stubble"
};
ldndc_string_t const  OutputArableReportHarvest_Header[] =
{
    "species", "days_on_field", "temp_cum", "dvs_flush",

    "dC_fru[kgCha-1]", "dC_fru_export[kgCha-1]",
    "dC_fol[kgCha-1]", "dC_fol_export[kgCha-1]",
    "dC_frt[kgCha-1]", "dC_frt_export[kgCha-1]",
    "dC_lst_above[kgCha-1]", "dC_lst_above_export[kgCha-1]",
    "dC_lst_below[kgCha-1]", "dC_lst_below_export[kgCha-1]",
    "dC_dst_above[kgCha-1]", "dC_dst_above_export[kgCha-1]",
    "dC_dst_below[kgCha-1]", "dC_dst_below_export[kgCha-1]",
    "dC_straw[kgCha-1]", "dC_straw_export[kgCha-1]", "dC_stubble[kgCha-1]",
    
    "dN_fru[kgNha-1]", "dN_fru_export[kgNha-1]",
    "dN_fol[kgNha-1]", "dN_fol_export[kgNha-1]",
    "dN_frt[kgNha-1]", "dN_frt_export[kgNha-1]",
    "dN_lst_above[kgNha-1]", "dN_lst_above_export[kgNha-1]",
    "dN_lst_below[kgNha-1]", "dN_lst_below_export[kgNha-1]",
    "dN_dst_above[kgNha-1]", "dN_dst_above_export[kgNha-1]",
    "dN_dst_below[kgNha-1]", "dN_dst_below_export[kgNha-1]",
    "dN_straw[kgNha-1]", "dN_straw_export[kgNha-1]", "dN_stubble[kgNha-1]"
};
#define  OutputArableReportHarvest_Datasize  (sizeof( OutputArableReportHarvest_Header) / sizeof( OutputArableReportHarvest_Header[0]))
ldndc_output_size_t const  OutputArableReportHarvest_Sizes[] =
{
    1,
    OutputArableReportHarvest_Datasize - 1/*species name*/,

    OutputArableReportHarvest_Datasize /*total size*/
};
ldndc_output_size_t const *  OutputArableReportHarvest_EntitySizes = NULL;
#define  OutputArableReportHarvest_Rank  ((ldndc_output_rank_t)(sizeof( OutputArableReportHarvest_Sizes) / sizeof( OutputArableReportHarvest_Sizes[0])) - 1)
atomic_datatype_t const  OutputArableReportHarvest_Types[] =
{
    LDNDC_STRING,
    LDNDC_FLOAT64
};

LMOD_OUTPUT_MODULE_NAME::LMOD_OUTPUT_MODULE_NAME(
        MoBiLE_State *  _state,
        cbm::io_kcomm_t *  _io_kcomm,
        timemode_e  _timemode)
        : LMOD_OUTPUT_MODULE_BASE( _state, _io_kcomm, _timemode),

        io_kcomm( _io_kcomm),
        m_veg( &_state->vegetation)
{
}


LMOD_OUTPUT_MODULE_NAME::~LMOD_OUTPUT_MODULE_NAME()
{
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::configure(
            ldndc::config_file_t const *  _cf)
{
    lerr_t  rc_setflags = this->set_metaflags( _cf);
    return  rc_setflags;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::initialize()
{
    this->m_sink = this->io_kcomm->sink_handle_acquire( "arablereportharvest");
    if ( this->m_sink.status() == LDNDC_ERR_OK)
    {
        lerr_t  rc_layout = LDNDC_OUTPUT_DEFINE_SINK_LAYOUT_DEFAULTS(
                    this->m_sink,OutputArableReportHarvest);
        RETURN_IF_NOT_OK(rc_layout);
    }
    else
    {
        KLOGERROR( "sink status bad  [sink=","arablereportharvest","]");
        return  this->m_sink.status();
    }

    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::solve()
{
    lerr_t  rc_dump = LDNDC_ERR_OK;
    if ( this->m_veg->size() > 0)
    {
        ldndc_flt64_t  data_flt64_0[OutputArableReportHarvest_Datasize];
        rc_dump = this->dump_harvest_( data_flt64_0);
    }

    return  rc_dump;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::finalize()
{
    this->io_kcomm->sink_handle_release( &this->m_sink);
    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::dump_harvest_( ldndc_flt64_t *  _buf)
{
    cbm::state_scratch_t *  mcom = this->io_kcomm->get_scratch();

    std::string  mcom_key;
    static double const  C = cbm::M2_IN_HA;

    while ( this->m_HarvestEvents)
    {
        EventAttributes  ev_harv = this->m_HarvestEvents.pop();
        char const *  species_name = ev_harv.get( "/name", "?");

        LDNDC_OUTPUT_SET_COLUMN_INDEX(0);

        double  val = 0.0;

        /* do not write to file if harvest was not handled (we do delete potentially available keys though) */
        cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:c_fru", species_name);
        bool const  species_reported = mcom->variable_exists( mcom_key.c_str());

        cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:daysOnField", species_name);
        mcom->get_and_remove( mcom_key.c_str(), &val, invalid_dbl);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(val);

        cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:gddsum", species_name);
        mcom->get_and_remove( mcom_key.c_str(), &val, 0.0);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(val);

        cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:dvsflush", species_name);
        mcom->get_and_remove( mcom_key.c_str(), &val, 0.0);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(val);

         /* CARBON */
        cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:c_fru", species_name);
        mcom->get_and_remove( mcom_key.c_str(), &val, 0.0);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(val*C);

        cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:c_fru_export", species_name);
        mcom->get_and_remove( mcom_key.c_str(), &val, 0.0);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(val*C);

        //fol
        cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:c_fol", species_name);
        mcom->get_and_remove( mcom_key.c_str(), &val, 0.0);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(val*C);

        cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:c_fol_export", species_name);
        mcom->get_and_remove( mcom_key.c_str(), &val, 0.0);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(val*C);

        //frt
        cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:c_frt", species_name);
        mcom->get_and_remove( mcom_key.c_str(), &val, 0.0);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(val*C);
        
        cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:c_frt_export", species_name);
        mcom->get_and_remove( mcom_key.c_str(), &val, 0.0);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(val*C);

        //lst
        cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:c_lst_below", species_name);
        mcom->get_and_remove( mcom_key.c_str(), &val, 0.0);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(val*C);
        
        cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:c_lst_below_export", species_name);
        mcom->get_and_remove( mcom_key.c_str(), &val, 0.0);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(val*C);
        
        cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:c_lst_above", species_name);
        mcom->get_and_remove( mcom_key.c_str(), &val, 0.0);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(val*C);
        
        cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:c_lst_above_export", species_name);
        mcom->get_and_remove( mcom_key.c_str(), &val, 0.0);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(val*C);

        
        
        
        //dst
        cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:c_dst_below", species_name);
        mcom->get_and_remove( mcom_key.c_str(), &val, 0.0);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(val*C);
        
        cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:c_dst_below_export", species_name);
        mcom->get_and_remove( mcom_key.c_str(), &val, 0.0);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(val*C);
        
        cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:c_dst_above", species_name);
        mcom->get_and_remove( mcom_key.c_str(), &val, 0.0);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(val*C);
        
        cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:c_dst_above_export", species_name);
        mcom->get_and_remove( mcom_key.c_str(), &val, 0.0);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(val*C);
        
        

        cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:c_straw", species_name);
        mcom->get_and_remove( mcom_key.c_str(), &val, 0.0);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(val*C);

        cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:c_straw_export", species_name);
        mcom->get_and_remove( mcom_key.c_str(), &val, 0.0);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(val*C);

        cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:c_stubble", species_name);
        mcom->get_and_remove( mcom_key.c_str(), &val, 0.0);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(val*C);

        
        /* NITROGEN */
        cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:n_fru", species_name);
        mcom->get_and_remove( mcom_key.c_str(), &val, 0.0);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(val*C);

        cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:n_fru_export", species_name);
        mcom->get_and_remove( mcom_key.c_str(), &val, 0.0);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(val*C);

        //fol
        cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:n_fol", species_name);
        mcom->get_and_remove( mcom_key.c_str(), &val, 0.0);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(val*C);

        cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:n_fol_export", species_name);
        mcom->get_and_remove( mcom_key.c_str(), &val, 0.0);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(val*C);

        //frt
        cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:n_frt", species_name);
        mcom->get_and_remove( mcom_key.c_str(), &val, 0.0);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(val*C);

        cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:n_frt_export", species_name);
        mcom->get_and_remove( mcom_key.c_str(), &val, 0.0);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(val*C);

        //lst
        cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:n_lst_below", species_name);
        mcom->get_and_remove( mcom_key.c_str(), &val, 0.0);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(val*C);
        
        cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:n_lst_below_export", species_name);
        mcom->get_and_remove( mcom_key.c_str(), &val, 0.0);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(val*C);
        
        cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:n_lst_above", species_name);
        mcom->get_and_remove( mcom_key.c_str(), &val, 0.0);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(val*C);
        
        cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:n_lst_above_export", species_name);
        mcom->get_and_remove( mcom_key.c_str(), &val, 0.0);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(val*C);

        //dst
        cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:n_dst_below", species_name);
        mcom->get_and_remove( mcom_key.c_str(), &val, 0.0);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(val*C);
        
        cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:n_dst_below_export", species_name);
        mcom->get_and_remove( mcom_key.c_str(), &val, 0.0);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(val*C);
        
        cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:n_dst_above", species_name);
        mcom->get_and_remove( mcom_key.c_str(), &val, 0.0);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(val*C);
        
        cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:n_dst_above_export", species_name);
        mcom->get_and_remove( mcom_key.c_str(), &val, 0.0);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(val*C);


        cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:n_straw", species_name);
        mcom->get_and_remove( mcom_key.c_str(), &val, 0.0);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(val*C);

        cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:n_straw_export", species_name);
        mcom->get_and_remove( mcom_key.c_str(), &val, 0.0);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(val*C);

        cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:n_stubble", species_name);
        mcom->get_and_remove( mcom_key.c_str(), &val, 0.0);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(val*C);

        if ( !species_reported)
        {
            continue;
        }

        /* species name */
        ldndc_string_t  data_string[1];
        cbm::as_strcpy( &data_string[0], species_name);

        void *  data[] = { data_string, _buf};

        lerr_t rc_write =
            this->write_fixed_record( &this->m_sink, data);
        if ( rc_write)
            { return LDNDC_ERR_FAIL; }
    }

    return  LDNDC_ERR_OK;
}

static int  _QueueEventHarvest( void const *  _msg, size_t  _msg_sz, void *  _queue)
{
    ldndc::EventQueue *  queue =
        static_cast< ldndc::EventQueue * >( _queue);
    if ( !queue)
        { return -1; }

    ldndc::EventAttributes  harvest_event(
                "harvest", (char const *)_msg, _msg_sz);
    queue->push( harvest_event);
    return 0;
}

lerr_t
OutputArableReportHarvest::register_ports( cbm::io_kcomm_t *  _io_kcomm)
{
    CBM_Callback  cb_harvest;
    cb_harvest.fn = &_QueueEventHarvest;
    cb_harvest.data = &this->m_HarvestEvents;
    this->m_HarvestHandle = _io_kcomm->subscribe_event( "harvest", cb_harvest);
    if ( !CBM_HandleOk(this->m_HarvestHandle))
        { return  LDNDC_ERR_FAIL; }

    return  LDNDC_ERR_OK;
}

lerr_t
OutputArableReportHarvest::unregister_ports( cbm::io_kcomm_t *  _io_kcomm)
{
    _io_kcomm->unsubscribe_event( this->m_HarvestHandle);

    return  LDNDC_ERR_OK;
}

#undef  LMOD_OUTPUT_MODULE_TIMEMODE
#undef  LMOD_OUTPUT_MODULE_BASE
#undef  LMOD_OUTPUT_MODULE_NAME

#undef  OutputArableReportHarvest_Rank
#undef  OutputArableReportHarvest_Datasize

} /*namespace ldndc*/
