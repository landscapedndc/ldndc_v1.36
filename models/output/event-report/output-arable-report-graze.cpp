/*!
 * @brief
 *
 * @author
 *    steffen klatt (created on: aug 27, 2012),
 *    edwin haas
 */

#include  "output/event-report/output-arable-report-graze.h"

#include  "constants/lconstants-conv.h"
#include  "constants/lconstants-phys.h"

#define  LMOD_OUTPUT_MODULE_BASE  MBE_LegacyOutputModel
#define  LMOD_OUTPUT_MODULE_NAME  OutputArableReportGraze
#define  LMOD_OUTPUT_MODULE_TIMEMODE  TMODE_SUBDAILY|TMODE_POST_DAILY

LMOD_MODULE_INFO(LMOD_OUTPUT_MODULE_NAME,LMOD_OUTPUT_MODULE_TIMEMODE,LMOD_FLAG_USER|LMOD_FLAG_OUTPUT);

/*!
 * @page managementoutput
 * @section grazingoutput Grazing output
 *
 * entity name | decription | unit
 * ----------- | ---------- | ----
 * dC\_fru\_export | Daily carbon of plant fruits exported from field due to grazing | [KgCha-1]
 * dC\_fol\_export | Daily carbon of plant foliage exported from field due to grazing | [KgCha-1]
 * dC\_lst\_export | Daily carbon of plant living structural tissue exported from field due to grazing | [KgCha-1]
 * dC\_dst\_export | Daily carbon of plant dead structural tissue exported from field due to grazing | [KgCha-1]
 * dC\_frt\_export | Daily carbon of plant fine roots exported from field due to grazing | [KgCha-1]
 * dC\_frt\_litter | Daily carbon of plant fine roots allocated to soil due to grazing | [KgCha-1]
 * dC\_dung\_litter | Daily carbon of dung allocated to soil due to grazing | [KgCha-1]
 * dN\_fru\_export | Daily nitrogen of plant fruits exported from field due to grazing | [KgNha-1]
 * dN\_fol\_export | Daily nitrogen of plant foliage exported from field due to grazing | [KgNha-1]
 * dN\_lst\_export | Daily nitrogen of plant living structural tissue exported from field due to grazing | [KgNha-1]
 * dN\_dst\_export | Daily nitrogen of plant dead structural tissue exported from field due to grazing | [KgNha-1]
 * dN\_frt\_export | Daily nitrogen of plant fineroots exported from field due to grazing | [KgNha-1]
 * dN\_frt\_litter | Daily nitrogen of plant fineroots allocated to soil due to grazing | [KgNha-1]
 * dN\_dung\_litter | Daily nitrogen of dung allocated to soil due to grazing | [KgNha-1]
 * dN\_urine\_litter | Daily nitrogen of urine allocated to soil due to grazing | [KgNha-1]
 */

namespace ldndc {

/* GRAZE */
ldndc_string_t const  OutputArableReportGraze_Ids[] =
{
    "dC_fru_export", "dC_fol_export", "dC_lst_export", "dC_dst_export", "dC_frt_export",
    "dC_frt_litter", "dC_dung_litter",
    "dN_fru_export", "dN_fol_export", "dN_lst_export", "dN_dst_export", "dN_frt_export",
    "dN_frt_litter", "dN_dung_litter", "dN_urine_litter"
};
ldndc_string_t const  OutputArableReportGraze_Header[] =
{
    "dC_fru_export[kgCha-1]", "dC_fol_export[kgCha-1]", "dC_lst_export[kgCha-1]", "dC_dst_export[kgCha-1]", "dC_frt_export[kgCha-1]",
    "dC_frt_litter[kgCha-1]", "dC_dung_litter[kgCha-1]",
    "dN_fru_export[kgNha-1]", "dN_fol_export[kgNha-1]", "dN_lst_export[kgNha-1]", "dN_dst_export[kgNha-1]", "dN_frt_export[kgNha-1]",
    "dN_frt_litter[kgNha-1]", "dN_dung_litter[kgNha-1]", "dN_urine_litter[kgNha-1]"
};
#define  OutputArableReportGraze_Datasize  (sizeof( OutputArableReportGraze_Header) / sizeof( OutputArableReportGraze_Header[0]))
ldndc_output_size_t const  OutputArableReportGraze_Sizes[] =
{
    OutputArableReportGraze_Datasize,

    OutputArableReportGraze_Datasize /*total size*/
};
ldndc_output_size_t const *  OutputArableReportGraze_EntitySizes = NULL;
#define  OutputArableReportGraze_Rank  ((ldndc_output_rank_t)(sizeof( OutputArableReportGraze_Sizes) / sizeof( OutputArableReportGraze_Sizes[0])) - 1)
atomic_datatype_t const  OutputArableReportGraze_Types[] =
{
    LDNDC_FLOAT64
};

LMOD_OUTPUT_MODULE_NAME::LMOD_OUTPUT_MODULE_NAME(
        MoBiLE_State *  _state,
        cbm::io_kcomm_t *  _io_kcomm,
        timemode_e  _timemode)
        : LMOD_OUTPUT_MODULE_BASE( _state, _io_kcomm, _timemode),

        io_kcomm( _io_kcomm),
        m_veg( &_state->vegetation)
{
    this->scratchpad = this->io_kcomm->get_scratch();
}


LMOD_OUTPUT_MODULE_NAME::~LMOD_OUTPUT_MODULE_NAME()
{
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::configure(
                ldndc::config_file_t const *  _cf)
{
    lerr_t  rc_setflags = this->set_metaflags( _cf);
    if ( rc_setflags)
        { return  rc_setflags; }

    int  producer_graze_timemode = TMODE_NONE;
    this->scratchpad->get( "producer.graze.timemode",
            &producer_graze_timemode, producer_graze_timemode);
    if ( producer_graze_timemode == TMODE_NONE)
    {
        KLOGWARN( "nobody sends me data :( going to sleep.");
        this->set_active_off();
        this->sleep();
    }
    else if ( this->timemode() != producer_graze_timemode)
    {
        timemode_e  tm =
            static_cast< timemode_e >( producer_graze_timemode);
        KLOGERROR( "oh no, i am in temporal disagreement with my producer :o",
                "  please select appropriate timemode",
                "  [expecting=",ldndc::time::timemode_name( tm),"]");
        return  LDNDC_ERR_FAIL;
    }
    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::initialize()
{
    this->m_sink =
        this->io_kcomm->sink_handle_acquire( "arablereportgraze");
    if ( this->m_sink.status() == LDNDC_ERR_OK)
    {
        lerr_t  rc_layout = LDNDC_OUTPUT_DEFINE_SINK_LAYOUT_DEFAULTS(
                    this->m_sink,OutputArableReportGraze);
        RETURN_IF_NOT_OK(rc_layout);
    }
    else
    {
        KLOGERROR( "sink status bad  [sink=","arablereportgraze","]");
        return  this->m_sink.status();
    }

    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::solve()
{
    lerr_t  rc_dump = LDNDC_ERR_OK;
    if ( this->m_veg->size() > 0)
    {
        ldndc_flt64_t  data_flt64_0[OutputArableReportGraze_Datasize];
        rc_dump = this->dump_graze_( data_flt64_0);
    }

    return  rc_dump;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::finalize()
{
    this->io_kcomm->sink_handle_release( &this->m_sink);
    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::dump_graze_( ldndc_flt64_t *  _buf)
{
    if ( !this->scratchpad->variable_exists( "graze:mfruC"))
    {
        /* we assume that all others are missing too */
        return  LDNDC_ERR_OK;
    }

    double val;
    static double const  C = cbm::M2_IN_HA;

    LDNDC_OUTPUT_SET_COLUMN_INDEX(0);

    /* carbon */
    this->scratchpad->get_and_remove( "graze:mfruC", &val, 0.0);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(val*C);

    this->scratchpad->get_and_remove( "graze:mfolC", &val, 0.0);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(val*C);

    this->scratchpad->get_and_remove( "graze:mlstC", &val, 0.0);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(val*C); //lst
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(0.0); //dst

    this->scratchpad->get_and_remove( "graze:mfrtC", &val, 0.0);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(val*C);

    this->scratchpad->get_and_remove( "graze:c_frt", &val, 0.0);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(val*C);

    this->scratchpad->get_and_remove( "graze:dungcarbon", &val, 0.0);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(val);

    /* nitrogen */
    this->scratchpad->get_and_remove( "graze:mfruN", &val, 0.0);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(val*C);

    this->scratchpad->get_and_remove( "graze:mfolN", &val, 0.0);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(val*C);

    this->scratchpad->get_and_remove( "graze:mlstN", &val, 0.0);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(val*C); //lst
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(0.0); //dst

    this->scratchpad->get_and_remove( "graze:mfrtN", &val, 0.0);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(val*C);

    this->scratchpad->get_and_remove( "graze:n_frt", &val, 0.0);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(val*C);

    this->scratchpad->get_and_remove( "graze:dungnitrogen", &val, 0.0);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(val);

    this->scratchpad->get_and_remove( "graze:urinenitrogen", &val, 0.0);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(val);

    void *  data[] = { _buf};
    this->write_fixed_record( &this->m_sink, data);

    return  LDNDC_ERR_OK;
}

#undef  LMOD_OUTPUT_MODULE_TIMEMODE
#undef  LMOD_OUTPUT_MODULE_BASE
#undef  LMOD_OUTPUT_MODULE_NAME

#undef  OutputArableReportGraze_Rank
#undef  OutputArableReportGraze_Datasize

} /*namespace ldndc*/

