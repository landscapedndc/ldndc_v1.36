/*!
 * @brief
 *
 * @author
 *  David Kraus
 *  Steffen Klatt
 */

#include  "output/event-report/output-event-report-thin.h"

#include  "constants/lconstants-conv.h"
#include  "constants/lconstants-phys.h"

#define  LMOD_OUTPUT_MODULE_BASE  MBE_LegacyOutputModel
#define  LMOD_OUTPUT_MODULE_NAME  OutputEventReportThin
#define  LMOD_OUTPUT_MODULE_TIMEMODE  TMODE_SUBDAILY|TMODE_POST_DAILY

LMOD_MODULE_INFO(LMOD_OUTPUT_MODULE_NAME,LMOD_OUTPUT_MODULE_TIMEMODE,LMOD_FLAG_USER|LMOD_FLAG_OUTPUT);
namespace ldndc {

/* Thinning */
ldndc_string_t const  OutputEventReportThin_Ids[] =
{
    "species",
    "dDW_fol", "dDW_fol_export",
    "dDW_fru", "dDW_fru_export",
    "dDW_frt", "dDW_frt_export",
    "dDW_sap_below", "dDW_sap_below_export",
    "dDW_sap_above", "dDW_sap_above_export",
    "dDW_cor_below", "dDW_cor_below_export",
    "dDW_cor_above", "dDW_cor_above_export",
    "dN_fol", "dN_fol_export",
    "dN_fru", "dN_fru_export",
    "dN_frt", "dN_frt_export",
    "dN_sap_below", "dN_sap_below_export",
    "dN_sap_above", "dN_sap_above_export",
    "dN_cor_below", "dN_cor_below_export",
    "dN_cor_above", "dN_cor_above_export"
};

ldndc_string_t const  OutputEventReportThin_Header[] =
{
    "species",
    "dDW_fol[kgDWm-2]", "dDW_fol_export[kgDWm-2]",
    "dDW_fru[kgDWm-2]", "dDW_fru_export[kgDWm-2]",
    "dDW_frt[kgDWm-2]", "dDW_frt_export[kgDWm-2]",
    "dDW_sap_below[kgDWm-2]", "dDW_sap_below_export[kgDWm-2]",
    "dDW_sap_above[kgDWm-2]", "dDW_sap_above_export[kgDWm-2]",
    "dDW_cor_below[kgDWm-2]", "dDW_cor_below_export[kgDWm-2]",
    "dDW_cor_above[kgDWm-2]", "dDW_cor_above_export[kgDWm-2]",

    "dN_fol[kgNm-2]", "dN_fol_export[kgNm-2]",
    "dN_fru[kgNm-2]", "dN_fru_export[kgNm-2]",
    "dN_frt[kgNm-2]", "dN_frt_export[kgNm-2]",
    "dN_sap_below[kgNm-2]", "dN_sap_below_export[kgNm-2]",
    "dN_sap_above[kgNm-2]", "dN_sap_above_export[kgNm-2]",
    "dN_cor_below[kgNm-2]", "dN_cor_below_export[kgNm-2]",
    "dN_cor_above[kgNm-2]", "dN_cor_above_export[kgNm-2]"
};

    
#define  OutputEventReportThin_Datasize  (sizeof( OutputEventReportThin_Header) / sizeof( OutputEventReportThin_Header[0]))
ldndc_output_size_t const  OutputEventReportThin_Sizes[] =
{
    1,
    OutputEventReportThin_Datasize - 1/*species name*/,

    OutputEventReportThin_Datasize /*total size*/
};
ldndc_output_size_t const *  OutputEventReportThin_EntitySizes = NULL;
#define  OutputEventReportThin_Rank  ((ldndc_output_rank_t)(sizeof( OutputEventReportThin_Sizes) / sizeof( OutputEventReportThin_Sizes[0])) - 1)
atomic_datatype_t const  OutputEventReportThin_Types[] =
{
    LDNDC_STRING,
    LDNDC_FLOAT64
};

LMOD_OUTPUT_MODULE_NAME::LMOD_OUTPUT_MODULE_NAME(
        MoBiLE_State *  _state,
        cbm::io_kcomm_t *  _io_kcomm,
        timemode_e  _timemode)
        : LMOD_OUTPUT_MODULE_BASE( _state, _io_kcomm, _timemode),

        io_kcomm( _io_kcomm),
        m_veg( &_state->vegetation)
{
}


LMOD_OUTPUT_MODULE_NAME::~LMOD_OUTPUT_MODULE_NAME()
{
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::configure(
            ldndc::config_file_t const *  _cf)
{
    lerr_t  rc_setflags = this->set_metaflags( _cf);
    return  rc_setflags;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::initialize()
{
    this->m_sink = this->io_kcomm->sink_handle_acquire( "eventreportthin");
    if ( this->m_sink.status() == LDNDC_ERR_OK)
    {
        lerr_t  rc_layout = LDNDC_OUTPUT_DEFINE_SINK_LAYOUT_DEFAULTS(
                    this->m_sink,OutputEventReportThin);
        RETURN_IF_NOT_OK(rc_layout);
    }
    else
    {
        KLOGERROR( "sink status bad  [sink=","eventreportthin","]");
        return  this->m_sink.status();
    }

    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::solve()
{
    lerr_t  rc_dump = LDNDC_ERR_OK;
    if ( this->m_veg->size() > 0)
    {
        ldndc_flt64_t  data_flt64_0[OutputEventReportThin_Datasize];
        rc_dump = this->dump_thin_( data_flt64_0);
    }

    return  rc_dump;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::finalize()
{
    this->io_kcomm->sink_handle_release( &this->m_sink);
    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::dump_thin_( ldndc_flt64_t *  _buf)
{
    cbm::state_scratch_t *  mcom = this->io_kcomm->get_scratch();

    std::string  mcom_key;

    while ( this->m_ThinEvents)
    {
        EventAttributes  ev_thin = this->m_ThinEvents.pop();
        char const *  species_name = ev_thin.get( "/name", "?");

        LDNDC_OUTPUT_SET_COLUMN_INDEX(0);

        double  val = 0.0;

        
        cbm::state_scratch_t::make_item_key(  &mcom_key, "thin:%s:m_fol", species_name);
        mcom->get_and_remove( mcom_key.c_str(), &val, 0.0);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( val);

        cbm::state_scratch_t::make_item_key(  &mcom_key, "thin:%s:m_fol_export", species_name);
        mcom->get_and_remove( mcom_key.c_str(), &val, 0.0);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( val);

        cbm::state_scratch_t::make_item_key(  &mcom_key, "thin:%s:m_fru", species_name);
        mcom->get_and_remove( mcom_key.c_str(), &val, 0.0);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( val);

        cbm::state_scratch_t::make_item_key(  &mcom_key, "thin:%s:m_fru_export", species_name);
        mcom->get_and_remove( mcom_key.c_str(), &val, 0.0);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( val);

        cbm::state_scratch_t::make_item_key(  &mcom_key, "thin:%s:m_frt", species_name);
        mcom->get_and_remove( mcom_key.c_str(), &val, 0.0);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( val);

        cbm::state_scratch_t::make_item_key(  &mcom_key, "thin:%s:m_frt_export", species_name);
        mcom->get_and_remove( mcom_key.c_str(), &val, 0.0);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( val);

        cbm::state_scratch_t::make_item_key(  &mcom_key, "thin:%s:m_lst_below", species_name);
        mcom->get_and_remove( mcom_key.c_str(), &val, 0.0);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( val);

        cbm::state_scratch_t::make_item_key(  &mcom_key, "thin:%s:m_lst_below_export", species_name);
        mcom->get_and_remove( mcom_key.c_str(), &val, 0.0);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( val);

        cbm::state_scratch_t::make_item_key(  &mcom_key, "thin:%s:m_lst_above", species_name);
        mcom->get_and_remove( mcom_key.c_str(), &val, 0.0);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( val);

        cbm::state_scratch_t::make_item_key(  &mcom_key, "thin:%s:m_lst_above_export", species_name);
        mcom->get_and_remove( mcom_key.c_str(), &val, 0.0);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( val);

        cbm::state_scratch_t::make_item_key(  &mcom_key, "thin:%s:m_dst_below", species_name);
        mcom->get_and_remove( mcom_key.c_str(), &val, 0.0);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( val);

        cbm::state_scratch_t::make_item_key(  &mcom_key, "thin:%s:m_dst_below_export", species_name);
        mcom->get_and_remove( mcom_key.c_str(), &val, 0.0);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( val);

        cbm::state_scratch_t::make_item_key(  &mcom_key, "thin:%s:m_dst_above", species_name);
        mcom->get_and_remove( mcom_key.c_str(), &val, 0.0);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( val);

        cbm::state_scratch_t::make_item_key(  &mcom_key, "thin:%s:m_dst_above_export", species_name);
        mcom->get_and_remove( mcom_key.c_str(), &val, 0.0);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( val);



        cbm::state_scratch_t::make_item_key(  &mcom_key, "thin:%s:n_fol", species_name);
        mcom->get_and_remove( mcom_key.c_str(), &val, 0.0);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( val);

        cbm::state_scratch_t::make_item_key(  &mcom_key, "thin:%s:n_fol_export", species_name);
        mcom->get_and_remove( mcom_key.c_str(), &val, 0.0);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( val);

        cbm::state_scratch_t::make_item_key(  &mcom_key, "thin:%s:n_fru", species_name);
        mcom->get_and_remove( mcom_key.c_str(), &val, 0.0);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( val);

        cbm::state_scratch_t::make_item_key(  &mcom_key, "thin:%s:n_fru_export", species_name);
        mcom->get_and_remove( mcom_key.c_str(), &val, 0.0);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( val);

        cbm::state_scratch_t::make_item_key(  &mcom_key, "thin:%s:n_frt", species_name);
        mcom->get_and_remove( mcom_key.c_str(), &val, 0.0);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( val);

        cbm::state_scratch_t::make_item_key(  &mcom_key, "thin:%s:n_frt_export", species_name);
        mcom->get_and_remove( mcom_key.c_str(), &val, 0.0);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( val);

        cbm::state_scratch_t::make_item_key(  &mcom_key, "thin:%s:n_lst_below", species_name);
        mcom->get_and_remove( mcom_key.c_str(), &val, 0.0);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( val);

        cbm::state_scratch_t::make_item_key(  &mcom_key, "thin:%s:n_lst_below_export", species_name);
        mcom->get_and_remove( mcom_key.c_str(), &val, 0.0);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( val);

        cbm::state_scratch_t::make_item_key(  &mcom_key, "thin:%s:n_lst_above", species_name);
        mcom->get_and_remove( mcom_key.c_str(), &val, 0.0);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( val);

        cbm::state_scratch_t::make_item_key(  &mcom_key, "thin:%s:n_lst_above_export", species_name);
        mcom->get_and_remove( mcom_key.c_str(), &val, 0.0);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( val);

        cbm::state_scratch_t::make_item_key(  &mcom_key, "thin:%s:n_dst_below", species_name);
        mcom->get_and_remove( mcom_key.c_str(), &val, 0.0);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( val);

        cbm::state_scratch_t::make_item_key(  &mcom_key, "thin:%s:n_dst_below_export", species_name);
        mcom->get_and_remove( mcom_key.c_str(), &val, 0.0);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( val);

        cbm::state_scratch_t::make_item_key(  &mcom_key, "thin:%s:n_dst_above", species_name);
        mcom->get_and_remove( mcom_key.c_str(), &val, 0.0);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( val);

        cbm::state_scratch_t::make_item_key(  &mcom_key, "thin:%s:n_dst_above_export", species_name);
        mcom->get_and_remove( mcom_key.c_str(), &val, 0.0);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( val);



        /* species name */
        ldndc_string_t  data_string[1];
        cbm::as_strcpy( &data_string[0], species_name);

        void *  data[] = { data_string, _buf};

        lerr_t rc_write =
            this->write_fixed_record( &this->m_sink, data);
        if ( rc_write)
            { return LDNDC_ERR_FAIL; }
    }
    
    return  LDNDC_ERR_OK;
}

static int  _QueueEventThin( void const *  _msg, size_t  _msg_sz, void *  _queue)
{
    ldndc::EventQueue *  queue = static_cast< ldndc::EventQueue * >( _queue);
    if ( !queue)
    { return -1; }

    ldndc::EventAttributes  thin_event( "thin", (char const *)_msg, _msg_sz);
    queue->push( thin_event);
    return 0;
}

lerr_t
OutputEventReportThin::register_ports( cbm::io_kcomm_t *  _io_kcomm)
{
    CBM_Callback  cb_thin;
    cb_thin.fn = &_QueueEventThin;
    cb_thin.data = &this->m_ThinEvents;
    this->m_ThinHandle = _io_kcomm->subscribe_event( "thin", cb_thin);

    if ( !CBM_HandleOk(this->m_ThinHandle))
    { return  LDNDC_ERR_FAIL; }

    return  LDNDC_ERR_OK;
}

lerr_t
OutputEventReportThin::unregister_ports( cbm::io_kcomm_t *  _io_kcomm)
{
    _io_kcomm->unsubscribe_event( this->m_ThinHandle);

    return  LDNDC_ERR_OK;
}

#undef  LMOD_OUTPUT_MODULE_TIMEMODE
#undef  LMOD_OUTPUT_MODULE_BASE
#undef  LMOD_OUTPUT_MODULE_NAME

#undef  OutputEventReportThin_Rank
#undef  OutputEventReportThin_Datasize

} /*namespace ldndc*/

