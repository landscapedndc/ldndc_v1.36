/*!
 * @brief
 *    agricultural output module: runs all available
 *    agricultural report modules.
 *
 * @author
 *    steffen klatt (created on: aug 27, 2012),
 *    edwin haas
 */
#ifndef  LM_OUTPUT_ARABLEREPORT_H_
#define  LM_OUTPUT_ARABLEREPORT_H_

#include  "mbe_legacyoutputmodel.h"
#include  "state/mbe_state.h"

#include  "output/event-report/output-arable-report-cut.h"
#include  "output/event-report/output-arable-report-fertilize.h"
#include  "output/event-report/output-arable-report-graze.h"
#include  "output/event-report/output-arable-report-harvest.h"
#include  "output/event-report/output-arable-report-manure.h"

#define  LMOD_OUTPUT_MODULE_BASE  MBE_LegacyModel
#define  LMOD_OUTPUT_MODULE_NAME  OutputArableReport
#define  LMOD_OUTPUT_MODULE_ID    "output:report:arable"
#define  LMOD_OUTPUT_MODULE_DESC  "Output Agricultural Summary"
namespace ldndc {
class  substate_physiology_t;
class  LDNDC_API  LMOD_OUTPUT_MODULE_NAME  :  public  LMOD_OUTPUT_MODULE_BASE
{
    LMOD_EXPORT_MODULE_INFO(LMOD_OUTPUT_MODULE_NAME,LMOD_OUTPUT_MODULE_ID,LMOD_OUTPUT_MODULE_DESC);
    public:
        LMOD_OUTPUT_MODULE_NAME(
                MoBiLE_State *,
                cbm::io_kcomm_t *,
                timemode_e);

        ~LMOD_OUTPUT_MODULE_NAME();


        lerr_t  configure( ldndc::config_file_t const *);
        lerr_t  register_ports( cbm::io_kcomm_t *);
        lerr_t  initialize();

        lerr_t  solve();
        lerr_t  finalize();
        lerr_t  unregister_ports( cbm::io_kcomm_t *); 

        lerr_t  wake();
        lerr_t  sleep();

    private:
        OutputArableReportCut  report_cut;
        OutputArableReportFertilize  report_fertilize;
        OutputArableReportGraze  report_graze;
        OutputArableReportHarvest  report_harvest;
        OutputArableReportManure  report_manure;

        MBE_LegacyOutputModel *  report_modules[5 + 1/*sentinel*/];
};
} /*namespace ldndc*/


#undef  LMOD_OUTPUT_MODULE_BASE
#undef  LMOD_OUTPUT_MODULE_NAME
#undef  LMOD_OUTPUT_MODULE_ID
#undef  LMOD_OUTPUT_MODULE_DESC


#endif  /*  !LM_OUTPUT_ARABLEREPORT_H_  */

