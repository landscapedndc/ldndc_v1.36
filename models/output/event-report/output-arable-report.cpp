/*!
 * @brief
 *
 * @author
 *    edwin haas,
 *    ruediger grote
 */

#include  "output/event-report/output-arable-report.h"

#define  LMOD_OUTPUT_MODULE_BASE  MBE_LegacyModel
#define  LMOD_OUTPUT_MODULE_NAME  OutputArableReport
#define  LMOD_OUTPUT_MODULE_TIMEMODE  TMODE_SUBDAILY|TMODE_POST_DAILY

LMOD_MODULE_INFO(LMOD_OUTPUT_MODULE_NAME,LMOD_OUTPUT_MODULE_TIMEMODE,LMOD_FLAG_USER|LMOD_FLAG_OUTPUT);
namespace ldndc {

LMOD_OUTPUT_MODULE_NAME::LMOD_OUTPUT_MODULE_NAME(
        MoBiLE_State *  _state,
        cbm::io_kcomm_t *  _io,
                timemode_e  _timemode)
                : LMOD_OUTPUT_MODULE_BASE( _state, _timemode),
          report_cut( _state, _io, _timemode),
          report_fertilize( _state, _io, _timemode),
          report_graze( _state, _io, _timemode),
          report_harvest( _state, _io, _timemode),
          report_manure( _state, _io, _timemode)
{
    this->report_modules[0] = &this->report_cut;
    this->report_modules[1] = &this->report_fertilize;
    this->report_modules[2] = &this->report_graze;
    this->report_modules[3] = &this->report_harvest;
    this->report_modules[4] = &this->report_manure;
    this->report_modules[5] = NULL; /*sentinel*/
}


LMOD_OUTPUT_MODULE_NAME::~LMOD_OUTPUT_MODULE_NAME()
{
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::configure(
                ldndc::config_file_t const *  _cf)
{
    lerr_t  rc = LDNDC_ERR_OK;

    size_t  m = 0;
    while ( this->report_modules[m])
    {
        MBE_LegacyOutputModel *  mod = this->report_modules[m];
        rc = mod->configure( _cf);
        if ( rc)
        {
            return  rc;
        }
        ++m;
    }

    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::register_ports( cbm::io_kcomm_t *  _iokcomm)
{
    lerr_t  rc = LDNDC_ERR_OK;

    size_t  m = 0;
    while ( this->report_modules[m])
    {
        MBE_LegacyOutputModel *  mod = this->report_modules[m];
        if ( mod->active())
        {
            rc = mod->register_ports( _iokcomm);
            if ( rc)
            {
                return  rc;
            }
        }
        ++m;
    }

    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::unregister_ports( cbm::io_kcomm_t *  _iokcomm)
{
    lerr_t  rc = LDNDC_ERR_OK;

    size_t  m = 0;
    while ( this->report_modules[m])
    {
        MBE_LegacyOutputModel *  mod = this->report_modules[m];
        if ( mod->active())
        {
            rc = mod->unregister_ports( _iokcomm);
            if ( rc)
            {
                return  rc;
            }
        }
        ++m;
    }

    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::initialize()
{
    lerr_t  rc = LDNDC_ERR_OK;

    size_t  m = 0;
    while ( this->report_modules[m])
    {
        MBE_LegacyOutputModel *  mod = this->report_modules[m];
        if ( mod->active())
        {
            rc = mod->initialize();
            if ( rc)
            {
                return  rc;
            }
        }
        ++m;
    }

    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::solve()
{
    lerr_t  rc = LDNDC_ERR_OK;

    size_t  m = 0;
    while ( this->report_modules[m])
    {
        MBE_LegacyOutputModel *  mod = this->report_modules[m];
        if ( mod->active())
        {
            rc = mod->solve();
            if ( rc)
            {
                return  rc;
            }
        }
        ++m;
    }

    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::finalize()
{
    lerr_t  rc = LDNDC_ERR_OK;

    size_t  m = 0;
    while ( this->report_modules[m])
    {
        MBE_LegacyOutputModel *  mod = this->report_modules[m];
        lerr_t  rc_fin = mod->finalize();
        if ( !rc && rc_fin)
        {
            /* report first error */
            rc = rc_fin;
        }
        ++m;
    }

    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::wake()
{
    lerr_t  rc = LDNDC_ERR_OK;

    size_t  m = 0;
    while ( this->report_modules[m])
    {
        MBE_LegacyOutputModel *  mod = this->report_modules[m];
        lerr_t  rc_wake = mod->wake();
        if ( !rc && rc_wake)
        {
            /* report first error */
            rc = rc_wake;
        }
    }

    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::sleep()
{
    lerr_t  rc = LDNDC_ERR_OK;

    size_t  m = 0;
    while ( this->report_modules[m])
    {
        MBE_LegacyOutputModel *  mod = this->report_modules[m];
        lerr_t  rc_sleep = mod->sleep();
        if ( !rc && rc_sleep)
        {
            /* report first error */
            rc = rc_sleep;
        }
    }

    return  LDNDC_ERR_OK;
}

#undef  LMOD_OUTPUT_MODULE_TIMEMODE
#undef  LMOD_OUTPUT_MODULE_BASE
#undef  LMOD_OUTPUT_MODULE_NAME

} /*namespace ldndc*/

