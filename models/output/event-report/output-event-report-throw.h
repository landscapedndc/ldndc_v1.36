/*!
 * @brief
 *
 * @author
 *  David Kraus
 *  Steffen Klatt
 */

    #ifndef  LM_OUTPUT_EVENTREPORTTHROW_H_
#define  LM_OUTPUT_EVENTREPORTTHROW_H_

#include  "mbe_legacyoutputmodel.h"
#include  "state/mbe_state.h"
#include  "ld_eventqueue.h"


#define  LMOD_OUTPUT_MODULE_BASE  MBE_LegacyOutputModel
#define  LMOD_OUTPUT_MODULE_NAME  OutputEventReportThrow
#define  LMOD_OUTPUT_MODULE_ID    "output:report:event:throw"
#define  LMOD_OUTPUT_MODULE_DESC  "Output Forest Event Summary (Throw)"
namespace ldndc {
class  substate_physiology_t;
class  LDNDC_API  LMOD_OUTPUT_MODULE_NAME  :  public  LMOD_OUTPUT_MODULE_BASE
{
    LMOD_EXPORT_MODULE_INFO(LMOD_OUTPUT_MODULE_NAME,LMOD_OUTPUT_MODULE_ID,LMOD_OUTPUT_MODULE_DESC);
    public:
        LMOD_OUTPUT_MODULE_NAME( MoBiLE_State *,
                cbm::io_kcomm_t *, timemode_e);
        ~LMOD_OUTPUT_MODULE_NAME();

        lerr_t  configure( ldndc::config_file_t const *);
        lerr_t  register_ports( cbm::io_kcomm_t *);
        lerr_t  initialize();

        lerr_t  solve();
        lerr_t  unregister_ports( cbm::io_kcomm_t *);
        lerr_t  finalize();

        lerr_t  wake() { return  LDNDC_ERR_OK; }
        lerr_t  sleep() { return  LDNDC_ERR_OK; }

    private:
        cbm::io_kcomm_t *  io_kcomm;

        MoBiLE_PlantVegetation *  m_veg;

        lerr_t  dump_throw_(
                            char const *,
                            ldndc_flt64_t *);

        ldndc::sink_handle_t  m_sink;

        EventQueue  m_ThrowEvents;
        CBM_Handle  m_ThrowHandle;
};
} /*namespace ldndc*/


#undef  LMOD_OUTPUT_MODULE_BASE
#undef  LMOD_OUTPUT_MODULE_NAME
#undef  LMOD_OUTPUT_MODULE_ID
#undef  LMOD_OUTPUT_MODULE_DESC


#endif  /*  !LM_OUTPUT_EVENTREPORTTHROW_H_  */

