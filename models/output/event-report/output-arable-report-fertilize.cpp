/*!
 * @brief
 *
 * @author
 *    steffen klatt (created on: aug 27, 2012),
 *    edwin haas
 */

#include  "output/event-report/output-arable-report-fertilize.h"

#include  "constants/lconstants-conv.h"
#include  "constants/lconstants-phys.h"

#define  LMOD_OUTPUT_MODULE_BASE  MBE_LegacyOutputModel
#define  LMOD_OUTPUT_MODULE_NAME  OutputArableReportFertilize
#define  LMOD_OUTPUT_MODULE_TIMEMODE  TMODE_SUBDAILY|TMODE_POST_DAILY

#ifdef  LDNDC_OUTPUT_HAVE_DUMMY_LINES
//#  define  LDNDC_OUTPUT_FERTILIZE_HAVE_DUMMY_LINES  1
#endif

LMOD_MODULE_INFO(LMOD_OUTPUT_MODULE_NAME,LMOD_OUTPUT_MODULE_TIMEMODE,LMOD_FLAG_USER|LMOD_FLAG_OUTPUT);

/*!
 * @page managementoutput
 * @section fertilizeoutput Fertilize output
 *
 * entity name | decription | unit
 * ----------- | ---------- | ----
 * dN\_fertilizer | Daily nitrogen application in form of different inorganic fertilizers | [kgNha-1]
 * dN\_yearsum | Daily nitrogen application in form of different inorganic fertilizers | [kgNha-1]
 */

namespace ldndc {

/* FERTILIZE */
ldndc_string_t const  OutputArableReportFertilize_Ids[] =
{
    "dN_fertilizer", "dN_yearsum"
};
ldndc_string_t const  OutputArableReportFertilize_Header[] =
{
    "dN_fertilizer[kgNha-1]", "dN_yearsum[kgNha-1]"
};
#define  OutputArableReportFertilize_Datasize  (sizeof( OutputArableReportFertilize_Header) / sizeof( OutputArableReportFertilize_Header[0]))
ldndc_output_size_t const  OutputArableReportFertilize_Sizes[] =
{
    OutputArableReportFertilize_Datasize,

    OutputArableReportFertilize_Datasize /*total size*/
};
ldndc_output_size_t const *  OutputArableReportFertilize_EntitySizes = NULL;
#define  OutputArableReportFertilize_Rank  ((ldndc_output_rank_t)(sizeof( OutputArableReportFertilize_Sizes) / sizeof( OutputArableReportFertilize_Sizes[0])) - 1)
atomic_datatype_t const  OutputArableReportFertilize_Types[] =
{
    LDNDC_FLOAT64
};

LMOD_OUTPUT_MODULE_NAME::LMOD_OUTPUT_MODULE_NAME(
        MoBiLE_State *  _state,
        cbm::io_kcomm_t *  _io_kcomm,
        timemode_e  _timemode)
        : LMOD_OUTPUT_MODULE_BASE( _state, _io_kcomm, _timemode),

          io_kcomm( _io_kcomm),
          fert_n_sum_yr_( 0.0)
{
}


LMOD_OUTPUT_MODULE_NAME::~LMOD_OUTPUT_MODULE_NAME()
{
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::configure(
                ldndc::config_file_t const *  _cf)
{
    lerr_t  rc_setflags = this->set_metaflags( _cf);
    if ( rc_setflags)
        { return  rc_setflags; }

    int  producer_fertilize_timemode = TMODE_NONE;
    this->io_kcomm->get_scratch()->get( "producer.fertilize.timemode",
            &producer_fertilize_timemode, producer_fertilize_timemode);
    if ( producer_fertilize_timemode == TMODE_NONE)
    {
        KLOGWARN( "nobody sends me data :( going to sleep.");
        this->set_active_off();
        this->sleep();
    }
    else if ( this->timemode() != producer_fertilize_timemode)
    {
        timemode_e  tm =
            static_cast< timemode_e >( producer_fertilize_timemode);
        KLOGERROR( "oh no, i am in temporal disagreement with my producer :o",
                "  please select appropriate timemode",
                "  [expecting=",ldndc::time::timemode_name( tm),"]");
        return  LDNDC_ERR_FAIL;
    }
    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::initialize()
{
    this->fert_n_sum_yr_ = 0.0;

    this->m_sink =
        this->io_kcomm->sink_handle_acquire( "arablereportfertilize");
    if ( this->m_sink.status() == LDNDC_ERR_OK)
    {
        lerr_t  rc_layout = LDNDC_OUTPUT_DEFINE_SINK_LAYOUT_DEFAULTS(
                    this->m_sink,OutputArableReportFertilize);
        RETURN_IF_NOT_OK(rc_layout);
    }
    else
    {
        KLOGERROR( "sink status bad  [sink=","arablereportfertilize","]");
        return  this->m_sink.status();
    }

    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::solve()
{
    ldndc_flt64_t  data_flt64_0[OutputArableReportFertilize_Datasize];
    lerr_t  rc_dump = this->dump_fertilize_( data_flt64_0);
    RETURN_IF_NOT_OK(rc_dump);

    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::finalize()
{
    this->io_kcomm->sink_handle_release( &this->m_sink);
    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::dump_fertilize_(
        ldndc_flt64_t *  _buf)
{
    cbm::state_scratch_t *  mcom = this->io_kcomm->get_scratch();

    double  fert_n_sum = 0.0;
    lerr_t  rc_have_fert = mcom->get_and_remove( "fertilize:amount", &fert_n_sum, 0.0);
    this->fert_n_sum_yr_ += fert_n_sum;

    bool const  is_eoy = ( this->lclock_ref().is_position( TMODE_POST_YEARLY)
            && this->lclock_ref().is_position( TMODE_POST_DAILY)) || ( false/*final day*/);
    if (( rc_have_fert == LDNDC_ERR_OK) || is_eoy)
    {
        LDNDC_OUTPUT_SET_COLUMN_INDEX(0);

        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(fert_n_sum);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->fert_n_sum_yr_);

        void *  data[] = { _buf};
        this->write_fixed_record( &this->m_sink, data);

        /* reset */
        if ( is_eoy)
        {
            this->fert_n_sum_yr_ = 0.0;
        }
    }

    return  LDNDC_ERR_OK;
}

#undef  LMOD_OUTPUT_MODULE_TIMEMODE
#undef  LMOD_OUTPUT_MODULE_BASE
#undef  LMOD_OUTPUT_MODULE_NAME

#undef  OutputArableReportFertilize_Rank
#undef  OutputArableReportFertilize_Datasize

} /*namespace ldndc*/

