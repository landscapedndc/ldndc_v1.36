/*!
 * @brief
 *    agricultural output module for event graze
 *
 * @author
 *    steffen klatt (created on: aug 27, 2012),
 *    edwin haas
 */
#ifndef  LM_OUTPUT_ARABLEREPORTGRAZE_H_
#define  LM_OUTPUT_ARABLEREPORTGRAZE_H_

#include  "mbe_legacyoutputmodel.h"
#include  "state/mbe_state.h"


#define  LMOD_OUTPUT_MODULE_BASE  MBE_LegacyOutputModel
#define  LMOD_OUTPUT_MODULE_NAME  OutputArableReportGraze
#define  LMOD_OUTPUT_MODULE_ID    "output:report:arable:graze"
#define  LMOD_OUTPUT_MODULE_DESC  "Output Agricultural Summary (Graze)"

namespace ldndc {
class  substate_physiology_t;
class  LDNDC_API  LMOD_OUTPUT_MODULE_NAME  :  public  LMOD_OUTPUT_MODULE_BASE
{
    LMOD_EXPORT_MODULE_INFO(LMOD_OUTPUT_MODULE_NAME,LMOD_OUTPUT_MODULE_ID,LMOD_OUTPUT_MODULE_DESC);
    public:
        LMOD_OUTPUT_MODULE_NAME(
                MoBiLE_State *,
                cbm::io_kcomm_t *,
                timemode_e);

        ~LMOD_OUTPUT_MODULE_NAME();


        lerr_t  configure( ldndc::config_file_t const *);

        lerr_t  initialize();

        lerr_t  solve();

        lerr_t  finalize();

        lerr_t  wake() { return  LDNDC_ERR_OK; }
        lerr_t  sleep() { return  LDNDC_ERR_OK; }

    private:
        cbm::io_kcomm_t *  io_kcomm;
        MoBiLE_PlantVegetation *  m_veg;

        cbm::state_scratch_t *  scratchpad;
        lerr_t  dump_graze_( ldndc_flt64_t *);
        ldndc::sink_handle_t  m_sink;
};
} /*namespace ldndc*/


#undef  LMOD_OUTPUT_MODULE_BASE
#undef  LMOD_OUTPUT_MODULE_NAME
#undef  LMOD_OUTPUT_MODULE_ID
#undef  LMOD_OUTPUT_MODULE_DESC


#endif  /*  !LM_OUTPUT_ARABLEREPORTGRAZE_H_  */

