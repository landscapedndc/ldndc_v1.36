/*!
 * @brief
 *
 * @author
 *    steffen klatt (created on: feb 17, 2014),
 *    edwin haas
 */

#include  "output/event-report/output-arable-report-manure.h"

#include  "constants/lconstants-conv.h"
#include  "constants/lconstants-phys.h"

#define  LMOD_OUTPUT_MODULE_BASE  MBE_LegacyOutputModel
#define  LMOD_OUTPUT_MODULE_NAME  OutputArableReportManure
#define  LMOD_OUTPUT_MODULE_TIMEMODE  TMODE_SUBDAILY|TMODE_POST_DAILY

LMOD_MODULE_INFO(LMOD_OUTPUT_MODULE_NAME,LMOD_OUTPUT_MODULE_TIMEMODE,LMOD_FLAG_USER|LMOD_FLAG_OUTPUT);

/*!
 * @page managementoutput
 * @section manureoutput Manure output
 *
 * entity name | decription | unit
 * ----------- | ---------- | ----
 * dN\_manure | Daily carbon application in form of manure | [kgCha-1]
 * dN\_yearsum | Daily nitrogen application in form of manure | [kgNha-1]
 * dC\_manure | Yearly carbon application in form of manure | [kgCha-1]
 * dC\_yearsum | Yearly nitrogen application in form of manure | [kgNha-1]
 */

namespace ldndc {

/* MANURE */
ldndc_string_t const  OutputArableReportManure_Ids[] =
{
    "dN_manure", "dN_yearsum", "dC_manure", "dC_yearsum"
};
ldndc_string_t const  OutputArableReportManure_Header[] =
{
    "dN_manure[kgNha-1]", "dN_yearsum[kgNha-1]", "dC_manure[kgCha-1]", "dC_yearsum[kgCha-1]"
};
#define  OutputArableReportManure_Datasize  (sizeof( OutputArableReportManure_Header) / sizeof( OutputArableReportManure_Header[0]))
ldndc_output_size_t const  OutputArableReportManure_Sizes[] =
{
    OutputArableReportManure_Datasize,

    OutputArableReportManure_Datasize /*total size*/
};
ldndc_output_size_t const *  OutputArableReportManure_EntitySizes = NULL;
#define  OutputArableReportManure_Rank  ((ldndc_output_rank_t)(sizeof( OutputArableReportManure_Sizes) / sizeof( OutputArableReportManure_Sizes[0])) - 1)
atomic_datatype_t const  OutputArableReportManure_Types[] =
{
    LDNDC_FLOAT64
};

LMOD_OUTPUT_MODULE_NAME::LMOD_OUTPUT_MODULE_NAME(
    MoBiLE_State *  _state,
    cbm::io_kcomm_t *  _io_kcomm,
    timemode_e  _timemode)
    : LMOD_OUTPUT_MODULE_BASE( _state, _io_kcomm, _timemode),

    io_kcomm( _io_kcomm),
    manure_n_sum_yr_( 0.0),
    manure_c_sum_yr_( 0.0)
{
}


LMOD_OUTPUT_MODULE_NAME::~LMOD_OUTPUT_MODULE_NAME()
{
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::configure(
                ldndc::config_file_t const *  _cf)
{
    lerr_t  rc_setflags = this->set_metaflags( _cf);
    if ( rc_setflags)
        { return  rc_setflags; }

    int  producer_manure_timemode = TMODE_NONE;
    this->io_kcomm->get_scratch()->get( "producer.manure.timemode",
            &producer_manure_timemode, producer_manure_timemode);
    if ( producer_manure_timemode == TMODE_NONE)
    {
        KLOGWARN( "nobody sends me data :( going to sleep.");
        this->set_active_off();
        this->sleep();
    }
    else if ( this->timemode() != producer_manure_timemode)
    {
        timemode_e  tm =
            static_cast< timemode_e >( producer_manure_timemode);
        KLOGERROR( "oh no, i am in temporal disagreement with my producer :o",
                "  please select appropriate timemode",
                "  [expecting=",ldndc::time::timemode_name( tm),"]");
        return  LDNDC_ERR_FAIL;
    }
    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::initialize()
{
    this->manure_n_sum_yr_ = 0.0;
    this->manure_c_sum_yr_ = 0.0;

    this->m_sink = this->io_kcomm->sink_handle_acquire( "arablereportmanure");
    if ( this->m_sink.status() == LDNDC_ERR_OK)
    {
        lerr_t  rc_layout = LDNDC_OUTPUT_DEFINE_SINK_LAYOUT_DEFAULTS(
                    this->m_sink,OutputArableReportManure);
        RETURN_IF_NOT_OK(rc_layout);
    }
    else
    {
        KLOGERROR( "sink status bad  [sink=","arablereportmanure","]");
        return  this->m_sink.status();
    }

    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::solve()
{
    ldndc_flt64_t  data_flt64_0[OutputArableReportManure_Datasize];
    lerr_t  rc_dump = this->dump_manure_( data_flt64_0);
    RETURN_IF_NOT_OK(rc_dump);

        return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::finalize()
{
    this->io_kcomm->sink_handle_release( &this->m_sink);
    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::dump_manure_(
        ldndc_flt64_t *  _buf)
{
    cbm::state_scratch_t *  mcom = this->io_kcomm->get_scratch();

    double  manure_n_sum = 0.0;
    lerr_t  rc_have_manure_n = mcom->get_and_remove( "manure:amountN", &manure_n_sum, 0.0);
    this->manure_n_sum_yr_ += manure_n_sum;

    double  manure_c_sum = 0.0;
    lerr_t  rc_have_manure_c = mcom->get_and_remove( "manure:amountC", &manure_c_sum, 0.0);
    this->manure_c_sum_yr_ += manure_c_sum;

    bool const  is_eoy = ( this->lclock_ref().is_position( TMODE_POST_YEARLY) && this->lclock_ref().is_position( TMODE_POST_DAILY)) || ( false/*final day?*/);
    if ( (rc_have_manure_c == LDNDC_ERR_OK) || ( rc_have_manure_n == LDNDC_ERR_OK) || is_eoy)
    {
        LDNDC_OUTPUT_SET_COLUMN_INDEX(0);

        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(manure_n_sum);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->manure_n_sum_yr_);

        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(manure_c_sum);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->manure_c_sum_yr_);

        void *  data[] = { _buf};
        lerr_t  rc_write =
            this->write_fixed_record( &this->m_sink, data);
        if ( rc_write)
            { return  LDNDC_ERR_FAIL; }

        /* reset */
        if ( is_eoy)
        {
            this->manure_n_sum_yr_ = 0.0;
            this->manure_c_sum_yr_ = 0.0;
        }
    }

    return  LDNDC_ERR_OK;
}

#undef  LMOD_OUTPUT_MODULE_TIMEMODE
#undef  LMOD_OUTPUT_MODULE_BASE
#undef  LMOD_OUTPUT_MODULE_NAME

#undef  OutputArableReportManure_Rank
#undef  OutputArableReportManure_Datasize

} /*namespace ldndc*/

