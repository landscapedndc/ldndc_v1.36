/*!
 * @brief
 *    dumping rates for isotope calculations
 *
 * @author
 *    tobias denk
 */

#include  "output/isotopes/output-isotopes-daily.h"
#include  "physiology/ld_canopylayer.h"
#include  "ld_isotopes.h"

#include  <scientific/meteo/ld_meteo.h>


#define  LMOD_OUTPUT_MODULE_BASE  MBE_LegacyOutputModel
#define  LMOD_OUTPUT_MODULE_NAME  OutputIsotopesDaily
#define  LMOD_OUTPUT_MODULE_TIMEMODE  TMODE_SUBDAILY|TMODE_POST_DAILY

//LMOD_MODULE_INFO(LMOD_OUTPUT_MODULE_NAME,TMODE_POST_DAILY,LMOD_FLAG_USER|LMOD_FLAG_OUTPUT);
LMOD_MODULE_INFO(LMOD_OUTPUT_MODULE_NAME,LMOD_OUTPUT_MODULE_TIMEMODE,LMOD_FLAG_USER|LMOD_FLAG_OUTPUT);
namespace ldndc {


ldndc_string_t const  OutputIsotopesPools_Ids[] =
{
    "ureainitialisation",
    "litterinitialisation",
    "aorginitialisation",
    "nh4initialisation",
    "humusinitialisation",
    "micro1initialisation",
    "micro3initialisation",
    "nh3initialisation",
    "clayinitialisation",
    "no3initialisation",
    "no3aninitialisation",
    "no2initialisation",
    "no2aninitialisation",
    "noinitialisation",
    "noaninitialisation",
    "n2oinitialisation",
    "n2oaninitialisation"
};
ldndc_string_t const  OutputIsotopesPools_Header[] =
{
    "urea",
    "litter",
    "aorg",
    "nh4",
    "humus",
    "micro1",
    "micro3",
    "nh3",
    "clay",
    "no3",
    "no3_an",
    "no2",
    "no2_an",
    "no",
    "no_an",
    "n2o",
    "n2o_an"
};
#define  OutputIsotopesPools_Datasize  (sizeof( OutputIsotopesPools_Header) / sizeof( OutputIsotopesPools_Header[0]))
ldndc_output_size_t const  OutputIsotopesPools_Sizes[] =
{
    OutputIsotopesPools_Datasize,

    OutputIsotopesPools_Datasize /*total size*/
};
ldndc_output_size_t const *  OutputIsotopesPools_EntitySizes = NULL;
#define  OutputIsotopesPools_Rank  ((ldndc_output_rank_t)(sizeof( OutputIsotopesPools_Sizes) / sizeof( OutputIsotopesPools_Sizes[0])) - 1)
atomic_datatype_t const  OutputIsotopesPools_Types[] =
{
    LDNDC_FLOAT64
};



ldndc_string_t const  OutputIsotopesDaily_Ids[] =
{
    "planting",                         //plantN from planting
    "nh4_throughfall",                  //nh4 throughfall
    "no3_throughfall",                  //no3 throughfall
    "nh4uptake",                        //nh4 uptake
    "no3uptake",                        //no3 uptake
    "an_no3uptake",                     //an_no3 uptake
    "nh3uptake",                        //nh3 uptake
    "n2fixation",                       //n2 fixation
    "cutting",                          //cutting
    "fertlitter",                       //litter fertilization
    "fertNaorg",                        //Naorg fertilization
    "fertnh4",                          //nh4 fertilization
    "ferturea",                         //urea fertilization
    "fertno3",                          //no3 fertilization
    "till_litter",                      //litter addition
    "till_nh4",                         //nh4 addition
    "littertilled",                     //real tilling
    "Naorgtilled",                      //real tilling
    "humustilled",                      //real tilling
    "no3tilled",                        //real tilling
    "anno3tilled",                      //real tilling
    "no2tilled",                        //real tilling
    "anno2tilled",                      //real tilling
    "nh4tilled",                        //real tilling
    "claynh4tilled",                    //real tilling
    "ureatilled",                       //real tilling
    "nh3tilled",                        //real tilling
    "micro1tilled",                     //real tilling
    "micro3tilled",                     //real tilling
    "urea_hydro",                       //urea to nh4
    "grazing_nh4",                      //nh4 addition
    "grazing_urine",                    //urea addition
    "litterproduction",                 //plants to litter
    "nh4production",                    //plants to nh4
    "perturbation",                     //litter "leaching"
    "nh4surfacewater",                  //nh4 surface water
    "no3surfacewater",                  //no3 surface water
    "leach_nh4",                        //nh4 leaching
    "leach_no3_ae",                     //no3 aerob leaching
    "leach_no3_an",                     //no3 anaerob leaching
    "litterdecompositionNaorg",         //litter to Naorg
    "litterdecompositionnh4",           //litter to nh4
    "humusdecomposition",               //humus to nh4
    "mic_nh4_growth",                   //aorg to nh4
    "humad_decomposotion",              //aorg to nh4
    "humad_trans",                      //nh4 to humus
    "equilibrium",                      //nh4 to nh3_liq
    "volatilization",                   //nh3_liq to nh3_emis
    "nitrify_no",                       //nh4 to no
    "nitrify_n2o",                      //nh4 to n2o
    "netNitrify",                       //nh4 to no2
    "nitrify_no2",                      //no2 to no3
    "mic3_no3nh4",                      //no3 to nh4
    "mic3_growthanddeath",              //aorg to mic3
    "combine_aorg_nh4flux",             //nh4 correction compared to Naorg
    "clay_nh4_flux"                     //nh4 to clay
};
ldndc_string_t const  OutputIsotopesDaily_Header[] =
{
    "planting[kgNm-2]",
    "nh4throughfall[kgNm-2]",
    "no3throughfall[kgNm-2]",
    "nh4uptake[kgNm-2]",
    "no3uptake[kgNm-2]",
    "anno3uptake[kgNm-2]",
    "nh3uptake[kgNm-2]",
    "n2fixation[kgNm-2]",
    "cutting[kgNm-2]",
    "fertlitter[kgNm-2]",
    "fertNaorg[kgNm-2]",
    "fertnh4[kgNm-2]",
    "ferturea[kgNm-2]",
    "fertno3[kgNm-2]",
    "tilllitter[kgNm-2]",
    "tillnh4[kgNm-2]",
    "littertilled[kgNm-2]",
    "Naorgtilled[kgNm-2]",
    "humustilled[kgNm-2]",
    "no3tilled[kgNm-2]",
    "anno3tilled[kgNm-2]",
    "no2tilled[kgNm-2]",
    "anno2tilled[kgNm-2]",
    "nh4tilled[kgNm-2]",
    "claynh4tilled[kgNm-2]",
    "ureatilled[kgNm-2]",
    "nh3tilled[kgNm-2]",
    "micro1tilled[kgNm-2]",
    "micro3tilled[kgNm-2]",
    "urea_hydro[kgNm-2]",
    "grazing_nh4[kgNm-2]",
    "grazing_urea[kgNm-2]",
    "litterproduction[kgNm-2]",
    "nh4littering[kgNm-2]",
    "perturbation[kgNm-2]",
    "nh4surfacewater[kgNm-2]",
    "no3surfacewater[kgNm-2]",
    "leachnh4[kgNm-2]",
    "leachno3ae[kgNm-2]",
    "leachno3an[kgNm-2]",
    "litterdecompositionNaorg[kgNm-2]",
    "litterdecompositionnh4[kgNm-2]",
    "humusdecomposition[kgNm-2]",
    "mic_nh4_growth[kgNm-2]",
    "humad_decomp[kgNm-2]",
    "humad_trans[kgNm-2]",
    "equilibrium[kgNm-2]",
    "volatilization[kgNm-2]",
    "nit_no[kgNm-2]",
    "nit_n2o[kgNm-2]",
    "nit_no2[kgNm-2]",
    "nit_no3[kgNm-2]",
    "mic3no3nh4[kgNm-2]",
    "mic3growth&death[kgNm-2]",
    "combine_aorg_nh4flux[kgNm-2]",
    "clay_nh4_flux[kgNm-2]"

};
#define  OutputIsotopesDaily_Datasize  (sizeof( OutputIsotopesDaily_Header) / sizeof( OutputIsotopesDaily_Header[0]))
ldndc_output_size_t const  OutputIsotopesDaily_Sizes[] =
{
    OutputIsotopesDaily_Datasize,

    OutputIsotopesDaily_Datasize /*total size*/
};
ldndc_output_size_t const *  OutputIsotopesDaily_EntitySizes = NULL;
#define  OutputIsotopesDaily_Rank  ((ldndc_output_rank_t)(sizeof( OutputIsotopesDaily_Sizes) / sizeof( OutputIsotopesDaily_Sizes[0])) - 1)
atomic_datatype_t const  OutputIsotopesDaily_Types[] =
{
    LDNDC_FLOAT64
};


LMOD_OUTPUT_MODULE_NAME::LMOD_OUTPUT_MODULE_NAME(
        MoBiLE_State *  _state,
        cbm::io_kcomm_t *  _io_kcomm,
        timemode_e  _timemode)
    : LMOD_OUTPUT_MODULE_BASE( _state, _io_kcomm, _timemode),

    io_kcomm( _io_kcomm),
    soillayers_in( _io_kcomm->get_input_class< input_class_soillayers_t >())
    { }


LMOD_OUTPUT_MODULE_NAME::~LMOD_OUTPUT_MODULE_NAME()
    { }


lerr_t
LMOD_OUTPUT_MODULE_NAME::configure(
            ldndc::config_file_t const *  _cf)
{
    lerr_t  rc_setflags =
        this->set_metaflags( _cf, RM_DEFAULT_LAYERDAILY);
    if ( rc_setflags)
        { return  LDNDC_ERR_FAIL; }

    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::initialize()
{
    this->m_sinkpools = this->io_kcomm->sink_handle_acquire( "isotopespools");
    if ( this->m_sinkpools.status() == LDNDC_ERR_OK)
    {
        lerr_t  rc_layout = LDNDC_OUTPUT_DEFINE_SINK_LAYOUT_DEFAULTS(
            this->m_sinkpools,OutputIsotopesPools);
        RETURN_IF_NOT_OK(rc_layout);
    }
    else
    {
        KLOGERROR( "sink status bad  [sink=","isotopespools","]");
        return  this->m_sinkpools.status();
    }


    ldndc_flt64_t  data_flt64_0[OutputIsotopesPools_Datasize];
    int const  L =
        static_cast< int >( this->soillayers_in->soil_layer_cnt());
    for ( int  l = 0;  l < L;  ++l)
    {
        lerr_t  rc_dump =
            this->dump_pools( data_flt64_0, l);
        if ( rc_dump)
            { return  LDNDC_ERR_FAIL; }

        void *  data[] = { data_flt64_0};
        this->set_layernumber( -l-1);
        lerr_t  rc_write =
            this->write_fixed_record( &this->m_sinkpools, data);
        if ( rc_write)
            { return  LDNDC_ERR_FAIL; }
    }
    this->io_kcomm->sink_handle_release( &this->m_sinkpools);


    this->m_sink = this->io_kcomm->sink_handle_acquire( "isotopesdaily");
    if ( this->m_sink.status() == LDNDC_ERR_OK)
    {
        lerr_t  rc_layout = LDNDC_OUTPUT_DEFINE_SINK_LAYOUT_DEFAULTS(
            this->m_sink,OutputIsotopesDaily);
        RETURN_IF_NOT_OK(rc_layout);
    }
    else
    {
        KLOGERROR( "sink status bad  [sink=","isotopesdaily","]");
        return  this->m_sink.status();
    }

    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::solve()
{

    //std::cout << "t = " << lclock()->subday() << std::endl;
    if( lclock()->subday() == 24 )
    {

      ldndc_flt64_t  data_flt64_0[OutputIsotopesDaily_Datasize];

      int const  L =
          static_cast< int >( this->soillayers_in->soil_layer_cnt());
      for ( int  l = 0;  l < L;  ++l)
      {
          lerr_t  rc_dump = this->dump_0( data_flt64_0, l);
          if ( rc_dump)
              { return  LDNDC_ERR_FAIL; }

          void *  data[] = { data_flt64_0};
          this->set_layernumber( -l-1);
          lerr_t  rc_write =
              this->write_fixed_record( &this->m_sink, data);
          if ( rc_write)
              { return  LDNDC_ERR_FAIL; }
      }
    }

        return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::finalize()
{
    this->io_kcomm->sink_handle_release( &this->m_sink);
    return  LDNDC_ERR_OK;
}

lerr_t
LMOD_OUTPUT_MODULE_NAME::dump_0(
        ldndc_flt64_t *  _buf,
        int _sl)
{


    cbm::state_scratch_t *  mcom = this->io_kcomm->get_scratch();


    if (_sl == 0)
    {


        std::string  mcom_key;
        double  value = 0.0;
        double  cutandharvest = 0.0;

        mcom->get( "cut:mbudN", &value, 0.0);
        cutandharvest += value;
        //mcom->get( "cut:mfolN", &value, 0.0);
        mcom->get( "cut:n_fol_export", &value, 0.0);
        cutandharvest += value;
        mcom->get( "cut:msapN", &value, 0.0);
        cutandharvest += value;
        Isotopes::keybuffer_t  iso_keyname;
        Isotopes::keyname(  iso_keyname, "plantloss", _sl);
        mcom->get_and_remove( iso_keyname, &value, 0.0);
        cutandharvest += value;
        mcom->get( "graze:mbudN", &value, 0.0);
        cutandharvest += value;
        mcom->get( "graze:mfolN", &value, 0.0);
        cutandharvest += value;
        mcom->get( "graze:msapN", &value, 0.0);
        cutandharvest += value;
        mcom->get( "graze:mfrtN", &value, 0.0);
        cutandharvest += value;
        mcom->get( "graze:dungnitrogen", &value, 0.0);
        cutandharvest -= value*cbm::HA_IN_M2;
        while ( this->m_HarvestEvents)
        {
            EventAttributes  ev_harv = this->m_HarvestEvents.pop();
            cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:budN", ev_harv.get( "/name", "?"));
            mcom->get( mcom_key.c_str(), &value, 0.0);
            cutandharvest += value;
            cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:strawN", ev_harv.get( "/name", "?"));
            mcom->get( mcom_key.c_str(), &value, 0.0);
            cutandharvest += value;
        }

        Isotopes::keybuffer_t  cutandharvestkey;
        Isotopes::keyname( cutandharvestkey, "cutting", 0);
        mcom->set( cutandharvestkey, cutandharvest);

        //std::cout << "cutandharvest = " << cutandharvest << std::endl;
    }
    else
    {
        /* cutting defaults to zero */
    }

    LDNDC_OUTPUT_SET_COLUMN_INDEX(0);
    for ( size_t  j = 0;  j < OutputIsotopesDaily_Datasize;  ++j)
    {
        double  itemvalue = 0.0;

        char const *  itemid = OutputIsotopesDaily_Ids[j];
        Isotopes::keybuffer_t  itemkey;
        Isotopes::keyname( itemkey, itemid, _sl);

        mcom->get_and_remove( itemkey, &itemvalue, 0.0);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( itemvalue);
    }

    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::dump_pools(
        ldndc_flt64_t *  _buf,
        int _sl)
{
    cbm::state_scratch_t *  mcom = this->io_kcomm->get_scratch();

    LDNDC_OUTPUT_SET_COLUMN_INDEX(0);
    for ( size_t  j = 0;  j < OutputIsotopesPools_Datasize;  ++j)
    {
        double  itemvalue = 0.0;

        char const *  itemid = OutputIsotopesPools_Ids[j];
        Isotopes::keybuffer_t  itemkey;
        Isotopes::keyname( itemkey, itemid, _sl);

        mcom->get_and_remove( itemkey, &itemvalue, 0.0);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( itemvalue);
    }

    return  LDNDC_ERR_OK;
}

static int  _QueueEventHarvest( void const *  _msg, size_t  _msg_sz, void *  _queue)
{
    ldndc::EventQueue *  queue =
        static_cast< ldndc::EventQueue * >( _queue);
    if ( !queue)
        { return -1; }

    ldndc::EventAttributes  harvest_event(
                "harvest", (char const *)_msg, _msg_sz);
    queue->push( harvest_event);
    return 0;
}

lerr_t
OutputIsotopesDaily::register_ports( cbm::io_kcomm_t *  _io_kcomm)
{
    CBM_Callback  cb_harvest;
    cb_harvest.fn = &_QueueEventHarvest;
    cb_harvest.data = &this->m_HarvestEvents;
    this->m_HarvestHandle = _io_kcomm->subscribe_event( "harvest", cb_harvest);
    if ( !CBM_HandleOk(this->m_HarvestHandle))
        { return  LDNDC_ERR_FAIL; }

    return  LDNDC_ERR_OK;
}

lerr_t
OutputIsotopesDaily::unregister_ports( cbm::io_kcomm_t *  _io_kcomm)
{
    _io_kcomm->unsubscribe_event( this->m_HarvestHandle);

    return  LDNDC_ERR_OK;
}


#undef  LMOD_OUTPUT_MODULE_BASE
#undef  LMOD_OUTPUT_MODULE_NAME

#undef  OutputIsotopesDaily_Rank
#undef  OutputIsotopesDaily_Datasize

} /*namespace ldndc*/
