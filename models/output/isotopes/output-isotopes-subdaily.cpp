/*!
 * @brief
 *    dumping rates for isotope calculations
 *
 * @author
 *    tobias denk
 */

#include  "output/isotopes/output-isotopes-subdaily.h"
#include  "ld_isotopes.h"

#include  <scientific/meteo/ld_meteo.h>

#define  LMOD_OUTPUT_MODULE_BASE  MBE_LegacyOutputModel
#define  LMOD_OUTPUT_MODULE_NAME  OutputIsotopesSubdaily

LMOD_MODULE_INFO(LMOD_OUTPUT_MODULE_NAME,TMODE_POST_DAILY,LMOD_FLAG_USER|LMOD_FLAG_OUTPUT);
namespace ldndc {

ldndc_string_t const  OutputIsotopesSubdaily_Ids[] =
{
    "hour",
    "mic_nitrateuse",                   //no3 use in microbes
    "mic_an_nitrateuse",                //no3_an use in microbes
    "mic_ammoniumuse",                  //nh4 use in microbes
    "mic_death",                        //microbes to aorg
    "mic_nitraterapid",                 //no3 use in nh4
    "mic_an_nitraterapid",              //no3_an use in nh4
    "mic_respiration",                  //microbes to nh4           
    "trans_no3",                        //an_no3 to no3 
    "trans_no2",                        //an_no2 to no2 
    "trans_no",                         //an_no to no 
    "trans_n2o",                        //an_n2o to n2o 
    "deni_no3_no2",                     //an_no3 to an_no2
    "deni_no3_mic",                     //an_no3 to microbes
    "deni_no2_n2o",                     //an_no2 to an_n2o
    "deni_no2_mic_1",                   //an_no2 to microbes
    "deni_no2_no",                      //an_no2 to an_no
    "deni_no2_mic_2",                   //an_no2 to microbes
    "deni_no_n2o",                      //an_no to an_n2o
    "deni_no_mic",                      //an_no to microbes
    "deni_n2o_n2",                      //an_n2o to an_n2
    "deni_n2o_mic",                     //an_n2o to microbes
    "deni_death_nh4",                   //microbes to nh4
    "deni_death_aorg",                  //microbes to aorg
    "calculate_aorg_nh4",               //nh4 correction compared to Naorg
    "no_diffusion",                     //no diffusion
    "an_no_diffusion",                  //anaerob diffusion
    "n2o_diffusion",                    //n2o diffusion
    "an_n2o_diffusion",                 //anaerob n2o diffusion
    "no_oxidation"                      //no oxidation to no3
};
ldndc_string_t const  OutputIsotopesSubdaily_Header[] =
{
    "hour",
    "mic_nitrateuse[kgNm-2]",
    "mic_an_nitrateuse[kgNm-2]",
    "mic_ammoniumuse[kgNm-2]",
    "mic_death[kgNm-2]",
    "mic_nitraterapid[kgNm-2]",
    "mic_an_nitraterapid[kgNm-2]",
    "mic_respiration[kgNm-2]",
    "trans_no3[kgNm-2]",
    "trans_no2[kgNm-2]",
    "trans_no[kgNm-2]",
    "trans_n2o[kgNm-2]",
    "deni_no3_no2[kgNm-2]",
    "deni_no3_mic[kgNm-2]",
    "deni_no2_n2o[kgNm-2]",
    "deni_no2_mic_1[kgNm-2]",
    "deni_no2_no[kgNm-2]",
    "deni_no2_mic_2[kgNm-2]",
    "deni_no_n2o[kgNm-2]",
    "deni_no_mic[kgNm-2]",
    "deni_n2o_n2[kgNm-2]",
    "deni_n2o_mic[kgNm-2]",
    "deni_death_nh4[kgNm-2]",
    "deni_death_aorg[kgNm-2]",
    "calculate_aorg_nh4[kgNm-2]",
    "no_diffusion[kgNm-2]",
    "an_no_diffusion[kgNm-2]",
    "n2o_diffusion[kgNm-2]",
    "an_n2o_diffusion[kgNm-2]",
    "no_oxidation[kgNm-2]"

};
#define  OutputIsotopesSubdaily_Datasize  (sizeof( OutputIsotopesSubdaily_Header) / sizeof( OutputIsotopesSubdaily_Header[0]))
ldndc_output_size_t const  OutputIsotopesSubdaily_Sizes[] =
{
    OutputIsotopesSubdaily_Datasize,

    OutputIsotopesSubdaily_Datasize /*total size*/
};
ldndc_output_size_t const *  OutputIsotopesSubdaily_EntitySizes = NULL;
#define  OutputIsotopesSubdaily_Rank  ((ldndc_output_rank_t)(sizeof( OutputIsotopesSubdaily_Sizes) / sizeof( OutputIsotopesSubdaily_Sizes[0])) - 1)
atomic_datatype_t const  OutputIsotopesSubdaily_Types[] =
{
    LDNDC_FLOAT64
};


LMOD_OUTPUT_MODULE_NAME::LMOD_OUTPUT_MODULE_NAME(
        MoBiLE_State *  _state,
        cbm::io_kcomm_t *  _io_kcomm,
        timemode_e  _timemode)
    : LMOD_OUTPUT_MODULE_BASE( _state, _io_kcomm, _timemode),

    io_kcomm( _io_kcomm),
    soillayers_in( _io_kcomm->get_input_class< input_class_soillayers_t >())
    { }

LMOD_OUTPUT_MODULE_NAME::~LMOD_OUTPUT_MODULE_NAME()
    { }


lerr_t
LMOD_OUTPUT_MODULE_NAME::configure(
                ldndc::config_file_t const *  _cf)
{
    lerr_t  rc_setflags =
        this->set_metaflags( _cf, RM_DEFAULT_LAYERSUBDAILY);
    if ( rc_setflags)
        { return  LDNDC_ERR_FAIL; }

    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::initialize()
{
    this->m_sink = this->io_kcomm->sink_handle_acquire( "isotopessubdaily");
    if ( this->m_sink.status() == LDNDC_ERR_OK)
    {
        lerr_t  rc_layout = LDNDC_OUTPUT_DEFINE_SINK_LAYOUT_DEFAULTS(
            this->m_sink,OutputIsotopesSubdaily);
        RETURN_IF_NOT_OK(rc_layout);
    }
    else
    {
        KLOGERROR( "sink status bad  [sink=","isotopessubdaily","]");
        return  this->m_sink.status();
    }

    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::solve()
{
    ldndc_flt64_t  data_flt64_0[OutputIsotopesSubdaily_Datasize];

    int  hours = 0;
    Isotopes::keybuffer_t  itemkey;
    Isotopes::keyname( itemkey, "hours");
    this->io_kcomm->get_scratch()->get( itemkey, &hours, 0);

    for ( int  hr = 0;  hr < hours;  ++hr)
    {
        int const  L =
            static_cast< int >( this->soillayers_in->soil_layer_cnt());
        for ( int  l = 0;  l < L;  ++l)
        {
            lerr_t  rc_dump = this->dump_0( data_flt64_0, hr, l);
            if ( rc_dump)
                { return  LDNDC_ERR_FAIL; }

            void *  data[] = { data_flt64_0};
            this->set_layernumber( -l-1);
            lerr_t  rc_write =
                this->write_fixed_record( &this->m_sink, data);
            if ( rc_write)
                { return  LDNDC_ERR_FAIL; }
        }
    }
    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::finalize()
{
    this->io_kcomm->sink_handle_release( &this->m_sink);
    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::dump_0(
        ldndc_flt64_t *  _buf, int _hour, int _sl)
{
    cbm::state_scratch_t *  mcom = this->io_kcomm->get_scratch();

    LDNDC_OUTPUT_SET_COLUMN_INDEX(0);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(_hour);

    for ( size_t  j = 1;  j < OutputIsotopesSubdaily_Datasize;  ++j)
    {
        double  itemvalue = 0.0;

        char const *  itemid = OutputIsotopesSubdaily_Ids[j];
        Isotopes::keybuffer_t  itemkey;
        Isotopes::keyname( itemkey, itemid, _sl, _hour);

        mcom->get_and_remove( itemkey, &itemvalue, 0.0);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( itemvalue);
    }
    return  LDNDC_ERR_OK;
}

#undef  LMOD_OUTPUT_MODULE_BASE
#undef  LMOD_OUTPUT_MODULE_NAME

#undef  OutputIsotopesSubdaily_Rank
#undef  OutputIsotopesSubdaily_Datasize

} /*namespace ldndc*/

