/*!
 * @brief
 *    dumping rates for isotope calculations
 *
 * @author
 *    tobias denk
 */

#ifndef  LM_OUTPUT_ISOTOPES_DAILY_H_
#define  LM_OUTPUT_ISOTOPES_DAILY_H_

#include  "mbe_legacyoutputmodel.h"
#include  "state/mbe_state.h"
#include  "ld_eventqueue.h"

#define  LMOD_OUTPUT_MODULE_BASE  MBE_LegacyOutputModel
#define  LMOD_OUTPUT_MODULE_NAME  OutputIsotopesDaily
#define  LMOD_OUTPUT_MODULE_ID    "output:isotopes:daily"
#define  LMOD_OUTPUT_MODULE_DESC  "Isotopes Daily Output"
namespace ldndc {
class  substate_soilchemistry_t;
class  LDNDC_API  LMOD_OUTPUT_MODULE_NAME  :  public  LMOD_OUTPUT_MODULE_BASE
{
    LMOD_EXPORT_MODULE_INFO(LMOD_OUTPUT_MODULE_NAME,LMOD_OUTPUT_MODULE_ID,LMOD_OUTPUT_MODULE_DESC);
    public:
        LMOD_OUTPUT_MODULE_NAME( MoBiLE_State *,
                cbm::io_kcomm_t *, timemode_e  _timemode);
        ~LMOD_OUTPUT_MODULE_NAME();

        lerr_t  configure( ldndc::config_file_t const *);
        lerr_t  register_ports( cbm::io_kcomm_t *);
        lerr_t  initialize();

        lerr_t  solve();
        lerr_t  unregister_ports( cbm::io_kcomm_t *);
        lerr_t  finalize();

        lerr_t  wake() { return  LDNDC_ERR_OK; }
        lerr_t  sleep() { return  LDNDC_ERR_OK; }

    private:
        lerr_t  dump_0(
                ldndc_flt64_t * /*buffer*/, int  /*layer index*/);
        lerr_t  dump_pools(
                ldndc_flt64_t * /*buffer*/, int  /*layer index*/);

    private:
        cbm::io_kcomm_t *  io_kcomm;
        input_class_soillayers_t const *  soillayers_in;

    private:
        ldndc::sink_handle_t  m_sink;
        ldndc::sink_handle_t  m_sinkpools;

        EventQueue  m_HarvestEvents;
        CBM_Handle  m_HarvestHandle;
};
} /*namespace ldndc*/


#undef  LMOD_OUTPUT_MODULE_BASE
#undef  LMOD_OUTPUT_MODULE_NAME
#undef  LMOD_OUTPUT_MODULE_ID
#undef  LMOD_OUTPUT_MODULE_DESC


#endif  /*  !LM_OUTPUT_isotopes_DAILY_H_  */

