/*!
 * @brief
 *    dumping modified water cycle related items
 *
 * @author
 *    steffen klatt,
 *    edwin haas
 */

#ifndef  LM_OUTPUT_WATERCYCLE_LAYER_SUBDAILY_H_
#define  LM_OUTPUT_WATERCYCLE_LAYER_SUBDAILY_H_

#include  "mbe_legacyoutputmodel.h"
#include  "state/mbe_state.h"

#define  LMOD_OUTPUT_MODULE_BASE  MBE_LegacyOutputModel
#define  LMOD_OUTPUT_MODULE_NAME  OutputWatercycleLayerSubdaily
#define  LMOD_OUTPUT_MODULE_ID    "output:watercycle-layer:subdaily"
#define  LMOD_OUTPUT_MODULE_DESC  "Water Cycle Subdaily Output (Layers)"
namespace ldndc {
class  LDNDC_API  LMOD_OUTPUT_MODULE_NAME  :  public  LMOD_OUTPUT_MODULE_BASE
{
    LMOD_EXPORT_MODULE_INFO(LMOD_OUTPUT_MODULE_NAME,LMOD_OUTPUT_MODULE_ID,LMOD_OUTPUT_MODULE_DESC);
    public:
        LMOD_OUTPUT_MODULE_NAME(
                MoBiLE_State *,
                cbm::io_kcomm_t *,
                timemode_e  _timemode);

        ~LMOD_OUTPUT_MODULE_NAME();


        lerr_t  configure( ldndc::config_file_t const *);

        lerr_t  initialize();

        lerr_t  solve();

        lerr_t  finalize();

        lerr_t  wake() { return  LDNDC_ERR_OK; }
        lerr_t  sleep() { return  LDNDC_ERR_OK; }

    private:
        lerr_t  dump_0( ldndc_flt64_t * /*buffer*/);

        struct  output_watercycle_cum_acc_t
        {
            double infiltration_cum;
            double * waterflux_cum_sl;
        };
        output_watercycle_cum_acc_t  acc;

        void  pop_accumulated_outputs();
        void  push_accumulated_outputs();

    private:
        cbm::io_kcomm_t *  io_kcomm;
        MoBiLE_PlantVegetation *  m_veg;

        input_class_soillayers_t const *  soillayers_;
        input_class_setup_t const *  setup_;
        substate_soilchemistry_t const *  sc;
        substate_physiology_t const *  ph;
        substate_watercycle_t const *  wc;

    private:
        ldndc::sink_handle_t  m_sink;

        /* configuration options: */
        /*    foliage and soil layer selection */
        int  f_sel_, s_sel_;
};
} /*namespace ldndc*/


#undef  LMOD_OUTPUT_MODULE_BASE
#undef  LMOD_OUTPUT_MODULE_NAME
#undef  LMOD_OUTPUT_MODULE_ID
#undef  LMOD_OUTPUT_MODULE_DESC


#endif  /*  !LM_OUTPUT_WATERCYCLE_LAYER_SUBDAILY_H_  */

