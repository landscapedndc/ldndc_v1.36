/*!
 * @brief
 *    dumping modified water cycle related items
 *
 * @author
 *    steffen klatt (created on: nov. 27, 2014)
 */

#ifndef  LM_OUTPUT_WATERCYCLE_SUBDAILY_H_
#define  LM_OUTPUT_WATERCYCLE_SUBDAILY_H_

#include  "mbe_legacyoutputmodel.h"
#include  "state/mbe_state.h"

#define  LMOD_OUTPUT_MODULE_BASE  MBE_LegacyOutputModel
#define  LMOD_OUTPUT_MODULE_NAME  OutputWatercycleSubdaily
#define  LMOD_OUTPUT_MODULE_ID    "output:watercycle:subdaily"
#define  LMOD_OUTPUT_MODULE_DESC  "Water Cycle Subdaily Output"
namespace ldndc {
class  substate_microclimate_t;
class  substate_physiology_t;
class  substate_soilchemistry_t;
class  substate_watercycle_t;
class  LDNDC_API  LMOD_OUTPUT_MODULE_NAME  :  public  LMOD_OUTPUT_MODULE_BASE
{
    LMOD_EXPORT_MODULE_INFO(LMOD_OUTPUT_MODULE_NAME,LMOD_OUTPUT_MODULE_ID,LMOD_OUTPUT_MODULE_DESC);
    public:
        LMOD_OUTPUT_MODULE_NAME(
                MoBiLE_State *,
                cbm::io_kcomm_t *,
                timemode_e  _timemode);

        ~LMOD_OUTPUT_MODULE_NAME();


        lerr_t  configure( ldndc::config_file_t const *);

        lerr_t  initialize();

        lerr_t  solve();

        lerr_t  finalize();

        lerr_t  wake() { return  LDNDC_ERR_OK; }
        lerr_t  sleep() { return  LDNDC_ERR_OK; }

    private:
        lerr_t  dump_0(
                ldndc_flt64_t *);

    private:
        cbm::io_kcomm_t *  io_kcomm;

        input_class_soillayers_t const *  soillayers_in;
        input_class_setup_t const *  setup_;
        substate_microclimate_t const *  microclim;
        substate_physiology_t const *  phys;
        substate_soilchemistry_t const *  soilchem;
        substate_watercycle_t const *  water;

        MoBiLE_PlantVegetation *  m_veg;

    private:
        ldndc::sink_handle_t  m_sink;

        struct  output_watercycle_daily_acc_t
        {
            double  throughfall;
            double  potentialevaporation;
            double  transpiration;
            double  interceptionevaporation;
            double  soilevaporation;
            double  runoff;
            double  percolation;
            double  infiltration;
        };
        output_watercycle_daily_acc_t  acc;
        void  pop_accumulated_outputs();
        void  push_accumulated_outputs();
};
} /*namespace ldndc*/


#undef  LMOD_OUTPUT_MODULE_BASE
#undef  LMOD_OUTPUT_MODULE_NAME
#undef  LMOD_OUTPUT_MODULE_ID
#undef  LMOD_OUTPUT_MODULE_DESC


#endif  /*  !LM_OUTPUT_WATERCYCLE_SUBDAILY_H_  */

