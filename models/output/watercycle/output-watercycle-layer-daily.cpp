/*!
 * @file
 * @author
 *  - Edwin Haas
 *  - Ruediger Grote
 *  - David Kraus
 */

#include  "output/watercycle/output-watercycle-layer-daily.h"
#include  "constants/lconstants-conv.h"

#define  LMOD_OUTPUT_MODULE_BASE  MBE_LegacyOutputModel
#define  LMOD_OUTPUT_MODULE_NAME  OutputWatercycleLayerDaily

#ifdef  OutputWatercycleLayerDaily_HAVE_request_api
#  include  "comm/lmessage.h"
static
int
ldndc_mobile_message_handler_output_watercycle_daily(
        void *  _module, lmessage_t const *  _msg, lreply_t *  _reply)
{
    int  rc = -1;
    ldndc::OutputWatercycleLayerDaily *  obj =
        static_cast< ldndc::OutputWatercycleLayerDaily * >( _module);
    if ( obj)
    {
        rc = obj->process_message( _msg, _reply);
    }
    return  rc;
}
#endif

LMOD_MODULE_INFO(LMOD_OUTPUT_MODULE_NAME,TMODE_POST_DAILY,LMOD_FLAG_USER|LMOD_FLAG_OUTPUT);
namespace ldndc {

#define  OutputWatercycleLayerDaily_Item__level
#define  OutputWatercycleLayerDaily_Item__extension

#define  OutputWatercycleLayerDaily_Item__water
#define  OutputWatercycleLayerDaily_Item__wfps
#define  OutputWatercycleLayerDaily_Item__ice
#define  OutputWatercycleLayerDaily_Item__flux
#define  OutputWatercycleLayerDaily_Item__droughtstress
#define  OutputWatercycleLayerDaily_Item__transpiration

ldndc_string_t const  OutputWatercycleLayerDaily_Ids[] =
{
#ifdef  OutputWatercycleLayerDaily_Item__level
    "level",
#endif
#ifdef  OutputWatercycleLayerDaily_Item__extension
    "extension",
#endif
#ifdef  OutputWatercycleLayerDaily_Item__water
    "water", 
#endif
#ifdef  OutputWatercycleLayerDaily_Item__wfps
    "wfps", 
#endif
#ifdef  OutputWatercycleLayerDaily_Item__ice
    "ice", 
#endif
#ifdef  OutputWatercycleLayerDaily_Item__flux
    "flux",
#endif
#ifdef  OutputWatercycleLayerDaily_Item__droughtstress
    "droughtstress",
#endif
#ifdef  OutputWatercycleLayerDaily_Item__transpiration
    "transpiration"
#endif
};
ldndc_string_t const  OutputWatercycleLayerDaily_Header[] =
{
#ifdef  OutputWatercycleLayerDaily_Item__level
    "level[m]",
#endif
#ifdef  OutputWatercycleLayerDaily_Item__extension
    "extension[m]",
#endif
#ifdef  OutputWatercycleLayerDaily_Item__water
    "water[l/m-2]", 
#endif
#ifdef  OutputWatercycleLayerDaily_Item__wfps
    "wfps[%]", 
#endif
#ifdef  OutputWatercycleLayerDaily_Item__ice
    "ice", 
#endif
#ifdef  OutputWatercycleLayerDaily_Item__flux
    "flux",
#endif
#ifdef  OutputWatercycleLayerDaily_Item__droughtstress
    "droughtstress[-]",
#endif
#ifdef  OutputWatercycleLayerDaily_Item__transpiration
    "transpiration[mm]"
#endif
};
#define  OutputWatercycleLayerDaily_Datasize  (sizeof( OutputWatercycleLayerDaily_Header) / sizeof( OutputWatercycleLayerDaily_Header[0]))
ldndc_output_size_t const  OutputWatercycleLayerDaily_Sizes[] =
{
    OutputWatercycleLayerDaily_Datasize,

    OutputWatercycleLayerDaily_Datasize /*total size*/
};
#define  OutputWatercycleLayerDaily_Rank  ((ldndc_output_rank_t)(sizeof( OutputWatercycleLayerDaily_Sizes) / sizeof( OutputWatercycleLayerDaily_Sizes[0])) - 1)
ldndc_output_size_t const *  OutputWatercycleLayerDaily_EntitySizes = NULL;
atomic_datatype_t const  OutputWatercycleLayerDaily_Types[] =
{
    LDNDC_FLOAT64
};


LMOD_OUTPUT_MODULE_NAME::LMOD_OUTPUT_MODULE_NAME(
        MoBiLE_State *  _state,
        cbm::io_kcomm_t *  _io_kcomm,
        timemode_e  _timemode)
    : LMOD_OUTPUT_MODULE_BASE( _state, _io_kcomm, _timemode),

          io_kcomm( _io_kcomm),

          soillayers_( _io_kcomm->get_input_class< input_class_soillayers_t >()),
          setup_( _io_kcomm->get_input_class< input_class_setup_t >()),
          sc( _state->get_substate< substate_soilchemistry_t >()),
          ph( _state->get_substate< substate_physiology_t >()),
          wc( _state->get_substate< substate_watercycle_t >()),

          m_veg( &_state->vegetation)
{
    this->rc.write_foliage = 1;
    this->rc.foliage_all = 1;

    this->rc.write_surface = 1;

    this->rc.write_soil = 1;
    this->rc.soil_all = 1;

    this->rc.bypass_mobile = 0;

    this->acc.infiltration_cum = 0.0;
    this->acc.waterflux_cum_sl = new double[this->soillayers_->soil_layer_cnt()];
    this->acc.transpiration_cum_sl = new double[this->soillayers_->soil_layer_cnt()];
    
    for (size_t sl = 0; sl < this->soillayers_->soil_layer_cnt(); ++sl)
    {
        this->acc.waterflux_cum_sl[sl] = 0.0;
        this->acc.transpiration_cum_sl[sl] = 0.0;
    }
}


LMOD_OUTPUT_MODULE_NAME::~LMOD_OUTPUT_MODULE_NAME()
{
    delete[] this->acc.waterflux_cum_sl;
    delete[] this->acc.transpiration_cum_sl;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::configure(
                ldndc::config_file_t const *  _cf)
{
    lerr_t  rc_setflags =
        this->set_metaflags( _cf, RM_DEFAULT_LAYERDAILY);
    if ( rc_setflags)
        { return  LDNDC_ERR_FAIL; }

    bool  rc_option;

    CF_LMOD_QUERY(_cf, "write_foliage_layers", rc_option, true);
    this->rc.write_foliage = rc_option;
    CF_LMOD_QUERY(_cf, "write_foliage_layers_all", rc_option, true);
    /* 0 = output is first, mid, and last of all foliage layers; 1 = all layers */
    this->rc.foliage_all = rc_option;

    CF_LMOD_QUERY(_cf, "write_surface_layer", rc_option, true);
    this->rc.write_surface = rc_option;

    CF_LMOD_QUERY(_cf, "write_soil_layers", rc_option, true);
    this->rc.write_soil = rc_option;
    CF_LMOD_QUERY(_cf, "write_soil_layers_all", rc_option, true);
    /* 0 = output is first, mid, and last of all soil and litter layers; 1 = all layers */
    this->rc.soil_all = rc_option;

#ifdef  OutputWatercycleLayerDaily_HAVE_request_api
    CF_LMOD_QUERY(_cf, "bypass_mobile", rc_option, false);
    this->rc.bypass_mobile = rc_option;

    if ( this->rc.bypass_mobile)
    {
        this->kcomm =
            this->io_kcomm->peer_communicator( this->id(), this->object_id());
        kcomm.register_message_handler(
                ldndc_mobile_message_handler_output_watercycle_daily, this);
    }
#endif

    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::initialize()
{
    this->m_sink = this->io_kcomm->sink_handle_acquire( "watercyclelayerdaily");
    if ( this->m_sink.status() == LDNDC_ERR_OK)
    {
        lerr_t  rc_layout = LDNDC_OUTPUT_DEFINE_SINK_LAYOUT_DEFAULTS(
            this->m_sink,OutputWatercycleLayerDaily);
        RETURN_IF_NOT_OK(rc_layout);
    }
    else
    {
        KLOGERROR( "sink status bad  [sink=","watercyclelayerdaily","]");
        return  this->m_sink.status();
    }

    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::solve()
{
    if ( this->rc.bypass_mobile)
    {
        return  LDNDC_ERR_OK;
    }
    return  this->execute();
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::execute()
{
    this->pop_accumulated_outputs();

    ldndc_flt64_t  data_flt64_0[OutputWatercycleLayerDaily_Datasize];
    lerr_t  rc_dump = this->dump_0( data_flt64_0);
    if ( rc_dump)
    { return  LDNDC_ERR_FAIL; }

    this->push_accumulated_outputs();

    return  LDNDC_ERR_OK;
}


void
LMOD_OUTPUT_MODULE_NAME::pop_accumulated_outputs()
{
    for (size_t sl = 0; sl < this->soillayers_->soil_layer_cnt(); ++sl)
    {
        this->acc.waterflux_cum_sl[sl] = wc->accumulated_waterflux_sl[sl] - this->acc.waterflux_cum_sl[sl];
        this->acc.transpiration_cum_sl[sl] = wc->accumulated_transpiration_sl[sl] - this->acc.transpiration_cum_sl[sl];
    }

    acc.infiltration_cum = wc->accumulated_infiltration - acc.infiltration_cum;
}


void
LMOD_OUTPUT_MODULE_NAME::push_accumulated_outputs()
{
    for (size_t sl = 0; sl < this->soillayers_->soil_layer_cnt(); ++sl)
    {
        this->acc.waterflux_cum_sl[sl] = wc->accumulated_waterflux_sl[sl];
        this->acc.transpiration_cum_sl[sl] = wc->accumulated_transpiration_sl[sl];
    }

    acc.infiltration_cum = wc->accumulated_infiltration;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::dump_0( ldndc_flt64_t *  _buf)
{
    /* above ground */
    if ( this->rc.write_foliage)
    {
        size_t const  CF = std::min( m_veg->canopy_layers_used(), this->ph->h_fl.size());
        size_t const  F = ( CF == 0) ? 0 : ( CF - 1);
        size_t const  F_MID = static_cast< size_t >( 0.5 * CF);
        double  h = ( CF == 0) ? 0.0 : this->ph->h_fl.sum(CF);

        for ( size_t  l = 0;  l < CF;  ++l)
        {
            size_t const  fl = F - l;

            if ( this->rc.foliage_all || (( fl == 0) || ( fl == F_MID) || ( fl == F)))
            {
                LDNDC_OUTPUT_SET_COLUMN_INDEX(0);

#ifdef  OutputWatercycleLayerDaily_Item__level
                LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(h);
#endif
#ifdef  OutputWatercycleLayerDaily_Item__extension
                LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->ph->h_fl[fl]);
#endif

#ifdef  OutputWatercycleLayerDaily_Item__water
                LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->wc->wc_fl[fl] * cbm::MM_IN_M);
#endif
#ifdef  OutputWatercycleLayerDaily_Item__wfps
                LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(ldndc::invalid_t< ldndc_flt64_t >::value);
#endif
#ifdef  OutputWatercycleLayerDaily_Item__ice
                LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(ldndc::invalid_t< ldndc_flt64_t >::value);
#endif
#ifdef  OutputWatercycleLayerDaily_Item__flux
                LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(ldndc::invalid_t< ldndc_flt64_t >::value);
#endif
#ifdef  OutputWatercycleLayerDaily_Item__droughtstress
                LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(ldndc::invalid_t< ldndc_flt64_t >::value);
#endif
#ifdef  OutputWatercycleLayerDaily_Item__transpiration
                LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(ldndc::invalid_t< ldndc_flt64_t >::value);
#endif
                
                void *  data[] = { _buf};
                this->set_layernumber( fl+1);
                lerr_t  rc_write = this->write_fixed_record( &this->m_sink, data);
                if ( rc_write)
                { return  LDNDC_ERR_FAIL; }
            }
            
            h -= this->ph->h_fl[fl];
        }
    }

    /* surface */
    if ( this->rc.write_surface)
    {
        {
            LDNDC_OUTPUT_SET_COLUMN_INDEX(0);

#ifdef  OutputWatercycleLayerDaily_Item__level
            LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(0.0);
#endif
#ifdef  OutputWatercycleLayerDaily_Item__extension
            LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(0.0);
#endif

#ifdef  OutputWatercycleLayerDaily_Item__water
            LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->wc->surface_water * cbm::MM_IN_M);
#endif
#ifdef  OutputWatercycleLayerDaily_Item__wfps
            LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(ldndc::invalid_t< ldndc_flt64_t >::value);
#endif
#ifdef  OutputWatercycleLayerDaily_Item__ice
            LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->wc->surface_ice * cbm::MM_IN_M);
#endif
#ifdef  OutputWatercycleLayerDaily_Item__flux
            LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->acc.infiltration_cum * cbm::MM_IN_M);
#endif
#ifdef  OutputWatercycleLayerDaily_Item__droughtstress
            LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(ldndc::invalid_t< ldndc_flt64_t >::value);
#endif
#ifdef  OutputWatercycleLayerDaily_Item__transpiration
            LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(ldndc::invalid_t< ldndc_flt64_t >::value);
#endif
            
            void *  data[] = { _buf};
            this->set_layernumber( 0);
            lerr_t  rc_write = this->write_fixed_record( &this->m_sink, data);
            if ( rc_write)
            { return  LDNDC_ERR_FAIL; }
        }
    }

    /* below ground */
    if ( this->rc.write_soil)
    {
        size_t const  CS = this->soillayers_->soil_layer_cnt();
        size_t const  S = ( CS == 0) ? 0 : ( CS - 1);
        size_t const  CL = this->soillayers_->soil_layers_in_litter_cnt();
        size_t const  S_MID = static_cast< size_t >( 0.5 * ( CS + CL));
        for ( size_t  sl = 0; sl < CS;  ++sl)
        {
            if ( this->rc.soil_all || (( sl == 0) || ( sl == CL) || ( sl == S_MID) || ( sl == S)))
            {
                LDNDC_OUTPUT_SET_COLUMN_INDEX(0);

#ifdef  OutputWatercycleLayerDaily_Item__level
                LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(-this->sc->depth_sl[sl]);
#endif
#ifdef  OutputWatercycleLayerDaily_Item__extension
                LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->sc->h_sl[sl]);
#endif

#ifdef  OutputWatercycleLayerDaily_Item__water
                LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->wc->wc_sl[sl] * cbm::MM_IN_M * sc->h_sl[sl]);
#endif
#ifdef  OutputWatercycleLayerDaily_Item__wfps
                LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->wc->wc_sl[sl] / sc->poro_sl[sl]);
#endif
#ifdef  OutputWatercycleLayerDaily_Item__ice
                LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->wc->ice_sl[sl] * cbm::MM_IN_M / ( 1.0 - this->sc->stone_frac_sl[sl]));
#endif
#ifdef  OutputWatercycleLayerDaily_Item__flux
                LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->acc.waterflux_cum_sl[sl] * cbm::MM_IN_M);
#endif
#ifdef  OutputWatercycleLayerDaily_Item__droughtstress
                LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT((this->wc->wc_sl[sl] - sc->wcmin_sl[sl]) / (this->sc->wcmax_sl[sl] - sc->wcmin_sl[sl]));
#endif
#ifdef  OutputWatercycleLayerDaily_Item__transpiration
                LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->acc.transpiration_cum_sl[sl] * cbm::MM_IN_M);
#endif
                
                void *  data[] = { _buf};
                this->set_layernumber( -static_cast< int >( sl) - 1);

                lerr_t  rc_write = this->write_fixed_record( &this->m_sink, data);
                if ( rc_write)
                    { return  LDNDC_ERR_FAIL; }
            }
        }
    }
    
    return  LDNDC_ERR_OK;
}

lerr_t
LMOD_OUTPUT_MODULE_NAME::finalize()
{
    this->io_kcomm->sink_handle_release( &this->m_sink);
    return  LDNDC_ERR_OK;
}

} /*namespace ldndc*/


#ifdef  OutputWatercycleLayerDaily_HAVE_request_api
namespace ldndc {
lerr_t
LMOD_OUTPUT_MODULE_NAME::process_message(
        lmessage_t const *  _msg, lreply_t * /*  _reply*/)
{
    if ( !this->active()
            || !this->lclock()->is_position( this->timemode()))
    {
        return  LDNDC_ERR_OK;
    }
    if ( !_msg)
    {
        return  LDNDC_ERR_RUNTIME_ERROR;
    }

    lerr_t  rc_req = LDNDC_ERR_FAIL;
    if ( _msg->command == "E" /*execute*/)
    {
//        KLOGVERBOSE( "executing..");
        rc_req = this->execute();
    }
    else
    {
        /* unknown command */
    }
    return  rc_req;
}
} /*namespace ldndc*/
#endif /* OutputWatercycleLayerDaily_HAVE_request_api */

#undef  LMOD_OUTPUT_MODULE_BASE
#undef  LMOD_OUTPUT_MODULE_NAME

#undef  OutputWatercycleLayerDaily_Rank
#undef  OutputWatercycleLayerDaily_Datasize

