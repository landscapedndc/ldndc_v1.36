/*!
 * @file
 * @author
 *  - Edwin Haas
 *  - Ruediger Grote
 *  - David Kraus
 */

#include  "output/watercycle/output-watercycle-layer-subdaily.h"
#include  "constants/lconstants-conv.h"

#define  LMOD_OUTPUT_MODULE_BASE  MBE_LegacyOutputModel
#define  LMOD_OUTPUT_MODULE_NAME  OutputWatercycleLayerSubdaily
#define  LMOD_OUTPUT_MODULE_TIMEMODE  TMODE_SUBDAILY

LMOD_MODULE_INFO(LMOD_OUTPUT_MODULE_NAME,LMOD_OUTPUT_MODULE_TIMEMODE,LMOD_FLAG_USER|LMOD_FLAG_OUTPUT);
namespace ldndc {


ldndc_string_t const  OutputWatercycleLayerSubdaily_Ids[] =
{
    "level",
    "extension",
    "water", 
    "wfps", 
    "ice", 
    "flux",
    "droughtstress"
};
ldndc_string_t const  OutputWatercycleLayerSubdaily_Header[] =
{
    "level[m]",
    "extension[m]",
    "water[mm/%]", 
    "wfps[%]", 
    "ice", 
    "flux",
    "droughtstress"
};
#define  OutputWatercycleLayerSubdaily_Datasize  (sizeof( OutputWatercycleLayerSubdaily_Header) / sizeof( OutputWatercycleLayerSubdaily_Header[0]))
ldndc_output_size_t const  OutputWatercycleLayerSubdaily_Sizes[] =
{
    OutputWatercycleLayerSubdaily_Datasize,

    OutputWatercycleLayerSubdaily_Datasize /*total size*/
};
#define  OutputWatercycleLayerSubdaily_Rank  ((ldndc_output_rank_t)(sizeof( OutputWatercycleLayerSubdaily_Sizes) / sizeof( OutputWatercycleLayerSubdaily_Sizes[0])) - 1)
ldndc_output_size_t const *  OutputWatercycleLayerSubdaily_EntitySizes = NULL;
atomic_datatype_t const  OutputWatercycleLayerSubdaily_Types[] =
{
    LDNDC_FLOAT64
};


LMOD_OUTPUT_MODULE_NAME::LMOD_OUTPUT_MODULE_NAME(
        MoBiLE_State *  _state,
        cbm::io_kcomm_t *  _io_kcomm,
        timemode_e  _timemode)
    : LMOD_OUTPUT_MODULE_BASE( _state, _io_kcomm, _timemode),

          io_kcomm( _io_kcomm),
          m_veg( &_state->vegetation),

          soillayers_( _io_kcomm->get_input_class< input_class_soillayers_t >()),
          setup_( _io_kcomm->get_input_class< input_class_setup_t >()),
          sc( _state->get_substate< substate_soilchemistry_t >()),
          ph( _state->get_substate< substate_physiology_t >()),
          wc( _state->get_substate< substate_watercycle_t >()),
          
          f_sel_( 0), s_sel_( 0)
{
    this->acc.infiltration_cum = 0.0;
    this->acc.waterflux_cum_sl = new double[this->soillayers_->soil_layer_cnt()];
    for (size_t sl = 0; sl < this->soillayers_->soil_layer_cnt(); ++sl)
    {
        this->acc.waterflux_cum_sl[sl] = 0.0;
    }
}


LMOD_OUTPUT_MODULE_NAME::~LMOD_OUTPUT_MODULE_NAME()
{
    delete[] this->acc.waterflux_cum_sl;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::configure(
                ldndc::config_file_t const *  _cf)
{
    lerr_t  rc_setflags =
        this->set_metaflags( _cf, RM_DEFAULT_LAYERSUBDAILY);
    if ( rc_setflags)
        { return  LDNDC_ERR_FAIL; }

    this->f_sel_ = 1; /* 0 = output is first, mid, and last of all foliage layers; 1 = all layers */
    this->s_sel_ = 1; /* 0 = output is first, mid, and last of all soil and litter layers; 1 = all layers */

    bool  bool_option = false;
    CF_LMOD_QUERY(_cf, "single_files", bool_option, false);

    cbm::state_scratch_t *  mcom = this->io_kcomm->get_scratch();
    mcom->set( "watercyclelayersubdaily.singlefiles", bool_option);

    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::initialize()
{
    cbm::state_scratch_t *  mcom = this->io_kcomm->get_scratch();
    bool  have_single_files = false;
    mcom->get( "watercyclelayersubdaily.singlefiles", &have_single_files, false);

    int  sink_index = 0;
    if ( have_single_files)
    {
        sink_index = static_cast< int >( this->object_id());
    }

    this->m_sink = this->io_kcomm->sink_handle_acquire( "watercyclelayersubdaily", sink_index);
    if ( this->m_sink.status() == LDNDC_ERR_OK)
    {
        lerr_t  rc_layout = LDNDC_OUTPUT_DEFINE_SINK_LAYOUT_DEFAULTS(
            this->m_sink,OutputWatercycleLayerSubdaily);
        RETURN_IF_NOT_OK(rc_layout);
    }
    else
    {
        KLOGERROR( "sink status bad  [sink=","watercyclelayersubdaily","]");
        return  this->m_sink.status();
    }

    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::solve()
{
    this->pop_accumulated_outputs();

    ldndc_flt64_t  data_flt64_0[OutputWatercycleLayerSubdaily_Datasize];
    lerr_t  rc_dump = this->dump_0( data_flt64_0);
    if ( rc_dump)
    { return  LDNDC_ERR_FAIL; }

    this->push_accumulated_outputs();

    return  LDNDC_ERR_OK;
}


void
LMOD_OUTPUT_MODULE_NAME::pop_accumulated_outputs()
{
    for (size_t sl = 0; sl < this->soillayers_->soil_layer_cnt(); ++sl)
    {
        this->acc.waterflux_cum_sl[sl] = wc->accumulated_waterflux_sl[sl] - this->acc.waterflux_cum_sl[sl];
    }

    acc.infiltration_cum = wc->accumulated_infiltration - acc.infiltration_cum;
}


void
LMOD_OUTPUT_MODULE_NAME::push_accumulated_outputs()
{
    for (size_t sl = 0; sl < this->soillayers_->soil_layer_cnt(); ++sl)
    {
        this->acc.waterflux_cum_sl[sl] = wc->accumulated_waterflux_sl[sl];
    }

    acc.infiltration_cum = wc->accumulated_infiltration;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::dump_0( ldndc_flt64_t *  _buf)
{
    size_t const  CF = std::min( m_veg->canopy_layers_used(), this->ph->h_fl.size());
    size_t const  F = ( CF == 0) ? 0 : ( CF - 1);
    size_t const  F_MID = static_cast< size_t >( 0.5 * CF);
    size_t const  CS = this->soillayers_->soil_layer_cnt();
    size_t const  S = ( CS == 0) ? 0 : ( CS - 1);
    size_t const  CL = this->soillayers_->soil_layers_in_litter_cnt();
    size_t const  S_MID = static_cast< size_t >( 0.5 * ( CS + CL));
    
    
    /* above ground */
    double  h = ( CF == 0) ? 0.0 : this->ph->h_fl.sum(CF);
    for ( size_t  l = 0;  l < CF;  ++l)
    {
        size_t const  fl = F - l;
        
        if (( this->f_sel_ == 1) || (( fl == 0) || ( fl == F_MID) || ( fl == F)))
        {
            LDNDC_OUTPUT_SET_COLUMN_INDEX(0);
            
            LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(h);
            LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->ph->h_fl[fl]);
            
            LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->wc->wc_fl[fl] * cbm::MM_IN_M);
            LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(ldndc::invalid_t< ldndc_flt64_t >::value);
            LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(ldndc::invalid_t< ldndc_flt64_t >::value);
            LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(ldndc::invalid_t< ldndc_flt64_t >::value);
            LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(ldndc::invalid_t< ldndc_flt64_t >::value);
            
            void *  data[] = { _buf};
            this->set_layernumber( fl+1);
            lerr_t  rc_write =
                this->write_fixed_record( &this->m_sink, data);
            if ( rc_write)
                { return  LDNDC_ERR_FAIL; }
        }
        
        h -= this->ph->h_fl[fl];
    }

    /* surface */
    {
        LDNDC_OUTPUT_SET_COLUMN_INDEX(0);
            
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(0.0);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(0.0);
            
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->wc->surface_water * cbm::MM_IN_M);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(ldndc::invalid_t< ldndc_flt64_t >::value);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->wc->surface_ice * cbm::MM_IN_M);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->acc.infiltration_cum * cbm::MM_IN_M);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(ldndc::invalid_t< ldndc_flt64_t >::value);
            
        void *  data[] = { _buf};
        this->set_layernumber( 0);
        lerr_t  rc_write =
            this->write_fixed_record( &this->m_sink, data);
        if ( rc_write)
            { return  LDNDC_ERR_FAIL; }
    }

    /* below ground */
    for ( size_t  sl = 0; sl < CS;  ++sl)
    {        
        if (( this->s_sel_ == 1) || (( sl == 0) || ( sl == CL) || ( sl == S_MID) || ( sl == S)))
        {
            LDNDC_OUTPUT_SET_COLUMN_INDEX(0);
            
            LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(-this->sc->depth_sl[sl]);
            LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->sc->h_sl[sl]);
            
            LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->wc->wc_sl[sl] * cbm::MM_IN_M / ( 1.0 - this->sc->stone_frac_sl[sl]));
            LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->wc->wc_sl[sl] / sc->poro_sl[sl]);
            LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->wc->ice_sl[sl] * cbm::MM_IN_M / ( 1.0 - this->sc->stone_frac_sl[sl]));
            LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->acc.waterflux_cum_sl[sl] * cbm::MM_IN_M);
            LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT((this->wc->wc_sl[sl] - sc->wcmin_sl[sl]) / (this->sc->wcmax_sl[sl] - sc->wcmin_sl[sl]));
            
            void *  data[] = { _buf};
            this->set_layernumber( -static_cast< int >( sl) - 1);
            lerr_t  rc_write =
                this->write_fixed_record( &this->m_sink, data);
            if ( rc_write)
                { return  LDNDC_ERR_FAIL; }
        }
    }
    
    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::finalize()
{
    this->io_kcomm->sink_handle_release( &this->m_sink);
    return  LDNDC_ERR_OK;
}


#undef  LMOD_OUTPUT_MODULE_BASE
#undef  LMOD_OUTPUT_MODULE_NAME

#undef  OutputWatercycleLayerSubdaily_Rank
#undef  OutputWatercycleLayerSubdaily_Datasize

} /*namespace ldndc*/

