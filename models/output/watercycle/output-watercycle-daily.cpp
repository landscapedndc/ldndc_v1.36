/*!
 * @file
 * @author
 *  - Edwin Haas
 *  - Ruediger Grote
 *  - David Kraus
 */

#include  "output/watercycle/output-watercycle-daily.h"
#include  <scientific/hydrology/ld_vangenuchten.h>

#include  "constants/lconstants-conv.h"

#define  LMOD_OUTPUT_MODULE_BASE  MBE_LegacyOutputModel
#define  LMOD_OUTPUT_MODULE_NAME  OutputWatercycleDaily

#ifdef  OutputWatercycleDaily_HAVE_request_api
#  include  "comm/lmessage.h"
static int
ldndc_mobile_message_handler_output_watercycle_daily(
        void *  _module, lmessage_t const *  _msg, lreply_t *  _reply)
{
    int  rc = -1;
    ldndc::OutputWatercycleDaily *  obj =
        static_cast< ldndc::OutputWatercycleDaily * >( _module);
    if ( obj)
    {
        rc = obj->process_message( _msg, _reply);
    }
    return  rc;
}
#endif

double const ldndc::OutputWatercycleDaily::WFPS_THRESHOLD = 0.9;

LMOD_MODULE_INFO(LMOD_OUTPUT_MODULE_NAME,TMODE_POST_DAILY,LMOD_FLAG_USER|LMOD_FLAG_OUTPUT);

REGISTER_OPTION(OutputWatercycleDaily, loggerstream, "Stream for sending OutputWatercycleDaily logging data");
REGISTER_OPTION(OutputWatercycleDaily, wfps,"  [bool]");

namespace ldndc {

cbm::logger_t *  OutputWatercycleDailyLogger = NULL;

#define  OutputWatercycleDaily_Item_irri /*irrigation*/
#define  OutputWatercycleDaily_Item_prec /*precipitation*/
#define  OutputWatercycleDaily_Item_throughf /*throughfall*/
#define  OutputWatercycleDaily_Item_evapot /* potential evapotranspiration*/
#define  OutputWatercycleDaily_Item_transppot /* potential transpiration*/
#define  OutputWatercycleDaily_Item_transp /*transpiration*/
#define  OutputWatercycleDaily_Item_evacep /*canopy evaporation*/
#define  OutputWatercycleDaily_Item_evasoil /*soil evaporation*/
#define  OutputWatercycleDaily_Item_evasurfacewater /*surface water evaporation*/
#define  OutputWatercycleDaily_Item_runoff /*surface runoff*/
#define  OutputWatercycleDaily_Item_percol /*percolation*/
#define  OutputWatercycleDaily_Item_infiltration /*infiltration*/
#define  OutputWatercycleDaily_Item_groundwateraccess /*groundwater access*/
#define  OutputWatercycleDaily_Item_capillaryrise /*capillary rise*/
#define  OutputWatercycleDaily_Item_surfacesnow /*snow*/
#define  OutputWatercycleDaily_Item_surfacewater /*surface water*/
#define  OutputWatercycleDaily_Item_soilwater_total /*total soil water*/
#define  OutputWatercycleDaily_Item_rootedsoilwater /*total soil water within rooted soil layers*/
#define  OutputWatercycleDaily_Item_leafwater /*total leaf water*/
#define  OutputWatercycleDaily_Item_soilwater_litter /*soil water in litter layer*/
#define  OutputWatercycleDaily_Item_soilwater_5cm /*soil water in 5 cm soil depth*/
#define  OutputWatercycleDaily_Item_soilwater_10cm /*soil water in 10 cm soil depth*/
#define  OutputWatercycleDaily_Item_soilwater_15cm /*soil water in 15 cm soil depth*/
#define  OutputWatercycleDaily_Item_soilwater_20cm /*soil water in 20 cm soil depth*/
#define  OutputWatercycleDaily_Item_soilwater_30cm /*soil water in 30 cm soil depth*/
#define  OutputWatercycleDaily_Item_soilwater_40cm /*soil water in 40 cm soil depth*/
#define  OutputWatercycleDaily_Item_soilwater_50cm /*soil water in 50 cm soil depth*/
#define  OutputWatercycleDaily_Item_soilwater_60cm /*soil water in 60 cm soil depth*/
#define  OutputWatercycleDaily_Item_soilwater_80cm /*soil water in 80 cm soil depth*/
#define  OutputWatercycleDaily_Item_soilwater_100cm /*soil water in 100 cm soil depth*/
#define  OutputWatercycleDaily_Item_soilwater_120cm /*soil water in 120 cm soil depth*/
#define  OutputWatercycleDaily_Item_van_genuchten /*soil water potential according to van Genuchten*/
#define  OutputWatercycleDaily_Item_groundwaterdepth /*depth of groundwater table*/
#define  OutputWatercycleDaily_Item_reservoir /*irrigation reservoir*/
#define  OutputWatercycleDaily_Item_waterdeficit /*(plant) water deficit*/

/*!
 * @page watercycleoutput
 * @section watercycleoutputdaily Watercycle output (daily)
 *
 * <b>xml-based module selection in project's setup:</b>
 * \n\code{.xml}< module id="output:watercycle-layer:daily" />\endcode\n
 *
 * entity name | decription | unit
 * ----------- | ---------- | ----
 * irri | Irrigation | [mm]
 * prec | Precipitation | [mm]
 * throughfall | Throughfall | [mm]
 * evapot | Potential evapotranspiration | [mm]
 * potential_transpiration | Potential transpiration | [mm]
 * transp | Transpiration | [mm]
 * evacep | Evaporation from the interception in the leaves | [mm]
 * evasoil | Evaporation from the soil surface | [mm]
 * evaporation_surfacewater | Evaporation from surface water | [mm]
 * runoff | Surface water runoff | [mm]
 * percol | Percolation out of last soil layer | [mm]
 * infiltration | Infiltration | [mm]
 * capillary rise | Capillary rise from groundwater | [mm]
 * snow_height | Height of  snow | [m]
 * snow | Amount of  snow in water equivalents | [mm\_H2Oeq]
 * surfacewater | Surface water | [mm\_H2Oeq]
 * soil\_water | Total soil water | [mm-m3]
 * root\_soil\_water | Soil water in rooted soil layers | [mm-m3]
 * leafwater | Leaf water storage interception from rainfall | [mm]
 * soilwater_floor | Soil water in the humus layer | [vol\_perc]
 * soilwater5cm | Soil water at 5 cm depth | [vol\_perc]
 * soilwater10cm | Soil water at 10 cm depth | [vol\_perc]
 * soilwater15cm | Soil water at 15 cm depth | [vol\_perc]
 * soilwater20cm | Soil water at 20 cm depth | [vol\_perc]
 * soilwater30cm | Soil water at 30 cm depth | [vol\_perc]
 * soilwater40cm | Soil water at 40 cm depth | [vol\_perc]
 * soilwater50cm | Soil water at 50 cm depth | [vol\_perc]
 * soilwater60cm | Soil water at 60 cm depth | [vol\_perc]
 * soilwater80cm | Soil water at 80 cm depth | [vol\_perc]
 * soilwater100cm | Soil water at 100 cm depth | [vol\_perc]
 * soilwater120cm | Soil water at 120 cm depth | [vol\_perc]
 * groundwaterdepth | Groundwater depth (Depth of the soil layer in which, including deeper layers, the water filled pore space is more than 0.9) | [vol\_perc]
 */


ldndc_string_t const  OutputWatercycleDaily_Ids[] =
{
#ifdef  OutputWatercycleDaily_Item_irri
    "irri",
#endif
#ifdef  OutputWatercycleDaily_Item_prec
    "prec",
#endif
#ifdef  OutputWatercycleDaily_Item_throughf
    "throughfall",
#endif
#ifdef  OutputWatercycleDaily_Item_evapot
    "evapot",
#endif
#ifdef  OutputWatercycleDaily_Item_transppot
    "potential_transpiration",
#endif
#ifdef  OutputWatercycleDaily_Item_transp
    "transp",
#endif
#ifdef  OutputWatercycleDaily_Item_evacep
    "evacep",
#endif
#ifdef  OutputWatercycleDaily_Item_evasoil
    "evasoil",
#endif
#ifdef  OutputWatercycleDaily_Item_evasurfacewater
    "evasurfacewater",
#endif
#ifdef  OutputWatercycleDaily_Item_runoff
    "runoff",
#endif
#ifdef  OutputWatercycleDaily_Item_percol
    "percol",
#endif
#ifdef  OutputWatercycleDaily_Item_infiltration
    "infiltration",
#endif
#ifdef  OutputWatercycleDaily_Item_groundwateraccess
    "groundwateraccess",
    "groundwaterloss",
#endif
#ifdef  OutputWatercycleDaily_Item_capillaryrise
    "capillaryrise",
#endif
#ifdef  OutputWatercycleDaily_Item_surfacesnow
    "snow_height",
    "snow",
#endif
#ifdef  OutputWatercycleDaily_Item_surfacewater
    "surfacewater",
#endif
#ifdef  OutputWatercycleDaily_Item_soilwater_total
    "soil_water",
#endif
#ifdef  OutputWatercycleDaily_Item_rootedsoilwater
    "root_soil_water",
#endif
#ifdef  OutputWatercycleDaily_Item_leafwater
    "leafwater",
#endif
#ifdef  OutputWatercycleDaily_Item_soilwater_litter
    "soilwater_floor",
#endif
#ifdef  OutputWatercycleDaily_Item_soilwater_5cm
    "soilwater_5cm",
#endif
#ifdef  OutputWatercycleDaily_Item_soilwater_10cm
    "soilwater_10cm",
#endif
#ifdef  OutputWatercycleDaily_Item_soilwater_15cm
    "soilwater_15cm",
#endif
#ifdef  OutputWatercycleDaily_Item_soilwater_20cm
    "soilwater_20cm",
#endif
#ifdef  OutputWatercycleDaily_Item_soilwater_30cm
    "soilwater_30cm",
#endif
#ifdef  OutputWatercycleDaily_Item_soilwater_40cm
    "soilwater_40cm",
#endif
#ifdef  OutputWatercycleDaily_Item_soilwater_50cm
    "soilwater_50cm",
#endif
#ifdef  OutputWatercycleDaily_Item_soilwater_60cm
    "soilwater_60cm",
#endif
#ifdef  OutputWatercycleDaily_Item_soilwater_80cm
    "soilwater_80cm",
#endif
#ifdef  OutputWatercycleDaily_Item_soilwater_100cm
    "soilwater_100cm",
#endif
#ifdef  OutputWatercycleDaily_Item_soilwater_120cm
    "soilwater_120cm",
#endif
#ifdef  OutputWatercycleDaily_Item_van_genuchten
    "capillary_pressure_5cm",
    "capillary_pressure_10cm",
    "capillary_pressure_15cm",
    "capillary_pressure_20cm",
    "capillary_pressure_30cm",
    "capillary_pressure_50cm",
    "capillary_pressure_100cm",
#endif
#ifdef  OutputWatercycleDaily_Item_groundwaterdepth
    "groundwaterdepth",
#endif
#ifdef  OutputWatercycleDaily_Item_reservoir
    "irrigationreservoir",
#endif
#ifdef  OutputWatercycleDaily_Item_waterdeficit
    "plant_waterdeficit",
#endif
};

ldndc_string_t const  OutputWatercycleDaily_Header[] =
{
#ifdef  OutputWatercycleDaily_Item_irri
    "irri[mm]",
#endif
#ifdef  OutputWatercycleDaily_Item_prec
    "prec[mm]",
#endif
#ifdef  OutputWatercycleDaily_Item_throughf
    "throughfall[mm]",
#endif
#ifdef  OutputWatercycleDaily_Item_evapot
    "evapot[mm]",
#endif
#ifdef  OutputWatercycleDaily_Item_transppot
    "potential_transpiration[mm]",
#endif
#ifdef  OutputWatercycleDaily_Item_transp
    "transp[mm]",
#endif
#ifdef  OutputWatercycleDaily_Item_evacep
    "evacep[mm]",
#endif
#ifdef  OutputWatercycleDaily_Item_evasoil
    "evasoil[mm]",
#endif
#ifdef  OutputWatercycleDaily_Item_evasurfacewater
    "evasurfacewater[mm]",
#endif
#ifdef  OutputWatercycleDaily_Item_runoff
    "runoff[mm]",
#endif
#ifdef  OutputWatercycleDaily_Item_percol
    "percol[mm]",
#endif
#ifdef  OutputWatercycleDaily_Item_infiltration
    "infiltration[mm]",
#endif
#ifdef  OutputWatercycleDaily_Item_groundwateraccess
    "groundwateraccess[mm]",
    "groundwaterloss[mm]",
#endif
#ifdef  OutputWatercycleDaily_Item_capillaryrise
    "capillaryrise[mm]",
#endif
#ifdef  OutputWatercycleDaily_Item_surfacesnow
    "snow_height[mm]",
    "snow[mmh2oeq]",
#endif
#ifdef  OutputWatercycleDaily_Item_surfacewater
    "surfacewater[mm]",
#endif
#ifdef  OutputWatercycleDaily_Item_soilwater_total
    "soil_water[mm]",
#endif
#ifdef  OutputWatercycleDaily_Item_rootedsoilwater
    "root_soil_water[mm]",
#endif
#ifdef  OutputWatercycleDaily_Item_leafwater
    "leafwater[mm]",
#endif
#ifdef  OutputWatercycleDaily_Item_soilwater_litter
    "soilwater_floor[%]",
#endif
#ifdef  OutputWatercycleDaily_Item_soilwater_5cm
    "soilwater_5cm[%]",
#endif
#ifdef  OutputWatercycleDaily_Item_soilwater_10cm
    "soilwater_10cm[%]",
#endif
#ifdef  OutputWatercycleDaily_Item_soilwater_15cm
    "soilwater_15cm[%]",
#endif
#ifdef  OutputWatercycleDaily_Item_soilwater_20cm
    "soilwater_20cm[%]",
#endif
#ifdef  OutputWatercycleDaily_Item_soilwater_30cm
    "soilwater_30cm[%]",
#endif
#ifdef  OutputWatercycleDaily_Item_soilwater_40cm
    "soilwater_40cm[%]",
#endif
#ifdef  OutputWatercycleDaily_Item_soilwater_50cm
    "soilwater_50cm[%]",
#endif
#ifdef  OutputWatercycleDaily_Item_soilwater_60cm
    "soilwater_60cm[%]",
#endif
#ifdef  OutputWatercycleDaily_Item_soilwater_80cm
    "soilwater_80cm[%]",
#endif
#ifdef  OutputWatercycleDaily_Item_soilwater_100cm
    "soilwater_100cm[%]",
#endif
#ifdef  OutputWatercycleDaily_Item_soilwater_120cm
    "soilwater_120cm[%]",
#endif
#ifdef  OutputWatercycleDaily_Item_van_genuchten
    "capillary_pressure_5cm[hPa]",
    "capillary_pressure_10cm[hPa]",
    "capillary_pressure_15cm[hPa]",
    "capillary_pressure_20cm[hPa]",
    "capillary_pressure_30cm[hPa]",
    "capillary_pressure_50cm[hPa]",
    "capillary_pressure_100cm[hPa]",
#endif
#ifdef  OutputWatercycleDaily_Item_groundwaterdepth
    "groundwaterdepth[m]",
#endif
#ifdef  OutputWatercycleDaily_Item_reservoir
    "irrigationreservoir[m]",
#endif
#ifdef  OutputWatercycleDaily_Item_waterdeficit
    "plant_waterdeficit[m]",
#endif
};
#define  OutputWatercycleDaily_Datasize  (sizeof( OutputWatercycleDaily_Header) / sizeof( OutputWatercycleDaily_Header[0]))
ldndc_output_size_t const  OutputWatercycleDaily_Sizes[] =
{
    OutputWatercycleDaily_Datasize,

    OutputWatercycleDaily_Datasize /*total size*/
};
ldndc_output_size_t const *  OutputWatercycleDaily_EntitySizes = NULL;
#define  OutputWatercycleDaily_Rank  ((ldndc_output_rank_t)(sizeof( OutputWatercycleDaily_Sizes) / sizeof( OutputWatercycleDaily_Sizes[0])) - 1)
atomic_datatype_t const  OutputWatercycleDaily_Types[] =
{
    LDNDC_FLOAT64
};


LMOD_OUTPUT_MODULE_NAME::LMOD_OUTPUT_MODULE_NAME(
        MoBiLE_State *  _state,
        cbm::io_kcomm_t *  _io_kcomm,
        timemode_e  _timemode)
        : LMOD_OUTPUT_MODULE_BASE( _state, _io_kcomm, _timemode),
          io_kcomm( _io_kcomm),
          soillayers_in( _io_kcomm->get_input_class< input_class_soillayers_t >()),
          setup_( _io_kcomm->get_input_class< input_class_setup_t >()),
          m_groundwater( NULL),
          microclim( _state->get_substate< substate_microclimate_t >()),
          phys( _state->get_substate< substate_physiology_t >()),
          soilchem( _state->get_substate< substate_soilchemistry_t >()),
          water( _state->get_substate< substate_watercycle_t >()),
          m_veg( &_state->vegetation)
{
    rc.bypass_mobile = 0;
    format_wfps = false;

    if ( !OutputWatercycleDailyLogger)
    {
        OutputWatercycleDailyLogger = CBM_RegisteredLoggers.new_logger( "OutputWatercycleDaily");
    }
}


LMOD_OUTPUT_MODULE_NAME::~LMOD_OUTPUT_MODULE_NAME()
{
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::configure(
                ldndc::config_file_t const *  _cf)
{
    lerr_t  rc_setflags = set_metaflags( _cf, RM_DEFAULT_DAILY);
    if ( rc_setflags)
    { return  LDNDC_ERR_FAIL; }


    std::string  loggerstream = get_option< char const * >( "loggerstream", "stderr");
    loggerstream = cbm::format_expand( loggerstream);
    OutputWatercycleDailyLogger->initialize( loggerstream.c_str(), _cf->log_level());

    format_wfps = get_option< bool >( "wfps", false);
    if ( format_wfps)
    {
        KLOGINFO_TO( OutputWatercycleDailyLogger, "Default soilwater output format changed from water content to water filled pore space");
    }

#ifdef  OutputWatercycleDaily_HAVE_request_api
    bool  rc_option;
    CF_LMOD_QUERY(_cf, "bypass_mobile", rc_option, false);
    this->rc.bypass_mobile = rc_option;

    if ( rc.bypass_mobile)
    {
        kcomm = io_kcomm->peer_communicator( this->id(), this->object_id());
        kcomm.register_message_handler( ldndc_mobile_message_handler_output_watercycle_daily, this);
    }
#endif

    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::initialize()
{
    this->m_sink = this->io_kcomm->sink_handle_acquire( "watercycledaily");
    if ( this->m_sink.status() == LDNDC_ERR_OK)
    {
        lerr_t  rc_layout = LDNDC_OUTPUT_DEFINE_SINK_LAYOUT_DEFAULTS( m_sink,OutputWatercycleDaily);
        RETURN_IF_NOT_OK(rc_layout);
    }
    else
    {
        KLOGERROR( "sink status bad  [sink=","watercycledaily","]");
        return  this->m_sink.status();
    }

    cbm::source_descriptor_t  gw_source_info;
    lid_t  gw_id = io_kcomm->get_input_class_source_info< input_class_groundwater_t >( &gw_source_info);
    if ( gw_id != invalid_lid)
    {
        m_groundwater = io_kcomm->acquire_input< input_class_groundwater_t >( &gw_source_info);
    }
    else
    {
        m_groundwater = NULL;
    }

    this->push_accumulated_outputs();
    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::solve()
{
    if ( this->rc.bypass_mobile)
    {
        return  LDNDC_ERR_OK;
    }
    return  this->execute();
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::execute()
{
    ldndc_flt64_t  data_flt64_0[OutputWatercycleDaily_Datasize];

    this->pop_accumulated_outputs();
    lerr_t  rc_dump = dump_0( data_flt64_0);
    this->push_accumulated_outputs();

    if ( rc_dump)
        { return  LDNDC_ERR_FAIL; }

    void *  data[] = { data_flt64_0};
    lerr_t  rc_write =
        this->write_fixed_record( &this->m_sink, data);
    if ( rc_write)
        { return  LDNDC_ERR_FAIL; }

    return  LDNDC_ERR_OK;
}

void
LMOD_OUTPUT_MODULE_NAME::pop_accumulated_outputs()
{
    this->acc.irrigation =
                this->water->accumulated_irrigation - this->acc.irrigation;
    this->acc.automatic_irrigation =
                this->water->accumulated_irrigation_automatic - this->acc.automatic_irrigation;
    this->acc.reservoir_irrigation =
                this->water->accumulated_irrigation_reservoir_withdrawal - this->acc.reservoir_irrigation;
    this->acc.ggcmi_irrigation =
                this->water->accumulated_irrigation_ggcmi - this->acc.ggcmi_irrigation;
    this->acc.throughfall =
               this->water->accumulated_throughfall - this->acc.throughfall;
    this->acc.potentialevaporation =
               this->water->accumulated_potentialevapotranspiration - this->acc.potentialevaporation;
    this->acc.potentialtranspiration =
               this->water->accumulated_potentialtranspiration - this->acc.potentialtranspiration;
    this->acc.transpiration =
               this->water->accumulated_transpiration_sl.sum() - this->acc.transpiration;
    this->acc.interceptionevaporation =
               this->water->accumulated_interceptionevaporation - this->acc.interceptionevaporation;
    this->acc.soilevaporation =
               this->water->accumulated_soilevaporation - this->acc.soilevaporation;
    this->acc.surfacewaterevaporation =
               this->water->accumulated_surfacewaterevaporation - this->acc.surfacewaterevaporation;
    this->acc.runoff =
               this->water->accumulated_runoff - this->acc.runoff;
    this->acc.percolation =
               this->water->accumulated_percolation - this->acc.percolation;
    this->acc.surface_waterflux =
                this->water->accumulated_infiltration - this->acc.surface_waterflux;
    this->acc.groundwateraccess =
                this->water->accumulated_groundwater_access - this->acc.groundwateraccess;
    this->acc.groundwaterloss =
                this->water->accumulated_groundwater_loss - this->acc.groundwaterloss;
    this->acc.capillaryrise =
                this->water->accumulated_capillaryrise - this->acc.capillaryrise;
}
void
LMOD_OUTPUT_MODULE_NAME::push_accumulated_outputs()
{
    this->acc.irrigation = this->water->accumulated_irrigation;
    this->acc.automatic_irrigation = this->water->accumulated_irrigation_automatic;
    this->acc.reservoir_irrigation = this->water->accumulated_irrigation_reservoir_withdrawal;
    this->acc.ggcmi_irrigation = this->water->accumulated_irrigation_ggcmi;
    this->acc.throughfall = this->water->accumulated_throughfall;
    this->acc.potentialevaporation = this->water->accumulated_potentialevapotranspiration;
    this->acc.potentialtranspiration = this->water->accumulated_potentialtranspiration;
    this->acc.transpiration = this->water->accumulated_transpiration_sl.sum();
    this->acc.interceptionevaporation = this->water->accumulated_interceptionevaporation;
    this->acc.soilevaporation = this->water->accumulated_soilevaporation;
    this->acc.surfacewaterevaporation = this->water->accumulated_surfacewaterevaporation;
    this->acc.runoff = this->water->accumulated_runoff;
    this->acc.percolation = this->water->accumulated_percolation;
    this->acc.surface_waterflux = this->water->accumulated_infiltration;
    this->acc.groundwateraccess = this->water->accumulated_groundwater_access;
    this->acc.groundwaterloss = this->water->accumulated_groundwater_loss;
    this->acc.capillaryrise = this->water->accumulated_capillaryrise;
}

lerr_t
LMOD_OUTPUT_MODULE_NAME::finalize()
{
    this->io_kcomm->sink_handle_release( &this->m_sink);
    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::dump_0(
                                ldndc_flt64_t *  _buf)
{
    size_t const  S = this->soillayers_in->soil_layer_cnt();
    LDNDC_FIX_UNUSED(S);

#if    defined(OutputWatercycleDaily_Item_soilwater_5cm) \
    || defined(OutputWatercycleDaily_Item_soilwater_10cm) \
    || defined(OutputWatercycleDaily_Item_soilwater_15cm) \
    || defined(OutputWatercycleDaily_Item_soilwater_20cm) \
    || defined(OutputWatercycleDaily_Item_soilwater_30cm) \
    || defined(OutputWatercycleDaily_Item_soilwater_40cm) \
    || defined(OutputWatercycleDaily_Item_soilwater_50cm) \
    || defined(OutputWatercycleDaily_Item_soilwater_60cm) \
    || defined(OutputWatercycleDaily_Item_soilwater_80cm) \
    || defined(OutputWatercycleDaily_Item_soilwater_100cm) \
    || defined(OutputWatercycleDaily_Item_soilwater_120cm)

    double soil_water_floor( 0.0);
    static const double  D[] = { 0.05, 0.1, 0.15, 0.2, 0.3, 0.4, 0.5, 0.6, 0.8, 1.0, 1.2};
    static const size_t  D_SIZE = sizeof(D) / sizeof( double);
    static const double  sw_ini = ldndc::invalid_t< double >::value;
    double  soil_water_at[D_SIZE] = { sw_ini, sw_ini, sw_ini, sw_ini, sw_ini, sw_ini, sw_ini, sw_ini, sw_ini, sw_ini, sw_ini};

    double const litter_height( (soillayers_in->soil_layers_in_litter_cnt() > 0) ? soilchem->depth_sl[soillayers_in->soil_layers_in_litter_cnt()-1] : 0.0);

    size_t  d( 0);
    if ( format_wfps)
    {
        for ( size_t  sl = 0;  ( d < D_SIZE) && ( sl < S);  ++sl)
        {
            if (sl < soillayers_in->soil_layers_in_litter_cnt())
            {
                soil_water_floor += (water->wc_sl[sl] + water->ice_sl[sl])
                                    / soilchem->poro_sl[sl] * cbm::CM_IN_M;
            }
            else if ( soilchem->depth_sl[sl] >= ( D[d] + litter_height - 1.0e-06))
            {
                while ( (soilchem->depth_sl[sl] >= ( D[d] + litter_height - 1.0e-06)) &&
                        (d < D_SIZE))
                {
                    soil_water_at[d] = (water->wc_sl[sl] + water->ice_sl[sl])
                    / soilchem->poro_sl[sl] * cbm::CM_IN_M;
                    ++d;
                }
            }
        }
    }
    else
    {
        for ( size_t  sl = 0;  ( d < D_SIZE) && ( sl < S);  ++sl)
        {
            if (sl < soillayers_in->soil_layers_in_litter_cnt())
            {
                soil_water_floor += (water->wc_sl[sl] + water->ice_sl[sl])
                                    / (1.0 - soilchem->stone_frac_sl[sl]) * cbm::CM_IN_M;
            }
            else if ( soilchem->depth_sl[sl] >= ( D[d] + litter_height - 1.0e-06))
            {
                while ( (soilchem->depth_sl[sl] >= ( D[d] + litter_height - 1.0e-06)) &&
                        (d < D_SIZE))
                {
                    soil_water_at[d] = (water->wc_sl[sl] + water->ice_sl[sl])
                    / (1.0 - soilchem->stone_frac_sl[sl]) * cbm::CM_IN_M;
                    ++d;
                }
            }
        }
    }
    if ( soillayers_in->soil_layers_in_litter_cnt() > 0)
    {
        soil_water_floor /= (double)soillayers_in->soil_layers_in_litter_cnt();
    }
    else
    {
        soil_water_floor = invalid_dbl;
    }

#endif
    
#ifdef  OutputWatercycleDaily_Item_van_genuchten

    static const double  D_CP[] = { 0.05, 0.1, 0.15, 0.2, 0.3, 0.5, 1.0};
    static const size_t  D_SIZE_CP = sizeof(D_CP) / sizeof( double);
    static const double  cp_ini = ldndc::invalid_t< double >::value;
    double  capillary_pressure_at[D_SIZE_CP] = { cp_ini, cp_ini, cp_ini, cp_ini, cp_ini, cp_ini, cp_ini};

    size_t dcp( 0);
    for ( size_t  sl = 0;  ( dcp < D_SIZE_CP) && ( sl < S);  ++sl)
    {
        if ( soilchem->depth_sl[sl] >= ( D_CP[dcp] + litter_height - 1.0e-06))
        {
            capillary_pressure_at[dcp] = ldndc::hydrology::capillary_pressure(
                                                                    water->wc_sl[sl]/soilchem->poro_sl[sl],
                                                                    soilchem->vga_sl[sl],
                                                                    soilchem->vgn_sl[sl],
                                                                    soilchem->vgm_sl[sl],
                                                                    soilchem->wfps_max_sl[sl],
                                                                    soilchem->wfps_min_sl[sl]);
            ++dcp;
        }
    }
    
#endif

    LDNDC_OUTPUT_SET_COLUMN_INDEX(0);

#ifdef  OutputWatercycleDaily_Item_irri
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT((this->acc.irrigation+this->acc.automatic_irrigation+this->acc.reservoir_irrigation+this->acc.ggcmi_irrigation) * cbm::MM_IN_M);
#endif
#ifdef  OutputWatercycleDaily_Item_prec
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->microclim->nd_precipitation * cbm::MM_IN_M);
#endif
#ifdef  OutputWatercycleDaily_Item_throughf
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->acc.throughfall * cbm::MM_IN_M);
#endif
#ifdef  OutputWatercycleDaily_Item_evapot
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->acc.potentialevaporation * cbm::MM_IN_M);
#endif
#ifdef  OutputWatercycleDaily_Item_transppot
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->acc.potentialtranspiration * cbm::MM_IN_M);
#endif
#ifdef  OutputWatercycleDaily_Item_transp /*transpiration*/
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->acc.transpiration * cbm::MM_IN_M);
#endif
#ifdef  OutputWatercycleDaily_Item_evacep /*canopy evoparation*/
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->acc.interceptionevaporation * cbm::MM_IN_M);
#endif
#ifdef  OutputWatercycleDaily_Item_evasoil /*soil evaporation*/
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->acc.soilevaporation * cbm::MM_IN_M);
#endif
#ifdef  OutputWatercycleDaily_Item_evasurfacewater /*surface water evaporation*/
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->acc.surfacewaterevaporation * cbm::MM_IN_M);
#endif
#ifdef  OutputWatercycleDaily_Item_runoff
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->acc.runoff * cbm::MM_IN_M);
#endif
#ifdef  OutputWatercycleDaily_Item_percol
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->acc.percolation * cbm::MM_IN_M);
#endif
#ifdef  OutputWatercycleDaily_Item_infiltration
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->acc.surface_waterflux * cbm::MM_IN_M);
#endif
#ifdef  OutputWatercycleDaily_Item_groundwateraccess
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->acc.groundwateraccess * cbm::MM_IN_M);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->acc.groundwaterloss * cbm::MM_IN_M);
#endif
#ifdef  OutputWatercycleDaily_Item_capillaryrise
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->acc.capillaryrise * cbm::MM_IN_M);
#endif
#ifdef  OutputWatercycleDaily_Item_surfacesnow
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->water->surface_ice * cbm::DWAT / cbm::DSNO * cbm::MM_IN_M);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->water->surface_ice * cbm::MM_IN_M);
#endif
#ifdef  OutputWatercycleDaily_Item_surfacewater
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->water->surface_water * cbm::MM_IN_M);
#endif
#ifdef  OutputWatercycleDaily_Item_soilwater_total
    double  soil_water_total = 0.0;
    /* total soil water incl. ice [mm] */
    for ( size_t  l = 0;  l < S;  ++l)
    {
        soil_water_total += soilchem->h_sl[l] *
            ( this->water->wc_sl[l] + this->water->ice_sl[l]);
    }
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(soil_water_total * cbm::MM_IN_M);
#endif
#ifdef  OutputWatercycleDaily_Item_rootedsoilwater
    double  soil_water_rooted = 0.0;
    /* rooted soil water incl. ice [mm] */
    for ( size_t  l = 0;  l < S;  ++l)
    {
        /* only rooted soil */
        if ( cbm::flt_greater_zero( phys->mfrt_sl(m_veg, l)))
        {
            soil_water_rooted += soilchem->h_sl[l] *
                ( this->water->wc_sl[l] + this->water->ice_sl[l]);
        }
        else
        {
            break;
        }
    }
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(soil_water_rooted * cbm::MM_IN_M);
#endif
#ifdef  OutputWatercycleDaily_Item_leafwater
    size_t  F = 0;
    if ( this->m_veg->size() > 0)
    {
        F = m_veg->canopy_layers_used();
    }
    double  leaf_water = 0.0;
    if (( F == 0) || ( this->water->wc_fl.size() == 0))
        { leaf_water = 0.0; }
    else
        { leaf_water = this->water->wc_fl.sum( F) * cbm::MM_IN_M; }
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(leaf_water);
#endif
#ifdef  OutputWatercycleDaily_Item_soilwater_litter
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(soil_water_floor);
#endif
#ifdef  OutputWatercycleDaily_Item_soilwater_5cm
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(soil_water_at[0]);
#endif
#ifdef  OutputWatercycleDaily_Item_soilwater_10cm
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(soil_water_at[1]);
#endif
#ifdef  OutputWatercycleDaily_Item_soilwater_15cm
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(soil_water_at[2]);
#endif
#ifdef  OutputWatercycleDaily_Item_soilwater_20cm
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(soil_water_at[3]);
#endif
#ifdef  OutputWatercycleDaily_Item_soilwater_30cm
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(soil_water_at[4]);
#endif
#ifdef  OutputWatercycleDaily_Item_soilwater_40cm
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(soil_water_at[5]);
#endif
#ifdef  OutputWatercycleDaily_Item_soilwater_50cm
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(soil_water_at[6]);
#endif
#ifdef  OutputWatercycleDaily_Item_soilwater_60cm
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(soil_water_at[7]);
#endif
#ifdef  OutputWatercycleDaily_Item_soilwater_80cm
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(soil_water_at[8]);
#endif
#ifdef  OutputWatercycleDaily_Item_soilwater_100cm
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(soil_water_at[9]);
#endif
#ifdef  OutputWatercycleDaily_Item_soilwater_120cm
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(soil_water_at[10]);
#endif
#ifdef  OutputWatercycleDaily_Item_van_genuchten
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(capillary_pressure_at[0]);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(capillary_pressure_at[1]);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(capillary_pressure_at[2]);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(capillary_pressure_at[3]);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(capillary_pressure_at[4]);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(capillary_pressure_at[5]);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(capillary_pressure_at[6]);
#endif
#ifdef  OutputWatercycleDaily_Item_groundwaterdepth
    double groundwaterdepth( ldndc::site::site_info_defaults.watertable);
    /* read groundwater table from input file if provided */
    if ( m_groundwater != NULL)
    {
        groundwaterdepth = m_groundwater->watertable_day( lclock_ref());
    }
    else if ( io_kcomm->get_input_class< input_class_site_t >())
    {
        groundwaterdepth = io_kcomm->get_input_class< site::input_class_site_t >()->watertable();
    }
    
    int sl( soillayers_in->soil_layer_cnt()-1);
    for ( ;  sl >= 0;  sl--)
    {
        if ( cbm::flt_greater_equal( water->wc_sl[sl], WFPS_THRESHOLD * soilchem->poro_sl[sl]))
        {
            groundwaterdepth = soilchem->depth_sl[sl] - soilchem->h_sl[sl];
        }
        else
        {
            break;
        }
    }
    if ( cbm::flt_greater_zero( water->surface_water) && sl == -1)
    {
        groundwaterdepth = -water->surface_water;
    }

    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(groundwaterdepth);
#endif
#ifdef  OutputWatercycleDaily_Item_reservoir
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(water->irrigation_reservoir);
#endif
#ifdef  OutputWatercycleDaily_Item_waterdeficit
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(water->plant_waterdeficit * cbm::MM_IN_M);
#endif
    return  LDNDC_ERR_OK;
}
} /*namespace ldndc*/

#ifdef  OutputWatercycleDaily_HAVE_request_api
namespace ldndc {
lerr_t
LMOD_OUTPUT_MODULE_NAME::process_message(
        lmessage_t const *  _msg, lreply_t * /*  _reply*/)
{
    if ( !this->active()
            || !this->lclock()->is_position( this->timemode()))
    {
        return  LDNDC_ERR_OK;
    }
    if ( !_msg)
    {
        return  LDNDC_ERR_RUNTIME_ERROR;
    }

    lerr_t  rc_req = LDNDC_ERR_FAIL;
    if ( _msg->command == "E" /*execute*/)
    {
//        KLOGVERBOSE( "executing..");
        rc_req = this->execute();
    }
    else
    {
        /* unknown command */
    }
    return  rc_req;
}
} /*namespace ldndc*/
#endif /* OutputWatercycleDaily_HAVE_request_api */

#undef  LMOD_OUTPUT_MODULE_BASE
#undef  LMOD_OUTPUT_MODULE_NAME

#undef  OutputWatercycleDaily_Rank
#undef  OutputWatercycleDaily_Datasize
