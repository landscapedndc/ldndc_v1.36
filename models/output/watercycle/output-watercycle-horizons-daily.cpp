/*!
 * @file
 * @author
 *  - David Kraus
 */

#include  "output/watercycle/output-watercycle-horizons-daily.h"
#include  "utils/lutils.h"

#define  LMOD_OUTPUT_MODULE_BASE  MBE_LegacyOutputModel
#define  LMOD_OUTPUT_MODULE_NAME  OutputWatercycleHorizonsDaily
#define  LMOD_OUTPUT_MODULE_TIMEMODE  TMODE_POST_DAILY

LMOD_MODULE_INFO(LMOD_OUTPUT_MODULE_NAME,TMODE_POST_DAILY,LMOD_FLAG_USER|LMOD_FLAG_OUTPUT);

namespace ldndc
{

/*!
 * @page watercycleoutput
 * @section watercycleoutputhorizonsdaily Watercycle output (horizons,daily)
 *
 * <b>xml-based module selection in project's setup:</b>
 * \n\code{.xml}< module id="output:watercycle-horizons:daily" />\endcode\n
 *
 * entity name | decription | unit
 * ----------- | ---------- | ----
 * soilwater10cm | Soil water at 10 cm depth | [vol\_perc]
 * soilwater10cm | Soil water at 20 cm depth | [vol\_perc]
 * soilwater10cm | Soil water at 40 cm depth | [vol\_perc]
 * soilwater10cm | Soil water at 60 cm depth | [vol\_perc]
 * soilwater10cm | Soil water at 80 cm depth | [vol\_perc]
 * soilwater10cm | Soil water at 120 cm depth | [vol\_perc]
 * soilwater10cm | Soil water at 150 cm depth | [vol\_perc]
 */
ldndc_string_t const  OutputWatercycleHorizonsDaily_Ids[] =
{
    "transpiration_0_10cm",
    "transpiration_10_20cm",
    "transpiration_20_40cm",
    "transpiration_40_60cm",
    "transpiration_60_80cm",
    "transpiration_80_120cm",
    "transpiration_120_150cm",
    "soilwater_10cm",
    "soilwater_20cm",
    "soilwater_40cm",
    "soilwater_60cm",
    "soilwater_80cm",
    "soilwater_120cm",
    "soilwater_150cm"
};
ldndc_string_t const  OutputWatercycleHorizonsDaily_Header[] =
{
    "transpiration_0_10cm",
    "transpiration_10_20cm",
    "transpiration_20_40cm",
    "transpiration_40_60cm",
    "transpiration_60_80cm",
    "transpiration_80_120cm",
    "transpiration_120_150cm",
    "soilwater_10cm[mm-3]",
    "soilwater_20cm[mm-3]",
    "soilwater_40cm[mm-3]",
    "soilwater_60cm[mm-3]",
    "soilwater_80cm[mm-3]",
    "soilwater_120cm[mm-3]",
    "soilwater_150cm[mm-3]"
};


#define  OutputWatercycleHorizonsDaily_Datasize  (sizeof( OutputWatercycleHorizonsDaily_Header) / sizeof( OutputWatercycleHorizonsDaily_Header[0]))
ldndc_output_size_t const  OutputWatercycleHorizonsDaily_Sizes[] =
{
    OutputWatercycleHorizonsDaily_Datasize,

    OutputWatercycleHorizonsDaily_Datasize /*total size*/
};


ldndc_output_size_t const *  OutputWatercycleHorizonsDaily_EntitySizes = NULL;
#define  OutputWatercycleHorizonsDaily_Rank  ((ldndc_output_rank_t)(sizeof( OutputWatercycleHorizonsDaily_Sizes) / sizeof( OutputWatercycleHorizonsDaily_Sizes[0])) - 1)
atomic_datatype_t const  OutputWatercycleHorizonsDaily_Types[] =
{
    LDNDC_FLOAT64
};


LMOD_OUTPUT_MODULE_NAME::LMOD_OUTPUT_MODULE_NAME(
        MoBiLE_State *  _state,
        cbm::io_kcomm_t *  _io_kcomm,
        timemode_e  _timemode)
        : LMOD_OUTPUT_MODULE_BASE( _state, _io_kcomm, _timemode),

          io_kcomm( _io_kcomm),

          soillayers_( _io_kcomm->get_input_class< input_class_soillayers_t >()),
          wc_( _state->get_substate_ref< substate_watercycle_t >()),
          mc_( _state->get_substate_ref< substate_microclimate_t >()),
          sc_( _state->get_substate_ref< substate_soilchemistry_t >()),
          accumulated_transpiration_old_sl( lvector_t< double >( 0))
{
}


LMOD_OUTPUT_MODULE_NAME::~LMOD_OUTPUT_MODULE_NAME()
{}


lerr_t
LMOD_OUTPUT_MODULE_NAME::configure(
                ldndc::config_file_t const *  _cf)
{
    lerr_t  rc_setflags = this->set_metaflags( _cf);
    if ( rc_setflags)
    { return  LDNDC_ERR_FAIL; }

    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::initialize()
{
    this->m_sink = this->io_kcomm->sink_handle_acquire( "watercyclehorizonsdaily");
    if ( this->m_sink.status() == LDNDC_ERR_OK)
    {
        lerr_t  rc_layout = LDNDC_OUTPUT_DEFINE_SINK_LAYOUT_DEFAULTS(
            this->m_sink,OutputWatercycleHorizonsDaily);
        RETURN_IF_NOT_OK(rc_layout);
    }
    else
    {
        KLOGERROR( "sink status bad  [sink=","watercyclehorizonsdaily","]");
        return  this->m_sink.status();
    }

    accumulated_transpiration_old_sl.resize_and_preserve( soillayers_->soil_layer_cnt(), 0.0);
    for ( size_t sl = 0; sl < soillayers_->soil_layer_cnt();  ++sl)
    {
        accumulated_transpiration_old_sl[sl] = wc_.accumulated_transpiration_sl[sl];
    }

    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::solve()
{
    ldndc_flt64_t  data_flt64_0[OutputWatercycleHorizonsDaily_Datasize];
        lerr_t  rc_dump = dump_0( data_flt64_0);
    RETURN_IF_NOT_OK(rc_dump);

    void *  data[] = { data_flt64_0};
    lerr_t  rc_write =
        this->write_fixed_record( &this->m_sink, data);
    if ( rc_write)
        { return  LDNDC_ERR_FAIL; }

    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::finalize()
{
    this->io_kcomm->sink_handle_release( &this->m_sink);
    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::dump_0(
                                ldndc_flt64_t *  _buf)
{
    static const double  D[] = { 0.1, 0.2, 0.4, 0.6, 0.8, 1.2, 1.5};
    static const size_t  D_SIZE = sizeof(D) / sizeof( double);
    static const double  val_ini = 0.0;
    double  transpiration_at[D_SIZE] = { val_ini, val_ini, val_ini, val_ini, val_ini, val_ini, val_ini};
    double  soilwater_at[D_SIZE] = { val_ini, val_ini, val_ini, val_ini, val_ini, val_ini, val_ini};

    size_t  d( 0);
    size_t soil_water_counter( 1);

    for ( size_t  sl = 0;  ( d < D_SIZE) && ( sl < soillayers_->soil_layer_cnt());  ++sl)
    {
        soilwater_at[d] += (wc_.wc_sl[sl] + wc_.ice_sl[sl]) / (1.0 - sc_.stone_frac_sl[sl]) * cbm::CM_IN_M;
        if ( cbm::flt_greater_equal( sc_.depth_sl[sl], D[d]))
        {
            transpiration_at[d] += (wc_.accumulated_transpiration_sl[sl] - accumulated_transpiration_old_sl[sl]) * cbm::MM_IN_M;
            soilwater_at[d] /= soil_water_counter;

            soil_water_counter = 1;
            ++d;
        }
        else
        {
            soil_water_counter += 1;
        }
    }

    for ( size_t sl = 0; sl < soillayers_->soil_layer_cnt();  ++sl)
    {
        accumulated_transpiration_old_sl[sl] = wc_.accumulated_transpiration_sl[sl];
    }

    LDNDC_OUTPUT_SET_COLUMN_INDEX(0);

    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(transpiration_at[0]);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(transpiration_at[1]);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(transpiration_at[2]);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(transpiration_at[3]);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(transpiration_at[4]);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(transpiration_at[5]);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(transpiration_at[6]);

    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(soilwater_at[0]);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(soilwater_at[1]);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(soilwater_at[2]);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(soilwater_at[3]);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(soilwater_at[4]);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(soilwater_at[5]);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(soilwater_at[6]);

    return  LDNDC_ERR_OK;
}


#undef  LMOD_OUTPUT_MODULE_BASE
#undef  LMOD_OUTPUT_MODULE_NAME

#undef  OutputWatercycleHorizonsDaily_Rank
#undef  OutputWatercycleHorizonsDaily_Datasize

} /*namespace ldndc*/

