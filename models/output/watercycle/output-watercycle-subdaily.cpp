/*!
 * @file
 * @author
 *  - Steffen Klatt
 *  - Edwin Haas
 *  - Ruediger Grote
 *  - David Kraus
 * @date
 *   Nov. 27, 2014
 */

#include  "output/watercycle/output-watercycle-subdaily.h"
#include  "constants/lconstants-conv.h"

#define  LMOD_OUTPUT_MODULE_BASE  MBE_LegacyOutputModel
#define  LMOD_OUTPUT_MODULE_NAME  OutputWatercycleSubdaily

LMOD_MODULE_INFO(LMOD_OUTPUT_MODULE_NAME,TMODE_SUBDAILY,LMOD_FLAG_USER|LMOD_FLAG_OUTPUT);
namespace ldndc {

#define  OutputWatercycleSubdaily_Item_prec /*precipitation*/
#define  OutputWatercycleSubdaily_Item_throughf /*through fall*/
#define  OutputWatercycleSubdaily_Item_evapot
#define  OutputWatercycleSubdaily_Item_transp /*transpiration*/
#define  OutputWatercycleSubdaily_Item_evacep /*canopy evaporation*/
#define  OutputWatercycleSubdaily_Item_evasoil /*soil evaporation*/
// sk:cmf #define  OutputWatercycleSubdaily_Item_evasurf /*surface water evaporation*/
// sk:cmf #define  OutputWatercycleSubdaily_Item_evasnow /*snow evaporation*/
#define  OutputWatercycleSubdaily_Item_runoff /*surface runoff*/
#define  OutputWatercycleSubdaily_Item_percol
#define  OutputWatercycleSubdaily_Item_infiltration
// sk:cmf #define  OutputWatercycleSubdaily_Item_exfiltration
#define  OutputWatercycleSubdaily_Item_surfacesnow
#define  OutputWatercycleSubdaily_Item_surfacewater
#define  OutputWatercycleSubdaily_Item_soilwater_total
#define  OutputWatercycleSubdaily_Item_rootedsoilwater
#define  OutputWatercycleSubdaily_Item_leafwater
#define  OutputWatercycleSubdaily_Item_soilwater_10cm
#define  OutputWatercycleSubdaily_Item_soilwater_20cm
#define  OutputWatercycleSubdaily_Item_soilwater_30cm
#define  OutputWatercycleSubdaily_Item_soilwater_50cm
#define  OutputWatercycleSubdaily_Item_soilwater_100cm

// sk:cmf #define  OutputWatercycleSubdaily_Item_fluxrainsource
// sk:cmf #define  OutputWatercycleSubdaily_Item_fluxtranspiration
// sk:cmf #define  OutputWatercycleSubdaily_Item_fluxevaporation
// sk:cmf #define  OutputWatercycleSubdaily_Item_fluxsurfacewater
// sk:cmf #define  OutputWatercycleSubdaily_Item_fluxsurfacerunoff
// sk:cmf #define  OutputWatercycleSubdaily_Item_fluxpercolation

ldndc_string_t const  OutputWatercycleSubdaily_Ids[] =
{
#ifdef  OutputWatercycleSubdaily_Item_prec
    "prec",
#endif
#ifdef  OutputWatercycleSubdaily_Item_throughf
    "throughf",
#endif
#ifdef  OutputWatercycleSubdaily_Item_evapot
    "evapot",
#endif
#ifdef  OutputWatercycleSubdaily_Item_transp
    "transp",
#endif
#ifdef  OutputWatercycleSubdaily_Item_evacep
    "evacep",
#endif
#ifdef  OutputWatercycleSubdaily_Item_evasoil
    "evasoil",
#endif
#ifdef  OutputWatercycleSubdaily_Item_evasurf
    "evasurf",
#endif
#ifdef  OutputWatercycleSubdaily_Item_evasnow
    "evasnow",
#endif
#ifdef  OutputWatercycleSubdaily_Item_runoff
    "runoff",
#endif
#ifdef  OutputWatercycleSubdaily_Item_percol
    "percol",
#endif
#ifdef  OutputWatercycleSubdaily_Item_infiltration
    "infiltration",
#endif
#ifdef  OutputWatercycleSubdaily_Item_exfiltration
    "exfiltration",
#endif
#ifdef  OutputWatercycleSubdaily_Item_surfacesnow
    "snow",
#endif
#ifdef  OutputWatercycleSubdaily_Item_surfacewater
    "surfacewater",
#endif
#ifdef  OutputWatercycleSubdaily_Item_soilwater_total
    "soil_water",
#endif
#ifdef  OutputWatercycleSubdaily_Item_rootedsoilwater
    "root_soil_water",
#endif
#ifdef  OutputWatercycleSubdaily_Item_leafwater
    "leafwater",
#endif
#ifdef  OutputWatercycleSubdaily_Item_soilwater_10cm
    "soilwater_10cm",
#endif
#ifdef  OutputWatercycleSubdaily_Item_soilwater_20cm
    "soilwater_20cm",
#endif
#ifdef  OutputWatercycleSubdaily_Item_soilwater_30cm
    "soilwater_30cm",
#endif
#ifdef  OutputWatercycleSubdaily_Item_soilwater_50cm
    "soilwater_50cm",
#endif
#ifdef  OutputWatercycleSubdaily_Item_soilwater_100cm
    "soilwater_100cm",
#endif
#ifdef  OutputWatercycleSubdaily_Item_fluxrainsource
    "flux_rainsource",
#endif
#ifdef  OutputWatercycleSubdaily_Item_fluxtranspiration
    "flux_transpiration",
#endif
#ifdef  OutputWatercycleSubdaily_Item_fluxevaporation
    "flux_evaporation",
#endif
#ifdef  OutputWatercycleSubdaily_Item_fluxsurfacewater
    "flux_surfacewater",
#endif
#ifdef  OutputWatercycleSubdaily_Item_fluxsurfacerunoff
    "flux_surfacerunoff",
#endif
#ifdef  OutputWatercycleSubdaily_Item_fluxpercolation
    "flux_percolation",
#endif
};

ldndc_string_t const  OutputWatercycleSubdaily_Header[] =
{
#ifdef  OutputWatercycleSubdaily_Item_prec
    "prec[mm]",
#endif
#ifdef  OutputWatercycleSubdaily_Item_throughf
    "throughf[mm]",
#endif
#ifdef  OutputWatercycleSubdaily_Item_evapot
    "evapot[mm]",
#endif
#ifdef  OutputWatercycleSubdaily_Item_transp
    "transp[mm]",
#endif
#ifdef  OutputWatercycleSubdaily_Item_evacep
    "evacep[mm]",
#endif
#ifdef  OutputWatercycleSubdaily_Item_evasoil
    "evasoil[mm]",
#endif
#ifdef  OutputWatercycleSubdaily_Item_evasurf
    "evasurf[mm]",
#endif
#ifdef  OutputWatercycleSubdaily_Item_evasnow
    "evasnow[mm]",
#endif
#ifdef  OutputWatercycleSubdaily_Item_runoff
    "runoff[mm]",
#endif
#ifdef  OutputWatercycleSubdaily_Item_percol
    "percol[mm]",
#endif
#ifdef  OutputWatercycleSubdaily_Item_infiltration
    "infiltration[mm]",
#endif
#ifdef  OutputWatercycleSubdaily_Item_exfiltration
    "exfiltration[mm]",
#endif
#ifdef  OutputWatercycleSubdaily_Item_surfacesnow
    "snow[mmh2oeq]",
#endif
#ifdef  OutputWatercycleSubdaily_Item_surfacewater
    "surfacewater[mm]",
#endif
#ifdef  OutputWatercycleSubdaily_Item_soilwater_total
    "soil_water[mm]",
#endif
#ifdef  OutputWatercycleSubdaily_Item_rootedsoilwater
    "root_soil_water[mm]",
#endif
#ifdef  OutputWatercycleSubdaily_Item_leafwater
    "leafwater[mm]",
#endif
#ifdef  OutputWatercycleSubdaily_Item_soilwater_10cm
    "soilwater_10cm[%]",
#endif
#ifdef  OutputWatercycleSubdaily_Item_soilwater_20cm
    "soilwater_20cm[%]",
#endif
#ifdef  OutputWatercycleSubdaily_Item_soilwater_30cm
    "soilwater_30cm[%]",
#endif
#ifdef  OutputWatercycleSubdaily_Item_soilwater_50cm
    "soilwater_50cm[%]",
#endif
#ifdef  OutputWatercycleSubdaily_Item_soilwater_100cm
    "soilwater_100cm[%]",
#endif
#ifdef  OutputWatercycleSubdaily_Item_fluxrainsource
    "flux_rainsource[mm]",
#endif
#ifdef  OutputWatercycleSubdaily_Item_fluxtranspiration
    "flux_transpiration[mm]",
#endif
#ifdef  OutputWatercycleSubdaily_Item_fluxevaporation
    "flux_evaporation[mm]",
#endif
#ifdef  OutputWatercycleSubdaily_Item_fluxsurfacewater
    "flux_surfacewater[mm]",
#endif
#ifdef  OutputWatercycleSubdaily_Item_fluxsurfacerunoff
    "flux_surfacerunoff[mm]",
#endif
#ifdef  OutputWatercycleSubdaily_Item_fluxpercolation
    "flux_percolation[mm]",
#endif

};
#define  OutputWatercycleSubdaily_Datasize  (sizeof( OutputWatercycleSubdaily_Header) / sizeof( OutputWatercycleSubdaily_Header[0]))
ldndc_output_size_t const  OutputWatercycleSubdaily_Sizes[] =
{
    OutputWatercycleSubdaily_Datasize,

    OutputWatercycleSubdaily_Datasize /*total size*/
};
ldndc_output_size_t const *  OutputWatercycleSubdaily_EntitySizes = NULL;
#define  OutputWatercycleSubdaily_Rank  ((ldndc_output_rank_t)(sizeof( OutputWatercycleSubdaily_Sizes) / sizeof( OutputWatercycleSubdaily_Sizes[0])) - 1)
atomic_datatype_t const  OutputWatercycleSubdaily_Types[] =
{
    LDNDC_FLOAT64
};


LMOD_OUTPUT_MODULE_NAME::LMOD_OUTPUT_MODULE_NAME(
        MoBiLE_State *  _state,
        cbm::io_kcomm_t *  _io_kcomm,
        timemode_e  _timemode)
        : LMOD_OUTPUT_MODULE_BASE( _state, _io_kcomm, _timemode),

          io_kcomm( _io_kcomm),

          soillayers_in( _io_kcomm->get_input_class< input_class_soillayers_t >()),
          setup_( _io_kcomm->get_input_class< input_class_setup_t >()),
          microclim( _state->get_substate< substate_microclimate_t >()),
          phys( _state->get_substate< substate_physiology_t >()),
          soilchem( _state->get_substate< substate_soilchemistry_t >()),
          water( _state->get_substate< substate_watercycle_t >()),

          m_veg( &_state->vegetation)
{
}


LMOD_OUTPUT_MODULE_NAME::~LMOD_OUTPUT_MODULE_NAME()
{
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::configure(
                ldndc::config_file_t const *  _cf)
{
    lerr_t  rc_setflags =
        this->set_metaflags( _cf, RM_DEFAULT_SUBDAILY);
    if ( rc_setflags)
        { return  LDNDC_ERR_FAIL; }

    bool  bool_option = false;
    CF_LMOD_QUERY(_cf, "single_files", bool_option, false);

    cbm::state_scratch_t *  mcom = this->io_kcomm->get_scratch();
    mcom->set( "watercyclesubdaily.singlefiles", bool_option);

    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::initialize()
{
    cbm::state_scratch_t *  mcom = this->io_kcomm->get_scratch();
    bool  have_single_files = false;
    mcom->get( "watercyclesubdaily.singlefiles", &have_single_files, false);

    int  sink_index = 0;
    if ( have_single_files)
    {
        sink_index = static_cast< int >( this->object_id());
    }

    this->m_sink = this->io_kcomm->sink_handle_acquire( "watercyclesubdaily", sink_index);
    if ( this->m_sink.status() == LDNDC_ERR_OK)
    {
        lerr_t  rc_layout = LDNDC_OUTPUT_DEFINE_SINK_LAYOUT_DEFAULTS(
            this->m_sink,OutputWatercycleSubdaily);
        RETURN_IF_NOT_OK(rc_layout);
    }
    else
    {
        KLOGERROR( "sink status bad  [sink=","watercyclesubdaily","]");
        return  this->m_sink.status();
    }

    this->push_accumulated_outputs();
    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::solve()
{
    ldndc_flt64_t  data_flt64_0[OutputWatercycleSubdaily_Datasize];

    this->pop_accumulated_outputs();
    lerr_t  rc_dump = dump_0( data_flt64_0);
    this->push_accumulated_outputs();

    if ( rc_dump)
        { return  LDNDC_ERR_FAIL; }

    void *  data[] = { data_flt64_0};
    lerr_t  rc_write =
        this->write_fixed_record( &this->m_sink, data);
    if ( rc_write)
        { return  LDNDC_ERR_FAIL; }

    return  LDNDC_ERR_OK;
}

void
LMOD_OUTPUT_MODULE_NAME::pop_accumulated_outputs()
{
    this->acc.throughfall =
               this->water->accumulated_throughfall - this->acc.throughfall;
    this->acc.potentialevaporation =
               this->water->accumulated_potentialevapotranspiration - this->acc.potentialevaporation;
    this->acc.transpiration =
               this->water->accumulated_transpiration_sl.sum() - this->acc.transpiration;
    this->acc.interceptionevaporation =
               this->water->accumulated_interceptionevaporation - this->acc.interceptionevaporation;
    this->acc.soilevaporation =
               this->water->accumulated_soilevaporation - this->acc.soilevaporation;
    this->acc.runoff =
               this->water->accumulated_runoff - this->acc.runoff;
    this->acc.percolation =
               this->water->accumulated_percolation - this->acc.percolation;
    this->acc.infiltration =
            this->water->accumulated_infiltration - this->acc.infiltration;
}
void
LMOD_OUTPUT_MODULE_NAME::push_accumulated_outputs()
{
    this->acc.throughfall =
        this->water->accumulated_throughfall;
    this->acc.potentialevaporation =
        this->water->accumulated_potentialevapotranspiration;
    this->acc.transpiration =
        this->water->accumulated_transpiration_sl.sum();
    this->acc.interceptionevaporation =
        this->water->accumulated_interceptionevaporation;
    this->acc.soilevaporation =
        this->water->accumulated_soilevaporation;
    this->acc.runoff =
        this->water->accumulated_runoff;
    this->acc.percolation =
        this->water->accumulated_percolation;
    this->acc.infiltration =
        this->water->accumulated_infiltration;
}

lerr_t
LMOD_OUTPUT_MODULE_NAME::finalize()
{
    this->io_kcomm->sink_handle_release( &this->m_sink);
    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::dump_0(
        ldndc_flt64_t *  _buf)
{
    size_t const  S = this->soillayers_in->soil_layer_cnt();
    LDNDC_FIX_UNUSED(S);

#if  defined(OutputWatercycleSubdaily_Item_soilwater_10cm) || defined(OutputWatercycleSubdaily_Item_soilwater_20cm) \
    || defined(OutputWatercycleSubdaily_Item_soilwater_30cm) || defined(OutputWatercycleSubdaily_Item_soilwater_50cm) \
    || defined(OutputWatercycleSubdaily_Item_soilwater_100cm)
    size_t const  L = this->soillayers_in->soil_layers_in_litter_cnt();

    static const double  D[] = { 0.1, 0.2, 0.3, 0.5, 1.0};
    static const size_t  D_SIZE = sizeof(D) / sizeof( double);
    static const double  sw_ini = ldndc::invalid_t< double >::value;
    double  soil_water_at[D_SIZE] = { sw_ini, sw_ini, sw_ini, sw_ini, sw_ini};

    // rg 21.09.10 considering only mineral soil for soil depth
    size_t  d( 0);
    for ( size_t  sl = L;  ( d < D_SIZE) && ( sl < S);  ++sl)
    {
        if ( this->soilchem->depth_sl[sl] >= ( D[d] - 1.0e-06))
        {
            // rg 05.11.10 volume percent should not be related to stones
            soil_water_at[d] = 0.1 * ( this->water->wc_sl[sl] + this->water->ice_sl[sl])
                / ( 1.0 - this->soilchem->stone_frac_sl[sl]);
            ++d;
        }
    }
#endif

    LDNDC_OUTPUT_SET_COLUMN_INDEX(0);

#ifdef  OutputWatercycleSubdaily_Item_prec
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->microclim->ts_precipitation * cbm::MM_IN_M);
#endif
#ifdef  OutputWatercycleSubdaily_Item_throughf
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->acc.throughfall * cbm::MM_IN_M);
#endif
#ifdef  OutputWatercycleSubdaily_Item_evapot
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->acc.potentialevaporation * cbm::MM_IN_M);
#endif
#ifdef  OutputWatercycleSubdaily_Item_transp
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->acc.transpiration * cbm::MM_IN_M);
#endif
#ifdef  OutputWatercycleSubdaily_Item_evacep
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->acc.interceptionevaporation * cbm::MM_IN_M);
#endif
#ifdef  OutputWatercycleSubdaily_Item_evasoil
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->acc.soilevaporation * cbm::MM_IN_M);
#endif
#ifdef  OutputWatercycleSubdaily_Item_evasurf
    {
    double  surfacewaterevaporation = 0.0;
    this->io_kcomm->get_scratch()->get(
            "surfacewaterevaporation", &surfacewaterevaporation, invalid_t< double >::value);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( surfacewaterevaporation * cbm::MM_IN_M);
    }
#endif
#ifdef  OutputWatercycleSubdaily_Item_evasnow
    {
    double  snowevaporation = 0.0;
    this->io_kcomm->get_scratch()->get(
            "snowevaporation", &snowevaporation, invalid_t< double >::value);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( snowevaporation * cbm::MM_IN_M);
    }
#endif
#ifdef  OutputWatercycleSubdaily_Item_runoff
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->acc.runoff * cbm::MM_IN_M);
#endif
#ifdef  OutputWatercycleSubdaily_Item_percol
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->acc.percolation * cbm::MM_IN_M);
#endif
#ifdef  OutputWatercycleSubdaily_Item_infiltration
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->acc.infiltration * cbm::MM_IN_M);
#endif
#ifdef  OutputWatercycleSubdaily_Item_exfiltration
    {
    double  exfiltration = 0.0;
    this->io_kcomm->get_scratch()->get(
            "exfiltration", &exfiltration, invalid_t< double >::value);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( exfiltration * cbm::MM_IN_M);
    }
#endif
#ifdef  OutputWatercycleSubdaily_Item_surfacesnow
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->water->surface_ice * cbm::MM_IN_M);
#endif
#ifdef  OutputWatercycleSubdaily_Item_surfacewater
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->water->surface_water * cbm::MM_IN_M);
#endif
#ifdef  OutputWatercycleSubdaily_Item_soilwater_total
    {
    double  soil_water = 0.0;
    /* total soil water incl. ice [mm] */
    for ( size_t  l = 0;  l < S;  ++l)
    {
        soil_water += soilchem->h_sl[l] *
            ( this->water->wc_sl[l] + this->water->ice_sl[l]);
    }
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(soil_water * cbm::MM_IN_M);
    }
#endif
#ifdef  OutputWatercycleSubdaily_Item_rootedsoilwater
    {
    /* surface water consideration */
    double  rooted_soil_water =  0.0;
    
    /* total soil water incl. ice [mm] */
    for ( size_t  sl = 0;  sl < S;  ++sl)
    {
        /* only rooted soil */
        if ( cbm::flt_greater_zero( phys->mfrt_sl(m_veg, sl)))
        {
            rooted_soil_water += soilchem->h_sl[sl] *
                ( this->water->wc_sl[sl] + this->water->ice_sl[sl]);
        }
        else
        {
            break;
        }
    }
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(rooted_soil_water * cbm::MM_IN_M);
    }
#endif
#ifdef  OutputWatercycleSubdaily_Item_leafwater

    size_t  F = 0;
    if ( this->m_veg->size() > 0)
        { F = m_veg->canopy_layers_used(); }
    double  leaf_water = 0.0;
    if (( F == 0) || ( this->water->wc_fl.size() == 0))
        { leaf_water = 0.0; }
    else
        { leaf_water = this->water->wc_fl.sum( F) * cbm::MM_IN_M; }
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(leaf_water);
#endif
#define  OutputWatercycleSubdaily_Item_soilwater_Xcm(__sw_slot__) \
    {                                                             \
    double  sw = soil_water_at[__sw_slot__];                      \
    if ( d > (size_t)__sw_slot__)                                 \
    {                                                             \
        sw *= cbm::MM_IN_M;                                    \
    }                                                             \
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(sw);                    \
    }
#ifdef  OutputWatercycleSubdaily_Item_soilwater_10cm
    OutputWatercycleSubdaily_Item_soilwater_Xcm(0)
#endif
#ifdef  OutputWatercycleSubdaily_Item_soilwater_20cm
    OutputWatercycleSubdaily_Item_soilwater_Xcm(1)
#endif
#ifdef  OutputWatercycleSubdaily_Item_soilwater_30cm
    OutputWatercycleSubdaily_Item_soilwater_Xcm(2)
#endif
#ifdef  OutputWatercycleSubdaily_Item_soilwater_50cm
    OutputWatercycleSubdaily_Item_soilwater_Xcm(3)
#endif
#ifdef  OutputWatercycleSubdaily_Item_soilwater_100cm
    OutputWatercycleSubdaily_Item_soilwater_Xcm(4)
#endif

#ifdef  OutputWatercycleSubdaily_Item_fluxrainsource
    {
    double  value = 0.0;
    this->io_kcomm->get_scratch()->get(
            "rainsource", &value, invalid_t< double >::value);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( value * cbm::MM_IN_M);
    }
#endif
#ifdef  OutputWatercycleSubdaily_Item_fluxtranspiration
    {
    double  value = 0.0;
    this->io_kcomm->get_scratch()->get(
            "transpiration", &value, invalid_t< double >::value);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( value * cbm::MM_IN_M);
    }
#endif
#ifdef  OutputWatercycleSubdaily_Item_fluxevaporation
    {
    double  value = 0.0;
    this->io_kcomm->get_scratch()->get(
            "evaporation", &value, invalid_t< double >::value);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( value * cbm::MM_IN_M);
    }
#endif
#ifdef  OutputWatercycleSubdaily_Item_fluxsurfacewater
    {
    double  value = 0.0;
    this->io_kcomm->get_scratch()->get(
            "surfacewater", &value, invalid_t< double >::value);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( value * cbm::MM_IN_M);
    }
#endif
#ifdef  OutputWatercycleSubdaily_Item_fluxsurfacerunoff
    {
    double  value = 0.0;
    this->io_kcomm->get_scratch()->get(
            "surfacerunoff", &value, invalid_t< double >::value);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( value * cbm::MM_IN_M);
    }
#endif
#ifdef  OutputWatercycleSubdaily_Item_fluxpercolation
    {
    double  value = 0.0;
    this->io_kcomm->get_scratch()->get(
            "percolation", &value, invalid_t< double >::value);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( value * cbm::MM_IN_M);
    }
#endif

    return  LDNDC_ERR_OK;
}


#undef  LMOD_OUTPUT_MODULE_BASE
#undef  LMOD_OUTPUT_MODULE_NAME

#undef  OutputWatercycleSubdaily_Rank
#undef  OutputWatercycleSubdaily_Datasize

} /*namespace ldndc*/

