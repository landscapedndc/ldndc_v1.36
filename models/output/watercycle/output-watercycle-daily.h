/*!
 * @brief
 *    dumping modified water cycle related items
 *
 * @author
 *    steffen klatt,
 *    edwin haas
 */

#ifndef  LM_OUTPUT_WATERCYCLE_DAILY_H_NEW_
#define  LM_OUTPUT_WATERCYCLE_DAILY_H_NEW_

#include  "mbe_legacyoutputmodel.h"
#include  "state/mbe_state.h"

/* if defined, watercycle daily output exhibits request API */
//#define  OutputWatercycleDaily_HAVE_request_api
#ifdef  OutputWatercycleDaily_HAVE_request_api
#  include  "kernel/kcomm.h"
struct  lmessage_t;
struct  lreply_t;
#endif


#define  LMOD_OUTPUT_MODULE_BASE  MBE_LegacyOutputModel
#define  LMOD_OUTPUT_MODULE_NAME  OutputWatercycleDaily
#define  LMOD_OUTPUT_MODULE_ID    "output:watercycle:daily"
#define  LMOD_OUTPUT_MODULE_DESC  "Water Cycle Daily Output"

namespace ldndc {
class  substate_microclimate_t;
class  substate_physiology_t;
class  substate_soilchemistry_t;
class  substate_watercycle_t;

extern cbm::logger_t *  OutputWatercycleDailyLogger;

class  LDNDC_API  LMOD_OUTPUT_MODULE_NAME  :  public  LMOD_OUTPUT_MODULE_BASE
{
    LMOD_EXPORT_MODULE_INFO(LMOD_OUTPUT_MODULE_NAME,LMOD_OUTPUT_MODULE_ID,LMOD_OUTPUT_MODULE_DESC);

    public:
        static double const WFPS_THRESHOLD;

    public:
        LMOD_OUTPUT_MODULE_NAME(
                MoBiLE_State *,
                cbm::io_kcomm_t *,
                timemode_e  _timemode);

        ~LMOD_OUTPUT_MODULE_NAME();


        lerr_t  configure( ldndc::config_file_t const *);

        lerr_t  initialize();

        lerr_t  solve();

        lerr_t  finalize();

        lerr_t  wake() { return  LDNDC_ERR_OK; }
        lerr_t  sleep() { return  LDNDC_ERR_OK; }

    private:
        lerr_t  execute();
        lerr_t  dump_0(
                ldndc_flt64_t *);

    private:
        cbm::io_kcomm_t *  io_kcomm;
#ifdef  OutputWatercycleDaily_HAVE_request_api
        ldndc::kcomm_t  kcomm;
    public:
        lerr_t  process_message( lmessage_t const *, lreply_t *);
    private:
#endif
        input_class_soillayers_t const *  soillayers_in;
        input_class_setup_t const *  setup_;
        input_class_groundwater_t const *  m_groundwater;
        substate_microclimate_t const *  microclim;
        substate_physiology_t const *  phys;
        substate_soilchemistry_t const *  soilchem;
        substate_watercycle_t const *  water;

        MoBiLE_PlantVegetation *  m_veg;

    private:
        ldndc::sink_handle_t  m_sink;

        /* configuration options: */
        struct  conf_t
        {
            bool  bypass_mobile:1;
        };
        struct conf_t  rc;

        /* configure format of water content output: water filled pore space / water content */
        bool format_wfps;

        struct  output_watercycle_daily_acc_t
        {
            double  irrigation;
            double  groundwateraccess;
            double  groundwaterloss;
            double  capillaryrise;
            double  automatic_irrigation;
            double  reservoir_irrigation;
            double  ggcmi_irrigation;
            double  throughfall;
            double  potentialevaporation;
            double  potentialtranspiration;
            double  transpiration;
            double  interceptionevaporation;
            double  soilevaporation;
            double  surfacewaterevaporation;
            double  runoff;
            double  percolation;
            double  surface_waterflux;
        };
        output_watercycle_daily_acc_t  acc;
        void  pop_accumulated_outputs();
        void  push_accumulated_outputs();
};
} /*namespace ldndc*/


#undef  LMOD_OUTPUT_MODULE_BASE
#undef  LMOD_OUTPUT_MODULE_NAME
#undef  LMOD_OUTPUT_MODULE_ID
#undef  LMOD_OUTPUT_MODULE_DESC


#endif  /*  !LM_OUTPUT_WATERCYCLE_DAILY_H_NEW_  */
