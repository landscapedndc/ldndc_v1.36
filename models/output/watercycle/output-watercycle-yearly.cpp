/*!
 * @file
 * @author
 *  - Edwin Haas
 *  - Ruediger Grote
 *  - David Kraus
 */

#include  "output/watercycle/output-watercycle-yearly.h"

#include  "constants/lconstants-conv.h"

#define  LMOD_OUTPUT_MODULE_BASE  MBE_LegacyOutputModel
#define  LMOD_OUTPUT_MODULE_NAME  OutputWatercycleYearly
#define  LMOD_OUTPUT_MODULE_TIMEMODE  TMODE_POST_YEARLY


LMOD_MODULE_INFO(LMOD_OUTPUT_MODULE_NAME,LMOD_OUTPUT_MODULE_TIMEMODE,LMOD_FLAG_USER|LMOD_FLAG_OUTPUT);
namespace ldndc {


#define  OutputWatercycleYearly_Item_prec
#define  OutputWatercycleYearly_Item_irri
#define  OutputWatercycleYearly_Item_throughf
#define  OutputWatercycleYearly_Item_evapot
#define  OutputWatercycleYearly_Item_transp
#define  OutputWatercycleYearly_Item_evacep
#define  OutputWatercycleYearly_Item_evasoil
#define  OutputWatercycleYearly_Item_runoff
#define  OutputWatercycleYearly_Item_percol
#define  OutputWatercycleYearly_Item_infiltration

/*!
 * @page watercycleoutput
 * @section watercycle_output_yearly Watercycle output (yearly)
 *
 * <b>xml-based module selection in project's setup:</b>
 * \n\code{.xml}< module id="output:watercycle:yearly" />\endcode\n
 *
 * entity name | decription | unit
 * ----------- | ---------- | ----
 * irri | Irrigation | [mm]
 * prec | Precipitation | [mm]
 * throughfall | Throughfall | [mm]
 * evapot | Potential evapotranspiration | [mm]
 * transp | Transpiration | [mm]
 * evacep | Evaporation from the interception in the leaves | [mm]
 * evasoil | Evaporation from the soil surface | [mm]
 * evasurf | Evaporation from surface water | [mm]
 * runoff | Surface water runoff | [mm]
 * percol | Percolation out of last soil layer | [mm]
 * infiltration | Infiltration | [mm]
 */
ldndc_string_t const  OutputWatercycleYearly_Ids[] =
{
#ifdef  OutputWatercycleYearly_Item_irri
    "irri",
#endif
#ifdef  OutputWatercycleYearly_Item_prec
    "prec",
#endif
#ifdef  OutputWatercycleYearly_Item_throughf
    "throughfall",
#endif
#ifdef  OutputWatercycleYearly_Item_evapot
    "evapot",
#endif
#ifdef  OutputWatercycleYearly_Item_transp
    "transp",
#endif
#ifdef  OutputWatercycleYearly_Item_evacep
    "evacep",
#endif
#ifdef  OutputWatercycleYearly_Item_evasoil
    "evasoil",
    "evasurf",
#endif
#ifdef  OutputWatercycleYearly_Item_runoff
    "runoff",
#endif
#ifdef  OutputWatercycleYearly_Item_percol
    "percol",
#endif
#ifdef  OutputWatercycleYearly_Item_infiltration
    "infiltration",
#endif
};

ldndc_string_t const  OutputWatercycleYearly_Header[] =
{
#ifdef  OutputWatercycleYearly_Item_irri
    "irri[mm]",
#endif
#ifdef  OutputWatercycleYearly_Item_prec
    "prec[mm]",
#endif
#ifdef  OutputWatercycleYearly_Item_throughf
    "throughf[mm]",
#endif
#ifdef  OutputWatercycleYearly_Item_evapot
    "evapot[mm]",
#endif
#ifdef  OutputWatercycleYearly_Item_transp
    "transp[mm]",
#endif
#ifdef  OutputWatercycleYearly_Item_evacep
    "evacep[mm]",
#endif
#ifdef  OutputWatercycleYearly_Item_evasoil
    "evasoil[mm]",
    "evaporation_surfacewater[mm]",
#endif
#ifdef  OutputWatercycleYearly_Item_runoff
    "runoff[mm]",
#endif
#ifdef  OutputWatercycleYearly_Item_percol
    "percol[mm]",
#endif
#ifdef  OutputWatercycleYearly_Item_infiltration
    "infiltration[mm]",
#endif
};
#define  OutputWatercycleYearly_Datasize  (sizeof( OutputWatercycleYearly_Header) / sizeof( OutputWatercycleYearly_Header[0]))
ldndc_output_size_t const  OutputWatercycleYearly_Sizes[] =
{
    OutputWatercycleYearly_Datasize,

    OutputWatercycleYearly_Datasize /*total size*/
};
ldndc_output_size_t const *  OutputWatercycleYearly_EntitySizes = NULL;
#define  OutputWatercycleYearly_Rank  ((ldndc_output_rank_t)(sizeof( OutputWatercycleYearly_Sizes) / sizeof( OutputWatercycleYearly_Sizes[0])) - 1)
atomic_datatype_t const  OutputWatercycleYearly_Types[] =
{
    LDNDC_FLOAT64
};


LMOD_OUTPUT_MODULE_NAME::LMOD_OUTPUT_MODULE_NAME(
        MoBiLE_State *  _state,
        cbm::io_kcomm_t *  _io_kcomm,
        timemode_e  _timemode)
        : LMOD_OUTPUT_MODULE_BASE( _state, _io_kcomm, _timemode),

          io_kcomm( _io_kcomm),

          soillayers_in( _io_kcomm->get_input_class< input_class_soillayers_t >()),
          microclim( _state->get_substate< substate_microclimate_t >()),
          phys( _state->get_substate< substate_physiology_t >()),
          soilchem( _state->get_substate< substate_soilchemistry_t >()),
          water( _state->get_substate< substate_watercycle_t >())
{
}


LMOD_OUTPUT_MODULE_NAME::~LMOD_OUTPUT_MODULE_NAME()
{
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::configure(
                ldndc::config_file_t const *  _cf)
{
    lerr_t  rc_setflags =
        this->set_metaflags( _cf, RM_DEFAULT_DAILY);
    if ( rc_setflags)
        { return  LDNDC_ERR_FAIL; }

    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::initialize()
{
    m_sink = io_kcomm->sink_handle_acquire( "watercycleyearly");
    if ( m_sink.status() == LDNDC_ERR_OK)
    {
        lerr_t  rc_layout = LDNDC_OUTPUT_DEFINE_SINK_LAYOUT_DEFAULTS(
            this->m_sink,OutputWatercycleYearly);
        RETURN_IF_NOT_OK(rc_layout);
    }
    else
    {
        KLOGERROR( "sink status bad  [sink=","watercycleyearly","]");
        return  m_sink.status();
    }

    push_accumulated_outputs();

    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::solve()
{
    ldndc_flt64_t  data_flt64_0[OutputWatercycleYearly_Datasize];

    this->pop_accumulated_outputs();
    lerr_t  rc_dump = dump_0( data_flt64_0);
    this->push_accumulated_outputs();

    if ( rc_dump)
        { return  LDNDC_ERR_FAIL; }

    void *  data[] = { data_flt64_0};
    lerr_t  rc_write =
        this->write_fixed_record( &this->m_sink, data);
    if ( rc_write)
        { return  LDNDC_ERR_FAIL; }

    return  LDNDC_ERR_OK;
}

void
LMOD_OUTPUT_MODULE_NAME::pop_accumulated_outputs()
{
    acc.prec = water->accumulated_precipitation - acc.prec;
    acc.irrigation = water->accumulated_irrigation - acc.irrigation;
    acc.irrigation_automatic = water->accumulated_irrigation_automatic - acc.irrigation_automatic;
    acc.irrigation_reservoir = water->accumulated_irrigation_reservoir_withdrawal - acc.irrigation_reservoir;
    acc.irrigation_ggcmi = water->accumulated_irrigation_ggcmi - acc.irrigation_ggcmi;
    acc.throughfall = water->accumulated_throughfall - acc.throughfall;
    acc.potentialevaporation = water->accumulated_potentialevapotranspiration - acc.potentialevaporation;
    acc.transpiration = water->accumulated_transpiration_sl.sum() - acc.transpiration;
    acc.interceptionevaporation = water->accumulated_interceptionevaporation - acc.interceptionevaporation;
    acc.soilevaporation = water->accumulated_soilevaporation - acc.soilevaporation;
    acc.surfacewaterevaporation = water->accumulated_surfacewaterevaporation - acc.surfacewaterevaporation;
    acc.runoff = water->accumulated_runoff - acc.runoff;
    acc.percolation = water->accumulated_percolation - acc.percolation;
    acc.infiltration = water->accumulated_infiltration - acc.infiltration;
}
void
LMOD_OUTPUT_MODULE_NAME::push_accumulated_outputs()
{
    acc.prec = water->accumulated_precipitation;
    acc.irrigation = water->accumulated_irrigation;
    acc.irrigation_automatic = water->accumulated_irrigation_automatic;
    acc.irrigation_reservoir = water->accumulated_irrigation_reservoir_withdrawal;
    acc.irrigation_ggcmi = water->accumulated_irrigation_ggcmi;
    acc.throughfall = water->accumulated_throughfall;
    acc.potentialevaporation = water->accumulated_potentialevapotranspiration;
    acc.transpiration = water->accumulated_transpiration_sl.sum();
    acc.interceptionevaporation = water->accumulated_interceptionevaporation;
    acc.soilevaporation = water->accumulated_soilevaporation;
    acc.surfacewaterevaporation = water->accumulated_surfacewaterevaporation;
    acc.runoff = water->accumulated_runoff;
    acc.percolation = water->accumulated_percolation;
    acc.infiltration = water->accumulated_infiltration;
}

lerr_t
LMOD_OUTPUT_MODULE_NAME::finalize()
{
    io_kcomm->sink_handle_release( &m_sink);
    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::dump_0(
                                ldndc_flt64_t *_buf)
{
    LDNDC_OUTPUT_SET_COLUMN_INDEX(0);

#ifdef  OutputWatercycleYearly_Item_irri
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT((acc.irrigation+acc.irrigation_automatic+acc.irrigation_reservoir+acc.irrigation_ggcmi) * cbm::MM_IN_M);
#endif
#ifdef  OutputWatercycleYearly_Item_prec
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT((acc.prec) * cbm::MM_IN_M);
#endif
#ifdef  OutputWatercycleYearly_Item_throughf
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(acc.throughfall * cbm::MM_IN_M);
#endif
#ifdef  OutputWatercycleYearly_Item_evapot
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(acc.potentialevaporation * cbm::MM_IN_M);
#endif
#ifdef  OutputWatercycleYearly_Item_transp
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(acc.transpiration * cbm::MM_IN_M);
#endif
#ifdef  OutputWatercycleYearly_Item_evacep
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(acc.interceptionevaporation * cbm::MM_IN_M);
#endif
#ifdef  OutputWatercycleYearly_Item_evasoil
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(acc.soilevaporation * cbm::MM_IN_M);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(acc.surfacewaterevaporation * cbm::MM_IN_M);
#endif
#ifdef  OutputWatercycleYearly_Item_runoff
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(acc.runoff * cbm::MM_IN_M);
#endif
#ifdef  OutputWatercycleYearly_Item_percol
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(acc.percolation * cbm::MM_IN_M);
#endif
#ifdef  OutputWatercycleYearly_Item_infiltration
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(acc.infiltration * cbm::MM_IN_M);
#endif


    return  LDNDC_ERR_OK;
}


#undef  LMOD_OUTPUT_MODULE_BASE
#undef  LMOD_OUTPUT_MODULE_NAME

#undef  OutputWatercycleYearly_Rank
#undef  OutputWatercycleYearly_Datasize

} /*namespace ldndc*/
