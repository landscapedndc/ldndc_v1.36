/*!
 * @brief
 *    dumping modified water cycle related items
 *
 * @author
 *    steffen klatt,
 *    edwin haas
 */

#ifndef  LM_OUTPUT_WATERCYCLE_LAYER_DAILY_H_
#define  LM_OUTPUT_WATERCYCLE_LAYER_DAILY_H_

#include  "mbe_legacyoutputmodel.h"
#include  "state/mbe_state.h"

/* if defined, watercycle daily output exhibits request API */
//#define  OutputWatercycleLayerDaily_HAVE_request_api
#ifdef  OutputWatercycleLayerDaily_HAVE_request_api
#  include  "kernel/kcomm.h"
struct  lmessage_t;
struct  lreply_t;
#endif

#define  LMOD_OUTPUT_MODULE_BASE  MBE_LegacyOutputModel
#define  LMOD_OUTPUT_MODULE_NAME  OutputWatercycleLayerDaily
#define  LMOD_OUTPUT_MODULE_ID    "output:watercycle-layer:daily"
#define  LMOD_OUTPUT_MODULE_DESC  "Water Cycle Daily Output (Layers)"
namespace ldndc {
class  LDNDC_API  LMOD_OUTPUT_MODULE_NAME  :  public  LMOD_OUTPUT_MODULE_BASE
{
    LMOD_EXPORT_MODULE_INFO(LMOD_OUTPUT_MODULE_NAME,LMOD_OUTPUT_MODULE_ID,LMOD_OUTPUT_MODULE_DESC);
    public:
        LMOD_OUTPUT_MODULE_NAME(
                MoBiLE_State *,
                cbm::io_kcomm_t *,
                timemode_e  _timemode);

        ~LMOD_OUTPUT_MODULE_NAME();


        lerr_t  configure( ldndc::config_file_t const *);

        lerr_t  initialize();

        lerr_t  solve();

        lerr_t  finalize();

        lerr_t  wake() { return  LDNDC_ERR_OK; }
        lerr_t  sleep() { return  LDNDC_ERR_OK; }

    private:
        lerr_t  execute();
        lerr_t  dump_0( ldndc_flt64_t * /*buffer*/);

    private:
        cbm::io_kcomm_t *  io_kcomm;
#ifdef  OutputWatercycleLayerDaily_HAVE_request_api
        ldndc::kcomm_t  kcomm;
    public:
        lerr_t  process_message( lmessage_t const *, lreply_t *);
    private:
#endif

        input_class_soillayers_t const *  soillayers_;
        input_class_setup_t const *  setup_;
        substate_soilchemistry_t const *  sc;
        substate_physiology_t const *  ph;
        substate_watercycle_t const *  wc;

        MoBiLE_PlantVegetation *  m_veg;

    private:

        ldndc::sink_handle_t  m_sink;

        struct  output_watercycle_cum_acc_t
        {
            double infiltration_cum;
            double * waterflux_cum_sl;
            double * transpiration_cum_sl;
        };
        output_watercycle_cum_acc_t  acc;
    
        void  pop_accumulated_outputs();
        void  push_accumulated_outputs();

        /* configuration options: */
        /*    foliage, surface and soil layer selection */
        struct  conf_t
        {
            bool  write_foliage:1;
            bool  foliage_all:1;

            bool  write_surface:1;

            bool  write_soil:1;
            bool  soil_all:1;

            bool  bypass_mobile:1;
        };
        struct conf_t  rc;
};
} /*namespace ldndc*/


#undef  LMOD_OUTPUT_MODULE_BASE
#undef  LMOD_OUTPUT_MODULE_NAME
#undef  LMOD_OUTPUT_MODULE_ID
#undef  LMOD_OUTPUT_MODULE_DESC


#endif  /*  !LM_OUTPUT_WATERCYCLE_LAYER_DAILY_H_  */

