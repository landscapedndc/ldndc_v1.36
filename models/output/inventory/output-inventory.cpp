/*!
 * @brief
 *
 * @author
 *    David Kraus (created on: january, 2022)
 *
 */

#include  "output/inventory/output-inventory.h"
#include  "constants/lconstants-conv.h"

#define  LMOD_OUTPUT_MODULE_BASE  MBE_LegacyOutputModel
#define  LMOD_OUTPUT_MODULE_NAME  OutputInventory
#define  LMOD_OUTPUT_MODULE_TIMEMODE  TMODE_POST_DAILY


LMOD_MODULE_INFO(LMOD_OUTPUT_MODULE_NAME,LMOD_OUTPUT_MODULE_TIMEMODE,LMOD_FLAG_USER|LMOD_FLAG_OUTPUT);
namespace ldndc {


#define  OutputInventory_Item_irri
#define  OutputInventory_Item_prec
#define  OutputInventory_Item_perc
#define  OutputInventory_Item_csoil
#define  OutputInventory_Item_nsoil
#define  OutputInventory_Item_dwabove
#define  OutputInventory_Item_dwfru
#define  OutputInventory_Item_plantfluxes
#define  OutputInventory_Item_cemis
#define  OutputInventory_Item_nemis
#define  OutputInventory_Item_cleach
#define  OutputInventory_Item_nleach
#define  OutputInventory_Item_fert


/*!
 * @page inventoryoutput
 * @section inventory_output Inventory output (daily/yearly)
 *
 * entity name | decription | unit
 * ----------- | ---------- | ----
 */
ldndc_string_t const  OutputInventory_Ids[] =
{
    "surfacetemperature",
    "surfacewater",
#ifdef  OutputInventory_Item_irri
    "irrigation",
#endif
#ifdef  OutputInventory_Item_prec
    "precipitation",
#endif
#ifdef  OutputInventory_Item_perc
    "percolation",
#endif
#ifdef  OutputInventory_Item_csoil
    "C_soil",
#endif
#ifdef  OutputInventory_Item_nsoil
    "N_soil",
#endif
#ifdef  OutputInventory_Item_dwabove
    "DW_above",
    "C_stubble",
#endif
#ifdef  OutputInventory_Item_dwfru
    "DW_fru",
#endif
#ifdef  OutputInventory_Item_plantfluxes
    "DW_fru_export",
    "C_plant_export",
    "C_plant_litter",
#endif
#ifdef  OutputInventory_Item_cemis
    "dC_ch4_emis",
    "dC_co2_emis_hetero",
#endif
#ifdef  OutputInventory_Item_nemis
    "dN_n2o_emis",
    "dN_no_emis",
    "dN_n2_emis",
    "dN_nh3_emis",
#endif
#ifdef  OutputInventory_Item_cleach
    "dC_doc_leach",
#endif
#ifdef  OutputInventory_Item_nleach
    "dN_no3_leach",
    "dN_don_leach",
#endif
#ifdef  OutputInventory_Item_fert
    "dN_fertilizer",
    "dC_fertilizer",
#endif
};


ldndc_string_t const  OutputInventory_Header[] =
{
    "surfacetemperature[oC]",
    "surfacewater[mm]",
#ifdef  OutputInventory_Item_irri
    "irrigation[mm]",
#endif
#ifdef  OutputInventory_Item_prec
    "precipitation[mm]",
#endif
#ifdef  OutputInventory_Item_perc
    "percolation[mm]",
#endif
#ifdef  OutputInventory_Item_csoil
    "C_soil[kgm-2]",
#endif
#ifdef  OutputInventory_Item_nsoil
    "N_soil[kgm-2]",
#endif
#ifdef  OutputInventory_Item_dwabove
    "DW_above[kgm-2]",
    "C_stubble[kgm-2]",
#endif
#ifdef  OutputInventory_Item_dwfru
    "DW_fru[kgm-2]",
#endif
#ifdef  OutputInventory_Item_plantfluxes
    "DW_fru_export[kgm-2]",
    "C_plant_export[kgm-2]",
    "C_plant_litter[kgm-2]",
#endif
#ifdef  OutputInventory_Item_cemis
    "dC_ch4_emis[kgm-2]",
    "dC_co2_emis_hetero[kgm-2]",
#endif
#ifdef  OutputInventory_Item_nemis
    "dN_n2o_emis[kgm-2]",
    "dN_no_emis[kgm-2]",
    "dN_n2_emis[kgm-2]",
    "dN_nh3_emis[kgm-2]",
#endif
#ifdef  OutputInventory_Item_cleach
    "dC_doc_leach[kgm-2]",
#endif
#ifdef  OutputInventory_Item_nleach
    "dN_no3_leach[kgm-2]",
    "dN_don_leach[kgm-2]",
#endif
#ifdef  OutputInventory_Item_fert
    "dN_fertilizer[kgm-2]",
    "dC_fertilizer[kgm-2]",
#endif
};


#define  OutputInventory_Datasize  (sizeof( OutputInventory_Header) / sizeof( OutputInventory_Header[0]))
ldndc_output_size_t const  OutputInventory_Sizes[] =
{
    OutputInventory_Datasize,
    OutputInventory_Datasize /*total size*/
};


ldndc_output_size_t const *  OutputInventory_EntitySizes = NULL;
#define  OutputInventory_Rank  ((ldndc_output_rank_t)(sizeof( OutputInventory_Sizes) / sizeof( OutputInventory_Sizes[0])) - 1)
atomic_datatype_t const  OutputInventory_Types[] =
{
    LDNDC_FLOAT64
};


LMOD_OUTPUT_MODULE_NAME::LMOD_OUTPUT_MODULE_NAME(
        MoBiLE_State *  _state,
        cbm::io_kcomm_t *  _io_kcomm,
        timemode_e  _timemode)
        : LMOD_OUTPUT_MODULE_BASE( _state, _io_kcomm, _timemode),

          io_kcomm( _io_kcomm),

          soillayers_in( _io_kcomm->get_input_class< input_class_soillayers_t >()),
          microclim( _state->get_substate< substate_microclimate_t >()),
          phys( _state->get_substate< substate_physiology_t >()),
          m_veg( &_state->vegetation),
          soilchem( _state->get_substate< substate_soilchemistry_t >()),
          water( _state->get_substate< substate_watercycle_t >())
{}


LMOD_OUTPUT_MODULE_NAME::~LMOD_OUTPUT_MODULE_NAME()
{}


lerr_t
LMOD_OUTPUT_MODULE_NAME::configure(
                ldndc::config_file_t const *  _cf)
{
    lerr_t  rc_setflags = set_metaflags( _cf, RM_DEFAULT_DAILY);
    if ( rc_setflags)
    { return  LDNDC_ERR_FAIL; }

    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::initialize()
{
    m_sink = io_kcomm->sink_handle_acquire( "inventory");
    if ( m_sink.status() == LDNDC_ERR_OK)
    {
        lerr_t  rc_layout = LDNDC_OUTPUT_DEFINE_SINK_LAYOUT_DEFAULTS( m_sink, OutputInventory);
        RETURN_IF_NOT_OK( rc_layout);
    }
    else
    {
        KLOGERROR( "sink status bad  [sink=","inventory","]");
        return  m_sink.status();
    }

    push_outputs();

    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::solve()
{
    ldndc_flt64_t  data_flt64_0[OutputInventory_Datasize];

    if ( true)
    {
        pop_outputs();

        lerr_t rc_dump = dump_0( data_flt64_0);
        if ( rc_dump)
        { return  LDNDC_ERR_FAIL; }

        push_outputs();
    }

    void *  data[] = { data_flt64_0};
    lerr_t  rc_write = write_fixed_record( &this->m_sink, data);
    if ( rc_write)
    { return  LDNDC_ERR_FAIL; }

    return  LDNDC_ERR_OK;
}


void
LMOD_OUTPUT_MODULE_NAME::pop_outputs()
{
    dump.c_soil = 0.0;
    dump.n_soil = 0.0;
    for ( size_t  sl = 0;  sl < soillayers_in->soil_layer_cnt();  ++sl)
    {
        dump.c_soil += soilchem->c_wood_sl[sl];
        dump.n_soil += soilchem->n_wood_sl[sl];

        if ( sl < soillayers_in->soil_layers_in_litter_cnt())
        {
            dump.c_soil += (soilchem->C_lit1_sl[sl] + soilchem->C_lit2_sl[sl] + soilchem->C_lit3_sl[sl]
                            + soilchem->c_raw_lit_1_sl[sl] + soilchem->c_raw_lit_2_sl[sl] + soilchem->c_raw_lit_3_sl[sl]
                            + soilchem->C_aorg_sl[sl] + soilchem->C_hum_sl[sl]);
            dump.n_soil += (soilchem->N_lit1_sl[sl]+soilchem->N_lit2_sl[sl]+soilchem->N_lit3_sl[sl]
                            + soilchem->n_raw_lit_1_sl[sl] + soilchem->n_raw_lit_2_sl[sl] + soilchem->n_raw_lit_3_sl[sl]
                            + soilchem->N_aorg_sl[sl] + soilchem->N_hum_sl[sl]);
            
            dump.c_soil += (soilchem->doc_sl[sl] + soilchem->an_doc_sl[sl]);
            dump.n_soil += (soilchem->nh3_liq_sl[sl] + soilchem->nh3_gas_sl[sl]
                            + soilchem->nh4_sl[sl] + soilchem->no3_sl[sl] + soilchem->no2_sl[sl]
                            + soilchem->no_sl[sl] + soilchem->n2o_sl[sl] + soilchem->don_sl[sl]
                            + soilchem->an_no3_sl[sl] + soilchem->an_no2_sl[sl]
                            + soilchem->an_no_sl[sl] + soilchem->an_n2o_sl[sl] + soilchem->clay_nh4_sl[sl]);
            
            dump.c_soil += soilchem->C_micro1_sl[sl] + soilchem->C_micro2_sl[sl] + soilchem->C_micro3_sl[sl];
            dump.n_soil += soilchem->N_micro_sl[sl];
        }
    }

    dump.c_stubble = soilchem->c_stubble_lit1 + soilchem->c_stubble_lit2 + soilchem->c_stubble_lit3;

    dump.dw_above = 0.0;
    dump.dw_fru = 0.0;
    for ( PlantIterator s = m_veg->begin(); s != m_veg->end(); ++s)
    {
        dump.dw_above += (*s)->aboveground_biomass();
        dump.dw_fru += (*s)->mBud;
    }
}


void
LMOD_OUTPUT_MODULE_NAME::push_outputs()
{
    dump.acc_irrigation_old = water->accumulated_irrigation +
                              water->accumulated_irrigation_automatic +
                              water->accumulated_irrigation_reservoir_withdrawal +
                              water->accumulated_irrigation_ggcmi;
    dump.acc_precipitation_old = water->accumulated_precipitation;
    dump.acc_percolation_old = water->accumulated_percolation;

    dump.acc_c_fru_export = phys->accumulated_c_fru_export_harvest;
    dump.acc_c_plant_export = phys->accumulated_c_fru_export_harvest;
    dump.acc_c_plant_litter = soilchem->accumulated_c_litter_below_sl.sum() + soilchem->accumulated_c_litter_stubble;

    dump.acc_c_ch4_emis_old = soilchem->accumulated_ch4_emis;
    dump.acc_c_co2_emis_hetero_old = soilchem->accumulated_co2_emis_hetero;

    dump.acc_n_n2o_emis_old = soilchem->accumulated_n2o_emis;
    dump.acc_n_no_emis_old = soilchem->accumulated_no_emis;
    dump.acc_n_n2_emis_old = soilchem->accumulated_n2_emis;
    dump.acc_n_nh3_emis_old = soilchem->accumulated_nh3_emis;
    
    dump.acc_c_doc_leach_old = soilchem->accumulated_doc_leach;
    
    dump.acc_n_no3_leach_old = soilchem->accumulated_no3_leach;
    dump.acc_n_don_leach_old = soilchem->accumulated_don_leach;
    
    dump.acc_n_fertilizer_old = soilchem->accumulated_n_fertilizer;
    dump.acc_c_fertilizer_old = soilchem->accumulated_c_fertilizer;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::finalize()
{
    io_kcomm->sink_handle_release( &m_sink);
    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::dump_0(
                                ldndc_flt64_t *_buf)
{
    LDNDC_OUTPUT_SET_COLUMN_INDEX(0);

    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(microclim->temp_a);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(water->surface_water * cbm::MM_IN_M);
#ifdef  OutputInventory_Item_irri
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT((water->accumulated_irrigation +
                                           water->accumulated_irrigation_automatic +
                                           water->accumulated_irrigation_reservoir_withdrawal +
                                           water->accumulated_irrigation_ggcmi - dump.acc_irrigation_old) * cbm::MM_IN_M);
#endif
#ifdef  OutputInventory_Item_prec
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT((water->accumulated_precipitation - dump.acc_precipitation_old) * cbm::MM_IN_M);
#endif
#ifdef  OutputInventory_Item_perc
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT((water->accumulated_percolation - dump.acc_percolation_old) * cbm::MM_IN_M);
#endif
#ifdef  OutputInventory_Item_csoil
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(dump.c_soil);
#endif
#ifdef  OutputInventory_Item_nsoil
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(dump.n_soil);
#endif
#ifdef  OutputInventory_Item_dwabove
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(dump.dw_above);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(dump.c_stubble);
#endif
#ifdef  OutputInventory_Item_dwfru
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(dump.dw_fru);
#endif
#ifdef  OutputInventory_Item_plantfluxes
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT((phys->accumulated_c_fru_export_harvest - dump.acc_c_fru_export) / cbm::CCDM);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(phys->accumulated_c_fru_export_harvest - dump.acc_c_plant_export);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(soilchem->accumulated_c_litter_below_sl.sum() + soilchem->accumulated_c_litter_stubble - dump.acc_c_plant_litter);
#endif
#ifdef  OutputInventory_Item_cemis
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(soilchem->accumulated_ch4_emis - dump.acc_c_ch4_emis_old);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(soilchem->accumulated_co2_emis_hetero - dump.acc_c_co2_emis_hetero_old);
#endif
#ifdef  OutputInventory_Item_nemis
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(soilchem->accumulated_n2o_emis - dump.acc_n_n2o_emis_old);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(soilchem->accumulated_no_emis - dump.acc_n_no_emis_old);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(soilchem->accumulated_n2_emis - dump.acc_n_n2_emis_old);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(soilchem->accumulated_nh3_emis - dump.acc_n_nh3_emis_old);
#endif
#ifdef  OutputInventory_Item_cleach
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(soilchem->accumulated_doc_leach - dump.acc_c_doc_leach_old);
#endif
#ifdef  OutputInventory_Item_nleach
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(soilchem->accumulated_no3_leach - dump.acc_n_no3_leach_old);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(soilchem->accumulated_don_leach - dump.acc_n_don_leach_old);
#endif
#ifdef  OutputInventory_Item_fert
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(soilchem->accumulated_n_fertilizer - dump.acc_n_fertilizer_old);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(soilchem->accumulated_c_fertilizer - dump.acc_c_fertilizer_old);
#endif

    return  LDNDC_ERR_OK;
}


#undef  LMOD_OUTPUT_MODULE_BASE
#undef  LMOD_OUTPUT_MODULE_NAME

#undef  OutputInventory_Rank
#undef  OutputInventory_Datasize

} /*namespace ldndc*/
