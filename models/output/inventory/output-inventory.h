/*!
 * @file
 *    Inventory output module
 *
 * @author
 *    David Kraus (created on: january, 2022)
 */


#ifndef  LM_OUTPUT_INVENTORY_
#define  LM_OUTPUT_INVENTORY_

#include  "mbe_legacyoutputmodel.h"
#include  "state/mbe_state.h"

#define  LMOD_OUTPUT_MODULE_BASE  MBE_LegacyOutputModel
#define  LMOD_OUTPUT_MODULE_NAME  OutputInventory
#define  LMOD_OUTPUT_MODULE_ID    "output:inventory"
#define  LMOD_OUTPUT_MODULE_DESC  "Inventory Output"

namespace ldndc {
class  substate_microclimate_t;
class  substate_physiology_t;
class  substate_soilchemistry_t;
class  substate_watercycle_t;
class  LDNDC_API  LMOD_OUTPUT_MODULE_NAME  :  public  LMOD_OUTPUT_MODULE_BASE
{
    LMOD_EXPORT_MODULE_INFO(LMOD_OUTPUT_MODULE_NAME,LMOD_OUTPUT_MODULE_ID,LMOD_OUTPUT_MODULE_DESC);
    public:
        LMOD_OUTPUT_MODULE_NAME(
                MoBiLE_State *,
                cbm::io_kcomm_t *,
                timemode_e  _timemode);

        ~LMOD_OUTPUT_MODULE_NAME();


        lerr_t  configure( ldndc::config_file_t const *);

        lerr_t  initialize();

        lerr_t  solve();

        lerr_t  finalize();

        lerr_t  wake() { return  LDNDC_ERR_OK; }
        lerr_t  sleep() { return  LDNDC_ERR_OK; }

    private:
        lerr_t  dump_0(
                ldndc_flt64_t *);

    private:
        cbm::io_kcomm_t *  io_kcomm;

        input_class_soillayers_t const *  soillayers_in;

        substate_microclimate_t const *  microclim;
        substate_physiology_t const *  phys;
        MoBiLE_PlantVegetation *  m_veg;
        substate_soilchemistry_t const *  soilchem;
        substate_watercycle_t const *  water;

    private:
        ldndc::sink_handle_t  m_sink;

        struct output_inventory_t
        {
            double acc_precipitation_old;
            double acc_irrigation_old;
            double acc_percolation_old;
            double c_soil;
            double n_soil;
            double dw_above;
            double c_stubble;
            double dw_fru;
            double acc_c_fru_export;
            double acc_c_plant_export;
            double acc_c_plant_litter;
            double acc_c_ch4_emis_old;
            double acc_c_co2_emis_hetero_old;
            double acc_n_n2o_emis_old;
            double acc_n_no_emis_old;
            double acc_n_n2_emis_old;
            double acc_n_nh3_emis_old;
            double acc_c_doc_leach_old;
            double acc_n_no3_leach_old;
            double acc_n_don_leach_old;
            double acc_n_fertilizer_old;
            double acc_c_fertilizer_old;
        };
        output_inventory_t  dump;

        void  pop_outputs();
        void  push_outputs();
};
} /*namespace ldndc*/


#undef  LMOD_OUTPUT_MODULE_BASE
#undef  LMOD_OUTPUT_MODULE_NAME
#undef  LMOD_OUTPUT_MODULE_ID
#undef  LMOD_OUTPUT_MODULE_DESC


#endif  /*  !LM_OUTPUT_INVENTORY_  */
