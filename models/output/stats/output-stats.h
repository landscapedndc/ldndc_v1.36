/*!
 * @brief
 *    stats writer module
 *
 * @author
 *    steffen klatt (created on: mar 22, 2015)
 */

#ifndef  LM_OUTPUT_OutputMoBiLEKernelStatistics_DAILY_H_
#define  LM_OUTPUT_OutputMoBiLEKernelStatistics_DAILY_H_

#include  "mbe_legacyoutputmodel.h"
#include  "state/mbe_state.h"

#define  LMOD_OUTPUT_MODULE_BASE  MBE_LegacyModel
#define  LMOD_OUTPUT_MODULE_NAME  OutputMoBiLEKernelStatistics
#define  LMOD_OUTPUT_MODULE_ID    "output:kernelstatistics"
#define  LMOD_OUTPUT_MODULE_DESC  "Kernel Statistics Output"
namespace ldndc {

class  LDNDC_API  LMOD_OUTPUT_MODULE_NAME  :  public  LMOD_OUTPUT_MODULE_BASE
{
    LMOD_EXPORT_MODULE_INFO(LMOD_OUTPUT_MODULE_NAME,LMOD_OUTPUT_MODULE_ID,LMOD_OUTPUT_MODULE_DESC);
    public:
        LMOD_OUTPUT_MODULE_NAME(
                MoBiLE_State *,
                cbm::io_kcomm_t *,
                timemode_e  _timemode);

        ~LMOD_OUTPUT_MODULE_NAME();


        lerr_t  configure( ldndc::config_file_t const *);

        lerr_t  initialize();

        lerr_t  solve();

        lerr_t  finalize();

        lerr_t  wake() { return  LDNDC_ERR_OK; }
        lerr_t  sleep() { return  LDNDC_ERR_OK; }

    private:
        lerr_t  dump_0(
                ldndc_flt64_t *);

    private:
        cbm::io_kcomm_t *  io_kcomm;
        ldndc::sink_handle_t  sink_hdl;
};
} /*namespace ldndc*/


#undef  LMOD_OUTPUT_MODULE_BASE
#undef  LMOD_OUTPUT_MODULE_NAME
#undef  LMOD_OUTPUT_MODULE_ID
#undef  LMOD_OUTPUT_MODULE_DESC


#endif  /*  !LM_OUTPUT_OutputMoBiLEKernelStatistics_DAILY_H_  */

