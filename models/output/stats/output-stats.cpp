/*!
 * @author
 *    steffen klatt (created on: mar 22, 2015)
 */

#include  "output/stats/output-stats.h"

#if  defined(_LDNDC_MONITOR_RESOURCES) && defined(_HAVE_LDNDC_KERNEL_STATISTICS)
#  define  OutputMoBiLEKernelStatistics_StatsAvail
#  include  "kernel/kstats.h"
#endif

#define  LMOD_OUTPUT_MODULE_BASE  MBE_LegacyModel
#define  LMOD_OUTPUT_MODULE_NAME  OutputMoBiLEKernelStatistics

LMOD_MODULE_INFO(LMOD_OUTPUT_MODULE_NAME,TMODE_SUBDAILY|TMODE_POST_DAILY,LMOD_FLAG_USER|LMOD_FLAG_OUTPUT);
namespace ldndc {


ldndc_string_t const  OutputMoBiLEKernelStatistics_Header[] =
{
#ifdef  OutputMoBiLEKernelStatistics_StatsAvail
    "cpumin", "cpumax", "cpuavg", "cputotal",
    "wallmin", "wallmax", "wallavg", "walltotal"
#else
    "timestep" /*dummy*/
#endif
};
ldndc_string_t const *  OutputMoBiLEKernelStatistics_Ids =
    OutputMoBiLEKernelStatistics_Header;

#define  OutputMoBiLEKernelStatistics_Datasize  (sizeof( OutputMoBiLEKernelStatistics_Header) / sizeof( OutputMoBiLEKernelStatistics_Header[0]))
ldndc_output_size_t const  OutputMoBiLEKernelStatistics_Sizes[] =
{
    OutputMoBiLEKernelStatistics_Datasize,
    OutputMoBiLEKernelStatistics_Datasize /*total size*/
};
ldndc_output_size_t const *  OutputMoBiLEKernelStatistics_EntitySizes = NULL;
#define  OutputMoBiLEKernelStatistics_Rank  ((ldndc_output_rank_t)(sizeof( OutputMoBiLEKernelStatistics_Sizes) / sizeof( OutputMoBiLEKernelStatistics_Sizes[0])) - 1)
atomic_datatype_t const  OutputMoBiLEKernelStatistics_Types[] =
{
    LDNDC_FLOAT64
};


LMOD_OUTPUT_MODULE_NAME::LMOD_OUTPUT_MODULE_NAME(
          MoBiLE_State *  _state,
          cbm::io_kcomm_t *  _io,
          timemode_e  _timemode)
          : LMOD_OUTPUT_MODULE_BASE( _state, _timemode),
            io_kcomm( _io)
{
}


LMOD_OUTPUT_MODULE_NAME::~LMOD_OUTPUT_MODULE_NAME()
{
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::configure(
                ldndc::config_file_t const *)
{
    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::initialize()
{
    lerr_t  rc_ini = LDNDC_ERR_OK;

    this->sink_hdl = io_kcomm->sink_handle_acquire( "kernelstatistics");
    if ( this->sink_hdl.status() == LDNDC_ERR_OK)
    {
        lflags_t  useregs = RM_CLIENTID|RM_DATETIME;
        lerr_t  rc_layout = LDNDC_OUTPUT_DEFINE_SINK_LAYOUT(
                this->sink_hdl,OutputMoBiLEKernelStatistics,useregs);
        rc_ini = rc_layout;
    }
    else
    {
        KLOGERROR( "sink status bad  [sink=","kernelstatistics","]");
        rc_ini = this->sink_hdl.status();
    }
    return  rc_ini;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::solve()
{
    ldndc_flt64_t  data_flt64_0[OutputMoBiLEKernelStatistics_Datasize];
    lerr_t  rc_dump = dump_0( data_flt64_0);
    RETURN_IF_NOT_OK(rc_dump);

    void *  data[] = { data_flt64_0};

    sink_fixed_record_t  rec;
    rec.data = data;
    rec.meta.t.reg.year = lclock()->year();
    rec.meta.t.reg.julianday = lclock()->yearday();
    rec.meta.t.reg.subday = lclock()->subday();

    sink_client_t  client;
    client.target_id = object_id();

    this->sink_hdl.write_fixed_record( &client, &rec);

    return  LDNDC_ERR_OK;
}

lerr_t
LMOD_OUTPUT_MODULE_NAME::finalize()
{
    io_kcomm->sink_handle_release( &this->sink_hdl);
    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::dump_0(
        ldndc_flt64_t *  _buf)
{
    LDNDC_OUTPUT_SET_COLUMN_INDEX(0);

#ifdef  OutputMoBiLEKernelStatistics_StatsAvail
    cbm::resources_statistics_t const *  kstats = this->io_kcomm->kstats;
    if ( kstats)
    {
        static const double  S_IN_NS = 1.0e-09;
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT((double)kstats->cpu_min()*S_IN_NS);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT((double)kstats->cpu_max()*S_IN_NS);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT((double)kstats->cpu_avg()*S_IN_NS);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT((double)kstats->cpu_total()*S_IN_NS);

        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT((double)kstats->wall_min()*S_IN_NS);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT((double)kstats->wall_max()*S_IN_NS);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT((double)kstats->wall_avg()*S_IN_NS);
        LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT((double)kstats->wall_total()*S_IN_NS);
    }
    else
    {
        cbm::mem_set( _buf, OutputMoBiLEKernelStatistics_Datasize, 0.0);
    }
#else
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT((double)this->lclock()->cycles());
#endif

    return  LDNDC_ERR_OK;
}


#undef  LMOD_OUTPUT_MODULE_BASE
#undef  LMOD_OUTPUT_MODULE_NAME

#undef  OutputMoBiLEKernelStatistics_Rank
#undef  OutputMoBiLEKernelStatistics_Datasize
#ifdef  OutputMoBiLEKernelStatistics_StatsAvail
#  undef  OutputMoBiLEKernelStatistics_StatsAvail
#endif

} /*namespace ldndc*/

