/*!
 * @brief
 *    dumping modified soil chemistry related items
 *
 * @author
 *    steffen klatt,
 *    edwin haas
 */

#ifndef  LM_OUTPUT_SOILCHEMISTRY_SUBDAILY_H_NEW_
#define  LM_OUTPUT_SOILCHEMISTRY_SUBDAILY_H_NEW_

#include  "mbe_legacyoutputmodel.h"
#include  "state/mbe_state.h"

#define  LMOD_OUTPUT_MODULE_BASE  MBE_LegacyOutputModel
#define  LMOD_OUTPUT_MODULE_NAME  OutputSoilchemistrySubdaily
#define  LMOD_OUTPUT_MODULE_ID    "output:soilchemistry:subdaily"
#define  LMOD_OUTPUT_MODULE_DESC  "Soil Chemistry Subdaily Output"
namespace ldndc {
class  substate_soilchemistry_t;
class  LDNDC_API  LMOD_OUTPUT_MODULE_NAME  :  public  LMOD_OUTPUT_MODULE_BASE
{
    LMOD_EXPORT_MODULE_INFO(LMOD_OUTPUT_MODULE_NAME,LMOD_OUTPUT_MODULE_ID,LMOD_OUTPUT_MODULE_DESC);
    public:
        LMOD_OUTPUT_MODULE_NAME(
                MoBiLE_State *,
                cbm::io_kcomm_t *,
                timemode_e  _timemode);

        ~LMOD_OUTPUT_MODULE_NAME();


        lerr_t  configure( ldndc::config_file_t const *);

        lerr_t  initialize();

        lerr_t  solve();

        lerr_t  finalize();

        lerr_t  wake() { return  LDNDC_ERR_OK; }
        lerr_t  sleep() { return  LDNDC_ERR_OK; }

    private:
        lerr_t  dump_0(
                ldndc_flt64_t *);

    private:
        cbm::io_kcomm_t *  io_kcomm;
        substate_soilchemistry_t const *  soilchem;
    private:
        ldndc::sink_handle_t  m_sink;

        struct  output_soilchemistry_subdaily_acc_t
        {
            double  doc_leach;
            double  don_leach;
            double  no3_leach;
            double  nh4_leach;

            double  co2_emis;
            double  co2_emis_auto;
            double  co2_emis_hetero;

            double  no_emis;
            double  n2o_emis;
            double  n2_emis;
            double  nh3_emis;
        };
        output_soilchemistry_subdaily_acc_t  acc;
        void  pop_accumulated_outputs();
        void  push_accumulated_outputs();
};
} /*namespace ldndc*/


#undef  LMOD_OUTPUT_MODULE_BASE
#undef  LMOD_OUTPUT_MODULE_NAME
#undef  LMOD_OUTPUT_MODULE_ID
#undef  LMOD_OUTPUT_MODULE_DESC


#endif  /*  !LM_OUTPUT_SOILCHEMISTRY_SUBDAILY_H_NEW_  */

