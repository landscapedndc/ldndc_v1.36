/*!
 * @brief
 *    dumping modified soil chemistry related items
 *
 * @author
 *    steffen klatt,
 *    edwin haas
 */

#ifndef  LM_OUTPUT_SOILCHEMISTRY_LAYER_DAILY_H_
#define  LM_OUTPUT_SOILCHEMISTRY_LAYER_DAILY_H_

#include  "mbe_legacyoutputmodel.h"
#include  "state/mbe_state.h"

#define  LMOD_OUTPUT_MODULE_BASE  MBE_LegacyOutputModel
#define  LMOD_OUTPUT_MODULE_NAME  OutputSoilchemistryLayerDaily
#define  LMOD_OUTPUT_MODULE_ID    "output:soilchemistry-layer:daily"
#define  LMOD_OUTPUT_MODULE_DESC  "Soil Chemistry Daily Output (Layers)"
namespace ldndc {
class  substate_microclimate_t;
class  substate_physiology_t;
class  substate_watercycle_t;
class  LDNDC_API  LMOD_OUTPUT_MODULE_NAME  :  public  LMOD_OUTPUT_MODULE_BASE
{
    LMOD_EXPORT_MODULE_INFO(LMOD_OUTPUT_MODULE_NAME,LMOD_OUTPUT_MODULE_ID,LMOD_OUTPUT_MODULE_DESC);
    public:
        LMOD_OUTPUT_MODULE_NAME(
                MoBiLE_State *,
                cbm::io_kcomm_t *,
                timemode_e  _timemode);

        ~LMOD_OUTPUT_MODULE_NAME();


        lerr_t  configure( ldndc::config_file_t const *);

        lerr_t  initialize();

        lerr_t  solve();

        lerr_t  finalize();

        lerr_t  wake() { return  LDNDC_ERR_OK; }
        lerr_t  sleep() { return  LDNDC_ERR_OK; }

    private:
        lerr_t  dump_0( ldndc_flt64_t * /*buffer*/,
            int  /*layer index*/);

    private:
        cbm::io_kcomm_t *  io_kcomm;

        /* configuration options: */
        /*    foliage, surface and soil layer selection */
        struct  conf_t
        {
            bool  soil_all:1;
        };
        struct conf_t  rc;

        input_class_siteparameters_t const *  siteparam_;
        input_class_soillayers_t const *  soillayers_;

        substate_soilchemistry_t const *  sc;

    private:
        ldndc::sink_handle_t  m_sink;

    struct  output_soilchemistry_layer_daily_acc_t
    {
        double * dN_mineral_sl;
        double * dN_nitrify_sl;
        double * dN_denitrify_sl;
    };
    output_soilchemistry_layer_daily_acc_t  acc;
    void  pop_accumulated_outputs( int /* soil layer */);
    void  push_accumulated_outputs( int /* soil layer */);
};
} /*namespace ldndc*/


#undef  LMOD_OUTPUT_MODULE_BASE
#undef  LMOD_OUTPUT_MODULE_NAME
#undef  LMOD_OUTPUT_MODULE_ID
#undef  LMOD_OUTPUT_MODULE_DESC


#endif  /*  !LM_OUTPUT_SOILCHEMISTRY_LAYER_DAILY_H_  */

