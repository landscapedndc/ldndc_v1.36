/*!
 * @file
 * @author
 *  - Edwin Haas
 *  - Ruediger Grote
 *  - David Kraus
 */

#include  "output/soilchemistry/output-soilchemistry-yearly.h"

#include  "constants/lconstants-plant.h"
#include  "constants/lconstants-conv.h"

#define  LMOD_OUTPUT_MODULE_BASE  MBE_LegacyOutputModel
#define  LMOD_OUTPUT_MODULE_NAME  OutputSoilchemistryYearly

LMOD_MODULE_INFO(LMOD_OUTPUT_MODULE_NAME,TMODE_POST_YEARLY,LMOD_FLAG_USER|LMOD_FLAG_OUTPUT);
namespace ldndc {


/*!
 * @page soilchemistryoutput
 * @section soilchemistryoutputyearly Soilchemistry output (yearly)
 *
 * entity name | decription | unit
 * ----------- | ---------- | ----
 * aC\_ch4\_emis | Annual methane emissions |  [kgCha-1]
 * aC\_co2\_emis\_auto | Annual autotrophic (fine roots) respiration |  [kgCha-1]
 * aC\_co2\_emis\_hetero | Annual heterotrophic soil respiration |  [kgCha-1]
 * aC\_sol\_leach | Annual dissolved organic carbon leaching |  [kgCha-1]
 * aN\_no\_emis | Annual nitric oxide emissions |  [kgNha-1]
 * aN\_n2o\_emis | Annual nitroux oxide emissions |  [kgNha-1]
 * aN\_n2\_emis | Annual molecular nitrogen emissions |  [kgNha-1]
 * aN\_nh3\_emis | Annual ammonia emissions |  [kgNha-1]
 * aN\_no3\_leach | Annual nitrate leaching |  [kgNha-1]
 * aN\_nh4\_leach | Annual ammonium leaching |  [kgNha-1]
 * aN\_don\_leach | Annual dissolved organic nitrogen leaching |  [kgNha-1]
 * aC\_litter\_above | Annual carbon in leave-litter fall |  [kgCha-1]
 * aC\_litter\_below | Annual rhizodeposited carbon (root exudates and root decay) |  [kgCha-1]
 * aC\_fertilize | Annual carbon input via fertilization/manuring |  [kgCha-1]
 * aN\_litter\_above | Annual nitrogen (from the litter) |  [kgNha-1]
 * aN\_litter\_below | Annual rhizodeposited nitrogen (from the litter) |  [kgNha-1]
 * aN\_fertilize | Annual nitrogen input via fertilization/manuring |  [kgNha-1]
 * aN\_dep\_no3 | Annual nitrate deposition |  [kgNha-1]
 * aN\_dep\_nh4 | Annual amonium deposition |  [kgNha-1]
 * aN\_up | Annual nitrogen uptake by plants |  [kgNha-1]
 * aN\_mineral\_floor | Annual nitrogen mineralization in the litter layer |  [kgNha-1]
 * aN\_mineral\_min | Annual nitrogen mineralization in the mineral soil |  [kgNha-1]
 * aN\_nitrify\_floor | Annual nitrification in the litter layer |  [kgNha-1]
 * aN\_nitrify\_min | Annual nitrification in the mineral soil |  [kgNha-1]
 * aN\_chemo\_floor | Annual chemodenitrification in the litter layer |  [kgNha-1]
 * aN\_chemo\_min | Annual chemodenitrification in the mineral soil |  [kgNha-1]
 * C\_stubble | Carbon stored in stubbles at the end of year |  [kgCha-1]
 * C\_wood\_above | Carbon stored in aboveground wood at the end of year |  [kgCha-1]
 * C\_wood\_below | Carbon stored in belowground wood at the end of year |  [kgCha-1]
 * C\_soil\_total | Total soil organic carbon at the end of year of the complete soil profile. Note, stubble and wood are excluded, microbial and dissolved carbon are included |  [kgCha-1]
 * C\_soil\_floor | Total soil organic carbon at the end of year of the litter layer. Note, if no litter layers have been defined, there is no specific soil layer associated  |  [kgCha-1]
 * C\_soil\_min | Total soil organic carbon at the end of year of the mineral soil. Note, the litter layer is not included |  [kgCha-1]
 * C\_soil\_min\_30cm | Total soil organic carbon at the end of year of the first 30cm of the mineral soil. Note, the litter layer is not included |  [kgCha-1]
 * C\_soil\_min\_20cm | Total soil organic carbon at the end of year of the first 20cm of the mineral soil. Note, the litter layer is not included |  [kgCha-1]
 * C\_mic\_total | Microbial carbon at the end of year of the complete soil profile |  [kgCha-1]
 * C\_sol\_total | Dissolved organic carbon at the end of year of the complete soil profile |  [kgCha-1]
 * N\_stubble | Nitrogen stored in stubbles at the end of year |  [kgNha-1]
 * N\_wood\_above | Nitrogen stored in aboveground wood at the end of year |  [kgNha-1]
 * N\_wood\_below | Nitrogen stored in belowground wood at the end of year |  [kgNha-1]
 * N\_soil\_total | Total soil organic nitrogen at the end of year of the complete soil profile. Note, stubble and wood are excluded, , microbial and dissolved nitrogen are included |  [kgNha-1]
 * N\_soil\_floor | Total soil organic nitrogen at the end of year of the litter layer. Note, if no litter layers have been defined, there is no specific soil layer associated  |  [kgNha-1]
 * N\_soil\_min | Total soil organic nitrogen at the end of year of the mineral soil. Note, the litter layer is not included |  [kgNha-1]
 * N\_soil\_min\_30cm | Total soil organic nitrogen at the end of year of the first 30cm of the mineral soil. Note, the litter layer is not included |  [kgNha-1]
 * N\_soil\_min\_20cm | Total soil organic nitrogen at the end of year of the first 20cm of the mineral soil. Note, the litter layer is not included |  [kgNha-1]
 * N\_mic\_total | Microbial nitrogen at the end of year of the complete soil profile |  [kgNha-1]
 * N\_sol\_total | Dissolved organic nitrogen at the end of year of the complete soil profile |  [kgNha-1]
 * aC\_change | Annual change of all nitrogen pools |  [kgCha-1]
 * aN\_change | Annual change of all nitrogen pools |  [kgNha-1]
 */
ldndc_string_t const  OutputSoilchemistryYearly_Ids[] =
{
    "aC_ch4_emis",
    "aC_co2_emis_auto",
    "aC_co2_emis_hetero",
    "aC_doc_leach",
    "aC_litter_above",
    "aC_litter_below",
    "aC_fertilize",
    "aC_fixation",
    "aN_no_emis",
    "aN_n2o_emis",
    "aN_n2_emis",
    "aN_nh3_emis",
    "aN_no3_leach",
    "aN_nh4_leach",
    "aN_don_leach",
    "aN_uptake",
    "aN_litter_above",
    "aN_litter_below",
    "aN_fertilize",
    "aN_fixation",
    "aN_dep_no3",
    "aN_dep_nh4",
    "aN_mineral_floor",
    "aN_mineral_min",
    "aN_nitrify_floor",
    "aN_nitrify_min",
    "aN_chemo_floor",
    "aN_chemo_min",
    "C_stubble",
    "C_wood_above",
    "C_wood_below",
    "C_soil_total",
    "C_soil_floor",
    "C_soil_min",
    "C_soil_min_30cm",
    "C_soil_min_20cm",
    "C_mic_total",
    "C_sol_total",
    "N_stubble",
    "N_wood_above",
    "N_wood_below",
    "N_soil_total",
    "N_soil_floor",
    "N_soil_min",
    "N_soil_min_30cm",
    "N_soil_min_20cm",
    "N_mic_total",
    "N_sol_total",
    "aC_change",
    "aN_change"
};
ldndc_string_t const  OutputSoilchemistryYearly_Header[] =
{
    "aC_ch4_emis[kgCha-1]",
    "aC_co2_emis_auto[kgCha-1]",
    "aC_co2_emis_hetero[kgCha-1]",
    "aC_doc_leach[kgCha-1]",
    "aC_litter_above[kgCha-1]",
    "aC_litter_below[kgCha-1]",
    "aC_fertilize[kgCha-1]",
    "aC_fixation[kgCha-1]",
    "aN_no_emis[kgNha-1]",
    "aN_n2o_emis[kgNha-1]",
    "aN_n2_emis[kgNha-1]",
    "aN_nh3_emis[kgNha-1]",
    "aN_no3_leach[kgNha-1]",
    "aN_nh4_leach[kgNha-1]",
    "aN_don_leach[kgNha-1]",
    "aN_uptake[kgNha-1]",
    "aN_litter_above[kgNha-1]",
    "aN_litter_below[kgNha-1]",
    "aN_fertilize[kgNha-1]",
    "aN_fixation[kgNha-1]",
    "aN_dep_no3[kgNha-1]",
    "aN_dep_nh4[kgNha-1]",
    "aN_mineral_floor[kgNha-1]",
    "aN_mineral_min[kgNha-1]",
    "aN_nitrify_floor[kgNha-1]",
    "aN_nitrify_min[kgNha-1]",
    "aN_chemo_floor[kgNha-1]",
    "aN_chemo_min[kgNha-1]",
    "C_stubble[kgCha-1]",
    "C_wood_above[kgCha-1]",
    "C_wood_below[kgCha-1]",
    "C_soil_total[kgCha-1]",
    "C_soil_floor[kgCha-1]",
    "C_soil_min[kgCha-1]",
    "C_soil_min_30cm[kgCha-1]",
    "C_soil_min_20cm[kgCha-1]",
    "C_mic_total[kgCha-1]",
    "C_sol_total[kgCha-1]",
    "N_stubble[kgNha-1]",
    "N_wood_above[kgNha-1]",
    "N_wood_below[kgNha-1]",
    "N_soil_total[kgNha-1]",
    "N_soil_floor[kgNha-1]",
    "N_soil_min[kgNha-1]",
    "N_soil_min_30cm[kgNha-1]",
    "N_soil_min_20cm[kgNha-1]",
    "N_mic_total[kgNha-1]",
    "N_sol_total[kgNha-1]",
    "aC_change[kgCha-1]",
    "aN_change[kgNha-1]"
};

#define  OutputSoilchemistryYearly_Datasize  (sizeof( OutputSoilchemistryYearly_Header) / sizeof( OutputSoilchemistryYearly_Header[0]))
ldndc_output_size_t const  OutputSoilchemistryYearly_Sizes[] =
{
    OutputSoilchemistryYearly_Datasize,

    OutputSoilchemistryYearly_Datasize /*total size*/
};

ldndc_output_size_t const *  OutputSoilchemistryYearly_EntitySizes = NULL;
#define  OutputSoilchemistryYearly_Rank  ((ldndc_output_rank_t)(sizeof( OutputSoilchemistryYearly_Sizes) / sizeof( OutputSoilchemistryYearly_Sizes[0])) - 1)
atomic_datatype_t const  OutputSoilchemistryYearly_Types[] =
{
    LDNDC_FLOAT64
};


LMOD_OUTPUT_MODULE_NAME::LMOD_OUTPUT_MODULE_NAME(
        MoBiLE_State *  _state,
        cbm::io_kcomm_t *  _io_kcomm,
        timemode_e  _timemode)
        : LMOD_OUTPUT_MODULE_BASE( _state, _io_kcomm, _timemode),
          io_kcomm( _io_kcomm),
          soillayers_in( _io_kcomm->get_input_class< input_class_soillayers_t >()),
          phys( _state->get_substate< substate_physiology_t >()),
          soilchem( _state->get_substate< substate_soilchemistry_t >())
{
}


LMOD_OUTPUT_MODULE_NAME::~LMOD_OUTPUT_MODULE_NAME()
{
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::configure(
          ldndc::config_file_t const *  _cf)
{
    lerr_t  rc_setflags = set_metaflags( _cf, RM_DEFAULT_YEARLY);
    if ( rc_setflags)
    { return  LDNDC_ERR_FAIL; }

    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::initialize()
{
    m_sink = io_kcomm->sink_handle_acquire( "soilchemistryyearly");
    if ( m_sink.status() == LDNDC_ERR_OK)
    {
        lerr_t  rc_layout = LDNDC_OUTPUT_DEFINE_SINK_LAYOUT_DEFAULTS( m_sink,OutputSoilchemistryYearly);
        RETURN_IF_NOT_OK(rc_layout);
    }
    else
    {
        KLOGERROR( "sink status bad  [sink=","soilchemistryyearly","]");
        return  m_sink.status();
    }

    push_accumulated_outputs();

    c_soil_old = 0.0;
    n_soil_old = 0.0;
    for ( size_t  sl = 0;  sl < soillayers_in->soil_layer_cnt();  ++sl)
    {
        c_soil_old += soilchem->C_lit1_sl[sl] + soilchem->C_lit2_sl[sl] + soilchem->C_lit3_sl[sl]
                       + soilchem->c_raw_lit_1_sl[sl] + soilchem->c_raw_lit_2_sl[sl] + soilchem->c_raw_lit_3_sl[sl]
                       + soilchem->C_aorg_sl[sl] + soilchem->C_hum_sl[sl];
        n_soil_old += soilchem->N_lit1_sl[sl] + soilchem->N_lit2_sl[sl] + soilchem->N_lit3_sl[sl]
                       + soilchem->n_raw_lit_1_sl[sl] + soilchem->n_raw_lit_2_sl[sl] + soilchem->n_raw_lit_3_sl[sl]
                       + soilchem->N_aorg_sl[sl] + soilchem->N_hum_sl[sl];

        c_soil_old += soilchem->doc_sl[sl] + soilchem->an_doc_sl[sl];
        n_soil_old += soilchem->nh3_liq_sl[sl] + soilchem->nh3_gas_sl[sl]
                          + soilchem->nh4_sl[sl] + soilchem->no3_sl[sl] + soilchem->no2_sl[sl]
                          + soilchem->no_sl[sl] + soilchem->n2o_sl[sl] + soilchem->don_sl[sl]
                          + soilchem->an_no3_sl[sl] + soilchem->an_no2_sl[sl]
                          + soilchem->an_no_sl[sl] + soilchem->an_n2o_sl[sl] + soilchem->clay_nh4_sl[sl];

        c_soil_old += soilchem->C_micro1_sl[sl] + soilchem->C_micro2_sl[sl] + soilchem->C_micro3_sl[sl];
        n_soil_old += soilchem->N_micro_sl[sl];
    }

    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::solve()
{
    ldndc_flt64_t  data_flt64_0[OutputSoilchemistryYearly_Datasize];

    pop_accumulated_outputs();
    lerr_t  rc_dump = dump_0( data_flt64_0);
    push_accumulated_outputs();

    if ( rc_dump)
    { return  LDNDC_ERR_FAIL; }

    void *  data[] = { data_flt64_0};
    write_fixed_record( &m_sink, data);

    return  LDNDC_ERR_OK;
}

void
LMOD_OUTPUT_MODULE_NAME::pop_accumulated_outputs()
{
    acc.c_fix_algae = soilchem->accumulated_c_fix_algae - acc.c_fix_algae;
    acc.n_fix_algae = soilchem->accumulated_n_fix_algae - acc.n_fix_algae;
    acc.n_fix_bio = soilchem->accumulated_n2_fixation - acc.n_fix_bio;

    acc.c_fertilizer = soilchem->accumulated_c_fertilizer - acc.c_fertilizer;
    acc.n_fertilizer = soilchem->accumulated_n_fertilizer - acc.n_fertilizer;

    acc.nh4_throughfall = phys->accumulated_nh4_throughfall - acc.nh4_throughfall;
    acc.no3_throughfall = phys->accumulated_no3_throughfall - acc.no3_throughfall;
    acc.n_uptake = (phys->accumulated_no3_uptake_sl.sum() +
                    phys->accumulated_nh4_uptake_sl.sum() +
                    phys->accumulated_don_uptake_sl.sum()) - acc.n_uptake;

    acc.doc_leach = soilchem->accumulated_doc_leach - acc.doc_leach;
    acc.ch4_leach = soilchem->accumulated_ch4_leach - acc.ch4_leach;

    acc.don_leach = soilchem->accumulated_don_leach - acc.don_leach;
    acc.no3_leach = soilchem->accumulated_no3_leach - acc.no3_leach;
    acc.nh4_leach = soilchem->accumulated_nh4_leach - acc.nh4_leach;

    acc.ch4_emis = soilchem->accumulated_ch4_emis - acc.ch4_emis;
    acc.co2_emis_auto = soilchem->accumulated_co2_emis_auto - acc.co2_emis_auto;
    acc.co2_emis_hetero = soilchem->accumulated_co2_emis_hetero - acc.co2_emis_hetero;
    acc.no_emis = soilchem->accumulated_no_emis - acc.no_emis;
    acc.n2o_emis = soilchem->accumulated_n2o_emis - acc.n2o_emis;
    acc.n2_emis = soilchem->accumulated_n2_emis - acc.n2_emis;
    acc.nh3_emis = soilchem->accumulated_nh3_emis - acc.nh3_emis;

    acc.c_litter_above = soilchem->accumulated_c_litter_above + soilchem->accumulated_c_litter_stubble + soilchem->accumulated_c_litter_wood_above - acc.c_litter_above;
    acc.n_litter_above = soilchem->accumulated_n_litter_above + soilchem->accumulated_n_litter_stubble + soilchem->accumulated_n_litter_wood_above - acc.n_litter_above;
    acc.c_litter_below = soilchem->accumulated_c_litter_wood_below_sl.sum() + soilchem->accumulated_c_litter_below_sl.sum() + soilchem->accumulated_c_root_exsudates_sl.sum() - acc.c_litter_below;
    acc.n_litter_below = soilchem->accumulated_n_litter_wood_below_sl.sum() + soilchem->accumulated_n_litter_below_sl.sum() - acc.n_litter_below;

    acc.MINfloor = -acc.MINfloor;
    acc.NITfloor = -acc.NITfloor;
    acc.CHEfloor = -acc.CHEfloor;

    acc.MINmin = -acc.MINmin;
    acc.NITmin = -acc.NITmin;
    acc.CHEmin = -acc.CHEmin;

    for ( size_t  l = 0;  l < soillayers_in->soil_layers_in_litter_cnt();  ++l)
    {
        acc.MINfloor += soilchem->accumulated_n_mineral_sl[l];
        acc.NITfloor += soilchem->accumulated_n_nitrify_sl[l];
        acc.CHEfloor += soilchem->accumulated_n_chemodenitrify_sl[l];
    }
    for ( size_t  l = soillayers_in->soil_layers_in_litter_cnt(); l < soillayers_in->soil_layer_cnt();  ++l)
    {
        acc.MINmin   += soilchem->accumulated_n_mineral_sl[l];
        acc.NITmin   += soilchem->accumulated_n_nitrify_sl[l];
        acc.CHEmin   += soilchem->accumulated_n_chemodenitrify_sl[l];
    }
}



void
LMOD_OUTPUT_MODULE_NAME::push_accumulated_outputs()
{
    acc.c_fix_algae = soilchem->accumulated_c_fix_algae;
    acc.n_fix_algae = soilchem->accumulated_n_fix_algae;
    acc.n_fix_bio = soilchem->accumulated_n2_fixation;

    acc.c_fertilizer = soilchem->accumulated_c_fertilizer;
    acc.n_fertilizer = soilchem->accumulated_n_fertilizer;

    acc.nh4_throughfall = phys->accumulated_nh4_throughfall;
    acc.no3_throughfall = phys->accumulated_no3_throughfall;
    acc.n_uptake = (phys->accumulated_no3_uptake_sl.sum() +
                    phys->accumulated_nh4_uptake_sl.sum() +
                    phys->accumulated_don_uptake_sl.sum());

    acc.doc_leach = soilchem->accumulated_doc_leach;
    acc.ch4_leach = soilchem->accumulated_ch4_leach;

    acc.don_leach = soilchem->accumulated_don_leach;
    acc.no3_leach = soilchem->accumulated_no3_leach;
    acc.nh4_leach = soilchem->accumulated_nh4_leach;

    acc.ch4_emis = soilchem->accumulated_ch4_emis;
    acc.co2_emis_auto = soilchem->accumulated_co2_emis_auto;
    acc.co2_emis_hetero = soilchem->accumulated_co2_emis_hetero;

    acc.no_emis = soilchem->accumulated_no_emis;
    acc.n2o_emis = soilchem->accumulated_n2o_emis;
    acc.n2_emis = soilchem->accumulated_n2_emis;
    acc.nh3_emis = soilchem->accumulated_nh3_emis;

    acc.c_litter_above = soilchem->accumulated_c_litter_above + soilchem->accumulated_c_litter_stubble + soilchem->accumulated_c_litter_wood_above;
    acc.n_litter_above = soilchem->accumulated_n_litter_above + soilchem->accumulated_n_litter_stubble + soilchem->accumulated_n_litter_wood_above;

    acc.c_litter_below = soilchem->accumulated_c_litter_wood_below_sl.sum() + soilchem->accumulated_c_litter_below_sl.sum() + soilchem->accumulated_c_root_exsudates_sl.sum();
    acc.n_litter_below = soilchem->accumulated_n_litter_wood_below_sl.sum() + soilchem->accumulated_n_litter_below_sl.sum();

    acc.MINfloor = 0.0;
    acc.NITfloor = 0.0;
    acc.CHEfloor = 0.0;

    acc.MINmin = 0.0;
    acc.NITmin = 0.0;
    acc.CHEmin = 0.0;

    for ( size_t  l = 0;  l < soillayers_in->soil_layers_in_litter_cnt();  ++l)
    {
        acc.MINfloor += soilchem->accumulated_n_mineral_sl[l];
        acc.NITfloor += soilchem->accumulated_n_nitrify_sl[l];
        acc.CHEfloor += soilchem->accumulated_n_chemodenitrify_sl[l];
    }
    for ( size_t  l = soillayers_in->soil_layers_in_litter_cnt(); l < soillayers_in->soil_layer_cnt();  ++l)
    {
        acc.MINmin += soilchem->accumulated_n_mineral_sl[l];
        acc.NITmin += soilchem->accumulated_n_nitrify_sl[l];
        acc.CHEmin += soilchem->accumulated_n_chemodenitrify_sl[l];
    }
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::finalize()
{
    io_kcomm->sink_handle_release( &m_sink);
    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::dump_0(
        ldndc_flt64_t *  _buf)
{
    double const c_stubble( soilchem->c_stubble_lit1 + soilchem->c_stubble_lit2 + soilchem->c_stubble_lit3);
    double const n_stubble( soilchem->n_stubble_lit1 + soilchem->n_stubble_lit2 + soilchem->n_stubble_lit3);

    double c_wood_above( soilchem->c_wood);
    double n_wood_above( soilchem->n_wood);
    
    double c_wood_below( 0.0);
    double n_wood_below( 0.0);

    double  c_soil_floor( 0.0);
    double  n_soil_floor( 0.0);

    double  c_soil_min( 0.0);
    double  c_soil_min30( 0.0);
    double  c_soil_min20( 0.0);
    double  n_soil_min( 0.0);
    double  n_soil_min30( 0.0);
    double  n_soil_min20( 0.0);
    
    double  c_solutes_floor( 0.0);
    double  n_solutes_floor( 0.0);
    
    double  c_solutes_min( 0.0);
    double  c_solutes_min30( 0.0);
    double  c_solutes_min20( 0.0);
    double  n_solutes_min( 0.0);
    double  n_solutes_min30( 0.0);
    double  n_solutes_min20( 0.0);
    
    double  c_microbes_floor( 0.0);
    double  n_microbes_floor( 0.0);

    double  c_microbes_min( 0.0);
    double  c_microbes_min30( 0.0);
    double  c_microbes_min20( 0.0);
    double  n_microbes_min( 0.0);
    double  n_microbes_min30( 0.0);
    double  n_microbes_min20( 0.0);
    
    for ( size_t  sl = 0;  sl < soillayers_in->soil_layer_cnt();  ++sl)
    {
        c_wood_below += soilchem->c_wood_sl[sl];
        n_wood_below += soilchem->n_wood_sl[sl];

        if ( sl < soillayers_in->soil_layers_in_litter_cnt())
        {
            c_soil_floor += (soilchem->C_lit1_sl[sl] + soilchem->C_lit2_sl[sl] + soilchem->C_lit3_sl[sl]
                             + soilchem->c_raw_lit_1_sl[sl] + soilchem->c_raw_lit_2_sl[sl] + soilchem->c_raw_lit_3_sl[sl]
                             + soilchem->C_aorg_sl[sl] + soilchem->C_hum_sl[sl]);
            n_soil_floor += (soilchem->N_lit1_sl[sl]+soilchem->N_lit2_sl[sl]+soilchem->N_lit3_sl[sl]
                             + soilchem->n_raw_lit_1_sl[sl] + soilchem->n_raw_lit_2_sl[sl] + soilchem->n_raw_lit_3_sl[sl]
                             + soilchem->N_aorg_sl[sl] + soilchem->N_hum_sl[sl]);
            
            c_solutes_floor += (soilchem->doc_sl[sl] + soilchem->an_doc_sl[sl]);
            n_solutes_floor += (soilchem->nh3_liq_sl[sl] + soilchem->nh3_gas_sl[sl]
                                + soilchem->nh4_sl[sl] + soilchem->no3_sl[sl] + soilchem->no2_sl[sl]
                                + soilchem->no_sl[sl] + soilchem->n2o_sl[sl] + soilchem->don_sl[sl]
                                + soilchem->an_no3_sl[sl] + soilchem->an_no2_sl[sl]
                                + soilchem->an_no_sl[sl] + soilchem->an_n2o_sl[sl] + soilchem->clay_nh4_sl[sl]);
            
            c_microbes_floor += soilchem->C_micro1_sl[sl] + soilchem->C_micro2_sl[sl] + soilchem->C_micro3_sl[sl];
            n_microbes_floor += soilchem->N_micro_sl[sl];
        }
        else
        {
            if ( cbm::flt_less_equal( soilchem->depth_sl[sl], 0.3))
            {
                c_soil_min30 += (soilchem->C_lit1_sl[sl] + soilchem->C_lit2_sl[sl] + soilchem->C_lit3_sl[sl]
                                 + soilchem->c_raw_lit_1_sl[sl] + soilchem->c_raw_lit_2_sl[sl] + soilchem->c_raw_lit_3_sl[sl]
                                 + soilchem->C_aorg_sl[sl] + soilchem->C_hum_sl[sl]);
                n_soil_min30 += (soilchem->N_lit1_sl[sl]+soilchem->N_lit2_sl[sl]+soilchem->N_lit3_sl[sl]
                                 + soilchem->n_raw_lit_1_sl[sl] + soilchem->n_raw_lit_2_sl[sl] + soilchem->n_raw_lit_3_sl[sl]
                                 + soilchem->N_aorg_sl[sl] + soilchem->N_hum_sl[sl]);
                
                c_solutes_min30 += (soilchem->doc_sl[sl] + soilchem->an_doc_sl[sl]);
                n_solutes_min30 += (soilchem->nh3_liq_sl[sl] + soilchem->nh3_gas_sl[sl]
                                    + soilchem->nh4_sl[sl] + soilchem->no3_sl[sl] + soilchem->no2_sl[sl]
                                    + soilchem->no_sl[sl] + soilchem->n2o_sl[sl] + soilchem->don_sl[sl]
                                    + soilchem->an_no3_sl[sl] + soilchem->an_no2_sl[sl]
                                    + soilchem->an_no_sl[sl] + soilchem->an_n2o_sl[sl] + soilchem->clay_nh4_sl[sl]);
                
                c_microbes_min30 += soilchem->C_micro1_sl[sl] + soilchem->C_micro2_sl[sl] + soilchem->C_micro3_sl[sl];
                n_microbes_min30 += soilchem->N_micro_sl[sl];
            }
            if ( cbm::flt_less_equal( soilchem->depth_sl[sl], 0.2))
            {
                c_soil_min20 += (soilchem->C_lit1_sl[sl] + soilchem->C_lit2_sl[sl] + soilchem->C_lit3_sl[sl]
                                 + soilchem->c_raw_lit_1_sl[sl] + soilchem->c_raw_lit_2_sl[sl] + soilchem->c_raw_lit_3_sl[sl]
                                 + soilchem->C_aorg_sl[sl] + soilchem->C_hum_sl[sl]);
                n_soil_min20 += (soilchem->N_lit1_sl[sl]+soilchem->N_lit2_sl[sl]+soilchem->N_lit3_sl[sl]
                                 + soilchem->n_raw_lit_1_sl[sl] + soilchem->n_raw_lit_2_sl[sl] + soilchem->n_raw_lit_3_sl[sl]
                                 + soilchem->N_aorg_sl[sl] + soilchem->N_hum_sl[sl]);
                c_solutes_min20 += (soilchem->doc_sl[sl] + soilchem->an_doc_sl[sl]);
                n_solutes_min20 += (soilchem->nh3_liq_sl[sl] + soilchem->nh3_gas_sl[sl]
                                    + soilchem->nh4_sl[sl] + soilchem->no3_sl[sl] + soilchem->no2_sl[sl]
                                    + soilchem->no_sl[sl] + soilchem->n2o_sl[sl] + soilchem->don_sl[sl]
                                    + soilchem->an_no3_sl[sl] + soilchem->an_no2_sl[sl]
                                    + soilchem->an_no_sl[sl] + soilchem->an_n2o_sl[sl] + soilchem->clay_nh4_sl[sl]);
                
                c_microbes_min20 += soilchem->C_micro1_sl[sl] + soilchem->C_micro2_sl[sl] + soilchem->C_micro3_sl[sl];
                n_microbes_min20 += soilchem->N_micro_sl[sl];
                
            }
            c_soil_min += (  soilchem->C_lit1_sl[sl] + soilchem->C_lit2_sl[sl] + soilchem->C_lit3_sl[sl]
                           + soilchem->c_raw_lit_1_sl[sl] + soilchem->c_raw_lit_2_sl[sl] + soilchem->c_raw_lit_3_sl[sl]
                           + soilchem->C_aorg_sl[sl] + soilchem->C_hum_sl[sl]);
            n_soil_min += (soilchem->N_lit1_sl[sl]+soilchem->N_lit2_sl[sl]+soilchem->N_lit3_sl[sl]
                           + soilchem->n_raw_lit_1_sl[sl] + soilchem->n_raw_lit_2_sl[sl] + soilchem->n_raw_lit_3_sl[sl]
                           + soilchem->N_aorg_sl[sl] + soilchem->N_hum_sl[sl]);
            
            c_solutes_min += (soilchem->doc_sl[sl] + soilchem->an_doc_sl[sl]);
            n_solutes_min += (soilchem->nh3_liq_sl[sl] + soilchem->nh3_gas_sl[sl]
                              + soilchem->nh4_sl[sl] + soilchem->no3_sl[sl] + soilchem->no2_sl[sl]
                              + soilchem->no_sl[sl] + soilchem->n2o_sl[sl] + soilchem->don_sl[sl]
                              + soilchem->an_no3_sl[sl] + soilchem->an_no2_sl[sl]
                              + soilchem->an_no_sl[sl] + soilchem->an_n2o_sl[sl] + soilchem->clay_nh4_sl[sl]);
            
            c_microbes_min += soilchem->C_micro1_sl[sl] + soilchem->C_micro2_sl[sl] + soilchem->C_micro3_sl[sl];
            n_microbes_min += soilchem->N_micro_sl[sl];
        }
    }

    double const  c_poolchange(  c_microbes_floor + c_microbes_min
                               + c_solutes_floor + c_solutes_min
                               + c_soil_floor + c_soil_min - c_soil_old);
    double const  n_poolchange(  n_microbes_floor + n_microbes_min
                               + n_solutes_floor + n_solutes_min
                               + n_soil_floor + n_soil_min - n_soil_old);
    c_soil_old = c_microbes_floor + c_microbes_min
                + c_solutes_floor + c_solutes_min
                + c_soil_floor + c_soil_min;
    n_soil_old = n_microbes_floor + n_microbes_min
                + n_solutes_floor + n_solutes_min
                + n_soil_floor + n_soil_min;

    LDNDC_OUTPUT_SET_COLUMN_INDEX(0);

    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( acc.ch4_emis * cbm::M2_IN_HA);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( acc.co2_emis_auto * cbm::M2_IN_HA);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( acc.co2_emis_hetero * cbm::M2_IN_HA);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( acc.doc_leach * cbm::M2_IN_HA);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( acc.c_litter_above * cbm::M2_IN_HA);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( acc.c_litter_below * cbm::M2_IN_HA);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( acc.c_fertilizer * cbm::M2_IN_HA);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( acc.c_fix_algae * cbm::M2_IN_HA);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( acc.no_emis * cbm::M2_IN_HA);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( acc.n2o_emis * cbm::M2_IN_HA);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( acc.n2_emis * cbm::M2_IN_HA);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( acc.nh3_emis * cbm::M2_IN_HA);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( acc.no3_leach * cbm::M2_IN_HA);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( acc.nh4_leach * cbm::M2_IN_HA);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( acc.don_leach * cbm::M2_IN_HA);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( acc.n_uptake * cbm::M2_IN_HA);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( acc.n_litter_above * cbm::M2_IN_HA);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( acc.n_litter_below * cbm::M2_IN_HA);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( acc.n_fertilizer * cbm::M2_IN_HA);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( (acc.n_fix_algae+acc.n_fix_bio) * cbm::M2_IN_HA);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( acc.no3_throughfall * cbm::M2_IN_HA);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( acc.nh4_throughfall * cbm::M2_IN_HA);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( acc.MINfloor * cbm::M2_IN_HA);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( acc.MINmin * cbm::M2_IN_HA);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( acc.NITfloor * cbm::M2_IN_HA);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( acc.NITmin * cbm::M2_IN_HA);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( acc.CHEfloor * cbm::M2_IN_HA);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( acc.CHEmin * cbm::M2_IN_HA);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( c_stubble * cbm::M2_IN_HA);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( c_wood_above * cbm::M2_IN_HA);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( c_wood_below * cbm::M2_IN_HA);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( (c_soil_floor+c_solutes_floor+c_microbes_floor +
                                            c_soil_min+c_solutes_min+c_microbes_min) * cbm::M2_IN_HA);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( (c_soil_floor+c_solutes_floor+c_microbes_floor) * cbm::M2_IN_HA);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( (c_soil_min+c_solutes_min+c_microbes_min) * cbm::M2_IN_HA);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( (c_soil_min30+c_solutes_min30+c_microbes_min30) * cbm::M2_IN_HA);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( (c_soil_min20+c_solutes_min20+c_microbes_min20) * cbm::M2_IN_HA);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( (c_microbes_floor+c_microbes_min) * cbm::M2_IN_HA);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( (c_solutes_floor+c_solutes_min) * cbm::M2_IN_HA);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( n_stubble * cbm::M2_IN_HA);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( n_wood_above * cbm::M2_IN_HA);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( n_wood_below * cbm::M2_IN_HA);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( (n_soil_floor+n_solutes_floor+n_microbes_floor +
                                            n_soil_min+n_solutes_min+n_microbes_min) * cbm::M2_IN_HA);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( (n_soil_floor+n_solutes_floor+n_microbes_floor) * cbm::M2_IN_HA);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( (n_soil_min+n_solutes_min+n_microbes_min) * cbm::M2_IN_HA);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( (n_soil_min30+n_solutes_min30+n_microbes_min30) * cbm::M2_IN_HA);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( (n_soil_min20+n_solutes_min20+n_microbes_min20) * cbm::M2_IN_HA);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( (n_microbes_floor+n_microbes_min) * cbm::M2_IN_HA);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( (n_solutes_floor+n_solutes_min) * cbm::M2_IN_HA);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( c_poolchange * cbm::M2_IN_HA);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT( n_poolchange * cbm::M2_IN_HA);

    return  LDNDC_ERR_OK;
}


#undef  LMOD_OUTPUT_MODULE_BASE
#undef  LMOD_OUTPUT_MODULE_NAME

#undef  OutputSoilchemistryYearly_Rank
#undef  OutputSoilchemistryYearly_Datasize

} /*namespace ldndc*/

