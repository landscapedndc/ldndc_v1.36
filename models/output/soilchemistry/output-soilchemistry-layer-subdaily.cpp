/*!
 * @file
 * @author
 *  - Edwin Haas
 *  - Ruediger Grote
 *  - David Kraus
 */

#include  "output/soilchemistry/output-soilchemistry-layer-subdaily.h"

#include  "constants/lconstants-conv.h"

#define  LMOD_OUTPUT_MODULE_BASE  MBE_LegacyOutputModel
#define  LMOD_OUTPUT_MODULE_NAME  OutputSoilchemistryLayerSubdaily
#define  LMOD_OUTPUT_MODULE_TIMEMODE  TMODE_SUBDAILY

LMOD_MODULE_INFO(LMOD_OUTPUT_MODULE_NAME,LMOD_OUTPUT_MODULE_TIMEMODE,LMOD_FLAG_USER|LMOD_FLAG_OUTPUT);
namespace ldndc {

/*!
 * @page soilchemistryoutput
 * @section soilchemistryoutputlayersubdaily Soilchemistry output (layer/subdaily)
 *
 * <b>xml-based module selection in project's setup:</b>
 * \n\code{.xml}< module id="output:soilchemistry-layer:subdaily" />\endcode\n
 *
 * entity name | decription | unit
 * ----------- | ---------- | ----
 * layer | Soil layer | [-]
 * height | Soil layer height | [m]
 * extension | extension | [m]
 * C\_doc | Dissolved organic carbon | [kgCha-1]
 * C\_microbes | Carbon of microbial biomass | [kgCha-1]
 * N\_nh4| Ammonium | [kgNha-1]
 * N\_nh3| Ammonia | [kgNha-1]
 * N\_no3| Nitrate | [kgNha-1]
 * N\_no2| Nitrite | [kgNha-1]
 * N\_no| Nitric oxide | [kgNha-1]
 * N\_n2o| Nitroux oxide | [kgNha-1]
 * N\_don| Dissolved organic nitrogen | [kgNha-1]
 * anvf | Anaerobic volume fraction | [-]
 * o2 | Oxygen partial pressure | [bar]
 */
ldndc_string_t const  OutputSoilchemistryLayerSubdaily_Ids[] =
{
    "level",
    "extension",
    "C_doc",
    "C_microbes",
    "N_nh4",
    "N_nh3",
    "N_no3",
    "N_no2",
    "N_no",
    "N_n2o",
    "N_don",
    "anvf",
    "o2"
};

ldndc_string_t const  OutputSoilchemistryLayerSubdaily_Header[] =
{
    "level[m]",
    "extension[m]",
    "C_doc[kgCha-1]",
    "C_microbes[kgCha-1]",
    "N_nh4[kgNha-1]",
    "N_nh3[kgNha-1]",
    "N_no3[kgNha-1]",
    "N_no2[kgNha-1]",
    "N_no[kgNha-1]",
    "N_n2o[kgNha-1]",
    "N_don[kgNha-1]",
    "anvf[-]",
    "o2[bar]"
};
#define  OutputSoilchemistryLayerSubdaily_Datasize  (sizeof( OutputSoilchemistryLayerSubdaily_Header) / sizeof( OutputSoilchemistryLayerSubdaily_Header[0]))
ldndc_output_size_t const  OutputSoilchemistryLayerSubdaily_Sizes[] =
{
    OutputSoilchemistryLayerSubdaily_Datasize,

    OutputSoilchemistryLayerSubdaily_Datasize /*total size*/
};
ldndc_output_size_t const *  OutputSoilchemistryLayerSubdaily_EntitySizes = NULL;
#define  OutputSoilchemistryLayerSubdaily_Rank  ((ldndc_output_rank_t)(sizeof( OutputSoilchemistryLayerSubdaily_Sizes) / sizeof( OutputSoilchemistryLayerSubdaily_Sizes[0])) - 1)
atomic_datatype_t const  OutputSoilchemistryLayerSubdaily_Types[] =
{
    LDNDC_FLOAT64
};


LMOD_OUTPUT_MODULE_NAME::LMOD_OUTPUT_MODULE_NAME(
          MoBiLE_State *  _state,
          cbm::io_kcomm_t *  _io_kcomm,
          timemode_e  _timemode)
          : LMOD_OUTPUT_MODULE_BASE( _state, _io_kcomm, _timemode),

          s_sel_( 0),

          io_kcomm( _io_kcomm),

          soillayers_( _io_kcomm->get_input_class< input_class_soillayers_t >()),
          sc( _state->get_substate< substate_soilchemistry_t >())
{
}


LMOD_OUTPUT_MODULE_NAME::~LMOD_OUTPUT_MODULE_NAME()
{
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::configure(
          ldndc::config_file_t const *  _cf)
{
    lerr_t  rc_setflags =
        this->set_metaflags( _cf, RM_DEFAULT_LAYERSUBDAILY);
    if ( rc_setflags)
        { return  LDNDC_ERR_FAIL; }

    this->s_sel_ = 1; /* layers to write: 1:=all; 2:={first,middle,last(litter,total)}; else=none */

    bool  bool_option = false;
    CF_LMOD_QUERY(_cf, "single_files", bool_option, false);

    cbm::state_scratch_t *  mcom = this->io_kcomm->get_scratch();
    mcom->set( "soilchemistrylayersubdaily.singlefiles", bool_option);

    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::initialize()
{
    cbm::state_scratch_t *  mcom = this->io_kcomm->get_scratch();
    bool  have_single_files = false;
    mcom->get( "soilchemistrylayersubdaily.singlefiles", &have_single_files, false);

    int  sink_index = 0;
    if ( have_single_files)
    {
        sink_index = static_cast< int >( this->object_id());
    }

    this->m_sink = this->io_kcomm->sink_handle_acquire( "soilchemistrylayersubdaily", sink_index);
    if ( this->m_sink.status() == LDNDC_ERR_OK)
    {
        lerr_t  rc_layout = LDNDC_OUTPUT_DEFINE_SINK_LAYOUT_DEFAULTS(
            this->m_sink,OutputSoilchemistryLayerSubdaily);
        RETURN_IF_NOT_OK(rc_layout);
    }
    else
    {
        KLOGERROR( "sink status bad  [sink=","soilchemistrylayersubdaily","]");
        return  this->m_sink.status();
    }

    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::solve()
{
    ldndc_flt64_t  data_flt64_0[OutputSoilchemistryLayerSubdaily_Datasize];

    size_t const  CS = this->soillayers_->soil_layer_cnt();
    size_t const  S = ( CS == 0) ? 0 : ( CS - 1);
    size_t const  CL = this->soillayers_->soil_layers_in_litter_cnt();
    size_t const  L = ( CL == 0) ? 0 : ( CL - 1);
    size_t const  L_MID = ( CS + CL) / 2;

    for ( size_t  l = 0;  l < CS;  ++l)
    {
        if ( this->s_sel_ == 1)
        {
            /* select all layers */
        }
        else if ( this->s_sel_ == 2)
        {
            /* select first, middle and last layer */
            if ( l == 0  ||  (l == L)  || (l == L_MID)  ||  (l == S))
            {
                /* write this layer record */
            }
            else
            {
                /* skip */
                continue;
            }
        }
        else
        {
            /* none */
            break;
        }

        lerr_t  rc_dump = this->dump_0( data_flt64_0, l);
        if ( rc_dump)
            { return LDNDC_ERR_FAIL; }

        void *  data[] = { data_flt64_0};
        this->set_layernumber( -static_cast< int >( l)-1);
        lerr_t  rc_write =
            this->write_fixed_record( &this->m_sink, data);
        if ( rc_write)
            { return  LDNDC_ERR_FAIL; }
    }

    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::finalize()
{
    this->io_kcomm->sink_handle_release( &this->m_sink);
    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::dump_0( ldndc_flt64_t *  _buf, size_t  _l)
{
    ldndc_assert( _buf);
    static const double  p = cbm::M2_IN_HA;

    LDNDC_OUTPUT_SET_COLUMN_INDEX(0);

    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(-this->sc->depth_sl[_l]);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->sc->h_sl[_l]);

    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * (this->sc->doc_sl[_l] + this->sc->an_doc_sl[_l]));
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * (this->sc->C_micro1_sl[_l] + this->sc->C_micro2_sl[_l] + this->sc->C_micro3_sl[_l]));

    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * (this->sc->nh4_sl[_l]+this->sc->clay_nh4_sl[_l]));
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * (this->sc->nh3_liq_sl[_l]+this->sc->nh3_gas_sl[_l]));
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * (this->sc->no3_sl[_l]+this->sc->an_no3_sl[_l]));
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * (this->sc->no2_sl[_l]+this->sc->an_no2_sl[_l]));
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * (this->sc->no_sl[_l]+this->sc->an_no_sl[_l]));
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * (this->sc->n2o_sl[_l]+this->sc->an_n2o_sl[_l]));
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * this->sc->don_sl[_l]);

    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->sc->anvf_sl[_l]);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->sc->o2_sl[_l]);

    return  LDNDC_ERR_OK;
}


#undef  LMOD_OUTPUT_MODULE_BASE
#undef  LMOD_OUTPUT_MODULE_NAME
#undef  LMOD_OUTPUT_MODULE_TIMEMODE

#undef  OutputSoilchemistryLayerSubdaily_Rank
#undef  OutputSoilchemistryLayerSubdaily_Datasize

} /*namespace ldndc*/

