/*!
 * @file
 * @author
 *  - Edwin Haas
 *  - Ruediger Grote
 *  - David Kraus
 */

#include  "output/soilchemistry/output-soilchemistry-subdaily.h"

#define  LMOD_OUTPUT_MODULE_BASE  MBE_LegacyOutputModel
#define  LMOD_OUTPUT_MODULE_NAME  OutputSoilchemistrySubdaily
#define  LMOD_OUTPUT_MODULE_TIMEMODE  TMODE_SUBDAILY

LMOD_MODULE_INFO(LMOD_OUTPUT_MODULE_NAME,LMOD_OUTPUT_MODULE_TIMEMODE,LMOD_FLAG_USER|LMOD_FLAG_OUTPUT);
namespace ldndc {


/*!
 * @page soilchemistryoutput
 * @section soilchemistryoutputsubdaily Soilchemistry output (subdaily)
 *
 * entity name | decription | unit
 * ----------- | ---------- | ----
 * sC_co2_emis | Soil CO2 emissions | [kgCm-2]
 * sC_co2_prod_auto | Autotrophic soil repiration | [kgCm-2]
 * sC_co2_prod_hetero | Heterotrophic soil repiration | [kgCm-2]
 * sC_doc_leach | Leaching of dissolved carbon | [kgCm-2]
 */
ldndc_string_t const  OutputSoilchemistrySubdaily_Ids[] =
{
    "sC_co2_emis",
    "sC_co2_prod_auto",
    "sC_co2_prod_hetero",
    "sC_doc_leach",

    "sN_nh4_leach",
    "sN_nh3_emis",
    "sN_no3_leach",
    "sN_no_emis",
    "sN_n2o_emis",
    "sN_n2_emis",
    "sN_don_leach"
};

ldndc_string_t const  OutputSoilchemistrySubdaily_Header[] =
{
    "sC_co2_emis[kgCm-2]",
    "sC_co2_prod_auto[kgCm-2]",
    "sC_co2_prod_hetero[kgCm-2]",
    "sC_doc[kgCm-2]",

    "sN_nh4_leach[kgNm-2]",
    "sN_nh3_emis[kgNm-2]",
    "sN_no3_leach[kgNm-2]",
    "sN_no_emis[kgNm-2]",
    "sN_n2o_emis[kgNm-2]",
    "sN_n2_emis[kgNm-2]",
    "sN_don_leach[kgNm-2]"
};
#define  OutputSoilchemistrySubdaily_Datasize  (sizeof( OutputSoilchemistrySubdaily_Header) / sizeof( OutputSoilchemistrySubdaily_Header[0]))
ldndc_output_size_t const  OutputSoilchemistrySubdaily_Sizes[] =
{
    OutputSoilchemistrySubdaily_Datasize,

    OutputSoilchemistrySubdaily_Datasize /*total size*/
};
ldndc_output_size_t const *  OutputSoilchemistrySubdaily_EntitySizes = NULL;
#define  OutputSoilchemistrySubdaily_Rank  ((ldndc_output_rank_t)(sizeof( OutputSoilchemistrySubdaily_Sizes) / sizeof( OutputSoilchemistrySubdaily_Sizes[0])) - 1)
atomic_datatype_t const  OutputSoilchemistrySubdaily_Types[] =
{
    LDNDC_FLOAT64
};


LMOD_OUTPUT_MODULE_NAME::LMOD_OUTPUT_MODULE_NAME(
        MoBiLE_State *  _state,
        cbm::io_kcomm_t *  _io_kcomm,
        timemode_e  _timemode)
        : LMOD_OUTPUT_MODULE_BASE( _state, _io_kcomm, _timemode),

        io_kcomm( _io_kcomm),
        soilchem( _state->get_substate< substate_soilchemistry_t >())
{
    this->push_accumulated_outputs();
}


LMOD_OUTPUT_MODULE_NAME::~LMOD_OUTPUT_MODULE_NAME()
{
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::configure(
        ldndc::config_file_t const *  _cf)
{
    lerr_t  rc_setflags =
        this->set_metaflags( _cf, RM_DEFAULT_SUBDAILY);
    if ( rc_setflags)
        { return  LDNDC_ERR_FAIL; }

    bool  bool_option = false;
    CF_LMOD_QUERY(_cf, "single_files", bool_option, false);

    cbm::state_scratch_t *  mcom = this->io_kcomm->get_scratch();
    mcom->set( "soilchemistrysubdaily.singlefiles", bool_option);

    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::initialize()
{
    cbm::state_scratch_t *  mcom = this->io_kcomm->get_scratch();
    bool  have_single_files = false;
    mcom->get( "soilchemistrysubdaily.singlefiles", &have_single_files, false);

    int  sink_index = 0;
    if ( have_single_files)
    {
        sink_index = static_cast< int >( this->object_id());
    }

    this->m_sink = this->io_kcomm->sink_handle_acquire( "soilchemistrysubdaily", sink_index);
    if ( this->m_sink.status() == LDNDC_ERR_OK)
    {
        lerr_t  rc_layout = LDNDC_OUTPUT_DEFINE_SINK_LAYOUT_DEFAULTS(
            this->m_sink,OutputSoilchemistrySubdaily);
        RETURN_IF_NOT_OK(rc_layout);
    }
    else
    {
        KLOGERROR( "sink status bad  [sink=","soilchemistrysubdaily","]");
        return  this->m_sink.status();
    }

    this->push_accumulated_outputs();
    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::solve()
{
    ldndc_flt64_t  data_flt64_0[OutputSoilchemistrySubdaily_Datasize];

    this->pop_accumulated_outputs();
    lerr_t  rc_dump = dump_0( data_flt64_0);
    this->push_accumulated_outputs();

    if ( rc_dump)
        { return  LDNDC_ERR_FAIL; }

    void *  data[] = { data_flt64_0};
    lerr_t  rc_write =
        this->write_fixed_record( &this->m_sink, data);
    if ( rc_write)
        { return  LDNDC_ERR_FAIL; }

    return  LDNDC_ERR_OK;
}

void
LMOD_OUTPUT_MODULE_NAME::pop_accumulated_outputs()
{
    this->acc.doc_leach =
               this->soilchem->accumulated_doc_leach - this->acc.doc_leach;
    this->acc.don_leach =
               this->soilchem->accumulated_don_leach - this->acc.don_leach;
    this->acc.no3_leach =
               this->soilchem->accumulated_no3_leach - this->acc.no3_leach;
    this->acc.nh4_leach =
               this->soilchem->accumulated_nh4_leach - this->acc.nh4_leach;

    this->acc.co2_emis =
        this->soilchem->accumulated_co2_emis - this->acc.co2_emis;
    this->acc.co2_emis_auto =
        this->soilchem->accumulated_co2_emis_auto - this->acc.co2_emis_auto;
    this->acc.co2_emis_hetero =
        this->soilchem->accumulated_co2_emis_hetero - this->acc.co2_emis_hetero;

    this->acc.no_emis =
               this->soilchem->accumulated_no_emis - this->acc.no_emis;
    this->acc.n2o_emis =
               this->soilchem->accumulated_n2o_emis - this->acc.n2o_emis;
    this->acc.n2_emis =
               this->soilchem->accumulated_n2_emis - this->acc.n2_emis;
    this->acc.nh3_emis =
               this->soilchem->accumulated_nh3_emis - this->acc.nh3_emis;
}
void
LMOD_OUTPUT_MODULE_NAME::push_accumulated_outputs()
{
    this->acc.doc_leach = this->soilchem->accumulated_doc_leach;
    this->acc.don_leach = this->soilchem->accumulated_don_leach;
    this->acc.no3_leach = this->soilchem->accumulated_no3_leach;
    this->acc.nh4_leach = this->soilchem->accumulated_nh4_leach;

    this->acc.co2_emis = this->soilchem->accumulated_co2_emis;
    this->acc.co2_emis_auto = this->soilchem->accumulated_co2_emis_auto;
    this->acc.co2_emis_hetero = this->soilchem->accumulated_co2_emis_hetero;

    this->acc.no_emis = this->soilchem->accumulated_no_emis;
    this->acc.n2o_emis = this->soilchem->accumulated_n2o_emis;
    this->acc.n2_emis = this->soilchem->accumulated_n2_emis;
    this->acc.nh3_emis = this->soilchem->accumulated_nh3_emis;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::finalize()
{
    this->io_kcomm->sink_handle_release( &this->m_sink);
    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::dump_0(
        ldndc_flt64_t *  _buf)
{
    LDNDC_OUTPUT_SET_COLUMN_INDEX(0);

    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->acc.co2_emis);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->acc.co2_emis_auto);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->acc.co2_emis_hetero);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->acc.doc_leach);

    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->acc.nh4_leach);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->acc.nh3_emis);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->acc.no3_leach);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->acc.no_emis);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->acc.n2o_emis);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->acc.n2_emis);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->acc.don_leach);

    return  LDNDC_ERR_OK;
}

#undef  LMOD_OUTPUT_MODULE_BASE
#undef  LMOD_OUTPUT_MODULE_NAME

#undef  OutputSoilchemistrySubdaily_Rank
#undef  OutputSoilchemistrySubdaily_Datasize

} /*namespace ldndc*/

