/*!
 * @brief
 *    dumping modified soil chemistry related items
 *
 * @author
 *    steffen klatt,
 *    edwin haas
 */

#ifndef  LM_OUTPUT_SOILCHEMISTRY_DAILY_H_NEW_
#define  LM_OUTPUT_SOILCHEMISTRY_DAILY_H_NEW_

#include  "mbe_legacyoutputmodel.h"
#include  "state/mbe_state.h"

#define  LMOD_OUTPUT_MODULE_BASE  MBE_LegacyOutputModel
#define  LMOD_OUTPUT_MODULE_NAME  OutputSoilchemistryDaily
#define  LMOD_OUTPUT_MODULE_ID    "output:soilchemistry:daily"
#define  LMOD_OUTPUT_MODULE_DESC  "Soil Chemistry Daily Output"
namespace ldndc {
class  substate_microclimate_t;
class  substate_physiology_t;
class  substate_watercycle_t;
class  LDNDC_API  LMOD_OUTPUT_MODULE_NAME  :  public  LMOD_OUTPUT_MODULE_BASE
{
    LMOD_EXPORT_MODULE_INFO(LMOD_OUTPUT_MODULE_NAME,LMOD_OUTPUT_MODULE_ID,LMOD_OUTPUT_MODULE_DESC);
    public:
        LMOD_OUTPUT_MODULE_NAME(
                MoBiLE_State *,
                cbm::io_kcomm_t *,
                timemode_e  _timemode);

        ~LMOD_OUTPUT_MODULE_NAME();


        lerr_t  configure( ldndc::config_file_t const *);

        lerr_t  initialize();

        lerr_t  solve();

        lerr_t  finalize();

        lerr_t  wake() { return  LDNDC_ERR_OK; }
        lerr_t  sleep() { return  LDNDC_ERR_OK; }

    private:
        lerr_t  dump_0(
                ldndc_flt64_t *);

    private:
        cbm::io_kcomm_t *  io_kcomm;

        input_class_soillayers_t const *  soillayers_in;

        substate_physiology_t const *  phys;
        MoBiLE_PlantVegetation * vegetation;
        substate_soilchemistry_t const *  soilchem;

    private:
        ldndc::sink_handle_t  m_sink;

        struct  output_soilchemistry_daily_acc_t
        {
            double  nh4_throughfall;
            double  no3_throughfall;

            double  doc_leach;
            double  don_leach;
            double  no3_leach;
            double  nh4_leach;

            double  ch4_emis;
            double  co2_emis_auto;
            double  co2_emis_hetero;
            double  no_emis;
            double  n2o_emis;
            double  n2_emis;
            double  nh3_emis;

            double  so4_leach;

            double  n2_fix_algae;
            double  n2_fix_bio;
            double  co2_fix;
            
            double  c_litter;
            double  n_litter;

            double  uptakeN_floor;
            double  mineralN_floor;
            double  nitrifyN_floor;
            double  denitrifyN_floor;
            double  denitrify_no2_floor;
            double  denitrify_no_floor;
            double  denitrify_n2o_floor;
            double  chemoN_floor;

            double  uptakeN_min;
            double  mineralN_min;
            double  nitrifyN_min;
            double  denitrifyN_min;
            double  denitrify_no2_min;
            double  denitrify_no_min;
            double  denitrify_n2o_min;
            double  chemoN_min;
        };
        output_soilchemistry_daily_acc_t  acc;
        void  pop_accumulated_outputs();
        void  push_accumulated_outputs();
};
} /*namespace ldndc*/


#undef  LMOD_OUTPUT_MODULE_BASE
#undef  LMOD_OUTPUT_MODULE_NAME
#undef  LMOD_OUTPUT_MODULE_ID
#undef  LMOD_OUTPUT_MODULE_DESC


#endif  /*  !LM_OUTPUT_SOILCHEMISTRY_DAILY_H_  */

