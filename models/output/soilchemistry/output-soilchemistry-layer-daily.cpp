/*!
 * @file
 * @author
 *  - Edwin Haas
 *  - Ruediger Grote
 *  - David Kraus
 */

#include  "output/soilchemistry/output-soilchemistry-layer-daily.h"

#include  "constants/lconstants-conv.h"

#define  LMOD_OUTPUT_MODULE_BASE  MBE_LegacyOutputModel
#define  LMOD_OUTPUT_MODULE_NAME  OutputSoilchemistryLayerDaily

LMOD_MODULE_INFO(LMOD_OUTPUT_MODULE_NAME,TMODE_POST_DAILY,LMOD_FLAG_USER|LMOD_FLAG_OUTPUT);
namespace ldndc {

#define  OutputSoilchemistryLayerDaily_Item__level
#define  OutputSoilchemistryLayerDaily_Item__extension
#define  OutputSoilchemistryLayerDaily_Item__som
#define  OutputSoilchemistryLayerDaily_Item__min
#define  OutputSoilchemistryLayerDaily_Item__C_hum
#define  OutputSoilchemistryLayerDaily_Item__C_aorg
#define  OutputSoilchemistryLayerDaily_Item__C_litter
#define  OutputSoilchemistryLayerDaily_Item__C_mic
#define  OutputSoilchemistryLayerDaily_Item__C_doc
#define  OutputSoilchemistryLayerDaily_Item__N_hum
#define  OutputSoilchemistryLayerDaily_Item__N_aorg
#define  OutputSoilchemistryLayerDaily_Item__N_litter
#define  OutputSoilchemistryLayerDaily_Item__N_mic
#define  OutputSoilchemistryLayerDaily_Item__N_nh4_clay
#define  OutputSoilchemistryLayerDaily_Item__N_nh4
#define  OutputSoilchemistryLayerDaily_Item__N_nh3
#define  OutputSoilchemistryLayerDaily_Item__N_no3
#define  OutputSoilchemistryLayerDaily_Item__N_no
#define  OutputSoilchemistryLayerDaily_Item__N_n2o
#define  OutputSoilchemistryLayerDaily_Item__N_don
#define  OutputSoilchemistryLayerDaily_Item__S_so4
#define  OutputSoilchemistryLayerDaily_Item__ph
#define  OutputSoilchemistryLayerDaily_Item__anvf
#define  OutputSoilchemistryLayerDaily_Item__o2
#define  OutputSoilchemistryLayerDaily_Item__mineral
#define  OutputSoilchemistryLayerDaily_Item__nitrify
#define  OutputSoilchemistryLayerDaily_Item__denitrify


/*!
 * @page soilchemistryoutput
 * @section soilchemistryoutputlayerdaily Soilchemistry output (layer/daily)
 *
 * <b>xml-based module selection in project's setup:</b>
 * \n\code{.xml}< module id="output:soilchemistry-layer:daily" />\endcode\n
 *
 * entity name | decription | unit
 * ----------- | ---------- | ----
 * layer | Soil layer | [-]
 * height | Soil layer height | [m]
 * extension | extension | [m]
 * som | Total soil organic matter | [kgDWha-1]
 * min | Mineral fraction | [kgDWha-1]
 * C\_hum | Carbon in soil humus pool | [kgCha-1]
 * C\_aorg | Carbon in active organic matter pool | [kgCha-1]
 * C\_litter\_1 | Carbon of very labile litter pool | [kgCha-1]
 * C\_litter\_2 | Carbon of labile litter pool | [kgCha-1]
 * C\_litter\_3 | Carbon of recalcitrant litter pool | [kgCha-1]
 * C\_mic\_1 | Carbon of microbial pool 1 | [kgCha-1]
 * C\_mic\_2 | Carbon of microbial pool 2 | [kgCha-1]
 * C\_mic\_3 | Carbon of microbial pool 3 | [kgCha-1]
 * C\_doc | Dissolved organic carbon | [kgCha-1]
 * N\_hum | Nitrogen in soil humus pool | [kgNha-1]
 * N\_aorg| Active organic nitrogen | [kgNha-1]
 * N\_litter| Nitrogen in soil litter | [kgNha-1]
 * N\_mic| Total microbial nitrogen | [kgNha-1]
 * N\_nh4\_clay| Ammonium adsorbed on clay minerals | [kgNha-1]
 * N\_nh4| Ammonium | [kgNha-1]
 * N\_nh3| Ammonia | [kgNha-1]
 * N\_no3| Nitrate | [kgNha-1]
 * N\_no2| Nitrite | [kgNha-1]
 * N\_no| Nitric oxide | [kgNha-1]
 * N\_n2o| Nitroux oxide | [kgNha-1]
 * N\_don| Dissolved organic nitrogen | [kgNha-1]
 * ph [-] | pH value | [-]
 * anvf | Anaerobic volume fraction | [-]
 * o2 | Oxygen partial pressure | [bar]
 * dN\_mineral | Daily mineralization rate | [kgNha-1]
 * dN\_nitrify | Daily nitrification rate | [kgNha-1]
 * dN\_denitrify | Daily denitrification rate | [kgNha-1]
 */
ldndc_string_t const  OutputSoilchemistryLayerDaily_Ids[] =
{
#ifdef  OutputSoilchemistryLayerDaily_Item__level
    "level",
#endif
#ifdef  OutputSoilchemistryLayerDaily_Item__extension
    "extension",
#endif
#ifdef  OutputSoilchemistryLayerDaily_Item__som
    "som",
#endif
#ifdef  OutputSoilchemistryLayerDaily_Item__min
    "min",
#endif
#ifdef  OutputSoilchemistryLayerDaily_Item__C_hum
    "C_hum",
#endif
#ifdef  OutputSoilchemistryLayerDaily_Item__C_aorg
    "C_aorg",
#endif
#ifdef  OutputSoilchemistryLayerDaily_Item__C_litter
    "C_litter_1", "C_litter_2", "C_litter_3",
#endif
#ifdef  OutputSoilchemistryLayerDaily_Item__C_mic
    "C_mic_1", "C_mic_2", "C_mic_3",
#endif
#ifdef  OutputSoilchemistryLayerDaily_Item__C_doc
    "C_doc",
#endif
#ifdef  OutputSoilchemistryLayerDaily_Item__N_hum
    "N_hum",
#endif
#ifdef  OutputSoilchemistryLayerDaily_Item__N_aorg
    "N_aorg",
#endif
#ifdef  OutputSoilchemistryLayerDaily_Item__N_litter
    "N_litter",
#endif
#ifdef  OutputSoilchemistryLayerDaily_Item__N_mic
    "N_mic",
#endif
#ifdef  OutputSoilchemistryLayerDaily_Item__N_nh4_clay
    "N_nh4_clay",
#endif
#ifdef  OutputSoilchemistryLayerDaily_Item__N_nh4
    "N_nh4",
#endif
#ifdef  OutputSoilchemistryLayerDaily_Item__N_nh3
    "N_nh3",
#endif
#ifdef  OutputSoilchemistryLayerDaily_Item__N_no3
    "N_no3",
#endif
#ifdef  OutputSoilchemistryLayerDaily_Item__N_no
    "N_no",
#endif
#ifdef  OutputSoilchemistryLayerDaily_Item__N_n2o
    "N_n2o",
#endif
#ifdef  OutputSoilchemistryLayerDaily_Item__N_don
    "N_don",
#endif
#ifdef  OutputSoilchemistryLayerDaily_Item__S_so4
    "S_so4",
#endif
#ifdef  OutputSoilchemistryLayerDaily_Item__ph
    "ph",
#endif
#ifdef  OutputSoilchemistryLayerDaily_Item__anvf
    "anvf",
#endif
#ifdef  OutputSoilchemistryLayerDaily_Item__o2
    "o2",
#endif
#ifdef  OutputSoilchemistryLayerDaily_Item__mineral
    "dN_mineral",
#endif
#ifdef  OutputSoilchemistryLayerDaily_Item__nitrify
    "dN_nitrify",
#endif
#ifdef  OutputSoilchemistryLayerDaily_Item__denitrify
    "dN_denitrify"
#endif
};

ldndc_string_t const  OutputSoilchemistryLayerDaily_Header[] =
{
#ifdef  OutputSoilchemistryLayerDaily_Item__level
    "level[m]",
#endif
#ifdef  OutputSoilchemistryLayerDaily_Item__extension
    "extension[m]",
#endif
#ifdef  OutputSoilchemistryLayerDaily_Item__som
    "som[kgDWha-1]",
#endif
#ifdef  OutputSoilchemistryLayerDaily_Item__min
    "min[kgDWha-1]",
#endif
#ifdef  OutputSoilchemistryLayerDaily_Item__C_hum
    "C_hum[kgCha-1]",
#endif
#ifdef  OutputSoilchemistryLayerDaily_Item__C_aorg
    "C_aorg[kgCha-1]",
#endif
#ifdef  OutputSoilchemistryLayerDaily_Item__C_litter
    "C_litter_1[kgCha-1]", "C_litter_2[kgCha-1]", "C_litter_3[kgCha-1]",
#endif
#ifdef  OutputSoilchemistryLayerDaily_Item__C_mic
    "C_mic_1[kgCha-1]", "C_mic_2[kgCha-1]", "C_mic_3[kgCha-1]",
#endif
#ifdef  OutputSoilchemistryLayerDaily_Item__C_doc
    "C_doc[kgCha-1]",
#endif
#ifdef  OutputSoilchemistryLayerDaily_Item__N_hum
    "N_hum[kgNha-1]",
#endif
#ifdef  OutputSoilchemistryLayerDaily_Item__N_aorg
    "N_aorg[kgNha-1]",
#endif
#ifdef  OutputSoilchemistryLayerDaily_Item__N_litter
    "N_litter[kgNha-1]",
#endif
#ifdef  OutputSoilchemistryLayerDaily_Item__N_mic
    "N_mic[kgNha-1]",
#endif
#ifdef  OutputSoilchemistryLayerDaily_Item__N_nh4_clay
    "N_nh4_clay[kgNha-1]",
#endif
#ifdef  OutputSoilchemistryLayerDaily_Item__N_nh4
    "N_nh4[kgNha-1]",
#endif
#ifdef  OutputSoilchemistryLayerDaily_Item__N_nh3
    "N_nh3[kgNha-1]",
#endif
#ifdef  OutputSoilchemistryLayerDaily_Item__N_no3
    "N_no3[kgNha-1]",
#endif
#ifdef  OutputSoilchemistryLayerDaily_Item__N_no
    "N_no[kgNha-1]",
#endif
#ifdef  OutputSoilchemistryLayerDaily_Item__N_n2o
    "N_n2o[kgNha-1]",
#endif
#ifdef  OutputSoilchemistryLayerDaily_Item__N_don
    "N_don[kgNha-1]",
#endif
#ifdef  OutputSoilchemistryLayerDaily_Item__S_so4
    "S_so4[kgSha-1]",
#endif
#ifdef  OutputSoilchemistryLayerDaily_Item__ph
    "ph[-]",
#endif
#ifdef  OutputSoilchemistryLayerDaily_Item__anvf
    "anvf[-]",
#endif
#ifdef  OutputSoilchemistryLayerDaily_Item__o2
    "o2[bar]",
#endif
#ifdef  OutputSoilchemistryLayerDaily_Item__mineral
    "dN_mineral[kgNha-1]",
#endif
#ifdef  OutputSoilchemistryLayerDaily_Item__nitrify
    "dN_nitrify[kgNha-1]",
#endif
#ifdef  OutputSoilchemistryLayerDaily_Item__denitrify
    "dN_denitrify[kgNha-1]"
#endif
};
#define  OutputSoilchemistryLayerDaily_Datasize  (sizeof( OutputSoilchemistryLayerDaily_Header) / sizeof( OutputSoilchemistryLayerDaily_Header[0]))
ldndc_output_size_t const  OutputSoilchemistryLayerDaily_Sizes[] =
{
    OutputSoilchemistryLayerDaily_Datasize,

    OutputSoilchemistryLayerDaily_Datasize /*total size*/
};
ldndc_output_size_t const *  OutputSoilchemistryLayerDaily_EntitySizes = NULL;
#define  OutputSoilchemistryLayerDaily_Rank  ((ldndc_output_rank_t)(sizeof( OutputSoilchemistryLayerDaily_Sizes) / sizeof( OutputSoilchemistryLayerDaily_Sizes[0])) - 1)
atomic_datatype_t const  OutputSoilchemistryLayerDaily_Types[] =
{
    LDNDC_FLOAT64
};


LMOD_OUTPUT_MODULE_NAME::LMOD_OUTPUT_MODULE_NAME(
          MoBiLE_State *  _state,
          cbm::io_kcomm_t *  _io_kcomm,
          timemode_e  _timemode)
          : LMOD_OUTPUT_MODULE_BASE( _state, _io_kcomm, _timemode),

          io_kcomm( _io_kcomm),
          siteparam_( _io_kcomm->get_input_class< input_class_siteparameters_t >()),
          soillayers_( _io_kcomm->get_input_class< input_class_soillayers_t >()),

          sc( _state->get_substate< substate_soilchemistry_t >())
{
    this->rc.soil_all = 1;

    this->acc.dN_mineral_sl = new double[this->soillayers_->soil_layer_cnt()];
    this->acc.dN_nitrify_sl = new double[this->soillayers_->soil_layer_cnt()];
    this->acc.dN_denitrify_sl = new double[this->soillayers_->soil_layer_cnt()];
}


LMOD_OUTPUT_MODULE_NAME::~LMOD_OUTPUT_MODULE_NAME()
{
    delete[] this->acc.dN_mineral_sl;
    delete[] this->acc.dN_nitrify_sl;
    delete[] this->acc.dN_denitrify_sl;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::configure(
                ldndc::config_file_t const *  _cf)
{
    lerr_t  rc_setflags =
        this->set_metaflags( _cf, RM_DEFAULT_LAYERDAILY);
    if ( rc_setflags)
        { return  LDNDC_ERR_FAIL; }

    bool  rc_option;
    /* layers to write: 1:=all; 0:={first,middle,last(litter,total)}; */
    CF_LMOD_QUERY(_cf, "write_soil_layers_all", rc_option, true);
    this->rc.soil_all = rc_option;

    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::initialize()
{
    this->m_sink = this->io_kcomm->sink_handle_acquire( "soilchemistrylayerdaily");
    if ( this->m_sink.status() == LDNDC_ERR_OK)
    {
        lerr_t  rc_layout = LDNDC_OUTPUT_DEFINE_SINK_LAYOUT_DEFAULTS(
            this->m_sink,OutputSoilchemistryLayerDaily);
        RETURN_IF_NOT_OK(rc_layout);
    }
    else
    {
        KLOGERROR( "sink status bad  [sink=","soilchemistrylayerdaily","]");
        return  this->m_sink.status();
    }

    for ( size_t  sl = 0;  sl < this->soillayers_->soil_layer_cnt();  ++sl)
    {
        this->push_accumulated_outputs( sl);
    }
    
    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::solve()
{
    ldndc_flt64_t  data_flt64_0[OutputSoilchemistryLayerDaily_Datasize];

    int const  l_total = static_cast< int >( this->soillayers_->soil_layer_cnt());
    int const  l_lit = static_cast< int >( this->soillayers_->soil_layers_in_litter_cnt());
    int const  l_mid = ( l_total + l_lit) / 2;

    for ( int  l = 0;  l < l_total;  ++l)
    {
        if ( this->rc.soil_all)
        {
            /* select all layers */
        }
        else
        {
            /* select first, middle and last layer */
            if ( l == 0  ||  (l == (l_lit-1))  || (l == l_mid)  ||  (l == (l_total-1)))
            {
                /* write this layer record */
            }
            else
            {
                /* skip */
                continue;
            }
        }

        this->pop_accumulated_outputs( l);
        lerr_t  rc_dump = this->dump_0( data_flt64_0, l);
        this->push_accumulated_outputs( l);

        if ( rc_dump)
        { return  LDNDC_ERR_FAIL; }
        
        void *  data[] = { data_flt64_0};
        this->set_layernumber( -l-1);
        lerr_t  rc_write =
            this->write_fixed_record( &this->m_sink, data);
        if ( rc_write)
            { return  LDNDC_ERR_FAIL; }
    }

    return  LDNDC_ERR_OK;
}


void
LMOD_OUTPUT_MODULE_NAME::pop_accumulated_outputs( int _sl)
{
    this->acc.dN_mineral_sl[_sl] = -this->acc.dN_mineral_sl[_sl];
    this->acc.dN_nitrify_sl[_sl] = -this->acc.dN_nitrify_sl[_sl];
    this->acc.dN_denitrify_sl[_sl] = -this->acc.dN_denitrify_sl[_sl];

    this->acc.dN_mineral_sl[_sl] += sc->accumulated_n_mineral_sl[_sl];
    this->acc.dN_nitrify_sl[_sl] += sc->accumulated_n_nitrify_sl[_sl];
    this->acc.dN_denitrify_sl[_sl] += sc->accumulated_no3_denitrify_sl[_sl];
}


void
LMOD_OUTPUT_MODULE_NAME::push_accumulated_outputs( int _sl)
{
    this->acc.dN_mineral_sl[_sl] = 0.0;
    this->acc.dN_nitrify_sl[_sl] = 0.0;
    this->acc.dN_denitrify_sl[_sl] = 0.0;

    this->acc.dN_mineral_sl[_sl] += sc->accumulated_n_mineral_sl[_sl];
    this->acc.dN_nitrify_sl[_sl] += sc->accumulated_n_nitrify_sl[_sl];
    this->acc.dN_denitrify_sl[_sl] += sc->accumulated_no3_denitrify_sl[_sl];
}



lerr_t
LMOD_OUTPUT_MODULE_NAME::finalize()
{
    this->io_kcomm->sink_handle_release( &this->m_sink);
    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::dump_0( ldndc_flt64_t *  _buf, int  _l)
{
    ldndc_assert( _buf);
    static const double  p = cbm::M2_IN_HA;

    LDNDC_OUTPUT_SET_COLUMN_INDEX(0);

#ifdef  OutputSoilchemistryLayerDaily_Item__level
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(-this->sc->depth_sl[_l]);
#endif
#ifdef  OutputSoilchemistryLayerDaily_Item__extension
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->sc->h_sl[_l]);
#endif

#ifdef  OutputSoilchemistryLayerDaily_Item__som
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * this->sc->som_sl[_l]);
#endif
#ifdef  OutputSoilchemistryLayerDaily_Item__min
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * this->sc->min_sl[_l]);
#endif

#ifdef  OutputSoilchemistryLayerDaily_Item__C_hum
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * this->sc->C_hum_sl[_l]);
#endif
#ifdef  OutputSoilchemistryLayerDaily_Item__C_aorg
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * this->sc->C_aorg_sl[_l]);
#endif
#ifdef  OutputSoilchemistryLayerDaily_Item__C_litter
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * this->sc->C_lit1_sl[_l]);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * this->sc->C_lit2_sl[_l]);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * this->sc->C_lit3_sl[_l]);    
#endif
#ifdef  OutputSoilchemistryLayerDaily_Item__C_mic
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * this->sc->C_micro1_sl[_l]);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * this->sc->C_micro2_sl[_l]);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * this->sc->C_micro3_sl[_l]);
#endif
#ifdef  OutputSoilchemistryLayerDaily_Item__C_doc
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * (this->sc->doc_sl[_l]+this->sc->an_doc_sl[_l]));
#endif

#ifdef  OutputSoilchemistryLayerDaily_Item__N_hum
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * this->sc->N_hum_sl[_l]);
#endif
#ifdef  OutputSoilchemistryLayerDaily_Item__N_aorg
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * this->sc->N_aorg_sl[_l]);
#endif
#ifdef  OutputSoilchemistryLayerDaily_Item__N_litter
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * (this->sc->N_lit1_sl[_l]+this->sc->N_lit2_sl[_l]+this->sc->N_lit3_sl[_l]));
#endif
#ifdef  OutputSoilchemistryLayerDaily_Item__N_mic
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * this->sc->N_micro_sl[_l]);
#endif

#ifdef  OutputSoilchemistryLayerDaily_Item__N_nh4_clay
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * this->sc->clay_nh4_sl[_l]);
#endif
#ifdef  OutputSoilchemistryLayerDaily_Item__N_nh4
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * this->sc->nh4_sl[_l]);
#endif
#ifdef  OutputSoilchemistryLayerDaily_Item__N_nh3
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * (this->sc->nh3_liq_sl[_l]+this->sc->nh3_gas_sl[_l]));
#endif
#ifdef  OutputSoilchemistryLayerDaily_Item__N_no3
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * (this->sc->no3_sl[_l]+this->sc->an_no3_sl[_l]));
#endif
#ifdef  OutputSoilchemistryLayerDaily_Item__N_no
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * (this->sc->no_sl[_l]+this->sc->an_no_sl[_l]));
#endif
#ifdef  OutputSoilchemistryLayerDaily_Item__N_n2o
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * (this->sc->n2o_sl[_l]+this->sc->an_n2o_sl[_l]));
#endif
#ifdef  OutputSoilchemistryLayerDaily_Item__N_don
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * this->sc->don_sl[_l]);
#endif
#ifdef  OutputSoilchemistryLayerDaily_Item__S_so4
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * this->sc->so4_sl[_l]);
#endif
#ifdef  OutputSoilchemistryLayerDaily_Item__ph
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->sc->ph_sl[_l]);
#endif
#ifdef  OutputSoilchemistryLayerDaily_Item__anvf
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->sc->anvf_sl[_l]);
#endif
#ifdef  OutputSoilchemistryLayerDaily_Item__o2
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(this->sc->o2_sl[_l]);
#endif
#ifdef  OutputSoilchemistryLayerDaily_Item__mineral
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * this->acc.dN_mineral_sl[_l]);
#endif
#ifdef  OutputSoilchemistryLayerDaily_Item__nitrify
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * this->acc.dN_nitrify_sl[_l]);
#endif
#ifdef  OutputSoilchemistryLayerDaily_Item__denitrify
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * this->acc.dN_denitrify_sl[_l]);
#endif
    return  LDNDC_ERR_OK;
}


#undef  LMOD_OUTPUT_MODULE_BASE
#undef  LMOD_OUTPUT_MODULE_NAME

#undef  OutputSoilchemistryLayerDaily_Rank
#undef  OutputSoilchemistryLayerDaily_Datasize

} /*namespace ldndc*/

