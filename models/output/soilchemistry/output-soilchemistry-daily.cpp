/*!
 * @file
 * @author
 *  - Edwin Haas
 *  - Ruediger Grote
 *  - David Kraus
 */

#include  "output/soilchemistry/output-soilchemistry-daily.h"

#include  "constants/lconstants-plant.h"
#include  "constants/lconstants-conv.h"

#define  LMOD_OUTPUT_MODULE_BASE  MBE_LegacyOutputModel
#define  LMOD_OUTPUT_MODULE_NAME  OutputSoilchemistryDaily

LMOD_MODULE_INFO(LMOD_OUTPUT_MODULE_NAME,TMODE_POST_DAILY,LMOD_FLAG_USER|LMOD_FLAG_OUTPUT);
namespace ldndc {


#define  OutputSoilchemistryDaily_Item__CH4
#define  OutputSoilchemistryDaily_Item__CO2f
#define  OutputSoilchemistryDaily_Item__CO2s
#define  OutputSoilchemistryDaily_Item__DOC
#define  OutputSoilchemistryDaily_Item__DON
#define  OutputSoilchemistryDaily_Item__NO3
#define  OutputSoilchemistryDaily_Item__NH4
#define  OutputSoilchemistryDaily_Item__NO
#define  OutputSoilchemistryDaily_Item__N2O
#define  OutputSoilchemistryDaily_Item__N2
#define  OutputSoilchemistryDaily_Item__NH3
#define  OutputSoilchemistryDaily_Item__N2FIX
#define  OutputSoilchemistryDaily_Item__CO2FIX
#define  OutputSoilchemistryDaily_Item__SO4
#define  OutputSoilchemistryDaily_Item__dCLIT
#define  OutputSoilchemistryDaily_Item__dNLIT
#define  OutputSoilchemistryDaily_Item__NDEP
#define  OutputSoilchemistryDaily_Item__NUPT_FLOOR
#define  OutputSoilchemistryDaily_Item__NUPT_MIN
#define  OutputSoilchemistryDaily_Item__MINERALN_FLOOR
#define  OutputSoilchemistryDaily_Item__MINERALN_MIN
#define  OutputSoilchemistryDaily_Item__NITRIFYN_FLOOR
#define  OutputSoilchemistryDaily_Item__NITRIFYN_MIN
#define  OutputSoilchemistryDaily_Item__DENITRIFYN_FLOOR
//#define  OutputSoilchemistryDaily_Item__DENITRIFYN_EXTENDED_FLOOR
#define  OutputSoilchemistryDaily_Item__DENITRIFYN_MIN
//#define  OutputSoilchemistryDaily_Item__DENITRIFYN_EXTENDED_MIN
#define  OutputSoilchemistryDaily_Item__CHEMON_FLOOR
#define  OutputSoilchemistryDaily_Item__CHEMON_MIN
#define  OutputSoilchemistryDaily_Item__PLANTC_FLOOR
#define  OutputSoilchemistryDaily_Item__PLANTC_MIN
#define  OutputSoilchemistryDaily_Item__SOILC_FLOOR
#define  OutputSoilchemistryDaily_Item__SOILC_MIN
#define  OutputSoilchemistryDaily_Item__AORGC_FLOOR
#define  OutputSoilchemistryDaily_Item__AORGC_MIN
#define  OutputSoilchemistryDaily_Item__CLIT_RAW
#define  OutputSoilchemistryDaily_Item__CLIT_FLOOR
#define  OutputSoilchemistryDaily_Item__CLIT_MIN
#define  OutputSoilchemistryDaily_Item__MICROC_FLOOR
#define  OutputSoilchemistryDaily_Item__MICROC_MIN
#define  OutputSoilchemistryDaily_Item__SOLC_FLOOR
#define  OutputSoilchemistryDaily_Item__SOLC_MIN
#define  OutputSoilchemistryDaily_Item__CWOODABOVE
#define  OutputSoilchemistryDaily_Item__CWOODBELOW
#define  OutputSoilchemistryDaily_Item__PLANTN_FLOOR
#define  OutputSoilchemistryDaily_Item__PLANTN_MIN
#define  OutputSoilchemistryDaily_Item__SOILN_FLOOR
#define  OutputSoilchemistryDaily_Item__SOILN_MIN
#define  OutputSoilchemistryDaily_Item__AORGN_FLOOR
#define  OutputSoilchemistryDaily_Item__AORGN_MIN
#define  OutputSoilchemistryDaily_Item__NLIT_RAW
#define  OutputSoilchemistryDaily_Item__NLIT_FLOOR
#define  OutputSoilchemistryDaily_Item__NLIT_MIN
#define  OutputSoilchemistryDaily_Item__MICRON_FLOOR
#define  OutputSoilchemistryDaily_Item__MICRON_MIN
#define  OutputSoilchemistryDaily_Item__SOLN_FLOOR
#define  OutputSoilchemistryDaily_Item__SOLN_MIN
#define  OutputSoilchemistryDaily_Item__NWOODABOVE
#define  OutputSoilchemistryDaily_Item__NWOODBELOW
#define  OutputSoilchemistryDaily_Item__ANVF


/*!
 * @page soilchemistryoutput
 * @section soilchemistryoutputdaily Soilchemistry output (daily)
 *
 * <b>xml-based module selection in project's setup:</b>
 * \n\code{.xml}< module id="output:soilchemistry:daily" />\endcode\n
 *
 * entity name | decription | unit
 * ----------- | ---------- | ----
 * dC_ch4_emis | Daily methane flux | [kgCha-1]
 * dC_co2_emis_auto | Daily autotrophic (fine roots) soil respiration | [kgCha-1]
 * dC_co2_emis_hetero | Daily heterotrophic soil respiration | [kgCha-1]
 * dC_sol_leach | Daily dissolved organic carbon leaching | [kgCha-1]
 * dN_don_leach | Daily dissolved organic nitrogen leaching |  [kgNha-1]
 * dN_no3_leach | Daily nitrate leaching | [kgNha-1]
 * dN_nh4_leach | Daily ammonium leaching | [kgNha-1]
 * dN_no_emis | Daily nitric oxide flux | [kgNha-1]
 * dN_n2o_emis | Daily nitrous oxide flux | [kgNha-1]
 * dN_n2_emis | Daily molecular nitrogen flux | [kgNha-1]
 * dN_n2_fix | Daily molecular nitrogen fixation by soil microorganisms | [kgNha-1]
 * dN_nh3_emis | Daily ammonia volatilization | [kgNha-1]
 * dC_litter | Daily carbon litter fall | [kgCha-1]
 * dN_litter | Daily nitrogen litter fall | [kgNha-1]
 * dN_dep | Daily nitrogen deposition from the atmosphere | [kgNha-1]
 * dN_up_floor | Daily nitrogen uptake by plants from the litter layer | [kgNha-1]
 * dN_up_min | Daily nitrogen uptake by plants from the mineral soil | [kgNha-1]
 * dN_mineral_floor | Daily nitrogen mineralization rate of litter layer | [kgNha-1]
 * dN_mineral_min | Daily nitrogen mineralization rate of mineral soil | [kgNha-1]
 * dN_nitrify_floor | Daily nitrification in the litter layer | [kgNha-1]
 * dN_nitrify_min | Daily nitrification in the mineral soil | [kgNha-1]
 * dN_chemo_floor | Daily chemodenitrification on the litter layer | [kgNha-1]
 * dN_chemo_min | Daily chemodenitrification in the mineral soil | [kgNha-1]
 * C_soil_floor | Total organic soil carbon in the litter layer | [kgCha-1]
 * C_soil_min | Total organic soil carbon in the mineral soil  | [kgCha-1]
 * C_soil_min_20cm | Total organic soil carbon in the first 20 cm in the mineral soil  | [kgCha-1]
 * C_soil_min_30cm | Total organic soil carbon in the first 30 cm in the mineral soil  | [kgCha-1]
 * C_aorg_floor | Total active organic carbon in the litter layer | [kgCha-1]
 * C_aorg_min | Total active organic carbon in the mineral soil | [kgCha-1]
 * C_litter_raw | Total above and belowground carbon in raw litter | [kgCha-1]
 * C_litter_floor | Carbon stored in litter pools in the litter layer | [kgCha-1]
 * C_litter_min | Carbon stored in litter pools in the mineral soil | [kgCha-1]
 * C_mic_floor | Microbial biomass carbon in the litter layer | [kgCha-1]
 * C_mic_min | Microbial biomass carbon  in the mineral soil | [kgCha-1]
 * C_sol_floor | Dissolved organic carbon in the litter layer | [kgCha-1]
 * C_sol_min | Dissolved organic carbon in the mineral soil | [kgCha-1]
 * C_wood | Wood carbon | [kgCha-1]
 * N_soil_floor | Total non-dissolved organic nitrogen in the litter layer | [kgNha-1]
 * N_soil_min | Total non-dissolved organic nitrogen in the mineral soil | [kgNha-1]
 * N_soil_min_20cm | Total non-dissolved organic nitrogen in the first 20 cm in the mineral soil | [kgNha-1]
 * N_soil_min_30cm | Total non-dissolved organic nitrogen in the first 30 cm in the mineral soil | [kgNha-1]
 * N_aorg_floor | Total active organic nitrogen in the litter layer | [kgCha-1]
 * N_aorg_min| Total active organic nitrogen in the mineral soil | [kgCha-1]
 * N_litter_raw | Total above and belowground nitrogen in raw litter | [kgNha-1]
 * N_litter_floor | Nitrogen stored in litter pools in the litter layer | [kgNha-1]
 * N_litter_min | Nitrogen stored in litter pools in the mineral soil | [kgNha-1]
 * N_mic_floor | Microbial biomass nitrogen in the litter layer | [kgNha-1]
 * N_mic_min | Microbial biomass nitrogen in the mineral soil | [kgNha-1]
 * N_sol_floor | Dissolved organic + inorganic nitrogen in the litter layer | [kgNha-1]
 * N_sol_min | Dissolved organic + inorganic nitrogen in the mineral soil | [kgNha-1]
 */
ldndc_string_t const  OutputSoilchemistryDaily_Ids[] =
{
#ifdef  OutputSoilchemistryDaily_Item__CH4
    "dC_ch4_emis",
#endif
#ifdef  OutputSoilchemistryDaily_Item__CO2f
    "dC_co2_emis_auto",
#endif
#ifdef  OutputSoilchemistryDaily_Item__CO2s
    "dC_co2_emis_hetero",
#endif
#ifdef  OutputSoilchemistryDaily_Item__DOC
    "dC_doc_leach",
#endif
#ifdef  OutputSoilchemistryDaily_Item__DON
    "dN_don_leach",
#endif
#ifdef  OutputSoilchemistryDaily_Item__NO3
    "dN_no3_leach",
#endif
#ifdef  OutputSoilchemistryDaily_Item__NH4
    "dN_nh4_leach",
#endif
#ifdef  OutputSoilchemistryDaily_Item__NO
    "dN_no_emis",
#endif
#ifdef  OutputSoilchemistryDaily_Item__N2O
    "dN_n2o_emis",
#endif
#ifdef  OutputSoilchemistryDaily_Item__N2
    "dN_n2_emis",
#endif
#ifdef  OutputSoilchemistryDaily_Item__NH3
    "dN_nh3_emis",
#endif
#ifdef  OutputSoilchemistryDaily_Item__N2FIX
    "dN_n2_fix",
#endif
#ifdef  OutputSoilchemistryDaily_Item__CO2FIX
    "dC_co2_fix",
#endif
#ifdef  OutputSoilchemistryDaily_Item__SO4
    "dS_so4_leach",
#endif
#ifdef  OutputSoilchemistryDaily_Item__dCLIT
    "dC_litter",
#endif
#ifdef  OutputSoilchemistryDaily_Item__dNLIT
    "dN_litter",
#endif
#ifdef  OutputSoilchemistryDaily_Item__NDEP
    "dN_dep",
#endif
#ifdef  OutputSoilchemistryDaily_Item__NUPT_FLOOR
    "dN_up_floor",
#endif
#ifdef  OutputSoilchemistryDaily_Item__NUPT_MIN
    "dN_up_min",
#endif
#ifdef  OutputSoilchemistryDaily_Item__MINERALN_FLOOR
    "dN_mineral_floor",
#endif
#ifdef  OutputSoilchemistryDaily_Item__MINERALN_MIN
    "dN_mineral_min",
#endif
#ifdef  OutputSoilchemistryDaily_Item__NITRIFYN_FLOOR
    "dN_nitrify_floor",
#endif
#ifdef  OutputSoilchemistryDaily_Item__NITRIFYN_MIN
    "dN_nitrify_min",
#endif
#ifdef  OutputSoilchemistryDaily_Item__DENITRIFYN_FLOOR
    "dN_denitrify_floor",
#endif
#ifdef  OutputSoilchemistryDaily_Item__DENITRIFYN_EXTENDED_FLOOR
    "dN_denitrify_no2_floor",
    "dN_denitrify_no_floor",
    "dN_denitrify_n2o_floor",
#endif
#ifdef  OutputSoilchemistryDaily_Item__DENITRIFYN_MIN
    "dN_denitrify_min",
#endif
#ifdef  OutputSoilchemistryDaily_Item__DENITRIFYN_EXTENDED_MIN
    "dN_denitrify_no2_min",
    "dN_denitrify_no_min",
    "dN_denitrify_n2o_min",
#endif
#ifdef  OutputSoilchemistryDaily_Item__CHEMON_FLOOR
    "dN_chemo_floor",
#endif
#ifdef  OutputSoilchemistryDaily_Item__CHEMON_MIN
    "dN_chemo_min",
#endif
#ifdef  OutputSoilchemistryDaily_Item__PLANTC_FLOOR
    "C_plants_floor",
#endif
#ifdef  OutputSoilchemistryDaily_Item__PLANTC_MIN
    "C_plants_min",
#endif
#ifdef  OutputSoilchemistryDaily_Item__SOILC_FLOOR
    "C_soil_floor",
#endif
#ifdef  OutputSoilchemistryDaily_Item__SOILC_MIN
    "C_soil_min",
    "C_soil_min_20cm",
    "C_soil_min_30cm",
#endif
#ifdef  OutputSoilchemistryDaily_Item__AORGC_FLOOR
    "C_aorg_floor",
#endif
#ifdef  OutputSoilchemistryDaily_Item__AORGC_MIN
    "C_aorg_min",
#endif    
#ifdef  OutputSoilchemistryDaily_Item__CLIT_RAW
    "C_litter_raw",
#endif
#ifdef  OutputSoilchemistryDaily_Item__CLIT_FLOOR
    "C_litter_floor",
#endif
#ifdef  OutputSoilchemistryDaily_Item__CLIT_MIN
    "C_litter_min",
#endif
#ifdef  OutputSoilchemistryDaily_Item__MICROC_FLOOR
    "C_mic_floor",
#endif
#ifdef  OutputSoilchemistryDaily_Item__MICROC_MIN
    "C_mic_min",
#endif
#ifdef  OutputSoilchemistryDaily_Item__SOLC_FLOOR
    "C_sol_floor",
#endif
#ifdef  OutputSoilchemistryDaily_Item__SOLC_MIN
    "C_sol_min",
#endif
#ifdef  OutputSoilchemistryDaily_Item__CWOODABOVE
    "C_wood_above",
#endif
#ifdef  OutputSoilchemistryDaily_Item__CWOODBELOW
    "C_wood_below",
#endif
#ifdef  OutputSoilchemistryDaily_Item__PLANTN_FLOOR
    "N_plants_floor",
#endif
#ifdef  OutputSoilchemistryDaily_Item__PLANTN_MIN
    "N_plants_min",
#endif
#ifdef  OutputSoilchemistryDaily_Item__SOILN_FLOOR
    "N_soil_floor",
#endif
#ifdef  OutputSoilchemistryDaily_Item__SOILN_MIN
    "N_soil_min",
    "N_soil_min_20cm",
    "N_soil_min_30cm",
#endif
#ifdef  OutputSoilchemistryDaily_Item__AORGN_FLOOR
    "N_aorg_floor",
#endif
#ifdef  OutputSoilchemistryDaily_Item__AORGN_MIN
    "N_aorg_min",
#endif
#ifdef  OutputSoilchemistryDaily_Item__NLIT_RAW
    "N_litter_raw",
#endif
#ifdef  OutputSoilchemistryDaily_Item__NLIT_FLOOR
    "N_litter_floor",
#endif
#ifdef  OutputSoilchemistryDaily_Item__NLIT_MIN
    "N_litter_min",
#endif
#ifdef  OutputSoilchemistryDaily_Item__MICRON_FLOOR
    "N_mic_floor",
#endif
#ifdef  OutputSoilchemistryDaily_Item__MICRON_MIN
    "N_mic_min",
#endif
#ifdef  OutputSoilchemistryDaily_Item__SOLN_FLOOR
    "N_sol_floor",
#endif
#ifdef  OutputSoilchemistryDaily_Item__SOLN_MIN
    "N_sol_min",
#endif
#ifdef  OutputSoilchemistryDaily_Item__NWOODABOVE
    "N_wood_above",
#endif
#ifdef  OutputSoilchemistryDaily_Item__NWOODBELOW
    "N_wood_below",
#endif
#ifdef  OutputSoilchemistryDaily_Item__ANVF
    "anvf_mean",
#endif    
};

ldndc_string_t const  OutputSoilchemistryDaily_Header[] =
{
#ifdef  OutputSoilchemistryDaily_Item__CH4
    "dC_ch4_emis[kgCha-1]",
#endif
#ifdef  OutputSoilchemistryDaily_Item__CO2f
    "dC_co2_emis_auto[kgCha-1]",
#endif
#ifdef  OutputSoilchemistryDaily_Item__CO2s
    "dC_co2_emis_hetero[kgCha-1]",
#endif
#ifdef  OutputSoilchemistryDaily_Item__DOC
    "dC_doc_leach[kgCha-1]",
#endif
#ifdef  OutputSoilchemistryDaily_Item__DON
    "dN_don_leach[kgNha-1]",
#endif
#ifdef  OutputSoilchemistryDaily_Item__NO3
    "dN_no3_leach[kgNha-1]",
#endif
#ifdef  OutputSoilchemistryDaily_Item__NH4
    "dN_nh4_leach[kgNha-1]",
#endif
#ifdef  OutputSoilchemistryDaily_Item__NO
    "dN_no_emis[kgNha-1]",
#endif
#ifdef  OutputSoilchemistryDaily_Item__N2O
    "dN_n2o_emis[kgNha-1]",
#endif
#ifdef  OutputSoilchemistryDaily_Item__N2
    "dN_n2_emis[kgNha-1]",
#endif
#ifdef  OutputSoilchemistryDaily_Item__NH3
    "dN_nh3_emis[kgNha-1]",
#endif
#ifdef  OutputSoilchemistryDaily_Item__N2FIX
    "dN_n2_fix[kgNha-1]",
#endif
#ifdef  OutputSoilchemistryDaily_Item__CO2FIX
    "dC_co2_fix[kgCha-1]",
#endif
#ifdef  OutputSoilchemistryDaily_Item__SO4
    "dS_so4_leach[kgSha-1]",
#endif
#ifdef  OutputSoilchemistryDaily_Item__dCLIT
    "dC_litter[kgCha-1]",
#endif
#ifdef  OutputSoilchemistryDaily_Item__dNLIT
    "dN_litter[kgNha-1]",
#endif
#ifdef  OutputSoilchemistryDaily_Item__NDEP
    "dN_dep[kgNha-1]",
#endif
#ifdef  OutputSoilchemistryDaily_Item__NUPT_FLOOR
    "dN_up_floor[kgNha-1]",
#endif
#ifdef  OutputSoilchemistryDaily_Item__NUPT_MIN
    "dN_up_min[kgNha-1]",
#endif
#ifdef  OutputSoilchemistryDaily_Item__MINERALN_FLOOR
    "dN_mineral_floor[kgNha-1]",
#endif
#ifdef  OutputSoilchemistryDaily_Item__MINERALN_MIN
    "dN_mineral_min[kgNha-1]",
#endif
#ifdef  OutputSoilchemistryDaily_Item__NITRIFYN_FLOOR
    "dN_nitrify_floor[kgNha-1]",
#endif
#ifdef  OutputSoilchemistryDaily_Item__NITRIFYN_MIN
    "dN_nitrify_min[kgNha-1]",
#endif
#ifdef  OutputSoilchemistryDaily_Item__DENITRIFYN_FLOOR
    "dN_denitrify_floor[kgNha-1]",
#endif
#ifdef  OutputSoilchemistryDaily_Item__DENITRIFYN_EXTENDED_FLOOR
    "dN_denitrify_no2_floor[kgNha-1]",
    "dN_denitrify_no_floor[kgNha-1]",
    "dN_denitrify_n2o_floor[kgNha-1]",
#endif
#ifdef  OutputSoilchemistryDaily_Item__DENITRIFYN_MIN
    "dN_denitrify_min[kgNha-1]",
#endif
#ifdef  OutputSoilchemistryDaily_Item__DENITRIFYN_EXTENDED_MIN
    "dN_denitrify_no2_min[kgNha-1]",
    "dN_denitrify_no_min[kgNha-1]",
    "dN_denitrify_n2o_min[kgNha-1]",
#endif
#ifdef  OutputSoilchemistryDaily_Item__CHEMON_FLOOR
    "dN_chemo_floor[kgNha-1]",
#endif
#ifdef  OutputSoilchemistryDaily_Item__CHEMON_MIN
    "dN_chemo_min[kgNha-1]",
#endif
#ifdef  OutputSoilchemistryDaily_Item__PLANTC_FLOOR
    "C_plants_floor[kgCha-1]",
#endif
#ifdef  OutputSoilchemistryDaily_Item__PLANTC_MIN
    "C_plants_min[kgCha-1]",
#endif
#ifdef  OutputSoilchemistryDaily_Item__SOILC_FLOOR
    "C_soil_floor[kgCha-1]",
#endif
#ifdef  OutputSoilchemistryDaily_Item__SOILC_MIN
    "C_soil_min[kgCha-1]",
    "C_soil_min_20cm[kgCha-1]",
    "C_soil_min_30cm[kgCha-1]",
#endif
#ifdef  OutputSoilchemistryDaily_Item__AORGC_FLOOR
    "C_aorg_floor[kgCha-1]",
#endif
#ifdef  OutputSoilchemistryDaily_Item__AORGC_MIN
    "C_aorg_min[kgCha-1]",
#endif    
#ifdef  OutputSoilchemistryDaily_Item__CLIT_RAW
    "C_litter_raw[kgCha-1]",
#endif
#ifdef  OutputSoilchemistryDaily_Item__CLIT_FLOOR
    "C_litter_floor[kgCha-1]",
#endif
#ifdef  OutputSoilchemistryDaily_Item__CLIT_MIN
    "C_litter_min[kgCha-1]",
#endif
#ifdef  OutputSoilchemistryDaily_Item__MICROC_FLOOR
    "C_mic_floor[kgCha-1]",
#endif
#ifdef  OutputSoilchemistryDaily_Item__MICROC_MIN
    "C_mic_min[kgCha-1]",
#endif
#ifdef  OutputSoilchemistryDaily_Item__SOLC_FLOOR
    "C_sol_floor[kgCha-1]",
#endif
#ifdef  OutputSoilchemistryDaily_Item__SOLC_MIN
    "C_sol_min[kgCha-1]",
#endif
#ifdef  OutputSoilchemistryDaily_Item__CWOODABOVE
    "C_wood_above[kgCha-1]",
#endif
#ifdef  OutputSoilchemistryDaily_Item__CWOODBELOW
    "C_wood_below[kgCha-1]",
#endif
#ifdef  OutputSoilchemistryDaily_Item__PLANTN_FLOOR
    "N_plants_floor[kgNha-1]",
#endif
#ifdef  OutputSoilchemistryDaily_Item__PLANTN_MIN
    "N_plants_min[kgNha-1]",
#endif
#ifdef  OutputSoilchemistryDaily_Item__SOILN_FLOOR
    "N_soil_floor[kgNha-1]",
#endif
#ifdef  OutputSoilchemistryDaily_Item__SOILN_MIN
    "N_soil_min[kgNha-1]",
    "N_soil_min_20cm[kgNha-1]",
    "N_soil_min_30cm[kgNha-1]",
#endif
#ifdef  OutputSoilchemistryDaily_Item__AORGN_FLOOR
    "N_aorg_floor[kgNha-1]",
#endif
#ifdef  OutputSoilchemistryDaily_Item__AORGN_MIN
    "N_aorg_min[kgNha-1]",
#endif
#ifdef  OutputSoilchemistryDaily_Item__NLIT_RAW
    "N_litter_raw[kgNha-1]",
#endif
#ifdef  OutputSoilchemistryDaily_Item__NLIT_FLOOR
    "N_litter_floor[kgNha-1]",
#endif
#ifdef  OutputSoilchemistryDaily_Item__NLIT_MIN
    "N_litter_min[kgNha-1]",
#endif
#ifdef  OutputSoilchemistryDaily_Item__MICRON_FLOOR
    "N_mic_floor[kgNha-1]",
#endif
#ifdef  OutputSoilchemistryDaily_Item__MICRON_MIN
    "N_mic_min[kgNha-1]",
#endif
#ifdef  OutputSoilchemistryDaily_Item__SOLN_FLOOR
    "N_sol_floor[kgNha-1]",
#endif
#ifdef  OutputSoilchemistryDaily_Item__SOLN_MIN
    "N_sol_min[kgNha-1]",
#endif
#ifdef  OutputSoilchemistryDaily_Item__NWOODABOVE
    "N_wood_above[kgNha-1]",
#endif
#ifdef  OutputSoilchemistryDaily_Item__NWOODBELOW
    "N_wood_below[kgNha-1]",
#endif
#ifdef  OutputSoilchemistryDaily_Item__ANVF
    "anvf_mean[-]",
#endif    
};

#define  OutputSoilchemistryDaily_Datasize  (sizeof( OutputSoilchemistryDaily_Header) / sizeof( OutputSoilchemistryDaily_Header[0]))
ldndc_output_size_t const  OutputSoilchemistryDaily_Sizes[] =
{
    OutputSoilchemistryDaily_Datasize,

    OutputSoilchemistryDaily_Datasize /*total size*/
};
ldndc_output_size_t const *  OutputSoilchemistryDaily_EntitySizes = NULL;
#define  OutputSoilchemistryDaily_Rank  ((ldndc_output_rank_t)(sizeof( OutputSoilchemistryDaily_Sizes) / sizeof( OutputSoilchemistryDaily_Sizes[0])) - 1)
atomic_datatype_t const  OutputSoilchemistryDaily_Types[] =
{
    LDNDC_FLOAT64
};


LMOD_OUTPUT_MODULE_NAME::LMOD_OUTPUT_MODULE_NAME(
          MoBiLE_State *  _state,
          cbm::io_kcomm_t *  _io_kcomm,
          timemode_e  _timemode)
          : LMOD_OUTPUT_MODULE_BASE( _state, _io_kcomm, _timemode),

          io_kcomm( _io_kcomm),

          soillayers_in( _io_kcomm->get_input_class< input_class_soillayers_t >()),

          phys( _state->get_substate< substate_physiology_t >()),
          vegetation( &_state->vegetation),
          soilchem( _state->get_substate< substate_soilchemistry_t >())
{
    this->push_accumulated_outputs();
}


LMOD_OUTPUT_MODULE_NAME::~LMOD_OUTPUT_MODULE_NAME()
{
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::configure(
                ldndc::config_file_t const *  _cf)
{
    lerr_t  rc_setflags =
        this->set_metaflags( _cf, RM_DEFAULT_DAILY);
    if ( rc_setflags)
        { return  LDNDC_ERR_FAIL; }

    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::initialize()
{
    this->m_sink = io_kcomm->sink_handle_acquire( "soilchemistrydaily");
    if ( this->m_sink.status() == LDNDC_ERR_OK)
    {
        lerr_t  rc_layout = LDNDC_OUTPUT_DEFINE_SINK_LAYOUT_DEFAULTS(
            this->m_sink,OutputSoilchemistryDaily);
        RETURN_IF_NOT_OK(rc_layout);
    }
    else
    {
        KLOGERROR( "sink status bad  [sink=","soilchemistrydaily","]");
        return  m_sink.status();
    }

    this->push_accumulated_outputs();
    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::solve()
{
    ldndc_flt64_t  data_flt64_0[OutputSoilchemistryDaily_Datasize];

    this->pop_accumulated_outputs();
    lerr_t  rc_dump = dump_0( data_flt64_0);
    this->push_accumulated_outputs();

    if ( rc_dump)
        { return  LDNDC_ERR_FAIL; }

    void *  data[] = { data_flt64_0};
    lerr_t  rc_write =
        this->write_fixed_record( &this->m_sink, data);
    if ( rc_write)
        { return  LDNDC_ERR_FAIL; }

    return  LDNDC_ERR_OK;
}


void
LMOD_OUTPUT_MODULE_NAME::pop_accumulated_outputs()
{
    this->acc.nh4_throughfall =
               this->phys->accumulated_nh4_throughfall - this->acc.nh4_throughfall;
    this->acc.no3_throughfall =
               this->phys->accumulated_no3_throughfall - this->acc.no3_throughfall;

    this->acc.doc_leach =
               this->soilchem->accumulated_doc_leach - this->acc.doc_leach;
    this->acc.don_leach =
               this->soilchem->accumulated_don_leach - this->acc.don_leach;
    this->acc.no3_leach =
               this->soilchem->accumulated_no3_leach - this->acc.no3_leach;
    this->acc.nh4_leach =
               this->soilchem->accumulated_nh4_leach - this->acc.nh4_leach;

    this->acc.ch4_emis =
               this->soilchem->accumulated_ch4_emis - this->acc.ch4_emis;
    this->acc.co2_emis_auto =
               this->soilchem->accumulated_co2_emis_auto - this->acc.co2_emis_auto;
    this->acc.co2_emis_hetero =
               this->soilchem->accumulated_co2_emis_hetero - this->acc.co2_emis_hetero;
    this->acc.no_emis =
               this->soilchem->accumulated_no_emis - this->acc.no_emis;
    this->acc.n2o_emis =
               this->soilchem->accumulated_n2o_emis - this->acc.n2o_emis;
    this->acc.n2_emis =
               this->soilchem->accumulated_n2_emis - this->acc.n2_emis;
    this->acc.nh3_emis =
               this->soilchem->accumulated_nh3_emis - this->acc.nh3_emis;

    this->acc.so4_leach =
               this->soilchem->accumulated_so4_leach - this->acc.so4_leach;
    this->acc.n2_fix_algae =
               this->soilchem->accumulated_n_fix_algae - this->acc.n2_fix_algae;
    this->acc.n2_fix_bio =
               this->soilchem->accumulated_n2_fixation - this->acc.n2_fix_bio;
    this->acc.co2_fix =
                this->soilchem->accumulated_c_fix_algae - this->acc.co2_fix;

    this->acc.c_litter =
               (this->soilchem->accumulated_c_litter_stubble+this->soilchem->accumulated_c_litter_wood_above
                +this->soilchem->accumulated_c_litter_below_sl.sum()+this->soilchem->accumulated_c_litter_wood_below_sl.sum()) - this->acc.c_litter;
    this->acc.n_litter =
               (this->soilchem->accumulated_n_litter_stubble+this->soilchem->accumulated_n_litter_wood_above
                +this->soilchem->accumulated_n_litter_below_sl.sum()+this->soilchem->accumulated_n_litter_wood_below_sl.sum()) - this->acc.n_litter;

    this->acc.uptakeN_floor = -this->acc.uptakeN_floor;
    this->acc.mineralN_floor = -this->acc.mineralN_floor;
    this->acc.nitrifyN_floor = -this->acc.nitrifyN_floor;
    this->acc.denitrifyN_floor = -this->acc.denitrifyN_floor;
    this->acc.denitrify_no2_floor = -this->acc.denitrify_no2_floor;
    this->acc.denitrify_no_floor = -this->acc.denitrify_no_floor;
    this->acc.denitrify_n2o_floor = -this->acc.denitrify_n2o_floor;
    this->acc.chemoN_floor = -this->acc.chemoN_floor;

    this->acc.uptakeN_min = -this->acc.uptakeN_min;
    this->acc.mineralN_min = -this->acc.mineralN_min;
    this->acc.nitrifyN_min = -this->acc.nitrifyN_min;
    this->acc.denitrifyN_min = -this->acc.denitrifyN_min;
    this->acc.denitrify_no2_min = -this->acc.denitrify_no2_min;
    this->acc.denitrify_no_min = -this->acc.denitrify_no_min;
    this->acc.denitrify_n2o_min = -this->acc.denitrify_n2o_min;
    this->acc.chemoN_min = -this->acc.chemoN_min;

    for ( size_t  sl = 0;  sl < this->soillayers_in->soil_layers_in_litter_cnt();  ++sl)
    {
        this->acc.uptakeN_floor += phys->accumulated_nh4_uptake_sl[sl] +
                                   phys->accumulated_no3_uptake_sl[sl] +
                                   phys->accumulated_nh3_uptake_sl[sl] +
                                   phys->accumulated_don_uptake_sl[sl];
        this->acc.mineralN_floor += soilchem->accumulated_n_mineral_sl[sl];
        this->acc.nitrifyN_floor += soilchem->accumulated_n_nitrify_sl[sl];
        this->acc.denitrifyN_floor += soilchem->accumulated_no3_denitrify_sl[sl];
        this->acc.denitrify_no2_floor += soilchem->accumulated_no2_denitrify_sl[sl];
        this->acc.denitrify_no_floor += soilchem->accumulated_no_denitrify_sl[sl];
        this->acc.denitrify_n2o_floor += soilchem->accumulated_n2o_denitrify_sl[sl];
        this->acc.chemoN_floor += soilchem->accumulated_n_chemodenitrify_sl[sl];
    }
    for ( size_t  sl = this->soillayers_in->soil_layers_in_litter_cnt(); sl < this->soillayers_in->soil_layer_cnt();  ++sl)
    {
        this->acc.uptakeN_min += phys->accumulated_nh4_uptake_sl[sl] +
                                 phys->accumulated_no3_uptake_sl[sl] +
                                 phys->accumulated_nh3_uptake_sl[sl] +
                                 phys->accumulated_don_uptake_sl[sl];
        this->acc.mineralN_min += soilchem->accumulated_n_mineral_sl[sl];
        this->acc.nitrifyN_min += soilchem->accumulated_n_nitrify_sl[sl];
        this->acc.denitrifyN_min += soilchem->accumulated_no3_denitrify_sl[sl];
        this->acc.denitrify_no2_min += soilchem->accumulated_no2_denitrify_sl[sl];
        this->acc.denitrify_no_min += soilchem->accumulated_no_denitrify_sl[sl];
        this->acc.denitrify_n2o_min += soilchem->accumulated_n2o_denitrify_sl[sl];
        this->acc.chemoN_min += soilchem->accumulated_n_chemodenitrify_sl[sl];
    }
}


void
LMOD_OUTPUT_MODULE_NAME::push_accumulated_outputs()
{
    this->acc.nh4_throughfall = this->phys->accumulated_nh4_throughfall;
    this->acc.no3_throughfall = this->phys->accumulated_no3_throughfall;

    this->acc.doc_leach = this->soilchem->accumulated_doc_leach;
    this->acc.don_leach = this->soilchem->accumulated_don_leach;
    this->acc.no3_leach = this->soilchem->accumulated_no3_leach;
    this->acc.nh4_leach = this->soilchem->accumulated_nh4_leach;

    this->acc.ch4_emis = this->soilchem->accumulated_ch4_emis;
    this->acc.co2_emis_auto = this->soilchem->accumulated_co2_emis_auto;
    this->acc.co2_emis_hetero = this->soilchem->accumulated_co2_emis_hetero;
    this->acc.no_emis = this->soilchem->accumulated_no_emis;
    this->acc.n2o_emis = this->soilchem->accumulated_n2o_emis;
    this->acc.n2_emis = this->soilchem->accumulated_n2_emis;
    this->acc.nh3_emis = this->soilchem->accumulated_nh3_emis;

    this->acc.so4_leach = this->soilchem->accumulated_so4_leach;

    this->acc.n2_fix_algae = this->soilchem->accumulated_n_fix_algae;
    this->acc.n2_fix_bio = this->soilchem->accumulated_n2_fixation;
    this->acc.co2_fix = this->soilchem->accumulated_c_fix_algae;

    this->acc.c_litter = this->soilchem->accumulated_c_litter_stubble+this->soilchem->accumulated_c_litter_wood_above
                        + this->soilchem->accumulated_c_litter_below_sl.sum() + this->soilchem->accumulated_c_litter_wood_below_sl.sum();
    this->acc.n_litter = this->soilchem->accumulated_n_litter_stubble+this->soilchem->accumulated_n_litter_wood_above
                        + this->soilchem->accumulated_n_litter_below_sl.sum() + this->soilchem->accumulated_n_litter_wood_below_sl.sum();

    this->acc.uptakeN_floor = 0.0;
    this->acc.mineralN_floor = 0.0;
    this->acc.nitrifyN_floor = 0.0;
    this->acc.denitrifyN_floor = 0.0;
    this->acc.denitrify_no2_floor = 0.0;
    this->acc.denitrify_no_floor = 0.0;
    this->acc.denitrify_n2o_floor = 0.0;
    this->acc.chemoN_floor = 0.0;

    this->acc.uptakeN_min = 0.0;
    this->acc.mineralN_min = 0.0;
    this->acc.nitrifyN_min = 0.0;
    this->acc.denitrifyN_min = 0.0;
    this->acc.denitrify_no2_min = 0.0;
    this->acc.denitrify_no_min = 0.0;
    this->acc.denitrify_n2o_min = 0.0;
    this->acc.chemoN_min = 0.0;

    for ( size_t  sl = 0;  sl < this->soillayers_in->soil_layers_in_litter_cnt();  ++sl)
    {
        this->acc.uptakeN_floor += phys->accumulated_nh4_uptake_sl[sl] +
                                   phys->accumulated_no3_uptake_sl[sl] +
                                   phys->accumulated_nh3_uptake_sl[sl] +
                                   phys->accumulated_don_uptake_sl[sl];
        this->acc.mineralN_floor += soilchem->accumulated_n_mineral_sl[sl];
        this->acc.nitrifyN_floor += soilchem->accumulated_n_nitrify_sl[sl];
        this->acc.denitrifyN_floor += soilchem->accumulated_no3_denitrify_sl[sl];
        this->acc.denitrify_no2_floor += soilchem->accumulated_no2_denitrify_sl[sl];
        this->acc.denitrify_no_floor += soilchem->accumulated_no_denitrify_sl[sl];
        this->acc.denitrify_n2o_floor += soilchem->accumulated_n2o_denitrify_sl[sl];
        this->acc.chemoN_floor += soilchem->accumulated_n_chemodenitrify_sl[sl];
    }
    for ( size_t  sl = this->soillayers_in->soil_layers_in_litter_cnt(); sl < this->soillayers_in->soil_layer_cnt();  ++sl)
    {
        this->acc.uptakeN_min += phys->accumulated_nh4_uptake_sl[sl] +
                                 phys->accumulated_no3_uptake_sl[sl] +
                                 phys->accumulated_nh3_uptake_sl[sl] +
                                 phys->accumulated_don_uptake_sl[sl];
        this->acc.mineralN_min += soilchem->accumulated_n_mineral_sl[sl];
        this->acc.nitrifyN_min += soilchem->accumulated_n_nitrify_sl[sl];
        this->acc.denitrifyN_min += soilchem->accumulated_no3_denitrify_sl[sl];
        this->acc.denitrify_no2_min += soilchem->accumulated_no2_denitrify_sl[sl];
        this->acc.denitrify_no_min += soilchem->accumulated_no_denitrify_sl[sl];
        this->acc.denitrify_n2o_min += soilchem->accumulated_n2o_denitrify_sl[sl];
        this->acc.chemoN_min += soilchem->accumulated_n_chemodenitrify_sl[sl];
    }
}

lerr_t
LMOD_OUTPUT_MODULE_NAME::finalize()
{
    io_kcomm->sink_handle_release( &m_sink);
    return  LDNDC_ERR_OK;
}


lerr_t
LMOD_OUTPUT_MODULE_NAME::dump_0(
        ldndc_flt64_t *  _buf)
{
    /*! conversion from kg m-2 to kg ha-1 */
    static const double  p( cbm::M2_IN_HA);

    double  plantC_floor( 0.0);
    double  plantC_min( 0.0);
    double  soilC_floor( 0.0);
    double  soilC_min( 0.0);
    double  soilC_min_20( 0.0);
    double  soilC_min_30( 0.0);
    double  aorgC_floor( 0.0);
    double  aorgC_min( 0.0);
    double  litC_floor( 0.0);
    double  litC_min( 0.0);
    double  microC_floor( 0.0);
    double  microC_min( 0.0);
    double  solC_floor( 0.0);
    double  solC_min( 0.0);

    double  plantN_floor( 0.0);
    double  plantN_min( 0.0);
    double  soilN_floor( 0.0);
    double  soilN_min( 0.0);
    double  soilN_min_20( 0.0);
    double  soilN_min_30( 0.0);
    double  aorgN_floor( 0.0);
    double  aorgN_min( 0.0);
    double  litN_floor( 0.0);
    double  litN_min( 0.0);    
    double  microN_floor( 0.0);
    double  microN_min( 0.0);
    double  solN_floor( 0.0);
    double  solN_min( 0.0);

    double c_lit_raw( soilchem->c_raw_lit_1_above + soilchem->c_raw_lit_2_above + soilchem->c_raw_lit_3_above);
    double n_lit_raw( soilchem->n_raw_lit_1_above + soilchem->n_raw_lit_2_above + soilchem->n_raw_lit_3_above);

    for( PlantIterator  vt = vegetation->begin(); vt != vegetation->end(); ++vt)
    {
        double const plantC( (*vt)->belowground_biomass() * cbm::CCDM);
        double const plantN( (*vt)->belowground_nitrogen());
        for ( size_t  sl = 0;  sl < soillayers_in->soil_layer_cnt();  ++sl)
        {
            if ( sl < soillayers_in->soil_layers_in_litter_cnt())
            {
                plantC_floor += plantC * (*vt)->fFrt_sl[sl];
                plantN_floor += plantN * (*vt)->fFrt_sl[sl];
            }
            else
            {
                plantC_min += plantC * (*vt)->fFrt_sl[sl];
                plantN_min += plantN * (*vt)->fFrt_sl[sl];
            }
        }
    }
    
    for ( size_t  sl = 0;  sl < soillayers_in->soil_layer_cnt();  ++sl)
    {        
        double const  microC_sl( soilchem->C_micro1_sl[sl] + soilchem->C_micro2_sl[sl] + soilchem->C_micro3_sl[sl]);
        double const  solC_sl( soilchem->doc_sl[sl] + soilchem->an_doc_sl[sl]);
        double const  litC_sl( soilchem->C_lit1_sl[sl] + soilchem->C_lit2_sl[sl] + soilchem->C_lit3_sl[sl]);
        double const  c_lit_raw_sl( soilchem->c_raw_lit_1_sl[sl] + soilchem->c_raw_lit_2_sl[sl] + soilchem->c_raw_lit_3_sl[sl]);
        double const  soilC_sl( microC_sl + solC_sl + litC_sl + c_lit_raw_sl + soilchem->C_aorg_sl[sl] + soilchem->C_hum_sl[sl]);

        double const  solN_sl( soilchem->nh4_sl[sl] + soilchem->don_sl[sl]
                              + soilchem->nh3_liq_sl[sl] + soilchem->urea_sl[sl]
                              + soilchem->no3_sl[sl] + soilchem->an_no3_sl[sl]);
        double const  n_lit_raw_sl( soilchem->n_raw_lit_1_sl[sl] + soilchem->n_raw_lit_2_sl[sl] + soilchem->n_raw_lit_3_sl[sl]);
        double const  soilN_sl( soilchem->N_hum_sl[sl]
                               + soilchem->N_aorg_sl[sl]
                               + soilchem->N_lit1_sl[sl]+soilchem->N_lit2_sl[sl]+soilchem->N_lit3_sl[sl] + n_lit_raw_sl
                               + soilchem->N_micro_sl[sl]
                               + solN_sl);

        c_lit_raw += c_lit_raw_sl;
        n_lit_raw += n_lit_raw_sl;

        if ( sl < soillayers_in->soil_layers_in_litter_cnt())
        {
            soilC_floor    += soilC_sl;
            aorgC_floor    += soilchem->C_aorg_sl[sl];
            litC_floor     += litC_sl;
            microC_floor   += microC_sl;
            solC_floor     += solC_sl;
            
            soilN_floor    += soilN_sl;
            aorgN_floor    += soilchem->N_aorg_sl[sl];
            litN_floor     += (soilchem->N_lit1_sl[sl]+soilchem->N_lit2_sl[sl]+soilchem->N_lit3_sl[sl]);
            microN_floor   += soilchem->N_micro_sl[sl];
            solN_floor     += solN_sl;
        }
        else
        {
            soilC_min    += soilC_sl;
            aorgC_min    += soilchem->C_aorg_sl[sl];
            litC_min     += litC_sl;
            microC_min   += microC_sl;
            solC_min     += solC_sl;
            
            soilN_min    += soilN_sl;
            aorgN_min    += soilchem->N_aorg_sl[sl];
            litN_min     += (soilchem->N_lit1_sl[sl]+soilchem->N_lit2_sl[sl]+soilchem->N_lit3_sl[sl]);
            microN_min   += soilchem->N_micro_sl[sl];
            solN_min     += solN_sl;

            //above 0.2m
            if ( cbm::flt_less_equal( soilchem->depth_sl[sl], 0.2))
            {
                soilC_min_20 += soilC_sl;
                soilN_min_20 += soilN_sl;
            }
            
            //above 0.3cm
            if ( cbm::flt_less_equal( soilchem->depth_sl[sl], 0.3))
            {
                soilC_min_30 += soilC_sl;
                soilN_min_30 += soilN_sl;
            }
            else if ( sl > 1)
            {
                //check if last soil layer was below 0.3m
                if ( cbm::flt_less( soilchem->depth_sl[sl-1], 0.3))
                {
                    double const scale( (0.3 - soilchem->depth_sl[sl-1]) / (soilchem->depth_sl[sl] - soilchem->depth_sl[sl-1]));
                    soilC_min_30 += soilC_sl * scale;
                    soilN_min_30 += soilN_sl * scale;
                }
            }
            //scale C and N since first soil layer larger than 0.3m
            else
            {
                double const scale( 0.3 / soilchem->depth_sl[sl]);
                soilC_min_30 += soilC_sl * scale;
                soilN_min_30 += soilN_sl * scale;
            }
        }
    }

    LDNDC_OUTPUT_SET_COLUMN_INDEX(0);

#ifdef  OutputSoilchemistryDaily_Item__CH4
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * this->acc.ch4_emis);
#endif
#ifdef  OutputSoilchemistryDaily_Item__CO2f
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * this->acc.co2_emis_auto);
#endif
#ifdef  OutputSoilchemistryDaily_Item__CO2s
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * this->acc.co2_emis_hetero);
#endif
#ifdef  OutputSoilchemistryDaily_Item__DOC
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * this->acc.doc_leach);
#endif
#ifdef  OutputSoilchemistryDaily_Item__DON
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * this->acc.don_leach);
#endif
#ifdef  OutputSoilchemistryDaily_Item__NO3
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * this->acc.no3_leach);
#endif
#ifdef  OutputSoilchemistryDaily_Item__NH4
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * this->acc.nh4_leach);
#endif
#ifdef  OutputSoilchemistryDaily_Item__NO
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * this->acc.no_emis);
#endif
#ifdef  OutputSoilchemistryDaily_Item__N2O
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * this->acc.n2o_emis);
#endif
#ifdef  OutputSoilchemistryDaily_Item__N2
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * this->acc.n2_emis);
#endif
#ifdef  OutputSoilchemistryDaily_Item__NH3
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * this->acc.nh3_emis);
#endif
#ifdef  OutputSoilchemistryDaily_Item__N2FIX
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * (this->acc.n2_fix_algae + this->acc.n2_fix_bio));
#endif
#ifdef  OutputSoilchemistryDaily_Item__CO2FIX
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * this->acc.co2_fix);
#endif
#ifdef  OutputSoilchemistryDaily_Item__SO4
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * this->acc.so4_leach);
#endif
#ifdef  OutputSoilchemistryDaily_Item__dCLIT
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * this->acc.c_litter);
#endif
#ifdef  OutputSoilchemistryDaily_Item__dNLIT
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * this->acc.n_litter);
#endif
#ifdef  OutputSoilchemistryDaily_Item__NDEP
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * ( this->acc.nh4_throughfall + this->acc.no3_throughfall));
#endif
#ifdef  OutputSoilchemistryDaily_Item__NUPT_FLOOR
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * this->acc.uptakeN_floor);
#endif
#ifdef  OutputSoilchemistryDaily_Item__NUPT_MIN
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * this->acc.uptakeN_min);
#endif
#ifdef  OutputSoilchemistryDaily_Item__MINERALN_FLOOR
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * this->acc.mineralN_floor);
#endif
#ifdef  OutputSoilchemistryDaily_Item__MINERALN_MIN
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * this->acc.mineralN_min);
#endif
#ifdef  OutputSoilchemistryDaily_Item__NITRIFYN_FLOOR
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * this->acc.nitrifyN_floor);
#endif
#ifdef  OutputSoilchemistryDaily_Item__NITRIFYN_MIN
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * this->acc.nitrifyN_min);
#endif
#ifdef  OutputSoilchemistryDaily_Item__DENITRIFYN_FLOOR
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * this->acc.denitrifyN_floor);
#endif
#ifdef  OutputSoilchemistryDaily_Item__DENITRIFYN_EXTENDED_FLOOR
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * this->acc.denitrify_no2_floor);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * this->acc.denitrify_no_floor);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * this->acc.denitrify_n2o_floor);
#endif
#ifdef  OutputSoilchemistryDaily_Item__DENITRIFYN_MIN
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * this->acc.denitrifyN_min);
#endif
#ifdef  OutputSoilchemistryDaily_Item__DENITRIFYN_EXTENDED_MIN
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * this->acc.denitrify_no2_min);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * this->acc.denitrify_no_min);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * this->acc.denitrify_n2o_min);
#endif
#ifdef  OutputSoilchemistryDaily_Item__CHEMON_FLOOR
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * this->acc.chemoN_floor);
#endif
#ifdef  OutputSoilchemistryDaily_Item__CHEMON_MIN
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * this->acc.chemoN_min);
#endif
#ifdef  OutputSoilchemistryDaily_Item__PLANTC_FLOOR
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * plantC_floor);
#endif
#ifdef  OutputSoilchemistryDaily_Item__PLANTC_MIN
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * plantC_min);
#endif
#ifdef  OutputSoilchemistryDaily_Item__SOILC_FLOOR
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * soilC_floor);
#endif
#ifdef  OutputSoilchemistryDaily_Item__SOILC_MIN
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * soilC_min);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * soilC_min_20);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * soilC_min_30);
#endif
#ifdef  OutputSoilchemistryDaily_Item__AORGC_FLOOR
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * aorgC_floor);
#endif
#ifdef  OutputSoilchemistryDaily_Item__AORGC_MIN
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * aorgC_min);
#endif    
#ifdef  OutputSoilchemistryDaily_Item__CLIT_RAW
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * c_lit_raw);
#endif
#ifdef  OutputSoilchemistryDaily_Item__CLIT_FLOOR
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * litC_floor);
#endif
#ifdef  OutputSoilchemistryDaily_Item__CLIT_MIN
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * litC_min);
#endif
#ifdef  OutputSoilchemistryDaily_Item__MICROC_FLOOR
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * microC_floor);
#endif
#ifdef  OutputSoilchemistryDaily_Item__MICROC_MIN
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * microC_min);
#endif
#ifdef  OutputSoilchemistryDaily_Item__SOLC_FLOOR
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * solC_floor);
#endif
#ifdef  OutputSoilchemistryDaily_Item__SOLC_MIN
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * solC_min);
#endif
#ifdef  OutputSoilchemistryDaily_Item__CWOODABOVE
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * soilchem->c_wood);
#endif
#ifdef  OutputSoilchemistryDaily_Item__CWOODBELOW
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * soilchem->c_wood_sl.sum());
#endif
#ifdef  OutputSoilchemistryDaily_Item__PLANTN_FLOOR
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * plantN_floor);
#endif
#ifdef  OutputSoilchemistryDaily_Item__PLANTN_MIN
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * plantN_min);
#endif
#ifdef  OutputSoilchemistryDaily_Item__SOILN_FLOOR
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * soilN_floor);
#endif
#ifdef  OutputSoilchemistryDaily_Item__SOILN_MIN
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * soilN_min);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * soilN_min_20);
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * soilN_min_30);
#endif
#ifdef  OutputSoilchemistryDaily_Item__AORGN_FLOOR
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * aorgN_floor);
#endif
#ifdef  OutputSoilchemistryDaily_Item__AORGN_MIN
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * aorgN_min);
#endif
#ifdef  OutputSoilchemistryDaily_Item__NLIT_RAW
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * n_lit_raw);
#endif
#ifdef  OutputSoilchemistryDaily_Item__NLIT_FLOOR
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * litN_floor);
#endif
#ifdef  OutputSoilchemistryDaily_Item__NLIT_MIN
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * litN_min);
#endif
#ifdef  OutputSoilchemistryDaily_Item__MICRON_FLOOR
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * microN_floor);
#endif
#ifdef  OutputSoilchemistryDaily_Item__MICRON_MIN
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * microN_min);
#endif
#ifdef  OutputSoilchemistryDaily_Item__SOLN_FLOOR
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * solN_floor);
#endif
#ifdef  OutputSoilchemistryDaily_Item__SOLN_MIN
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * solN_min);
#endif
#ifdef  OutputSoilchemistryDaily_Item__CWOODABOVE
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * soilchem->n_wood);
#endif
#ifdef  OutputSoilchemistryDaily_Item__CWOODBELOW
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(p * soilchem->n_wood_sl.sum());
#endif
#ifdef  OutputSoilchemistryDaily_Item__ANVF
    LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(soilchem->anvf_sl.mean());
#endif

    return  LDNDC_ERR_OK;
}


#undef  LMOD_OUTPUT_MODULE_BASE
#undef  LMOD_OUTPUT_MODULE_NAME

#undef  OutputSoilchemistryDaily_Rank
#undef  OutputSoilchemistryDaily_Datasize

} /*namespace ldndc*/

