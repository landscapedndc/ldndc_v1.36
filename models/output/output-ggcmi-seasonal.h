/*!
 * @brief
 *    seasonal outputs needed for ggcmi project
 *
 * @author
 *    Andrew Smerald
 */

#ifndef  LM_OUTPUT_GGCMI_SEASONAL_H_NEW_
#define  LM_OUTPUT_GGCMI_SEASONAL_H_NEW_

#include  "mbe_legacyoutputmodel.h"
#include  "state/mbe_state.h"

#define  LMOD_OUTPUT_MODULE_BASE  MBE_LegacyOutputModel
#define  LMOD_OUTPUT_MODULE_NAME  OutputGgcmiSeasonal
#define  LMOD_OUTPUT_MODULE_ID    "output:ggcmi:seasonal"
#define  LMOD_OUTPUT_MODULE_DESC  "GGCMI Seasonal Output"
namespace ldndc {
class  substate_microclimate_t;
class  substate_physiology_t;
class  substate_watercycle_t;
class  LDNDC_API  LMOD_OUTPUT_MODULE_NAME  :  public  LMOD_OUTPUT_MODULE_BASE
{
    LMOD_EXPORT_MODULE_INFO(LMOD_OUTPUT_MODULE_NAME,LMOD_OUTPUT_MODULE_ID,LMOD_OUTPUT_MODULE_DESC);
    public:
        LMOD_OUTPUT_MODULE_NAME(
                MoBiLE_State *,
                cbm::io_kcomm_t *,
                timemode_e  _timemode);

        ~LMOD_OUTPUT_MODULE_NAME();


        lerr_t  configure( ldndc::config_file_t const *);

        lerr_t  initialize();

        lerr_t  solve();

        lerr_t  finalize();

        lerr_t  wake() { return  LDNDC_ERR_OK; }
        lerr_t  sleep() { return  LDNDC_ERR_OK; }

    private:
        lerr_t  dump_0(
                ldndc_flt64_t *);

    private:
        cbm::io_kcomm_t *  io_kcomm;

        input_class_soillayers_t const *  soillayers_in;

        substate_physiology_t const *  phys;
        substate_soilchemistry_t const *  soilchem;
        substate_watercycle_t const *  water;
        MoBiLE_PlantVegetation *  m_veg;

    private:
        ldndc::sink_handle_t  m_sink;

        struct  output_ggcmi_daily_acc_t
        {
            double nh3_uptake;
            double nh4_uptake;
            double no3_uptake;
            double don_uptake;

            double  nh4_throughfall;
            double  no3_throughfall;

            double  don_leach;
            double  no3_leach;
            double  nh4_leach;

            double  ch4_emis;
            double  co2_emis_auto;
            double  co2_emis_hetero;
            double  no_emis;
            double  n2o_emis;
            double  n2_emis;
            double  nh3_emis;

            double  n2_fix_algae;
            double  n2_fix_bio;

            double  irrigation;
            double  transpiration;
            double  interceptionevaporation;
            double  soilevaporation;
            double  surfacewaterevaporation;
            double  runoff;
            double  percolation;
        };
        output_ggcmi_daily_acc_t  acc;
        void  pop_accumulated_outputs();
        void  push_accumulated_outputs();

        struct  output_ggcmi_seasonal_acc_t
        {
            double nh3_uptake;
            double nh4_uptake;
            double no3_uptake;
            double don_uptake;

            double  nh4_throughfall;
            double  no3_throughfall;

            double  don_leach;
            double  no3_leach;
            double  nh4_leach;

            double  ch4_emis;
            double  co2_emis_auto;
            double  co2_emis_hetero;
            double  no_emis;
            double  n2o_emis;
            double  n2_emis;
            double  nh3_emis;

            double  n2_fix_algae;
            double  n2_fix_bio;
            double  n2_fix_plant;

            double  irrigation;
            double  transpiration;
            double  interceptionevaporation;
            double  soilevaporation;
            double  surfacewaterevaporation;
            double  runoff;
            double  percolation;
        };
        output_ggcmi_seasonal_acc_t  acc_seas;

        void  reset_seasonal_outputs();
        void  update_seasonal_outputs();


        int plant_flag;
        int dplant_flag;
        int planting_year;
        int planting_yearday;
        int anthesis_yearday;
        int harvest_yearday;

        double dvs_val;
        double gdd_val;

};
} /*namespace ldndc*/


#undef  LMOD_OUTPUT_MODULE_BASE
#undef  LMOD_OUTPUT_MODULE_NAME
#undef  LMOD_OUTPUT_MODULE_ID
#undef  LMOD_OUTPUT_MODULE_DESC


#endif  /*  !LM_OUTPUT_SOILCHEMISTRY_DAILY_H_  */
