/*!
 * @brief
 *  base header for LandscapeDNDC kernels
 *
 * @author
 *  steffen klatt (created on: nov. 18, 2016)
 */

#ifndef  LD_LEGACY_H_
#define  LD_LEGACY_H_


#include  "ldndc-dllexport.h"
#include  "ldndc-config.h.inc"
#include  "ld_modelsconfig.h"

#include  "legacy/mbe_config.h.inc"

using  namespace ldndc;

#include  <input/airchemistry/airchemistry.h>
using  namespace ldndc::airchemistry;
#include  <input/checkpoint/checkpoint.h>
using  namespace ldndc::checkpoint;
#include  <input/climate/climate.h>
using  namespace ldndc::climate;
#include  <input/event/event.h>
using  namespace ldndc::event;
#include  <input/groundwater/groundwater.h>
using  namespace ldndc::groundwater;
#include  <input/setup/setup.h>
using  namespace ldndc::setup;
#include  <input/site/site.h>
using  namespace ldndc::site;
#include  <input/siteparameters/siteparameters.h>
using  namespace ldndc::siteparameters;
#include  <input/soillayers/soillayers.h>
using  namespace ldndc::soillayers;
#include  <input/soilparameters/soilparameters.h>
using  namespace ldndc::soilparameters;
#include  <input/species/species.h>
using  namespace ldndc::species;
#include  <input/speciesparameters/speciesparameters.h>
using  namespace ldndc::speciesparameters;

#define  MoBiLE_YearDoYOffset(clk,latitude)  (((latitude)>0.0) ? 1u : 182u)
#define  MoBiLE_YearDoYEnd(clk,latitude)  ((clk)->days_in_year())
#define  MoBiLE_IsBeginningOfYear(clk,latitude) \
    ( ((clk)->day()==1) \
    && (((this->timemode() & (TMODE_SUBDAILY|TMODE_PRE_DAILY|TMODE_PRE_YEARLY)) && ((clk)->subday()==1)) \
           || ((this->timemode() & (TMODE_POST_DAILY|TMODE_POST_YEARLY)) && ((int)(clk)->subday()==(int)(clk)->time_resolution()))) \
    && (((latitude)>0.0) ? ((clk)->month()==1) : ((clk)->month()==7)))

/* MoBiLE objects */
#define  MOBILE_OBJECT(__type__)                  \
    LDNDC_OBJECT(__type__)                        \
    private: static char const * const  kname;    \
    public: char const *  name() const { return  kname;}

#define  MOBILE_OBJECT_DEFN(__type__,__kname__)   \
    LDNDC_OBJECT_DEFN(ldndc::__type__)   \
    char const * const  ldndc::__type__::kname = __kname__;

#include  <kernel/io-kcomm.h>
namespace ldndc {
    typedef cbm::io_kcomm_t io_kcomm_t; }

#endif /* !LD_LEGACY_H_ */

