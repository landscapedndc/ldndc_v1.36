/*!
 * @brief
 *    common configuration and execution interface
 *    for sinks.
 *
 * @author
 *    steffen klatt (created on: oct 18, 2016)
 */

#ifndef  LD_SINKINTERFACE_H_
#define  LD_SINKINTERFACE_H_

#include  "ld_modelsconfig.h"

#include  <cbm_types.h>
#include  <io/outputtypes.h>
#include  <io/sink-handle.h>

namespace ldndc {
    class  config_file_t;

/* output helpers */

#define  LDNDC_OUTPUT_SET_COLUMN_INDEX(__i0__)  size_t  _____append_column_index = (__i0__)/*;*/
#define  LDNDC_OUTPUT_GET_CURRENT_COLUMN  (_____append_column_index)
#define  LDNDC_OUTPUT_GET_CURRENT_COLUMN_VALUE(__buf__)  ((__buf__)[LDNDC_OUTPUT_GET_CURRENT_COLUMN])
#define  LDNDC_OUTPUT_SET_COLUMN_(__val__,__op__,__buf__,__inc__) \
    if ( __buf__) \
    { (__buf__)[LDNDC_OUTPUT_GET_CURRENT_COLUMN] __op__ (__val__); } \
    /*KLOGDEBUG( #__buf__ "@",(void*)__buf__,"\t[I=",LDNDC_OUTPUT_GET_CURRENT_COLUMN,",op='",#__op__,"',i=",__inc__,"]");*/\
    LDNDC_OUTPUT_GET_CURRENT_COLUMN += __inc__/*;*/

#define  LDNDC_OUTPUT_SET_COLUMN_AND_GOTO_NEXT(__val__)  LDNDC_OUTPUT_SET_COLUMN_((__val__),=,_buf,1)

#define  LDNDC_OUTPUT_SET_COLUMN_AVG_SUM(__val__)  LDNDC_OUTPUT_SET_COLUMN_((__val__),+=,_buf_avg,0)
#define  LDNDC_OUTPUT_SET_COLUMN_AVG_SET(__val__)  LDNDC_OUTPUT_SET_COLUMN_((__val__),=,_buf_avg,0)
#define  LDNDC_OUTPUT_SET_COLUMN_AVG_MAX(__val__) \
    if ( _buf_avg) \
    { \
        ldndc_flt64_t  v = std::max( (ldndc_flt64_t)(__val__), (ldndc_flt64_t)LDNDC_OUTPUT_GET_CURRENT_COLUMN_VALUE(_buf_avg)); \
        LDNDC_OUTPUT_SET_COLUMN_AVG_SET(v); \
    } \
    (void)_buf_avg/*;*/
#define  LDNDC_OUTPUT_SET_COLUMN_AVG_MIN(__val__) \
    if ( _buf_avg) \
    { \
        ldndc_flt64_t  v = std::min( (ldndc_flt64_t)(__val__), (ldndc_flt64_t)LDNDC_OUTPUT_GET_CURRENT_COLUMN_VALUE(_buf_avg)); \
        LDNDC_OUTPUT_SET_COLUMN_AVG_SET(v); \
    } \
    (void)_buf_avg/*;*/

#define  LDNDC_OUTPUT_DEFINE_SINK_LAYOUT_(__sink__,__name__,__Ids__,__useregs__) \
    LDNDC_ERR_OK; \
    \
    sink_entities_t  __name__##_Entities = sink_entities_defaults; \
    __name__##_Entities.size = __name__##_Sizes[__name__##_Rank]; \
    __name__##_Entities.ids = __name__##__Ids__; \
    __name__##_Entities.names = __name__##_Header; \
    \
    sink_layout_t  __name__##_Layout = sink_layout_defaults; \
    __name__##_Layout.rank = __name__##_Rank; \
    __name__##_Layout.sizes = __name__##_Sizes; \
    __name__##_Layout.types = __name__##_Types; \
    __name__##_Layout.entsizes = __name__##_EntitySizes; \
    \
    sink_entity_layout_t  s_layout = sink_entity_layout_defaults; \
    s_layout.ents = __name__##_Entities; \
    s_layout.layout = __name__##_Layout; \
    \
    sink_record_meta_t  record_meta; \
    record_meta.useregs = __useregs__; \
    rc_layout = __sink__.set_fixed_layout( &record_meta, &s_layout);

#define  LDNDC_SET_SOURCE_IDENTIFIER \
    input_class_setup_t const *  __setup_in = \
        this->io_kcomm->get_input_class< setup::input_class_setup_t >(); \
    if ( !__setup_in) { return  LDNDC_ERR_FAIL; } \
    this->client.target_id = __setup_in->object_id(); \
    cbm::source_descriptor_t  __source_info; \
    __setup_in->block_consume( &__source_info, __setup_in->input_class_type()); \
    cbm::as_strcpy( this->client.target_sid, __source_info.source_identifier)/*;*/

#define  LDNDC_OUTPUT_DEFINE_SINK_LAYOUT_W_(__sink__,__name__,__Ids__,__useregs__) \
    LDNDC_ERR_OK; \
    LDNDC_SET_SOURCE_IDENTIFIER; \
    rc_layout = LDNDC_OUTPUT_DEFINE_SINK_LAYOUT_(__sink__,__name__,_Ids,RM_CLIENTID|RM_CLIENTSOURCE|__useregs__)

#define  LDNDC_OUTPUT_DEFINE_SINK_LAYOUT(__sink__,__name__,__useregs__) \
    LDNDC_OUTPUT_DEFINE_SINK_LAYOUT_(__sink__,__name__,_Ids,__useregs__)
#define  LDNDC_OUTPUT_DEFINE_SINK_LAYOUT_W(__sink__,__name__,__useregs__) \
    LDNDC_OUTPUT_DEFINE_SINK_LAYOUT_W_(__sink__,__name__,_Ids,RM_CLIENTSOURCE|__useregs__)
#define  LDNDC_OUTPUT_DEFINE_SINK_LAYOUT_NOIDS(__sink__,__name__,__useregs__) \
    LDNDC_OUTPUT_DEFINE_SINK_LAYOUT_(__sink__,__name__,_Header,__useregs__)

#define  RM_NONE  0
#define  RM_CLIENTSOURCE  ldndc::io::CLIENT_SOURCE
#define  RM_CLIENTID  ldndc::io::CLIENT_ID
#define  RM_X  ldndc::io::CLIENT_X
#define  RM_Y  ldndc::io::CLIENT_Y
#define  RM_Z  ldndc::io::CLIENT_Z
#define  RM_AREA  ldndc::io::CLIENT_AREA
#define  RM_DATETIME  ldndc::io::DATETIME
#define  RM_YEAR  ldndc::io::YEAR
#define  RM_MONTH  ldndc::io::MONTH
#define  RM_DAY  ldndc::io::DAY
#define  RM_HOUR  ldndc::io::HOUR
#define  RM_MINUTE  ldndc::io::MINUTE
#define  RM_SECOND  ldndc::io::SECOND
#define  RM_YEARDAY  ldndc::io::JULIANDAY
#define  RM_SUBDAY  ldndc::io::SUBDAY
#define  RM_LAYER  ldndc::io::SEQNB

#define  RM_DEFAULT_SUBDAILY (RM_CLIENTSOURCE|RM_CLIENTID|RM_DATETIME)
#define  RM_DEFAULT_LAYERSUBDAILY (RM_CLIENTSOURCE|RM_CLIENTID|RM_DATETIME|RM_LAYER)
#define  RM_DEFAULT_DAILY (RM_CLIENTSOURCE|RM_CLIENTID|RM_DATETIME)
#define  RM_DEFAULT_LAYERDAILY (RM_CLIENTSOURCE|RM_CLIENTID|RM_DATETIME|RM_LAYER)
#define  RM_DEFAULT_YEARLY (RM_CLIENTSOURCE|RM_CLIENTID|RM_DATETIME)
#define  RM_DEFAULT_LAYERYEARLY (RM_CLIENTSOURCE|RM_CLIENTID|RM_DATETIME|RM_LAYER)

#define  RM_DEFAULTS (RM_CLIENTSOURCE|RM_CLIENTID|RM_DATETIME)

#define  COLUMN_NAME_ALLSPECIES ":ALL:"


class LDNDC_API SinkInterface
{
    struct outputmodule_configuration_t
    {
        bool  write_id:1;  /* "write_id" */
        bool  write_source:1;  /* "write_source" */

        bool  write_xyz:1;  /* "write_xyz" */
        /* rather than x,y,z use longitude, latitude, elevation instead */
        bool  use_location:1; /* "use_location" */
        bool  write_area:1;  /* "write_area" */

        bool  write_datetime:1;  /* "write_datetime" */

        bool  write_year:1;  /* "write_year" */
        bool  write_month:1;  /* "write_month" */
        bool  write_day:1;  /* "write_day" */
        bool  write_hour:1;  /* "write_hour" */
        bool  write_minute:1;  /* "write_minute" */
        bool  write_second:1;  /* "write_second" */

        bool  write_yearday:1;  /* "write_yearday" */
        bool  write_subday:1;  /* "write_subday" */

        bool  write_seqnb:1;  /* "write_layer" */
        bool  separate_concurrent:1;  /* "separate_concurrent" */
    };

    public:
        SinkInterface( cbm::io_kcomm_t * = NULL /*i/o*/);
        virtual ~SinkInterface();

        lerr_t  configure( cbm::io_kcomm_t *);

    public:
        sink_client_t const &  m_client() const
            { return  this->client; }

        lerr_t  set_metaflags( ldndc::config_file_t const *,
                char const * /*object id*/,
                lflags_t const & = RM_DEFAULTS /*predefines*/);
        lflags_t  get_metaflags() const;
        bool  separate_concurrent() const
            { return !this->m_rc.separate_concurrent; }

        void  set_layernumber( int  _seqnb)
            { this->seqnb = _seqnb; }

        lerr_t  write_fixed_record( ldndc::sink_handle_t * /*target sink*/,
            void ** /*data record*/, cbm::sclock_t const *);
        lerr_t  write_fixed_zero_record(
            ldndc::sink_handle_t * /*target sink*/,
            cbm::sclock_t const *);

    private:
        struct outputmodule_configuration_t  m_rc;
        int  seqnb;
    protected:
        sink_client_t  client;

        lerr_t  set_client_info( cbm::io_kcomm_t *);

        lerr_t  update_meta(
            ldndc::sink_record_meta_t *,
            cbm::sclock_t const *) const;
};

} /* namespace ldndc */

#endif /* !LD_SINKINTERFACE_H_ */

