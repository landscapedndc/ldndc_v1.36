/*!
 * @brief
 *    ...
 *
 * @author
 *   tobias denk (created on: dec, 01, 2015)
 */

#ifndef  LD_ISOTOPES_H_
#define  LD_ISOTOPES_H_

#include  "ld_modelsconfig.h"
#include  "models/ld_isotopes.h.inc"

#include  <kernel/io-kcomm.h>
#include  <cbm_nan.h>

namespace ldndc {

#define  ISOTOPES_ITEMKEY_MAXLEN  64
class LDNDC_API Isotopes
{
    public:
        typedef  char  keybuffer_t[ISOTOPES_ITEMKEY_MAXLEN];
    public:
        Isotopes()
            : io_kcomm( NULL), hour( 0)
        { }
        cbm::io_kcomm_t *  io_kcomm;

    public:
        void rewrite(
            double /*value*/, char const * /*key*/,
            size_t = ldndc::invalid_t< size_t >::value /*index*/);
        void cumulative_write(
            double /*value*/, char const * /*key*/,
            size_t = ldndc::invalid_t< size_t >::value /*index*/);
        void subdaily_write(
            double /*value*/, char const * /*key*/,
            size_t = ldndc::invalid_t< size_t >::value /*index*/);

        static char const *  keyname(
            char * /*keybuffer*/, char const * /*key basename*/,
            size_t = ldndc::invalid_t< size_t >::value /*index*/, int = -1 /*hour*/);

        int  hour;
};

} /* namespace ldndc */

#endif  /*  !LD_ISOTOPES_H_  */

