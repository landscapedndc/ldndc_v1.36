/*!
 * @file
 * 
 * @author
 *      felix wiss (created on: april 5, 2017)
 */

#ifndef  LM_VOCEMISSIONHELPER_H_
#define  LM_VOCEMISSIONHELPER_H_

#include  "ld_modelsconfig.h"

namespace ldndc {
class MoBiLE_Plant;
lerr_t
get_voc_emission_factors( ldndc::MoBiLE_Plant *, int /*foliage layer*/,
        bool /*synthase activity*/, double /*latitude*/, int /*yearday*/,
        double /*foliage temperature*/, double /*foliage radiation*/);

} /* namespace ldndc */

#endif  /*  !LM_VOCEMISSIONHELPER_H_  */

