/*!
 * @file ld_plantfunctions.h
 *
 * @brief
 *    initialization and helper functions for plant species
 *
 * @author
 *    - Steffen Klatt
 *    - David Kraus,
 *    - Edwin Haas
 *
 * @date Feb 09, 2012
 */

#ifndef  LD_PLANTFUNCTIONS_H_
#define  LD_PLANTFUNCTIONS_H_

#include  "ld_modelsconfig.h"

#include  "mbe_plant.h"
#include  "state/mbe_state.h"

#include  <input/climate/climate.h>
#include  <input/soillayers/soillayers.h>
#include  <input/setup/setup.h>
#include  <input/species/species.h>
#include  "physiology/ld_allometry.h"

#include  <containers/lbstack.h>


namespace ldndc {

class LDNDC_API LD_PlantFunctions
{
public:
    LD_PlantFunctions(
                      MoBiLE_State *,
                      cbm::io_kcomm_t *);
    ~LD_PlantFunctions();

    lerr_t  initialize_crop( MoBiLE_Plant *, species::crop_properties_t const *);
    lerr_t  initialize_grass( MoBiLE_Plant *, species::grass_properties_t const *);
    lerr_t  initialize_tree( MoBiLE_Plant *, species::wood_properties_t const *, ecosystem_type_e ,
                             cbm::string_t /* branchfraction */,
                             cbm::string_t /* crownlength */,
                             bool /* competition */);

protected:

    climate::input_class_climate_t const &  cl_;
    setup::input_class_setup_t const &  se_;
    soillayers::input_class_soillayers_t const &  sl_;

    substate_microclimate_t &  mc_;
    substate_physiology_t &  ph_;
    substate_soilchemistry_t &  sc_;

    LD_Allometry m_alm;
    MoBiLE_PlantVegetation *  m_veg;

    /* ##############   WOOD   ############## */
    lerr_t  set_wood_defaults_( MoBiLE_Plant *, species::wood_properties_t const *);
    lerr_t  gapfill_wood_defaults_( MoBiLE_Plant *,
                                    species::wood_properties_t const *,
                                    bool /* competition */);
    /*!
     * @brief
     *    initialization that was previously done in Site::vs_ac_init
     */
    lerr_t  initialize_vegetationstructure_wood_( MoBiLE_Plant *,
                                                  cbm::string_t /* branchfraction */,
                                                  cbm::string_t /* crownlength */);

    /* ##############   GRASS  ############## */
    lerr_t  set_grass_defaults_( MoBiLE_Plant *, species::grass_properties_t const *);
    lerr_t  gapfill_grass_defaults_( MoBiLE_Plant *, species::species_properties_t const *);
    lerr_t  reset_grass_properties_( MoBiLE_Plant *);

    /*!
     * @brief
     *    initialization that was previously done in Site::vs_ac_init
     */
    lerr_t  initialize_vegetationstructure_grass_( MoBiLE_Plant *);

    /* ##############   CROP   ############## */
    lerr_t  set_crop_defaults_( MoBiLE_Plant *, species::crop_properties_t const *);
    lerr_t  reset_crop_properties_( MoBiLE_Plant *);
    



    /*!
     * @brief
     *
     */
    lerr_t  initialize_nitrogen_concentrations( MoBiLE_Plant *);

    /*!
     * @brief
     *  Updates canopylayer-specific microclimatic state.
     *
     */
    lerr_t  canopy_microclimate();

    
public:

    /*!
     * @brief
     *      returns optimum foliage biomass for trees
     */
    double
    get_optimum_foliage_biomass_trees( MoBiLE_Plant * /* plant species */);

    
    /*!
     * @brief
     *      returns optimum foliage biomass for grass depending on
     *      - MFOLOPT (species parameter for optimum foliage biomass)
     *      - area fraction
     *
     */
    double
    get_optimum_foliage_biomass_grass( MoBiLE_Plant * /* plant species */);


    int
    get_foliage_age(
                    int _day_of_year_start,
                    int _doy,
                    int _nd_age_classes);

    double
    get_dvs_mortality_of_age_class(int /* day of start senescence */,
                                   int /* number of days of senescence */,
                                   int /* day of year */);

    double
    get_dvs_mortality_contribution_of_age_class(
                                                MoBiLE_Plant *,
                                                int /* age class */,
                                                double /* mortality of age class */);

    /*!
     * @brief
     */
    lerr_t
    initialize_seasonality(
                           MoBiLE_Plant *,
                           unsigned int /* day of year start */,
                           unsigned int /* current day of year */,
                           unsigned int &  /* bud state */);

    /*!
     * @brief
     */
    lerr_t
    initialize_new_foliage_biomass(
                                   MoBiLE_Plant *,
                                   double /*initial biomass*/,
                                   unsigned int /* day of year start */,
                                   unsigned int /* current day of year */,
                                   unsigned int /* bud state */);

    /*!
     * @brief
     */
    lerr_t
    initialize_new_biomass(
                           MoBiLE_Plant *,
                           double /*initial biomass*/);

    /*!
     * @brief
     */
    double
    update_dvs_flush(
                     MoBiLE_Plant *  vt,
                     int _days_since_emergence);
    
    lerr_t
    initialize_wood_biomass(
                            MoBiLE_Plant *,
                            double /*initial biomass*/,
                            double /*fom*/);

    lerr_t
    initialize_foliage_dependent_fine_root_biomass(
                                                   MoBiLE_Plant *,
                                                   double /*fom*/);

    lerr_t
    update_foliage_layer_biomass_and_lai(
                                         MoBiLE_Plant *,
                                         size_t, /*foliage layer count*/
                                         double /*minimum allowed lai*/);

    /*!
     * @brief
     *
     */
    double
    aboveground_wood_biomass_from_structure(
                                            MoBiLE_Plant * /* species */);
    
    /*!
     * @brief
     *    initialization of vertical biomass profile
     */
    lerr_t
    update_vertical_biomass_distribution(
                                         MoBiLE_Plant *);
    
    /*!
     * @brief
     *    Calculates average (effective) temperature
     *    of all leaves of a vegetation type.
     *
     */
    double
    leaf_temperature_(
                      lvector_t< double > /* canopy air temperature */,
                      MoBiLE_Plant * /* species */);

    double
    nitrogen_uptake_temperature_dependency(
                                           double /* minimum temperature */,
                                           double /* optimum temperature */,
                                           double /* temperature */);

    /*!
     * @brief
     *
     */
    double
    optimum_nitrogen_content_foliage(
                                     MoBiLE_Plant * /* plant species */);

    /*!
     * @brief
     *
     */
    double
    optimum_nitrogen_content_buds(
                                  MoBiLE_Plant * /* plant species */);

    /*!
     * @brief
     *
     */
    double
    get_foliage_age_of_senescence(
                                  MoBiLE_Plant * /* species */,
                                  double /* fraction */);

    double
    accumulated_n_uptake();
};


/*!
 * @brief
 * Vertical biomass estimation
 *
 * @param
 *    _ps form parameter
 * @param
 *    _h_bottom height of start of distributed biomass
 * @param
 *    _h_top height of end of distributed biomass
 * @param
 *    _lw
 * @param
 *    _fract
 * @param
 *    _h_cum_bottom
 * @param
 *    _h_cum_top
 *
 * @return
 *    No return value
 */
void
estimate_single_layer_biomass(
                              double const _ps,
                              double const _h_bottom,
                              double const _h_top,
                              double  _lw,
                              double  &_fract,
                              double  _h_cum_bottom,
                              double  _h_cum_top);


} /* namespace ldndc */


#endif  /*  !LD_PLANTFUNCTIONS_H_   */

