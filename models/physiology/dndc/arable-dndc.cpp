/*!
 * @file
 * @author
 *    - Ruediger Grote
 *    - David Kraus
 * @date  Mar 04, 2012
 */

/*!
 * @page arabledndc
 * @section arabledndc_guide User guide
 * The crop growth model ArableDNDC originates from the DNDC Model (@cite li:1994a).
 * ArableDNDC simulates the carbon and nitrogen cycle of crops. Processes are described in a universal way
 * and plants are primarily distinguished by species-specific parameters that can be accessed and calibrated externally.
 * Dynamics of plant carbon (C) and nitrogen (N) are primarily calculated based on
 * temperature driven crop development and associated N demand and N uptake/fixation.
 *
 * @subsection arabledndc_guide_structure Model structure
 * ArableDNDC can be either run in subdaily or daily time resolution.
 *
 * @subsection arabledndc_guide_parametrization Parametrization
 * The following lists includes species parameters that might be calibrated in order to represent a specific plant.
 * See the description of respective sections for more details on parameter behaviour.
 *
 * Nitrogen related parameters:
 * - nc_fruit_max (optimum nitrogen content of the fruit)
 * - nc_fruit_min (minimum nitrogen content of the fruit)
 * - nc_fineroots_max (optimum nitrogen content of fine roots)
 * - nc_fineroots_min (minimum nitrogen content of fine roots)
 * - nc_structural_tissue_max (optimum nitrogen content of structural tissue)
 * - nc_structural_tissue_min (minimum nitrogen content of structural tissue)
 * - nc_foliage_max (optimum nitrogen content of foliage)
 * - nc_foliage_min (minimum nitrogen content of foliage)
 *
 * Allocation related parameters:
 * - fraction_root (root fraction of total biomass at maturity)
 * - fraction_fruit (fruit fraction of total biomass at maturity)
 * - fraction_foliage (foliage fraction of total biomass at maturity)
 *
 * Plant development related parameters:
 * - gdd_base_temperature
 * - gdd_max_temperature
 * - gdd_maturity
 * - gdd_grain_filling
 * - gdd_flowering
 *
 * Drought
 * - h2oref_a (determines drought resistance)
 *
 * Nitrogen uptake
 * - tlimit (minimum temperature required for nitrogen uptake)
 * - k_mm_nitrogen_uptake (root affinity to soil nitrogen)
 *
 * Nitrogen fixation
 * - ini_n_fix (fraction of total nitrogen that might be fixed)
 *
 * Respiration
 * - maintenance_temp_ref (reference temperature for maintenance respiration)
 * - mc_root (maintenance respiration coefficient for roots)
 *
 * Root exsudation
 * - doc_resp_ratio (ratio of doc exsudation in relation to root respiration)
 *
 * Senescence
 * - senescence_drought (coefficient of senescence related to drought)
 * - senescence_frost (coefficient of senescence related to frost)
 * - senescence_age (coefficient of senescence related to age)
 *
 * Fineroots turnover
 * - tofrtbas
 *
 * water demand
 * - wuecmax (water use efficiency)
 *
 * Structure
 * - exp_root_distribution (coefficient for exponential root distribution)
 * - height_max (maximum plant height)
 */


#include  "physiology/dndc/arable-dndc.h"
#include  "physiology/ld_rootsystem.h"
#include  "physiology/ld_transpiration.h"
#include  "physiology/dndc/common.h"
#include  "watercycle/ld_droughtstress.h"
#include  "physiology/ld_vernalization.h"

#include  <logging/cbm_logging.h>

#include  <input/soillayers/soillayers.h>
#include  <input/species/species.h>

#include  <constants/cbm_const.h>
#include  <math/cbm_math.h>
#include  <numeric>

LMOD_MODULE_INFO(PhysiologyArableDNDC,TMODE_SUBDAILY|TMODE_POST_DAILY,LMOD_FLAG_USER);

namespace ldndc {
#define  ON_ENTRY 1
#define  ON_EXIT 2

double const  PhysiologyArableDNDC::GROWTH_RESPIRATION_FRACTION = 0.25;
double const  PhysiologyArableDNDC::INITIAL_BIOMASS = 10.0;
double const  PhysiologyArableDNDC::S_TLIMIT = 0.0;

PhysiologyArableDNDC::PhysiologyArableDNDC(
                                           MoBiLE_State *  _state,
                                           cbm::io_kcomm_t *  _io_kcomm,
                                           timemode_e  _timemode)
                                            : MBE_LegacyModel( _state, _timemode),
                                            m_state( _state),
                                            io_kcomm( _io_kcomm),
                                            m_setup( _io_kcomm->get_input_class_ref< input_class_setup_t >()),
                                            sl_( _io_kcomm->get_input_class_ref< input_class_soillayers_t >()),
                                            m_species( _io_kcomm->get_input_class< species::input_class_species_t >()),
                                            m_speciesparameters( _io_kcomm->get_input_class< speciesparameters::input_class_speciesparameters_t >()),
                                            ac_( _state->get_substate_ref< substate_airchemistry_t >()),
                                            mc_( _state->get_substate_ref< substate_microclimate_t >()),
                                            ph_( _state->get_substate_ref< substate_physiology_t >()),
                                            sc_( _state->get_substate_ref< substate_soilchemistry_t >()),
                                            wc_( _state->get_substate_ref< substate_watercycle_t >()),
                                            m_veg( &_state->vegetation),
                                            root_system( 0, NULL),
                                            mc_temp( (_timemode == TMODE_SUBDAILY) ? &mc_.ts_airtemperature : &mc_.nd_airtemperature),
                                            FTS_TOT_( 1.0 / LD_RtCfg.clk->time_resolution()),
                                            m_pf( _state, _io_kcomm)
{
    tot_c_ = 0.0;
    tot_n_ = 0.0;
}


PhysiologyArableDNDC::~PhysiologyArableDNDC()
{
    for ( size_t  r = 0;  r < root_system.size();  ++r)
    {
        if ( root_system[r])
        {
            LD_Allocator->destroy( root_system[r]);
        }
    }
}


lerr_t
PhysiologyArableDNDC::configure(
                                ldndc::config_file_t const *)
{
    return  LDNDC_ERR_OK;
}


lerr_t
PhysiologyArableDNDC::initialize()
{
    return  LDNDC_ERR_OK;
}


lerr_t
PhysiologyArableDNDC::solve()
{
    (void)this->ArableDNDC_step_init();

    EventAttributes const *  ev_plant = NULL;
    while (( ev_plant = m_PlantEvents.pop()) != NULL)
    {
        char const *  p_name = ev_plant->get( "/name", "?");
        char const *  p_type = ev_plant->get( "/type", "?");
        char const *  p_group = ev_plant->get( "/group", "?");
        if ( !m_state->vegetation.have_plant( p_name))
        {
            species_t const *  p = NULL;
            if ( m_species)
            { p = m_species->get_species( p_name); }
            
            MoBiLE_PlantParameters  p_parameters;
            if ( p)
            { p_parameters = p->get_parameters(); }
            else if ( m_speciesparameters) /* assume event was not provided via input */
            { p_parameters = (*m_speciesparameters)[p_type]; }
            else
            { /* no parameters.. */ }
            
            MoBiLE_PlantSettings  plant_settings( static_cast< unsigned int >(m_setup.canopylayers()),
                                                  p_name, p_type, p_group, &p_parameters);
            plant_settings.nb_soillayers = sl_.soil_layer_cnt();

            MoBiLE_Plant *  plant = this->m_state->vegetation.new_plant( &plant_settings);
            if ( plant)
            {
                plant->initialize();
            }
            else
            {
                KLOGERROR( "Species creation failed  [species=", p->name(),"]");
                return  LDNDC_ERR_FAIL;
            }
        }

        ArableDNDC_resize();

        char const *  group = ev_plant->get( "/group", "?");
        if ( cbm::is_equal( group, "crop"))
        {
            MoBiLE_Plant *  vt = this->m_veg->get_plant( ev_plant->get( "/name", "?"));
            if ( !vt)
            { return LDNDC_ERR_RUNTIME_ERROR; }

            lerr_t  rc_plant = ArableDNDC_event_plant( vt, *ev_plant);
            if ( rc_plant)
            {
                KLOGERROR( "Handling plant event failed  [species=", vt->name(),"]");
                return  LDNDC_ERR_FAIL;
            }
        }
    }

    this->balance_check( ON_ENTRY);

    for ( CropIterator vt = this->m_veg->groupbegin< species::crop >();
         vt != this->m_veg->groupend< species::crop >(); ++vt)
    {
        MoBiLE_Plant *p = *vt;

        /* temperature driven crop development */
        lerr_t const  rc_devstage = this->development_stage( p);
        if ( rc_devstage)
        { return  LDNDC_ERR_FAIL; }

        /* crop nitrogen demand */
        n_demand_vt_[p->slot] = nitrogen_demand( p);
        if ( !cbm::flt_greater_equal_zero( n_demand_vt_[p->slot]) ||
              cbm::is_invalid( n_demand_vt_[p->slot]))
        {
            KLOGERROR( "got negative nitrogen demand.", "  [N demand=",n_demand_vt_[p->slot],"]");
            return  LDNDC_ERR_FAIL;
        }
    }

    this->nitrogen_uptake();

    for ( CropIterator vt = this->m_veg->groupbegin< species::crop >();
         vt != this->m_veg->groupend< species::crop >(); ++vt)
    {
        MoBiLE_Plant *p = *vt;

        /* N2 fixation [kgN m-2]  (if the crop is a legume) */
        double const  day_n_growth = this->nitrogen_fixation( *vt, n_demand_vt_[p->slot], n_uptake_vt_[p->slot]);

        /* biomass partitioning to the straw, grain, and root */
        this->biomass_partition( p, day_n_growth);

        /* total growth respiration  [kgC m-2] */
        this->growth_respiration( p, day_n_growth);

        /* root respiration and exudation */
        this->maintenance_respiration( p);

        /* root development */
        if ( cbm::flt_greater_zero( day_n_growth))
        {
            root_system[p->slot]->root_development( vt->GZRTZ() * FTS_TOT_, root_q_vt_[p->slot], p);
        }

        /* crop senescence */
        this->senescence( p);

        /* set further properties based on newly calculated ones */
        this->update_species_related_state_items( *vt);

        /*!
         * @page arabledndc
         * @section arabledndc_transpiration Transpiration
         *  Calculates potential transpiration on \n
         *  a) water use efficiency and carbon uptake:
         *     \link ldndc::potential_crop_transpiration() potential_crop_transpiration() \endlink \n
         */
        wc_.accumulated_potentialtranspiration +=
        potential_crop_transpiration(
                                     ac_.nd_co2_concentration,
                                     cbm::sum( p->carbonuptake_fl, m_setup.canopylayers()),
                                     vt->WUECMAX());
    }

    this->balance_check( ON_EXIT);

    EventAttributes const *  ev_harv = NULL;
    while (( ev_harv = this->m_HarvestEvents.pop()) != NULL)
    {
        MoBiLE_Plant *  vt = this->m_veg->get_plant( ev_harv->get( "/name", "?"));
        if ( !vt)
        { return LDNDC_ERR_RUNTIME_ERROR; }

        if ( cbm::is_equal( vt->group().c_str(), "crop"))
        {
            CBM_LogDebug( "Harvest plant '",vt->name(),"'");
            lerr_t  rc_harv = this->event_harvest( vt, *ev_harv);
            if ( rc_harv)
            {
                KLOGERROR( "Handling harvest event failed  [species=", vt->name(),"]");
                return  LDNDC_ERR_FAIL;
            }
            // TODO  delete plant..
        }
    }

    (void)this->ArableDNDC_step_out();

    return  LDNDC_ERR_OK;
}


#define TRANSFER_FROM_SUBDAILY_TO_DAILY(__SP__,__VAL__)      \
{                                                            \
__SP__->d_##__VAL__ = __SP__->__VAL__;                       \
__SP__->__VAL__ = 0.0;                                       \
}

lerr_t
PhysiologyArableDNDC::ArableDNDC_step_out()
{
    if ( this->timemode() == TMODE_POST_DAILY)
    {
        for ( PlantIterator vt = this->m_veg->begin(); vt != this->m_veg->end(); ++vt)
        {
            MoBiLE_Plant *p = *vt;

            TRANSFER_FROM_SUBDAILY_TO_DAILY(p, dcFol);
            TRANSFER_FROM_SUBDAILY_TO_DAILY(p, dcSap);
            TRANSFER_FROM_SUBDAILY_TO_DAILY(p, dcBud);
            TRANSFER_FROM_SUBDAILY_TO_DAILY(p, dcFrt);

            TRANSFER_FROM_SUBDAILY_TO_DAILY(p, rRes);
            TRANSFER_FROM_SUBDAILY_TO_DAILY(p, rGro);
            TRANSFER_FROM_SUBDAILY_TO_DAILY(p, rGroBelow);
            TRANSFER_FROM_SUBDAILY_TO_DAILY(p, rFol);
            TRANSFER_FROM_SUBDAILY_TO_DAILY(p, rSap);
            TRANSFER_FROM_SUBDAILY_TO_DAILY(p, rBud);
            TRANSFER_FROM_SUBDAILY_TO_DAILY(p, rFrt);

            TRANSFER_FROM_SUBDAILY_TO_DAILY(p, sFol);
            TRANSFER_FROM_SUBDAILY_TO_DAILY(p, sBud);

            TRANSFER_FROM_SUBDAILY_TO_DAILY(p, nLitFol);
            TRANSFER_FROM_SUBDAILY_TO_DAILY(p, nLitBud);

            TRANSFER_FROM_SUBDAILY_TO_DAILY(p, exsuLoss);

            for ( size_t fl = 0; fl < p->nb_foliagelayers(); ++fl)
            {
                TRANSFER_FROM_SUBDAILY_TO_DAILY(p, carbonuptake_fl[fl]);
            }

            for ( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
            {
                TRANSFER_FROM_SUBDAILY_TO_DAILY(p, sFrt_sl[sl]);
                TRANSFER_FROM_SUBDAILY_TO_DAILY(p, nLitFrt_sl[sl]);
            }
        }
    }

    return  LDNDC_ERR_OK;
}


lerr_t
PhysiologyArableDNDC::ArableDNDC_step_init()
{
    if ( this->timemode() == TMODE_SUBDAILY)
    {
        temp_sl.reference( mc_.temp_sl);
    }
    else if ( this->timemode() == TMODE_POST_DAILY)
    {
        temp_sl.reference( mc_.nd_temp_sl);
    }

    ArableDNDC_resize();

    for ( CropIterator vt = this->m_veg->groupbegin< species::crop >();
         vt != this->m_veg->groupend< species::crop >(); ++vt)
    {
        MoBiLE_Plant *p = *vt;

        dw_fol_old_vt[p->slot] = p->mFol;
        dw_frt_old_vt[p->slot] = p->mFrt;
        dw_lst_old_vt[p->slot] = p->dw_lst;
        dw_bud_old_vt[p->slot] = p->mBud;
    }

    return  LDNDC_ERR_OK;
}


lerr_t
PhysiologyArableDNDC::ArableDNDC_resize()
{
    size_t const  slot_cnt = m_veg->slot_cnt();

    n_demand_vt_.resize_and_preserve( slot_cnt, 0.0);
    n_uptake_vt_.resize_and_preserve( slot_cnt, 0.0);
    n_avail_tot_vt_.resize_and_preserve( slot_cnt, 0.0);
    n_plant_vt_.resize_and_preserve( slot_cnt, 0.0);

    root_q_vt_.resize_and_preserve( slot_cnt, invalid_uint);

    dw_fol_old_vt.resize_and_preserve( slot_cnt, 0.0);
    dw_frt_old_vt.resize_and_preserve( slot_cnt, 0.0);
    dw_lst_old_vt.resize_and_preserve( slot_cnt, 0.0);
    dw_bud_old_vt.resize_and_preserve( slot_cnt, 0.0);

    chill_factor.resize_and_preserve( slot_cnt, 1.0);
    chill_units.resize_and_preserve( slot_cnt, 0.0);

    return  LDNDC_ERR_OK;
}


/*!
 * @page arabledndc
 * @section roots Roots
 * Roots are described by the rooting depth, which grows linearly with GZRTZ (default 1cm/day)
 * up to a maximum value ZRTMC (default 1m) (see @ref veglibs_roots).The distribution of fine
 * roots follows a sigmoid function (see @ref veglibs_roots_sigmoid).
 */
void
PhysiologyArableDNDC::update_species_related_state_items( MoBiLE_Plant * _vt)
{
    /* day of foliage budburst */
    if ( cbm::flt_greater_zero( _vt->dvsFlush) && ( _vt->dEmerg == 0))
    {
        _vt->dEmerg = lclock_ref().yearday();
    }

    /* does not depend on root mass */
    root_system[_vt->slot]->update_root_fraction_sigmoid( 1.0, root_q_vt_[_vt->slot], 0.005, 10.0, 120.0, 10.0, _vt);

    /* coverage */
    this->update_canopy_coverage( _vt);

    /* plant height */
    this->update_canopy_height( _vt);

    /* canopy structure */
    this->update_canopy_items( _vt);

    /* total carbon uptake  [kgC m-2] */
    double const cUpt_tot(   (_vt->mFol - dw_fol_old_vt[_vt->slot]
                              + _vt->mFrt - dw_frt_old_vt[_vt->slot]
                              + _vt->dw_lst - dw_lst_old_vt[_vt->slot]
                              + _vt->mBud - dw_bud_old_vt[_vt->slot]
                              + cbm::sum( _vt->sFrt_sl, sl_.soil_layer_cnt())
                              + _vt->sFol + _vt->sBud) * cbm::CCDM
                              + _vt->rGro + _vt->rRes + _vt->exsuLoss);

    double const fl_cnt_inv( 1.0 / static_cast< double >( _vt->nb_foliagelayers()));
    for (size_t fl = 0; fl < _vt->nb_foliagelayers(); ++fl)
    {
        _vt->carbonuptake_fl[fl] = cUpt_tot * fl_cnt_inv;
    }
    for ( int fl = _vt->nb_foliagelayers(); fl < m_setup.canopylayers(); ++fl)
    {
        _vt->carbonuptake_fl[fl] = 0.0;
    }
}


void
PhysiologyArableDNDC::update_canopy_coverage( MoBiLE_Plant *  _vt)
{
    /* totally empirical 'rule of thumb' estimation */
    _vt->f_area = cbm::bound( 0.01, 2.0 * _vt->dvsFlush, 1.0);
}


void
PhysiologyArableDNDC::update_canopy_height( MoBiLE_Plant *  _vt)
{
    double const c_above( _vt->aboveground_biomass() * cbm::CCDM);
    double const c_above_opt( optimum_aboveground_c( _vt));

    _vt->height_max = (c_above / c_above_opt) * (*_vt)->HEIGHT_MAX();
    _vt->height_max = cbm::bound(0.03, _vt->height_max, (*_vt)->HEIGHT_MAX());
    _vt->height_at_canopy_start = 0.0;

    ph_.update_canopy_layers_height( m_veg);
}


void
PhysiologyArableDNDC::update_canopy_items( MoBiLE_Plant *  _vt)
{
    double const fl_cnt_inv( 1.0 / static_cast< double >( _vt->nb_foliagelayers()));

    double const lst_lai_red( 0.5);                                              // stems have less photosynthesis than leafs
    double const sla_red( 1.0 - 0.8 * _vt->dvsFlush);                            // reduction of specific leaf area with crop age

    double h_cum( 0.0);
    for (size_t fl = 0; fl < _vt->nb_foliagelayers(); ++fl)
    {
        h_cum += (0.5 * ph_.h_fl[fl]);
        _vt->sla_fl[fl] = (((*_vt)->SLAMAX() - ((*_vt)->SLAMAX() - (*_vt)->SLAMIN()) * h_cum / _vt->height_max) * sla_red);
        _vt->lai_fl[fl] = (_vt->sla_fl[fl] * (_vt->mFol + lst_lai_red * _vt->dw_lst) * fl_cnt_inv);
        h_cum += (0.5 * ph_.h_fl[fl]);
    }
    for ( int  fl = _vt->nb_foliagelayers();  fl < m_setup.canopylayers();  ++fl)
    {
        _vt->sla_fl[fl] = 0.0;
        _vt->lai_fl[fl] = 0.0;
    }
}


lerr_t
PhysiologyArableDNDC::ArableDNDC_event_plant(
                                             MoBiLE_Plant * _vt,
                                             EventAttributes const &  _attributes)
{
    species_t const *  sp = NULL;
    if ( m_species)
    {
        sp = m_species->get_species( _vt->cname());
    }

    lerr_t rc_plant = m_pf.initialize_crop( _vt, sp ? sp->crop() : NULL);
    if ( rc_plant)
    {
        return LDNDC_ERR_OK;
    }

    if ( !cbm::flt_greater_zero( (*_vt)->M_FRUIT_OPT()))
    {
        KLOGERROR( "[BUG] Optimum fruit biomass (yield) not set [species=", _vt->name(),"]");
        return  LDNDC_ERR_RUNTIME_ERROR;
    }

    /* T_LIMIT (temperature limit for growing) by definition greater or equal zero */
    if (( (*_vt)->TLIMIT() < 0.0) || ( (*_vt)->GDD_MATURITY() <= 0.0))
    {
        KLOGERROR( "TLIMIT or GDD_MATURITY have illegal values",
                   " [TLIMIT=",(*_vt)->TLIMIT(),", GDD_MATURITY=",(*_vt)->GDD_MATURITY(),"]");
        return  LDNDC_ERR_RUNTIME_ERROR;
    }

    CBM_LogInfo( "slot@plant=",_vt->slot);

    _vt->a_fix_n = 0.0;
    _vt->dvsFlush = 0.0;
    _vt->dvsFlushOld = 0.0;
    _vt->growing_degree_days = 0.0;
    _vt->dEmerg = 0;

    chill_units[_vt->slot] = 0.0;
    chill_factor[_vt->slot] = 1.0;

    _vt->is_covercrop = _attributes.get( "/cover-crop", false);

    double const  ib = _attributes.get( "/initial-biomass", INITIAL_BIOMASS);
    double const  bm = ib * cbm::HA_IN_M2 / cbm::CCDM;

    double const straw( 1.0 - (*_vt)->FRACTION_ROOT());

    _vt->mFol = bm * (*_vt)->FALEAF() * straw;
    _vt->dw_lst = bm * (1.0 - (*_vt)->FALEAF()) * straw;
    _vt->mFrt = bm * (*_vt)->FRACTION_ROOT();
    _vt->mBud = 0.0;

    dw_fol_old_vt[_vt->slot] = _vt->mFol;
    dw_frt_old_vt[_vt->slot] = _vt->mFrt;
    dw_lst_old_vt[_vt->slot] = _vt->dw_lst;
    dw_bud_old_vt[_vt->slot] = _vt->mBud;

    _vt->ncBud = (*_vt)->NC_FRUIT_MAX();
    _vt->ncFol = (*_vt)->NC_FOLIAGE_MAX();
    _vt->n_lst = (*_vt)->NC_STRUCTURAL_TISSUE_MAX() * _vt->dw_lst;
    _vt->ncFrt = (*_vt)->NC_FINEROOTS_MAX();
    
    n_plant_vt_[_vt->slot] = _vt->total_nitrogen();

    /* root system */
    if ( root_system.size() <= _vt->slot)
    {
        root_system.resize(_vt->slot+1);
    }

    ldndc_kassert( root_system[_vt->slot] == NULL);
    root_system[_vt->slot] = LD_Allocator->construct_args< RootSystemDNDC >( 1, this->m_state, this->io_kcomm);

    if ( !root_system[_vt->slot])
    {
        KLOGERROR( "Failed to allocate root-system object");
        return  LDNDC_ERR_NOMEM;
    }

    root_system[_vt->slot]->set_rooting_depth( root_q_vt_[_vt->slot], _vt, 0.01);
    root_system[_vt->slot]->update_root_fraction_sigmoid( 1.0, root_q_vt_[_vt->slot], 0.005, 10.0, 120.0, 10.0, _vt);

    /* coverage */
    this->update_canopy_coverage( _vt);

    /* plant height */
    this->update_canopy_height( _vt);

    /* canopy structure */
    this->update_canopy_items( _vt);

    return  LDNDC_ERR_OK;
}


/* harvesting of crops
 *
 *
 */
lerr_t
PhysiologyArableDNDC::event_harvest(
                                    MoBiLE_Plant *  _vt,
                                    EventAttributes const &  _attributes)
{
    if ( root_system[_vt->slot] == NULL)
    {
        KLOGERROR( "[BUG] Object 'root_system' is NULL when not expected \"",_vt->name(),"\"!");
        return  LDNDC_ERR_RUNTIME_ERROR;
    }

    double const export_root = _attributes.get( "/fraction-export-rootwood", 0.0);
    double const rootlitter_c( (1.0 - export_root) * _vt->mFrt * cbm::CCDM);
    double const rootlitter_n( (1.0 - export_root) * _vt->mFrt * _vt->ncFrt);
    double const root_c_export( export_root * _vt->mFrt * cbm::CCDM);
    double const root_n_export( export_root * _vt->mFrt * _vt->ncFrt);

    double const straw_c( (_vt->mFol + _vt->dw_dfol + _vt->dw_lst+ _vt->dw_dst) * cbm::CCDM);
    double const straw_n( _vt->n_fol() + _vt->n_dfol + _vt->n_lst + _vt->n_dst);

    double const fru_c( _vt->mBud * cbm::CCDM);
    double const fru_n( _vt->n_bud());

    double fru_c_export( 0.0);
    double fru_n_export( 0.0);

    double straw_c_export( 0.0);
    double straw_n_export( 0.0);

    double stubble_c( 0.0);
    double stubble_n( 0.0);

    double gdd_sum( _vt->growing_degree_days );
    double dvs_flush( _vt->dvsFlush );

    if ( !_vt->is_covercrop)
    {
        double remains_relative( _attributes.get( "/remains_relative", invalid_flt));
        double stubble_height( _attributes.get( "/height", invalid_flt));

        if (   !cbm::flt_greater_equal_zero( remains_relative)
            && !cbm::flt_greater_equal_zero( stubble_height))
        {
            KLOGWARN( "harvest event attribute \"remains_relative\" and \"height\" were not set; \"remains_relative\" set to zero",
                    "  [species=", _vt->name(),"]");
            remains_relative = 0.0;
        }
        else  if (   cbm::flt_greater_equal_zero( remains_relative)
                  && cbm::flt_greater_equal_zero( stubble_height))
        {
            KLOGWARN( "harvest event attribute \"remains_relative\" and \"height\" were both set; attribute \"remains_relative\" used",
                    "  [species=", _vt->name(),"]");
        }

        fru_c_export = fru_c;
        fru_n_export = fru_n;

        if ( cbm::flt_greater_equal_zero( remains_relative))
        { /* no op */}
        else if ( cbm::flt_greater_equal_zero( stubble_height))
        {
            // 1cm stubble assumed to equal 50 kg C ha-1
            remains_relative = cbm::bound_min( straw_c, stubble_height * cbm::CM_IN_M * 0.007) / straw_c;
        }
        else
        {
            KLOGFATAL("Bug in harvest event. Absolute amount of remains and height of stubbles are both invalid");
            return LDNDC_ERR_FAIL;
        }

        stubble_c = remains_relative * straw_c;
        stubble_n = remains_relative * straw_n;

        straw_c_export = (1.0-remains_relative) * straw_c;
        straw_n_export = (1.0-remains_relative) * straw_n;
    }
    else
    {
        stubble_c = straw_c + fru_c;
        stubble_n = straw_n + fru_n;
    }

    sc_.c_stubble_lit3 += stubble_c * (*_vt)->LIGNIN();
    sc_.c_stubble_lit2 += stubble_c * (*_vt)->CELLULOSE();
    sc_.c_stubble_lit1 += stubble_c * (1.0 - (*_vt)->LIGNIN() - (*_vt)->CELLULOSE());

    sc_.n_stubble_lit3 += stubble_n * (*_vt)->LIGNIN();
    sc_.n_stubble_lit2 += stubble_n * (*_vt)->CELLULOSE();
    sc_.n_stubble_lit1 += stubble_n * (1.0 - (*_vt)->LIGNIN() - (*_vt)->CELLULOSE());

    sc_.accumulated_c_litter_stubble += stubble_c;
    sc_.accumulated_n_litter_stubble += stubble_n;

    cbm::state_scratch_t *  mcom = io_kcomm->get_scratch();
    std::string  mcom_key;

    char const *  species_name = _attributes.get( "/name", "?");

    cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:gddsum", species_name);
    mcom->set( mcom_key.c_str(), gdd_sum);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:dvsflush", species_name);
    mcom->set( mcom_key.c_str(), dvs_flush);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:c_fru", species_name);
    mcom->set( mcom_key.c_str(), fru_c);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:c_fru_export", species_name);
    mcom->set( mcom_key.c_str(), fru_c_export);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:n_fru", species_name);
    mcom->set( mcom_key.c_str(), fru_n);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:n_fru_export", species_name);
    mcom->set( mcom_key.c_str(), fru_n_export);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:c_straw", species_name);
    mcom->set( mcom_key.c_str(), straw_c);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:c_straw_export", species_name);
    mcom->set( mcom_key.c_str(), straw_c_export);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:n_straw", species_name);
    mcom->set( mcom_key.c_str(), straw_n);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:n_straw_export", species_name);
    mcom->set( mcom_key.c_str(), straw_n_export);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:c_stubble", species_name);
    mcom->set( mcom_key.c_str(), stubble_c);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:n_stubble", species_name);
    mcom->set( mcom_key.c_str(), stubble_n);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:c_frt", species_name);
    mcom->set( mcom_key.c_str(), rootlitter_c);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:n_frt", species_name);
    mcom->set( mcom_key.c_str(), rootlitter_n);

    ph_.accumulated_c_export_harvest += straw_c_export + fru_c_export + root_c_export;
    ph_.accumulated_n_export_harvest += straw_n_export + fru_n_export + root_n_export;

    ph_.accumulated_c_fru_export_harvest += fru_c_export;
    ph_.accumulated_n_fru_export_harvest += fru_n_export;

    for ( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
    {
        double const dw_rootlitter( rootlitter_c / cbm::CCDM * _vt->fFrt_sl[sl]);
        double const n_rootlitter( dw_rootlitter * _vt->ncFrt);

        sc_.c_raw_lit_1_sl[sl] += dw_rootlitter * (1.0 - (*_vt)->CELLULOSE() - (*_vt)->LIGNIN()) * cbm::CCDM;
        sc_.c_raw_lit_2_sl[sl] += dw_rootlitter * (*_vt)->CELLULOSE() * cbm::CCDM;
        sc_.c_raw_lit_3_sl[sl] += dw_rootlitter * (*_vt)->LIGNIN() * cbm::CCDM;
        sc_.accumulated_c_litter_below_sl[sl] += dw_rootlitter * cbm::CCDM;

        sc_.n_raw_lit_1_sl[sl] += n_rootlitter * (1.0 - (*_vt)->CELLULOSE() - (*_vt)->LIGNIN());
        sc_.n_raw_lit_2_sl[sl] += n_rootlitter * (*_vt)->CELLULOSE();
        sc_.n_raw_lit_3_sl[sl] += n_rootlitter * (*_vt)->LIGNIN();
        sc_.accumulated_n_litter_below_sl[sl] += n_rootlitter;

        _vt->fFrt_sl[sl] = 0.0;
    }

    cbm::invalidate( root_q_vt_[_vt->slot]);
    _vt->rooting_depth = 0.0;

    _vt->mFol = 0.0;
    _vt->dw_dfol = 0.0;
    _vt->ncFol = 0.0;
    _vt->n_dfol = 0.0;

    _vt->dw_lst = 0.0;
    _vt->n_lst = 0.0;

    _vt->dw_dst = 0.0;
    _vt->n_dst = 0.0;

    _vt->mBud = 0.0;
    _vt->mBudStart = 0.0;
    _vt->ncBud = 0.0;

    _vt->mFrt = 0.0;
    _vt->ncFrt = 0.0;

    _vt->dEmerg = 0;
    _vt->growing_degree_days = 0.0;
    _vt->dvsFlush = 0.0;
    _vt->dvsFlushOld = 0.0;

    _vt->a_fix_n = 0.0;

    _vt->height_max = 0.0;
    _vt->height_at_canopy_start = 0.0;

    _vt->f_area = 0.0;
    for ( int fl = 0; fl < m_setup.canopylayers(); ++fl)
    {
        _vt->fFol_fl[fl] = 0.0;
        _vt->lai_fl[fl] = 0.0;
    }

    _vt->f_fac = 0.0;

    _vt->rBud = 0.0;
    _vt->rFol = 0.0;
    _vt->rSap = 0.0;
    _vt->rFrt = 0.0;

    _vt->rRes = 0.0;
    _vt->rGro = 0.0;
    _vt->rGroBelow = 0.0;

    _vt->sBud = 0.0;
    _vt->sFol = 0.0;

    _vt->nLitFol = 0.0;
    _vt->nLitBud = 0.0;

    _vt->dcBud = 0.0;
    _vt->dcFol = 0.0;
    _vt->dcFrt = 0.0;
    _vt->dcSap = 0.0;

    _vt->exsuLoss = 0.0;
    n_plant_vt_[_vt->slot] = 0.0;

    chill_units[_vt->slot] = 0.0;
    chill_factor[_vt->slot] = 1.0;

    LD_Allocator->destroy( root_system[_vt->slot]);
    root_system[_vt->slot] = NULL;

    return  LDNDC_ERR_OK;
}


/*!
 * @page arabledndc
 * @section crop_development Crop development
 *  Crop development is determined by growing degree days \f$GDD\f$:
 *  \f[
 *  DVS = \frac{GDD}{GDD_{max}}
 *  \f]
 */
lerr_t
PhysiologyArableDNDC::development_stage( MoBiLE_Plant *  _vt)
{
    _vt->dvsFlushOld = _vt->dvsFlush;

    /*!
     * @page arabledndc
     *  Vernalization
     */
        if ( cbm::flt_greater_zero( (*_vt)->CHILL_UNITS()) &&
             cbm::flt_greater_zero( (*_vt)->GDD_FLOWERING()))
    {

        double const temp_avg( cbm::bound_min( 0.0, mc_.nd_airtemperature));

        chill_units[_vt->slot] += get_chilling_units( temp_avg, (*_vt)->CHILL_TEMP_MAX()) * FTS_TOT_;
        chill_factor[_vt->slot] = get_chilling_factor( _vt->growing_degree_days,
                                                      (*_vt)->GDD_FLOWERING(),
                                                      chill_units[_vt->slot],
                                                      (*_vt)->CHILL_UNITS());
    }
    else
    {
        chill_factor[_vt->slot] = 1.0;
    }
    
    if ( cbm::flt_greater( _vt->growing_degree_days, (*_vt)->GDD_FLOWERING()))
    {
        chill_factor[_vt->slot] = 1.0;
    }

    /*!
     * @page arabledndc
     *  Growing degree days are only upgraded for:
     * \f{eqnarray*}{
     *  T &\overset{!}{>}& max \left(0, \Psi_{TLIMIT} \right) \\
     *  GDD &\overset{!}{<}& \Psi_{GDD\_MATURITY}
     *  \f}
     */
    if ( cbm::flt_less_equal( *mc_temp, (*_vt)->GDD_BASE_TEMPERATURE()) ||
         cbm::flt_less_equal( (*_vt)->GDD_MATURITY(), _vt->growing_degree_days))
    {
        /* no op */
    }
    else
    {
        if ( _vt->growing_degree_days < (*_vt)->GDD_MATURITY())
        {
            _vt->growing_degree_days += cbm::bound_max( (*mc_temp) - (*_vt)->GDD_BASE_TEMPERATURE(), (*_vt)->GDD_MAX_TEMPERATURE()) * chill_factor[_vt->slot] * FTS_TOT_;
        }
    }

    _vt->dvsFlush = cbm::bound( 0.0, _vt->growing_degree_days / (*_vt)->GDD_MATURITY(), 1.0);
    ldndc_kassert( _vt->dvsFlush >= _vt->dvsFlushOld);

    return  LDNDC_ERR_OK;
}


/*!
 * @page arabledndc
 * @section nitrogen_demand Nitrogen demand and crop growth
 *  Carbon assimilation on biomass growth are calculated depending on nitrogen demand and uptake.
 *
 */
double
PhysiologyArableDNDC::nitrogen_demand( MoBiLE_Plant * _vt)
{
    /* plant daily nitrogen demand [kg N/ha/day] */
    double  day_n_demand( 0.0);

    /* straw fraction of total crop biomass */
    double  fraction_straw( 1.0 - (*_vt)->FRACTION_FRUIT() - (*_vt)->FRACTION_ROOT());
    double  fraction_fol( fraction_straw * (*_vt)->FALEAF());
    double  fraction_lst( fraction_straw - fraction_fol);

    double const total_optimum_dry_matter( optimum_aboveground_c( _vt) / ((1.0 - (*_vt)->FRACTION_ROOT()) * cbm::CCDM));

    /* NC_FOLIAGE_MIN is used on purpose since this is the target N-concentration at maturity */
    double const total_optimum_nitrogen = total_optimum_dry_matter * ((*_vt)->FRACTION_FRUIT() * (*_vt)->NC_FRUIT_MAX() +
                                                                      fraction_lst * (*_vt)->NC_STRUCTURAL_TISSUE_MAX() +
                                                                      fraction_fol * (*_vt)->NC_FOLIAGE_MIN() +
                                                                      (*_vt)->FRACTION_ROOT() * (*_vt)->NC_FINEROOTS_MAX());

    // Check wether the crop development has reached its final stage
    if ( cbm::flt_greater( total_optimum_nitrogen, n_plant_vt_[_vt->slot]) &&
         cbm::flt_greater( (*_vt)->GDD_MATURITY(), _vt->growing_degree_days) &&
         cbm::flt_greater( _vt->dvsFlush, _vt->dvsFlushOld))
    {
        if ( cbm::flt_greater_zero( (*_vt)->WINTERLIMIT()) &&
             cbm::flt_greater( lclock_ref().yearday(), _vt->seeding_date.yearday()) &&
             cbm::flt_greater( cbm::CCDM * ( _vt->mFol + _vt->dw_lst + _vt->mBud), cbm::HA_IN_M2 * (*_vt)->WINTERLIMIT()))
        {
            /* no op */
        }
        else
        {
            /* drought stress */
            DroughtStress  droughtstress;
            droughtstress.fh2o_ref = (*_vt)->H2OREF_A();

            double  drought_stress = 1.0;
            if ( (*_vt)->C4_TYPE())
            {
                if ( cbm::flt_in_range( 0.3, _vt->dvsFlush, 0.7))
                {
                    _vt->f_h2o = this->m_state->get_fh2o( _vt);
                    drought_stress = cbm::sqrt( droughtstress.linear_threshold( _vt->f_h2o));
                }
                else
                {
                    _vt->f_h2o = 1.0;
                    drought_stress = 1.0;
                }
            }
            else
            {
                _vt->f_h2o = this->m_state->get_fh2o( _vt);
                drought_stress = droughtstress.linear_threshold( _vt->f_h2o);
            }

            double const delta_dvs( _vt->dvsFlush - _vt->dvsFlushOld);
            double const f_dvs( _vt->dvsFlush / ( 0.5 + _vt->dvsFlush) * ( 1.0 - _vt->dvsFlush) / ( 0.5 + _vt->dvsFlush));
            double const f_development(( _vt->dvsFlush <= 0.5) ? (*_vt)->N_DEMAND_VEG() : (*_vt)->N_DEMAND_REPROD());

            day_n_demand = total_optimum_nitrogen * delta_dvs * f_dvs * f_development * drought_stress;
            day_n_demand = cbm::bound_max( day_n_demand, total_optimum_nitrogen - n_plant_vt_[_vt->slot]);
        }
    }
    else
    {
        return  0.0;
    }

    if ( day_n_demand < 0.0)
    {
        KLOGERROR( "day_n_demand < 0.0 [",day_n_demand,"]: (check parameters N_DEMAND_VEG and N_DEMAND_REPROD)",
                  " N_DEMAND_VEG=",(*_vt)->N_DEMAND_VEG(), "; N_DEMAND_REPROD=",(*_vt)->N_DEMAND_REPROD(),
                  "; total N demand=",total_optimum_nitrogen, "; plant N=",n_plant_vt_[_vt->slot],
                  "; pgi(t)=",_vt->dvsFlush, "; pgi(t-1)=",_vt->dvsFlushOld);
        return  invalid_t< double >::value;
    }
    double const fact_co2( cbm::poly3( ac_.nd_co2_concentration, -5.47e-02, 4.3e-03, -4.0e-06, 1.0e-09));
    if ( fact_co2 < 0.0)
    {
        KLOGERROR( "fact_co2 < 0.0:",
                  " Check CO2 concentration and time settings in your airchemistry input",
                  "  [id=",this->object_id(),",co2=",ac_.nd_co2_concentration,"]");
        return  invalid_t< double >::value;
    }

    return  (day_n_demand * fact_co2);
}


/*

 @subsection respiration Respiration ans Exsudation

 DNDC treats root respiration in dependency on root age, which results in
 unrealistic high respiration rates for winter crops during wintertime.
 Therefore the model considers root biomass instead of root age for root
 respiration (\f$R_{CO2,root}\f$) which is calculated proportional to the
 ratio of the root weight at the current development stage relative to the
 root weight at optimum yield (\f$BM_{OY,root}\f$) \f[ R_{CO2,root} = RESP
 \cdot \frac{ BM_{root} }{ BM_{OY,root} } \cdot  f_{RT} \f] where
 \f$f_{RT}\f$ reduces the root respiration linearly with an optimum value of
 35 degree C and RESP is the respiration constant adapting the root activity
 to sites with differing nutrition status.

 Based on root respiration, the respiration of leaves (\f$R_{CO2,leaf}\f$),
 stem (\f$R_{CO2,stem}\f$) and grains (\f$R_{CO2,grain}\f$) are

 \f[ R_{CO2,leaf}  = R_{CO2,root} \cdot BM_{leaf}  / BM_{root} \f]
 \f[ R_{CO2,stem}  = R_{CO2,root} \cdot BM_{stem}  / BM_{root} \f]
 \f[ R_{CO2,grain} = R_{CO2,root} \cdot BM_{grain} / BM_{root} \f]

 Growth respiration (\f$R_{CO2,growth}\f$) is estimated as a fraction of daily growth
 \f[ R_{CO2,growth} = N_{uptake} \frac{ C }{ C_{plant} } \cdot 0.25 \f]

 According to Kracher (2011) 0.8 g C is turned into new biomass per g C used
 for growth and respiration, which corresponds to a growth respiration
 fraction of 0.25 of daily growth. Daily C assimilation (\f$C_{assi}\f$)

 \f[ C_{assi} = N_{uptake} \cdot C/N_{plant} + R_{CO2,growth} + R_{CO2,root} + R_{CO2,leaf} + R_{CO2,stem} + R_{CO2,grain} \f]

 is calculated to determine daily water demand.

 Root exudation (EXUD) is a multiple of the root respiration and thus linked to
 root activity
 \f[ EXUD=f_{EXUD} \cdot R_{CO2,root} \f]

 EXUD is part of the the soil litter pools.

 In contrast to DNDC, where the root biomass is available as one single pool
 and root respiration is assumed to be homogeneously distributed throughout
 the 5O cm soil profile The model enables simulations of deeper soil profiles
 as well as an individual characterization of the total soil profile
 discretized into layers. Roots influence soil processes due to an increase
 of the CO2 concentration in the soil as well as a release of root exudates.
 A homogeneous distribution of root biomass is not appropriate any more as
 root biomass decreases drastically below 30 cm for most crop types. Hence
 the continuouse fraction of root biomass

 \f[ \delta_{root}(z) = exp \left( \frac{-(z-\delta_{root,Max} )^2}{\pi_1} \right)^{-1/\pi_2} + \delta_{root,Min} \f]

 \u2061 describes the distribution of root biomass and root respiration in
 the soil profile with the shape parameters \f$\pi_1\f$ as width and
 \f$\pi_2\f$ as the amplitude of the root biomass distribution. Equ. \f$
 \delta_{root}(z) \f$  describs the distribution of root biomass among the
 soil profile depending on four parameters:

 @li
 the depth with the highest root biomass fraction (\f$ \delta_{root,Max}\f$),
 @li
 the width of the curve (\f$\pi_1\f$),
 @li
 the amplitude of the curve maximum (\f$\pi_1\f$) and
 @li
 the minimum fraction of root biomass for the deeper soil layers (\f$\delta_{root,Min}\f$).

 After defining the shape of the distribution curve it is normalized by the integral
 of the calculated biomass fractions for the soil profile
 \f[ \delta_{root,profile} = \int_0^{depth_{profile}} \delta_{root}(z) dz \f]
 to ensure that the fractions of the root biomass add up to one.

 */
void
PhysiologyArableDNDC::growth_respiration( MoBiLE_Plant *  _vt,
                                          double  _day_n_growth)
{
    if ( cbm::flt_greater_zero( _day_n_growth))
    {
        _vt->rGro = GROWTH_RESPIRATION_FRACTION * _day_n_growth * _vt->cn_ratio();

        double const dc_sum( _vt->dcBud + _vt->dcFol + _vt->dcSap + _vt->dcFrt);
        if( (*_vt)->TUBER())
        {
            _vt->rGroBelow = ( cbm::flt_greater_zero( dc_sum) ? (_vt->dcFrt + _vt->dcBud) / dc_sum * _vt->rGro : 0.0);
        }
        else
        {
            _vt->rGroBelow = ( cbm::flt_greater_zero( dc_sum) ? _vt->dcFrt / dc_sum * _vt->rGro : 0.0);
        }
    }
    else
    {
        _vt->rGro = 0.0;
        _vt->rGroBelow = 0.0;
    }
}


void
PhysiologyArableDNDC::maintenance_respiration( MoBiLE_Plant *  _vt)
{
    /* temperature dependency of residual-respiration (maintenance) */

    double const f_temp( pow(2.0, ((*mc_temp) - (*_vt)->MAINTENANCE_TEMP_REF()) / 10.0) );

    double const root_age( cbm::flt_greater_zero(_vt->mFrt) ?
                          (_vt->mFrt * cbm::CCDM) / ( (*_vt)->FRACTION_ROOT() * optimum_aboveground_c( _vt) / ( 1.0 - (*_vt)->FRACTION_ROOT()))
                          : 0.0);

    _vt->rFrt = ((*_vt)->RESP() * root_age * f_temp * cbm::HA_IN_M2 * FTS_TOT_);

    if ( cbm::is_valid( root_q_vt_[_vt->slot]) &&
        (root_q_vt_[_vt->slot] > 0u) &&
         cbm::flt_greater_zero( _vt->mFrt))
    {
        double const MCSO( 0.1); // maintenance respiration coefficent for storage organs
        double const rmFrt( _vt->rFrt / _vt->mFrt);
        _vt->rBud = MCSO * _vt->mBud * rmFrt;
        _vt->rFol = _vt->mFol * rmFrt;
        _vt->rSap = _vt->dw_lst * rmFrt;

        /* species total residual respiration  [kgC m-2] */
        _vt->rRes = _vt->rFol + _vt->rFrt + _vt->rSap + _vt->rBud;
    }
    else
    {
        _vt->rFrt = 0.0;
        _vt->rBud = 0.0;
        _vt->rFol = 0.0;
        _vt->rSap = 0.0;
        _vt->rRes = 0.0;
    }

    _vt->exsuLoss = (*_vt)->DOC_RESP_RATIO() * _vt->rFrt;

    if ( cbm::flt_greater_zero( _vt->exsuLoss))
    {
        for ( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
        {
            double fdoc( sc_.doc_sl[sl] + sc_.an_doc_sl[sl]);
            if ( cbm::flt_greater_zero( fdoc))
            {
                fdoc = sc_.doc_sl[sl] / fdoc;
            }
            else
            {
                fdoc = 0.5;
                sc_.doc_sl[sl] = 0.0;
                sc_.an_doc_sl[sl] = 0.0;
            }

            double  add_doc( _vt->exsuLoss * _vt->fFrt_sl[sl]);
            sc_.doc_sl[sl]    += add_doc * fdoc;
            sc_.an_doc_sl[sl] += add_doc * ( 1.0 - fdoc);
            sc_.accumulated_c_root_exsudates_sl[sl] += add_doc;
        }
    }
}


/*!
 * @page arabledndc
 * @section nitrogenuptake Nitrogen Uptake
 *  Crops are able to take up \f$NO_3^-\f$ and \f$NH_4^+\f$.
 */
void
PhysiologyArableDNDC::nitrogen_uptake()
{
    for ( CropIterator vt = this->m_veg->groupbegin< species::crop >();
          vt != this->m_veg->groupend< species::crop >(); ++vt)
    {
        MoBiLE_Plant *p = *vt;

        n_uptake_vt_[p->slot] = (ph_.accumulated_no3_uptake_sl.sum() +
                                 ph_.accumulated_nh4_uptake_sl.sum() +
                                 ph_.accumulated_don_uptake_sl.sum()) * -1.0;

        double plant_n( p->total_nitrogen());
        double n_opt( p->total_nitrogen() + n_demand_vt_[p->slot] );

        root_system[p->slot]->nitrogen_uptake(
                                               root_q_vt_[p->slot],
                                               plant_n,
                                               n_opt,
                                               1.0,
                                               sc_.no3_sl,
                                               ph_.accumulated_no3_uptake_sl,
                                               sc_.nh4_sl,
                                               ph_.accumulated_nh4_uptake_sl,
                                               sc_.don_sl,
                                               ph_.accumulated_don_uptake_sl,
                                               temp_sl,
                                               p);

        n_uptake_vt_[p->slot] += (ph_.accumulated_no3_uptake_sl.sum() +
                                  ph_.accumulated_nh4_uptake_sl.sum() +
                                  ph_.accumulated_don_uptake_sl.sum());
    }
}


/*
 @subsection nitrogenfixation Nitrogen Fixation

 N-fixing plants (legumes), indicated by an N fixation factor
 (\f$f_{Nfix}>1\f$), \f$N_{demand}\f$ can be fulfilled if the N taken up from
 the soil was not sufficient. The amount of total atmospheric N fixed by the
 legumes during the growing season (\f$N_{fix}\f$)

 \f[ N_{fix} = \left( 1 - \frac{1}{f_{Nfix} } \right) \cdot  N_{Max} \f]

 is regulated by \f$f_{Nfix}\f$.

 The current N content of the plant (\f$N_{plant}\f$) is updated by the daily
 N uptake from soil and atmospheric N and distributed to all plant
 compartments (roots, straw and grains). The current fractions of straw,
 grains, and roots (\f$\delta_{root}\f$, \f$\delta_{straw}\f$ and
 \f$\delta_{grain}\f$) are changing during the growing season
 depending on crop type and development stage.

 The development phase for all plants is separated in a period before and
 after grains start to grow (GST). The definition of GST as well as the
 calculation of the current fractions of straw, grains, and roots are
 dependent on the crop types, see the table below.

 <TABLE>
 <TR><TD></TD><TD>PGI \< GST</TD><TD>PGI \> GST</TD></TR>
 <TR><TD>Crops </TD><TD> </TD><TD> </TD></TR>
 <TR><TD>\f$\delta_{straw}\f$ </TD><TD> </TD><TD> </TD></TR>
 <TR><TD>\f$\delta_{grain}\f$ </TD><TD> </TD><TD> </TD></TR>
 <TR><TD>\f$\delta_{root}\f$  </TD><TD> </TD><TD> </TD></TR>
 <TR><TD>Grassy Plants </TD><TD> </TD><TD> </TD></TR>
 <TR><TD>\f$\delta_{straw}\f$ </TD><TD> </TD><TD> </TD></TR>
 <TR><TD>\f$\delta_{grain}\f$ </TD><TD> </TD><TD> </TD></TR>
 <TR><TD>\f$\delta_{root}\f$  </TD><TD> </TD><TD> </TD></TR>
 <TR><TD>Vegetables </TD><TD> </TD><TD> </TD></TR>
 <TR><TD>\f$\delta_{straw}\f$ </TD><TD> </TD><TD> </TD></TR>
 <TR><TD>\f$\delta_{grain}\f$ </TD><TD> </TD><TD> </TD></TR>
 <TR><TD>\f$\delta_{root}\f$  </TD><TD> </TD><TD> </TD></TR>
 </TABLE>

 GST is assumed to be equal for grassy plants and crops (PGI = 0.5), but is
 assumed to occur later for vegetables (PGI = 0.8). The distribution of
 biomass for vegetables is also calculated in dependency on the days that
 have passed since the sowing date (GrowDays).

 */
double
PhysiologyArableDNDC::nitrogen_fixation( MoBiLE_Plant *  _vt,
                                         double  _day_n_demand,
                                         double  _day_n_uptake)
{
    double  day_n_growth( _day_n_uptake);

    double const  total_nitrogen_fixation( this->total_fixed_nitrogen( _vt, total_nitrogen_demand( _vt)));

    if ( cbm::flt_greater_zero( total_nitrogen_fixation))
    {
        double const  pot_n_fix(( _vt->dvsFlush * total_nitrogen_fixation) - _vt->a_fix_n );

        if (( _day_n_uptake < _day_n_demand) && ( pot_n_fix > 0.0))
        {
            double const n_fix( std::min( _day_n_demand - _day_n_uptake, pot_n_fix));
            day_n_growth += n_fix;
            _vt->d_n2_fixation += n_fix;
            _vt->a_fix_n += n_fix;
        }
    }
    n_plant_vt_[_vt->slot] += day_n_growth;

    /* [kg N m-2 day-1] */
    return  day_n_growth;
}


double
PhysiologyArableDNDC::total_fixed_nitrogen(
                                           MoBiLE_Plant const *  _vt,
                                           double  _total_n_demand)
const
{
    return  ((*_vt)->INI_N_FIX() * _total_n_demand);
}


/*

 @subsection partitioning Carbon and Nitrogen partitioning

 The crop biomass (BM) is decomposed into root (\f$BM_{root}\f$), stem
 (\f$BM_{stem}\f$), leaf (\f$BM_{leaf}\f$) and grain (\f$BM_{grain}\f$)
 biomass. The biomass of the grains (\f$BM_{grain}\f$) and roots
 (\f$BM_{root}\f$) are calculated from \f$N_{plant}\f$ by use of the
 current biomass fractions and their constant C/N ratios. The straw
 biomass is further distributed to the stem (\f$BM_{stem}\f$) and
 leaves (\f$BM_{leaf}\f$) by using the parameter \f$\delta_{leaf}\f$
 (the fraction of the leaves from the straw biomass).

 \f[ BM_{grain} = N_{plant}  \cdot \delta_{grain} \cdot C/N_{grain} \f]
 \f[ BM_{root} = N_{plant}   \cdot \delta_{root}  \cdot C/N_{root} \f]
 \f[ BM_{leaf} = N_{plant}   \cdot \delta_{straw} \cdot C/N_{straw} \cdot \delta_{leaf} \f]
 \f[ BM_{stem} = N_{plant}   \cdot \delta_{straw} \cdot C/N_{grain} \cdot (1-\delta_{leaf} ) \f]




 @section biomass Biomass growth

 Maximum nitrogen uptake and biomass growth (yield) are defined by a set of
 species-specific parameters:
 - Maximum amounts of total carbon ( \f$C_{max}\f$) and nitrogen
 (\f$N_{max}\f$) assimilation under optimal conditions
 - Allocation coefficients for the crop compartments root (\f$ \delta_{root} \f$),
 straw (\f$ \delta_{straw} \f$) and grain (\f$ \delta_{grain} \f$) biomass
 - C/N ratios for each crop compartment


 The daily increment of plant development \f$ \Delta DVS_i \f$ is used to determine
 daily nitrogen demand \f$N_{demand,i}\f$:

 \f[
 N_{demand,i} = N_{Max} \cdot \Delta DVS_i \cdot \frac{ GDD }{0.5 + GDD}
 \cdot \frac{ 1-GDD }{ 0.5+GDD } \cdot f_{w}
 \cdot f_{d} \cdot f_{CO_2} \cdot f_{GDD}
 \f]

 The maximum amount of N assimilation is
 The factors \f$ f_{w}, f_{d}, f_{CO_2} \text{and} f_{GDD} \f$
 represent environmental influences on crop growth due to water logging, drought,
 atmospheric \f$ f_{CO_2} \f$ concentration and crop development stage,
 respectively.

 The influence of atmospheric\f$ f_{CO_2} \f$ concentration is given by:

 \f[
 f_{CO2} = - 5.47 \cdot 10^{-2} \cdot CO_2
 + 4.3 \cdot 10^{-3} \cdot CO_2^2
 + 4.0 \cdot 10^{-6} \cdot CO_2^2
 + 1.0 \cdot 10^{-9} \cdot CO_2^2
 \f]


 \f$N_{Max}\f$ represents the maximum potential nitrogen content of the
 plant, which corresponds to the nitrogen content at \f$C_{Max}\f$.

 \f$N_{Max}\f$ is estimated by using the plant C/N ratio

 \f[ N_{Max} = \frac{ C_{Max} }{C/N}  \f]

 \f[ C/N = \left( \frac{\delta_{root}}{C/N_{root} } + \frac{ \delta_{straw} }{C/N_{straw} } + \frac{\delta_{grain} }{C/N_{grain} } \right)^{-1} \f]

 where \f$\delta_root\f$, \f$\delta_straw\f$ and \f$\delta_grain\f$ are the
 fractions of the roots, straw and grains, respectively, at maturity state,
 which usually corresponds to the harvest date and \f$C/N_{root}\f$,
 \f$C/N_{straw}\f$ and \f$C/N_{grain}\f$ are their respective C/N ratios.
 Both characteristics are crop specific parameters in the model modeled by
 the input_class_species_tPar class. The straw fraction represents leaves and stem biomass.


 Stress conditions resulting from a high water table due to irrigation/
 flooding as a management event are realized by \f$f_{flood}\f$. As rice
 plants are not stressed by higher water table, \f$f_{flood,rice}\f$ is
 always one. For all other plants \f$f_{flood}\f$ decreases with increasing
 length of the flooding event (FloodingDays): \f$ f_{flood} =
 1/(FloodingDays+1)\f$. Water stress \f$f_{WS}\f$ is calculated per soil
 layer in the water cycle module.

 For maize and other C-4 species, \f$f_{WS}\f$ is only taken into account
 when PGI values are above 0.5 (The plant/crop is in reproductive stage). In
 doing so, the square root of \f$f_{WS}\f$ is used instead in the equ. for
 \f$N_{demand}\f$.


 */
void
PhysiologyArableDNDC::biomass_partition(
                                        MoBiLE_Plant *  _vt,
                                        double _day_n_growth)
{
    if ( cbm::flt_equal( 1.0, _vt->dvsFlush))
    {
        return;
    }

    /* grain fraction of total crop biomass */
    double  f_grain( (*_vt)->FRACTION_FRUIT());
    /* root fraction of total crop biomass */
    double  f_root( (*_vt)->FRACTION_ROOT());
    /* straw fraction of total crop biomass */
    double  f_straw( 1.0 - f_grain - f_root);


    double const grain_development( (_vt->growing_degree_days < (*_vt)->GDD_GRAIN()) ? 0.0 :
                                    (_vt->growing_degree_days - (*_vt)->GDD_GRAIN()) / ((*_vt)->GDD_MATURITY()- (*_vt)->GDD_GRAIN()));

    //approaching full development of grains linearly for growing_degree_days
    double const f_grain_target( grain_development * (*_vt)->FRACTION_FRUIT());

    // Distinguish between growth development and dormacy of the crop.
    if ( cbm::flt_greater_zero( _vt->dvsFlush))
    {
        double const m_bud_target( f_grain_target * _vt->total_biomass());
        if ( cbm::flt_greater( m_bud_target, _vt->mBud))
        {
            f_grain = 1.0;
            f_root = 0.0;
            f_straw = 0.0;
        }
        else
        {
            f_root = (*_vt)->FRACTION_ROOT();
            f_grain = f_grain_target;
            f_straw = (1.0 - f_root - f_grain);
        }
    }


    /* Daily carbon growth via new nitrogen */
    if ( cbm::flt_greater_zero( _day_n_growth))
    {
        _vt->dcBud = cbm::flt_greater_zero( _vt->nc_bud()) ? cbm::CCDM * _day_n_growth * f_grain / _vt->nc_bud() : 0.0;
        _vt->dcFol = cbm::flt_greater_zero( _vt->nc_fol()) ? cbm::CCDM * _day_n_growth * f_straw * (*_vt)->FALEAF() / _vt->nc_fol() : 0.0;
        _vt->dcSap = cbm::flt_greater_zero( _vt->nc_lst()) ? cbm::CCDM * _day_n_growth * f_straw * ( 1.0 - (*_vt)->FALEAF()) / _vt->nc_lst() : 0.0;
        _vt->dcFrt = cbm::flt_greater_zero( _vt->nc_frt()) ? cbm::CCDM * _day_n_growth * f_root / _vt->nc_frt() : 0.0;
    }
    else
    {
        _vt->dcBud = 0.0;
        _vt->dcFol = 0.0;
        _vt->dcSap = 0.0;
        _vt->dcFrt = 0.0;
    }

    double n_growth_lst( _vt->dcSap / cbm::CCDM * _vt->nc_lst());
    
    _vt->mFrt   += _vt->dcFrt / cbm::CCDM;
    _vt->mBud   += _vt->dcBud / cbm::CCDM;
    _vt->mFol   += _vt->dcFol / cbm::CCDM;
    _vt->dw_lst += _vt->dcSap / cbm::CCDM;
                                                                       
    _vt->n_lst += n_growth_lst;

    /* Retranslocation of carbon and nitrogen */

    double const m_bud_before( _vt->mBud);
    double const m_fol_before( _vt->mFol);
    double const m_lst_before( _vt->dw_lst);
    double const m_frt_before( _vt->mFrt);

    double const total_nitrogen_before( _vt->total_nitrogen());

    double const c_above_now( _vt->aboveground_biomass() * cbm::CCDM);
    double const c_above_optimum_now( _vt->dvsFlush * optimum_aboveground_c( _vt));

    //nitrogen satisfaction depending on carbon biomass
    double  nitrogen_satisfaction( cbm::flt_greater_zero( c_above_optimum_now) ?
                                   cbm::bound( 0.0,
                                               c_above_now / c_above_optimum_now,
                                               1.0) : 1.0);


    //adjust nitrogen concentrations in grain and foliage
    if ( cbm::flt_greater_zero( _vt->mBud))
    {
        //decrease nitrogen concentration in foliage with age
        _vt->ncFol -= (_vt->ncFol - ((1.0 - grain_development) * (*_vt)->NC_FOLIAGE_MAX()
                                     + grain_development * (*_vt)->NC_FOLIAGE_MIN())) * 0.1 * FTS_TOT_;
        _vt->ncFol = cbm::bound((*_vt)->NC_FOLIAGE_MIN(), _vt->ncFol, (*_vt)->NC_FOLIAGE_MAX());


        double const m_bud_pot( f_grain_target * _vt->total_biomass());
        if ( cbm::flt_greater( m_bud_pot, _vt->mBud) &&
             cbm::flt_greater( _vt->nc_bud(), (*_vt)->NC_FRUIT_MIN()))
        {
            _vt->ncBud -= (_vt->ncBud - (*_vt)->NC_FRUIT_MIN()) * 0.1 * FTS_TOT_;
            _vt->ncBud = cbm::bound((*_vt)->NC_FRUIT_MIN(), _vt->ncBud, (*_vt)->NC_FRUIT_MAX());

            nitrogen_satisfaction = (_vt->nc_bud() - (*_vt)->NC_FRUIT_MIN())  / ((*_vt)->NC_FRUIT_MAX() - (*_vt)->NC_FRUIT_MIN()) ;
            nitrogen_satisfaction = cbm::bound(0.0, nitrogen_satisfaction , 1.0);
        }
    }


    //adjust nitrogen concentrations in stem and roots
    double nc_lst( cbm::flt_greater_zero( _vt->dw_lst) ? _vt->n_lst / _vt->dw_lst : 0.0);
    nc_lst -= (nc_lst - (nitrogen_satisfaction * (*_vt)->NC_STRUCTURAL_TISSUE_MAX()
                                 + (1.0 - nitrogen_satisfaction) * (*_vt)->NC_STRUCTURAL_TISSUE_MIN())) * 0.1 * FTS_TOT_;
    nc_lst = cbm::bound((*_vt)->NC_STRUCTURAL_TISSUE_MIN(), nc_lst, (*_vt)->NC_STRUCTURAL_TISSUE_MAX());
    _vt->n_lst = nc_lst * _vt->dw_lst;
    
    _vt->ncFrt -= (_vt->ncFrt - (nitrogen_satisfaction * (*_vt)->NC_FINEROOTS_MAX()
                                 + (1.0 - nitrogen_satisfaction) * (*_vt)->NC_FINEROOTS_MIN())) * 0.1 * FTS_TOT_;
    _vt->ncFrt = cbm::bound((*_vt)->NC_FINEROOTS_MIN(), _vt->ncFrt, (*_vt)->NC_FINEROOTS_MAX());

    double const total_nitrogen_after( _vt->total_nitrogen());


    if ( cbm::flt_greater( total_nitrogen_before, total_nitrogen_after) &&
         cbm::flt_greater_zero( _vt->mBud))
    {
        double const n_surplus( total_nitrogen_before - total_nitrogen_after);
        _vt->mBud += n_surplus / _vt->nc_bud();
    }
    else
    {
        double const scale( total_nitrogen_before / total_nitrogen_after);
        _vt->mBud *= scale;
        _vt->mFol *= scale;
        _vt->dw_lst *= scale;
        _vt->n_lst *= scale;
        _vt->mFrt *= scale;
    }

    //SCALE DOWN BIOMASS INCREASE NC
    if ( cbm::flt_greater_zero( _vt->dvsFlush))
    {
        double const scale( _vt->aboveground_biomass() * cbm::CCDM > _vt->dvsFlush *  optimum_aboveground_c( _vt) ?
                            _vt->dvsFlush * optimum_aboveground_c( _vt) / (_vt->aboveground_biomass() * cbm::CCDM) : 1.0);
        
        if ( cbm::flt_greater( 1.0, scale))
        {
            if ( cbm::flt_greater_zero( _vt->mBud))
            {
                double const n_bud_old( _vt->mBud * _vt->nc_bud());
                _vt->mBud *= scale;
                _vt->ncBud = n_bud_old / _vt->mBud;
            }
            if ( cbm::flt_greater_zero( _vt->dw_lst))
            {
                _vt->dw_lst *= scale;
            }
        }
        
        //SHIFT
        if ( cbm::flt_greater_zero( _vt->mBud))
        {
            if ( cbm::flt_greater_zero( _vt->mFol) && cbm::flt_greater( 1.0, scale))
            {
                double const n_fol_old( _vt->mFol * _vt->ncFol);
                double const n_target( _vt->mFol * scale * _vt->ncFol);
                double const n_transfer( (n_fol_old - n_target) * 0.1 * FTS_TOT_);
                _vt->mFol -= n_transfer / _vt->ncFol;
                _vt->mBud += n_transfer / _vt->nc_bud();
            }
            
            if ( cbm::flt_greater_zero( _vt->mFrt) && cbm::flt_greater( _vt->mFrt, _vt->total_biomass() * (*_vt)->FRACTION_ROOT()))
            {
                double const n_surplus( (_vt->mFrt - _vt->total_biomass() * (*_vt)->FRACTION_ROOT()) * _vt->ncFrt);
                double const n_transfer( n_surplus * 0.1 * FTS_TOT_);
                _vt->mFrt -= n_transfer / _vt->ncFrt;
                _vt->mBud += n_transfer / _vt->nc_bud();
            }
        }
        else
        {
            if ( cbm::flt_greater_zero( _vt->mFol) && cbm::flt_greater( 1.0, scale))
            {
                double const n_fol_old( _vt->mFol * _vt->ncFol);
                _vt->mFol *= scale;
                double const n_transfer( n_fol_old - (_vt->mFol * _vt->ncFol));
                _vt->mFrt += n_transfer / _vt->ncFrt;
            }
        }
    }

    _vt->dcBud += (_vt->mBud - m_bud_before) * cbm::CCDM;
    _vt->dcFol += (_vt->mFol - m_fol_before) * cbm::CCDM;
    _vt->dcSap += (_vt->dw_lst - m_lst_before) * cbm::CCDM;
    _vt->dcFrt += (_vt->mFrt - m_frt_before) * cbm::CCDM;
}


/*
 @subsection senescence Senescence

 In addition, for winter crops, an aboveground biomass limit \f$AGB_{limit}\f$
 for the winter season was introduced. Respecitve limitation does not exist in
 the original DNDC model.

 @subsection lai Leaf area index

 Leaf area Index \f$LAI\f$ is determined by the biomass and the species-specific
 paraemter specific leaf weight \f$SLA\f$:

 \f[  LAI = \frac{ BM_{leaf} + BM_{stem} }{SLN} \f]
 */
void
PhysiologyArableDNDC::senescence( MoBiLE_Plant *  _vt)
{
    if ( !cbm::flt_greater_zero( (*_vt)->SENESCENCE_FROST()))
    {
        return;
    }

    /*************************************/
    /* senescence of aboveground biomass */
    /*************************************/

    double const s_fact_temp_shoot( ((*mc_temp) < S_TLIMIT) ?
                                   cbm::bound(0.0, (*_vt)->SENESCENCE_FROST() * std::abs( (*mc_temp) - S_TLIMIT)/10.0 * FTS_TOT_, 1.0)
                                   : 0.0);

    if ( cbm::flt_greater_zero( s_fact_temp_shoot))
    {
        double const s_fol_dm( _vt->mFol * s_fact_temp_shoot);
        double const s_lst_dm( _vt->dw_lst * s_fact_temp_shoot);
        double const s_bud_dm( _vt->mBud * s_fact_temp_shoot);

        double const s_fol_n( _vt->n_fol() * s_fact_temp_shoot);
        double const s_lst_n( _vt->n_lst * s_fact_temp_shoot);
        double const s_bud_n( _vt->n_bud() * s_fact_temp_shoot);

        _vt->sFol += (s_fol_dm + s_lst_dm);
        _vt->nLitFol += (s_fol_n + s_lst_n);

        // in case storage organ is belowground
        // we add bud senescence to fine root senescence
        if ((*_vt)->TUBER() == true)
        {
            for ( size_t  sl = 0;  sl < root_q_vt_[_vt->slot];  ++sl)
            {
                _vt->sFrt_sl[sl] += s_bud_dm * _vt->fFrt_sl[sl];
                _vt->nLitFrt_sl[sl] += s_bud_n * _vt->fFrt_sl[sl];
            }
        }
        else
        {
            _vt->sBud += s_bud_dm;
            _vt->nLitBud += s_bud_n;
        }

        _vt->mBud -= s_bud_dm;
        _vt->mFol -= s_fol_dm;
        _vt->dw_lst -= s_lst_dm;
        _vt->n_lst -= s_lst_n;

        n_plant_vt_[_vt->slot] -= (s_fol_n + s_lst_n + s_bud_n);

        double c_litter_above( (s_fol_dm + s_lst_dm) * cbm::CCDM);
        double n_litter_above( s_fol_n + s_lst_n);

        if ((*_vt)->TUBER() == true)
        {
            double const c_litter_below( s_bud_dm * cbm::CCDM);
            double const n_litter_below( s_bud_n);

            for ( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
            {
                sc_.c_raw_lit_1_sl[sl] += c_litter_below * _vt->fFrt_sl[sl] * (1.0 - (*_vt)->CELLULOSE() - (*_vt)->LIGNIN());
                sc_.c_raw_lit_2_sl[sl] += c_litter_below * _vt->fFrt_sl[sl] * (*_vt)->CELLULOSE();
                sc_.c_raw_lit_3_sl[sl] += c_litter_below * _vt->fFrt_sl[sl] * (*_vt)->LIGNIN();
                sc_.accumulated_c_litter_below_sl[sl] += c_litter_below * _vt->fFrt_sl[sl];
                
                sc_.n_raw_lit_1_sl[sl] += n_litter_below * _vt->fFrt_sl[sl] * (1.0 - (*_vt)->CELLULOSE() - (*_vt)->LIGNIN());
                sc_.n_raw_lit_2_sl[sl] += n_litter_below * _vt->fFrt_sl[sl] * (*_vt)->CELLULOSE();
                sc_.n_raw_lit_3_sl[sl] += n_litter_below * _vt->fFrt_sl[sl] * (*_vt)->LIGNIN();
                sc_.accumulated_n_litter_below_sl[sl] += n_litter_below * _vt->fFrt_sl[sl];
            }
        }
        else
        {
            c_litter_above += s_bud_dm * cbm::CCDM;
            n_litter_above += s_bud_n;
        }

        sc_.c_raw_lit_1_above += c_litter_above * (1.0 - (*_vt)->CELLULOSE() - (*_vt)->LIGNIN());
        sc_.c_raw_lit_2_above += c_litter_above * (*_vt)->CELLULOSE();
        sc_.c_raw_lit_3_above += c_litter_above * (*_vt)->LIGNIN();
        sc_.accumulated_c_litter_above += c_litter_above;
        
        sc_.n_raw_lit_1_above += n_litter_above * (1.0 - (*_vt)->CELLULOSE() - (*_vt)->LIGNIN());
        sc_.n_raw_lit_2_above += n_litter_above * (*_vt)->CELLULOSE();
        sc_.n_raw_lit_3_above += n_litter_above * (*_vt)->LIGNIN();
        sc_.accumulated_n_litter_above += n_litter_above;
    }


    /*************************************/
    /* senescence of belowground biomass */
    /*************************************/

    double s_frt_dw( 0.0);
    for ( size_t  sl = 0;  sl < root_q_vt_[_vt->slot];  ++sl)
    {
        double const s_fact_temp_root_sl( (temp_sl[sl] < S_TLIMIT) ?
                                         cbm::bound(0.0, (*_vt)->SENESCENCE_FROST() * std::abs( temp_sl[sl] - S_TLIMIT)/10.0 * FTS_TOT_, 1.0)
                                         : 0.0);
        s_frt_dw += (_vt->mFrt * _vt->fFrt_sl[sl] * s_fact_temp_root_sl);
    }

    if ( cbm::flt_greater_zero( s_frt_dw))
    {
        double const s_frt_n( s_frt_dw * _vt->ncFrt);

        for ( size_t  sl = 0;  sl < root_q_vt_[_vt->slot];  ++sl)
        {
            _vt->sFrt_sl[sl] += s_frt_dw * _vt->fFrt_sl[sl];
            _vt->nLitFrt_sl[sl] += s_frt_n * _vt->fFrt_sl[sl];
        }

        _vt->mFrt -= s_frt_dw;

        root_system[_vt->slot]->update_root_fraction_sigmoid( 1.0, root_q_vt_[_vt->slot], 0.005, 10.0, 120.0, 10.0, _vt);
    }
}


lerr_t
PhysiologyArableDNDC::register_ports( cbm::io_kcomm_t *  _io_kcomm)
{
    int rc_harvest = this->m_HarvestEvents.subscribe( "harvest", _io_kcomm);
    if ( rc_harvest != LD_PortOk)
        { return  LDNDC_ERR_FAIL; }

    int rc_plant = this->m_PlantEvents.subscribe( "plant", _io_kcomm);
    if ( rc_plant != LD_PortOk)
        { return  LDNDC_ERR_FAIL; }

    return LDNDC_ERR_OK;
}


lerr_t
PhysiologyArableDNDC::unregister_ports( cbm::io_kcomm_t *)
{
    this->m_HarvestEvents.unsubscribe();
    this->m_PlantEvents.unsubscribe();

    return  LDNDC_ERR_OK;
}


/*
 * @brief checks carbon and nitrogen balance each time step
 *
 */
void
PhysiologyArableDNDC::balance_check( int  _stage)
{
    double tot_c( 0.0);
    double tot_n( 0.0);
    for ( CropIterator vt = this->m_veg->groupbegin< species::crop >();
          vt != this->m_veg->groupend< species::crop >(); ++vt)
    {
        MoBiLE_Plant *p = *vt;
        tot_c += ((p->mFol + p->mFrt + p->dw_lst + p->mBud
                   + cbm::sum( p->sFrt_sl, sl_.soil_layer_cnt())
                   + p->sFol + p->sBud) * cbm::CCDM
                  + p->rGro + p->rRes + p->exsuLoss
                  - cbm::sum( p->carbonuptake_fl, m_setup.canopylayers()));
        
        tot_n += (p->total_nitrogen()
                  + cbm::sum( p->nLitFrt_sl, sl_.soil_layer_cnt())
                  + p->nLitFol + p->nLitBud
                  - m_pf.accumulated_n_uptake() - p->a_fix_n);
    }

    if ( _stage == ON_ENTRY)
    {
        tot_c_ = tot_c;
        tot_n_ = tot_n;
    }
    else if ( _stage == ON_EXIT)
    {
        double difference( tot_c_ - tot_c);
        if ( cbm::flt_greater( std::abs( difference), cbm::DIFFMAX))
        {
            KLOGWARN("C-Leak:  difference is ", difference * cbm::M2_IN_HA, " kg C ha-1");
        }

        difference = tot_n_ - tot_n;
        if ( cbm::flt_greater( std::abs( difference), cbm::DIFFMAX))
        {
            KLOGWARN("N-Leak:  difference is ", difference * cbm::M2_IN_HA, " kg N ha-1  ", n_uptake_vt_.sum());
        }
    }
    else
    { KLOGFATAL( "[BUG]  Unknown stage for Balance Check"); }
}

} /*namespace ldndc*/
