/*!
 *
 */

#include  "physiology/dndc/common.h"



/*!
 * @brief
 *
 */
double
ldndc::optimum_aboveground_biomass( MoBiLE_Plant const * _vt)
{
    return (*_vt)->M_FRUIT_OPT() / (*_vt)->FRACTION_FRUIT() * (1.0 - (*_vt)->FRACTION_ROOT());
}



/*!
 * @brief
 *
 */
double
ldndc::optimum_aboveground_c( MoBiLE_Plant const * _vt)
{
    return optimum_aboveground_biomass( _vt) * cbm::CCDM;
}



/*!
 * @brief
 *
 */
double
ldndc::total_nitrogen_demand( MoBiLE_Plant const * _vt, double const *_optimum_aboveground_dry_weight)
{
    double dw_above_opt;
    if ( _optimum_aboveground_dry_weight == NULL)
    {
        dw_above_opt = optimum_aboveground_biomass( _vt);
    }
    else
    {
        dw_above_opt = (*_optimum_aboveground_dry_weight);
    }
    
    double const dw_below_opt( dw_above_opt * (*_vt)->FRACTION_ROOT() / (1.0 - (*_vt)->FRACTION_ROOT()));    
    double const dw_fruit_opt( (*_vt)->FRACTION_FRUIT() * dw_above_opt);
    double const dw_foliage_opt( (1.0 - (*_vt)->FRACTION_FRUIT()) * (*_vt)->FALEAF() * dw_above_opt);
    double const dw_living_structural_tissue_opt( (1.0 - (*_vt)->FRACTION_FRUIT()) * (1.0 - (*_vt)->FALEAF()) * dw_above_opt);
    
    return  dw_below_opt * (*_vt)->NC_FINEROOTS_MAX()
          + dw_fruit_opt * (*_vt)->NC_FRUIT_MAX()
          + dw_foliage_opt * (*_vt)->NC_FOLIAGE_MIN()
          + dw_living_structural_tissue_opt * (*_vt)->NC_STRUCTURAL_TISSUE_MAX();
}
