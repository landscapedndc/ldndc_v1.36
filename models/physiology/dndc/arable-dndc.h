/*!
 * @file
 * @author
 *    - Ruediger Grote
 *    - David Kraus
 * @date  Mar 04, 2012
 */

#ifndef  LM_PHYSIOLOGY_ARABLE_DNDC_H_
#define  LM_PHYSIOLOGY_ARABLE_DNDC_H_


#include  "mbe_legacymodel.h"
#include  "state/mbe_state.h"
#include  "ld_shared.h"

#include  "physiology/ld_plantfunctions.h"
#include  "eventhandler/graze/graze.h"
#include  "eventhandler/cut/cut.h"

#include  <containers/lgrowarray.h>
#include  <utils/cbm_utils.h>

namespace ldndc {

    struct BaseRootSystemDNDC;
    class  LDNDC_API  PhysiologyArableDNDC  :  public  MBE_LegacyModel
    {
        LMOD_EXPORT_MODULE_INFO(PhysiologyArableDNDC,"physiology:arabledndc","ArableDNDC");

        /***  module specific constants  ***/

        /*! growth respiration fraction (Kracher (2011)) */
        static double const  GROWTH_RESPIRATION_FRACTION;

        /*! initial biomass if no other value is given as management [kg ha-1] */
        static const double  INITIAL_BIOMASS;

        /*! temperature limit for death of grass (not to be confused with temperature limit for growth) */
        static const double  S_TLIMIT;

    public:
        PhysiologyArableDNDC( MoBiLE_State *,
                             cbm::io_kcomm_t *, timemode_e);
        ~PhysiologyArableDNDC();

        lerr_t  configure( ldndc::config_file_t const *);
        lerr_t  register_ports( cbm::io_kcomm_t *);
        lerr_t  initialize();

        lerr_t  solve();
        lerr_t  unregister_ports( cbm::io_kcomm_t *);
        lerr_t  finalize() { return  LDNDC_ERR_OK; }

        lerr_t  sleep() { return  LDNDC_ERR_OK; }
        lerr_t  wake() { return  LDNDC_ERR_OK; }

    private:
        MoBiLE_State *  m_state;
        cbm::io_kcomm_t *  io_kcomm;

        /* required input classes */
        input_class_setup_t const &  m_setup;
        input_class_soillayers_t const &  sl_;
        input_class_species_t const *  m_species;
        input_class_speciesparameters_t const * m_speciesparameters;

        /* required state components */
        substate_airchemistry_t const &  ac_;
        substate_microclimate_t &  mc_;
        substate_physiology_t &  ph_;
        substate_soilchemistry_t &  sc_;
        substate_watercycle_t &  wc_;

        MoBiLE_PlantVegetation *  m_veg;

        SubscribedEvent<LD_EventHandlerQueue>  m_HarvestEvents;
        SubscribedEvent<LD_EventHandlerQueue>  m_PlantEvents;

        ldndc::growable_array < BaseRootSystemDNDC*, 1000, 1 >  root_system;

        double const *mc_temp;

        double const FTS_TOT_;

        /*!
         * @brief
         *     All kind of plant related functions.
         */
        LD_PlantFunctions  m_pf;

        double tot_c_;
        double tot_n_;

        lvector_t< double >  n_demand_vt_;
        lvector_t< double >  n_uptake_vt_;
        lvector_t< double >  n_avail_tot_vt_;
        lvector_t< double >  n_plant_vt_;

        lvector_t< double >  dw_fol_old_vt;
        lvector_t< double >  dw_frt_old_vt;
        lvector_t< double >  dw_lst_old_vt;
        lvector_t< double >  dw_bud_old_vt;

        /*!
         * @brief
         *  Factor that retards plant development if vernalization requirement is not fullfilled.
         */
        lvector_t< double >  chill_factor;

        /*!
         * @brief
         *  Accumulated chilling units for vernalization;
         */
        lvector_t< double >  chill_units;

        /*!
         * @brief
         *    largest soil layer index where roots exist [-]
         *
         * @note
         *    updated by means of class internal method
         *    @fn update_deepest_rooted_soil_layer_index_
         */
        lvector_t< unsigned int >  root_q_vt_;

        lvector_t< double > temp_sl;

        /***  methods  ***/

        /*!
         * @brief
         *    preform pre/post run initialization each time step
         */
        lerr_t  ArableDNDC_step_init();
        lerr_t  ArableDNDC_step_out();
        lerr_t  ArableDNDC_resize();

        /*!
         * @brief
         *    update state items specific to species
         */
        void  update_species_related_state_items( MoBiLE_Plant *);

        /*!
         * @brief
         *    handle plant event for crop given by index. the
         *    method checks itself if a plant event is pending.
         *
         * @param
         *    species
         * @param
         *    index of crop in species properties related arrays (<A>_vt)
         *
         * @return
         *    LDNDC_ERR_OK if no plant event is pending
         *    or everything went well.
         *
         *    ...
         */
        lerr_t  ArableDNDC_event_plant( MoBiLE_Plant *, EventAttributes const &);

        /*!
         * @brief
         *    handle harvest event for crop given by index.
         *    the method checks itself if a harvest event
         *    is pending.
         *
         * @param
         *    species
         * @param
         *    index of crop in species properties related arrays (<A>_vt)
         *
         * @return
         *    LDNDC_ERR_OK if no harvest event is pending
         *    or everything went well.
         *
         *    ...
         */
        lerr_t  event_harvest( MoBiLE_Plant *, EventAttributes const &);

        /*!
         * @brief
         *    development_stage advances the crop growth for one day
         *
         *    The method adds the daily mean temperature to the TDD sum
         *    and updates the plant growth index PGI according to the
         *    new TDD:
         *
         *    \f[ PGI = \frac{TDD}{TDD_{max}} \;\;\; for T > T_{limit}  \f]
         *
         * @param
         *    species
         * @param
         *    index of crop in species properties related arrays (<A>_vt)
         */
        lerr_t  development_stage( MoBiLE_Plant *);

        void  growth_respiration( MoBiLE_Plant *, double);

        /*!
         * @brief
         *    Root Respiration
         *    @n
         *    The root growth is depending on a growth parameter with a
         *    default value of 1 cm per day and the maximum root depth is 1 m.
         *
         *    DNDC treats root respiration in dependency on root age, which
         *    results in unrealistic high respiration rates for winter crops
         *    during wintertime. Therefore the model considers root biomass
         *    instead of root age for root respiration (\f$R_{CO2,root}\f$)
         *    which is calculated proportional to the ratio of the root weight
         *    at the current development stage relative to the root weight at
         *    optimum yield (\f$BM_{OY,root}\f$) \f[ R_{CO2,root} = RESP \cdot
         *    \frac{ BM_{root} }{ BM_{OY,root} } \cdot  f_{RT} \f] where
         *    \f$f_{RT}\f$ reduces the root respiration linearly with an optimum
         *    value of 35 degree C and RESP is the respiration constant adapting
         *    the root activity to sites with differing nutrition status.

         *    Based on root respiration, the respiration of leaves (\f$R_{CO2,leaf}\f$),
         *    stem (\f$R_{CO2,stem}\f$) and grains (\f$R_{CO2,grain}\f$) are
         *
         *     \f[ R_{CO2,leaf}  = R_{CO2,root} \cdot BM_{leaf}  / BM_{root} \f]
         *     \f[ R_{CO2,stem}  = R_{CO2,root} \cdot BM_{stem}  / BM_{root} \f]
         *     \f[ R_{CO2,grain} = R_{CO2,root} \cdot BM_{grain} / BM_{root} \f]
         *
         *    Growth respiration (\f$R_{CO2,growth}\f$) is estimated as a fraction
         *    of daily growth
         *
         *    \f[ R_{CO2,growth} = N_{uptake} \frac{ C }{ C_{plant} } \cdot 0.25 \f]
         *
         *    According to Kracher (2011) 0.8 g C is turned into new biomass per g C
         *    used for growth and respiration, which corresponds to a growth
         *    respiration fraction of 0.25 of daily growth.
         *
         * @param
         *    species
         * @param
         *    index of crop in species properties related arrays (<A>_vt)
         */
        void  maintenance_respiration( MoBiLE_Plant *);

        /*!
         * @brief
         *    calculates the daily N demand of each crop.
         *
         *    The daily crop growth \f$ \Delta PGI\f$ (increase of PGI) is used to
         *    determine the plant daily nitrogen demand (\f$N_{demand}\f$)
         *
         *     \f[ N_{demand} = N_{Max} \cdot \Delta PGI \cdot \frac{ PGI }{0.5 + PGI} \cdot \frac{ 1-PGI }{ 0.5+PGI } \cdot 5 \cdot f_{flood} \cdot f_{WS} \cdot f_{CO2}  \f]
         *
         *    except for perennial grass
         *
         *     \f[ N_{demand} = N_{Max} \cdot \Delta PGI \f]
         *
         *    and for maize where \f$N_{demand,maize}\f$ is calculated by using the
         *    factor 3 instead of 5 before grains start growing and 20 with grain
         *    evolution. \f$N_{Max}\f$ represents the maximum potential nitrogen
         *    content of the plant, which corresponds to the nitrogen content at CMax.
         *    The CO2 impact on the nitrogen demand by the CO2 impact factor is
         *    calculated by a cubic polynome of CO2
         *
         *    \f[ f_{CO2} = 10^{-09} \cdot (CO_2)^3 - 4 \cdot 10^{-06} \cdot (CO_2)^2 + 4.3 \cdot 10^{-3} \cdot CO_2 - 0.0547 \f]
         *
         * @param
         *    species
         * @param
         *    index of crop in species properties related arrays (<A>_vt)
         *
         * @return
         *    nitrogen demand for species [kg N/ha/day]
         */
        double  nitrogen_demand( MoBiLE_Plant *);

        /*!
         * @brief
         *    nitrogen uptage by the crop
         *
         * @param
         *    species
         * @param
         *    index of crop in species properties related arrays (<A>_vt)
         * @param
         *    nitrogen demand [kg N/ha/day]
         *
         * @return
         *    nitrogen uptake [kg N/ha/day]
         */
        void  nitrogen_uptake();

        /*!
         * @brief
         *    nitrogen fixation by legumes
         *
         * @param
         *    species
         * @param
         *    nitrogen demand [kg N/ha/day]
         * @param
         *    nitrogen uptake [kg N/ha/day]
         *
         * @return
         *    day growth nitrogen uptake [kg N/ha/day]
         */
        double  nitrogen_fixation( MoBiLE_Plant *, double, double);
        double  total_fixed_nitrogen( MoBiLE_Plant const *, double) const;

        /*!
         * @brief
         *    Daily crop growth and partitioning of biomass C into grain, straw
         *    and root
         *
         * @n
         *    The development phase for all plants is separated in a period
         *    before and after grains start to grow (GST). The definition of GST
         *    as well as the calculation of the current fractions of straw,
         *    grains, and roots are dependent on the crop types. (see table tab.
         *    1.)
         *
         *    GST is assumed to be equal for grassy plants and crops (PGI =
         *    0.5), but is assumed to occur later for vegetables (PGI = 0.8).
         *    The distribution of biomass for vegetables is also calculated in
         *    dependency on the days that have passed since the sowing date
         *    (GrowDays).

         *    The crop biomass (\f$BM\f$) is decomposed into root
         *    (\f$BM_{root}\f$), stem (\f$BM_{stem}\f$), leaf (\f$BM_{leaf}\f$)
         *    and grain (\f$BM_{grain}\f$) biomass. The biomass of the grains
         *    (\f$BM_{grain}\f$) and roots (\f$BM_{root}\f$) are calculated from
         *    \f$N_{plant}\f$ by use of the current biomass fractions and their
         *    constant C/N ratios. The straw biomass is further distributed to
         *    the stem (\f$BM_{stem}\f$) and leaves (\f$BM_{leaf}\f$) by using
         *    the parameter \f$\delta_{leaf}\f$ (the fraction of the leaves from
         *    the straw biomass).
         *
         *    \f[ BM_{grain} = N_{plant} \cdot \delta_{grain} \cdot C/N_{grain}  \f]
         *    \f[ BM_{root}  = N_{plant} \cdot \delta_{root} \cdot  C/N_{root} \f]
         *    \f[ BM_{leaf}  = N_{plant} \cdot \delta_{straw} \cdot C/N_{straw} \cdot \delta_leaf \f]
         *    \f[ BM_{stem}  = N_{plant} \cdot \delta_{straw} \cdot C/N_{grain} \cdot (1-\delta_{leaf} ) \f]
         *
         * @param
         *    species
         * @param
         *    daily nitrogen growth [kg N/ha/day]
         *
         * @return
         *    carbon rhizodeposition [kg doc-C/ha/d]
         */
        void  biomass_partition( MoBiLE_Plant *, double);

        /*!
         * @brief
         *    Crop senescence accounts for the plant aging due to frost.
         *
         *    The method determines a senescence parameter dPG which determines the
         *    senescence rate of the plant biomass (leaf, stem, root and grain).
         *    @n
         *    Crop senescence is implemented only for 2 crop types:
         *    @li Cover crop id == (corn)
         *
         *            \f[ dPG = Min(1.0,Abs( T_{mean} - T_{limit,senescence}) / 10)  \f]
         *
         *        with \f$ T_{mean} \f$ as daily mean temerature
         *        and \f$ T_{limit,senescence} \f$ as senescence limit
         *        temperature (constant set to 0 deg. C)
         *
         *    @li Crop type = (Perennial grass)
         *
         *            \f[ dPG = \frac{1.0}{365.0  \cdot  f_{H2O} }  \f]
         *
         *         with \f$ f_{H2O} \f$ as a species specific drought
         *         stress factor. dPG is limited to 0.03 at maximum
         *         and set to 0.02 if \f$ T_{mean} - T_{limit,senescence} \f$.
         *
         * @param
         *    species
         * @param
         *    index of crop in species properties related arrays (<A>_vt)
         */
        void  senescence( MoBiLE_Plant *);

        /* coverage */
        void  update_canopy_coverage( MoBiLE_Plant *);

        /* plant height */
        void  update_canopy_height( MoBiLE_Plant *);

        /* canopy structure */
        void  update_canopy_items( MoBiLE_Plant *);

        /* C & N balance checker */
        void  balance_check( int /*stage*/);
    };
} /*namespace ldndc*/

#endif  /*  !LM_PHYSIOLOGY_ARABLE_DNDC_H_  */

