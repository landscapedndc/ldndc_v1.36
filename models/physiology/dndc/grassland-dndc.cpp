/*!
 * @file
 * @author
 *    - Ruediger Grote
 *    - David Kraus
 * @date  Mar 04, 2012
 *
 */

/*!
 * @page grasslanddndc
 * @tableofcontents
 * @section grasslanddndc_guide User guide
 * The grassland growth model GrasslandDNDC originates from the DNDC Model (@cite li:1994a).
 * GrasslandDNDC simulates the carbon and nitrogen cycle of grass species only (no crops). Processes are described in a universal way
 * and plants are primarily distinguished by species-specific parameters that can be accessed and calibrated externally.
 *
 * @subsection grasslanddndc_guide_structure Model structure
 * GrasslandDNDC can be either run in subdaily or daily time resolution.
 *
 * @subsection grasslanddndc_guide_parametrization Parametrization
 * The following lists includes species parameters that might be calibrated in order to represent a specific plant.
 * See the description of respective sections for more details on parameter behaviour.
 *
 * Nitrogen related parameters:
 * - nc_fruit_max (nitrogen content of the fruit)
 * - nc_fineroots_max (nitrogen content of fine roots)
 * - nc_structural_tissue_max (nitrogen content of structural tissue)
 *
 * Allocation related parameters:
 * - root (assimilated carbon fraction allocated to roots)
 * - grain (assimilated carbon fraction allocated to the fruit)
 * - faleaf (determines fraction of carbon that is allocated to leafs)
 *
 * Plant development related parameters:
 * - gdd_base_temperature
 * - gdd_maturity
 *
 *
 * Drought
 * - h2oref_a (determines drought resistance)
 *
 * Nitrogen uptake
 * - tlimit (minimum temperature required for nitrogen uptake)
 * - k_mm_nitrogen_uptake (root affinity to soil nitrogen)
 *
 * Nitrogen fixation
 * - ini_n_fix (fraction of total nitrogen that might be fixed)
 *
 * Respiration
 * - maintenance_temp_ref (reference temperature for maintenance respiration)
 * - mc_root (maintenance respiration coefficient for roots)
 *
 * Root exsudation
 * - doc_resp_ratio (ratio of doc exsudation in relation to root respiration)
 *
 * Senescence
 * - senescence_drought (coefficient of senescence related to drought)
 * - senescence_frost (coefficient of senescence related to frost)
 * - senescence_age (coefficient of senescence related to age)
 *
 * Fineroots turnover
 * - tofrtbas
 *
 * water demand
 * - wuecmax (water use efficiency)
 *
 * Structure
 * - exp_root_distribution (coefficient for exponential root distribution)
 * - height_max (maximum plant height)
 */

#include  "physiology/dndc/grassland-dndc.h"
#include  "physiology/ld_rootsystem.h"
#include  "physiology/ld_transpiration.h"
#include  "physiology/dndc/common.h"
#include  "watercycle/ld_droughtstress.h"

#include  <input/soillayers/soillayers.h>
#include  <input/species/species.h>

#include  <constants/cbm_const.h>
#include  <math/cbm_math.h>

#include  <logging/cbm_logging.h>
#include  <numeric>

LMOD_MODULE_INFO(PhysiologyGrasslandDNDC,TMODE_SUBDAILY|TMODE_POST_DAILY,LMOD_FLAG_USER);

namespace ldndc {
#define  ON_ENTRY 1
#define  ON_EXIT 2

REGISTER_OPTION(PhysiologyGrasslandDNDC, exponentialrootdistribution,"  [bool]");


double const  PhysiologyGrasslandDNDC::GROWTH_RESPIRATION_FRACTION = 0.25;
double const  PhysiologyGrasslandDNDC::S_TLIMIT = 0.0;

PhysiologyGrasslandDNDC::PhysiologyGrasslandDNDC(
                                 MoBiLE_State *  _state,
                                 cbm::io_kcomm_t *  _io_kcomm,
                                 timemode_e  _timemode)
                                : MBE_LegacyModel(
                                                       _state,
                                                       _timemode),

                                m_state( _state),
                                io_kcomm( _io_kcomm),

                                m_setup( _io_kcomm->get_input_class< input_class_setup_t >()),
                                m_soillayers( _io_kcomm->get_input_class< input_class_soillayers_t >()),
                                m_species( _io_kcomm->get_input_class< species::input_class_species_t >()),

                                ac_( _state->get_substate_ref< substate_airchemistry_t >()),
                                mc_( _state->get_substate_ref< substate_microclimate_t >()),
                                ph_( _state->get_substate_ref< substate_physiology_t >()),
                                sc_( _state->get_substate_ref< substate_soilchemistry_t >()),
                                wc_( _state->get_substate_ref< substate_watercycle_t >()),
                                m_veg( &_state->vegetation),
                                root_system(0, NULL),
                                mc_temp( (_timemode == TMODE_SUBDAILY) ? &mc_.ts_airtemperature : &mc_.nd_airtemperature),
                                FTS_TOT_( 1.0 / LD_RtCfg.clk->time_resolution()),
                                m_pf( _state, _io_kcomm),
                                m_eventgraze( _state, _io_kcomm, _timemode),
                                m_eventcut( _state, _io_kcomm, _timemode),
                                no3_sl( m_soillayers->soil_layer_cnt(), 0.0),
                                nh4_sl( m_soillayers->soil_layer_cnt(), 0.0),
                                don_sl( m_soillayers->soil_layer_cnt(), 0.0)
{
    delta_c_at_planting = 0.0;
    tot_c_ = 0.0;
    tot_n_ = 0.0;
}



PhysiologyGrasslandDNDC::~PhysiologyGrasslandDNDC()
{
    for ( size_t  r = 0;  r < root_system.size();  ++r)
    {
        if ( root_system[r])
        {
            LD_Allocator->destroy( root_system[r]);
        }
    }
}



lerr_t
PhysiologyGrasslandDNDC::configure( 
                                   ldndc::config_file_t const *)
{
    exponential_root_distribution = get_option< bool >( "exponentialrootdistribution", false);

    return  LDNDC_ERR_OK;
}



lerr_t
PhysiologyGrasslandDNDC::initialize()
{
    return  LDNDC_ERR_OK;
}



lerr_t
PhysiologyGrasslandDNDC::solve()
{
    (void)this->step_init();

    while ( this->m_PlantEvents)
    {
        EventAttributes  ev_plant = this->m_PlantEvents.pop();
        char const *  group = ev_plant.get( "/group", "?");
        if ( cbm::is_equal( group, "grass")) // TODO could be done at "push-time"
        {
            // TODO  create plant..
            MoBiLE_Plant *  vt = this->m_veg->get_plant( ev_plant.get( "/name", "?"));
            if ( !vt)
            { return LDNDC_ERR_RUNTIME_ERROR; }

            lerr_t  rc_plant = this->event_plant( vt, ev_plant);
            if ( rc_plant)
            {
                KLOGERROR( "Handling plant event failed  [species=", vt->name(),"]");
                return  LDNDC_ERR_FAIL;
            }
            if ( cbm::is_invalid( this->root_q_vt_[vt->slot]))
            {
                KLOGERROR( "[BUG]  Rooting depth not initialized;",
                          " did we 'plant' the species?  [species=", vt->name(),"]");
                return  LDNDC_ERR_RUNTIME_ERROR;
            }
        }
    }

    GrasslandDNDCBalanceCheck( ON_ENTRY);

    GrasslandDNDCCuttingGrazing();

    GrasslandDNDCPlantDevelopment();

    for ( GrassIterator vt = this->m_veg->groupbegin< species::grass >();
            vt != this->m_veg->groupend< species::grass >(); ++vt)
    {
        MoBiLE_Plant *p = *vt;

        /* nitrogen demand [kgN m-2] */
        this->n_demand_vt_[p->slot] = this->nitrogen_demand( *vt);
        if (this->n_demand_vt_[p->slot] < 0.0)
        {
            KLOGERROR( "Got negative nitrogen demand.  [species=",(*vt)->name(),"]");
            return  LDNDC_ERR_FAIL;
        }
    }

    this->nitrogen_uptake();

    for ( GrassIterator vt = this->m_veg->groupbegin< species::grass >();
            vt != this->m_veg->groupend< species::grass >(); ++vt)
    {
        MoBiLE_Plant *p = *vt;
        
        /* N2 fixation [kgN m-2] */
        double  day_n_growth( this->nitrogen_fixation( *vt, this->n_demand_vt_[p->slot], this->n_uptake_vt_[p->slot]));

        GrasslandDNDCPhotosynthesis( *vt, day_n_growth);

        /* total growth respiration  [kgC m-2] */
        this->growth_respiration( *vt);

        /* root respiration and exsudation */
        this->maintenance_respiration( *vt);

        /* root development */
        if ( dc_frt_vt[p->slot] > 0.0)
        {
            root_system[p->slot]->root_development( vt->GZRTZ() * FTS_TOT_, this->root_q_vt_[p->slot], *vt);
        }

        /* senescence */
        this->senescence( *vt);

        /* set further properties based on newly calculated ones */
        this->update_species_related_state_items( *vt);

        /* potential transpiration */
        wc_.accumulated_potentialtranspiration +=
        potential_crop_transpiration( ac_.nd_co2_concentration,
                                      std::accumulate(c_upt_vtfl[p->slot].begin(), c_upt_vtfl[p->slot].end(), 0.0),
                                      vt->WUECMAX());
    }

    GrasslandDNDCBalanceCheck( ON_EXIT);

    while ( this->m_HarvestEvents)
    {
        EventAttributes  ev_harv = this->m_HarvestEvents.pop();
        MoBiLE_Plant *  vt = this->m_veg->get_plant( ev_harv.get( "/name", "?"));
        if ( !vt)
            { return LDNDC_ERR_RUNTIME_ERROR; }

        if ( cbm::is_equal( vt->cgroup(), "grass"))
        {
            lerr_t  rc_harv = this->event_harvest( vt, ev_harv);
            if ( rc_harv)
            {
                KLOGERROR( "Handling harvest event failed  [species=", vt->name(),"]");
                return  LDNDC_ERR_FAIL;
            }
            // TODO  delete plant..
        }
    }

    (void)this->step_out();

    return  LDNDC_ERR_OK;
}


/*!
 * @page grasslanddndc
 * @section grasslanddndc_cutting_grazing Cutting and grazing
 *  After cutting and grazing events plant development is linearly reduced
 *  with nitrogen loss.
 *  \f[
 *  DVS(t_{i+1}) = \frac{N(t_{i+1})}{N(t_i)} \cdot DVS(t_i)
 *  \f]
 */
lerr_t
PhysiologyGrasslandDNDC::GrasslandDNDCCuttingGrazing()
{
    /* grazing */
    if ( (lclock()->subday() == 1))
    {
        m_eventgraze.reset_daily_food_consumption();
    }

    m_eventgraze.update_available_freshfood_c( this->m_veg, species_groups_select_t< species::grass >());

    for ( GrassIterator vt = this->m_veg->groupbegin< species::grass >();
         vt != this->m_veg->groupend< species::grass >(); ++vt)
    {
        double const plant_n_old_graze( (*vt)->total_nitrogen());
        lerr_t  rc_graze = m_eventgraze.event_graze_physiology( *vt);
        if (rc_graze == LDNDC_ERR_EVENT_MATCH)
        {
            double const reduction_factor( cbm::flt_greater_zero( plant_n_old_graze) ? (*vt)->total_nitrogen() / plant_n_old_graze : 0.0);
            (*vt)->dvsFlush *= reduction_factor;
            (*vt)->dvsFlushOld *= reduction_factor;
            (*vt)->growing_degree_days *= reduction_factor;
        }
    }


    /* cutting */
    double plant_n_old_cut( 0.0);
    for ( GrassIterator vt = this->m_veg->groupbegin< species::grass >();
         vt != this->m_veg->groupend< species::grass >(); ++vt)
    {
        plant_n_old_cut += (*vt)->total_nitrogen();
    }

    lerr_t  rc_cut = m_eventcut.event_cut_physiology( m_veg, species_groups_select_t< species::grass >());
    if (rc_cut == LDNDC_ERR_EVENT_MATCH)
    {
        double plant_n_new( 0.0);
        for ( GrassIterator vt = this->m_veg->groupbegin< species::grass >();
             vt != this->m_veg->groupend< species::grass >(); ++vt)
        {
            plant_n_new += (*vt)->total_nitrogen();
        }

        for ( GrassIterator vt = this->m_veg->groupbegin< species::grass >();
             vt != this->m_veg->groupend< species::grass >(); ++vt)
        {
            double const reduction_factor( cbm::flt_greater_zero( plant_n_old_cut) ? plant_n_new / plant_n_old_cut : 0.0);
            (*vt)->dvsFlush *= reduction_factor;
            (*vt)->dvsFlushOld *= reduction_factor;
            (*vt)->growing_degree_days *= reduction_factor;
        }
    }

    return  LDNDC_ERR_OK;
}

lerr_t
PhysiologyGrasslandDNDC::step_out()
{
    if ( this->timemode() == TMODE_SUBDAILY)
    {
        for ( PlantIterator vt = this->m_veg->begin(); vt != this->m_veg->end(); ++vt)
        {
            MoBiLE_Plant *p = (*vt);

            p->n2_fixation = n2_fixation_vt[p->slot];

            p->dcFol = dc_fol_vt[p->slot];
            p->dcSap = dc_lst_vt[p->slot];
            p->dcBud = dc_bud_vt[p->slot];
            p->dcFrt = dc_frt_vt[p->slot];

            p->rRes = r_res_vt[p->slot];
            p->rGro = r_gro_vt[p->slot];
            p->rGroBelow = r_gro_below_vt[p->slot];
            p->rFol = r_fol_vt[p->slot];
            p->rSap = r_lst_vt[p->slot];
            p->rBud = r_bud_vt[p->slot];
            p->rFrt = r_frt_vt[p->slot];

            p->sFol = s_fol_vt[p->slot];
            p->sBud = s_bud_vt[p->slot];

            p->nLitFol = n_lit_fol_vt[p->slot];
            p->nLitBud = n_lit_bud_vt[p->slot];

            p->exsuLoss = exsuLoss_vt[p->slot];

            for ( size_t  sl = 0;  sl < m_soillayers->soil_layer_cnt();  ++sl)
            {
                p->sFrt_sl[sl] = s_frt_vtsl[p->slot][sl];
                p->nLitFrt_sl[sl] = n_lit_frt_vtsl[p->slot][sl];
            }

            for ( size_t  fl = 0;  fl < m_setup->canopylayers();  ++fl)
            {
                p->carbonuptake_fl[fl] = c_upt_vtfl[p->slot][fl];
            }
        }
    }
    else if ( this->timemode() == TMODE_POST_DAILY)
    {
        for ( PlantIterator vt = this->m_veg->begin(); vt != this->m_veg->end(); ++vt)
        {
            MoBiLE_Plant *p = *vt;

            p->d_n2_fixation = n2_fixation_vt[p->slot];

            p->d_dcFol = dc_fol_vt[p->slot];
            p->d_dcSap = dc_lst_vt[p->slot];
            p->d_dcBud = dc_bud_vt[p->slot];
            p->d_dcFrt = dc_frt_vt[p->slot];

            p->d_rRes = r_res_vt[p->slot];
            p->d_rGro = r_gro_vt[p->slot];
            p->d_rGroBelow = r_gro_below_vt[p->slot];
            p->d_rFol = r_fol_vt[p->slot];
            p->d_rSap = r_lst_vt[p->slot];
            p->d_rBud = r_bud_vt[p->slot];
            p->d_rFrt = r_frt_vt[p->slot];

            p->d_sFol = s_fol_vt[p->slot];
            p->d_sBud = s_bud_vt[p->slot];

            p->d_nLitFol = n_lit_fol_vt[p->slot];
            p->d_nLitBud = n_lit_bud_vt[p->slot];

            p->d_exsuLoss = exsuLoss_vt[p->slot];

            for ( size_t  sl = 0;  sl < m_soillayers->soil_layer_cnt();  ++sl)
            {
                p->d_sFrt_sl[sl] = s_frt_vtsl[p->slot][sl];
                p->d_nLitFrt_sl[sl] = n_lit_frt_vtsl[p->slot][sl];
            }

            for ( size_t  fl = 0;  fl < m_setup->canopylayers();  ++fl)
            {
                p->d_carbonuptake_fl[fl] = c_upt_vtfl[p->slot][fl];
            }
        }
    }

    for ( size_t  sl = 0;  sl < m_soillayers->soil_layer_cnt();  ++sl)
    {
        if ( cbm::flt_greater_zero( nh4_sl[sl]))
        {
            sc_.nh4_sl[sl] = nh4_sl[sl];
        }
        else { sc_.nh4_sl[sl] = 0.0; }
        
        if ( cbm::flt_greater_zero( don_sl[sl]))
        {
            sc_.don_sl[sl] = don_sl[sl];
        }
        else { sc_.don_sl[sl] = 0.0; }

        double const no3_tot( sc_.no3_sl[sl] + sc_.an_no3_sl[sl]);
        if ( cbm::flt_greater_zero( no3_tot))
        {
            sc_.no3_sl[sl] = sc_.no3_sl[sl] / no3_tot * no3_sl[sl];
            sc_.an_no3_sl[sl] = no3_sl[sl] - sc_.no3_sl[sl];
        }
        else
        {
            sc_.no3_sl[sl] = no3_sl[sl] * (1.0 - sc_.anvf_sl[sl]);
            sc_.an_no3_sl[sl] = no3_sl[sl] - sc_.no3_sl[sl];
        }
    }

    return  LDNDC_ERR_OK;
}

lerr_t
PhysiologyGrasslandDNDC::step_init()
{
    size_t const  slot_cnt = m_veg->slot_cnt();

    n_demand_vt_.resize_and_preserve( slot_cnt, 0.0);
    n_uptake_vt_.resize_and_preserve( slot_cnt, 0.0);
    n_avail_tot_vt_.resize_and_preserve( slot_cnt, 0.0);

    root_q_vt_.resize_and_preserve( slot_cnt, invalid_uint);

    n2_fixation_vt.resize_and_preserve( slot_cnt, 0.0);

    dc_fol_vt.resize_and_preserve( slot_cnt, 0.0);
    dc_lst_vt.resize_and_preserve( slot_cnt, 0.0);
    dc_bud_vt.resize_and_preserve( slot_cnt, 0.0);
    dc_frt_vt.resize_and_preserve( slot_cnt, 0.0);

    r_res_vt.resize_and_preserve( slot_cnt, 0.0);
    r_gro_vt.resize_and_preserve( slot_cnt, 0.0);
    r_gro_below_vt.resize_and_preserve( slot_cnt, 0.0);
    r_fol_vt.resize_and_preserve( slot_cnt, 0.0);
    r_lst_vt.resize_and_preserve( slot_cnt, 0.0);
    r_bud_vt.resize_and_preserve( slot_cnt, 0.0);
    r_frt_vt.resize_and_preserve( slot_cnt, 0.0);

    s_fol_vt.resize_and_preserve( slot_cnt, 0.0);
    s_bud_vt.resize_and_preserve( slot_cnt, 0.0);

    n_lit_fol_vt.resize_and_preserve( slot_cnt, 0.0);
    n_lit_bud_vt.resize_and_preserve( slot_cnt, 0.0);

    exsuLoss_vt.resize_and_preserve( slot_cnt, 0.0);

    while ( slot_cnt > s_frt_vtsl.size())
    {
        std::vector<double> help_sl( m_soillayers->soil_layer_cnt(), 0.0);
        s_frt_vtsl.push_back( help_sl);
        n_lit_frt_vtsl.push_back( help_sl);
    }

    while ( slot_cnt > c_upt_vtfl.size())
    {
        std::vector<double> help_sl( m_setup->canopylayers(), 0.0);
        c_upt_vtfl.push_back( help_sl);
    }

    if ( this->timemode() == TMODE_SUBDAILY)
    {
        for ( PlantIterator vt = this->m_veg->begin(); vt != this->m_veg->end(); ++vt)
        {
            MoBiLE_Plant *p = *vt;

            n2_fixation_vt[p->slot] = p->n2_fixation;

            dc_fol_vt[p->slot] = p->dcFol;
            dc_lst_vt[p->slot] = p->dcSap;
            dc_bud_vt[p->slot] = p->dcBud;
            dc_frt_vt[p->slot] = p->dcFrt;

            r_res_vt[p->slot] = p->rRes;
            r_gro_vt[p->slot] = p->rGro;
            r_gro_below_vt[p->slot] = p->rGroBelow;
            r_fol_vt[p->slot] = p->rFol;
            r_lst_vt[p->slot] = p->rSap;
            r_bud_vt[p->slot] = p->rBud;
            r_frt_vt[p->slot] = p->rFrt;

            s_fol_vt[p->slot] = p->sFol;
            s_bud_vt[p->slot] = p->sBud;

            n_lit_fol_vt[p->slot] = p->nLitFol;
            n_lit_bud_vt[p->slot] = p->nLitBud;

            exsuLoss_vt[p->slot] = p->exsuLoss;

            for ( size_t  sl = 0;  sl < m_soillayers->soil_layer_cnt();  ++sl)
            {
                s_frt_vtsl[p->slot][sl] = p->sFrt_sl[sl];
                n_lit_frt_vtsl[p->slot][sl] = p->nLitFrt_sl[sl];
            }

            for ( size_t  fl = 0;  fl < p->nb_foliagelayers();  ++fl)
            {
                c_upt_vtfl[p->slot][fl] = p->carbonuptake_fl[fl];
            }
            for ( size_t  fl = p->nb_foliagelayers();  fl < m_setup->canopylayers();  ++fl)
            {
                c_upt_vtfl[p->slot][fl] = 0.0;
            }
        }

        temp_sl.reference( mc_.temp_sl);
    }
    else if ( this->timemode() == TMODE_POST_DAILY)
    {
        for ( PlantIterator vt = this->m_veg->begin(); vt != this->m_veg->end(); ++vt)
        {
            MoBiLE_Plant *p = *vt;

            n2_fixation_vt[p->slot] = p->d_n2_fixation;

            dc_fol_vt[p->slot] = p->d_dcFol;
            dc_lst_vt[p->slot] = p->d_dcSap;
            dc_bud_vt[p->slot] = p->d_dcBud;
            dc_frt_vt[p->slot] = p->d_dcFrt;

            r_res_vt[p->slot] = p->d_rRes;
            r_gro_vt[p->slot] = p->d_rGro;
            r_gro_below_vt[p->slot] = p->d_rGroBelow;
            r_fol_vt[p->slot] = p->d_rFol;
            r_lst_vt[p->slot] = p->d_rSap;
            r_bud_vt[p->slot] = p->d_rBud;
            r_frt_vt[p->slot] = p->d_rFrt;

            s_fol_vt[p->slot] = p->d_sFol;
            s_bud_vt[p->slot] = p->d_sBud;

            n_lit_fol_vt[p->slot] = p->d_nLitFol;
            n_lit_bud_vt[p->slot] = p->d_nLitBud;

            exsuLoss_vt[p->slot] = p->d_exsuLoss;

            for ( size_t  sl = 0;  sl < m_soillayers->soil_layer_cnt();  ++sl)
            {
                s_frt_vtsl[p->slot][sl] = p->d_sFrt_sl[sl];
                n_lit_frt_vtsl[p->slot][sl] = p->d_nLitFrt_sl[sl];
            }

            for ( size_t  fl = 0;  fl < p->nb_foliagelayers();  ++fl)
            {
                c_upt_vtfl[p->slot][fl] = p->d_carbonuptake_fl[fl];
            }
            for ( size_t  fl = p->nb_foliagelayers();  fl < m_setup->canopylayers();  ++fl)
            {
                c_upt_vtfl[p->slot][fl] = 0.0;
            }
        }

        temp_sl.reference( mc_.nd_temp_sl);
    }

    /* reset grow indicators at beginning of year */
    if ( MoBiLE_IsBeginningOfYear( this->lclock(), this->m_setup->latitude()))
    {
        for ( GrassIterator vt = m_veg->groupbegin< species::grass >(); vt != m_veg->groupend< species::grass >(); ++vt)
        {
            MoBiLE_Plant *p( *vt);
            if ( !vt->RATOON())
            {
                p->growing_degree_days = 0.0;
                p->dEmerg = 0;
                p->dvsFlush = 0.0;
                p->dvsFlushOld = 0.0;
                p->dvsMort = 0.0;
                p->a_fix_n = 0.0;
            }
        }
    }

    delta_c_at_planting = 0.0;

    for ( size_t  sl = 0;  sl < m_soillayers->soil_layer_cnt();  ++sl)
    {
        nh4_sl[sl] = sc_.nh4_sl[sl];
        don_sl[sl] = sc_.don_sl[sl];
        no3_sl[sl] = sc_.no3_sl[sl] + sc_.an_no3_sl[sl];
    }
    
    return  LDNDC_ERR_OK;
}



void
PhysiologyGrasslandDNDC::update_species_related_state_items( MoBiLE_Plant *  _vt)
{    
    /* day of foliage budburst */
    if ( cbm::flt_greater_zero( _vt->dvsFlush) && ( _vt->dEmerg == 0))
    {
        _vt->dEmerg = lclock_ref().yearday();
    }
    
    /* does not depend on root mass */
    if ( exponential_root_distribution)
    {
        root_system[_vt->slot]->update_root_fraction_exponential( 1.0, this->root_q_vt_[_vt->slot], (*_vt)->EXP_ROOT_DISTRIBUTION(), _vt);
    }
    else
    {
        root_system[_vt->slot]->update_root_fraction_sigmoid( 1.0, root_q_vt_[_vt->slot], 0.005, 10.0, 120.0, 10.0, _vt);
    }
    
    /* coverage */    
    this->update_canopy_coverage( _vt);
    
    /* plant height */    
    this->update_canopy_height( _vt);
    
    /* canopy structure */
    this->update_canopy_items( _vt);
    
    /* total carbon uptake  [kgC m-2] */
    double const cUpt_tot(  dc_frt_vt[_vt->slot] + dc_bud_vt[_vt->slot] + dc_fol_vt[_vt->slot] + dc_lst_vt[_vt->slot] +
                            r_gro_vt[_vt->slot] + r_res_vt[_vt->slot] + exsuLoss_vt[_vt->slot]);
    size_t const fl_cnt( _vt->nb_foliagelayers());
    double  fl_cnt_inv( 1.0 / (( fl_cnt == 0) ? 1.0 : static_cast< double >( fl_cnt)));
    for (size_t fl = 0; fl < fl_cnt; ++fl)
    {
        c_upt_vtfl[_vt->slot][fl] += (cUpt_tot * fl_cnt_inv);
    }
}



void
PhysiologyGrasslandDNDC::update_canopy_coverage( MoBiLE_Plant *  _vt)
{
    /* totally empirical 'rule of thumb' estimation */
    _vt->f_area = cbm::bound_max( 1.0 / double(this->m_veg->size()), 1.0);
}



void
PhysiologyGrasslandDNDC::update_canopy_height( MoBiLE_Plant *  _vt)
{
    double const c_above( _vt->aboveground_biomass() * cbm::CCDM);
    double const c_above_opt( (*_vt)->MFOLOPT() / (*_vt)->FALEAF() * cbm::CCDM);
    
    _vt->height_max = (c_above / c_above_opt)  * (*_vt)->HEIGHT_MAX();
    _vt->height_max = cbm::bound(0.03, _vt->height_max, (*_vt)->HEIGHT_MAX());
    _vt->height_at_canopy_start = 0.0;

    ph_.update_canopy_layers_height( m_veg);
}



void
PhysiologyGrasslandDNDC::update_canopy_items( MoBiLE_Plant *  _vt)
{
    size_t const  fl_cnt( _vt->nb_foliagelayers());
    double  fl_cnt_inv( 1.0 / (( fl_cnt == 0) ? 1.0 : static_cast< double >( fl_cnt)));
    for (size_t fl = 0; fl < fl_cnt; ++fl)
    {
        _vt->fFol_fl[fl] = fl_cnt_inv;
        _vt->sla_fl[fl] = (*_vt)->SLAMAX();
        _vt->lai_fl[fl] = (_vt->sla_fl[fl] * (_vt->mFol + _vt->dw_lst) * fl_cnt_inv);
    }

    for ( size_t  fl = fl_cnt;  fl < m_setup->canopylayers();  ++fl)
    {
        _vt->fFol_fl[fl] = 0.0;
        _vt->sla_fl[fl] = 0.0;
        _vt->lai_fl[fl] = 0.0;
    }
}



lerr_t
PhysiologyGrasslandDNDC::event_plant(
        MoBiLE_Plant *  _vt, EventAttributes const &  /*_attributes*/)
{
    species_t const *  sp = NULL;
    if ( m_species)
    {
        sp = m_species->get_species( _vt->cname());
    }

    lerr_t rc_plant = m_pf.initialize_grass( _vt, sp ? sp->grass() : NULL);
    if ( rc_plant)
    {
        KLOGERROR( "Plant initialization failed  [species=", _vt->name(),"]");
        return  LDNDC_ERR_FAIL;
    }

    if (!cbm::flt_greater_zero( (*_vt)->FALEAF()))
    {
        KLOGERROR( "Plant initialization failed  [species=", _vt->name(),"] Parameter FALEAF must be greater zero!");
        return  LDNDC_ERR_FAIL;
    }

    _vt->ncBud = (*_vt)->NC_FRUIT_MAX();
    _vt->ncFol = (*_vt)->NC_STRUCTURAL_TISSUE_MAX();
    _vt->n_lst = (*_vt)->NC_STRUCTURAL_TISSUE_MAX() * _vt->dw_lst;
    _vt->ncFrt = (*_vt)->NC_FINEROOTS_MAX();

    delta_c_at_planting = _vt->total_biomass() * cbm::CCDM;
    double n_at_planting =  _vt->total_nitrogen();

    _vt->a_fix_n = 0.0;
    _vt->dvsFlush = 0.0;
    _vt->dvsMort = 0.0;

    _vt->growing_degree_days = 0.0;
    _vt->dEmerg = 0;

    _vt->dw_lst = (0.5 * (1.0 - (*_vt)->FALEAF()) * n_at_planting / (*_vt)->NC_STRUCTURAL_TISSUE_MAX());
    _vt->mFol   = (0.5 * (*_vt)->FALEAF() * n_at_planting / _vt->ncFol);
    _vt->mFrt   = (0.5  * n_at_planting / _vt->ncFrt);
    _vt->mBud   = 0.0;
    delta_c_at_planting -= _vt->total_biomass() * cbm::CCDM;

    /* root system */
    if (root_system.size() <= _vt->slot)
    {
        root_system.resize(_vt->slot+1);
    }

    ldndc_kassert( root_system[_vt->slot] == NULL);
    root_system[_vt->slot] = LD_Allocator->construct_args< RootSystemDNDC >( 1, this->m_state, io_kcomm);

    if ( !root_system[_vt->slot])
    {
        KLOGERROR( "Failed to allocate root-system object");
        return  LDNDC_ERR_NOMEM;
    }

    root_system[_vt->slot]->set_rooting_depth(this->root_q_vt_[_vt->slot], _vt, (*_vt)->ZRTMC());
    if ( exponential_root_distribution)
    {
        root_system[_vt->slot]->update_root_fraction_exponential( 1.0, this->root_q_vt_[_vt->slot], (*_vt)->EXP_ROOT_DISTRIBUTION(), _vt);
    }
    else
    {
        root_system[_vt->slot]->update_root_fraction_sigmoid( 1.0, root_q_vt_[_vt->slot], 0.005, 10.0, 120.0, 10.0, _vt);
    }

    /* coverage */    
    this->update_canopy_coverage( _vt);

    /* plant height */    
    this->update_canopy_height( _vt);

    /* canopy structure */
    this->update_canopy_items( _vt);

    return  LDNDC_ERR_OK;
}



/* harvesting
 *
 */
lerr_t
PhysiologyGrasslandDNDC::event_harvest(
        MoBiLE_Plant *  _vt, EventAttributes const &  _attributes)
{
    if (root_system[_vt->slot] == NULL)
    {
        KLOGERROR( "[BUG]  Object 'root_system' is NULL when not expected \"",_vt->name(),"\"!");
        return  LDNDC_ERR_RUNTIME_ERROR;                        
    }

    double const export_root = _attributes.get( "/fraction-export-rootwood", 0.0);
    double const rootlitter_c( (1.0 - export_root) * _vt->mFrt * cbm::CCDM);
    double const rootlitter_n( (1.0 - export_root) * _vt->mFrt * _vt->ncFrt);
    double const root_c_export( export_root * _vt->mFrt * cbm::CCDM);
    double const root_n_export( export_root * _vt->mFrt * _vt->ncFrt);

    double const straw_c( (_vt->mFol + _vt->dw_lst + s_fol_vt[_vt->slot]) * cbm::CCDM);
    double const straw_n( (_vt->mFol * _vt->ncFol) + n_lit_fol_vt[_vt->slot]
                        + (_vt->n_lst));

    double const bud_c( (_vt->mBud + s_bud_vt[_vt->slot]) * cbm::CCDM);
    double const bud_n( _vt->n_bud() + n_lit_bud_vt[_vt->slot]);

    double bud_c_export( 0.0);
    double bud_n_export( 0.0);

    double straw_c_export( 0.0);
    double straw_n_export( 0.0);

    double stubble_c( 0.0);
    double stubble_n( 0.0);

    char const *  species_name = _attributes.get( "/name", "?");
    if ( !_attributes.get( "/cover-crop", false))
    {
        double remains_relative( _attributes.get( "/remains_relative", invalid_flt));
        double stubble_height( _attributes.get( "/height", invalid_flt));

        if (   !cbm::flt_greater_equal_zero( remains_relative)
            && !cbm::flt_greater_equal_zero( stubble_height))
        {
            KLOGWARN( "harvest event attribute \"remains_relative\" and \"height\" were not set; \"remains_relative\" set to zero",
                    "  [species=", _vt->name(),"]");
            remains_relative = 0.0;
        }
        else  if (   cbm::flt_greater_equal_zero( remains_relative)
                  && cbm::flt_greater_equal_zero( stubble_height))
        {
            KLOGWARN( "harvest event attribute \"remains_relative\" and \"height\" were both set; attribute \"remains_relative\" used",
                    "  [species=", _vt->name(),"]");
        }

        bud_c_export = bud_c;
        bud_n_export = bud_n;

        if ( remains_relative > 0.0)
        {
            stubble_c = remains_relative * straw_c;
            stubble_n = remains_relative * straw_n;

            straw_c_export = (1.0-remains_relative) * straw_c;
            straw_n_export = (1.0-remains_relative) * straw_n;
        }
        else if ( stubble_height > 0.0)
        {
            // 1cm stubble assumed to equal 50 kg C ha-1
            remains_relative = std::min( stubble_height * cbm::CM_IN_M * 0.007, straw_c) / straw_c;
            stubble_c = remains_relative * straw_c;
            stubble_n = remains_relative * straw_n;

            straw_c_export = (1.0-remains_relative) * straw_c;
            straw_n_export = (1.0-remains_relative) * straw_n;
        }
    }
    else
    {
        stubble_c = straw_c + bud_c;
        stubble_n = straw_n + bud_n;
    }

    sc_.c_stubble_lit3 += stubble_c * (*_vt)->LIGNIN();
    sc_.c_stubble_lit2 += stubble_c * (*_vt)->CELLULOSE();
    sc_.c_stubble_lit1 += stubble_c * (1.0 - (*_vt)->LIGNIN() - (*_vt)->CELLULOSE());

    sc_.n_stubble_lit3 += stubble_n * (*_vt)->LIGNIN();
    sc_.n_stubble_lit2 += stubble_n * (*_vt)->CELLULOSE();
    sc_.n_stubble_lit1 += stubble_n * (1.0 - (*_vt)->LIGNIN() - (*_vt)->CELLULOSE());

    sc_.accumulated_c_litter_stubble += stubble_c;
    sc_.accumulated_n_litter_stubble += stubble_n;
    
    cbm::state_scratch_t *  mcom = io_kcomm->get_scratch();
    std::string  mcom_key;
    
    cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:c_fru", species_name);
    mcom->set( mcom_key.c_str(), bud_c);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:c_fru_export", species_name);
    mcom->set( mcom_key.c_str(), bud_c_export);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:n_fru", species_name);
    mcom->set( mcom_key.c_str(), bud_n);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:n_fru_export", species_name);
    mcom->set( mcom_key.c_str(), bud_n_export);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:c_straw", species_name);
    mcom->set( mcom_key.c_str(), straw_c);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:c_straw_export", species_name);
    mcom->set( mcom_key.c_str(), straw_c_export);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:n_straw", species_name);
    mcom->set( mcom_key.c_str(), straw_n);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:n_straw_export", species_name);
    mcom->set( mcom_key.c_str(), straw_n_export);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:c_stubble", species_name);
    mcom->set( mcom_key.c_str(), stubble_c);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:n_stubble", species_name);
    mcom->set( mcom_key.c_str(), stubble_n);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:c_frt", species_name);
    mcom->set( mcom_key.c_str(), rootlitter_c);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:n_frt", species_name);
    mcom->set( mcom_key.c_str(), rootlitter_n);

    ph_.accumulated_c_export_harvest += straw_c_export + bud_c_export + root_c_export;
    ph_.accumulated_n_export_harvest += straw_n_export + bud_n_export + root_n_export;

    ph_.accumulated_c_fru_export_harvest += bud_c_export;
    ph_.accumulated_n_fru_export_harvest += bud_n_export;

    if ( exponential_root_distribution)
    {
        root_system[_vt->slot]->update_root_fraction_exponential( 1.0, this->root_q_vt_[_vt->slot], (*_vt)->EXP_ROOT_DISTRIBUTION(), _vt);
    }
    else
    {
        root_system[_vt->slot]->update_root_fraction_sigmoid( 1.0, root_q_vt_[_vt->slot], 0.005, 10.0, 120.0, 10.0, _vt);
    }

    for ( size_t  l = 0;  l < this->m_soillayers->soil_layer_cnt();  ++l)
    {
        if ( l < this->root_q_vt_[_vt->slot])
        {                
            s_frt_vtsl[_vt->slot][l] += rootlitter_c / cbm::CCDM * _vt->fFrt_sl[l];
            n_lit_frt_vtsl[_vt->slot][l] += rootlitter_n * _vt->fFrt_sl[l];
        }
        _vt->fFrt_sl[l] = 0.0;
    }

    cbm::invalidate( this->root_q_vt_[_vt->slot]);
    _vt->rooting_depth = 0.0;

    _vt->mFol = 0.0;
    for ( size_t fl = 0; fl < m_setup->canopylayers(); ++fl)
    {
        _vt->fFol_fl[fl] = 0.0;
        _vt->lai_fl[fl] = 0.0;
    }

    _vt->dw_lst = 0.0;
    _vt->mBud = 0.0;
    _vt->mBudStart = 0.0;

    _vt->mFrt = 0.0;

    _vt->dEmerg = 0;
    _vt->growing_degree_days = 0.0;
    _vt->dvsFlush = 0.0;
    _vt->dvsFlushOld = 0.0;
    _vt->dvsMort = 0.0;

    _vt->ncBud = 0.0;
    _vt->ncFol = 0.0;
    _vt->ncFrt = 0.0;
    _vt->n_lst = 0.0;

    r_bud_vt[_vt->slot] = 0.0;
    r_fol_vt[_vt->slot] = 0.0;
    r_lst_vt[_vt->slot] = 0.0;
    r_frt_vt[_vt->slot] = 0.0;

    r_res_vt[_vt->slot] = 0.0;
    r_gro_vt[_vt->slot] = 0.0;

    s_bud_vt[_vt->slot] = 0.0;
    s_fol_vt[_vt->slot] = 0.0;

    n_lit_fol_vt[_vt->slot] = 0.0;
    n_lit_bud_vt[_vt->slot] = 0.0;

    dc_bud_vt[_vt->slot] = 0.0;
    dc_fol_vt[_vt->slot] = 0.0;
    dc_frt_vt[_vt->slot] = 0.0;
    dc_lst_vt[_vt->slot] = 0.0;

    exsuLoss_vt[_vt->slot] = 0.0;
    _vt->a_fix_n = 0.0;

    _vt->height_max = 0.0;
    _vt->height_at_canopy_start = 0.0;

    _vt->f_area = 0.0;
    std::fill( c_upt_vtfl[_vt->slot].begin(), c_upt_vtfl[_vt->slot].end(), 0.0);

    LD_Allocator->destroy( root_system[_vt->slot]);
    root_system[_vt->slot] = NULL;

    return  LDNDC_ERR_OK;
}



/*!
 * @page grasslanddndc
 * @section grasslanddndc_plant_development Plant Development
 *  Plant development is calculated by growing degree days (GDD):
 *  \f[
 *  GDD = \sum (T_{avg} - T_{base})
 *  \f]
 *  Plant development is given by:
 *  \f[
 *  DVS = \frac{GDD}{GDD\_MATURITY}
 *  \f]
 */
lerr_t
PhysiologyGrasslandDNDC::GrasslandDNDCPlantDevelopment()
{
    for ( GrassIterator vt = this->m_veg->groupbegin< species::grass >();
          vt != this->m_veg->groupend< species::grass >(); ++vt)
    {
        MoBiLE_Plant *p = *vt;

        p->dvsFlushOld = p->dvsFlush;

        /* check if it's too cold or full growth has been reached. */
        if ( cbm::flt_greater( *mc_temp, vt->GDD_BASE_TEMPERATURE()) &&
             cbm::flt_greater( vt->GDD_MATURITY(), p->growing_degree_days))
        {
            double const delta_t( ((*mc_temp) - vt->GDD_BASE_TEMPERATURE()) * FTS_TOT_);
            double const delta_dvs( delta_t / vt->GDD_MATURITY());

            if ( cbm::flt_greater_equal( *mc_temp, vt->TLIMIT()))
            {
                p->growing_degree_days += delta_t;
            }

            ldndc_kassert( cbm::flt_greater_zero( vt->GDD_MATURITY()));
            p->dvsFlush = cbm::bound( 0.0, p->growing_degree_days / vt->GDD_MATURITY(), 1.0);

            //increase dvsMort_vt also for Temp < TLIMIT
            p->dvsMort = cbm::bound( 0.0, p->dvsMort + delta_dvs, 1.0);
        }
    }

    return  LDNDC_ERR_OK;
}



void
PhysiologyGrasslandDNDC::growth_respiration( MoBiLE_Plant * _vt)
{
    double const dc_sum( dc_bud_vt[_vt->slot] + dc_fol_vt[_vt->slot] + dc_lst_vt[_vt->slot] + dc_frt_vt[_vt->slot]);
    if ( cbm::flt_greater_zero( dc_sum))
    {
        r_gro_vt[_vt->slot] += (GROWTH_RESPIRATION_FRACTION * dc_sum);

        if( (*_vt)->TUBER())
        {
            r_gro_below_vt[_vt->slot] += (dc_frt_vt[_vt->slot] + dc_bud_vt[_vt->slot]) / dc_sum * r_gro_vt[_vt->slot];
        }
        else
        {
            r_gro_below_vt[_vt->slot] += dc_frt_vt[_vt->slot] / dc_sum * r_gro_vt[_vt->slot];
        }
    }
    else
    {
        r_gro_below_vt[_vt->slot] = 0.0;
        r_gro_vt[_vt->slot] = 0.0;
    }
}



void
PhysiologyGrasslandDNDC::maintenance_respiration( MoBiLE_Plant *  _vt)
{
    /* temperature dependency of residual-respiration (maintenance) */

    double const f_temp( pow(2.0, ((*mc_temp) - (*_vt)->MAINTENANCE_TEMP_REF()) / 10.0));

    r_frt_vt[_vt->slot] = ((*_vt)->MC_ROOT() * _vt->mFrt * f_temp * FTS_TOT_);

    if ( cbm::is_valid( this->root_q_vt_[_vt->slot]) &&
        (this->root_q_vt_[_vt->slot] > 0u) &&
         cbm::flt_greater_zero( _vt->mFrt))
    {
        double const rmFrt( r_frt_vt[_vt->slot] / _vt->mFrt);
        r_bud_vt[_vt->slot] = (_vt->mBud * rmFrt);
        r_fol_vt[_vt->slot] = (_vt->mFol * rmFrt);
        r_lst_vt[_vt->slot] = (_vt->dw_lst * rmFrt);
        
        /* species total residual respiration  [kgC m-2] */
        r_res_vt[_vt->slot] = (r_fol_vt[_vt->slot] + r_frt_vt[_vt->slot] + r_lst_vt[_vt->slot] + r_bud_vt[_vt->slot]);
    }
    else
    {
        r_frt_vt[_vt->slot] = 0.0;
        r_bud_vt[_vt->slot] = 0.0;
        r_fol_vt[_vt->slot] = 0.0;
        r_lst_vt[_vt->slot] = 0.0;
        r_res_vt[_vt->slot] = 0.0;
    }
    
    /* Root exsudation */
    
    exsuLoss_vt[_vt->slot] = (*_vt)->DOC_RESP_RATIO() * r_frt_vt[_vt->slot];
    
    if ( cbm::flt_greater_zero( exsuLoss_vt[_vt->slot]))
    {
        for ( size_t  sl = 0;  sl < m_soillayers->soil_layer_cnt();  ++sl)
        {
            double fdoc( sc_.doc_sl[sl] + sc_.an_doc_sl[sl]);
            if ( cbm::flt_greater_zero( fdoc))
            {
                fdoc = sc_.doc_sl[sl] / fdoc;
            }
            else
            {
                fdoc = 0.5;
                sc_.doc_sl[sl] = 0.0;
                sc_.an_doc_sl[sl] = 0.0;
            }

            double  add_doc( exsuLoss_vt[_vt->slot] * _vt->fFrt_sl[sl]);
            sc_.doc_sl[sl]    += add_doc * fdoc;
            sc_.an_doc_sl[sl] += add_doc * ( 1.0 - fdoc);
            
            sc_.accumulated_c_root_exsudates_sl[sl] += add_doc;
        }
    }
}



double
PhysiologyGrasslandDNDC::nitrogen_demand( MoBiLE_Plant *  _vt)
{
    /* plant daily nitrogen demand [kg N/ha/day] */
    double  day_n_demand( 0.0);
        
    /* precompute total nitrogen demand */
    double const optimum_aboveground_dry_weight( (*_vt)->MFOLOPT() / (*_vt)->FALEAF());
    double const total_n_demand( total_nitrogen_demand( _vt, &optimum_aboveground_dry_weight));

    if ( cbm::flt_greater( total_n_demand, _vt->total_nitrogen()) &&
         cbm::flt_greater( (*_vt)->GDD_MATURITY(), _vt->growing_degree_days) &&
         cbm::flt_greater( _vt->dvsFlush, _vt->dvsFlushOld))
    {
        double  delta_dvs( _vt->dvsFlush - _vt->dvsFlushOld);
        double  f_dvs( _vt->dvsFlush / ( 0.5 + _vt->dvsFlush) * ( 1.0 - _vt->dvsFlush) / ( 0.5 + _vt->dvsFlush) * 5.0);

        double age_factor( (_vt->dvsMort < 1.0) ? 2.5 : 2.5 * std::max(1.0-(*_vt)->SENESCENCE_AGE(), 0.0));
        day_n_demand = (total_n_demand * delta_dvs * f_dvs * age_factor);
        day_n_demand = cbm::bound_max( day_n_demand, total_n_demand - _vt->total_nitrogen());
    }

    double const fact_co2( cbm::poly3( ac_.nd_co2_concentration, -5.47e-02, 4.3e-03, -4.0e-06, 1.0e-09));
    if (fact_co2 < 0.0)
    {
        KLOGERROR("fact_co2 < 0.0 in GrasslandDNDC. check co2 concentration in your airchemistry input");
        return  LDNDC_ERR_RUNTIME_ERROR;
    }
    
    return  (day_n_demand * fact_co2);
}



void
PhysiologyGrasslandDNDC::nitrogen_uptake()
{
    n_uptake_vt_ = 0.0;

    {
        for ( GrassIterator vt = this->m_veg->groupbegin< species::grass >();
                vt != this->m_veg->groupend< species::grass >(); ++vt)
        {
            MoBiLE_Plant *p = *vt;

            this->n_avail_tot_vt_[p->slot] = 0.0;
            if ( cbm::flt_equal_zero( this->n_demand_vt_[p->slot]) ||
                 (this->root_q_vt_[p->slot] == 0))
            {
                continue;
            }

            /* drought stress */
            DroughtStress  droughtstress;
            droughtstress.fh2o_ref = vt->H2OREF_A();
            p->f_h2o = this->m_state->get_fh2o( p);
            double const drought_stress( droughtstress.linear_threshold( p->f_h2o));

            for ( size_t  sl = 0;  sl < this->root_q_vt_[p->slot];  ++sl)
            {
                double const mfrt_veg_sl( ph_.mfrt_sl(m_veg, sl));
                if ( !cbm::flt_greater_zero( mfrt_veg_sl))
                {
                    break;
                }

                double const mFrt_vtsl( p->mFrt * p->fFrt_sl[sl]);
                double const fr_mfrt( mFrt_vtsl / mfrt_veg_sl);

                double const fact_frt_h2o( fr_mfrt * drought_stress);
                double const ava_nh4( fact_frt_h2o * (pow( nh4_sl[sl], 2.0) / (nh4_sl[sl] + vt->K_MM_NITROGEN_UPTAKE() / mfrt_veg_sl)));
                double const ava_don( fact_frt_h2o * (pow( don_sl[sl], 2.0) / (don_sl[sl] + vt->K_MM_NITROGEN_UPTAKE() / mfrt_veg_sl)));
                double const ava_no3( fact_frt_h2o * (pow( no3_sl[sl], 2.0) / (no3_sl[sl] + vt->K_MM_NITROGEN_UPTAKE() / mfrt_veg_sl)));
                
                this->n_avail_tot_vt_[p->slot] += (ava_nh4 + ava_don + ava_no3);
            }
        }
    }

    for ( size_t  sl = 0;  sl < this->m_soillayers->soil_layer_cnt();  ++sl)
    {
        double const mfrt_veg_sl( ph_.mfrt_sl(m_veg, sl));
        if ( !cbm::flt_greater_zero( mfrt_veg_sl))
        {
            break;
        }

        for ( GrassIterator vt = this->m_veg->groupbegin< species::grass >();
                vt != this->m_veg->groupend< species::grass >(); ++vt)
        {
            MoBiLE_Plant *p = *vt;
            if (sl >= this->root_q_vt_[p->slot] || cbm::flt_equal_zero( this->n_avail_tot_vt_[p->slot]))
            {
                continue;
            }

            double const layer_frac_min( std::min( this->n_demand_vt_[p->slot] / this->n_avail_tot_vt_[p->slot], 0.99));
            
            double const mFrt_vtsl( p->mFrt * p->fFrt_sl[sl]);
            double const fr_mfrt( mFrt_vtsl / mfrt_veg_sl);

            DroughtStress  droughtstress;
            droughtstress.fh2o_ref = 1.0;
            double const  drought_stress( droughtstress.linear_threshold( m_state->get_fh2o( p)));
            
            double const fact_frt_h2o( fr_mfrt * drought_stress);
            double const ava_nh4( fact_frt_h2o * (pow( nh4_sl[sl], 2.0) / (nh4_sl[sl] + vt->K_MM_NITROGEN_UPTAKE() / mfrt_veg_sl)));
            double const ava_don( fact_frt_h2o * (pow( don_sl[sl], 2.0) / (don_sl[sl] + vt->K_MM_NITROGEN_UPTAKE() / mfrt_veg_sl)));
            double const ava_no3( fact_frt_h2o * (pow( no3_sl[sl], 2.0) / (no3_sl[sl] + vt->K_MM_NITROGEN_UPTAKE() / mfrt_veg_sl)));

            double const n_avail_vtsl( ava_nh4 + ava_don + ava_no3);

            double const layer_demand_scale( this->n_demand_vt_[p->slot] * std::max(n_avail_vtsl / this->n_avail_tot_vt_[p->slot],
                                                                         layer_frac_min) / n_avail_vtsl);
            double const nh4_demand( layer_demand_scale * ava_nh4);
            double const don_demand( layer_demand_scale * ava_don);
            double const no3_demand( layer_demand_scale * ava_no3);

            double const nh4_uptake( std::min(nh4_demand, ava_nh4));
            double const don_uptake( std::min(don_demand, ava_don));
            double const no3_uptake( std::min(no3_demand, ava_no3));

            if ( cbm::flt_greater_zero( nh4_uptake))
            {
                ph_.accumulated_nh4_uptake_sl[sl] += nh4_uptake;
                n_uptake_vt_[p->slot]  += nh4_uptake;

                nh4_sl[sl] -= nh4_uptake;
                if ( !cbm::flt_greater_zero( nh4_sl[sl]))
                {
                    nh4_sl[sl] = 0.0;
                }
            }

            if ( cbm::flt_greater_zero( don_uptake))
            {
                ph_.accumulated_don_uptake_sl[sl] += don_uptake;
                n_uptake_vt_[p->slot]  += don_uptake;

                don_sl[sl] -= don_uptake;
                if ( !cbm::flt_greater_zero( don_sl[sl]))
                {
                    don_sl[sl] = 0.0;
                }
            }

            if ( cbm::flt_greater_zero( no3_uptake))
            {
                ph_.accumulated_no3_uptake_sl[sl] += no3_uptake;
                n_uptake_vt_[p->slot]  += no3_uptake;

                no3_sl[sl] -= no3_uptake;
                if ( !cbm::flt_greater_zero( no3_sl[sl]))
                {
                    no3_sl[sl] = 0.0;
                }
            }
        }
    }
}



double
PhysiologyGrasslandDNDC::nitrogen_fixation( MoBiLE_Plant *  _vt,
        double  _day_n_demand, double  _day_n_uptake)
{
    double  day_n_growth( _day_n_uptake);
    
    double const  total_nitrogen_fixation( this->total_fixed_nitrogen( _vt, total_nitrogen_demand( _vt)));
    
    if ( cbm::flt_greater_zero( total_nitrogen_fixation))
    {                
        double const  pot_n_fix(( _vt->dvsMort * total_nitrogen_fixation) - _vt->a_fix_n );
        
        if (( _day_n_uptake < _day_n_demand) && ( pot_n_fix > 0.0))
        {            
            double const n_fix( std::min( _day_n_demand - _day_n_uptake, pot_n_fix));
            day_n_growth += n_fix;
            n2_fixation_vt[_vt->slot] += n_fix;
            _vt->a_fix_n += n_fix;
        }
    }

    /* [kg N m-2 day-1] */
    return  day_n_growth;
}



double
PhysiologyGrasslandDNDC::total_fixed_nitrogen( MoBiLE_Plant *  _vt, double  _total_n_demand)
const
{
    return  ((*_vt)->INI_N_FIX() * _total_n_demand);
}



/*
* @page grasslanddndc
* @section grasslanddndc_photosynthesis Photosynthesis
*  Photosynthesis is calculated indirectly via nitrogen uptake and plant C/N ratio.
*/
void
PhysiologyGrasslandDNDC::GrasslandDNDCPhotosynthesis( MoBiLE_Plant *  _vt, double  _day_n_growth)
{
    /* grain fraction of total crop biomass */
    double  f_grain( (*_vt)->FRACTION_FRUIT());
    /* root fraction of total crop biomass */
    double  f_root( (*_vt)->FRACTION_ROOT());
    /* straw fraction of total crop biomass */
    double  f_straw( 1.0 - f_grain - f_root);
    
    // Distinguish between growth development and dormacy
    if ( _vt->dvsFlush > 0.0)
    {
        if( (*_vt)->FRACTION_FRUIT() < 0.05)
        {
            // grassy plants
            if( _vt->dvsFlush < 0.5)
            {
                f_grain = 0.0;
                f_root  = (*_vt)->FRACTION_ROOT() / (1.0 - (*_vt)->FRACTION_FRUIT());
                f_straw = 1.0 - f_root;
            }
        }
        else
        {
            double const CTF(( (*_vt)->C4_TYPE()) ? 1.0 : 0.4);
            f_root = cbm::bound_min( (*_vt)->FRACTION_ROOT(), CTF * ( (*_vt)->FRACTION_ROOT() + ( 0.6 - (*_vt)->FRACTION_ROOT()) * ( 0.9 - _vt->dvsFlush) / 0.9));
            f_grain = ((_vt->dvsFlush < 0.5) ? 0.0 : (((_vt->dvsFlush - 0.5) / 0.5) * (*_vt)->FRACTION_FRUIT()));
            f_straw = (1.0 - f_root - f_grain);
        }
    }
    
    double nc_lst( _vt->nc_lst());
    if ( _vt->mFrt / _vt->total_biomass() > f_root)
    {
        double const scale( cbm::CCDM * _day_n_growth / (f_grain + f_straw));
        dc_bud_vt[_vt->slot] = cbm::flt_greater_zero( _vt->nc_bud()) ? scale * f_grain / _vt->nc_bud() : 0.0;
        dc_fol_vt[_vt->slot] = cbm::flt_greater_zero( _vt->nc_fol()) ? scale * f_straw * (*_vt)->FALEAF() / _vt->ncFol : 0.0;
        dc_lst_vt[_vt->slot] = cbm::flt_greater_zero( nc_lst)     ? scale * f_straw * ( 1.0 - (*_vt)->FALEAF()) / nc_lst : 0.0;
        dc_frt_vt[_vt->slot] = 0.0;
    }
    else
    {
        double const scale( cbm::CCDM * _day_n_growth);
        dc_bud_vt[_vt->slot] = scale * f_grain / _vt->nc_bud();
        dc_fol_vt[_vt->slot] = scale * f_straw * (*_vt)->FALEAF() / _vt->ncFol;
        dc_lst_vt[_vt->slot] = scale * f_straw * ( 1.0 - (*_vt)->FALEAF()) / nc_lst;
        dc_frt_vt[_vt->slot] = scale * f_root / _vt->ncFrt;
    }

    _vt->mFrt   += dc_frt_vt[_vt->slot] / cbm::CCDM;
    _vt->mBud   += dc_bud_vt[_vt->slot] / cbm::CCDM;
    _vt->mFol   += dc_fol_vt[_vt->slot] / cbm::CCDM;
    _vt->dw_lst += dc_lst_vt[_vt->slot] / cbm::CCDM;
    _vt->n_lst  += dc_lst_vt[_vt->slot] / cbm::CCDM * nc_lst;

    double const c_above_optimum( _vt->dvsFlush * (*_vt)->MFOLOPT() / (*_vt)->FALEAF() * cbm::CCDM);

    double const nitrogen_satisfaction( cbm::flt_greater_zero( c_above_optimum) ?
                                        cbm::bound(0.0, _vt->aboveground_biomass() * cbm::CCDM / c_above_optimum, 1.0) : 1.0);

    double const aboveground_nitrogen_old( _vt->aboveground_nitrogen());
    _vt->ncBud -= (_vt->ncBud - (nitrogen_satisfaction * (*_vt)->NC_FRUIT_MAX() + (1.0 - nitrogen_satisfaction) * (*_vt)->NC_FRUIT_MIN())) * 0.1 * FTS_TOT_;
    _vt->ncFol -= (_vt->ncFol - (nitrogen_satisfaction * (*_vt)->NC_STRUCTURAL_TISSUE_MAX() + (1.0 - nitrogen_satisfaction) * (*_vt)->NC_STRUCTURAL_TISSUE_MIN())) * 0.1 * FTS_TOT_;
    nc_lst     -= (nc_lst     - (nitrogen_satisfaction * (*_vt)->NC_STRUCTURAL_TISSUE_MAX() + (1.0 - nitrogen_satisfaction) * (*_vt)->NC_STRUCTURAL_TISSUE_MIN())) * 0.1 * FTS_TOT_;
    _vt->n_lst = _vt->dw_lst * nc_lst;
    
    double const aboveground_nitrogen_new( _vt->aboveground_nitrogen());

    double const scale_c( aboveground_nitrogen_old / aboveground_nitrogen_new);

    double const m_bud_old( _vt->mBud);
    double const m_fol_old( _vt->mFol);
    double const m_lst_old( _vt->dw_lst);

    _vt->mBud *= scale_c;
    _vt->mFol *= scale_c;
    _vt->dw_lst *= scale_c;
    _vt->n_lst *= scale_c;

    dc_bud_vt[_vt->slot] += (_vt->mBud - m_bud_old) * cbm::CCDM;
    dc_fol_vt[_vt->slot] += (_vt->mFol - m_fol_old) * cbm::CCDM;
    dc_lst_vt[_vt->slot] += (_vt->dw_lst - m_lst_old) * cbm::CCDM;
}



void
PhysiologyGrasslandDNDC::senescence( MoBiLE_Plant *  _vt)
{
    double const DPG_MAX( 0.5); // Maximal 50% senescence are allowed per timestep
    double const plant_n_old( _vt->total_nitrogen());

    /* Senescence due to drought stress */
    DroughtStress  droughtstress;
    droughtstress.fh2o_ref = (*_vt)->H2OREF_A();
    _vt->f_h2o = this->m_state->get_fh2o( _vt);
    double const  drought_stress = droughtstress.linear_threshold( _vt->f_h2o);
    double const  s_fact_drought(( drought_stress < 1.0) ?
        ((*_vt)->SENESCENCE_DROUGHT() * 1.0 / (lclock()->days_in_year()
            * std::max(drought_stress, 0.01)) * FTS_TOT_) : 0.0);
    
    
    /*************************************/
    /* senescence of aboveground biomass */
    /*************************************/
    
    /* Senescence due to temperature stress */
    double const s_fact_temp_shoot( ((*mc_temp) < S_TLIMIT) ? 
                                   cbm::bound(0.0, (*_vt)->SENESCENCE_FROST() * std::abs( (*mc_temp) - S_TLIMIT)/10.0 * FTS_TOT_, 1.0)
                                   : 0.0);

    double const s_fact_shoot( std::min( std::max(s_fact_drought, 
                                                  s_fact_temp_shoot), 
                                        DPG_MAX));
    
    if ( cbm::flt_greater_zero( s_fact_shoot))
    {
        double const s_fol_dm( _vt->mFol * s_fact_shoot);
        double const s_lst_dm( _vt->dw_lst * s_fact_shoot);
        double const s_bud_dm( _vt->mBud * s_fact_shoot);

        double const s_fol_n( s_fol_dm * _vt->nc_fol());
        double const s_lst_n( s_lst_dm * _vt->nc_lst());
        double const s_bud_n( s_bud_dm * _vt->nc_bud());

        s_fol_vt[_vt->slot] += (s_fol_dm + s_lst_dm);
        n_lit_fol_vt[_vt->slot] += (s_fol_n + s_lst_n);

        s_bud_vt[_vt->slot] += s_bud_dm;
        n_lit_bud_vt[_vt->slot] += s_bud_n;

        _vt->mBud -= s_bud_dm;
        _vt->mFol -= s_fol_dm;
        _vt->dw_lst -= s_lst_dm;
        _vt->n_lst -= s_lst_n;

        double c_litter_above( (s_fol_dm + s_lst_dm) * cbm::CCDM);
        double n_litter_above( s_fol_n + s_lst_n);

        if ((*_vt)->TUBER() == true)
        {
            double const c_litter_below( s_bud_dm * cbm::CCDM);
            double const n_litter_below( s_bud_n);

            for ( size_t  sl = 0;  sl < m_soillayers->soil_layer_cnt();  ++sl)
            {
                sc_.c_raw_lit_1_sl[sl] += c_litter_below * _vt->fFrt_sl[sl] * (1.0 - (*_vt)->CELLULOSE() - (*_vt)->LIGNIN());
                sc_.c_raw_lit_2_sl[sl] += c_litter_below * _vt->fFrt_sl[sl] * (*_vt)->CELLULOSE();
                sc_.c_raw_lit_3_sl[sl] += c_litter_below * _vt->fFrt_sl[sl] * (*_vt)->LIGNIN();
                sc_.accumulated_c_litter_below_sl[sl] += c_litter_below * _vt->fFrt_sl[sl];

                sc_.n_raw_lit_1_sl[sl] += n_litter_below * _vt->fFrt_sl[sl] * (1.0 - (*_vt)->CELLULOSE() - (*_vt)->LIGNIN());
                sc_.n_raw_lit_2_sl[sl] += n_litter_below * _vt->fFrt_sl[sl] * (*_vt)->CELLULOSE();
                sc_.n_raw_lit_3_sl[sl] += n_litter_below * _vt->fFrt_sl[sl] * (*_vt)->LIGNIN();
                sc_.accumulated_n_litter_below_sl[sl] += n_litter_below * _vt->fFrt_sl[sl];
            }
        }
        else
        {
            c_litter_above += s_bud_dm * cbm::CCDM;
            n_litter_above += s_bud_n;
        }

        sc_.c_raw_lit_1_above += c_litter_above * (1.0 - (*_vt)->CELLULOSE() - (*_vt)->LIGNIN());
        sc_.c_raw_lit_2_above += c_litter_above * (*_vt)->CELLULOSE();
        sc_.c_raw_lit_3_above += c_litter_above * (*_vt)->LIGNIN();
        sc_.accumulated_c_litter_above += c_litter_above;
        
        sc_.n_raw_lit_1_above += n_litter_above * (1.0 - (*_vt)->CELLULOSE() - (*_vt)->LIGNIN());
        sc_.n_raw_lit_2_above += n_litter_above * (*_vt)->CELLULOSE();
        sc_.n_raw_lit_3_above += n_litter_above * (*_vt)->LIGNIN();
        sc_.accumulated_n_litter_above += n_litter_above;
    }

    /*************************************/
    /* senescence of belowground biomass */
    /*************************************/

    double s_frt_dw( 0.0);
    for ( size_t  sl = 0;  sl < this->root_q_vt_[_vt->slot];  ++sl)
    {
        double const s_fact_temp_root( (temp_sl[sl] < S_TLIMIT) ? 
                                      cbm::bound(0.0, (*_vt)->SENESCENCE_FROST() * std::abs( temp_sl[sl] - S_TLIMIT)/10.0 * FTS_TOT_, 1.0) 
                                      : 0.0);
        
        double const s_fact_root( std::min( std::max(s_fact_drought, 
                                                     s_fact_temp_root), 
                                           DPG_MAX));
        s_frt_dw += (_vt->mFrt * _vt->fFrt_sl[sl] * s_fact_root);
    }
    
    if ( cbm::flt_greater_zero( s_frt_dw))
    {
        double const s_frt_n( s_frt_dw * _vt->ncFrt);

        for ( size_t  sl = 0;  sl < m_soillayers->soil_layer_cnt();  ++sl)
        {
            s_frt_vtsl[_vt->slot][sl] += s_frt_dw * _vt->fFrt_sl[sl];
            n_lit_frt_vtsl[_vt->slot][sl] += s_frt_n * _vt->fFrt_sl[sl];
        }

        _vt->mFrt -= s_frt_dw;
        
        for ( size_t  sl = 0;  sl < m_soillayers->soil_layer_cnt();  ++sl)
        {
            sc_.c_raw_lit_1_sl[sl] += s_frt_dw * _vt->fFrt_sl[sl] * (1.0 - (*_vt)->CELLULOSE() - (*_vt)->LIGNIN()) * cbm::CCDM;
            sc_.c_raw_lit_2_sl[sl] += s_frt_dw * _vt->fFrt_sl[sl] * (*_vt)->CELLULOSE() * cbm::CCDM;
            sc_.c_raw_lit_3_sl[sl] += s_frt_dw * _vt->fFrt_sl[sl] * (*_vt)->LIGNIN() * cbm::CCDM;
            sc_.accumulated_c_litter_below_sl[sl] += s_frt_dw * _vt->fFrt_sl[sl] * cbm::CCDM;
            
            sc_.n_raw_lit_1_sl[sl] += s_frt_n * _vt->fFrt_sl[sl] * (1.0 - (*_vt)->CELLULOSE() - (*_vt)->LIGNIN());
            sc_.n_raw_lit_2_sl[sl] += s_frt_n * _vt->fFrt_sl[sl] * (*_vt)->CELLULOSE();
            sc_.n_raw_lit_3_sl[sl] += s_frt_n * _vt->fFrt_sl[sl] * (*_vt)->LIGNIN();
            sc_.accumulated_n_litter_below_sl[sl] += s_frt_n * _vt->fFrt_sl[sl];
        }
        
        if ( exponential_root_distribution)
        {
            root_system[_vt->slot]->update_root_fraction_exponential( 1.0, this->root_q_vt_[_vt->slot], (*_vt)->EXP_ROOT_DISTRIBUTION(), _vt);
        }
        else
        {
            root_system[_vt->slot]->update_root_fraction_sigmoid( 1.0, root_q_vt_[_vt->slot], 0.005, 10.0, 120.0, 10.0, _vt);
        }
    }

    double const reduction_factor( (plant_n_old > 0.0)? _vt->total_nitrogen() / plant_n_old : 0.0);
    _vt->dvsFlush *= reduction_factor;
    _vt->dvsFlushOld *= reduction_factor;
    _vt->growing_degree_days *= reduction_factor;
}



/*
 * @brief checks carbon and nitrogen balance each time step
 *
 */
void
PhysiologyGrasslandDNDC::GrasslandDNDCBalanceCheck( int _stage)
{
    double tot_c(  ph_.accumulated_c_export_grazing + ph_.accumulated_c_export_harvest
                 + sc_.accumulated_c_litter_below_sl.sum() + sc_.accumulated_c_root_exsudates_sl.sum());
    double tot_n(  ph_.accumulated_n_export_grazing + ph_.accumulated_n_export_harvest
                 + sc_.accumulated_n_litter_below_sl.sum() - m_pf.accumulated_n_uptake());
    
    for ( GrassIterator vt = this->m_veg->groupbegin< species::grass >();
            vt != this->m_veg->groupend< species::grass >(); ++vt)
    {
        MoBiLE_Plant *p = *vt;
        
        tot_c += (p->total_biomass() * cbm::CCDM
                  + r_gro_vt[p->slot] + r_res_vt[p->slot] 
                  - std::accumulate(c_upt_vtfl[p->slot].begin(), c_upt_vtfl[p->slot].end(), 0.0) + delta_c_at_planting);
        tot_n += p->total_nitrogen() - p->a_fix_n;
    }

    if ( _stage == ON_ENTRY)
    {
        tot_c_ = tot_c;
        tot_n_ = tot_n;
    }
    else if ( _stage == ON_EXIT)
    {
        double difference( tot_c_-tot_c);
        if ( cbm::flt_greater( std::abs( difference), cbm::DIFFMAX))
        {
            KLOGWARN("C-Leak:  difference is ", difference);
        }

        difference = tot_n_-tot_n;
        if ( cbm::flt_greater( std::abs( difference), cbm::DIFFMAX))
        {
            KLOGWARN("N-Leak:  difference is ", difference);
        }
    }
    else
    { KLOGFATAL( "[BUG]  Unknown stage for Balance Check"); }
}


static int  _QueueEventHarvest( void const *  _msg, size_t  _msg_sz, void *  _queue)
{
    ldndc::EventQueue *  queue =
        static_cast< ldndc::EventQueue * >( _queue);
    if ( !queue)
        { return -1; }

    ldndc::EventAttributes  harvest_event(
                "harvest", (char const *)_msg, _msg_sz);
    queue->push( harvest_event);
    return 0;
}

static int  _QueueEventPlant( void const *  _msg, size_t  _msg_sz, void *  _queue)
{
    ldndc::EventQueue *  queue =
        static_cast< ldndc::EventQueue * >( _queue);
    if ( !queue)
        { return -1; }

    ldndc::EventAttributes  plant_event(
                "plant", (char const *)_msg, _msg_sz);
    queue->push( plant_event);
    return 0;
}


lerr_t
PhysiologyGrasslandDNDC::register_ports( cbm::io_kcomm_t *  _io_kcomm)
{
    lerr_t  rc = LDNDC_ERR_OK;
    
    rc = this->m_eventcut.register_ports( _io_kcomm);
    if ( rc)
        { return LDNDC_ERR_FAIL; }

    rc = this->m_eventgraze.register_ports( _io_kcomm);
    if ( rc)
        { return LDNDC_ERR_FAIL; }

    CBM_Callback  cb_harvest;
    cb_harvest.fn = &_QueueEventHarvest;
    cb_harvest.data = &this->m_HarvestEvents;
    this->m_HarvestHandle = _io_kcomm->subscribe_event( "harvest", cb_harvest);
    if ( !CBM_HandleOk(this->m_HarvestHandle))
        { return  LDNDC_ERR_FAIL; }

    CBM_Callback  cb_plant;
    cb_plant.fn = &_QueueEventPlant;
    cb_plant.data = &this->m_PlantEvents;
    this->m_PlantHandle = _io_kcomm->subscribe_event( "plant", cb_plant);
    if ( !CBM_HandleOk(this->m_PlantHandle))
        { return  LDNDC_ERR_FAIL; }

    return rc;
}

lerr_t
PhysiologyGrasslandDNDC::unregister_ports( cbm::io_kcomm_t *  _io_kcomm)
{
    this->m_eventcut.unregister_ports( _io_kcomm);
    this->m_eventgraze.unregister_ports( _io_kcomm);

    _io_kcomm->unsubscribe_event( this->m_HarvestHandle);
    _io_kcomm->unsubscribe_event( this->m_PlantHandle);

    return  LDNDC_ERR_OK;
}

} /*namespace ldndc*/
