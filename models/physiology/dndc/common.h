/*!
 */

#ifndef  LD_PHYSIOLOGYDNDCCOMMON_H_
#define  LD_PHYSIOLOGYDNDCCOMMON_H_

#include  "mbe_plant.h"
//#include  <input/soillayers/soillayers.h>
//#include  <containers/cbm_vector.h>

namespace ldndc {

double  optimum_aboveground_biomass( MoBiLE_Plant const *);
double  optimum_aboveground_c( MoBiLE_Plant const *);
double  total_nitrogen_demand( MoBiLE_Plant const *, double const *_optimum_aboveground_c=NULL);

} /* namespace ldndc */


#endif  /*  !LD_PHYSIOLOGYDNDCCOMMON_H_   */

