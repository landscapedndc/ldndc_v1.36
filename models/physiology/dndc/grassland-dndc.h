/*!
 * @brief
 */

#ifndef  LM_PHYSIOLOGY_GRASSLANDDNDC_H_
#define  LM_PHYSIOLOGY_GRASSLANDDNDC_H_

#include  "mbe_legacymodel.h"
#include  "state/mbe_state.h"
#include  "ld_shared.h"
#include  "ld_eventqueue.h"

#include  "eventhandler/graze/graze.h"
#include  "eventhandler/cut/cut.h"
#include  "physiology/ld_plantfunctions.h"

#include  <containers/lgrowarray.h>

namespace ldndc {

struct BaseRootSystemDNDC;
class  LDNDC_API  PhysiologyGrasslandDNDC  :  public  MBE_LegacyModel
{
    LMOD_EXPORT_MODULE_INFO(PhysiologyGrasslandDNDC,"physiology:grasslanddndc","Physiology GrasslandDNDC");
    
    /***  module specific constants  ***/
    
    /*! growth respiration fraction (Kracher (2011)) */
    static double const  GROWTH_RESPIRATION_FRACTION;
            
    /*! temperature limit for death of grass (not to be confused with temperature limit for growth) */
    static const double  S_TLIMIT;
    
public:
    PhysiologyGrasslandDNDC(
                            MoBiLE_State *,
                            cbm::io_kcomm_t *,
                            timemode_e);
    
    ~PhysiologyGrasslandDNDC();
    
    lerr_t  configure( ldndc::config_file_t const *);
    lerr_t  register_ports( cbm::io_kcomm_t *);
    lerr_t  initialize();

    lerr_t  solve();
    lerr_t  unregister_ports( cbm::io_kcomm_t *);
    lerr_t  finalize() { return  LDNDC_ERR_OK; }
    
    lerr_t  sleep() { return  LDNDC_ERR_OK; }
    lerr_t  wake() { return  LDNDC_ERR_OK; }
    
private:
    MoBiLE_State *  m_state;
    cbm::io_kcomm_t *  io_kcomm;
    
    /* required input classes */
    input_class_setup_t const *  m_setup;
    input_class_soillayers_t const *  m_soillayers;
    input_class_species_t const *  m_species;

    /* required state components */
    substate_airchemistry_t const &  ac_;
    substate_microclimate_t &  mc_;
    substate_physiology_t &  ph_;
    substate_soilchemistry_t &  sc_;
    substate_watercycle_t &  wc_;

    MoBiLE_PlantVegetation *  m_veg;

    EventQueue  m_HarvestEvents;
    CBM_Handle  m_HarvestHandle;
    EventQueue  m_PlantEvents;
    CBM_Handle  m_PlantHandle;

    ldndc::growable_array < BaseRootSystemDNDC*, 1000, 1 >  root_system;
    
    double const *mc_temp;
    
    double const FTS_TOT_;    

    /*!
     * @brief
     *     All kind of plant related functions.
     */
    LD_PlantFunctions  m_pf;

    double tot_c_;
    double tot_n_;

    EventHandlerGraze  m_eventgraze;
    EventHandlerCut  m_eventcut;
    
    lvector_t< double >  n_demand_vt_;
    lvector_t< double >  n_uptake_vt_;
    lvector_t< double >  n_avail_tot_vt_;
        
    /*!
     * @brief
     *    largest soil layer index where roots exist [-]
     *
     * @note
     *    updated by means of class internal method
     *    @fn update_deepest_rooted_soil_layer_index_
     */
    lvector_t< unsigned int >  root_q_vt_;

    lvector_t< double >  n2_fixation_vt;

    lvector_t< double >  dc_fol_vt;
    lvector_t< double >  dc_lst_vt;
    lvector_t< double >  dc_bud_vt;
    lvector_t< double >  dc_frt_vt;

    lvector_t< double >  r_res_vt;
    lvector_t< double >  r_gro_vt;
    lvector_t< double >  r_gro_below_vt;
    lvector_t< double >  r_fol_vt;
    lvector_t< double >  r_lst_vt;
    lvector_t< double >  r_bud_vt;
    lvector_t< double >  r_frt_vt;

    lvector_t< double >  s_fol_vt;
    lvector_t< double >  s_bud_vt;

    lvector_t< double >  n_lit_fol_vt;
    lvector_t< double >  n_lit_bud_vt;

    lvector_t< double >  exsuLoss_vt;

    std::vector< std::vector< double > > s_frt_vtsl;
    std::vector< std::vector< double > > n_lit_frt_vtsl;

    std::vector< std::vector< double > > c_upt_vtfl;

    lvector_t< double > temp_sl;
    lvector_t< double > no3_sl;
    lvector_t< double > nh4_sl;
    lvector_t< double > don_sl;

    lvector_t< double > nh4_uptake_sl;
    lvector_t< double > don_uptake_sl;
    lvector_t< double > no3_uptake_sl;

    bool exponential_root_distribution;

    /*!
     * @brief
     *    used for closed C balance at planting
     */
    double delta_c_at_planting;

    /***  methods  ***/

    /*!
     * @brief
     *    perform pre/post run initialization each time step
     */
    lerr_t  step_init();
    lerr_t  step_out();

    /*!
     * @brief
     *    perform cutting and garzing
     */
    lerr_t GrasslandDNDCCuttingGrazing();

    /*!
     * @brief
     *    update state items specific to species
     *    given by index. this sets e.g. some aggregated
     *    values, etc...
     */
    void  update_species_related_state_items( MoBiLE_Plant *);
    
    /*!
     * @brief
     *    handle plant event 
     *
     * @param
     *    species
     * @param
     *    index of crop in species properties related arrays (<A>_vt)
     *
     * @return
     *    LDNDC_ERR_OK if no plant event is pending
     *    or everything went well.
     *
     *    ...
     */
    lerr_t  event_plant( MoBiLE_Plant *, EventAttributes const &);
    
    /*!
     * @brief
     *    handle harvest event 
     *
     * @param
     *    species
     * @param
     *    index of crop in species properties related arrays (<A>_vt)
     *
     * @return
     *    LDNDC_ERR_OK if no harvest event is pending
     *    or everything went well.
     *
     *    ...
     */
    lerr_t  event_harvest( MoBiLE_Plant *, EventAttributes const &);
    
    /*!
     * @brief
     *    development_stage advances the crop growth for one day
     *
     */
    lerr_t  GrasslandDNDCPlantDevelopment();
    
    
    void  growth_respiration( MoBiLE_Plant *);
    
    /*!
     * @brief
     *    Root Respiration
     *    @n
     *    The root growth is depending on a growth parameter with a
     *    default value of 1 cm per day and the maximum root depth is 1 m.
     *
     *
     * @param
     *    species
     * @param
     *    index of crop in species properties related arrays (<A>_vt)
     */
    void  maintenance_respiration( MoBiLE_Plant *);
    
    /*!
     * @brief
     *    calculates the daily N demand of each crop.
     *
     *
     * @param
     *    species
     * @param
     *    index of crop in species properties related arrays (<A>_vt)
     *
     * @return
     *    nitrogen demand for species [kg N/ha/day]
     */
    double  nitrogen_demand( MoBiLE_Plant *);
    
    /*!
     * @brief
     *    nitrogen uptage by the crop
     */
    void  nitrogen_uptake();
    
    /*!
     * @brief
     *    nitrogen fixation by legumes
     *
     * @param
     *    species
     * @param
     *    nitrogen demand [kg N/ha/day]
     * @param
     *    nitrogen uptake [kg N/ha/day]
     *
     * @return
     *    day growth nitrogen uptake [kg N/ha/day]
     */
    double  nitrogen_fixation( MoBiLE_Plant *, double, double);
    double  total_fixed_nitrogen( MoBiLE_Plant *, double) const;
    

    /*!
     * @brief
     *
     */
    void
    GrasslandDNDCPhotosynthesis( MoBiLE_Plant *, double);



    /*!
     * @brief
     *    Crop senescence accounts for the plant aging due to frost.
     *
     *
     * @param
     *    species
     * @param
     *    index of crop in species properties related arrays (<A>_vt)
     */
    void  senescence( MoBiLE_Plant *);
    
    /* coverage */    
    void  update_canopy_coverage( MoBiLE_Plant *);
    
    /* plant height */    
    void  update_canopy_height( MoBiLE_Plant *);
    
    /* canopy structure */
    void  update_canopy_items( MoBiLE_Plant *);
    

    void  GrasslandDNDCBalanceCheck( int /*stage*/);
};
} /*namespace ldndc*/

#endif  /*  !LM_PHYSIOLOGY_GRASSLANDDNDC_H_  */

