/*!
 * @file
 * @author
 *    David Kraus
 * @date
 *  January, 2019
 */


#include  "physiology/ld_vernalization.h"
#include  <logging/cbm_logging.h>


/*!
 * @page veglibs
 * @section veglibs_vernalization Vernalization
 * Vernalization after @cite haenninen:1990a.
 */
double
ldndc::get_chilling_factor(
                           double _gdd,
                           double _gdd_flowering,
                           double _chilling_units,
                           double _required_chilling_units)
{
    /*!
     * @page veglibs
     * Influence of vernalization starts as soon as accumulated growing degree days \f$ AGDD \f$
     * are greater than half of the required growing degree days for the state of  flowering \f$ GDD\_FLOWERING \f$:
     * \f[
     * AGDD > 0.5 \cdot GDD\_FLOWERING
     * \f]
     */
    double gdd_vernalization( 0.5 * _gdd_flowering);
    if ( cbm::flt_greater( _gdd, gdd_vernalization))
    {
        /*!
         * @page veglibs
         * Chill factor \f$ f_{chill} \f$ is given by:
         * \f[
         *  f_{chill} = 1 - \frac{AGDD - 0.5 \cdot GDD\_FLOWERING}{0.5 \cdot GDD\_FLOWERING}
         * + \frac{ACU}{CHILL\_UNITS}
         * \f]
         * wherein \f$ ACU \f$ and \f$ CHILL\_UNITS \f$ refer to accumulated chill units and required chill units for
         * complete vernalization, respectively. The factor \f$ f_{chill} \f$ is bound between 0 and 1.
         */
        double const chill_required( cbm::bound_max(
                                                    (_gdd - gdd_vernalization) / gdd_vernalization,
                                                    1.0));
        return cbm::bound(
                          0.0,
                          1.0 - chill_required + _chilling_units / _required_chilling_units,
                          1.0);
    }

    return 1.0;
}



/*!
 * @page veglibs
 * Accumulated chilling units \f$ ACU \f$ are given by:
 * \f{eqnarray*}{
 *  ACU &=& \sum CU \\
 *  CU &=& \frac{CHILL\_TEMP\_MAX - T_{avg}}{CHILL\_TEMP\_MAX}
 * \f}
 * \f$ CHILL\_TEMP\_MAX \f$ and \f$ T_{avg} \f$ refer to a species-specific parameter
 * and daily mean temperature, respectively.
 */
double
ldndc::get_chilling_units(
                          double _air_temperature,
                          double _maximum_chilling_temperature)
{
    double const temp_avg( cbm::bound_min( 0.0, _air_temperature));

    if ( cbm::flt_greater( _maximum_chilling_temperature, temp_avg))
    {

        return cbm::bound_max( (_maximum_chilling_temperature - temp_avg) / _maximum_chilling_temperature, 1.0) ;
    }

    return 0.0;
}
