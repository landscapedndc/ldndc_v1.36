/*!
 * @brief
 *  Plant related functions, which are globally used from various
 *  vegetation models.
 *
 * @authors
 *    - Ruediger Grote
 *    - Steffen Klatt
 *    - Edwin Haas,
 *    - David Kraus
 *
 * @date
 *      Feb 09, 2012
 */

#include  <cbm_rtcfg.h>

#include  <kernel/io-kcomm.h>
#include  <input/climate/climate.h>
#include  <input/setup/setup.h>
#include  <input/soillayers/soillayers.h>
#include  <input/species/species.h>

#include  "physiology/ld_plantfunctions.h"

#include  "physiology/ld_biomassdistribution.h"
#include  "physiology/ld_sapwoodbiomass.h"
#include  "physiology/ld_rootsystem.h"

#include  "soilchemistry/ld_wood_initialization.h"

#include  <scientific/meteo/ld_meteo.h>

using namespace ldndc;
using namespace ldndc::species;

ldndc::LD_PlantFunctions::LD_PlantFunctions(
        ldndc::MoBiLE_State *  _state,
        cbm::io_kcomm_t *  _io_kcomm) :

        cl_( _io_kcomm->get_input_class_ref< climate::input_class_climate_t >()),
        se_( _io_kcomm->get_input_class_ref< setup::input_class_setup_t >()),
        sl_( _io_kcomm->get_input_class_ref< soillayers::input_class_soillayers_t >()),

        mc_( _state->get_substate_ref< ldndc::substate_microclimate_t >()),
        ph_( _state->get_substate_ref< ldndc::substate_physiology_t >()),
        sc_( _state->get_substate_ref< ldndc::substate_soilchemistry_t >()),

        m_alm( _state, _io_kcomm),
        m_veg( &_state->vegetation)
{ }


ldndc::LD_PlantFunctions::~LD_PlantFunctions()
{ }



lerr_t
ldndc::LD_PlantFunctions::initialize_crop(
                                          MoBiLE_Plant *  _plant,
                                          species::crop_properties_t const *  _properties)
{
    if ( !_plant)
        { return LDNDC_ERR_ATTRIBUTE_NOT_FOUND; }

    crop_properties_t const *  crop_properties = _properties;
    if ( !crop_properties)
        { crop_properties = &crop_properties_default; }

    /* set species properties */
    lerr_t  rc = this->set_crop_defaults_( _plant, crop_properties);
    RETURN_IF_NOT_OK(rc);

    /* number of foliage layers after species height is set */
    rc = ph_.update_canopy_layers_height( m_veg);
    RETURN_IF_NOT_OK(rc);

    /* reset species related physiological state items */
    rc = this->reset_crop_properties_( _plant);
    RETURN_IF_NOT_OK(rc);

    return  LDNDC_ERR_OK;
}


/*!
 * @brief
 *
 */
lerr_t
ldndc::LD_PlantFunctions::set_crop_defaults_(
                                             MoBiLE_Plant * vt,
                                             species::crop_properties_t const *  _cp)
{
    double const  f_cover = _cp->fractional_cover;
    vt->f_area = cbm::is_valid( f_cover) ? f_cover : ( 1.0 / this->m_veg->size());

    if ( cbm::flt_equal_zero( vt->f_area))
    {
        LOGWARN( "LD_PlantFunctions::set_crop_defaults(): ",
                "please acknowledge that your species ground coverage is zero",
                "  [species=", vt->name(),"]");
    }

    vt->cdr = 1.0;
    vt->dbh = 0.0;
    vt->dbas = 0.0;
    vt->qsfa = 0.0;

    vt->f_ncc = 1.0;
    vt->rooting_depth = 0.0;
    vt->tree_number = 0.0;

    vt->height_max = 0.0;
    vt->height_at_canopy_start = 0.0;

    for ( size_t fl = 0; fl < se_.canopylayers(); ++fl)
    {
        vt->fFol_fl[fl] = 0.0;
    }

    size_t  n( vt->nb_foliagelayers());
    for ( size_t  fl = 0;  fl < n;  ++fl)
    {
        vt->fFol_fl[fl] = 1.0/(double)n;
    }

    return  LDNDC_ERR_OK;
}



/*!
 * @brief
 *
 */
lerr_t
ldndc::LD_PlantFunctions::reset_crop_properties_( MoBiLE_Plant *  vt)
{
    vt->f_fac = 0.0;
    vt->mBud = 0.0;
    vt->mFol = 0.0;
    vt->dw_dfol = 0.0;
    vt->mSap = 0.0;
    vt->dw_lst = 0.0;
    vt->mCor = 0.0;
    vt->dw_dst = 0.0;
    vt->mFrt = 0.0;

    vt->n_lst = 0.0;
    vt->n_dst = 0.0;
    vt->n_dfol = 0.0;
    
    vt->d_rBud = 0.0;
    vt->d_rFol = 0.0;
    vt->d_rSap = 0.0;
    vt->d_rSapBelow = 0.0;
    vt->d_rFrt = 0.0;
    vt->d_rRes = 0.0;
    vt->d_rGro = 0.0;
    vt->d_rGroBelow = 0.0;
    vt->d_sBud = 0.0;
    vt->d_sFol = 0.0;

    vt->d_dcBud = 0.0;
    vt->d_dcFol = 0.0;
    vt->d_dcSap = 0.0;
    vt->d_dcFrt = 0.0;

    vt->dEmerg = -1;

    vt->growing_degree_days = 0;
    vt->dvsFlush = 0;

    return  LDNDC_ERR_OK;
}


lerr_t
ldndc::LD_PlantFunctions::initialize_grass(
                                           MoBiLE_Plant *  _plant,
                                           species::grass_properties_t const *  _properties)
{
    if ( !_plant)
        { return LDNDC_ERR_ATTRIBUTE_NOT_FOUND; }

    grass_properties_t const *  grass_properties = _properties;
    if ( !grass_properties)
        { grass_properties = &grass_properties_default; }

    /* reset species related physiological state items */
    lerr_t  rc = this->reset_grass_properties_( _plant);
    RETURN_IF_NOT_OK(rc);

    /* set species properties */
    rc = this->set_grass_defaults_( _plant, grass_properties);
    RETURN_IF_NOT_OK(rc);

    rc = this->gapfill_grass_defaults_( _plant, grass_properties);
    RETURN_IF_NOT_OK(rc);

    /* species height is set before and number of foliage
     * layers used after call to this function
     */
    rc = ph_.update_canopy_layers_height( m_veg);
    RETURN_IF_NOT_OK(rc);

    /* set more species properties */
    rc = this->initialize_vegetationstructure_grass_( _plant);
    RETURN_IF_NOT_OK(rc);

    /* other vegetation structural and physiological vegetation variables */
    rc = this->initialize_new_biomass( _plant, grass_properties->initial_biomass);
    RETURN_IF_NOT_OK(rc);

    /* canopy microclimate */
    rc = this->canopy_microclimate();
    RETURN_IF_NOT_OK(rc);

    return  LDNDC_ERR_OK;
}


/*!
 * @brief
 *
 */
lerr_t
ldndc::LD_PlantFunctions::set_grass_defaults_(
                                              MoBiLE_Plant * vt,
                                              species::grass_properties_t const *  _gp)
{
    double const  f_cover = _gp->fractional_cover;
    vt->f_area = cbm::is_valid( f_cover) ? f_cover : ( 1.0 / this->m_veg->size());

    if ( vt->f_area <= 0.0)
    {
        LOGWARN( "LD_PlantFunctions::set_grass_defaults_(): ",
                "please acknowledge that your species ground coverage is invalid",
                " [species=",vt->name(),",value=",vt->f_area,"]");
    }

    vt->cdr = 1.0;
    vt->dbh = 0.0;
    vt->dbas = 0.0;
    vt->height_max = cbm::is_valid( _gp->height_max) ? _gp->height_max : 0.5;
    vt->height_at_canopy_start = 0.0;
    vt->f_ncc = cbm::is_valid( _gp->fractional_cover) ? _gp->fractional_cover : 1.0;
    vt->rooting_depth = cbm::is_valid( _gp->root_depth) ? _gp->root_depth : 0.1;
    vt->tree_number = 0.0;

    if ( !cbm::flt_greater_zero( vt->height_max))
    {
        LOGWARN( "LD_PlantFunctions::set_grass_defaults(): ",
                "maximum height of species is invalid  [name=",vt->name(),",value=",vt->height_max,"]");
    }

    if ( !cbm::flt_greater_zero( vt->rooting_depth))
    {
        LOGWARN( "LD_PlantFunctions::set_grass_defaults(): ",
                "rooting depth of species is invalid  [name=",vt->name(),",value=",vt->rooting_depth,"]");
    }

    return  LDNDC_ERR_OK;
}


/*!
 * @brief
 *
 */
lerr_t
ldndc::LD_PlantFunctions::gapfill_grass_defaults_(
                                                  MoBiLE_Plant * vt,
                                                  species::species_properties_t const *  /* _properties */)
{
    /* rooting depth */
    double  cumulated_height( sc_.h_sl.sum());
    vt->rooting_depth = cbm::bound_max( (*vt)->QHRD() * vt->height_max, cumulated_height);

    /* others */
    vt->dbas = 0.0;
    vt->qsfa = 0.0;
    vt->cdr = 1.0;
    vt->tree_number = 0.0;

    return  LDNDC_ERR_OK;
}


/*!
 * @brief
 *
 */
lerr_t
ldndc::LD_PlantFunctions::reset_grass_properties_( MoBiLE_Plant *  vt)
{
    return  reset_crop_properties_( vt);
}


lerr_t
ldndc::LD_PlantFunctions::initialize_tree(
                                          MoBiLE_Plant *  _plant,
                                          species::wood_properties_t const *  _properties,
                                          ecosystem_type_e _ecosystemtype,
                                          cbm::string_t _branchfraction,
                                          cbm::string_t _crownlength,
                                          bool _competition)
{
    if ( !_plant)
    {
        return LDNDC_ERR_ATTRIBUTE_NOT_FOUND;
    }

    wood_properties_t const *  wood_properties = _properties;
    if ( !wood_properties)
    {
        wood_properties = &wood_properties_default;
    }

    /* set species properties */
    lerr_t  rc = this->set_wood_defaults_( _plant, wood_properties);
    RETURN_IF_NOT_OK(rc);

    rc = this->gapfill_wood_defaults_( _plant, wood_properties, _competition);
    RETURN_IF_NOT_OK(rc);


    for (size_t i = 0; i < 10; ++i)
    {
        rc = this->initialize_vegetationstructure_wood_( _plant, _branchfraction, _crownlength);
        if ( rc)
        { return  LDNDC_ERR_FAIL; }
    }
    for (size_t i = 0; i < 10; ++i)
    {
        rc = this->initialize_new_biomass( _plant, wood_properties->initial_biomass);
        if ( rc)
        { return  LDNDC_ERR_FAIL; }
    }

    /* canopy microclimate */
    rc = this->canopy_microclimate();
    RETURN_IF_NOT_OK(rc);

    wood_initialization(sl_, *m_veg, sc_, _ecosystemtype);

    return  LDNDC_ERR_OK;

}


/*!
 * @brief
 *  Estimating missing vegetation type parameters
 *  relations that are set preliminary equal for
 *  all species and should be replaced with parameters
 *  for equations.
 */
lerr_t
ldndc::LD_PlantFunctions::set_wood_defaults_(
                                             MoBiLE_Plant * _vt,
                                             species::wood_properties_t const * _wp)
{
    ldndc_assert( (*_vt)->IS_WOOD() && _wp);

    cbm::invalidate( _vt->cdr);
    cbm::invalidate( _vt->dbas);

    _vt->dbh = _wp->dbh;
    _vt->height_max = _wp->height_max;
    _vt->height_at_canopy_start = _wp->height_min;
    _vt->f_ncc = _wp->reduc_fac_c;
    _vt->rooting_depth = _wp->root_depth;
    _vt->tree_number = _wp->number;
    _vt->initial_biomass = _wp->initial_biomass;

    return  LDNDC_ERR_OK;
}


/*!
 * @details
 *
 */
lerr_t
ldndc::LD_PlantFunctions::gapfill_wood_defaults_(
                                             MoBiLE_Plant * vt,
                                             species::wood_properties_t const *  _wp,
                                             bool _competition)
{
    /* preliminary competition factor */
	update_vertical_biomass_distribution( vt);
    double hd_competition_factor(1.0);
    if ( cbm::flt_greater_zero( vt->dbh ))
    {
        double const competition_factor( !_competition ? 1.0 : m_alm.height_competition_factor( vt, m_veg));
        hd_competition_factor = competition_factor;
	}

    /* stand volume [m3 ha-1] */
    double stand_volume( _wp->volume);
    if ( !cbm::flt_greater_zero( stand_volume))
    {
        /* get volume from biomass [kg ha-1] */
        if ( cbm::flt_greater_zero( vt->initial_biomass))
        {
            stand_volume = vt->initial_biomass / ((*vt)->DSAP() * cbm::DM3_IN_M3);
        }
        /* get volume from structure */
        else if ( cbm::flt_greater_zero( vt->tree_number))
        {
            if ( (!cbm::flt_greater_zero( vt->dbh)) &&
                 (!cbm::flt_greater_zero( vt->height_max)))
            {
                LOGERROR( "Insufficient stand information provided! \n",
                          "Missing tree diameter or height");
                return  LDNDC_ERR_FAIL;
            }
            else
            {
                //only true for dbh > 0.0
                if ( !cbm::flt_greater_zero( vt->height_max))
                {
                    vt->height_max = m_alm.height_from_diameter_at_breast_height(
                                                                         vt->dbh,
                                                                         (*vt)->HD_MIN(),
                                                                         (*vt)->HD_MAX(),
                                                                         (*vt)->HD_EXP(),
                                                                         hd_competition_factor);
                }

                //only true for height_max > 0.0
                if ( !cbm::flt_greater_zero( vt->dbh))
                {
                    vt->dbh = m_alm.diameter_at_breast_height_from_height(
                                                                  vt->height_max,
                                                                  (*vt)->HD_MIN(),
                                                                  (*vt)->HD_MAX(),
                                                                  (*vt)->HD_EXP(),
                                                                  hd_competition_factor);
                }
            }
            stand_volume = m_alm.taper_volume(
                                      (*vt)->CONIFEROUS(),
                                      (*vt)->TAP_P1(), (*vt)->TAP_P2(), (*vt)->TAP_P3(),
                                      vt->dbh, vt->height_max) * vt->tree_number;
        }
        else
        {
            LOGERROR( "Insufficient stand information provided! \n",
                      "Either missing: \n",
                      "a) stand volume \n",
                      "b) initial biomass \n",
                      "c) tree number");
            return  LDNDC_ERR_FAIL;
        }
    }

    /* stand biomass [kg ha-1] */
    vt->initial_biomass = stand_volume * (*vt)->DSAP() * cbm::DM3_IN_M3;

    /* dbh [m] and height_max [m] */
    if ( (!cbm::flt_greater_zero( vt->dbh)) &&
         (!cbm::flt_greater_zero( vt->height_max)))
    {
        if ( cbm::flt_greater_zero( vt->tree_number))
        {
            vt->height_max = m_alm.height_from_biomass(
                                               (*vt)->CONIFEROUS(),
                                               (*vt)->TAP_P1(),
                                               (*vt)->TAP_P2(),
                                               (*vt)->TAP_P3(),
                                               (*vt)->HD_MIN(),
                                               (*vt)->HD_MAX(),
                                               (*vt)->HD_EXP(),
                                               stand_volume / vt->tree_number,
                                               hd_competition_factor);
            vt->dbh = m_alm.diameter_at_breast_height_from_height(
                                                          vt->height_max,
                                                          (*vt)->HD_MIN(),
                                                          (*vt)->HD_MAX(),
                                                          (*vt)->HD_EXP(),
                                                          hd_competition_factor);
        }
        else
        {
            LOGERROR( "Insufficient stand information provided!");
            return  LDNDC_ERR_FAIL;
        }
    }
    else
    {
        //only true for dbh > 0.0
        if ( !cbm::flt_greater_zero( vt->height_max))
        {
            vt->height_max = m_alm.height_from_diameter_at_breast_height(
                                                                 vt->dbh,
                                                                 (*vt)->HD_MIN(),
                                                                 (*vt)->HD_MAX(),
                                                                 (*vt)->HD_EXP(),
                                                                 hd_competition_factor);
        }

        //only true for height_max > 0.0
        if ( !cbm::flt_greater_zero( vt->dbh))
        {
            vt->dbh = m_alm.diameter_at_breast_height_from_height(
                                                          vt->height_max,
                                                          (*vt)->HD_MIN(),
                                                          (*vt)->HD_MAX(),
                                                          (*vt)->HD_EXP(),
                                                          hd_competition_factor);
        }
    }

    vt->dbas = m_alm.diameter_at_ground(
                                        vt->dbh,
                                        vt->height_max,
                                        (*vt)->HD_MIN(),
                                        (*vt)->HD_MAX(),
                                        (*vt)->HD_EXP(),
                                        hd_competition_factor);

    /* tree number */
    if ( !cbm::flt_greater_zero( vt->tree_number))
    {
        /* single tree volume */
        double const stem_volume( m_alm.stem_volume( vt->height_max,
                                                     vt->dbh,
                                                     vt->dbas,
                                                    (*vt)->CONIFEROUS(),
                                                    (*vt)->TAP_P1(),
                                                    (*vt)->TAP_P2(),
                                                    (*vt)->TAP_P3()));
        if ( cbm::flt_greater_zero( stem_volume))
        {
            vt->tree_number = stand_volume / stem_volume;
        }
        else
        {
            LOGERROR( "Initialized stem volume not valid!");
            return  LDNDC_ERR_FAIL;
        }
    }

    return  LDNDC_ERR_OK;
}



/*!
 * @brief
 *      calculates average (effective) temperature
 *      of all leaves of a vegetation type
 *
 */
double
ldndc::LD_PlantFunctions::leaf_temperature_(
                                            lvector_t< double > _temp_fl,
                                            MoBiLE_Plant * _vt)
{
    size_t  fl_cnt( _vt->nb_foliagelayers());

    double lai( 0.0);
    for ( size_t  fl = 0;  fl < fl_cnt;  ++fl)
    {
        lai += _vt->lai_fl[fl];
    }

    double leaftemp( 0.0);
    if ( lai > 0.0)
    {
        for ( size_t  fl = 0;  fl < fl_cnt;  ++fl)
        {
            leaftemp += _temp_fl[fl] * _vt->lai_fl[fl];
        }
        leaftemp *= (1.0 / lai);
    }
    else
    {
        for ( size_t  fl = 0;  fl < fl_cnt;  ++fl)
        {
            leaftemp += _temp_fl[fl];
        }
        leaftemp *= (1.0 / (double)fl_cnt);
    }

    return leaftemp;
}


/*!
 * @brief
 *      returns optimum foliage biomass depending on
 *      - tree height (defines canopy length to check if minimum has been reached)
 *      - MFOLOPT (species parameter for optimum foliage biomass)
 *      - HREF (species parameter for reference tree height of full canopy development)
 */
double
ldndc::LD_PlantFunctions::get_optimum_foliage_biomass_trees( MoBiLE_Plant * vt)
{
    ldndc_assert( (*vt)->IS_WOOD());

    double const relcover(cbm::bound_max(1.0, (vt->dbh * vt->cdr) * (vt->dbh * vt->cdr) * cbm::PI * 0.25 * vt->tree_number / cbm::M2_IN_HA ) );
    double const hmax( std::max( cbm::H_FLMIN * MIN_FOLIAGE_LAYER, vt->height_max));
    double const rel_crown_length( cbm::bound_max( 1.0,
                                                   (hmax - vt->height_at_canopy_start) / (*vt)->HREF()));
    return (*vt)->MFOLOPT() * relcover * sqrt( rel_crown_length);
}
// */


/*!
 * @brief
 *      returns optimum foliage biomass depending on
 *      - sapwood area
 *      - ratio between sapwood and foliage area (qsfa)
 */
/*!
double
ldndc::LD_PlantFunctions::get_optimum_foliage_biomass_trees(MoBiLE_Plant* vt)
{
    ldndc_assert((*vt)->IS_WOOD());

    double dcrown( vt->dbh - (vt->height_at_canopy_start - 1.3) * vt->dbh / vt->height_max );
    double sap_area( cbm::sqr( dcrown ) * cbm::PI * 0.25 );

    double sla_avg(0.0);
    for ( size_t fl = 0; fl < se_.canopylayers(); ++fl)
    {
        sla_avg = vt->sla_fl[fl] * vt->fFol_fl[fl];
    }

    return ( sap_area / vt->qsfa ) / sla_avg;
}
*/


/*!
 * @brief
 *      returns optimum foliage biomass depending on
 *      - MFOLOPT (species parameter for optimum foliage biomass)
 *      - area fraction
 */
double
ldndc::LD_PlantFunctions::get_optimum_foliage_biomass_grass( MoBiLE_Plant * _vt)
{
    ldndc_assert( !(*_vt)->IS_WOOD());
    return (*_vt)->MFOLOPT() * _vt->f_area;
}



/*!
 * @brief
 *
 */
lerr_t
ldndc::LD_PlantFunctions::update_vertical_biomass_distribution( MoBiLE_Plant * vt)
{
    double const hmax( std::max(cbm::H_FLMIN * MIN_FOLIAGE_LAYER, vt->height_max));
    if ( cbm::flt_greater_zero( hmax))
    {
        size_t fl_cnt( vt->nb_foliagelayers());
        if (hmax <= ph_.h_fl[0])
        {
            fl_cnt = 1;
        }
        ldndc_assert( fl_cnt != 0);

        double const length( vt->height_max - vt->height_at_canopy_start);
        double const ps( crown_shape_parameter( length, (*vt)->HREF(), (*vt)->PFL()));

        // - vertical foliage distribution
        canopy_biomass_distribution(
                                se_.canopylayers(), fl_cnt, ps,
                                vt->height_at_canopy_start, vt->height_max,
                                &ph_.h_fl[0], vt->fFol_fl);
    }
    else
    {
        vt->height_max = 0.0;
        for ( size_t fl = 0; fl < se_.canopylayers(); ++fl)
        {
            vt->fFol_fl[fl] = 0.0;
        }
    }

    return  LDNDC_ERR_OK;
}



/*!
 * @brief
 */
lerr_t
ldndc::LD_PlantFunctions::initialize_vegetationstructure_wood_( MoBiLE_Plant * vt,
                                                                cbm::string_t _branchfraction,
                                                                cbm::string_t _crownlength)
{
    /* species height is set before call to this function */
    lerr_t rc = ph_.update_canopy_layers_height( m_veg);
    RETURN_IF_NOT_OK(rc);

    /* fractional cover
     * reduces number of individuals from standard assumption
     * if not explicitly given (only possible for woods)
     */
    vt->f_ncc = ( vt->f_ncc > 0.0) ? vt->f_ncc : 1.0;

    /* crown diameter ratio */
    vt->height_at_canopy_start = ( vt->height_at_canopy_start > 0.0) ? vt->height_at_canopy_start : 0.0;
	vt->cdr = m_alm.crown_diameter_ratio( vt);

    /* crown length */
    if ( _crownlength == "parameter")
    {
        double const crown_length( m_alm.crown_length_from_parameter( vt->height_max,
                                                                      vt->dbh,
                                                                      (*vt)->HREF(),
                                                                      (*vt)->DIAMMAX(),
                                                                      (*vt)->CB()));
        vt->height_at_canopy_start = cbm::bound_min( vt->height_at_canopy_start,
                                                     vt->height_max - crown_length);
    }
    else
    {
        double const crown_length( m_alm.crown_length_from_crown_diameter( vt->height_max,
                                                                           vt->cdr * vt->dbh,
                                                                           (*vt)->CL_P1(),
                                                                           (*vt)->CL_P2()));
        vt->height_at_canopy_start = cbm::bound_min( vt->height_at_canopy_start,
                                                     vt->height_max - crown_length);
    }

    update_vertical_biomass_distribution( vt);

    /* rooting depth and fine root distribution */
    vt->rooting_depth = cbm::bound_max(
                                  (*vt)->QHRD() * vt->height_max,
                                  sc_.depth_sl[sl_.soil_layer_cnt()-1]);

    fineroots_biomass_distribution(
                           sl_.soil_layers_in_litter_cnt(),
                           sl_.soil_layer_cnt(),
                           (*vt)->PSL(), vt->rooting_depth,
                           vt->fFrt_sl, &sc_.h_sl[0]);

    /* sap-wood area - foliage area ratio (m2 m-2), Huber Value */
    vt->qsfa = sapwood_foliage_area_ratio( vt->height_max, (*vt)->QSF_P1(), (*vt)->QSF_P2());

    /* branch fraction */
    if ( _branchfraction == "diameter")
    {
        vt->f_branch = m_alm.branch_fraction_from_diameter(
                                                   vt->groupId(),
                                                   (*vt)->FBRAF_M(),
                                                   (*vt)->FBRAF_Y(),
                                                   (*vt)->DIAMMAX(),
                                                   vt->dbh,
                                                   vt->dbas);
    }
    else
    {
        vt->f_branch = m_alm.branch_fraction_from_canopy_volume( vt);
    }

    vt->f_area = m_alm.area_fraction_wood( vt->dbh, vt->tree_number, vt->cdr);

    return  LDNDC_ERR_OK;
}



/*!
 * @brief
 */
lerr_t
ldndc::LD_PlantFunctions::initialize_vegetationstructure_grass_( MoBiLE_Plant * vt)
{
    update_vertical_biomass_distribution( vt);

    /* rooting depth (ensure minimum height of 5 cm for proper rooting depth derivation) */
    vt->rooting_depth = cbm::bound_max(
                                   (*vt)->QHRD() * std::max( vt->height_max, 0.05),
                                   sc_.depth_sl[sl_.soil_layer_cnt()-1]);
    fineroots_biomass_distribution(
                               sl_.soil_layers_in_litter_cnt(),
                               sl_.soil_layer_cnt(),
                               (*vt)->PSL(), vt->rooting_depth,
                               vt->fFrt_sl, &sc_.h_sl[0]);

    return  LDNDC_ERR_OK;
}



double
ldndc::LD_PlantFunctions::update_dvs_flush(
                                          MoBiLE_Plant *  vt,
                                          int _days_since_emergence)
{
    static double const log_2( log(2.0));

    // current state
    if ( vt->dEmerg == -1)
    {
        return 0.0;
    }
    else if ( cbm::flt_less( vt->dvsFlush, 1.0))
    {
        return exp(-1.0 * cbm::sqr( 1 + (*vt)->NDFLUSH() - _days_since_emergence) / ( cbm::sqr( 0.5 * (*vt)->NDFLUSH()) * log_2));
    }
    else
    {
        return 1.0;
    }
}



/*!
 * @brief 
 * 
 * updates species specific mass of foliage and lai in all foliage layers
 *
 */
lerr_t
ldndc::LD_PlantFunctions::update_foliage_layer_biomass_and_lai(
                                                               MoBiLE_Plant * vt,
                                                               size_t _fl_cnt,
                                                               double _epsilon)
{
    for ( size_t fl = 0; fl < se_.canopylayers(); ++fl)
    {
        vt->lai_fl[fl]  = 0.0;
    }

    for ( size_t  fl = 0;  fl < _fl_cnt;  ++fl)
    {
        if (vt->mFol > 0.0)
        {
            vt->lai_fl[fl]  = vt->m_fol_fl(fl) * vt->sla_fl[fl];
            if ( vt->lai_fl[fl] <= _epsilon) vt->lai_fl[fl] = 0.0;
        }
    }

    return  LDNDC_ERR_OK;
}



/*!
 * @brief
 *
 */
double
ldndc::LD_PlantFunctions::aboveground_wood_biomass_from_structure(
                                                                  MoBiLE_Plant * _vt)
{
    double const stem_volume( m_alm.stem_volume( _vt->height_max,
                                                 _vt->dbh,
                                                 _vt->dbas,
                                                 (*_vt)->CONIFEROUS(),
                                                 (*_vt)->TAP_P1(),
                                                 (*_vt)->TAP_P2(),
                                                 (*_vt)->TAP_P3()));
    return stem_volume * _vt->tree_number / (1.0 - _vt->f_branch) * (*_vt)->DSAP() * cbm::DM3_IN_M3 * cbm::HA_IN_M2;
}



/*!
 * @brief updates structure of fine roots in all soil layers
 *
 */
lerr_t
ldndc::LD_PlantFunctions::initialize_wood_biomass(
                                                  MoBiLE_Plant * vt,
                                                  double  _initial_biomass,
                                                  double  _fom)
{
    if ( vt->groupId() != SPECIES_GROUP_WOOD)
    {
        vt->qsfm = 0.0;
        vt->mSap = 0.0;
        vt->mCor = 0.0;
        vt->tree_number = 0.0;

        return  LDNDC_ERR_OK;
    }

    // aboveground woody biomass
    double const aboveground_wood( (_initial_biomass > 0.0) ?
                                  cbm::bound_min( 0.0, _initial_biomass * cbm::HA_IN_M2 - vt->mFol - vt->mBud) :
                                  aboveground_wood_biomass_from_structure( vt));

    // total woody biomass
    double const total_wood( aboveground_wood / ( 1.0 - (*vt)->UGWDF()));

    // sapwood foliage mass relation
    vt->qsfm = get_sapwood_foliage_ratio(vt);

    // optimum sapwood biomass
    double const optimum_sapwood( get_optimum_sap_wood_biomass( vt));

    // sapwood biomass
    vt->mSap = std::max( optimum_sapwood * ( 1.0 - (*vt)->TOSAPMAX() * ( 365.0 - (*vt)->SENESCSTART())),
                         vt->qsfm * vt->mFolMax * ( 1.0 - _fom));
    vt->mSap = cbm::bound( 0.0, vt->mSap, total_wood);

    // core wood biomass
    vt->mCor = cbm::bound_min(0.0 , total_wood - vt->mSap);

    return  LDNDC_ERR_OK;
}



/*!
 * @brief updates structure of fine roots in all soil layers
 *
 */
lerr_t
ldndc::LD_PlantFunctions::initialize_foliage_dependent_fine_root_biomass(
                MoBiLE_Plant * vt, double _fom)
{
    // fine root biomass
    vt->mFrt = (*vt)->QRF() * ( vt->mBud + vt->mFol) * ( 1.0 - _fom);

    // FIXME: Depending on CFCROPT() we decide the existence of mycorrhyza modules
    if ( !cbm::flt_equal_zero((*vt)->CFCROPT()))
    {
        vt->mFrt *= ( 1.0 - (*vt)->CFCROPT() / ( 1.0 + (*vt)->CFCROPT()));
    }

    return  LDNDC_ERR_OK;
}



/*!
 * @brief
 *
 */
double
ldndc::LD_PlantFunctions::get_foliage_age_of_senescence(
                                                        MoBiLE_Plant * _p,
                                                        double _frac)
{
    return ( (*_p)->DLEAFSHED() - (*_p)->NDMORTA() + int( _frac * (*_p)->NDMORTA()));
}



double
ldndc::LD_PlantFunctions::get_dvs_mortality_of_age_class(
                                                         int _foliage_age_of_start_senescence,
                                                         int _days_of_senescence,
                                                         int _foliage_age)
{
    int const _foliage_age_of_full_senescence( _foliage_age_of_start_senescence + _days_of_senescence);

    // remaining foliage is shed at the last day of foliage longvity
    if ( _foliage_age >= _foliage_age_of_full_senescence)
    {
        return 1.0;
    }
    else if ( _foliage_age >= _foliage_age_of_start_senescence)
    {
        return cbm::bound(0.0,
                          exp( -1.0 * cbm::sqr( _foliage_age - _foliage_age_of_full_senescence)
                              / ( cbm::sqr(0.5 * _days_of_senescence) * log(2.0))),
                          1.0);
    }
    else
    {
        return 0.0;
    }
}


double
ldndc::LD_PlantFunctions::get_dvs_mortality_contribution_of_age_class(
                                                                      MoBiLE_Plant *_p,
                                                                      int _age_class,
                                                                      double _dvs_mort)
{
    if ( cbm::flt_greater_zero( _p->mFol))
    {
        return  (_dvs_mort * _p->mFol_na[_age_class] / _p->mFol);
    }
    else
    {
        return  (_dvs_mort / double(_p->nb_ageclasses()));
    }
}




/*!
 * @brief
 *      calculates age of foliage age class in days
 */
int
ldndc::LD_PlantFunctions::get_foliage_age(
                                          int _start_of_year,
                                          int _doy,
                                          int _nd_age_classes)
{
    // age of the foliage age class beginning with the first day of the flushing year
    int  foliage_age( _nd_age_classes * 365);
    if ( _doy >= _start_of_year)
    {
        foliage_age += _doy - _start_of_year + 1;
    }
    else
    {
        foliage_age += _doy - _start_of_year + 365;
    }

    return foliage_age;
}


/*!
 * @brief
 *
 */
lerr_t
ldndc::LD_PlantFunctions::initialize_seasonality(
                                                 MoBiLE_Plant *  vt,
                                                 unsigned int _year_start,
                                                 unsigned int _day_of_year,
                                                 unsigned int & _bud_state)
{
    size_t  fl_cnt( vt->nb_foliagelayers());
    for ( size_t  fl = 0;  fl < fl_cnt;  fl++)
    {
        vt->ef_iso_fl[fl]  = 0.0;
        vt->isoAct_fl[fl]  = 0.0;
        vt->monoAct_fl[fl] = 0.0;
        vt->ef_mono_fl[fl] = 0.0;
        vt->vcAct25_fl[fl] = (*vt)->VCMAX25();
    }

    double daylength_min( 24.0);
    double daylength_max( 0.0);
    for (size_t doy = 1; doy <= 365; ++doy)
    {
        double const daylength( ldndc::meteo::daylength( se_.latitude(), doy));
        if ( cbm::flt_greater( daylength, daylength_max))
        {
            daylength_max = daylength;
        }
        if ( cbm::flt_greater( daylength_min, daylength))
        {
            daylength_min = daylength;
        }
    }

    double const amplitude_temp( this->cl_.annual_temperature_amplitude());
    double const annual_temp( this->cl_.annual_temperature_average());
    double const annual_temp_min( annual_temp - amplitude_temp);
    double const annual_temp_max( annual_temp + amplitude_temp);

    unsigned int  ndcount( 0);
    while ( ++ndcount < _day_of_year)
    {
        double const daylength( ldndc::meteo::daylength( se_.latitude(), ndcount));
        double const day_temp( annual_temp_min + (annual_temp_max - annual_temp_min) * (daylength - daylength_min) / (daylength_max - daylength_min));
        
        if ( cbm::flt_greater_zero( day_temp))
        {
            vt->growing_degree_days += day_temp;
        }

        // day since MUP at which budbreak is assumed
        if (( vt->growing_degree_days >= (*vt)->GDDFOLSTART())  &&  ( vt->dEmerg == -1))
        {
            vt->dEmerg = ndcount;
        }

        vt->dvsMort = 0.0;
        for ( size_t  na = 0;  na < vt->nb_ageclasses();  ++na)
        {
            unsigned int foliage_age( get_foliage_age( _year_start, ndcount, na));
            double const dvs_mort( get_dvs_mortality_of_age_class( get_foliage_age_of_senescence( vt, 0.0), (*vt)->NDMORTA(), foliage_age));

            vt->dvsMort += get_dvs_mortality_contribution_of_age_class( vt, na, dvs_mort);
        }

        vt->dvsFlush = update_dvs_flush( vt, ndcount - vt->dEmerg);
        vt->dvsWood = vt->dvsFlush;
        double  pstatus( std::min(vt->dvsFlush, 1.0 - vt->dvsMort));

        if (vt->dvsFlush >= 0.99)
        {
            _bud_state++;
        }

        double  rad( cbm::bound_min( 0.0,
                                     ldndc::meteo::daily_solar_radiation(
                                                            se_.latitude(), LD_RtCfg.clk->yearday(),
                                                            LD_RtCfg.clk->days_in_year()) * ( 1.0 - cl_.station_info()->cloudiness)));

        if ( cbm::flt_greater_zero( pstatus))
        {
            /* daylength normalized to 12 h; i.e. 12 h = 1; Lehning et al. 2001 */
            double  dayl_norm( daylength / ( cbm::HR_IN_DAY * 0.5));
            
            double const arrh( (*vt)->PA() * exp(-(*vt)->AEIS() / ( cbm::RGAS * (annual_temp + cbm::D_IN_K))));
            for ( size_t  fl = 0; fl < fl_cnt;  fl++)
            {
                /* following lehning et al. 2001 */
                vt->isoAct_fl[fl]  += ((*vt)->ALPHA0_IS() * pstatus * rad * cbm::UMOL_IN_W * dayl_norm * arrh
                                             - ((*vt)->MUE_IS() * vt->isoAct_fl[fl]));

                vt->monoAct_fl[fl] += ((*vt)->ALPHA0_MT() * pstatus * rad * cbm::UMOL_IN_W * dayl_norm * arrh
                                             - ((*vt)->MUE_MT() * vt->monoAct_fl[fl]));
                
                vt->ef_iso_fl[fl] = (*vt)->EF_ISO();
                vt->ef_mono_fl[fl] = (*vt)->EF_MONO();
            }
        }
    }

    return LDNDC_ERR_OK;
}


/*!
 * @brief
 *
 */
lerr_t
ldndc::LD_PlantFunctions::initialize_new_foliage_biomass(
                                                         MoBiLE_Plant *  vt,
                                                         double _initial_biomass,
                                                         unsigned int _year_start,
                                                         unsigned int _day_of_year,
                                                         unsigned int _bud_state)
{
    double const biomass_vt = ( cbm::is_valid( _initial_biomass) ? _initial_biomass : 100.0) * cbm::HA_IN_M2;

    vt->mFol = 0.0;
    vt->mBud = 0.0;

    if ( vt->nb_ageclasses() > 0)
    {
        double *  dvs = new double[vt->nb_ageclasses()];

        double  dvsSum( 1.0);
        for ( size_t  na = 1;  na < vt->nb_ageclasses();  ++na)
        {
            unsigned int foliage_age( get_foliage_age( _year_start, _day_of_year, na));
            dvs[na] = 1.0 - get_dvs_mortality_of_age_class( get_foliage_age_of_senescence( vt, 0.0), (*vt)->NDMORTA(), foliage_age);
            dvsSum += dvs[na];
        }

        double const mFolOpt( (*vt)->IS_WOOD() ? get_optimum_foliage_biomass_trees( vt) :
                                                 get_optimum_foliage_biomass_grass( vt));

        for ( size_t  na = 0;  na < vt->nb_ageclasses();  ++na)
        {
            // non woody, non evergreen species
            if ((*vt)->IS_WOOD() == false && vt->nb_ageclasses() == 1)
            {
                vt->mBud = mFolOpt * (1.0 - vt->dvsFlush) / dvsSum;
                vt->mFol = mFolOpt * vt->dvsFlush / dvsSum;
                vt->mFol_na[na] = vt->mFol;
            }
            // trees or evergreens before flushing
            else if (na == 0)
            {
                vt->mBud = mFolOpt * (1.0 - vt->dvsFlush) / dvsSum;
                vt->mFol_na[na] = mFolOpt * vt->dvsFlush / dvsSum;
                vt->mFol = vt->mFol_na[na];
            }
            // trees or evergreen after flushing
            else
            {
                vt->mFol_na[na] = mFolOpt * dvs[na] / dvsSum;
                vt->mFol += vt->mFol_na[na];
            }
        }

        delete[] dvs;
    }


    vt->mBudStart = vt->mBud;

    /*
     * bud biomass is decreased in dependence on time after full
     * leaf development due to respiration (reasonable??)
     *
     * 367 is arbitrarily set to ensure the equation is valid
     * for both leap years and others (doys := current julian day)
     */
    if ( _bud_state > 0)
    {
        vt->mBud = (1.0 - ( _bud_state / double(367 - _day_of_year))) * vt->mBud;
    }


    if ((*vt)->IS_WOOD() == false)
    {
        double const foliage( vt->mFol + vt->mBud);
        if ( cbm::flt_greater_zero( biomass_vt) &&
             cbm::flt_greater_zero( foliage))
        {
            vt->mFol = biomass_vt * vt->mFol / foliage;
            vt->mBud = biomass_vt * vt->mBud / foliage;
        }
    }

    vt->mFolMax = (vt->mFol + vt->mBud * (1.0 - vt->dvsFlush));
    vt->mFolMin = (vt->mFol - vt->mFol_na[0] * vt->dvsFlush);

    return  LDNDC_ERR_OK;
}


/*!
 * @brief
 *
 */
lerr_t
ldndc::LD_PlantFunctions::initialize_new_biomass(
                                                 MoBiLE_Plant * _vt,
                                                 double  _initial_biomass)
{
    size_t  fl_cnt( _vt->nb_foliagelayers());

    // further physiological state variables more or less independent of season
    double const fom( 0.3);

    _vt->f_fac  = (*_vt)->FFACMAX();
    _vt->d_n_retention  = 0.0;

    _vt->dEmerg   = -1;
    _vt->growing_degree_days = 0.0;
    _vt->dvsFlush = 0.0;
    _vt->dvsWood  = 0.0;
    _vt->dvsMort  = 0.0;

    unsigned int  year_start = MoBiLE_YearDoYOffset( LD_RtCfg.clk, this->se_.latitude());
    unsigned int  year_end = MoBiLE_YearDoYEnd( LD_RtCfg.clk, this->se_.latitude());
    unsigned int  doys( LD_RtCfg.clk->yearday());
    (void)ldndc::meteo::latitude_rad( se_.latitude(), &doys, &year_start, &year_end);

    /**/
    unsigned int  budstate( 0);
    initialize_seasonality( _vt, year_start, doys, budstate);

    /* initialize foliage including buds */
    initialize_new_foliage_biomass( _vt, _initial_biomass, year_start, doys, budstate);

    /* initialize fine roots */
    initialize_foliage_dependent_fine_root_biomass( _vt, fom);

    fineroots_biomass_distribution(
                           sl_.soil_layers_in_litter_cnt(),
                           sl_.soil_layer_cnt(),
                           (*_vt)->PSL(), _vt->rooting_depth,
                           _vt->fFrt_sl, &sc_.h_sl[0]);

    /* initialize maximum leaf area that can be supported by foliage and bud biomass */
    {
        canopy_lai_distribution(
                 (*_vt)->SLAMIN(), (*_vt)->SLAMAX(),
                 _vt->height_max, _vt->height_at_canopy_start,
                 _vt->mFol + _vt->mBud, _vt->fFol_fl, &ph_.h_fl[0],
                 _vt->lai_fl, _vt->sla_fl, fl_cnt, se_.canopylayers());

        _vt->lai_max = cbm::sum( _vt->lai_fl, _vt->nb_foliagelayers());
    }

    initialize_wood_biomass( _vt, _initial_biomass, fom);

    initialize_nitrogen_concentrations( _vt);

    /* actual leaf area */
    {
        canopy_lai_distribution(
                 (*_vt)->SLAMIN(), (*_vt)->SLAMAX(),
                 _vt->height_max, _vt->height_at_canopy_start,
                 _vt->mFol, _vt->fFol_fl, &ph_.h_fl[0],
                 _vt->lai_fl, _vt->sla_fl, fl_cnt, se_.canopylayers());
    }

    return  LDNDC_ERR_OK;
}


/*!
 * @details
 *  Function to account for increasing N demand with canopy height. 
 *  This is related to SLA but not quite clear if it should be linear or non-linear.
 * 
 *  @todo: find a literature based exponent for the function
  */
double
ldndc::LD_PlantFunctions::optimum_nitrogen_content_foliage(
                                                           MoBiLE_Plant * _vt)
{
    double nFolOpt( 0.0);
    double exp(0.3); 
    for ( size_t  fl = 0;  fl < _vt->nb_foliagelayers();  ++fl)
    {
        if ( cbm::flt_greater_zero( _vt->sla_fl[fl]))
        {
            nFolOpt += (*_vt)->NCFOLOPT() * _vt->m_fol_fl(fl) * pow( (*_vt)->SLAMIN() / _vt->sla_fl[fl], exp);

        }
    }
    return nFolOpt;
}


/*!
 * @details
 *  Function to account for increasing N demand with canopy height. 
 *  This is related to SLA but not quite clear if it should be linear or non-linear.
 * 
 *  @todo: find a literature based exponent for the function
 */
double
ldndc::LD_PlantFunctions::optimum_nitrogen_content_buds( MoBiLE_Plant * _vt)
{
    double nBudOpt( 0.0);
    double exp(0.3);
    for ( size_t  fl = 0;  fl < _vt->nb_foliagelayers();  ++fl)
    {
        if ( cbm::flt_greater_zero( _vt->sla_fl[fl]))
        {
            nBudOpt += (*_vt)->NCFOLOPT() * _vt->fFol_fl[fl] * _vt->mBud * pow( (*_vt)->SLAMIN() / _vt->sla_fl[fl], exp);
        }
    }
    return nBudOpt;
}


/*!
 * @details
 */
lerr_t
ldndc::LD_PlantFunctions::initialize_nitrogen_concentrations( MoBiLE_Plant * vt)
{
    /* foliage */
        /* @todo
         *  rg: shouldn't the f_ncc factor be applied for foliage and buds too?
         */
    if ( cbm::flt_greater_zero( vt->mFol))
    {
        vt->ncFol = optimum_nitrogen_content_foliage( vt) / vt->mFol;
    }
    else{ vt->ncFol = 0.0; }

    /* buds */
    if ( vt->mBud > 0.0)
    {
        vt->ncBud = optimum_nitrogen_content_buds( vt) / vt->mBud;
    }
    else{ vt->ncBud = 0.0; }

    /* fine roots */
    if ( vt->mFrt > 0.0)
    {
        /* @todo
         *  Check this statement:
         *  0.8 to ensure a deficit that triggers at least some uptake
         *  rg: looks strange to me. Where do you put the nitrogen, if any compartment is at it's maximum?
         */
        vt->ncFrt = (*vt)->NC_FINEROOTS_MAX() * vt->f_ncc * 0.8;
    }
    else{ vt->ncFrt = 0.0; }

    /*sap (living) and core (dead) wood*/
    if ( vt->mSap > 0.0)
    {
        vt->ncSap = (*vt)->NCSAPOPT() * vt->f_ncc;
        vt->ncCor = vt->ncSap;
    }
    else
    {
        vt->ncSap = 0.0;
        vt->ncCor = 0.0;
    }

    return  LDNDC_ERR_OK;
}


/*!
 * @details
 *
 */
lerr_t
ldndc::LD_PlantFunctions::canopy_microclimate()
{
    // initialisation of canopy variables
    size_t const  fl_cnt( m_veg->canopy_layers_used());
    double const  ffl( 1.0 / (double)fl_cnt);

    bool  temp_above_zero( cbm::flt_greater_zero( mc_.nd_airtemperature));

    for ( size_t  fl = 0;  fl < fl_cnt;  ++fl)
    {
        double const  fl_dbl( fl + 1);

        mc_.temp_fl[fl]    = mc_.temp_sl[0] - ( mc_.temp_sl[0] - mc_.nd_airtemperature) * (fl_dbl - 0.5) * ffl;

        mc_.tempOld_fl[fl] = mc_.temp_fl[fl];
        mc_.tFol_fl[fl]    = mc_.temp_fl[fl];
        mc_.shortwaveradiation_fl[fl]     = 0.0;
        mc_.vpd_fl[fl]     = mc_.nd_watervaporsaturationdeficit;
        mc_.win_fl[fl]     = mc_.nd_windspeed * fl_dbl * ffl;
        mc_.rad24_fl[fl]   = mc_.nd_shortwaveradiation_in * fl_dbl * ffl;
        mc_.rad240_fl[fl]  = mc_.nd_shortwaveradiation_in * fl_dbl * ffl;
        mc_.tFol24_fl[fl]  = mc_.temp_fl[fl];
        mc_.tFol240_fl[fl] = mc_.temp_fl[fl];
        mc_.sunlitfoliagefraction24_fl[fl]  =  mc_.ts_sunlitfoliagefraction_fl[fl];
        mc_.sunlitfoliagefraction240_fl[fl]  =  mc_.ts_sunlitfoliagefraction_fl[fl];
        mc_.parsun24_fl[fl] = mc_.parsun_fl[fl];
        mc_.parsun240_fl[fl] = mc_.parsun_fl[fl];
        mc_.parshd24_fl[fl] = mc_.parshd_fl[fl];
        mc_.parshd240_fl[fl] = mc_.parshd_fl[fl];        

        mc_.nd_temp_fl[fl] = mc_.temp_fl[fl];
        mc_.nd_vpd_fl[fl]  = mc_.vpd_fl[fl];
        mc_.nd_win_fl[fl]  = mc_.win_fl[fl];
        mc_.nd_shortwaveradiation_fl[fl]  = 0.0;

        if ( temp_above_zero)
        {
            mc_.nd_tMax_fl[fl] = mc_.nd_maximumairtemperature * (mc_.nd_temp_fl[fl] + cbm::D_IN_K) / ( mc_.nd_airtemperature + cbm::D_IN_K);
            mc_.nd_tMin_fl[fl] = mc_.nd_minimumairtemperature * mc_.nd_temp_fl[fl] / mc_.nd_airtemperature;
        }
    }

    /* subdaily items */
    mc_.temp_a = 0.5 * ( mc_.temp_fl[0] + mc_.temp_sl[0]);

    /* daily items */
    mc_.nd_temp_a = mc_.temp_a;

    return  LDNDC_ERR_OK;
}


double
ldndc::LD_PlantFunctions::nitrogen_uptake_temperature_dependency(
                                                                 double _t_min,
                                                                 double _t_max,
                                                                 double _temp)
{
    /*! Minimum temperature where N uptake is possible (estimated from Gessler et al. 1998, corroborated by Alvarez-Uria and Koerner 2007) */

    /*! Optimum temperature where maximum N uptake occurs (Gessler et al. 1998) */


    // temperature dependency of root uptake
    if ( cbm::flt_greater_equal( _t_min, _temp))
    {
        return 0.0;
    }
    else if ( cbm::flt_greater( _t_max, _temp))
    {
        return (_temp - _t_min) / (_t_max - _t_min);
    }
    else
    {
        return 1.0;
    }
}


double
ldndc::LD_PlantFunctions::accumulated_n_uptake()
{
    return ph_.accumulated_no3_uptake_sl.sum() +
           ph_.accumulated_nh4_uptake_sl.sum() +
           ph_.accumulated_nh3_uptake_sl.sum() +
           ph_.accumulated_don_uptake_sl.sum();
}


/*!
 * @details
 * The standard vertical distribution of biomass is done with the same assumptions
 * for canopy (@ref veglibs_canopy) and rooting space (@ref veglibs_roots). Only
 * for root distribution there are several more options available.
 */
void
ldndc::estimate_single_layer_biomass(
                                 double const _ps,
                                 double const _h_bottom,
                                 double const _h_top,
                                 double  _lw,
                                 double  &_fract,
                                 double  _h_cum_bottom,
                                 double  _h_cum_top)
{
    // calculation proceeds as long as cumulative length is smaller than total crown length
    if ( cbm::flt_greater( _h_cum_top, _h_bottom) &&
         cbm::flt_less_equal( _h_cum_bottom, _h_top))
    {
        // distance from the crown start/ soil surface for which the relative biomass shall be estimated
        double  hAct( 0.0);
        double const length( _h_top - _h_bottom);
        
        // if total crown height is within the first canopy layer
        if ( cbm::flt_less( _h_top, _lw))
        {
            hAct = length;
        }
        // for the partially filled last canopy/ soil layer
        else if ( cbm::flt_less_equal( _h_top, _h_cum_top))
        {
            hAct = length - ( _h_top - _h_cum_bottom) * 0.5;
        }
        // for the partially filled first canopy/ soil layer
        else if ( cbm::flt_less( _h_cum_bottom, _h_bottom))
        {
            hAct = ( _h_cum_top - _h_bottom) * 0.5;
        }
        // for fully filled canopy/ soil layers
        else
        {
            hAct = ( _h_cum_bottom - _h_bottom) + (_h_cum_top - _h_cum_bottom) * 0.5;
        }

        double const relH( cbm::flt_less( length, _lw) ? 1.0 : ((length - hAct) / length));

        // relative biomass distribution throughout the canopy/ soil according to Grote 2007
        double  fH( pow( _ps, 100.0 * hAct / ( cbm::flt_greater( length, 5.0) ? (length * length) : 25.0)));

        _fract = relH * fH;
    }
    else
    {
        _fract = 0.0;
    }
}


