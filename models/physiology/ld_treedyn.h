/*!
 * @file
 * @author
 *    - Ruediger Grote
 *    - David Kraus
 *
 * @date
 *
 */

#ifndef  LD_TREEDYN_H_
#define  LD_TREEDYN_H_

#include  "ld_modelsconfig.h"
#include  "physiology/ld_plantfunctions.h"
#include  "physiology/ld_rootsystem.h"

#include  "state/mbe_state.h"
#include  "ld_eventqueue.h"
//
//#include  <input/species/species.h>
//
//namespace ldndc {
//
//extern cbm::logger_t *  VegStructureTREEDYNLogger;
//
namespace ldndc
{

    class  LDNDC_API  TreeDynamics
    {


    public:
        TreeDynamics(
                     MoBiLE_State *,
                     cbm::io_kcomm_t *);

        ~TreeDynamics();

        lerr_t register_ports( cbm::io_kcomm_t *);
        lerr_t unregister_ports( cbm::io_kcomm_t *);

        LD_Allometry m_alm;
        LD_PlantFunctions  m_pf;
        substate_soilchemistry_t &  sc_;
        substate_physiology_t &  ph_;
        MoBiLE_PlantVegetation *  m_veg;
        input_class_setup_t const *  m_se;
        input_class_soillayers_t const *  m_sl;

        lerr_t
        Mortality( MoBiLE_Plant *,
                  double &_sWoodAbove,
                  double &_nLitWoodAbove,
                  double *_sWoodBelow_sl,
                  double *_nLitWoodBelow_sl);

        lerr_t
        Growth( MoBiLE_Plant *,
                bool,
                cbm::string_t,
                cbm::string_t,
                double &);

        lerr_t
        update_grass_coverage( MoBiLE_Plant * /* plant species */);
        
        lerr_t
        Crowding( MoBiLE_Plant *,
                  cbm::string_t);

        double
        get_branch_fraction(
                            cbm::string_t ,
                            MoBiLE_Plant *);

        lerr_t
        OnStructureChange( MoBiLE_Plant *,
                           RootSystemDNDC *_rs = NULL);

        lerr_t
        UpdateBiomass( MoBiLE_Plant *);

        lerr_t
        MassDistribution( MoBiLE_Plant *,
                          RootSystemDNDC *_rs = NULL);

        lerr_t
        LaiDistribution( MoBiLE_Plant *);

        /* events methods */

        int
        HandleEvents(
                     cbm::io_kcomm_t *_io_kcomm,
                     cbm::string_t /* crownlength_method */,
                     cbm::string_t /* branchfraction method */,
                     bool /* _competition method */,
                     double & /* foliage dry weight litter */,
                     double & /* foliage nitrogen litter */,
                     double & /* fruit dry weight litter */,
                     double & /* fruit nitrogen dry weight litter */,
                     double & /* aboveground wood dry weight litter */,
                     double & /* aboveground wood nitrogen litter */,
                     double * /* belowground wood dry weight litter */,
                     double * /* belowground wood dry weight litter */,
                     double * /* fineroots dry weight litter */,
                     double * /* fineroots nitrogen litter */);

        EventQueue  DefoliateEvents;
        CBM_Handle  DefoliateHandle;
        struct  EventDefoliateData
        {
            double  reduction_volume;
            bool  export_foliage;
        };
        /* defoliation, e.g., for insect calamities or crown fires */
        lerr_t  HandleEventDefoliate(
                                     MoBiLE_Plant*, EventDefoliateData const *,
                                     double & /* foliage dry weight litter */,
                                     double & /* foliage nitrogen litter */);
        int  HandleEventsDefoliate(
                                   double & /* foliage dry weight litter */,
                                   double & /* foliage nitrogen litter */);

        EventQueue  RegrowEvents;
        CBM_Handle  RegrowHandle;
        struct  EventRegrowData
        {
            double  export_agb;
            double  tree_number;
            double  tree_number_resize_factor;
            double  height_max;
        };
        lerr_t  HandleEventRegrow(
                                  MoBiLE_Plant*, EventRegrowData const *,
                                  cbm::string_t /* crownlength method */,
                                  cbm::string_t /* branchfraction method */,
                                  double & /* foliage dry weight litter */,
                                  /* double * foliage dry weight litter per age class ,*/
                                  double & /* foliage nitrogen litter */,
                                  double & /* fruit dry weight litter */,
                                  double & /* fruit nitrogen dry weight litter */,
                                  double & /* aboveground wood dry weight litter */,
                                  double & /* aboveground wood nitrogen litter */);
        int  HandleEventsRegrow(
                                cbm::string_t /* crownlength method */,
                                cbm::string_t /* branchfraction method */,
                                double & /* foliage dry weight litter */,
                                double & /* foliage nitrogen litter */,
                                double & /* fruit dry weight litter */,
                                double & /* fruit nitrogen dry weight litter */,
                                double & /* aboveground wood dry weight litter */,
                                double & /* aboveground wood nitrogen litter */);

        EventQueue  ThrowEvents;
        CBM_Handle  ThrowHandle;
        struct  EventThrowData
        {
            double  reduction_volume;
        };
        lerr_t  HandleEventThrow(
                                 cbm::io_kcomm_t *_io_kcomm, MoBiLE_Plant*, EventThrowData const *,
                                 double & /* foliage dry weight litter */,
                                 double & /* foliage nitrogen litter */,
                                 double & /* fruit dry weight litter */,
                                 double & /* fruit nitrogen dry weight litter */,
                                 double & /* aboveground wood dry weight litter */,
                                 double & /* aboveground wood nitrogen litter */,
                                 double * /* belowground wood dry weight litter */,
                                 double * /* belowground wood dry weight litter */,
                                 double * /* fineroots dry weight litter */,
                                 double * /* fineroots nitrogen litter */);
        int  HandleEventsThrow(
                               cbm::io_kcomm_t *_io_kcomm,
                               double & /* foliage dry weight litter */,
                               double & /* foliage nitrogen litter */,
                               double & /* fruit dry weight litter */,
                               double & /* fruit nitrogen dry weight litter */,
                               double & /* aboveground wood dry weight litter */,
                               double & /* aboveground wood nitrogen litter */,
                               double * /* belowground wood dry weight litter */,
                               double * /* belowground wood dry weight litter */,
                               double * /* fineroots dry weight litter */,
                               double * /* fineroots nitrogen litter */);

        EventQueue  ThinEvents;
        CBM_Handle  ThinHandle;
        struct  EventThinData
        {
            double  reduction_volume;
            double  reduction_number;
            bool  export_corewood;
            bool  export_sapwood;
        };
        lerr_t  HandleEventThin(
                                cbm::io_kcomm_t *_io_kcomm,
                                MoBiLE_Plant*, EventThinData const *,
                                cbm::string_t /* crownlength method */,
                                bool /* competition method */,
                                double & /* foliage dry weight litter */,
                                double & /* foliage nitrogen litter */,
                                double & /* fruit dry weight litter */,
                                double & /* fruit nitrogen dry weight litter */,
                                double & /* aboveground wood dry weight litter */,
                                double & /* aboveground wood nitrogen litter */,
                                double * /* belowground wood dry weight litter per layer */,
                                double * /* belowground wood nitrogen litter */,
                                double * /* fineroots dry weight litter per layer*/,
                                double * /* fineroots nitrogen litter per layer*/);
        int  HandleEventsThin(
                              cbm::io_kcomm_t *_io_kcomm,
                              cbm::string_t /* crownlength method */,
                              bool /* competition method */,
                              double & /* foliage dry weight litter */,
                              double & /* foliage nitrogen litter */,
                              double & /* fruit dry weight litter */,
                              double & /* fruit nitrogen dry weight litter */,
                              double & /* aboveground wood dry weight litter */,
                              double & /* aboveground wood nitrogen litter */,
                              double * /* belowground wood dry weight litter */,
                              double * /* belowground wood dry weight litter */,
                              double * /* fineroots dry weight litter */,
                              double * /* fineroots nitrogen litter */);

        EventQueue  HarvestEvents;
        CBM_Handle  HarvestHandle;
        struct  EventHarvestData
        {
            double  export_branch_wood;
            double  export_root_wood;
            double  export_stem_wood;
        };
        lerr_t  HandleEventHarvest(
                                   cbm::io_kcomm_t *_io_kcomm,
                                   MoBiLE_Plant*, EventHarvestData const *,
                                   double & /* foliage dry weight litter */,
                                   double & /* foliage nitrogen litter */,
                                   double & /* fruit dry weight litter */,
                                   double & /* aboveground wood dry weight litter */,
                                   double & /* aboveground wood nitrogen litter */,
                                   double * /* belowground wood dry weight litter */,
                                   double * /* belowground wood dry weight litter */,
                                   double * /* fineroots dry weight litter */,
                                   double * /* fineroots nitrogen litter */);
        int  HandleEventsHarvest(
                                 cbm::io_kcomm_t *_io_kcomm,
                                 double & /* foliage dry weight litter */,
                                 double & /* foliage nitrogen litter */,
                                 double & /* fruit dry weight litter */,
                                 double & /* aboveground wood dry weight litter */,
                                 double & /* aboveground wood nitrogen litter */,
                                 double * /* belowground wood dry weight litter */,
                                 double * /* belowground wood dry weight litter */,
                                 double * /* fineroots dry weight litter */,
                                 double * /* fineroots nitrogen litter */);

    };
}

#endif  /*  !LD_TREEDYN_H_  */

