/*!
 * @file
 * @author
 *      Ruediger Grote
 *
 * @date 2001
 */
#ifndef  LM_PHYSIOLOGY_PNET_H_
#define  LM_PHYSIOLOGY_PNET_H_


#include  "mbe_legacymodel.h"
#include  "state/mbe_state.h"
#include  "ld_shared.h"
#include  "physiology/ld_plantfunctions.h"
#include  "physiology/ld_treedyn.h"

#include  <logging/cbm_logging.h>

namespace ldndc {

extern cbm::logger_t *  PhysiologyPNETLogger;

class  LDNDC_API  PhysiologyPNET  :  public  MBE_LegacyModel
{
    LMOD_EXPORT_MODULE_INFO(PhysiologyPNET,"physiology:pnet","Physiology PNET");

    /***  module specific constants  ***/

    /*! Minimum temperature where N uptake is possible (estimated from Gessler et al. 1998, corroborated by Alvarez-Uria and Koerner 2007) */
    static const double TMIN_NUP;

    /*! Optimum temperature where maximum N uptake occurs (Gessler et al. 1998) */
    static const double TMAX_NUP;

    /*! ?? */
    static const double KM_NO3;

    /*! ?? */
    static const double KM_NH4;

    /*! ?? */
    static const double SRATEMAX;

    /*! parameter from frost hardiness function */
    static const double C;

    /*!  */
    static const double K;

    /*!  */
    static const double P;

    /*!  */
    static const double BETA;

    /*!  */
    static const double TEQ;

    /*!  */
    static const double TRANGE;

    /*! fraction of nitrogen from total deposition that is taken up by the canopy */
    static const double NUPTAIR;


    public:
        PhysiologyPNET(
                MoBiLE_State *,
                cbm::io_kcomm_t *,
                                timemode_e);

        ~PhysiologyPNET();

        lerr_t  register_ports( cbm::io_kcomm_t * );
        lerr_t  unregister_ports( cbm::io_kcomm_t * );

        lerr_t  configure( ldndc::config_file_t const *);

        lerr_t  initialize();

        lerr_t  solve();

        lerr_t  finalize() { return  LDNDC_ERR_OK; }
        
        /*!
         * @note
         *    it is sadly wrong to assume that you can
         *    put me to sleep during times when multiple
         *    species exist: in general, i cannot handle
         *    the resulting slot layout. if you do, i
         *    also have no way of knowing .. so beware!
         */
        lerr_t  sleep() { return  LDNDC_ERR_OK; }
        lerr_t  wake() { return  LDNDC_ERR_OK; }

        cbm::string_t branchfraction_opt;
        cbm::string_t crownlength_opt;
        bool competition_opt;

    private:

        cbm::io_kcomm_t *  io_kcomm;

        input_class_setup_t const &  m_setup;
        input_class_siteparameters_t const &  sipar_;
        input_class_soillayers_t const &  sl_;
        input_class_species_t const *  m_species;

        substate_airchemistry_t const &  ac_;
        substate_microclimate_t const &  mc_;
        substate_physiology_t &  ph_;
        substate_soilchemistry_t &  sc_;
        substate_watercycle_t &  wc_;

        MoBiLE_PlantVegetation *  m_veg;

        LD_PlantFunctions  m_pf;
        TreeDynamics  m_treedyn;

        SubscribedEvent<LD_EventHandlerQueue>  m_HarvestEvents;
        SubscribedEvent<LD_EventHandlerQueue>  m_PlantEvents;

        lvector_t< double > no3_sl;
        lvector_t< double > nh4_sl;

        double  latitude;

        double  FolNCon;
        double  accumulated_transpiration_old;

        unsigned int  year_start_, year_end_;

        lvector_t< double > LayerGrossPsn;  // for putting into layer specific assimilation variable
        template < typename  _E >
        struct  scalar_with_array_look_and_feel_t
        {
            _E  value;
            _E & operator=( _E const &  _value)
                { this->value = _value; return  this->value; }
            _E &  operator[]( size_t)
                { return  this->value; }
            _E &  operator[]( MoBiLE_Plant *)
                { return  this->value; }
        };
        /* NOTE  change this as soon as PNET becomes multi-species capable */
        scalar_with_array_look_and_feel_t< double >  mFolOld_vt;    // saved initial foliage biomass for new nitrogen concentration calculations
        scalar_with_array_look_and_feel_t< double >  mFrtOld_vt;    // saved initial fine root biomass for new nitrogen concentration calculations
        scalar_with_array_look_and_feel_t< double >  mBudOld_vt;    // saved initial bud (reserve) biomass for new nitrogen concentration calculations
        scalar_with_array_look_and_feel_t< double >  mSapOld_vt;    // saved initial sapwood biomass for new nitrogen concentration calculations
        scalar_with_array_look_and_feel_t< double >  mCorOld_vt;    // saved initial core wood biomass for new nitrogen concentration calculations

        lvector_t< double > volume_old_vt;

        lvector_t< double > mBudLoss_vt;
        lvector_t< double > mSapLoss_vt;

        double c_begin;                   // carbon balance variables
        double n_begin;                   // nitrogen balance variables
    
        // - water balance variables
        double DWater;                    // average relative water availability (drought) during an integration step (mm)
        double FolLitSum;                 // litterfall (gDW m-2)

        double tra;                       // transpiration
        double uptNAir;

        // - physical environment variables (representing the average day of the integration timestep (day or month))

        double Dvpd;                      // Gradient on vapour pressure deficit (?)
        double DayResp;                   // Respiration during daytime (?)
        double NightResp;                 // Respiration during nighttime (?)
        double CanopyNetPsn;              // Canopy net photosynthesis (?)
        double CanopyGrossPsn;            // Canopy gross photosynthesis (?)
        double FolProdCMo;                // foliage carbon production over the integration timestep (day or month) (?)
        double FolGRespMo;                // foliage growth respiration over the integration timestep (day or month) (?)
        double NetPsnMo;                  // net photosynthesis over the integration timestep (day or month) (?)
        double CanopyGrossPsnMo;          // canopy gross photosynthesis over the integration timestep (day or month) (?)
        double RootCAddMo;                // fine root growth over the integration timestep (day or month)

        // - allocation and mass variables
        double qwodfolact;                // relation between wood and foliage biomass at the start of the year
        double transfDWbs;                // annual mass transfer from reserves (buds) to (sap)wood (gC m-2)
        double transfDWsb;                // annual mass transfer from (sap)wood to reserves (buds) (gC m-2)
        double WoodC;                     // living wood carbon (gC m-2)
        double BudC;                      // reserve carbon for future foliage (gC m-2)
        double RootC;                     // fine root carbon (gC m-2)
        double FolMass;                   // foliage biomass (gDW m-2)
        double FolMassMax;                // maximum summer foliar biomass (gDW m-2) (not really a parameter)
        double FolMassMin;                // minimum summer foliar biomass (gDW m-2) (not really a parameter)
        double FolReten;                  // foliage retention time (yr) (not really a parameter)

        double WoodMRespMo;               // daily maintanance respiration of the wood
        double WoodGRespMo;               // daily growth respiration of the wood
        double RootMRespMo;               // daily maintanance root respiration
        double RootGRespMo;               // daily root growth respiration
        double WoodProdCMo;               // daily wood production
        double RootProdCMo;               // daily carbon loss form the root system other than respiration;

        double PosCBalMass;               //!<
        double PosCBalMassTot;            //!<
        double PosCBalMassIx;             //!<

        double LightEffMin;               //!<
        double WoodCStart;                //!<
        double BudCStart;                 //!<

        // other variables
        double fFrost;
        bool mature;

        /***  methods  ***/

        lerr_t step_init();

        lerr_t step_out();

        void balance_check( MoBiLE_Plant *p, unsigned int _i);

// sk:unused        /*! CalcFrostDamage
// sk:unused         *
// sk:unused         */
// sk:unused        double CalcFrostDamage();

        /*! update_nitrogen_concentration
         *
         */
        void update_nitrogen_concentration( MoBiLE_Plant *);

        /*!
         *
         */
        void YearlyReset( MoBiLE_Plant *);

        /*! Phenology
         *
         */
        void Phenology(
                       MoBiLE_Plant *,
                       int /*GrowthPhase*/,
                       double /*DWater*/);

        /*! Photosyn
         *
         */
        void Photosyn( MoBiLE_Plant *);

        /*! AllocateMo
         *
         */
        void AllocateMo( MoBiLE_Plant *);

        /*! AllocateYr
         *
         */
        void AllocateYr( MoBiLE_Plant *);

        /*! N_uptake
         *
         */
        void N_uptake( MoBiLE_Plant *, double &);

        /*! Update
         *
         */
        void Update( MoBiLE_Plant *, double  /*upt1_vt*/, double  /*upt2_vt*/);

        /*! TransformPhysiology
         *
         */
        void TransformPhysiology( MoBiLE_Plant *);

        lerr_t CalcVegStructure();

        /*! get_na_max_pnet
         *
         */
        size_t get_na_max_pnet();

};
} /*namespace ldndc*/

#endif  /*  !LM_PHYSIOLOGY_PNET_H_  */

