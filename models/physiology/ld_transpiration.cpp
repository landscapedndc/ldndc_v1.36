/*!
 *
 * @author
 *  - David Kraus
 *
 * @date
 *  13 Nov, 2017
 *
 */

#include  "ld_modelsconfig.h"
#include  "physiology/ld_transpiration.h"


/*!
 * @page waterlibs
 * @section secpotentialtranspiration Potential transpiration
 * @subsection potentialcroptranspiration Potential transpiration for non-woody plants
 * This function calculates the potential transpiration \f$T_{pot}\f$ (water in kg).
 * Therefore, it is assumed that the loss of water is directly related to the gain
 * of carbon (by photosynthesis):
 * \f[
 *   T_{pot} = scale \cdot \_carbon\_uptake \cdot mco2 / mc \,
 * \f]
 * \f[
 *   scale = \frac {1.3 - 0.0009 \cdot \_co2} {WUECMAX}
 * \f]
 * with:
 * - carbon_uptake: CO2 uptake by photosynthesis [kg m-2 ground]
 * - mco2, mc: molar masses of CO2 and carbon, respectively [g/mol]
 * - _co2: CO2 concentration of the air [ppb]
 * - WUECMAX: parameter describing the species-specific water use efficiency [mg CO2 / g H2O]
 *
 * The function assumes an increase in carbon uptake efficiency (a decrease of water
 * loss per unit carbon uptake) with increasing CO2 concentration in the air.
 *
 * @todo
 * citation for the scale factor?
 */
double
ldndc::potential_crop_transpiration(
    double _co2,
    double _carbon_uptake,
    double _wuecmax)
{
    if (cbm::flt_greater_zero(_carbon_uptake))
    {
        double const fco2(1.3 - 0.0009 * _co2);
        return _carbon_uptake / _wuecmax * cbm::MCO2 / cbm::MC * fco2;
    }
    else
    {
        return 0.0;
    }
}


/*!
 * @page waterlibs
 * @subsection potentialtranspiration_wue Potential transpiration for woody plants based on water use efficiency
 * Similar as for non-woody plants, the potential transpiration \f$T_{pot}\f$ 
 * for woody plants is assumed to correlate with the gain of carbon by photosynthesis.
 * The water use efficiency, however, is allowed to vary with relative available
 * soil water. This accounts for the frequent observation that the efficiency of 
 * carbon uptake increases with decreases in water supply.
 * \f[
 *   T_{pot} = \frac {\_carbon\_uptake}{wue} \cdot \frac {vpd \cdot mco2} {mc}
 * \f]
 * \f[
 *   wue = (WUECMAX - WUECMIN) \cdot sum( rwa\_{sl} \cdot \frac { h\_{sl} }{rd} )
 * \f]
 * \f[
 *   rwa\_{sl} = \frac { wc\_{sl} - wcmin\_{sl} } {wcmax\_{sl} - wcmin\_{sl}}
 * \f]
 * with:
 * - sl: soil layer indicator for all layers within rooting depth
 * - vpd: vapor pressure deficit
 * - carbon_uptake: CO2 uptake by photosynthesis [kg m-2 ground]
 * - mco2, mc: molar masses of CO2 and carbon, respectively [g/mol]
 * - rwa: relative available water
 * - h: height of a soil layer 
 * - rd: rooting depth
 * - wc: water content in a soil layer
 * - wcmax, wcmin: maximum and minimum water holding capacity
 * - WUECMAX, WUECMAX: parameter describing the species-specific maximum and minimum
 *   water use efficiency respectively [mg CO2 / g H2O]
 *
 * @note
 *  Transpiration demand is derived from photosynthesis, thus it might be not
 *  consistent with potential evaporation determined from temperature.
 */
double
ldndc::potential_wood_transpiration(
                             soillayers::input_class_soillayers_t const &  _sl,
                             double _vpd,
                             double _carbon_uptake,
                             double _f_area,
                             double _wuecmax,
                             double _wuecmin,
                             lvector_t< double > _h_sl,
                             lvector_t< double > _wc,
                             lvector_t< double > _wc_min,
                             lvector_t< double > _wc_max)
{
    // daily potential transpiration [m]
    double day_pot_trans( 0.0);
    double hslSum( _h_sl.sum());

    // potential transpiration for forests
    if ( cbm::flt_greater_zero( _carbon_uptake))
    {
        double wuec( _wuecmax);
        if ( cbm::flt_equal( _wuecmax, _wuecmin))
        {
            /* yields factor  "f = wuec_max - wuec_min = 0" */
        }
        else
        {
            double wuec_0( 0.0);
            for ( size_t sl = 0;  sl < _sl.soil_layer_cnt();  ++sl)
            {
                wuec_0 += std::min( 1.0, ( _wc[sl] - _wc_min[sl]) / (_wc_max[sl] - _wc_min[sl])) * _h_sl[sl];
            }
            wuec -= wuec_0 * ( _wuecmax - _wuecmin) * _f_area / hslSum;
        }

        day_pot_trans += _carbon_uptake / wuec;

        return  ((_vpd * cbm::MCO2) / cbm::MC) * day_pot_trans;
    }
    else
    {
        return 0.0;
    }
}


/*!
 * @page waterlibs
 * @subsection potentialtranspiration_gs Potential transpiration for woody plants based on stomatal conductance
 * The potential transpiration stream (in m hr-1) is calculated from canopylayer-specific
 * stomatal conductance and the vapor pressure deficits in each layer @cite jarvis:1986a.
 * These layered values are added up to scale to the whole-canopy.
 * The canopylayer-specific transpiration \f$ tr_{fl} \f$ is thus calculated as:
 * \n
 * \f$ tr_{fl} = \frac{vpd_{fl}}{P_{atm}} \; c_{fl} \; lai_{fl} \f$
 * \n
 * With stomatal conductance \f$ c_{fl} \f$ is either:
 * \n
 * \f$ c_{fl} = GSMIN + relativeconductance_{fl} \; (GSMIN - GSMAX)\f$
 * \n
 * if stomatal conductance is not directly calculated, or:
 * \n
 * \f$ c_{fl} = GSMIN + frad_{fl} (gs_{fl} - GSMIN)\f$
 * \n
 * if stomatal conductance has been determined by the Berry Ball method.
 * 
 * with:
 * - fl: canopy layer indicator
 * - vpd: vapor pressure deficit
 * - lai: leaf area index
 * - Patm: atmospheric air pressure
 * - GSMIN, GSMAX: species-specific parameters for minimum (cuticular) and maximum stomatal conductance
 * 
 * This option is chosen in the setup file by selecting transpirationmethod="potentialtranspiration" 
 * (see @ref psimoptions).
 */
double
ldndc::potential_transpiration(
                        size_t _nd_foliage_layers,
                        double gsmin,
                        double gsmax,
                        double *_lai_fl,
                        lvector_t< double > _vpd_fl,
                        double * _relative_conductance_fl)
{
    double pot_transpiration( 0.0);
    for ( size_t  fl = 0;  fl < _nd_foliage_layers;  ++fl)
    {
        double const conductance( gsmin + _relative_conductance_fl[fl] * (gsmax - gsmin) );
        if (_vpd_fl[fl] > 0.0)
        {
            pot_transpiration += _vpd_fl[fl] * cbm::MBAR_IN_KPA / cbm::PRESS0 * conductance
                                 * cbm::MOL_IN_MMOL * cbm::MH2O * _lai_fl[fl];
        }
    }

    double const M_IN_G( 1.0e-6);
    
    ldndc_assert( cbm::flt_greater_equal_zero( pot_transpiration));
    
    return pot_transpiration * M_IN_G * cbm::SEC_IN_HR;
}

