import os
import numpy as np
import math
from mpl_toolkits.mplot3d import axes3d
import matplotlib.pyplot as plt
import xml.etree.ElementTree as xml
import pathlib

script_dir = os.path.dirname(os.path.abspath(__file__))

parameterfile = script_dir+'/../../../../resources/parameters-species.xml'
xmlparameters = xml.parse( parameterfile)
xmlparameters = xmlparameters.getroot()
xmlparameters = xmlparameters.find( 'speciesparameters')

def get_par( _name, _species) :
    for node in xmlparameters.iter('species'):
        if node.attrib['mnemonic']==_species:
            for elem in node:
                if elem.attrib.get( 'name').lower() == _name.lower() :
                    return ( float(elem.attrib.get( 'value')))

def get_shape( _pfl, _height_canopy_start, _height_max):
    step = 0.01
    height = np.arange(0, _height_max, step)
    shape = np.array([])
    for h in height:
        if (h > _height_canopy_start) and (h < _height_max):
            length = _height_max - _height_canopy_start
            hact = h - _height_canopy_start
            shape = np.append(shape, ((length - hact)/ length) * _pfl**(100.0 * hact / max(length**2, 25)))
        else:
            shape = np.append(shape, 0.0)
    shape *= 1/shape.max()
    return (height, shape)

def get_shape_sigmoid( _r_min, _r_max, _r_width, _r_height, _depth):
    step = 0.01
    height = np.arange(0, _depth+2*step, step)
    shape = np.array([])
    for h in height:
        if h <= _depth:
            shape = np.append(shape, math.exp( -((h - _r_max)**2)/_r_width) / _r_height + _r_min)
        else:
            shape = np.append(shape, 0.0)
    shape *= 1/shape.max()
    return (height, shape)

def get_shape_exponential( _exp, _depth):
    step = 0.01
    height = np.arange(0, _depth+2*step, step)
    shape = np.array([])
    for h in height:
        if h <= _depth:
            shape = np.append(shape, math.exp( -_exp * h))
        else:
            shape = np.append(shape, 0.0)
    shape *= 1/shape.max()
    return (height, shape)


pfl = get_par('PFL', 'piab')
fig = plt.figure( figsize=(7, 4))
ax = fig.add_subplot(1,1,1)
ax.set_title(f"Distribution parameter PFL: {pfl}")

offset = 0.0
for hm, frac in zip([5,10,20,30,40], [0.5, 0.7, 0.7, 0.7, 0.7]):
    hc = frac * hm
    height, shape = get_shape(pfl, hc, hm) 
    ax.plot(shape+offset, height)
    ax.plot([offset for i in range(len(shape))], height, linestyle='dashed', color='black')
    offset += shape.max()+0.2

ax.set_xlabel('Foliage distribution (picea abis)')
ax.set_ylabel('Height [m]')
plt.tick_params( labelbottom = False, bottom = False)
plt.savefig(script_dir+"/../figures/foliage_distribution_grote.png")


psl = get_par('PSL', ':woods:')
fig = plt.figure( figsize=(7, 4))
ax = fig.add_subplot(1,1,1)
ax.set_title(f"Distribution parameter PSL: {psl}")

offset = 0.0
for hm in [0.1,0.2,0.4,1.0,2.0]:
    hc = 0.0
    hm *= 10.0
    height, shape = get_shape(psl, hc, hm) 
    ax.plot(shape+offset, -height/10.0)
    ax.plot([offset for i in range(len(shape))], -height/10.0, linestyle='dashed', color='black')
    offset += shape.max()+0.2

ax.set_xlabel('Fine roots distribution')
ax.set_ylabel('Depth [m]')
plt.tick_params( labelbottom = False, bottom = False)
plt.savefig(script_dir+"/../figures/fineroots_distribution_grote.png")


fig = plt.figure( figsize=(7, 4))
ax = fig.add_subplot(1,1,1)
ax.set_title(f"Sigmoid distribution")

offset = 0.0
for _r_height in [0.01,0.05,0.1,0.5,1.0]:
    scale = 0.1
    height, shape = get_shape_sigmoid(  _r_min=scale*_r_height, _r_max=scale*_r_height, _r_width=0.1*scale*_r_height, _r_height=_r_height, _depth=_r_height) 
    ax.plot(shape+offset, -height)
    ax.plot([offset for i in range(len(shape))], -height, linestyle='dashed', color='black')
    offset += shape.max()+0.2

ax.set_xlabel('Fine roots distribution')
ax.set_ylabel('Depth [m]')
plt.tick_params( labelbottom = False, bottom = False)
plt.savefig(script_dir+"/../figures/fineroots_distribution_sigmoid.png")


fig = plt.figure( figsize=(7, 4))
ax = fig.add_subplot(1,1,1)
ax.set_title(f"Exponential distribution")

offset = 0.0
for _r_height in [0.01,0.05,0.1,0.5,1.0]:
    height, shape = get_shape_exponential(  _exp=3.0, _depth=_r_height) 
    ax.plot(shape+offset, -height)
    ax.plot([offset for i in range(len(shape))], -height, linestyle='dashed', color='black')
    offset += shape.max()+0.2

ax.set_xlabel('Fine roots distribution')
ax.set_ylabel('Depth [m]')
plt.tick_params( labelbottom = False, bottom = False)
plt.savefig(script_dir+"/../figures/fineroots_distribution_exponential.png")