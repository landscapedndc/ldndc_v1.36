/*!
 * @file
 * @author
 *    Ruediger Grote
 *    David Kraus (created on: march 18, 2016)
 */

#ifndef  LD_BIOMASSDISTRIBUTION_H_
#define  LD_BIOMASSDISTRIBUTION_H_

#include  "ld_modelsconfig.h"
#include  "mbe_config.h.inc"

namespace ldndc {


/*!
 * @brief
 *  Lower bounds for number of foliage layers
 */
enum
{
    /* no foliage layers (no vegetation) */
    NO_FOLIAGE_LAYER = 0,
    /* minimum number of supported foliage layers */
    MIN_FOLIAGE_LAYER = 2,
};


/*!
 * @brief
 * Foliage biomass distribution vertically across canopy
 *
 * @param
 *    _fl_cnt_max maximum number of foliage parameters
 * @param
 *    _fl_cnt current actively used number of foliage parameters
 * @param
 *    _ps
 * @param
 *    _h_bottom
 * @param
 *    _h_top
 * @param
 *    _h_fl
 * @param
 *    _fFol_fl
 *
 * @return
 *    no return value
 */
void
canopy_biomass_distribution(
                            size_t const _fl_cnt_max,
                            size_t const _fl_cnt,
                            double const _ps,
                            double const _h_bottom,
                            double const _h_top,
                            double const *_h_fl,
                            double  *_fFol_fl);


/*!
 * @brief
 * Leaf area distribution vertically across the whole canopy
 *
 * @param
 *    _sla_min specific leaf area at canopy top
 * @param
 *    _sla_max specific leaf area at canopy bottom
 * @param
 *    _height_max total plant height
 * @param
 *    _height_min plant height at start of canopy
 * @param
 *    _mFol foliage biomass
 * @param
 *    _fFol_fl foliage biomass distribution
 * @param
 *    _h_fl canopy discretization
 * @param
 *    _lai_fl canopy layer leaf area index
 * @param
 *    _sla_fl canopy layer specific leaf are
 * @param
 *    _foliage_layer_cnt number of canopy layers
 * @param
 *    _foliage_layer_cnt_max maximum number of canopy layers
 *
 * @return
 *    no return value
 */
void
canopy_lai_distribution(
                    double  _sla_min,
                    double  _sla_max,
                    double  _height_max,
                    double  _height_min,
                    double  _mFol,
                    double const *  _fFol_fl,
                    double const *  _h_fl,
                    double *  _lai_fl,
                    double *  _sla_fl,
                    size_t  _foliage_layer_cnt,
                    size_t  _foliage_layer_cnt_max);

} /* namespace ldndc */

#endif  /*  !LD_BIOMASSDISTRIBUTION_H_  */
