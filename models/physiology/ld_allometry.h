/*!
 * @file
 * @brief
 *    Plant related allometry functions, which are globally
 *    used from various vegetation models.
 *
 * @author
 *    - Ruediger Grote
 *    - David Kraus
 *    - Edwin Haas
 *
 * @date
 *    Nov, 2017
 */

#ifndef  LD_ALLOMETRY_H_
#define  LD_ALLOMETRY_H_

#include  "mbe_plant.h"
#include  "state/mbe_state.h"

namespace cbm {
    class io_kcomm_t; }

namespace ldndc {

class LDNDC_API LD_Allometry
{
public:
    LD_Allometry(
                 MoBiLE_State *,
                 cbm::io_kcomm_t *);
    ~LD_Allometry();

    input_class_setup_t const &  m_setup;
    substate_physiology_t &  ph_;
    MoBiLE_PlantVegetation *  m_veg;
    
public:

    /*!
     * @brief
     *  Returns ratio between tree height and diameter at breast height
     *  based on given diameter.
     */
    double
    height_dbh_ratio(
                     double /* diameter */,
                     double /* minimum height-dbh ratio */,
                     double /* maximum height-dbh ratio */,
                     double /* exponent */,
                     double /* stand density factor */);

    /*!
     * @brief
     *  Returns a competition factor due to space affecting
     *  the height to diameter ratio.
     */
    double
    height_competition_factor(
                              MoBiLE_Plant *,
                              MoBiLE_PlantVegetation *);

    /*!
     * @brief
     *    Returns tree stem volume. Details are provided in @ref allometry.
     */
    double
    stem_volume(
                double /* height */,
                double /* diameter at breast height */,
                double /* diameter at ground*/,
                bool /* is coniferous */,
                double /* taper parameter 1 */,
                double /* taper parameter 2 */,
                double /* taper parameter 3 */);
    
    /*!
     * @brief
     *    Returns taper function based tree volume
     *    depending on diameter and height.
     *    Details are provided in @ref allometry.
     */
    double
    taper_volume(
                 bool /* is coniferous */,
                 double /* taper parameter 1 */,
                 double /* taper parameter 2 */,
                 double /* taper parameter 3 */,
                 double /* diameter */,
                 double /* height */);
    
    /*!
     * @brief
     *    Checks species-specific taper parameters.
     *    Details are provided in @ref allometry.
     */
    lerr_t
    check_taper_parameters(
                           bool /* is coniferous */,
                           double & /* taper parameter 1 */,
                           double & /* taper parameter 2 */,
                           double & /* taper parameter 3 */);
    
    /*!
     * @brief
     *    Returns taper function based tree height
     *    depending on volume and diameter.
     *    Details are provided in @ref allometry.
     */
    double
    taper_height_from_diameter(
                               bool /* is coniferous */,
                               double /* taper parameter 1 */,
                               double /* taper parameter 2 */,
                               double /* taper parameter 3 */,
                               double /* diameter */,
                               double /* volume */);

    /*!
     * @brief
     *    Returns taper function based tree diameter
     *    depending on volume and height.
     *    Details are provided in @ref allometry.
     */
    double
    diameter_at_breast_height_from_volume_and_height(
                               bool /* is coniferous */,
                               double /* taper parameter 1 */,
                               double /* taper parameter 2 */,
                               double /* taper parameter 3 */,
                               double /* height */,
                               double /* volume */);


    /*!
     * @brief
     *    Returns taper function based tree diameter
     *    depending on volume and height.
     *    Details are provided in @ref allometry.
     */
    double
    taper_diameter_from_height(
                               bool /* is coniferous */,
                               double /* taper parameter 1 */,
                               double /* taper parameter 2 */,
                               double /* taper parameter 3 */,
                               double /* height */,
                               double /* volume */);

    /*!
     * @brief
     *    Sets upper and lower height of canopy
     */
    lerr_t
    set_canopy_heights(
                   cbm::string_t,
                   MoBiLE_Plant * /* species */,
                   double /* upper canopy height */);

    /*!
     * @brief
     *    Restructure biomass, e.g., height, diameter
     */
    void restructure_vegetation(
                            MoBiLE_Plant *_p,
                            cbm::string_t _crownlength_method,
                            double _hd_competition_factor_old,
                            double _height_fraction,
                            double _dbas_fraction,
                            double _dbh_fraction);
    
    double
    height_from_biomass(
                        bool _coniferous,
                        double _t1,
                        double _t2,
                        double _t3,
                        double /* hd_min */,
                        double /* hd_max */,
                        double _hd_exp,
                        double _vol,
                        double _hd_competition_factor);

    /*!
     * @brief
     *    Returns tree diameter at breast height.
     *    Details are provided in @ref allometry.
     */
    double
    height_from_diameter_at_breast_height(
                                          double /* diameter */,
                                          double /* hd_min */,
                                          double /* hd_max */,
                                          double /* hd_exp */,
                                          double /* stand density factor */);

    /*!
     * @brief
     *    Returns tree diameter at breast height.
     *    Details are provided in @ref allometry.
     */
    double
    diameter_at_breast_height_from_height(
                                  double /* height */,
                                  double /* hd_min */,
                                  double /* hd_max */,
                                  double /* hd_exp */,
                                  double /* stand density factor */);

    /*!
     * @brief
     *    Returns tree diameter at ground.
     *    Details are provided in @ref allometry.
     */
    double
    diameter_at_ground(
                       double /* diameter */,
                       double /* height */,
                       double /* hd_min */,
                       double /* hd_max */,
                       double /* hd_exp */,
                       double /* stand density factor */);

    /*!
     * @brief
     *    Returns effective tree diameter.
     *    Details are provided in @ref allometry.
     */
    static  double
    effective_dbh(
                  double /* tree height */,
                  double /* diamter at breast height */,
                  double /* diamter at ground */);
    
    /*!
     * @brief
     *    Return linear scaling factor for transition
     *    from small to large trees.
     */
    static  double
    hlimit_fraction( double  /* tree height */);

    /*!
     * @brief
     *
     */
    double
    branch_fraction_from_diameter(
                          species::species_group_e _group,
                          double _FBRAF_M,
                          double _FBRAF_Y,
                          double _DIAMMAX,
                          double _dbh,
                          double _dbas);

    /*!
     * @brief
     *
     */
    double
    stand_stemwood(
                   MoBiLE_Plant *);

    /*!
     * @brief
     *
     */
    double
    branch_fraction_from_canopy_volume(
                               MoBiLE_Plant *);

    /*!
     * @brief
     *    Returns crown diameter
     */
    double
    crown_diameter(
                   double _dbh,
                   double _h,
                   double cdr_p1,
                   double cdr_p2,
                   double cdr_p3);

    double
    crown_length_from_crown_diameter(
                 double _h,
                 double _cd,
                 double _cl_p1,
                 double _cl_p2);

    double
    crown_length_from_parameter(
                            double _height,
                            double _dbh,
                            double _href,
                            double _diammax,
                            double _cb);

    /*!
     * @brief
     *    Returns crown diameter ratio assuming open range conditions.
     */
    double
    crown_diameter_ratio_open_range(
                                    double /* dbh */,
                                    double /* height */,
                                    double _cdr_p1,
                                    double _cdr_p2,
                                    double _cdr_p3);
    
    /*!
     * @brief
     *    Returns potential canopy-height depending ground coverage
     *    of selected species assuming 'open range' conditions, i.e.,
     *    no limitations by other species.
     */
    double
    area_fraction_open_range(
                             MoBiLE_Plant * /* species */,
                             size_t /* foliage layer */);
    
    double  tree_crown_ground_coverage(
                                   double /* dbh */,
                                   double /* tree number */,
                                   double /* crown diameter ratio */) const;

    /*!
     * @brief
     *    Returns tree species specific area fraction in [m^2]
     */
    double
    area_fraction_wood(
                   double /* dbh */,
                   double /* tree number */,
                   double /* crown diameter ratio */);

    /*!
     * @brief
     *    Returns crown diameter ratio.
     */
    double
    crown_diameter_ratio(
                         MoBiLE_Plant * /* species */);
    };

double crown_shape_parameter(
                             double  _length,
                             double  lref,
                             double  ps);
} /* namespace ldndc */


#endif  /*  !LD_ALLOMETRY_H_   */

