/*!
 * @file
 * @author
 *  - Ruediger Grote
 *  - David Kraus
 * @date
 *  2001
 */


#include  "physiology/ld_treedyn.h"
#include  <input/species/species.h>

#include  "physiology/ld_sapwoodbiomass.h"
#include  "physiology/ld_biomassdistribution.h"
#include  "physiology/ld_rootsystem.h"

#include  <constants/cbm_const.h>
#include  <math/cbm_math.h>

#include  <input/setup/setup.h>
#include  <input/soillayers/soillayers.h>


TreeDynamics::TreeDynamics( MoBiLE_State *  _state,
                            cbm::io_kcomm_t *_iokcomm) :
                            m_alm( _state, _iokcomm),
                            m_pf( _state, _iokcomm),
                            sc_( _state->get_substate_ref< substate_soilchemistry_t >()),
                            ph_( _state->get_substate_ref< substate_physiology_t >()),
                            m_veg( &_state->vegetation),
                            m_se( _iokcomm->get_input_class< setup::input_class_setup_t >()),
                            m_sl( _iokcomm->get_input_class< soillayers::input_class_soillayers_t >())
{

}


TreeDynamics::~TreeDynamics()
{}


static int  _QueueEvent( void const *  _msg, size_t  _msg_sz, void *  _queue)
{
    ldndc::EventQueue *  queue = static_cast< ldndc::EventQueue * >( _queue);
    if ( !queue)
    { return -1; }

    ldndc::EventAttributes  new_event( NULL /*name not used*/,
                                      (char const *)_msg, _msg_sz);
    queue->push( new_event);
    return 0;
}


lerr_t
TreeDynamics::register_ports( cbm::io_kcomm_t *  _io_kcomm)
{
    CBM_Callback  cb_defoliate;
    cb_defoliate.fn = &_QueueEvent;
    cb_defoliate.data = &DefoliateEvents;
    DefoliateHandle = _io_kcomm->subscribe_event( "defoliate", cb_defoliate);
    if ( !CBM_HandleOk(DefoliateHandle))
    { return  LDNDC_ERR_FAIL; }

    CBM_Callback  cb_regrow;
    cb_regrow.fn = &_QueueEvent;
    cb_regrow.data = &RegrowEvents;
    RegrowHandle = _io_kcomm->subscribe_event( "regrow", cb_regrow);
    if ( !CBM_HandleOk(RegrowHandle))
    { return  LDNDC_ERR_FAIL; }

    CBM_Callback  cb_throw;
    cb_throw.fn = &_QueueEvent;
    cb_throw.data = &ThrowEvents;
    ThrowHandle = _io_kcomm->subscribe_event( "throw", cb_throw);
    if ( !CBM_HandleOk(ThrowHandle))
    { return  LDNDC_ERR_FAIL; }

    CBM_Callback  cb_thin;
    cb_thin.fn = &_QueueEvent;
    cb_thin.data = &ThinEvents;
    ThinHandle = _io_kcomm->subscribe_event( "thin", cb_thin);
    if ( !CBM_HandleOk(ThinHandle))
    { return  LDNDC_ERR_FAIL; }

    CBM_Callback  cb_harvest;
    cb_harvest.fn = &_QueueEvent;
    cb_harvest.data = &HarvestEvents;
    HarvestHandle = _io_kcomm->subscribe_event( "harvest", cb_harvest);
    if ( !CBM_HandleOk(HarvestHandle))
    { return  LDNDC_ERR_FAIL; }

    return LDNDC_ERR_OK;
}


lerr_t
TreeDynamics::unregister_ports( cbm::io_kcomm_t *  _io_kcomm)
{
    _io_kcomm->unsubscribe_event( DefoliateHandle);
    _io_kcomm->unsubscribe_event( RegrowHandle);
    _io_kcomm->unsubscribe_event( ThrowHandle);
    _io_kcomm->unsubscribe_event( ThinHandle);
    _io_kcomm->unsubscribe_event( HarvestHandle);

    return LDNDC_ERR_OK;
}


/*!
 * @page treedyn
 * @tableofcontents
 * @section treedyn_mortality Mortality
 * Tree mortality rate (mort) is assumed to have a fixed component not related to stand density
 * (\f$ mort\_{natural}\f$), e.g. caused by diseases, and a stand density related component 
 * (\f$ mort\_{dens}\f$). The latter only applies if trees are growing across a threshold 
 * of tree canopy coverage. Then, the number of trees is reduced to this threshold.
 * \f[
 *   mort = max(mort\_{dens}, mort\_{natural})
 * \f]
 * \f[
 *   mort\_{natural} = \frac {MORTNORM}{365}
 * \f]
 * \f[
 *   if (relcover > 1) mort\_{dens} = 1.0 - (MORTCROWD \cdot \frac {cdr ^ {2}}{cdr\_{pot} ^ {2}})
 * \f]
 * \f[
 *   relcover = \frac {(dbh \cdot cdr) ^ {2} \cdot PI \cdot 0.25 \cdot tree\_{number} }{10000 \cdot MORTCROWD}
 * \f]
 * with
 * - dbh: diameter at breast height (1.3m) (m)
 * - cdr: ratio between dbh and crown diameter
 * - \f$ cdr\_{pot}\f$: ratio between dbh and potential crown diameter under unconstrained conditions
 * - tree_number: number of trees per hectare
 * - relcover: relation between actual tree cover and potential coverage with no constraints
 * - PI: constant (3.146)
 * - MORTNORM, MORTCROWD: parameters for annual intrinsic mortality rate and maximum tree coverage, respectively
 * 
 */
lerr_t
TreeDynamics::Mortality(
                        MoBiLE_Plant *_p,
                        double &_sWoodAbove,
                        double &_nLitWoodAbove,
                        double *_sWoodBelow_sl,
                        double *_nLitWoodBelow_sl)
{
        /* natural mortality without crowding */
        double const natural_mortality((*_p)->MORTNORM() / 365.0);

        /* potential crown diameter of open range */
        double const cdr_pot( m_alm.crown_diameter_ratio( _p));

        /* increase natural mortality due to crowding; considering previously defined maximum stand density rg 15.10.21 */
        double mortality( natural_mortality);
        double const relcover((_p->dbh * _p->cdr) * (_p->dbh * _p->cdr) * cbm::PI * 0.25 * _p->tree_number / (cbm::M2_IN_HA * (*_p)->MORTCROWD()));
        if ( cbm::flt_greater( relcover, 1.0))
        {
            if ( cbm::flt_greater( cbm::sqr( cdr_pot), (*_p)->MORTCROWD() * cbm::sqr(_p->cdr)))
            {
                mortality = std::max( mortality, 1.0 - ((*_p)->MORTCROWD() * cbm::sqr(_p->cdr) / cbm::sqr(cdr_pot)));
            }
        }

        /* assuming death of trees with average size */
        _p->tree_number *= (1.0 - mortality);

        /* all biomass into litter (no export) */
        double const sCorAbove( mortality * _p->mCor * (1.0 - (*_p)->UGWDF()));
        double const nLitCorAbove( sCorAbove * _p->ncCor);
        double const sCorBelow( mortality * _p->mCor * (*_p)->UGWDF());
        double const nLitCorBelow( sCorBelow * _p->ncCor);

        double const sWoodAbove( mortality * _p->mSap * (1.0 - (*_p)->UGWDF()));
        double const nLitWoodAbove( sWoodAbove * _p->ncSap);
        double const sWoodBelow( mortality * _p->mSap * (*_p)->UGWDF());
        double const nLitWoodBelow( sWoodBelow * _p->ncSap);

        _sWoodAbove += sCorAbove;
        _nLitWoodAbove += nLitCorAbove;
        for ( size_t  l = 0;  l < m_sl->soil_layer_cnt();  l++)
        {
            _sWoodBelow_sl[l] += (sCorBelow * _p->fFrt_sl[l]);
            _nLitWoodBelow_sl[l] += (nLitCorBelow * _p->fFrt_sl[l]);
        }
        _p->mCor -= (sCorAbove + sCorBelow);

        _sWoodAbove += sWoodAbove;
        _nLitWoodAbove += nLitWoodAbove;
        for ( size_t  l = 0;  l < m_sl->soil_layer_cnt();  l++)
        {
            _sWoodBelow_sl[l] += (sWoodBelow * _p->fFrt_sl[l]);
            _nLitWoodBelow_sl[l] += (nLitWoodBelow * _p->fFrt_sl[l]);
        }
        _p->mSap -= (sWoodAbove + sWoodBelow);

    return LDNDC_ERR_OK;
}


/*!
 * @page treedyn
 * @section treedyn_growth Dimensional growth
 * All dimensional growth is calculated on the basis of the new wood volume
 * which consists of sapwood and core(or heart)wood, reduced by fractions for 
 * underground wood parts (coarse roots) and branches. 
 * \f[
 *   vol = \frac {(m\_{sap} + m\_{cor}) \cdot ( 1.0 - UGWDF) \cdot ( 1.0 - f\_{branch})}{DSAP }
 * \f]
 * with:
 * - m_sap:    living sapwood
 * - m_cor:    core (or heart)wood
 * - f_branch: fraction of branch biomass of aboveground woody biomass
 * - UGWDF:    species-specific paramter describing the belowground fraction of total woody biomass
 * - DSAP:     species-specific wood density (kgDW dm-3)
 *
 * The branch fraction is calculated based on a allometric relationship to stem diameter or 
 * based on crown volume (= option 'branchfraction, @ref psimoptions). See @ref veglibs_branch_fraction.
 * The new volume is then distributed into height- and diameter growth considering the a stand-density
 * related optimum height:diameter ratio (@ref veglibs_height_diameter_ratio). 
 */
lerr_t
TreeDynamics::Growth(
                     MoBiLE_Plant *_p,
                     bool _competition_method,
                     cbm::string_t _branchfraction_method,
                     cbm::string_t _crownlength_method,
                     double &_volume_old_vt)
{
    if ( !cbm::flt_greater_zero( _p->stand_volume()))
    { return  LDNDC_ERR_OK; }

    /* crown diameter ratio */
    double const cdr_pot( m_alm.crown_diameter_ratio( _p));

    /* branch fraction */
    double const f_branch_pot( get_branch_fraction( _branchfraction_method, _p));

    /* competition effect on height diameter ratio */
    double const hd_competition_factor( !_competition_method ? 1.0 :
                                          m_alm.height_competition_factor( _p, m_veg));

    /* height and diameters */
    double const height_pot( m_alm.height_from_diameter_at_breast_height(
                                                                         _p->dbh,
                                                                         (*_p)->HD_MIN(),
                                                                         (*_p)->HD_MAX(),
                                                                         (*_p)->HD_EXP(),
                                                                         hd_competition_factor));

    double const tree_volume( cbm::flt_greater_zero( _p->tree_number) ?
                              _p->stand_volume() / _p->tree_number :
                              0.0);

    double const dbh_pot( m_alm.diameter_at_breast_height_from_volume_and_height(
                                                                                 (*_p)->CONIFEROUS(),
                                                                                 (*_p)->TAP_P1(),
                                                                                 (*_p)->TAP_P2(),
                                                                                 (*_p)->TAP_P3(),
                                                                                 _p->height_max,
                                                                                 tree_volume));

    double const dbas_pot( m_alm.diameter_at_ground(
                                                    _p->dbh,
                                                    _p->height_max,
                                                    (*_p)->HD_MIN(),
                                                    (*_p)->HD_MAX(),
                                                    (*_p)->HD_EXP(),
                                                    hd_competition_factor));

    //ensure a minimum positive change factor
    double const change_factor( cbm::bound(
                                           1.0e-9,
                                           (_p->stand_volume() - _volume_old_vt) / _p->stand_volume(),
                                           1.0));

    double const cdr_delta( cbm::flt_greater_zero( _p->cdr) ?
                           cbm::bound( -change_factor * _p->cdr,
                                      cdr_pot - _p->cdr,
                                      change_factor * _p->cdr) :
                           cdr_pot);

    double const f_branch_delta( cbm::flt_greater_zero( _p->f_branch) ?
                                cbm::bound( -change_factor * _p->f_branch,
                                           f_branch_pot - _p->f_branch,
                                           change_factor * _p->f_branch) :
                                f_branch_pot);

    double const height_delta( cbm::flt_greater_zero( _p->height_max) ?
                              cbm::bound( 0.0,
                                         height_pot - _p->height_max,
                                         change_factor * _p->height_max) :
                              height_pot);

    double const dbh_delta( cbm::flt_greater_zero( _p->dbh) ?
                           cbm::bound( 0.0,
                                      dbh_pot - _p->dbh,
                                      change_factor * _p->dbh) :
                           dbh_pot);

    double const dbas_delta( cbm::flt_greater_zero( _p->dbh) ?
                            cbm::bound( 0.0,
                                       dbas_pot - _p->dbas,
                                       change_factor * _p->dbas) :
                            dbas_pot);

    _p->cdr += cdr_delta;
    _p->f_branch += f_branch_delta;
    m_alm.set_canopy_heights( _crownlength_method, _p, _p->height_max + height_delta);
    _p->dbh += dbh_delta;
    _p->dbas += dbas_delta;


    /* rooting depth */
    double const rooting_depth_pot( cbm::bound_max( (*_p)->QHRD() * _p->height_max,
                                                   sc_.depth_sl[m_sl->soil_layer_cnt()-1]));
    double const rooting_depth_delta( cbm::bound( 0.0, rooting_depth_pot - _p->rooting_depth, change_factor * _p->rooting_depth));
    _p->rooting_depth += rooting_depth_delta;

    /* area fraction */
    _p->f_area = m_alm.area_fraction_wood( _p->dbh, _p->tree_number, _p->cdr);

    /* sap-wood foliage area ratio */
    _p->qsfa = sapwood_foliage_area_ratio( _p->height_max, (*_p)->QSF_P1(), (*_p)->QSF_P2());

    _volume_old_vt = _p->stand_volume();
    
    return  LDNDC_ERR_OK;
}


/*!
 * @page treedyn
 * @section treedyn_crowding Sapwood:Foliage relation
 * Assuming a relation between sapwood area and foliage area (@ref  veglibs_sap_fol_area_ratio),
 * which is calculated empirically in dependence on tree height,
 * the sapwood biomass necessary to supply the maximum foliage in the crown can be determined
 * assuming a cone shape decline within the canopy and throughout the rooting zone.
 * This optimum sapwood biomass (mSap_opt) is then devided by current foliage (and bud) masses
 * in order to gain a sapwood/foliage biomass ratio.
 * \f[
 *   qsm = \frac { mSap\_{opt} }{ mFol + mBud }
 * \f]
 * \f[
 *   mSap\_{opt} = 1000 \cdot DSAP \cdot mSap\_{demand} \cdot (height\_{base} + \frac { rooting\_{depth} } { 3.0 } + \frac { height - height\_{base} } { 3.0 } )
 * \f]
 * \f[
 *   mSap\_{demand} = lai\_{max} \cdot qsfa
 * \f]
* with
 * - mFol, mBud: biomass of foliage and structural reserves (buds) (kg m-2)
 * - height_base: crown base height (m)
 * - rooting_depth: rooting depth (m)
 * - lai_pot: potential leaf area calculated from foliage and bud mass weighted by specific leaf area,
 *            derived from canopy length and species-specific parameters (SLAMAX, SLAMIN)
 * - qsfa: relation between sapwood area and foliage area using QSF_P1, and QSF_P2 (@ref sap-fol-ratio)
 * - DSAP: species-specific sapwood density (kg dm3)
 * 
 * The height at crown base is calculated EITHER from a parameterized relationship
 * to tree height (= option 'height, described under 'PSIM') using the
 * species-specific parameters HREF (tree height at which crown base height
 * starts to increase) and CB (relation between crown base height and top  
 * height at a mature state) or based on crown diameter (@ref crown-length)
 * using the species specific parameters (CL_P1 and CL_P2).
 */
lerr_t
TreeDynamics::Crowding(
                       MoBiLE_Plant *_p,
                       cbm::string_t _crownlength_method)
{
    if ( cbm::flt_less_equal( _p->total_biomass(), 0.0))
    {
        _p->tree_number = 0.0;
        _p->dbas = 0.0;
        _p->dbh = 0.0;
        _p->cdr = 0.0;
        _p->rooting_depth = 0.0;
        m_alm.set_canopy_heights( _crownlength_method, _p, 0.0);
    }

    // potential (restricted by stand structure) and maximum (restricted by actual foliage and bud biomass) lai
    _p->lai_pot = 0.0;
    _p->lai_max = 0.0;
    double const mfolopt = m_pf.get_optimum_foliage_biomass_trees(_p);
    for ( size_t  fl = 0;  fl < m_se->canopylayers();  ++fl)
    {
        _p->lai_pot += _p->sla_fl[fl] * mfolopt * _p->fFol_fl[fl];
        _p->lai_max += _p->sla_fl[fl] * (_p->mFol + _p->mBud) * _p->fFol_fl[fl];
    }

    // sapwood/foliage ratio
    _p->qsfm = get_sapwood_foliage_ratio( _p);

    return  LDNDC_ERR_OK;
}


/*!
 * @page treedyn
 * @section treedyn_distribution Biomass and leaf area distribution
 * 
 * Foliage biomass throughout the foliated canopy and fine root biomass
 * throughout the rooted soil are empirically distributed using the
 * same function that depends on distribution length (either canopy depth
 * or rooting depth) with different parameters @cite grote:2003a,
 * @ref veglibs_biomass_distribution. 
 * 
 * Leaf area throughout the foliated canopy follows foliage biomass distribution
 * but is weighted by specific leaf area per canopy layer. The latter varies
 * linearly between a predefined minimum (at the top of the canopy) and maximum
 * value (SLAMIN, SLAMAX).
 */
lerr_t
TreeDynamics::OnStructureChange(
                                MoBiLE_Plant *_p,
                                RootSystemDNDC *_rs)
{
    lerr_t  rc = LDNDC_ERR_OK;

    rc = UpdateBiomass( _p);
    if ( rc)
    { return  LDNDC_ERR_FAIL; }

    rc = MassDistribution( _p, _rs);
    if ( rc)
    { return  LDNDC_ERR_FAIL; }

    rc = LaiDistribution( _p);
    if ( rc)
    { return  LDNDC_ERR_FAIL; }

    return  rc;
}


/*!
 */
lerr_t
TreeDynamics::UpdateBiomass(
                            MoBiLE_Plant *_p)
{
    double mFol_old(  cbm::sum( _p->mFol_na, _p->nb_ageclasses()));
    if ( cbm::flt_greater_zero( mFol_old))
    {
        for ( size_t  na = 0;  na < _p->nb_ageclasses();  ++na)
        {
            double const fna( _p->mFol_na[na] / mFol_old);
            _p->mFol_na[na] = (_p->mFol * fna);
        }
    }

    return  LDNDC_ERR_OK;
}


/*!
 *
 */
lerr_t
TreeDynamics::MassDistribution(
                               MoBiLE_Plant *_p,
                               RootSystemDNDC *)
{
    // Canopy distribution:
    ph_.update_canopy_layers_height( m_veg);
    m_pf.update_vertical_biomass_distribution( _p);

    // Fine roots distribution:
    fineroots_biomass_distribution(
                                   m_sl->soil_layers_in_litter_cnt(),
                                   m_sl->soil_layer_cnt(),
                                   (*_p)->PSL(), _p->rooting_depth,
                                   _p->fFrt_sl, &sc_.h_sl[0]);
    
//    if ( (_rs != NULL) && (*_p)->ROOTS_ENVIRONMENTAL())
//    {
//        _rs->update_roots_dynamic( FTS_TOT_, root_q_vt_[_vt->slot], _vt, deltamassFrt);
//    }

    return  LDNDC_ERR_OK;
}


/*!
 *
 */
lerr_t
TreeDynamics::LaiDistribution(
                              MoBiLE_Plant *_p)
{
    canopy_lai_distribution( (*_p)->SLAMIN(), (*_p)->SLAMAX(),
                     _p->height_max, _p->height_at_canopy_start,
                     _p->mFol, _p->fFol_fl, &ph_.h_fl[0], _p->lai_fl, _p->sla_fl,
                     _p->nb_foliagelayers(), m_se->canopylayers());

    return  LDNDC_ERR_OK;
}


/*!
 * @author
 *      David Kraus, Thomas Dirnboeck
 *
 * @brief
 *      calculates change of grass coverage depending on tree cover.
 *      speed of change depends on the species parameter FOLRELGROMAX
 * @comment
 *      calculation is based on an empirical function derived from observations
 *      in the limestone national parc, austria.
 *
 */
lerr_t
TreeDynamics::update_grass_coverage( MoBiLE_Plant *_p)
{
    //explicitly considered only for grass species having FOLRELGROMAX > 0.0
    if ( cbm::flt_greater_zero( (*_p)->FOLRELGROMAX()))
    {
        double tree_cover( 0.0);
        for ( TreeIterator w = this->m_veg->groupbegin< species::wood >();
             w != this->m_veg->groupend< species::wood >();  ++w)
        {
            if ( (*w)->f_area > tree_cover)
            {
                tree_cover = (*w)->f_area;
            }
        }

        //tree cover in percentage
        tree_cover *= 100.0;

        //potential grass cover with respect to given tree cover
        double const f_area_pot( 0.91 + 0.0016 * tree_cover - 0.000067 * pow( tree_cover, 2.0));

        //change grass cover gradually depending on FOLRELGROMAX
        _p->f_area += (*_p)->FOLRELGROMAX() * (f_area_pot - _p->f_area);
        _p->f_area = cbm::bound(0.01, _p->f_area, 1.0);
    }

    return LDNDC_ERR_OK;
}


/*!
 * @page treedyn
 * @section treedyn_events Forest disturbances
 * - Defoliate
 * - Regrowth
 * - Throwing
 * - Thinning
 * - Harvest
 */
int
TreeDynamics::HandleEvents(
                           cbm::io_kcomm_t *_io_kcomm,
                           cbm::string_t _crownlength_method,
                           cbm::string_t _branchfraction_method,
                           bool _competition_method,
                           double &_sFol_vt,
                           double &_nLitFol_vt,
                           double &_sBud_vt,
                           double &_nLitBud_vt,
                           double &_sWoodAbove_vt,
                           double &_nLitWoodAbove_vt,
                           double *_sWoodBelow_vtsl,
                           double *_nLitWoodBelow_vtsl,
                           double *_sFrt_vtsl,
                           double *_nLitFrt_vtsl)
{
    /* counter for handled events */
    int  nb_events = 0;

    int  nb_events_defoliate = HandleEventsDefoliate(
                                                     _sFol_vt,
                                                     _nLitFol_vt);
    if ( nb_events_defoliate == -1)
    { return -1; }
    nb_events += nb_events_defoliate;


    int  nb_events_regrow = HandleEventsRegrow(
                                               _crownlength_method,
                                               _branchfraction_method,
                                               _sFol_vt, _nLitFol_vt,
                                               _sBud_vt, _nLitBud_vt,
                                               _sWoodAbove_vt, _nLitWoodAbove_vt);
    if ( nb_events_regrow == -1)
    { return -1; }
    nb_events += nb_events_regrow;


    int  nb_events_throw = HandleEventsThrow(
                                             _io_kcomm,
                                             _sFol_vt, _nLitFol_vt,
                                             _sBud_vt, _nLitBud_vt,
                                             _sWoodAbove_vt, _nLitWoodAbove_vt,
                                             _sWoodBelow_vtsl, _nLitWoodBelow_vtsl,
                                             _sFrt_vtsl, _nLitFrt_vtsl);
    if ( nb_events_throw == -1)
    { return -1; }
    nb_events += nb_events_throw;

    int  nb_events_thin = HandleEventsThin(
                                           _io_kcomm,
                                           _crownlength_method, _competition_method,
                                           _sFol_vt, _nLitFol_vt,
                                           _sBud_vt, _nLitBud_vt,
                                           _sWoodAbove_vt, _nLitWoodAbove_vt,
                                           _sWoodBelow_vtsl, _nLitWoodBelow_vtsl,
                                           _sFrt_vtsl, _nLitFrt_vtsl);
    if ( nb_events_thin == -1)
    { return -1; }
    nb_events += nb_events_thin;


    int  nb_events_harvest = HandleEventsHarvest( _io_kcomm,
                                                  _sFol_vt, _nLitFol_vt, _sBud_vt,
                                                  _sWoodAbove_vt, _nLitWoodAbove_vt,
                                                  _sWoodBelow_vtsl, _nLitWoodBelow_vtsl,
                                                  _sFrt_vtsl, _nLitFrt_vtsl);

    if ( nb_events_harvest == -1)
    { return -1; }
    nb_events += nb_events_harvest;

    return  nb_events;
}


/*!
 *
 */
double
TreeDynamics::get_branch_fraction(
                                  cbm::string_t _branchfraction_method,
                                  MoBiLE_Plant *_p)
{
    /* branch fraction */
    if ( _branchfraction_method == "diameter")
    {
        return m_alm.branch_fraction_from_diameter(
                                               _p->groupId(),
                                               (*_p)->FBRAF_M(),
                                               (*_p)->FBRAF_Y(),
                                               (*_p)->DIAMMAX(),
                                               _p->dbh,
                                               _p->dbas);
    }
    else
    {
        return m_alm.branch_fraction_from_canopy_volume( _p);
    }
}


/*!
 *
 */
int
TreeDynamics::HandleEventsDefoliate(
                                    double &_sFol_vt,
                                    double &_nLitFol_vt)
{
    int  nb_events = 0;
    while ( DefoliateEvents)
    {
        EventAttributes  ev = DefoliateEvents.pop();

        MoBiLE_Plant *  tree = NULL;
        if ( cbm::is_equal( "*", ev.get( "/name", "*")))
        { /*TODO  strategy?*/ }
        else
        {
            for ( TreeIterator w = m_veg->groupbegin< species::wood >();
                 w != m_veg->groupend< species::wood >();  ++w)
            {
                if ( IS_SPECIE( (*w)->cname(), ev.get( "/name", "?")))
                {
                    tree = *w;
                    break;
                }
            }
        }

        if ( tree)
        {
            EventDefoliateData  defoliatedata;
            defoliatedata.reduction_volume = ev.get( "/reduction-volume", 0.0);
            defoliatedata.export_foliage = ev.get( "/export-foliage", false);

            lerr_t  rc_defoliate = HandleEventDefoliate( tree, &defoliatedata, _sFol_vt, _nLitFol_vt);
            if ( rc_defoliate)
            { return  -1; }

            nb_events += 1;
        }
        else
        { LOGWARN( "Event ", "defoliate", ":",
                   " No species found for \"",ev.get( "/name", "?"),"\""); }
    }

    return  nb_events;
}


/*!
 *
 */
int
TreeDynamics::HandleEventsRegrow(
                                 cbm::string_t _crownlength_method,
                                 cbm::string_t _branchfraction_method,
                                 double &_sFol_vt,
                                 double &_nLitFol_vt,
                                 double &_sBud_vt,
                                 double &_nLitBud_vt,
                                 double &_sWoodAbove_vt,
                                 double &_nLitWoodAbove_vt)
{
    int  nb_events = 0;

    while ( RegrowEvents)
    {
        EventAttributes  ev = RegrowEvents.pop();

        MoBiLE_Plant *  tree = NULL;
        for ( TreeIterator w = m_veg->groupbegin< species::wood >();
             w != m_veg->groupend< species::wood >();  ++w)
        {
            if ( IS_SPECIE( (*w)->cname(), ev.get( "/name", "?")))
            {
                tree = *w;
                break;
            }
        }

        if ( tree)
        {
            EventRegrowData  regrowdata;
            regrowdata.export_agb = ev.get( "/fraction-export-aboveground-biomass", 0.0);
            regrowdata.tree_number = ev.get( "/tree-number", invalid_dbl);
            regrowdata.tree_number_resize_factor = ev.get( "/tree-number-resize-factor", invalid_dbl);
            regrowdata.height_max = ev.get( "/maximum-height", 0.4);

            lerr_t  rc_regrow = HandleEventRegrow( tree, &regrowdata,
                                                  _crownlength_method,
                                                  _branchfraction_method,
                                                  _sFol_vt, _nLitFol_vt,
                                                  _sBud_vt, _nLitBud_vt,
                                                  _sWoodAbove_vt, _nLitWoodAbove_vt);
            if ( rc_regrow)
            { return  -1; }

            nb_events += 1;
        }
        else
        { LOGWARN( "Event ", "regrow", ":",
                   " No species found for \"", ev.get( "/name", "?"),"\""); }
    }

    return  nb_events;
}

/*!
 * @brief
 *  Default: High trees are preferentially affected by throw event.
 */
static double  TreeDynamics_ThrowGenericScore( MoBiLE_Plant *  _plant)
{
    double  score = _plant->height_max;
    return  score;
}

/*!
 * @brief
 *  Wind: High trees are preferentially affected by throw event.
 */
static double  TreeDynamics_ThrowWindScore( MoBiLE_Plant *  _plant)
{
    double  score = _plant->height_max;
    return  score;
}

/*!
 * @brief
 *  Snow: High and coniferous trees are preferentially affected by throw event.
 */
static double  TreeDynamics_ThrowSnowScore( MoBiLE_Plant *  _plant)
{
    double  score = _plant->height_max;
    score *= _plant->parameters()->CONIFEROUS() ? 1.0 : 0.25;
    return  score;
}

/*!
 * @brief
 *  Bark beetle: High (old) and coniferous (only) trees are preferentially affected by throw event.
 */
static double  TreeDynamics_ThrowBarkbeetleScore( MoBiLE_Plant *  _plant)
{
    double  score = _plant->parameters()->CONIFEROUS() ? _plant->height_max : -1.0;
    return  score;
}

typedef  double (*ThrowScore)( MoBiLE_Plant *);
static MoBiLE_Plant *  TreeDynamics_SelectTree( MoBiLE_PlantVegetation *  _veg,
                                                      ThrowScore  _throw_score)
{
    MoBiLE_Plant *  tree = NULL;
    for ( TreeIterator w = _veg->groupbegin< species::wood >();
         w != _veg->groupend< species::wood >();  ++w)
    {
        // todo: find solution for exception
        //        std::string name = (*w)->cname();
        //        if (name.find("regrow") != std::string::npos)
        //        {
        //            continue;
        //        }

        if ((*w)->tree_number > 0)
        {
            if ( !tree && ( _throw_score( *w) > 0.0))
            { tree = *w; }
            else if ( tree && ( _throw_score( tree) < _throw_score( *w)))
            { tree = *w; }
        }
    }

    return tree;
}

ThrowScore  TreeDynamics_FindThrowScore( cbm::string_t const &  _reason)
{
    ThrowScore  throw_score = NULL;
    if ( _reason == "wind")
    { throw_score = &TreeDynamics_ThrowWindScore; }
    else if ( _reason == "snow")
    { throw_score = &TreeDynamics_ThrowSnowScore; }
    else if ( _reason == "barkbeetle")
    { throw_score = &TreeDynamics_ThrowBarkbeetleScore; }
    else
    { throw_score = &TreeDynamics_ThrowGenericScore; }
    return  throw_score;
}

int
TreeDynamics::HandleEventsThrow(
                                cbm::io_kcomm_t *_io_kcomm,
                                double &_sFol_vt,
                                double &_nLitFol_vt,
                                double &_sBud_vt,
                                double &_nLitBud_vt,
                                double &_sWoodAbove_vt,
                                double &_nLitWoodAbove_vt,
                                double *_sWoodBelow_vtsl,
                                double *_nLitWoodBelow_vtsl,
                                double *_sFrt_vtsl,
                                double *_nLitFrt_vtsl)
{
    /* return number of throwing events per time step */
    int  nb_events = 0;

    while ( ThrowEvents)
    {
        /* continue dynamic throw event until target reduction volume is met */
        bool  throw_continue( true);
        bool  exhausted( false);

        EventAttributes  ev = ThrowEvents.pop();

        cbm::string_t  reason = ev.get( "/reason", "");
        ThrowScore  throw_score = TreeDynamics_FindThrowScore( reason);

        double  total_reduction_volume = ev.get( "/reduction-volume", 0.0);
        double  reduction_volume = 0.0;

        bool const  reduction_volume_is_absolute = ev.get( "/reduction-volume-is-absolute", false);

        while ( throw_continue)
        {
            MoBiLE_Plant *  tree = NULL;
            if ( cbm::is_equal( "*", ev.get( "/name", "*")))
            {
                /* find most likely tree species to be affected by event */
                tree = TreeDynamics_SelectTree( m_veg, throw_score);

                if ( tree && cbm::flt_greater_zero( tree->stand_volume()))
                {
                    /* interpret as absolute volume */
                    if ( reduction_volume_is_absolute)
                    {
                        reduction_volume = cbm::bound( 0.0, total_reduction_volume / tree->stand_volume(), 1.0);
                        total_reduction_volume -= tree->stand_volume() * reduction_volume;

                        if ( !cbm::flt_greater_zero( total_reduction_volume))
                        {
                            throw_continue = false;
                        }
                    }
                    else
                    {
                        LOGERROR( "Dynamic 'throw' event does not support reduction volume in %"); return -1;
                        throw_continue = false;
                    }
                }
                else
                {
                    LOGWARN( "Event ", "throw", ":", " Exhausted all targeted living biomass");
                    throw_continue = false;
                    exhausted = true;
                }
            }
            else
            {
                for ( TreeIterator w = m_veg->groupbegin< species::wood >();
                     w != m_veg->groupend< species::wood >();  ++w)
                {
                    if ( IS_SPECIE( (*w)->cname(), ev.get( "/name", "?")))
                    {
                        tree = *w;
                        if ( reduction_volume_is_absolute)
                        {
                            reduction_volume = cbm::bound_max( total_reduction_volume / tree->stand_volume(), 1.0);
                        }
                        else
                        {
                            reduction_volume = total_reduction_volume;
                        }
                        total_reduction_volume = 0.0;
                        break;
                    }
                }
                throw_continue = false;
            }

            if ( tree)
            {
                EventThrowData  throwdata;
                throwdata.reduction_volume = reduction_volume;

                lerr_t  rc_throw = HandleEventThrow(
                                                    _io_kcomm, tree, &throwdata,
                                                    _sFol_vt, _nLitFol_vt,
                                                    _sBud_vt, _nLitBud_vt,
                                                    _sWoodAbove_vt, _nLitWoodAbove_vt,
                                                    _sWoodBelow_vtsl, _nLitWoodBelow_vtsl,
                                                    _sFrt_vtsl, _nLitFrt_vtsl);
                if ( rc_throw)
                { return  -1; }

                nb_events += 1;
            }
            else
            {
                if ( !exhausted)
                {
                    LOGWARN( "Event ", "throw: No species found for \"", ev.get( "/name", "?"),"\"");
                }
            }
        }
    }

    return  nb_events;
}


int
TreeDynamics::HandleEventsThin(
                               cbm::io_kcomm_t *_io_kcomm,
                               cbm::string_t _crownlength_method,
                               bool _competition_method,
                               double &_sFol_vt,
                               double &_nLitFol_vt,
                               double &_sBud_vt,
                               double &_nLitBud_vt,
                               double &_sWoodAbove_vt,
                               double &_nLitWoodAbove_vt,
                               double *_sWoodBelow_vtsl,
                               double *_nLitWoodBelow_vtsl,
                               double *_sFrt_vtsl,
                               double *_nLitFrt_vtsl)
{
    int  nb_events = 0;

    while ( ThinEvents)
    {
        EventAttributes  ev = ThinEvents.pop();

        MoBiLE_Plant *  tree = NULL;
        for ( TreeIterator w = m_veg->groupbegin< species::wood >();
             w != m_veg->groupend< species::wood >();  ++w)
        {
            if ( IS_SPECIE( (*w)->cname(), ev.get( "/name", "?")))
            {
                tree = *w;
                break;
            }
        }

        if ( tree)
        {
            EventThinData  thindata;
            thindata.reduction_number = ev.get( "/reduction-number", 0.0);
            thindata.reduction_volume = ev.get( "/reduction-volume", 0.0);
            thindata.export_corewood = ev.get( "/export-corewood", false);
            thindata.export_sapwood = ev.get( "/export-sapwood", false);

            lerr_t  rc_thin = HandleEventThin(
                                              _io_kcomm, tree, &thindata,
                                              _crownlength_method, _competition_method,
                                              _sFol_vt, _nLitFol_vt,
                                              _sBud_vt, _nLitBud_vt,
                                              _sWoodAbove_vt, _nLitWoodAbove_vt,
                                              _sWoodBelow_vtsl, _nLitWoodBelow_vtsl,
                                              _sFrt_vtsl, _nLitFrt_vtsl);
            if ( rc_thin)
            { return  -1; }

            nb_events += 1;
        }
        else
        { LOGWARN( "Event ", "thin", ":",
                   " No species found for \"", ev.get( "/name", "?"),"\""); }
    }

    return  nb_events;
}



int
TreeDynamics::HandleEventsHarvest(
                                  cbm::io_kcomm_t *_io_kcomm,
                                  double &_sFol_vt,
                                  double &_nLitFol_vt,
                                  double &_sBud_vt,
                                  double &_sWoodAbove_vt,
                                  double &_nLitWoodAbove_vt,
                                  double *_sWoodBelow_vtsl,
                                  double *_nLitWoodBelow_vtsl,
                                  double *_sFrt_vtsl,
                                  double *_nLitFrt_vtsl)
{
    int  nb_events = 0;

    while ( HarvestEvents)
    {
        EventAttributes  ev = HarvestEvents.pop();

        MoBiLE_Plant *  tree = NULL;
        for ( TreeIterator w = m_veg->groupbegin< species::wood >();
             w != m_veg->groupend< species::wood >();  ++w)
        {
            if ( IS_SPECIE( (*w)->cname(), ev.get( "/name", "?")))
            {
                tree = *w;
                break;
            }
            ldndc_assert( 0);
        }

        if ( tree)
        {
            EventHarvestData  harvestdata;
            harvestdata.export_branch_wood = ev.get( "/fraction-export-branchwood", 0.0);
            harvestdata.export_root_wood = ev.get( "/fraction-export-rootwood", 0.0);
            harvestdata.export_stem_wood = ev.get( "/fraction-export-stemwood", 0.0);

            lerr_t  rc_harvest = HandleEventHarvest( _io_kcomm, tree, &harvestdata,
                                                     _sFol_vt, _nLitFol_vt, _sBud_vt,
                                                     _sWoodAbove_vt, _nLitWoodAbove_vt,
                                                     _sWoodBelow_vtsl, _nLitWoodBelow_vtsl,
                                                     _sFrt_vtsl, _nLitFrt_vtsl);
            if ( rc_harvest)
            { return  -1; }

            nb_events += 1;
        }
        else
        { LOGWARN( "Event ", "harvest", ":",
                   " No species found for \"", ev.get( "/name", "?"),"\""); }
    }

    return  nb_events;
}




lerr_t  ldndc::TreeDynamics::HandleEventDefoliate(
                                                  MoBiLE_Plant *  _p, TreeDynamics::EventDefoliateData const *  _ev,
                                                  double &_sFol_vt,
                                                  double &_nLitFol_vt)
{
    double const sFolE( _ev->reduction_volume * _p->mFol);
    double const nLitFolE( sFolE * _p->ncFol);

    if ( !_ev->export_foliage)      // for insects
    {
        _sFol_vt += sFolE;
        _nLitFol_vt += nLitFolE;
    }

    /* update biomass */
    _p->mFol -= sFolE;
    _p->mFolMin *= (1.0 - _ev->reduction_volume);
    _p->mFolMax *= (1.0 - _ev->reduction_volume);
    if ( !cbm::flt_greater_zero( _p->mFol))
    {
        _p->mFol = 0.0;
    }

    return  LDNDC_ERR_OK;
}



lerr_t  ldndc::TreeDynamics::HandleEventHarvest(
                                                cbm::io_kcomm_t *_io_kcomm,
                                                MoBiLE_Plant *  _p,
                                                TreeDynamics::EventHarvestData const *  _ev,
                                                double &_sFol_vt,
                                                double &_nLitFol_vt,
                                                double &_sBud_vt,
                                                double &_sWoodAbove_vt,
                                                double &_nLitWoodAbove_vt,
                                                double *_sWoodBelow_vtsl,
                                                double *_nLitWoodBelow_vtsl,
                                                double *_sFrt_vtsl,
                                                double *_nLitFrt_vtsl)
{
    /* foliage (all left at the site) */
    double const s_fol_total( _p->mFol);
    double const s_fol_export( 0.0);
    
    double const n_fol_total( _p->mFol * _p->ncFol);
    double const n_fol_export( 0.0);
    
    _sFol_vt += s_fol_total;
    _nLitFol_vt += n_fol_total;


    /* buds (assumed to be located in aboveground wood - export to the same fractions)*/
    double const s_bud_branch( _p->mBud * _p->f_branch);
    double const s_bud_stem( _p->mBud * (1.0 - _p->f_branch));
    double const s_bud_above_litter( (1.0 - _ev->export_stem_wood) * s_bud_stem +
                                     (1.0 - _ev->export_branch_wood) * s_bud_branch);
    double const s_bud_total( s_bud_branch + s_bud_stem);
    double const s_bud_export( cbm::bound_min( 0.0, s_bud_total - s_bud_above_litter));
    
    double const n_bud_total( s_bud_total * _p->ncBud);
    double const n_bud_export( s_bud_export * _p->ncBud);
    
    _sBud_vt += s_bud_above_litter;
    _nLitWoodAbove_vt += s_bud_above_litter * _p->ncBud;


    /* sap wood (exported according to defined fractions)*/
    double const s_lst_branch( _p->mSap * (1.0 - (*_p)->UGWDF()) * _p->f_branch);
    double const s_lst_stem(   _p->mSap * (1.0 - (*_p)->UGWDF()) * (1.0 - _p->f_branch));
    double const s_lst_above_total( s_lst_branch + s_lst_stem);
    double const s_lst_below_total( _p->mSap * (*_p)->UGWDF());
    double const s_lst_above_litter( (1.0 - _ev->export_stem_wood) * s_lst_stem +
                                     (1.0 - _ev->export_branch_wood) * s_lst_branch);
    double const s_lst_below_litter( (1.0 - _ev->export_root_wood) * s_lst_below_total);
    double const s_lst_above_export( cbm::bound_min( 0.0, s_lst_above_total - s_lst_above_litter));
    double const s_lst_below_export( cbm::bound_min( 0.0, s_lst_below_total - s_lst_below_litter));
    
    double const n_lst_above_total( s_lst_above_total * _p->ncSap);
    double const n_lst_below_total( s_lst_below_total * _p->ncSap);
    double const n_lst_above_litter( s_lst_above_litter * _p->ncSap);
    double const n_lst_below_litter( s_lst_below_litter * _p->ncSap);
    double const n_lst_above_export( s_lst_above_export * _p->ncSap);
    double const n_lst_below_export( s_lst_below_export * _p->ncSap);
    
    _sWoodAbove_vt += s_lst_above_litter;
    _nLitWoodAbove_vt += n_lst_above_litter;
    for ( size_t  l = 0;  l < m_sl->soil_layer_cnt();  l++)
    {
        _sWoodBelow_vtsl[l]    += s_lst_below_litter * _p->fFrt_sl[l];
        _nLitWoodBelow_vtsl[l] += n_lst_below_litter * _p->fFrt_sl[l];
    }


    /* core wood (exported according to defined fractions)*/
    double const s_dst_above_total( _p->mCor * (1.0 - (*_p)->UGWDF()));
    double const s_dst_below_total( _p->mCor * (*_p)->UGWDF());
    double const s_dst_above_litter( (1.0 - _ev->export_stem_wood) * s_dst_above_total);
    double const s_dst_below_litter( (1.0 - _ev->export_root_wood) * s_dst_below_total);
    double const s_dst_above_export( cbm::bound_min( 0.0, s_dst_above_total - s_dst_above_litter));
    double const s_dst_below_export( cbm::bound_min( 0.0, s_dst_below_total - s_dst_below_litter));

    double const n_dst_above_total( s_dst_above_total * _p->ncCor);
    double const n_dst_below_total( s_dst_below_total * _p->ncCor);
    double const n_dst_above_litter( s_dst_above_litter * _p->ncCor);
    double const n_dst_below_litter( s_dst_below_litter * _p->ncCor);
    double const n_dst_above_export( s_dst_above_export * _p->ncCor);
    double const n_dst_below_export( s_dst_below_export * _p->ncCor);
    
    _sWoodAbove_vt += s_dst_above_litter;
    _nLitWoodAbove_vt += n_dst_above_litter;
    for ( size_t  l = 0;  l < m_sl->soil_layer_cnt();  l++)
    {
        _sWoodBelow_vtsl[l]    += s_dst_below_litter * _p->fFrt_sl[l];
        _nLitWoodBelow_vtsl[l] += n_dst_below_litter * _p->fFrt_sl[l];
    }


    /* fine roots (all left at the site within the respective soil layer)*/

    double const s_frt_total( _p->mFrt);
    double const s_frt_export( 0.0);
    
    double const n_frt_total( _p->mFrt * _p->ncFrt);
    double const n_frt_export( 0.0);

    for ( size_t  l = 0;  l < m_sl->soil_layer_cnt();  l++)
    {
        _sFrt_vtsl[l]    += s_frt_total * _p->fFrt_sl[l];
        _nLitFrt_vtsl[l] += n_frt_total * _p->fFrt_sl[l];
    }

    _p->reset();

    ph_.update_canopy_layers_height( m_veg);


    cbm::state_scratch_t *  mcom = _io_kcomm->get_scratch();
    std::string  mcom_key;

    char const *  species_name = _p->cname();

    cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:c_fol", species_name);
    mcom->set( mcom_key.c_str(), s_fol_total * cbm::CCDM);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:c_fol_export", species_name);
    mcom->set( mcom_key.c_str(), s_fol_export * cbm::CCDM);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:c_fru", species_name);
    mcom->set( mcom_key.c_str(), s_bud_total * cbm::CCDM);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:c_fru_export", species_name);
    mcom->set( mcom_key.c_str(), s_bud_export * cbm::CCDM);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:c_frt", species_name);
    mcom->set( mcom_key.c_str(), s_frt_total * cbm::CCDM);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:c_frt_export", species_name);
    mcom->set( mcom_key.c_str(), s_frt_export * cbm::CCDM);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:c_lst_below", species_name);
    mcom->set( mcom_key.c_str(), s_lst_below_total * cbm::CCDM);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:c_lst_below_export", species_name);
    mcom->set( mcom_key.c_str(), s_lst_below_export * cbm::CCDM);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:c_lst_above", species_name);
    mcom->set( mcom_key.c_str(), s_lst_above_total * cbm::CCDM);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:c_lst_above_export", species_name);
    mcom->set( mcom_key.c_str(), s_lst_above_export * cbm::CCDM);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:c_dst_below", species_name);
    mcom->set( mcom_key.c_str(), s_dst_below_total * cbm::CCDM);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:c_dst_below_export", species_name);
    mcom->set( mcom_key.c_str(), s_dst_below_export * cbm::CCDM);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:c_dst_above", species_name);
    mcom->set( mcom_key.c_str(), s_dst_above_total * cbm::CCDM);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:c_dst_above_export", species_name);
    mcom->set( mcom_key.c_str(), s_dst_above_export * cbm::CCDM);



    cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:n_fol", species_name);
    mcom->set( mcom_key.c_str(), n_fol_total);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:n_fol_export", species_name);
    mcom->set( mcom_key.c_str(), n_fol_export);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:n_fru", species_name);
    mcom->set( mcom_key.c_str(), n_bud_total);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:n_fru_export", species_name);
    mcom->set( mcom_key.c_str(), n_bud_export);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:n_frt", species_name);
    mcom->set( mcom_key.c_str(), n_frt_total);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:n_frt_export", species_name);
    mcom->set( mcom_key.c_str(), n_frt_export);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:n_lst_below", species_name);
    mcom->set( mcom_key.c_str(), n_lst_below_total);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:n_lst_below_export", species_name);
    mcom->set( mcom_key.c_str(), n_lst_below_export);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:n_lst_above", species_name);
    mcom->set( mcom_key.c_str(), n_lst_above_total);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:n_lst_above_export", species_name);
    mcom->set( mcom_key.c_str(), n_lst_above_export);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:n_dst_below", species_name);
    mcom->set( mcom_key.c_str(), n_dst_below_total);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:n_dst_below_export", species_name);
    mcom->set( mcom_key.c_str(), n_dst_below_export);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:n_dst_above", species_name);
    mcom->set( mcom_key.c_str(), n_dst_above_total);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:n_dst_above_export", species_name);
    mcom->set( mcom_key.c_str(), n_dst_above_export);

    return  LDNDC_ERR_OK;
}



/*!
 * @todo:
 *  Check what really should happen during regrow event.
 */
lerr_t  ldndc::TreeDynamics::HandleEventRegrow(
                                               MoBiLE_Plant *  _p,
                                               TreeDynamics::EventRegrowData const *  _ev,
                                               cbm::string_t _crownlength_method,
                                               cbm::string_t _branchfraction_method,
                                               double &_sFol_vt,
                                               double &_nLitFol_vt,
                                               double &_sBud_vt,
                                               double &_nLitBud_vt,
                                               double &_sWoodAbove_vt,
                                               double &_nLitWoodAbove_vt)
{
    double const left_over_fraction = cbm::bound_min( 0.0, ( 1.0 - _ev->export_agb));

    // biomass updates (export_aboveground_biomass defines removal of aboveground biomass only. all roots stay in the system)
    double const s_fol( left_over_fraction * _p->mFol);
    _sFol_vt += s_fol;
    _nLitFol_vt += s_fol * _p->ncFol;
    _p->mFol -= s_fol;
    if ( ! cbm::flt_greater_zero( _p->mFol))
    {
        _p->mFol = 0.0;
    }

    double const s_bud( left_over_fraction * _p->mBud);
    _sBud_vt += s_bud;
    _nLitBud_vt += s_bud * _p->ncBud;

    /*!
     * @todo:
     *  implement in a mass-balanced way
     */
    _p->mBud = m_pf.get_optimum_foliage_biomass_trees( _p);
    _p->mBudStart = _p->mBud;

    double const s_sap_above( left_over_fraction * _p->mSap * (1.0 - (*_p)->UGWDF()));
    _sWoodAbove_vt += s_sap_above;
    _nLitWoodAbove_vt += s_sap_above * _p->ncSap;
    _p->mSap -= s_sap_above;
    if ( ! cbm::flt_greater_zero( _p->mSap))
    {
        _p->mSap = 0.0;
    }

    double const s_cor_above( left_over_fraction * _p->mCor * (1.0 - (*_p)->UGWDF()));
    _sWoodAbove_vt += s_cor_above;
    _nLitWoodAbove_vt += s_cor_above * _p->ncCor;
    _p->mCor -= s_cor_above;
    if ( ! cbm::flt_greater_zero( _p->mCor))
    {
        _p->mCor = 0.0;
    }

    double tree_volume( 0.0);
    if ( cbm::is_valid( _ev->tree_number))
    {
        _p->tree_number = _ev->tree_number;
        tree_volume = _p->stand_volume() / _p->tree_number;
    }
    else if ( cbm::is_valid( _ev->tree_number_resize_factor))
    {
        _p->tree_number *= _ev->tree_number_resize_factor;
        tree_volume = _p->stand_volume() / _p->tree_number;
    }
    else
    {
        _p->tree_number = 0.0;
    }

    /* no competition effect on height diameter ratio at regrowth */
    double const hd_competition_factor( 1.0);

    /* heights */
    double const height( m_alm.height_from_biomass(
                                                   (*_p)->CONIFEROUS(),
                                                   (*_p)->TAP_P1(),
                                                   (*_p)->TAP_P2(),
                                                   (*_p)->TAP_P3(),
                                                   (*_p)->HD_MIN(),
                                                   (*_p)->HD_MAX(),
                                                   (*_p)->HD_EXP(),
                                                   tree_volume,
                                                   hd_competition_factor));
    m_alm.set_canopy_heights( _crownlength_method, _p, height);

    /* diameters */
    _p->dbh = m_alm.diameter_at_breast_height_from_volume_and_height(
                                                                         (*_p)->CONIFEROUS(),
                                                                         (*_p)->TAP_P1(),
                                                                         (*_p)->TAP_P2(),
                                                                         (*_p)->TAP_P3(),
                                                                         _p->height_max,
                                                                         _p->stand_volume() / _p->tree_number);
    _p->dbas = m_alm.diameter_at_ground(
                                            _p->dbh,
                                            _p->height_max,
                                            (*_p)->HD_MIN(),
                                            (*_p)->HD_MAX(),
                                            (*_p)->HD_EXP(),
                                            hd_competition_factor);

    /* branch fraction */
    _p->f_branch = get_branch_fraction( _branchfraction_method, _p);

    /* crown diameter ratio */
    _p->cdr = m_alm.crown_diameter_ratio( _p);

    return  LDNDC_ERR_OK;
}




lerr_t  ldndc::TreeDynamics::HandleEventThrow(
                                              cbm::io_kcomm_t *_io_kcomm,
                                              MoBiLE_Plant *  _p,
                                              TreeDynamics::EventThrowData const *  _ev,
                                              double &_sFol_vt,
                                              double &_nLitFol_vt,
                                              double &_sBud_vt,
                                              double &_nLitBud_vt,
                                              double &_sWoodAbove_vt,
                                              double &_nLitWoodAbove_vt,
                                              double *_sWoodBelow_vtsl,
                                              double *_nLitWoodBelow_vtsl,
                                              double *_sFrt_vtsl,
                                              double *_nLitFrt_vtsl)
{
    /* foliage, buds and fineroots */
    double const s_fol( _ev->reduction_volume * _p->mFol);
    double const s_n_fol( s_fol * _p->ncFol);
    _sFol_vt += s_fol;
    _nLitFol_vt += s_n_fol;

    double const s_bud( _ev->reduction_volume * _p->mBud);
    double const s_n_bud( s_bud * _p->ncBud);
    _sBud_vt += s_bud;
    _nLitBud_vt += s_n_bud;

    double const s_frt( _ev->reduction_volume * _p->mFrt);
    double const s_n_frt( s_frt * _p->ncFrt);
    for ( size_t  l = 0;  l < m_sl->soil_layer_cnt();  l++)
    {
        _sFrt_vtsl[l] += s_frt * _p->fFrt_sl[l];
        _nLitFrt_vtsl[l] += s_n_frt * _p->fFrt_sl[l];
    }

    double const s_sap_above( _ev->reduction_volume * _p->mSap * (1.0 - (*_p)->UGWDF()));
    double const s_sap_below( _ev->reduction_volume * _p->mSap * (*_p)->UGWDF());
    double const s_n_sap_above( s_sap_above * _p->ncSap);
    double const s_n_sap_below( s_sap_below * _p->ncSap);

    _sWoodAbove_vt += s_sap_above;
    _nLitWoodAbove_vt += s_n_sap_above;
    for ( size_t  l = 0;  l < m_sl->soil_layer_cnt();  l++)
    {
        _sWoodBelow_vtsl[l] += s_sap_below * _p->fFrt_sl[l];
        _nLitWoodBelow_vtsl[l] += s_n_sap_below * _p->fFrt_sl[l];
    }

    double const s_cor_above( _ev->reduction_volume * _p->mCor * (1.0 - (*_p)->UGWDF()));
    double const s_cor_below( _ev->reduction_volume * _p->mCor * (*_p)->UGWDF());
    double const s_n_cor_above( s_cor_above * _p->ncCor);
    double const s_n_cor_below( s_cor_below * _p->ncCor);

    _sWoodAbove_vt += s_cor_above;
    _nLitWoodAbove_vt += s_n_cor_above;
    for ( size_t  l = 0;  l < m_sl->soil_layer_cnt();  l++)
    {
        _sWoodBelow_vtsl[l] += s_cor_below * _p->fFrt_sl[l];
        _nLitWoodBelow_vtsl[l] += s_n_cor_below * _p->fFrt_sl[l];
    }

    /* tree number and volume */
    _p->tree_number *= ( 1.0 - _ev->reduction_volume);

    /* update biomass */
    _p->mFol -= s_fol;
    _p->mFolMin *= (1.0 - _ev->reduction_volume);
    _p->mFolMax *= (1.0 - _ev->reduction_volume);
    if ( !cbm::flt_greater_zero(_p->mFol))
    {
        _p->mFol = 0.0;
    }

    _p->mBud -= s_bud;
    _p->mBudStart *= (1.0 - _ev->reduction_volume);
    if ( !cbm::flt_greater_zero(_p->mBud))
    {
        _p->mBud = 0.0;
    }

    _p->mFrt -= s_frt;
    if ( !cbm::flt_greater_zero(_p->mFrt))
    {
        _p->mFrt = 0.0;
    }

    _p->mSap -= (s_sap_above + s_sap_below);
    if ( !cbm::flt_greater_zero(_p->mSap))
    {
        _p->mSap = 0.0;
    }

    _p->mCor -= (s_cor_above + s_cor_below);
    if ( !cbm::flt_greater_zero(_p->mCor))
    {
        _p->mCor = 0.0;
    }

    cbm::state_scratch_t *  mcom = _io_kcomm->get_scratch();
    std::string  mcom_key;

    std::string  species_name = _p->cname();

    double s_fol_tmp( 0.0);
    std::string s_fol_name = "throw:"+species_name+":m_fol";
    mcom->get( s_fol_name.c_str(), &s_fol_tmp, 0.0);
    mcom->set( s_fol_name.c_str(), s_fol_tmp+s_fol);

    double s_bud_tmp( 0.0);
    std::string s_bud_name = "throw:"+species_name+":m_fru";
    mcom->get( s_bud_name.c_str(), &s_bud_tmp, 0.0);
    mcom->set( s_bud_name.c_str(), s_bud_tmp+s_bud);

    double s_frt_tmp( 0.0);
    std::string s_frt_name = "throw:"+species_name+":m_frt";
    mcom->get( s_frt_name.c_str(), &s_frt_tmp, 0.0);
    mcom->set( s_frt_name.c_str(), s_frt_tmp+s_frt);

    double s_sap_below_tmp( 0.0);
    std::string s_sap_below_name = "throw:"+species_name+":m_lst_below";
    mcom->get( s_sap_below_name.c_str(), &s_sap_below_tmp, 0.0);
    mcom->set( s_sap_below_name.c_str(), s_sap_below_tmp+s_sap_below);

    double s_sap_above_tmp( 0.0);
    std::string s_sap_above_name = "throw:"+species_name+":m_lst_above";
    mcom->get( s_sap_above_name.c_str(), &s_sap_above_tmp, 0.0);
    mcom->set( s_sap_above_name.c_str(), s_sap_above_tmp+s_sap_above);

    double s_cor_below_tmp( 0.0);
    std::string s_cor_below_name = "throw:"+species_name+":m_dst_below";
    mcom->get( s_cor_below_name.c_str(), &s_cor_below_tmp, 0.0);
    mcom->set( s_cor_below_name.c_str(), s_cor_below_tmp+s_cor_below);

    double s_cor_above_tmp( 0.0);
    std::string s_cor_above_name = "throw:"+species_name+":m_dst_above";
    mcom->get( s_cor_above_name.c_str(), &s_cor_above_tmp, 0.0);
    mcom->set( s_cor_above_name.c_str(), s_cor_above_tmp+s_cor_above);




    double s_n_fol_tmp( 0.0);
    std::string s_n_fol_name = "throw:"+species_name+":n_fol";
    mcom->get( s_n_fol_name.c_str(), &s_n_fol_tmp, 0.0);
    mcom->set( s_n_fol_name.c_str(), s_n_fol_tmp+s_n_fol);

    double s_n_bud_tmp( 0.0);
    std::string s_n_bud_name = "throw:"+species_name+":n_fru";
    mcom->get( s_n_bud_name.c_str(), &s_n_bud_tmp, 0.0);
    mcom->set( s_n_bud_name.c_str(), s_n_bud_tmp+s_n_bud);

    double s_n_frt_tmp( 0.0);
    std::string s_n_frt_name = "throw:"+species_name+":n_frt";
    mcom->get( s_n_frt_name.c_str(), &s_n_frt_tmp, 0.0);
    mcom->set( s_n_frt_name.c_str(), s_n_frt_tmp+s_n_frt);

    double s_n_sap_below_tmp( 0.0);
    std::string s_n_sap_below_name = "throw:"+species_name+":n_lst_below";
    mcom->get( s_n_sap_below_name.c_str(), &s_n_sap_below_tmp, 0.0);
    mcom->set( s_n_sap_below_name.c_str(), s_n_sap_below_tmp+s_n_sap_below);

    double s_n_sap_above_tmp( 0.0);
    std::string s_n_sap_above_name = "throw:"+species_name+":n_lst_above";
    mcom->get( s_n_sap_above_name.c_str(), &s_n_sap_above_tmp, 0.0);
    mcom->set( s_n_sap_above_name.c_str(), s_n_sap_above_tmp+s_n_sap_above);

    double s_n_cor_below_tmp( 0.0);
    std::string s_n_cor_below_name = "throw:"+species_name+":n_dst_below";
    mcom->get( s_n_cor_below_name.c_str(), &s_n_cor_below_tmp, 0.0);
    mcom->set( s_n_cor_below_name.c_str(), s_n_cor_below_tmp+s_n_cor_below);

    double s_n_cor_above_tmp( 0.0);
    std::string s_n_cor_above_name = "throw:"+species_name+":n_dst_above";
    mcom->get( s_n_cor_above_name.c_str(), &s_n_cor_above_tmp, 0.0);
    mcom->set( s_n_cor_above_name.c_str(), s_n_cor_above_tmp+s_n_cor_above);



    return  LDNDC_ERR_OK;
}



lerr_t  ldndc::TreeDynamics::HandleEventThin(
                                             cbm::io_kcomm_t *_io_kcomm,
                                             MoBiLE_Plant *  _p,
                                             TreeDynamics::EventThinData const *  _ev,
                                             cbm::string_t _crownlength_method,
                                             bool _competition_method,
                                             double &_sFol_vt,
                                             double &_nLitFol_vt,
                                             double &_sBud_vt,
                                             double &_nLitBud_vt,
                                             double &_sWoodAbove_vt,
                                             double &_nLitWoodAbove_vt,
                                             double *_sWoodBelow_vtsl,
                                             double *_nLitWoodBelow_vtsl,
                                             double *_sFrt_vtsl,
                                             double *_nLitFrt_vtsl)
{
    double const hd_competition_factor_old( !_competition_method ? 1.0 :
                                            m_alm.height_competition_factor( _p, m_veg));

    /* potential height according to current biomass */
    double  height_pot( m_alm.height_from_biomass(
                                                  (*_p)->CONIFEROUS(),
                                                  (*_p)->TAP_P1(),
                                                  (*_p)->TAP_P2(),
                                                  (*_p)->TAP_P3(),
                                                  (*_p)->HD_MIN(),
                                                  (*_p)->HD_MAX(),
                                                  (*_p)->HD_EXP(),
                                                  _p->stand_volume() / _p->tree_number,
                                                  hd_competition_factor_old));
    /* fraction between actual and potenital biomass */
    double const height_fraction( cbm::flt_greater_zero( height_pot) ? _p->height_max / height_pot : 1.0);

    double  dbh_pot( m_alm.diameter_at_breast_height_from_volume_and_height(
                                                                            (*_p)->CONIFEROUS(),
                                                                            (*_p)->TAP_P1(),
                                                                            (*_p)->TAP_P2(),
                                                                            (*_p)->TAP_P3(),
                                                                            height_pot,
                                                                            _p->stand_volume() / _p->tree_number));
    double const dbh_fraction( cbm::flt_greater_zero( dbh_pot) ? _p->dbh / dbh_pot : 1.0);

    double  dbas_pot( m_alm.diameter_at_ground(
                                               dbh_pot,
                                               height_pot,
                                               (*_p)->HD_MIN(),
                                               (*_p)->HD_MAX(),
                                               (*_p)->HD_EXP(),
                                               hd_competition_factor_old));
    double const dbas_fraction( cbm::flt_greater_zero( dbas_pot) ? _p->dbas / dbas_pot : 1.0);

    /* foliage, buds and fineroots */
    double const s_fol( _ev->reduction_volume * _p->mFol);
    double const s_n_fol( s_fol * _p->ncFol);
    _sFol_vt += s_fol;
    _nLitFol_vt += s_n_fol;

    double const s_bud( _ev->reduction_volume * _p->mBud);
    double const s_n_bud( s_bud * _p->ncBud);
    _sBud_vt += s_bud;
    _nLitBud_vt += s_n_bud;

    double const s_frt( _ev->reduction_volume * _p->mFrt);
    double const s_n_frt( s_frt * _p->ncFrt);
    for ( size_t  l = 0;  l < m_sl->soil_layer_cnt();  l++)
    {
        _sFrt_vtsl[l] += s_frt * _p->fFrt_sl[l];
        _nLitFrt_vtsl[l] += s_n_frt * _p->fFrt_sl[l];
    }


    /* sap wood */
    double const s_sap_branch( _ev->reduction_volume * _p->mSap * _p->f_branch);
    double const s_sap_stem( _ev->reduction_volume * _p->mSap * (1.0 - _p->f_branch - (*_p)->UGWDF()));
    double const s_sap_below( _ev->reduction_volume * _p->mSap * (*_p)->UGWDF());
    double s_sap_above_export( 0.0);
    double const s_n_sap_branch( s_sap_branch * _p->ncSap);
    double const s_n_sap_stem( s_sap_stem * _p->ncSap);
    double const s_n_sap_below( s_sap_below * _p->ncSap);
    double s_n_sap_above_export( 0.0);
    if ( _ev->export_sapwood)
    {
        _sWoodAbove_vt    += s_sap_branch;
        _nLitWoodAbove_vt += s_n_sap_branch;
        s_sap_above_export += s_sap_stem;
        s_n_sap_above_export += s_n_sap_stem;
    }
    else
    {
        _sWoodAbove_vt    += s_sap_stem + s_sap_branch;
        _nLitWoodAbove_vt += s_n_sap_stem + s_n_sap_branch;
    }
    for ( size_t  l = 0;  l < m_sl->soil_layer_cnt();  l++)
    {
        _sWoodBelow_vtsl[l]    += s_sap_below * _p->fFrt_sl[l];
        _nLitWoodBelow_vtsl[l] += s_n_sap_below * _p->fFrt_sl[l];
    }


    /* core wood */
    double const  s_cor_above( _ev->reduction_volume * _p->mCor * (1.0 - (*_p)->UGWDF()));
    double const  s_cor_below( _ev->reduction_volume * _p->mCor * (*_p)->UGWDF());
    double s_cor_above_export( 0.0);
    double const  s_n_cor_above( s_cor_above * _p->ncCor);
    double const  s_n_cor_below( s_cor_below * _p->ncCor);
    double s_n_cor_above_export( 0.0);
    if ( _ev->export_corewood)
    {
        s_cor_above_export += s_cor_above;
        s_n_cor_above_export += s_n_cor_above;
    }
    else
    {
        _sWoodAbove_vt    += s_cor_above;
        _nLitWoodAbove_vt += s_n_cor_above;
    }
    for ( size_t  l = 0;  l < m_sl->soil_layer_cnt();  l++)
    {
        _sWoodBelow_vtsl[l]    += s_cor_below * _p->fFrt_sl[l];
        _nLitWoodBelow_vtsl[l] += s_n_cor_below * _p->fFrt_sl[l];
    }


    /* tree number and volume */
    _p->tree_number *= ( 1.0 - _ev->reduction_number);

    /* update biomass */
    _p->mFol -= s_fol;
    _p->mFolMin *= (1.0 - _ev->reduction_volume);
    _p->mFolMax *= (1.0 - _ev->reduction_volume);
    if ( !cbm::flt_greater_zero(_p->mFol))
    {
        _p->mFol = 0.0;
    }

    _p->mBud -= s_bud;
    _p->mBudStart *= (1.0 - _ev->reduction_volume);
    if ( !cbm::flt_greater_zero(_p->mBud))
    {
        _p->mBud = 0.0;
    }

    _p->mFrt -= s_frt;
    if ( !cbm::flt_greater_zero(_p->mFrt))
    {
        _p->mFrt = 0.0;
    }

    _p->mSap -= (s_sap_branch + s_sap_stem + s_sap_below);
    if ( !cbm::flt_greater_zero(_p->mSap))
    {
        _p->mSap = 0.0;
    }

    _p->mCor -= (s_cor_above + s_cor_below);
    if ( !cbm::flt_greater_zero(_p->mCor))
    {
        _p->mCor = 0.0;
    }

    m_alm.restructure_vegetation( _p,
                             _crownlength_method,
                             hd_competition_factor_old,
                             height_fraction,
                             dbas_fraction,
                             dbh_fraction);

    cbm::state_scratch_t *  mcom = _io_kcomm->get_scratch();
    std::string  mcom_key;

    char const *  species_name = _p->cname();

    cbm::state_scratch_t::make_item_key(  &mcom_key, "thin:%s:m_fol", species_name);
    mcom->set( mcom_key.c_str(), s_fol);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "thin:%s:m_fol_export", species_name);
    mcom->set( mcom_key.c_str(), 0.0);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "thin:%s:m_fru", species_name);
    mcom->set( mcom_key.c_str(), s_bud);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "thin:%s:m_fru_export", species_name);
    mcom->set( mcom_key.c_str(), 0.0);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "thin:%s:m_frt", species_name);
    mcom->set( mcom_key.c_str(), s_frt);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "thin:%s:m_frt_export", species_name);
    mcom->set( mcom_key.c_str(), 0.0);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "thin:%s:m_lst_below", species_name);
    mcom->set( mcom_key.c_str(), s_sap_below);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "thin:%s:m_lst_below_export", species_name);
    mcom->set( mcom_key.c_str(), 0.0);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "thin:%s:m_lst_above", species_name);
    mcom->set( mcom_key.c_str(), s_sap_branch + s_sap_stem);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "thin:%s:m_lst_above_export", species_name);
    mcom->set( mcom_key.c_str(), s_sap_above_export);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "thin:%s:m_dst_below", species_name);
    mcom->set( mcom_key.c_str(), s_cor_below);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "thin:%s:m_dst_below_export", species_name);
    mcom->set( mcom_key.c_str(), 0.0);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "thin:%s:m_dst_above", species_name);
    mcom->set( mcom_key.c_str(), s_cor_above);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "thin:%s:m_dst_above_export", species_name);
    mcom->set( mcom_key.c_str(), s_cor_above_export);



    cbm::state_scratch_t::make_item_key(  &mcom_key, "thin:%s:n_fol", species_name);
    mcom->set( mcom_key.c_str(), s_n_fol);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "thin:%s:n_fol_export", species_name);
    mcom->set( mcom_key.c_str(), 0.0);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "thin:%s:n_fru", species_name);
    mcom->set( mcom_key.c_str(), s_n_bud);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "thin:%s:n_fru_export", species_name);
    mcom->set( mcom_key.c_str(), 0.0);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "thin:%s:n_frt", species_name);
    mcom->set( mcom_key.c_str(), s_n_frt);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "thin:%s:n_frt_export", species_name);
    mcom->set( mcom_key.c_str(), 0.0);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "thin:%s:n_lst_below", species_name);
    mcom->set( mcom_key.c_str(), s_n_sap_below);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "thin:%s:n_lst_below_export", species_name);
    mcom->set( mcom_key.c_str(), 0.0);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "thin:%s:n_lst_above", species_name);
    mcom->set( mcom_key.c_str(), s_n_sap_branch + s_n_sap_stem);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "thin:%s:n_lst_above_export", species_name);
    mcom->set( mcom_key.c_str(), s_n_sap_above_export);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "thin:%s:n_dst_below", species_name);
    mcom->set( mcom_key.c_str(), s_n_cor_below);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "thin:%s:n_dst_below_export", species_name);
    mcom->set( mcom_key.c_str(), 0.0);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "thin:%s:n_dst_above", species_name);
    mcom->set( mcom_key.c_str(), s_n_cor_above);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "thin:%s:n_dst_above_export", species_name);
    mcom->set( mcom_key.c_str(), s_n_cor_above_export);

    return  LDNDC_ERR_OK;
}
