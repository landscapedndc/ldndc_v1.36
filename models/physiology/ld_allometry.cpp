/*!
 * @file
 *  Plant related allometry functions, which can be globally
 *  used by vegetation models.
 *
 * @authors
 *    - Ruediger Grote
 *    - David Kraus
 *    - Edwin Haas
 *
 * @date
 *    Nov, 2017
 */

/*!
 * @page veglibs
 * @section veglibs_stem_dimensions Stem dimensions
 * The shape of tree stems is defined by the relation between tree height, diameter
 * and stem volume. While the volume is only constraint by stem biomass and wood density,
 * a change in volume can thus be related to a change in tree height and diameter 
 * based on:
 *      - a height-diameter relationship that is dynamically related to stand density
 *      - an empirically defined taper (shape) function
 */

#include  "physiology/ld_allometry.h"

using namespace ldndc;

ldndc::LD_Allometry::LD_Allometry(
                                  ldndc::MoBiLE_State *  _state,
                                  cbm::io_kcomm_t * _io_kcomm):
m_setup( _io_kcomm->get_input_class_ref< input_class_setup_t >()),
ph_( _state->get_substate_ref< ldndc::substate_physiology_t >()),
m_veg( &_state->vegetation)
{ }


ldndc::LD_Allometry::~LD_Allometry()
{ }


/*!
 * @page veglibs
 * Because the taper function is based on tree height and diameter at breast height -
 * but the latter is unavailable for trees shorter than 1.3 m - it is assumed that 
 * the form of short trees \f$(height < 0.5 \cdot HLIMIT)\f$ is that of a cone. Thus, 
 * dimensional growth can be determined from volume growth with only a given height-diameter ratio. 
 * The stem volume of large trees \f$(height > HLIMIT)\f$ is determined by a taper function.
 */
double
ldndc::LD_Allometry::stem_volume(
                                 double _height,
                                 double _dbh,
                                 double _dbas,
                                 bool _coniferous,
                                 double _T1,
                                 double _T2,
                                 double _T3)
{
    // above HLIMIT tree shape is given by taper function
    if ( cbm::flt_greater( _height, cbm::HLIMIT))
    {
        return taper_volume( _coniferous, _T1, _T2, _T3, _dbh, _height);
    }
    // below 0.5* HLIMIT (=1.3 m) trees are shaped as cones
    else if ( cbm::flt_less_equal( _height, 0.5 * cbm::HLIMIT))
    {
        return cbm::conus_volume( _dbas, _height);
    }
    // linear switch between cone and taper function
    else
    {
        double const volume_c( cbm::conus_volume( _dbas, _height));
        double const volume_t( taper_volume( _coniferous, _T1, _T2, _T3, _dbh, _height));
        double const fraction( hlimit_fraction( _height));
        return fraction * volume_t + (1.0 - fraction) * volume_c;
    }
}


/*!
 * @page veglibs
 * @subsection veglibs_height_diameter_ratio Height-diameter ratio
 * Both calculations of stem dimensional developments (for short as well as tall trees)
 * require an indication of the optimal height-diameter relationship, which is given as:
 * \f[
 *   hd\_{opt} = (HDMIN + (HDMAX - HDMIN) \cdot exp(-HDEXP \cdot dbh)) \cdot competition\_{factor}
 * \f]
 * \f[
 *   competition\_{factor} = (\frac {areacover\_{openrange} }{DFLIMIT} ) ^ {DFEXP}
 * \f]
 * with
 * - dbh: diameter at breast height (1.3m) (m)
 * - competition_factor: stand density factor
 * - areacover_openrange: stand coverage, calculated by the number of trees multiplied by the largest crown diameter
 * - HDMIN, HDMAX, HDEXP: species-specific parameters for height-diameter function
 * - DFLIM, DFEXP: species-specific parameters for competition function
 *
 * If the optimum height:diameter ratio (hd ratio) can be achieved based on the current tree dimensions,
 * it is realized. Otherwise diameter growth will be established iteratively with a hd ratio as close as
 * possible to the optimum one until the new wood volume (vol, m3) matches the calculated volume growth
 * (see @ref taper).
 * 
 */
double
ldndc::LD_Allometry::height_dbh_ratio(
    double _diameter,
    double _hd_min,
    double _hd_max,
    double _hd_exp,
    double _hd_competition_factor)
{
    return (_hd_min + (_hd_max - _hd_min) * std::exp(-_hd_exp * _diameter)) * _hd_competition_factor;
}


/*!
 * height competition factor, described above with height-diameter ratio
 */
double
ldndc::LD_Allometry::height_competition_factor(
    MoBiLE_Plant* _vt,
    MoBiLE_PlantVegetation* _veg)
{
    size_t fl_max(0);
    double fFol_max(0.0);
    for (size_t fl = 0; fl < _vt->nb_foliagelayers(); fl++)
    {
        if (cbm::flt_greater(_vt->fFol_fl[fl], fFol_max))
        {
            fl_max = fl;
            fFol_max = _vt->fFol_fl[fl];
        }
    }

    double area_cover_open_range_total(0.0);
    for (TreeIterator w = _veg->groupbegin< species::wood >();
        w != _veg->groupend< species::wood >(); ++w)
    {
        MoBiLE_Plant* p = *w;
        if (cbm::flt_greater_zero(p->fFol_fl[fl_max]))
        {
            double const fFol_fraction(p->fFol_fl[fl_max] / p->f_fol_maximum());

            /* crown diameter ratio for open range conditions */
            double const cdr_open_range(crown_diameter_ratio_open_range(
                p->dbh, p->height_max,
                w->CDR_P1(), w->CDR_P2(), w->CDR_P3()));

            /* maximum area cover fraction of open range [m^2:ha^-1] */
            double const area_cover_open_range_vt(tree_crown_ground_coverage(
                p->dbh,
                p->tree_number,
                cdr_open_range));
            area_cover_open_range_total += fFol_fraction * area_cover_open_range_vt;
        }
    }

    return pow(cbm::bound_min(1.0, area_cover_open_range_total / (*_vt)->DF_LIMIT()), (*_vt)->DF_EXP());
}


/*!
 * @page veglibs
 * @subsection veglibs_cone Cone-shape based dimensional growth
 * For short trees, a cone-shaped trunk is assumed where the relationship between
 * volume, height, and (base) diameter can be calculated as:
 * \f[
 *   vol_{cone} = \frac {PI \cdot (d\_{base} \cdot 0.5) ^ {2} \cdot height} {3.0}
 * \f]
 * 
 * All dimensional changes based on wood volume increase can thus be derived  
 * considering the previously defined height-diameter relationship \f$ f_{hd}(d) \f$:
 * \f[
 *   height = \frac {vol_{cone} \cdot 3.0} {PI \cdot (d\_{base} \cdot 0.5) ^ {2} }
 * \f]
 * \f[
 *   d\_{base} = \frac {height} { hd\_{opt} }
 * \f]
 * with
 * - PI: constant (3.146)
 * - d_base: base diameter (m)
 */
double
ldndc::LD_Allometry::height_from_diameter_at_breast_height(
                                                           double _diameter,
                                                           double _hd_min,
                                                           double _hd_max,
                                                           double _hd_exp,
                                                           double _hd_competition_factor)
{
    double const hd_ratio(height_dbh_ratio(
        _diameter, _hd_min, _hd_max,
        _hd_exp, _hd_competition_factor));
    return _diameter * hd_ratio;
}


/*!
 * @page veglibs
 * @subsection veglibs_taper Taper-function based dimensional growth
 * The taper function that has been selected for the calculation of tree volume [\f$m^3\f$]
 * depends on tree height \f$ h [m] \f$ and tree diameter \f$ d [m] \f$:
 * \f[
 *   vol_{tap} = 0.001 \cdot (100 \cdot dbh)^{TAP\_{P1} } \cdot height^{TAP\_{P2}} \cdot exp(TAP\_{P3})
 * \f]
 * with
 * - TAP_P1, -P2, P3: species-specific parameters
 * It has been particularly advocated in @cite dik:1984a with parameters for various species (see citation in @cite zianis:2005a ).
 * 
 */
double
ldndc::LD_Allometry::taper_volume(
                                  bool _coniferous,
                                  double _T1,
                                  double _T2,
                                  double _T3,
                                  double _d,
                                  double _h)
{
    double  T1( _T1), T2( _T2), T3( _T3);
    check_taper_parameters( _coniferous, T1, T2, T3);
    
    return cbm::M3_IN_DM3 * pow( _d * cbm::CM_IN_M, T1) * pow( _h, T2) * exp( T3);
}


/*!
 * @page veglibs
 *  The taper parameters \f$ TAP_P1, TAP_P2, TAP_P3 \f$ are species-specific.
 *  Default parameters for coniferous trees are:
 *      - \f$ TAP_P1: 1.75 \f$
 *      - \f$ TAP_P2: 1.1 \f$
 *      - \f$ TAP_P3: -2.75 \f$
 *
 *  Default parameters for deciduous trees are:
 *      - \f$ TAP_P1: 1.95 \f$
 *      - \f$ TAP_P2: 0.75 \f$
 *      - \f$ TAP_P3: -2.4 \f$
 */
lerr_t
ldndc::LD_Allometry::check_taper_parameters(
                                        bool _coniferous,
                                        double &_T1,
                                        double &_T2,
                                        double &_T3)
{
    if ( !cbm::flt_not_equal_zero( _T1 + _T2 + _T3))
    {
        // if parameterisation is not valid/available, parameters that describe average developments are used
        if ( _coniferous)
        {
            _T1 = 1.75; _T2 = 1.1; _T3 = -2.75;
        }
        else
        {
            _T1 = 1.95; _T2 = 0.75; _T3 = -2.4;
        }
    }

    return  LDNDC_ERR_OK;
}


/*!
 * @page veglibs
 * According to the taper equation, tree height is can be defined in dependence 
 * on tree volume \f$ V \f$ [m\f$^3\f$] and tree diameter at breast height [m]:
 * \f[
 *   height = \left( \frac{vol_{tap}}{0.001 \cdot (100 \cdot dbh)^{TAP_P1}} \cdot exp(TAP_P3) \right)^{\frac{1}{TAP_P2}}
 * \f]
 */
double
ldndc::LD_Allometry::taper_height_from_diameter(
                                            bool _coniferous,
                                            double _t1,
                                            double _t2,
                                            double _t3,
                                            double _d,
                                            double _vol)
{
    if ( cbm::flt_greater_zero( _d))
    {
        double  t1( _t1), t2( _t2), t3( _t3);
        check_taper_parameters( _coniferous, t1, t2, t3);
        
        return pow( _vol / ( cbm::M3_IN_DM3 * pow( _d * cbm::CM_IN_M, t1) * exp( t3)), 1.0 / t2);
    }
    else
    {
        return 0.0;
    }
}


/*!
 * @page veglibs
 * Also, tree diameter at breast height can be calculated with this equation based on
 * tree volume \f$ V \f$ [m\f$^3\f$] and tree height [m] using the same taper parameters:
 * \f[
 *   dbh = 0.01 \cdot \left( \frac{vol_{tap}}{0.001 \cdot height^{TAP_P2}} \cdot exp(TAP_P3) \right)^{\frac{1}{TAP_P1}}
 * \f]
 */
double
ldndc::LD_Allometry::taper_diameter_from_height(
                                                bool _coniferous,
                                                double _t1,
                                                double _t2,
                                                double _t3,
                                                double _h,
                                                double _vol)
{
    if ( cbm::flt_greater_zero( _h))
    {
        double  t1( _t1), t2( _t2), t3( _t3);
        check_taper_parameters( _coniferous, t1, t2, t3);

        return pow( _vol / ( cbm::M3_IN_DM3 * pow( _h, t2) * exp( t3)), 1.0 / t1) / cbm::CM_IN_M;
    }
    else
    {
        return 0.0;
    }
}


/*!
 * @page veglibs
 * For larger trees (\f$ h > HLIMIT \f$), diameter at ground height is determined
 * depending on diameter at breast height \f$ dbh \f$ and tree height \f$ height \f$:
 * \n\n
 * \f$ d_{base} = dbh + \frac{HLIMIT}{height} \cdot dbh \f$ 
 * \n\n
  */
double
ldndc::LD_Allometry::diameter_at_ground(
                                        double _diameter,
                                        double _height,
                                        double _hd_min,
                                        double _hd_max,
                                        double _hd_exp,
                                        double _hd_competition_factor)
{
    if (cbm::flt_greater_zero(_height))
    {
        double const h_dbh_ratio(height_dbh_ratio(
            _diameter,
            _hd_min,
            _hd_max,
            _hd_exp,
            _hd_competition_factor));

        // empirically adjusted hd to account for diameter at ground level
        double const h_dbas_ratio(0.5 * h_dbh_ratio);

        // diameter at ground height for large trees
        if (cbm::flt_greater(_height, cbm::HLIMIT))
        {
            return ((_height / h_dbh_ratio) + (0.5 * cbm::HLIMIT / h_dbas_ratio));
        }

        // diameter at ground height for small trees
        else if (cbm::flt_less_equal(_height, 0.5 * cbm::HLIMIT))
        {
            return _height / h_dbas_ratio;
        }
        // diameter at ground height during transition
        else
        {
            double const fraction(hlimit_fraction(_height));
            return (fraction * ((_height / h_dbh_ratio) + (0.5 * cbm::HLIMIT / h_dbas_ratio))
                + (1.0 - fraction) * _height / h_dbas_ratio);
        }
    }
    else
    {
        return 0.0;
    }
}


/*!
 * @page veglibs
 * @subsection veglibs_transition Transition phase dimensional growth
 * For \f$ \; 0.5 \cdot HLIMIT < height \le HLIMIT \; \f$ a linear transition between the two
 * shapes is assumed:
 * \f[
 *    vol = \phi \; vol_{tap} + (1.0 - \phi) \; vol_{cone}
 * \f]
 * with \f$ \phi \f$ representing a linear scaling factor.
 * Accordingly, the diameter used for allometric scaling (e.g. regarding height/diameter ratio)
 * is shifting during this period between ground height (dbase) and breast height (dbh), and is
 * regarded as effective tree diameter (@cite condes_sterba:2005a). As described for the volume, the
 * effective tree diameter is defined by a linear transition using the scaling factor \f$ \phi \f$):
 * \f[
 *   d_{eff} = (1 - \phi) \; d_{base} + \phi \; dbh
 * \f]
 */
double
ldndc::LD_Allometry::effective_dbh(
                                   double _h /* tree height */,
                                   double _dbh /* diameter at breast height */,
                                   double _dbas /* diameter at ground */)
{
    if (cbm::flt_greater(_h, cbm::HLIMIT))
    {
        return  cbm::bound_min(cbm::DBHLIMIT, _dbh);
    }
    else if (cbm::flt_less_equal(_h, 0.5 * cbm::HLIMIT))
    {
        return  cbm::bound_min(cbm::DBHLIMIT, _dbas);
    }
    else
    {
        double const fraction(hlimit_fraction(_h));
        return cbm::bound_min(cbm::DBHLIMIT, fraction * _dbh + (1.0 - fraction) * _dbas);
    }
}


/*!
 * @details
 * crown base should not decline
 */
lerr_t
ldndc::LD_Allometry::set_canopy_heights(
                                        cbm::string_t _crownlength_method,
                                        MoBiLE_Plant * _vt,
                                        double _height_max)
{
    _vt->height_max = _height_max;

    if ( (*_vt)->IS_WOOD())
    {
        if ( _crownlength_method == "parameter")
        {
            double const crown_length( crown_length_from_parameter(
                                                                         _vt->height_max,
                                                                         _vt->dbh,
                                                                         (*_vt)->HREF(),
                                                                         (*_vt)->DIAMMAX(),
                                                                         (*_vt)->CB()));
            _vt->height_at_canopy_start = cbm::bound_min( _vt->height_at_canopy_start,
                                                         _vt->height_max - crown_length);
        }
        else
        {
            double const crown_length( crown_length_from_crown_diameter(
                                                                              _vt->height_max,
                                                                              _vt->cdr * _vt->dbh,
                                                                              (*_vt)->CL_P1(),
                                                                              (*_vt)->CL_P2()));
            _vt->height_at_canopy_start = cbm::bound_min( _vt->height_at_canopy_start,
                                                         _vt->height_max - crown_length);
        }
    }
    else
    {
        _vt->height_at_canopy_start = 0.0;
    }

    return LDNDC_ERR_OK;
}


void
ldndc::LD_Allometry::restructure_vegetation(
                                        MoBiLE_Plant *_p,
                                        cbm::string_t _crownlength_method,
                                        double _hd_competition_factor_old,
                                        double _height_fraction,
                                        double _dbas_fraction,
                                        double _dbh_fraction)
{
    double height_pot = height_from_biomass(
                                           (*_p)->CONIFEROUS(),
                                           (*_p)->TAP_P1(),
                                           (*_p)->TAP_P2(),
                                           (*_p)->TAP_P3(),
                                           (*_p)->HD_MIN(),
                                           (*_p)->HD_MAX(),
                                           (*_p)->HD_EXP(),
                                           _p->stand_volume() / _p->tree_number,
                                           _hd_competition_factor_old);
    set_canopy_heights( _crownlength_method, _p, _height_fraction * height_pot);


    double dbh_pot = diameter_at_breast_height_from_volume_and_height(
                                                                     (*_p)->CONIFEROUS(),
                                                                     (*_p)->TAP_P1(),
                                                                     (*_p)->TAP_P2(),
                                                                     (*_p)->TAP_P3(),
                                                                     height_pot,
                                                                     _p->stand_volume() / _p->tree_number);
    _p->dbh = _dbh_fraction * dbh_pot;


    double dbas_pot = diameter_at_ground(
                                        dbh_pot,
                                        height_pot,
                                        (*_p)->HD_MIN(),
                                        (*_p)->HD_MAX(),
                                        (*_p)->HD_EXP(),
                                        _hd_competition_factor_old);
    _p->dbas = _dbas_fraction * dbas_pot;
}


/*!
 * @page veglibs
 * @subsection veglibs_iteration Iteration between volume and dimensional growth
 * Since the optimum height-diameter ratio might not be achievable given the dimensions
 * of the previous state, a change in trunk volume is translated into height and diameter
 * changes iteratively:
 * \f{eqnarray*}{
 *  d_{test} = DBHLIMIT \\
 *  \text{while} \; (vol_{test} < vol): \\
 *  height_{test} &=& d_{test} \cdot hd\_{opt}(d_{test}) \\
 *  vol_{test} &=& V(d_{test}, height_{test}) \\
 *  d_{test} &=& d_{test} + increment
 * \f}
 * With the increment used for diameter modification is plus or minus 10, 1, and 0.5 cm steps.
 */
double
ldndc::LD_Allometry::height_from_biomass(
                                         bool _coniferous,
                                         double _t1,
                                         double _t2,
                                         double _t3,
                                         double _hd_min,
                                         double _hd_max,
                                         double _hd_exp,
                                         double _vol,
                                         double _hd_competition_factor)
{
    if ( cbm::flt_greater_zero( _vol))
    {
        check_taper_parameters( _coniferous, _t1, _t2, _t3);

        double diameter( cbm::DBHLIMIT);
        double hd( height_dbh_ratio( diameter, _hd_min, _hd_max,
                                    _hd_exp, _hd_competition_factor));

        double height( diameter * hd);
        double volume( taper_volume(_coniferous, _t1, _t2, _t3, diameter, height));
        while ( cbm::flt_less( volume, _vol) &&
                cbm::flt_less( diameter, 5.0))
        {
            // 10 cm increments
            diameter += 0.1;
            hd = height_dbh_ratio( diameter, _hd_min, _hd_max,
                                  _hd_exp, _hd_competition_factor);
            height = diameter * hd;
            volume = taper_volume(_coniferous, _t1, _t2, _t3, diameter, height);
        }

        // decrement 10 cm
        diameter = cbm::bound_min( cbm::DBHLIMIT, diameter - 0.1);
        hd = height_dbh_ratio( diameter, _hd_min, _hd_max,
                              _hd_exp, _hd_competition_factor);
        height = diameter * hd;
        volume = taper_volume(_coniferous, _t1, _t2, _t3, diameter, height);

        while ( cbm::flt_less( volume, _vol) &&
               cbm::flt_less( diameter, 5.0))
        {
            // 1 cm increment
            diameter += 0.01;
            hd = height_dbh_ratio( diameter, _hd_min, _hd_max,
                                  _hd_exp, _hd_competition_factor);
            height = diameter * hd;
            volume = taper_volume(_coniferous, _t1, _t2, _t3, diameter, height);
        }

        // decrement 0.5 cm
        diameter = cbm::bound_min( cbm::DBHLIMIT, diameter - 0.005);
        hd = height_dbh_ratio( diameter, _hd_min, _hd_max,
                              _hd_exp, _hd_competition_factor);

        return diameter * hd;
    }
    else
    {
        return 0.0;
    }
}


double
ldndc::LD_Allometry::diameter_at_breast_height_from_volume_and_height(
                                                                      bool _coniferous,
                                                                      double _t1,
                                                                      double _t2,
                                                                      double _t3,
                                                                      double _h,
                                                                      double _vol)
{
    if ( cbm::flt_greater( _h, cbm::HLIMIT))
    {
        return taper_diameter_from_height( _coniferous,
                                           _t1, _t2, _t3,
                                           _h, _vol);
    }
    else if ( cbm::flt_less( _h, 0.5 * cbm::HLIMIT))
    {
        return cbm::conus_diameter( _h, _vol);
    }
    else
    {
        // tree diameter determined by cone function
        double const d_conus( cbm::conus_diameter( _h, _vol));

        // tree diameter determined by taper function
        double const d_taper( taper_diameter_from_height( _coniferous,
                                                          _t1, _t2, _t3,
                                                          _h, _vol));

        // scaling factor for tree height from taper or cone function
        double const fraction( hlimit_fraction( _h));

        return fraction * d_taper + (1.0 - fraction) * d_conus;
    }
}


double
ldndc::LD_Allometry::diameter_at_breast_height_from_height(
                                                       double _height,
                                                       double _hd_min,
                                                       double _hd_max,
                                                       double _hd_exp,
                                                       double _hd_competition_factor)
{
    double const diameter_min( 1.0e-9);
    double diameter( 10.0); // start with very large diameter (10m)
    double hd_1( height_dbh_ratio( diameter, _hd_min, _hd_max, _hd_exp,
                                                _hd_competition_factor));
    double hd_2( _height / diameter);

    // decrement in 1m steps
    double decrement( 1.0);
    while ( cbm::flt_greater( hd_1, hd_2) &&
            cbm::flt_greater_zero( diameter))
    {
        diameter = cbm::bound_min( diameter_min, diameter - decrement);
        hd_1 = height_dbh_ratio( diameter, _hd_min, _hd_max, _hd_exp,
                                              _hd_competition_factor);
        hd_2 = _height / diameter;
    }

    // go one step back
    diameter = cbm::bound_min( diameter_min, diameter + decrement);
    hd_1 = height_dbh_ratio( diameter, _hd_min, _hd_max, _hd_exp,
                                          _hd_competition_factor);
    hd_2 = _height / diameter;

    // decrement in 10cm steps
    decrement = 0.1;
    while ( cbm::flt_greater( hd_1, hd_2) &&
            cbm::flt_greater_zero( diameter))
    {
        diameter = cbm::bound_min( diameter_min, diameter - decrement);
        hd_1 = height_dbh_ratio( diameter, _hd_min, _hd_max, _hd_exp,
                                              _hd_competition_factor);
        hd_2 = _height / diameter;
    }

    // go one step back
    diameter = cbm::bound_min( diameter_min, diameter + decrement);
    hd_1 = height_dbh_ratio( diameter, _hd_min, _hd_max, _hd_exp,
                                          _hd_competition_factor);
    hd_2 = _height / diameter;

    // decrement in 1cm steps
    decrement = 0.01;
    while ( cbm::flt_greater( hd_1, hd_2) &&
            cbm::flt_greater_zero( diameter))
    {
        diameter = cbm::bound_min( diameter_min, diameter - decrement);
        hd_1 = height_dbh_ratio( diameter, _hd_min, _hd_max, _hd_exp,
                                              _hd_competition_factor);
        hd_2 = _height / diameter;
    }

    // go one step back
    diameter = cbm::bound_min( diameter_min, diameter + decrement);
    hd_1 = height_dbh_ratio( diameter, _hd_min, _hd_max, _hd_exp,
                                          _hd_competition_factor);
    hd_2 = _height / diameter;

    // decrement in 1mm steps
    decrement = 0.001;
    while ( cbm::flt_greater( hd_1, hd_2) &&
            cbm::flt_greater_zero( diameter))
    {
        diameter = cbm::bound_min( diameter_min, diameter - decrement);
        hd_1 = height_dbh_ratio( diameter, _hd_min, _hd_max, _hd_exp,
                                              _hd_competition_factor);
        hd_2 = _height / diameter;
    }

    // go halfway back
    diameter += 0.5 * decrement;

    return diameter;
}


/*!
 * @details
 *
 */
double
ldndc::LD_Allometry::hlimit_fraction(
                                     double _height)
{
    return cbm::bound(0.0, (_height - (0.5 * cbm::HLIMIT)) / (0.5 * cbm::HLIMIT), 1.0);
}


/*!
 * @page veglibs
 * @section veglibs_crown-dimensions Crown dimensions
 * The crown is generally described by crown length, diameter and a shape function that
 * defines the extension of the crown in each canopy layer. The concept is descript in 
 * @cite grote_pretzsch:2002a.
 * The resulting volume is also used to calculate the total demand of branches. Since
 * it is assumed that foliage biomass scales linearly with crown volume, the shape
 * function used to describe the relative canopy volume of each layer and the foliage 
 * biomass distribution within the canopy is the same. It is documended under 
 * @ref veglibs_biomass_distribution.
 */


 /*!
  * @page veglibs
  * @subsection veglibs_branch_fraction Branch fraction
  * The branch fraction is used to calculate the stem biomass and volume that is
  * needed for tree dimensional growth (@ref treedyn_growth). It is EITHER defined
  * using the crown size by assuming a specific branch density per unit volume
  * (@ref veglibs_branch_fraction_volume) or based on species-specific limit values
  * (@ref veglibs_branch_fraction_diameter). See option settings in @ref psimoptions.
  */


/*!
 * @page veglibs
 * @subsubsection veglibs_branch_fraction_volume Branch fraction from canopy volume
 *  The crown volume is calculated assuming that every layer can be described as a 
 *  disc with a horizontal extension defined by the biomass distribution function 
 *  @ref veglibs_canopy. The total volume can then be integrated across all canopy
 *  layers.
 * \f[
 *   f\_{branch} = \frac {m\_{branch} } { (m\_{stem} + m\_{branch}) }
 * \f]
 * \f[
 *   m\_{branch} = DBRANCH \cdot can\_{vol}
 * \f]
 * \f[
 *   can\_{vol} = \int ( ( \frac {fFol_{fl} } {fFol\_{max} } \cdot crown\_{diam} )^{2} \cdot PI \cdot 0.25 \cdot hfl_{fl} )
 * \f]
 * with:
 * - fl: counter for canopy layers (1 - flmax)
 * - fFol: crown extension relative to maximum crown diameter (0 - 1)
 * - crown_diam: maximum crown diameter (see @ref veglibs_crown-diameter)
 * - hfl: height of the canopy layer (m)
 * - m_branch, m_stem: branch and stem biomass, respectively (kg m2 ground)
 * - PI: constant (3.1416)
 * - DBRANCH: species-specific parameter describing the density of branches within the canopy volume (kg m3)
 */
double
ldndc::LD_Allometry::branch_fraction_from_canopy_volume(
                                                MoBiLE_Plant * _vt)
{
    // maximum foliage fraction of canopy
    double const f_fol_max( _vt->f_fol_maximum());

    // crown diameter [m]
    double const cd( _vt->cdr * _vt->dbh);

    double h_cum( 0.0);
    double canopy_volume( 0.0);
    for ( size_t  fl = 0; fl < _vt->nb_foliagelayers(); fl++)
    {
        h_cum += ph_.h_fl[fl];
        if ( cbm::flt_greater_equal( h_cum, _vt->height_at_canopy_start))
        {
            canopy_volume += cbm::sqr(_vt->fFol_fl[fl] / f_fol_max * cd) * cbm::PI * 0.25 * ph_.h_fl[fl];
            if ( cbm::flt_greater_equal( h_cum, _vt->height_max))
            {
                break;
            }
        }
    }

    double const branch_wood_per_tree( (*_vt)->DBRANCH() * canopy_volume);
    double const stem_wood_per_tree (stem_volume(
                                                 _vt->height_max,
                                                 _vt->dbh,
                                                 _vt->dbas,
                                                 (*_vt)->CONIFEROUS(),
                                                 (*_vt)->TAP_P1(),
                                                 (*_vt)->TAP_P2(),
                                                 (*_vt)->TAP_P3()) * (*_vt)->DSAP() * cbm::DM3_IN_M3);

    if ( cbm::flt_greater_zero( stem_wood_per_tree + branch_wood_per_tree ))
    {
        // limit branch fraction by 90%
        return cbm::bound_max( branch_wood_per_tree / (stem_wood_per_tree + branch_wood_per_tree), 0.9);
    }
    else
    {
        // return branch fraction of young trees in the absence of aboveground wood biomass
        return (*_vt)->FBRAF_Y();
    }
}


/*!
 * @page veglibs
 * @subsubsection veglibs_branch_fraction_diameter Branch fraction from stem diameter
 * If this option is selected (@ref psimoptions), the branch fraction is empirically related
 * to stem diameter at breast height (dbh). Since the development of this term is different
 * in young seedlings (where no dbh is available) and mature trees, branch fraction is 
 * calculated as the maximum of two functions:
 * \f[
 *   f\_{branch} = max(fbranch\_{young}, fbranch\_{mature})
 * \f]
 * For young trees, a maximumum value is intiated that decreases steeply with increasing size,
 * expressed as diameter at ground height.
 * \f[
 *   fbranch\_{young} = FBRAF\_{Y} \cdot ( 1.0 - exp( FSLOPE\_{Y} \cdot dbas) )^{FEXP\_{Y}}
 * \f]
 * After a minimum branch fraction has reached, it is slowly increasing again approaching a
 * final value for mature trees.
 * \f[
 *   fbranch\_{mature} = FBRAF\_{M} + (1.0 - FBRAF\_{M}) \cdot exp(FSLOPE\_{M} \cdot \frac {dbh}{DIAMMAX} )
 * \f]
 * with
 * - dbas, dbh: stem diameter at ground height and breast height (1.3m), respectively (m)
 * - FBRAF_Y, FSLOPE_Y, FEXP_Y: species-specific allometric paramters for branch development in young trees
 * - FBRAF_M, FSLOPE_M: species-specific allometric paramters for branch development in more mature trees
 * - DIAMMAX: tree diameter at which a mature state is defined (m)
 */
double
ldndc::LD_Allometry::branch_fraction_from_diameter(
                                           species::species_group_e _group,
                                           double _FBRAF_M,
                                           double _FBRAF_Y,
                                           double _DIAMMAX,
                                           double _dbh,
                                           double _dbas)
{
    if ( _group != SPECIES_GROUP_WOOD)
    {
        return  0.0;
    }

    double const FSLOPE_M = -10.0; // slope parameter for mature branch development
    double const FSLOPE_Y = -50.0; // slope parameter for young branch development
    double const FEXP_Y = 5.0;     // exponential parameter for young branch development

    double f_branch( _FBRAF_M);

    if (   cbm::flt_greater_zero( _dbh)
        && cbm::flt_greater_zero( _FBRAF_M)
        && cbm::flt_greater_zero( _DIAMMAX))
    {
        f_branch = _FBRAF_M + (1.0 - _FBRAF_M) * exp(FSLOPE_M * _dbh/ _DIAMMAX);
    }

    double  fbranch_young( _FBRAF_Y * pow(1.0 - exp( FSLOPE_Y * _dbas), FEXP_Y));

    if (f_branch > fbranch_young)
    {
        f_branch = fbranch_young;
    }

    return  f_branch;
}


/*!
 * @details
 */
double
ldndc::LD_Allometry::area_fraction_open_range(
                                              MoBiLE_Plant * _vt,
                                              size_t _fl)
{
    // maximum foliage fraction of canopy
    double f_fol_max( _vt->f_fol_maximum());

    if ( cbm::flt_greater_zero( f_fol_max))
    {
        // crown diameter of open range
        double const cdr_open_range( crown_diameter_ratio_open_range(
                                                                     _vt->dbh,
                                                                     _vt->height_max,
                                                                     (*_vt)->CDR_P1(),
                                                                     (*_vt)->CDR_P2(),
                                                                     (*_vt)->CDR_P3()));

        // maximum area cover fraction of open range
        double const f_area_open_range( tree_crown_ground_coverage(
                                                                   _vt->dbh,
                                                                   _vt->tree_number,
                                                                   cdr_open_range) * cbm::HA_IN_M2);

        // area cover fraction of open range in considered layer
        return f_area_open_range * _vt->fFol_fl[_fl] / f_fol_max;
    }
    else
    {
        return 0.0;
    }
}


/*!
 * @page veglibs
 * @subsection veglibs_crown-diameter Crown diameter
 * Crown diameter calculation is based on the open-range calculations that depend 
 * on breast-height diameter and tree height as presented by Condes and Sterba 
 * @cite condes_sterba:2005a.
 * \f[
 *   crown\_{diam} = CDR\_{P1} + (CDR\_{P2} \cdot log(dbh)) + (CDR\_{P3} \cdot log(height))
 * \f]
 * with
 * - dbh:    breast-height (1.3m) diameter (m) (limited to values abouve 1cm)
 * - height: tree height (m)
 * - CDR_P1, -P2, -P3: species specific allometry parameters
 * 
 * In case that the canopy coverage of crowns calculated for open-grown trees is
 * larger that a predefined species-specific threshold value (MORDCROWD), the crown
 * diameter is reduced by a reduction factor that restricts the coverage to the 
 * threshold.
 */
double
ldndc::LD_Allometry::crown_diameter(
                                double _d,
                                double _h,
                                double _cdr_p1,
                                double _cdr_p2,
                                double _cdr_p3)
{
    double const d_min( 0.01); // crown diameter limited by 1cm
    return exp(   _cdr_p1
               + (_cdr_p2 * log( cbm::bound_min( d_min, _d)))
               + (_cdr_p3 * log( _h)));
}


/*!
 * @brief
 *  Returns the ratio between open-range crown diameter and stem diameter at breast height
 */
double
ldndc::LD_Allometry::crown_diameter_ratio_open_range(
                                                 double _dbh,
                                                 double _h,
                                                 double _cdr_p1,
                                                 double _cdr_p2,
                                                 double _cdr_p3)
{
    return crown_diameter( _dbh, _h, _cdr_p1, _cdr_p2, _cdr_p3) / cbm::bound_min( 0.01, _dbh);
}


/*!
 * @details
 *  Returns the ratio between actual crown diameter and stem diameter at breast height
 *  considering a reduction factor that depends on stand density.
 */
double
ldndc::LD_Allometry::crown_diameter_ratio(
                                      MoBiLE_Plant * _vt)
{
    // area needed to for full coverage of one open-range grown tree
    double  f_tree_area_max( 0.0);

    // area needed to for full coverage of all open-range grown trees in one cohort
    double  f_stand_area_max( 0.0);

    // cumulative canopy height
    double  h_cum( 0.0);

    for ( size_t  fl = 0; fl < m_setup.canopylayers(); fl++)
    {
        h_cum += ph_.h_fl[fl];

        if ( cbm::flt_greater_equal( h_cum, _vt->height_at_canopy_start))
        {
            // area needed to for full coverage of all open-range grown trees in the current foliage layer
            double  f_stand_area_fl( 0.0);

            for ( TreeIterator w = this->m_veg->groupbegin< species::wood >();
                 w != this->m_veg->groupend< species::wood >();  ++w)
            {
                f_stand_area_fl += area_fraction_open_range( (*w), fl);
            }

            if ( f_stand_area_fl > f_stand_area_max)
            {
                f_tree_area_max = area_fraction_open_range( _vt, fl);
                f_stand_area_max = f_stand_area_fl;
            }
        }

        if ( cbm::flt_greater_equal( h_cum, _vt->height_max))
        {
            break;
        }
    }

    // reduction of crown diameter ratio due to limited space
    if ( cbm::flt_greater( f_stand_area_max, (*_vt)->MORTCROWD()))
    {
        // crown coverage beyond the threshold value of stand density
        double const f_stand_area_reduction( f_stand_area_max - (*_vt)->MORTCROWD());

        // the fraction of tree area in relation to stand area
        double const tree_fraction( f_tree_area_max / f_stand_area_max);

        // the tree area per tree is reduced */
        double const tree_area_reduction( tree_fraction * f_stand_area_reduction);

        // reduction factor
        double const reduction_factor( sqrt((f_tree_area_max - tree_area_reduction) / f_tree_area_max));

        return crown_diameter_ratio_open_range(
                                           _vt->dbh,
                                           _vt->height_max,
                                           (*_vt)->CDR_P1(),
                                           (*_vt)->CDR_P2(),
                                           (*_vt)->CDR_P3()) * reduction_factor;
    }

    // in case the stand density is lower than the threshold value, the crown diameter ratio is calculated under open range conditions
    else
    {
        return crown_diameter_ratio_open_range(
                                           _vt->dbh,
                                           _vt->height_max,
                                           (*_vt)->CDR_P1(),
                                           (*_vt)->CDR_P2(),
                                           (*_vt)->CDR_P3());
    }
}


/*!
 * @page veglibs
 * @subsection veglibs_crown-length Crown length
 *
 * For all vegetation types other than woody species, crown length is equal to height.
 * For trees, it is calculated depending on the setup-option selected (options depicted
 * in 'PSIM' descriptions). 
 * 
 * The first option is to calculate crown length based on an allometric relationship to 
 * tree height and diameter at breast height (= option height):
 * \f[
 *   crown\_{length} = min(HLIMIT, height\_{eff} \cdot CB )
 * \f]
 * \f[
 *   height\_{eff} = height - HREF \cdot (1.0 - \frac {dbh}{DIAMMAX} )
 * \f]
 * with:
 *  \f$ \text{h}_{\text{cs}} \f$: height at canopy start \n
 *  \f$ \text{h}_{\text{max}} \f$: height at canopy top \n
 *  \f$ DIAMMAX \f$: tree diameter at which a mature state is defined \n
 *  \f$ CB \f$: relation between crown base height and top height at a mature state (dbh >= DIAMMAX) \n
 *  \f$ HREF \f$: tree height at which crown base height starts to increase \n
 *
 */
double
ldndc::LD_Allometry::crown_length_from_parameter(
    double _height,
    double _dbh,
    double _href,
    double _diammax,
    double _cb)
{
    double const height_eff(_height - _href * cbm::bound_min(0.0, 1.0 - _dbh / _diammax));
    double const cl(cbm::bound_min(cbm::HLIMIT, height_eff * _cb));

    // limit crown length by total tree height
    return cbm::bound_max(cl, _height);
}


/*!
 * @page veglibs
 * The second option is to calculate crown length from crown diameter (option 'diameter')
 * according to @cite thorpe:2010a .
 * \f[
 *   crown\_{length} = CL\_{P1} \cdot ( crown\_{diam} ) ^ {CL\_{P2} }
 * \f]
 * with:
 * - crown_diam:  crown diameter (m)
 * - CL_P1, CLP2: species-specific allometric parameters
 *
 * Crown length is limited by tree height. The crown base height (or crown start) is defined as
 * tree height - crown length.
 */
double
ldndc::LD_Allometry::crown_length_from_crown_diameter(
    double _height,
    double _cd,
    double _cl_p1,
    double _cl_p2)
{
    double cl(cbm::bound_min(cbm::HLIMIT, _cl_p1 * pow(_cd, _cl_p2)));

    // limit crown length to total tree height
    return cbm::bound_max(cl, _height);
}


/*!
 * @brief
 * Tree crown shape
 */
double
ldndc::crown_shape_parameter(
    double  _length,
    double  lref,
    double  ps)
{
    // crown/ root profile shape value shifts with crown length/ root profile depth
    double  ps2(ps);
    if (_length <= 1.0)
    {
        ps2 = 1.0;
    }
    else if (lref > 1.0)
    {
        ps2 = cbm::bound_max(1.0 + (ps - 1.0) * (_length - 1.0) / (lref - 1.0), ps);
    }

    return ps2;
}



/*!
 * @brief
 *
 */
double
ldndc::LD_Allometry::area_fraction_wood(
                                    double _dbh,
                                    double _tree_number,
                                    double _crown_diameter_ratio)
{
    return cbm::bound( 0.0,
                      tree_crown_ground_coverage(
                                         _dbh,
                                         _tree_number,
                                         _crown_diameter_ratio)
                      * cbm::HA_IN_M2,
                      1.0);
}


/*!
 * @brief
 *      returns ground coverage of tree crowns in m2 per hectare
 */
double
ldndc::LD_Allometry::tree_crown_ground_coverage(
                                            double _dbh,
                                            double _tree_number,
                                            double _crown_diameter_ratio)
const
{
    //ground area coverage of one woody vegetation type
    return  cbm::sqr( _dbh * _crown_diameter_ratio) * cbm::PI * 0.25 * _tree_number;
}

