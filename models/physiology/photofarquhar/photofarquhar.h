/*!
 * @file
 * @author
 *    Ruediger Grote
 */

#ifndef  LM_PHYSIOLOGY_PHOTOFARQUHAR_H_
#define  LM_PHYSIOLOGY_PHOTOFARQUHAR_H_

#include  "mbe_legacymodel.h"
#include  "state/mbe_state.h"
#include  "physiology/photofarquhar/berryball.h"

namespace ldndc {

class  LDNDC_API  PhysiologyPHOTOFARQUHAR  :  public  MBE_LegacyModel
{
    LMOD_EXPORT_MODULE_INFO(PhysiologyPHOTOFARQUHAR,"physiology:photofarquhar","Physiology Photo-FARQUHAR");

    /*! reference temperature  [K] */
    static double const  TK25;
    /*! temperature under standard conditions [K] */
    static double const  TK30;
    /*! temperature under standard conditions [C] */
    static double const  T30;
    /*! triose phosphate production efficienty of photosynthesis  [-] (Harley et al. 1992)*/
    static double const  TPU25;
    /*! ratio of diffusivities of H2O/CO2 (Wullschleger 1993, cit. in Leuning et al. 1995: 2.1) */
    static double const  FGC;
    /*! CO2 turnover rate at 25 oC  [gCO2 gRub-1 d-1] */
    static double const  TO25;
    /*! detoxified fraction of O3 flux */
    static double const  FDET;
    /*! detoxification cost coefficient  [g g-1] */
    static double const  CDET;
    /*! ozone damage coefficient  [g g-1] */
    static double const  PHI;
    /*! fraction leaf assimilates used in repair  [g g-1] */
    static double const  FREP;
    /*! repair cost coefficient  [g g-1] */
    static double const  CREP;
    /*! maximum biomass formation efficiency  [gDW g?] */
    static double const  GAMMA0;
    /*! CLM4 model description (Oleson et al. 2010); Collatz et al. 1992 (cited in CLM4 model description): 18000; Sellers et al. 1996 (cited in CLM4 model description): 20000 */
    static double const  C4WPFACT;
    
    enum  stomatal_conductance_e
    {
        STOMATAL_CONDUCTANCE_JARVIS,
        STOMATAL_CONDUCTANCE_BERRY_BALL,

        STOMATAL_CONDUCTANCE_CNT,
        STOMATAL_CONDUCTANCE_NONE
    };
    static char const * const  STOMATAL_CONDUCTANCE_NAMES[STOMATAL_CONDUCTANCE_CNT];

    public:
        PhysiologyPHOTOFARQUHAR(
                MoBiLE_State *,
                cbm::io_kcomm_t *,
                timemode_e);

        ~PhysiologyPHOTOFARQUHAR();


        lerr_t  configure( ldndc::config_file_t const *);

        lerr_t  initialize();

        lerr_t  solve();

        lerr_t  finalize() { return  LDNDC_ERR_OK; }

        lerr_t  sleep() { return  LDNDC_ERR_OK; }
        lerr_t  wake() { return  LDNDC_ERR_OK; }

    protected:
        /*! relative canopy conductance */
        void  CalcConductance( MoBiLE_Plant *);

        /*! carbon uptake ( photosynthesis, gross assimilation) */
        void  CalcPhotosynthesisJARVIS( MoBiLE_Plant *);

        void  send_state( MoBiLE_Plant *);
        void  receive_state( MoBiLE_Plant *);

// sk:unused        /*! ozone damage function */
// sk:unused        double  CalcO3Damage(
// sk:unused                species_t const &, size_t vt, size_t fl, double gc, double ci);

    private:
        input_class_setup_t const & se_;
        input_class_climate_t const &  cl_;
        substate_airchemistry_t const &  ac_;
        substate_microclimate_t const &  mc_;
        substate_physiology_t &  ph_;
        substate_watercycle_t const &  wc_;

        MoBiLE_PlantVegetation *  m_veg;

        BerryBall m_berryball;
    
        stomatal_conductance_e  stomatal_conductance_;
};
} /*namespace ldndc*/

#endif  /*  !LM_PHYSIOLOGY_PHOTOFARQUHAR_H_  */

