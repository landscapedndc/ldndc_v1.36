/*!
 * @file
 * @brief
 *    This gas exchange module calculates stomatal conductance and photsynthesis.
 *
 * @author
 *    - Ruediger Grote
 */

#include  "physiology/photofarquhar/photofarquhar.h"
#include  <cfgfile/cbm_cfgfile.h>


LMOD_MODULE_INFO(PhysiologyPHOTOFARQUHAR,TMODE_SUBDAILY,LMOD_FLAG_USER);
namespace ldndc {

/* configuration options for this module */
#define  PHOTOFARQUHAR_CONF_OPT_STOM_COND  "stomatal_conductance"    /* enum (string) */

/* definition of module specific parameters */
double const  PhysiologyPHOTOFARQUHAR::TK25         = 298.16;
double const  PhysiologyPHOTOFARQUHAR::TK30         = 303.16;
double const  PhysiologyPHOTOFARQUHAR::T30          = 30;
double const  PhysiologyPHOTOFARQUHAR::TPU25        = 23.59;
double const  PhysiologyPHOTOFARQUHAR::FGC          = 1.6;
double const  PhysiologyPHOTOFARQUHAR::TO25         = 138.0;
double const  PhysiologyPHOTOFARQUHAR::FDET         = 0.9;
double const  PhysiologyPHOTOFARQUHAR::CDET         = 0.375;
double const  PhysiologyPHOTOFARQUHAR::PHI          = 10.0;
double const  PhysiologyPHOTOFARQUHAR::FREP         = 0.05;
double const  PhysiologyPHOTOFARQUHAR::CREP         = 18.0;
double const  PhysiologyPHOTOFARQUHAR::GAMMA0       = 0.4;
double const  PhysiologyPHOTOFARQUHAR::C4WPFACT     = 4000.0;

char const * const  PhysiologyPHOTOFARQUHAR::STOMATAL_CONDUCTANCE_NAMES[PhysiologyPHOTOFARQUHAR::STOMATAL_CONDUCTANCE_CNT] = { "jarvis", "berry/ball"};


/*!
* @page photofarquhar
* @section PhysiologyPHOTOFARQUHAR Farquhar model 
* This gas exchange module calculates stomatal conductance and photsynthesis based
* on @cite farquhar:1980a. The implementation is according to @cite caemmerer:2009a.
* \f[
*   A = \left( 1.0 - \frac {c^{\ast}} {c_i} \right) \cdot min(w_c, w_j, w_p) - rd
* \f]
*
* with
*  - \f$ A        \f$: assimilation rate
*  - \f$ c^{\ast} \f$: carbon dioxide compensation point
*  - \f$ c_i      \f$: internal carbon dioxide concentration
*  - \f$ w_c      \f$: carboxylation limited assimilation rate
*  - \f$ w_j      \f$: electron transport limited assimilation rate
*  - \f$ w_p      \f$: phosphorylation limited assimilation rate
*  - \f$ rd       \f$: dark respiration
*
* The calculation of \f$ w_c, w_j \f$ and \f$ w_p  \f$ depend on \f$ c_i,
* w_c \f$ additionally on \f$ o_i \f$,
* \f$ w_j \f$ and \f$ w_p \f$ additionally on the compensation point:
* \f[
*   w_c = vc \cdot \frac {c_i} {c_i + kc \cdot (1.0 + \frac {o_i}{ko}})
* \f]
* \f[
*   w_j =  \frac {j} { 4.0 + 8.0 \cdot  \frac {c^{\ast}}{c_i} }
* \f]
* \f[
*   w_p =  \frac { 3.0 \cdot tpu }{ 1.0 -  \frac {c^{\ast}}{c_i} }
* \f]
*
* with \f$ c^{\ast} \f$ according to @cite caemmerer_farquhar:1981a :
* \f[
*   c^{\ast} = 0.5 \cdot vo \cdot kc \cdot \frac {o_i} {vc \cdot ko}
* \f]
* 
* with
*  - \f$ vc  \f$: carboxylation activity (umol m-2 s-1)
*  - \f$ vo  \f$: oxygenation activity (umol m-2 s-1)
*  - \f$ j   \f$: electron transport rate (umol m-2 s-1)
*  - \f$ tpu \f$: rate of phosphate release (umol m-2 s-1)
*  - \f$ ko  \f$: Michaelis Menten constant for O2 (mmol mol-1, empirically determined according to Long 1991 @cite long:1991a)
*  - \f$ kc  \f$: Michaelis Menten constant for CO2 (mmol mol-1)
*  - \f$ oi  \f$: intercellular concentration of oxygen (mmol mol-1)
* 
* All enzyme activities \f$ (vc, vo, j, tpu, rd) \f$ rates are calculated using canopy layer-specific temperature
* and radiation. Regarding temperature dependencies an Arrhenius function is used @cite long:1991a that has
* been corrected for high temperatures according to @cite medlyn:2002a. For these dependencies, process-specific 
* activation energies (AEVC, AEVO, AEJM, AETP, AERD), and activity rates at 25oC are parameterized (VCMAX25, TPU25)
* or put into a fixed relation to carboxylation activity (QJVC, QVOVC, QRD25). Similarly, also for Michaelis-Menten
* constants the rates at 25oC have been parameterized (KO25, KC25) and temperature corrected in the same way as 
* carboxylation and oxygenation (using AEKC, AEKO). For C4 plants simplified assumptions are applied for
* carboxylation dependencies @cite harley:1992a and phosphorylation limits @cite collatz:1992a.
*
* Radiation intensity is needed to define electron transport rate only @cite evans:1989a, using a species-specific
* parameter to define the slope of the relationship (THETA). Furthermore, it is assumed that the potential (parameterized)
* carboxylation activity, electron transport rate and photorespiration at 25oC is reduced with increasing canopy depth
* (linked to specific leaf area) and can be further reduced if nitrogen supply is not sufficient to reach a predefined
* target value. In addition, the enzymatic activities depend on phenological developments, and are eventually reduced
* by heat, frost or drought stress (@ref PSIM_PhotosynthesisRates).

* @section options Modes
* Internal carbon dioxide concentration is calculated with the Berry-Ball @cite ball:1987a
* optimization approach (standard) or the Jarvis @cite jarvis:1976a multiplicative approach (optional).
*
* @author
*  - Ruediger Grote
*
*/
PhysiologyPHOTOFARQUHAR::PhysiologyPHOTOFARQUHAR( MoBiLE_State *  _state,
                                                  cbm::io_kcomm_t *_io_kcomm,
                                                  timemode_e  _timemode):
    MBE_LegacyModel( _state, _timemode),
    se_( _io_kcomm->get_input_class_ref< input_class_setup_t >()),
    cl_( _io_kcomm->get_input_class_ref< input_class_climate_t >()),
    ac_( _state->get_substate_ref< substate_airchemistry_t >()),
    mc_( _state->get_substate_ref< substate_microclimate_t >()),
    ph_( _state->get_substate_ref< substate_physiology_t >()),
    wc_( _state->get_substate_ref< substate_watercycle_t >()),

    m_veg( &_state->vegetation),
    m_berryball(1.0 / LD_RtCfg.clk->time_resolution(), se_.canopylayers()),

    stomatal_conductance_( STOMATAL_CONDUCTANCE_NONE)
{
}


/*!
 * @details
 *    Nothing to delete.
 */
PhysiologyPHOTOFARQUHAR::~PhysiologyPHOTOFARQUHAR()
{
}


/*!
 * @details
 *  Configuration of stomatal conductance model: \n
 *  - Berry Ball
 *  - Jarvis
 *
 * @param[in] _cf configuration
 */
lerr_t
PhysiologyPHOTOFARQUHAR::configure(
                                   ldndc::config_file_t const *  _cf)
{
    // read stomatal conductance types from configuration file
    std::string  stom_cond_type;
    CF_LMOD_QUERY(_cf,PHOTOFARQUHAR_CONF_OPT_STOM_COND,stom_cond_type,STOMATAL_CONDUCTANCE_NAMES[STOMATAL_CONDUCTANCE_BERRY_BALL]);

    stomatal_conductance_ = STOMATAL_CONDUCTANCE_NONE;
    cbm::find_enum( stom_cond_type.c_str(), STOMATAL_CONDUCTANCE_NAMES, STOMATAL_CONDUCTANCE_CNT, &stomatal_conductance_);
    if ( stomatal_conductance_ == STOMATAL_CONDUCTANCE_NONE)
    {
        KLOGERROR( "photo-farquhar: unknown stomatal conductance type in configuration file  [type="+stom_cond_type+"]");
        return  LDNDC_ERR_FAIL;
    }
    KLOGDEBUG( "(",this->name(), ") selecting stomatal conductance type: ", stom_cond_type);

    return  LDNDC_ERR_OK;
}



/*!
 * @details
 *    Nothing to initialize.
 */
lerr_t
PhysiologyPHOTOFARQUHAR::initialize()
{
    return  LDNDC_ERR_OK;
}



/*!
 * @details
 * Calling emission class functions.
 */
lerr_t
PhysiologyPHOTOFARQUHAR::solve()
{
    /* selection of mechanism for calculating stomatal conductance */
    switch ( stomatal_conductance_)
    {
        case  STOMATAL_CONDUCTANCE_JARVIS:
        {
            for( PlantIterator  vt = this->m_veg->begin(); vt != this->m_veg->end(); ++vt)
            {
                CalcConductance( (*vt));
                CalcPhotosynthesisJARVIS( (*vt));
            }
            break;
        }
        case  STOMATAL_CONDUCTANCE_BERRY_BALL:
        {
            for( PlantIterator  vt = this->m_veg->begin(); vt != this->m_veg->end(); ++vt)
            {
                MoBiLE_Plant *p = (*vt);

                send_state( p);

                m_berryball.solve();

                receive_state( p);
            }
            break;
        }
        default:
        {
            KLOGFATAL( "how did you get here?");
        }
    }

    return  LDNDC_ERR_OK;
}

void
PhysiologyPHOTOFARQUHAR::send_state( MoBiLE_Plant *p)
{
    for (size_t fl = 0; fl < p->nb_foliagelayers(); ++fl)
    {
        m_berryball.vcAct25_fl[fl] = p->vcAct25_fl[fl];
        m_berryball.jAct25_fl[fl] = p->jAct25_fl[fl];
        m_berryball.rdAct25_fl[fl] = p->rdAct25_fl[fl];

        m_berryball.sla_fl[fl] = p->sla_fl[fl];
        m_berryball.lai_fl[fl] = p->lai_fl[fl];
        m_berryball.fFol_fl[fl] = p->fFol_fl[fl];

        m_berryball.vpd_fl[fl] = mc_.vpd_fl[fl];
        m_berryball.rh_fl[fl] = cl_.rel_humidity_subday( lclock_ref());
        m_berryball.temp_fl[fl] = mc_.temp_fl[fl];
        m_berryball.parsun_fl[fl] = mc_.parsun_fl[fl];
        m_berryball.parshd_fl[fl] = mc_.parshd_fl[fl];
        m_berryball.tFol_fl[fl] = mc_.tFol_fl[fl];
        m_berryball.co2_concentration_fl[fl] = ac_.ts_co2_concentration_fl[fl];
        m_berryball.sunlitfoliagefraction_fl[fl] = mc_.ts_sunlitfoliagefraction_fl[fl];
    }
    for (size_t fl = p->nb_foliagelayers(); fl < se_.canopylayers(); ++fl)
    {
        m_berryball.vcAct25_fl[fl] = 0.0;
        m_berryball.jAct25_fl[fl] = 0.0;
        m_berryball.rdAct25_fl[fl] = 0.0;

        m_berryball.sla_fl[fl] = 0.0;
        m_berryball.lai_fl[fl] = 0.0;
        m_berryball.fFol_fl[fl] = 0.0;

        m_berryball.vpd_fl[fl] = 0.0;
        m_berryball.temp_fl[fl] = 0.0;
        m_berryball.parsun_fl[fl] = 0.0;
        m_berryball.parshd_fl[fl] = 0.0;
        m_berryball.tFol_fl[fl] = 0.0;
        m_berryball.co2_concentration_fl[fl] = 0.0;;
        m_berryball.sunlitfoliagefraction_fl[fl] = 0.0;;
    }

    m_berryball.fl_cnt = p->nb_foliagelayers();
    m_berryball.nd_airpressure = mc_.nd_airpressure;

    m_berryball.f_h2o = p->f_h2o;
    m_berryball.f_fac = p->f_fac;
    m_berryball.mFol = p->mFol;
    m_berryball.height_max = p->height_max;

    m_berryball.CSR_REF = (*p)->CSR_REF();
    m_berryball.CWP_REF = (*p)->CWP_REF();
    m_berryball.PSI_REF = (*p)->PSI_REF();
    m_berryball.PSI_EXP = (*p)->PSI_EXP();
    m_berryball.RPMIN = (*p)->RPMIN();
    
    m_berryball.KC25 = (*p)->KC25();
    m_berryball.AEKC = (*p)->AEKC();
    m_berryball.AEVO = (*p)->AEVO();
    m_berryball.KO25 = (*p)->KO25();
    m_berryball.AEKO = (*p)->AEKO();
    m_berryball.AEVC = (*p)->AEVC();
    m_berryball.QVOVC = (*p)->QVOVC();
    m_berryball.GSMIN = (*p)->GSMIN();
    m_berryball.GSMAX = (*p)->GSMAX();
    m_berryball.H2OREF_GS = (*p)->H2OREF_GS();
    m_berryball.SLOPE_GSA = (*p)->SLOPE_GSA();
    m_berryball.AERD = (*p)->AERD();
    m_berryball.SDJ = (*p)->SDJ();
    m_berryball.HDJ = (*p)->HDJ();
    m_berryball.THETA = (*p)->THETA();
    m_berryball.AEJM = (*p)->AEJM();
    m_berryball.QJVC = (*p)->QJVC();
    m_berryball.C4_TYPE = (*p)->C4_TYPE();
}


void
PhysiologyPHOTOFARQUHAR::receive_state( MoBiLE_Plant *p)
{
    for (size_t fl = 0; fl < p->nb_foliagelayers(); ++fl)
    {
        p->ko2_fl[fl] = m_berryball.ko2_fl[fl];
        p->kco2_fl[fl] = m_berryball.kco2_fl[fl];
        p->co2i_fl[fl] = m_berryball.co2i_fl[fl];
        p->o2i_fl[fl] = m_berryball.o2i_fl[fl];
        p->rd_fl[fl] = m_berryball.rd_fl[fl];
        p->co2comp25_fl[fl] = m_berryball.co2comp25_fl[fl];
        p->jMax_fl[fl] = m_berryball.jMax_fl[fl];
        p->jPot_fl[fl] = m_berryball.jPot_fl[fl];
        p->vcMax_fl[fl] = m_berryball.vcMax_fl[fl];
        p->vcAct_fl[fl] = m_berryball.vcAct_fl[fl];
        p->carbonuptake_fl[fl] = m_berryball.carbonuptake_fl[fl];
        p->relativeconductance_fl[fl] = m_berryball.relativeconductance_fl[fl];

        p->ko2_std_fl[fl] = m_berryball.ko2_std_fl[fl];
        p->kco2_std_fl[fl] = m_berryball.kco2_std_fl[fl];
        p->co2i_std_fl[fl] = m_berryball.co2i_std_fl[fl];
        p->o2i_std_fl[fl] = m_berryball.o2i_std_fl[fl];
        p->co2comp25_std_fl[fl] = m_berryball.co2comp25_std_fl[fl];
        p->jMax_std_fl[fl] = m_berryball.jMax_std_fl[fl];
        p->vcMax_std_fl[fl] = m_berryball.vcMax_std_fl[fl];
        p->vcMax25_fl[fl] = m_berryball.vcMax25_fl[fl];
        p->jMax25_fl[fl] = m_berryball.jMax25_fl[fl];
    }
    for (size_t fl = p->nb_foliagelayers(); fl < se_.canopylayers(); ++fl)
    {
        p->ko2_fl[fl] = 0.0;
        p->kco2_fl[fl] = 0.0;
        p->co2i_fl[fl] = 0.0;
        p->o2i_fl[fl] = 0.0;
        p->rd_fl[fl] = 0.0;
        p->co2comp25_fl[fl] = 0.0;
        p->jMax_fl[fl] = 0.0;
        p->jPot_fl[fl] = 0.0;
        p->vcMax_fl[fl] = 0.0;
        p->vcAct_fl[fl] = 0.0;
        p->carbonuptake_fl[fl] = 0.0;
        p->relativeconductance_fl[fl] = 0.0;

        p->ko2_std_fl[fl] = 0.0;
        p->kco2_std_fl[fl] = 0.0;
        p->co2i_std_fl[fl] = 0.0;
        p->o2i_std_fl[fl] = 0.0;
        p->co2comp25_std_fl[fl] = 0.0;
        p->jMax_std_fl[fl] = 0.0;
        p->vcMax_std_fl[fl] = 0.0;
        p->vcMax25_fl[fl] = 0.0;
        p->jMax25_fl[fl] = 0.0;
    }
}

} /*namespace ldndc*/

