/*!
 * @file
 * @author
 *    - Ruediger Grote
 *    - David Kraus
 *    
 * @date
 *    September, 2018
 */

#ifndef  BERRYBALL_H_
#define  BERRYBALL_H_

#include  "ld_modelsconfig.h"
#include  <containers/cbm_vector.h>
#include  "mbe_legacymodel.h"
#include  "physiology/ld_stomatalconductance.h"

namespace ldndc {

class  LDNDC_API  BerryBall
{
    /*! reference temperature  [K] */
    static double const  TK25;
    /*! temperature under standard conditions [K] */
    static double const  TK30;
    /*! temperature under standard conditions [C] */
    static double const  T30;
    /*! triose phosphate production efficienty of photosynthesis  [-] (Harley et al. 1992)*/
    static double const  TPU25;
    /*! ratio of diffusivities of H2O/CO2 (Wullschleger 1993, cit. in Leuning et al. 1995: 2.1) */
    static double const  FGC;
    /*! CO2 turnover rate at 25 oC  [gCO2 gRub-1 d-1] */
    static double const  TO25;
    /*! detoxified fraction of O3 flux */
    static double const  FDET;
    /*! detoxification cost coefficient  [g g-1] */
    static double const  CDET;
    /*! ozone damage coefficient  [g g-1] */
    static double const  PHI;
    /*! fraction leaf assimilates used in repair  [g g-1] */
    static double const  FREP;
    /*! repair cost coefficient  [g g-1] */
    static double const  CREP;
    /*! maximum biomass formation efficiency  [gDW g?] */
    static double const  GAMMA0;
    /*! CLM4 model description (Oleson et al. 2010); Collatz et al. 1992 (cited in CLM4 model description): 18000; Sellers et al. 1996 (cited in CLM4 model description): 20000 */
    static double const  C4WPFACT;

public:
    BerryBall(
              double /* day fraction */,
              size_t /* maximum foliage layers*/);
    ~BerryBall();

public:

    double *ko2_fl;
    double *kco2_fl;
    double *co2i_fl;
    double *o2i_fl;
    double *rd_fl;
    double *co2comp25_fl;
    double *jMax_fl;
    double *jPot_fl;
    double *vcMax_fl;
    double *vcAct_fl;
    double *carbonuptake_fl;
    double *relativeconductance_fl;

    double *ko2_std_fl;
    double *kco2_std_fl;
    double *co2i_std_fl;
    double *o2i_std_fl;
    double *co2comp25_std_fl;
    double *jMax_std_fl;
    double *vcMax_std_fl;
    double *vcAct25_fl;
    double *vcMax25_fl;
    double *jMax25_fl;
    double *jAct25_fl;
    double *rdAct25_fl;

    double *sla_fl;
    double *lai_fl;
    double *fFol_fl;

    double rplant;
    double psi_mean;
    double psi_12;
    double psi_pd;

    double f_h2o;
    double f_fac;
    double mFol;
    double height_max;

    double CSR_REF;
    double CWP_REF;
    double PSI_REF;
    double PSI_EXP;
    double RPMIN;

    double KC25;    // Michaelis Menten constant for CO2 (umol mol-1) under standard conditions
    double AEKC;
    double AEVO;
    double KO25;    // Michaelis Menten constant for O2 (mmol mol-1) under standard conditions
    double AEKO;
    double AEVC;
    double QVOVC;
    double GSMIN;
    double GSMAX;
    double H2OREF_GS;
    double SLOPE_GSA;
    double AERD;
    double SDJ;
    double HDJ;
    double THETA;
    double AEJM;
    double QJVC;
    double VPDREF;
    bool C4_TYPE;

    size_t fl_cnt;
    double max_foliage_layer;
    double nd_airpressure;
    double day_fraction;
    double *vpd_fl;
    double *rh_fl;
    double *temp_fl;
    double *parsun_fl;
    double *parshd_fl;
    double *tFol_fl;
    double *co2_concentration_fl;
    double *sunlitfoliagefraction_fl;

    /*!
     * @brief
     *     Stomatal conductance method used by PSIM
     */
    stomatalconductance_method_e stomatalconductance_method;
    
    void solve();

    /*!
     * @brief
     *     Resets members
     */
    void
    BerryBallReset();

    /*!
     * @brief
     *      basic farquhar model according to Farquhar 1980 from which the stomatal conductance routine is selected
     */
    void
    BerryBallFarquhar(
                      double _ko,
                      double _kc,
                      double _ci,
                      double _oi,
                      double _rd,
                      double _tpu,
                      double _cstar,
                      double _jpot,
                      double _vcmax,
                      double &_wj,
                      double &_wp,
                      double &_wc,
                      double &_assi);
    
    lerr_t
    set_vegetation_base_state( MoBiLE_Plant *);
    
    lerr_t
    set_vegetation_non_stomatal_water_limitation_state( double const & /* rplant */,
                                                        double const &/* psi_mean */,
                                                        double const & /* psi_pd */);

    lerr_t
    get_vegetation_state( MoBiLE_Plant *);
};

} /* namespace ldndc */

#endif  /*  BERRYBALL_H_  */
