/*!
 * @file
 *  Implementing model proposed by Jarvis
 *
 * @author
 *  Ruediger grote
 * @date ?
 *
 */
#include  "physiology/photofarquhar/photofarquhar.h"
#include  "watercycle/ld_droughtstress.h"

#include  <input/species/species.h>

#include  <math/cbm_math.h>
#include  <constants/cbm_const.h>

namespace ldndc {

/* relative canopy conductance
 *    according to Jarvis 1976 with vapor pressure deficit, 
 *    CO2 air concentration and soil drought considered as
 *    independent influences.
 *
 *    NOTE:
 *    it is assumed that
 *    - limiting soil water affects all canopy layers equally
 */
void
PhysiologyPHOTOFARQUHAR::CalcConductance( MoBiLE_Plant *p)
{
    if ( p->mFol > 0.0)
    {
        // soil drought influence
        DroughtStress  droughtstress;
        droughtstress.fh2o_ref = (*p)->H2OREF_GS();
        double const  h2oFac = droughtstress.linear_threshold( p->f_h2o);

        size_t  fl_cnt( p->nb_foliagelayers());

        for ( size_t  fl = 0; fl < fl_cnt;  ++fl)
        {
            // humidity influence
            double const  vpdFac = std::max( 0.0, 1.0 - mc_.vpd_fl[fl] / (*p)->VPDREF());
            // carbon dioxide influence
            double const  co2Fac = std::max( 0.0, 1.0 - ( 1.0 - (*p)->SLOPE_GSCO2()) * ( ac_.ts_co2_concentration_fl[fl] / cbm::CO2REF - 1.0));
            // relative foliage conductance for H2O (0-1)
            double const  gsrel = h2oFac * vpdFac * co2Fac;
            p->relativeconductance_fl[fl] = ( gsrel > ( (*p)->GSMIN() / (*p)->GSMAX())) ? gsrel : (*p)->GSMIN() / (*p)->GSMAX();
        }
    }
    else
    {
        for ( size_t  fl = 0; fl < se_.canopylayers();  ++fl)
        {
            p->relativeconductance_fl[fl] = 0.0;
        }
    }
}



/* carbon uptake (photosynthesis, gross assimilation)
 *
 * after Martin at al. 2000 (all parameters)
 *    (= mechanistic relation to CO2, temperature and radiation
 *    based on the Farquhar model as described in Evans and Farquhar 1991,
 *    Long 1991, and Sharkey 1985 using Arrhenius functions to describe all
 *    temperature dependencies)
 *
 *    - stomatal conductance calculated after Jarvis 1976 (see function CalcConductance)
 *    - conductance impact implemented after Ethier and Livingston 2004
 *    - vcMax dependency on SLA assumed linear (Niinemets et al. 2002)
 *
 * it is assumed that:
 *    - jMax is linearly related to vcMax
 *    - nitrogen concentration impact VCMAX25 linearily (see module PSIM,
 *      Reich et al. 1995, Aber et al. 1996, Lewis et al. 2004)
 *
 * NOTE:
 *    - Enzyme activity is affected by plant ontogenetic stage
 *    - different nitrogen concentrations across the canopy are not considered
 *      (could be interrelated with sla dependency)
*/
void
PhysiologyPHOTOFARQUHAR::CalcPhotosynthesisJARVIS( MoBiLE_Plant *p)
{
    // sk:off    double 
    // 
    // (0.0);        // irradiance absorbed by photosystem 2 (umol m-2 s-1)
    // sk:off    double jPot(0.0);      // potential rate of electron transport (umol m-2 s-1)
    // sk:off    double oi(0.0);        // intercellular concentration of oxygen (mmol mol-1)
    // sk:off    double ci(0.0);        // intercellular concentration of oxygen (umol mol-1)
    // sk:off    double assi(0.0);      // total photosynthesis rate without stress (umol CO2 m-2LA s-1)
    // sk:off    double fo3(0.0);       // ozone damage impact factor
    // sk:off    double tslength(0.0);  // length of timestep (s)
    // sk:off
    // sk:off    int count,nn,nss;
    // sk:off    double ciOld(0.0);

    // length of timestep (s)
    double const  tslength = double(cbm::SEC_IN_HR * cbm::HR_IN_DAY) / double( this->lclock_ref().time_resolution());

    // RubP saturated rate of carboxylation (umol m-2 s-1)
    double  wc( 0.0);
    // RubP limited rate of carboxylation (umol m-2 s-1)
    double  wj( 0.0);
    // phosphorylation limited rate of carboxylation (umol m-2 s-1)
    double  wp( 0.0);

    for ( size_t  fl = 0;  fl < se_.canopylayers();  ++fl)
    {
        p->ko2_fl[fl] = 0.0;
        p->kco2_fl[fl] = 0.0;
        p->co2i_fl[fl] = 0.0;
        p->o2i_fl[fl] = 0.0;
        p->rd_fl[fl] = 0.0;
        p->co2comp25_fl[fl] = 0.0;
        p->jMax_fl[fl] = 0.0;
        p->jPot_fl[fl] = 0.0;
        p->vcMax_fl[fl] = 0.0;
        p->vcAct_fl[fl] = 0.0;
        p->carbonuptake_fl[fl] = 0.0;
    }

    // conversion term
    double  conv = tslength * cbm::MOL_IN_UMOL * cbm::MC * cbm::KG_IN_G;

    if ( p->mFol > 0.0)
    {
        size_t  fl_cnt( p->nb_foliagelayers());
        for ( size_t  fl = 0;  fl < fl_cnt;  ++fl)
        {
            double  i2( mc_.parsun_fl[fl]);


            double  frad( 1.0); int  nss( 1);  // no light
            if (( mc_.parsun_fl[fl] + mc_.parshd_fl[fl]) > 0.0)
            {
                // shade and sun cycle
                frad = mc_.ts_sunlitfoliagefraction_fl[fl];
                nss  = 2;
            }

            // available carbon reserves for negative assimilation
            double  cava( 0.0);
            if (p->lai_fl[fl] > 0.0)
            {
                cava = p->m_fol_fl(fl) * cbm::CCDM * p->f_fac / (p->lai_fl[fl] * conv * double(this->lclock_ref().time_resolution()));
            }

            for ( int  n = 0;  n < nss;  ++n)
            {
                if (( p->sla_fl[fl] > 0.0) && ( p->vcAct25_fl[fl] > 0.0))
                {
                    // radiation and (preliminary) internal gas concentration
                    double  ci( 0.7 * ac_.ts_co2_concentration_fl[fl]);
                    double  oi( cbm::PO2 * cbm::MMOL_IN_MOL);
                    if ( mc_.tFol_fl[fl] > 25.0)
                    {
                        ci *= ((1.674 - 0.061294 * mc_.tFol_fl[fl] + 0.0011688
                                * cbm::sqr( mc_.tFol_fl[fl]) - 0.0000088741 * pow( mc_.tFol_fl[fl], 3.0)) / 0.73547);

                        oi *= ((0.047 - 0.001308 * mc_.tFol_fl[fl] + 0.000025603
                                * cbm::sqr( mc_.tFol_fl[fl]) - 0.00000021441 * pow( mc_.tFol_fl[fl], 3.0)) / 0.026934);
                    }

                    // stomata conductance to CO2 (mol CO2 m-2 s-1)
                    double  gc( std::max( (*p)->GSMIN(),                                          // minimum
                                          (*p)->GSMAX() * p->relativeconductance_fl[fl]) / FGC);  // restrictions due to soil water, vpd, and co2

                    // ozone dependency of enzyme activity
                    double const fo3 = 1.0; // not tested yet
                    //double  fo3 = CalcO3Damage( s, vt, fl, gc, ci);

                    /*!
                     *  sla (proxy for nitrogen and light adaptation) and ozone impact on: \n
                     *  - enzyme activity
                     *  - electron transport rate
                     *  - dark respiration at 25oC (umol m-2 s-1)
                     */
                    double const  sla_scale( p->sla_fl[fl_cnt-1] / p->sla_fl[fl]);
                    p->vcMax25_fl[fl] = p->vcAct25_fl[fl] * sla_scale * fo3;
                    p->jMax25_fl[fl] = p->jAct25_fl[fl] * sla_scale;
                    double const rdMax25( p->rdAct25_fl[fl] * sla_scale);


                    // enzyme activity using an Arrhenius function for representing temperature dependency
                    double  tempK = mc_.tFol_fl[fl] + cbm::D_IN_K;                                                    // temperature in Kelvin
                    double const term_arrh((tempK - TK25) / (TK25 * tempK * cbm::RGAS));                              // Arrhenius function (Long 1991)
                    double const add_peak((1.0 + exp( (TK25 * (*p)->SDJ() - (*p)->HDJ() ) / (TK25 * cbm::RGAS)))
                                        / (1.0 + exp( (tempK * (*p)->SDJ() - (*p)->HDJ() ) / (tempK * cbm::RGAS))));  // Arrhenius function considering peaked temperature response (Medlyn et al. 2002 and references therein)

                    // temperature corrected Michaelis Menten constant for CO2 (umol mol-1)
                    double const kc((*p)->KC25() * exp((*p)->AEKC() * term_arrh) * add_peak);                         // temperature corrected Michaelis Menten constant for CO2 (umol mol-1)
                    // temperature corrected Michaelis Menten constant for O2 (umol mol-1)
                    double const ko((*p)->KO25() * exp((*p)->KO25() * term_arrh) * add_peak);                         // temperature corrected Michaelis Menten constant for O2 (mmol mol-1)
                    // temperature corrected maximum RubP saturated rate of carboxylation (umol m-2 s-1)
                    double const vcMax(p->vcMax25_fl[fl] * exp((*p)->AEVC() * term_arrh) * add_peak);                 // temperature corrected maximum RubP saturated rate of carboxylation (umol m-2 s-1)
                    // maximum RubP saturated rate of oxygenation (umol m-2 s-1)
                    double const voMax(p->vcMax25_fl[fl] * (*p)->QVOVC() * exp((*p)->AEVO() * term_arrh) * add_peak); // maximum RubP saturated rate of oxygenation (umol m-2 s-1)
                    // temperature corrected light saturated rate of electron transport (umol m-2 s-1)
                    double jMax = p->jMax25_fl[fl] * exp((*p)->AEJM() * term_arrh) * add_peak;


                    // temperature corrected rate of dark respiration (umol m-2 s-1)
                    double  rd = rdMax25 * exp( (*p)->AERD() * term_arrh) * add_peak;
                    // reduction of dark respiration when substrate supply is limiting
                    if (( cava * frad) < rd)
                    {
                        rd = cava * frad;
                    }

                    // potential electron transport rate
                    double  jPot(( i2 > 0.0) ? std::max( 0.0, ( i2 + jMax - sqrt( cbm::sqr( i2 + jMax) - 4.0 * (*p)->THETA() * i2 * jMax)) / ( 2.0 * (*p)->THETA())) : 0.0);

                    // compensation point
                    // co2 compensation point for photosynthesis (umol mol-1)
                    double  comp(( vcMax > 0.0) ? (( 0.5 * voMax * kc * oi) / (vcMax * ko)) : 0.0);

                    // assimilation
                    int  count = 0;  int  nn = 0;
                    double  ciOld = 0.0;
                    double  assi = 0.0;

                    static double const  b1( sqrt( 0.001));
                    while ( !cbm::flt_in_range_lu( -b1, ci - ciOld, b1))
                    {

                        // electron transport limited assimilation
                        wj  = jPot * ci / ( 4.5 * ci + 10.5 * comp);
                        //wj = jPot * (ci - comp) / (4.0 * ci + 2.0 * comp);  // Diaz-Espejo et al. 2006
                        wp  = 3.0 * TPU25 * (1.0 - comp / ci);
                        // RubP and phosphorylation limited rate of carboxylation (umol m-2 s-1)
                        double  wpj = std::min( wp, wj);

                        // carboxylation limited assimilation (susceptible to oscillation)
                        wc = vcMax * ci / (ci + kc * (1.0 + oi / ko));
                        //wc = vcMax * (ci - comp) / (ci + kc * (1.0 + oi / ko));  // Diaz-Espejo et al. 2006

                        // actual assimilation
                        assi = (1.0 - comp / ci) * std::min( wc, wpj) - rd;

                        // internal carbon dioxide concentration according to assimilation
                        ciOld = ci;
                        ci = std::max( 0.0, ac_.ts_co2_concentration_fl[fl] - assi / ( gc * cbm::MOL_IN_MMOL));

                        // if assi is negative it does not depend on ci anymore
                        if ( assi < 0.0)
                        {
                            if ( rd >= -assi)
                            {
                                double  dispo( std::min( rd, -assi));
                                rd   -= dispo;
                                assi += dispo;
                            }
                            else
                            {
                                rd   = 0.0;
                                assi = 0.0;
                                break;
                            }
                        }

                        // iterative change of ci
                        if (ci < 50.0)
                            ci = 50.;
                        else if (ci > 450.0)
                            ci = 450.;
                        else if (ciOld-ci > 50.0)
                            ci = ciOld - 10.;
                        else if (ciOld-ci > 20.0)
                            ci = ciOld - 5.;
                        else if (ciOld-ci > 5.0)
                            ci = ciOld - 1.;
                        else if (ciOld-ci > 2.0)
                            ci = ciOld - 0.5;
                        else if (ciOld-ci > 0)
                        {
                            nn++;
                            ci = ciOld - 0.5 / double(1+nn);
                        }
                        else if (ci-ciOld > 50.0)
                            ci = ciOld + 10.;
                        else if (ci-ciOld > 20.0)
                            ci = ciOld + 5;
                        else if (ci-ciOld > 5.0)
                            ci = ciOld + 1;
                        else if (ci-ciOld > 2.0)
                            ci = ciOld + 0.5;
                        else
                        {
                            nn++;
                            ci = ciOld + 0.5 / double(1+nn);
                        }

                        // maximum number of iterations
                        if ( ++count == 100) { break; }
                    }

                    // storage of intermediate variables
                    p->ko2_fl[fl] += (frad * ko);
                    p->kco2_fl[fl] += (frad * kc);
                    p->co2i_fl[fl] += (frad * ci);
                    p->o2i_fl[fl] += (frad * oi);
                    p->rd_fl[fl] += (frad * rd);
                    p->co2comp25_fl[fl] += (frad * comp);
                    p->jMax_fl[fl] += (frad * jMax);
                    p->jPot_fl[fl] += (frad * jPot);
                    p->vcMax_fl[fl] += (frad * vcMax);
                    p->vcAct_fl[fl] += (frad * std::min( wc, wj));
                    p->carbonuptake_fl[fl] += (frad * assi * p->lai_fl[fl] * conv);
                }
                else
                {
                    p->ko2_fl[fl] = 0.0;
                    p->kco2_fl[fl] = 0.0;
                    p->co2i_fl[fl] = ac_.ts_co2_concentration_fl[fl];
                    p->o2i_fl[fl] = cbm::PO2 * cbm::MMOL_IN_MOL;
                    p->rd_fl[fl] = 0.0;
                    p->co2comp25_fl[fl] = 0.0;
                    p->jMax_fl[fl] = 0.0;
                    p->jPot_fl[fl] = 0.0;
                    p->vcMax_fl[fl] = 0.0;
                    p->vcAct_fl[fl] = 0.0;
                    p->carbonuptake_fl[fl] = 0.0;
                }

                i2 = mc_.parshd_fl[fl];
                frad = 1.0 - mc_.ts_sunlitfoliagefraction_fl[fl];
            }
        }
    }
    else
    {
        for ( size_t  fl = 0;  fl < se_.canopylayers();  ++fl)
        {
            p->ko2_fl[fl] = 0.0;
            p->kco2_fl[fl] = 0.0;
            p->co2i_fl[fl] = 0.0;
            p->o2i_fl[fl] = 0.0;
            p->rd_fl[fl] = 0.0;
            p->co2comp25_fl[fl] = 0.0;
            p->jMax_fl[fl] = 0.0;
            p->jPot_fl[fl] = 0.0;
            p->vcMax_fl[fl] = 0.0;
            p->vcAct_fl[fl] = 0.0;
            p->carbonuptake_fl[fl] = 0.0;
        }
    }
}


// sk:unused /* ozone damage function
// sk:unused  *
// sk:unused  * after Van Oijen et al. 2004
// sk:unused  *
// sk:unused  * NOTE:
// sk:unused  *    equation not tested for reasonability of results yet
// sk:unused  */
// sk:unused double
// sk:unused PhysiologyPHOTOFARQUHAR::CalcO3Damage(
// sk:unused         species_t const &  _s, size_t  vt, size_t  fl, double gc, double ci)
// sk:unused {
// sk:unused     // ozone concentration (ppb)
// sk:unused     double  co3_ppb( ac_.ts_o3_concentration_fl[fl]);   // correct???
// sk:unused     // (crop) growth rate (gDW m-2 d-1)
// sk:unused     double  cgr(( ph_.dcFolOld_vt[vt] + ph_.dcFrtOld_vt[vt] + ph_.dcSapOld_vt[vt]) * 1000.0 * this->lclock_ref().time_resolution() / cbm::CCDM);
// sk:unused
// sk:unused     // fraction of assimilates allocated into leaves
// sk:unused     double  flv( 0.0);
// sk:unused     if (( ph_.nd_cUptOld_vt[vt] > 0.0) && (( p->mFol + ph_.mFrt_vt[vt] + ph_.mSap_vt[vt]) > 0.0))
// sk:unused     {
// sk:unused         flv = (ph_.dcFolOld_vt[vt] + ph_.rResOld_vt[vt] * p->mFol / (p->mFol + ph_.mFrt_vt[vt] + ph_.mSap_vt[vt])) / ph_.nd_cUptOld_vt[vt];
// sk:unused     }
// sk:unused
// sk:unused     // stomatal conductance for co2 and o3 (g m-2LA d-1)
// sk:unused     double const  gc_g( gc * cbm::MCO2 * cbm::SEC_IN_DAY * p->lai_fl[fl]);
// sk:unused     // maximum RubP saturated rate of carboxylation (gCO2 m-2LA d-1)
// sk:unused     double const  vcmax_g( ph_.vcAct25_vtfl[vt][fl] * cbm::MCO2 * cbm::SEC_IN_DAY / 1000000.0);
// sk:unused     // rubisco content of upper leaves (g m2LA-1)
// sk:unused     double  rub( vcmax_g / TO25);
// sk:unused     // effective ozone uptake by the canopy (gO3 m-2 d-1)
// sk:unused     double const  o3eff( gc_g * co3_ppb  * (1.0 - FDET));
// sk:unused     // efficiency of biomass formation from assimilates (gDW gCO2-1)
// sk:unused     double const  gamma( GAMMA0 * (1.0 - cbm::MCO2 / cbm::MCH2O * co3_ppb * FDET * CDET / ( ac_.ts_co2_concentration_fl[fl] - ci) - flv * FREP));
// sk:unused     // repair rate of rubisco
// sk:unused     double const  rrep( cgr / gamma * cbm::MCH2O / cbm::MCO2 * flv * FREP / CREP);
// sk:unused
// sk:unused     rub += ( rrep - PHI * o3eff);
// sk:unused     // co2 turnover rate at 25 oC(umolCO2 gRub-1 s-1)
// sk:unused     double const  kc25_umol( _s->KC25() * 1000000.0 / ( cbm::MCO2 * cbm::SEC_IN_DAY));
// sk:unused
// sk:unused     return  rub * kc25_umol / ph_.vcAct25_vtfl[vt][fl];
// sk:unused }

} /*namespace ldndc*/

