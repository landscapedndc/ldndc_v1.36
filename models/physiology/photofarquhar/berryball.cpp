/*!
 * @file
 * @author
 *    - Ruediger Grote
 *    - David Kraus
 *    
 * @date
 *    September, 2018
 */

#include  "physiology/photofarquhar/berryball.h"
#include  "physiology/ld_plantfunctions.h"
#include  "physiology/ld_stomatalconductance.h"
#include  "watercycle/ld_droughtstress.h"

#include  <math/cbm_math.h>
#include  <constants/cbm_const.h>
#include  <scientific/meteo/ld_meteo.h>

#include  <logging/cbm_logging.h>

namespace ldndc {

double const  BerryBall::TK25         = 298.16;
double const  BerryBall::TK30         = 303.16;
double const  BerryBall::T30          = 30;
double const  BerryBall::TPU25        = 23.59;
double const  BerryBall::FGC          = 1.6;
double const  BerryBall::TO25         = 138.0;
double const  BerryBall::FDET         = 0.9;
double const  BerryBall::CDET         = 0.375;
double const  BerryBall::PHI          = 10.0;
double const  BerryBall::FREP         = 0.05;
double const  BerryBall::CREP         = 18.0;
double const  BerryBall::GAMMA0       = 0.4;
double const  BerryBall::C4WPFACT     = 4000.0;


BerryBall::~BerryBall()
{
    delete[] ko2_fl;
    delete[] kco2_fl;
    delete[] co2i_fl;
    delete[] o2i_fl;
    delete[] rd_fl;
    delete[] co2comp25_fl;
    delete[] jMax_fl;
    delete[] jPot_fl;
    delete[] vcMax_fl;
    delete[] vcAct_fl;
    delete[] carbonuptake_fl;
    delete[] relativeconductance_fl;

    delete[] ko2_std_fl;
    delete[] kco2_std_fl;
    delete[] co2i_std_fl;
    delete[] o2i_std_fl;
    delete[] co2comp25_std_fl;
    delete[] jMax_std_fl;
    delete[] vcMax_std_fl;
    delete[] vcAct25_fl;
    delete[] vcMax25_fl;
    delete[] jMax25_fl;
    delete[] jAct25_fl;
    delete[] rdAct25_fl;

    delete[] sla_fl;
    delete[] lai_fl;
    delete[] fFol_fl;

    delete[] vpd_fl;
    delete[] rh_fl;
    delete[] temp_fl;
    delete[] parsun_fl;
    delete[] parshd_fl;
    delete[] tFol_fl;
    delete[] co2_concentration_fl;
    delete[] sunlitfoliagefraction_fl;
}


BerryBall::BerryBall(
                     double _day_fraction,
                     size_t _max_foliage_layer)
{
    day_fraction = _day_fraction;
    max_foliage_layer = _max_foliage_layer;

    ko2_fl = new double[_max_foliage_layer];
    kco2_fl = new double[_max_foliage_layer];
    co2i_fl = new double[_max_foliage_layer];
    o2i_fl = new double[_max_foliage_layer];
    rd_fl = new double[_max_foliage_layer];
    co2comp25_fl = new double[_max_foliage_layer];
    jMax_fl = new double[_max_foliage_layer];
    jPot_fl = new double[_max_foliage_layer];
    vcMax_fl = new double[_max_foliage_layer];
    vcAct_fl = new double[_max_foliage_layer];
    carbonuptake_fl = new double[_max_foliage_layer];
    relativeconductance_fl = new double[_max_foliage_layer];

    ko2_std_fl = new double[_max_foliage_layer];
    kco2_std_fl = new double[_max_foliage_layer];
    co2i_std_fl = new double[_max_foliage_layer];
    o2i_std_fl = new double[_max_foliage_layer];
    co2comp25_std_fl = new double[_max_foliage_layer];
    jMax_std_fl = new double[_max_foliage_layer];
    vcMax_std_fl = new double[_max_foliage_layer];
    vcAct25_fl = new double[_max_foliage_layer];
    vcMax25_fl = new double[_max_foliage_layer];
    jMax25_fl = new double[_max_foliage_layer];
    jAct25_fl = new double[_max_foliage_layer];
    rdAct25_fl = new double[_max_foliage_layer];

    sla_fl = new double[_max_foliage_layer];
    lai_fl = new double[_max_foliage_layer];
    fFol_fl = new double[_max_foliage_layer];

    vpd_fl = new double[_max_foliage_layer];
    rh_fl = new double[_max_foliage_layer];
    temp_fl = new double[_max_foliage_layer];
    parsun_fl = new double[_max_foliage_layer];
    parshd_fl = new double[_max_foliage_layer];
    tFol_fl = new double[_max_foliage_layer];
    co2_concentration_fl = new double[_max_foliage_layer];
    sunlitfoliagefraction_fl = new double[_max_foliage_layer];

    stomatalconductance_method = leuning_1995_b;
}


/*!
 * @page photofarquhar
 * @section berryball Farquhar model precalculations
 */
void
BerryBall::solve()
{
    BerryBallReset();

    if ( !cbm::flt_greater_zero( mFol))
    {
        return;
    }

    // length of timestep [s]
    double const tslength = static_cast<double>(cbm::SEC_IN_DAY) * day_fraction;

    // conversion term
    double const conv( tslength * cbm::MOL_IN_UMOL * cbm::MC * cbm::KG_IN_G);

    for ( size_t  fl = 0;  fl < fl_cnt;  ++fl)
    {
        /*!
         * @page photofarquhar
         * @subsection light_dependency Light dependency
         * The electron transport rate depends on radiation intensity @cite evans:1989a.
         * \f[
         *  jPot = \frac { (i_{S} + jMax - ((i_{S} + jMax)^ { 2 } - 4.0 \cdot THETA \cdot i_{S} \cdot jMax))^ { 0.5 } }{ 2.0 \cdot THETA }
         * \f]
         * with
         * - jPot:  potential rate of electron transport (umol m-2 s-1)
         * - S:     indicator for sunlit and shaded canopy fractions
         * - THETA: species-specific slope parameter
         *
         * The radiation perceived by the photosystem is calculated separately 
         * for sunlit and shaded fractions in each canopy layer
         * \f[
         *   i_{S} = par_{S} \cdot (1.0 - 0.15)  \cdot 0.5
         * \f]
         * with
         * - par: absorbed photosynthetic active radiation
         * 
         * The losses of absorbed radiation are fix ( (1.0 - 0.15) * 0.5 = 0.425),
         * assuming a constant correction for spectral quality of 0.15 @cite evans:1987a.
         * (reduction by a factor of 0.5 is considering an equal distribution between two photosystems).
         *
         * could be improved by
         * - variable light use efficiency (mol electrons mol-1 photons) with canopy depth (0.20-0.24, @cite harley_baldocchi:1995a)
         * - dependency of light use efficiency with temperature (0.34-0.72, @cite harley:1985a).
         * 
         */

        // available carbon reserves for negative assimilation; CB: f_fac is currently always 0
        double const cava( cbm::flt_greater_zero( lai_fl[fl]) ?
                           (mFol * fFol_fl[fl] * cbm::CCDM * f_fac / (lai_fl[fl] * conv)) * day_fraction :
                           0.0);

        double i2( parsun_fl[fl] * (1.0 - 0.15) * 0.5);
        double frad( 1.0);
        size_t nss( 1);
        if ( cbm::flt_greater_zero( parsun_fl[fl] + parshd_fl[fl]))
        {
            // shade and sun cycle
            frad = sunlitfoliagefraction_fl[fl];
            nss  = 2;
        }

        /*!
         * @page photofarquhar
         * @subsection temperature_dependency Temperature dependency
         * Carboxylation and oxygenation activities as well as the electron transport rate
         * depend on temperature using an Arrhenius function based on @cite long:1991a
         * that has been corrected for high temperatures according to @cite medlyn:2002a.
         * \f[
         *   v_{E} = ( v25_{E} \cdot exp( AE_{E} \cdot term_{arrh} ) \cdot add_{peak} )
         * \f]
         * \f[
         *   term_{arrh} = \frac {tempK - TK25} {TK25 \cdot tempK \cdot RGAS}
         * \f]
         * \f[
         *   add_{peak} = \frac { 1.0 + exp( \frac {TK25 \cdot SDJ - HDJ} {TK25 \cdot RGAS} ) } { 1.0 + exp( \frac{ tempK \cdot SDJ - HDJ} {tempK \cdot RGAS} ) }
         * \f]
         * with
         * - v:     enzymatic velocity (umol m-2 s-1)
         * - E:     indicator for different enzymes (kc, ko, vcMax, voMax, rd)
         * - v25:   standard value for velocity at 25 oC (umol m-2 s-1)
         * - tempK: tissue temperature [K]
         * - TK25:  standard temperature [303.15 K]
         * - AE:    activation energies for the respective enzymatic process (J mol-1)
         * - SDJ:   entropy factor (-)
         * - HDJ:   deactivation energy (J mol-1)
         * - RGAS:  general gas constant (=8.3143) [J mol-1 K-1]
         *
         * For these dependencies oxygenation activity vo is assumed to be in a fixed relation (QVOVC)
         * to parameterized carboxylation activity at 25 oC (VCMAX25). Similarly, the electron transport
         * as well as the photorespiration rate at 25 oC is assumed to be in a fixed relation to
         * carboxylation velocity (QJVC, QRD25).
         *
         * could be improved by
         *    - introduction of specific activation and deactivation energies for each process @cite harley:1992a
                (disregarded except for vcMax and jMax, @cite harley_baldocchi:1995a)
         *    - acclimation to carbon dioxide @cite cannell_thornley:1998a
         *
        */
        /*!
         * @page photofarquhar
         * @subsection coe_dependency Internal co2 concentration
         * The internal co2 concentration is calculated iteratively considering assimilation
         * and stomatal conductance (which depends on assimilation) until ci = ciPot. With
         * \f[
         *   ciPot = ca - \frac {assi} {gs / FGC}
         * \f]
         * with
         * - ca:   air co2 concentration 
         * - gs:   stomatal conductance for water
         * - FGC:  constant describing the relation between water and co2 molecules
         * 
         * Note that there is no iteration if assi < 0 because dark respiration is not 
         * influenced by ci and the rest impact is supposed to be negligible.
         * Furthermore, stomatal conductance of water is constraint by minimum and maximum
         * values (GSMIN and GSMAX, respectively).
         *
         * The co2 compensation point which is also used for defining gs is derived from
         * carboxylation (vc) as well as oxygenation (vo) velocities @cite caemmerer_farquhar:1981a
         * using an empirical estimate of O2 concentrations in the plant tissue @cite long:1991a,
         * kinetic values that are derived from temperature-corrected species-specific
         * parameteres (KO25, KC25) and enzyme activities (AEKC, AEKO).
         * \f[
         *   c\_star = 0.5 \cdot vo \cdot kc \cdot \frac {oi} {vc \cdot ko}
         * \f]
         * with
         * - oi:   internal o2 concentration
         * - kc, ko: temperature corrected Michaelis-Menten parameters for carboxylation and oxygenation
         * - vc, vo: temperature corrected carboxylation and oxygenation velocities
         *
         */
        for ( size_t  n = 0;  n < nss;  n++)
        {
            if ( cbm::flt_greater_zero( sla_fl[fl]) &&
                 cbm::flt_greater_zero( vcAct25_fl[fl]))
            {
                double const sla_scale( sla_fl[fl_cnt-1] / sla_fl[fl]);
                double const rdMax25( rdAct25_fl[fl] * sla_scale);
                vcMax25_fl[fl] = vcAct25_fl[fl] * sla_scale;
                jMax25_fl[fl] = jAct25_fl[fl] * sla_scale;

                double const tempK( tFol_fl[fl] + cbm::D_IN_K);                                     // temperature in Kelvin
                double const term_arrh( (tempK - TK25) / ( TK25 * tempK * cbm::RGAS));              // Arrhenius function (Long 1991)

                // Arrhenius function considering peaked temperature response (Medlyn et al. 2002 and references therein)
                double const add_peak( (1.0 + exp( (TK25 * SDJ - HDJ) / (TK25 * cbm::RGAS))) 
                                     / (1.0 + exp( (tempK * SDJ - HDJ) / (tempK * cbm::RGAS))));

                double const kc( KC25 * exp( AEKC * term_arrh ) * add_peak);                        // temperature corrected Michaelis Menten constant for CO2 (umol mol-1)
                double const ko( KO25 * exp( AEKO * term_arrh ) * add_peak);                        // temperature corrected Michaelis Menten constant for O2 (mmol mol-1)

                double const vcMax( vcMax25_fl[fl] * exp( AEVC * term_arrh ) * add_peak);           // temperature corrected maximum RubP saturated rate of carboxylation (umol m-2 s-1)
                double const vcMax_std( vcMax25_fl[fl]);                                            // maximum RubP saturated rate of carboxylation (umol m-2 s-1) under standard conditions

                double const voMax( vcMax25_fl[fl] * QVOVC * exp( AEVO * term_arrh) * add_peak);    // maximum RubP saturated rate of oxygenation (umol m-2 s-1)
                double const voMax_std( vcMax25_fl[fl] * QVOVC);                                    // maximum RubP saturated rate of oxygenation (umol m-2 s-1) under standard conditions

                // temperature corrected rate of dark respiration limited by substrate supply (umol m-2 s-1)
                double const rd( cbm::bound_max( rdMax25 * exp( AERD * term_arrh) * add_peak, cava * frad));

                // rate of phosphate release from starch and sucrose production in dependence on temperature (umol m-2 s-1),
                // (Harley et al. 1992, determined for cotton)
                static double const AETP = 53.1;  // activation energy of TPU (kJ mol-1)
                static double const HDTP = 201.8; // deactivation energy of TPU (kJ mol-1)
                static double const SDTP = 0.65;  // entropy term of TPU (kJ mol-1)
                double const tpu( exp( TPU25 - AETP / ( cbm::RGAS * cbm::KJ_IN_J * tempK)) / ( 1.0 + exp( (SDTP * tempK - HDTP) / (cbm::RGAS * cbm::KJ_IN_J * tempK))));

                // drought stress consideration (same reduction of jMax as vcAct25 relative to standard value at 25oC)
                // CB: Currently, in PlaMox this factor is always 1, since jAct25_fl is set as vcAct25_fl * QJVC
                double const qjvc_scale( cbm::flt_greater_zero( jAct25_fl[fl]) ? vcAct25_fl[fl] / jAct25_fl[fl] * QJVC : 1.0);

                // maximum electron transport
                // temperature dependence of electron transport rate (De Pury and Farquhar 1997)
                // double const term_peak     = (1.0 + exp((TK25 * SDJ - HDJ) / (TK25 * cbm::RGAS))) / (1.0 + exp((SDJ * tempK - HDJ) / (cbm::RGAS * tempK)));
                double const jMax( jMax25_fl[fl] * exp( AEJM * term_arrh) * add_peak * qjvc_scale);
                double const jMax_std( jMax25_fl[fl] * qjvc_scale);

                // electron transport rate
                double const jPot( cbm::flt_greater_zero( i2) ?
                                   (i2 + jMax - sqrt( cbm::sqr( i2 + jMax) - 4.0 * THETA * i2 * jMax)) / (2.0 * THETA) :
                                   0.0);

                // preliminary start value for intercellular concentration of carbon (umol mol-1))
                // Vico and Porporato 2008 without reduction due to drought stress
                // alternative calculation methods for ci starting value (Knorr 2000)
                // double ci = (C4_TYPE ? 0.67 : 0.87) * co2_concentration_fl[fl];
                double ci = (C4_TYPE ? 0.4 : 0.7) * co2_concentration_fl[fl];

                // reference CO2 concentration for recent conditions ( 1990 - 2020)
                double const ci_ref = (( C4_TYPE) ? 0.4 : 0.7) * 400;

                // preliminary start value for intercellular concentration of oxygen (mmol mol-1)
                double oi = cbm::PO2 * cbm::MMOL_IN_MOL;

                // gas concentration with empirical equations after Long (1991)
                static double const ci_a0( 1.674);
                static double const ci_a1( -0.061294);
                static double const ci_a2( 0.0011688);
                static double const ci_a3( -0.0000088741);
                static double const ci_b( 0.73547);

                static double const oi_a0( 0.047);
                static double const oi_a1( -0.001308);
                static double const oi_a2( 0.000025603);
                static double const oi_a3( -0.00000021441);
                static double const oi_b( 0.026934);

                double const ci_std( ci_ref * cbm::poly3( T30, ci_a0, ci_a1, ci_a2, ci_a3) / ci_b);
                double const oi_std( oi * cbm::poly3( T30, oi_a0, oi_a1, oi_a2, oi_a3) / oi_b);

                if ( cbm::flt_greater( tFol_fl[fl], 25.0))
                {
                    ci *= cbm::poly3( tFol_fl[fl], ci_a0, ci_a1, ci_a2, ci_a3) / ci_b;
                    oi *= cbm::poly3( tFol_fl[fl], oi_a0, oi_a1, oi_a2, oi_a3) / oi_b;
                }

                double const cstar     = 0.5 * voMax * kc * oi / (vcMax * ko);                      // compensation point in umol mol-1 (von Caemmerer and Farquahr 1981)
                double const cstar_std = 0.5 * voMax_std * KC25 * oi_std / (vcMax_std * KO25);      // compensation point in umol mol-1 (von Caemmerer and Farquahr 1981) under standard conditions

                /*
                 // alternative calculation methods for cstar
                 double const cstar = exp(19.02 - 37.83/(cbm::RGAS * tempK)); // Bernacchi et al. (2003)

                 static double const C25 = 36.9;     // compensation point value at 25 oC in ubar (von Cammerer et al. 1994); Brooks and Farquhar 1985: 44.7; Leuning et al. 1995: 34.6; Bernacchi et al. 2002: 37.4 (nicotiana); Bernacchi et al. 2001: 42.75
                 double const cstar = C25 + 1.88 * (tempK - TK25) + 0.036 * cbm::sqr(tempK - TK25); // Brooks and Farquahr (1985)

                 double const cstar = C25 *(1 + 0.0451 * (tempK - TK25) + 0.000347 * cbm::sqr(tempK - TK25)); // Leuning et al. (1995)

                 static double const AEC = 37830.0;  // activation energy for compensation point in kJ mol-1 (Bernacchi et al. 2001); Bernacchi et al. 2002: 24600; von Caemmerer et al. 2009: 23400
                 double const cstar = C25 * exp(AEC * term_arrh); // Bernacchi et al. 2001 (cit. in Medlyn et al. 2002 ())
                 */
                bool incr = false;
                bool flag = false;

                // internal co2 concentration as required for photosynthesis (umol mol-1)
                double  ciPot( 0.0);
                // RubP limited rate of carboxylation (umol m-2 s-1)
                double  wj( 0.0);
                // phosphorylation limited rate of carboxylation (umol m-2 s-1)
                double  wp( 0.0);
                // RubP saturated rate of carboxylation (umol m-2 s-1)
                double  wc( 0.0);
                // stomatal conductance (mmolH2O m-2 s-1)
                double  gs( 0.0);
                // actual assimilation (umol m-2 s-1)
                double  assi( 0.0);

                while ( int(ci) != int(ciPot))
                {
                    double gcp( 0.0);

                    // calculate Farquhar for each ci
                    BerryBallFarquhar( ko, kc, ci, oi, rd, tpu, cstar, jPot, vcMax, wj, wp, wc, assi);

                     // - stomatal conductance required to sustain assimilation
                    gs = GSMIN;
                    if ( cbm::flt_greater_zero( co2_concentration_fl[fl]))
                    {
                        switch ( stomatalconductance_method)
                        {
                            case ballberry_1987:
                            { gs += stomatal_conductance_ballberry_1987( co2_concentration_fl[fl], SLOPE_GSA, assi, rh_fl[fl]*0.01); break; }
                            case leuning_1990:
                            { gs += stomatal_conductance_leuning_1990( co2_concentration_fl[fl], SLOPE_GSA, assi, rh_fl[fl]*0.01, cstar); break; }
                            case leuning_1995_a:
                            { gs += stomatal_conductance_leuning_1995_a( vpd_fl[fl], co2_concentration_fl[fl], SLOPE_GSA, VPDREF, assi, rh_fl[fl]*0.01, cstar); break; }
                            case leuning_1995_b:
                            { gs += stomatal_conductance_leuning_1995_b( co2_concentration_fl[fl], SLOPE_GSA, H2OREF_GS, f_h2o, assi, rh_fl[fl]*0.01, cstar); break; }
                            case medlyn_2011_a:
                            { gs += stomatal_conductance_medlyn_2011_a( vpd_fl[fl], co2_concentration_fl[fl], SLOPE_GSA, assi); break; }
                            case medlyn_2011_b:
                            { gs += stomatal_conductance_medlyn_2011_b( vpd_fl[fl], co2_concentration_fl[fl], SLOPE_GSA, f_h2o, assi); break; }
                            case eller_2020:
                            {
                                // calculate Farquhar with ci = ca for Eller et al. 2020 (suplm. S2)
                                double wj_ref(0.0);
                                double wp_ref(0.0);
                                double wc_ref(0.0);
                                double assi_ref(0.0);
                                BerryBallFarquhar( ko, kc, co2_concentration_fl[fl], oi, rd, tpu, cstar, jPot, vcMax, wj_ref, wp_ref, wc_ref, assi_ref);
                                gs += stomatal_conductance_eller_2020( nd_airpressure, tempK, vpd_fl[fl], ci, co2_concentration_fl[fl], assi, assi_ref, rplant, psi_mean, PSI_REF, PSI_EXP);
                                break;
                            }
                            default:
                            { 
                                gs += stomatal_conductance_leuning_1995_b( co2_concentration_fl[fl], SLOPE_GSA, H2OREF_GS, f_h2o, assi, rh_fl[fl]*0.01, cstar); 
                            }
                        }
                    }

                    // constraining gs calculations
                    gs = cbm::bound( GSMIN, gs, GSMAX);

                    // physiological equilibrium stomatal conductance to CO2 (mol CO2 m-2 s-1)
                    gcp = gs / FGC;

                    // - adjustment of internal carbon dioxide concentration
                    if ( cbm::flt_greater_zero( gcp) && cbm::flt_greater_zero( assi))
                    {
                        ciPot = cbm::bound_min( 0.0, co2_concentration_fl[fl] - assi / ( gcp * cbm::MOL_IN_MMOL));

                        if ( flag == false)
                        {
                            flag = true;
                            incr = ( ciPot > ci);
                        }

                        double const istep = floor( 1.0 + 0.1 * std::abs( ciPot - ci));

                        if ( incr && ( ciPot > ( ci + 1.0)))
                        {
                            ci += istep;
                        }
                        else if ( !incr && ( ciPot < ( ci - 1.0)))
                        {
                            ci -= istep;
                        }
                        else
                        {
                            ci  = ciPot;  // end of loop condition
                        }
                    }
                    else
                    {
                        ci    = co2_concentration_fl[fl];
                        ciPot = co2_concentration_fl[fl];
                    }
                }

                // storage of intermediate variables
                ko2_fl[fl] += (frad * ko);
                kco2_fl[fl] += (frad * kc);
                co2i_fl[fl] += (frad * ci);
                o2i_fl[fl] += (frad * oi);
                rd_fl[fl] += (frad * rd);
                co2comp25_fl[fl] += (frad * cstar);
                jMax_fl[fl] += (frad * jMax);
                jPot_fl[fl] += (frad * jPot);
                vcMax_fl[fl] += (frad * vcMax);
                vcAct_fl[fl] += (frad * std::min( wc, wj));
                carbonuptake_fl[fl] += (frad * assi * lai_fl[fl] * conv);	// in kg C per m2 leaf
                relativeconductance_fl[fl]  += (frad * ( gs - GSMIN) / (GSMAX - GSMIN));

                ko2_std_fl[fl] += (frad * KO25);
                kco2_std_fl[fl] += (frad * KC25);
                co2i_std_fl[fl] += (frad * ci_std);
                o2i_std_fl[fl] += (frad * oi_std);
                co2comp25_std_fl[fl] += (frad * cstar_std);
                jMax_std_fl[fl] += (frad * jMax_std);
                vcMax_std_fl[fl] += (frad * vcMax_std);
            }
            else
            {
                ko2_fl[fl] = 0.0;
                kco2_fl[fl] = 0.0;
                co2i_fl[fl] = co2_concentration_fl[fl];
                o2i_fl[fl] = cbm::PO2 * cbm::MMOL_IN_MOL;
                rd_fl[fl] = 0.0;
                co2comp25_fl[fl] = 0.0;
                jMax_fl[fl] = 0.0;
                jPot_fl[fl] = 0.0;
                vcMax_fl[fl] = 0.0;
                vcAct_fl[fl] = 0.0;
                carbonuptake_fl[fl] = 0.0;
                relativeconductance_fl[fl] = 0.0;
                
                ko2_std_fl[fl] = 0;
                kco2_std_fl[fl] = 0;
                co2i_std_fl[fl] = co2_concentration_fl[fl];
                o2i_std_fl[fl] = cbm::PO2 * cbm::MMOL_IN_MOL;
                co2comp25_std_fl[fl] = 0;
                jMax_std_fl[fl] = 0;
                vcMax_std_fl[fl] = 0;
            }

            i2 = parshd_fl[fl];
            frad = 1.0 - sunlitfoliagefraction_fl[fl];
        }
    }
}


 /*!
  * @page photofarquhar
  */
void
BerryBall::BerryBallFarquhar(
                             double _ko /**/,
                             double _kc /**/,
                             double _ci /* internal CO2 concentration*/,
                             double _oi /**/,
                             double _rd /**/,
                             double _tpu /**/,
                             double _cstar /* CO2 compensation point*/,
                             double _jpot /**/,
                             double _vcmax /**/,
                             double &_wj /* electron transport limited assimilation rate */,
                             double &_wp /* phosphorylation limited assimilation rate */,
                             double &_wc /* carboxylation limited assimilation rate */,
                             double &_assi /* assimilation rate */)
{
    // - carboxylation (Rubisco) limited assimilation (Farquhar et al. 1980, Harley et al. 1992)
    _wc = C4_TYPE ? _vcmax :
                    _vcmax * _ci / (_ci + _kc * (1.0 + _oi / _ko));
    
    double wpj( _wj);

    if ( cbm::flt_greater( _ci, _cstar))
    {
        // - electron transport limited assimilation
        //   (could be improved by linking NADPH and ATP dependency (Yin et al. 2004))
        _wj = _jpot / (4.0 + 8.0 * _cstar / _ci);
        
        // - phosphorylation limited rate of carboxylation (umol m-2 s-1)
        _wp = C4_TYPE ? C4WPFACT * _vcmax * _ci / (nd_airpressure * cbm::MBAR_IN_PA) :
                        3.0 * _tpu / (1.0 - _cstar / _ci);
 
        // restricting the limitations according to Gu et al. 2010(b)
        wpj = std::min(_wp, _wj);
        
        // - actual assimilation (orinially in von Caemmerer and Farquhar 1981)
        _assi = (1.0 - _cstar / _ci) * std::min(_wc, wpj) - _rd;
    }
    else
    {
        // - actual assimilation (von Caemmerer and Farquhar 1981)
        _assi = -1.0 * _rd;
    }

}


void
BerryBall::BerryBallReset()
{
    for ( size_t  fl = 0;  fl < max_foliage_layer;  ++fl)
    {
        ko2_fl[fl] = 0.0;
        kco2_fl[fl] = 0.0;
        co2i_fl[fl] = 0.0;
        o2i_fl[fl] = 0.0;
        rd_fl[fl] = 0.0;
        co2comp25_fl[fl] = 0.0;
        jMax_fl[fl] = 0.0;
        jPot_fl[fl] = 0.0;
        vcMax_fl[fl] = 0.0;
        vcAct_fl[fl] = 0.0;
        carbonuptake_fl[fl] = 0.0;
        relativeconductance_fl[fl] = 0.0;

        ko2_std_fl[fl] = 0;
        kco2_std_fl[fl] = 0;
        co2i_std_fl[fl] = 0;
        o2i_std_fl[fl] = 0;
        co2comp25_std_fl[fl] = 0;
        jMax_std_fl[fl] = 0;
        vcMax_std_fl[fl] = 0;
    }
}


lerr_t
BerryBall::set_vegetation_base_state( MoBiLE_Plant *_plant)
{
    for (size_t fl = 0; fl < _plant->nb_foliagelayers(); ++fl)
    {
        vcAct25_fl[fl] = _plant->vcAct25_fl[fl];
        jAct25_fl[fl] = _plant->jAct25_fl[fl];
        rdAct25_fl[fl] = _plant->rdAct25_fl[fl];

        sla_fl[fl] = _plant->sla_fl[fl];
        lai_fl[fl] = _plant->lai_fl[fl];
        fFol_fl[fl] = _plant->fFol_fl[fl];
    }

    fl_cnt = _plant->nb_foliagelayers();

    f_h2o = _plant->f_h2o;
    f_fac = _plant->f_fac;
    mFol = _plant->mFol;
    height_max = _plant->height_max;

    CSR_REF = (*_plant)->CSR_REF();
    CWP_REF = (*_plant)->CWP_REF();
    PSI_REF = (*_plant)->PSI_REF();
    PSI_EXP = (*_plant)->PSI_EXP();
    RPMIN = (*_plant)->RPMIN();

    KC25 = (*_plant)->KC25();
    AEKC = (*_plant)->AEKC();
    AEVO = (*_plant)->AEVO();
    KO25 = (*_plant)->KO25();
    AEKO = (*_plant)->AEKO();
    AEVC = (*_plant)->AEVC();
    QVOVC = (*_plant)->QVOVC();
    GSMIN = (*_plant)->GSMIN();
    GSMAX = (*_plant)->GSMAX();
    H2OREF_GS = (*_plant)->H2OREF_GS();
    SLOPE_GSA = (*_plant)->SLOPE_GSA();
    AERD = (*_plant)->AERD();
    SDJ = (*_plant)->SDJ();
    HDJ = (*_plant)->HDJ();
    THETA = (*_plant)->THETA();
    AEJM = (*_plant)->AEJM();
    QJVC = (*_plant)->QJVC();
    C4_TYPE = (*_plant)->C4_TYPE();

    return LDNDC_ERR_OK;
}


lerr_t
BerryBall::set_vegetation_non_stomatal_water_limitation_state( double const &_rplant,
                                                               double const &_psi_mean,
                                                               double const &_psi_pd)
{
    rplant = _rplant;
    psi_mean = _psi_mean;
    psi_pd = _psi_pd;
    
    return LDNDC_ERR_OK;
}


lerr_t
BerryBall::get_vegetation_state( MoBiLE_Plant *_plant)
{
    for (size_t fl = 0; fl < _plant->nb_foliagelayers(); ++fl)
    {
        _plant->ko2_fl[fl] = ko2_fl[fl];
        _plant->kco2_fl[fl] = kco2_fl[fl];
        _plant->co2i_fl[fl] = co2i_fl[fl];
        _plant->o2i_fl[fl] = o2i_fl[fl];
        _plant->rd_fl[fl] = rd_fl[fl];
        _plant->co2comp25_fl[fl] = co2comp25_fl[fl];
        _plant->jMax_fl[fl] = jMax_fl[fl];
        _plant->jPot_fl[fl] = jPot_fl[fl];
        _plant->vcMax_fl[fl] = vcMax_fl[fl];
        _plant->vcAct_fl[fl] = vcAct_fl[fl];
        _plant->carbonuptake_fl[fl] = carbonuptake_fl[fl];
        _plant->relativeconductance_fl[fl] = relativeconductance_fl[fl];

        _plant->ko2_std_fl[fl] = ko2_std_fl[fl];
        _plant->kco2_std_fl[fl] = kco2_std_fl[fl];
        _plant->co2i_std_fl[fl] = co2i_std_fl[fl];
        _plant->o2i_std_fl[fl] = o2i_std_fl[fl];
        _plant->co2comp25_std_fl[fl] = co2comp25_std_fl[fl];
        _plant->jMax_std_fl[fl] = jMax_std_fl[fl];
        _plant->vcMax_std_fl[fl] = vcMax_std_fl[fl];
        _plant->vcMax25_fl[fl] = vcMax25_fl[fl];
        _plant->jMax25_fl[fl] = jMax25_fl[fl];
    }

    return LDNDC_ERR_OK;
}

}
