/*!
 * @file
 * @author
 *  David Kraus
 * @date
 *  July, 2022
 */

#include  "watercycle/ld_droughtstress.h"
#include  "physiology/ld_stomatalconductance.h"
#include  <logging/cbm_logging.h>

/*!
 * @page photofarquhar
 * @section ld_stomatalconductance Stomatal conductance models
 * @subsection stomatal_conductance_ballberry_1987 Ball, Woodrow, and Berry 1987
 * The model @cite ball:1987a is the original version to use iteratively with the Farquhar model @cite farquhar:1980a.
 * The stomatal conductance of every foliage layer is determined by
 * \f[
 *   gs = GSMIN + SLOPE\_{GSA} \cdot assi \cdot \frac {rh}{ca}
 * \f]
 *
 * with
 * - \f$ assi \f$: the assimilated carbon (per canopy layer)
 * - \f$ rh \f$: the relative humidity (per canopy layer)
 * - \f$ ca \f$: the mole fraction of CO2 (per canopy layer)
 * - GSMIN, SLOPE_GSA: species-specific parameters (for minimum leaf conductance and sensititvity to assimilation)
 *
 */
double
ldndc::stomatal_conductance_ballberry_1987(
                                    double _co2,
                                    double _slope_gsa,
                                    double _assi,
                                    double _rh)
{
    return std::max( 0.0, ( _slope_gsa * _assi * _rh / _co2) / cbm::MOL_IN_MMOL);
}


// modified BWB model according to Leuning 1990
double
ldndc::stomatal_conductance_leuning_1990(
                                  double _co2,
                                  double _slope_gsa,
                                  double _assi,
                                  double _rh,
                                  double _cstar)
{
    return std::max( 0.0, ( _slope_gsa * _assi * _rh / ( _co2 - _cstar)) / cbm::MOL_IN_MMOL);
}


// modified BWB model according to Leuning 1995 (replaces rh with vpd function)
double
ldndc::stomatal_conductance_leuning_1995_a(
                                    double _vpd,
                                    double _co2,
                                    double _slope_gsa,
                                    double _vpdref,
                                    double _assi,
                                    double _rh,
                                    double _cstar)
{
    double const  vpdFac = 1.0 + _vpd / _vpdref;
    return std::max( 0.0, _slope_gsa * _assi * _rh / (( _co2 - _cstar) * vpdFac) / cbm::MOL_IN_MMOL);
}

/*!
 * @page photofarquhar
 * @subsection stomatal_conductance_leuning_1995_a Leuning 1995
 * The model @cite leuning:1995a has been modified by adding an additional soil water impact @cite knauer:2015a.
 * Stomatal conductance of every foliage layer is determined by
 * \f[
 *   gs = GSMIN + SLOPE\_GSA \cdot fwat \cdot assi \cdot \frac{rh}{ca - c\_star}
 * \f]
 * \f[
 *   fwat = min \left(1.0, \frac{\frac{wc - wc\_{min}}{wc\_{max} - wc\_{min}}}{H2OREF\_GS} \right)
 * \f]
 * with:
 * -\f$ assi\f$:                 assimilated carbon (per canopy layer)
 * -\f$ rh\f$:                   relative humidity (per canopy layer)
 * -\f$ ca\f$:                   mole fraction of CO2 air concentration (per canopy layer)
 * -\f$ c\_star\f$:              CO2 compensation point
 * -\f$ fwat\f$:                 drought stress factor, see also @ref 
 * 
 * -\f$ wc \f$:                  soil water content (mm m-3)
 * -\f$ wc\_{max}, wc\_{min}\f$: maximum and minimum water content within the rooting zone (field capacity and wilting point)
 * - H2OREF_GS:                  species-specific threshold relative water content at which stomata start to close
 * - GSMIN, SLOPE_GSA:           species-specific parameters (for minimum leaf conductance and sensitivity to assimilation)
 *
 */
// conductance model according to Leunig et al. 1995 with additional soil water consideration (Knauer et al. 2015)
double
ldndc::stomatal_conductance_leuning_1995_b(
                                    double _co2,
                                    double _slope_gsa,
                                    double _f_h2o_ref,
                                    double _f_h2o,
                                    double _assi,
                                    double _rh,
                                    double _cstar)
{
    DroughtStress  droughtstress;
    droughtstress.fh2o_ref = _f_h2o_ref;
    double const  fwat = droughtstress.linear_threshold( _f_h2o);
    return std::max( 0.0, _slope_gsa * fwat * _assi * _rh / (( _co2 - _cstar)) / cbm::MOL_IN_MMOL);
}

// conductance model according to Medlyn et al. 2011 (GCB)
double
ldndc::stomatal_conductance_medlyn_2011_a(
                                   double _vpd,
                                   double _co2,
                                   double _slope_gsa,
                                   double _assi)
{
    double const  vpdFac = std::max( 1.0, _slope_gsa / sqrt( _vpd));
    return std::max( 0.0, (1.0 + vpdFac) * _assi / ( _co2 ) / cbm::MOL_IN_MMOL);
}


// conductance model according to Medlyn et al. 2011, additionally considering soil water availability according to Drake et al. 2017 (Yang et al. 2019)
double
ldndc::stomatal_conductance_medlyn_2011_b(
                                   double _vpd,
                                   double _co2,
                                   double _slope_gsa,
                                   double _f_h2o,
                                   double _assi)
{
    DroughtStress  droughtstress;
    double  sgsa = _slope_gsa * pow(droughtstress.linear( _f_h2o), 0.38) ;
    double  vpdFac = std::max( 1.0, sgsa / sqrt( _vpd));
    return std::max( 0.0, (1.0 + vpdFac) * _assi / _co2 / cbm::MOL_IN_MMOL);
}

/*!
* @page photofarquhar
* @subsection stomatal_conductance_eller_2020 Eller et al. 2020
* The model @cite eller:2020a considers canopy water potential as influencial for stomatal conductance.
* Stomatal conductance of every foliage layer is determined by
* \f[
*   gs = GSMIN + 0.5 \cdot qac \cdot (sqrt(1.0 + (4.0 \cdot \frac {epsilon}{qac} ) - 1.0) )
* \f]
* \f[
*   qac = \frac {assi\_{ref} - assi}{ca - ci}
* \f]
*
* with
* - assi:     photosynthesis rate
* - assi_ref: photosynthesis rate under standard conditions (25oC)
* - ca:       the mole fraction of CO2
* - ci:       plant internal mole fraction of CO2
* - GSMIN:    species-specific parameter (for minimum leaf conductance)
*
* The conductance impact 'epsilon' is a complex interaction of plant conductance
* modified by canopy water potential and evaporative demand:
* \f[
*   epsilon = \frac { 2.0 }{ qkr \cdot rplant \cdot 1.6 \cdot vpd\_{mmol} }
* \f]
* \f[
*   qkr = ( \frac {kcr - kcr\_{ref} } { psi\_{mean} - (0.5 \cdot (psi\_{mean}+PSI\_{REF}) ) } ) / kcr
* \f]
* \f[
*   kcr =        1.0 - (1.0 - exp(- ( \frac {psi\_{mean} }{ PSI\_{REF} } ) ^ { PSI\_{EXP} } ) )
* \f]
* \f[
*   kcr\_{ref} = 1.0 - (1.0 - exp(- ( \frac { 0.5 \cdot ( psi\_{mean} - PSI\_{EXP}) }{ PSI\_{REF} } ) ^ { PSI\_{EXP} } ) )
* \f]
*
* with
* - rplant: plant resistance (MPa m2 s mmol-1)
* - kcr: relative conductance between roots and canopy
* - vpd_mmol: vapor pressure deficit (mmol)
* - psi_mean: mean between canopy water potential and predawn water potential (MPa)
* - PSI_EXP, PSI_REF: species-specific parameter
*/
// conductance model according to Eller et al. 2020
double
ldndc::stomatal_conductance_eller_2020(
                                double _air_pressure,
                                double _tempK,
                                double _vpd,
                                double _ci,
                                double _ca,
                                double _assi,
                                double _assi_ref,
                                double _rplant,
                                double _psi_mean,
                                double _psi_ref,
                                double _psi_exp)
{
    // loss function related to increases in whole tree resistance
    double qkr(1.0);
    if (cbm::flt_greater_zero(-_psi_mean))
    {
        // loss functions of hydraulic conductance with the term kcr representing xylem conductivity
        double const kcr = 1.0 - (1.0 - std::exp(-std::pow( _psi_mean / _psi_ref, _psi_exp)));
        double const kcr_ref = 1.0 - (1.0 - std::exp(-std::pow( (0.5*(_psi_mean+_psi_ref)) / _psi_ref, _psi_exp)));
        qkr = ( (kcr - kcr_ref) / (_psi_mean - (0.5*(_psi_mean+_psi_ref))) ) / kcr;
    }

    // cost of stomatal opening in terms of loss of xylem conductivity [mol m2leaf-1 s-1 MPa-1]
    double epsilon(1.0);
    if (cbm::flt_greater_zero(_rplant))
    {
        double const mol_air = _air_pressure * cbm::KPA_IN_MBAR / (cbm::RGAS * _tempK); // mbar * kPa mbar-1 / (L kPa K-1 mol-1 * K-1)
        double const vpd_mmol = _vpd * cbm::MMOL_IN_MOL / (cbm::RGAS * _tempK * mol_air); // kPa * mmol mol-1 / (L kPa K-1 mol-1 * K * mol L-1)
        epsilon = 2.0 / (qkr * _rplant * 1.6 * vpd_mmol);  // 1 / (MPa m2 s mmol-1 * mmol mol-1)
    }

    // ratio between d(assi) and d(ci)
    double qac(0.0);
    if (cbm::flt_greater_zero(_ca - _ci))
    {
        qac = (_assi_ref - _assi) / (_ca - _ci);
    }

    // stomatal conductance (mmol m2leaf-1 s-1 MPa-1)
    double x(0.0);
    if (cbm::flt_greater_zero(qac))
    {
        x = std::max(0.0, 0.5 * qac * (sqrt(1.0 + (4.0 * epsilon / qac)) - 1.0) / cbm::MOL_IN_MMOL);
    }
    return x;
}
