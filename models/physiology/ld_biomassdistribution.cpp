/*!
 * @file
 * @authors
 *  - Ruediger Grote
 *  - David Kraus
 *
 * @date
 *  March 18, 2015
 */


#include  "physiology/ld_biomassdistribution.h"
#include  "physiology/ld_plantfunctions.h"
#include  <logging/cbm_logging.h>


/*!
 * @page veglibs
 * @section veglibs_canopy_structure Canopy structure
 * The distribution of foliage biomass \f$ m(z) \f$ within the canopy
 * depends on canopy length with a tendency to put more weight to the
 * bottom of the distribution with larger length (@cite grote:2003a, @cite grote:2007a):
 * \f[
 *   m(z) = M \cdot \frac{r_{ih} f(z)}{\int r_{ih} f(z)} \\
 *   r_{ih} = \frac{CL - z}{CL} \\
 *   f(z) = p^{100 \frac{z}{CL^2}}
 * \f]
 * \f$ CL \f$: canopy length (difference between start and end of canopy, m) \n
 * \f$ z \f$: distribution depth for which the percentage foliage should be defined (between 0-1, refers to canopy start and top height) \n
 * \f$ p \f$: shape parameter
 *
 * @image html foliage_distribution_grote.png "Canopy biomass distribution" width=500
 * @image latex foliage_distribution_grote.png "Canopy biomass distribution"
 */
void
ldndc::canopy_biomass_distribution(
                                   size_t const _fl_cnt_max,
                                   size_t const _fl_cnt,
                                   double const _ps,
                                   double const _h_bottom,
                                   double const _h_top,
                                   double const *_h_fl,
                                   double *_fFol_fl)
{
    /* reset */
    for ( size_t fl = 0; fl < _fl_cnt_max; ++fl)
    {
        _fFol_fl[fl] = 0.0;
    }

    // total distribution length
    if ( cbm::flt_greater( _h_top, _h_bottom))
    {
        double h_cum_top( 0.0);
        for ( size_t fl = 0;  fl < _fl_cnt;  ++fl)
        {
            double const h_cum_bottom( h_cum_top);
            h_cum_top += _h_fl[fl];

            estimate_single_layer_biomass(
                                          _ps, _h_bottom, _h_top,
                                          _h_fl[fl], _fFol_fl[fl],
                                          h_cum_bottom, h_cum_top);
        }

        double const fFol_sum( cbm::sum( _fFol_fl, _fl_cnt));
        if ( cbm::flt_equal_zero( fFol_sum))
        {
            for ( size_t fl = 0; fl < _fl_cnt; ++fl)
            {
                _fFol_fl[fl] = 1.0 / double( _fl_cnt);
            }
        }
        else
        {
            for ( size_t fl = 0; fl < _fl_cnt; ++fl)
            {
                _fFol_fl[fl] /= fFol_sum;
            }
        }
    }
}


/*!
 * @page veglibs
 */
void
ldndc::canopy_lai_distribution(
                 double  _sla_min,
                 double  _sla_max,
                 double  _height_max,
                 double  _height_min,
                 double  _mFol,
                 double const *  _fFol_fl,
                 double const *  _h_fl,
                 double *  _lai_fl,
                 double *  _sla_fl,
                 size_t  _foliage_layer_cnt,
                 size_t  _foliage_layer_cnt_max)
{
    /* reset canopy */
    for ( size_t  fl = 0;  fl < _foliage_layer_cnt_max;  ++fl)
    {
        _sla_fl[fl] = 0.0;
        _lai_fl[fl] = 0.0;
    }

    double const crown_length( _height_max - _height_min);
    if ( cbm::flt_greater_zero( crown_length))
    {
        double const delta_sla( cbm::bound_min( 0.0, _sla_max - _sla_min));

        /* cumulative height of total plant */
        double h_cum_total_top( 0.0);
        double h_cum_crown_bottom( 0.0);
        for ( size_t  fl = 0;  fl < _foliage_layer_cnt;  ++fl)
        {
            h_cum_total_top += _h_fl[fl];

            /* sla and lai are greater zero only within canopy */
            if ( cbm::flt_greater( h_cum_total_top, _height_min))
            {
                double const h_cum_crown_top( cbm::bound_max( h_cum_total_top - _height_min, crown_length));
                double const h_cum_crown( 0.5 * (h_cum_crown_bottom + h_cum_crown_top));

                _sla_fl[fl] = _sla_max - (delta_sla * h_cum_crown / crown_length);
                _lai_fl[fl] = _sla_fl[fl] * _mFol * _fFol_fl[fl];

                h_cum_crown_bottom = h_cum_crown_top;
            }
        }
    }
}
