/*!
 * @file
 * 
 * @author
 *    David Kraus (created on: march 6, 2015)
 */

#ifndef  LD_NITROGENFIXATION_H_
#define  LD_NITROGENFIXATION_H_

#include  "state/mbe_state.h"

namespace ldndc {

class LDNDC_API BaseNitrogenFixation
{
    public:
        virtual ~BaseNitrogenFixation() = 0;
};



class LDNDC_API NitrogenFixation : public BaseNitrogenFixation
{
public:

    NitrogenFixation(
            MoBiLE_State *, cbm::io_kcomm_t *);

    ~NitrogenFixation();

    substate_watercycle_t *  water;
    substate_soilchemistry_t *  soilchem;

    double
    get_nitrogen_fixation( MoBiLE_Plant *, double /*temperature*/);

    double
    get_fact_water( MoBiLE_Plant *);

    double
    get_fact_temperature( MoBiLE_Plant *, double /*temperature*/);

    double
    get_fact_nitrogen( MoBiLE_Plant *);

    double
    get_fact_carbon( MoBiLE_Plant *);
};

} /* namespace ldndc */

#endif  /*  !LD_NITROGENFIXATION_H_  */

