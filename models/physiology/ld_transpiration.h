/*!
 */

#ifndef  LD_TRANSPIRATION_H_
#define  LD_TRANSPIRATION_H_

#include  <input/soillayers/soillayers.h>
#include  <containers/cbm_vector.h>

namespace ldndc {

/*!
 * @brief
 *    Returns potential transpiration in [m] for trees.
 */
double
potential_wood_transpiration(
                             soillayers::input_class_soillayers_t const &  /* soil layer */,
                             double _vpd,
                             double _carbon_uptake,
                             double _f_area,
                             double _wuecmax,
                             double _wuecmin,
                             lvector_t< double > _h_sl,
                             lvector_t< double > _wc,
                             lvector_t< double > _wc_min,
                             lvector_t< double > _wc_max);

/*!
 * @brief
 *    Returns potential transpiration in [m] for crops and grass.
 */
double
potential_crop_transpiration(
                             double _co2,
                             double _carbon_uptake,
                             double _wuecmax);

/*!
 * @brief
 *    Returns potential transpiration in [m] for any species type.
 */
double
potential_transpiration(
                        size_t /* nd of foliage layers */,
                        double gsmin,
                        double gsmax,
                        double *_lai_fl,
                        lvector_t< double > _vpd_fl,
                        double * _relative_conductance_fl);
} /* namespace ldndc */


#endif  /*  !LD_TRANSPIRATION_H_   */

