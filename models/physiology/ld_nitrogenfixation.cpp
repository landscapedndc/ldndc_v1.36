/*!
 * @file
 * 
 * @author
 *    david kraus (created on: march 25, 2015)
 */

#include  "physiology/ld_nitrogenfixation.h"

#include  <math/cbm_math.h>


ldndc::BaseNitrogenFixation::~BaseNitrogenFixation()
{
}


ldndc::NitrogenFixation::NitrogenFixation(
        ldndc::MoBiLE_State *  _state, cbm::io_kcomm_t *)
        : ldndc::BaseNitrogenFixation(),
        
          water( _state->get_substate< substate_watercycle_t >()),
          soilchem( _state->get_substate< substate_soilchemistry_t >())
{   
}


ldndc::NitrogenFixation::~NitrogenFixation()
{
}


double
ldndc::NitrogenFixation::get_nitrogen_fixation( MoBiLE_Plant * _sp, double _temp)
{
    double const fact_t( get_fact_temperature( _sp, _temp));
    if ( cbm::flt_equal_zero( fact_t))
    {
        return 0.0;
    }
    double const fact_n( get_fact_carbon( _sp));
    double const fact_c( get_fact_nitrogen( _sp));
    double const fact_w( get_fact_water( _sp));

    return (fact_n * fact_c * fact_w * fact_t);
}



/*!
 * @brief
 *      Influence of water availability on nitrogen fixation
 */
double
ldndc::NitrogenFixation::get_fact_water( MoBiLE_Plant * vt)
{
    double ftws( 0.0);
    size_t const  S = water->wc_sl.size();
    for ( size_t  sl = 0;  sl < S;  ++sl)
    {
        ftws += cbm::bound(0.001, (water->wc_sl[sl]-soilchem->wcmin_sl[sl]) / (soilchem->wcmax_sl[sl]-soilchem->wcmin_sl[sl]), 0.999);
    }
    ftws /= static_cast< double >( S);

    return (-1 + 2.0 / (1.0 + std::exp( (*vt)->NFIX_W() * ftws)));
}


/*!
 * @brief
 *      Influence of temperature on nitrogen fixation
 */
double
ldndc::NitrogenFixation::get_fact_temperature( MoBiLE_Plant * _sp, double _temp)
{
    if ( _temp <= (*_sp)->NFIX_TMIN())
    {
        return 0.0;
    }
    else if ( _temp <= (*_sp)->NFIX_TOPT())
    {
        ldndc_kassert( !cbm::flt_equal( (*_sp)->NFIX_TOPT(), (*_sp)->NFIX_TMIN()));
        return (_temp - (*_sp)->NFIX_TMIN()) / ((*_sp)->NFIX_TOPT() - (*_sp)->NFIX_TMIN());
    }
    else if ( _temp <= (*_sp)->NFIX_TMAX())
    {
        ldndc_kassert( !cbm::flt_equal( (*_sp)->NFIX_TOPT(), (*_sp)->NFIX_TMIN()));
        return ((*_sp)->NFIX_TMAX() - _temp) / ((*_sp)->NFIX_TMAX() - (*_sp)->NFIX_TOPT());
    }
    else
    {
        return 0.0;
    }
}


double
ldndc::NitrogenFixation::get_fact_nitrogen( MoBiLE_Plant *)
{
    return 1.0;
}


double
ldndc::NitrogenFixation::get_fact_carbon( MoBiLE_Plant *)
{
    return 1.0;
}

