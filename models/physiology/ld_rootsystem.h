/*!
 * @file
 * 
 * @author
 *    david kraus (created on: may 6, 2014)
 */

#ifndef  LD_ROOTSYSTEM_H_
#define  LD_ROOTSYSTEM_H_

#include  "state/mbe_state.h"
#include  "physiology/ld_plantfunctions.h"

namespace ldndc {

void
fineroots_biomass_distribution(
                               size_t _sl_cnt_litter,
                               size_t _sl_cnt_soil,
                               double _ps,
                               double _rooting_depth,
                               double *_f_frt_sl,
                               double const *_h_sl);

struct LDNDC_API BaseRootSystemDNDC
{
    virtual 
    ~BaseRootSystemDNDC() = 0;

    /*!
     * \brief
     * resets all root related physiology/vegetationstructure state variables
     *
     */
    virtual void reset( MoBiLE_Plant *) = 0;

    /*!
     * \brief root_development
     *
     */    
    virtual void 
    root_development( double /*increment*/, unsigned int &, MoBiLE_Plant *) = 0;
    
    /*!
     * \brief set_rooting_depth
     *
     */
    virtual void 
    set_rooting_depth( unsigned int &, MoBiLE_Plant *, double,
                      double  _max_rooting_depth = invalid_dbl) = 0;
    
    /*!
     * \brief update_deepest_rooted_soil_layer_index
     *
     */
    virtual unsigned int 
    update_deepest_rooted_soil_layer_index( unsigned int &,
                    MoBiLE_Plant *, double) = 0;
    
    /*!
     * \brief update_root_fraction_sigmoid
     *
     */
    virtual void
    update_root_fraction_sigmoid(
                                 double /* adaption rate of fine roots distribution */,
                                 unsigned int /* deepest rooted soillayer */,
                                 double /* sigmoid curve parameter: min */,
                                 double /* sigmoid curve parameter: max */,
                                 double  /* sigmoid curve parameter: width */,
                                 double  /* sigmoid curve parameter: height */,
                                 MoBiLE_Plant * /* plant */) = 0;

    /*!
     * \brief update_root_fraction_exonential
     *
     */
    virtual void
    update_root_fraction_exponential(
                                     double /* adaption rate of fine roots distribution */,
                                     unsigned int /* deepest rooted soillayer */,
                                     double /* exponential factor */,
                                     MoBiLE_Plant * /* plant */) = 0;
    
    /*!
     * \brief update_root_fraction_exonential
     *
     */
    virtual void
    update_root_fraction_nutrient_based(
                                        double /* adaption rate of fine roots distribution */,
                                        unsigned int /* deepest rooted soillayer */,
                                        lvector_t< double > &/* soillayer specific nutrient uptake */,
                                        MoBiLE_Plant * /* plant */) = 0;
    
    
    /*!
     * \brief update_roots_static
     *
     */
    virtual void
    update_roots_static(
                               const double /* FTS_TOT_ */,
                               unsigned int &/* root_q_vt_ */,
                               MoBiLE_Plant * /* _vt */,
                               double /* deltamassFrt */,
                               ldndc::growable_array < BaseRootSystemDNDC*, 1000, 1 > & /* root_system */,
                               double /*rootgrowthrestriction*/) = 0;
    
    /*!
     * \brief specific_rngth
     *
     */
    virtual void
    specific_root_length(
                               unsigned int &/* root_q_vt_ */,
                               MoBiLE_Plant * /* _vt */,
                               lvector_t< double > &/*specifificrootlength*/) = 0;
                               
    /*!
     * \brief update_rootlength_meanSRL_allsl
     *
     */
    virtual void
    update_rootlength_meanSRL_allsl(
                               unsigned int &/* root_q_vt_ */,
                               MoBiLE_Plant * /* _vt */,
                               double /* deltamass */) = 0;
                               
    /*!
     * \brief update_roots_dynamic
     *
     */
    virtual void
    update_roots_dynamic(
                               const double /* FTS_TOT_ */,
                               unsigned int &/* root_q_vt_ */,
                               MoBiLE_Plant * /* _vt */,
                               double /* deltamassFrt */) = 0;

    /*!
     * \brief determine_root_restrictions_coarse_fragments
     *
     */
    virtual lvector_t< double >
    determine_root_restrictions_coarse_fragments(
                         unsigned int /* iterator_potentially_deepest_rooted_layer */) = 0;
                           
    /*!
     * \brief determine_root_restrictions_soil_strength
     *
     */
    virtual lvector_t< double >
    determine_root_restrictions_soil_strength(
                      unsigned int /* iterator_potentially_deepest_rooted_layer */) = 0;
                           
    /*!
     * \brief determine_root_restrictions_aeration
     *
     */
    virtual lvector_t< double >
    determine_root_restrictions_aeration(
                         unsigned int /* iterator_potentially_deepest_rooted_layer */,
                         bool /* notaffected */) = 0;
                           
    /*!
     * \brief determine_root_restrictions_temperature
     *
     */
    virtual lvector_t< double >
    determine_root_restrictions_temperature(
                        unsigned int /* iterator_potentially_deepest_rooted_layer */,
                        MoBiLE_Plant * /* _vt */) = 0;
    
    virtual void
    update_rooting_depth(
                         const double /*target_rooting_depth*/,
                         double &/* layerrestrictions */,
                         unsigned int & /* root_q_vt_ */,
                         MoBiLE_Plant * /* _vt */) = 0;
    
    virtual void
    determine_target_rooting_depth_increase(
                                        double &/* target_rooting_depthincrease */,
                                        unsigned int &/* target_iterator_first_non_rooted_layer */,
                                        const double /* FTS_TOT_ */,
                                        MoBiLE_Plant * /* _vt */) = 0;

    virtual void
    nitrogen_uptake(
                    unsigned int _deepest_rooted_layer,
                    double &n_plant,
                    double n_opt,
                    double _fts_tot,
                    lvector_t< double > _no3_sl,
                    lvector_t< double > _no3_uptake_sl,
                    lvector_t< double > _nh4_sl,
                    lvector_t< double > _nh4_uptake_sl,
                    lvector_t< double > _don_sl,
                    lvector_t< double > _don_uptake_sl,
                    lvector_t< double > _temp_sl,
                    MoBiLE_Plant * /* plant */) = 0;
};


struct LDNDC_API RootSystemDNDC : public BaseRootSystemDNDC
{
    RootSystemDNDC(
                   MoBiLE_State *,
                   cbm::io_kcomm_t *);
    ~RootSystemDNDC();

    input_class_soillayers_t const *  m_soillayers;
    
    /*!
     * @brief
     *     All kind of plant related functions.
     */
    LD_PlantFunctions  m_pf;
    substate_soilchemistry_t &  sc_;
    substate_microclimate_t &  mc_;
    //~ substate_vegstructure_t &  vs_;
    substate_physiology_t &  ph_;
    substate_watercycle_t &  wc_;

    MoBiLE_PlantVegetation *  m_veg;
    

    /*!
     * \brief
     * resets all root related physiology/vegetationstructure state variables
     *
     */
    void reset( MoBiLE_Plant *);

    /*!
     * \brief root_development
     *
     */    
    void 
    root_development( double /*increment*/, unsigned int &, MoBiLE_Plant *);

    /*!
     * \brief set_rooting_depth
     *
     */
    void 
    set_rooting_depth( unsigned int &, MoBiLE_Plant *, 
                    double, double  _max_rooting_depth = invalid_dbl);
    
    /*!
     * \brief update_deepest_rooted_soil_layer_index
     *
     */
    unsigned int 
    update_deepest_rooted_soil_layer_index( unsigned int &, MoBiLE_Plant *, double);
    
    /*!
     * \brief update_root_fraction_sigmoid
     *
     */
    void
    update_root_fraction_sigmoid(
                                 double /* adaption rate of fine roots distribution */,
                                 unsigned int /* deepest rooted soillayer */,
                                 double /* sigmoid curve parameter: min */,
                                 double /* sigmoid curve parameter: max */,
                                 double  /* sigmoid curve parameter: width */,
                                 double  /* sigmoid curve parameter: height */,
                                 MoBiLE_Plant * /* plant */);

    /*!
     * \brief update_root_fraction_exonential
     *
     */
    void
    update_root_fraction_exponential(
                                     double /* adaption rate of fine roots distribution */,
                                     unsigned int /* deepest rooted soillayer */,
                                     double /* exponential factor */,
                                     MoBiLE_Plant * /* plant */);
                                        
    /*!
     * \brief update_root_fraction_nutrient_based
     *
     */
    void
    update_root_fraction_nutrient_based(
                                        double /* adaption rate of fine roots distribution */,
                                        unsigned int /* deepest rooted soillayer */,
                                        lvector_t< double > &/* soillayer specific nutrient uptake */,
                                        MoBiLE_Plant * /* plant */);
    
    /*!
     * \brief update_roots_static
     *
     */
    void
    update_roots_static(
                               const double /* FTS_TOT_ */,
                               unsigned int &/* root_q_vt_ */,
                               MoBiLE_Plant * /* _vt */,
                               double /* deltamassFrt */,
                               ldndc::growable_array < BaseRootSystemDNDC*, 1000, 1 > &/* root_system */,
                               double /* rootgrowthrestriction */);
    /*!
     * \brief specific_root_length
     *
     */
    void
    specific_root_length(
                               unsigned int &/* root_q_vt_ */,
                               MoBiLE_Plant * /* _vt */,
                               lvector_t< double > &/*specifificrootlength*/);
                               
    /*!
     * \brief update_rootlength_meanSRL_allsl
     *
     */
    void
    update_rootlength_meanSRL_allsl(
                               unsigned int &/* root_q_vt_ */,
                               MoBiLE_Plant * /* _vt */,
                               double /* deltamass */);
                               
    /*!
     * \brief update_roots_dynamic
     *
     */
    void
    update_roots_dynamic(
                               const double /* FTS_TOT_ */,
                               unsigned int &/* root_q_vt_ */,
                               MoBiLE_Plant * /* _vt */,
                               double /* deltamassFrt */);
    
    /*!
     * \brief determine_root_restrictions_coarse_fragments
     *
     */
    lvector_t< double >
    determine_root_restrictions_coarse_fragments(
                         unsigned int /* iterator_potentially_deepest_rooted_layer */);
    /*!
     * \brief determine_root_restrictions_soil_strength
     *
     */
    lvector_t< double >
    determine_root_restrictions_soil_strength(
                          unsigned int /* iterator_potentially_deepest_rooted_layer */);

    /*!
     * \brief determine_root_restrictions_aeration
     *
     */
    lvector_t< double >
    determine_root_restrictions_aeration(
                         unsigned int /* iterator_potentially_deepest_rooted_layer */,
                         bool /* notaffected */);
                           
    /*!
     * \brief determine_root_restrictions_temperature
     *
     */
    lvector_t< double >
    determine_root_restrictions_temperature(
                        unsigned int /* iterator_potentially_deepest_rooted_layer */,
                        MoBiLE_Plant * /* _vt */);
    
    /*!
     * \brief deepest_rooted_layer
     *
     */
    void
    update_rooting_depth(
                         const double /*target_rooting_depth*/,
                         double &/* layerrestrictions */,
                         unsigned int & /* root_q_vt_ */,
                         MoBiLE_Plant * /* _vt */);
    
    /*!
     * \brief determine_target_rooting_depth_increase
     *
     */
    void
    determine_target_rooting_depth_increase(
                            double &/* target_rooting_depthincrease */,
                            unsigned int &/* target_iterator_first_non_rooted_layer */,
                            const double /* FTS_TOT_ */,
                            MoBiLE_Plant * /* _vt */);
    
    /*!
     * \brief update_root_structure
     *
     */
    void
    update_root_structure( unsigned int & _root_q_vt, MoBiLE_Plant *);

    void
    nitrogen_uptake(
                    unsigned int _deepest_rooted_layer,
                    double &n_plant,
                    double n_opt,
                    double _fts_tot,
                    lvector_t< double > _no3_sl,
                    lvector_t< double > _no3_uptake_sl,
                    lvector_t< double > _nh4_sl,
                    lvector_t< double > _nh4_uptake_sl,
                    lvector_t< double > _don_sl,
                    lvector_t< double > _don_uptake_sl,
                    lvector_t< double > _temp_sl,
                    MoBiLE_Plant * /* plant */);
};

}

#endif  /*  !LD_ROOTSYSTEM_H_  */
