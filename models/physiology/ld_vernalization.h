/*!
 * @file
 * @author
 *    David Kraus
 * @date
 *  January, 2019
 */

#ifndef  LD_VERNALIZATION_H_
#define  LD_VERNALIZATION_H_

#include  "ld_modelsconfig.h"

namespace ldndc {

    double
    get_chilling_units(
                       double _air_temperature,
                       double _maximum_chilling_temperature);

    double
    get_chilling_factor(
                        double _gdd,
                        double _gdd_flowering,
                        double _chilling_units,
                        double _required_chilling_units);

} /* namespace ldndc */

#endif  /*  !LD_VERNALIZATION_H_  */
