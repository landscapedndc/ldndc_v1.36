/*!
 * @file
 * @author
 *  David Kraus
 * @date
 *  July, 2022
 */

#ifndef  LD_STOMATALCONDUCTANCE_H_
#define  LD_STOMATALCONDUCTANCE_H_

#include  "ld_modelsconfig.h"

namespace ldndc {

/*!
 * @brief
 *     Stomatal conductance methods
 */
enum stomatalconductance_method_e{
    ballberry_1987,
    leuning_1990,
    leuning_1995_a,
    leuning_1995_b,
    medlyn_2011_a,
    medlyn_2011_b,
    eller_2020
};

/*!
 * @brief
 *      Stomatal conductance according to Berry, Woodward and Ball 1987.
 */
double
stomatal_conductance_ballberry_1987(
                                    double /* co2 concentration */,
                                    double /* slope gsa */,
                                    double /* assimilation */,
                                    double /* relative humidity */);

/*!
 * @brief
 *      Stomatal conductance according to Leuning 1990.
 */
double
stomatal_conductance_leuning_1990(
                                  double /* co2 concentration */,
                                  double /* slope gsa */,
                                  double /* assimilation */,
                                  double /* relative humidity */,
                                  double /* CO2 compensation point */);

/*!
 * @brief
 *      Stomatal conductance according to Leuning 1995.
 */
double
stomatal_conductance_leuning_1995_a(
                                    double /* vapour pressure deficit */,
                                    double /* co2 concentration */,
                                    double /* slope gsa */,
                                    double /* vpd reference */,
                                    double /* assimilation */,
                                    double /* relative humidity */,
                                    double /* CO2 compensation point */);

// modified BWB model according to Leuning 1995 with additional soil water influence
double
stomatal_conductance_leuning_1995_b(
                                    double /* co2 concentration */,
                                    double /* slope gsa */,
                                    double /* f_h2o_ref */,
                                    double /* f_h2o */,
                                    double /* assimilation */,
                                    double /* relative humidity */,
                                    double /* CO2 compensation point */);

// modified BWB model according to Medlyn et al. 1990
double
stomatal_conductance_medlyn_2011_a(
                                   double /* vapour pressure deficit */,
                                   double /* co2 concentration */,
                                   double /* slope gsa */,
                                   double /* assimilation */);

// modified BWB model according to Medlyn et al. 1990 with additional soil water influence
double
stomatal_conductance_medlyn_2011_b(
                                   double /* vapour pressure deficit */,
                                   double /* co2 concentration */,
                                   double /* slope gsa */,
                                   double /* f_h2o */,
                                   double /* assimilation */);

/*!
 * @brief
 *      Stomatal conductance according to Eller et al. 2020.
 */
double
stomatal_conductance_eller_2020(
                                double /* air pressure in kPa */,
                                double /* temperature in Kelvin */,
                                double /* vapour pressure deficit */,
                                double /* ci */,
                                double /* ca */,
                                double /* assimilation */,
                                double /* reference assimilation */,
                                double /* _rplant */,
                                double /* _psi_mean */,
                                double /* _psi_ref */,
                                double /* _psi_exp */);

} /* namespace ldndc */

#endif  /*  !LD_STOMATALCONDUCTANCE_H_  */
