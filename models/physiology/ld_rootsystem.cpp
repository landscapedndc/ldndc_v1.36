/*!
 * @file
 * 
 * @author
 *    david kraus (created on: may 6, 2014)
 */

#include  "physiology/ld_plantfunctions.h"
#include  "physiology/ld_rootsystem.h"
#include  "state/mbe_state.h"

#include  <kernel/io-kcomm.h>

#include  <logging/cbm_logging.h>
#include  <math/lmath-float.h>

#include  <constants/lconstants-conv.h>
#include  <input/soillayers/soillayers.h>
#include  <input/species/species.h>


/*!
 * @page veglibs
 * @section veglibs_root_system Root system
 * @subsection veglibs_roots Fine root distribution
 * Root distribution and root properties are generally fixed throughout the simulation period.
 * Only, if the initial condition of planting doesn't allow the soil profile to be fully rooted
 * (e.g. plants are too small), rooting depth increases linearly with \f$ GZRTZ \f$ until the
 * maximum root depth (defined by the soil profile) is reached (\f$ ZRTMC \f$).
 */

/*!
 * @page veglibs
 * @subsubsection veglibs_roots_grote Standard empirial root distribution
 * The distribution of fine root biomass \f$ m(z) \f$ within the rooting space
 * depends on the rooting depth and is calculated with the same empirical 
 * equation as the foliage biomass (@ref veglibs_canopy). The main difference
 * is that the parameter 'p' is generally below 1 now to get a concave distribution
 * (@cite grote_pretzsch:2002a).
 * \f[
 *   m(z) = M \cdot \frac{r_{ih} f(z)}{\int r_{ih} f(z)} \\
 *   r_{ih} = \frac{RD - z}{RD} \\
 *   f(z) = p^{100 \frac{z}{RD^2}}
 * \f]
 * \f$ RD \f$: root depth (m) \n
 * \f$ z \f$: distribution depth for which the percentage roots should be defined (between 0-1, from upper soil boundary to rooting depth) \n
 * \f$ p \f$: shape parameter
 *
 * @image html fineroots_distribution_grote.png "Fine roots biomass distribution" width=500
 * @image latex fineroots_distribution_grote.png "Fine roots biomass distribution"
 * @note This function is currently the standard option for PSIM/TREEDYN (implicitly) and PLAMOX
 */
void
ldndc::fineroots_biomass_distribution(
                                  size_t _sl_cnt_litter,
                                  size_t _sl_cnt_soil,
                                  double _ps,
                                  double _rooting_depth,
                                  double *_f_frt_sl,
                                  double const *_h_sl)
{
    if (  cbm::flt_greater_zero( _rooting_depth))
    {
        /*! scaling factor for root distribution (basically to enable root and canopy profile calculations with the same function)*/
        double const SCALE( 10.0);
        
        /*! fraction of fine roots in the litter layer*/
        double f_litter( 0.0);
        
        double litter_depth( 0.0);
        for ( size_t  sl = 0;  sl < _sl_cnt_litter;  ++sl)
        {
            litter_depth += _h_sl[sl];
        }
        
        /* if rooting depth <= litter depth neglect litter layer (new seedlings start in mineral layer)*/
        if ( cbm::flt_greater( litter_depth, _rooting_depth))
        {
            _sl_cnt_litter = 0;
            f_litter = 0.0;
        }
        /* else assume that fine roots are distributed according to litter height relative to soil profile*/
        else
        {
            f_litter = litter_depth / _rooting_depth;
        }
        
        for ( size_t  sl = 0;  sl < _sl_cnt_litter;  ++sl)
        {
            _f_frt_sl[sl] = f_litter / (double)_sl_cnt_litter;
        }
        
        /*! fraction of fine roots in the mineral soil */
        double const f_soil( 1.0 - f_litter);
        
        double h_cum( 0.0);
        double f_frt_sum( 0.0);
        for ( size_t  sl = _sl_cnt_litter;  sl < _sl_cnt_soil;  ++sl)
        {
            double const  h_cum_old( h_cum);
            h_cum += _h_sl[sl] * SCALE;
            
            estimate_single_layer_biomass(
                                          _ps, 0.0, SCALE * _rooting_depth,
                                          SCALE * _h_sl[sl], _f_frt_sl[sl],
                                          h_cum_old, h_cum);
            f_frt_sum += _f_frt_sl[sl];
        }
        
        for ( size_t  sl = _sl_cnt_litter;  sl < _sl_cnt_soil;  ++sl)
        {
            _f_frt_sl[sl] = _f_frt_sl[sl] / f_frt_sum * f_soil;
        }
    }
    else
    {
        for ( size_t sl = 0; sl < _sl_cnt_soil;  ++sl)
        {
            _f_frt_sl[sl] = 0.0;
        }
    }
}


namespace ldndc {

BaseRootSystemDNDC::~BaseRootSystemDNDC()
{}


RootSystemDNDC::RootSystemDNDC(
                               MoBiLE_State *_state,
                               cbm::io_kcomm_t *_io_kcomm)
        : BaseRootSystemDNDC(),

        m_soillayers( _io_kcomm->get_input_class< input_class_soillayers_t >()),
        m_pf( _state, _io_kcomm),
        sc_( _state->get_substate_ref< substate_soilchemistry_t >()),
        mc_( _state->get_substate_ref< substate_microclimate_t >()),
        //~ vs_( _state->get_substate_ref< substate_vegstructure_t >()),
        ph_( _state->get_substate_ref< substate_physiology_t >()),
        wc_( _state->get_substate_ref< substate_watercycle_t >()),
        m_veg( &_state->vegetation)
{
}


RootSystemDNDC::~RootSystemDNDC()
{
}


/*!
 * @brief
 * initializes root biomass and root biomass fraction across soil layers
 */
void
RootSystemDNDC::reset( MoBiLE_Plant * _vt)
{
    _vt->rooting_depth = 0.0;

    _vt->mFrt = 0.0;
    for ( size_t  l = 0;  l < _vt->nb_soillayers();  ++l)
    {
        _vt->fFrt_sl[l] = 0.0;
    }
}



/*!
 * @brief
 *  calls
 *  \link ldndc::set_rooting_depth() set_rooting_depth() \endlink \n
 */
void
RootSystemDNDC::root_development(
                                 double _increment,
                                 unsigned int & _root_q_vt,
                                 MoBiLE_Plant * _vt)
{
    set_rooting_depth( _root_q_vt, _vt, _vt->rooting_depth + _increment);
}



/*!
 * @brief
 *  Increases the rooting depth up to the species-specific maximum depth which is defined by
 *  allometric or phenological constraints. Rooting depth is further limited by soil depth.
 */
void
RootSystemDNDC::set_rooting_depth(
            unsigned int & _root_q_vt, MoBiLE_Plant * _vt,
            double _rooting_depth, double _max_rooting_depth)
{
    if ( cbm::is_valid( _max_rooting_depth))
    {
        _vt->rooting_depth = ( _rooting_depth > _max_rooting_depth) ? _max_rooting_depth : _rooting_depth;
        update_deepest_rooted_soil_layer_index(_root_q_vt, _vt, _vt->rooting_depth);
    }
    else
    {
        _vt->rooting_depth = ( _rooting_depth > (*_vt)->ZRTMC()) ? (*_vt)->ZRTMC() : _rooting_depth;
        update_deepest_rooted_soil_layer_index(_root_q_vt, _vt, _vt->rooting_depth);
    }
}


/*!
 * @brief
 *
 */
unsigned int
RootSystemDNDC::update_deepest_rooted_soil_layer_index(
                                                       unsigned int & _root_q_vt, MoBiLE_Plant * _vt,
                                                       double _rooting_depth)
{
    LDNDC_FIX_UNUSED(_vt);
    if ( cbm::flt_equal_zero( _rooting_depth))
    {
        ldndc_kassert( cbm::flt_equal_zero( _vt->mFrt));
        _root_q_vt = 0;
    }
    else
    {
        ldndc_kassert( cbm::flt_greater_zero( _vt->mFrt));

        double  h_cum( 0.0);
        for( size_t  l = 0;  l < _vt->nb_soillayers();  ++l)
        {
            h_cum += sc_.h_sl[l];
            if ( h_cum > _rooting_depth)
            {
                _root_q_vt = l + 1;
                return  std::min(_root_q_vt, (unsigned int)m_soillayers->soil_layer_cnt()-1);
            }
        }
        
        _root_q_vt = _vt->nb_soillayers();
    }
    
    return  std::min(_root_q_vt, (unsigned int)m_soillayers->soil_layer_cnt()-1);
}


/*!
 * @page veglibs
 * @subsubsection veglibs_roots_sigmoid Sigmoid root distribution
 * The root distribution decreases sigmoidel with the soil depth:
 * \f[
 *   m(z) = M \cdot \frac{f(z)}{\int f(z)} \\
 *   f(z) = \left( \frac{e^{(-\frac{(z - 0.1 \cdot RD)^2}{0.01 \cdot RD}}}{RD} + 0.1 \cdot L \right)
 * \f]
 * with
 * \f$ M \f$: root growth (kg m-2) \n
 * \f$ RD \f$: root depth (m) \n
 * \f$ z \f$: distribution depth for which the percentage roots should be defined (between 0-1, from upper soil boundary to rooting depth) \n
 * 
 * @image html fineroots_distribution_sigmoid.png "Fine roots biomass distribution" width=500
 * @image latex fineroots_distribution_sigmoid.png "Fine roots biomass distribution"
 * @note This function is currently the standard option for ARABLE-DNDC and an option to select in PLAMOX and GRASSLAND
*/
void
RootSystemDNDC::update_root_fraction_sigmoid(
                                             double _adaption_rate,
                                             unsigned int _deepest_rooted_layer,
                                             double _root_min,
                                             double _root_max,
                                             double _root_width,
                                             double _root_height,
                                             MoBiLE_Plant * _vt)
{
    
    ldndc_kassert( cbm::flt_greater_equal( 1.0, _adaption_rate));

    size_t sl = 0;
    double fFrt_sum( 0.0);

    for ( sl = 0;  sl < _deepest_rooted_layer;  ++sl)
    {
        double const fFrt_target( (exp( -( cbm::sqr( (sc_.depth_sl[sl] * cbm::CM_IN_M) - _root_max) / _root_width)) / _root_height + _root_min) * sc_.h_sl[sl]);
        _vt->fFrt_sl[sl] -= (_vt->fFrt_sl[sl] - fFrt_target) * _adaption_rate;
        _vt->fFrt_sl[sl] = cbm::bound_min( 0.0, _vt->fFrt_sl[sl]);
        fFrt_sum += _vt->fFrt_sl[sl];
    }

    if ( cbm::flt_greater_zero( fFrt_sum))
    {
        for ( sl = 0;  sl < _deepest_rooted_layer;  ++sl)
        {
            _vt->fFrt_sl[sl] /= fFrt_sum;
        }
        for ( ;  sl < _vt->nb_soillayers();  ++sl)
        {
            _vt->fFrt_sl[sl] = 0.0;
        }
    }
    else
    {
        for ( sl = 0;  sl < _vt->nb_soillayers();  ++sl)
        {
            _vt->fFrt_sl[sl] = 0.0;
        }
    }
}


/*!
 * @page veglibs
 * @subsubsection veglibs_roots_exponential Exponential root distribution
 * The root distribution decreases exponentially with the soil depth:
 * \f[
 *   m(z) = M \cdot \frac{f(z)}{\int f(z)} \\
 *   f(z) = \exp(-EXP\_ROOT\_DISTRIBUTION \cdot z)
 * \f]
 * with:
 * \f$ M \f$: root growth (kg m-2) \n
 * \f$ z \f$: distribution depth for which the percentage roots should be defined (between 0-1, from upper soil boundary to rooting depth) \n
 * \f$ EXP\_ROOT\_DISTRIBUTION \f$: species-specific distribution parameter \n
 *
 * @image html fineroots_distribution_exponential.png "Fine roots biomass distribution" width=500
 * @image latex fineroots_distribution_exponential.png "Fine roots biomass distribution"
 * @note This function is currently the standard option in ORYZA and GRASSLAND and can be selected in PLAMOX.
*/
void
RootSystemDNDC::update_root_fraction_exponential(
                                                 double _adaption_rate,
                                                 unsigned int _deepest_rooted_layer,
                                                 double _fact_exp,
                                                 MoBiLE_Plant *_vt)
{
    ldndc_kassert( cbm::flt_greater_equal( 1.0, _adaption_rate));

    size_t sl = 0;
    double fFrt_sum( 0.0);

    for ( sl = 0;  sl < _deepest_rooted_layer;  ++sl)
    {
        double const fFrt_target( exp(-_fact_exp * sc_.depth_sl[sl]) * sc_.h_sl[sl]);
        _vt->fFrt_sl[sl] -= (_vt->fFrt_sl[sl] - fFrt_target) * _adaption_rate;
        _vt->fFrt_sl[sl] = cbm::bound_min( 0.0, _vt->fFrt_sl[sl]);
        fFrt_sum += _vt->fFrt_sl[sl];
    }

    if ( cbm::flt_greater_zero( fFrt_sum))
    {
        for ( sl = 0;  sl < _deepest_rooted_layer;  ++sl)
        {
            _vt->fFrt_sl[sl] /= fFrt_sum;
        }
        for ( ;  sl < _vt->nb_soillayers();  ++sl)
        {
            _vt->fFrt_sl[sl] = 0.0;
        }
    }
    else
    {
        for ( sl = 0;  sl < _vt->nb_soillayers();  ++sl)
        {
            _vt->fFrt_sl[sl] = 0.0;
        }
    }
}


/*!
 * @page veglibs
 * @subsection veglibs_root_growth Root growth
 * The function is entered with total carbon allocated to the roots (with costs 
 * for growth respiration alread considered before), and it is then decided
 * where to put the new biomass according to specific rules. There is no
 * active redistribution of old root biomass but root biomass which is not
 * supported by growth will continously decrease due to senescence.
 */

/*!
 * @page veglibs
 * @subsubsection veglibs_root_growth_static Empirical root growth distribution
 * If no other options are defined, new root biomass is distributed to the different
 * soil layers according to the selected root distribution scheme (standard, sigmoidal,
 * or exponential). Therefore, the overall distribution stays constant.
 */
void
RootSystemDNDC::update_roots_static(
    const double FTS_TOT_,
    unsigned int& root_q_vt_,
    MoBiLE_Plant* _vt,
    double deltamassFrt,
    ldndc::growable_array < BaseRootSystemDNDC*, 1000, 1 >& root_system,
    double rootgrowthrestriction)
{
    if (cbm::flt_less(_vt->rooting_depth, (*_vt)->ZRTMC()))
    {
        _vt->rooting_depth += rootgrowthrestriction * (*_vt)->GZRTZ() * FTS_TOT_;
    }

    //update rooted layers
    root_q_vt_ = m_soillayers->soil_layer_cnt();
    for (size_t sl = 0; sl < m_soillayers->soil_layer_cnt(); ++sl)
    {
        if (cbm::flt_greater(sc_.depth_sl[sl], _vt->rooting_depth))
        {
            /* The layer depths are not necessarily commensurate with GZRTZ. So the current layer probably still holds roots.
             * For the first non-rooted layer the index needs to be shifted. */
            root_q_vt_ = (sl + 1);
            break;
        }
    }

    //root distribution
    if (cbm::flt_greater_zero((*_vt)->EXP_ROOT_DISTRIBUTION()))
    {
        root_system[_vt->slot]->update_root_fraction_exponential(1.0, root_q_vt_, (*_vt)->EXP_ROOT_DISTRIBUTION(), _vt);
    }
    else
    {
        root_system[_vt->slot]->update_root_fraction_sigmoid(1.0, root_q_vt_, 0.005, 10.0, 200.0, 10.0, _vt);
    }

    /*!
     * @details
     * specific root length for the inclusion of root length in every layer.
     */
    lvector_t< double > specifificrootlength(root_q_vt_, 99.0);
    specific_root_length(root_q_vt_, _vt, specifificrootlength);

    /*!
     * @details
     * root length in every layer
     */
    for (size_t sl = 0; sl <= root_q_vt_ - 1; ++sl)
    {
        _vt->rootlength_sl[sl] = specifificrootlength[sl] * _vt->fFrt_sl[sl] * (deltamassFrt + _vt->mFrt);
    }
}

/*!
 * @page veglibs
 * @subsubsection veglibs_roots_dynamicN Nutrient-based root distribution
 * Carbon distribution to roots is based on previous nitrogen uptake of a 
 * particular layer. Thus, roots grow stronger in soil layers with high nitrogen
 * availability.
 * @note This function is currently not used at all in LDNDC
 *
 * \cond Initial distribution also according to N distribution? \endcond
 * \cond Why the uptake and not the availability of N? \endcond
 * \cond TODO: daily? how runs the loop that day_n_demand is calculated for each day?! 
 *             or is it more plant_n_demand like in n fixation? \endcond
 * 
 */
void
RootSystemDNDC::update_root_fraction_nutrient_based(
                                                    double _adaption_rate,
                                                    unsigned int _deepest_rooted_layer,
                                                    lvector_t< double > &_layer_specific_nitrogen_uptake,
                                                    MoBiLE_Plant * _vt)
{
    ldndc_kassert( cbm::flt_greater_equal( 1.0, _adaption_rate));

    double const total_nitrogen_uptake( _layer_specific_nitrogen_uptake.sum());
    if ( cbm::flt_greater_zero( total_nitrogen_uptake))
    {
        size_t sl = 0;
        for ( sl = 0;  sl < _deepest_rooted_layer;  ++sl)
        {
            double const fFrt_target( _layer_specific_nitrogen_uptake[sl] / total_nitrogen_uptake);
            _vt->fFrt_sl[sl] -= (_vt->fFrt_sl[sl] - fFrt_target) * _adaption_rate;
            _vt->fFrt_sl[sl] = cbm::bound_min( 0.0, _vt->fFrt_sl[sl]);
        }

        for (;  sl < _vt->nb_soillayers();  ++sl)
        {
            _vt->fFrt_sl[sl] = 0.0;
        }

        // CB: determine the fractions of frts in each layer from all frts
        double fFrt_sum( 0.0);
        for ( sl = 0;  sl < _deepest_rooted_layer;  ++sl)
        {
            fFrt_sum += _vt->fFrt_sl[sl];
        }
        for ( sl = 0;  sl < _deepest_rooted_layer;  ++sl)
        {
            _vt->fFrt_sl[sl] /= fFrt_sum;
        }
    }
}


/*!
 * @details
 * 
 */
void
RootSystemDNDC::nitrogen_uptake(
                                unsigned int _deepest_rooted_layer,
                                double &_n_plant,
                                double _n_opt,
                                double _fts_tot,
                                lvector_t< double > _no3_sl,
                                lvector_t< double > _no3_uptake_sl,
                                lvector_t< double > _nh4_sl,
                                lvector_t< double > _nh4_uptake_sl,
                                lvector_t< double > _don_sl,
                                lvector_t< double > _don_uptake_sl,
                                lvector_t< double > _temp_sl,
                                MoBiLE_Plant *  _vt)
{
    /* Nitrogen status update  */
    double const n_demand( _n_opt - _n_plant);

    if ( cbm::flt_greater_zero( n_demand))
    {
        double n_up_total(0.0);
        for (size_t sl = 0; sl <= _deepest_rooted_layer; sl++)
        {
            double const mFrt( cbm::bound_min( 0.01, _vt->mFrt) * _vt->fFrt_sl[sl]);
            if ( !cbm::flt_greater_zero( mFrt))
            {
                break;
            }

            double const fact_t( m_pf.nitrogen_uptake_temperature_dependency(
                                                                     0.8 * (*_vt)->TLIMIT(),
                                                                     1.2 * (*_vt)->TLIMIT(),
                                                                     _temp_sl[sl]));

            double const nh4_up( cbm::bound_max( (*_vt)->US_NH4() * mFrt * fact_t * _fts_tot
                                                 * _nh4_sl[sl] / (_nh4_sl[sl] + (*_vt)->K_MM_NITROGEN_UPTAKE() * sc_.h_sl[sl]),
                                                 0.9 * _nh4_sl[sl]));
            double const no3_up( cbm::bound_max( (*_vt)->US_NO3() * mFrt * fact_t * _fts_tot
                                                 * _no3_sl[sl] / (_no3_sl[sl] + (*_vt)->K_MM_NITROGEN_UPTAKE() * sc_.h_sl[sl]),
                                                 0.9 * _no3_sl[sl] ));
            double const don_up( cbm::bound_max( (*_vt)->US_NO3() * mFrt * fact_t * _fts_tot
                                                 * _don_sl[sl] / (_don_sl[sl] + (*_vt)->K_MM_NITROGEN_UPTAKE() * sc_.h_sl[sl]),
                                                 0.9 * _don_sl[sl]));
            n_up_total += nh4_up + no3_up + don_up;
        }


        double const scale( ( n_up_total > 0.0) ? cbm::bound_max(n_demand / n_up_total, 1.0) : 0.0);

        if ( cbm::flt_greater_zero( scale))
        {
            for (size_t sl = 0; sl <= _deepest_rooted_layer; sl++)
            {
                double const mFrt( cbm::bound_min( 0.01, _vt->mFrt) * _vt->fFrt_sl[sl]);

                if ( !cbm::flt_greater_zero( mFrt))
                {
                    break;
                }

                double const fact_t( m_pf.nitrogen_uptake_temperature_dependency(
                                                                                 0.8 * (*_vt)->TLIMIT(),
                                                                                 1.2 * (*_vt)->TLIMIT(),
                                                                                 _temp_sl[sl]));

                double const nh4_up( scale * cbm::bound_max( (*_vt)->US_NH4() * mFrt * fact_t * _fts_tot
                                                            * _nh4_sl[sl] / (_nh4_sl[sl] + (*_vt)->K_MM_NITROGEN_UPTAKE() * sc_.h_sl[sl]),
                                                            0.9 * _nh4_sl[sl]));
                double const no3_up( scale * cbm::bound_max( (*_vt)->US_NO3() * mFrt * fact_t * _fts_tot
                                                            * _no3_sl[sl] / (_no3_sl[sl] + (*_vt)->K_MM_NITROGEN_UPTAKE() * sc_.h_sl[sl]),
                                                            0.9 * _no3_sl[sl] ));
                double const don_up( scale * cbm::bound_max( (*_vt)->US_NO3() * mFrt * fact_t * _fts_tot
                                                            * _don_sl[sl] / (_don_sl[sl] + (*_vt)->K_MM_NITROGEN_UPTAKE() * sc_.h_sl[sl]),
                                                            0.9 * _don_sl[sl]));
                if ( cbm::flt_greater_zero( nh4_up))
                {
                    _nh4_uptake_sl[sl] += nh4_up;
                    _nh4_sl[sl] -= nh4_up;
                    if ( _nh4_sl[sl] < 0.0)
                    {
                        _nh4_sl[sl] = 0.0;
                    }
                    _n_plant += nh4_up;
                }

                if ( cbm::flt_greater_zero( no3_up))
                {
                    _no3_uptake_sl[sl] += no3_up;
                    _no3_sl[sl] -= no3_up;
                    if ( _no3_sl[sl] < 0.0)
                    {
                        _no3_sl[sl] = 0.0;
                    }
                    _n_plant += no3_up;
                }

                if ( cbm::flt_greater_zero(don_up))
                {
                    _don_uptake_sl[sl] += don_up;
                    _don_sl[sl] -= don_up;
                    if ( _don_sl[sl] < 0.0)
                    {
                        _don_sl[sl] = 0.0;
                    }
                    _n_plant += don_up;
                }
            }
        }
    }
}


/*!
 * @page veglibs
 * @subsubsection veglibs_roots_sinkstrength Sink-strength driven root distribution
 * Root biomass distribution may be limited by various influences that prevent 
 * the realization of a particular distribution scheme. Based on an optimum root 
 * biomass distribution assuming either exponential or sigmoidal functions, these
 * restrictions are calculated separately for all relevant layers, following @cite jones:1991a.
 * @note This function is available as an option in PLAMOX.
 * 
 */
void
RootSystemDNDC::update_roots_dynamic(
                                        const double FTS_TOT_,
                                        unsigned int &root_q_vt_,
                                        MoBiLE_Plant * _vt,
                                        double deltamassFrt)
{
    size_t sl;
    //~ LOGINFO(" ");
    //~ LOGINFO("Incoming data:");
    //~ LOGINFO("_vt->mFrt ", _vt->mFrt);
    //~ LOGINFO("deltamassFrt ", deltamassFrt);
    //~ LOGINFO("growing_degree_days ", _vt->growing_degree_days);
    //~ LOGINFO("root_q_vt_ ", root_q_vt_);
    //~ LOGINFO( "_vt->rooting_depth ", _vt->rooting_depth);
    //~ for( sl = 0; sl < root_q_vt_; sl++)
    //~ {
        //~ LOGINFO("fFrt_sl[", sl, "] ", _vt->fFrt_sl[sl]);
        //~ LOGINFO("rootlength[", sl, "] ", _vt->rootlength_sl[sl]);
        //~ LOGINFO("SRL[", sl, "] ", _vt->rootlength_sl[sl]/(_vt->fFrt_sl[sl]*_vt->mFrt));
    //~ }
    //~ LOGINFO("  ");
    
    /*!
     * @page veglibs
     * Sink driven allocation starts and ends with germination period. Outside this period, 
     * carbon distribution is done empirically. The different restrictions within the
     * relevant period are considered as follows:
     */
    if( cbm::flt_equal_zero( deltamassFrt))
    {
        double redistribute = 1; // per day
        if( cbm::flt_greater( _vt->growing_degree_days, (*_vt)->GDD_EMERGENCE()))
        {
            redistribute = (*_vt)->REDISTRIBUTION_ROOT(); // per day
        }
        deltamassFrt = redistribute * FTS_TOT_ * _vt->mFrt;
        //~ LOGINFO("deltamassFrt ", deltamassFrt, " _vt->mFrt ", _vt->mFrt);
        update_rootlength_meanSRL_allsl( root_q_vt_, _vt, -deltamassFrt);
        //~ LOGINFO("Set deltamassFrt ", deltamassFrt);
    }
    
    // store data from last time step and the root mass in every layer
    size_t root_q_vt_tminusone = root_q_vt_;
    lvector_t< double > mFrt_sl_tminusone( root_q_vt_tminusone, 99.);
    for ( sl = 0;  sl < root_q_vt_tminusone;  ++sl)
    {
        mFrt_sl_tminusone[sl] = _vt->fFrt_sl[sl] * (_vt->mFrt - deltamassFrt); // mass(t=i-1)
    }
    
    /*!
     * @details
     * Update rooting depth
     */
    double target_rooting_depth_increase = 99.;
    unsigned int target_iterator_first_non_rooted_layer = 99;
    determine_target_rooting_depth_increase(target_rooting_depth_increase, target_iterator_first_non_rooted_layer, FTS_TOT_, _vt);
    
    /*!
     * @page veglibs
     * I Restrictions due to coarse fragments
     * \f[
     *   S_{Fc}(sl) = 1 - stone\_content [\text{volume fraction}](sl)
     * \f]
     * with
     * - sl: counter of soil layers     
     *
     * II Restrictions due to bulk density
     * The bulk density enters via \f$S_{BD}\f$, which is given by
     * \f[
     *   S_{BD} = 1, \quad  BD < BD_O\,,\\
     *   S_{BD} = \frac{BD_X-BD}{BD_X-BD_O}, \quad BD_O \leq BD \leq BD_X\,,\\
     *   S_{BD} = 0, \quad  BD > BD_X\, ,
     * \f]
     * where the threshold \f$BD_O\f$ is the bulk density at which root growth is first affected,
     * and \f$BD_X\f$ is the bulk density above which no root growth occurs. Following @cite jones:1991a,
     * these parameters can be estimated from the sand content [weight fraction] \f$sand\f$
     * \f[
     *   BD_O = 1.1 + 0.005 \cdot sand\, ,\\
     *   BD_X = 1.6 + 0.004 \cdot sand\, .
     * \f]
     * 
     * III Restrictions due to poor aeration
     * Plants do not have any stress if the water filled porosity is below a critical value,
     * whereas above this critical value the stress factor scales linearly between 1 and 0
     * in a fully saturated soil.
     * \f[
     *   S_{AI} = S_{FT} + (1- WFP) \cdot \frac{1- S_{FT}}{1 - CWP}, \quad  WFP \geq CWP  \,,\\
     *   S_{AI} = 1, \quad  WFP < CWP \,.
     * \f]
     * With the fraction of water filled pore space calculated as:
     * \f[
     *   WFP = \frac{\text{water filled porosity}}{\text{total porosity}}
     * \f]
     * The critical value of water filled pore space depends on the clay content:
     * \f[
     *   CWP = 0.4 + 0.004 \cdot clay \, .
     * \f]
     * 
     * IV Restriction due to temperature stress
     * If the soil temperature is below a critical value ($T_{BS}$), root growth doesn't occur. 
     * Similar, too hot conditions also restricted carbon allocation into roots. Here, it is 
     * assumed that the limitations occur symmetrically around an species-specifc optimum temperature
     * $T_{OP}$, using a sinus function around an optimum temperature to describe the dependency.
     * \f[
     *   S_{TP} = 0, \quad  T<T_{BS} \,,\\
     *   S_{TP} = \sin\left( \frac{\pi}{2} \frac{T-T_{BS}}{T_{OP}-T_{BS}}\right), \quad   T_{BS} \leq T \leq 2T_{OP} - T_{BS}\,,\\
     *   S_{TP} = 0, \quad   T > 2T_{OP} - T_{BS}\,.
     * \f]
     * 
     * V Restriction due to flooding
     * In addition, a species-(or genotype-)specific sensititivity to flooding \f$S_{FT}\f$ 
     * is introduced.
     * \f[
     *   S_{FT} = 0, \quad \text{corn, maize, soybean, etc.} \,,\\
     *   S_{FT} = 1, \quad \text{rice, etc.} \,.
     * \f]
     *
     * \cond It seems that aeration problems and flooding stress is double accounting for the same stress \endcond
     */
    lvector_t< double > rootrestrictions_coarse_fragments = determine_root_restrictions_coarse_fragments(target_iterator_first_non_rooted_layer);
    lvector_t< double > rootrestrictions_soil_strength = determine_root_restrictions_soil_strength(target_iterator_first_non_rooted_layer);
    lvector_t< double > rootrestrictions_aeration = determine_root_restrictions_aeration(target_iterator_first_non_rooted_layer, m_veg->is_family( _vt, ":rice:"));    
    lvector_t< double > rootrestrictions_temperature = determine_root_restrictions_temperature(target_iterator_first_non_rooted_layer, _vt);
    
    /*!
     * @page veglibs
     * The combination of individual restrictions is then done following @cite jones:1991a :
     * \f[
     *   \Delta rd_{red} = \Delta rd_{pot} \cdot \min\left(STP, \min(SST,SAI,SCF)^{0.5} \right),
     * \f]
     * The restrictions in the bottom layer are assumed to represent a reasonable simplification
     * as long as the time steps are small so that root elongation is also small in comparison
     * to the soil layer widths. 
     */
    unsigned int target_iterator_deepest_rooted_layer = target_iterator_first_non_rooted_layer-1;
    double rootgrowthrestrictiona = pow(std::min({rootrestrictions_coarse_fragments[target_iterator_deepest_rooted_layer], rootrestrictions_soil_strength[target_iterator_deepest_rooted_layer], rootrestrictions_aeration[target_iterator_deepest_rooted_layer]}), 0.5);
    double rootgrowthrestriction = std::min(rootrestrictions_temperature[target_iterator_deepest_rooted_layer], rootgrowthrestrictiona);
    
    /*!
     * @details
     * calculate new rooting depth
     */
    if( cbm::flt_greater_zero( deltamassFrt))
    {
        update_rooting_depth(target_rooting_depth_increase, rootgrowthrestriction, root_q_vt_, _vt);
    }
    
    /*!
     * @details
     * root mass determination in every layer before new C was allocated
     */
    lvector_t< double > mFrt_sl( root_q_vt_, 99.);
    for ( sl = 0;  sl < root_q_vt_;  ++sl)
    {
        mFrt_sl[sl] = _vt->fFrt_sl[sl] * (_vt->mFrt - deltamassFrt); // mass(t=i-1)
        //~ LOGINFO(sl, " ", mFrt_sl[sl]);
    }
    
    if( cbm::flt_greater_zero( deltamassFrt))
    {
        /*!
         * @details
         * determine an optimal root mass distribution
         */
        lvector_t< double > fFrtopt_sl( root_q_vt_, 99.);
        double normalizeoptdist = 0.;
        for ( sl = 0;  sl < root_q_vt_;  ++sl)
        {
            // todo: could be unified with functions in static root model
            // exponential:
            if( cbm::flt_greater_zero( (*_vt)->EXP_ROOT_DISTRIBUTION()))
            {
                fFrtopt_sl[sl] = exp(-(*_vt)->EXP_ROOT_DISTRIBUTION() * sc_.depth_sl[sl]) * sc_.h_sl[sl];
            }
            else
            {   
                // sigmoid:
                if( cbm::flt_equal_zero( (*_vt)->EXP_ROOT_DISTRIBUTION()))
                {
                    double const _root_min = 0.005;
                    double const _root_max = 10.0;
                    double const _root_width = 200.0;
                    double const _root_height = 10.0;
                    fFrtopt_sl[sl] = ( (exp( -( cbm::sqr( (sc_.depth_sl[sl] * cbm::CM_IN_M) - _root_max) / _root_width)) / _root_height + _root_min) * sc_.h_sl[sl]);
                }
            }
            normalizeoptdist += fFrtopt_sl[sl];
        }
        
        /*!
         * @details
         * take root restrictions in every layer into account and determine the target root distribution
         * 
         * Note: A warning message is given out, if all layers have a restriction below some threshold (0.05).
         * In this case C-allocation to roots should be modified, which is not yet implemented.
         */
        //~ bool mostlyrestricted = true;
        //~ double mostlyrestrictedlimit = 0.05;
        //~ bool totallyrestricted = true;
        lvector_t< double > fFrtoptrest_sl( root_q_vt_, 99.);
        double normalizeoptrestdist = 0.;
        for ( sl = 0;  sl < root_q_vt_;  ++sl)
        {
            double restrictions = std::min({rootrestrictions_coarse_fragments[sl], rootrestrictions_soil_strength[sl], rootrestrictions_aeration[sl], rootrestrictions_temperature[sl]});
            fFrtoptrest_sl[sl] = restrictions * fFrtopt_sl[sl] / normalizeoptdist;
            // if a single layer has a restriction factor bigger than the limit, no warning is given out
            //~ if ( cbm::flt_greater( restrictions, mostlyrestrictedlimit))
            //~ {
                //~ mostlyrestricted = false;
            //~ }
            //~ if ( cbm::flt_greater_zero( restrictions))
            //~ {
                //~ totallyrestricted = false;
            //~ }
            normalizeoptrestdist += fFrtoptrest_sl[sl];
        }
        //~ if (mostlyrestricted == true)
        //~ {
            //~ if (totallyrestricted == true)
            //~ {
                //~ LOGWARN("All restrictions are 0!");
            //~ }
        //~ }
        
        lvector_t< double > mFrtoptrest_sl( root_q_vt_, 99.);
        for ( sl = 0;  sl < root_q_vt_;  ++sl)
        {
            if ( cbm::flt_greater_zero( normalizeoptrestdist))
            {
                mFrtoptrest_sl[sl] = fFrtoptrest_sl[sl] / normalizeoptrestdist * _vt->mFrt; // mass(t=i)
            }
            else
            {
                // if restrictions in all layers are 0 -> no restrictions
                mFrtoptrest_sl[sl] = fFrtopt_sl[sl] / normalizeoptdist * _vt->mFrt; // mass(t=i)
            }
        }
        
        /*!
         * @details
         * determine the changes in mass.
         * -> Negative values are set to 0. Root decrease happens elsewhere.
         */
        lvector_t< double > deltamFrtpot_sl( root_q_vt_, 99.);
        double deltaMFrtpot( 0.0);
        for ( sl = 0;  sl < root_q_vt_;  ++sl)
        {
            deltamFrtpot_sl[sl] = mFrtoptrest_sl[sl] - mFrt_sl[sl];     // t=i  - t=i-1  overall >0
            if( cbm::flt_greater( 0., deltamFrtpot_sl[sl]))
            {
               deltamFrtpot_sl[sl] = 0.; 
            }
            deltaMFrtpot += deltamFrtpot_sl[sl];
        }
        //~ if (mostlyrestricted == true)
        //~ {
            //~ LOGWARN("deltamFrtpot_sl\n", deltamFrtpot_sl);
            //~ LOGWARN("mFrt_sl \n", mFrt_sl);
        //~ }
        
        
        /*!
         * @details
         * add actual root mass increase (gets rescaled to the full new biomass)
         */
        if( cbm::flt_greater_zero(deltaMFrtpot))
        {
            for ( sl = 0;  sl < root_q_vt_;  ++sl)
            {
                mFrt_sl[sl] += deltamassFrt/deltaMFrtpot * deltamFrtpot_sl[sl];         // t=i
                //~ LOGINFO(sl, " ", mFrt_sl[sl]);
            }
        }
        else
        {
            if( !cbm::flt_greater_equal_zero(deltaMFrtpot))
            {
                LOGERROR( "deltaMFrtpot smaller 0? ", deltaMFrtpot);
            }
            // no potential growth (for instance, fully frozen ground)
            // However, the root mass must currently go somewhere..
            // todo: potentially better to allocate less C to roots
            for ( sl = 0;  sl < root_q_vt_;  ++sl)
            {
                //~ mFrt_sl[sl] = _vt->fFrt_sl[sl] * _vt->mFrt;
                mFrt_sl[sl] += _vt->fFrt_sl[sl] * deltamassFrt;
            }
        }
        //~ if (mostlyrestricted == true)
        //~ {
            //~ LOGWARN(mFrt_sl, "\n");
        //~ }
        
        /*!
         * @details
         * determine and store the root distribution
         */
        double normalizedend = 0.;
        for ( sl = 0;  sl < root_q_vt_;  ++sl)
        {
            normalizedend += mFrt_sl[sl]/_vt->mFrt;     // t=i
        }
        double addtocheck = 0.;
        for ( sl = 0;  sl < root_q_vt_;  ++sl)
        {
            _vt->fFrt_sl[sl] = mFrt_sl[sl]/_vt->mFrt/normalizedend;
            addtocheck += _vt->fFrt_sl[sl];
        }
        for (;  sl < _vt->nb_soillayers();  ++sl)
        {
            _vt->fFrt_sl[sl] = 0.0;
        }
        if( ! cbm::flt_equal(addtocheck, 1.0))
        {
            LOGERROR("distribution of fine roots not normalized!", addtocheck);
        }
    } // end if deltamass > 0

    // if deltamass < 0:
    // * fractions already changed in senescence
    // * fractions dont need changing from respiration and exudation
    // rootlength need to be reduced
 
    // specific root length 
    lvector_t< double > specifificrootlength(root_q_vt_, 99.);
    specific_root_length(root_q_vt_, _vt, specifificrootlength);
    // use restrictions for SRL
    for( sl = 0;  sl < root_q_vt_;  ++sl)
    {
        specifificrootlength[sl] = specifificrootlength[sl]/(4.-3.*std::min(rootrestrictions_coarse_fragments[sl], rootrestrictions_soil_strength[sl]));
    }
    
    /*!
     * @details
     * Root length in every soil layer
     */
    for ( sl = 0;  sl < root_q_vt_;  ++sl)
    {
        if( sl < root_q_vt_tminusone)
        {
            double deltamass_sl = mFrt_sl[sl] - mFrt_sl_tminusone[sl];
            if( cbm::flt_greater( deltamass_sl, 0.0))
            {   
                _vt->rootlength_sl[sl] += specifificrootlength[sl] * deltamass_sl;
                if( cbm::flt_greater( specifificrootlength[sl], (*_vt)->SRLMAX()+0.000001))
                {
                    LOGERROR("specifificrootlength[sl]", specifificrootlength[sl], " larger than SRLMAX ", (*_vt)->SRLMAX());
                }
            }
            else
            {
                if( cbm::flt_greater( 0.0, deltamass_sl))
                {
                    double srl_mean_sl = _vt->rootlength_sl[sl] / mFrt_sl_tminusone[sl];             // rootlength t-1 -> take rootmass t-1
                    _vt->rootlength_sl[sl] += srl_mean_sl * deltamass_sl;
                    if( cbm::flt_greater( specifificrootlength[sl], (*_vt)->SRLMAX()+0.000001))
                    {
                        LOGERROR("Subtract rootlength (respiration etc.) srl_mean_sl ", srl_mean_sl, " larger than SRLMAX ", (*_vt)->SRLMAX());
                    }
                }
            }
        }
        else
        {
            _vt->rootlength_sl[sl] = 0;         // @David; could this not be set at initialization/planting?
            double deltamass_sl = mFrt_sl[sl];
            if( cbm::flt_greater( deltamass_sl, 0.0))
            {   
                _vt->rootlength_sl[sl] += specifificrootlength[sl] * deltamass_sl;
            }
            else
            {
                LOGERROR("newly rooted layer without biomass!!");
                LOGERROR(_vt->growing_degree_days);
            }
        }
    }
    //~ else
    //~ {
        //~ root_system[_vt->slot]->update_roots_static( FTS_TOT_, root_q_vt_, _vt, deltamassFrt, root_system, rootgrowthrestriction/2.);
        //~ // since the root growth is defined for optimum conditions, considereing Carbon allocation, and since in this case
        //~ // downwardsgrowth is independent of allocation, we slow the growth rate by 50%, which roughly relates to the number of
        //~ // time steps in which C is allocated to the roots.
    //~ }
    
    //~ LOGINFO(" ");
    //~ LOGINFO("Outgoing data:");
    //~ LOGINFO("_vt->mFrt ", _vt->mFrt);
    //~ LOGINFO("deltamassFrt ", deltamassFrt);
    //~ LOGINFO("root_q_vt_ ", root_q_vt_);
    //~ LOGINFO( "_vt->rooting_depth ", _vt->rooting_depth);
    //~ for( sl = 0; sl < root_q_vt_; sl++)
    //~ {
        //~ LOGINFO("fFrt_sl[", sl, "] ", _vt->fFrt_sl[sl]);
        //~ LOGINFO("rootlength[", sl, "] ", _vt->rootlength_sl[sl]);
        //~ LOGINFO("SRL[", sl, "] ", _vt->rootlength_sl[sl]/(_vt->fFrt_sl[sl]*_vt->mFrt));
    //~ }
    //~ LOGINFO("  ");
    //~ LOGINFO(" ");
    //~ LOGINFO(" ");
}


/*!
 * @page veglibs
 * @subsection veglibs_static_root_growth Root length
 * While root biomass is defined by one of the rooting distribution schemes, root length is
 * calculated based on root biomass but considering a specific root length for every single
 * soil layer. Specific root lenghts scales linearly between \f$SRLMIN\f$ and \f$SRLMAX\f$ as:
 * \f[
 *  specific\_{rootlength}_{sl} = SRLMAX - dvsFlush \cdot (SRLMAX - SRLMIN) \cdot scale_{sl};
 * \f]
 * \f[
    scale = 1.0 - \frac {depth_{sl}} {depth\_{root}}
 * \f]
 * with
 * - sl: counter of soil layers
 * - dvsFlush: foliage development stage (0-1, 1 if fully grown leaves/needles are established)
 * - depth: depth of the respective soil layer
 * - depth_root: rooting depth
 */

/*!
 * @detail
 *  specific root length calculated for every rooted layer
 */
void
RootSystemDNDC::specific_root_length(
    unsigned int& root_q_vt_,
    MoBiLE_Plant* _vt,
    lvector_t< double >& specifificrootlength)
{
    for (size_t sl = 0; sl < root_q_vt_; ++sl)
    {
        double const scale(cbm::bound(0.0, 1.0 - sc_.depth_sl[sl] / sc_.depth_sl[root_q_vt_ - 1], 1.0));
        // GS = growth stage elem [0,1]
        specifificrootlength[sl] = (*_vt)->SRLMAX() - _vt->dvsFlush * ((*_vt)->SRLMAX() - (*_vt)->SRLMIN()) * scale;
        //~ LOGINFO("SRL[", sl, "] ", specifificrootlength[sl], " dvsFlush ", _vt->dvsFlush, " scale ", scale);
    }
}


/*!
 * @brief
 * root length reduction when root mass is lost
 */
void
RootSystemDNDC::update_rootlength_meanSRL_allsl(
    unsigned int& root_q_vt_,
    MoBiLE_Plant* _vt,
    double deltamass)
{
    for (size_t sl = 0; sl < root_q_vt_; ++sl)
    {
        if (cbm::flt_greater_zero(_vt->rootlength_sl[sl]))
        {
            if (cbm::flt_greater_zero(_vt->fFrt_sl[sl]))
            {
                double srl_mean_sl = _vt->rootlength_sl[sl] / (_vt->mFrt * _vt->fFrt_sl[sl]);
                //~ LOGINFO(sl, " srl_mean_sl ", srl_mean_sl);
                //~ LOGINFO(sl, " _vt->mFrt ", _vt->mFrt);
                //~ LOGINFO(sl, " _vt->fFrt_sl[sl] ", _vt->fFrt_sl[sl]);
                //~ LOGINFO(sl, " _vt->rootlength_sl[sl] ", _vt->rootlength_sl[sl]);
                //~ LOGINFO("SRL[", sl, "] ", _vt->rootlength_sl[sl]/(_vt->fFrt_sl[sl]*_vt->mFrt));
                if (cbm::flt_greater(srl_mean_sl, (*_vt)->SRLMAX() + 0.0000001))
                {
                    LOGERROR(sl, " srl_mean_sl ", srl_mean_sl, " larger than SRLMAX ", (*_vt)->SRLMAX());
                }
                //~ if( cbm::flt_greater( (*_vt)->SRLMIN()-0.0000001, srl_mean_sl))
                //~ {
                    //~ LOGERROR(sl, " srl_mean_sl ", srl_mean_sl, " smaller than SRLMIN ", (*_vt)->SRLMIN());
                //~ }
                _vt->rootlength_sl[sl] += srl_mean_sl * deltamass * _vt->fFrt_sl[sl];
                //~ LOGINFO("SRL[", sl, "] ", _vt->rootlength_sl[sl]/(_vt->fFrt_sl[sl]*_vt->mFrt));
                //~ LOGINFO(" ");
            }
        } // else: wasn't set yet for this layer (newly rooted layer)
    }
}


/*!
 * @brief
 * This functions determines the target depth of the roots under optimum conditions in this time step.
 * A linear increase with the slope GZRTZ is assumed.
 */
void
RootSystemDNDC::determine_target_rooting_depth_increase(
                                                double &target_rooting_depth_increase,
                                                unsigned int &target_iterator_first_non_rooted_layer,
                                                const double FTS_TOT_,
                                                MoBiLE_Plant * _vt)
{
    // growth: currently linear as in Jones 1991
    // a) similar to conventional model
    target_rooting_depth_increase = (*_vt)->GZRTZ() * FTS_TOT_;
    // b) as suggested by Jones
    // todo: what is deltaGDD?
    //~ target_rooting_depth_increase = (*_vt)->GZRTZ() * (_vt->growing_degree_days - GDD(t-1)???) / (*_vt)->GDD_ROOTS_GROWN();
    
    double target_rooting_depth ( _vt->rooting_depth + target_rooting_depth_increase);
    if( cbm::flt_greater( (*_vt)->ZRTMC(), target_rooting_depth) && cbm::flt_greater( (*_vt)->GDD_ROOTS_GROWN(), _vt->growing_degree_days))
    {
        // all good
    }
    else{
        target_rooting_depth -= target_rooting_depth_increase;
        target_rooting_depth_increase = 0.;
    }
    
    // update iterator of the deepest rooted layer
    target_iterator_first_non_rooted_layer = m_soillayers->soil_layer_cnt();
    for ( size_t  sl = 0;  sl < m_soillayers->soil_layer_cnt();  ++sl)
    {
        if ( cbm::flt_greater( sc_.depth_sl[sl], target_rooting_depth))
        {
            /* CB: The layer depths are not necessarily commensurate with GZRTZ. So the current layer probably still holds roots.
             * For the first NON-ROOTED layer the index needs to be shifted. */
            target_iterator_first_non_rooted_layer = (sl+1);
            break;
        }
    }
}



/*!
 * @brief
 * This functions determines the depth of the roots.
 * 
 */
void
RootSystemDNDC::update_rooting_depth(
                                    const double target_rooting_depthincrease,
                                    double &growthrestriction,
                                    unsigned int &root_q_vt_,
                                    MoBiLE_Plant * _vt)
{
    // 1. New depth
    // linear
    // GZRTZ() is the growth rate under optimum conditions
    double newrootingdepth = _vt->rooting_depth + target_rooting_depthincrease * growthrestriction;
    if( cbm::flt_greater( (*_vt)->ZRTMC(), newrootingdepth) && cbm::flt_greater( (*_vt)->GDD_ROOTS_GROWN(), _vt->growing_degree_days))
    {
        _vt->rooting_depth = newrootingdepth;
    }
    
    // 2. Update iterator of the deepest rooted layer
    root_q_vt_ = m_soillayers->soil_layer_cnt();
    for ( size_t  sl = 0;  sl < m_soillayers->soil_layer_cnt();  ++sl)
    {
        if ( cbm::flt_greater( sc_.depth_sl[sl], _vt->rooting_depth))
        {
            /* The layer depths are not necessarily commensurate with GZRTZ. So the current layer probably still holds roots.
             * For the first non-rooted layer the index needs to be shifted. */
            root_q_vt_ = (sl+1);
            break;
        }
    }
}


/*!
 * @brief
 * Accounting for stones and other coarse fragments.
 *
 */
lvector_t< double >
RootSystemDNDC::determine_root_restrictions_coarse_fragments(
                                       unsigned int iterator_potentially_deepest_rooted_layer)
{
    lvector_t< double > layerrestrictions( iterator_potentially_deepest_rooted_layer, 99.);
    for( unsigned int sl = 0;  sl < iterator_potentially_deepest_rooted_layer;  ++sl)
    {
        layerrestrictions[sl] = 1.0 - sc_.stone_frac_sl[sl];
        //~ std::cout << layerrestrictions[sl] << std::endl;
        
        if( !cbm::flt_in_range_lu( 0., layerrestrictions[sl], 1.))
        {
            LOGERROR( "layerrestriction coarse fragments in layer ", sl, " < 0 or > 1: ", layerrestrictions[sl]);
        }
    }
    return layerrestrictions;
}


/*!
 * @brief
 * Accounting for bulk density in relation to sand and water content.
 * 
 */
lvector_t< double >
RootSystemDNDC::determine_root_restrictions_soil_strength(
                                       unsigned int iterator_potentially_deepest_rooted_layer)
{
    lvector_t< double > layerrestrictions( iterator_potentially_deepest_rooted_layer, 99.);
    for( unsigned int sl = 0;  sl < iterator_potentially_deepest_rooted_layer;  ++sl)
    {
        layerrestrictions[sl] = 1.;
        // bulk density
        // estimate limits via sand content (without stones)
        double bdo = 1.1 + 0.5 * sc_.sand_sl[sl];
        double bdx = 1.6 + 0.4 * sc_.sand_sl[sl];
        // Reduction due to stones is already contained in coarse fragments.
        double densitywithoutstones = (sc_.dens_sl[sl] - sc_.stone_frac_sl[sl] * cbm::DMIN) / (1. - sc_.stone_frac_sl[sl]);
        if( cbm::flt_greater( bdo, densitywithoutstones))
        {
            layerrestrictions[sl] = 1.;
        }
        else
        {
            if( cbm::flt_greater_equal( bdx, densitywithoutstones))
            {
                layerrestrictions[sl] = (bdx - densitywithoutstones)/(bdx-bdo);
            }
            else
            {
                layerrestrictions[sl] = 0.;
            }
        }
        // take water content into account
        double thetaf = 99.;
        if( cbm::flt_greater( sc_.wcmin_sl[sl], wc_.wc_sl[sl]))
        {
            thetaf = 0.;
        }
        else{
            if( cbm::flt_greater_equal( sc_.wcmax_sl[sl], wc_.wc_sl[sl]))
            {
                thetaf = (wc_.wc_sl[sl] - sc_.wcmin_sl[sl])/(sc_.wcmax_sl[sl] - sc_.wcmin_sl[sl]);
            }
            else
            {
                thetaf = 1.;
            }
        }
        layerrestrictions[sl] *= sin( cbm::PI/2. * thetaf);
        if( !cbm::flt_in_range_lu( 0., layerrestrictions[sl], 1.))
        {
            LOGERROR( "layerrestriction soil strength in layer ", sl, " < 0 or > 1: ", layerrestrictions[sl]);
        }
        //~ std::cout << bdo << " bdx " << bdx << " densitywithoutstones " << densitywithoutstones << " thetaf " << thetaf << " layerrestrictions " << layerrestrictions[sl] << std::endl;
    }
    return layerrestrictions;
}


/*!
 * @brief
 * Root growth restricions due to oxygen limitations.
 * 
 */
lvector_t< double >
RootSystemDNDC::determine_root_restrictions_aeration(
                                       unsigned int iterator_potentially_deepest_rooted_layer,
                                       bool notaffected)
{
    lvector_t< double > layerrestrictions( iterator_potentially_deepest_rooted_layer, 99.);
    // for specific species like rice there is no impact
    double sft = 0.;
    if (notaffected)
    {
            sft = 1.;
    }
    
    for( unsigned int sl = 0;  sl < iterator_potentially_deepest_rooted_layer;  ++sl)
    {
        // critical value of water filled pore space
        // sc_.clay_sl[sl] < 1
        double cwfp = 0.4 + 0.4 * sc_.clay_sl[sl];
        double wfps = wc_.wc_sl[sl]/sc_.poro_sl[sl];
        if( cbm::flt_greater_equal(wfps, cwfp))
        {
            layerrestrictions[sl] = sft + (1. - wfps) * (1. - sft)/(1. - cwfp);
        }
        else{
            layerrestrictions[sl] = 1.;
        }
        if( !cbm::flt_in_range_lu( 0., layerrestrictions[sl], 1.))
        {
            LOGERROR( "layerrestriction aeration in layer ", sl, " < 0 or > 1: ", layerrestrictions[sl]);
        }
        //~ std::cout << sl << " cwfp " << cwfp << " wfps " << wfps << " wc " << wc_.wc_sl[sl] << " poro " << sc_.poro_sl[sl] << " " << layerrestrictions[sl] << std::endl;
    }
    return layerrestrictions;
}


/*!
 * @brief
 * The upper and lower temperature limits for growth are not necessarily identical to
 * the GDD_BASE_TEMPERATURE and GDD_MAX_TEMPERATURE; This seems a first reasonable approach.
 * We also assume, that the optimum temperatuer is centered in the middle of the two.
 * 
 */
lvector_t< double >
RootSystemDNDC::determine_root_restrictions_temperature(
                                       unsigned int iterator_potentially_deepest_rooted_layer,
                                       MoBiLE_Plant * _vt)
{
    lvector_t< double > layerrestrictions( iterator_potentially_deepest_rooted_layer, 99.);
    for( unsigned int sl = 0;  sl < iterator_potentially_deepest_rooted_layer;  ++sl)
    {
        if( cbm::flt_greater( (*_vt)->GDD_BASE_TEMPERATURE(), mc_.temp_sl[sl]))
        {
            layerrestrictions[sl] = 0.;
        }
        else{
            if( cbm::flt_greater( (*_vt)->GDD_MAX_TEMPERATURE(), mc_.temp_sl[sl]))
            {
                double Topt = (*_vt)->GDD_BASE_TEMPERATURE() + ((*_vt)->GDD_MAX_TEMPERATURE() - (*_vt)->GDD_BASE_TEMPERATURE())/2.;
                layerrestrictions[sl] = sin(cbm::PI/2. * (mc_.temp_sl[sl] - (*_vt)->GDD_BASE_TEMPERATURE())/(Topt - (*_vt)->GDD_BASE_TEMPERATURE()));
            }
            else
            {
                layerrestrictions[sl] = 0.;
            }
        }
        if( !cbm::flt_in_range_lu( 0., layerrestrictions[sl], 1.))
        {
            LOGERROR( "layerrestriction temperature in layer ", sl, " < 0 or > 1: ", layerrestrictions[sl]);
        }
        //~ std::cout << (*_vt)->GDD_BASE_TEMPERATURE() << " T " <<  mc_.temp_sl[sl] << " Tmax " <<  (*_vt)->GDD_MAX_TEMPERATURE() << " " << layerrestrictions[sl] << std::endl;
    }
    return layerrestrictions;
}


} /*namespace ldndc*/
