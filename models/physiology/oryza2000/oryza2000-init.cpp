/*!
 * @brief
 *
 * @author
 *    david kraus,
 *    steffen klatt (created on: oct 27, 2016)
 */



#include  "physiology/oryza2000/oryza2000.h"
#include  "physiology/ld_rootsystem.h"

#include  "kernels/oryza2000/ld_oryza2000.h"

#include  <constants/cbm_const.h>
#include  <logging/cbm_logging.h>



LMOD_MODULE_INFO(PhysiologyOryza2000,TMODE_SUBDAILY,LMOD_FLAG_USER);

REGISTER_OPTION(PhysiologyOryza2000, loggerstream, "Stream for sending Oryza2000 logging data");
REGISTER_OPTION(PhysiologyOryza2000, npot,"  [bool]");
REGISTER_OPTION(PhysiologyOryza2000, wpot,"  [bool]");
REGISTER_OPTION(PhysiologyOryza2000, farquhar,"  [bool]");
REGISTER_OPTION(PhysiologyOryza2000, root_turnover,"  [bool]");

namespace ldndc {

cbm::logger_t *  PhysiologyOryza2000Logger = NULL;

PhysiologyOryza2000::PhysiologyOryza2000( MoBiLE_State *  _state,
                                          cbm::io_kcomm_t *  _io_kcomm,
                                          timemode_e  _timemode)
    : MBE_LegacyModel( _state, _timemode),
      m_state( _state),
      m_iokcomm( _io_kcomm),
      timemode( _timemode),
      m_setup( _io_kcomm->get_input_class< input_class_setup_t >()),
      m_soillayers( _io_kcomm->get_input_class< input_class_soillayers_t >()),
      m_climate( _io_kcomm->get_input_class< input_class_climate_t >()),
      mc( _state->get_substate< substate_microclimate_t >()),
      ac( _state->get_substate< substate_airchemistry_t >()),
      wc( _state->get_substate< substate_watercycle_t >()),
      ph( _state->get_substate< substate_physiology_t >()),
      sc( _state->get_substate< substate_soilchemistry_t >()),
      m_veg( &_state->vegetation),
      root_system(0, NULL)
{
    if ( !PhysiologyOryza2000Logger)
    {
        PhysiologyOryza2000Logger = CBM_RegisteredLoggers.new_logger( "PhysiologyOryza2000");
    }

    this->oryza2000_kernel = LK_Oryza2000::new_instance( this->object_id());

    this->ncrop_potential = false;
    this->wcrop_potential = false;
    this->farquhar = false;
    this->root_turnover = true;
}



PhysiologyOryza2000::~PhysiologyOryza2000()
{
    if ( this->oryza2000_kernel){ this->oryza2000_kernel->delete_instance(); }

    //get rid of root systems
    for ( size_t  r = 0;  r < root_system.size();  ++r)
    {
        if ( root_system[r])
        {
            LD_Allocator->destroy( root_system[r]);
        }
    }
}



lerr_t
PhysiologyOryza2000::configure(
                               ldndc::config_file_t const *  _cf)
{
    if ( !this->oryza2000_kernel){ return  LDNDC_ERR_FAIL; }

    cbm::RunLevelArgs  ra_config( cbm::RL_CONFIGURE);
    ra_config.iokcomm = this->m_iokcomm;
    ra_config.cfg = _cf;
    ra_config.clk = this->lclock();
    lerr_t  rc_conf = this->oryza2000_kernel->configure( &ra_config);

    std::string  loggerstream = get_option< char const * >( "loggerstream", "null");
    loggerstream = cbm::format_expand( loggerstream);
    PhysiologyOryza2000Logger->initialize( loggerstream.c_str(), _cf->log_level());

    this->ncrop_potential = get_option< bool >( "npot", false);
    KLOGINFO_TO( PhysiologyOryza2000Logger, "consider potential crop-N: ",
                 cbm::n2s( this->ncrop_potential));

    this->wcrop_potential = get_option< bool >( "wpot", true);
    KLOGINFO_TO( PhysiologyOryza2000Logger, "consider no water related stress for crop growth: ",
                 cbm::n2s( this->wcrop_potential));

    this->farquhar = get_option< bool >( "farquhar", false);
    KLOGINFO_TO( PhysiologyOryza2000Logger, "consider farquhar photosynthesis for crop growth: ",
                 cbm::n2s( this->farquhar));

    this->root_turnover = get_option< bool >( "root_turnover", true);
    KLOGINFO_TO( PhysiologyOryza2000Logger, "considerfine root turnover: ",
                cbm::n2s( this->root_turnover));

    return  rc_conf;
}



lerr_t
PhysiologyOryza2000::initialize()
{
    /*******************************/
    /* initialize oryza2000 module */
    /*******************************/

    this->accumulated_potentialtranspiration_old = wc->accumulated_potentialtranspiration;


    /*******************************/
    /* initialize oryza2000 kernel */
    /*******************************/

    if ( !this->oryza2000_kernel){ return  LDNDC_ERR_FAIL; }

    cbm::RunLevelArgs  ra_init( cbm::RL_INITIALIZE);
    ra_init.iokcomm = this->m_iokcomm;
    ra_init.clk = this->lclock();
    lerr_t  rc_init = this->oryza2000_kernel->initialize( &ra_init);
    if ( rc_init)
    {
        KLOGERROR("Initialization of kernel: ", oryza2000_kernel->name(), " failed!");
        return LDNDC_ERR_FAIL;
    }

    //re-initialize depending on module option
    this->oryza2000_kernel->ncrop_potential = this->ncrop_potential;
    this->oryza2000_kernel->wcrop_potential = this->wcrop_potential;
    this->oryza2000_kernel->farquhar = this->farquhar;
    this->oryza2000_kernel->root_turnover = this->root_turnover;

    //others
    oryza2000_kernel->timemode = timemode;

    return rc_init;
}



lerr_t
PhysiologyOryza2000::finalize()
{
    if ( !this->oryza2000_kernel){ return  LDNDC_ERR_OK; }

    cbm::RunLevelArgs  ra_fin( cbm::RL_FINALIZE);
    ra_fin.iokcomm = this->m_iokcomm;
    ra_fin.clk = this->lclock();
    lerr_t  rc_fin = this->oryza2000_kernel->finalize( &ra_fin);

    return rc_fin;
}



static int  _QueueEventHarvest( void const *  _msg, size_t  _msg_sz, void *  _queue)
{
    ldndc::EventQueue *  queue = static_cast< ldndc::EventQueue * >( _queue);
    if ( !queue)
    { return -1; }

    ldndc::EventAttributes  harvest_event("harvest", (char const *)_msg, _msg_sz);
    queue->push( harvest_event);

    return 0;
}



static int  _QueueEventPlant( void const *  _msg, size_t  _msg_sz, void *  _queue)
{
    ldndc::EventQueue *  queue = static_cast< ldndc::EventQueue * >( _queue);
    if ( !queue)
    { return -1; }

    ldndc::EventAttributes  plant_event("plant", (char const *)_msg, _msg_sz);
    queue->push( plant_event);
    
    return 0;
}



lerr_t
PhysiologyOryza2000::register_ports( cbm::io_kcomm_t *  _io_kcomm)
{
    cbm::RunLevelArgs  ra_register( cbm::RL_REGISTERPORTS);
    ra_register.iokcomm = this->m_iokcomm;
    ra_register.clk = this->lclock();
    lerr_t  rc_register = this->oryza2000_kernel->register_ports( &ra_register);

    CBM_Callback  cb_harvest;
    cb_harvest.fn = &_QueueEventHarvest;
    cb_harvest.data = &this->m_HarvestEvents;
    this->m_HarvestHandle = _io_kcomm->subscribe_event( "harvest", cb_harvest);
    if ( !CBM_HandleOk(this->m_HarvestHandle))
    { return  LDNDC_ERR_FAIL; }

    CBM_Callback  cb_plant;
    cb_plant.fn = &_QueueEventPlant;
    cb_plant.data = &this->m_PlantEvents;
    this->m_PlantHandle = _io_kcomm->subscribe_event( "plant", cb_plant);
    if ( !CBM_HandleOk(this->m_PlantHandle))
    { return  LDNDC_ERR_FAIL; }

    this->hdl_ini = _io_kcomm->subscribe_port("Oryza2000.Initialization", CBM_NoUnit, 0, NULL, 0);
    if ( !CBM_HandleOk(this->hdl_ini))
    { return LDNDC_ERR_FAIL; }

    return rc_register;
}



lerr_t
PhysiologyOryza2000::unregister_ports( cbm::io_kcomm_t *  _io_kcomm)
{
    cbm::RunLevelArgs  ra_unregister( cbm::RL_UNREGISTERPORTS);
    ra_unregister.iokcomm = this->m_iokcomm;
    ra_unregister.clk = this->lclock();
    lerr_t  rc_unregister = this->oryza2000_kernel->unregister_ports( &ra_unregister);

    _io_kcomm->unsubscribe_event( this->m_HarvestHandle);
    _io_kcomm->unsubscribe_event( this->m_PlantHandle);

    _io_kcomm->unsubscribe_port( this->hdl_ini);

    return  rc_unregister;
}

} /*namespace ldndc*/
