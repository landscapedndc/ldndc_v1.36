/*!
 * @brief
 *
 * @author
 *    David Kraus,
 *    Steffen Klatt (created on: oct 27, 2016)
 */


#include  "physiology/oryza2000/oryza2000.h"
#include  "physiology/ld_rootsystem.h"

#include  "kernels/oryza2000/ld_oryza2000.h"

#include  <constants/cbm_const.h>
#include  <logging/cbm_logging.h>



lerr_t
ldndc::PhysiologyOryza2000::Oryza2000PhysiologyPlant(Oryza2000State * _os,
        cbm::RunLevelArgs * _r_level)
{
    while ( this->m_PlantEvents)
    {
        EventAttributes  ev_plant = this->m_PlantEvents.pop();

        MoBiLE_Plant *  p = this->m_veg->get_plant( ev_plant.get( "/name", "?"));
        if ( !this->m_veg->is_family( p, ":rice:"))
        {
            continue;
        }

        /* call oryza2000 kernel planting event */
        this->oryza2000_kernel->OryzaPlant( ev_plant.get( "/seedbed-duration", 0),
                                            ev_plant.get( "/seedling-number", 0.0),
                                            ev_plant.get( "/name", "?"));

        /* initialize root system */
        if ( this->root_system.size() <= p->slot)
        {
            this->root_system.resize(p->slot+1);
        }

        ldndc_kassert( this->root_system[p->slot] == NULL);
        root_system[p->slot] = LD_Allocator->construct_args< RootSystemDNDC >( 1, this->m_state, this->m_iokcomm);
        if ( !root_system[p->slot])
        {
            KLOGERROR( "Failed to allocate root-system object");
            return  LDNDC_ERR_NOMEM;
        }

        unsigned int sl_fineroots( 1);
        p->mFrt = cbm::bound_min(1.0e-9, p->mFrt);
        root_system[p->slot]->set_rooting_depth( sl_fineroots, p, _os->zrti);
        root_system[p->slot]->update_root_fraction_exponential( 1.0,
                                                                sl_fineroots,
                                                                (*p)->EXP_ROOT_DISTRIBUTION(), p);

        p->height_max = 0.01;

        /* Farquhar Photosynthesis parameter */
        _os->m_photo.CSR_REF = (*p)->CSR_REF();
        _os->m_photo.PSI_REF = (*p)->PSI_REF();
        _os->m_photo.PSI_EXP = (*p)->PSI_EXP();
        _os->m_photo.RPMIN = (*p)->RPMIN();
        
        _os->m_photo.KC25 = (*p)->KC25();
        _os->m_photo.AEKC = (*p)->AEKC();
        _os->m_photo.AEVO = (*p)->AEVO();
        _os->m_photo.KO25 = (*p)->KO25();
        _os->m_photo.AEKO = (*p)->AEKO();
        _os->m_photo.AEVC = (*p)->AEVC();
        _os->m_photo.QVOVC = (*p)->QVOVC();
        _os->m_photo.GSMIN = (*p)->GSMIN();
        _os->m_photo.GSMAX = (*p)->GSMAX();
        _os->m_photo.SLOPE_GSA = (*p)->SLOPE_GSA();
        _os->m_photo.AERD = (*p)->AERD();
        _os->m_photo.SDJ = (*p)->SDJ();
        _os->m_photo.HDJ = (*p)->HDJ();
        _os->m_photo.THETA = (*p)->THETA();
        _os->m_photo.AEJM = (*p)->AEJM();
        _os->m_photo.QJVC = (*p)->QJVC();
        _os->m_photo.C4_TYPE = (*p)->C4_TYPE();


        char *  inidata = NULL;
        //TODO  use  this->m_iokcomm->recv_dynport( this->hdl_ini, inidata);
        int  inisize = _r_level->iokcomm->ping_port( this->hdl_ini);
        if ( inisize > 0)
        {
            inidata = (char *)CBM_Allocate( sizeof(char)*(inisize+1));
        }

        size_t  recv_sz = 0;
        if ( inidata)
        {
            recv_sz = _r_level->iokcomm->recv_port(this->hdl_ini, inidata, inisize);
            inidata[inisize]='\0';

            cbm::jquery_t  ji( inidata);

            /* Phenological development parameters */
            _os->tbd = ji.query_double( "/TBD", _os->tbd);
            _os->tblv = ji.query_double( "/TBLV", _os->tblv);
            _os->tmd = ji.query_double( "/TMD", _os->tmd);
            _os->tod = ji.query_double( "/TOD", _os->tod);

            _os->dvrj = ji.query_double( "/DVRJ", _os->dvrj);
            _os->dvri = ji.query_double( "/DVRI", _os->dvri);
            _os->dvrp = ji.query_double( "/DVRP", _os->dvrp);
            _os->dvrr = ji.query_double( "/DVRR", _os->dvrr);

            _os->mopp = ji.query_double( "/MOPP", _os->mopp);
            _os->ppse = ji.query_double( "/PPSE", _os->ppse);
            _os->shckd = ji.query_double( "/SHCKD", _os->shckd);

            _os->rgrlmx = ji.query_double( "/RGRLMX", _os->rgrlmx);
            _os->rgrlmn = ji.query_double( "/RGRLMN", _os->rgrlmn);
            _os->shckl = ji.query_double( "/SHCKL", _os->shckl);

            _os->asla = ji.query_double( "/ASLA", _os->asla);
            _os->bsla = ji.query_double( "/BSLA", _os->bsla);
            _os->csla = ji.query_double( "/CSLA", _os->csla);
            _os->dsla = ji.query_double( "/DSLA", _os->dsla);
            _os->slamax = ji.query_double( "/SLMAX", _os->slamax);

            CBM_Free( inidata);
        }
        
        if ( recv_sz!=(size_t)inisize)
        { return  LDNDC_ERR_FAIL; }

        if ( !this->m_PlantEvents.is_empty())
        {
            KLOGERROR( "I do not handle multiple species at the same time");
            this->m_PlantEvents.clear();
            return  LDNDC_ERR_RUNTIME_ERROR;
        }
    }

    return LDNDC_ERR_OK;
}



lerr_t
ldndc::PhysiologyOryza2000::Oryza2000PhysiologyHarvest()
{
    while ( this->m_HarvestEvents)
    {
        EventAttributes ev_harv = this->m_HarvestEvents.pop();

        MoBiLE_Plant * vt = this->m_veg->get_plant( ev_harv.get( "/name", "?"));
        if ( !vt)
        {
            return LDNDC_ERR_RUNTIME_ERROR;
        }

        double const export_root = ev_harv.get( "/fraction-export-rootwood", 0.0);
        double const rootlitter_c( (1.0 - export_root) * vt->mFrt * cbm::CCDM);
        double const rootlitter_n( (1.0 - export_root) * vt->mFrt * vt->ncFrt);
        double const root_c_export( export_root * vt->mFrt * cbm::CCDM);
        double const root_n_export( export_root * vt->mFrt * vt->ncFrt);

        double const straw_c( (vt->mFol + vt->dw_lst) * cbm::CCDM);
        double const straw_n( (vt->mFol * vt->ncFol) +
                              (vt->dw_lst * vt->nc_lst()));

        double const bud_c( vt->mBud * cbm::CCDM);
        double const bud_n( vt->mBud * vt->ncBud);

        double bud_c_export( 0.0);
        double bud_n_export( 0.0);

        double straw_c_export( 0.0);
        double straw_n_export( 0.0);

        double stubble_c( 0.0);
        double stubble_n( 0.0);

        double remains_relative( ev_harv.get( "/remains_relative", invalid_flt));
        double stubble_height( ev_harv.get( "/height", invalid_flt));

        if (   !cbm::flt_greater_equal_zero( remains_relative)
            && !cbm::flt_greater_equal_zero( stubble_height))
        {
            KLOGWARN( "harvest event attribute \"remains_relative\" and \"height\" were not set; \"remains_relative\" set to zero",
                    "  [species=", vt->name(),"]");
            remains_relative = 0.0;
        }
        else  if (   cbm::flt_greater_equal_zero( remains_relative)
                  && cbm::flt_greater_equal_zero( stubble_height))
        {
            KLOGWARN( "harvest event attribute \"remains_relative\" and \"height\" were both set; attribute \"remains_relative\" used",
                    "  [species=", vt->name(),"]");
        }

        bud_c_export = bud_c;
        bud_n_export = bud_n;

        if ( remains_relative > 0.0)
        {
            stubble_c = remains_relative * straw_c;
            stubble_n = remains_relative * straw_n;

            straw_c_export = (1.0-remains_relative) * straw_c;
            straw_n_export = (1.0-remains_relative) * straw_n;
        }
        else if ( stubble_height > 0.0)
        {
            // 1cm stubble assumed to equal 70 kg C ha-1
            remains_relative = std::min( stubble_height * cbm::CM_IN_M * 0.007, straw_c) / straw_c;
            stubble_c = remains_relative * straw_c;
            stubble_n = remains_relative * straw_n;

            straw_c_export = (1.0-remains_relative) * straw_c;
            straw_n_export = (1.0-remains_relative) * straw_n;
        }
        else
        {
            stubble_c = straw_c + bud_c;
            stubble_n = straw_n + bud_n;
        }

        sc->c_stubble_lit3 += stubble_c * (*vt)->LIGNIN();
        sc->c_stubble_lit2 += stubble_c * (*vt)->CELLULOSE();
        sc->c_stubble_lit1 += stubble_c * (1.0 - (*vt)->LIGNIN() - (*vt)->CELLULOSE());

        sc->n_stubble_lit3 += stubble_n * (*vt)->LIGNIN();
        sc->n_stubble_lit2 += stubble_n * (*vt)->CELLULOSE();
        sc->n_stubble_lit1 += stubble_n * (1.0 - (*vt)->LIGNIN() - (*vt)->CELLULOSE());

        if ( this->timemode == TMODE_SUBDAILY)
        {
            for ( size_t  sl = 0;  sl < m_soillayers->soil_layer_cnt();  ++sl)
            {
                double const frt_vtsl( rootlitter_c / cbm::CCDM * vt->fFrt_sl[sl]);
                vt->sFrt_sl[sl] += frt_vtsl;
                vt->nLitFrt_sl[sl] += (frt_vtsl * vt->ncFrt);
            }
        }
        else
        {
            for ( size_t  sl = 0;  sl < m_soillayers->soil_layer_cnt();  ++sl)
            {
                double const frt_vtsl( rootlitter_c / cbm::CCDM * vt->fFrt_sl[sl]);
                vt->d_sFrt_sl[sl] += frt_vtsl;
                vt->d_nLitFrt_sl[sl] += (frt_vtsl * vt->ncFrt);
            }
        }


        cbm::state_scratch_t *  mcom = m_iokcomm->get_scratch();
        std::string  mcom_key;

        cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:c_fru", vt->cname());
        mcom->set( mcom_key.c_str(), bud_c);

        cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:c_fru_export", vt->cname());
        mcom->set( mcom_key.c_str(), bud_c_export);

        cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:n_fru", vt->cname());
        mcom->set( mcom_key.c_str(), bud_n);

        cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:n_fru_export", vt->cname());
        mcom->set( mcom_key.c_str(), bud_n_export);

        cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:c_straw", vt->cname());
        mcom->set( mcom_key.c_str(), straw_c);

        cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:c_straw_export", vt->cname());
        mcom->set( mcom_key.c_str(), straw_c_export);

        cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:n_straw", vt->cname());
        mcom->set( mcom_key.c_str(), straw_n);

        cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:n_straw_export", vt->cname());
        mcom->set( mcom_key.c_str(), straw_n_export);

        cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:c_stubble", vt->cname());
        mcom->set( mcom_key.c_str(), stubble_c);

        cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:n_stubble", vt->cname());
        mcom->set( mcom_key.c_str(), stubble_n);

        cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:c_frt", vt->cname());
        mcom->set( mcom_key.c_str(), rootlitter_c);

        cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:n_frt", vt->cname());
        mcom->set( mcom_key.c_str(), rootlitter_n);

        ph->accumulated_c_export_harvest += straw_c_export + bud_c_export + root_c_export;
        ph->accumulated_n_export_harvest += straw_n_export + bud_n_export + root_n_export;

        ph->accumulated_c_fru_export_harvest += bud_c_export;
        ph->accumulated_n_fru_export_harvest += bud_n_export;

        vt->mFol = 0.0;
        vt->dw_lst = 0.0;
        vt->mBud = 0.0;
        vt->mBudStart = 0.0;

        vt->dEmerg = 0;
        vt->growing_degree_days = 0.0;

        vt->dvsFlush = 0.0;
        vt->dvsFlushOld = 0.0;

        vt->ncBud = 0.0;
        vt->ncFol = 0.0;
        vt->ncFrt = 0.0;
        vt->ncSap = 0.0;
        vt->n_lst = 0.0;

        vt->a_fix_n = 0.0;

        vt->height_max = 0.0;
        vt->height_at_canopy_start = 0.0;

        vt->f_area = 0.0;
        vt->f_fac = 0.0;

        for ( size_t fl = 0; fl < m_setup->canopylayers(); ++fl)
        {
            vt->lai_fl[fl] = 0.0;
        }

        root_system[vt->slot]->reset( vt);
        LD_Allocator->destroy( root_system[vt->slot]);
        root_system[vt->slot] = NULL;

        this->oryza2000_kernel->OryzaHarvest();
    }

    if ( !this->m_HarvestEvents.is_empty())
    {
        KLOGERROR( "I do not handle multiple species at the same time");
        this->m_HarvestEvents.clear();
        return  LDNDC_ERR_RUNTIME_ERROR;
    }

    return LDNDC_ERR_OK;
}

