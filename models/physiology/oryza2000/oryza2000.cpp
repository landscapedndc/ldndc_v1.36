/*!
 * @brief
 *
 * @author
 *    David Kraus,
 *    Steffen Klatt (created on: oct 27, 2016)
 */


#include  "physiology/oryza2000/oryza2000.h"
#include  "kernels/oryza2000/ld_oryza2000.h"

#include  <constants/cbm_const.h>
#include  <logging/cbm_logging.h>


lerr_t
ldndc::PhysiologyOryza2000::read()
{
    ldndc_assert( oryza2000_kernel);

    cbm::RunLevelArgs  ra_read( cbm::RL_READ);
    ra_read.iokcomm = m_iokcomm;
    ra_read.clk = lclock();

    oryza2000_kernel->read( &ra_read);

    return  LDNDC_ERR_OK;
}


lerr_t
ldndc::PhysiologyOryza2000::solve()
{
    ldndc_assert( oryza2000_kernel);

    cbm::RunLevelArgs  ra_solve( cbm::RL_SOLVE);
    ra_solve.iokcomm = m_iokcomm;
    ra_solve.clk = lclock();

    /* get state of oryza2000 kernel */
    Oryza2000State * os = oryza2000_kernel->get_state();

    /* planting event */
    Oryza2000PhysiologyPlant( os, &ra_solve);

    /* run rate calculation step */
    if ( os->cropsta > 0)
    {
        Oryza2000PhysiologyReceiveState( os);

        lerr_t rc_solve = oryza2000_kernel->solve( &ra_solve);
        if ( rc_solve){ return rc_solve; }

        Oryza2000PhysiologySendState( os);
    }

    /* harvest event */
    Oryza2000PhysiologyHarvest();

    return  LDNDC_ERR_OK;
}



lerr_t
ldndc::PhysiologyOryza2000::integrate()
{
    ldndc_assert( oryza2000_kernel);

    cbm::RunLevelArgs  ra_integrate( cbm::RL_INTEGRATE);
    ra_integrate.iokcomm = m_iokcomm;
    ra_integrate.clk = lclock();

    oryza2000_kernel->integrate( &ra_integrate);

    return LDNDC_ERR_OK;
}



lerr_t
ldndc::PhysiologyOryza2000::write()
{
    ldndc_assert( oryza2000_kernel);

    cbm::RunLevelArgs  ra_write( cbm::RL_WRITE);
    ra_write.iokcomm = m_iokcomm;
    ra_write.clk = lclock();

    oryza2000_kernel->write( &ra_write);
    
    return LDNDC_ERR_OK;
}

