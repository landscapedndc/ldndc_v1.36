/*!
 * @brief
 *
 * @author
 *    david kraus,
 *    steffen klatt (created on: oct 27, 2016)
 */



#include  "physiology/oryza2000/oryza2000.h"
#include  "physiology/ld_rootsystem.h"

#include  "kernels/oryza2000/ld_oryza2000.h"

#include  <constants/cbm_const.h>
#include  <logging/cbm_logging.h>

lerr_t
ldndc::PhysiologyOryza2000::Oryza2000PhysiologyReceiveState(
                                                            Oryza2000State * _os)
{
    //set updated soil profile depth
    _os->tklt = sc->h_sl.sum();
    for( size_t  sl = 0;  sl < m_soillayers->soil_layer_cnt();  ++sl)
    {
        _os->tkl_sl[sl] = sc->h_sl[sl];
    }


    /*****************/
    /* soilchemistry */
    /*****************/

    this->oryza2000_kernel->m_ncrop.soiln = 0.0;
    for( size_t  sl = 0;  sl < m_soillayers->soil_layer_cnt();  ++sl)
    {
        this->oryza2000_kernel->m_ncrop.soiln += sc->no3_sl[sl] * cbm::M2_IN_HA;
        this->oryza2000_kernel->m_ncrop.soiln += sc->an_no3_sl[sl] * cbm::M2_IN_HA;
        this->oryza2000_kernel->m_ncrop.soiln += sc->nh4_sl[sl] * cbm::M2_IN_HA;
    }


    /*************/
    /* hydrology */
    /*************/

    for( size_t  sl = 0;  sl < m_soillayers->soil_layer_cnt();  ++sl)
    {
        _os->wc_sl[sl] = wc->wc_sl[sl];
        _os->poro_sl[sl] = sc->poro_sl[sl];
        _os->wcwp_sl[sl] = sc->wcmin_sl[sl];
        _os->mskpa_sl[sl] = wc->mskpa_sl[sl];
    }

    /**************/
    /* physiology */
    /**************/
    
    for ( CropIterator  vt = this->m_veg->groupbegin< species::crop >();
         vt != this->m_veg->groupend< species::crop >();  ++vt)
    {
        MoBiLE_Plant *p = *vt;

        _os->nb_foliagelayers = p->nb_foliagelayers();

        for (size_t fl = 0; fl < _os->nb_foliagelayers; ++fl)
        {
            if ( ncrop_potential)
            {
                _os->vcAct25_fl[fl] = vt->VCMAX25();
            }
            else
            {
                _os->vcAct25_fl[fl] = vt->VCMAX25() * oryza2000_kernel->m_ncrop.fnlv / oryza2000_kernel->m_ncrop.nmaxl;
            }

            _os->jAct25_fl[fl] = _os->vcAct25_fl[fl] * vt->QJVC();
            _os->rdAct25_fl[fl] = _os->vcAct25_fl[fl] * vt->QRD25();
            _os->sla_fl[fl] = ((p->mFol * p->fFol_fl[fl]) > 0.0) ? _os->lai_fl(fl) / (p->mFol * p->fFol_fl[fl]) : vt->SLAMAX();

            _os->vpd_fl[fl] = mc->vpd_fl[fl];
            _os->rh_fl[fl] = m_climate->rel_humidity_subday( lclock_ref());
            _os->temp_fl[fl] = mc->temp_fl[fl];
            _os->parsun_fl[fl] = mc->parsun_fl[fl];
            _os->parshd_fl[fl] = mc->parshd_fl[fl];
            _os->tFol_fl[fl] = mc->tFol_fl[fl];
            _os->co2_concentration_fl[fl] = ac->ts_co2_concentration_fl[fl];
            _os->sunlitfoliagefraction_fl[fl] = mc->ts_sunlitfoliagefraction_fl[fl];
        }

        _os->airpressure = mc->nd_airpressure;

        _os->f_h2o = p->f_h2o;
        _os->f_fac = p->f_fac;

        _os->CSR_REF = vt->CSR_REF();
        _os->PSI_REF = vt->PSI_REF();
        _os->PSI_EXP = vt->PSI_EXP();
        _os->RPMIN = vt->RPMIN();

        _os->KC25 = vt->KC25();
        _os->AEKC = vt->AEKC();
        _os->AEVO = vt->AEVO();
        _os->KO25 = vt->KO25();
        _os->AEKO = vt->AEKO();
        _os->AEVC = vt->AEVC();
        _os->QVOVC = vt->QVOVC();
        _os->GSMIN = vt->GSMIN();
        _os->GSMAX = vt->GSMAX();
        _os->SLOPE_GSA = vt->SLOPE_GSA();
        _os->AERD = vt->AERD();
        _os->SDJ = vt->SDJ();
        _os->HDJ = vt->HDJ();
        _os->THETA = vt->THETA();
        _os->AEJM = vt->AEJM();
        _os->QJVC = vt->QJVC();
        _os->C4_TYPE = vt->C4_TYPE();

        /* root c release to soil reduced to larger root biomass */
        _os->TOFRTBAS = vt->TOFRTBAS();
        _os->DOC_RESP_RATIO = vt->DOC_RESP_RATIO();
    }

    return  LDNDC_ERR_OK;
}



lerr_t
ldndc::PhysiologyOryza2000::Oryza2000PhysiologySendState(
                                                         Oryza2000State * _os)
{
    if (  (_os->cropsta == 4) &&
          cbm::flt_less_equal( _os->dvs, 2.0))
    {
        /**************/
        /* physiology */
        /**************/

        for ( CropIterator  vt = this->m_veg->groupbegin< species::crop >();
             vt != this->m_veg->groupend< species::crop >();  ++vt)
        {
            MoBiLE_Plant *p = *vt;

            unsigned int sl_fineroots = m_soillayers->soil_layer_cnt();
            for ( size_t  sl = 0;  sl < m_soillayers->soil_layer_cnt();  ++sl)
            {
                if ( cbm::flt_greater( sc->depth_sl[sl], _os->zrt))
                {
                    sl_fineroots = (sl+1);
                    break;
                }
            }

            root_system[p->slot]->set_rooting_depth( sl_fineroots, p, _os->zrt);
            root_system[p->slot]->update_root_fraction_exponential( 1.0,
                                                                   sl_fineroots,
                                                                   (*p)->EXP_ROOT_DISTRIBUTION(), p);

            /* root gas conductivity */
            p->root_tc = p->mFrt * (*p)->RS_CONDUCT();

            for (size_t fl = 0; fl < _os->nb_foliagelayers; ++fl)
            {
                p->sla_fl[fl] = _os->sla_fl[fl];
                p->fFol_fl[fl] = _os->fFol_fl(fl);
                p->lai_fl[fl] = _os->lai_fl(fl);

                p->vcAct25_fl[fl] = _os->vcAct25_fl[fl];
                p->jAct25_fl[fl] = _os->jAct25_fl[fl];
                p->rdAct25_fl[fl] = _os->rdAct25_fl[fl];
            }


            if ( this->timemode == TMODE_SUBDAILY)
            {
                double const scale( 1.0 / 24.0);
                p->rGro = _os->rgcr * cbm::HA_IN_M2 * cbm::C_IN_CO2 * scale;
                p->rRes = _os->rmcr() * cbm::HA_IN_M2 * cbm::C_IN_CO2 * scale;
                p->rGroBelow = _os->rgcr_rt * cbm::HA_IN_M2 * cbm::C_IN_CO2 * scale;

                p->sFol = 0.0;
                p->sBud = 0.0;
                for( size_t  sl = 0;  sl < m_soillayers->soil_layer_cnt();  ++sl)
                {
                    p->sFrt_sl[sl] = _os->c_root_turnover * cbm::HA_IN_M2 / cbm::CCDM * scale * p->fFrt_sl[sl];
                }

                p->nLitFol = 0.0;
                p->nLitBud = 0.0;
                for( size_t  sl = 0;  sl < m_soillayers->soil_layer_cnt();  ++sl)
                {
                    p->nLitFrt_sl[sl] = 0.0;
                }

                p->rFrt = _os->rmcr_rt * cbm::HA_IN_M2 * cbm::C_IN_CO2 * scale;
                p->rFol = _os->rmcr_lv * cbm::HA_IN_M2 * cbm::C_IN_CO2 * scale;
                p->rBud = _os->rmcr_so * cbm::HA_IN_M2 * cbm::C_IN_CO2 * scale;
                p->rSap = _os->rmcr_st * cbm::HA_IN_M2 * cbm::C_IN_CO2 * scale;

                p->exsuLoss = _os->c_root_exsudation * cbm::HA_IN_M2 * scale;

                for (size_t fl = 0; fl < _os->nb_foliagelayers; ++fl)
                {
                    p->carbonuptake_fl[fl] = _os->ts_dtga * cbm::C_IN_CO2 * cbm::HA_IN_M2 * _os->fFol_fl(fl);
                }

//                p->dcFol = m_oryza-> * _os->fclv;
                p->dcSap = _os->gst * _os->fcst * cbm::HA_IN_M2;
                p->dcBud = _os->gso * _os->fcso * cbm::HA_IN_M2;
                p->dcFrt = _os->grt * _os->fcrt * cbm::HA_IN_M2;
            }
            else
            {
                p->d_rGro = _os->rgcr * cbm::HA_IN_M2 * cbm::C_IN_CO2;
                p->d_rRes = _os->rmcr() * cbm::HA_IN_M2 * cbm::C_IN_CO2;
                p->d_rGroBelow = _os->rgcr_rt * cbm::HA_IN_M2 * cbm::C_IN_CO2;

                p->d_sFol = 0.0;
                p->d_sBud = 0.0;
                for( size_t  sl = 0;  sl < m_soillayers->soil_layer_cnt();  ++sl)
                {
                    p->d_sFrt_sl[sl] = _os->c_root_turnover * cbm::HA_IN_M2 / cbm::CCDM * p->fFrt_sl[sl];
                }

                p->d_nLitFol = 0.0;
                p->d_nLitBud = 0.0;

                for( size_t  sl = 0;  sl < m_soillayers->soil_layer_cnt();  ++sl)
                {
                    p->d_nLitFrt_sl[sl] = 0.0;
                }

                p->d_rFrt = _os->rmcr_rt * cbm::HA_IN_M2 * cbm::C_IN_CO2;;
                p->d_rFol = _os->rmcr_lv * cbm::HA_IN_M2 * cbm::C_IN_CO2;
                p->d_rBud = _os->rmcr_so * cbm::HA_IN_M2 * cbm::C_IN_CO2;
                p->d_rSap = _os->rmcr_st * cbm::HA_IN_M2 * cbm::C_IN_CO2;

                p->d_exsuLoss = _os->c_root_exsudation * cbm::HA_IN_M2;

                for (size_t fl = 0; fl < _os->nb_foliagelayers; ++fl)
                {
                    p->d_carbonuptake_fl[fl] = _os->d_dtga * cbm::C_IN_CO2 * cbm::HA_IN_M2 * _os->fFol_fl(fl);
                }

                //                p->dcFol = m_oryza-> * _os->fclv;
                p->d_dcSap = _os->gst * _os->fcst * cbm::HA_IN_M2;
                p->d_dcBud = _os->gso * _os->fcso * cbm::HA_IN_M2;
                p->d_dcFrt = _os->grt * _os->fcrt * cbm::HA_IN_M2;
            }


            p->growing_degree_days = _os->hu;
            p->dvsFlush = _os->dvs;

            p->mFrt = _os->wrt * _os->fcrt / cbm::CCDM * cbm::HA_IN_M2;
            p->mFol = _os->wlv * _os->fclv / cbm::CCDM * cbm::HA_IN_M2;
            p->dw_lst = _os->wst * _os->fcst / cbm::CCDM * cbm::HA_IN_M2;
            p->mBud = _os->wso * _os->fcso / cbm::CCDM * cbm::HA_IN_M2;

            if ( cbm::flt_greater_zero( p->mFrt))
            {
                p->ncFrt = (this->oryza2000_kernel->m_ncrop.anrt / p->mFrt * cbm::HA_IN_M2);
            }
            else{ p->ncFrt = 0.0; }

            if ( cbm::flt_greater_zero( p->mFol))
            {
                p->ncFol = ((this->oryza2000_kernel->m_ncrop.anlv + this->oryza2000_kernel->m_ncrop.anld)
                            / p->mFol * cbm::HA_IN_M2);
            }
            else { p->ncFol = 0.0; }

            if ( cbm::flt_greater_zero( p->dw_lst))
            {
                p->n_lst = (this->oryza2000_kernel->m_ncrop.anst * cbm::HA_IN_M2);
            }
            else{ p->n_lst = 0.0; }

            if ( cbm::flt_greater_zero( p->mBud))
            {
                p->ncBud = (this->oryza2000_kernel->m_ncrop.anso / p->mBud * cbm::HA_IN_M2);
            }
            else{ p->ncBud = 0.0; }


            // update height of canopy layers (m)
            for ( size_t  fl = 0;  fl < p->nb_foliagelayers();  ++fl)
            {
                ph->h_fl[fl] = p->height_max / p->nb_foliagelayers();
            }
            for ( size_t  fl = p->nb_foliagelayers();  fl < 40;  ++fl)
            {
                ph->h_fl[fl] = 0.0;
            }

            (*vt)->f_area = 1.0;
        }

        /*****************/
        /* soilchemistry */
        /*****************/

        double const total_n(  sc->nh4_sl.sum() + sc->no3_sl.sum() + sc->an_no3_sl.sum());
        if ( cbm::flt_greater_zero( total_n))
        {
            double const n_uptake( oryza2000_kernel->m_ncrop.nacr * cbm::HA_IN_M2);
            for( size_t  sl = 0;  sl < m_soillayers->soil_layer_cnt();  ++sl)
            {
                double const frac_nh4( sc->nh4_sl[sl] / total_n);
                double const frac_no3( sc->no3_sl[sl] / total_n);
                double const frac_an_no3( sc->an_no3_sl[sl] / total_n);
                sc->nh4_sl[sl] = cbm::bound_min( 0.0, sc->nh4_sl[sl] - frac_nh4 * n_uptake);
                sc->no3_sl[sl] = cbm::bound_min( 0.0, sc->no3_sl[sl] - frac_no3 * n_uptake);
                sc->an_no3_sl[sl] = cbm::bound_min( 0.0, sc->an_no3_sl[sl] - frac_an_no3 * n_uptake);
            }
        }

        /*************/
        /* hydrology */
        /*************/

        if ( cbm::flt_less_equal( _os->dvs, 2.0))
        {
            for( size_t  sl = 0;  sl < m_soillayers->soil_layer_cnt();  ++sl)
            {
                wc->accumulated_potentialtranspiration += _os->trwl_sl[sl] * cbm::M_IN_MM;
            }
        }
    }

    return  LDNDC_ERR_OK;
}

