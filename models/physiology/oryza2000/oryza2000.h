/*!
 * @file
 *    This is a Oryza2000 wrapper module
 *
 * @author
 *    David Kraus,
 *    Steffen Klatt (created on: oct 27, 2016)
 */

#ifndef  LM_ORYZA2000_H_
#define  LM_ORYZA2000_H_

#include  "mbe_legacymodel.h"
#include  "state/mbe_state.h"
#include  "ld_shared.h"
#include  "ld_eventqueue.h"

#include  <containers/lgrowarray.h>

namespace cbm {
    struct RunLevelArgs; }
namespace ldndc {
    class LK_Oryza2000;
    class Oryza2000State;
}

namespace ldndc {

extern cbm::logger_t *  PhysiologyOryza2000Logger;

struct BaseRootSystemDNDC;
class  LDNDC_API  PhysiologyOryza2000  :  public  MBE_LegacyModel
{
    LMOD_EXPORT_MODULE_INFO(PhysiologyOryza2000,"physiology:oryza2000","Physiology ORYZA2000");
    public:
        PhysiologyOryza2000( MoBiLE_State *,
                        cbm::io_kcomm_t *, timemode_e);

        ~PhysiologyOryza2000();

        lerr_t  configure( ldndc::config_file_t const *);
        lerr_t  register_ports( cbm::io_kcomm_t *);
        lerr_t  initialize();

        lerr_t  read();
        lerr_t  solve();
        lerr_t  integrate();
        lerr_t  write();

        lerr_t  unregister_ports( cbm::io_kcomm_t *);
        lerr_t  finalize();

        lerr_t  sleep()
            { return  LDNDC_ERR_OK; }
        lerr_t  wake()
            { return  LDNDC_ERR_OK; }

    private:
        MoBiLE_State *  m_state;
        cbm::io_kcomm_t *  m_iokcomm;

        timemode_e const timemode;

        /* required input classes */
        input_class_setup_t const *  m_setup;
        input_class_soillayers_t const *  m_soillayers;
        input_class_climate_t const *  m_climate;

        /* required state components */
        substate_microclimate_t *  mc;
        substate_airchemistry_t *  ac;
        substate_watercycle_t *  wc;
        substate_physiology_t *  ph;
        substate_soilchemistry_t *  sc;

        MoBiLE_PlantVegetation *  m_veg;

        EventQueue  m_HarvestEvents;
        CBM_Handle  m_HarvestHandle;
        EventQueue  m_PlantEvents;
        CBM_Handle  m_PlantHandle;

        ldndc::growable_array < BaseRootSystemDNDC*, 1000, 1 >  root_system;

        CBM_Handle  hdl_ini;

        LK_Oryza2000 *  oryza2000_kernel;

        bool ncrop_potential;
        bool wcrop_potential;
        bool farquhar;
        bool root_turnover;

        double accumulated_potentialtranspiration_old;

        lerr_t Oryza2000PhysiologyReceiveState(
                                               Oryza2000State * /* oryza state */);

        lerr_t Oryza2000PhysiologySendState(
                                            Oryza2000State * /* oryza state */);

        lerr_t Oryza2000PhysiologyPlant(Oryza2000State * /* oryza state */,
                                        cbm::RunLevelArgs *  /* run level */);

        lerr_t Oryza2000PhysiologyHarvest();
};
} /*namespace ldndc*/

#endif  /*  !LM_ORYZA2000_H_  */
