/*!
 * @file
 * @authors
 *  - Ruediger Grote
 * 
 * @date
 */

#include  "physiology/psim/psim.h"
#include  "soilchemistry/ld_sapwoodfraction.h"
#include  "physiology/ld_transpiration.h"
#include  "physiology/ld_plantfunctions.h"
#include  "watercycle/ld_droughtstress.h"

#include  <math/cbm_math.h>
#include  <constants/cbm_const.h>

#include  <input/setup/setup.h>
#include  <input/soillayers/soillayers.h>
#include  <input/species/species.h>

#include  <scientific/meteo/ld_meteo.h>
#include  <scientific/hydrology/ld_vangenuchten.h>

#define  set_config_item(__key__,__value__)  this->io_kcomm->get_scratch()->set( __key__, __value__)
#define  get_config_item(__key__,__value__)  this->io_kcomm->get_scratch()->get( __key__, __value__)

#define  PSIM_YEAR_START  (MoBiLE_YearDoYOffset(this->lclock(),this->m_setup->latitude()))

LMOD_MODULE_INFO(PhysiologyGrowthPSIM,TMODE_SUBDAILY,LMOD_FLAG_USER);

namespace ldndc {

cbm::logger_t *  PhysiologyGrowthPSIMLogger = NULL;

const double  PhysiologyGrowthPSIM::C1 = 0.0367;
const double  PhysiologyGrowthPSIM::CCOMNOX = 1.0;
const double  PhysiologyGrowthPSIM::CSATNH3 = 50.0;
const double  PhysiologyGrowthPSIM::CSATNOX = 5.0;
const double  PhysiologyGrowthPSIM::EPOTNOX = 1.0;
const double  PhysiologyGrowthPSIM::FRFRT = 0.5;
const double  PhysiologyGrowthPSIM::FMIN = 0.05;
const double  PhysiologyGrowthPSIM::KMMM = 0.05;
const double  PhysiologyGrowthPSIM::PAMM = 0.17;
const double  PhysiologyGrowthPSIM::PGDD = 0.0075;
const double  PhysiologyGrowthPSIM::PMIN = 0.06;
const double  PhysiologyGrowthPSIM::PNIT = 0.34;
const double  PhysiologyGrowthPSIM::PPHLOE = 0.06;
const double  PhysiologyGrowthPSIM::PREDFRT = 1.72;
const double  PhysiologyGrowthPSIM::PREDSAP = 0.855;
const double  PhysiologyGrowthPSIM::PTW = 0.29;
const double  PhysiologyGrowthPSIM::TAU = 330.0;
const double  PhysiologyGrowthPSIM::TGLIM = 6.0;
const double  PhysiologyGrowthPSIM::TRMAX = 45.0;
const double  PhysiologyGrowthPSIM::TRMIN = -7.0;
const double  PhysiologyGrowthPSIM::TROPT = 20.0;
const double  PhysiologyGrowthPSIM::UPOTNH3 = 10000.0;

REGISTER_OPTION(PhysiologyGrowthPSIM, loggerstream, "Stream for sending PhysiologyGrowthPSIM logging data");
REGISTER_OPTION(PhysiologyGrowthPSIM, timemode,"  [char]");
REGISTER_OPTION(PhysiologyGrowthPSIM, branchfraction,"  [char]");
REGISTER_OPTION(PhysiologyGrowthPSIM, crownlength,"  [char]");
REGISTER_OPTION(PhysiologyGrowthPSIM, competition,"  [char]");
REGISTER_OPTION(PhysiologyGrowthPSIM, foreststructure,"  [bool]");
REGISTER_OPTION(PhysiologyGrowthPSIM, targetbiomass,"  [float]");
REGISTER_OPTION(PhysiologyGrowthPSIM, targettreenumber,"  [int]");
REGISTER_OPTION(PhysiologyGrowthPSIM, transpirationmethod,"  [char]");
REGISTER_OPTION(PhysiologyGrowthPSIM, stomatalconductance,"  [char]");


PhysiologyGrowthPSIM::PhysiologyGrowthPSIM(
                                           MoBiLE_State *  _state,
                                          cbm::io_kcomm_t *  _io,
                                           timemode_e  _timemode)
                                        : MBE_LegacyModel(
                                                               _state,
                                                               _timemode),
                                        m_state( _state),
                                        io_kcomm( _io),
                                        cl_( _io->get_input_class_ref< input_class_climate_t >()),
                                        m_setup( _io->get_input_class< input_class_setup_t >()),
                                        sl_( _io->get_input_class_ref< input_class_soillayers_t >()),
                                        m_species( _io->get_input_class< species::input_class_species_t >()),

                                        ac_( _state->get_substate_ref< substate_airchemistry_t >()),
                                        mc_( _state->get_substate_ref< substate_microclimate_t >()),
                                        ph_( _state->get_substate_ref< substate_physiology_t >()),
                                        sc_( _state->get_substate_ref< substate_soilchemistry_t >()),
                                        wc_( _state->get_substate_ref< substate_watercycle_t >()),

                                        m_veg( &_state->vegetation),

                                        NitrogenFixation_( NULL),

                                        fl_cnt_( 0u),
                                        m_pf( _state, _io),
                                        m_treedyn( _state, _io),
                                        no3_sl( sl_.soil_layer_cnt(), 0.0),
                                        nh4_sl( sl_.soil_layer_cnt(), 0.0),
                                        don_sl( sl_.soil_layer_cnt(), 0.0),
                                        m_eventgraze( _state, _io, _timemode),
                                        m_eventcut( _state, _io, _timemode),
                                        m_photo( 1.0 / LD_RtCfg.clk->time_resolution(), m_setup->canopylayers())
{
    NitrogenFixation_ = new NitrogenFixation(_state, _io);

    accumulated_transpiration_old = wc_.accumulated_transpiration_sl.sum();
    accumulated_potentialtranspiration_old = wc_.accumulated_potentialtranspiration;

    mFol_emergence = 0.0;
    plant_waterdeficit_old = 0.0;

    tot_c = 0.0;
    tot_n = 0.0;

    if ( !PhysiologyGrowthPSIMLogger)
    {
        PhysiologyGrowthPSIMLogger = CBM_RegisteredLoggers.new_logger( "PhysiologyGrowthPSIMLogger");
    }
}


PhysiologyGrowthPSIM::~PhysiologyGrowthPSIM()
{
    delete NitrogenFixation_;
}

lerr_t
PhysiologyGrowthPSIM::configure(
            config_file_t const *_cf)
{
    // logger stream
    std::string  loggerstream = get_option< char const * >( "loggerstream", "stderr");
    loggerstream = cbm::format_expand( loggerstream);
    PhysiologyGrowthPSIMLogger->initialize( loggerstream.c_str(), _cf->log_level());

    /*!
     * @page psim
     * @section psimoptions Model options
     * Available options (default options are marked with bold letters):
     *
     *  - Branch fraction ("branchfraction" = \b diameter / volume) \n
     *    Branch fraction is either estimated from parameterized tree diameter relationship
     *    (option: diameter) or from crown volume (option: volume).
     */
    branchfraction_opt = get_option< char const * >( "branchfraction", "diameter");
    if ( branchfraction_opt != "diameter")
    {
        KLOGINFO_TO( PhysiologyGrowthPSIMLogger,
                     "Branch fraction estimated by crown volume relationship");
    }

    /*!
     * @page psim
     *  - Crown length ("crownlength" = \b diameter / height) \n
     *    Crown length is either estimated from a parameterized relation to tree height
     *    (option: height, see: @ref veglibs_crown-length_parameter) or based on
     *    crown diameter (option: diameter).
     */
    crownlength_opt = get_option< char const * >( "crownlength", "diameter");
    if ( crownlength_opt != "diameter")
    {
        KLOGINFO_TO( PhysiologyGrowthPSIMLogger,
                     "Canopy starting height estimated by relation to tree height");
    }

    /*!
     * @page psim
     *  - Competition ("competition" = \b true / false) \n
     *    The Competition effect for stand density on height/diameter ratios is either neglected
     *    (option: false) or considered (option: true).
     */
    competition_opt = get_option< bool >( "competition", true);
    if ( !competition_opt)
    {
        KLOGINFO_TO( PhysiologyGrowthPSIMLogger, "No competition considered");
    }

    /*!
     * @page psim
     */
    timemode_opt = get_option< char const * >( "timemode", "daily");

    /*!
     * @page psim
     *  - Forest structure ("foreststructure" = \b true / false ) \n
     *    Structural growth either neglected (option: false) or considered (option: true).
     *  If structural growth is not included, seasonal woody biomass growth is reset in order to avoid structural growth.
     *  Removed woody biomass is added to the litter pools (see: @ref PSIM_ResetBiomassFromLastYear)
     */
    forest_structure_opt = get_option< bool >( "foreststructure", true);
    if (! forest_structure_opt)
    {
        KLOGINFO_TO( PhysiologyGrowthPSIMLogger,
                     "No forest structure development considered");
    }

    target_biomass = get_option< float >( "targetbiomass", -1.0);
    if ( cbm::flt_greater_zero( target_biomass))
    {
        if (! forest_structure_opt)
        {
            KLOGINFO_TO( PhysiologyGrowthPSIMLogger, "Targeted biomass requires calculation of forest structure!");
            target_biomass = -1.0;
        }
        else
        {
            KLOGINFO_TO( PhysiologyGrowthPSIMLogger, "Targeted biomass: ", target_biomass*cbm::M2_IN_HA," [kg DW ha-1]");
        }
    }

    target_treenumber = get_option< int >( "targettreenumber", -1);
    if ( target_treenumber > 0)
    {
        if (! forest_structure_opt)
        {
            KLOGINFO_TO( PhysiologyGrowthPSIMLogger, "Targeted treenumber requires calculation of forest structure!");
            target_treenumber = -1;
        }
        else
        {
            KLOGINFO_TO( PhysiologyGrowthPSIMLogger, "Targeted target_treenumber: ", target_treenumber, "[ha-1]");
        }
    }

    /*!
     * @page psim
     *  - Transpiration (transpirationmethod: \b wateruseefficiency / potentialtranspiration) \n
     *    Transpiration is either calculated using fixed relations to photosynthesis (WUECMAX, WUECMIN), 
     *    or the transpiration that is defined by stomatal conductance, depending on vapor pressure deficit
     *    and plant hydraulic conductance (with GSMIN as minimum conductance). 
     */
    transpiration_method = get_option< char const * >( "transpirationmethod", "wateruseefficiency");
    if ( transpiration_method == "wateruseefficiency")
    {
        KLOGINFO_TO( PhysiologyGrowthPSIMLogger,
                     "Transpiration estimated via carbon assimilation and water use efficiency!");
    }
    else
    {
        KLOGINFO_TO( PhysiologyGrowthPSIMLogger,
                     "Transpiration estimated via stomatal conductance and vapour pressure deficit!");
    }

    /*!
     * @page psim
     *  - Stomatal conductance (stomatalconductance: \b leuning_1995 / ballberry_1987 / medlyn_2011(a,b) / eller_2020) \n
     *    Various stomatal conductance routines are available with and without consideration of soil water supply.
     *    While Leuning et al. @cite leuning:1995a with linear reduction of conductance with relative available soil water
     *    is the standard option @cite knauer:2015a, Eller et al. @cite eller:2020a is the only option that considers
     *    plant water potential instead soil water availability.
     */
    cbm::string_t stomaconduc_method = get_option< char const * >( "stomatalconductance", "leuning_1995_b");
    KLOGINFO_TO( PhysiologyGrowthPSIMLogger, "Stomatal conductance method used: ", stomaconduc_method);
    
    if ( stomaconduc_method == "ballberry_1987")
    {
        m_photo.stomatalconductance_method = ballberry_1987;
    }
    else if ( stomaconduc_method == "leuning_1990")
    {
        m_photo.stomatalconductance_method = leuning_1990;
    }
    else if ( stomaconduc_method == "leuning_1995_a")
    {
        m_photo.stomatalconductance_method = leuning_1995_a;
    }
    else if ( stomaconduc_method == "leuning_1995_b")
    {
        m_photo.stomatalconductance_method = leuning_1995_b;
    }
    else if ( stomaconduc_method == "medlyn_2011_a")
    {
        m_photo.stomatalconductance_method = medlyn_2011_a;
    }
    else if ( stomaconduc_method == "medlyn_2011_b")
    {
        m_photo.stomatalconductance_method = medlyn_2011_b;
    }
    else if ( stomaconduc_method == "eller_2020")
    {
        m_photo.stomatalconductance_method = eller_2020;
    }
    else
    {
        m_photo.stomatalconductance_method = leuning_1995_b;
    }

    /* constant used to reduce fluxes when switching from daily to subdaily time steps */
    if ( subdaily_timemode())
    {
        SIM_DAY_FRACTION = LD_RtCfg.clk->day_fraction();
    }
    else
    {
        SIM_DAY_FRACTION = 1.0;
    }

    return  LDNDC_ERR_OK;
}

lerr_t
PhysiologyGrowthPSIM::initialize()
{
    cNOx_fl.resize_and_preserve( m_setup->canopylayers(), 0.0);   // nitrous oxide concentration per canopy layer (ppm)

    return  LDNDC_ERR_OK;
}

lerr_t
PhysiologyGrowthPSIM::sleep()
{
    return  LDNDC_ERR_OK;
}

lerr_t
PhysiologyGrowthPSIM::wake()
{
    return  LDNDC_ERR_OK;
}


bool
PhysiologyGrowthPSIM::subdaily_timemode()
{
    return (timemode_opt != "daily");
}


lerr_t
PhysiologyGrowthPSIM::PSIM_ResizeVegetationVectors()
{
    // compare number of vegetation types
    size_t const slot_cnt( m_veg->slot_cnt());
    size_t const slot_cnt_old( additional_season.size());
    
    if ( slot_cnt <= slot_cnt_old)
    {
        return LDNDC_ERR_OK;
    }

    matrix_2d_dbl_t::extent_type const vtsl[] =
    { static_cast< matrix_2d_dbl_t::extent_type >( slot_cnt),
        static_cast< matrix_2d_dbl_t::extent_type >( sl_.soil_layer_cnt())};

    additional_season.resize_and_preserve( slot_cnt, false);
    phenological_year.resize_and_preserve( slot_cnt, 0);
    foliage_age_of_start_senescence.resize_and_preserve( slot_cnt, 0);
    days_since_emergence.resize_and_preserve( slot_cnt, 0);
    volume_old_vt.resize_and_preserve( slot_cnt, 0.0);

    // average (effective) temperatures of all leaves of a vegetation type
    ts_leaftemp_vt.resize_and_preserve( slot_cnt, 0.0);
    nd_leaftemp_vt.resize_and_preserve( slot_cnt, 0.0);

    uptNH4Max_vt.resize_and_preserve( slot_cnt, 0.0);    // max. ammonium uptake
    uptNO3Max_vt.resize_and_preserve( slot_cnt, 0.0);    // max. nitrate uptake
    uptDONMax_vt.resize_and_preserve( slot_cnt, 0.0);    // max. dissolved organic nitrogen uptake
    uptNWetMax_vt.resize_and_preserve( slot_cnt, 0.0);   // tot. nitrogen uptake
    uptNWet_vt.resize_and_preserve( slot_cnt, 0.0);      // tot. nitrogen uptake
    uptNTot_vt.resize_and_preserve( slot_cnt, 0.0);      // tot. nitrogen uptake

    rFolOld_vt.resize_and_preserve( slot_cnt, 0.0);
    rBudOld_vt.resize_and_preserve( slot_cnt, 0.0);
    rSapOld_vt.resize_and_preserve( slot_cnt, 0.0);
    rFrtOld_vt.resize_and_preserve( slot_cnt, 0.0);
    rTraOld_vt.resize_and_preserve( slot_cnt, 0.0);
    exsuLossOld_vt.resize_and_preserve( slot_cnt, 0.0);

    uptNH4_vt.resize_and_preserve( slot_cnt, 0.0);
    uptNO3_vt.resize_and_preserve( slot_cnt, 0.0);
    uptNH3_vt.resize_and_preserve( slot_cnt, 0.0);
    uptDON_vt.resize_and_preserve( slot_cnt, 0.0);

    uptNH4Old_vt.resize_and_preserve( slot_cnt, 0.0);
    uptNO3Old_vt.resize_and_preserve( slot_cnt, 0.0);
    uptNH3Old_vt.resize_and_preserve( slot_cnt, 0.0);
    uptDONOld_vt.resize_and_preserve( slot_cnt, 0.0);

    uptN2Old_vt.resize_and_preserve( slot_cnt, 0.0);
    uptNOxOld_vt.resize_and_preserve( slot_cnt, 0.0);

    uptNH4Max_vtsl.resize_and_preserve( vtsl, 0.0);    // max. ammonium uptake
    uptNO3Max_vtsl.resize_and_preserve( vtsl, 0.0);    // max. nitrate uptake
    uptDONMax_vtsl.resize_and_preserve( vtsl, 0.0);    // max. dissolved organic nitrogen
    
    while ( slot_cnt > foliage_age_na.size())
    {
        std::vector< int > help_sl( MoBiLE_MaximumAgeClasses, 0);
        foliage_age_na.push_back( help_sl);
    }

    carbonuptake_vt.resize_and_preserve( slot_cnt, 0.0);
    xylem_resistance_old_vt.resize_and_preserve(slot_cnt, 0.0);
    psidecline_cum_vt.resize_and_preserve(slot_cnt, 0.0);
    psidecline_daycum_vt.resize_and_preserve(slot_cnt, 0.0);
    rehydrationindex_cum_vt.resize_and_preserve(slot_cnt, 0.0);

    //do not initialize to zero!
    mSapOpt_vt.resize_and_preserve( slot_cnt, 0.0);
    mSapOld_vt.resize_and_preserve( slot_cnt, 0.0);
    mCorOld_vt.resize_and_preserve( slot_cnt, 0.0);
    qsaparea_vt.resize_and_preserve(slot_cnt, 0.0);

    mBudLoss_vt.resize_and_preserve( slot_cnt, 0.0);    // dry matter transfered from buds to foliage
    mSapLoss_vt.resize_and_preserve( slot_cnt, 0.0);    // dry matter transfered from sapwood to corewood
    ncFolOpt.resize_and_preserve( slot_cnt, 0.0);       // optimum nitrogen concentration of the whole canopy of one species (gN gDW-1)
    ncBudOpt.resize_and_preserve( slot_cnt, 0.0);       // optimum nitrogen concentration of the whole canopy of one species (gN gDW-1)
    nDem_vt.resize_and_preserve( slot_cnt, 0.0);        // species specific nitrogen demand (kgN)
    nFol_vt.resize_and_preserve( slot_cnt, 0.0);        // last days foliage nitrogen content (kg)
    nFrt_vt.resize_and_preserve( slot_cnt, 0.0);        // last days fine root nitrogen content (kg)
    nSap_vt.resize_and_preserve( slot_cnt, 0.0);        // last days sapwood nitrogen content (kg)
    nCor_vt.resize_and_preserve( slot_cnt, 0.0);        // last days corwood nitrogen content (kg)
    nBud_vt.resize_and_preserve( slot_cnt, 0.0);        // last days bud nitrogen content (kg)
    
    n_bud_to_fol_vt.resize_and_preserve( vtsl, 0.0);
    n_sap_to_cor_vt.resize_and_preserve( vtsl, 0.0);
    n_bud_to_cor_vt.resize_and_preserve( vtsl, 0.0);
    n_fol_to_cor_vt.resize_and_preserve( vtsl, 0.0);
    n_frt_to_cor_vt.resize_and_preserve( vtsl, 0.0);
    n_sap_to_litter_vt.resize_and_preserve( vtsl, 0.0);
    n_sap_to_redistribute_vt.resize_and_preserve( vtsl, 0.0);
    n_bud_to_redistribute_vt.resize_and_preserve( vtsl, 0.0);
    n_fol_to_redistribute_vt.resize_and_preserve( vtsl, 0.0);
    n_frt_to_redistribute_vt.resize_and_preserve( vtsl, 0.0);
    n_sap_gain_vt.resize_and_preserve( vtsl, 0.0);
    n_bud_gain_vt.resize_and_preserve( vtsl, 0.0);
    n_fol_gain_vt.resize_and_preserve( vtsl, 0.0);
    n_frt_gain_vt.resize_and_preserve( vtsl, 0.0);
    
    return  LDNDC_ERR_OK;
}


lerr_t
PhysiologyGrowthPSIM::PSIM_PlantingEvent()
{
    EventAttributes const *  ev_plant = NULL;
    while ( (ev_plant = this->m_PlantEvents.pop()) != NULL)
    {
        MoBiLE_Plant *vt = this->m_veg->get_plant( ev_plant->get( "/name", "?"));
        if ( !vt)
        {
            KLOGERROR( "Handling plant event failed  [species=", ev_plant->get( "/name", "?"),"]");
            return LDNDC_ERR_RUNTIME_ERROR;
        }
        CBM_LogDebug( "Seed plant '",vt->name(),"'");

        species_t const *  sp = NULL;
        if ( m_species)
        {
            sp = m_species->get_species( vt->cname());
        }

        root_system.insert( { vt->slot, RootSystemDNDC( m_state, io_kcomm) } );

        char const *  group = ev_plant->get( "/group", "?");
        if ( cbm::is_equal( group, "wood"))
        {
            site::input_class_site_t const *s_site( io_kcomm->get_input_class< site::input_class_site_t >());
            lerr_t rc_plant = m_pf.initialize_tree( vt, sp->wood(), s_site->soil_use_history(),
                                                    branchfraction_opt, crownlength_opt, competition_opt);
            if ( rc_plant)
            {
                KLOGERROR( "Plant initialization failed  [species=", vt->name(),"]");
                return  LDNDC_ERR_FAIL;
            }
        }
        else if ( cbm::is_equal( group, "grass")) // TODO could be done at "push-time"
        {
            lerr_t rc_plant = m_pf.initialize_grass( vt, sp->grass());
            if ( rc_plant)
            {
                KLOGERROR( "Plant initialization failed  [species=", vt->name(),"]");
                return  LDNDC_ERR_FAIL;
            }
        }
        else if ( cbm::is_equal( group, "crop")) // TODO could be done at "push-time"
        {
            lerr_t rc_plant = m_pf.initialize_crop( vt, sp->crop());
            if ( rc_plant)
            {
                KLOGERROR( "Plant initialization failed  [species=", vt->name(),"]");
                return  LDNDC_ERR_FAIL;
            }
        }

        // initialize local state variables after planting
        if ( vt->dEmerg > 0)
        {
            if ( (int)lclock()->yearday() > vt->dEmerg)
            {
                days_since_emergence[vt->slot] = lclock()->yearday() - vt->dEmerg;
            }
            else
            {
                // use 366 on purpose to ensure always: days_since_emergence > 0
                days_since_emergence[vt->slot] = lclock()->yearday() + (366 - vt->dEmerg);
            }
        }
        else
        {
            days_since_emergence[vt->slot] = 0;
        }

        if ( lclock()->yearday() >= PSIM_YEAR_START)
        {
            phenological_year[vt->slot] = lclock()->year();
        }
        else
        {
            phenological_year[vt->slot] = lclock()->year() - 1;
        }
            
        foliage_age_of_start_senescence[vt->slot] = m_pf.get_foliage_age_of_senescence( vt, 0.0);
        
        for ( size_t  na = 0;  na < vt->nb_ageclasses();  ++na)
        {
            foliage_age_na[vt->slot][na] = m_pf.get_foliage_age( PSIM_YEAR_START, lclock()->yearday(), na);
        }
    }
    
    return LDNDC_ERR_OK;
}


lerr_t
PhysiologyGrowthPSIM::PSIM_StepInit()
{
    for ( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
    {
        nh4_sl[sl] = sc_.nh4_sl[sl];
        no3_sl[sl] = sc_.no3_sl[sl] + sc_.an_no3_sl[sl];
        don_sl[sl] = sc_.don_sl[sl];
    }

    for ( PlantIterator vt = this->m_veg->begin(); vt != this->m_veg->end(); ++vt)
    {
        MoBiLE_Plant *p = *vt;
        uptNH4_vt[p->slot] = 0.0;
        uptNO3_vt[p->slot] = 0.0;
        uptNH3_vt[p->slot] = 0.0;
        uptDON_vt[p->slot] = 0.0;
    }

    fl_cnt_ = m_veg->canopy_layers_used();

    mBudLoss_vt = 0.0;
    mSapLoss_vt = 0.0;
    ncFolOpt = 0.0;
    ncBudOpt = 0.0;
    nDem_vt = 0.0;
    nFol_vt = 0.0;
    nFrt_vt = 0.0;
    nSap_vt = 0.0;
    nCor_vt = 0.0;
    nBud_vt = 0.0;
    cNOx_fl = 0.0;

    n_bud_to_fol_vt = 0.0;
    n_sap_to_cor_vt = 0.0;
    n_bud_to_cor_vt = 0.0;
    n_fol_to_cor_vt = 0.0;
    n_frt_to_cor_vt = 0.0;
    n_sap_to_litter_vt = 0.0;
    n_sap_to_redistribute_vt = 0.0;

    n_bud_to_redistribute_vt = 0.0;
    n_fol_to_redistribute_vt = 0.0;
    n_frt_to_redistribute_vt = 0.0;

    n_sap_gain_vt = 0.0;
    n_bud_gain_vt = 0.0;
    n_fol_gain_vt = 0.0;
    n_frt_gain_vt = 0.0;

    if ( subdaily_timemode())
    {
        ts_temp_fl.reference( mc_.temp_fl);
        no_concentration_fl.reference( ac_.ts_no_concentration_fl);
        no2_concentration_fl.reference( ac_.ts_no2_concentration_fl);
        nh3_concentration_fl.reference( ac_.ts_nh3_concentration_fl);
        nh3_uptake_fl.reference( ph_.ts_nh3_uptake_fl);
        nox_uptake_fl.reference( ph_.ts_nox_uptake_fl);

        temp_sl.reference( mc_.temp_sl);

        for ( PlantIterator vt = this->m_veg->begin(); vt != this->m_veg->end(); ++vt)
        {
            MoBiLE_Plant *p = *vt;
            carbonuptake_vt[p->slot] = 0.0;
            for ( size_t  fl = 0;  fl < p->nb_foliagelayers();  ++fl)
            {
                carbonuptake_vt[p->slot] += p->carbonuptake_fl[fl];
            }
        }

        // grazing event by external module
        if ( lclock()->subday() == 1)
        {
            m_eventgraze.reset_daily_food_consumption();
        }
    }
    else
    {
        ts_temp_fl.reference( mc_.nd_temp_fl);  // fw: both the same ...
        no_concentration_fl.reference( ac_.nd_no_concentration_fl);
        no2_concentration_fl.reference( ac_.nd_no2_concentration_fl);
        nh3_concentration_fl.reference( ac_.nd_nh3_concentration_fl);
        nh3_uptake_fl.reference( ph_.nd_nh3_uptake_fl);
        nox_uptake_fl.reference( ph_.nd_nox_uptake_fl);

        temp_sl.reference( mc_.nd_temp_sl);

        for ( PlantIterator vt = this->m_veg->begin(); vt != this->m_veg->end(); ++vt)
        {
            MoBiLE_Plant *p = *vt;
            carbonuptake_vt[p->slot] = 0.0;
            for ( size_t  fl = 0;  fl < p->nb_foliagelayers();  ++fl)
            {
                carbonuptake_vt[p->slot] += p->d_carbonuptake_fl[fl];
            }
        }

        m_eventgraze.reset_daily_food_consumption();
    }

    for ( PlantIterator vt = this->m_veg->begin(); vt != this->m_veg->end(); ++vt)
    {
        MoBiLE_Plant *p = *vt;
        if ( cbm::flt_equal_zero( volume_old_vt[p->slot]))
        {
            volume_old_vt[p->slot] = p->stand_volume();
        }

        // leaf temperature
        ts_leaftemp_vt[p->slot] = m_pf.leaf_temperature_( ts_temp_fl, p);
        nd_leaftemp_vt[p->slot] = m_pf.leaf_temperature_( mc_.nd_temp_fl, p);
    }

    // nitrous oxygen concentration in the air
    for ( size_t  fl = 0;  fl < fl_cnt_;  ++fl)
    {
        cNOx_fl[fl] = no_concentration_fl[fl] + no2_concentration_fl[fl];
    }

    return  LDNDC_ERR_OK;
}


lerr_t
PhysiologyGrowthPSIM::PSIM_ResetBiomassFromLastYear()
{
    if ( (lclock()->yearday() == PSIM_YEAR_START) &&
        lclock()->is_position( TMODE_POST_DAILY))
    {
        bool reduce_biomass( forest_structure_opt ? false : true);

        if( (target_biomass > 0.0) || (target_treenumber > 0))
        {
            double bio_now( 0.0);
            int n_now( 0.0);
            for ( TreeIterator vt = m_veg->groupbegin< species::wood >();
                  vt != m_veg->groupend< species::wood >();  ++vt)
            {
                MoBiLE_Plant* p = (*vt);
                bio_now += p->aboveground_biomass();
                n_now += p->tree_number;
            }

            if ( cbm::flt_greater( bio_now, target_biomass))
            {
                reduce_biomass = true;
            }

            if ( n_now < target_treenumber)
            {
                double const treenumber_new( cbm::bound( 0.1,
                                                         bio_now / target_biomass,
                                                         1.0) * target_treenumber);

                for ( TreeIterator vt = m_veg->groupbegin< species::wood >();
                      vt != m_veg->groupend< species::wood >();  ++vt)
                {
                    MoBiLE_Plant *p = (*vt);
                    
                    double const hd_competition_factor_old( !competition_opt ?
                                                           1.0 : m_treedyn.m_alm.height_competition_factor( p, m_veg));
                    
                    // potential height according to current biomass
                    double  height_pot( m_treedyn.m_alm.height_from_biomass(
                                                                            (*p)->CONIFEROUS(),
                                                                            (*p)->TAP_P1(), (*p)->TAP_P2(), (*p)->TAP_P3(),
                                                                            (*p)->HD_MIN(), (*p)->HD_MAX(), (*p)->HD_EXP(),
                                                                            p->stand_volume() / p->tree_number,
                                                                            hd_competition_factor_old));
                    // fraction between actual and potenital biomass
                    double const height_fraction( cbm::flt_greater_zero( height_pot) ?
                                                 p->height_max / height_pot : 1.0);
                    
                    double  dbh_pot( m_treedyn.m_alm.diameter_at_breast_height_from_volume_and_height(
                                                                                                      (*p)->CONIFEROUS(),
                                                                                                      (*p)->TAP_P1(), (*p)->TAP_P2(), (*p)->TAP_P3(),
                                                                                                      height_pot,
                                                                                                      p->stand_volume() / p->tree_number));
                    double const dbh_fraction( cbm::flt_greater_zero( dbh_pot) ? p->dbh / dbh_pot : 1.0);
                    
                    double  dbas_pot( m_treedyn.m_alm.diameter_at_ground(
                                                                 dbh_pot,
                                                                 height_pot,
                                                                 (*p)->HD_MIN(), (*p)->HD_MAX(), (*p)->HD_EXP(),
                                                                 hd_competition_factor_old));
                    double const dbas_fraction( cbm::flt_greater_zero( dbas_pot) ? p->dbas / dbas_pot : 1.0);
                    
                    p->tree_number = std::max( p->tree_number, treenumber_new);
                    m_treedyn.m_alm.restructure_vegetation(
                                                   p,
                                                   crownlength_opt,
                                                   hd_competition_factor_old,
                                                   height_fraction,
                                                   dbas_fraction,
                                                   dbh_fraction);
                }
            }
        }

        if ( reduce_biomass)
        {
            for ( TreeIterator vt = m_veg->groupbegin< species::wood >();
                  vt != m_veg->groupend< species::wood >();  ++vt)
            {
                MoBiLE_Plant *p = (*vt);

                // distributing below- and aboveground biomass according to tree section heights (consistent with assumptions of biomass/volume relations)
                double const relative_belowground_fraction( (p->rooting_depth / 3.0) /
                                                            (p->height_at_canopy_start + (p->height_max - p->height_at_canopy_start) / 3.0));

                if ( cbm::flt_greater_zero( mSapOld_vt[p->slot]) &&
                    cbm::flt_greater( p->mSap, mSapOld_vt[p->slot]))
                {
                    double const sap_loss( p->mSap - mSapOld_vt[p->slot]);   // difference in sapwood biomass between years
                    double const n_sap_loss( sap_loss * p->ncSap);           // not strictly true if ncSap has changed during the year
                    
                    p->sWoodAbove += (1.0 - relative_belowground_fraction) * sap_loss;
                    p->nLitWoodAbove += (1.0 - relative_belowground_fraction) * n_sap_loss;

                    for ( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
                    {
                        p->sWoodBelow_sl[sl] += relative_belowground_fraction * sap_loss * p->fFrt_sl[sl];
                        p->nLitWoodBelow_sl[sl] += relative_belowground_fraction * n_sap_loss * p->fFrt_sl[sl];
                    }
                    p->mSap -= sap_loss;
                }
                mSapOld_vt[p->slot] = p->mSap;
                
                if ( cbm::flt_greater_zero( mCorOld_vt[p->slot]) &&
                    cbm::flt_greater( p->mCor, mCorOld_vt[p->slot]))
                {
                    double const cor_loss( p->mCor - mCorOld_vt[p->slot]);
                    double const n_cor_loss( cor_loss * p->ncCor);
                    
                    p->sWoodAbove += (1.0 - relative_belowground_fraction) * cor_loss;
                    p->nLitWoodAbove += (1.0- relative_belowground_fraction) * n_cor_loss;

                    for ( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
                    {
                        p->sWoodBelow_sl[sl] += relative_belowground_fraction * cor_loss * p->fFrt_sl[sl];
                        p->nLitWoodBelow_sl[sl] += relative_belowground_fraction * n_cor_loss * p->fFrt_sl[sl];
                    }
                    p->mCor -= cor_loss;
                }
                mCorOld_vt[p->slot] = p->mCor;
            }
        }
    }

    return  LDNDC_ERR_OK;
}


lerr_t
PhysiologyGrowthPSIM::PSIM_Photosynthesis()
{
    for ( PlantIterator vt = this->m_veg->begin(); vt != this->m_veg->end(); ++vt)
    {
        MoBiLE_Plant *p = *vt;

        for (size_t fl = 0; fl < p->nb_foliagelayers(); ++fl)
        {
            m_photo.vpd_fl[fl] = mc_.vpd_fl[fl];
            m_photo.rh_fl[fl] = cl_.rel_humidity_subday( lclock_ref());
            m_photo.temp_fl[fl] = mc_.temp_fl[fl];
            m_photo.parsun_fl[fl] = mc_.parsun_fl[fl];
            m_photo.parshd_fl[fl] = mc_.parshd_fl[fl];
            m_photo.tFol_fl[fl] = mc_.tFol_fl[fl];
            m_photo.co2_concentration_fl[fl] = ac_.ts_co2_concentration_fl[fl];
            m_photo.sunlitfoliagefraction_fl[fl] = mc_.ts_sunlitfoliagefraction_fl[fl];
        }
        m_photo.nd_airpressure = mc_.nd_airpressure;
        
        m_photo.set_vegetation_base_state( p);

        m_photo.set_vegetation_non_stomatal_water_limitation_state( p->xylem_resistance, p->psi_mean, p->psi_pd);
        
        m_photo.solve();
        
        m_photo.get_vegetation_state( p);
    }

    return LDNDC_ERR_OK;
}


lerr_t
PhysiologyGrowthPSIM::solve()
{
    lerr_t rc( LDNDC_ERR_OK);

    // no plants
    if ( m_veg->size() == 0)
    { return rc; }

    rc = PSIM_ResizeVegetationVectors();
    if ( rc){ KLOGERROR("Resizing vectors not successful"); return rc; }
    
    rc = PSIM_PlantingEvent();
    if ( rc){ KLOGERROR("Planting not successful"); return rc; }
    
    rc = PSIM_HydraulicConductance();
    if ( rc){ KLOGERROR("Calculating hydraulic conductance not successful"); return rc; }
    
    rc = PSIM_Photosynthesis();
    if ( rc){ KLOGERROR("Calculating Photosynthesis not successful"); return rc; }

    rc = PSIM_Potentialranspiration();
    if ( rc){ KLOGERROR("Calculating Potential Transpiration not successful"); return rc; }
    
    if ( (!lclock()->is_position( TMODE_POST_DAILY)) &&
         (!subdaily_timemode()))
    {
        return LDNDC_ERR_OK;
    }

    rc = PSIM_StepInit();
    if ( rc){ KLOGERROR("Initialization not successful"); return rc; }

    rc = PSIM_BalanceCheck(1);
    if ( rc){ KLOGERROR("Balance check at position 1 not successful"); return rc; }
    
    rc = PSIM_ResetFlushing();
    if ( rc){ KLOGERROR("Reset of flushing not successful"); return rc; }
    
    rc = PSIM_ResetBiomassFromLastYear();
    if ( rc){ KLOGERROR("Biomass update from last year not successful"); return rc; }
    
    rc = PSIM_AgriculturalManagement();
    if ( rc){ KLOGERROR("Consideration of agricultural management not successful"); return rc; }
    
    rc = PSIM_BudBurst();
    if ( rc){ KLOGERROR("Calculation of bud bursting not successful"); return rc; }
    
    rc = PSIM_UpdateNitrogenConc();
    if ( rc){ KLOGERROR("Calculation of nitrogen concentrations not successful"); return rc; }
    
    rc = PSIM_Senescence();
    if ( rc){ KLOGERROR("Senescence not successful"); return rc; }
    
    rc = PSIM_CarAllocation();
    if ( rc){ KLOGERROR("Carbon allocation not successful"); return rc; }
    
    rc = PSIM_SoilCarbonRelease();
    if ( rc){ KLOGERROR("Soil carbon release not successful"); return rc; }
    
    rc = PSIM_NitAllocation();
    if ( rc){ KLOGERROR("Nitrogen allocation not successful"); return rc; }
    
    rc = PSIM_Respiration();
    if ( rc){ KLOGERROR("Calculation of respiration not successful"); return rc; }
    
    rc = PSIM_NitrogenFixation();
    if ( rc){ KLOGERROR("Nitrogen fixation not successful"); return rc; }
    
    rc = PSIM_NitrogenUptake();
    if ( rc){ KLOGERROR("Nitrogen uptake not successful"); return rc; }
    
    rc = PSIM_BiomassUpdate();
    if ( rc){ KLOGERROR("Calculation of Biomass not successful"); return rc; }
    
    rc = PSIM_PhotosynthesisRates();
    if ( rc){ KLOGERROR("Calculation of Photosynthesis rates not successful"); return rc; }
    
    rc = PSIM_DroughtStress();
    if ( rc){ KLOGERROR("Calculation of drought stress not successful"); return rc; }
    
    rc = PSIM_NitrogenBalance();
    if ( rc){ KLOGERROR("Nitrogen balance not successful"); return rc; }

    if( forest_structure_opt)
    {
        rc = PSIM_VegStructure();
        if ( rc){ KLOGERROR("Calculation of vegetation structure not successful"); return rc; }
    }

    rc = PSIM_BalanceCheck(2);
    if ( rc){ KLOGERROR("Balance check at position 2 not successful"); return rc; }

    rc = PSIM_Harvest();
    if ( rc){ KLOGERROR("Harvest not successful"); return rc; }
    
    rc = PSIM_StepOut();
    if ( rc){ KLOGERROR("Finalization not successful"); return rc; }
    
    return LDNDC_ERR_OK;
}


lerr_t
PhysiologyGrowthPSIM::PSIM_StepOut()
{
    for ( PlantIterator vt = this->m_veg->begin(); vt != this->m_veg->end(); ++vt)
    {
        MoBiLE_Plant *p = (*vt);
        
        rFolOld_vt[p->slot] = p->rFol;
        rBudOld_vt[p->slot] = p->rBud;
        rSapOld_vt[p->slot] = p->rSap;
        rFrtOld_vt[p->slot] = p->rFrt;
        rTraOld_vt[p->slot] = p->rTra;
        exsuLossOld_vt[p->slot] = p->exsuLoss;

        uptNH4Old_vt[p->slot] = uptNH4_vt[p->slot];
        uptNO3Old_vt[p->slot] = uptNO3_vt[p->slot];
        uptNH3Old_vt[p->slot] = uptNH3_vt[p->slot];
        uptDONOld_vt[p->slot] = uptDON_vt[p->slot];

        uptN2Old_vt[p->slot] = p->n2_fixation;
        uptNOxOld_vt[p->slot] = p->nox_uptake;
        
        // aboveground litter
        sc_.c_raw_lit_1_above += ((p->sFol + p->sBud) * (1.0 - vt->CELLULOSE() - vt->LIGNIN())) * cbm::CCDM;
        sc_.c_raw_lit_2_above += ((p->sFol + p->sBud) * vt->CELLULOSE()) * cbm::CCDM;
        sc_.c_raw_lit_3_above += ((p->sFol + p->sBud) * vt->LIGNIN()) * cbm::CCDM;
        
        sc_.n_raw_lit_1_above += (p->nLitFol + p->nLitBud) * (1.0 - vt->CELLULOSE() - vt->LIGNIN());
        sc_.n_raw_lit_2_above += (p->nLitFol + p->nLitBud) * vt->CELLULOSE();
        sc_.n_raw_lit_3_above += (p->nLitFol + p->nLitBud) * vt->LIGNIN();
        
        sc_.accumulated_c_litter_above += (p->sFol + p->sBud) * cbm::CCDM;
        sc_.accumulated_n_litter_above += (p->nLitFol + p->nLitBud);

        sc_.c_wood += p->sWoodAbove * cbm::CCDM;
        sc_.accumulated_c_litter_wood_above += p->sWoodAbove * cbm::CCDM;

        sc_.n_wood += p->nLitWoodAbove;
        sc_.accumulated_n_litter_wood_above += p->nLitWoodAbove;
        
        
        // belowground wood and fine roots
        for ( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  sl++)
        {
            sc_.c_wood_sl[sl] += p->sWoodBelow_sl[sl] * cbm::CCDM;
            sc_.accumulated_c_litter_wood_below_sl[sl] += p->sWoodBelow_sl[sl] * cbm::CCDM;
            
            sc_.n_wood_sl[sl] += p->nLitWoodBelow_sl[sl];
            sc_.accumulated_n_litter_wood_below_sl[sl] += p->nLitWoodBelow_sl[sl];
            
            sc_.c_raw_lit_1_sl[sl] += p->sFrt_sl[sl] * (1.0 - vt->CELLULOSE() - vt->LIGNIN()) * cbm::CCDM;
            sc_.c_raw_lit_2_sl[sl] += p->sFrt_sl[sl] * vt->CELLULOSE() * cbm::CCDM;
            sc_.c_raw_lit_3_sl[sl] += p->sFrt_sl[sl] * vt->LIGNIN() * cbm::CCDM;
            sc_.accumulated_c_litter_below_sl[sl] += p->sFrt_sl[sl] * cbm::CCDM;
            
            sc_.n_raw_lit_1_sl[sl] += p->nLitFrt_sl[sl] * (1.0 - vt->CELLULOSE() - vt->LIGNIN());
            sc_.n_raw_lit_2_sl[sl] += p->nLitFrt_sl[sl] * vt->CELLULOSE();
            sc_.n_raw_lit_3_sl[sl] += p->nLitFrt_sl[sl] * vt->LIGNIN();
            sc_.accumulated_n_litter_below_sl[sl] += p->nLitFrt_sl[sl];
        }

        // root exsudation
        if ( cbm::flt_greater_zero( p->exsuLoss))
        {
            for ( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
            {
                double fdoc( sc_.doc_sl[sl] + sc_.an_doc_sl[sl]);
                if ( cbm::flt_greater_zero( fdoc))
                {
                    fdoc = sc_.doc_sl[sl] / fdoc;
                }
                else
                {
                    fdoc = 0.5;
                    sc_.doc_sl[sl] = 0.0;
                    sc_.an_doc_sl[sl] = 0.0;
                }

                double  add_doc( p->exsuLoss * p->fFrt_sl[sl]);
                sc_.doc_sl[sl]    += add_doc * fdoc;
                sc_.an_doc_sl[sl] += add_doc * ( 1.0 - fdoc);
                
                sc_.accumulated_c_root_exsudates_sl[sl] += add_doc;
            }
        }
    }

    for ( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
    {
        if ( cbm::flt_greater_zero( nh4_sl[sl]))
        {
            sc_.nh4_sl[sl] = nh4_sl[sl];
        }
        else { sc_.nh4_sl[sl] = 0.0; }
        
        if ( cbm::flt_greater_zero( don_sl[sl]))
        {
            sc_.don_sl[sl] = don_sl[sl];
        }
        else { sc_.don_sl[sl] = 0.0; }

        double const no3_tot( sc_.no3_sl[sl] + sc_.an_no3_sl[sl]);
        if ( cbm::flt_greater_zero( no3_tot))
        {
            sc_.no3_sl[sl] = sc_.no3_sl[sl] / no3_tot * no3_sl[sl];
            sc_.an_no3_sl[sl] = no3_sl[sl] - sc_.no3_sl[sl];
        }
        else
        {
            sc_.no3_sl[sl] = no3_sl[sl] * (1.0 - sc_.anvf_sl[sl]);
            sc_.an_no3_sl[sl] = no3_sl[sl] - sc_.no3_sl[sl];
        }
    }

    return LDNDC_ERR_OK;
}


lerr_t
PhysiologyGrowthPSIM::PSIM_Harvest()
{
    EventAttributes const *  ev_harv = NULL;
    while (( ev_harv = this->m_HarvestEvents.pop()) != NULL)
    {
        MoBiLE_Plant * p = m_veg->get_plant( ev_harv->get( "/name", "?"));
        if ( !p){ return LDNDC_ERR_RUNTIME_ERROR; }

        p->dcFol = 0.0;
        p->dcSap = 0.0;
        p->dcBud = 0.0;
        p->dcFrt = 0.0;
        p->dcFac = 0.0;

        p->rRes = 0.0;
        p->rGro = 0.0;
        p->rGroBelow = 0.0;
        p->rFol = 0.0;
        p->rSap = 0.0;
        p->rSapBelow = 0.0;
        p->rBud = 0.0;
        p->rFrt = 0.0;
        p->rTra = 0.0;

        p->sBud = 0.0;
        p->sWoodAbove = 0.0;
        p->sFol = 0.0;
        for ( size_t  na = 0;  na < p->nb_ageclasses();  ++na)
        {
            p->sFol_na[na] = 0.0;
        }

        p->nLitFol = 0.0;
        p->nLitWoodAbove = 0.0;
        p->nLitBud = 0.0;
        p->n_retention = 0.0;

        p->nox_uptake = 0.0;
        p->n2_fixation = 0.0;

        p->exsuLoss = 0.0;

        carbonuptake_vt[p->slot] = 0.0;

        for ( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
        {
            p->sFrt_sl[sl] = 0.0;
            p->nLitFrt_sl[sl] = 0.0;
        }

        mSapOpt_vt[p->slot] = 0.0;
        mSapOld_vt[p->slot] = 0.0;
        mCorOld_vt[p->slot] = 0.0;
        mBudLoss_vt[p->slot] = 0.0;
        mSapLoss_vt[p->slot] = 0.0;
        ncFolOpt[p->slot] = 0.0;
        ncBudOpt[p->slot] = 0.0;
        ts_leaftemp_vt[p->slot] = 0.0;
        nd_leaftemp_vt[p->slot] = 0.0;
        nDem_vt[p->slot] = 0.0;
        nFol_vt[p->slot] = 0.0;
        nFrt_vt[p->slot] = 0.0;
        nSap_vt[p->slot] = 0.0;
        nCor_vt[p->slot] = 0.0;
        nBud_vt[p->slot] = 0.0;

        uptNH4Max_vt[p->slot] = 0.0;
        uptNO3Max_vt[p->slot] = 0.0;
        uptDONMax_vt[p->slot] = 0.0;
        uptNWetMax_vt[p->slot] = 0.0;
        uptNWet_vt[p->slot] = 0.0;
        uptNTot_vt[p->slot] = 0.0;
        uptNH4Max_vtsl[p->slot] = 0.0;
        uptNO3Max_vtsl[p->slot] = 0.0;
        uptDONMax_vtsl[p->slot] = 0.0;

        rFolOld_vt[p->slot] = 0.0;
        rBudOld_vt[p->slot] = 0.0;
        rSapOld_vt[p->slot] = 0.0;
        rFrtOld_vt[p->slot] = 0.0;
        rTraOld_vt[p->slot] = 0.0;
        exsuLossOld_vt[p->slot] = 0.0;

        uptNH4_vt[p->slot] = 0.0;
        uptNO3_vt[p->slot] = 0.0;
        uptNH3_vt[p->slot] = 0.0;
        uptDON_vt[p->slot] = 0.0;

        uptNH4Old_vt[p->slot] = 0.0;
        uptNO3Old_vt[p->slot] = 0.0;
        uptNH3Old_vt[p->slot] = 0.0;
        uptDONOld_vt[p->slot] = 0.0;

        uptN2Old_vt[p->slot] = 0.0;
        uptNOxOld_vt[p->slot] = 0.0;

        n_bud_to_fol_vt[p->slot] = 0.0;

        n_sap_to_cor_vt[p->slot] = 0.0;
        n_bud_to_cor_vt[p->slot] = 0.0;
        n_fol_to_cor_vt[p->slot] = 0.0;
        n_frt_to_cor_vt[p->slot] = 0.0;

        n_sap_to_litter_vt[p->slot] = 0.0;
        
        n_sap_to_redistribute_vt[p->slot] = 0.0;
        n_bud_to_redistribute_vt[p->slot] = 0.0;
        n_fol_to_redistribute_vt[p->slot] = 0.0;
        n_frt_to_redistribute_vt[p->slot] = 0.0;

        n_sap_gain_vt[p->slot] = 0.0;
        n_bud_gain_vt[p->slot] = 0.0;
        n_fol_gain_vt[p->slot] = 0.0;
        n_frt_gain_vt[p->slot] = 0.0;

        nh3_uptake_fl[p->slot] = 0.0;

        root_system.erase( p->slot);

        p->nox_uptake = 0.0;
        p->n2_fixation = 0.0;
    }
    return  LDNDC_ERR_OK;
}


lerr_t
PhysiologyGrowthPSIM::PSIM_BalanceCheck(size_t _i)
{
    if (_i == 1)
    {
        // all incoming C and N pools for balance checks (NOTE: done after management so that fertilization is already included!)
        tot_c = 0.0;
        tot_n = 0.0;
        for ( PlantIterator vt = this->m_veg->begin(); vt != this->m_veg->end(); ++vt)
        {
            MoBiLE_Plant *p = (*vt);
            tot_c += p->total_biomass() * cbm::CCDM;
            tot_n += p->total_nitrogen();
        }
    }
    else
    {
        // C and N conservation checks
        double totC_after( 0.0);
        double totN_after( 0.0);
        for ( PlantIterator vt = this->m_veg->begin(); vt != this->m_veg->end(); ++vt)
        {
            MoBiLE_Plant *p = (*vt);
            totC_after += p->total_biomass() * cbm::CCDM;
            totN_after += p->total_nitrogen();
        }

        // - input
        for ( PlantIterator vt = this->m_veg->begin(); vt != this->m_veg->end(); ++vt)
        {
            MoBiLE_Plant *p = (*vt);
            totC_after -= carbonuptake_vt[p->slot];
            totN_after -= (uptNH4Old_vt[p->slot] + uptNO3Old_vt[p->slot] + uptDONOld_vt[p->slot]
                           + uptNOxOld_vt[p->slot] + uptNH3Old_vt[p->slot] + uptN2Old_vt[p->slot]);
        }

        // - loss
        for ( PlantIterator vt = this->m_veg->begin(); vt != this->m_veg->end(); ++vt)
        {
            MoBiLE_Plant *p = (*vt);
            totC_after += (rFolOld_vt[p->slot] + rFrtOld_vt[p->slot]+ rSapOld_vt[p->slot] + rBudOld_vt[p->slot]
                           + rTraOld_vt[p->slot] + p->rGro
                           + cbm::CCDM * (p->sFol + p->sWoodAbove + p->sBud)
                           + p->exsuLoss);
            totN_after += (p->nLitFol + p->nLitWoodAbove + p->nLitBud);
            
            for ( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
            {
                totC_after += p->sFrt_sl[sl] + p->sWoodBelow_sl[sl];
                totN_after += p->nLitFrt_sl[sl] + p->nLitWoodBelow_sl[sl];
            }
        }

        double difference( std::abs(tot_c - totC_after));
        if (difference > cbm::DIFFMAX)
        {
            //KLOGWARN("C-Leak: ", tot_c - totC_after, " [kg C m-2]");
        }

        difference = std::abs(tot_n - totN_after);
        if (difference > cbm::DIFFMAX)
        {
            //KLOGWARN("N-Leak: ", tot_n - totN_after, " [kg N m-2]");
        }
    }
    
    return  LDNDC_ERR_OK;
}



lerr_t
PhysiologyGrowthPSIM::PSIM_ResetFlushing()
{
    for ( PlantIterator vt = this->m_veg->begin(); vt != this->m_veg->end(); ++vt)
    {
        MoBiLE_Plant *p = (*vt);

        if ( p->nb_ageclasses() > 1)
        {
            // for evergreen plants always reset at the beginning of the new year
            if ( (lclock()->yearday() >= PSIM_YEAR_START) &&
                 (lclock()->year() > phenological_year[p->slot]))
            {
                phenological_year[p->slot] = lclock()->year();
                PSIM_ResetPhenology( p);
            }
        }
        else
        {
            // for non-evergreen plants phenological season ends with full mortality
            if ( cbm::flt_greater_equal( p->dvsMort, 1.0))
            {
                // immediate reset if more than one phenological season is possible within one year
                if ( additional_season[p->slot] == true)
                {
                    PSIM_ResetPhenology( p);
                }
                // for one phenological season per year always reset at the beginning of the new year
                else if ( (lclock()->yearday() >= PSIM_YEAR_START) &&
                          (lclock()->year() > phenological_year[p->slot]))
                {
                    phenological_year[p->slot] = lclock()->year();
                    PSIM_ResetPhenology( p);
                }
            }
        }
    }
    
    return  LDNDC_ERR_OK;
}


lerr_t
PhysiologyGrowthPSIM::PSIM_ResetPhenology( MoBiLE_Plant *_p)
{
    _p->dEmerg = -1;
    foliage_age_of_start_senescence[_p->slot] = m_pf.get_foliage_age_of_senescence( _p, 0.0);
    days_since_emergence[_p->slot] = 0;
    _p->dvsFlush = 0.0;
    _p->dvsFlushOld = 0.0;
    _p->growing_degree_days = 0.0;
    _p->mFolMax = 0.0;
    _p->mBudStart = _p->mBud;
    
    //shift foliage age class
    for ( int na = _p->nb_ageclasses()-1; na > 0; --na)
    {
        _p->mFol_na[na] = _p->mFol_na[na-1];
        foliage_age_na[_p->slot][na] = foliage_age_na[_p->slot][na-1];
    }
    _p->mFol_na[0] = 0.0;
    foliage_age_na[_p->slot][0] = 0;
    additional_season[_p->slot] = false;

    return  LDNDC_ERR_OK;
}


lerr_t
PhysiologyGrowthPSIM::PSIM_UpdateNitrogenConc()
{
    for ( PlantIterator vt = this->m_veg->begin(); vt != this->m_veg->end(); ++vt)
    {
        MoBiLE_Plant *p = (*vt);

        /* Update optimum N concentrations */
        double nFolOpt( m_pf.optimum_nitrogen_content_foliage( p));
        ncFolOpt[p->slot] = ( p->mFol > 0.0) ? ( nFolOpt / p->mFol) : vt->NCFOLOPT();

        double nBudOpt( m_pf.optimum_nitrogen_content_buds( p));
        ncBudOpt[p->slot] = ( p->mBud > 0.0) ? ( nBudOpt / p->mBud) : vt->NCFOLOPT();

        /* N amounts in each compartment */
        nFol_vt[p->slot] = p->n_fol();
        nFrt_vt[p->slot] = p->n_frt();
        nSap_vt[p->slot] = p->n_sap();
        nBud_vt[p->slot] = p->n_bud();
        nCor_vt[p->slot] = p->n_cor();
    }
    
    return  LDNDC_ERR_OK;
}


/*!
 * PSIM considers:
 * - grazing (for grasses)
 * - cutting (for grasses)
 * (for other management options on trees, see ref@ ld_treeDyn:treedyn_events)
 */
lerr_t
PhysiologyGrowthPSIM::PSIM_AgriculturalManagement()
{
    m_eventgraze.update_available_freshfood_c( m_veg, species_groups_select_t< species::grass >());

    for ( PlantIterator vt = m_veg->begin(); vt != m_veg->end(); ++vt)
    {
        MoBiLE_Plant *p = (*vt);
        if ( p->group() == "grass")
        {
            lerr_t rc_graze = m_eventgraze.event_graze_physiology( *vt);
            if ( (rc_graze != LDNDC_ERR_EVENT_MATCH) &&
                 (rc_graze != LDNDC_ERR_EVENT_EMPTY_QUEUE))
            {
                LOGERROR("Grazing not successful");
                return rc_graze;
            }
        }
    }

    // cutting event by external module
    lerr_t rc_cut = m_eventcut.event_cut_physiology( m_veg, species_groups_select_t< species::grass >());
    if ( (rc_cut != LDNDC_ERR_EVENT_MATCH) &&
         (rc_cut != LDNDC_ERR_EVENT_EMPTY_QUEUE))
    {
        LOGERROR("Cutting not successful");
        return rc_cut;
    }
    else if (rc_cut == LDNDC_ERR_EVENT_MATCH)
    {
        for (PlantIterator vt = m_veg->begin(); vt != m_veg->end(); ++vt)
        {
            MoBiLE_Plant* p = (*vt);
            m_treedyn.OnStructureChange(p);
        }
    }

    return LDNDC_ERR_OK;
}


//----------------------------------------------------------------
/*!
 * @page psim
 * @section PSIM_HydraulicConductance Hydraulic conductance
 * The hydraulic approach calculates canopy water potential based on soil water potential,
 * which then affects stomal conductance following the 'Stomata on Xylem' (SOX) model 
 * (see: @ref PhysiologyPHOTOFARQUHAR):
 * First, xylem water potential is derived from the soil conditions, weighting soil layer
 * importance by fine root abundance and considering a threshold soil water potential
 * at which plant tissues are decoupled from the soil:
 * \f[
 *   psi\_{sr} = Sum[(min(CSR\_{REF}; psi_{sl} \cdot fFrt_{sl})]
 * \f]
 * with
 * - CSR_REF: Threshold soil water potential at which roots and soil get decoupled (MPa)
 * - psi:     Soil water potential (MPa) (see @ref assign_soillayer_van_genuchten_parameter)
 * - fFrt:    Biomass fraction of fine roots in a particular soil layer
 * - sl:      Indicator for a specific soil layer
 *
 * Canopy water potential is then calculated from the gradient between the water potential 
 * in the xylem and from evaporative demand, also including the gravitational force.
 * \f[
 * psi\_{can} = (psi\_{sr} + psidecline\_{cum}) - (\frac {transp}{CWP\_{REF} \cdot kxyl} ) - dpsi
 * \f]
 * with
 * - psidecline_cum: previous day cumulative water deficit within the tree
 * - kxyl:           previous day root-to-canopy (plant hydraulic) conductance (in mol MPa-1 s-1 m-2leaf)
 * - transp:         current day transpiration (mol m-2leaf s-1)
 * - CWP_REF:        reference (=maximum) value for the (leaf area-normalized) specific xylem conductance (mol MPa-1 m-2leaf s-1)
 * - dpsi:           water potential decline due to plant height (MPa)
 *
 * The water deficit is build from the water that is transpired but was not available from the rooting zone.
 * It is supposed to reflect the water resources within a tree, i.e. the stem water content. 
 * Surplus on water supply will refill this storage while additional deficits will empty it further.
 * 
 * The total plant, or xylem, conductance is calculated based on empirical relations with species-specific parameters (PSI_REF, PSI_EXP).
 * \f[
 *   kxyl = 1.0 - (1.0 - exp(-((\frac {psi\_{can}} {PSI\_{REF}})^{PSI\_{EXP}} )))
 * \f]
 *
 * @note The CSR_REF parameter defines a limit for water uptake that is in principle the same as the wilting point and might replace this site property.
 */
lerr_t
PhysiologyGrowthPSIM::PSIM_HydraulicConductance()
{
    // realized (soil water limited) transpiration (per ground area) in m per time step (should vary per hour) and vegetation type
    accumulated_transpiration_old = wc_.accumulated_transpiration_sl.sum();

    // transpiration demand given by stomatal conductance
    double transpiration_demand(0.0);
    if (transpiration_method == "potentialtranspiration")
    {
        transpiration_demand = wc_.accumulated_potentialtranspiration - accumulated_potentialtranspiration_old;   //currently m per time step
    }
    accumulated_potentialtranspiration_old = wc_.accumulated_potentialtranspiration;

    // relative recovery of plant water deficit from the last time step
    double rehydrationindex(0.0);
    if (cbm::flt_greater_zero(plant_waterdeficit_old))
    {
        rehydrationindex = cbm::bound(0.0,
            (plant_waterdeficit_old - wc_.plant_waterdeficit) / plant_waterdeficit_old,
            1.0);
    }
    plant_waterdeficit_old = wc_.plant_waterdeficit;

    // number of hours per day
    double const daylength_hours(meteo::daylength(m_setup->latitude(), this->lclock()->yearday()));

    double const lai_sum(m_veg->lai());

    // Setting soil to root hydraulic conductance equal to soil conductance
    for (PlantIterator vt = this->m_veg->begin(); vt != this->m_veg->end(); ++vt)
    {
        MoBiLE_Plant* p = *vt;

        // length of timestep [s]
        double const tslength = double(cbm::SEC_IN_HR) * double(cbm::HR_IN_DAY) / double(this->lclock_ref().time_resolution());

        // transform transpiration units from m timestep-1 to mol m-2leaf s-1 and relate it to daylength
        // TODO: find a better way to differentiate transpiration into vegetation classes
        double transp_mol(0.0);
        if ( cbm::flt_greater_zero( lai_sum))
        {
            double transp_vt = transpiration_demand * p->lai() / lai_sum;
            transp_mol = transp_vt * cbm::MM_IN_M * cbm::G_IN_KG / (tslength * cbm::MH2O * p->lai())
                                / (meteo::daylength(m_setup->latitude(), this->lclock()->yearday()) / cbm::HR_IN_DAY);
        }
   
        // calling soil hydraulic conductance as well as soil water potential and weighting by fine root abundance
        double psi_sr(0.0);
        //double psi_soil(0.0);
        double psi_detach(0.0);
        for (size_t sl = 0; sl < sl_.soil_layer_cnt(); ++sl)
        {
            double const wc_min(sc_.wfps_min_sl[sl] * sc_.poro_sl[sl]);
            double const wc_max(sc_.wfps_max_sl[sl] * sc_.poro_sl[sl]);
            double const wc(cbm::bound(1.001 * wc_min, wc_.wc_sl[sl], 0.999 * wc_max));

            // soil layer water potential at which roots get detached
            double psi_detach_sl((-1) * vt->CSR_REF());

            // soil layer water potential in MPa (positive units)
            double psi_sl(ldndc::hydrology::capillary_pressure(wc, sc_.vga_sl[sl], sc_.vgn_sl[sl],
                sc_.vgm_sl[sl], wc_max, wc_min));

            // soil, soil to root and threshold water potential (MPa) weighted by fine root abundance
            psi_sr += (std::min(psi_detach_sl, psi_sl) * p->fFrt_sl[sl]);
            //psi_soil += (psi_sl * p->fFrt_sl[sl]);
            psi_detach += (psi_detach_sl * p->fFrt_sl[sl]);
        }

        // soil/root water potential in MPa (in negative terms), psi_soil only for checking (not used)
        psi_sr *= -1.0;
        //psi_soil *= -1.0;
        psi_detach *= -1.0;

        // plant water potential, calculated by adding the (negative) water potential decrease of the previous day
        double psi_vt(psi_sr + psidecline_cum_vt[p->slot]);

        // relative xylem conductivity based on current soil water state and previous plant water deficit
        double krc_rel = 1.0 - (1.0 - std::exp(-std::pow(psi_vt / vt->PSI_REF(), vt->PSI_EXP())));
        krc_rel = std::max(0.001, krc_rel);

        // canopy water potential
        // (calculated in a lumped fashion, alternatively define psi for each canopy layer separately)
        double h_weighted(0.0);    // [m]
        double height_cum(0.0);    // [m]
        for (size_t fl = 0; fl < p->nb_foliagelayers(); ++fl)
        {
            height_cum += 0.5 * ph_.h_fl[fl];
            h_weighted += height_cum * p->fFol_fl[fl];
            height_cum += 0.5 * ph_.h_fl[fl];
        }

        // increase of water potential due to vertical transport
        double const dpsi = h_weighted * cbm::MPA_IN_KPA * cbm::KPA_IN_PA * cbm::DWAT * cbm::DM3_IN_M3 * cbm::G;

        // predawn canopy water potential
        int night(1);
        if (cbm::flt_equal_zero(mc_.shortwaveradiation_in))
        {
            // in the night, the multiplier for psi that accounts for increasing tensure is set to zero
            night = 0;

            // calculations once per day directly after sunset (TODO: ensure that daytime is true at the first call)
            if (daytime == true)
            {
                // weighting by leaf/wood ratio as proxy for capacitance change
//                double const qcapacitance = p->mFol / (p->mFol + p->mSap + p->mCor + p->mFrt);  // assuming that active water is also stored in dead wood
                double const qcapacitance = p->mFol / (p->mFol + p->mSap + p->mFrt);  // assuming that there is no active water in the core of the stem

                // recovery of cumulative plant water potential due to rehydration during the previous day
                // NOTE: assumes that relative rehydration and plant water potential recovers in parallel
                psidecline_cum_vt[p->slot] *= std::min(1.0, (1.0 - rehydrationindex_cum_vt[p->slot]));

                // decline of cumulative plant water potential due to cumulative canopy water potential decline during the previous day
                if (psi_sr <= psi_detach)
                {
                    psidecline_cum_vt[p->slot] += std::min(0.0, (psidecline_daycum_vt[p->slot] / daylength_hours) * qcapacitance);
                }

                // reset of the cumulative rehydration index 
                rehydrationindex_cum_vt[p->slot] = 0.0;

                // reset the cumulative water potential deficit
                psidecline_daycum_vt[p->slot] = 0.0;
            }

            daytime = false;
        }
        else
        {
            //  calculations once per day directly after sunrise
            if (daytime == false)
            {
                // predawn canopy water potential (corrected: formerly in the first day of night)
                p->psi_pd = psi_vt - dpsi;
            }

            daytime = true;
        }

        // canopy hydraulic potential in MPa (negative) due to water loss (only at daytime) considering height induced gradient
        double psi(psi_vt);
        if (cbm::flt_greater_zero(krc_rel))
        {
            psi = psi_vt - (transp_mol * double(night) / (vt->CWP_REF() * krc_rel)) - dpsi;
        }

        // mean canopy water potential (not important, but smoothed between timesteps)
        p->psi_mean = (p->psi_pd + psi) * 0.5;

        // drop in plant water potential (!) due to evaporative demand cumulated over the day
        psidecline_daycum_vt[p->slot] += std::min(0.0, (p->psi_mean + dpsi - psi_vt));

        // daily cumulative reduction in drop in water potential after refilling (rehydration_index is in fraction per hour, needed in fraction per day)
        rehydrationindex_cum_vt[p->slot] += rehydrationindex;
        rehydrationindex_cum_vt[p->slot] = cbm::bound(0.0, rehydrationindex_cum_vt[p->slot], 1.0);

        // new overall root-to-canopy (plant hydraulic) conductance considering current day demands (in mol MPa-1 s-1 m-2)
        double k_rc = 1.0 - (1.0 - std::exp(-std::pow(p->psi_mean / vt->PSI_REF(), vt->PSI_EXP())));
        k_rc = std::max(0.001, k_rc);

        // whole plant resistances (in MPa s m2 mol-1)
        p->xylem_resistance = vt->RPMIN();
        if (cbm::flt_greater_zero(k_rc))
        {
            p->xylem_resistance /= k_rc;
        }

    } // vegetation type loop end

    return LDNDC_ERR_OK;
}


//----------------------------------------------------------------
/*!
 * @page psim
 * @section psim_budburst Phenology
 * Leaf flushing is calculated using the growing degree days approach, 
 * which is using the temperature sum since this years 1st January,
 * considering chilling requirements and drought stress limitations.
 * 
 * Therefore, temperature sum (cumulative growing degree days, gdd),is first
 * calculated from daily temperature sum (@cite lehning:2001a): 
 * \f[
 * gdd\_cum = temp \cdot \frac{dayl}{12.0}
 * \f]
 * with
 * - temp: daily average leaf surface temperature (oC)
 * - dayl: daylength (h)
 *
 * When cumulative gdd reaches a threshhold value (gdd\_thresh) that depends  
 * on the degree chilling requirements are met, flushing is initiated. This value
 * is calculated as:
 * \f[
 *   gdd\_thresh = 100.0 + GDDSTART \cdot exp( -0.0075 \cdot chilldays)
 * \f]
 * with
 * - GDDSTART: temperature sum value for flushing without considering chilling, ON TOP of the minimum GDD which is assumed to be 100.
 *
 * Chilling days (chilldays) are calculated from average temperature and amplitude @cite linkosalo:2008a :
 * \f[
 *   chilldays = 2.0 \cdot arcos( 1.0 - ( max( 0.0, - \frac {temp\_avg - 0.5 \cdot temp\_ampl} {0.5 \cdot temp\_ampl} )) \cdot \frac {diy}{2.0 \cdot PI}
 * \f]
 * with
 * - temp\_avg: average annual temperature (oC)
 * - temp\_ampl: annual temperature amplitude (oC)
 * - diy: number of days in year (-)
 * - PI: constant (3.1416)
 * 
 * After temperature requirements are met, flushing goes on provided water supply 
 * is above a species-specific treshold value (H2OREF_FLUSHING) and there is no 
 * water deficit in the stem.
 *
 * @todo
 *  - second flush per year
 *  - check if 0.0075 (=PGDD) can really be used as a constant instead of a species-specific parameter
 *  - reset of flushing development after freezing temperatures (see for example Bailey and Harrington 2006 @cite bailey_harrington:2006a)
 */
lerr_t
PhysiologyGrowthPSIM::PSIM_BudBurst()
{
    // Budburst runs only daily
    if ( !lclock()->is_position( TMODE_POST_DAILY))
    {
        return LDNDC_ERR_OK;
    }

    // relative daylength normalized to 12 hours
    double const dayl_norm( meteo::daylength( m_setup->latitude(), this->lclock()->yearday()) / (0.5 * cbm::HR_IN_DAY));
    for ( PlantIterator vt = this->m_veg->begin(); vt != this->m_veg->end(); ++vt)
    {
        MoBiLE_Plant *p = (*vt);

        if ( cbm::flt_greater_zero( nd_leaftemp_vt[p->slot]))
        {
//            p->growing_degree_days += (nd_leaftemp_vt[p->slot] * dayl_norm * droughtstress, e.g. wc/h2oref_a?);
            p->growing_degree_days += (nd_leaftemp_vt[p->slot] * dayl_norm);
        }

         // chilling days are calculated based on Linkosala et al. that are estimated from average temperature and amplitude 
        double const help_1( 0.5 * cl_.annual_temperature_amplitude());
        double const help_2( -(cl_.annual_temperature_average() - help_1) / help_1);
        double const chilldays( 2.0 * acos( 1.0 - ( std::max( 0.0, help_2))) * lclock()->days_in_year() / ( 2.0 * cbm::PI));
        
        // foliage emerges according to temperature requirements considering chilling
        double const gddstart( 100.0 + vt->GDDFOLSTART() * exp( -PGDD * chilldays));
        if ( cbm::flt_greater_equal( p->growing_degree_days, gddstart) && (p->dEmerg == -1))
        {
            if ( cbm::flt_greater_equal( p->f_h2o, vt->H2OREF_FLUSHING()) 
                && cbm::flt_greater_equal(0.0, psidecline_cum_vt[p->slot]))
            {
                p->dEmerg = lclock()->yearday();
            }
        }
        // after temperature requirements are met, flushing goes on if water supply is sufficient
        else if ( p->dEmerg > 0 && cbm::flt_greater_equal( p->f_h2o, vt->H2OREF_FLUSHING()) 
            && cbm::flt_greater_equal(0.0, psidecline_cum_vt[p->slot]))
        {
            days_since_emergence[p->slot] += 1;
        }

        // save development state of last day
        p->dvsFlushOld = p->dvsFlush;
        p->dvsFlush = m_pf.update_dvs_flush( *vt, days_since_emergence[p->slot]);
    }
    
    return  LDNDC_ERR_OK;
}


//----------------------------------------------------------------
/*!
 * @page psim
 * @section psim_senescence Senescence
 * Dry matter senescence loss is calculated assuming fixed tissue longevity separately
 * for foliage, fine roots, and living (sap-)wood. Fine root and sapwood senescence is
 * decreased by a species-specific fraction each day. Senescence of buds is directly
 * related to foliage growth, which is driven by a similar function than foliage senescence
 * (see @ref PSIM_CarAllocation).
 * 
 * Foliage senescence (sFol) is empirically determined (for evergreen species in every
 * age class, na) based on a mortality factor ranging between 1 and 0:
 * \f[
 *   sFol = mFol\_{na} \cdot (1.0 - \frac {1.0 - dvsmort} {1.0 - dvsmort\_{old}} )
 * \f]
 * \f[
 *   dvsmort = exp( -1.0 \cdot \frac { foliage\_{age} - DLEAFSHED)^{ 2 } } { (0.5 \cdot NDMORTA)^ { 2 } \cdot log(2.0) } )
 * \f]
 * with:
 * - mFol:        foliage biomass (kgDW m-2)
 * - foliage_age: age of foliage (age class) (days) 
 * - DLEAFSHED:   parameter describing the day where all foliage (of one age class) is shed (days)
 * - NDMORTA :    parameter indicating the duration of senescence (days)
 * 
 * The nitrogen loss is defined by the nitrogen concentration in the tissue considering
 * a species-specific retranslocation rate that is the same for all tissues. The maximum
 * retranslocation rate is reduced if the overall nitrogen demand is smaller than the
 * maximum amount of nitrogen that could be retranslocated.
 * 
 * Under some circumstances, stress induced senescence (i.e. xylem and foliage loss
 * due to extensive drought stress) is also considered. 
 * This part is under development.
 */
lerr_t
PhysiologyGrowthPSIM::PSIM_Senescence()
{
    for ( PlantIterator vt = this->m_veg->begin(); vt != this->m_veg->end(); ++vt)
    {
        MoBiLE_Plant *p = (*vt);

        // increase foliage age of one day
        if ( lclock()->is_position( TMODE_POST_DAILY))
        {
            for ( size_t na = 0; na < p->nb_ageclasses(); ++na)
            {
                foliage_age_na[p->slot][na] += 1;   
            }
        }

        // plant water potential at air entry point
        p->psi_thresh = -1000; // with such a threshold, no tissue damage will be calculated
        if (vt->DFOL() < 0.9999)
        {
            p->psi_thresh = vt->PSI_REF() * pow((-log(1.0 - vt->DFOL())), (1.0 / vt->PSI_EXP()));
        }

        // xylem vulnerabilty depend on plant water potential
        // TODO: evaluate rLoss for more species. Then remove the security option with Eller
        double rLoss(0.0);
        if (m_photo.stomatalconductance_method == eller_2020 
           && p->psi_mean < p->psi_thresh && p->xylem_resistance > xylem_resistance_old_vt[p->slot])
        {
            rLoss = cbm::flt_greater(p->xylem_resistance, xylem_resistance_old_vt[p->slot]) ?
                    std::max(0.0, std::min(1.0, vt->RPMIN() / xylem_resistance_old_vt[p->slot]) 
                    - vt->RPMIN() / p->xylem_resistance) : 0.0;
        }

        // stress related mass loss of sapwood (kg m-2)
        // - current sapwood area based on sapwood biomass and tree dimension
        double sap_area = p->mSap * (1.0 - vt->UGWDF() - p->f_branch)
                          / (vt->DSAP() * cbm::DM3_IN_M3 * (p->height_max - p->height_at_canopy_start));

        // - mass loss calculated as area loss proportional to stress related losses
        double const sap_loss_stress( (cbm::flt_greater_zero( p->mFol) && cbm::flt_greater_zero( rLoss)) ?
                                      vt->DSAP() * cbm::DM3_IN_M3 * (sap_area * rLoss) *
                                      (p->height_at_canopy_start + (p->rooting_depth / 3.0) +
                                      (p->height_max - p->height_at_canopy_start) / 3.0) :
                                      0.0);

        // age related mass loss of sapwood (kg m-2)
        double const sap_loss_age( p->mSap * vt->TOSAPMAX() * SIM_DAY_FRACTION);

        // the actual sapwood senescence is the smaller of either age and stress related senescence
        mSapLoss_vt[p->slot] = cbm::bound_max( std::max( sap_loss_stress, sap_loss_age), p->mSap);

        // non-branch wood senescence is allocated to corewood compartment
        // NOTE: This is the total senescence of living wood and not only the aboveground part.
        p->sWoodAbove += ( mSapLoss_vt[p->slot] * p->f_branch);

        // update xylem resistance factor
        xylem_resistance_old_vt[p->slot] = p->xylem_resistance;

        // early senescence is not possible before 90% of flushing has been reached.
        double const DVS_FLUSH_NEEDED_FOR_SENESCENCE( 0.9);
        if ( ( cbm::flt_greater( (*p)->H2OREF_SENESCENCE(), p->f_h2o)) &&
             ( cbm::flt_greater( p->dvsFlush, DVS_FLUSH_NEEDED_FOR_SENESCENCE)))
        {
            foliage_age_of_start_senescence[p->slot] = std::min( foliage_age_na[p->slot][p->nb_ageclasses()-1],
                                                                 foliage_age_of_start_senescence[p->slot]);
            additional_season[p->slot] = true;
        }

        p->sFol = 0.0;
        p->dvsMort = 0.0;
        for ( size_t  na = 0;  na < p->nb_ageclasses();  ++na)
        {
            //no senescence during flushing of new foliage
            if ( na == 0)
            {
                if ( !cbm::flt_greater( p->dvsFlush, DVS_FLUSH_NEEDED_FOR_SENESCENCE))
                {
                    continue;
                }
            }

            // senescence development stage of the previous day/timestep
            double const dvs_mort_old( m_pf.get_dvs_mortality_of_age_class( foliage_age_of_start_senescence[p->slot],
                                                                            (*p)->NDMORTA(), foliage_age_na[p->slot][na]-1));

            // actual senescence development stage
            double const dvs_mort( m_pf.get_dvs_mortality_of_age_class( foliage_age_of_start_senescence[p->slot],
                                                                        (*p)->NDMORTA(), foliage_age_na[p->slot][na]));

            // foliage senescence and average mortality state
            if (dvs_mort_old <= 0.0001)
            {
                p->sFol_na[na] = 0.0;
            }
            else if (dvs_mort < 0.99999)
            {
                p->sFol_na[na] = p->mFol_na[na] * (1.0-(1.0-dvs_mort)/(1.0-dvs_mort_old)) * SIM_DAY_FRACTION;
            }
            else if ( cbm::flt_greater_zero( p->mFol_na[na]))
            {
                p->sFol_na[na] = p->mFol_na[na] * SIM_DAY_FRACTION;
            }
            else
            {
                p->sFol_na[na] = 0.0;
            }

            p->sFol += p->sFol_na[na];
            p->dvsMort += m_pf.get_dvs_mortality_contribution_of_age_class( p, na, dvs_mort);
        }

        // check for sufficient supply tissue for foliage and increase senescence if necessary
        if ( cbm::flt_greater_zero( rLoss))
        {
            double const sFol_stress( rLoss * p->mFol);

            // the larger of the two stress factors is taken for further compilation (similar to sapwood)
            if ( cbm::flt_greater( sFol_stress, p->sFol))
            {
                double srest( sFol_stress - p->sFol);
                p->sFol = sFol_stress;

                // reduce foliage starting from oldest age classes
                double sFol_stress_na( 0.0);
                for ( int na = p->nb_ageclasses(); na > 0; --na)
                {
                    if ( cbm::flt_greater_zero( p->mFol_na[na] - p->sFol_na[na]) &&
                         cbm::flt_greater_zero( srest))
                    {
                        sFol_stress_na = cbm::bound_max( p->mFol_na[na] - p->sFol_na[na], srest);
                        p->sFol_na[na] += sFol_stress_na;
                        srest = cbm::bound_min( 0.0, srest - sFol_stress_na);
                    }
                }
            }

        }

        // control
        if ( cbm::flt_greater_equal( p->sFol, p->mFol ) ||
             cbm::flt_greater_equal( p->dvsMort, 1.0 ))
        {
            p->sFol = p->mFol;
            p->dvsMort = 1.0;
        }

        // fine root senescence
        double const mFrt_min( 0.001);
        if ( cbm::flt_greater( p->mFrt, mFrt_min))
        {
            for ( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
            {
                p->sFrt_sl[sl] = cbm::bound_max( p->mFrt * vt->TOFRTBAS() * SIM_DAY_FRACTION * p->fFrt_sl[sl],
                                                 p->mFrt * p->fFrt_sl[sl]);
            }
        }
        else
        {
            for ( size_t sl = 0; sl < sl_.soil_layer_cnt(); ++sl)
            {
                p->sFrt_sl[sl] = 0.0;
            }
        }

        // senescence of buds is currently not considered.
        p->sBud = 0.0;

        // nitrogen retention
        double const nDemOld(( std::max(0.0, ncFolOpt[p->slot] - p->ncFol) * p->mFol
                             + std::max(0.0, ncBudOpt[p->slot] - p->ncBud) * p->mBud
                             + std::max(0.0, vt->NC_FINEROOTS_MAX() - p->ncFrt) * p->mFrt
                             + std::max(0.0, vt->NCSAPOPT() - p->ncSap) * p->mSap) * SIM_DAY_FRACTION);

        // total nitrogen in tissue litterfall before abscision
        double const sFrt( cbm::sum( p->sFrt_sl, sl_.soil_layer_cnt()));
        double const nRetMax(( p->sFol * p->ncFol +
                               mSapLoss_vt[p->slot] * p->ncSap +
                               p->sBud * p->ncBud +
                               sFrt * p->ncFrt) * vt->FRET_N());

        double nRetFol( 0.0);        // amount of nitrogen relocated from foliage litter
        double nRetFrt( 0.0);        // amount of nitrogen relocated from fine root litter
        double nRetSap( 0.0);        // amount of nitrogen relocated from bud litter
        double nRetBud( 0.0);        // amount of nitrogen relocated from sapwood litter
        if ( cbm::flt_greater_zero( nRetMax))
        {
            // fraction of total nitrogen in litter that is relocated into other tissues
            double const fRet( std::min(1.0, nDemOld / nRetMax) * vt->FRET_N());
            nRetFol = fRet * p->ncFol * p->sFol;
            nRetSap = fRet * p->ncSap * mSapLoss_vt[p->slot];
            nRetBud = fRet * p->ncBud * p->sBud;
            nRetFrt = fRet * p->ncFrt * sFrt;
            p->n_retention = nRetFol + nRetFrt + nRetSap + nRetBud;
        }

        // nitrogen losses from the tree into the litter
        p->nLitFol = (p->sFol * p->ncFol - nRetFol);
        p->nLitBud = (p->sBud * p->ncBud - nRetBud);

        n_sap_to_cor_vt[p->slot] += (mSapLoss_vt[p->slot] * p->ncSap - nRetSap) * (1.0 - p->f_branch);
        n_sap_to_litter_vt[p->slot] = (mSapLoss_vt[p->slot] * p->ncSap - nRetSap) * p->f_branch; // only branch litter (after n-retention)!!

        p->nLitWoodAbove += n_sap_to_litter_vt[p->slot];
        for ( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
        {
            p->nLitFrt_sl[sl] += (p->sFrt_sl[sl] * p->ncFrt) - (nRetFrt * p->fFrt_sl[sl]);
        }

        n_fol_to_redistribute_vt[p->slot] += nRetFol;
        n_frt_to_redistribute_vt[p->slot] += nRetFrt;
        n_bud_to_redistribute_vt[p->slot] += nRetBud;
        n_sap_to_redistribute_vt[p->slot] += nRetSap;
    }

    return  LDNDC_ERR_OK;
}


//----------------------------------------------------------------
/*!
 * @page psim
 * @section PSIM_CarAllocation Carbon allocation and growth
 * Carbon provided by photosynthesis (minus growth respiration) is distributed into any compartment
 * that didn't comply with parameterized allometric relations which define the optimum biomass. 
 * Allocation strength is linearly related to the difference between actual and optimum biomass @cite grote:1998a .
 * \f[
 *    dc_{C} = cPool \cdot afc_{C} - res_{C}Old
 * \f]
 * \f[
 *    afc_{C} = \frac {dem_{C} }{cPool }
 * \f]
 * \f[
 *    dem_{C} = \frac { max(0.0, m_{C}Opt - m_{C}) \cdot CCDM }{1.0 - FYIELD} + res_{C}Old
 * \f]
 * with
 * - C:      indicator for plant compartments foliage, fine roots, sapwood, buds (= structural reserves), 
 *           and the free available carbon (nonstructural reserves) that are represented in percent of all other living tissues (fac)
 * - cPool:  available carbon for allocation originating from photosynthesis minus growth respiration
 * - afc:    allocation factor (0-1)
 * - res:    (maintenance) respiration from the compartments in the last timesteps (C m-2)
 * - dem:    carbon demand (C m-2)
 * - m:      biomass (DW m-2)
 * - m_Opt:  optimum biomass of a compartment (DW m-2)
 * - CCDM:   constant transforming biomass to carbon values (carbon content)
 * - FYIELD: species-specific parameter for growth efficiency (growth respiration fraction)
 *
 * The foliage compartment is primarily increased by the depletion of reserves (bud compartment),
 * and therefore has no 'demand' to its own. The transfer and thus the increase of foliage biomass is
 * triggering the demand of buds, sapwood, and fine roots via allometric relations:
 * \f[
 *   m_{Frt}Opt = QRF \cdot mFol;
 * \f]
 * \f[
 *   m_{Sap}Opt = qsfm \cdot mFol;
 * \f]
 * \f[
 *   m_{Bud}Opt = MFOLOPT;
 * \f]
 * \f[
 *   m_{Fac}Opt = sum( m_{C} ) \cdot (FACMAX - fac);
 * \f]
 * with
 * - frt, sap, bud, fac: descriptors for plant compartments finroots, sapwood, buds (= constitutive reserves)
 * - qsfm:               desired relation between sapwood and foliage biomass
 * - QRF:                species-specific parameter describing the desired relation between fine roots and foliage biomass
 * - MFOLOPT:            species-specific parameter for foliage biomass per area (kgDW m-2ground)
 * - FACMAX:             species-specific parameter for maximum percentage of free available carbon
 *
 * qsfm is derived from a desired relation between sapwood area and foliage area (Huber value, QSFA)
 * and the dimension of the tree (assuming species-specific taper functions and fixed fractions for branchiness
 * and coarse roots.
 * 
 * If the supply is larger than all demands, the surplus is distributed between buds, foliage,
 * and fine roots in case of herbaceous plants, wood and buds in case of determined growth (FREEGROWTH = false)
 * or into foliage growth if species are growing leaves continously (FREEGROWTH = true).
 * 
 * Free available carbon can be depleted only by respiration demands higher than supply rates.
 *
 * Carbon exudation is assumed to be species specific fraction of total fine root growth
 * (see: @ref PSIM_SoilCarbonRelease).
 * 
 */
lerr_t
PhysiologyGrowthPSIM::PSIM_CarAllocation()
{
    double mFrtSum( 0.0);
    for ( PlantIterator vt = this->m_veg->begin(); vt != this->m_veg->end(); ++vt)
    {
        mFrtSum += (*vt)->mFrt;
    }

    for ( PlantIterator vt = this->m_veg->begin(); vt != this->m_veg->end(); ++vt)
    {
        MoBiLE_Plant * p = *vt;

        // amount of dry matter that is transfered from buds into foliage
        if ( p->dvsFlush > p->dvsFlushOld )
        {
            mBudLoss_vt[p->slot] = std::min( p->mBud, p->mBudStart * (p->dvsFlush - p->dvsFlushOld)) * SIM_DAY_FRACTION;
        }
        else
        {
            mBudLoss_vt[p->slot] = 0.0;
        }

        // living biomass that contains general reserve tissue (kgDM)
        double const mLiv( p->mBud + p->mFrt + p->mSap);

        // actual amount of free available carbon (kgC)
        double const fac( mLiv * p->f_fac);

        // maximum amount of free available carbon (kgC)
        double const facMax( mLiv * vt->FFACMAX());

        // available carbon for growth
        double const cPool( carbonuptake_vt[p->slot] - rTraOld_vt[p->slot]);

        // optimum biomasses
        // optimum foliage biomass (equal to actual biomass)
        double mFolOpt = p->mFol;

        // optimum fine root biomass (depends on foliage biomass but is restricted by soil temperature)
        double temp( temp_sl[0]);
        if ( mFrtSum > 0.0)
        {
            temp = 0.0;
            for ( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
            {
                temp += temp_sl[sl] * ph_.mfrt_sl( m_veg, sl);
            }
            temp *= 1.0 / mFrtSum;
        }

        double mFrtOpt = p->mFrt;
        if ( temp >= TGLIM)
        {
            mFrtOpt = vt->QRF() * p->mFol;

            //increase optimum fine root biomass when nitrogen uptake is insufficient
            if ( ((ncBudOpt[p->slot] + ncFolOpt[p->slot]) > (p->ncBud + p->ncFol))
                && cbm::flt_greater_zero( (p->ncBud + p->ncFol)))
            {
                mFrtOpt *= (ncBudOpt[p->slot] + ncFolOpt[p->slot]) / (p->ncBud + p->ncFol);
            }
        }

        // current sapwood area based on sapwood biomass and tree dimension
        double sap_area = p->mSap * (1.0 - vt->UGWDF() - p->f_branch)
                          / (vt->DSAP() * cbm::DM3_IN_M3 * (p->height_max - p->height_at_canopy_start));

        // sapwood area demand defined by maximum leaf area to be supplied
        /* NOTE: at the first day there is no lai_pot and thus no potential sap area (??) */
        double d_saparea_pot(p->lai_pot * p->qsfa);

        // option for restricting allocation into buds when sapwood is not sufficient to supply new leaves
        if (d_saparea_pot > 0.0)
        {
            qsaparea_vt[p->slot] = std::min(1.0, sap_area / d_saparea_pot);
        }
        else
        {
            qsaparea_vt[p->slot] = 1.0;
        }

        // optimum sapwood biomass (kgDM)
        // - initialize mSapOpt_vt (e.g. after planting)
        if ( cbm::flt_equal_zero( mSapOpt_vt[p->slot]))
        {
            mSapOpt_vt[p->slot] = p->qsfm * std::max(p->mFol, p->mBud);
        }
        // - define mSapOpt demand according to leaf biomass
        else if ( cbm::flt_greater_zero( p->mFol))
        {
            double x = d_saparea_pot * vt->DSAP() * cbm::DM3_IN_M3  
                       * (p->height_max - p->height_at_canopy_start) / (1.0 - vt->UGWDF() - p->f_branch);
            double z = p->qsfm * p->mFol;
            mSapOpt_vt[p->slot] = std::min(x, z);
        }      
        mSapOpt_vt[p->slot] *= qsaparea_vt[p->slot];

        // optimum bud (fruit) biomass
        double mBudOpt(0.0);
        if ( ( !cbm::flt_greater_zero( p->qsfm)) &&
             ( !(*p)->IS_WOOD()))
        {
            // herbaceous
            mBudOpt = m_pf.get_optimum_foliage_biomass_grass( *vt);
        }
        else if ( cbm::flt_greater_zero( p->mFolMax) && (*p)->IS_WOOD())
        {
            // woody
            // for evergreen species bud demand is the difference between potential and actual foliage mass 
            if (p->nb_ageclasses() > 1.0)
            {
                mBudOpt = std::max(0.0, (m_pf.get_optimum_foliage_biomass_trees(*vt) - p->mFol));
            }
            // for deciduous species bud demand is equal to potential foliage mass
            else
            {
                mBudOpt = (m_pf.get_optimum_foliage_biomass_trees(*vt));
            }

            // flushing demand is reduced by conductance limitation
            mBudOpt *= std::min(1.0, qsaparea_vt[p->slot] / 0.9);
        }

        // growth demand potential(defined as difference between optimum/maximum and actual biomass)
        double demFol = std::max(0.0, mFolOpt - p->mFol) * cbm::CCDM * SIM_DAY_FRACTION / (1.0 - vt->FYIELD()) + rFolOld_vt[p->slot];
        double demFrt = std::max(0.0, mFrtOpt - p->mFrt) * cbm::CCDM * SIM_DAY_FRACTION / (1.0 - vt->FYIELD()) + rFrtOld_vt[p->slot];
        double demSap = std::max(0.0, mSapOpt_vt[p->slot] - p->mSap) * cbm::CCDM * SIM_DAY_FRACTION / (1.0 - vt->FYIELD()) + rSapOld_vt[p->slot];
        double demBud = std::max(0.0, mBudOpt - p->mBud) * cbm::CCDM * SIM_DAY_FRACTION / (1.0 - vt->FYIELD()) + rBudOld_vt[p->slot];
        double demFac = std::max(0.0, facMax - fac) * cbm::CCDM * SIM_DAY_FRACTION;
        double demTot = demFol + demFrt + demSap + demBud + demFac;

        // allocation factors
        double afcFol( 0.0);      // fraction of growth going into foliage
        double afcFrt( 0.0);      // fraction of growth going into fine roots
        double afcSap( 0.0);      // fraction of growth going into sapwood
        double afcBud( 0.0);      // fraction of growth going into buds
        double afcFac( 0.0);      // fraction of growth going into free available carbohydrates
        double rest( 1.0);        // excess carbon not required to fulfill allocation demands (kgDM)

        // demand can be fully supplied
        if ( (demTot <= cPool) && (cPool > 0.0))
        {
            afcFol = demFol / cPool;
            afcFrt = demFrt / cPool;
            afcSap = demSap / cPool;
            afcBud = demBud / cPool;
            afcFac = demFac / cPool;
            rest   = 1.0 - afcFol - afcFrt - afcSap - afcBud - afcFac;
        }
        // demand can not be fully supplied
        else if ( (demTot > cPool) && cbm::flt_greater_zero( cPool))
        {
            // the demand potential of buds is increased in species with ongoing growth,
            // reflecting a higher priority than indicated by their relatively small biomass
            double const demBud_increase = std::max(1.0, (p->mBud > 0.0) ? mBudOpt / p->mBud : 1.0);
            demTot = (demTot - demBud + (demBud_increase * demBud));
            demBud = demBud_increase * demBud;

            afcFol = demFol / demTot;
            afcFrt = demFrt / demTot;
            afcSap = demSap / demTot;
            afcBud = demBud / demTot;
            afcFac = demFac / demTot;

            // arbitrary security setup to prevent unrealistically high root allocation (probably not needed)
            rest   = 0.0;
            if (afcFrt > 0.5)
            {
                rest   = afcFrt - 0.5;
                afcFrt = 0.5;
            }
        }

        if ((rest > 0.0) && ( p->qsfm <= 0.0))
        {
            // for herbaceous species (grass and herbs)
            afcFrt += (rest * vt->QRF() / (2.0 + vt->QRF()));
            afcFol += (rest * 1.0 / (2.0 + vt->QRF()));
            afcBud += (rest * 1.0 / (2.0 + vt->QRF()));
        }
        else if (rest > 0.0)
        {
            // optimum foliage is oriented to the current foliage demand
            mFolOpt = ( (*p)->IS_WOOD() ? m_pf.get_optimum_foliage_biomass_trees( p) :
                                          m_pf.get_optimum_foliage_biomass_grass( p));

            // optimum bud biomass is oriented to the maximum foliage achievable at the site to develop towards closed and mature conditions
            if (p->mFolMax > 0.0)
            {
                // state of bud reserves (maximum foliage needed to prevent ever increasing demand after flushing)
                mBudOpt = mFolOpt * std::min(1.0, qsaparea_vt[p->slot] / 0.9);

                // decrease of bud demand according to the relative contribution of the first age class 
                if ( p->nb_ageclasses() > 1.0)
                {
                    mBudOpt -= mFol_emergence;
                }
            }
            else
            {
                mBudOpt = 0.0;
            }

            double qsfm_corr = p->qsfm * qsaparea_vt[p->slot];
            if (( vt->FREEGROWTH() == false) && ( p->mBud < mBudOpt))
            {
                // determined growth
                afcSap += (rest * qsfm_corr / (1.0 + qsfm_corr));
                afcBud += (rest * (1.0 - qsfm_corr / (1.0 + qsfm_corr)));
            }
            else if ( vt->FREEGROWTH() && ( p->mFol < mFolOpt))
            {
                // free growth
                afcSap += (rest * qsfm_corr / (1.0 + qsfm_corr));
                afcFol += (rest * (1.0 - qsfm_corr / (1.0 + qsfm_corr)));
            }
            else
            {
                // overshooting carbon goes into wood growth
                afcSap += rest;
            }
        }

        // increase or decrease of carbon
        p->dcFol = cPool * afcFol - rFolOld_vt[p->slot];
        p->dcFrt = cPool * afcFrt - rFrtOld_vt[p->slot];
        p->dcSap = cPool * afcSap - rSapOld_vt[p->slot];
        p->dcBud = cPool * afcBud - rBudOld_vt[p->slot];
        p->dcFac = cPool * afcFac;

        // all negative increase is transfered to the free available carbon pool
        if (p->dcFol < 1.0e-8)
        {
            p->dcFac += p->dcFol;
            p->dcFol  = 0.0;
        }
        if (p->dcFrt < 1.0e-8)
        {
            p->dcFac += p->dcFrt;
            p->dcFrt  = 0.0;
        }
        if (p->dcSap < 1.0e-8)
        {
            p->dcFac += p->dcSap;
            p->dcSap  = 0.0;
        }
        if (p->dcBud < 1.0e-8)
        {
            p->dcFac += p->dcBud;
            p->dcBud  = 0.0;
        }

        // consideration of growth respiration
        p->dcFol *= (1.0 - vt->FYIELD());
        p->dcFrt *= (1.0 - vt->FYIELD());
        p->dcSap *= (1.0 - vt->FYIELD());
        p->dcBud *= (1.0 - vt->FYIELD());
    }
    
    return  LDNDC_ERR_OK;
}


lerr_t
PhysiologyGrowthPSIM::PSIM_SoilCarbonRelease()
{
    for ( PlantIterator vt = this->m_veg->begin(); vt != this->m_veg->end(); ++vt)
    {
        MoBiLE_Plant *p = (*vt);

        // exudation is determined from remaining root growth
        p->exsuLoss = (p->dcFrt * vt->PEXS());

        // root growth is decreased
        p->dcFrt -= p->exsuLoss;

        if (p->dcFrt < 0.0)
        {
            p->dcFrt = 0.0;
        }
    }
    
    return  LDNDC_ERR_OK;
}


//----------------------------------------------------------------
/*!
 * @page psim
 * @section PSIM_NitAllocation Nitrogen allocation
 * The nitrogen provided from uptake and retranslocation is distributed according to biomass growth
 * and optimum tissue concentrations @cite grote:1998a.
 *
 */
lerr_t
PhysiologyGrowthPSIM::PSIM_NitAllocation()
{
    for ( PlantIterator vt = this->m_veg->begin(); vt != this->m_veg->end(); ++vt)
    {
        MoBiLE_Plant *p = (*vt);

        // amount of nitrogen that is transferred from buds into foliage
        double const nBudLoss = mBudLoss_vt[p->slot] * p->ncBud;
        n_bud_to_fol_vt[p->slot] += nBudLoss;

        // nitrogen available for allocation includes:
        //  - previous time step (uptxOld)
        //  - atmosphere (uptN2Old_vt)
        //  - retranslocation of senescence (nRet_vt)
        double const nPool( uptDONOld_vt[p->slot] + uptNH4Old_vt[p->slot] + uptNO3Old_vt[p->slot]
                          + uptNOxOld_vt[p->slot] + uptNH3Old_vt[p->slot]
                          + uptN2Old_vt[p->slot]
                          + p->n_retention);

        // optimum nitrogen contents (with updated biomass values)
        double nFolOpt( m_pf.optimum_nitrogen_content_foliage( p)); // optimum foliage nitrogen content
        double nBudOpt( m_pf.optimum_nitrogen_content_buds( p));    // optimum Bud nitrogen contents

        double const nFrtOpt = vt->NC_FINEROOTS_MAX() * (p->mFrt + p->dcFrt / cbm::CCDM);
        double const nSapOpt = vt->NCSAPOPT() * (p->mSap + p->dcSap / cbm::CCDM);

        // nitrogen demand
        double const demFol = std::max(0.0, nFolOpt - p->n_fol()) * SIM_DAY_FRACTION;
        double const demSap = std::max(0.0, nSapOpt - p->n_sap()) * SIM_DAY_FRACTION;
        double  demBud = std::max(0.0, nBudOpt - p->n_bud()) * SIM_DAY_FRACTION;
        double  demFrt = std::max(0.0, nFrtOpt - p->n_frt()) * SIM_DAY_FRACTION;
        double const demTot = demFol + demFrt + demSap + demBud;

        double  afnFol( 0.0);    // fraction of distributed nitrogen going into foliage
        double  afnFrt( 0.0);    // fraction of distributed nitrogen going into fine roots
        double  afnSap( 0.0);    // fraction of distributed nitrogen going into sapwood
        double  afnBud( 0.0);    // fraction of distributed nitrogen going into buds

        if ( cbm::flt_greater_zero( nPool))
        {
            // demand can be fully supplied
            if ( demTot <= nPool)
            {
                afnFol = demFol / nPool;
                afnFrt = demFrt / nPool;
                afnSap = demSap / nPool;
                afnBud = demBud / nPool;
            }
            // demand can not be fully supplied
            else
            {
                // The demand potential of buds and fine roots is increased in species with ongoing growth,
                // reflecting a higher priority than indicated by their relatively small biomass (buds)
                // or position relative to nutrient uptake (roots).
                double const virDem( demTot - demBud - demFrt +
                                     std::max(1.0, vt->RBUDDEM()) * demBud +
                                     std::max(1.0, vt->RBUDDEM()) * demFrt);
                demBud *= std::max(1.0, vt->RBUDDEM());
                demFrt *= std::max(1.0, vt->RBUDDEM());
                afnFol = demFol / virDem;
                afnFrt = demFrt / virDem;
                afnSap = demSap / virDem;
                afnBud = demBud / virDem;
            }
        }

        // excess nitrogen is distributed beween compartments
        double const rest( 1.0 - afnFol - afnFrt - afnSap - afnBud);
        if ( cbm::flt_greater_zero( rest))
        {
            double const n_total(  p->mFol * ncFolOpt[p->slot] + p->mBud * ncBudOpt[p->slot]
                                 + p->mFrt * vt->NC_FINEROOTS_MAX() + p->mSap * vt->NCSAPOPT());

            if ( cbm::flt_greater_zero( n_total))
            {
                afnFol += (rest * p->mFol * ncFolOpt[p->slot] / n_total);
                afnBud += (rest * p->mBud * ncBudOpt[p->slot] / n_total);
                afnFrt += (rest * p->mFrt * vt->NC_FINEROOTS_MAX()  / n_total);
                afnSap += (rest * p->mSap * vt->NCSAPOPT()  / n_total);
            }
        }

        // new nitrogen contents
        double const nFol( nFol_vt[p->slot] + nPool * afnFol);
        double const nFrt( nFrt_vt[p->slot] + nPool * afnFrt);
        double const nSap( nSap_vt[p->slot] + nPool * afnSap);
        double const nBud( nBud_vt[p->slot] + nPool * afnBud);
        n_fol_gain_vt[p->slot] += nPool * afnFol;
        n_frt_gain_vt[p->slot] += nPool * afnFrt;
        n_sap_gain_vt[p->slot] += nPool * afnSap;
        n_bud_gain_vt[p->slot] += nPool * afnBud;

        // new nitrogen concentrations
        if (p->mFol > 0.0) p->ncFol = std::max(0.0,nFol / (p->mFol + p->dcFol / cbm::CCDM)); else p->ncFol = 0.0;
        if (p->mFrt > 0.0) p->ncFrt = std::max(0.0,nFrt / (p->mFrt + p->dcFrt / cbm::CCDM)); else p->ncFrt = 0.0;
        if (p->mSap > 0.0) p->ncSap = std::max(0.0,nSap / (p->mSap + p->dcSap / cbm::CCDM)); else p->ncSap = 0.0;
        if (p->mBud > 0.0) p->ncBud = std::max(0.0,nBud / (p->mBud + p->dcBud / cbm::CCDM)); else p->ncBud = 0.0;

        // before-hand update of nitrogen concentration to prevent 0 concentrations after flushing
        if (nBudLoss > 0.0)
        {
            p->ncFol = (p->n_fol() + nBudLoss) / (p->mFol + mBudLoss_vt[p->slot]);
        }

        // The following changes exclude N allocation from the stem, which was not reasonable
        // additionally, nitrogen may be allocated to the core wood in case of excess nitrogen elsewhere
        if ( cbm::flt_greater_zero( p->mCor))
        {
            // - from sapwood
            if (p->mSap > 0.0 && p->ncSap > vt->NCSAPOPT())
            {
                double const transfer = std::min(p->mCor * (vt->NCSAPOPT() - p->ncCor),
                                       p->mSap * (p->ncSap - vt->NCSAPOPT())) * SIM_DAY_FRACTION;
                p->ncCor = (p->n_cor() + transfer) / p->mCor;
                p->ncSap = (p->n_sap() - transfer) / p->mSap;
                n_sap_to_cor_vt[p->slot] += transfer;
            }
            // - from buds
            if (p->mBud > 0.0 && p->ncBud > ncBudOpt[p->slot])
            {
                double const transfer = std::min(p->mCor * (vt->NCSAPOPT() - p->ncCor),
                                       p->mBud * (p->ncBud - ncBudOpt[p->slot])) * SIM_DAY_FRACTION;
                p->ncCor = (p->n_cor() + transfer) / p->mCor;
                p->ncBud = (p->n_bud() - transfer) / p->mBud;
                n_bud_to_cor_vt[p->slot] += transfer;
            }
            // - from foliage
            if (p->mFol > 0.0 && p->ncFol > ncFolOpt[p->slot])
            {
                double const transfer = std::min(p->mCor * (vt->NCSAPOPT() - p->ncCor),
                                       p->mFol * (p->ncFol - ncFolOpt[p->slot])) * SIM_DAY_FRACTION;
                p->ncCor = (p->n_cor() + transfer) / p->mCor;
                p->ncFol = (p->n_fol() - transfer) / p->mFol;
                n_fol_to_cor_vt[p->slot] += transfer;
            }
            // - from fine roots
            if (p->mFrt > 0.0 && p->ncFrt > vt->NC_FINEROOTS_MAX())
            {
                double const transfer = std::min(p->mCor * (vt->NCSAPOPT() - p->ncCor),
                                       p->mFrt * (p->ncFrt - vt->NC_FINEROOTS_MAX())) * SIM_DAY_FRACTION;
                p->ncCor = (p->n_cor() + transfer) / p->mCor;
                p->ncFrt = (p->n_frt() - transfer) / p->mFrt;
                n_frt_to_cor_vt[p->slot] += transfer;
            }
        }

        // remaining nitrogen demand
        nDem_vt[p->slot] = ( std::max(0.0, ncFolOpt[p->slot] - p->ncFol) * p->mFol
                           + std::max(0.0, ncBudOpt[p->slot] - p->ncBud) * p->mBud
                           + std::max(0.0, vt->NC_FINEROOTS_MAX() - p->ncFrt) * p->mFrt
                           + std::max(0.0, vt->NCSAPOPT() - p->ncSap) * p->mSap) * SIM_DAY_FRACTION;
    }
    
    return  LDNDC_ERR_OK;
}


//----------------------------------------------------------------
/*!
 * @page psim
 * @section PSIM_Respiration Respiration
 * Respiration consists of growth (rGro) and maintenance respiration, with the latter split up into respiration
 * that is related to nutrient uptake and transport (rTra), and into remaining (residual) respiration (rRes). 
 * I Growth respiration is a fraction of growth is assumed to apply equally for every tissue
 * \f[
 *   rGro = sum( dc_{C} \cdot (1.0 - FYIELD) )
 * \f]
 * with
 * - C:      indicator for plant compartments foliage, fine roots, sapwood, buds (= structural reserves)
 * - FYIELD: species-specific parameter expressing fraction of growth parameters
 *
 * II Transport respiration summarizes carbon costs due to uptake of nitrogen and other nutrients (rUpt),
 *    phloem transport of carbon into the roots (rPhl), and the reduction of oxygenized nitrogen compounds (rNit).
 *    Uptake of nitrogen compounds is explicitly modeleled, while additional costs from uptake and incorporation
 *    of other nutrients are estimated from biomass growth, assuming a that all requirements are met. 
 * \f[
     rTra = rUpt + rPhl + rNit
 * \f]
 * \f[
     rUpt = PAMM \cdot (uptNH4 + uptNH3) + PNIT \cdot (uptNO3 + uptNOx) + PMIN \cdot growth \cdot \frac{FMIN}{CCDM} 
 * \f] 
 * \f[
     rPhl = PPHLOE \cdot (dcFrt + dcSap + exsuLoss)
 * \f]
 * \f[
     rNit = PREDFRT \cdot uptNO3 \cdot FRFRT + PREDSAP \cdot uptNO3 \cdot (1.0 - FRFRT) + PREDSAP \cdot uptNOx
 * \f]
 * with
 * - uptNH4, uptNH3:   taken up reduced nitrogen from the air (NH4) and the soil (NH3)
 * - uptNOx, uptNO3:   taken up oxygenized nitrogen from the air (NOx) and the soil (NO3)
 * - dcFrt, dcSap:     carbon increase of fineroots and sapwood (C)
 * - exsuLoss:         carbon loss by exudation out of fine roots into the soil (C)
 * - PAMM, PNIT, PMIN: parameters for uptake cost of ammonia, nitrate, and other mineral components (0.17, 0.34, 0.06)
 * - FMIN:             parameter for fraction of mineral components other than nitrogen to total plant biomass (0.05)
 * - CCDM:             constant indicating the carbon content of plant dry matter (0.45)
 * - PPHLOE:           parameter for phloem loading costs to support growth in sapwood, fine roots as well a exudates (0.06)
 * - PREDFRT, PREDSAP: parameters for reduction costs of taken up oxygenized nitrogen forms in fine roots and sapwood, respectively (1.72, 0.855)
 * - FRFRT:            parameter indicating the fraction of nitrate that is reduced in the roots (0.5)
 * 
 * III Residual respiration according to temperature and nitrogen content @cite cannell_thornley:2000a
 * \f[
     rRes = sum( km \cdot fsub \cdot n_{C} )
 * \f]
 * \f[
 *   km = KM20 \cdot (temp_{C} - TRMIN) ^ { 2 } \cdot (TRMAX - temp_{C}) \cdot \frac{1.0}{ ( TROPT - TRMIN) ^ { 2 } \cdot ( TRMAX - TROPT) }
 * \f]
 * \f[
 *    fsub = \frac {ffac \cdot CCDM}{KMMM + ffac \cdot CCDM}
 * \f]
 * with
 * - C:      indicator for plant compartments foliage, fine roots, sapwood, buds (= structural reserves)
 * - n:      nitrogen within a plant compartment
 * - fsub:   reduction factor due to depleted non-structural reserves
 * - km:     temperature modification factor
 * - temp:   tissue temperature
 * - ffac:   degree to which the virtual pool of non-structural carbon reserves is filled up (0-1)
 * - KMMM:   Michaelis-Menton constant for the importance of relative available non-structural carbon on respiration
 *
 */
lerr_t
PhysiologyGrowthPSIM::PSIM_Respiration()
{
    double  km( 0.0);      // maintanence coefficient; set zero at the beginning of timestep

    for ( PlantIterator vt = this->m_veg->begin(); vt != this->m_veg->end(); ++vt)
    {
        MoBiLE_Plant *p = (*vt);

        p->rFol = 0.0;
        p->rFrt = 0.0;
        p->rSap = 0.0;
        p->rBud = 0.0;

        // total carbon allocated for structural compartment growthm
        double const growth( p->dcFol + p->dcFrt + p->dcSap + p->dcBud + exsuLossOld_vt[p->slot]);

        // respiration for mineral- and nitrogen uptake, nitrate reduction, phloem carbon transport
        // TODO: variable for DON and NH3(soil) should be added, currently not in MoBiLE available
        double const rUpt( PAMM * std::max(0.0,uptNH4Old_vt[p->slot] + uptNH3Old_vt[p->slot])
                          + PNIT * std::max(0.0,uptNO3Old_vt[p->slot] + uptNOxOld_vt[p->slot])
                          + PMIN * growth * FMIN / cbm::CCDM);

        // respiration costs for nitrogen reduction
        double const rNit( PREDFRT * uptNO3Old_vt[p->slot] * FRFRT + PREDSAP * uptNO3Old_vt[p->slot] * (1.0 - FRFRT)
                          + PREDSAP * uptNOxOld_vt[p->slot]);

        double const rPhl( PPHLOE * (p->dcFrt + p->dcSap + exsuLossOld_vt[p->slot]));

        p->rTra = rUpt + rPhl + rNit;

        // growth respiration (note that growth is already reduced by (1-FYIELD). This needs to be compensated first if growth respiration should be derived)
        p->rGro = growth * vt->FYIELD() / (1.0 - vt->FYIELD());

        // sapwood below-ground fraction
        double const sap_wood_frac( sap_wood_fraction( p->lai_max, p->qsfa, p->height_at_canopy_start,
                                                       p->height_max, p->rooting_depth));
        double const dc_sum(  std::max( 0.0, p->dcFol)
                            + std::max( 0.0, p->dcFrt)
                            + std::max( 0.0, p->dcSap)
                            + std::max( 0.0, p->dcBud));

        double const dc_below( std::max( 0.0, p->dcFrt) + std::max( 0.0, p->dcSap * sap_wood_frac));
        p->rGroBelow = ((dc_sum > 0.0) ? p->rGro * dc_below / dc_sum : 0.0);


        // residual respiration

        // - carbon availability restriction factor
        double const fsub( p->f_fac * cbm::CCDM / (KMMM + p->f_fac * cbm::CCDM));

        // - foliage (only nighttime)
        static double const  zft( 1.0 / (( TROPT - TRMIN) * ( TROPT - TRMIN) * ( TRMAX - TROPT)));
        if (p->mFol > 0.0)
        {
            for ( size_t  fl = 0;  fl < fl_cnt_;  ++fl)
            {
                // n content of the particular layer (kg)
                double const nlay( p->ncFol * p->m_fol_fl(fl));

                // for subdaily simulation time steps during "night"
                if ( subdaily_timemode() && ( mc_.shortwaveradiation_in < 0.01))
                {
                    if (mc_.temp_fl[fl] < TRMIN)
                    {
                        km = 0.0;
                    }
                    else if (mc_.temp_fl[fl] > TRMAX)
                    {
                        km = vt->KM20();
                    }
                    else
                    {
                        km = vt->KM20() * cbm::sqr(mc_.temp_fl[fl] - TRMIN) * (TRMAX - mc_.temp_fl[fl]) * zft;
                    }
                    p->rFol += (km * fsub * nlay * SIM_DAY_FRACTION);
                }

                // for subdaily simulation time steps during "day"
                else if ( subdaily_timemode() && ( mc_.shortwaveradiation_in >= 0.01))
                {
                    p->rFol += 0.0;
                }

                // for daily simulation time steps
                else if ( !subdaily_timemode())
                {
                    double  tnight( PTW * mc_.nd_tMax_fl[fl] + (1.0 - PTW) * mc_.nd_tMin_fl[fl]);
                    if (tnight < TRMIN)
                    {
                        km = 0.0;
                    }
                    else if (tnight > TRMAX)
                    {
                        km = vt->KM20();
                    }
                    else
                    {
                        km = vt->KM20() * cbm::sqr(tnight - TRMIN) * (TRMAX - tnight) * zft;
                    }
                    double const daylength_hours( meteo::daylength( m_setup->latitude(), this->lclock()->yearday()));
                    p->rFol += (km * fsub * nlay * ( 1.0 - daylength_hours / cbm::HR_IN_DAY));
                }
            }
        }


        // - fine roots
        if (p->mFrt > 0.0)
        {
            for ( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
            {
                if (( temp_sl[sl] > TRMIN) && ( temp_sl[sl] < TRMAX))
                {
                    km = vt->KM20() * cbm::sqr(temp_sl[sl] - TRMIN) * (TRMAX - temp_sl[sl]) * zft;
                }
                else
                {
                    km = 0.0;
                }

                // n content of the particular layer (kg)
                double const nlay( p->n_frt() * p->fFrt_sl[sl]);
                p->rFrt += ( km * fsub * nlay) * SIM_DAY_FRACTION;
            }
        }

        // - sapwood and buds
        if (( ts_leaftemp_vt[p->slot] > TRMIN) && ( ts_leaftemp_vt[p->slot] < TRMAX))
        {
            km = vt->KM20() * cbm::sqr( ts_leaftemp_vt[p->slot] - TRMIN) * (TRMAX - ts_leaftemp_vt[p->slot]) * zft;
        }
        else
        {
            km = 0.0;
        }

        // Reduction factor accounting to activity loss with age
        double  fsapage( pow( this->lclock()->days_in_year() * vt->TOSAPMAX(), vt->FAGE()));
        if (p->mSap > 0.0)
        {
            p->rSap = km * fsapage * fsub * p->n_sap() * SIM_DAY_FRACTION;
            p->rSapBelow = (p->rSap * sap_wood_frac);
        }

        if (p->mBud > 0.0)
        {
            p->rBud = km * fsub * p->n_bud() * SIM_DAY_FRACTION;
        }

        // - total residual respiration
        p->rRes = p->rFol + p->rFrt + p->rSap + p->rBud;
    }

    return  LDNDC_ERR_OK;
}


//----------------------------------------------------------------
/*!
 * @page psim
 * @section PSIM_NitrogenUptake Nitrogen uptake
 * Uptake of ammonia and nitrate from the soil and canopy is calculated
 * according to supply, soil water availability and fine root density.
 * Total nitrogen uptake is limited to the current whole plant demand.
 * Nitrogen uptake/emission from the canopy depends on NOx air concentration.
 *
 * @note Uptake through the canopy may be small but could be significant
 * relative to NOx emissions from the soil. Parameterization should be species
 * specific but necessary information is yet not available. Instead, rough 
 * estimates from a tropical landscape are used @cite sparks:2001a.
 *
 * @note Nitrogen fixation is considered similar to agricultural plants,
 * using a species-specific parameter (INI_N_FIX) that indicates that a 
 * fraction of nitrogen demand is covered by fixation at no cost 
 * (see PSIM_NitrogenFixation).
 * 
*/
lerr_t
PhysiologyGrowthPSIM::PSIM_NitrogenUptake()
{
    // uptake of NOx and NH3 from the canopy (not demand restricted)
    nh3_uptake_fl = 0.0;
    nox_uptake_fl = 0.0;

    for ( PlantIterator vt = this->m_veg->begin(); vt != this->m_veg->end(); ++vt)
    {
        MoBiLE_Plant *p = (*vt);

        p->nox_uptake  = 0.0;
        uptNH3_vt[p->slot]  = 0.0;
        uptNWet_vt[p->slot] = 0.0;

        double const lai_vt( p->lai());
        if ( cbm::flt_greater_zero( lai_vt))
        {
            double relativeconductance( 0.0);
            for ( size_t  fl = 0;  fl < fl_cnt_;  ++fl)
            {
                relativeconductance += p->d_relativeconductance_fl[fl] * p->lai_fl[fl] / lai_vt;
            }

            // nitrogen uptake reduction due to limited demand
            double  fNit = cbm::bound( 0.1, p->ncFol / ncFolOpt[p->slot], 1.0);
            // max. nitrogen emission from the canopy
            double  noxEMax = EPOTNOX * relativeconductance / fNit;
            // max. ammoniak uptake from the canopy
            double  nh3UMax = UPOTNH3 * relativeconductance;

            for ( size_t  fl = 0;  fl < fl_cnt_;  ++fl)
            {
                // - unit conversion from ppm to ppb
                double const cnox_ppb( cbm::bound_min( 0.0, cNOx_fl[fl] * 1000.0));
                double const cnh3_ppb( cbm::bound_min( 0.0, nh3_concentration_fl[fl] * 1000.0));

                // - NOx uptake
                double const rel_nox( ldndc::meteo::relativeImpact( cnox_ppb, CSATNOX));
                double const relc( ldndc::meteo::relativeImpact( CCOMNOX, CSATNOX));

                // - maximum uptake of nitrous gases
                double const uptNOx( cbm::bound_min(0.0, (cnox_ppb < CSATNOX) ?
                                                           (rel_nox / relc - 1.0) * noxEMax :
                                                           (1.0 / relc - 1.0) * noxEMax));

                nox_uptake_fl[fl] = uptNOx * p->lai_fl[fl] * cbm::SEC_IN_DAY * SIM_DAY_FRACTION * cbm::MN * cbm::KG_IN_G * cbm::MOL_IN_PMOL;
                p->nox_uptake += nox_uptake_fl[fl];

                // - NH3 uptake
                double const rel_nh3( ldndc::meteo::relativeImpact( cnh3_ppb, CSATNH3));

                double const uptNH3( std::min( 1.0, rel_nh3) * nh3UMax);
                nh3_uptake_fl[fl]  = uptNH3 * p->lai_fl[fl] * cbm::SEC_IN_DAY * SIM_DAY_FRACTION * cbm::MN * cbm::KG_IN_G * cbm::MOL_IN_PMOL;
                uptNH3_vt[p->slot] += nh3_uptake_fl[fl];
            }
        }
    }

    // potential nitrogen uptake as NHx, NOy from soil and leaf water (<= N-Demand)
    for ( PlantIterator vt = this->m_veg->begin(); vt != this->m_veg->end(); ++vt)
    {
        MoBiLE_Plant * p = *vt;

        double uptNMax( 0.0);        // plant specific potential total soil nitrogen uptake.    [kg N m-2]

        uptNH4Max_vt[p->slot]  = 0.0;    // plant specific potential soil nh4 uptake.            [kg N m-2]
        uptNO3Max_vt[p->slot]  = 0.0;    // plant specific potential soil no3 nitrogen uptake.    [kg N m-2]
        uptDONMax_vt[p->slot]  = 0.0;    // plant specific potential soil don nitrogen uptake.    [kg N m-2]
        uptNWetMax_vt[p->slot] = 0.0;
        uptNTot_vt[p->slot]    = 0.0;

        // no leaves, no transpiration, no nitrogen uptake - could be questioned, however
        double const lai_vt( p->lai());
        if ( cbm::flt_greater_zero( lai_vt))
        {
            // maximum specific uptake for nitrogen (rg 01.07.11)
            double usNH4 = vt->US_NH4() * SIM_DAY_FRACTION;
            double usNO3 = vt->US_NO3() * SIM_DAY_FRACTION;

            for ( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
            {
                if ( cbm::flt_greater_zero( p->fFrt_sl[sl]))
                {
                    //TODO: multispecies_stability
                    double const mFrt_vtsl( std::max(p->mFrt, 0.005) * p->fFrt_sl[sl]);

                    // nitrogen availabilty
                    double const nh4_avail( 0.99 * nh4_sl[sl]);   // total plant available nh4.    [kg N m-2]
                    double const no3_avail( 0.99 * no3_sl[sl]);   // total plant available no3.    [kg N m-2]
                    double const don_avail( 0.0 * don_sl[sl]);    // total plant available don.    [kg N m-2]

                    double const mfrt_veg_sl( ph_.mfrt_sl(m_veg, sl));
                    ldndc_assert( cbm::flt_greater_zero( mfrt_veg_sl));

                    // plant specific fraction of fine roots per soil layer.    [-]
                    double const fr_mfrt( mFrt_vtsl / mfrt_veg_sl);

                    // root density related to 'optimal' root density, decreased for small plants (rg 12.04.17).    [-]
                    double dfrt_opt = 0.01 + (vt->DFRTOPT() - 0.01) * std::min(1.0, p->height_max / 1.3);
                    double const f_frt( std::min(1.0, (mFrt_vtsl / sc_.h_sl[sl]) / dfrt_opt));
                    
                    // root factor determining nitrogen availability.    [-]        TODO: Add water dependency (* fH20?)
                    double const fact_frt = ( fr_mfrt * f_frt);

                    uptNH4Max_vtsl[p->slot][sl] = nh4_avail * fact_frt * SIM_DAY_FRACTION;
                    uptNO3Max_vtsl[p->slot][sl] = no3_avail * fact_frt * SIM_DAY_FRACTION;
                    uptDONMax_vtsl[p->slot][sl] = don_avail * fact_frt * SIM_DAY_FRACTION;

                    uptNH4Max_vt[p->slot] += uptNH4Max_vtsl[p->slot][sl];
                    uptNO3Max_vt[p->slot] += uptNO3Max_vtsl[p->slot][sl];
                    uptDONMax_vt[p->slot] += uptDONMax_vtsl[p->slot][sl];

                    // potential nitrogen uptake
                    uptNMax += (  std::min(usNH4 * mFrt_vtsl, uptNH4Max_vtsl[p->slot][sl])
                                + std::min(usNO3 * mFrt_vtsl, uptNO3Max_vtsl[p->slot][sl])
                                + std::min(0.0   * mFrt_vtsl, uptDONMax_vtsl[p->slot][sl]));
                }
            }

            uptNTot_vt[p->slot] = std::min( nDem_vt[p->slot] - p->nox_uptake - uptNH3_vt[p->slot],
                                            uptNMax + uptNWetMax_vt[p->slot]);
        }
    }

    // nitrogen uptake as NHx, NOy from soil and leaf water relative to their availability
    for ( PlantIterator vt = this->m_veg->begin(); vt != this->m_veg->end(); ++vt)
    {
        MoBiLE_Plant *p = (*vt);

        if ( cbm::flt_greater_zero( uptNTot_vt[p->slot]))
        {
            // rel. ammonia uptake
            double  fNH4 = uptNH4Max_vt[p->slot] / uptNTot_vt[p->slot];
            // rel. nitrate uptake
            double  fNO3 = uptNO3Max_vt[p->slot] / uptNTot_vt[p->slot];
            // rel. organic diluted nitrogen uptake
            double  fDON = uptDONMax_vt[p->slot] / uptNTot_vt[p->slot];
            // rel. uptake from canopy water
            double  fWet = uptNWetMax_vt[p->slot] / uptNTot_vt[p->slot];

            // rel. ammonia uptake per soil layer
            double  fNH4_sl = 0.0;
            double  fNO3_sl = 0.0;
            double  fDON_sl = 0.0;

            double const  upt_f(( uptNTot_vt[p->slot] / (fNH4 + fNO3 + fWet + fDON)));

            // root N uptake per soil layer
            for ( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
            {
                if ( cbm::flt_greater_zero( p->fFrt_sl[sl]))
                {
                    if (uptNH4Max_vt[p->slot] > 0.0) fNH4_sl = uptNH4Max_vtsl[p->slot][sl] / uptNH4Max_vt[p->slot];
                    if (uptNO3Max_vt[p->slot] > 0.0) fNO3_sl = uptNO3Max_vtsl[p->slot][sl] / uptNO3Max_vt[p->slot];
                    if (uptDONMax_vt[p->slot] > 0.0) fDON_sl = uptDONMax_vtsl[p->slot][sl] / uptDONMax_vt[p->slot];

                    double uptake_nh4( fNH4 * fNH4_sl * upt_f);
                    double uptake_no3( fNO3 * fNO3_sl * upt_f);
                    double uptake_don( fDON * fDON_sl * upt_f);

                    // solely fine root uptake
                    if ( cbm::flt_greater_zero(uptake_nh4))
                    {
                        // security
                        if ( cbm::flt_greater_equal( uptake_nh4, 0.99 * nh4_sl[sl] ))
                        {
                            uptake_nh4 = 0.99 * nh4_sl[sl] ;
                        }

                        ph_.accumulated_nh4_uptake_sl[sl] += uptake_nh4;
                        uptNH4_vt[p->slot] += uptake_nh4;
                        nh4_sl[sl] -= uptake_nh4;
                    }

                    if ( cbm::flt_greater_zero(uptake_no3))
                    {
                        // security
                        if ( cbm::flt_greater_equal( uptake_no3, 0.99 * no3_sl[sl]))
                        {
                            uptake_no3 = 0.99 * no3_sl[sl];
                        }
                        ph_.accumulated_no3_uptake_sl[sl] += uptake_no3;
                        uptNO3_vt[p->slot] += uptake_no3;
                        no3_sl[sl] -= uptake_no3;
                    }

                    if ( cbm::flt_greater_zero( uptake_don))
                    {
                        if ( cbm::flt_greater_equal( uptake_don, don_sl[sl]))
                        {
                            uptake_don = 0.99 * don_sl[sl];
                        }
                        ph_.accumulated_don_uptake_sl[sl] += uptake_don;
                        uptDON_vt[p->slot] += uptake_don;
                        don_sl[sl] -= uptake_don;
                    }
                }
            }
        }

        /* TODO: check and evaluate uptake from wet deposition. 
                 - currently none of the parameter estimates are based on data - */
//        uptNWet_vt[p->slot] = std::max(0.0, uptNTot_vt[p->slot] - p->nh4_uptake - p->no3_uptake - p->don_uptake - p->nh3_uptake - p->nox_uptake);
    }

    // wet deposition uptake and new nitrogen concentrations in throughfall
    double  uptWetNO3( 0.0);            // no3 uptake from wet deposition
    double  uptWetNH4( 0.0);            // nh4 uptake from wet deposition
    double  fnox(0.0);                  // relation between NOx and total N air concentration

    // if air concentrations of NH3 and NOx equal zero, the concentrations of NOx and NHy in wet deposition are assumed equal
    for ( size_t  fl = 0;  fl < fl_cnt_;  ++fl)
    {
        if (( cNOx_fl[fl] + nh3_concentration_fl[fl]) > 0.0)
        {
            fnox = cNOx_fl[fl] / ( cNOx_fl[fl] + nh3_concentration_fl[fl]);
        }
        else
        {
            fnox = 0.5;
        }

        for ( PlantIterator vt = this->m_veg->begin(); vt != this->m_veg->end(); ++vt)
        {
            MoBiLE_Plant *p = (*vt);

            if ( p->lai_fl[fl] > 0.0)
            {
                double const  flai = p->lai_fl[fl] / p->lai();
                p->nox_uptake      += (uptNWet_vt[p->slot] * flai * fnox);
                uptNH3_vt[p->slot] += (uptNWet_vt[p->slot] * flai * (1.0 - fnox));
                uptWetNO3          += (uptNWet_vt[p->slot] * flai * fnox);
                uptWetNH4          += (uptNWet_vt[p->slot] * flai * (1.0 - fnox));
            }
        }
    }

    if (( uptWetNO3 + uptWetNH4) > 0.0)
    {
        fnox = uptWetNO3 / ( uptWetNO3 + uptWetNH4);
    }
    else
    {
        fnox = 0.5;
    }
    
    return  LDNDC_ERR_OK;
}

/*!
 * @brief
 * Nitrogen fixation described similar to agricultural routines.
 * Noted under PSIM_NitrogenUptake.
 */
lerr_t
PhysiologyGrowthPSIM::PSIM_NitrogenFixation()
{
    for (PlantIterator vt = this->m_veg->begin(); vt != this->m_veg->end(); ++vt)
    {
        MoBiLE_Plant* p = (*vt);

        if (vt->INI_N_FIX() > 0.0 && vt->SAVANNA())
        {
            /* daily mean temperature is used also for subdaily simulations according to a regional/global function for daily N fixation */
            p->n2_fixation = nDem_vt[p->slot] * vt->INI_N_FIX() * NitrogenFixation_->get_nitrogen_fixation(vt, this->mc_.nd_airtemperature);
            p->rTra += p->n2_fixation * vt->NFIX_CEFF();
            nDem_vt[p->slot] -= p->n2_fixation;
        }
        else if (vt->INI_N_FIX() > 0.0)
        {
            /* TODO: consider that INI_N_FIX and n2_fixation are not for the same time step */
            p->n2_fixation = nDem_vt[p->slot] * vt->INI_N_FIX();
            nDem_vt[p->slot] -= p->n2_fixation;
        }
    }

    return  LDNDC_ERR_OK;
}


/*!
 * @brief
 * Biomass growth according to allocation gains and respiration losses
 * in different plant compartments
 */
lerr_t
PhysiologyGrowthPSIM::PSIM_BiomassUpdate()
{
    for ( PlantIterator vt = this->m_veg->begin(); vt != this->m_veg->end(); ++vt)
    {
        MoBiLE_Plant *p = (*vt);

        // remembering starting values
        double const fFacOld = p->f_fac;

        double const mFrtOld = p->mFrt;
        double const mSapOld = p->mSap;
        double const mBudOld = p->mBud;
        double const mLivOld = mFrtOld + mSapOld + mBudOld;

        // increase of dry matter
        double const groFol = p->dcFol / cbm::CCDM;
        double const groFrt = p->dcFrt / cbm::CCDM;
        double const groSap = p->dcSap / cbm::CCDM;
        double const groBud = p->dcBud / cbm::CCDM;
        double const groSum = groFrt + groSap + groBud;

        /* biomass adjustment according to the shift in free available carbon
         * following the basic assumption that free available carbon is proportionally
         * distributed between compartments but leaving reserves unchanged.*/
        if ( cbm::flt_greater_zero( mLivOld))
        {
            double const redistribC( p->dcFac / cbm::CCDM);
            p->mFrt += (redistribC * mFrtOld / mLivOld);
            p->mSap += (redistribC * mSapOld / mLivOld);
            p->mBud += (redistribC * mBudOld / mLivOld);
        }

        // considering growth and loss effects on compartment biomasses
        p->mBud += (groBud - p->sBud - mBudLoss_vt[p->slot]);
        p->mFol += (groFol - p->sFol + mBudLoss_vt[p->slot]);
        p->mFrt += (groFrt - cbm::sum( p->sFrt_sl, sl_.soil_layer_cnt()));
        p->mSap += (groSap - mSapLoss_vt[p->slot]);
        p->mCor += (mSapLoss_vt[p->slot] * (1.0 - p->f_branch));

        if ( !cbm::flt_greater_zero( p->mBud)) p->mBud = 0.0;
        if ( !cbm::flt_greater_zero( p->mFol)) p->mFol = 0.0;
        if ( !cbm::flt_greater_zero( p->mFrt)) p->mFrt = 0.0;
        if ( !cbm::flt_greater_zero( p->mSap)) p->mSap = 0.0;

        // new biomass and foliage area in layers
        m_pf.update_foliage_layer_biomass_and_lai((*vt), fl_cnt_, 0.0);

        // change foliage biomass in age classes due to growth and senescence
        if ( p->nb_ageclasses() == 1)
        {
            p->mFol_na[0] = p->mFol;
        }
        else
        {
            // allowing foliage mass increase in plants with non-annual growth behaviour
            p->mFol_na[0] += cbm::bound_min( 0.0, groFol + mBudLoss_vt[p->slot] - p->sFol_na[0]);
            for ( size_t  na = 1;  na < p->nb_ageclasses();  ++na)
            {
                p->mFol_na[na] = cbm::bound_min( 0.0, p->mFol_na[na] - p->sFol_na[na]);
            }
        }

        // total living biomass after growth (kgDM)
        double const mLivNew( p->mFrt + p->mSap + p->mBud);

        /*!
         * @todo multispecies check lower limit (used to be: mLivNew > 0.0)
         */
        if ( cbm::flt_greater( mLivNew, 0.001))
        {
            // new ratio of free available carbohydrates
            double const senSum = cbm::sum( p->sFrt_sl, sl_.soil_layer_cnt()) + mSapLoss_vt[p->slot];
            p->f_fac = (fFacOld * mLivOld + vt->FFACMAX() * (groSum - senSum) + p->dcFac / cbm::CCDM) / mLivNew;

            //Loss of structural carbon
            if ( !cbm::flt_greater_zero( p->f_fac)) p->f_fac = 0.0;
        }
        else
        {
            p->f_fac = 0.0;
        }

        // new peak foliage amount
        if ( cbm::flt_greater( p->mFol, p->mFolMax))
        {
            p->mFolMax = p->mFol;
        }
    }
    
    return  LDNDC_ERR_OK;
}


//----------------------------------------------------------------
/*!
 * @page psim
 * @section PSIM_PhotosynthesisRates Daily enzyme activity
 * \f[
 *   v25_{E} = V25_{E} \cdot fdorm \cdot fnit \cdot fwat
 * \f]
 * 
 * with
 * - v25: value for velocity at 25 oC (umol m-2 s-1)
 * - E:   indicator for different enzymes (carboxylation reactivity, electron transport activity, dark respiration)
 * - V25: parameterized standard values for enzyme velocities at 25 oC (umol m-2 s-1)
 * - fdorm, fnit, and fwat: multiplicative reduction factors for phenology, nitrogen- and water supply, respectively
 *
 * The reduction factor for phenology is differently calculated for deciduous species, where enzyme activity is 
 * assumed to develop in parallel to flushing and senescence, and for evergreen species, where the activity is 
 * determined from temperature development according to Maekelae et al @cite maekelae:2004a :
 * \f[
 *   fdorm = C1 \cdot (tFol24 + \frac{24.0}{TAU} \cdot ( temp - tFol24) - PSNTFROST)
 * \f]
 *
 * with
 * - temp:      (leaf surface) temperature (oC)
 * - tFol24:    temperature average of the previousl 24 hours (oC)
 * - C1, TAU:   empirical model constants (0.0367, 330.0, respectively) @cite maekelae:2004a
 * - PSNTFROST: species-specific temperature at which photosynthesis enzymes start to decline (oC)
 *
 * The reduction factor for nitrogen supply is defined as the degree to which the optimum (equal to maximum)
 * foliage nitrogen concentration has been established:
 * \f[
 *   fnit = \frac {ncFol - NCFOL\_{MIN} }{NCFOL\_{OPT} - NCFOL\_{MIN} }
 * \f]
 *
 * with
 * - ncFol:             foliage nitrogen concentration (gN gDM-1)
 * - NC_FOL (MIN, OPT): species-specific minimum and optimum foliage nitrogen concentrations (gN gDM-1)
 *
 * The reduction factor for water supply is based on the same function as has been used
 * for determining the loss of hydraulic conductance according to @cite tuzet:2003a.
 * The recovery of the water damaged enzyme activity is done with a limited rate of 5% (currently fixed).
 * \f[
 *   fwat = \frac {1.0 + exp(A\_{REF} \cdot A\_{EXP}) } {1.0 + exp((A\_{REF} - psi\_{mean}) \cdot A\_{EXP}) }
 * \f]
 *
 * with
 * - psi_mean:     mean canopy water potential (MPa)
 * - A_REF, A_EXP: species-specific empirical parameters that describe the sensitivity (EXP)
 *                 and a reference water potential (REF) of the drought impact on enzyme activity
 * 
 * @note vcAct25 is not changed in dependence on season alone as is e.g. indicated in @cite ito:2006a.
 *
*/
lerr_t
PhysiologyGrowthPSIM::PSIM_PhotosynthesisRates()
{
    for ( PlantIterator vt = this->m_veg->begin(); vt != this->m_veg->end(); ++vt)
    {
        MoBiLE_Plant *p = (*vt);

        // phenological state
        bool const evergreen( (p->nb_ageclasses() > 1) && cbm::flt_greater_zero( p->mFol));
        double const pstatus( evergreen ?
                     (p->dvsFlush * p->mFol_na[0] + (1.0 - p->dvsMort) * (cbm::sum( p->mFol_na, p->nb_ageclasses()) - p->mFol_na[0])) / p->mFol :
                     std::min( p->dvsFlush, 1.0 - p->dvsMort));

        // drought stress impact
        double fwat (ldndc::non_stomatal_water_limitation( p->psi_pd, (*p)->A_REF(), (*p)->A_EXP() ) );

        // - in case drought stress decreases, its decrease is limited by recovery rate (legacy effect)
        const double RECOVER(1.05); // needs to be larger than 1.0, otherwise no recovery possible
        if (fwat > p->fwatOld)
        {
            fwat = std::min(fwat, p->fwatOld * RECOVER);
        }
        p->fwatOld = fwat;

        // enzyme activity
        // (activity of isoprene and monoterpene enzymes are unchanged during the dormant season)
        double nFolOpt(m_pf.optimum_nitrogen_content_foliage(p));
        double ncFol_opt = nFolOpt / p->mFol;
        if ( cbm::flt_greater_zero( pstatus))
        {
            for ( size_t  fl = 0;  fl < fl_cnt_;  ++fl)
            {
                if ( cbm::flt_greater_zero( p->m_fol_fl(fl)) &&
                     cbm::flt_greater_zero( p->sla_fl[fl]))
                {
                    // nitrogen supply in dependence on specific leaf area
                    /*!
                     * Nitrogen concentration is related to specific leaf area but declines generally not linear. 
                     * Therefore, an exponential relationship with a parameter of 0.3 is used, fitted to various 
                     * canopy gradient studies (e.g. Meir et al. 2002, Al Afas et al. 2007, Ellsworth and Reich 1993)
                     */
                    double const ncFol_act( p->ncFol * pow( vt->SLAMIN() / p->sla_fl[fl], 0.3));

                    // dependency of photosynthesis linearly related to nitrogen (Reynolds et al. 1992)
                    double const fnit( cbm::bound( 0.0, (ncFol_act - vt->NC_FOLIAGE_MIN()) / (ncFol_opt - vt->NC_FOLIAGE_MIN()), 1.0));

                    // seasonality factor
                    double  fdorm(0.0);
                    if (p->nb_ageclasses() == 1)
                    {
                        // deciduous
                        if ( p->dvsFlush < 1.0)
                        {
                            fdorm = cbm::sqr( p->dvsFlush);
                        }
                        else
                        {
                            fdorm = 1.0 - cbm::sqr( p->dvsMort);
                        }
                    }
                    else
                    {
                        // evergreen (Maekelae et al. 2004 (S model))
                        if (( mc_.tFol24_fl[fl] + cbm::HR_IN_DAY / TAU * ( mc_.nd_temp_fl[fl] - mc_.tFol24_fl[fl])) >= vt->PSNTFROST())
                        {
                            fdorm = C1 * (mc_.tFol24_fl[fl] + cbm::HR_IN_DAY / TAU * ( mc_.nd_temp_fl[fl] - mc_.tFol24_fl[fl]) - vt->PSNTFROST());
                        }
                        else
                        {
                            fdorm = 0.0;
                        }
                    }

                    fdorm = cbm::bound( 0.0, fdorm, 1.0);

                    // activity of rubisco under standard conditions
                    p->vcAct25_fl[fl] = vt->VCMAX25() * fnit * fdorm * fwat;
                    // activity of maximum electron transport rate under standard conditions
                    p->jAct25_fl[fl]  = p->vcAct25_fl[fl] * vt->QJVC(); 
                    // activity of dark respiration under standard conditions
                    p->rdAct25_fl[fl] = p->vcAct25_fl[fl] * vt->QRD25();
                }
                else
                {
                    p->vcAct25_fl[fl] = 0.0;
                    p->jAct25_fl[fl]  = 0.0;
                    p->rdAct25_fl[fl] = 0.0;
                }
            }
        }
        else
        {
            for ( size_t  fl = 0;  fl < fl_cnt_;  ++fl)
            {
                p->vcAct25_fl[fl] = 0.0;
                p->jAct25_fl[fl] = 0.0;
                p->rdAct25_fl[fl] = 0.0;
            }
        }
    }
    
    return  LDNDC_ERR_OK;
}


lerr_t
PhysiologyGrowthPSIM::PSIM_Potentialranspiration()
{
    for ( PlantIterator vt = this->m_veg->begin(); vt != this->m_veg->end(); ++vt)
    {
        MoBiLE_Plant *p = (*vt);
        if (vt->IS_WOOD())
        {
            if (transpiration_method == "wateruseefficiency")
            {
                wc_.accumulated_potentialtranspiration += potential_wood_transpiration(
                    sl_,
                    mc_.nd_watervaporsaturationdeficit,
                    ph_.carbonuptake(m_veg, fl_cnt_),
                    p->f_area,
                    vt->WUECMAX(),
                    vt->WUECMIN(),
                    sc_.h_sl,
                    wc_.wc_sl,
                    sc_.wcmin_sl,
                    sc_.wcmax_sl);
            }
            else
            {
                wc_.accumulated_potentialtranspiration += potential_transpiration(
                    p->nb_foliagelayers(),
                    vt->GSMIN(),
                    vt->GSMAX(),
                    p->lai_fl,
                    mc_.vpd_fl,
                    p->relativeconductance_fl) * cbm::HR_IN_DAY * lclock()->day_fraction();
            }
        }
        else
        {
            wc_.accumulated_potentialtranspiration += potential_crop_transpiration(
                ac_.nd_co2_concentration,
                ph_.carbonuptake( m_veg, fl_cnt_),
                vt->WUECMAX());
        }
    }
    
    return  LDNDC_ERR_OK;
}

lerr_t
PhysiologyGrowthPSIM::PSIM_DroughtStress()
{
    double f_area_sum( 0.0);
    for ( PlantIterator vt = this->m_veg->begin(); vt != this->m_veg->end(); ++vt)
    {
        MoBiLE_Plant *p = (*vt);
        f_area_sum += p->f_area;
    }
    f_area_sum = cbm::bound_max( f_area_sum, 1.0);

    for ( PlantIterator vt = this->m_veg->begin(); vt != this->m_veg->end(); ++vt)
    {
        MoBiLE_Plant *p = (*vt);
        p->f_h2o = this->m_state->get_fh2o( *vt, f_area_sum);
    }

    return  LDNDC_ERR_OK;
}


lerr_t
PhysiologyGrowthPSIM::PSIM_NitrogenBalance()
{
    for ( PlantIterator vt = this->m_veg->begin(); vt != this->m_veg->end(); ++vt)
    {
        MoBiLE_Plant *p = (*vt);

        // - N amounts in each compartment
        double n_rest( 0.0);
        if ( cbm::flt_greater_zero( p->mFol))
        {
            p->ncFol = (nFol_vt[p->slot] - n_fol_to_cor_vt[p->slot] + n_bud_to_fol_vt[p->slot] - p->nLitFol
                        - n_fol_to_redistribute_vt[p->slot] + n_fol_gain_vt[p->slot]) / p->mFol;
        }
        else
        {
            n_rest += (nFol_vt[p->slot] - n_fol_to_cor_vt[p->slot] + n_bud_to_fol_vt[p->slot] - p->nLitFol
                       - n_fol_to_redistribute_vt[p->slot] + n_fol_gain_vt[p->slot]);
            p->mFol = 0.0;
            p->ncFol = 0.0;
        }

        if ( cbm::flt_greater_zero( p->mFrt))
        {
            p->ncFrt = (n_rest + nFrt_vt[p->slot] - n_frt_to_cor_vt[p->slot] - cbm::sum( p->nLitFrt_sl, sl_.soil_layer_cnt())
                        - n_frt_to_redistribute_vt[p->slot] + n_frt_gain_vt[p->slot]) / p->mFrt;
            n_rest = 0.0;
        }
        else
        {
            n_rest += (nFrt_vt[p->slot] - n_frt_to_cor_vt[p->slot] - cbm::sum( p->nLitFrt_sl, sl_.soil_layer_cnt())
                       - n_frt_to_redistribute_vt[p->slot] + n_frt_gain_vt[p->slot]);
            p->mFrt = 0.0;
            p->ncFrt = 0.0;
        }

        if ( cbm::flt_greater_zero( p->mSap))
        {
            p->ncSap = (n_rest + nSap_vt[p->slot] - n_sap_to_cor_vt[p->slot] - n_sap_to_litter_vt[p->slot]
                        - n_sap_to_redistribute_vt[p->slot] + n_sap_gain_vt[p->slot]) / p->mSap;
            n_rest = 0.0;
        }
        else
        {
            n_rest += (nSap_vt[p->slot] - n_sap_to_cor_vt[p->slot] - n_sap_to_litter_vt[p->slot]
                       - n_sap_to_redistribute_vt[p->slot] + n_sap_gain_vt[p->slot]);
            p->mSap = 0.0;
            p->ncSap = 0.0;
        }

        if ( cbm::flt_greater_zero( p->mBud))
        {
            p->ncBud = (n_rest + nBud_vt[p->slot] - n_bud_to_cor_vt[p->slot] - n_bud_to_fol_vt[p->slot] - p->nLitBud
                        - n_bud_to_redistribute_vt[p->slot] + n_bud_gain_vt[p->slot]) / p->mBud;
            n_rest = 0.0;
        }
        else
        {
            n_rest += (nBud_vt[p->slot] - n_bud_to_cor_vt[p->slot] - n_bud_to_fol_vt[p->slot] - p->nLitBud
                       - n_bud_to_redistribute_vt[p->slot] + n_bud_gain_vt[p->slot]);
            p->mBud = 0.0;
            p->ncBud = 0.0;
        }

        if ( cbm::flt_greater_zero( p->mCor))
        {
            p->ncCor = (n_rest + nCor_vt[p->slot] + n_fol_to_cor_vt[p->slot] + n_frt_to_cor_vt[p->slot] + n_sap_to_cor_vt[p->slot] + n_bud_to_cor_vt[p->slot]) / p->mCor;  
            n_rest = 0.0;
        }
        else
        {
            n_rest += (nCor_vt[p->slot] + n_fol_to_cor_vt[p->slot] + n_frt_to_cor_vt[p->slot] + n_sap_to_cor_vt[p->slot] + n_bud_to_cor_vt[p->slot]);
            p->mCor = 0.0;
            p->ncCor = 0.0;
        }
    }
    
    return  LDNDC_ERR_OK;
}
// rg:off //------------------------------------------------------------------------------
// rg:off /* frost damage function after King and Ball 1998 */
// rg:off double PhysiologyGrowthPSIM::CalcFrostDamage
// rg:off          (double *ph_.lai_vt)
// rg:off {
// rg:off     static double thard = 0.0;
// rg:off     static double fca   = 0.0;
// rg:off     static double flong = 0.0;
// rg:off
// rg:off     const double A = -26.5;
// rg:off     const double B = 3.0;
// rg:off     const double C = 0.02;
// rg:off     const double K = 0.5;
// rg:off     const double P = 0.15;
// rg:off     const double BETA = 1.0;
// rg:off     const double TEQ = 12.0;
// rg:off     const double TRANGE = 7.0;
// rg:off
// rg:off     double tnight,tday,tstat;
// rg:off     double fa,falpha;
// rg:off     double lai;
// rg:off     double d;
// rg:off
// rg:off     tnight = nd_minimumairtemperature + 0.25 * (nd_maximumairtemperature - nd_minimumairtemperature);
// rg:off     tday   = nd_airtemperature * 24.0 - tnight * (1.0 - mc_.dayl) / mc_.dayl;
// rg:off     tstat  = A + B * (tnight + BETA * (tday - TEQ));
// rg:off     thard  = thard + C * (tstat - thard);
// rg:off
// rg:off     if (nd_minimumairtemperature > thard + 0.5 * TRANGE)
// rg:off         fa = 1.0;
// rg:off     else if (nd_minimumairtemperature > thard - 0.5 * TRANGE)
// rg:off         fa = 0.5 * (1.0 + sin(PI * (nd_minimumairtemperature - thard) / TRANGE));
// rg:off     else
// rg:off         fa = 0.0;
// rg:off
// rg:off     lai = Sum(ph_.lai_vt,si_.vtMax);
// rg:off     d = 1.5 + 0.5 * exp(-2.0 * K * lai);
// rg:off
// rg:off     if (fa >= 0.5)
// rg:off         falpha = pow(fa,d);
// rg:off     else
// rg:off         falpha = fa / d;
// rg:off
// rg:off     if (fca < 0.8)
// rg:off         fca = fa * (fca + 0.2);
// rg:off     else
// rg:off         fca = fa;
// rg:off
// rg:off     if (falpha < 1.0)
// rg:off         flong = pow(falpha,P) * flong;
// rg:off     else
// rg:off         flong = 0.01 + 0.99 * flong;
// rg:off
// rg:off     return (fca * flong);
// rg:off }


// Vegetation Structure (dimensional growth and mortality) is calculated in TreeDyn
lerr_t
PhysiologyGrowthPSIM::PSIM_VegStructure()
{
    lerr_t  rc = LDNDC_ERR_OK;

    if( lclock()->is_position( TMODE_POST_DAILY))
    {
        for ( TreeIterator w = m_veg->groupbegin< species::wood >(); w != m_veg->groupend< species::wood >();  ++w)
        {
            MoBiLE_Plant *p = *w;
            rc = m_treedyn.Mortality( p,
                                     p->sWoodAbove, p->nLitWoodAbove,
                                     p->sWoodBelow_sl, p->nLitWoodBelow_sl);
            if ( rc)
            { return LDNDC_ERR_FAIL; }

            rc = m_treedyn.OnStructureChange( p, &root_system.at( p->slot));
            if ( rc)
            { return LDNDC_ERR_FAIL; }


            rc = m_treedyn.Growth( p, competition_opt,
                                   branchfraction_opt,
                                   crownlength_opt,
                                   volume_old_vt[p->slot]);
            if ( rc)
            { return LDNDC_ERR_FAIL; }


            rc = m_treedyn.OnStructureChange( p, &root_system.at( p->slot));
            if ( rc)
            { return LDNDC_ERR_FAIL; }


            if ( (this->lclock()->day()==1) && (this->lclock()->month()==1))
            {
                rc = m_treedyn.Crowding( p, crownlength_opt);
                if ( rc)
                { return LDNDC_ERR_FAIL; }

                rc = m_treedyn.OnStructureChange( p, &root_system.at( p->slot));
                if ( rc)
                { return LDNDC_ERR_FAIL; }
            }

        }
    }

    for ( TreeIterator w = m_veg->groupbegin< species::wood >(); w != m_veg->groupend< species::wood >();  ++w)
    {
        MoBiLE_Plant *p = *w;
        int  nb_events = m_treedyn.HandleEvents(
                                                io_kcomm,
                                                crownlength_opt,
                                                branchfraction_opt,
                                                competition_opt,
                                                p->sFol, p->nLitFol,
                                                p->sBud, p->nLitBud,
                                                p->sWoodAbove, p->nLitWoodAbove,
                                                p->sWoodBelow_sl, p->nLitWoodBelow_sl,
                                                p->sFrt_sl, p->nLitFrt_sl);
        if ( nb_events > 0)
        {
            rc = m_treedyn.Crowding(p, crownlength_opt);
            rc = m_treedyn.OnStructureChange( p, &root_system.at( p->slot));
        }
        else if ( nb_events == -1){ return LDNDC_ERR_FAIL; }
        else { /* no op */}
    }

    return rc;
}


lerr_t
PhysiologyGrowthPSIM::register_ports( cbm::io_kcomm_t *  _io_kcomm)
{
    lerr_t  rc = LDNDC_ERR_OK;

    rc = m_eventgraze.register_ports( _io_kcomm);
    if ( rc)
    { return LDNDC_ERR_FAIL; }

    rc = this->m_eventcut.register_ports( _io_kcomm);
    if ( rc)
    { return LDNDC_ERR_FAIL; }

    rc = m_treedyn.register_ports( _io_kcomm);
    if ( rc)
    { return LDNDC_ERR_FAIL; }


    int rc_plant = this->m_PlantEvents.subscribe( "plant", _io_kcomm);
    if ( rc_plant != LD_PortOk)
    { return  LDNDC_ERR_FAIL; }
    
    int rc_harvest = this->m_HarvestEvents.subscribe( "harvest", _io_kcomm);
    if ( rc_harvest != LD_PortOk)
    { return  LDNDC_ERR_FAIL; }

    return rc;
}


lerr_t
PhysiologyGrowthPSIM::unregister_ports( cbm::io_kcomm_t *  _io_kcomm)
{
    this->m_eventgraze.unregister_ports( _io_kcomm);
    this->m_eventcut.unregister_ports( _io_kcomm);
    this->m_treedyn.unregister_ports( _io_kcomm);

    this->m_PlantEvents.unsubscribe();
    this->m_HarvestEvents.unsubscribe();

    return LDNDC_ERR_OK;
}


} /*namespace ldndc*/
