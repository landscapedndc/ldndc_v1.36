/*!
 * @file
 * @author
 *  - Ruediger Grote
 *
 * @date
 * 
 */

#ifndef  LM_PHYSIOLOGY_GROWTHPSIM_H_
#define  LM_PHYSIOLOGY_GROWTHPSIM_H_

#include  "mbe_legacymodel.h"
#include  "state/mbe_state.h"
#include  "ld_shared.h"

#include  "physiology/ld_nitrogenfixation.h"
#include  "physiology/ld_plantfunctions.h"
#include  "physiology/ld_rootsystem.h"
#include  "physiology/ld_treedyn.h"
#include  "physiology/photofarquhar/berryball.h"

#include  "eventhandler/graze/graze.h"
#include  "eventhandler/cut/cut.h"

namespace ldndc {

extern cbm::logger_t *  PhysiologyGrowthPSIMLogger;

class  LDNDC_API  PhysiologyGrowthPSIM  :  public  MBE_LegacyModel
{
    LMOD_EXPORT_MODULE_INFO(PhysiologyGrowthPSIM,"physiology:psim","Physiology GrowthPSIM");

    public:
        /*! second parameter for temperature influence on vcmax activity */
        static const double  C1;
        /*! compensation NOx concentration without gas exchange  [ppb] */
        static const double  CCOMNOX;
        /*! saturation NH3 concentration  [ppb] */
        static const double  CSATNH3;
        /*! saturation NOx concentration  [ppb] */
        static const double  CSATNOX;
        /*! potential specific emission rate of NOx [ppb] */
        static const double  EPOTNOX;
        /*! power coefficient that reduces specific sapwood respiration with age */
        static const double  FAGE;
        /*! fraction of nitrate that is reduced in the roots */
        static const double  FRFRT;
        /*! average concentration of minerals others than nitrogen [kg/kgDW] */
        static const double  FMIN;
        /*! fraction of growth respiration relative to gross assimilation */
        static const double  FYIELD;
        /*! Michaelis Menten constant */
        static const double  KMMM;
        /*! carbon costs for ammonia uptake */
        static const double  PAMM;
        /*! parameter for GDD relation to temperature related site conditions */
        static const double  PGDD;
        /*! carbon costs for minerals */
        static const double  PMIN;
        /*! carbon costs for nitrate uptake */
        static const double  PNIT;
        /*! carbon costs for carbon transport from the leafs */
        static const double  PPHLOE;
        /*! carbon costs for nitrate reduction in the roots */
        static const double  PREDFRT;
        /*! carbon costs for nitrate reduction in the shoot */
        static const double  PREDSAP;
        /*! weighting factor for maximum temperature */
        static const double  PTW;
        /*! first parameter for temperature influence on vcmax activity */
        static const double  TAU;
        /*! soil temperature below which no fine root growth occurs (oC) */
        static const double  TGLIM;
        /*! maximum temperature for maintanence respiration */
        static const double  TRMAX;
        /*! minimum temperature for maintanence respiration */
        static const double  TRMIN;
        /*! temperature for optimum maintanence respiration */
        static const double  TROPT;
        /*! potential specific uptake rate of NH3 (ppb) */
        static const double  UPOTNH3;

    public:
        PhysiologyGrowthPSIM(
                             MoBiLE_State *,
                             ::cbm::io_kcomm_t *,
                             timemode_e);

        ~PhysiologyGrowthPSIM();

        lerr_t  configure( ::ldndc::config_file_t const *);
        lerr_t  register_ports( cbm::io_kcomm_t *);
        lerr_t  initialize();

        lerr_t  solve();
        lerr_t  unregister_ports( cbm::io_kcomm_t *);
        lerr_t  finalize() { return  LDNDC_ERR_OK; }

        lerr_t  sleep();
        lerr_t  wake();

        /*!
         * @brief
         *  ...
         */
        cbm::string_t branchfraction_opt;

        /*!
         * @brief
         *  ...
         */
        cbm::string_t crownlength_opt;

        /*!
         * @brief
         *  ...
         */
        bool competition_opt;

        /*!
         * @brief
         *  ...
         */
        cbm::string_t timemode_opt;

        /*!
         * @brief
         *  ...
         */
        bool forest_structure_opt;
        double target_biomass;
        int target_treenumber;

        /*!
         * @brief
         *     Transpiration method used by PSIM
         */
        cbm::string_t transpiration_method;

    protected:
    
        /*!
         * @return error code
         */
        lerr_t PSIM_ResizeVegetationVectors();

        /*!
         * @return error code
         */
        lerr_t PSIM_StepInit();

        /*!
         * @return error code
         */
        lerr_t PSIM_PlantingEvent();

        /*!
         * @return error code
         */
        lerr_t PSIM_HydraulicConductance();

        /*!
         * @return error code
         */
        lerr_t PSIM_Photosynthesis();

        /*!
         * @return error code
         */
        lerr_t  PSIM_Potentialranspiration();
    
        lerr_t  PSIM_ResetFlushing();
        
        lerr_t  PSIM_ResetPhenology( MoBiLE_Plant *);

        lerr_t  PSIM_ResetBiomassFromLastYear();

        lerr_t  PSIM_UpdateNitrogenConc();

        lerr_t  PSIM_AgriculturalManagement();

        lerr_t  PSIM_BudBurst();

        lerr_t  PSIM_Senescence();

        lerr_t  PSIM_CarAllocation();

        lerr_t  PSIM_SoilCarbonRelease();

        lerr_t  PSIM_NitAllocation();

        lerr_t  PSIM_Respiration();

        lerr_t  PSIM_NitrogenFixation();

        lerr_t  PSIM_NitrogenUptake();

        lerr_t  PSIM_BiomassUpdate();

        lerr_t  PSIM_PhotosynthesisRates();

        lerr_t  PSIM_DroughtStress();

        lerr_t  PSIM_NitrogenBalance();

        lerr_t  PSIM_VegStructure();

        lerr_t  PSIM_BalanceCheck(size_t);

        bool  subdaily_timemode();

        lerr_t PSIM_Harvest();

        lerr_t PSIM_StepOut();

    private:

        MoBiLE_State *  m_state;
        cbm::io_kcomm_t *  io_kcomm;

        input_class_climate_t const &  cl_;
        input_class_setup_t const *  m_setup;
        input_class_soillayers_t const &  sl_;
        input_class_species_t const *  m_species;

        substate_airchemistry_t &  ac_;
        substate_microclimate_t &  mc_;
        substate_physiology_t &  ph_;
        substate_soilchemistry_t &  sc_;
        substate_watercycle_t &  wc_;

        MoBiLE_PlantVegetation *  m_veg;

        NitrogenFixation *NitrogenFixation_;

        SubscribedEvent<LD_EventHandlerQueue>  m_PlantEvents;
        SubscribedEvent<LD_EventHandlerQueue>  m_HarvestEvents;
    
        lvector_t< bool > additional_season;              //set to true if additional growing season within the current phenological year is possible
        lvector_t< unsigned int > phenological_year;      //stores year of start of current growing season
        lvector_t< int > foliage_age_of_start_senescence; // age of foliage in days at which leaves start to shed

        lvector_t< double > carbonuptake_vt;

        lvector_t< int > days_since_emergence;
        lvector_t< double > volume_old_vt;
        std::vector< std::vector< int > > foliage_age_na;
        lvector_t< double > temp_sl;

        lvector_t< double > ts_temp_fl;
        lvector_t< double > nd_temp_fl;

        lvector_t< double > no_concentration_fl;
        lvector_t< double > no2_concentration_fl;
        lvector_t< double > nh3_concentration_fl;

        lvector_t< double > xylem_resistance_old_vt;
        lvector_t< double > psidecline_cum_vt;
        lvector_t< double > psidecline_daycum_vt;
        lvector_t< double > rehydrationindex_cum_vt;

        lvector_t< double > mSapOld_vt;
        lvector_t< double > mCorOld_vt;

        lvector_t< double > qsaparea_vt;

        lvector_t< double > mSapOpt_vt;
        lvector_t< double > mBudLoss_vt;
        lvector_t< double > mSapLoss_vt;
        lvector_t< double > ncFolOpt;
        lvector_t< double > ncBudOpt;
        lvector_t< double > ts_leaftemp_vt;
        lvector_t< double > nd_leaftemp_vt;
        lvector_t< double > nDem_vt;
        lvector_t< double > nFol_vt;
        lvector_t< double > nFrt_vt;
        lvector_t< double > nSap_vt;
        lvector_t< double > nCor_vt;
        lvector_t< double > nBud_vt;
        lvector_t< double > cNOx_fl;

        lvector_t< double > uptNH4Max_vt;
        lvector_t< double > uptNO3Max_vt;
        lvector_t< double > uptDONMax_vt;
        lvector_t< double > uptNWetMax_vt;
        lvector_t< double > uptNWet_vt;
        lvector_t< double > uptNTot_vt;
        lvector_t< double,2 > uptNH4Max_vtsl;
        lvector_t< double,2 > uptNO3Max_vtsl;
        lvector_t< double,2 > uptDONMax_vtsl;

        lvector_t< double > rFolOld_vt;
        lvector_t< double > rBudOld_vt;
        lvector_t< double > rSapOld_vt;
        lvector_t< double > rFrtOld_vt;
        lvector_t< double > rTraOld_vt;
        lvector_t< double > exsuLossOld_vt;

        lvector_t< double > uptNH4_vt;
        lvector_t< double > uptNO3_vt;
        lvector_t< double > uptNH3_vt;
        lvector_t< double > uptDON_vt;

        lvector_t< double > uptNH4Old_vt;
        lvector_t< double > uptNO3Old_vt;
        lvector_t< double > uptNH3Old_vt;
        lvector_t< double > uptDONOld_vt;

        lvector_t< double > uptN2Old_vt;
        lvector_t< double > uptNOxOld_vt;

        lvector_t< double > n_bud_to_fol_vt;

        lvector_t< double > n_sap_to_cor_vt;
        lvector_t< double > n_bud_to_cor_vt;
        lvector_t< double > n_fol_to_cor_vt;
        lvector_t< double > n_frt_to_cor_vt;

        lvector_t< double > n_sap_to_litter_vt;
    
        lvector_t< double > n_sap_to_redistribute_vt;
        lvector_t< double > n_bud_to_redistribute_vt;
        lvector_t< double > n_fol_to_redistribute_vt;
        lvector_t< double > n_frt_to_redistribute_vt;

        lvector_t< double > n_sap_gain_vt;
        lvector_t< double > n_bud_gain_vt;
        lvector_t< double > n_fol_gain_vt;
        lvector_t< double > n_frt_gain_vt;

        lvector_t< double > nh3_uptake_fl;
        lvector_t< double > nox_uptake_fl;

        double mFol_emergence;
        double accumulated_transpiration_old;
        double accumulated_potentialtranspiration_old;

        double plant_waterdeficit_old;

        double  tot_c;
        double  tot_n;

        bool daytime;
        double SIM_DAY_FRACTION;

        size_t  fl_cnt_;

        LD_PlantFunctions  m_pf;
        TreeDynamics  m_treedyn;

        lvector_t< double > no3_sl;
        lvector_t< double > nh4_sl;
        lvector_t< double > don_sl;

        EventHandlerGraze  m_eventgraze;
        EventHandlerCut  m_eventcut;

        BerryBall m_photo;

        /*!
         * @brief
         *     Root system
         *      - rooting depth
         *      - root distribution
         */
        std::map< int, RootSystemDNDC > root_system;
};
} /*namespace ldndc*/

#endif  /*  !LM_PHYSIOLOGY_GROWTHPSIM_H_  */

