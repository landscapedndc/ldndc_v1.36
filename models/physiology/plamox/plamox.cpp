/*!
 * @file
 * @author
 *  - David Kraus
 *
 * @date
 *  May 12, 2014
 */

/*!
 * @page plamox
 * @tableofcontents
 * @section plamox_guide User guide
 * PlaMox simulates the carbon and nitrogen cycle of crops and grass species. Processes are described in a universal way
 * and plants are primarily distinguished by species-specific parameters that can be accessed and calibrated externally.
 * However, for a growing number of specific species (e.g., rice, maize, ...), there exist specific functionalities,
 * which are continuously developed.
 *
 * @subsection plamox_guide_structure Model structure
 * PlaMox includes a submodel for photosynthesis calculation based on von Caemmerer and Farquahr 1981. Since the
 * photosynthesis submodel requires a subdaily time step, PlaMox can also only be used with a subdaily time resolution.
 * The recommendation is 24 time steps per day.
 * PlaMox requires further models for:
 * - watercycle (e.g., transpiration)
 * - soilchemistry (e.g., nitrogen uptake)
 * - microclimate (e.g., radiation distribution within the canopy)
 *
 * @subsection plamox_guide_parametrization Parametrization
 * The following lists include species parameters that might be calibrated in order to represent a specific plant.
 * See the description of respective sections for more details on parameter behaviour.
 *
 * Photosynthesis:
 * - \f$ C4\_TYPE \f$
 * - \f$ VCMAX25 \f$ (rubisco activity)
 * - \f$ SLAMAX \f$ (leaf area)
 * - \f$ SLADECLINE \f$ (decline of specific leaf area with plant development)
 *
 * Nitrogen related parameters:
 * - \f$ NC\_FRUIT\_MAX \f$ (optimum nitrogen content of the fruit)
 * - \f$ NC\_FRUIT\_MIN \f$ (minimum nitrogen content of the fruit)
 * - \f$ NC\_FINEROOTS\_MAX \f$ (optimum nitrogen content of fine roots)
 * - \f$ NC\_FINEROOTS\_MIN \f$ (minimum nitrogen content of fine roots)
 * - \f$ NC\_FOLIAGE\_MAX \f$ (optimum nitrogen content of foliage)
 * - \f$ NC\_FOLIAGE\_MIN \f$ (minimum nitrogen content of foliage)
 * - \f$ NC\_STRUCTURAL\_TISSUE\_MAX \f$ (optimum nitrogen content of structural tissue)
 * - \f$ NC\_STRUCTURAL\_TISSUE\_MIN \f$ (minimum nitrogen content of structural tissue)
 *
 * Allocation related parameters:
 * - \f$ FRACTION\_ROOT \f$ (assimilated carbon fraction allocated to roots)
 * - \f$ FRACTION\_FRUIT \f$ (assimilated carbon fraction allocated to the fruit)
 * - \f$ FRACTION\_FOLIAGE \f$ (assimilated carbon fraction allocated to foliage)
 * - \f$ MFOLOPT \f$ (optimum foliage biomass)
 *
 * Plant development related parameters:
 * - \f$ GDD\_BASE\_TEMPERATURE \f$
 * - \f$ GDD\_MAX\_TEMPERATURE \f$
 * - \f$ GDD\_STEM\_ELONGATION \f$
 * - \f$ GDD\_FLOWERING \f$
 * - \f$ GDD\_GRAIN\_FILLING \f$
 * - \f$ GDD\_MATURITY \f$
 *
 * Vernalization related parameters:
 * - \f$ CHILL\_TEMP\_MAX \f$
 * - \f$ CHILL\_UNITS \f$
 *
 * Cutting
 * - \f$ SHOOT\_STIMULATION\_REPROD \f$ (changes root/shoot ratio before first cutting)
 *
 * Stress
 * - \f$ H2OREF\_A \f$ (determines drought resistance)
 *
 * Nitrogen uptake
 * - \f$ TLIMIT \f$ (minimum temperature required for nitrogen uptake)
 * - \f$ K\_MM\_NITROGEN\_UPTAKE \f$ (root affinity to soil nitrogen)
 *
 * Nitrogen fixation
 * - \f$ INI\_N\_FIX \f$ (fraction of total nitrogen that might be fixed)
 * - \f$ NFIX\_RATE \f$ (maximum daily rate of nitrogen fixation)
 *
 * Respiration
 * - \f$ MAINTENANCE\_TEMP\_REF \f$ (reference temperature for maintenance respiration)
 * - \f$ MC\_LEAF \f$ (maintenance respiration coefficient for leafs)
 * - \f$ MC\_ROOT \f$ (maintenance respiration coefficient for roots)
 * - \f$ MC\_STEM \f$ (maintenance respiration coefficient for stems)
 * - \f$ MC\_STORAGE \f$ (maintenance respiration coefficient for fruit/storage organs)
 * - \f$ FYIELD \f$ (growth respiration efficiency)
 *
 * Root exudation
 * - \f$ DOC\_RESP\_RATIO \f$ (ratio of doc exudation in relation to root respiration)
 *
 * Senescence
 * - \f$ SENESCENCE\_DROUGHT \f$ (coefficient of senescence related to drought)
 * - \f$ SENESCENCE\_FROST \f$ (coefficient of senescence related to frost)
 * - \f$ SENESCENCE\_HEAT \f$ (coefficient of senescence related to heat)
 * - \f$ SENESCENCE\_AGE \f$ (coefficient of senescence related to age)
 * - \f$ FRET\_N \f$
 *
 * Fineroots turnover
 * - \f$ TOFRTBAS \f$
 *
 * water demand
 * - \f$ WUECMAX \f$ (water use efficiency)
 *
 * Structure
 * - \f$ EXP\_ROOT\_DISTRIBUTION \f$ (coefficient for exponential root distribution)
 * - \f$ RS\_CONDUCT \f$ (conductiviy of root aerenchyma)
 * - \f$ HEIGHT\_MAX \f$ (maximum plant height)
 */

#include  "physiology/plamox/plamox.h"
#include  "physiology/ld_nitrogenfixation.h"
#include  "physiology/ld_transpiration.h"
#include  "physiology/ld_vernalization.h"
#include  "watercycle/ld_droughtstress.h"
#include  "physiology/ld_stomatalconductance.h"

#include  <logging/cbm_logging.h>
#include  <scientific/meteo/ld_meteo.h>

#include  <input/soillayers/soillayers.h>
#include  <input/species/species.h>

#include  <math/cbm_math.h>
#include  <constants/cbm_const.h>


LMOD_MODULE_INFO(PhysiologyPlaMox,TMODE_SUBDAILY,LMOD_FLAG_USER);

namespace ldndc {

cbm::logger_t *  PhysiologyPlaMoxLogger = NULL;

#define  ON_ENTRY 1
#define  ON_EXIT 2

REGISTER_OPTION(PhysiologyPlaMox, loggerstream, "Stream for sending logging data");
REGISTER_OPTION(PhysiologyPlaMox, npot,"  [bool]");
REGISTER_OPTION(PhysiologyPlaMox, transpirationmethod,"  [char]");
REGISTER_OPTION(PhysiologyPlaMox, droughtstressmethod,"  [char]");
REGISTER_OPTION(PhysiologyPlaMox, plantfamilies,"  [char]");


PhysiologyPlaMox::PhysiologyPlaMox(
                   MoBiLE_State *  _state,
                   cbm::io_kcomm_t *  _io_kcomm,
                   timemode_e  _timemode)
                    : MBE_LegacyModel( _state, _timemode),
                    m_state( _state),
                    io_kcomm( _io_kcomm),
                    cl_( _io_kcomm->get_input_class_ref< input_class_climate_t >()),
                    m_setup( _io_kcomm->get_input_class< input_class_setup_t >()),
                    sl_( _io_kcomm->get_input_class_ref< input_class_soillayers_t >()),
                    m_species( _io_kcomm->get_input_class< species::input_class_species_t >()),
                    m_speciesparameters( _io_kcomm->get_input_class< speciesparameters::input_class_speciesparameters_t >()),
                    m_veg( &_state->vegetation),
                    ac_( _state->get_substate_ref< substate_airchemistry_t >()),
                    mc_( _state->get_substate_ref< substate_microclimate_t >()),
                    ph_( _state->get_substate_ref< substate_physiology_t >()),
                    sc_( _state->get_substate_ref< substate_soilchemistry_t >()),
                    wc_( _state->get_substate_ref< substate_watercycle_t >()),
                    m_eventgraze( _state, _io_kcomm, _timemode),
                    m_eventcut( _state, _io_kcomm, _timemode),
                    m_photo( 1.0 / LD_RtCfg.clk->time_resolution(), m_setup->canopylayers()),
                    NitrogenFixation_( NULL),
                    m_pf( _state, _io_kcomm),
                    no3_sl( sl_.soil_layer_cnt(), 0.0),
                    nh4_sl( sl_.soil_layer_cnt(), 0),
                    don_sl( sl_.soil_layer_cnt(), 0.0),
                    root_system(0, NULL),
                    FTS_TOT_( 1.0 / LD_RtCfg.clk->time_resolution()),
                    mc_temp( &mc_.ts_airtemperature),
                    daytime_temperatures( 19, mc_.ts_airtemperature),
                    influence_heat_daily( 19, 1)
{
    if ( !PhysiologyPlaMoxLogger)
    {
        PhysiologyPlaMoxLogger = CBM_RegisteredLoggers.new_logger( "PhysiologyPlaMox");
    }

    NitrogenFixation_ = new NitrogenFixation(_state, _io_kcomm);

    automatic_nitrogen_uptake = 0.0;

    photoperiod_min = 24.0;
    photoperiod_max = 0.0;
    for ( size_t d = 1; d <= 365; ++d)
    {
        photoperiod_min = cbm::bound_max( ldndc::meteo::daylength( m_setup->latitude(), d), photoperiod_min);
        photoperiod_max = cbm::bound_min( photoperiod_max, ldndc::meteo::daylength( m_setup->latitude(), d));
    }

    /* carbon and nitrogen balance */
    tot_c_ = 0.0;
    tot_n_ = 0.0;
    timestep_c_assi = 0.0;
    n_at_planting = 0.0;

    c_total_exported = 0.0;
    c_fruit_exported = 0.0;
    n_total_exported = 0.0;
    n_fruit_exported = 0.0;

    influence_heat_reduction_grainfilling = 1;
}



/*!
 * @details
 *      Delete:
 *      - roots system
 *      - nitrogen fixation
 */
PhysiologyPlaMox::~PhysiologyPlaMox()
{
    for ( size_t  r = 0;  r < root_system.size();  ++r)
    {
        if ( root_system[r])
        {
            LD_Allocator->destroy( root_system[r]);
        }
    }

    delete NitrogenFixation_;
}


lerr_t
PhysiologyPlaMox::configure( ldndc::config_file_t const * _cf)
{
    /* logger stream */
    std::string  loggerstream = get_option< char const * >( "loggerstream", "null");
    loggerstream = cbm::format_expand( loggerstream);
    PhysiologyPlaMoxLogger->initialize( loggerstream.c_str(), _cf->log_level());

    /*!
     * @page plamox
     * @section plamox_options Model options
     * Available options:
     * Default options are marked with bold letters.
     *  - Unlimited nitrogen availability (npot: \b no / yes)
     */
    have_automatic_nitrogen = get_option< bool >( "npot", false);
    if ( have_automatic_nitrogen)
    {
        KLOGINFO_TO( PhysiologyPlaMoxLogger, "PlaMox runs in potential nitrogen mode!");
    }

    /*!
     * @page plamox
     *  - Transpiration (transpirationmethod: \b wateruseefficiency / stomatalconductance)
     */
    transpiration_method = get_option< char const * >( "transpirationmethod", "wateruseefficiency");
    if ( transpiration_method == "wateruseefficiency")
    {
        KLOGINFO_TO( PhysiologyPlaMoxLogger,
                     "Transpiration estimated via carbon assimilation and water use efficiency!");
    }
    else
    {
        KLOGINFO_TO( PhysiologyPlaMoxLogger,
                     "Transpiration estimated via stomatal conductance and vapour pressure deficit!");
    }

    /*!
     * @page plamox
     *  - Droughtstress (droughtstressmethod: \b uniform / rootweighted)
     */
    droughtstress_method = get_option< char const * >( "droughtstressmethod", "uniform");
    if ( droughtstress_method == "uniform")
    {
        KLOGINFO_TO( PhysiologyPlaMoxLogger,
                     "Drought stress estimated from water availabilty in the rooted zone!");
    }
    else
    {
        KLOGINFO_TO( PhysiologyPlaMoxLogger,
                     "Drought stress estimated from water availabilty in the rooted zone weighted by fine roots distribution!");
    }

    m_photo.stomatalconductance_method = leuning_1995_b;

    /*!
     * @page plamox
     *  - Considered species (plantfamilies: \b crops \b grass)
     */
    cbm::string_t  plantfamilylist = get_option< char const * >( "plantfamilies", "crops grass");
    plantfamilies = plantfamilylist.split( ' ');

    return  LDNDC_ERR_OK;
}



/*!
 * @brief
 *
 */
lerr_t
PhysiologyPlaMox::initialize()
{
    /* check for proper time resolution */
    if ( lclock()->time_resolution() < 2)
    {
        KLOGERROR( "module \"", name(), "\" requires higher time resolution!\nchosen time resolution: ",
                   lclock()->time_resolution()," required time resolution: >1");
        return  LDNDC_ERR_OBJECT_INIT_FAILED;
    }
    return  LDNDC_ERR_OK;
}



PhysiologyPlaMox::PlaMoxIterator
PhysiologyPlaMox::begin()
{ return PlaMoxIterator( m_veg, plantfamilies, m_veg->begin()); }



PhysiologyPlaMox::PlaMoxIterator
PhysiologyPlaMox::end()
{ return PlaMoxIterator( NULL, plantfamilies, m_veg->end()); }



/*!
 * @details
 *
 */
lerr_t
PhysiologyPlaMox::solve()
{
    /* Pre-time step setting */
    (void)PlaMox_step_init();

    /* Begin balance check */
    PlaMox_balance_check( ON_ENTRY);

    /* Grazing and Cutting */
    PlaMox_management();

    /* Planting */
    PlaMox_planting();

    /* Vernalization */
    for ( PlaMoxIterator vt = begin(); vt != end(); ++vt)
    {
        MoBiLE_Plant *p = *vt;

        PlaMox_growing_degree_days( p);
        PlaMox_vernalization( p);
    }

    /* Plant development
     * determines how much C is given to which plant part in photosynthesis (C allocation) */
    for ( PlaMoxIterator vt = begin(); vt != end(); ++vt)
    {
        MoBiLE_Plant *p = *vt;
        PlaMox_heat_stress_limitation( p);

        if ( p->group() == "crop")
        {
            PlaMox_plant_development_crop( p);
        }
        else if ( p->group() == "grass")
        {
            PlaMox_allocation_grass( p);
        }
    }

    /* Photosynthesis activity and carbon allocation
     * The C is translated into biomass via CCDM. */
    for ( PlaMoxIterator vt = begin(); vt != end(); ++vt)
    {
        MoBiLE_Plant *p = *vt;

        lerr_t rc_photosynthesis = PlaMox_photosynthesis( p);

        if ( rc_photosynthesis)
        {
            KLOGERROR( "Photosynthesis calculation failed  [species=", p->name(),"]");
            return  LDNDC_ERR_FAIL;
        }

        if ( p->group() == "grass")
        {
            PlaMox_bud_burst( p);
        }
        
        PlaMox_redistribution( p);

    }

    /* Nitrogen uptake/fixation */
    for ( PlaMoxIterator vt = begin(); vt != end(); ++vt)
    {
        MoBiLE_Plant *p = *vt;

        PlaMox_nitrogen_uptake( p);
        PlaMox_nitrogen_fixation( p);
    }


    /* C-losses (respiration, exudation and senescence */
    for ( PlaMoxIterator vt = begin(); vt != end(); ++vt)
    {
        MoBiLE_Plant *p = *vt;

        /* respiration */
        PlaMox_respiration( p);

        /* exudation */
        PlaMox_exsudation( p);

        /* senescence */
        PlaMox_senescence( p);

        /* abscission */
        PlaMox_abscission( p);

        /* root growth */
        PlaMox_update_root_structure( p, p->dcFrt / cbm::CCDM);

        /* ground coverage */
        PlaMox_update_ground_cover( p);

        /* plant height */
        PlaMox_update_height( p);

        /* specific leaf area */
        PlaMox_update_specific_leaf_area( p);

        /* leaf area index */
        PlaMox_update_foliage_structure( p);

        /* drought stress */
        if ( droughtstress_method == "uniform")
        {
            p->f_h2o = m_state->get_fh2o( p, 1.0, false, false);
        }
        else
        {
            p->f_h2o = m_state->get_fh2o( p, 1.0, false, true);
        }

        /* transpiration */
        PlaMox_transpiration( p);
    }


    /* Harvest */
    PlaMox_harvest();

    /* End balance check */
    PlaMox_balance_check( ON_EXIT);

    /* Post-time step setting */
    PlaMox_step_out();

    return  LDNDC_ERR_OK;
}



/*!
 * @details
 *
 */
lerr_t
PhysiologyPlaMox::PlaMox_step_init()
{
    temp_sl.reference( mc_.temp_sl);
    temp_fl.reference( mc_.temp_fl);
    vpd_fl.reference( mc_.vpd_fl);

    PlaMox_step_resize();

    for ( PlaMoxIterator vt = begin(); vt != end(); ++vt)
    {
        MoBiLE_Plant *p = (*vt);

        dcLst[p->slot] = p->dcSap;
        dcFru[p->slot] = p->dcBud;

        n2_fixation[p->slot] = p->n2_fixation;

        /* reset */
        for ( int  fl = 0;  fl < m_setup->canopylayers();  ++fl)
        {
            p->vcAct25_fl[fl] = 0.0;
            p->jAct25_fl[fl]  = 0.0;
            p->rdAct25_fl[fl] = 0.0;
        }
    }

    timestep_c_assi = 0.0;
    automatic_nitrogen_uptake = 0.0;
    n_at_planting = 0.0;

    c_total_exported = 0.0;
    c_fruit_exported = 0.0;
    n_total_exported = 0.0;
    n_fruit_exported = 0.0;

    if ( lclock()->subday() == 1)
    {
        m_eventgraze.reset_daily_food_consumption();

        for ( PlaMoxIterator vt = begin(); vt != end(); ++vt)
        {
            MoBiLE_Plant *p = (*vt);
            if ( days_after_emergence[p->slot] >= 0)
            {
                days_after_emergence[p->slot] += 1;
            }
        }
    }

    
    if ( MoBiLE_IsBeginningOfYear( lclock(), m_setup->latitude()))
    {
        for ( PlaMoxIterator vt = begin(); vt != end(); ++vt)
        {
            MoBiLE_Plant *p = (*vt);
            if (   (PlaMox_get_life_cycle( p) > PhysiologyPlaMox::ANNUAL)
                && (lclock()->seconds() > seconds_crop_planting[p->slot])) //avoid reset directly after planting at start of year
            {
                reset_phenology[p->slot] = true;
            }
        }
    }

    for ( PlaMoxIterator vt = begin(); vt != end(); ++vt)
    {
        MoBiLE_Plant *p = (*vt);
        if (   reset_phenology[p->slot]
            && cbm::flt_greater(p->dvsMort, 0.99))
        {
            PlaMox_reset_phenology( p);
        }
    }
  
    for ( size_t  sl = 0; sl < sl_.soil_layer_cnt(); ++sl)
    {
        nh4_sl[sl] = sc_.nh4_sl[sl];
        don_sl[sl] = sc_.don_sl[sl];
        no3_sl[sl] = sc_.no3_sl[sl] + sc_.an_no3_sl[sl];
    }

    return  LDNDC_ERR_OK;
}


lerr_t
PhysiologyPlaMox::PlaMox_step_resize()
{
    size_t const  slot_cnt = m_veg->slot_cnt();

    root_q_vt_.resize_and_preserve( slot_cnt, invalid_uint);

    transplanting_shock_vt.resize_and_preserve( slot_cnt, 0.0);

    yearly_cuts.resize_and_preserve( slot_cnt, 0);
    reset_phenology.resize_and_preserve( slot_cnt, false);

    n_plant.resize_and_preserve( slot_cnt, 0.0);

    seconds_crop_planting.resize_and_preserve( slot_cnt, 0);

    gdd_grain_filling.resize_and_preserve( slot_cnt, 0.0);

    n2_fixation.resize_and_preserve( slot_cnt, 0.0);

    days_after_emergence.resize_and_preserve( slot_cnt, -1);

    location.resize_and_preserve( slot_cnt, "?");
    reserves_stem.resize_and_preserve( slot_cnt, 0.0);

    dcLst.resize_and_preserve( slot_cnt, 0.0);
    dcFru.resize_and_preserve( slot_cnt, 0.0);

    allocation_factor_leafs.resize_and_preserve( slot_cnt, 0.0);
    allocation_factor_stems.resize_and_preserve( slot_cnt, 0.0);
    allocation_factor_fruit.resize_and_preserve( slot_cnt, 0.0);
    allocation_factor_roots.resize_and_preserve( slot_cnt, 0.0);

    lai_min.resize_and_preserve( slot_cnt, 0.0);
    fractional_cover.resize_and_preserve( slot_cnt, 1.0);

    chill_factor.resize_and_preserve( slot_cnt, 1.0);
    chill_units.resize_and_preserve( slot_cnt, 0.0);

    m_fruit_maximum.resize_and_preserve( slot_cnt, -1.0);
    nc_fol_opt.resize_and_preserve( slot_cnt, 0.0);

    return  LDNDC_ERR_OK;
}


/*!
 * @details
 *
 */
lerr_t
PhysiologyPlaMox::PlaMox_reset_phenology( MoBiLE_Plant *_p)
{
    _p->a_fix_n = 0.0;
    _p->growing_degree_days = 0.0;
    _p->dEmerg = -1;
    _p->dvsMort = 0.0;
    _p->dvsFlush = 0.0;
    _p->dvsFlushOld = 0.0;
    yearly_cuts[_p->slot] = 0;
    reset_phenology[_p->slot] = false;

    _p->mFol_na[0] = 0.0;
    _p->mFol_na[1] = _p->mFol;

    return  LDNDC_ERR_OK;
}


/*!
 * @details
 *
 */
lerr_t
PhysiologyPlaMox::PlaMox_step_out()
{
    for ( PlaMoxIterator vt = begin(); vt != end(); ++vt)
    {
        MoBiLE_Plant *p = (*vt);

        p->dcSap = dcLst[p->slot];
        p->dcBud = dcFru[p->slot];

        p->n2_fixation = n2_fixation[p->slot];

        p->dvsFlushOld = p->dvsFlush;
    }

    double agb( 0.0);
    double dvs( 0.0);
    double gdd( 0.0);
    double dEmerg_send(0.0);
    for ( PlaMoxIterator vt = begin(); vt != end(); ++vt)
    {
        MoBiLE_Plant *p = *vt;
        agb += p->aboveground_biomass();
        dvs += p->dvsFlush;
        gdd += p->growing_degree_days;
        dEmerg_send += p->dEmerg;
    }

    for ( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
    {
        if ( cbm::flt_greater_zero( nh4_sl[sl]))
        {
            sc_.nh4_sl[sl] = nh4_sl[sl];
        }
        else { sc_.nh4_sl[sl] = 0.0; }

        if ( cbm::flt_greater_zero( don_sl[sl]))
        {
            sc_.don_sl[sl] = don_sl[sl];
        }
        else { sc_.don_sl[sl] = 0.0; }

        double const no3_tot( sc_.no3_sl[sl] + sc_.an_no3_sl[sl]);
        if ( cbm::flt_greater_zero( no3_tot))
        {
            sc_.no3_sl[sl] = sc_.no3_sl[sl] / no3_tot * no3_sl[sl];
            sc_.an_no3_sl[sl] = no3_sl[sl] - sc_.no3_sl[sl];
        }
        else
        {
            sc_.no3_sl[sl] = no3_sl[sl] * (1.0 - sc_.anvf_sl[sl]);
            sc_.an_no3_sl[sl] = no3_sl[sl] - sc_.no3_sl[sl];
        }
    }

    aboveground_biomass.send( agb);
    maturity_status.send( dvs);
    growing_degree_days_maturity.send( gdd);
    dEmerg.send(dEmerg_send);

    carbon_plant_biomass_total_exported_from_field.send( c_total_exported);
    carbon_plant_biomass_fruit_exported_from_field.send( c_fruit_exported);
    nitrogen_plant_biomass_total_exported_from_field.send( n_total_exported);
    nitrogen_plant_biomass_fruit_exported_from_field.send( n_fruit_exported);


    return  LDNDC_ERR_OK;
}



/*!
 * @page plamox
 * @section management Management
 * Considered field mangement includes:
 *  - Planting
 *  - Harvest
 *  - Grazing
 *  - Cutting
 */
lerr_t
PhysiologyPlaMox::PlaMox_planting()
{
    EventAttributes const *  ev_plant = NULL;
    while ((ev_plant = this->m_PlantEvents.pop()) != NULL)
    {
        char const *  p_name = ev_plant->get( "/name", "?");
        char const *  p_type = ev_plant->get( "/type", "?");
        char const *  p_group = ev_plant->get( "/group", "?");
        if ( !m_state->vegetation.have_plant( p_name))
        {
            species_t const *  p = NULL;
            if ( m_species)
            { p = m_species->get_species( p_name); }
            
            MoBiLE_PlantParameters  p_parameters;
            if ( p)
            { p_parameters = p->get_parameters(); }
            else if ( m_speciesparameters) /* assume event was not provided via input */
            { p_parameters = (*m_speciesparameters)[p_type]; }
            else
            { /* no parameters.. */ }
            
            MoBiLE_PlantSettings  plant_settings( static_cast< unsigned int >(m_setup->canopylayers()),
                                                  p_name, p_type, p_group, &p_parameters);
            plant_settings.nb_soillayers = sl_.soil_layer_cnt();

            MoBiLE_Plant *  plant = this->m_state->vegetation.new_plant( &plant_settings);
            if ( plant)
            {
                plant->initialize();
            }
            else
            {
                KLOGERROR( "Species creation failed  [species=", p->name(),"]");
                return  LDNDC_ERR_FAIL;
            }
        }

        PlaMox_step_resize();

        MoBiLE_Plant *  vt = m_veg->get_plant( ev_plant->get( "/name", "?"));
        if ( !vt){ return LDNDC_ERR_RUNTIME_ERROR; }

        for (size_t i = 0; i < this->plantfamilies.size(); ++i)
        {
            cbm::string_t family = ":";
            family << this->plantfamilies[i] <<":";
            if (   this->m_veg->is_family( vt->ctype(), family.c_str())
                || cbm::is_equal( vt->cname(), family.c_str()))
            {
                lerr_t  rc_plant = PlaMox_event_plant( vt, *ev_plant);
                if ( rc_plant)
                {
                    KLOGERROR( "Handling plant event failed  [species=", vt->name(),"]");
                    return  LDNDC_ERR_FAIL;
                }
            }
        }
    }

    return  LDNDC_ERR_OK;
}



/*!
 * @page plamox
 * @subsection PlaMox_planting Planting event
 * For planting events, the following event inputs are considered:
 * - Plant type and name
 * - Initial biomass
 *
 * All other quantities are determined by the model:
 * - Annual nitrogen fixation is assumed to be zero at planting
 * - Plant development index and growing degree days are set to 0
 * - Tissue nitrogen concentration is set to optimum
 *
 * The N-contents of fine roots, foliage, and structural tissue are set to optimum
 * (parameters NC_FINEROOTS_MAX, NC_FOLIAGE_MAX, and NC_STRUCTURAL_TISSUE_MAX).
 * Mass is only distributed to fine roots and foliage by FRACTION_ROOT and 1 - FRACTION_ROOT.
 *
 */
lerr_t
PhysiologyPlaMox::PlaMox_event_plant(
                                     MoBiLE_Plant *  _vt,
                                     EventAttributes const &  _attributes)
{
    species_t const *  sp = NULL;
    if ( m_species)
    {
        sp = m_species->get_species( _vt->cname());
    }

    daytime_temperatures = 0;

    seconds_crop_planting[_vt->slot] = lclock()->seconds();
    fractional_cover[_vt->slot] = _attributes.get( "/fractional-cover", 1.0);

    _vt->is_covercrop = _attributes.get( "/cover-crop", false);

    //amount of seeds
    double initial_biomass = _attributes.get( "/initial-biomass", 0.0);
    if ( cbm::flt_equal( initial_biomass, 0.0))
    {
        if ( m_veg->is_family( _vt, ":rice:"))
        {
            initial_biomass = 200.0;
        }
        else if ( m_veg->is_family( _vt, ":wheat:"))
        {
            initial_biomass = 150.0;
        }
        else if ( m_veg->is_family( _vt, ":corn:"))
        {
            initial_biomass = 50.0;
        }
        else if ( m_veg->is_family( _vt, ":rapeseed:"))
        {
            initial_biomass = 10.0;
        }
    }

    initial_biomass = cbm::bound_min( 0.0001, initial_biomass * cbm::HA_IN_M2);
    lai_min[_vt->slot] = 0.01;

    //emergence has happened for biomass greater 50kg ha-1
    if ( cbm::flt_greater( initial_biomass, 0.01) &&
         m_veg->is_family( _vt, ":rice:"))
    {
        days_after_emergence[_vt->slot] = 0;
    }
    else
    {
        _vt->dvsFlush = 0.0;
        _vt->dvsMort = 0.0;
        _vt->growing_degree_days = 0.0;
        _vt->dEmerg = -1;
        days_after_emergence[_vt->slot] = -1;
    }

    location[_vt->slot] = _attributes.get( "/location", "?");
    reserves_stem[_vt->slot] = 0.0;

    //plant gets biomass by species initializer
    //store and substract later according amounts of C and N
    //for balance check at day of planting
    double const plant_n_old( _vt->total_nitrogen());
    double const plant_c_old( _vt->total_biomass() * cbm::CCDM);

    _vt->height_max = 0.0;
    _vt->height_at_canopy_start = 0.0;
    _vt->a_fix_n = 0.0;

    chill_units[_vt->slot] = 0.0;
    chill_factor[_vt->slot] = 1.0;
   
    if ( cbm::flt_greater_zero( (*_vt)->M_FRUIT_OPT()))
    {
        m_fruit_maximum[_vt->slot] = (*_vt)->M_FRUIT_OPT();
    }
    else
    {
        m_fruit_maximum[_vt->slot] = -1.0;
    }

    if ( m_veg->is_family( _vt, ":rice:"))
    {
        nc_fol_opt[_vt->slot] = 0.5 * ((*_vt)->NC_FOLIAGE_MAX() + (*_vt)->NC_FOLIAGE_MIN());
    }
    else if ( m_veg->is_family( _vt, ":wheat:"))
    {
        nc_fol_opt[_vt->slot] = 0.5 * ((*_vt)->NC_FOLIAGE_MAX() + (*_vt)->NC_FOLIAGE_MIN());
    }
    else
    {
        nc_fol_opt[_vt->slot] = (*_vt)->NC_FOLIAGE_MAX();
    }

    gdd_grain_filling[_vt->slot] = (*_vt)->GDD_GRAIN();

    if ( cbm::is_equal( _vt->group().c_str(), "crop"))
    {
        lerr_t rc_plant = m_pf.initialize_crop( _vt, sp ? sp->crop() : NULL);
        if ( rc_plant)
        {
            return LDNDC_ERR_OK;
        }

        /* only considered for rice so far */
        int const seedbedduration( _attributes.get( "/seedbed-duration", 0));
        if ( seedbedduration > 0)
        {
            double const mean_gdd( cbm::bound_max(mc_.nd_airtemperature - (*_vt)->GDD_BASE_TEMPERATURE(), (*_vt)->GDD_MAX_TEMPERATURE()));
            _vt->growing_degree_days = seedbedduration * mean_gdd;
            _vt->dvsFlush = _vt->growing_degree_days / (*_vt)->GDD_MATURITY();
        }
        else if ( cbm::flt_greater_equal( initial_biomass, 0.01) &&
                  m_veg->is_family( _vt, ":rice:"))
        {
            _vt->growing_degree_days = 200.0;
            _vt->dvsFlush = _vt->growing_degree_days / (*_vt)->GDD_MATURITY();
            lai_min[_vt->slot] = cbm::bound_min( lai_min[_vt->slot],
                                                 initial_biomass * (1.0 - (*_vt)->FRACTION_ROOT()) * (*_vt)->SLAMAX());
        }
        else
        {
            _vt->growing_degree_days = 0.0;
            _vt->dvsFlush = 0.0;
        }

        transplanting_shock_vt[_vt->slot] = pow(initial_biomass/0.005, 0.2);

        allocation_factor_roots[_vt->slot] = (*_vt)->FRACTION_ROOT();
        allocation_factor_leafs[_vt->slot] = 1.0 - (*_vt)->FRACTION_ROOT();
        allocation_factor_stems[_vt->slot] = 0.0;
        allocation_factor_fruit[_vt->slot] = 0.0;

        _vt->ncFrt = (*_vt)->NC_FINEROOTS_MAX();
        _vt->ncFol = (*_vt)->NC_FOLIAGE_MAX();
        _vt->n_lst = 0.0;
        _vt->ncBud = 0.0;
        _vt->ncSap = 0.0;
        _vt->ncCor = 0.0;

        double const initial_foliage_biomass_max( 0.02);
        _vt->mFol = cbm::bound_max( (1.0 - (*_vt)->FRACTION_ROOT()) * initial_biomass,
                                    initial_foliage_biomass_max );
        _vt->mFrt = initial_biomass - _vt->mFol;
        _vt->dw_lst = 0.0;
        _vt->mBud = 0.0;
        _vt->mSap = 0.0;
        _vt->mCor = 0.0;
        
        
        lai_min[_vt->slot] = cbm::bound_min( lai_min[_vt->slot], _vt->mFol * (*_vt)->SLAMAX());
        n_plant[_vt->slot] = _vt->total_nitrogen();

        PlaMox_update_nitrogen_concentrations( _vt);

        n_at_planting += (n_plant[_vt->slot] - plant_n_old);
        timestep_c_assi += (initial_biomass * cbm::CCDM - plant_c_old);
    }
    else if ( cbm::is_equal( _vt->cgroup(), "grass"))
    {
        lerr_t rc_plant = m_pf.initialize_grass( _vt, sp ? sp->grass() : NULL);
        if ( rc_plant)
        {
            return LDNDC_ERR_OK;
        }

        _vt->dEmerg = -1;
        _vt->growing_degree_days = 0.0;
        _vt->dvsFlush = 0.0;
        _vt->dvsMort = 0.0;

        /* set to fixed value */
        lai_min[_vt->slot] = 0.1;

        allocation_factor_roots[_vt->slot] = (*_vt)->FRACTION_ROOT();
        allocation_factor_fruit[_vt->slot] = (*_vt)->FRACTION_FRUIT();
        allocation_factor_leafs[_vt->slot] = (*_vt)->FRACTION_FOLIAGE();
        allocation_factor_stems[_vt->slot] = (1.0 - allocation_factor_leafs[_vt->slot] - allocation_factor_roots[_vt->slot] - allocation_factor_fruit[_vt->slot]);

        _vt->mFrt   = allocation_factor_roots[_vt->slot] * initial_biomass;
        _vt->mFol   = allocation_factor_leafs[_vt->slot] * initial_biomass;
        _vt->dw_lst = allocation_factor_stems[_vt->slot] * initial_biomass;
        _vt->mBud   = allocation_factor_fruit[_vt->slot] * initial_biomass;

        _vt->ncFrt = (*_vt)->NC_FINEROOTS_MAX();
        _vt->ncFol = (*_vt)->NC_FOLIAGE_MAX();
        _vt->n_lst = (*_vt)->NC_STRUCTURAL_TISSUE_MAX() * _vt->dw_lst;
        _vt->ncBud = (*_vt)->NC_FRUIT_MAX();

        n_plant[_vt->slot] = _vt->total_nitrogen();

        PlaMox_update_nitrogen_concentrations( _vt);

        n_at_planting += (n_plant[_vt->slot] - plant_n_old);
        timestep_c_assi += (initial_biomass * cbm::CCDM - plant_c_old);
    }
    else
    {
        KLOGERROR( "I do not handle this group of species  ",
                  "[species=\"",_vt->name(),"\",group=",_vt->group(),"]");
        return  LDNDC_ERR_RUNTIME_ERROR;
    }

    // currently no concept for free available carbon
    _vt->f_fac = 0.0;

    // initialize root system
    if (root_system.size() <= _vt->slot)
    {
        root_system.resize(_vt->slot+1);
    }

    ldndc_kassert( root_system[_vt->slot] == NULL);
    root_system[_vt->slot] = LD_Allocator->construct_args< RootSystemDNDC >( 1, m_state, io_kcomm);
    if ( !root_system[_vt->slot])
    {
        KLOGERROR( "Failed to allocate root-system object");
        return  LDNDC_ERR_NOMEM;
    }
    
    root_q_vt_[_vt->slot] = 0; // no rooted layers yet
    PlaMox_update_root_structure( _vt, _vt->mFrt);

    return  LDNDC_ERR_OK;
}



lerr_t
PhysiologyPlaMox::PlaMox_harvest()
{
    EventAttributes const *  ev_harv = NULL;
    while (( ev_harv = this->m_HarvestEvents.pop()) != NULL)
    {
        MoBiLE_Plant *  vt = m_veg->get_plant( ev_harv->get( "/name", "?"));
        if ( !vt){ return LDNDC_ERR_RUNTIME_ERROR; }

        for ( size_t i = 0; i < this->plantfamilies.size(); ++i)
        {
            cbm::string_t family = ":";
            family << this->plantfamilies[i] <<":";
            if (   this->m_veg->is_family( vt->ctype(), family.c_str())
                || cbm::is_equal( vt->cname(), family.c_str()))
            {
                lerr_t  rc_harv = PlaMox_event_harvest( vt, *ev_harv);
                if ( rc_harv)
                {
                    KLOGERROR( "Handling harvest event failed  [species=", vt->name(),"]");
                    return  LDNDC_ERR_FAIL;
                }
                // TODO  delete plant..
            }
        }
    }
    return  LDNDC_ERR_OK;
}



/*!
 * @page plamox
 * @subsection PlaMox_harvest Harvest event
 * For harvest events, the following event inputs are considered:
 * - Export root wood
 * - Remains
 * - Stubble height
 *
 * The term wood with regard to the export of roots is neglected and all root parts are considered.
 * The fraction given as remains determines the fraction of straw that remains on the field.
 * If there is not remains fraction given, the amount of straw can be determined via stubble height.
 * If neither remains nor stubble height are given all aboveground biomass is removed from the field.
 */
lerr_t
PhysiologyPlaMox::PlaMox_event_harvest(
                                       MoBiLE_Plant *  _vt,
                                       EventAttributes const &  _attributes)
{
    if ( root_system[_vt->slot] == NULL)
    {
        KLOGERROR( "[BUG] ", "root_system is NULL when not expected \"",_vt->name(),"\"!");
        return  LDNDC_ERR_RUNTIME_ERROR;
    }

    bool const mulching = _attributes.get( "/mulching", false);

    double const export_root = _attributes.get( "/fraction-export-rootwood", 0.0);
    double const rootlitter_c( (1.0 - export_root) * _vt->mFrt * cbm::CCDM);
    double const rootlitter_n( (1.0 - export_root) * _vt->n_frt());
    double const root_c_export( export_root * _vt->mFrt * cbm::CCDM);
    double const root_n_export( export_root * _vt->mFrt * _vt->ncFrt);

    double const straw_c( (_vt->mFol + _vt->dw_dfol + _vt->dw_lst+ _vt->dw_dst) * cbm::CCDM);
    double const straw_n( _vt->n_fol() + _vt->n_dfol + _vt->n_lst + _vt->n_dst);

    double const fru_c( _vt->mBud * cbm::CCDM);
    double const fru_n( _vt->n_bud());

    double fru_c_export( 0.0);
    double fru_n_export( 0.0);

    double straw_c_export( 0.0);
    double straw_n_export( 0.0);

    double mulching_c( 0.0);
    double mulching_n( 0.0);

    double stubble_c( 0.0);
    double stubble_n( 0.0);

    double gdd_sum( _vt->growing_degree_days );
    double dvs_flush( _vt->dvsFlush );

    double remains_relative( _attributes.get( "/remains_relative", invalid_flt));
    double height( _attributes.get( "/height", invalid_flt));

    if (   !cbm::flt_greater_equal_zero( remains_relative)
        && !cbm::flt_greater_equal_zero( height))
    {
        if ( _vt->is_covercrop)
        {
            remains_relative = 1.0;
        }
        else
        {
            KLOGWARN( "harvest event attribute \"remains_relative\" and \"height\" not set; \"remains_relative\" set to zero",
                      " [species=", _vt->name(),"]");
        }
    }
    else  if (   cbm::flt_greater_equal_zero( remains_relative)
              && cbm::flt_greater_equal_zero( height))
    {
        KLOGWARN( "harvest event attribute \"remains_relative\" and \"height\" were both set; attribute \"remains_relative\" used",
                  " [species=", _vt->name(),"]");
    }

    if ( cbm::flt_greater_equal_zero( remains_relative))
    {
        /* no op */
    }
    else if ( cbm::flt_greater_equal_zero( height))
    {
        // 1cm stubble assumed to equal 70 kg C ha-1
        remains_relative = std::min( height * cbm::CM_IN_M * 0.007, straw_c) / straw_c;
    }
    else
    {
        KLOGERROR( "[BUG] ", "harvest event attribute \"remains_relative\" and \"height\" are invalid",
                   " [species=", _vt->name(),"]");
        return LDNDC_ERR_FAIL;
    }

    stubble_c += remains_relative * straw_c;
    stubble_n += remains_relative * straw_n;
    if ( mulching)
    {
        mulching_c += (1.0-remains_relative) * straw_c;
        mulching_n += (1.0-remains_relative) * straw_n;
    }
    else
    {
        straw_c_export += (1.0-remains_relative) * straw_c;
        straw_n_export += (1.0-remains_relative) * straw_n;
    }

    if ( _vt->is_covercrop)
    {
        stubble_c += remains_relative * fru_c;
        stubble_n += remains_relative * fru_n;
        if ( mulching)
        {
            mulching_c += (1.0-remains_relative) * fru_c;
            mulching_n += (1.0-remains_relative) * fru_n;
        }
        else
        {
            stubble_c += (1.0-remains_relative) * fru_c;
            stubble_n += (1.0-remains_relative) * fru_n;
        }
    }
    else
    {
        fru_c_export = fru_c;
        fru_n_export = fru_n;
    }

    //export
    c_total_exported += straw_c_export + fru_c_export;
    c_fruit_exported += fru_c_export;
    n_total_exported += straw_n_export + fru_n_export;
    n_fruit_exported += fru_n_export;

    ph_.accumulated_c_export_harvest += straw_c_export + fru_c_export + root_c_export;
    ph_.accumulated_n_export_harvest += straw_n_export + fru_n_export + root_n_export;

    ph_.accumulated_c_fru_export_harvest += fru_c_export;
    ph_.accumulated_n_fru_export_harvest += fru_n_export;

    //stubble litter
    sc_.c_stubble_lit3 += stubble_c * (*_vt)->LIGNIN();
    sc_.c_stubble_lit2 += stubble_c * (*_vt)->CELLULOSE();
    sc_.c_stubble_lit1 += stubble_c * (1.0 - (*_vt)->LIGNIN() - (*_vt)->CELLULOSE());
    sc_.accumulated_c_litter_stubble += stubble_c;

    sc_.n_stubble_lit3 += stubble_n * (*_vt)->LIGNIN();
    sc_.n_stubble_lit2 += stubble_n * (*_vt)->CELLULOSE();
    sc_.n_stubble_lit1 += stubble_n * (1.0 - (*_vt)->LIGNIN() - (*_vt)->CELLULOSE());
    sc_.accumulated_n_litter_stubble += stubble_n;

    //raw litter
    sc_.c_raw_lit_1_above += mulching_c * (1.0 - (*_vt)->CELLULOSE() - (*_vt)->LIGNIN());
    sc_.c_raw_lit_2_above += mulching_c * (*_vt)->CELLULOSE();
    sc_.c_raw_lit_3_above += mulching_c * (*_vt)->LIGNIN();
    sc_.accumulated_c_litter_above += mulching_c;

    sc_.n_raw_lit_1_above += mulching_n * (1.0 - (*_vt)->CELLULOSE() - (*_vt)->LIGNIN());
    sc_.n_raw_lit_2_above += mulching_n * (*_vt)->CELLULOSE();
    sc_.n_raw_lit_3_above += mulching_n * (*_vt)->LIGNIN();
    sc_.accumulated_n_litter_above += mulching_n;

    //root litter
    for ( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
    {
        double const dw_rootlitter( rootlitter_c / cbm::CCDM * _vt->fFrt_sl[sl]);
        double const n_rootlitter( dw_rootlitter * _vt->ncFrt);

        sc_.c_raw_lit_1_sl[sl] += dw_rootlitter * (1.0 - (*_vt)->CELLULOSE() - (*_vt)->LIGNIN()) * cbm::CCDM;
        sc_.c_raw_lit_2_sl[sl] += dw_rootlitter * (*_vt)->CELLULOSE() * cbm::CCDM;
        sc_.c_raw_lit_3_sl[sl] += dw_rootlitter * (*_vt)->LIGNIN() * cbm::CCDM;
        sc_.accumulated_c_litter_below_sl[sl] += dw_rootlitter * cbm::CCDM;

        sc_.n_raw_lit_1_sl[sl] += n_rootlitter * (1.0 - (*_vt)->CELLULOSE() - (*_vt)->LIGNIN());
        sc_.n_raw_lit_2_sl[sl] += n_rootlitter * (*_vt)->CELLULOSE();
        sc_.n_raw_lit_3_sl[sl] += n_rootlitter * (*_vt)->LIGNIN();
        sc_.accumulated_n_litter_below_sl[sl] += n_rootlitter;
    }

    cbm::state_scratch_t *  mcom = io_kcomm->get_scratch();
    std::string  mcom_key;

    char const *  species_name = _attributes.get( "/name", "?");

    cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:daysOnField", species_name);
    mcom->set( mcom_key.c_str(), (double)((lclock()->seconds() - seconds_crop_planting[_vt->slot]) / cbm::SEC_IN_DAY));

    cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:gddsum", species_name);
    mcom->set( mcom_key.c_str(), gdd_sum);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:dvsflush", species_name);
    mcom->set( mcom_key.c_str(), dvs_flush);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:c_fru", species_name);
    mcom->set( mcom_key.c_str(), fru_c);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:c_fru_export", species_name);
    mcom->set( mcom_key.c_str(), fru_c_export);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:n_fru", species_name);
    mcom->set( mcom_key.c_str(), fru_n);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:n_fru_export", species_name);
    mcom->set( mcom_key.c_str(), fru_n_export);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:c_straw", species_name);
    mcom->set( mcom_key.c_str(), straw_c);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:c_straw_export", species_name);
    mcom->set( mcom_key.c_str(), straw_c_export);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:n_straw", species_name);
    mcom->set( mcom_key.c_str(), straw_n);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:n_straw_export", species_name);
    mcom->set( mcom_key.c_str(), straw_n_export);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:c_stubble", species_name);
    mcom->set( mcom_key.c_str(), stubble_c);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:n_stubble", species_name);
    mcom->set( mcom_key.c_str(), stubble_n);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:c_frt", species_name);
    mcom->set( mcom_key.c_str(), rootlitter_c);

    cbm::state_scratch_t::make_item_key(  &mcom_key, "harvest:%s:n_frt", species_name);
    mcom->set( mcom_key.c_str(), rootlitter_n);

    for ( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
    {
        _vt->fFrt_sl[sl] = 0.0;
    }

    cbm::invalidate( root_q_vt_[_vt->slot]);
    _vt->rooting_depth = 0.0;

    days_after_emergence[_vt->slot] = -1;
    reserves_stem[_vt->slot] = 0.0;

    _vt->mFol = 0.0;
    _vt->dw_dfol = 0.0;
    _vt->ncFol = 0.0;
    _vt->n_dfol = 0.0;

    _vt->dw_lst = 0.0;
    _vt->n_lst = 0.0;

    _vt->dw_dst = 0.0;
    _vt->n_dst = 0.0;

    _vt->mBud = 0.0;
    _vt->mBudStart = 0.0;
    _vt->ncBud = 0.0;

    _vt->mFrt = 0.0;
    _vt->ncFrt = 0.0;

    _vt->dEmerg = -1;
    _vt->growing_degree_days = 0.0;
    _vt->dvsMort = 0.0;
    _vt->dvsFlush = 0.0;
    _vt->dvsFlushOld = 0.0;

    n_plant[_vt->slot] = 0.0;
    _vt->a_fix_n = 0.0;

    _vt->height_max = 0.0;
    _vt->height_at_canopy_start = 0.0;

    _vt->f_area = 0.0;
    for ( int  fl = 0;  fl < m_setup->canopylayers();  ++fl)
    {
        _vt->fFol_fl[fl] = 0.0;
        _vt->lai_fl[fl] = 0.0;
    }

    _vt->f_fac = 0.0;
    chill_units[_vt->slot] = 0.0;
    chill_factor[_vt->slot] = 1.0;

    yearly_cuts[_vt->slot] = 0;

    allocation_factor_leafs[_vt->slot] = 0.0;
    allocation_factor_stems[_vt->slot] = 0.0;
    allocation_factor_fruit[_vt->slot] = 0.0;
    allocation_factor_roots[_vt->slot] = 0.0;

    m_fruit_maximum[_vt->slot] = -1.0;

    LD_Allocator->destroy( root_system[_vt->slot]);
    root_system[_vt->slot] = NULL;

    return  LDNDC_ERR_OK;
}



/*!
 * @page plamox
 * @subsection PlaMox_grazing Grazing
 * After grazing the development index of the plant is set to 0.
 *
 * @subsection PlaMox_cutting Cutting
 * After cutting the development index of the plant is set to 0.
 */
lerr_t
PhysiologyPlaMox::PlaMox_management()
{
    double *agb_vt = new double[m_veg->slot_cnt()];
    for ( PlaMoxIterator vt = begin(); vt != end(); ++vt)
    {
        MoBiLE_Plant *p = (*vt);
        agb_vt[p->slot] = p->aboveground_biomass();
    }

    m_eventgraze.update_available_freshfood_c( m_veg, species_groups_select_t< species::grass >());
    for ( PlaMoxIterator vt = begin(); vt != end(); ++vt)
    {
        MoBiLE_Plant *p = (*vt);
        if ( p->group() == "grass")
        {
            lerr_t  rc_graze = m_eventgraze.event_graze_physiology( p);
            if ( rc_graze == LDNDC_ERR_EVENT_MATCH)
            {
                n_plant[p->slot] = p->total_nitrogen();
            }
        }
    }

    lerr_t  rc_cut = m_eventcut.event_cut_physiology(
                                                 m_veg,
                                                 species_groups_select_t< species::grass, species::crop >());
    if ( rc_cut == LDNDC_ERR_EVENT_MATCH)
    {
        for ( PlaMoxIterator vt = begin(); vt != end(); ++vt)
        {
            MoBiLE_Plant *p = (*vt);
            yearly_cuts[p->slot] += 1;
            n_plant[p->slot] = p->total_nitrogen();

            if ( (*p)->RATOON())
            {
                PlaMox_reset_phenology( p);
            }
        }
    }

    for ( PlaMoxIterator vt = begin(); vt != end(); ++vt)
    {
        MoBiLE_Plant *p = (*vt);
        if ( cbm::flt_less( p->aboveground_biomass(), agb_vt[p->slot]))
        {
            double const scale( p->aboveground_biomass() / agb_vt[p->slot]);
            p->dvsFlush *= scale;
            p->dvsFlushOld *= scale;
        }
    }

    delete [] agb_vt;

    return  LDNDC_ERR_OK;
}


/*!
 * @page plamox
 * @section PlaMox_phenology Phenology
 * Phenology of plant growth depends on the plant development
 * stage \f$ DVS \f$, which is defined between 0 (germination)
 * and 1 (maturity).
 * @subsection gdd Growing degree days
 */
void
PhysiologyPlaMox::PlaMox_growing_degree_days( MoBiLE_Plant *  _vt)
{
    if (cbm::flt_equal( cbm::round(_vt->growing_degree_days / 10) * 10, (*_vt)->GDD_FLOWERING()))
    {
        day_of_flowering = days_after_emergence[_vt->slot];
    }
    else if (cbm::flt_less(_vt->growing_degree_days, (*_vt)->GDD_FLOWERING())) 
    {
        day_of_flowering = 999;
    }


     if ( m_veg->is_family( _vt, "cassava"))
    {
        double const delta_gdd( FTS_TOT_);
        _vt->growing_degree_days += delta_gdd;

        double const delta_dvs( delta_gdd / (*_vt)->GDD_MATURITY());
        _vt->dvsFlush += delta_dvs;
        _vt->dvsMort += delta_dvs;
    }
    else
    {
        /* plant development only takes place for daily average temperature above
         * species specific base temperature
         */
        if ( cbm::flt_greater( mc_.nd_airtemperature, (*_vt)->GDD_BASE_TEMPERATURE()))
        {
            /*!
             * @page plamox
             *  Plant development depends on accumulated
             *  growing degree days \f$ AGDD \f$, which is the sum of growing degree days over the
             *  complete vegetation period:
             * \f[
             *  AGDD = \sum GDD
             *  \f]
             *  Growing degree days depend on daily mean temperature and a species-specific base temperature:
             * \f[
             *  GDD = (T_{avg} - GDD\_BASE\_TEMPERATURE) \; f_{chill}
             *  \f]
             *  The factor \f$f_{chill} \f$ retards plant development due to
             *  insufficient vernalization (see: vernalization).
             */
            double delta_gdd( cbm::bound_max(mc_.nd_airtemperature - (*_vt)->GDD_BASE_TEMPERATURE(), (*_vt)->GDD_MAX_TEMPERATURE()) * chill_factor[_vt->slot] * FTS_TOT_ );

            //delay emergence under severe drought
            //todo: add species dependency
            if ( (_vt->growing_degree_days < (*_vt)->GDD_EMERGENCE()))
            {
                DroughtStress  droughtstress;
                droughtstress.fh2o_ref = (*_vt)->H2OREF_A();
                delta_gdd *= droughtstress.linear_threshold( _vt->f_h2o);
            }
            _vt->growing_degree_days += delta_gdd;

            /*!
             * @page plamox
             *  Plant development \f$ \frac{d DVS}{dt} \f$ is given by:
             *  \f[
             *  \frac{d DVS}{dt} = \frac{GDD}{GDD\_MATURITY}
             *  \f]
             */
            double const delta_dvs( delta_gdd / (*_vt)->GDD_MATURITY());

            /*!
             * @page plamox
             * In addition to \f$ DVS \f$, there exists a mortality state index \f$ MOS \f$ that is calculated in
             * the same way as \f$ DVS \f$ but interpreted differently and not reset after grazing and cutting events.
             */
            _vt->dvsFlush += delta_dvs;
            _vt->dvsMort += delta_dvs;

            //retard fruit establishment if drought happens during flowering phase
            if ( m_veg->is_family( _vt, ":corn:"))
            {
                if ( cbm::flt_greater_zero( (*_vt)->GDD_FLOWERING()) &&
                     cbm::flt_greater_equal( _vt->growing_degree_days, (*_vt)->GDD_FLOWERING()) &&
                     cbm::flt_less( _vt->growing_degree_days, gdd_grain_filling[_vt->slot]))
                {
                    DroughtStress  droughtstress;
                    droughtstress.fh2o_ref = (*_vt)->H2OREF_A();
                    double const fact_d( droughtstress.linear_threshold( _vt->f_h2o));
                    gdd_grain_filling[_vt->slot] += (1.0 - fact_d) * delta_gdd;
                }
            }
        }

        /* effect of */
        if ( cbm::flt_greater_zero( (*_vt)->PHOTOPERIODISM()) &&
             cbm::flt_greater( photoperiod_max, (*_vt)->PHOTOPERIODISM()))
        {
            int const yesterday( lclock()->yearday() > 1 ? lclock()->yearday() - 1 : 365);
            double const photoperiod_yesterday( ldndc::meteo::daylength( m_setup->latitude(), yesterday));
            double const photoperiod_today( ldndc::meteo::daylength( m_setup->latitude(), lclock()->yearday()));
            if ( cbm::flt_less( photoperiod_today, photoperiod_yesterday))
            {
                _vt->dvsMort = cbm::bound_min( _vt->dvsMort,
                                               1.0 - (photoperiod_today - (*_vt)->PHOTOPERIODISM()) / (photoperiod_max - (*_vt)->PHOTOPERIODISM()));
            }
        }
    }

    if ( cbm::flt_greater( _vt->dvsFlush, 1.0))
    {
        _vt->dvsFlush = 1.0;
    }
    if ( cbm::flt_greater( _vt->dvsMort, 1.0))
    {
        _vt->dvsMort = 1.0;
    }

    /*!
     * @page plamox
     * @subsection PlaMox_emergence Emergence
     * Emergence is regulate by accumulated growing degree days \f$ AGDD \f$, drought stress and snow.
     * Three conditions must be satisfied for emergence:
     * \f[
     *  AGDD >  GDD\_EMERGENCE \\
     *  f_h2o > H2OREF\_FLUSHING \\
     *  snow < 0.01 [m]
     *  \f]
     * In case GDD\_EMERGENCE is not defined, the plant development index must be greater 5%
     */
    if ( (_vt->dEmerg < 0) &&
         cbm::flt_less( wc_.surface_ice, 0.01))
    {
        if ( cbm::flt_greater_zero( (*_vt)->GDD_EMERGENCE()))
        {
            if ( cbm::flt_greater( _vt->growing_degree_days, (*_vt)->GDD_EMERGENCE()) &&
                 cbm::flt_greater_equal( _vt->f_h2o, (*_vt)->H2OREF_FLUSHING()))
            {
                _vt->dEmerg = lclock_ref().yearday();
                days_after_emergence[_vt->slot] = 0;
            }
        }
        else
        {
            if ( cbm::flt_greater( _vt->dvsFlush, 0.05) &&
                 cbm::flt_greater_equal( _vt->f_h2o, (*_vt)->H2OREF_FLUSHING()))
            {
                _vt->dEmerg = lclock_ref().yearday();
                days_after_emergence[_vt->slot] = 0;
            }
        }
    }
}



/*!
 * @page plamox
 * @subsection PlaMox_vernalization Vernalization
 * Vernalization is only implemented for crops. The following species-specific parameters determine
 * vernalization:
 *  - \f$ CHILL\_UNITS \f$
 *  - \f$ CHILL\_TEMP\_MAX \f$
 *  - \f$ GDD\_FLOWERING \f$
 */
void
PhysiologyPlaMox::PlaMox_vernalization( MoBiLE_Plant *  _vt)
{
    if ( _vt->groupId() == SPECIES_GROUP_GRASS)
    {
        chill_factor[_vt->slot] = 1.0;
        return;
    }


    /*!
     * @page plamox
     * The state of chilling if calculated following @cite haenninen:1990a (see: @ref veglibs_vernalization).
     */
    if ( cbm::flt_greater_zero( (*_vt)->CHILL_UNITS()) &&
         cbm::flt_greater_zero( (*_vt)->GDD_FLOWERING()))
    {

        double const temp_avg( cbm::bound_min( 0.0, mc_.nd_airtemperature));

        chill_units[_vt->slot] += get_chilling_units( temp_avg, (*_vt)->CHILL_TEMP_MAX()) * FTS_TOT_;
        chill_factor[_vt->slot] = get_chilling_factor( _vt->growing_degree_days,
                                                       (*_vt)->GDD_FLOWERING(),
                                                       chill_units[_vt->slot],
                                                       (*_vt)->CHILL_UNITS());
    }
    else
    {
        chill_factor[_vt->slot] = 1.0;
    }

    if ( cbm::flt_greater( _vt->growing_degree_days, (*_vt)->GDD_FLOWERING()))
    {
        chill_factor[_vt->slot] = 1.0;
    }
}



/*!
 * @page plamox
 * @subsection PlaMox_allocation Allocation
 * Allocation of assimilated carbon and nitrogen is determined by the plant development stage \f$ DVS \f$.
 * PlaMox distinguishes the following compartments:
 *  - Fruit / Reserves
 *  - Roots
 *  - Stem
 *  - Leaves
 *
 * The plant growth is reduced by a deficient N-content of the leaves, which limits photosynthesis.
 * Then, also the C to N ratio increases.
 * @subsubsection PlaMox_cropallocation Crops
 * \n
 */
void
PhysiologyPlaMox::PlaMox_plant_development_crop( MoBiLE_Plant *  _vt)
{
    if ( cbm::flt_greater_zero( _vt->dvsFlush))
    {
        /*!
         * @page plamox
         *  The fruit fraction \f$ \theta_{fruit} \f$ is given by:
         *  \f[
         *  \theta_{fruit} = \left\{\begin{array}{cc} 0 & AGDD \le GDD\_GRAIN\_FILLING \\
         *  FRACTION\_FRUIT \cdot \frac{AGDD - GDD\_GRAIN\_FILLING}{GDD\_MATURITY - GDD\_GRAIN\_FILLING}
         *  & AGDD > GDD\_GRAIN\_FILLING \end{array} \right. \label{eq2}
         *  \f]
         * 
         * If there was heatstress during the flowering phase, the fruit fraction is reduced by the factor influence\_heat\_reduction\_grainfilling
         * as calculated in @ref plamox_common_heat_stress_limitation
         * 
         *  * The plant allocation into the fruit is reduced if TMINCRIT() and M\_FRUIT\_OPT() are set. 
         * If temperatures are higher than TMINCRIT(), the grain development capacity (m\_fruit\_max) is reduced. 
         * 
         * m\_fruit\_max = M\_FRUIT\_OPT() * (1- influence\_heat\_reduction\_grainfilling)
         * 
         * 
         */
        if ( _vt->growing_degree_days <= gdd_grain_filling[_vt->slot])
        {
            // no buds for no sufficient cumulative degree days
            allocation_factor_fruit[_vt->slot] = 0.0;
        }
        else
        {
            double const grain_development( cbm::bound_max( (_vt->growing_degree_days - gdd_grain_filling[_vt->slot]) /
                                                            ((*_vt)->GDD_MATURITY() - gdd_grain_filling[_vt->slot]),
                                                             1.0));
            // approaching full development of grains linearly for growing_degree_days
            double m_fru_pot( grain_development * (*_vt)->FRACTION_FRUIT() * _vt->total_biomass());

            // Reduce Maximum Fruit allocation 
            
            if (cbm::flt_less(influence_heat_reduction_grainfilling, 1.0))
            {
                if ( cbm::flt_equal( m_fruit_maximum[_vt->slot], -1.0))
                {
                    KLOGERROR("M_FRUIT_OPT() necessary for heat stress calculation");
                }
                else 
                {
                    m_fruit_maximum[_vt->slot] = (*_vt)->M_FRUIT_OPT() * (1 - influence_heat_reduction_grainfilling);
                }
            }
            

            if ( cbm::flt_greater_zero( m_fruit_maximum[_vt->slot]))
            {
                m_fru_pot = cbm::bound_max( m_fru_pot, grain_development * m_fruit_maximum[_vt->slot]);
            }
         
            double scale_n( 1.0);
            if ( cbm::flt_less( _vt->nc_bud(), (*_vt)->NC_FRUIT_MIN()))
            {
                scale_n = _vt->nc_bud() / (*_vt)->NC_FRUIT_MIN();
            }
            if ( cbm::flt_greater( m_fru_pot * scale_n, _vt->mBud))
            {
                    allocation_factor_fruit[_vt->slot] = 1.0;
                    allocation_factor_roots[_vt->slot] = 0.0;
                    allocation_factor_leafs[_vt->slot] = 0.0;
                    allocation_factor_stems[_vt->slot] = 0.0;
            }
            else
            {
                allocation_factor_fruit[_vt->slot] = 0.0;
            }
        }
        
        /*! if no C is being allocated to the fruit */
        if ( ! cbm::flt_greater_zero( allocation_factor_fruit[_vt->slot]))
        {
            /*!
             * @page plamox
             * The root fraction \f$ \theta_{root} \f$ is constant over time before grain filling starts and then decreases to \f$ FRACTION\_ROOT \f$.
             * \f[
             * \theta_{root} = (1-\theta_{fruit})/(1-FRACTION\_FRUIT) \cdot FRACTION\_ROOT
             * \f]
             * For some species families specific calculations exist:
             */
            if ( m_veg->is_family( _vt, ":rice:"))
            {
                /*!
                 * @page plamox
                 * - Rice
                 * \f[
                 * \theta_{root} = FRACTION\_ROOT - 0.5 \cdot DVS \cdot FRACTION\_ROOT
                 * \f]
                 * According to @cite yoshida:1981a, root fraction of rice declines from about
                 * 20% at seedling stage to 10% at maturity, which can be reflected by setting the
                 * species-specific parameter \f$ FRACTION\_ROOT = 0.2 \f$.
                 */
                double const root_ratio( _vt->belowground_biomass() / _vt->total_biomass());
                double const root_ratio_target( (*_vt)->FRACTION_ROOT() - (0.5 * _vt->dvsFlush * (*_vt)->FRACTION_ROOT()));
                if ( cbm::flt_less( root_ratio, root_ratio_target))
                {
                    allocation_factor_roots[_vt->slot] = 0.8;
                }
                else
                {
                    allocation_factor_roots[_vt->slot] = root_ratio_target;
                }

                //correct root growth due to transplanting shock
                if ( transplanting_shock_vt[_vt->slot] > 1.0)
                {
                    allocation_factor_roots[_vt->slot] = 0.8;
                }
                else if ( cbm::flt_greater_zero( transplanting_shock_vt[_vt->slot]))
                {
                    allocation_factor_roots[_vt->slot] = allocation_factor_roots[_vt->slot] + (0.8 - allocation_factor_roots[_vt->slot]) * transplanting_shock_vt[_vt->slot];
                }
            }
            else if ( m_veg->is_family( _vt, ":corn:"))
            {
                /*!
                 * @page plamox
                 * - Corn
                 * \f[
                 * \theta_{root} = \frac{1-\theta_{fruit}}{1 + \frac{1}{RS}}
                 * \f]
                 *  with the root-shoot ratio \f$ RS \f$ given by :
                 * \f[
                 * RS = 0.45 \cdot \left (0.15 + 0.5 \cdot e^{-3 \cdot DVS} \right)
                 * \f]
                 */
                //double const root_shoot_ratio( 0.45 * (0.15 + 0.5 * std::exp(-3.0 * _vt->dvsFlush)));
                double const root_shoot_ratio( 0.15 + 0.53 * std::exp( -0.03 * days_after_emergence[_vt->slot]));
                allocation_factor_roots[_vt->slot] = ((1.0 - allocation_factor_fruit[_vt->slot]) / (1.0 + 1.0 / root_shoot_ratio));
            }
            else if ( m_veg->is_family( _vt, ":rapeseed:"))
            {
                /*!
                 * @page plamox
                 * - Rapeseed
                 * \f[
                 * \theta_{root} = \frac{1-\theta_{fruit}}{1 + \frac{1}{RS}}
                 * \f]
                 *  with the root-shoot ratio \f$ RS \f$ given by :
                 * \f[
                 * RS = 0.3 - 0.22 \cdot DVS
                 * \f]
                 */
                double const root_shoot_ratio( 0.3 - _vt->dvsFlush * (0.3 - 0.08));
                allocation_factor_roots[_vt->slot] = ((1.0 - allocation_factor_fruit[_vt->slot]) / (1.0 + 1.0 / root_shoot_ratio));
            }
            else if ( m_veg->is_family( _vt, "milt"))
            {
                /*!
                 * @page plamox
                 * - Milt
                 */
                if ( _vt->growing_degree_days < (*_vt)->GDD_EMERGENCE())
                {
                    allocation_factor_roots[_vt->slot] = 0.5;
                }
                else if ( _vt->growing_degree_days < gdd_grain_filling[_vt->slot])
                {
                    allocation_factor_roots[_vt->slot] = cbm::bound( 0.0, 0.5 - 0.5 * _vt->growing_degree_days / gdd_grain_filling[_vt->slot], 1.0 - allocation_factor_fruit[_vt->slot]);
                }
                else
                {
                    allocation_factor_roots[_vt->slot] = 0.0;
                }
            }
            else if ( m_veg->is_family( _vt, "sorg"))
            {
                /*!
                 * @page plamox
                 * - Sorg
                 */
                if ( _vt->growing_degree_days < (*_vt)->GDD_FLOWERING())
                {
                    allocation_factor_roots[_vt->slot] = cbm::bound( 0.0, 0.5, 1.0 - allocation_factor_fruit[_vt->slot]);
                }
                else if ( _vt->growing_degree_days < gdd_grain_filling[_vt->slot])
                {
                    allocation_factor_roots[_vt->slot] = cbm::bound( 0.0, 0.087, 1.0 - allocation_factor_fruit[_vt->slot]);
                }
                else
                {
                    allocation_factor_roots[_vt->slot] = 0.0;
                }
            }
            else if ( m_veg->is_family( _vt, ":wheat:"))
            {
                /*!
                * @page plamox
                * - Wheat
                * \f[
                * target_{root} = 1 - DVS \cdot 0.5 + DVS \cdot FRACTION\_ROOT
                * \f]
                * \f[
                * value_{root} = \frac{fineroots}{total\_biomass}
                * \f]
                * If the target biomass is larger than the current value, 
                \f[ \theta_{root} =  target_{root} \f]
                */
                double const final_target( (*_vt)->FRACTION_ROOT());
                double const current_target( (1.0 - _vt->dvsFlush) * 0.5 + _vt->dvsFlush * final_target);
                double const current_value( cbm::flt_greater_zero(_vt->total_biomass()) ? _vt->mFrt / _vt->total_biomass() : (*_vt)->FRACTION_ROOT());
                if ( cbm::flt_greater( current_target, current_value))
                {
                    allocation_factor_roots[_vt->slot] = cbm::bound_max( current_target, (1.0 - allocation_factor_fruit[_vt->slot]));
                }
                else
                {
                    allocation_factor_roots[_vt->slot] = 0.0;
                }
            }
            else
            {
                allocation_factor_roots[_vt->slot] = ((1.0 - allocation_factor_fruit[_vt->slot]) * (*_vt)->FRACTION_ROOT() / (1.0 - (*_vt)->FRACTION_FRUIT()));
            }

            /*!
             * @page plamox
             * Stem and leaf fraction are given by:
             * - If the number of growing degree days is larger than GDD_STEM_ELONGATION = \b 0 (for crops),
             * no C is allocated to stems, but only to leaves.
             * - Otherwise, and in case of drought, no C is allocated to leaves. Instead it goes into stems.
             * - Otherwise, and without drought, it is
             * \f[
             * \theta_{stem} = (1 - \theta_{fruit} - \theta_{root}) \cdot (1-FALEAF)\\
             * \theta_{leaf} = 1 - \theta_{fruit} - \theta_{root} - \theta_{stem}
             * \f]
             * where \f$FALEAF\f$ is the fraction of straw (leaves + stems) forming leaves.
             */
            double const frac_straw( 1.0 - allocation_factor_fruit[_vt->slot] - allocation_factor_roots[_vt->slot]);

            DroughtStress  droughtstress;
            droughtstress.fh2o_ref = (*_vt)->H2OREF_LEAF_GROWTH();

            double const mfoldem( cbm::bound_max( 0.3 + _vt->height_max / (*_vt)->HEIGHT_MAX(), 1.0)
                                  * (*_vt)->MFOLOPT() * fractional_cover[_vt->slot] * droughtstress.linear_threshold( _vt->f_h2o));
            if (   cbm::flt_greater_zero( _vt->mFol)
                && cbm::flt_greater_equal( _vt->growing_degree_days, (*_vt)->GDD_STEM_ELONGATION()))
            {
                double const lst_fraction_now( _vt->dw_lst / (_vt->mFol + _vt->dw_lst));
                double const lst_fraction_final( 1.0 - (*_vt)->FALEAF());

                if ( cbm::flt_greater( _vt->mFol, mfoldem))
                {
                    allocation_factor_leafs[_vt->slot] = 0.0;
                    allocation_factor_stems[_vt->slot] = frac_straw;
                }
                else if ( cbm::flt_less( lst_fraction_now, cbm::bound( 0.0,
                                                                       (_vt->growing_degree_days - (*_vt)->GDD_STEM_ELONGATION()) /
                                                                       (gdd_grain_filling[_vt->slot] - (*_vt)->GDD_STEM_ELONGATION()),
                                                                       1.0) * lst_fraction_final))
                {
                    allocation_factor_leafs[_vt->slot] = 0.0;
                    allocation_factor_stems[_vt->slot] = frac_straw;
                }
                else
                {
                    allocation_factor_leafs[_vt->slot] = frac_straw * (*_vt)->FALEAF();
                    allocation_factor_stems[_vt->slot] = frac_straw * (1.0 - (*_vt)->FALEAF());
                }
            }
            else
            {
                if ( cbm::flt_greater( _vt->mFol, mfoldem))
                {
                    allocation_factor_leafs[_vt->slot] = 0.0;
                    allocation_factor_stems[_vt->slot] = frac_straw;
                }
                else
                {
                    allocation_factor_leafs[_vt->slot] = frac_straw;
                    allocation_factor_stems[_vt->slot] = 0.0;
                }
            }

            if ( m_veg->is_family( _vt, ":wheat:") &&
                 cbm::flt_less_equal( _vt->growing_degree_days, (*_vt)->GDD_FLOWERING()))
            {
                reserves_stem[_vt->slot] = 0.3 * _vt->growing_degree_days / (*_vt)->GDD_FLOWERING() * _vt->dw_lst;
            }
            else if ( m_veg->is_family( _vt, ":barley:") &&
                 cbm::flt_less_equal( _vt->growing_degree_days, (*_vt)->GDD_FLOWERING()))
            {
                reserves_stem[_vt->slot] = 0.1 * _vt->growing_degree_days / (*_vt)->GDD_FLOWERING() * _vt->dw_lst;
            }
        }

        /***********************/
        /* transplanting shock */
        /***********************/

        transplanting_shock_vt[_vt->slot] = std::max(transplanting_shock_vt[_vt->slot] - 0.03 * FTS_TOT_, 0.0);
    }
}



/*!
 * @page plamox
 * @subsubsection PlaMox_grassallocation Grass
 * \n
 */
lerr_t
PhysiologyPlaMox::PlaMox_allocation_grass( MoBiLE_Plant *  _vt)
{
    if ( _vt->dvsFlush > 0.0)
    {
        // total plant dry weight
        double const total_dw( _vt->total_biomass());

        /*!
         * @page plamox
         * The reserve/fruit fraction (\f$ \theta_{fruit}\f$) increases linearly with the plant development in accordance to \f$ FRACTION\_FRUIT\f$:
         * \f[
         *  \theta_{fruit} = DVS \cdot FRACTION\_FRUIT
         *  \f]
         */
        if ( _vt->mBud < (*_vt)->FRACTION_FRUIT() / (*_vt)->FRACTION_FOLIAGE() * (*_vt)->MFOLOPT())
        {
            allocation_factor_fruit[_vt->slot] = _vt->dvsMort * (*_vt)->FRACTION_FRUIT();
        }
        else
        {
            allocation_factor_fruit[_vt->slot] = 0.0;
        }


        //drought influence on root fraction (currently not considered)
        //DroughtStress  droughtstress;
        //droughtstress.fh2o_ref = (*_vt)->H2OREF_A();
        //double const drought_stress = droughtstress.linear( _vt->f_h2o);
        //double const allocation_factor_rootsdrought_stress( (*_vt)->FRACTION_ROOT());

        /*!
         * @page plamox
         * A cutting event influences the root/shoot ratio by a factor \f$ \gamma_{roots}\f$ (here determined by the fraction of roots \f$ \theta_{roots}\f$): \n
         * - Before the first cutting of the year: \n
         * \f$ \gamma_{roots} = \frac{1.0}{1.0 \; + \; SHOOT\_STIMULATION\_REPROD} \f$, with \f$SHOOT\_STIMULATION\_REPROD\f$ = \b 0 \n\n
         * - After the first cutting of the year: \n
         * \f$ \gamma_{roots} = 1.0 \f$
         */
        double const cut_factor( (yearly_cuts[_vt->slot] == 0) ? 1.0 / (1.0 + (*_vt)->SHOOT_STIMULATION_REPROD()) : 1.0);

        /*!
         * @page plamox
         * The root fraction is given by:
         * \f[
         * \theta_{roots} = (1.0 - \theta_{fruit}) \frac{\gamma_{roots} \; FRACTION\_ROOT}{1 - FRACTION\_FRUIT - (1- \gamma_{roots}) \; FRACTION\_ROOT}
         * \f]
         * As a default or after the first cutting of the year it is
         * \f[
         * \theta_{roots} = \frac{1.0 - \theta_{fruit}}{1 - FRACTION\_FRUIT} \cdot FRACTION\_ROOT \, .
         * \f]
         */
        allocation_factor_roots[_vt->slot] = (1.0 - allocation_factor_fruit[_vt->slot]) * cut_factor * (*_vt)->FRACTION_ROOT() / (1.0 - (*_vt)->FRACTION_FRUIT() - (1.0 - cut_factor) * (*_vt)->FRACTION_ROOT());

        /*!
         * @page plamox
         * If the current root mass is higher than predicted by the allocation factor, the root allocation factor is decreased exponentially.
         */
        if ( _vt->mFrt > (allocation_factor_roots[_vt->slot] * total_dw))
        {
            double const exponent( 10.0);
            allocation_factor_roots[_vt->slot] *= std::exp(exponent * (1.0 - (_vt->mFrt / (allocation_factor_roots[_vt->slot] * total_dw))));
        }

        /*!
         * @page plamox
         * The straw fraction is given by
         * \f[
         * FRACTION\_STRAW = 1 - \theta_{roots} - \theta_{fruit} \, .
         * \f]
         */
        double const frac_straw( 1.0 - allocation_factor_roots[_vt->slot] - allocation_factor_fruit[_vt->slot]);

        /*!
         * @page plamox
         * The current foliage to straw ratio is given by
         * \f[
         * faleaf = \frac{m_{fol}}{m_{fol}+m_{stem}} \, .
         * \f]
         * If the foliage biomass, \f$ m_{fol}\f$, is 0, \f$ faleaf = 0 \f$.
         */
        if (   cbm::flt_greater_zero( _vt->mFol)
            && cbm::flt_greater( _vt->dvsFlush, (*_vt)->GDD_STEM_ELONGATION() / (*_vt)->GDD_MATURITY()))
        {
            double const faleaf( _vt->mFol / (_vt->mFol + _vt->dw_lst));

            /* increase foliage allocation if current foliage to straw ratio is lower than predicted by FALEAF */
            double const faleafopt( (faleaf < (*_vt)->FALEAF()) ? 0.99 : (*_vt)->FALEAF());
            allocation_factor_leafs[_vt->slot] = frac_straw * faleafopt;
            allocation_factor_stems[_vt->slot] = frac_straw - allocation_factor_leafs[_vt->slot];
        }
        else
        {
            allocation_factor_leafs[_vt->slot] = frac_straw;
            allocation_factor_stems[_vt->slot] = 0.0;
        }
    }

    return LDNDC_ERR_OK;
}



/*!
 *  Translocation of stored carbon from buds to foliage,
 *  e.g., during spring, after defoliation (grazing, cutting)
 *
 *  \b Nitrogen concentration: \n
 *  Nitrogen concentrations are updated in each compartment
 *  update_nitrogen_concentrations()
 */
lerr_t
PhysiologyPlaMox::PlaMox_bud_burst( MoBiLE_Plant *  _vt)
{
    if (   cbm::flt_greater( (1.0 - _vt->dvsMort) * _vt->mBud, allocation_factor_fruit[_vt->slot] * _vt->total_biomass())
        && cbm::flt_less( wc_.surface_ice, 0.01)
        && (_vt->dEmerg >= 0)
        && (_vt->dvsMort < 0.99))
    {
        double const retranslocate( 0.05 * FTS_TOT_ * _vt->mBud);
        _vt->mBud -= retranslocate;
        _vt->mFol += retranslocate;
    }

    /* nitrogen concentration */
    PlaMox_update_nitrogen_concentrations( _vt);

    return LDNDC_ERR_OK;
}



/*!
 * @page plamox
 * @section redistribution Redistribution
 * If some reserves exist in the stem and after grain filling, some biomass from the stem goes into
 * the grain (reproductive tissue).
 */
lerr_t
PhysiologyPlaMox::PlaMox_redistribution( MoBiLE_Plant *  _vt)
{
    if ( cbm::flt_greater_zero( reserves_stem[_vt->slot]) &&
         //~ cbm::flt_greater( _vt->growing_degree_days, (*_vt)->GDD_FLOWERING()) &&
         cbm::flt_greater( _vt->growing_degree_days, gdd_grain_filling[_vt->slot]))
    {
        //security if _vt->dw_lst for whatever reason is less than reserves_stem[_vt->slot]
        reserves_stem[_vt->slot] = cbm::bound_max( reserves_stem[_vt->slot], 0.99 * _vt->dw_lst);

        double const grain_development( cbm::bound_max( (_vt->growing_degree_days - gdd_grain_filling[_vt->slot]) /
                                                        ((*_vt)->GDD_MATURITY() - gdd_grain_filling[_vt->slot]),
                                                         1.0));
        // approaching full development of grains linearly for growing_degree_days
        double const m_fru_pot( grain_development * (*_vt)->FRACTION_FRUIT() * _vt->total_biomass());
        if ( cbm::flt_greater( m_fru_pot, _vt->mBud))
        {
            double const redistribute_pot( m_fru_pot - _vt->mBud);
            if ( cbm::flt_greater( reserves_stem[_vt->slot], redistribute_pot))
            {
                reserves_stem[_vt->slot] -= redistribute_pot;
                _vt->dw_lst -= redistribute_pot;
                _vt->mBud += redistribute_pot;
            }
            else
            {
                _vt->dw_lst -= reserves_stem[_vt->slot];
                _vt->mBud += reserves_stem[_vt->slot];
                reserves_stem[_vt->slot] = 0.0;
            }
        }
    }

    return LDNDC_ERR_OK;
}



/*!
 * @page plamox
 * @section plamox-photosynthesis Photosynthesis
 *
 * @details
 * Actual photosynthesis is calculated by the external model PhotoFarquhar (Berry Ball).
 * This requires the canopy height specific information of: \n
 * - Rubisco activity
 * - Electron transport
 * - Photorespiration
 *
 * The latter two quantities are calculated depending on the rusbisco activity and with the species specific parameters
 * QJVC (Maximum electron transport rate and RubP saturated rate of carboxylation) and QRD25, respectively.
 *
 * The rubisco activity depends on the species specific parameter VCMAX25 (Maximum RubP saturated rate of carboxylation
 * at 25oC for sun leaves).
 * Further, the following properties are factored in:
 * - Severe drought stress reduces enzyme activity
 * - Plant age reduces enzyme activity
 * - Temperature: Heat and frost stress
 * - Nitrogen availability
 */

 /* Influencing factors for calculation of rubisco activity, electron transport and dark respiration are: \n
 *
 * \b Droughtstress: \n
 *  Severe drought stress reduces enzyme activity \n
 *  \f$ f_d = \f$ DroughtStress::linear_threshold() \n\n
 *
 * \b Plant \b age: \n
 *  Plant age reduces enzyme activity \n
 *  \f$ f_a = \f$ get_age_factor() \n\n
 *
 * \b Temperature \b sensitivity: \n
 *  Effect of temperature on enzyme activity \n
 *  \f$ f_t = \f$ get_frost_factor() \n\n
 *
 * \b Nitrogen \b availability: \n
 *  Effect of nitrogen availability on enzyme activity \n
 *  \f$ f_n = \f$ get_nitrogen_deficiency() \n\n
 *
 * \b Factor \b combination
 *  \f$ f_{all} = f_d f_a f_c f_f f_t f_n \f$
 */
lerr_t
PhysiologyPlaMox::PlaMox_photosynthesis( MoBiLE_Plant *  _vt)
{
    DroughtStress  droughtstress;
    droughtstress.fh2o_ref = (*_vt)->H2OREF_A();
    double const fact_d( droughtstress.linear_threshold( _vt->f_h2o));
    double const fact_a( PlaMox_get_age_factor( _vt));
    double const fact_t( PlaMox_get_frost_factor( _vt, *mc_temp));
    double const fact_h( PlaMox_get_heat_factor( _vt));
    double const fact_n( PlaMox_get_nitrogen_deficiency( _vt));

    double const fact_all( cbm::bound_max( fact_d * fact_t * fact_h * fact_n, fact_a));

    /* update activity of photosynthesis apparat */
    if ( cbm::flt_greater_zero( fact_all))
    {
        for ( size_t  fl = 0;  fl < _vt->nb_foliagelayers();  ++fl)
        {
            //rubisco activity
            _vt->vcAct25_fl[fl] = (*_vt)->VCMAX25() * fact_all;

            //electron transport under standard conditions
            _vt->jAct25_fl[fl]  = _vt->vcAct25_fl[fl] * (*_vt)->QJVC();

            //photorespiration under standard conditions
            _vt->rdAct25_fl[fl] = _vt->vcAct25_fl[fl] * (*_vt)->QRD25();
        }
    }

    for (size_t fl = 0; fl < _vt->nb_foliagelayers(); ++fl)
    {
        m_photo.vpd_fl[fl] = mc_.vpd_fl[fl];
        m_photo.rh_fl[fl] = cl_.rel_humidity_subday( lclock_ref());
        m_photo.temp_fl[fl] = mc_.temp_fl[fl];
        m_photo.parsun_fl[fl] = mc_.parsun_fl[fl];
        m_photo.parshd_fl[fl] = mc_.parshd_fl[fl];
        m_photo.tFol_fl[fl] = mc_.tFol_fl[fl];
        m_photo.co2_concentration_fl[fl] = ac_.ts_co2_concentration_fl[fl];
        m_photo.sunlitfoliagefraction_fl[fl] = mc_.ts_sunlitfoliagefraction_fl[fl];
    }
    m_photo.nd_airpressure = mc_.nd_airpressure;
    m_photo.set_vegetation_base_state( _vt);
    m_photo.set_vegetation_non_stomatal_water_limitation_state( 0.0, 0.0, 0.0);

    m_photo.solve();

    m_photo.get_vegetation_state( _vt);

    double const carbonuptake_sum( cbm::sum( _vt->carbonuptake_fl, _vt->nb_foliagelayers()));

    if ( cbm::flt_greater_zero( carbonuptake_sum))
    {
        timestep_c_assi += carbonuptake_sum;
        double const dw_assi( carbonuptake_sum / cbm::CCDM);

        _vt->mBud += dw_assi * allocation_factor_fruit[_vt->slot];
        _vt->mFrt += dw_assi * allocation_factor_roots[_vt->slot];
        _vt->mFol += dw_assi * allocation_factor_leafs[_vt->slot];
        _vt->dw_lst += dw_assi * allocation_factor_stems[_vt->slot];

        dcFru[_vt->slot] = allocation_factor_fruit[_vt->slot] * carbonuptake_sum;
        _vt->dcFrt = allocation_factor_roots[_vt->slot] * carbonuptake_sum;
        _vt->dcFol = allocation_factor_leafs[_vt->slot] * carbonuptake_sum;
        dcLst[_vt->slot] = allocation_factor_stems[_vt->slot] * carbonuptake_sum;

        /*! \b Nitrogen \b concentration: \n
         *  Nitrogen concentrations are updated in each compartment
         *  update_nitrogen_concentrations()
         */
        PlaMox_update_nitrogen_concentrations( _vt);
    }

    return LDNDC_ERR_OK;
}



/*!
 * @page plamox
 * @section plamox_nitrogen_uptake Nitrogen uptake
 */
void
PhysiologyPlaMox::PlaMox_nitrogen_uptake( MoBiLE_Plant *  _vt)
{
    /*!
     * @page plamox
     * Nitrogen availability can be dependent on location specific N-distribution.
     * Only the share \f$ \phi_L \f$ of total N that is either located close to the
     * plant or that is homogenously distributed is available.
     */
    double plant_available_n( 0.0);
    double location_specific_n( 0.0);
    for ( std::map< std::string , double>::iterator it = sc_.n_subdivision.begin();
          it != sc_.n_subdivision.end(); ++it)
    {
        if ( location[_vt->slot] == it->first)
        {
            plant_available_n = cbm::bound_max( plant_available_n + it->second, 1.0);
        }
        location_specific_n = cbm::bound_max( location_specific_n + it->second, 1.0);
    }
    plant_available_n = cbm::bound_max( 1.0 - location_specific_n + plant_available_n, 1.0);

    /*!
     * @page plamox
     * Daily nitrogen demand is calculated by: \n
     * n_opt() \f$ - \f$ total_nitrogen() \n\n
     */
    double n_living_plant( cbm::bound_min( 0.0, n_plant[_vt->slot] - _vt->n_dst - _vt->n_dfol));
    double const n_demand( PlaMox_n_opt( _vt) - n_living_plant);

    if ( cbm::flt_greater_zero( n_demand))
    {
        /*!
         * @page plamox
         * Nitrogen uptake is calculated for every layer individually.
         * Only layers containing roots are considered.
         */
        for (size_t sl = 0; sl < root_q_vt_[_vt->slot]; sl++)
        {
            /* mass of fine roots per layer [kg DW] */
            double const mFrt( cbm::bound_min( 0.01, _vt->mFrt) * _vt->fFrt_sl[sl]);

            if ( !cbm::flt_greater_zero( mFrt))
            {
                break;
            }

            /*!
             * @page plamox
             * Temperature dependency of N uptake is given by:
             * \f[
             * \phi_T =
             * \begin{cases}
             * & 0, \;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\
             * T < 0.8 \cdot TLIMIT \\
             * & \frac{t - 0.8 \cdot TLIMIT}{TLIMIT - 0.8 \cdot TLIMIT}, \
             * 0.8 \cdot TLIMIT < T < TLIMIT \\
             * & 1, \;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\
             * T > TLIMIT \\
             * \end{cases}
             * \f]
             */
            double const fact_t( m_pf.nitrogen_uptake_temperature_dependency(
                                                                     0.8 * (*_vt)->TLIMIT(),
                                                                     (*_vt)->TLIMIT(),
                                                                     temp_sl[sl]));

            /*!
             * @page plamox
             * Nitrogen uptake of \f$ N_x \f$ is given by;
             * \f[
             * \frac{dN_x}{dt} = US\_NH4 \cdot N_x \cdot m_{roots} \cdot \phi_L \cdot \phi_T \
             *                   \frac{N_x}{N_x + K\_MM\_NITROGEN\_UPTAKE}
             * \f]
             */
            double const nh4_up( cbm::bound_max(
                                    (*_vt)->US_NH4() * mFrt * plant_available_n * fact_t * FTS_TOT_
                                    * nh4_sl[sl] / (nh4_sl[sl] + (*_vt)->K_MM_NITROGEN_UPTAKE() * sc_.h_sl[sl]),
                                    0.9 * nh4_sl[sl]));
            double const no3_up( cbm::bound_max(
                                    (*_vt)->US_NO3() * mFrt * plant_available_n * fact_t * FTS_TOT_
                                    * no3_sl[sl] / (no3_sl[sl] + (*_vt)->K_MM_NITROGEN_UPTAKE() * sc_.h_sl[sl]),
                                    0.9 * no3_sl[sl] ));
            double const don_up( cbm::bound_max(
                                    (*_vt)->US_DON() * mFrt * plant_available_n * fact_t * FTS_TOT_
                                    * don_sl[sl] / (don_sl[sl] + (*_vt)->K_MM_NITROGEN_UPTAKE() * sc_.h_sl[sl]),
                                    0.9 * don_sl[sl]));

            if ( cbm::flt_greater_zero( nh4_up))
            {
                nh4_sl[sl] = cbm::bound_min( 0.0, nh4_sl[sl] - nh4_up);
                n_plant[_vt->slot] += nh4_up;
                ph_.accumulated_nh4_uptake_sl[sl] += nh4_up;
            }

            if ( cbm::flt_greater_zero( no3_up))
            {
                no3_sl[sl] = cbm::bound_min( 0.0, no3_sl[sl] - no3_up);
                n_plant[_vt->slot] += no3_up;
                ph_.accumulated_no3_uptake_sl[sl] += no3_up;
            }

            if ( cbm::flt_greater_zero(don_up))
            {
                don_sl[sl] = cbm::bound_min( 0.0, don_sl[sl] - don_up);
                n_plant[_vt->slot] += don_up;
                ph_.accumulated_don_uptake_sl[sl] += don_up;
            }
        }

        PlaMox_update_nitrogen_concentrations( _vt);
    }

    if ( have_automatic_nitrogen)
    {
        n_living_plant = cbm::bound_min( 0.0, n_plant[_vt->slot] - _vt->n_dst - _vt->n_dfol);
        double const missing_n_demand( PlaMox_n_opt( _vt) - n_living_plant);
        if ( cbm::flt_greater_zero( missing_n_demand))
        {
            n_plant[_vt->slot] += missing_n_demand;
            automatic_nitrogen_uptake += missing_n_demand;

            PlaMox_update_nitrogen_concentrations( _vt);
        }
    }
}



/*!
 * @page plamox
 * @section plamox_nitrogen_fixation Nitrogen fixation
 *
 * Two different approaches are considered: \n
 * - Nitrogen fixation based on potential nitrogen fixation rate under consideration of water availability, temperature and the nodule surface.
 *   This approach is chosen as soon as: \f$ NFIX\_RATE > 0.0\f$.
 * - Nitrogen fixation based on total fixable nitrogen (TFN) amount and plant development.
 *   This approach is chosen as soon as: \f$ NFIX\_RATE = 0.0\f$ and \f$ INI\_N\_FIX > 0.0\f$.
 */
lerr_t
PhysiologyPlaMox::PlaMox_nitrogen_fixation( MoBiLE_Plant *  _vt)
{
    /*!
     * @page plamox
     * \b Approach \f$ NFIX\_RATE > 0.0\f$ \n
     * - \b Plant \b N \b demand: \n
     *    - \f$ \text{N}_{\text{demand}} = \text{N}_{\text{optimum}} - \text{N}_{\text{plant}} \f$ @see n_opt() \n
     *
     * - \b Water \b availability @cite sinclair:1986a \n
     *    - \f$ f_{w} = \f$ NitrogenFixation::get_fact_water() \n
     *
     * - \b Temperature \n
     *    - \f$ f_{t} = \f$ NitrogenFixation::get_fact_temperature() \n
     *
     * - \b Nitrogen \b availability \n
     *    - Hurley Pasture Model (Thornley, 1998) \n
     *    - \f$ f_{n} = \frac{1}{1 + \frac{\text{Fine root nitrogen concentration}}{0.01}} \f$ \n
     *
     * - \b Nodule \b surface: (Weisz et al., 1985) \n
     *    - Nodule surface area represented by vegetative plant biomass \n
     *
     * - \b Actual \b nitrogen \b fixation \b rate: \n
     *    - If the plant N demand is \f$ > 0 \f$: \n
     *      - \f$ f = \text{Potential N fixation rate} * \text{Nodule surface area} * \text{Water availability} * \text{Temperature}
     *        * \text{Nitrogen availability} \f$
     *      - the carbon costs of the nitrogen fixation are restricted by the fine root biomass and are determined: \n
     *        - MIN(Actual fixation rate \f$ * \f$ NFIX_CEFF, 0.99 \f$ * \f$ Fine root biomass carbon content)
     *
     */
    if ( cbm::flt_greater_zero( (*_vt)->NFIX_RATE()))
    {
        double const n_living_plant( cbm::bound_min( 0.0, n_plant[_vt->slot] - _vt->n_dst - _vt->n_dfol));
        double const plant_n_demand( PlaMox_n_opt( _vt) - n_living_plant);
        if ( cbm::flt_greater_zero( plant_n_demand))
        {
            double const fact_water( NitrogenFixation_->get_fact_water( _vt));
            double const fact_temp( NitrogenFixation_->get_fact_temperature( _vt, *mc_temp));
            double const fact_n( cbm::bound_min( 0.0, 1.0 - _vt->ncFrt / (*_vt)->NC_FINEROOTS_MAX()));
            double const biomass_vegetative( _vt->mFol + _vt->dw_lst + _vt->mFrt);

            //potential nitrogen fixation rate (kg N timestep-1 kg-1 DM-1)
            double const pot_n_fix_rate( (*_vt)->NFIX_RATE() * FTS_TOT_ * cbm::KG_IN_G);
            double const n_fix( pot_n_fix_rate * biomass_vegetative * cbm::harmonic_mean2( fact_water, fact_temp) * fact_n);

            //carbon costs of nitrogen fixation restricted by fine root biomass
            double const c_cost( std::min(n_fix * (*_vt)->NFIX_CEFF(), 0.99 * _vt->mFrt * cbm::CCDM));

            //update C state
            _vt->rTra += c_cost;
            _vt->mFrt -= (c_cost / cbm::CCDM);

            //update N state
            n_plant[_vt->slot] += n_fix;
            n2_fixation[_vt->slot] += n_fix;
            _vt->a_fix_n += n_fix;

            PlaMox_update_nitrogen_concentrations( _vt);
        }
    }

    /*!
     * @page plamox
     * \b Approach \f$ INI\_N\_FIX > 0.0\f$ \n
     *
     * - \b Plant \b N \b demand: \n
     *    - \f$ \text{N}_{\text{demand}} = \text{N}_{\text{optimum}} - \text{N}_{\text{plant}} \f$ @see n_opt()
     *
     * - \b Total \b nitrogen \b demand: \n
     *    - \f$ f_{n} = \f$ Foliage biomass under optimal, closed canopy conditions (parameter MFOLOPT) \f$ * \f$
     *      optimum nitrogen concentration of foliage (parameter NC_FOLIAGE_MAX) \n
     *
     * - \b Potential \b N \b fixation \b rate: \n
     *    - \f$ f_{pot} = \f$ MIN(plant development stage \f$ * \f$ parameter INI_N_FIX (0-10) \f$ * \f$ total foliage nitrogen demand \f$ - \f$
     *      the yearly nitrogen fixation, \f$ \text{N}_{\text{demand}} \f$)
     *
     * - \b Fine \b root \b carbon \b costs: \n
     *    -  \f$ f_{pot} * \f$ carbon use efficiency for nitrogen fixation (parameter NFIX_CEFF) \n
     *
     * - \b Biological \b nitrogen \b fixation (BNF) happens if the potential N fixation rate \f$ > 0 \f$ AND
     *   fine root carbon costs (total transport and uptake respiration) \f$ < \f$ total fine root carbon \n
     */
    else if ( cbm::flt_greater_zero( (*_vt)->INI_N_FIX()))
    {
        double const n_living_plant( cbm::bound_min( 0.0, n_plant[_vt->slot] - _vt->n_dst - _vt->n_dfol));
        double const plant_n_demand( PlaMox_n_opt( _vt) - n_living_plant);

        if ( cbm::flt_greater_zero( plant_n_demand))
        {
            //reduced optimum foliage due to seeding density
            double const m_fol_opt( (*_vt)->MFOLOPT() * fractional_cover[_vt->slot]);

            //relate all other biomass to optimum foliage
            double const fraction_stem( 1.0 - (*_vt)->FRACTION_ROOT() - (*_vt)->FRACTION_FRUIT() - (*_vt)->FRACTION_FOLIAGE());
            double const nitrogen_demand_foliage( m_fol_opt * (*_vt)->NC_FOLIAGE_MIN()); //use here minimum since this the target value at maturity
            double const nitrogen_demand_roots( m_fol_opt * (*_vt)->FRACTION_ROOT() / (*_vt)->FRACTION_FOLIAGE() * (*_vt)->NC_FINEROOTS_MAX());
            double const nitrogen_demand_fruit( m_fol_opt * (*_vt)->FRACTION_FRUIT() / (*_vt)->FRACTION_FOLIAGE() * (*_vt)->NC_FRUIT_MAX());
            double const nitrogen_demand_stem( m_fol_opt * fraction_stem / (*_vt)->FRACTION_FOLIAGE() * (*_vt)->NC_STRUCTURAL_TISSUE_MAX());
            double const total_nitrogen_demand( nitrogen_demand_foliage + nitrogen_demand_roots + nitrogen_demand_fruit + nitrogen_demand_stem);

            //nitrogen fixation restricted by fine root abundance
            double const maximum_nitrogen_fixation_rate( cbm::bound_max( _vt->mFrt * cbm::CCDM / (*_vt)->NFIX_CEFF() * FTS_TOT_, plant_n_demand));

            //phenological development regulates fraction of total N fixation that can be fixed per time step
            double const delta_dvs( cbm::bound_min( 0.0, _vt->dvsFlush - _vt->dvsFlushOld));
            double const pot_n_fix( cbm::bound_max( delta_dvs * (*_vt)->INI_N_FIX() * total_nitrogen_demand, maximum_nitrogen_fixation_rate));

            if ( cbm::flt_greater_zero( pot_n_fix))
            {
                double const r_tra_frt( cbm::bound_max( pot_n_fix * (*_vt)->NFIX_CEFF(), 0.9 * _vt->mFrt * cbm::CCDM));

                //update C state
                _vt->rTra += r_tra_frt;
                _vt->mFrt -= r_tra_frt / cbm::CCDM;

                //update N state
                n_plant[_vt->slot] += pot_n_fix;
                _vt->a_fix_n += pot_n_fix;
                n2_fixation[_vt->slot] += pot_n_fix;

                PlaMox_update_nitrogen_concentrations( _vt);
            }
        }
    }
    else
    { /* no op */ }

    return LDNDC_ERR_OK;
}



/*!
 * @page plamox
 * @section Respiration Respiration
 *
 * @details
 * \b Maintenance \b respiration is calculated after @cite spitters:1989a :
 * - Maintenance respiration coefficient of leaves, roots, stems and storage organs
 *   (parameter MC_LEAF, MC_ROOT, MC_STEM and MC_STORAGE (the latter three are by default chosen differently to @cite spitters:1989a )
 *      are used. \n
 * - If the plant is in chilling stadium (get_chill_factor()) there is no maintenance respiration. \n \n
 *
 * - Maintenance respiration is based on the cost of metabolic activity (lower temperature, higher age of plant (latter not for grass)): \n
 *   - \b Temperature \b scale \b factor: \n
 *     - \f$ f_{ts} = \f$ get_frost_factor() * (2.0^{((T -  Reference temperature for maintenance respiration)/10)} * 1/24) \n
 *
 *   - \b Temperature \b chill \b factor: \n
 *     - \f$ f_{tc} = f_{ts} * \text{Carbon Content} * \f$ get_chill_factor()
 *
 *   - \b Reduction \b with \b age: \n
 *        Maintenance \b respiration is reduced to 50% of daily photosynthesis as long as plant has only little foliage biomass \n
 *        (guarantees accrue of plant growth)
 *     - \f$ f_{a} = \f$ MIN((1.0 - dvsFlush * 0.5), get_age_factor()) for crops \n
 *     - \f$ f_{a} = 1 \f$ for grass species
 *
 *   - \b Respiration is calculated for all compartments seperately and then added up to the complete maintenance respiration: \n
 *     - \f$ r_{fol} = MC\_LEAF \cdot \text{Foliage Biomass} * f_{a} * f_{tc} \f$
 *     - \f$ r_{root} = MC\_ROOT \cdot \text{Fine Root Biomass} * f_{a} * f_{tc} \f$
 *     - \f$ r_{stem} = MC\_STEM \cdot \text{Sapwood Biomass} * f_{a} * f_{tc} \f$
 *     - \f$ r_{storage} = MC\_STORAGE \cdot \text{Bud Biomass} * f_{tc} \f$
 *
 */
void
PhysiologyPlaMox::PlaMox_respiration( MoBiLE_Plant *  _vt)
{
    /******************************************************************/
    /* Maintenance respiration after Spitters et al. (1989) and ORYZA */
    /******************************************************************/

    // if plant is in chilling stadium there is no more maintenance respiration
    if ( cbm::flt_greater_zero( chill_factor[_vt->slot]))
    {
        // Maintenance respiration is reduced due to decreased metabolic activity (temperature, age of plant)
        double const fact_a( PlaMox_get_age_factor( _vt));
        double const fact_t( PlaMox_get_frost_factor( _vt, *mc_temp) * pow(2.0, ((*mc_temp) - (*_vt)->MAINTENANCE_TEMP_REF()) / 10.0));
        double const fact_at_ccdm( fact_a * fact_t * cbm::CCDM * FTS_TOT_);

        _vt->rFol = ((*_vt)->MC_LEAF()    * _vt->mFol * cbm::bound_max( _vt->ncFol / (*_vt)->NC_FOLIAGE_MAX(), 1.0) * fact_at_ccdm);
        _vt->rFrt = ((*_vt)->MC_ROOT()    * _vt->mFrt * cbm::bound_max( _vt->ncFrt / (*_vt)->NC_FINEROOTS_MAX(), 1.0) * fact_at_ccdm);
        _vt->rSap = ((*_vt)->MC_STEM()    * _vt->dw_lst * cbm::bound_max( _vt->nc_lst() / (*_vt)->NC_STRUCTURAL_TISSUE_MAX(), 1.0) * fact_at_ccdm);
        _vt->rBud = ((*_vt)->MC_STORAGE() * _vt->mBud * cbm::bound_max( _vt->nc_bud() / (*_vt)->NC_FRUIT_MAX(), 1.0) * fact_at_ccdm);
        _vt->rRes = (_vt->rFol + _vt->rFrt + _vt->rSap + _vt->rBud);

        /************************************************/
        /* Update plant biomass with respiration losses */
        /************************************************/

        
        // adjust root length in dynamic model
        if( (*_vt)->ROOTS_ENVIRONMENTAL())
        {
            root_system[_vt->slot]->update_rootlength_meanSRL_allsl( root_q_vt_[_vt->slot], _vt, -_vt->rFrt / cbm::CCDM);
        }
        
        _vt->mFol   -= _vt->rFol / cbm::CCDM;
        _vt->mFrt   -= _vt->rFrt / cbm::CCDM;
        _vt->dw_lst -= _vt->rSap / cbm::CCDM;
        _vt->mBud   -= _vt->rBud / cbm::CCDM;
        
        // the daily carbon allocated to the roots is reduced by the C used for respiration
        _vt->dcFrt -= _vt->rFrt;
    }
    else
    {
        _vt->rFol = 0.0;
        _vt->rFrt = 0.0;
        _vt->rSap = 0.0;
        _vt->rBud = 0.0;
        _vt->rRes = 0.0;
    }

    /**********************/
    /* Growth respiration */
    /**********************/

    /*!
     * @page plamox
     * \b Growth \b respiration:
     * - The fraction of growth respiration (respired C) relative to gross assimilation (assimilated C) \f$FYIELD\f$ is used to calculate the
     *   respired C from the uptaken carbon.
     *   The assimilated part goes into biomass, the respired C is used energetically for building this biomass.
     * \f[
     * C_{resp} = \frac{\text{FYIELD}}{1.0 - \text{FYIELD}} * carbonuptake
     * \f]
     * - The separate growth respiration rates for foliage, sapwood, storage organs (buds), and fine roots are calculated by multiplying
     *   \f$ C_{resp} \f$ with the (static?) fractions of foliage, sapwood, buds, or fine roots.
     * - A maximum of 90% of the biomass of every compartment is allowed for growth respiration.\n
     *   Respiration of storage organs is only added if the plant species is a tuber plant (parameter TUBER),
     *   which means it has belowground storage organs, otherwise this factor is 0. \n
    */

    double const carbonuptake_sum( cbm::sum( _vt->carbonuptake_fl, _vt->nb_foliagelayers()));
    if ( cbm::flt_greater_zero( carbonuptake_sum))
    {
        double const c_resp_tot( ((*_vt)->FYIELD() / (1.0 - (*_vt)->FYIELD())) * carbonuptake_sum);

        double const r_grow_fol( cbm::bound_max( c_resp_tot * allocation_factor_leafs[_vt->slot], 0.9 * _vt->mFol * cbm::CCDM));
        double const r_grow_sap( cbm::bound_max( c_resp_tot * allocation_factor_stems[_vt->slot], 0.9 * _vt->dw_lst * cbm::CCDM));
        double const r_grow_fru( cbm::bound_max( c_resp_tot * allocation_factor_fruit[_vt->slot], 0.9 * _vt->mBud * cbm::CCDM));
        double const r_grow_frt( cbm::bound_max( c_resp_tot * allocation_factor_roots[_vt->slot], 0.9 * _vt->mFrt * cbm::CCDM));

        _vt->rGro = (r_grow_fol + r_grow_frt + r_grow_sap + r_grow_fru);
        _vt->rGroBelow = (r_grow_frt + ((*_vt)->TUBER() ? r_grow_fru : 0.0));

        if( (*_vt)->ROOTS_ENVIRONMENTAL())
        {
            root_system[_vt->slot]->update_rootlength_meanSRL_allsl( root_q_vt_[_vt->slot], _vt, -r_grow_frt / cbm::CCDM);
        }
        
        _vt->mFol -= r_grow_fol / cbm::CCDM;
        _vt->dw_lst -= r_grow_sap / cbm::CCDM;
        _vt->mBud -= r_grow_fru / cbm::CCDM;
        _vt->mFrt -= r_grow_frt / cbm::CCDM;
        
        // the daily carbon allocated to the roots is reduced by the C used for respiration
        _vt->dcFrt -= r_grow_frt;
    }
    else
    {
        _vt->rGro = 0.0;
        _vt->rGroBelow = 0.0;
    }

    PlaMox_update_nitrogen_concentrations( _vt);
}



/*!
 * @page plamox
 * @section exudation Root exudation
 * - Exudation is modelled as a loss of fine root biomass.
 *   The exudation losses are calculated using the parameter DOC_RESP_RATIO, which gives
 *   the ratio between root exudates and losses from root respiration.
 *   The total exudation losses are calculated by \n
 *   \f$ C_{loss} = DOC\_RESP\_RATIO * \text{Belowground Respiration} \f$ .
 *
 * - A maximum of 10% of living root biomass is allowed to be used for exudation: \n
 *   \f$ C_{maxloss} = 0.1 * \text{Fine Root Biomass} * \text{Carbon Content} \f$
 */
void
PhysiologyPlaMox::PlaMox_exsudation( MoBiLE_Plant *  _vt)
{
    // maximum 10% of living root biomass is allowed to be used for exudation
    double const max_frac_loss( 0.1 * _vt->mFrt * cbm::CCDM);
    _vt->exsuLoss = cbm::bound_max( (*_vt)->DOC_RESP_RATIO() * (_vt->rGroBelow + _vt->rFrt), max_frac_loss);

    if ( cbm::flt_greater_zero( _vt->exsuLoss))
    {
        if( (*_vt)->ROOTS_ENVIRONMENTAL())
        {
            root_system[_vt->slot]->update_rootlength_meanSRL_allsl( root_q_vt_[_vt->slot], _vt, -_vt->exsuLoss / cbm::CCDM);
        }
        
        _vt->mFrt -= _vt->exsuLoss / cbm::CCDM;
        
        // the daily carbon allocated to the roots is reduced by the C used for exudation
        _vt->dcFrt -= _vt->exsuLoss;
        
        PlaMox_update_nitrogen_concentrations( _vt);

        for ( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
        {
            double fdoc( sc_.doc_sl[sl] + sc_.an_doc_sl[sl]);
            if ( cbm::flt_greater_zero( fdoc))
            {
                fdoc = sc_.doc_sl[sl] / fdoc;
            }
            else
            {
                fdoc = 0.5;
                sc_.doc_sl[sl] = 0.0;
                sc_.an_doc_sl[sl] = 0.0;
            }

            double const add_doc( _vt->exsuLoss * _vt->fFrt_sl[sl]);
            sc_.doc_sl[sl]    += add_doc * fdoc;
            sc_.an_doc_sl[sl] += add_doc * ( 1.0 - fdoc);

            sc_.accumulated_c_root_exsudates_sl[sl] += add_doc;
        }
    }
    else
    {
        _vt->exsuLoss = 0.0;
    }
}



/*!
 * @page plamox
 * @section plamox_senescence Senescence
 * Senescence calculates fluxes from living to dead plant tissue
 * separately for above- and belowground plant parts.
 *
 * @todo
 * - Nitrogen retention from senescence
 * - Senescence due to radiation/light?
 * - Senescence of fruit / storage
 */
lerr_t
PhysiologyPlaMox::PlaMox_senescence( MoBiLE_Plant *  _vt)
{
    double const MINIMUM_BIOMASS( 0.01);
    /*!
     * @page plamox
     * @subsection plamox_senescence_above Aboveground senescence
     * List of senescence processes affecting aboveground tissue: \n
     * - Drought
     * - Frost
     * - Heat
     * - Age
     *
     * @subsubsection plamox_senescence_above_drought Drought stress
     * \f[
     * \Phi_d = SENESCENCE\_DROUGHT \cdot \phi_d
     * \f]
     * The drought stress factor \f$ \phi_d \f$ is given by: @ref veglibs_drough_stress_linear
     */
    DroughtStress  droughtstress;
    droughtstress.fh2o_ref = (*_vt)->H2OREF_SENESCENCE();
    double drought_stress( cbm::bound_min( 0.0, 1.0 - droughtstress.linear_threshold( _vt->f_h2o)));
    double const s_fact_drought_shoot( cbm::flt_greater_zero( drought_stress) ?
                                       ((*_vt)->SENESCENCE_DROUGHT() * drought_stress * FTS_TOT_)
                                       : 0.0);

    /*!
     * @page plamox
     * @subsubsection plamox_senescence_above_frost Frost stress
     * Frost stress for \f$ T < 0 \f$ is given by:
     * \f[
     * \Phi_f =
     * \begin{cases}
     * & 0, T >= 0  \\
     * & SENESCENCE\_FROST \cdot \frac{T}{-20}, -20 < T < 0 \\
     * & SENESCENCE\_FROST, T <= -20 \\
     * \end{cases}
     * \f]
     */
    double const frost_temp_ref_above_a( 0.0);
    double const frost_temp_ref_above_b( -20.0);
    double const s_fact_frost_shoot( cbm::flt_less( (*mc_temp), frost_temp_ref_above_a) ?
                                     cbm::bound( 0.0,
                                                 (*_vt)->SENESCENCE_FROST() * ((*mc_temp) - frost_temp_ref_above_a) / frost_temp_ref_above_b * FTS_TOT_,
                                                 (*_vt)->SENESCENCE_FROST())
                                     : 0.0);

    /*!
     * @page plamox
     * @subsubsection plamox_senescence_above_heat Heat stress
     * \f[
     * \Phi_h = SENESCENCE\_HEAT \cdot (1 - \phi_{h})
     * \f]
     * The heat stress factor \f$ \phi_h \f$ is given by: @ref plamox_common_heat_factor
     */
    double const s_fact_heat_shoot( (*_vt)->SENESCENCE_HEAT() * cbm::bound_min(0.0, (1.0 - PlaMox_get_heat_factor( _vt)) * FTS_TOT_));

    /*!
     * @page plamox
     * @subsubsection plamox_senescence_above_age Senescence due to age
     */
    double s_fact_age_lst( 0.0);
    double s_fact_age_fol( 0.0);
    if ( _vt->groupId() == SPECIES_GROUP_GRASS)
    {
        /*!
         * Grass:
         * \f[
         * \Phi_{a,leaf} = SENESCENCE\_AGE \cdot DVS \\
         * \Phi_{a,stem} = SENESCENCE\_AGE \cdot DVS
         * \f]
         */
        s_fact_age_lst = _vt->mFol / (*_vt)->MFOLOPT() * (*_vt)->SENESCENCE_AGE() * FTS_TOT_;
        s_fact_age_fol = s_fact_age_lst;
    }
    else if ( cbm::flt_greater( _vt->growing_degree_days, gdd_grain_filling[_vt->slot]) &&
              cbm::flt_greater( (*_vt)->GDD_MATURITY(), gdd_grain_filling[_vt->slot]))
    {
        /*!
         * Crops:
         * \f[
         * \Phi_{a,leaf} = SENESCENCE\_AGE \cdot \frac{GDD - GDD\_GRAIN\_FILLING}{GDD\_MATURITY - GDD\_GRAIN\_FILLING} \\
         * \Phi_{a,stem} = 0.0
         * \f]
         */
        double scale( cbm::bound_max(
                      (_vt->growing_degree_days - gdd_grain_filling[_vt->slot]) /
                      ((*_vt)->GDD_MATURITY() - gdd_grain_filling[_vt->slot]),
                      1.0));
        s_fact_age_fol = scale * (*_vt)->SENESCENCE_AGE() * FTS_TOT_;
    }

    // Combining senescence by maximum rule
    double const sen_fact_lst( cbm::bound_max(
                             std::max({ s_fact_drought_shoot,
                                        s_fact_frost_shoot,
                                        s_fact_heat_shoot,
                                        s_fact_age_lst }),
                                        0.99));
    double const sen_fact_fol( cbm::bound_max(
                             std::max({ s_fact_drought_shoot,
                                        s_fact_frost_shoot,
                                        s_fact_heat_shoot,
                                        s_fact_age_fol }),
                                        0.99));

    if ( cbm::flt_greater_zero( sen_fact_lst) &&
         cbm::flt_greater( _vt->dw_lst, MINIMUM_BIOMASS))
    {
        double const dw_sen_lst( _vt->dw_lst * sen_fact_lst);
        double const n_sen_lst( dw_sen_lst * _vt->nc_lst() * (1.0 - (*_vt)->FRET_N()));

        _vt->dw_lst -= dw_sen_lst;
        _vt->dw_dst += dw_sen_lst;
        _vt->n_dst += n_sen_lst;

        _vt->sFol += dw_sen_lst;  //currently no extra output for structural matter
        _vt->nLitFol += n_sen_lst;  //currently no extra output for structural matter
    }

    if ( cbm::flt_greater_zero( sen_fact_fol) &&
         cbm::flt_greater( _vt->mFol, MINIMUM_BIOMASS))
    {
        double const dw_sen_fol( _vt->mFol * sen_fact_fol);
        double const n_sen_fol( dw_sen_fol * _vt->ncFol * (1.0 - (*_vt)->FRET_N()));

        _vt->mFol -= dw_sen_fol;
        _vt->dw_dfol += dw_sen_fol;
        _vt->n_dfol += n_sen_fol;

        _vt->sFol += dw_sen_fol;
        _vt->nLitFol += n_sen_fol;
    }

    /*!
     * @page plamox
     * @subsection plamox_senescence_below Belowground senescence
     * List of senescence processes affecting belowground tissue: \n
     * - Drought (considering every soil layer)
     * - Frost (considering every soil layer)
     * - Age (for non-grass), temperature (for grass), (single value for the whole root system)
     */
    if (  cbm::flt_greater( _vt->mFrt, MINIMUM_BIOMASS))
    {
        // biomass of fine roots which dies at least due to senescence
        double s_frt_min( 0.0);
        // senescence in every layer, later normalised to a distribution
        lvector_t< double > s_frt_sl(root_q_vt_[_vt->slot], 0.0);
        double normalizefrtdist( 0.0);
        for ( size_t  sl = 0;  sl < root_q_vt_[_vt->slot];  ++sl)
        {
            /*!
             * @page plamox
             * @subsubsection plamox_senescence_below_drought Drought stress
             * \f[
             * \Phi_d = SENESCENCE\_DROUGHT \cdot \phi_d
             * \f]
             */
            droughtstress.fh2o_ref = (*_vt)->H2OREF_SENESCENCE();
            drought_stress = cbm::bound_min( 0.0, 1.0 - droughtstress.linear_threshold( m_state->get_fh2o_sl( _vt, &wc_, &sc_, sl, sc_.wcmin_sl[sl])));
            double const s_fact_drought_root( cbm::flt_greater_zero( drought_stress) ?
                                              ((*_vt)->SENESCENCE_DROUGHT() * drought_stress * FTS_TOT_)
                                              : 0.0);

            /*!
             * @page plamox
             * @subsubsection plamox_senescence_below_frost Frost stress
             * Frost stress for \f$ T < 0 \f$ is given by:
             * \f[
             * \Phi_f =
             * \begin{cases}
             * & 0, T >= 0 \\
             * & SENESCENCE\_FROST \cdot \frac{T}{-20}, -20 < T < 0 \\
             * & SENESCENCE\_FROST, T <= -20 \\
             * \end{cases}
             * \f]
             */
            double const frost_temp_ref_below_a( 0.0);
            double const frost_temp_ref_below_b( -20.0);
            double const s_fact_frost_root( cbm::flt_less( (*mc_temp), frost_temp_ref_below_a) ?
                                            cbm::bound( 0.0,
                                                        (*_vt)->SENESCENCE_FROST() * (temp_sl[sl] - frost_temp_ref_below_a) / frost_temp_ref_below_b * FTS_TOT_,
                                                        (*_vt)->SENESCENCE_FROST())
                                            : 0.0);

            // Combining senescence in single layer by maximum rule
            double const s_fact_root( std::max( s_fact_drought_root,
                                                s_fact_frost_root));

            // CB: part which gets subtracted in this soil layer
            s_frt_sl[sl] = _vt->mFrt * _vt->fFrt_sl[sl] * s_fact_root; // absolute value of mass dying in this layer
            s_frt_min += s_frt_sl[sl];

            // CB: adapt the root distribution accordingly
            if( (*_vt)->ROOTS_ENVIRONMENTAL())
            {
                _vt->fFrt_sl[sl] -= s_frt_sl[sl]/_vt->mFrt;
                normalizefrtdist += _vt->fFrt_sl[sl];
            }
            // else: the distribution doesn't change
        }
        if( !cbm::flt_greater_equal_zero( s_frt_min))
        {
            KLOGWARN("senescence creates below ground biomass: ", s_frt_min);
        }

        // biomass of fine roots which actually dies due to senescence
        // Only if the amount of roots dying from stress is larger than the amount determined by the species
        // parameter TOFRTBAS, the stress is considered.
        /*!
         * @page plamox
         * @subsubsection plamox_senescence_below_age_temperature Age or temperature stress
         * Age or temperature stress
         * - Age for non-grass:
         * Similar to age for above ground biomass, the parameter is TOFRTBAS
         * 
         * - T for grass, the parameter is TOFRTBAS
         * 
         */
        double s_frt( 0.0);
        if ( _vt->groupId() == SPECIES_GROUP_GRASS)
        {
            double const fact_t_high( cbm::bound_min( 0.0, pow( 1.6, (*mc_temp) / 10.0) - 1.0));
            double const fact_t_low( PlaMox_get_frost_factor( _vt, temp_sl[0]));
            double const fact_t_scale( fact_t_high * fact_t_low * FTS_TOT_);
            s_frt = std::max( s_frt, (*_vt)->TOFRTBAS() * _vt->mFrt * fact_t_scale);
        }
        else
        {
            s_frt = std::max( s_frt_min, (*_vt)->TOFRTBAS() * _vt->mFrt * FTS_TOT_ * _vt->dvsFlush);
            //~ LOGINFO("s_frt_min ", s_frt_min);
            //~ LOGINFO("s_frt ", (*_vt)->TOFRTBAS() * _vt->mFrt * FTS_TOT_ * _vt->dvsFlush);
        }
        double const s_frt_n( s_frt * _vt->ncFrt);

        if( (*_vt)->ROOTS_ENVIRONMENTAL())
        {
            if( cbm::flt_greater_zero( s_frt))
            {
                // CB: renormalise fFrt_sl showing the senescence
                for ( size_t  sl = 0;  sl < root_q_vt_[_vt->slot];  ++sl)
                {
                    //~ LOGINFO("sl ", sl);
                    _vt->fFrt_sl[sl] = _vt->fFrt_sl[sl]/normalizefrtdist;
                }
                // CB: renormalise s_frt_sl for the choice between s_frt_min and s_frt
                if( cbm::flt_greater(s_frt, s_frt_min))
                {
                    if( cbm::flt_greater_zero(s_frt_min))
                    {
                        for ( size_t  sl = 0;  sl < root_q_vt_[_vt->slot];  ++sl)
                        {
                            //~ LOGINFO("sl ", sl);
                            //~ LOGINFO("s_frt_sl[sl] ", s_frt_sl[sl]);
                            //~ LOGINFO("s_frt ", s_frt);
                            //~ LOGINFO("s_frt_min ", s_frt_min);
                            s_frt_sl[sl] = s_frt_sl[sl] * s_frt / s_frt_min;        // rescale
                        }
                    }
                    else
                    {
                        for ( size_t  sl = 0;  sl < root_q_vt_[_vt->slot];  ++sl)
                        {
                            // fracfrt was not changed; needs not to be adjusted, just the mass
                            //~ LOGINFO("sl ", sl);
                            //~ LOGINFO("s_frt ", s_frt);
                            //~ LOGINFO("s_frt_sl[sl] ", s_frt_sl[sl]);
                            s_frt_sl[sl] = s_frt * _vt->fFrt_sl[sl];        // set
                            //~ LOGINFO("s_frt_sl[sl] ", s_frt_sl[sl]);
                        }
                    }
                }
                
                // reduce root length
                for ( size_t  sl = 0;  sl < root_q_vt_[_vt->slot];  ++sl)
                {
                    //~ LOGINFO("sl ", sl);
                    //~ LOGINFO("s_frt ", s_frt);
                    //~ LOGINFO("s_frt_min ", s_frt_min);
                    // reduce the rootlength in every layer for the dynamic root model
                    // for the static model this is done in the function update_roots_static 
                    double srl_mean_sl = _vt->rootlength_sl[sl] / (_vt->mFrt * _vt->fFrt_sl[sl]);  // rootlength t-1 -> take rootmass t-1
                    if( cbm::flt_greater( srl_mean_sl, (*_vt)->SRLMAX()+0.000001))
                    {
                        LOGERROR("PlaMox_senescence: srl_mean_sl ", srl_mean_sl, " larger than SRLMAX ", (*_vt)->SRLMAX());
                    }
                    //~ LOGINFO("_vt->rootlength_sl[sl] ", _vt->rootlength_sl[sl]);
                    //~ LOGINFO("s_frt_sl[sl] ", s_frt_sl[sl]);
                    //~ LOGINFO("srl_mean_sl ", srl_mean_sl);
                    _vt->rootlength_sl[sl] -= srl_mean_sl * s_frt_sl[sl];
                    //~ LOGINFO("_vt->rootlength_sl[sl] ", _vt->rootlength_sl[sl]);
                }
            }
        }

        if ( cbm::flt_greater_zero( s_frt))
        {
            for ( size_t  sl = 0;  sl < root_q_vt_[_vt->slot];  ++sl)
            {
                _vt->sFrt_sl[sl] += s_frt * _vt->fFrt_sl[sl];
                _vt->nLitFrt_sl[sl] += s_frt_n * _vt->fFrt_sl[sl];
            }
        }
        
        // the daily carbon allocated to the roots is reduced by the C used for respiration
        // This doesn't really make sense; senescence rather effects the old roots.
        //~ _vt->dcFrt -= s_frt * cbm::CCDM;
        _vt->mFrt -= s_frt;
        
        
        n_plant[_vt->slot] -= s_frt_n;

        /* Belowground litter */
        for ( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  sl++)
        {
            sc_.c_raw_lit_1_sl[sl] += s_frt * _vt->fFrt_sl[sl] * (1.0 - (*_vt)->CELLULOSE() - (*_vt)->LIGNIN()) * cbm::CCDM;
            sc_.c_raw_lit_2_sl[sl] += s_frt * _vt->fFrt_sl[sl] * (*_vt)->CELLULOSE() * cbm::CCDM;
            sc_.c_raw_lit_3_sl[sl] += s_frt * _vt->fFrt_sl[sl] * (*_vt)->LIGNIN() * cbm::CCDM;
            sc_.accumulated_c_litter_below_sl[sl] += s_frt * _vt->fFrt_sl[sl] * cbm::CCDM;

            sc_.n_raw_lit_1_sl[sl] += s_frt_n * _vt->fFrt_sl[sl] * (1.0 - (*_vt)->CELLULOSE() - (*_vt)->LIGNIN());
            sc_.n_raw_lit_2_sl[sl] += s_frt_n * _vt->fFrt_sl[sl] * (*_vt)->CELLULOSE();
            sc_.n_raw_lit_3_sl[sl] += s_frt_n * _vt->fFrt_sl[sl] * (*_vt)->LIGNIN();
            sc_.accumulated_n_litter_below_sl[sl] += s_frt_n * _vt->fFrt_sl[sl];
        }
    }

    PlaMox_update_nitrogen_concentrations( _vt);

    return LDNDC_ERR_OK;
}



lerr_t
PhysiologyPlaMox::PlaMox_abscission( MoBiLE_Plant *  _vt)
{
    double const k_abscise( 0.1 * FTS_TOT_);

    double c_litter( 0.0);
    double n_litter( 0.0);

    if ( cbm::flt_greater_zero( _vt->dw_dfol) ||
         cbm::flt_greater_zero( _vt->n_dfol))
    {
        double const dw_abscise_dfol( k_abscise * _vt->dw_dfol);
        double const n_abscise_dfol( k_abscise * _vt->n_dfol);
        _vt->dw_dfol -= dw_abscise_dfol;
        _vt->n_dfol -= n_abscise_dfol;
        n_plant[_vt->slot] -= n_abscise_dfol;

        c_litter += dw_abscise_dfol * cbm::CCDM;
        n_litter += n_abscise_dfol;
    }
    else
    {
        _vt->dw_dfol = 0.0;
        _vt->n_dfol = 0.0;
    }

    if ( cbm::flt_greater_zero( _vt->dw_dst) ||
         cbm::flt_greater_zero( _vt->n_dst))
    {
        double const dw_abscise_dst( k_abscise * _vt->dw_dst);
        double const n_abscise_dst( k_abscise * _vt->n_dst);
        _vt->dw_dst -= dw_abscise_dst;
        _vt->n_dst -= n_abscise_dst;
        n_plant[_vt->slot] -= n_abscise_dst;

        c_litter += dw_abscise_dst * cbm::CCDM;
        n_litter += n_abscise_dst;
    }
    else
    {
        _vt->dw_dst = 0.0;
        _vt->n_dst = 0.0;
    }

    if ( cbm::flt_greater_zero( c_litter) ||
         cbm::flt_greater_zero( n_litter))
    {
        sc_.c_raw_lit_1_above += c_litter * (1.0 - (*_vt)->CELLULOSE() - (*_vt)->LIGNIN());
        sc_.c_raw_lit_2_above += c_litter * (*_vt)->CELLULOSE();
        sc_.c_raw_lit_3_above += c_litter * (*_vt)->LIGNIN();
        sc_.accumulated_c_litter_above += c_litter;

        sc_.n_raw_lit_1_above += n_litter * (1.0 - (*_vt)->CELLULOSE() - (*_vt)->LIGNIN());
        sc_.n_raw_lit_2_above += n_litter * (*_vt)->CELLULOSE();
        sc_.n_raw_lit_3_above += n_litter * (*_vt)->LIGNIN();
        sc_.accumulated_n_litter_above += n_litter;
    }

    return LDNDC_ERR_OK;
}



/*!
 * @page plamox
 * @section transpiration Transpiration
 *  Calculates potential transpiration on \n
 *  a) water use efficiency and carbon uptake:
 *         @li @link potentialcroptranspiration Potentialcroptranspiration @endlink
 *
 *  b) stomatal conductance and vapour pressure deficit:
 *         @li @link potentialtranspiration Potentialtranspiration @endlink
 *
 * If an hourly timestep is chosen, this is done hourly.
 * However, only the accumulated potential transpiration is stored.
 */
void
PhysiologyPlaMox::PlaMox_transpiration( MoBiLE_Plant *  _vt)
{
    if ( transpiration_method == "wateruseefficiency")
    {
        double const carbonuptake_sum( cbm::sum( _vt->carbonuptake_fl, _vt->nb_foliagelayers()));
        wc_.accumulated_potentialtranspiration += potential_crop_transpiration(
                                                                           ac_.nd_co2_concentration,
                                                                           carbonuptake_sum,
                                                                           (*_vt)->WUECMAX());
    }
    else
    {
        wc_.accumulated_potentialtranspiration += potential_transpiration(
                                                                      _vt->nb_foliagelayers(),
                                                                      (*_vt)->GSMIN(),
                                                                      (*_vt)->GSMAX(),
                                                                      _vt->lai_fl,
                                                                      mc_.vpd_fl,
                                                                      _vt->relativeconductance_fl) * cbm::HR_IN_DAY * FTS_TOT_;

    }
}



/*!
 * @page plamox
 * @section plamox_root_structure Root Structure
 * @subsection plamox_root_structure_distribution Distribution
 *
 * Roots are represented in a one-dimensional way by the fine root mass distribution and the total fine root mass.
 *
 * Currently available distribution functions for vertical root distribution:
 * - root depth dependend (see @ref veglibs_roots_grote)
 * - exponential (see @ref veglibs_roots_exponential)
 * - sigmoid (see @ref veglibs_roots_sigmoid)
 *
 * The environmental function is used for \f$ ROOTS\_ENVIRONMENTAL = true\f$.
 * If \f$ ROOTS\_ENVIRONMENTAL = false\f$, the exponential function is used for \f$ EXP\_ROOT\_DISTRIBUTION > 0\f$.
 * If \f$ ROOTS\_ENVIRONMENTAL = false\f$ and \f$ EXP\_ROOT\_DISTRIBUTION <= 0\f$, the sigmoid function is used.
 */
void
PhysiologyPlaMox::PlaMox_update_root_structure( MoBiLE_Plant * _vt, double deltamassFrt)
{
    /*!
     * @page plamox
     * @subsubsection plamox_root_structure_dynamic Environmentally/dynamic determined root growth
     * See @ref veglibs_roots_sinkstrength.
     */
    if( (*_vt)->ROOTS_ENVIRONMENTAL())
    {
        root_system[_vt->slot]->update_roots_dynamic( FTS_TOT_, root_q_vt_[_vt->slot], _vt, deltamassFrt);
    }
    else
    {
        /*!
         * @page plamox
         * @subsubsection plamox_root_structure_static Default static root growth
         * See @ref veglibs_root_growth_static
         */
        root_system[_vt->slot]->update_roots_static( FTS_TOT_, root_q_vt_[_vt->slot], _vt, deltamassFrt, root_system, 1.);
    }

   /*!
    * @page plamox
    * @subsection plamox_root_structure_conductivity Root conductivity
    * Gaseous conductivity of roots is expressed by an
    * root aerenchyme transport \f$ r_{tc}\f$ coefficient:
    *  \f[
    *  r_{tc} = m_r \cdot RS\_CONDUCT
    *  \f]
    */
    _vt->root_tc = _vt->mFrt * (*_vt)->RS_CONDUCT();
}



/*!
 * @page plamox
 * @section plamox_groundcover Ground coverage
 */
void
PhysiologyPlaMox::PlaMox_update_ground_cover( MoBiLE_Plant *  _vt)
{
    /*!
     * @page plamox
     * Ground coverage of grass is always assumed to be 100%
     */
    if ( _vt->groupId() == SPECIES_GROUP_GRASS)
    {
        _vt->f_area = fractional_cover[_vt->slot];
    }
    /*!
     * @page plamox
     * Ground coverage of crops is estimated by lai:
     *  \f[
     *  gc = \frac{lai}{3}^{0.5}
     *  \f]
     *  Full cover is reached with a leaf area index of three (FAO).
     */
    else
    {
        double const f_area( cbm::bound_min( 0.01,
                                             std::pow( _vt->lai() / 3.0, 0.5)));

        //bound area coverage by lai and fractional_cover
        double const f_area_max( cbm::bound_max( _vt->lai(), fractional_cover[_vt->slot]));
        if ( cbm::flt_greater( f_area, f_area_max))
        {
            _vt->f_area = f_area_max;
        }
        else
        {
            _vt->f_area = f_area;
        }
    }
}



/*!
 * @brief calculates new height of plant depending on the ratio of
 * current to optimum aboveground biomass
 */
void
PhysiologyPlaMox::PlaMox_update_height( MoBiLE_Plant *  _vt)
{
    double const fol_opt( (*_vt)->MFOLOPT() * fractional_cover[_vt->slot]);
    double const lst_opt( (1.0 - (*_vt)->FALEAF()) / (*_vt)->FALEAF() * fol_opt);
    if ( _vt->group() == "crop")
    {
        //height evegrowth starts with stem elongation
        //minimum of 1% development until start of height growth required: dvs_of_start_height !> 0.0
        double const dvs_of_start_height( cbm::bound_min( 0.01, (*_vt)->GDD_STEM_ELONGATION() / (*_vt)->GDD_MATURITY()));
        //minimum of 2% development until end of height growth required: dvs_of_end_height !> dvs_of_start_height
        double const dvs_of_max_height( cbm::bound_min( 0.02, gdd_grain_filling[_vt->slot] / (*_vt)->GDD_MATURITY()));

        //until start of stem elongation, height modeled with plant development
        //10% of final height is always reached until start of stem elongation
        double const height_fraction_at_stem_elongation( 0.1);
        double height_fraction( cbm::bound_max( _vt->dvsFlush / dvs_of_start_height, 1.0) * height_fraction_at_stem_elongation);
        if ( cbm::flt_greater( _vt->dvsFlush, dvs_of_start_height))
        {
            double const biomass_fraction( cbm::bound_max( cbm::sqrt((_vt->dw_lst + _vt->mFol) / (lst_opt + fol_opt)), 1.0));
            //bound biomass-depending height by plant development
            //max should not be reached before start of grain filling (end of phase of stem elongation)
            height_fraction = cbm::bound( height_fraction,
                                          biomass_fraction,
                                          height_fraction_at_stem_elongation +
                                          (1.0 - height_fraction_at_stem_elongation) *
                                          (_vt->dvsFlush - dvs_of_start_height)/ (dvs_of_max_height- dvs_of_start_height));
        }
        _vt->height_max = cbm::bound_min( _vt->height_max, height_fraction * (*_vt)->HEIGHT_MAX());
        _vt->height_at_canopy_start = (*_vt)->CB() * _vt->height_max;
    }
    else
    {
        _vt->height_max = cbm::bound_max( (_vt->dw_lst + _vt->mFol) / (lst_opt + fol_opt) * (*_vt)->HEIGHT_MAX(), (*_vt)->HEIGHT_MAX());
        _vt->height_at_canopy_start = (*_vt)->CB() * _vt->height_max;
    }

    // update height of canopy layers (m)
    ph_.update_canopy_layers_height( m_veg);
}



/*!
 * @brief
 *      Calculation of canopy layer properties:
 *       - foliage biomass
 *       - foliage biomass fraction
 *       - leaf area index
 *      By these means, germination and growth of very small biomasses
 *      depend less on early lai development, which is not well represented
 *      by lai<->photosynthesis feedbacks.
 */
void
PhysiologyPlaMox::PlaMox_update_foliage_structure( MoBiLE_Plant *  _vt)
{
    size_t const fl_cnt( _vt->nb_foliagelayers());

    double sla_cum( 0.0);
    for (size_t fl = 0; fl < fl_cnt; ++fl)
    {
        sla_cum += _vt->sla_fl[fl];
    }

    /* Foliage biomass for lai calculation has lower boundary ensuring: lai >= lai_min */
    double const mFol_lai( cbm::bound_min( lai_min[_vt->slot] / (*_vt)->SLAMAX(),
                                           _vt->mFol));

    if ( cbm::flt_greater_zero( sla_cum))
    {
        DroughtStress  droughtstress;
        droughtstress.fh2o_ref = (*_vt)->H2OREF_LEAF_GROWTH();

        for (size_t fl = 0; fl < fl_cnt; ++fl)
        {
            _vt->fFol_fl[fl] = _vt->sla_fl[fl] / sla_cum;
            _vt->lai_fl[fl] = mFol_lai * _vt->fFol_fl[fl] * _vt->sla_fl[fl] * droughtstress.linear_threshold( _vt->f_h2o);
        }
        for (int fl = fl_cnt; fl < m_setup->canopylayers(); ++fl)
        {
            _vt->fFol_fl[fl] = 0.0;
            _vt->lai_fl[fl] = 0.0;
            _vt->sla_fl[fl] = 0.0;
        }
    }
    else
    {
        for (int fl = 0; fl < m_setup->canopylayers(); ++fl)
        {
            _vt->fFol_fl[fl] = 0.0;
            _vt->lai_fl[fl] = 0.0;
            _vt->sla_fl[fl] = 0.0;
        }
    }
}



/*!
 * @page plamox
 * @section update_specific_leaf_area Specific leaf area weight (sla)
 *  Calculates specific leaf area weight sla [kg m-2](CB:?? not m2/kg?) in each canopy layer:
 *  - sla is assumed to be homogeneously distributed throughout the whole canopy.
 *  - sla decreases with plant development dvs depending on
 *  the species parameter SLADECLINE (mostly \b 0 or \b 0.5)
 *  \f[
 *  sla = SLAMAX \cdot ( 1 - dvs \cdot SLADECLINE \cdot dvsMort)
 *  \f]
 *  For selected species (mungbean, rice, grass), specific formulations exist.
 */
void
PhysiologyPlaMox::PlaMox_update_specific_leaf_area( MoBiLE_Plant *  _vt)
{
    size_t const fl_cnt( _vt->nb_foliagelayers());

    /*!
     * @brief
     *  For mungbean, the species parameter SLAMAX is neglected and sla is calculated based
     *  on the following table taken from the WOFOST model parametrisation.
     *
     * dvs sla
     * 0.0 26.0
     * 1.0 33.0
     * 2.0 16.0
     */
    if ( IS_SPECIE( _vt->cname(), "mungbean"))
    {
        double slamax( 26.0);
        if ( !cbm::flt_greater( _vt->dvsFlush, 0.5))
        {
            slamax = 26.0 + _vt->dvsFlush / 0.5 * 7.0;
        }
        else
        {
            slamax = 33.0 - (_vt->dvsFlush - 0.5) / 0.5 * 17.0;
        }

        for (size_t fl = 0; fl < fl_cnt; ++fl)
        {
            _vt->sla_fl[fl] = slamax;
        }
    }
    else if ( m_veg->is_family( _vt, ":rice:"))
    {
        double const dvs( (*_vt)->GDD_FLOWERING() > 0.0 ?
                         cbm::bound_max( _vt->growing_degree_days / (*_vt)->GDD_FLOWERING(), 1.0) :
                         _vt->dvsMort);

        double const delta_sla( dvs * (*_vt)->SLADECLINE() * (*_vt)->SLAMAX());
        for (size_t fl = 0; fl < fl_cnt; ++fl)
        {
            _vt->sla_fl[fl] = cbm::bound_min( 0.0, (*_vt)->SLAMAX() - delta_sla);
        }
    }
    else
    {
        // reduction of specific leaf area with crop age
        double sla_red( 1.0);
        if (_vt->groupId() == SPECIES_GROUP_GRASS)
        {
            if ( cbm::flt_greater( _vt->dvsMort, 0.5))
            {
                sla_red = 1.0 - ((*_vt)->SLADECLINE() * (_vt->dvsMort - 0.5) / 0.5);
            }
        }
        else
        {
            sla_red = 1.0 - ((*_vt)->SLADECLINE() * _vt->dvsMort);
        }

        for (size_t fl = 0; fl < fl_cnt; ++fl)
        {
            _vt->sla_fl[fl] = (*_vt)->SLAMAX() * sla_red;
        }
    }

    for (int fl = fl_cnt; fl < m_setup->canopylayers(); ++fl)
    {
        _vt->sla_fl[fl] = 0.0;
    }
}



/*!
 * \return Optimum nitrogen concentration of the whole plant
 *
 * @brief
 * Adding the optimum concentrations of foliage, sapwood, fine roots and buds
 *
 * @details
 *  - Optimum concentration for foliage is calculated by: get_foliage_nitrogen_concentration()
 *  - For sapwood by Sapwood Biomass \f$ * \f$ NC_STRUCTURAL_TISSUE \n
 *  - For fine roots by Fine Root Biomass \f$ * \f$ NC_FINE_ROOTS \n
 *  - For buds by: \f$ \frac{BudBiomass * CarbonContent}{BudC:N} \f$ \n
 */
double
PhysiologyPlaMox::PlaMox_n_opt( MoBiLE_Plant *  _vt)
{
    return (  _vt->mFol * PlaMox_get_foliage_nitrogen_concentration( _vt)
            + _vt->dw_lst * (*_vt)->NC_STRUCTURAL_TISSUE_MAX()
            + _vt->mFrt * (*_vt)->NC_FINEROOTS_MAX()
            + _vt->mBud * (*_vt)->NC_FRUIT_MAX());
}



/*!
 * @details
 *  Distribution of total plant nitrogen throughout complete plant
 *  assumed to occur instantaneously.
 *
 * @param[in] _vt Plant species
 */
void
PhysiologyPlaMox::PlaMox_update_nitrogen_concentrations( MoBiLE_Plant *  _vt)
{
    double const n_living_plant( cbm::bound_min( 0.0, n_plant[_vt->slot] - _vt->n_dst - _vt->n_dfol));
    double const nitrogen_satisfaction( cbm::flt_greater_zero( PlaMox_n_opt( _vt)) ? cbm::bound(0.0, n_living_plant / PlaMox_n_opt( _vt), 1.0) : 1.0);

//    double const n_fol_opt( get_foliage_nitrogen_concentration( _vt) * _vt->mFol);

    double const n_fol_opt( (cbm::flt_greater_zero( (*_vt)->NC_FOLIAGE_MAX()) &&
                             cbm::flt_greater_zero( (*_vt)->NC_FOLIAGE_MIN())) ?
                            _vt->mFol * ((1.0 - nitrogen_satisfaction) * (*_vt)->NC_FOLIAGE_MIN()
                                       + (nitrogen_satisfaction * PlaMox_get_foliage_nitrogen_concentration( _vt))) :
                            _vt->mFol * (*_vt)->NC_FOLIAGE_MAX());
    double const n_lst_opt( (cbm::flt_greater_zero( (*_vt)->NC_STRUCTURAL_TISSUE_MAX()) &&
                             cbm::flt_greater_zero( (*_vt)->NC_STRUCTURAL_TISSUE_MIN())) ?
                            _vt->dw_lst * ((1.0 - nitrogen_satisfaction) * (*_vt)->NC_STRUCTURAL_TISSUE_MIN()
                                       + (nitrogen_satisfaction * (*_vt)->NC_STRUCTURAL_TISSUE_MAX())) :
                            _vt->dw_lst * (*_vt)->NC_STRUCTURAL_TISSUE_MAX());
    double const n_frt_opt( (cbm::flt_greater_zero( (*_vt)->NC_FINEROOTS_MAX()) &&
                             cbm::flt_greater_zero( (*_vt)->NC_FINEROOTS_MIN())) ?
                            _vt->mFrt * ((1.0 - nitrogen_satisfaction) * (*_vt)->NC_FINEROOTS_MIN()
                                       + (nitrogen_satisfaction * (*_vt)->NC_FINEROOTS_MAX())) :
                            _vt->mFrt * (*_vt)->NC_FINEROOTS_MAX());
    double const n_fru_opt( (cbm::flt_greater_zero( (*_vt)->NC_FRUIT_MAX()) &&
                             cbm::flt_greater_zero( (*_vt)->NC_FRUIT_MIN())) ?
                            _vt->mBud * ((1.0 - nitrogen_satisfaction) * (*_vt)->NC_FRUIT_MIN()
                                       + (nitrogen_satisfaction * (*_vt)->NC_FRUIT_MAX())) :
                            _vt->mBud * (*_vt)->NC_FRUIT_MAX());

    double const n_tot_opt( n_fol_opt + n_lst_opt + n_fru_opt + n_frt_opt);
    if ( cbm::flt_greater_zero( n_tot_opt))
    {
        // CB: fraction of available N to optimum N
        double const n_tot_opt_scale( n_living_plant / n_tot_opt);
        // CB: N gets distributed evenly over all compartments regarding their optimum N content
        _vt->ncFol = cbm::flt_greater_zero( _vt->mFol) ? n_tot_opt_scale * n_fol_opt / _vt->mFol :
                                                         (*_vt)->NC_FOLIAGE_MAX();
        _vt->n_lst = cbm::flt_greater_zero( _vt->dw_lst) ? n_tot_opt_scale * n_lst_opt :
                                                         0.0;
        _vt->ncBud = cbm::flt_greater_zero( _vt->mBud) ? n_tot_opt_scale * n_fru_opt / _vt->mBud :
                                                         (*_vt)->NC_FRUIT_MAX();
        _vt->ncFrt = cbm::flt_greater_zero( _vt->mFrt) ? n_tot_opt_scale * n_frt_opt / _vt->mFrt :
                                                         (*_vt)->NC_FINEROOTS_MAX();
    }
}



/*!
 * @param[in] _vt Plant species
 * @return Temperature factor
 * @details
 * Temperature factor is calculated depending on species parameter TLIMIT = Temperature limit for plant growth \n
 * For simplification reasons the soil temperature from the top soiler layer is used instead of the global temperature \n
 * - If the temperarure \f$ \text{T} < (0.9 * \text{TLIMIT}) \f$: \n
 *   - \f$ f_{t} = 0.0 \f$ \n
 * - If the temperarure \f$ \text{T} < (1.1 * \text{TLIMIT}) \f$ for a smother transition: \n
 *   - \f$ f_{t} =  T - \frac{(0.9 * \text{TLIMIT})}{(0.2 * \text{TLIMIT})} \f$ \n
 * - All other cases: \n
 *   - \f$ f_{t} = 1.0 \f$ \n
 */
double
PhysiologyPlaMox::PlaMox_get_frost_factor( MoBiLE_Plant *  _vt,
                                    double _temp)
{
    double const t_min( (*_vt)->TLIMIT() - 2.0);
    double const t_max( (*_vt)->TLIMIT());

    if ( cbm::flt_less( _temp, t_min))
    {
        return 0.0;
    }
    else if ( cbm::flt_less( _temp, t_max))
    {
        return (_temp - t_min) / (t_max - t_min);
    }
    else
    {
        return 1.0;
    }
}



/*!
 * @page plamox
 * \section plamox_common Common
 */


 /*!
  * @page plamox
  * \subsection plamox_common_heat_stress_limitation Heat Stress Limitation
  * 
  * If the plant experiences heat stress during the critical time around flowering, the pod set is reduced. 
  * The approach followes the ones introduced by Challinor et al. 2005 and Nendel 2011. 
  * 
  * The relevant temperature for this heat stress factor is the temperature during the photoactive period (\f$ T_{d}\f$ ), since it affects the time during which flowers are open. 
  * 
  * \f[
  * T_d = T_{max} - \frac{T_{max} - T_{min}}{4}
  * \f] 
  * (following Mirschel & Wenke (2007)). 
  * 
  * Challinor et al. 2005 introduced a variable Temperature threshold \f$ T_{crit} \f$ dependent on timing and duration of the heat stress during the flowering period. 
  * 
  * The daily influence (\f$ heat_{d}\f$ ) of the heat limitation is calculated dependent on the daily fraction of flowers open: 
  * 
  * \f[
  * heat_{daily} =  1 - (\frac{(T_d - T_{crit})}{(T_{zero} - T_{crit})}) * frac\_flower;
  * \f]
  * 
  * The daily fraction of flowers newly opened: 
  * \f[
  * frac\_flower = openFlowers_{today} - openFlowers_{yesterday}
  * \f]
  * 
  * The open Flowers on a specific day after flowering (daf) (Moriondo et al. 2011) :
  * \f[
  * openFlowers = \frac{1}{(1 + \frac{1}{0.015 - 1} * \exp{-1.4 * daf})};
  * \f]
  * 
  * The overall influence on the grain reduction is: 
  *  \f[
  * influence\_heat\_reduction\_grainfilling = min(heat_{daily})
  * \f]
  *
 */
void
PhysiologyPlaMox::PlaMox_heat_stress_limitation(MoBiLE_Plant* _vt)
{
    /* Get Temperature around Flowering,for 6 days before and 12 days after specified by GDD_FLOWERING */
    double const init_tcrit((*_vt)->GDD_FLOWERING() - (6 * 60)); /* timing at which Temperature observation should start */
    double const dae(days_after_emergence[_vt->slot]); /* days after emergence */

    // Daytime Temperature (Photosensitive period, Mirschel & Wenkel 2007) 
    double const daytime_temperature(mc_.nd_maximumairtemperature - ((mc_.nd_maximumairtemperature - mc_.nd_minimumairtemperature) / 4));

    if ((cbm::flt_greater(_vt->growing_degree_days, init_tcrit)) &&
        (lclock()->subday() == 1))
    {
        if (cbm::flt_less(dae, day_of_flowering + 12) &&
            cbm::flt_greater(_vt->growing_degree_days, init_tcrit))
        {
            daytime_temperatures.shift_right(daytime_temperature);
        };
    }

    int duration_heatshock(0); /* reset duration of the episode (days) */
    int timing_heatshock(0); /* rest timing of the epoisode relative to day of anthesis (days) */

    // double Tzero(0);  /* Temerature at zero pod-set (oC) */
    // double Tcrit(0); /* critical Temperature (oC) */
    int start_heat = -1;
    int end_heat = -1;

    //if (cbm::flt_greater(dae, (day_of_flowering - 6.0)) && cbm::flt_less(dae, (day_of_flowering + 12)) && (lclock()->subday() == 1))
    if (cbm::flt_less(dae, (day_of_flowering + 12)) &&
        cbm::flt_greater(dae, day_of_flowering) &&
        (lclock()->subday() == 1))
    {
        for (size_t i = 1; i < 18; ++i)
        {
            //float dt1 = daytime_temperatures[i]; float dt2 = daytime_temperatures[i - 1]; float dt3 = daytime_temperatures[i + 1];
            if (cbm::flt_less_equal(daytime_temperatures[i - 1], (*_vt)->TMINCRIT())
                && cbm::flt_greater(daytime_temperatures[i], (*_vt)->TMINCRIT())
                && cbm::flt_greater(daytime_temperatures[i + 1], (*_vt)->TMINCRIT()))
            {
                start_heat = i;
            }

            if (cbm::flt_greater(daytime_temperatures[i], (*_vt)->TMINCRIT())
                && cbm::flt_greater(daytime_temperatures[i - 1], (*_vt)->TMINCRIT())
                && cbm::flt_less_equal(daytime_temperatures[i + 1], (*_vt)->TMINCRIT()))
            {
                end_heat = i;
            }
            if (cbm::flt_greater_equal_zero(start_heat) &&
                cbm::flt_greater_equal_zero(end_heat))
            {
                timing_heatshock = (end_heat + start_heat) / 2;
            }
        }

        for (size_t i = 1; i < 12; ++i)
        {
            if (cbm::flt_greater(daytime_temperatures[i], (*_vt)->TMINCRIT()) &&
                cbm::flt_greater(daytime_temperatures[i + 1], (*_vt)->TMINCRIT()))
            {
                duration_heatshock++;
            }
        }
        /* The following parameters are set as defaults in this first iteration ,might need definition for different crops */
        double sensitivity_Tcrit = 0.3; /* sensitivity of Tcrit to timing (timing_heatshock) for negative t (oC day^-1) */
        double sensitivity_Tzero = 2.5; /* sensitivity of Tzero to timing (timing_heatshock) for negative t (oC day^-1) */
        double Tintersect = 52; /* intercept of post-anthesis Tzero parameterisation (oC) */

        if (cbm::flt_in_range_lu(12, timing_heatshock, 18))
        {
            timing_heatshock = 12 - timing_heatshock; //reorder 
            Tcrit = std::min((*_vt)->TMINCRIT(), 36 + sensitivity_Tcrit * (timing_heatshock - 6));
            Tzero = 60 + sensitivity_Tzero * (timing_heatshock - 6);
        }
        else if (cbm::flt_in_range_lu(0, timing_heatshock, 12))
        {
            //timing_heatshock = 12 - timing_heatshock; // reorder 
            //double tmincrit = (*_vt)->TMINCRIT();
            Tcrit = std::min((*_vt)->TMINCRIT(), 37.8 + 1.8 * timing_heatshock - 3 * duration_heatshock);
            Tzero = Tintersect + 0.75 * timing_heatshock - 1.5 * duration_heatshock;
        }
        /* Calculating the opening rate of the flowers, according to Moriondo et al 2011  */
        // Influence on harvest index, accoriding to MONICA (Nendel et al 2011)

        double daf = dae - day_of_flowering; /* days after flowering*/

        double openFlowers = 1 / (1 + (1 / 0.015 - 1) * std::exp(-1.4 * daf));
        double openFlowers_y = 1 / (1 + (1 / 0.015 - 1) * std::exp(-1.4 * (daf - 1)));

        double frac_flower = openFlowers - openFlowers_y;

        double heat_daily = 1 - ((daytime_temperature - Tcrit) / (Tzero - Tcrit)) * frac_flower;
        influence_heat_daily.shift_right(heat_daily);
    }
    influence_heat_reduction_grainfilling = influence_heat_daily.min();
}


/*!
 * @page plamox
 * \subsection plamox_common_heat_factor Heat factor
 * Heat factor \f$ \phi_h \f$ is given by:
 * \f[
 * \phi_h = 1 - \frac{1}{1 + e^{-2 (T_{leaf} - PSNTMAX)}}
 * \f]
 */
double
PhysiologyPlaMox::PlaMox_get_heat_factor( MoBiLE_Plant *  _vt)
{
    double const leaf_temp( m_pf.leaf_temperature_( temp_fl, _vt));

    return 1.0 - 1.0 / (1.0 + exp(-2.0 * (leaf_temp - (*_vt)->PSNTMAX())));
}



/*!
 * @page plamox
 * \subsection plamox_common_nitrogen_deficiency Nitrogen deficiency
 * Nitrogen deficiency factor \f$ \phi_n \f$ is given by:
 * \f[
 * \phi_n = \frac{c_{N,fol}}{c_{N,fol,opt}}^{N\_DEF\_FACTOR}
 * \f]
 */
double
PhysiologyPlaMox::PlaMox_get_nitrogen_deficiency( MoBiLE_Plant *  _vt)
{
//    if ( m_veg->is_family( _vt, ":winterwheat:"))
//    {
//        double const nc_lai_based_opt( cbm::flt_greater_zero( _vt->lai()) ?
//                                       (*_vt)->NC_FOLIAGE_MAX() * _vt->mFol * cbm::G_IN_KG / _vt->lai() : 0.0);
//        double const nc_lai_based( cbm::flt_greater_zero( _vt->lai()) ?
//                                   _vt->ncFol * _vt->mFol * cbm::G_IN_KG / _vt->lai() : 0.0);
//        double const vc( cbm::bound_min( 0.01, 87.3 * (nc_lai_based - 0.33)));
//        double const vc_max( cbm::bound_min( 0.01, 87.3 * (nc_lai_based_opt - 0.33)))   ;
//
//        return cbm::bound( 0.01,
//                           vc / vc_max,
//                           1.0);
//    }
//    else
    {
        double const n_ratio( _vt->ncFol / nc_fol_opt[_vt->slot]);
        if ( n_ratio < 1.0)
        {
            return cbm::bound( 0.01,
                               pow( n_ratio, (*_vt)->N_DEF_FACTOR()),
                               1.0);
        }
        else
        {
            return 1.0;
        }
    }
}


/*!
 * @page plamox
 * @subsection plamox_age_factor Aging
 * The age factor is calculated dependend on the growing degree days (GDD) (see vernalization()), the minimum temperature sum for \n
 * foliage activity onset (parameter GDDFOLSTART) and temperature degree days for full plant development . \n\n
 *
 * For grass: \n
 * - If \f$ \text{GDDFOLSTART}  >  \text{GDD} \f$:
 * \f[
 * f_{a} = 1.0 - \frac{(\text{GDDFOLSTART} - \text{GDD})}{\text{GDDFOLSTART}}
 * \f]
 * - else: \f$ f_{a} = 1.0 \f$.
 *
 * For all other species: \n
 * - If \f$ \text{GDD} < (0.9 * GDD\_MATURITY): f_{a} = 1.0 \f$ \n
 * - else:
 * \f[
 * f_{a} = \text{max}\left(0.0, 1.0 - \frac{\text{GDD} - (0.9 * GDD\_MATURITY)}{GDD\_MATURITY - (0.9 * GDD\_MATURITY)} \right)
 * \f]
 */
double
PhysiologyPlaMox::PlaMox_get_age_factor( MoBiLE_Plant *  _p)
{
    if ( PlaMox_get_life_cycle( _p) > PhysiologyPlaMox::ANNUAL)
    {
        if ( (*_p)->GDD_EMERGENCE() > _p->growing_degree_days)
        {
            return ( 1.0 - ((*_p)->GDD_EMERGENCE() - _p->growing_degree_days) / (*_p)->GDD_EMERGENCE());
        }
        return ( 1.0);
    }
    else
    {
        if ( days_after_emergence[_p->slot] < 0)
        {
            return 0.0;
        }
        else
        {
            if ( cbm::flt_less( _p->dvsFlush, 0.95))
            {
                return ( 1.0);
            }
            else
            {
                return cbm::bound_min( 0.0, (1.0 - _p->dvsFlush) / 0.05);
            }
        }
    }
}


/*!
 * @page plamox
 * \subsection plamox_common_optimum_nitrogen_concentration Optimum nitrogen concentration
 * Grass: foliage nitrogen concentration is constant (from species parameter NC_FOLIAGE_MAX) \n
 * Crops: foliage nitrogen concentration is highest at planting (NC_FOLIAGE_MAX)
 *        and decreases until harvest to NC_FOLIAGE_MIN.
 */
double
PhysiologyPlaMox::PlaMox_get_foliage_nitrogen_concentration( MoBiLE_Plant *  _vt)
{
    if (_vt->groupId() == SPECIES_GROUP_GRASS)
    {
        double const ncfol_reduction_max( (*_vt)->NC_FOLIAGE_MAX() - (*_vt)->NC_FOLIAGE_MIN());
        return ( (*_vt)->NC_FOLIAGE_MAX() - _vt->dvsMort * ncfol_reduction_max);
    }
    else if ( _vt->growing_degree_days <= gdd_grain_filling[_vt->slot])
    {
        return ( (*_vt)->NC_FOLIAGE_MAX());
    }
    else
    {
        double const temp_diff( _vt->growing_degree_days - gdd_grain_filling[_vt->slot]);
        double const temp_diff_max( (*_vt)->GDD_MATURITY() - gdd_grain_filling[_vt->slot]);
        double const ncfol_reduction_max( (*_vt)->NC_FOLIAGE_MAX() - (*_vt)->NC_FOLIAGE_MIN());

        return (*_vt)->NC_FOLIAGE_MAX() - ncfol_reduction_max * cbm::bound(0.0, temp_diff / temp_diff_max, 1.0);
    }
}


ldndc::PhysiologyPlaMox::plant_life_cycle_e
PhysiologyPlaMox::PlaMox_get_life_cycle( MoBiLE_Plant *_p)
{
    if ( (_p->group() == "grass") ||
         m_veg->is_family( _p, "ment") ||
         m_veg->is_family( _p, "alfa"))
    {
        return PhysiologyPlaMox::PERENNIAL;
    }
    else
    {
        return PhysiologyPlaMox::ANNUAL;
    }
}


void
PhysiologyPlaMox::PlaMox_balance_check( int  _stage)
{
    double tot_c( - timestep_c_assi
                  + sc_.accumulated_c_litter_stubble
                  + ph_.accumulated_c_export_harvest
                  + ph_.accumulated_c_export_grazing
                  + sc_.accumulated_c_litter_above
                  + sc_.accumulated_c_litter_below_sl.sum()
                  + sc_.accumulated_c_root_exsudates_sl.sum());
    double tot_n( - automatic_nitrogen_uptake - n_at_planting
                  - m_pf.accumulated_n_uptake()
                  + sc_.accumulated_n_litter_stubble
                  + ph_.accumulated_n_export_harvest
                  + ph_.accumulated_n_export_grazing
                  + sc_.accumulated_n_litter_above
                  + sc_.accumulated_n_litter_below_sl.sum());

    for ( PlaMoxIterator vt = begin(); vt != end(); ++vt)
    {
        MoBiLE_Plant *p = *vt;
        tot_c += p->total_biomass() * cbm::CCDM + p->rGro + p->rRes + p->rTra;

        tot_n += p->total_nitrogen() - n2_fixation[p->slot];
    }
    if ( _stage == ON_ENTRY)
    {
        tot_c_ = tot_c;
        tot_n_ = tot_n;
    }
    else if ( _stage == ON_EXIT)
    {
        double difference( tot_c_ - tot_c);
        if ( cbm::flt_greater( std::abs( difference), cbm::DIFFMAX))
        {
            KLOGWARN("C-Leak:  difference is ", difference * cbm::M2_IN_HA, " kg C ha-1");
        }

        difference = tot_n_ - tot_n;
        if ( cbm::flt_greater( std::abs( difference), cbm::DIFFMAX))
        {
            KLOGWARN("N-Leak:  difference is ", difference * cbm::M2_IN_HA, " kg N ha-1");
        }
    }
    else
    {
        KLOGFATAL( "[BUG]  Unknown stage for Balance Check");
    }
}


/*
 * @brief helping function for debugging
 *    checks for all kind of non-positive numbers (including nan)
 */
lerr_t
PhysiologyPlaMox::PLAMOX_CHECK_FOR_NEGATIVE_VALUE( char const *  _position)
{
    for ( PlaMoxIterator vt = begin(); vt != end(); ++vt)
    {
        MoBiLE_Plant *p = *vt;

        if (!(n_plant[p->slot] >= 0.0)
            ||!(p->mFol >= 0.0)
            ||!(p->dw_lst >= 0.0)
            ||!(p->mBud >= 0.0)
            ||!(p->mFrt >= 0.0))
        {
            if (!(n_plant[p->slot] >= 0.0))
            {
                KLOGERROR( "Illegal value for n_plant at ", _position, ": ", n_plant[p->slot]);
            }
            if (!(p->mFol >= 0.0))
            {
                KLOGERROR( "Illegal value for mFol at ", _position, ": ", p->mFol);
            }
            if (!(p->dw_lst >= 0.0))
            {
                KLOGERROR( "Illegal value for dw_lst at ", _position, ": ", p->dw_lst);
            }
            if (!(p->mBud >= 0.0))
            {
                KLOGERROR( "Illegal value for mBud at ", _position, ": ", p->mBud);
            }
            if (!(p->mFrt >= 0.0))
            {
                KLOGERROR( "Illegal value for mFrt at ", _position, ": ", p->mFrt);
            }

            return LDNDC_ERR_FAIL;
        }
    }

    return LDNDC_ERR_OK;
}


lerr_t
PhysiologyPlaMox::register_ports( cbm::io_kcomm_t *  _io_kcomm)
{
    lerr_t  rc = LDNDC_ERR_OK;

    aboveground_biomass.publish( "AbovegroundBiomass", "kg/m2", _io_kcomm);
    maturity_status.publish( "MaturityStatus", "-", _io_kcomm);
    growing_degree_days_maturity.publish( "GrowingDegreeDays", "-", _io_kcomm);
    dEmerg.publish("dEmerg_send", "-", _io_kcomm);

    carbon_plant_biomass_total_exported_from_field.publish( "carbon_plant_biomass_total_exported_from_field", "kg/m2", _io_kcomm);
    carbon_plant_biomass_fruit_exported_from_field.publish( "carbon_plant_biomass_fruit_exported_from_field", "kg/m2", _io_kcomm);
    nitrogen_plant_biomass_total_exported_from_field.publish( "nitrogen_plant_biomass_total_exported_from_field", "kg/m2", _io_kcomm);
    nitrogen_plant_biomass_fruit_exported_from_field.publish( "nitrogen_plant_biomass_fruit_exported_from_field", "kg/m2", _io_kcomm);

    rc = m_eventcut.register_ports( _io_kcomm);
    if ( rc)
        { return LDNDC_ERR_FAIL; }

    rc = m_eventgraze.register_ports( _io_kcomm);
    if ( rc)
        { return LDNDC_ERR_FAIL; }

    int rc_harvest = this->m_HarvestEvents.subscribe( "harvest", _io_kcomm);
    if ( rc_harvest != LD_PortOk)
        { return  LDNDC_ERR_FAIL; }

    int rc_plant = this->m_PlantEvents.subscribe( "plant", _io_kcomm);
    if ( rc_plant != LD_PortOk)
        { return  LDNDC_ERR_FAIL; }

    return rc;
}


lerr_t
PhysiologyPlaMox::unregister_ports( cbm::io_kcomm_t *  _io_kcomm)
{
    aboveground_biomass.unpublish();
    maturity_status.unpublish();
    growing_degree_days_maturity.unpublish();
    dEmerg.unpublish();

    carbon_plant_biomass_total_exported_from_field.unpublish();
    carbon_plant_biomass_fruit_exported_from_field.unpublish();
    nitrogen_plant_biomass_total_exported_from_field.unpublish();
    nitrogen_plant_biomass_fruit_exported_from_field.unpublish();

    m_eventcut.unregister_ports( _io_kcomm);
    m_eventgraze.unregister_ports( _io_kcomm);

    m_HarvestEvents.unsubscribe();
    m_PlantEvents.unsubscribe();

    return  LDNDC_ERR_OK;
}

} /*namespace ldndc*/
