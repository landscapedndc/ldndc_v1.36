import os

import numpy as np
import math

from mpl_toolkits.mplot3d import axes3d
import matplotlib.pyplot as plt

import xml.etree.ElementTree as xml

script_dir = os.path.dirname( __file__)

parameterfile = script_dir+'/../../../../../resources/parameters-site.xml'
xmlparameters = xml.parse( parameterfile)
xmlparameters = xmlparameters.getroot()
xmlparameters = xmlparameters.find( 'siteparameters')

def get_par( _name, _xml) :
    for elem in _xml :
        if elem.attrib.get( 'name').lower() == _name.lower() :
            return float(elem.attrib.get( 'value', ''))
    return None
    
def weibull( _x, _p1, _p2, _p3) :
    return  1.0 - ( _p3 / ( 1.0 + math.exp((_x - _p1) * _p2)))
def temp( _x, _p1, _p2) :
    return math.exp(-_p1 * (1.0 - _x / _p2)**2.0)
def oneill( _x, _p_max, _p_opt, _exp) :
    d0 = _p_max - _x
    d1 = _p_max - _p_opt
    return  (d0 / d1)**_exp * math.exp( _exp * ( d1 - d0) / d1)
def logisitc( _x, _p1, _p2) :
    return 1.0 / (1.0 + math.exp(-_p1 * (_x-_p2)))


#fig = plt.figure()
#ax = fig.add_subplot(111, projection="3d")
#X, Y = np.mgrid[0:40:1, 0:1:0.02]
#
#t1 = get_par('METRX_F_DECOMP_T_EXP_1', xmlparameters)
#t2 = get_par('METRX_F_DECOMP_T_EXP_2', xmlparameters)
#
#w1 = get_par('METRX_F_DECOMP_M_WEIBULL_1', xmlparameters)
#w2 = get_par('METRX_F_DECOMP_M_WEIBULL_2', xmlparameters)
#w3 = get_par('METRX_F_DECOMP_M_WEIBULL_3', xmlparameters)
#
##zs = np.array([temp(x, t1, t2) * weibull(y, w1, w2, w3) for x,y in zip(np.ravel(X), np.ravel(Y))])
#zs = np.array([2.0 / (1.0/temp(x, t1, t2) + 1.0/weibull(y, w1, w2, w3)) for x,y in zip(np.ravel(X), np.ravel(Y))])
#Z = zs.reshape(X.shape)
#
#ax.plot_surface(X, Y, Z, cmap="winter", rstride=1, cstride=1)
#ax.set_title('Temperature-moisture factor')
#ax.set_xlabel('T [oC]')
#ax.set_ylabel('wfps [-]')
#ax.set_zlabel('response [-]')
#plt.savefig(script_dir+"/../figures/temp_moist.png")



fig = plt.figure( figsize=(8, 4))
ax = fig.add_subplot(1,2,1)
temp = np.arange(30, 50, 0.1)
temp_response = [1.0-logisitc(i, 2.0, 40.0) for i in temp]
ax.plot(temp, temp_response)
#ax.set_ylim(0, 1.0)
plt.savefig(script_dir+"/../figures/heat_stress.png")
