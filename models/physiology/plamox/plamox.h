/*!
 * @file
 * @author
 *  - David Kraus
 *  - Steffen Klatt
 *
 * @date
 *  May 12, 2014
 */

#ifndef  LM_PHYSIOLOGY_PLAMOX_H_
#define  LM_PHYSIOLOGY_PLAMOX_H_

#include  "mbe_legacymodel.h"
#include  "state/mbe_state.h"
#include  "ld_shared.h"

#include  "physiology/ld_plantfunctions.h"
#include  "physiology/ld_rootsystem.h"
#include  "physiology/photofarquhar/berryball.h"

#include  "eventhandler/graze/graze.h"
#include  "eventhandler/cut/cut.h"

#include  <containers/lgrowarray.h>

namespace ldndc {

extern cbm::logger_t *  PhysiologyPlaMoxLogger;

struct BaseRootSystemDNDC;
class  NitrogenFixation;

/*!
 * @brief
 * Plamox
 *
 *
 * @author
 *    David Kraus
 */
class  LDNDC_API  PhysiologyPlaMox  :  public  MBE_LegacyModel
{
    LMOD_EXPORT_MODULE_INFO(PhysiologyPlaMox,"physiology:plamox","Physiology PlaMox");

    enum plant_life_cycle_e
    {
        ANNUAL = 0,
        BIENNIAL,
        PLURIENNIAL,
        PERENNIAL
    };

public:

    PhysiologyPlaMox(
                     MoBiLE_State *,
                     cbm::io_kcomm_t *,
                     timemode_e);

    /*!
     * @brief
     *      Delete allocated classes
     */
    ~PhysiologyPlaMox();

    lerr_t  configure( ldndc::config_file_t const *);
    lerr_t  register_ports( cbm::io_kcomm_t *);
    lerr_t  initialize();

    lerr_t  solve();
    lerr_t  unregister_ports( cbm::io_kcomm_t *);
    lerr_t  finalize() { return  LDNDC_ERR_OK; }

    lerr_t  sleep() { return  LDNDC_ERR_OK; }
    lerr_t  wake() { return  LDNDC_ERR_OK; }

private:

    MoBiLE_State *  m_state;
    cbm::io_kcomm_t *  io_kcomm;

    /* required input classes */
    input_class_climate_t const &  cl_;
    input_class_setup_t const *  m_setup;
    input_class_soillayers_t const &  sl_;
    input_class_species_t const *  m_species;
    input_class_speciesparameters_t const * m_speciesparameters;
    
    MoBiLE_PlantVegetation *  m_veg;

    /* required state components */
    substate_airchemistry_t & ac_;
    substate_microclimate_t &  mc_;
    substate_physiology_t &  ph_;
    substate_soilchemistry_t &  sc_;
    substate_watercycle_t &  wc_;

    /* management handling */
    SubscribedEvent<LD_EventHandlerQueue>  m_HarvestEvents;
    SubscribedEvent<LD_EventHandlerQueue>  m_PlantEvents;

    PublishedField<double>  aboveground_biomass;
    PublishedField<double>  maturity_status;
    PublishedField<double>  growing_degree_days_maturity;
    PublishedField<double>  dEmerg;

    PublishedField<double>  carbon_plant_biomass_total_exported_from_field;
    PublishedField<double>  carbon_plant_biomass_fruit_exported_from_field;
    PublishedField<double>  nitrogen_plant_biomass_total_exported_from_field;
    PublishedField<double>  nitrogen_plant_biomass_fruit_exported_from_field;

    EventHandlerGraze  m_eventgraze;
    EventHandlerCut  m_eventcut;

    BerryBall m_photo;

    /*!
     * @brief
     *     Nitrogen fixation.
     */
    NitrogenFixation *NitrogenFixation_;

    /*!
     * @brief
     *     All kind of plant related functions.
     */
    LD_PlantFunctions  m_pf;

    lvector_t< double > no3_sl;
    lvector_t< double > nh4_sl;
    lvector_t< double > don_sl;


    /*!
     * @brief
     *     Root system
     *      - rooting depth
     *      - root distribution
     */
    ldndc::growable_array < BaseRootSystemDNDC*, 1000, 1 >  root_system;

    /*!
     * @brief
     *     Transpiration method used by PlaMox
     */
    cbm::string_t transpiration_method;

    /*!
     * @brief
     *     Transpiration method used by PlaMox
     */
    cbm::string_t droughtstress_method;

    /*!
     * @brief
     *     Plant families that are considered by PlaMox
     */
    cbm::string_t::cbmstring_array_t plantfamilies;

    /*!
     * @brief
     *      Time resolution factor:
     *      \f$ \text{FTS_TOT_} = \frac{1.0}{\text{time resolution}} \f$
     */
    double const FTS_TOT_;


    /***********************************/
    /* Mobile state exchange variables */
    /***********************************/

    double const *mc_temp;

    lvector_t< long long int > seconds_crop_planting;

    lvector_t< double > gdd_grain_filling;

    lvector_t< double > n2_fixation;

    lvector_t< int > days_after_emergence;

    lvector_t< std::string > location;
    lvector_t< double > reserves_stem;

    lvector_t< double > dcLst;
    lvector_t< double > dcFru;

    lvector_t< double > temp_sl;
    lvector_t< double > temp_fl;
    lvector_t< double > vpd_fl;

    /*************************/
    /* PlaMox only variables */
    /*************************/

    /*---------------*/
    /* model options */

    /*!
     * @brief
     */
    bool have_automatic_nitrogen;

    /*!
     * @brief
     */
    double automatic_nitrogen_uptake;

    /*-----------------------------*/
    /* carbon and nitrogen balance */

    double photoperiod_min;
    double photoperiod_max;
    double tot_c_;
    double tot_n_;

    double timestep_c_assi;

    /*!
     * @brief
     */
    double n_at_planting;

    /*-----------------*/
    /* state variables */

    /*!
     * @brief
     */
    lvector_t< double > transplanting_shock_vt;

    /*!
     * @brief
     */
    lvector_t< int > yearly_cuts;

    /*!
     * @brief
     */
    lvector_t< bool > reset_phenology;
    
    /*!
     * @brief
     */
    lvector_t< double > n_plant;

    /*!
     * @brief
     * Allocation factor of daily assimilated carbon to leaf growth [-]
     */
    lvector_t< double >  allocation_factor_leafs;

    /*!
     * @brief
     * Allocation factor of daily assimilated carbon to stem growth [-]
     */
    lvector_t< double >  allocation_factor_stems;

    /*!
     * @brief
     * Allocation factor of daily assimilated carbon to fruit growth [-]
     */
    lvector_t< double >  allocation_factor_fruit;

    /*!
     * @brief
     * Allocation factor of daily assimilated carbon to root growth [-]
     */
    lvector_t< double >  allocation_factor_roots;

    /*!
     * @brief
     *  Minimum value of üplamt leaf area index (m^2:m^-2).
     *  For crops, this value is fixed until between sowing and emergence.
     */
    lvector_t< double >  lai_min;

    /*!
     * @brief
     *  Density of seeded plants with regard to optimum (full area) seeding (-).
     */
    lvector_t< double >  fractional_cover;

    /*!
     * @brief
     *  Factor that retards plant development if vernalization requirement is not fullfilled.
     */
    lvector_t< double >  chill_factor;

    /*!
     * @brief
     *  Accumulated chilling units for vernalization;
     */
    lvector_t< double >  chill_units;

    /*!
     * @brief
     *
     */
    lvector_t< double >  m_fruit_maximum;

    /*!
     * @brief
     *
     */
    lvector_t< double >  nc_fol_opt;

    /*!
     * @brief
     * Largest soil layer index where roots exist [-]
     */
    lvector_t< unsigned int >  root_q_vt_;

    /*!
    * @brief
    * store daytime temperature for heat stress analysis 
    */
    lvector_t< double > daytime_temperatures; 

    /*!
    * @brief
    * store daily impact factor of heat stress on grain yield;
    */
    lvector_t< double > influence_heat_daily;

    /*!
   * @brief
   * Resulting impact factor of the heat stress influence on grain yield
   */
    double influence_heat_reduction_grainfilling;

    
    /*!
     * @brief day after emergence where flowering is onset
     */
    double day_of_flowering;

    /*!
    * @brief
    * the timing of the episode of high temperatures relative to flowering
    */
    //double timing_heatshock;
    /*!
    * @brief
    * the duration of the episode of high temperatures relative to flowering
    */
    //double duration_heatshock;

    /*!
    * @brief
    * The critical temperature at which temperatuer affects pod-set, dependent on timing and duration of the heatshock (°C) 
    */
    double Tcrit;
    /*!
    * @brief
    * The temperature at which there is zero pod-set (°C) 
    */
    double Tzero;

    double c_total_exported;
    double c_fruit_exported;
    double n_total_exported;
    double n_fruit_exported;


    /********************/
    /* function members */
    /********************/

    /*!
     * @brief
     *      pre-run initialization each time step
     */
    lerr_t
    PlaMox_step_init();

    /*!
     * @brief
     *      resize plant species vector
     */
    lerr_t
    PlaMox_step_resize();
    /*!
     * @brief
     *
     */
    lerr_t
    PlaMox_step_out();

    /*!
     * @brief
     * Applies grazing and cutting
     */
    lerr_t
    PlaMox_management();

    /*!
     * @brief
     *
     */
    lerr_t
    PlaMox_planting();

    /*!
     * @brief
     *    handle plant event
     *
     * @param
     *    species
     *
     * @return
     *    LDNDC_ERR_OK if no plant event is pending
     *    or everything went well.
     *
     *    ...
     */
    lerr_t
    PlaMox_event_plant( MoBiLE_Plant *, EventAttributes const &);

    /*!
     * @brief
     *
     */
    lerr_t
    PlaMox_harvest();

    /*!
     * @brief
     *    handle harvest event
     *
     * @param
     *    species
     *
     * @return
     *    LDNDC_ERR_OK if no harvest event is pending
     *    or everything went well.
     *
     *    ...
     */
    lerr_t
    PlaMox_event_harvest( MoBiLE_Plant *, EventAttributes const &);

    lerr_t
    PlaMox_reset_phenology( MoBiLE_Plant * /* plant species */);

    /*!
     * @brief
     * Update of information needed by photofarquhar
     */
    lerr_t
    PlaMox_photosynthesis( MoBiLE_Plant * /* plant species */);

    /*!
     * @brief
     * Determines the growing degree days and the plant development stage based on if vernalization requirements are fullfilled or not
     */
    void
    PlaMox_vernalization( MoBiLE_Plant * /* plant species */);

    /*!
     * @brief
     *  Calculates growing degree days depending
     *  on daily average temperature.
     */
    void
    PlaMox_growing_degree_days( MoBiLE_Plant * /* plant species */);

    /*!
    * @brief
    *  Limits pod filling due to heat stress around anthesis
    */
    void
    PlaMox_heat_stress_limitation(MoBiLE_Plant* _vt /* plant species */);

    /*!
     * @brief
     *
     */
    void
    PlaMox_plant_development_crop( MoBiLE_Plant * /* plant species */);

    /*!
     * @brief
     * Allocation metrics for grass species.
     */
    lerr_t
    PlaMox_allocation_grass( MoBiLE_Plant * /* plant species */);

    /*!
     * @brief
     * Burst of buds
     */
    lerr_t
    PlaMox_bud_burst( MoBiLE_Plant * /* plant species */);

    /*!
     * @brief
     * Redistribution of reserves
     */
    lerr_t
    PlaMox_redistribution( MoBiLE_Plant * /* plant species */);
    
    /*!
     * @brief
     * Calculates maintenance/residual and growth respiration
     */
    void
    PlaMox_respiration( MoBiLE_Plant * /* plant species */);

    /*!
     * @brief
     * Determines root exsudation as fraction of root growth respiration
     */
    void
    PlaMox_exsudation( MoBiLE_Plant * /* plant species */);

    /*!
     * @brief
     * Calculates plant nitrogen uptake based on daily demand
     */
    void
    PlaMox_nitrogen_uptake( MoBiLE_Plant * /* plant species */);

    /*!
     * @brief
     * Calculates plant nitrogen fixation.
     */
    lerr_t
    PlaMox_nitrogen_fixation( MoBiLE_Plant * /* plant species */);

    /*!
     * @brief
     * Calculates aboveground and belowground plant senecsence
     */
    lerr_t
    PlaMox_senescence( MoBiLE_Plant * /* plant species */);

    /* senescence */
    lerr_t
    PlaMox_abscission( MoBiLE_Plant * /* plant species */);
    
    /*!
     * @brief
     * Calculates potential transpiration
     */
    void
    PlaMox_transpiration( MoBiLE_Plant * /* plant species */);

    /*!
     * @brief
     * Updates all relevant root structural matters
     */
    void
    PlaMox_update_root_structure( MoBiLE_Plant * /* plant species */,
                                  double /* deltamassFrt */);

    /*!
     * @brief
     *
     */
    void
    PlaMox_update_height( MoBiLE_Plant * /* plant species */);

    /*!
     * @brief
     *
     */
    void
    PlaMox_update_ground_cover( MoBiLE_Plant * /* plant species */);

    /*!
     * @brief
     *      Calculation of canopy layer properties:
     *       - foliage biomass
     *       - foliage biomass fraction
     *       - leaf area index
     */
    void
    PlaMox_update_foliage_structure( MoBiLE_Plant * /* plant species */);

    /*!
     * @brief
     *      Calculates specific leaf area (sla) in each canopy layer
     */
    void
    PlaMox_update_specific_leaf_area( MoBiLE_Plant * /* plant species */);

    /*!
     * @brief
     *
     */
    void
    PlaMox_update_nitrogen_concentrations( MoBiLE_Plant * /* plant species */);

    /*!
     * @brief
     *
     */
    double
    PlaMox_get_nitrogen_deficiency( MoBiLE_Plant * /* plant species */);

    /*!
     * @brief
     * Calculates an age factor
     */
    double
    PlaMox_get_age_factor( MoBiLE_Plant * /* plant species */);

    /*!
     * @brief
     * Calculates a temperature factor
     */
    double
    PlaMox_get_frost_factor( MoBiLE_Plant * /* plant species */,
                             double /* temperature */);

    /*!
     * @brief
     * Calculates a temperature factor
     */
    double
    PlaMox_get_heat_factor( MoBiLE_Plant * /* plant species */);

    /*!
     * @brief
     * Calculates optimum foliage nitrogen concentration
     */
    double
    PlaMox_get_foliage_nitrogen_concentration( MoBiLE_Plant * /* plant species */);

    /*!
     * @brief
     * Determines optimum nitrogen concentration
     */
    double
    PlaMox_n_opt( MoBiLE_Plant * /* plant species */);

    /*!
     * @brief
     *
     */
    void
    PlaMox_balance_check( int /*stage*/);

    /*!
     * @brief
     */
    lerr_t  PLAMOX_CHECK_FOR_NEGATIVE_VALUE( char const *  /*name*/);


    /*****************/
    /* class members */
    /*****************/

    /*!
     * @brief
     *
     */
    class PlaMoxIterator
    {
        public:

            PlaMoxIterator(MoBiLE_PlantVegetation *_vg,
                           cbm::string_t::cbmstring_array_t &_f,
                           PlantIterator const &_it) :
                            vegetation( _vg),
                            plantfamilies( _f),
                            iterator( _it)
            {
                if ( this->vegetation && this->not_my_family())
                {
                    this->find_next();
                }
            }
            ~PlaMoxIterator(){}

        private:

            bool not_my_family()
            {
                if ( this->iterator != this->vegetation->end())
                {
                    for (size_t i = 0; i < this->plantfamilies.size(); ++i)
                    {
                        cbm::string_t family = ":";
                        family << this->plantfamilies[i] <<":";
                        if ( this->vegetation->is_family( this->iterator, family.c_str()))
                        {
                            return false;
                        }
                    }
                }
                return true;
            }

            void find_next()
            {
                while ( ++this->iterator != this->vegetation->end() && not_my_family())
                {}
            }

        public:

            PlaMoxIterator operator++()
            {
                this->find_next();
                return *this;
            }

            MoBiLE_Plant * operator*()
            {
                return this->iterator.operator*();
            }

            bool operator==(const PlaMoxIterator &_it) const
            {
                return this->iterator.operator==( _it.iterator);
            }

            bool operator!=(const PlaMoxIterator &_it) const
            {
                return this->iterator.operator!=( _it.iterator);
            }

        private:
            MoBiLE_PlantVegetation *vegetation;
            cbm::string_t::cbmstring_array_t plantfamilies;
            PlantIterator iterator;
    };

    PlaMoxIterator begin();

    PlaMoxIterator end();
    
    plant_life_cycle_e
    PlaMox_get_life_cycle( MoBiLE_Plant *);
};
} /*namespace ldndc*/

#endif  /*  !LM_PHYSIOLOGY_PLAMOX_H_  */
