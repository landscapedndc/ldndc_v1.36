/*!
 * @file
 *      Calculation and conversion of common emission relevant factors      
 * @author
 *      felix wiss (created on: april 5, 2017)
 */

#include  "physiology/ld_vocemissionhelper.h"
#include  "mbe_plant.h"

#include  <math/cbm_math.h>
#include  <constants/cbm_const.h>
#include  <scientific/meteo/ld_meteo.h>

#include  <logging/cbm_logging.h>

/*!
 *      standard emission factors are used
 */
static lerr_t
ldndc_use_standard_emission_factors( ldndc::MoBiLE_Plant * _vt, int _fl)
{
    LOGINFO_ONCE( "Usage of standard emission factors for voc emissions");
    _vt->ef_iso_fl[_fl] = ( *_vt)->EF_ISO();
    _vt->ef_mono_fl[_fl] = ( *_vt)->EF_MONO();
        
    return  LDNDC_ERR_OK;
}

/*! 
 *      Calculation of the phenological status (pstatus)
 */
static double
ldndc_calc_pstatus( ldndc::MoBiLE_Plant const * _vt)
{
    double  pstatus( 0.0);
    if (( _vt->nb_ageclasses() > 1) && ( _vt->mFol > 0.0))
    {
        // evergreen
        LOGINFO_ONCE("According to age classes we are evergreen!");
        pstatus = ( _vt->dvsFlush * _vt->mFol_na[0] + ( 1.0 - _vt->dvsMort) * 
            ( cbm::sum( _vt->mFol_na, _vt->nb_ageclasses()) - _vt->mFol_na[0])) / _vt->mFol;
    }
    else
    {
        // deciduous
        LOGINFO_ONCE("According to age classes we are deciduous!");
        pstatus = std::min( _vt->dvsFlush, 1.0 - _vt->dvsMort);
    }
    
    return  pstatus;
}



/*! 
 *      activity of emission relevant enzymes
 *      after Lehning et al. 1999, 2001 (S (easonal) I (soprene synthase) M (odel))
 *      parameter of q.ilex by Grote et al. 2006
 * 
 *      function to calc daily mean isoprene/monoterpene synthase activity with PPFD m-2 (lehning, 2001) or PFD m-2 (grote, 2006)
 *      Lehning, 2001 (ch. 4.3): light + temp conditions from preceding day!!!!
 */
static lerr_t
ldndc_calc_voc_synthase_activity( ldndc::MoBiLE_Plant * _vt, int _fl,
            double _latitude, int _yearday,
            double _foliage_temperature, double _foliage_radiation)
{
        
    double const pstatus( ldndc_calc_pstatus( _vt));
    
    if ( pstatus > 0.0)
    {   
        
        // fw: do i really need this here??
//         nd_temp_fl.resize_and_preserve( se_.canopylayers(), this->_mc->nd_airtemperature);
//         nd_shortwaveradiation_fl.resize_and_preserve( se_.canopylayers(), this->_mc->nd_shortwaveradiation_in);
        
        /* fw: consider last (=current) time step in daily means for subdaily sim */
        double const  nd_temp_fl( _foliage_temperature);
        double const  nd_shortwaveradiation_fl( _foliage_radiation);
        
        double const  daylength_hours( meteo::daylength( _latitude, _yearday )); 
        /* daylength normalized to 12 h; i.e. 12 h = 1; Lehning et al. 2001 */
        double const  dayl_norm( daylength_hours / ( cbm::HR_IN_DAY * 0.5));
        /* daily sum of global photon flux density [umol m-2] (not PAR --> PPFD, but PFD) */
        double const  nd_photonfluxdens( nd_shortwaveradiation_fl * cbm::UMOL_IN_W * dayl_norm);
        /* Ar
        enius equation/factpr [-] */
        double const  arrh((*_vt)->PA() * exp( -(*_vt)->AEIS() / ( cbm::RGAS * ( nd_temp_fl + cbm::D_IN_K))));

        /* increase of emission activity per wheighted temperature unit [s-1] */
        double  alpha( (*_vt)->ALPHA0_IS() * pstatus);
        double  act_decay( (*_vt)->MUE_IS() * _vt->isoAct_fl[_fl]);

        // (nmol "Isoprene production" m-2 (leaf area) s-1)
        _vt->isoAct_fl[_fl]  += ( alpha * nd_photonfluxdens * arrh - act_decay);

        /* calculation for monoterpene synthase activity */
        alpha    = (*_vt)->ALPHA0_MT() * pstatus;
        act_decay    = (*_vt)->MUE_MT() * _vt->monoAct_fl[_fl];

        // (nmol "Monoterpene production" m-2 (leaf area) s-1)
        _vt->monoAct_fl[_fl] += ( alpha * nd_photonfluxdens * arrh - act_decay);

        if ( _yearday > 330)
        {
            _vt->isoAct_fl[_fl]  /= ( double( _yearday - 330));
            _vt->monoAct_fl[_fl] /= ( double( _yearday - 330));
        }

        _vt->isoAct_fl[_fl] = cbm::bound_min( 0.0, _vt->isoAct_fl[_fl]);
        _vt->monoAct_fl[_fl] = cbm::bound_min( 0.0, _vt->monoAct_fl[_fl]);
        
        
        if ( _vt->isoAct_fl[_fl]  < 0.001)
            { _vt->isoAct_fl[_fl]  = 0.0; }
        if ( _vt->monoAct_fl[_fl] < 0.001)
            { _vt->monoAct_fl[_fl] = 0.0; }

    }
    else
    {
        _vt->isoAct_fl[_fl]  = 0.0;
        _vt->monoAct_fl[_fl] = 0.0;
    }
    
    return LDNDC_ERR_OK;
}



/*!
 *  Daily calculation of voc (isoprene and monoterpenes) synthase activity
 *   NOTE that fCO2 depends on outside CO2, internal CO2 would probably be better (explaining relatively high emissions when stomata closed)
 */
lerr_t
ldndc::get_voc_emission_factors( ldndc::MoBiLE_Plant * _vt, int _fl,
        bool _calc_synthase_activity, double _latitude, int _yearday,
            double _foliage_temperature, double _foliage_radiation)
{
// if running subdaily calc activity on last subdaily time step    
   
    
//     if ("beginning of simulation and first time that pointers are assigned")
//         --> assign ef with sef
//         --> assign ef with iso/monoAct
//         
//     else if ( "further within simulation ")
//         --> assign ef with iso/monoAct
        
    
    
    if ( !_calc_synthase_activity)
    {
        ldndc_use_standard_emission_factors( _vt, _fl);
        return  LDNDC_ERR_OK;
    }
    
    else if (( _calc_synthase_activity) && (( ( *_vt)->ALPHA0_IS() + ( *_vt)->ALPHA0_MT()) > 0.0) && ( ( *_vt)->PA() > 0.0))
    {
        LOGINFO_ONCE( "Calculation of voc synthase activity instead of standard emission factors");
        ldndc_calc_voc_synthase_activity( _vt, _fl, _latitude, _yearday, _foliage_temperature, _foliage_radiation);
            
         /* factors for conversion from enzyme activity (nmol m-2 (leaf area) s-1) to emission factor (ug component gDW-1 h-1) */
        double const  lsw = cbm::G_IN_KG / _vt->sla_fl[_fl];
        double const  UNIT_CONV = cbm::SEC_IN_HR * cbm::UMOL_IN_NMOL; 
        
        /* "isoAct_vtfl"/"monoAct_vtfl" [nmol enzyme product m-2 leaf area s-1] activity of isoprene/monterpene synthase to produce isoprene/monoterpenes */
        _vt->ef_iso_fl[_fl]  = _vt->isoAct_fl[_fl] * cbm::MC5H8 * UNIT_CONV / ( lsw * ( *_vt)->SCALE_I());
        _vt->ef_mono_fl[_fl] = _vt->monoAct_fl[_fl] * cbm::MC10H16 * UNIT_CONV / ( lsw * ( *_vt)->SCALE_M());

        
        return LDNDC_ERR_OK;
    }
    
    else if (( _calc_synthase_activity) && ((( ( *_vt)->ALPHA0_IS() + ( *_vt)->ALPHA0_MT()) <= 0.0) || ( ( *_vt)->PA() <= 0.0)))
    {
        LOGWARN_ONCE( "Dynamic calculation of emission activity not possible",
                " because species specific paramters ALPHA0_IS + ALPHA0_MT, or PA is equal or less than zero",
                "  [ALPHA0_IS=",( *_vt)->ALPHA0_IS(),",ALPHA0_MT=",( *_vt)->ALPHA0_MT(),",PA=",( *_vt)->PA(),"]", 
                " Thus standard emission factors are used!");
        
        ldndc_use_standard_emission_factors( _vt, _fl);
        return  LDNDC_ERR_OK;

    }
    
    else
    {
        LOGERROR("Wrong configuration of CalcSynthaseActivity, CalcParTempDependence, or species specific parameters...");
        return LDNDC_ERR_FAIL;
    }

}


