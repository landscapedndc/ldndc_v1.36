/*!
 * @file
 * @brief
 *    Functions related to sap-wood biomass (implementation)
 *
 * @author
 *    Ruediger Grote,
 *    David Kraus 
 *
 * @date April, 2017
 */

#include  "physiology/ld_sapwoodbiomass.h"


/*!
 * @page veglibs
 * @section veglibs_sapwood Sapwood
 * @subsection veglibs_sapwood_optimum_sap_wood_biomass Optimum sapwood biomass
 * Assumptions:
 *   - Sap wood area is constant from the ground to crown base
 *   - Stem in the crown is only sapwood,
 *   - Stem in the crown + branches are cone shaped
 *   - Coarse root volume is cone shaped
 */
double
ldndc::get_optimum_sap_wood_biomass( MoBiLE_Plant *_p)
{
    double const sapwood_area_demand( _p->lai_max  * _p->qsfa);
    double  optimum_sap_wood( (*_p)->DSAP() * cbm::DM3_IN_M3 * sapwood_area_demand
                             * ( _p->height_at_canopy_start
                                + (_p->rooting_depth / 3.0)
                                + (_p->height_max - _p->height_at_canopy_start) / 3.0));

    if ( (*_p)->IS_WOOD())
    {
        double const total_wood( _p->aboveground_wood() + _p->belowground_wood());
        return cbm::bound_max( optimum_sap_wood, total_wood);
    }
    else
    {
        return optimum_sap_wood;
    }
}



/*!
 * @details
 *
 */
double
ldndc::get_sapwood_foliage_ratio( MoBiLE_Plant *_p)
{
    double const biomass_limit( 0.001);

    if ( !(*_p)->FREEGROWTH() && (_p->nb_ageclasses() > 0) )
    {
        size_t na_max( _p->nb_ageclasses() - 1);
        if ( cbm::flt_greater( _p->mBud + _p->mFol - _p->mFol_na[na_max], biomass_limit))
        {
            double const m_sap_opt( get_optimum_sap_wood_biomass(_p));
            return m_sap_opt / ( _p->mBud + _p->mFol - _p->mFol_na[na_max]);
        }
    }
    else if ( cbm::flt_greater( _p->mFol + _p->mBud, biomass_limit))
    {
        double const m_sap_opt( get_optimum_sap_wood_biomass(_p));
        return m_sap_opt / ( _p->mFol + _p->mBud);
    }

    return 0.0;
}



/*!
 * @page veglibs
 * @subsection veglibs_sap_fol_area_ratio Sapwood to foliage area ration
 * Ratio between sapwood and foliage area (Huber Value)
 * The sapwood to foliage area relationship (m2 sapwood area m-2 leaf area) is calculated
 * following @cite koestner:2002a, which is assumed to scale linearly with tree height.
 * \f[
 *   qsfa = QSF\_{P1} + height \cdot QSF\_{P2}
 * \f]
 * with
 * - height: tree height (m), limited to 0.5 HLIMIT
 * - QSF_P1: species-specific allometric parameter
 * - QSF_P2: species-specific allometric parameter
 *
 */
double
ldndc::sapwood_foliage_area_ratio(
                                  double _height,
                                  double _qsf_p1,
                                  double _qsf_p2)
{
    return (_qsf_p1 + std::max( 0.5 * cbm::HLIMIT, _height) * _qsf_p2) * cbm::M2_IN_CM2;
}
