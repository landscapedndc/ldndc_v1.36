/*!
 * @file
 * @brief
 *    Functions related to sap-wood biomass
 *
 * @author
 *    Ruediger Grote,
 *    David Kraus 
 *
 * @date April, 2017
 */

#ifndef  LD_SAPWOODBIOMASS_H_
#define  LD_SAPWOODBIOMASS_H_

#include  "mbe_plant.h"


namespace ldndc {

/*!
 * @brief
 *
 * @param
 *    _p Plant
 *
 * @return
 *    Optimum sap wood biomass depending on foliage biomass.
 */
double
get_optimum_sap_wood_biomass(
                             MoBiLE_Plant *_p);


/*!
 * @brief
 *
 * @param
 *    _p Plant
 *
 * @return
 *    sap wood to foliage ratio
 */
double
get_sapwood_foliage_ratio(
                          MoBiLE_Plant *_p);


/*!
 * @brief
 
 * @param
 *    _height Tree height
 * @param
 *    _qsf_p1 species parameter
 * @param
 *    _qsf_p2 species parameter
 *
 * @return
 *    Ratio of sap wood area to foliage area.
 */
double
sapwood_foliage_area_ratio(
                           double _height,
                           double _qsf_p1,
                           double _qsf_p2);
} /* namespace ldndc */

#endif  /*  !LD_SAPWOODBIOMASS_H_  */
