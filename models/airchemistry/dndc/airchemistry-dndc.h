/*! 
 * @brief
 *
 * @author
 *    David Kraus (created on: october 24, 2014)
 *
 */

#ifndef  LM_AIRCHEMISTRYDNDC_H_
#define  LM_AIRCHEMISTRYDNDC_H_

#include  "mbe_legacymodel.h"
#include  "state/mbe_state.h"
#include  "ld_shared.h"
#include  "ld_isotopes.h"

namespace ldndc {

class LDNDC_API AirchemistryDNDC : public MBE_LegacyModel
{
    LMOD_EXPORT_MODULE_INFO(AirchemistryDNDC,"airchemistry:airchemistrydndc", "Airchemistry AirchemistryDNDC");
    public:
        AirchemistryDNDC(
                 MoBiLE_State *,
                 cbm::io_kcomm_t *,
                 timemode_e);
        
        ~AirchemistryDNDC();
        
        lerr_t  configure( ldndc::config_file_t const *) { return  LDNDC_ERR_OK; }




        lerr_t  register_ports( cbm::io_kcomm_t *);
        lerr_t  unregister_ports( cbm::io_kcomm_t *);
    
        lerr_t  initialize();
        lerr_t  receive_state();
        lerr_t  solve();
        lerr_t  send_state();
        lerr_t  finalize() { return  LDNDC_ERR_OK; }
        
        lerr_t  sleep() { return  LDNDC_ERR_OK; }
        lerr_t  wake() { return  LDNDC_ERR_OK; }
        
    private:
        input_class_setup_t const &  m_setup;
        input_class_soillayers_t const &  sl_;
        input_class_climate_t const &  cl_;

        substate_airchemistry_t &  ac_;
        substate_soilchemistry_t &  sc_;
        substate_physiology_t &  ph_;
        substate_surfacebulk_t &  sb_;
        substate_watercycle_t &  wc_;

        MoBiLE_PlantVegetation *  m_veg;
        
    private:

        double  get_precipitation() const;

        lvector_t< double > no3_sl;
        lvector_t< double > nh4_sl;

        double  accumulated_throughfall;

        Isotopes  isotope;
};

}

#endif  /* !LM_AIRCHEMISTRYDNDC_H_ */

