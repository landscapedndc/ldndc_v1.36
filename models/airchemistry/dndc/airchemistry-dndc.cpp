/*! 
 * @file
 *
 * @author
 *   - David Kraus (created on: october 24, 2014)
 *
 */

#include  "airchemistry/dndc/airchemistry-dndc.h"
#include  <input/siteparameters/siteparameters.h>

#include  <math/cbm_math.h>
#include  <constants/cbm_const.h>
#include  <time/cbm_time.h>
#include  <utils/cbm_utils.h>
#include  <logging/cbm_logging.h>

LMOD_MODULE_INFO(AirchemistryDNDC,TMODE_SUBDAILY|TMODE_POST_DAILY,LMOD_FLAG_USER);

ldndc::AirchemistryDNDC::AirchemistryDNDC(
        ldndc::MoBiLE_State *  _state,
        cbm::io_kcomm_t *  _iokcomm,
        timemode_e  _timemode)
        : MBE_LegacyModel( _state, _timemode),

          m_setup( _iokcomm->get_input_class_ref< input_class_setup_t >()),
          sl_( _iokcomm->get_input_class_ref< input_class_soillayers_t >()),
          cl_( _iokcomm->get_input_class_ref< climate::input_class_climate_t >()),
          ac_( _state->get_substate_ref< substate_airchemistry_t >()),
          sc_( _state->get_substate_ref< substate_soilchemistry_t >()),
          ph_( _state->get_substate_ref< substate_physiology_t >()),
          sb_( _state->get_substate_ref< substate_surfacebulk_t >()),
          wc_( _state->get_substate_ref< substate_watercycle_t >()),

          m_veg( &_state->vegetation),
          no3_sl( sl_.soil_layer_cnt(), 0.0),
          nh4_sl( sl_.soil_layer_cnt(), 0.0),
          accumulated_throughfall( 0.0)
{
    this->isotope.io_kcomm = _iokcomm;
}


ldndc::AirchemistryDNDC::~AirchemistryDNDC()
{
}


lerr_t
ldndc::AirchemistryDNDC::initialize()
{    
    return  LDNDC_ERR_OK;
}


lerr_t
ldndc::AirchemistryDNDC::receive_state()
{
    return  LDNDC_ERR_OK;
}

lerr_t
ldndc::AirchemistryDNDC::send_state()
{
    return  LDNDC_ERR_OK;
}

lerr_t
ldndc::AirchemistryDNDC::solve()
{
    for ( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
    {
        no3_sl[sl] = sc_.no3_sl[sl] + sc_.an_no3_sl[sl];
        nh4_sl[sl] = sc_.nh4_sl[sl];
    }

    double const prec_scale( cbm::KG_IN_G * this->get_precipitation());
    double const prec_nh4( (prec_scale * ac_.nd_nh4_wet_deposition) +
                           (ac_.nd_nh4_dry_deposition / lclock()->time_resolution() * cbm::KG_IN_G));
    double const prec_no3( (prec_scale * ac_.nd_no3_wet_deposition) +
                           (ac_.nd_no3_dry_deposition / lclock()->time_resolution() * cbm::KG_IN_G));

    double nh4_deposit( cbm::flt_greater_zero( prec_nh4)? prec_nh4 : 0.0);
    double no3_deposit( cbm::flt_greater_zero( prec_no3)? prec_no3 : 0.0);
    
    double nh4_through( 0.0);
    double no3_through( 0.0);
    double const lai( m_veg->lai());
    if ( cbm::flt_greater_zero( lai))
    {
        for ( PlantIterator vt = this->m_veg->begin(); vt != this->m_veg->end(); ++vt)
        {
            for ( size_t  fl = 0;  fl < m_setup.canopylayers();  ++fl)
            {            
                (*vt)->nh4_fl[fl] += nh4_deposit * ( (*vt)->lai_fl[fl] / lai);
                (*vt)->no3_fl[fl] += no3_deposit * ( (*vt)->lai_fl[fl] / lai);
            }
        }
    }
    else
    {
        nh4_through += nh4_deposit;
        no3_through += no3_deposit;
    }
    
    double const leaf_water( (this->m_veg->size() > 0u) ? wc_.wc_fl.sum() : 0.0);
    double const throughfall( this->wc_.accumulated_throughfall - this->accumulated_throughfall);
    this->accumulated_throughfall = this->wc_.accumulated_throughfall;
    
    if ( cbm::flt_greater_zero( leaf_water) &&
         cbm::flt_greater_zero( throughfall))
    {
        double const frac_through( throughfall / (throughfall + leaf_water));
        for ( PlantIterator vt = this->m_veg->begin(); vt != this->m_veg->end(); ++vt)
        {
            for ( size_t  fl = 0;  fl < m_setup.canopylayers();  ++fl)
            {            
                double const leaf_loos_nh4( (*vt)->nh4_fl[fl] * frac_through);
                double const leaf_loos_no3( (*vt)->no3_fl[fl] * frac_through);
                
                (*vt)->nh4_fl[fl] -= leaf_loos_nh4;
                (*vt)->no3_fl[fl] -= leaf_loos_no3;
                
                nh4_through += leaf_loos_nh4;
                no3_through += leaf_loos_no3;
            }
        }
    }
    else
    {
        for ( PlantIterator vt = this->m_veg->begin(); vt != this->m_veg->end(); ++vt)
        {
            double const leaf_loos_nh4( cbm::sum( (*vt)->nh4_fl, (*vt)->nb_foliagelayers()));
            double const leaf_loos_no3( cbm::sum( (*vt)->no3_fl, (*vt)->nb_foliagelayers()));

            nh4_through += leaf_loos_nh4;
            no3_through += leaf_loos_no3;

            for (size_t fl = 0; fl < m_setup.canopylayers(); ++fl)
            {
                (*vt)->nh4_fl[fl] = 0.0;
                (*vt)->no3_fl[fl] = 0.0;
            }
        }
    }

    if (wc_.surface_water > 0.0 || wc_.surface_ice > 0.0)
    {
        sb_.nh4_sbl[0] += nh4_through;
        sb_.no3_sbl[0] += no3_through;
    }
    else
    {
        nh4_sl[0] += nh4_through;
        no3_sl[0] += no3_through;
        
        this->isotope.rewrite(nh4_through,"nh4_throughfall",0);
        this->isotope.rewrite(no3_through,"no3_throughfall",0);
    }

    ph_.accumulated_nh4_throughfall += nh4_through;
    ph_.accumulated_no3_throughfall += no3_through;

    for ( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
    {
        double const no3_old( sc_.no3_sl[sl] + sc_.an_no3_sl[sl]);
        if ( cbm::flt_greater_zero( no3_old))
        {
            sc_.no3_sl[sl] = sc_.no3_sl[sl] / no3_old * no3_sl[sl];
            sc_.an_no3_sl[sl] = sc_.an_no3_sl[sl] / no3_old * no3_sl[sl];
        }
        else
        {
            sc_.no3_sl[sl] = no3_sl[sl] * (1.0 - sc_.anvf_sl[sl]);
            sc_.an_no3_sl[sl] = no3_sl[sl] * sc_.anvf_sl[sl];
        }

        sc_.nh4_sl[sl] = nh4_sl[sl];
    }

    return  LDNDC_ERR_OK;
}

double  ldndc::AirchemistryDNDC::get_precipitation() const
{
    return ( ((this->timemode() & TMODE_SUBDAILY) ? cl_.precip_subday( lclock_ref()) : cl_.precip_day( lclock_ref())) * cbm::M_IN_MM);
}

lerr_t
AirchemistryDNDC::register_ports( cbm::io_kcomm_t * )
{
    return LDNDC_ERR_OK;
}

lerr_t
AirchemistryDNDC::unregister_ports( cbm::io_kcomm_t * )
{
    return  LDNDC_ERR_OK;
}
