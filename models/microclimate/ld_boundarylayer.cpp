/*!
 * @file
 *
 * @author
 *    david kraus (created on: april 20, 2015)
 */

#include  "microclimate/ld_boundarylayer.h"

#include  <math/cbm_math.h>
#include  <constants/cbm_const.h>

namespace ldndc {

double
resistance_aero_abovecanopy(
                            double const &_v_wind,
                            double const &_canopy_height)
{
    double const ROUGHNESS( 0.13);

    double const v_friction( cbm::KARMAN * _v_wind);
    double const roughness( ROUGHNESS * _canopy_height);

    return  (_v_wind / v_friction * roughness);
}



double
resistance_aero_incanopy(
                         double const &_v_wind,
                         double const &_canopy_height,
                         double const &_lai)
{
    double const ROUGHNESS( 0.13);

    double const v_friction( cbm::KARMAN * _v_wind);
    double const roughness( ROUGHNESS * _canopy_height);

    return  (_v_wind / v_friction * roughness * _lai);
}



double
resistance_quasilaminar(
                        double const &_v_wind,
                        double const &_canopy_height,
                        double const &_lai)
{
    double const ROUGHNESS( 0.13);

    double const v_friction( cbm::KARMAN * _v_wind);
    double const roughness( ROUGHNESS * _canopy_height);

    return  (_v_wind / v_friction * roughness * _lai);
}



double
resistance_ground(
                  double const &_v_wind,
                  double const &_v_wind_height,
                  double const &_canopy_height,
                  double const &_lai,
                  double const &_temp,
                  double const &_D)
{
    double const schmitt( schmitt_number( _temp, _D));
    double const v_friction( friction_velocity(_v_wind, _v_wind_height, _canopy_height));

    return  (schmitt / v_friction * _lai);
}



double
reynolds_number(
                double const &_v_wind,
                double const &_v_wind_height,
                double const &_canopy_height,
                double const &_temp)
{
    double z0( 0.0);
    return  ( z0 * friction_velocity(_v_wind, _v_wind_height, _canopy_height) / air_viscosity_kinematic( _temp));
}



double
schmitt_number(
               double const &_temp,
               double const &_D)
{

    return  ( air_viscosity_kinematic( _temp) / _D);
}



double
air_viscosity_kinematic(
                        double const &_temp)
{

    return  ( 9.08e-8 * _temp + 1.33e-5);
}



double
friction_velocity(
                  double const &_v_wind,
                  double const &_v_wind_height,
                  double const &_canopy_height)
{
    double const ROUGHNESS( 0.13);

    double const roughness( ROUGHNESS * _canopy_height);

    return  (cbm::KARMAN * _v_wind / std::log(_v_wind_height / roughness));
}



double
friction_velocity_ground(
                  double const &_v_wind,
                  double const &_v_wind_height,
                  double const &_canopy_height)
{
    double const ROUGHNESS( 0.13);

    double const roughness( ROUGHNESS * _canopy_height);

    return  (cbm::KARMAN * _v_wind / std::log(_v_wind_height / roughness));
}

} /*namespace ldndc */

