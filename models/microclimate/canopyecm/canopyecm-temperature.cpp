/*!
 * @file
 * @author
 *  David Kraus
 * @date
 *  21 November, 2018
 */

#include  "microclimate/canopyecm/canopyecm.h"
#include  <constants/lconstants-chem.h>
#include  <scientific/meteo/ld_meteo.h>

namespace ldndc {

const double MicroClimateCanopyECM::HLMIN = 0.02;
const double MicroClimateCanopyECM::HBASE = 3.0;
const double MicroClimateCanopyECM::SDAMP = 0.01;


double
MicroClimateCanopyECM::get_sky_view(
                                    double const &_lai)
{
    double fact_sky_view( 0.4);
    return std::exp( -fact_sky_view * _lai);
}


double
MicroClimateCanopyECM::get_lai_fraction(
                                        size_t _fl,
                                        size_t _fl_cnt)
{
    if ( cbm::flt_greater_zero( lai_sum))
    {
        return lai_fl[_fl] / lai_sum;
    }
    else
    {
        return 1.0 / _fl_cnt;
    }
}


double
MicroClimateCanopyECM::foliage_layer_heat_capacity(
                                                   size_t _fl)
{
    double const m_air( ph_.h_fl[_fl] * cbm::DAIR * cbm::DM3_IN_M3);
    
    double hcap_canopy( cbm::CAIR * m_air);
    for ( PlantIterator vt = this->m_veg->begin(); vt != this->m_veg->end(); ++vt)
    {
        if ( cbm::flt_greater_zero( (*vt)->lai()))
        {
            hcap_canopy += (*vt)->aboveground_biomass() * (*vt)->lai_fl[_fl] / (*vt)->lai() * cbm::CAIR;
        }
    }
    return hcap_canopy;
}


double MicroClimateCanopyECM::get_canopy_energy()
{
    double energy( 0.0);
    for ( size_t fl = 0;  fl < se_.canopylayers(); ++fl)
    {
        energy += loc_temp_fl[fl] * foliage_layer_heat_capacity( fl);
    }

    return energy;
}

double MicroClimateCanopyECM::get_surface_bulk_energy()
{
    double energy( 0.0);
    return energy;
}

double MicroClimateCanopyECM::get_soil_energy(
                                              lvector_t< double > &  _temp_sl)
{
    double energy( 0.0);
    for ( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
    {
        energy += _temp_sl[sl] * hcap_sl[sl];
    }
    return energy;
}


double
MicroClimateCanopyECM::get_lw_radiation_down( double const &_f_open_top,
                                              double const &_f_open,
                                              double const &_sky_view_factor,
                                              double const &_lw_in)
{
    //downward longwave radiation is absobed if canopy density increases
    if (cbm::flt_greater( _f_open_top, _f_open))
    {
        return _lw_in * (1.0 - (1.0 - _sky_view_factor) * (_f_open_top - _f_open) / _f_open_top);
    }
    //downward longwave radiation unchanged if canopy density decreases or remains constant
    else
    {
        return _lw_in;
    }
}



/*!
 * @page canopyecm
 * @section temperature Temperature
 *  CanopyECM calculates a temperature buffering effect in the canopy
 *  as leaf area-weighed connection between temperature above the soil
 *  and temperature above the canopy (field temperature)
 */
void MicroClimateCanopyECM::CalcTemperature()
{
    if ( cbm::is_equal(soiltemperature_method.c_str(), "implicite"))
    {
        //update soil layer heights
        for ( size_t  sl = 0;  sl < sl_.soil_layer_cnt()-1;  ++sl)
        {
            h_extended_sl[sl] = sc_.h_sl[sl];
            som_extended_sl[sl] = h_extended_sl[sl] / sc_.h_sl[sl] * sc_.som_sl[sl];
            min_extended_sl[sl] = h_extended_sl[sl] / sc_.h_sl[sl] * sc_.min_sl[sl];
        }
        size_t sl_max( sl_.soil_layer_cnt()-1);
        double const h_extended_sl_max( domainextension - sc_.depth_sl[sl_max]);
        h_extended_sl[sl_max] = ((h_extended_sl_max > sc_.h_sl[sl_max]) ? h_extended_sl_max : sc_.h_sl[sl_max]);
        som_extended_sl[sl_max] = (h_extended_sl[sl_max] / sc_.h_sl[sl_max] * sc_.som_sl[sl_max]);
        min_extended_sl[sl_max] = (h_extended_sl[sl_max] / sc_.h_sl[sl_max] * sc_.min_sl[sl_max]);

        if ( timemode() & TMODE_SUBDAILY)
        {
            if ( energybalance == false)
            {
                CalcSoilTemperature_implicite( mc_.temp_sl, loc_temp, loc_temp_a);
            }
            else
            {
                CalcSoilTemperature_implicite_extended( mc_.temp_sl, loc_temp, loc_temp_a);
            }
        }
        else
        {
            if ( energybalance == false)
            {
                CalcSoilTemperature_implicite( mc_.nd_temp_sl, loc_temp, loc_temp_a);
            }
            else
            {
                CalcSoilTemperature_implicite_extended( mc_.nd_temp_sl, loc_temp, loc_temp_a);
            }
        }
    }
    else
    /*!
     *Temperature above the soil \f$ T_s \f$ depends on atmospheric temperature
     * above canopy \f$ T_{atm} \f$, leaf area index and an empirical species - specific
     * temperature damping parameter (CDAMP):
     * \f{eqnarray*}{
     * bvc &= &\frac{LAI}{LAI + \text{CDAMP}} \\
     * T_s(t + 1) &= &bvc \cdot T_s(t) + (1 - bvc) T_ { atm }
     * \f}
     * 
     */
    {
        double fdamp = 0.0;
        if ( cbm::flt_greater_zero( lai_sum))
        {
            for ( PlantIterator vt = this->m_veg->begin(); vt != this->m_veg->end(); ++vt)
            {
                double const lai_vt_sum( cbm::sum( (*vt)->lai_fl, (*vt)->nb_foliagelayers()));
                fdamp += vt->CDAMP() * lai_vt_sum;
            }
            fdamp *= lclock_ref().day_fraction() / lai_sum;
        }
        
        double  bcv( 0.5);
        if (( lai_sum + fdamp) > 0.0)
        {
            bcv = std::min(lai_sum / (lai_sum + fdamp), 0.7);
        }
        
        loc_temp_a = bcv * loc_temp_a + (1.0 - bcv) * loc_temp;
        
        
        if ( timemode() & TMODE_SUBDAILY)
        {
            CalcSoilTemperature_explicite( mc_.temp_sl, loc_temp_a);
        }
        else
        {
            CalcSoilTemperature_explicite( mc_.nd_temp_sl, loc_temp_a);
        }
    }
    
    double ts_fraction( 1.0 / (lclock()->days_in_year() * lclock()->time_resolution()));
    double ts_fraction_inv( 1.0 - ts_fraction);
    annual_average_temperature = ts_fraction * loc_temp + ts_fraction_inv * annual_average_temperature;
    
    /*!
     * @page canopyecm
     * Temperature within the canopy \f$ T_c \f$ is linearly correlated with
     * canopy height \f$ z \f$ specific leaf area index:
     * \f{eqnarray*}{
     * \phi(z) &=& \frac{\sum_0^z LAI}{\sum LAI} \\
     * T_c(z) &=& T_s + \phi (T_{atm} - T_s)
     * \f}
     */
    size_t const  fl_cnt( m_veg->canopy_layers_used());
    double  laiCum( 0.0);
    for ( size_t  fl = 0;  fl < fl_cnt;  ++fl)
    {
        mc_.tempOld_fl[fl] = loc_temp_fl[fl];
        
        if ( energybalance == false)
        {
            laiCum += lai_fl[fl] * 0.5;

            double const scale( (lai_sum > 0.01) ? laiCum / lai_sum : ((double)((fl<<1)+1)) / (double)(fl_cnt<<1) );
            loc_temp_fl[fl] = loc_temp_a + (loc_temp - loc_temp_a) * scale;
            laiCum += (lai_fl[fl] * 0.5);
        }
    }
    
    laiCum = 0.0;
    for ( size_t  fl = fl_cnt;  fl > 0;  fl--)
    {
        size_t const  l( fl-1);
        
        laiCum += lai_fl[l];
        mc_.tFol_fl[l] = loc_temp_fl[l];
        
        if ( lai_fl[l] > 0.0)
        {
            //rg: totally empirical guess for a rule of thump
            if (( fl == fl_cnt) || ( laiCum <= 1.0))
            {
                mc_.tFol_fl[l] += 0.38 * std::min( 1.0, lai_fl[l]);
            }
            else
            {
                mc_.tFol_fl[l]  = mc_.tFol_fl[l+1] - 0.16 * lai_fl[l+1];
            }
        }
    }
}






/* Set atmospheric Dirichlet boundary condition at ground level */
#define DELTA_X_0 (0.5*h_extended_sl[sl])
#define DELTA_X_1 (cbm::arithmetic_mean2(h_extended_sl[sl-1], h_extended_sl[sl]))
#define DELTA_X_2 (cbm::arithmetic_mean2(h_extended_sl[sl], h_extended_sl[sl+1]))

#define LAMBDA_0 (lambda_sl[sl])
#define LAMBDA_1 (cbm::harmonic_mean2(lambda_sl[sl-1], lambda_sl[sl]))
#define LAMBDA_2 (cbm::harmonic_mean2(lambda_sl[sl], lambda_sl[sl+1]))

#define INCREMENT_SOIL(__sl__,__xl__)                            \
{                                                                \
(__sl__) ++;                                                     \
(__xl__) ++;                                                     \
}


#define ASSEMBLE_INNER_SOIL_MATRIX                                               \
{                                                                                \
double tau_10( LAMBDA_1 / DELTA_X_1 );                                           \
double tau_20( LAMBDA_2 / DELTA_X_2 );                                           \
int const  L = static_cast< int >( sl_.soil_layer_cnt()) - 2;                    \
while( static_cast< int >( sl) < L)                                              \
{                                                                                \
double const tau_11( tau_10 / hcap_sl[sl] );                                     \
double const tau_22( tau_20 / hcap_sl[sl] );                                     \
tdma_solver_.set_row(                                                            \
xl, -tau_11, (1.0 + tau_11 + tau_22), -tau_22, temp_old_sl[sl]);                 \
INCREMENT_SOIL( sl,xl);                                                          \
tau_10 = tau_20;                                                                 \
tau_20 = ( LAMBDA_2 / DELTA_X_2 );                                               \
}                                                                                \
{                                                                                \
double const tau_11( tau_10 / hcap_sl[sl] );                                     \
double const tau_22( tau_20 / hcap_sl[sl] );                                     \
tdma_solver_.set_row(                                                            \
xl, -tau_11, (1.0 + tau_11 + tau_22), -tau_22, temp_old_sl[sl]);                 \
INCREMENT_SOIL( sl,xl);                                                          \
}                                                                                \
}

/*!
 * @brief
 *
 */
lerr_t
MicroClimateCanopyECM::CalcSoilTemperature_implicite(
                                                     lvector_t< double > &  _temp_sl,
                                                     double  _temp_atm,
                                                     double  &_temp_surface)
{
    double const fact_time( cbm::SEC_IN_DAY / lclock()->time_resolution());

    /* densities in  (kg m-3) */
    double const DORG_SCALE( cbm::DORG * cbm::DM3_IN_M3);        // density of soil organic matter
    double const DMIN_SCALE( cbm::DMIN * cbm::DM3_IN_M3);        // density of mineral substance
    double const DWAT_SCALE( cbm::DWAT * cbm::DM3_IN_M3);        // density of water
    double const DICE_SCALE( cbm::DICE * cbm::DM3_IN_M3);        // density of ice
    double const DSNO_SCALE( cbm::DSNO * cbm::DM3_IN_M3);        // density of snow
    double const DAIR_SCALE( cbm::DAIR * cbm::DM3_IN_M3);        // density of air

    for ( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
    {
        temp_old_sl[sl] = _temp_sl[sl];

        /* volumes in m-3 */
        double const vol_liq( this->wc_.wc_sl[sl] * h_extended_sl[sl]);
        double const vol_ice( this->wc_.ice_sl[sl] * h_extended_sl[sl] * cbm::DWAT / cbm::DICE);
        double const vol_org( som_extended_sl[sl] / DORG_SCALE);
        double const vol_min( min_extended_sl[sl] / DMIN_SCALE);
        double const vol_non_air( vol_org + vol_min + vol_liq + vol_ice);
        double const vol_air( std::max(h_extended_sl[sl] - vol_non_air, 0.0));
        double const vol_tot( vol_non_air + vol_air);

        /* volumetric fractions */
        double const frac_org( vol_org / vol_tot);
        double const frac_min( vol_min / vol_tot);
        double const frac_liq( vol_liq / vol_tot);
        double const frac_ice( vol_ice / vol_tot);
        double const frac_air( vol_air / vol_tot);
        
        /* hcap_sl in (J K-1) */
        hcap_sl[sl] = (cbm::CORG * som_extended_sl[sl]
                       + cbm::CMIN * min_extended_sl[sl]
                       + cbm::CWAT * vol_liq * DWAT_SCALE
                       + cbm::CICE * vol_ice * DICE_SCALE
                       + cbm::CAIR * vol_air * DAIR_SCALE);

        /* lambda_sl in (J K-1 m-1 time_step-1) */
        // CB: Why take KHUM and not KORG? I changed it
        lambda_sl[sl] = ((cbm::KMIN * frac_min
                          + cbm::KORG * frac_org
                          + cbm::KWAT * frac_liq
                          + cbm::KAIR * frac_air
                          + cbm::KICE * frac_ice) * fact_time * cbm::CM_IN_M);
    }


    size_t surface_layers(0);
    size_t sl( 0);
    size_t xl( 0);


    /* upper dirichlet boundary conditions */
    tdma_solver_.set_row( xl, 0.0, 1.0, 0.0, _temp_atm);
    xl++;

    {
        double const canopy_height( cbm::bound_min( 2.0, m_veg->canopy_height()));
        double const tau( 0.1 * fact_time / (canopy_height * canopy_height));
        tdma_solver_.set_row( xl, -tau, (1 + tau + tau), -tau, _temp_surface);
        xl++;
    }
    
    if ( cbm::flt_equal_zero( wc_.surface_ice) &&
         cbm::flt_equal_zero( wc_.surface_water))
    {
        // as soon as there are additional layers like snow or floodwater al_new indicates their first occurence
        al_new = true;
        cbm::invalidate( sbl_.temp_sbl);

        /* uppest soil layer (no boundary) */
        {
            double const tau_1( LAMBDA_0 / (DELTA_X_0 * hcap_sl[sl]));
            double const tau_2( LAMBDA_2 / (DELTA_X_2 * hcap_sl[sl]));
            tdma_solver_.set_row( xl, -tau_1, (1.0 + tau_1 + tau_2), -tau_2, temp_old_sl[sl]);
            INCREMENT_SOIL(sl, xl);
        }

        ASSEMBLE_INNER_SOIL_MATRIX;
    }
    else
    {
        surface_layers = sbl_.surfacebulk_layer_cnt();
        size_t sbl( surface_layers-1);

        // in case of first occurence of snow or surface water temperature profile is initialized with _temp_a
        if (al_new)
        {
            al_new = false;
            for (size_t l = 0; l < surface_layers; ++l)
            {
                temp_old_sbl[l] = _temp_surface;
            }
        }
        else
        {
            for (size_t l = 0; l < surface_layers; ++l)
            {
                temp_old_sbl[l] = sbl_.temp_sbl[l];
            }
        }

        double const surface_ice_height( wc_.surface_ice * cbm::DWAT / cbm::DSNO);
        double const surface_bulk_height( wc_.surface_water + surface_ice_height);

        double const delta_x_sbl( surface_bulk_height / surface_layers);

        double const frac_water( wc_.surface_water / surface_bulk_height);
        double const frac_snow( surface_ice_height / surface_bulk_height);

        // CB: Do you really need to multiply with delta_x_sbl here AND below?
        double const hcap_sbl( (frac_water * cbm::CWAT * DWAT_SCALE
                                + frac_snow * cbm::CICE * DSNO_SCALE) * delta_x_sbl);
        // CB: We assume that above ground we have only snow, and below ground only ice
        // further CSNO should be only slightly lower than CICE

        // check why KSNO needs to be reduced for proper snow damping
        double const lambda_sbl( (frac_water * cbm::KWAT
                                  + frac_snow * cbm::KSNO) * fact_time * cbm::CM_IN_M);

        double const tau_sbl( lambda_sbl / (hcap_sbl * delta_x_sbl));
        double const tau_sbl_double( 1.0 + tau_sbl + tau_sbl);


        /* Discretize surface layers */
        while (sbl > 0)
        {
            tdma_solver_.set_row( xl, -tau_sbl, tau_sbl_double, -tau_sbl, temp_old_sbl[sbl]);
            sbl--;
            xl++;
        }


        /* Discretize lowest surface layer and first soil layer */
        {
            double delta_x_sbl_sl( cbm::arithmetic_mean2(delta_x_sbl, h_extended_sl[sl]));
            double lambda_sbl_sl( cbm::harmonic_mean2(lambda_sbl, lambda_sl[sl]));

            double tau_sbl_sl( lambda_sbl_sl / (hcap_sbl * delta_x_sbl_sl));
            tdma_solver_.set_row( xl, -tau_sbl, (1.0 + tau_sbl + tau_sbl_sl), -tau_sbl_sl, temp_old_sbl[sbl]);
            xl++;

            double const tau_2( LAMBDA_2 / (hcap_sl[sl] * DELTA_X_2));
            tdma_solver_.set_row( xl, -tau_sbl_sl, (1.0 + tau_sbl_sl + tau_2), -tau_2, temp_old_sl[sl]);
            INCREMENT_SOIL(sl, xl);
        }

        ASSEMBLE_INNER_SOIL_MATRIX;
    }

    /****************************/
    /* lower boundary condition */
    /****************************/

    {
        if( dirichletboundarycondition)
        {
            tdma_solver_.set_row( xl, 0.0, 1.0, 0.0, annual_average_temperature);
        }
        else
        {
            double const tau_1( LAMBDA_1 / (DELTA_X_1 * hcap_sl[sl]));
            tdma_solver_.set_row( xl, -tau_1, (1 + tau_1), 0.0, temp_old_sl[sl]);
        }
    }


    /********************************************/
    /* solve equation system and write solution */
    /********************************************/

    xl = 1;
    int sbl( surface_layers-1);
    if ( surface_layers > 0)
    {
        tdma_solver_.solve_in_place(NULL, sl_.soil_layer_cnt()+surface_layers+2);

        _temp_surface = tdma_solver_.x(xl);
        xl++;

        while(sbl >= 0)
        {
            sbl_.temp_sbl[sbl] = tdma_solver_.x(xl);
            sbl--;
            xl++;
        }
    }
    else
    {
        tdma_solver_.solve_in_place(NULL, sl_.soil_layer_cnt()+2);
        _temp_surface = tdma_solver_.x(xl);
        xl++;
    }

    for (size_t l = 0; l < sl_.soil_layer_cnt(); ++l)
    {
        _temp_sl[l] = tdma_solver_.x(xl);
        xl++;
    }

    return  LDNDC_ERR_OK;
}



/*!
 * @brief
 *
 */
lerr_t
MicroClimateCanopyECM::CalcSoilTemperature_implicite_extended(
                                                     lvector_t< double > &  _temp_sl,
                                                     double  _temp_atm,
                                                     double  &_temp_surface)
{
    double const sky_view_factor( get_sky_view( lai_sum));
    loc_lw_rad_up_fl = 0.0;
    
    /* densities in  (kg m-3) */
    double const DORG_SCALE( cbm::DORG * cbm::DM3_IN_M3);        // density of soil organic matter
    double const DMIN_SCALE( cbm::DMIN * cbm::DM3_IN_M3);        // density of mineral substance
    double const DWAT_SCALE( cbm::DWAT * cbm::DM3_IN_M3);        // density of water
    double const DICE_SCALE( cbm::DICE * cbm::DM3_IN_M3);        // density of ice
    double const DSNO_SCALE( cbm::DSNO * cbm::DM3_IN_M3);        // density of snow
    double const DAIR_SCALE( cbm::DAIR * cbm::DM3_IN_M3);        // density of air

    size_t radiation_iter( 10);
    size_t const fl_cnt( m_veg->canopy_layers_used());
    double const fact_time( cbm::SEC_IN_DAY / lclock()->time_resolution());
    double const fact_time_rad( fact_time / double(radiation_iter));

    double const accumulated_evapotranspiration_canopy_new =  wc_.accumulated_transpiration_sl.sum() + wc_.accumulated_interceptionevaporation;
    double const accumulated_evapotranspiration_soil_new = wc_.accumulated_soilevaporation + wc_.accumulated_surfacewaterevaporation;
    
    double evapo_canopy( (accumulated_evapotranspiration_canopy_new - accumulated_evapotranspiration_canopy) *
                          cbm::MM_IN_M * cbm::LAMBDA * cbm::J_IN_MJ / (cbm::SEC_IN_DAY / lclock()->time_resolution()));
    double evapo_soil( (accumulated_evapotranspiration_soil_new - accumulated_evapotranspiration_soil) *
                          cbm::MM_IN_M * cbm::LAMBDA * cbm::J_IN_MJ / (cbm::SEC_IN_DAY / lclock()->time_resolution()));
    
    double e_before( get_canopy_energy() + _temp_sl[0] * hcap_sl[0]);
    
    double e_diff_sum( 0.0);

    loc_lw_rad_out = 0.0;
    
    for ( size_t t = 0; t < radiation_iter; ++t)
    {
        /* Downward radiation */

        //downward longwave radiation transmission [W m-2]
        double f_open_top( 1.0);                                //canopy cover of top layer
        double f_open( 1.0 - f_area_fl[fl_cnt-1]);              //canopy cover of current layer
        double f_open_bottom( 1.0 - f_area_fl[fl_cnt-2]);       //canopy cover of bottom layer
        loc_lw_rad_down_fl[fl_cnt-1] = get_lw_radiation_down( f_open_top, f_open, sky_view_factor, loc_rad_lw);

        //up- and downward longwave radiation emission [W m-2]
        double lw_emit( ldndc::meteo::stefan_boltzmann( loc_temp_fl[fl_cnt-1]));        //longwave emission [Wm-2]
        double lw_emit_up( lw_emit * cbm::bound_min( 0.0, f_open_top - f_open));        //longwave emission from canopy to open space (up) [Wm-2]
        double lw_emit_down( lw_emit * cbm::bound_min( 0.0, f_open_bottom - f_open));   //longwave emission from canopy to open space (down) [Wm-2]
        loc_lw_rad_up_fl[fl_cnt-1] = lw_emit_up;
        loc_lw_rad_down_fl[fl_cnt-1] += lw_emit_down;


        for ( int fl = fl_cnt-2;  fl > 0;  fl--)
        {
            //downward longwave radiation transmission [W m-2]
            f_open_top = f_open;                        //canopy cover of top layer
            f_open = f_open_bottom;                     //canopy cover of current layer
            f_open_bottom = 1.0 - f_area_fl[fl-1];      //canopy cover of bottom layer
            loc_lw_rad_down_fl[fl] = get_lw_radiation_down( f_open_top, f_open, sky_view_factor, loc_lw_rad_down_fl[fl+1]);

            //up- and downward longwave radiation emission [W m-2]
            lw_emit = ldndc::meteo::stefan_boltzmann( loc_temp_fl[fl]);             //longwave emission [Wm-2]
            lw_emit_up = lw_emit * cbm::bound_min( 0.0, f_open_top - f_open);       //longwave emission from canopy to open space (up) [Wm-2]
            lw_emit_down = lw_emit * cbm::bound_min( 0.0, f_open_bottom - f_open);  //longwave emission from canopy to open space (down) [Wm-2]
            loc_lw_rad_up_fl[fl] = lw_emit_up;
            loc_lw_rad_down_fl[fl] += lw_emit_down;
        }

        //downward longwave radiation transmission [W m-2]
        f_open_top = f_open;
        f_open = f_open_bottom;
        loc_lw_rad_down_fl[0] = get_lw_radiation_down( f_open_top, f_open, sky_view_factor, loc_lw_rad_down_fl[1]);

        //up- and downward longwave radiation emission [W m-2]
        lw_emit = ldndc::meteo::stefan_boltzmann( loc_temp_fl[0]);
        lw_emit_up = lw_emit * cbm::bound_min( 0.0, f_open_top - f_open);
        lw_emit_down = 0.0;
        loc_lw_rad_up_fl[0] = lw_emit_up;


        /* Upward radiation */

        //upward longwave radiation emission from soil [W m-2]
        double lw_emit_up_soil( ldndc::meteo::stefan_boltzmann( _temp_sl[0]) * f_open);
        loc_lw_rad_up_fl[0] += lw_emit_up_soil;

        for ( size_t fl = 1;  fl < fl_cnt;  fl++)
        {
            f_open_bottom = f_open;
            f_open = (1.0 - f_area_fl[fl]);

            //downward longwave radiation is absobed if canopy density increases
            double const lw_in_below( cbm::flt_greater( f_open_bottom, f_open) ?
                                      (1.0 - (1.0 - sky_view_factor) * (f_open_bottom - f_open) / f_open_bottom) * loc_lw_rad_up_fl[fl-1] :
                                      loc_lw_rad_up_fl[fl-1]);

            loc_lw_rad_up_fl[fl] += lw_in_below;
        }


        /* Energy balance */

        double e_in_top( mc_.shortwaveradiation_in - loc_sw_rad_refl_fl[fl_cnt-1] + mc_.longwaveradiation_in);
        double e_in_bottom( loc_lw_rad_up_fl[fl_cnt-2]);

        double e_out_top( loc_lw_rad_up_fl[fl_cnt-1]);
        double e_out_bottom( loc_sw_rad_fl[fl_cnt-1] + loc_lw_rad_down_fl[fl_cnt-1]);

        double e_diff( e_in_top + e_in_bottom - e_out_top - e_out_bottom - (evapo_canopy * get_lai_fraction( fl_cnt-1, fl_cnt)));
        double delta( e_diff * fact_time_rad / foliage_layer_heat_capacity( fl_cnt-1));
        loc_temp_fl[fl_cnt-1] += delta;
        e_diff_sum += e_diff * fact_time_rad;

        for ( int fl = fl_cnt-2;  fl > 0; --fl)
        {
            e_in_top = loc_sw_rad_fl[fl+1] - loc_sw_rad_refl_fl[fl] + loc_lw_rad_down_fl[fl+1];
            e_in_bottom = loc_lw_rad_up_fl[fl-1];

            e_out_top = loc_lw_rad_up_fl[fl];
            e_out_bottom = loc_sw_rad_fl[fl] + loc_lw_rad_down_fl[fl];

            e_diff = e_in_top + e_in_bottom - e_out_top - e_out_bottom - (evapo_canopy * get_lai_fraction( fl, fl_cnt));
            delta = e_diff * fact_time_rad / foliage_layer_heat_capacity( fl);
            loc_temp_fl[fl] += delta;
            e_diff_sum += e_diff * fact_time_rad;
        }

        e_in_top = loc_sw_rad_fl[1] - loc_sw_rad_refl_fl[0] + loc_lw_rad_down_fl[1];
        e_in_bottom = ldndc::meteo::stefan_boltzmann( _temp_sl[0]) * (1.0 - f_area_fl[0]);

        e_out_top = loc_lw_rad_up_fl[0];
        e_out_bottom = loc_sw_rad_fl[0] + loc_lw_rad_down_fl[0];

        e_diff = e_in_top + e_in_bottom - e_out_top - e_out_bottom - (evapo_canopy * get_lai_fraction( 0, fl_cnt));
        delta = e_diff * fact_time_rad / foliage_layer_heat_capacity( 0);
        loc_temp_fl[0] += delta;
        e_diff_sum += e_diff * fact_time_rad;
        
        double e_diff_soil( loc_rad_a + loc_lw_rad_down_fl[0] - ldndc::meteo::stefan_boltzmann( _temp_sl[0]) * (1.0 - f_area_fl[0]) - evapo_soil);
        if ( hcap_sl[0] > 0.0)
        {
            double delta_soil = e_diff_soil * fact_time_rad / hcap_sl[0];
            _temp_sl[0] += delta_soil;
            e_diff_sum += e_diff_soil * fact_time_rad;
        }
        
        loc_lw_rad_out += loc_lw_rad_up_fl[fl_cnt-1] / radiation_iter;
    }

    double e_after( get_canopy_energy() + _temp_sl[0] * hcap_sl[0]);

    double e_total_in( mc_.shortwaveradiation_in + mc_.longwaveradiation_in);
    double e_total_out( mc_.shortwaveradiation_in * mc_.albedo + loc_lw_rad_out + evapo_canopy + evapo_soil);
    
    if (false)
    {
        LOGINFO("in-out ",(e_total_in - e_total_out) * fact_time,"  after-before ",e_after-e_before," diff ",e_diff_sum);
    }
    
    
    double const surface_ice_height( wc_.surface_ice * cbm::DWAT / cbm::DSNO);
    double const surface_bulk_height( wc_.surface_water + surface_ice_height);
    
    double hcap_sbl( invalid_dbl);
    double lambda_sbl( invalid_dbl);
    double delta_x_sbl( invalid_dbl);
    
    bool have_surface_bulk( cbm::flt_greater( surface_bulk_height, 0.01) ? true : false);
    if ( have_surface_bulk)
    {
        // in case of first occurence of snow or surface water temperature profile is initialized with _temp_a
        if (al_new)
        {
            al_new = false;
            for (size_t l = 0; l < sbl_.surfacebulk_layer_cnt(); ++l)
            {
                temp_old_sbl[l] = _temp_surface;
            }
        }
        else
        {
            for (size_t l = 0; l < sbl_.surfacebulk_layer_cnt(); ++l)
            {
                temp_old_sbl[l] = sbl_.temp_sbl[l];
            }
        }
        
        delta_x_sbl = ( surface_bulk_height / sbl_.surfacebulk_layer_cnt());
        
        double const frac_water( wc_.surface_water / surface_bulk_height);
        double const frac_snow( surface_ice_height / surface_bulk_height);

        hcap_sbl = ( (frac_water * cbm::CWAT * DWAT_SCALE
                                + frac_snow * cbm::CICE * DSNO_SCALE) * delta_x_sbl);

        // check why KSNO needs to be reduced for proper snow damping
        lambda_sbl = ( (frac_water * cbm::KWAT
                                  + frac_snow * cbm::KSNO) * fact_time * cbm::CM_IN_M);
    }
    else
    {
        // as soon as there are additional layers like snow or floodwater al_new indicates their first occurence
        al_new = true;
        cbm::invalidate( sbl_.temp_sbl);
    }



    for ( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
    {
        temp_old_sl[sl] = _temp_sl[sl];

        /* volumes in m-3 */
        double const vol_liq( this->wc_.wc_sl[sl] * h_extended_sl[sl]);
        double const vol_ice( this->wc_.ice_sl[sl] * h_extended_sl[sl] * cbm::DWAT / cbm::DICE);
        double const vol_org( som_extended_sl[sl] / DORG_SCALE);
        double const vol_min( min_extended_sl[sl] / DMIN_SCALE);
        double const vol_non_air( vol_org + vol_min + vol_liq + vol_ice);
        double const vol_air( std::max(h_extended_sl[sl] - vol_non_air, 0.0));
        double const vol_tot( vol_non_air + vol_air);

        /* volumetric fractions */
        double const frac_org( vol_org / vol_tot);
        double const frac_min( vol_min / vol_tot);
        double const frac_liq( vol_liq / vol_tot);
        double const frac_ice( vol_ice / vol_tot);
        double const frac_air( vol_air / vol_tot);
        
        /* hcap_sl in (J K-1) */
        hcap_sl[sl] = (cbm::CORG * som_extended_sl[sl]
                       + cbm::CMIN * min_extended_sl[sl]
                       + cbm::CWAT * vol_liq * DWAT_SCALE
                       + cbm::CICE * vol_ice * DICE_SCALE
                       + cbm::CAIR * vol_air * DAIR_SCALE);
        // CB: I added air here, or was there a reason to not take it into account?

        /* lambda_sl in (J K-1 m-1 time_step-1) */
        lambda_sl[sl] = ((cbm::KMIN * frac_min
                          + cbm::KHUM * frac_org
                          + cbm::KWAT * frac_liq
                          + cbm::KAIR * frac_air
                          + cbm::KICE * frac_ice) * fact_time * cbm::CM_IN_M);
    }

    size_t sl( 0);
    size_t xl( 0);

    /* upper dirichlet boundary conditions */
    tdma_solver_.set_row( xl, 0.0, 1.0, 0.0, _temp_atm);
    xl++;

    double const k_air_scale( 5000.0);
    double lambda_canopy( k_air_scale * cbm::KAIR * fact_time * cbm::CM_IN_M);

    //canopy
    if ( fl_cnt > 2)
    {
        double hcap_canopy( foliage_layer_heat_capacity( fl_cnt-1));
        double delta_x1( 0.5 * ph_.h_fl[fl_cnt-1]);
        double delta_x2( cbm::arithmetic_mean2( ph_.h_fl[fl_cnt-1], ph_.h_fl[fl_cnt-2]));
        double tau_1( lambda_canopy / (hcap_canopy * delta_x1));
        double tau_2( lambda_canopy / (hcap_canopy * delta_x2));
        
        tdma_solver_.set_row( xl, -tau_1, (1 + tau_1 + tau_2), -tau_2, loc_temp_fl[fl_cnt-1]);
        xl++;
        
        for ( size_t fl = fl_cnt-2; fl>0; --fl)
        {
            hcap_canopy = foliage_layer_heat_capacity( fl);
            delta_x1 = cbm::arithmetic_mean2( ph_.h_fl[fl], ph_.h_fl[fl+1]);
            delta_x2 = cbm::arithmetic_mean2( ph_.h_fl[fl], ph_.h_fl[fl-1]);
            tau_1 = ( lambda_canopy / (hcap_canopy * delta_x1));
            tau_2 = ( lambda_canopy / (hcap_canopy * delta_x2));
            
            tdma_solver_.set_row( xl, -tau_1, (1 + tau_1 + tau_2), -tau_2, loc_temp_fl[fl]);
            xl++;
        }
        
        hcap_canopy = foliage_layer_heat_capacity( 0);
        delta_x1 = cbm::arithmetic_mean2( ph_.h_fl[1], ph_.h_fl[0]);
        if ( have_surface_bulk)
        {
            delta_x2 = cbm::arithmetic_mean2( ph_.h_fl[0], delta_x_sbl);
        }
        else
        {
            delta_x2 = cbm::arithmetic_mean2( ph_.h_fl[0], sc_.h_sl[0]);
        }
        tau_1 = ( lambda_canopy / (hcap_canopy * delta_x1));
        tau_2 = ( lambda_canopy / (hcap_canopy * delta_x2));
            
        tdma_solver_.set_row( xl, -tau_1, (1 + tau_1 + tau_2), -tau_2, loc_temp_fl[0]);
        xl++;
    }
    
    if ( have_surface_bulk)
    {
        double const tau_sbl( lambda_sbl / (hcap_sbl * delta_x_sbl));
        double const tau_sbl_double( 1.0 + tau_sbl + tau_sbl);
        
        if ( fl_cnt > 2)
        {
            double const tau_sbl_canopy( lambda_canopy / (hcap_sbl * cbm::arithmetic_mean2( ph_.h_fl[0], delta_x_sbl)));
            tdma_solver_.set_row( xl, -tau_sbl_canopy, 1+tau_sbl_canopy+tau_sbl, -tau_sbl, temp_old_sbl[sbl_.surfacebulk_layer_cnt()-1]);
            xl++;
        }
        else
        {
            tdma_solver_.set_row( xl, -tau_sbl, tau_sbl_double, -tau_sbl, temp_old_sbl[sbl_.surfacebulk_layer_cnt()-1]);
            xl++;
        }
        
        for ( int sbl = sbl_.surfacebulk_layer_cnt()-2; sbl > 0; --sbl)
        {
            tdma_solver_.set_row( xl, -tau_sbl, tau_sbl_double, -tau_sbl, temp_old_sbl[sbl]);
            xl++;
        }
        
        double const tau_sbl_soil( cbm::harmonic_mean2( lambda_sbl, lambda_sl[0]) / (hcap_sbl * cbm::arithmetic_mean2( delta_x_sbl, sc_.h_sl[0])));
        tdma_solver_.set_row( xl, -tau_sbl, 1+tau_sbl+tau_sbl_soil, -tau_sbl_soil, temp_old_sbl[0]);
        xl++;
    }
    
    
    double tau_1( LAMBDA_0 / (DELTA_X_0 * hcap_sl[0]));
    if ( have_surface_bulk)
    {
        double delta_x2 = cbm::arithmetic_mean2( delta_x_sbl, sc_.h_sl[0]);
        tau_1 = ( cbm::harmonic_mean2( lambda_sbl, lambda_sl[0]) / (delta_x2 * hcap_sl[sl]));
    }
    else if ( fl_cnt > 2)
    {
        double delta_x2 = cbm::arithmetic_mean2( ph_.h_fl[0], sc_.h_sl[0]);
        tau_1 = ( lambda_canopy / (delta_x2 * hcap_sl[sl]));
    }
    double tau_2( LAMBDA_2 / (DELTA_X_2 * hcap_sl[sl]));
    tdma_solver_.set_row( xl, -tau_1, (1 + tau_1 + tau_2), -tau_2, temp_old_sl[sl]);
    
    INCREMENT_SOIL(sl, xl);

    ASSEMBLE_INNER_SOIL_MATRIX;
    
    
    /****************************/
    /* lower boundary condition */
    /****************************/
    
    {
        if( dirichletboundarycondition)
        {
            tdma_solver_.set_row( xl, 0.0, 1.0, 0.0, annual_average_temperature);
        }
        else
        {
            tau_1 = LAMBDA_1 / (DELTA_X_1 * hcap_sl[sl]);
            tdma_solver_.set_row( xl, -tau_1, (1 + tau_1), 0.0, temp_old_sl[sl]);
        }
    }


    /********************************************/
    /* solve equation system and write solution */
    /********************************************/

    xl = 1;
    
    //soil only
    if ( have_surface_bulk == false)
    {
        if (fl_cnt <= 2)
        {
            tdma_solver_.solve_in_place(NULL, 1+sl_.soil_layer_cnt());
            
            //surface temperature = temperature of first soil layer
            _temp_surface = tdma_solver_.x(xl);
            
            for (size_t l = 0; l < sl_.soil_layer_cnt(); ++l)
            {
                _temp_sl[l] = tdma_solver_.x(xl);
                xl++;
            }
        }
        else
        {
            tdma_solver_.solve_in_place(NULL, 1+fl_cnt+sl_.soil_layer_cnt());
            
            for ( int fl = fl_cnt-1; fl>=0; --fl)
            {
                loc_temp_fl[fl] = tdma_solver_.x( xl);
                xl++;
            }
            
            //surface temperature = temperature of first soil layer
            _temp_surface = tdma_solver_.x( xl);
            
            for (size_t l = 0; l < sl_.soil_layer_cnt(); ++l)
            {
                _temp_sl[l] = tdma_solver_.x( xl);
                xl++;
            }
        }
    }
    else
    {
        if (fl_cnt <= 2)
        {
            tdma_solver_.solve_in_place(NULL, 1+sbl_.surfacebulk_layer_cnt()+sl_.soil_layer_cnt());
            
            //surface temperature = temperature of first surfacebulk layer
            _temp_surface = tdma_solver_.x(xl);
            
            for ( int sbl = sbl_.surfacebulk_layer_cnt()-1; sbl >= 0; --sbl)
            {
                sbl_.temp_sbl[sbl] = tdma_solver_.x(xl);
                xl++;
            }
            
            for (size_t l = 0; l < sl_.soil_layer_cnt(); ++l)
            {
                _temp_sl[l] = tdma_solver_.x(xl);
                xl++;
            }
        }
        else
        {
            tdma_solver_.solve_in_place(NULL, 1+fl_cnt+sbl_.surfacebulk_layer_cnt()+sl_.soil_layer_cnt());
            
            for ( int fl = fl_cnt-1; fl>=0; --fl)
            {
                loc_temp_fl[fl] = tdma_solver_.x( xl);
                xl++;
            }
            
            //surface temperature = temperature of first surfacebulk layer
            _temp_surface = tdma_solver_.x( xl);

            for ( int sbl = sbl_.surfacebulk_layer_cnt()-1; sbl >= 0; --sbl)
            {
                sbl_.temp_sbl[sbl] = tdma_solver_.x(xl);
                xl++;
            }
            
            for (size_t l = 0; l < sl_.soil_layer_cnt(); ++l)
            {
                _temp_sl[l] = tdma_solver_.x( xl);
                xl++;
            }
        }
    }
    
    for ( size_t fl = se_.canopylayers()-1; fl > fl_cnt-1; --fl)
    {
        loc_temp_fl[fl] = loc_temp_fl[fl_cnt-1];
    }
    
    loc_sensible_heat_out_top = k_air_scale * cbm::KAIR * cbm::CM_IN_M * (loc_temp_fl[fl_cnt-1] - loc_temp) / 0.5 * ph_.h_fl[fl_cnt-1];
    loc_sensible_heat_out_bottom = 0.0;
    
    mc_.heat_content = get_canopy_energy() + get_surface_bulk_energy() + get_soil_energy( _temp_sl);
 
//    LOGINFO(k_air_scale * cbm::KAIR * fact_time * cbm::CM_IN_M * (loc_temp_fl[fl_cnt-1] - loc_temp) / 0.5 * ph_.h_fl[fl_cnt-1],"  ",
//            loc_temp_fl[fl_cnt-1] ,"  ", loc_temp);
//    LOGINFO("fact_time ",fact_time);
    return  LDNDC_ERR_OK;
}



/*!
 * @brief
 *
 */
lerr_t
MicroClimateCanopyECM::CalcSoilTemperature_explicite(
                                           lvector_t< double > &  _temp_sl, double  _temp_a)
{
    for ( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
    {
        tempOld_sl[sl] = _temp_sl[sl];
    }

    // surface temp without snow
    double temp_l( _temp_a);

    if ( cbm::flt_greater_zero( wc_.surface_ice))
    {
        /* Snow pack [m] */
        double  sp_m( wc_.surface_ice / cbm::DSNO);
        double  bcv( sp_m / (sp_m + SDAMP * this->lclock_ref().day_fraction()) );
        // rg: degree of temperature damping due to snow (similar as canopy damping)

        /*! Heat conductivity factor bcv affects relative contributions of heat efflux from the upper soil and influx through
         * the canopy according to:
         * \f[temp_l=bcv*nd\_temp\_sl[0]+temp*(1-bcv)\f] Where <PRE>
         * temp_l           surface temperature        (oC)
         * _temp_sl          Temperature per soil layer        (oC)
         * temp                 Above-canopy temperature          (oC) </PRE>
         */

        temp_l = bcv * _temp_sl[0] + (1.0 - bcv) * _temp_a;    // surface temp below snow
    }

    // soil layers
    /* Starting depth of the soil layer */
    double  dsl0( 0.0);
    /* cumulated soil depth */
    double  hCum( 0.0);
    /* Minimum time step required for heat balance solution of the profile [s] */
    double dt( cbm::SEC_IN_DAY);
    /* Minimum time step required for heat balance solution of a soil layer [s] */
    double dt_sl( 0.0);

    for ( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
    {
        double  hl( std::max( HLMIN, sc_.h_sl[sl]));

        /*! For each soil layer, soil volume without air space is calculated as the sum of its fractions of organic
         * material, minerals, water and ice:
         *  \f[vol= \frac{som\_sl}{DORG}+ \frac{min\_sl}{DMIN}+ \frac{wc\_sl*hl}{DWAT}+ \frac{ice\_sl*hl}{DICE} \f] Where <PRE>
         *    vol             Volume of a soil column                       (m3)
         *    som_sl          Soil organic matter concentration             (kgDM m-2)
         *    min_sl          Mineral concentration in the soil             (kgDM m-2)
         *    wc_sl           Amount of water in soil layers                (mm m-3)
         *    h1              Height of a soil layer                        (m)
         *    ice_sl          Amount of ice in the soil                     (mm m-3)
         */
        double  vol(( sc_.som_sl[sl] / cbm::DORG + sc_.min_sl[sl] / cbm::DMIN + wc_.wc_sl[sl] * cbm::MM_IN_M * hl / cbm::DWAT + wc_.ice_sl[sl] * cbm::MM_IN_M * hl / cbm::DICE) * (1.0 / cbm::DM3_IN_M3));

        /*! Volume fractions for the different components (forg, fmin, fwat and fice) are calculated: */

        /*! forg             Volume fraction of soil organic matter        (-)
         \f[forg=\frac{som\_sl[sl]}{CORG/vol}\f] Where <PRE>*/
        double  forg = (sc_.som_sl[sl] / (cbm::DORG * cbm::DM3_IN_M3)) / vol;

        /*! fmin             Volume fraction of mineral soil               (-)
         \f[fmin=\frac{min\_sl[sl]}{CMIN/vol}\f] Where <PRE>*/
        double  fmin = (sc_.min_sl[sl] / (cbm::DMIN * cbm::DM3_IN_M3)) / vol;

        /*! fwat             Volume fraction of soil water                 (-)
         \f[fwat=\frac{wat\_sl[sl]}{CWAT/vol}\f] Where <PRE>*/
        double  fwat = (wc_.wc_sl[sl] * cbm::MM_IN_M * hl / (cbm::DWAT * cbm::DM3_IN_M3)) / vol;

        /*! fice             Volume fraction of soil ice                   (-)
         \f[fice=\frac{ice\_sl[sl]}{CICE/vol}\f] Where <PRE>*/
        double  fice = (wc_.ice_sl[sl] * cbm::MM_IN_M * hl / (cbm::DICE * cbm::DM3_IN_M3)) / vol;

        /*! Overall heat conductivity lambda is implemented as a weighted average of the different soil fractions and their conductivity:
         \f[lambda[sl] = KORG * forg + KMIN * fmin + KWAT * fwat + KICE * fice\f]*/
        lambda[sl] = cbm::KORG * forg + cbm::KMIN * fmin + cbm::KWAT * fwat + cbm::KICE * fice;

        /*! Overall heat conductivity for the different soil layers is implemented as a summarisation of the different volume
         fractions with their respecive conductivity values according to Richter(1986).
         \f[hcap[sl] = CORG*som\_sl+CMIN*min\_sl+CWAT*wc\_sl*hl+CICE*ice\_sl*h1\f]*/
        hcap[sl] = cbm::CORG * sc_.som_sl[sl] + cbm::CMIN * sc_.min_sl[sl] + cbm::CWAT * wc_.wc_sl[sl] * cbm::MM_IN_M * hl + cbm::CICE * wc_.ice_sl[sl] * cbm::MM_IN_M * hl;

        // soil layer depth
        dsl[sl] = dsl0 + hl;
        dsl0    = dsl[sl];
        hCum   += sc_.h_sl[sl];

        // time step required (Newman criterion of the SOHE model of B.Huwe)
        if ( sl == 0)
        {
            dt_sl = hcap[sl] * cbm::sqr(dsl[sl] - 0.0 /* was dsl[sl-1]*/) / (2.0 * lambda[sl]);
        }
        else
        {
            dt_sl = hcap[sl] * cbm::sqr(dsl[sl] - dsl[sl-1]) / (2.0 * lambda[sl]);
        }
        if (dt_sl < dt)
        {
            dt = floor( std::max( 1.0, dt_sl));
        }
    }

    // Number of necessary time step fractions per time step
    ldndc_assert( dt > 0.0);
    size_t  ntStep( cbm::bound_min( (size_t)1, (size_t)( (double)cbm::SEC_IN_DAY / dt)));
    /* Fraction of the day that is simulated [-] */
    double  fd( 1.0 / double(ntStep));
    // reference depth
    double  dref( std::max( HBASE, hCum + sc_.h_sl[sl_.soil_layer_cnt()-1]));

    size_t  L( sl_.soil_layer_cnt() - 1);
    double  hden_1( 0.0);
    // temperature change
    for ( size_t  n = 0;  n < ntStep;  n++)
    {
        //time loop

        /* Surface temperature with depth equal to the upper layer is assumed to be upper boundary condition for heat flux density */
        double  hden_0( lambda[0] * ( temp_l - _temp_sl[0]) / dsl[0]);
        for ( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
        {
            /* Heat flux density for all soil layers except the last one is calculated as:
             \f[hden = \int_{t=0}^{ntStep} \int_{sl=1}^{sl_.soil_layer_cnt()} hden[sl] = 0.5 * (lambda[sl] + lambda[sl+1]) * (nd\_temp\_sl[sl] - nd\_temp\_sl[sl+1]) / (dsl[sl+1] - dsl[sl])\f] */
            if (sl == L)
            {
                /* Annual average temperature is considered to be lower boundary condition for heat flux */
                hden_1 = lambda[L] * ( _temp_sl[L] - annual_average_temperature) / ( dref - dsl[L]);
            }
            else
            {
                hden_1 = 0.5 * (lambda[sl] + lambda[sl+1]) * (_temp_sl[sl] - _temp_sl[sl+1]) / (dsl[sl+1] - dsl[sl]);
            }

            // actual temperature calculation using available time steps, heat density and heat capacity
            _temp_sl[sl] += ( cbm::SEC_IN_DAY * fd * ( hden_0 - hden_1) / ( std::max( HLMIN, sc_.h_sl[sl]) * hcap[sl]));
            hden_0 = hden_1;


            // rg 08.12.10: exception introduced that accounts for possible oscillations in the system
            if ( _temp_sl[sl] > 60.0)
            {
                KLOGERROR( "Exponentially increasing soil temperature at time step ",this->lclock_ref().to_string(),". Probably due to too large differences in layer properties.");
                return  LDNDC_ERR_FAIL;
            }
        }
    }

    if ( this->lclock_ref().time_resolution() > 1)
    {
        for ( size_t  sl = 0;  sl < sl_.soil_layer_cnt();  ++sl)
        {
            _temp_sl[sl] = tempOld_sl[sl] + ( _temp_sl[sl] - tempOld_sl[sl]) * this->lclock_ref().day_fraction();
        }
    }

    return  LDNDC_ERR_OK;
}








} /*namespace ldndc*/

