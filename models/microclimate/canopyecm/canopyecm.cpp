/*!
 * @file
 * @author
 *  - Ruediger Grote
 *  - David Kraus
 *
 */

/*!
 * @page canopyecm
 * @tableofcontents
 * @section canopyecm_guide User guide
 *  The microclimate model CanopyECM calculates:
 *   - Radiation
 *   - Temperature
 *   - Wind velocity
 *   - vapor pressure deficit (air humidity)
 *   - Albedo
 *
 * @subsection canopyecm_guide_structure Model structure
 *  CanopyECM requires further models for:
 *  - plant growth (e.g., distribution of leaf area)
 *  - watercycle (e.g., soil water content)
 */

#include  "microclimate/canopyecm/canopyecm.h"

#include  <input/setup/setup.h>
#include  <input/species/species.h>

#include  <constants/cbm_const.h>
#include  <utils/cbm_utils.h>
#include  <scientific/meteo/ld_meteo.h>

REGISTER_OPTION(MicroClimateCanopyECM, soiltemperature,"  [char]");
REGISTER_OPTION(MicroClimateCanopyECM, dirichletboundarycondition,"  [bool]");
REGISTER_OPTION(MicroClimateCanopyECM, soilprofileextension,"  [float]");
REGISTER_OPTION(MicroClimateCanopyECM, energybalance,"  [bool]");

LMOD_MODULE_INFO(MicroClimateCanopyECM,TMODE_SUBDAILY|TMODE_POST_DAILY,LMOD_FLAG_USER);
namespace ldndc {

MicroClimateCanopyECM::MicroClimateCanopyECM(
                                             MoBiLE_State *  _state,
                                             cbm::io_kcomm_t *  _io,
                                             timemode_e  _timemode)
                                            : MBE_LegacyModel(
                                              _state,
                                              _timemode),

                            se_( _io->get_input_class_ref< input_class_setup_t >()),
                            wc_( _state->get_substate_ref< substate_watercycle_t >()),
                            mc_( _state->get_substate_ref< substate_microclimate_t >()),
                            ph_( _state->get_substate_ref< substate_physiology_t >()),
                            m_veg( &_state->vegetation),
                            climate_( _io->get_input_class_ref< input_class_climate_t >()),
                            sl_( _io->get_input_class_ref< input_class_soillayers_t >()),
                            sc_( _state->get_substate_ref< substate_soilchemistry_t >()),
                            sbl_( _state->get_substate_ref< substate_surfacebulk_t >()),
                            tdma_solver_( solver_tridiagonal_t( 1+se_.canopylayers()+sbl_.surfacebulk_layer_cnt()+sl_.soil_layer_cnt())),
                            loc_sw_rad_refl_fl( se_.canopylayers(), 0.0),
                            loc_sw_rad_refl_a( 0.0),
                            f_area_fl( se_.canopylayers(), 0.0),
                            lai_fl( se_.canopylayers(), 0.0)
{
    accumulated_evapotranspiration_canopy = 0.0;
    accumulated_evapotranspiration_soil = 0.0;
    loc_sw_rad_out = 0.0;
    loc_lw_rad_out = 0.0;
    loc_sensible_heat_out_top = 0.0;
    loc_sensible_heat_out_bottom = 0.0;
    loc_rad_a = 0.0;
    loc_temp_a = 0.0;
    loc_rad_sw = 0.0;
    loc_rad_lw = 0.0;
    loc_temp = 0.0;
    loc_win = 0.0;
    loc_vpd = 0.0;
    loc_fdif = 0.0;
    loc_sinb = 0.0;
    fdrag = 0.0;

    dirichletboundarycondition = false;
    domainextension = 0.0;
    energybalance = false;
    al_new = true;

    hcap_sl = new double[sl_.soil_layer_cnt()];
    lambda_sl = new double[sl_.soil_layer_cnt()];

    temp_old_sl = new double[sl_.soil_layer_cnt()];
    temp_old_sbl = new double[sl_.soil_layer_cnt() + sbl_.surfacebulk_layer_cnt()];

    h_extended_sl = new double[sl_.soil_layer_cnt()];
    som_extended_sl = new double[sl_.soil_layer_cnt()];
    min_extended_sl = new double[sl_.soil_layer_cnt()];

    dsl = new double[sl_.soil_layer_cnt()];
    hcap = new double[sl_.soil_layer_cnt()];
    lambda = new double[sl_.soil_layer_cnt()];
    tempOld_sl = new double[sl_.soil_layer_cnt()];
    
    annual_average_temperature = climate_.annual_temperature_average();
}

MicroClimateCanopyECM::~MicroClimateCanopyECM()
{
    delete[] hcap_sl;
    delete[] lambda_sl;
    
    delete[] temp_old_sl;
    delete[] temp_old_sbl;
    
    delete[] h_extended_sl;
    delete[] som_extended_sl;
    delete[] min_extended_sl;

    delete[] dsl;
    delete[] hcap;
    delete[] lambda;
    delete[] tempOld_sl;
}

lerr_t
MicroClimateCanopyECM::initialize()
{
    return  CanopyECM_step_init();
}


/*!
 * @brief
 *
 */
lerr_t
MicroClimateCanopyECM::configure(
                                 ldndc::config_file_t const *  )
{
    /*!
     * @page canopyecm
     * @section canopyecmoptions Model options
     * Available options:
     * Default options are marked with bold letters.
     *  - Soiltemperature (soiltemperature: \b implicite / explicite)
     */
    soiltemperature_method = get_option< char const * >( "soiltemperature", "implicite");

    /*!
     * @page canopyecm
     *  - Dirichlet boundary condition (annual average temperature) of bottom soil layer
     *    for soil temperature calculation (dirichletboundarycondition: \b false / true)
     */
    dirichletboundarycondition = get_option< bool >( "dirichletboundarycondition", false);

    /*!
     * @page canopyecm
     *  - Depth of lower boundary for soil temperature calculation
     *    (soilprofileextension: \b 0.0 / ...)
     */
    double const depth = get_option< float >( "soilprofileextension", 5.0);
    domainextension = cbm::bound_min( 0.0, depth - sc_.depth_sl[sl_.soil_layer_cnt()-1]);

    /*!
     * @page canopyecm
     *  - Consider energy balance
     *    (energybalance: \b false / true)
     */
    energybalance = get_option< bool >( "energybalance", false);
    
    return  LDNDC_ERR_OK;
}


lerr_t
MicroClimateCanopyECM::CanopyECM_step_init()
{
    if ( timemode() & TMODE_SUBDAILY)
    {
        loc_lw_rad_down_fl.reference( mc_.longwaveradiation_down_fl);
        loc_lw_rad_up_fl.reference( mc_.longwaveradiation_up_fl);
        loc_sw_rad_fl.reference( mc_.shortwaveradiation_fl);
        loc_temp_fl.reference( mc_.temp_fl);
        loc_fsun_fl.reference( mc_.ts_sunlitfoliagefraction_fl);
        loc_parshd_fl.reference( mc_.parshd_fl);
        loc_parsun_fl.reference( mc_.parsun_fl);
        loc_vpd_fl.reference( mc_.vpd_fl);
        loc_win_fl.reference( mc_.win_fl);

        loc_temp_a = mc_.temp_a;
        loc_rad_sw = mc_.shortwaveradiation_in;
        loc_rad_lw = mc_.longwaveradiation_in;
        loc_temp   = mc_.ts_airtemperature;
        loc_win    = mc_.ts_windspeed;
        loc_vpd    = mc_.ts_watervaporsaturationdeficit;
        loc_sinb   = ldndc::meteo::solar_elevation_sinus( se_.latitude(), lclock_ref().yearday(), lclock_ref().subday(), lclock_ref().time_resolution());
        loc_fdif   = ldndc::meteo::subdaily_solar_radiation_fraction( loc_sinb);
    }
    else if ( timemode() & (TMODE_PRE_DAILY|TMODE_POST_DAILY))
    {
        loc_lw_rad_down_fl.reference( mc_.nd_longwaveradiation_down_fl);
        loc_lw_rad_up_fl.reference( mc_.nd_longwaveradiation_up_fl);
        loc_sw_rad_fl.reference( mc_.nd_shortwaveradiation_fl);
        loc_temp_fl.reference( mc_.nd_temp_fl);
        loc_fsun_fl.reference( mc_.nd_sunlitfoliagefraction_fl);
        loc_parshd_fl.reference( mc_.nd_parshd_fl);
        loc_parsun_fl.reference( mc_.nd_parsun_fl);
        loc_vpd_fl.reference( mc_.nd_vpd_fl);
        loc_win_fl.reference( mc_.nd_win_fl);

        loc_temp_a = mc_.nd_temp_a;
        loc_rad_sw = mc_.nd_shortwaveradiation_in;
        loc_rad_lw = mc_.nd_longwaveradiation_in;
        loc_temp   = mc_.nd_airtemperature;
        loc_win    = mc_.nd_windspeed;
        loc_vpd    = mc_.nd_watervaporsaturationdeficit;
        loc_sinb   = ldndc::meteo::solar_elevation_sinus( se_.latitude(), lclock_ref().yearday());
        double  rad_max( ldndc::meteo::daily_solar_radiation( se_.latitude(), lclock_ref().yearday(), lclock_ref().days_in_year()));
        loc_fdif   = ldndc::meteo::daily_solar_radiation_fraction( mc_.nd_shortwaveradiation_in, rad_max);
    }
    else
    {
        KLOGERROR( "unsupported time mode .. huh?!");
        return  LDNDC_ERR_FAIL;
    }

    lai_fl = 0.0;
    f_area_fl = 0.0;
    for ( size_t fl = 0;  fl < m_veg->canopy_layers_used();  ++fl)
    {
        for ( PlantIterator vt = this->m_veg->begin(); vt != this->m_veg->end(); ++vt)
        {
            lai_fl[fl] += (*vt)->lai_fl[fl];

            double const lai_max_vt( cbm::max( (*vt)->lai_fl, (*vt)->nb_foliagelayers()));
            double const f_area_vtfl( cbm::flt_greater_zero( lai_max_vt) ?
                                                    (*vt)->f_area * (*vt)->lai_fl[fl] / lai_max_vt :
                                                    0.0);

            f_area_fl[fl] += f_area_vtfl;
        }
        f_area_fl[fl] = cbm::bound_min( 1.0, f_area_fl[fl]);
    }

    lai_sum = 0.0;
    for ( PlantIterator vt = this->m_veg->begin(); vt != this->m_veg->end(); ++vt)
    {
        lai_sum += (*vt)->lai();
    }

    fdrag = 0.0;
    if ( cbm::flt_greater_zero( lai_sum))
    {
        for ( PlantIterator vt = this->m_veg->begin(); vt != this->m_veg->end(); ++vt)
        {
            fdrag += vt->DRAGC() * (*vt)->lai();
        }
        fdrag *= 1.0 / lai_sum;
    }
    else if ( this->m_veg->size() > 0)
    {
        for ( PlantIterator vt = this->m_veg->begin(); vt != this->m_veg->end(); ++vt)
        {
            fdrag += vt->DRAGC();
        }
        fdrag *= 1.0 / double( this->m_veg->size());
    }

    loc_sw_rad_refl_fl = 0.0;
    loc_sw_rad_refl_a = 0.0;

    return  LDNDC_ERR_OK;
}


lerr_t
MicroClimateCanopyECM::solve()
{
    lerr_t rc = CanopyECM_step_init();
    RETURN_IF_NOT_OK( rc);

    CalcShortwaveRadiation();

    CalcTemperature();

    CalcAlbedo();

    CalcHumidity();

    CalcWindspeed();

    rc = CanopyECM_step_out();
    RETURN_IF_NOT_OK( rc);

    return  LDNDC_ERR_OK;
}


lerr_t
MicroClimateCanopyECM::CanopyECM_step_out()
{
    size_t const  fl_cnt( m_veg->canopy_layers_used());

    //transfer from local variables according to time step
    if ( timemode() & TMODE_SUBDAILY)
    {
        /* sub-daily output */
        mc_.shortwaveradiation_out = loc_sw_rad_out;
        mc_.longwaveradiation_out = loc_lw_rad_out;
        mc_.sensible_heat_out = loc_sensible_heat_out_top;
        mc_.rad_a  = loc_rad_a;
        mc_.temp_a = loc_temp_a;

        double f1( lclock_ref().day_fraction());
        double f2( 0.1 * lclock_ref().day_fraction());

        for ( size_t fl = 0; fl < fl_cnt; ++fl)
        {
            mc_.sunlitfoliagefraction24_fl[fl] = mc_.sunlitfoliagefraction24_fl[fl] * (1.0-f1) + mc_.ts_sunlitfoliagefraction_fl[fl] * f1;
            mc_.sunlitfoliagefraction240_fl[fl] = mc_.sunlitfoliagefraction240_fl[fl] * (1.0-f2) + mc_.ts_sunlitfoliagefraction_fl[fl] * f2;

            mc_.parsun24_fl[fl] = mc_.parsun24_fl[fl] * (1.0-f1) + mc_.parsun_fl[fl] * f1;
            mc_.parsun240_fl[fl] = mc_.parsun240_fl[fl] * (1.0-f2) + mc_.parsun_fl[fl] * f2;
            
            mc_.parshd24_fl[fl] = mc_.parshd24_fl[fl] * (1.0-f1) + mc_.parshd_fl[fl] * f1;
            mc_.parshd240_fl[fl] = mc_.parshd240_fl[fl] * (1.0-f2) + mc_.parshd_fl[fl] * f2;
            
            mc_.rad24_fl[fl] = mc_.rad24_fl[fl] * (1.0-f1) + mc_.shortwaveradiation_fl[fl] * f1;
            mc_.rad240_fl[fl] = mc_.rad240_fl[fl] * (1.0-f2) + mc_.shortwaveradiation_fl[fl] * f2;

            mc_.tFol24_fl[fl] = mc_.tFol24_fl[fl] * (1.0-f1) + mc_.tFol_fl[fl] * f1;
            mc_.tFol240_fl[fl] = mc_.tFol240_fl[fl] * (1.0-f2) + mc_.tFol_fl[fl] * f2;
        }
    }
    else if ( timemode() & (TMODE_PRE_DAILY|TMODE_POST_DAILY))
    {
        /* daily output */
        mc_.nd_shortwaveradiation_out = loc_sw_rad_out;
        mc_.nd_longwaveradiation_out = loc_lw_rad_out;
        mc_.nd_sensible_heat_out = loc_sensible_heat_out_top;
        mc_.nd_rad_a  = loc_rad_a;
        mc_.nd_temp_a = loc_temp_a;

        double laiCum( 0.0);
        for ( size_t fl = 0; fl < fl_cnt; ++fl)
        {
            if ( cbm::flt_greater_zero( lai_sum))
            {
                //no damping when no foliage
                if (( mc_.nd_airtemperature < 0.001) && ( mc_.nd_airtemperature > -0.001))
                {
                    //to avoid division by zero
                    for ( PlantIterator vt = this->m_veg->begin(); vt != this->m_veg->end(); ++vt)
                    {
                        laiCum += (*vt)->lai_fl[fl];
                    }
                    mc_.nd_tMax_fl[fl] = mc_.nd_temp_fl[fl] + (mc_.nd_maximumairtemperature - mc_.nd_airtemperature) * (1.0-laiCum/lai_sum);
                    mc_.nd_tMin_fl[fl] = mc_.nd_temp_fl[fl] - (mc_.nd_airtemperature - mc_.nd_minimumairtemperature) * (1.0-laiCum/lai_sum);
                }
                else
                {
                    mc_.nd_tMax_fl[fl] = mc_.nd_temp_fl[fl] * mc_.nd_maximumairtemperature / mc_.nd_airtemperature;
                    mc_.nd_tMin_fl[fl] = mc_.nd_temp_fl[fl] - mc_.nd_minimumairtemperature / mc_.nd_airtemperature;
                }
            }
            else
            {
                mc_.nd_tMax_fl[fl] = mc_.nd_maximumairtemperature;
                mc_.nd_tMin_fl[fl] = mc_.nd_minimumairtemperature;
            }
        }

        /* subdaily output */

        //this output might be used by subdaily running modules
        //when canopyecm itself runs daily
        //the best is to avoid this situation
        //run canopyecm subdaily as soon you have any other module running subdaily
        mc_.shortwaveradiation_out = mc_.nd_shortwaveradiation_out;
        mc_.rad_a = mc_.nd_rad_a;
        mc_.temp_a = mc_.nd_temp_a;

        for ( size_t fl = 0; fl < fl_cnt;  ++fl)
        {
            mc_.temp_fl[fl] = mc_.nd_temp_fl[fl];
            mc_.vpd_fl[fl] = mc_.nd_vpd_fl[fl];
            mc_.win_fl[fl] = mc_.nd_win_fl[fl];
            mc_.shortwaveradiation_fl[fl] = 0.0;
        }
    }
    else
    {
        KLOGERROR( "unsupported time mode .. huh?!");
        return  LDNDC_ERR_FAIL;
    }

    accumulated_evapotranspiration_canopy =  wc_.accumulated_transpiration_sl.sum() + wc_.accumulated_interceptionevaporation;
    accumulated_evapotranspiration_soil =  wc_.accumulated_soilevaporation + wc_.accumulated_surfacewaterevaporation;

    return  LDNDC_ERR_OK;
}


/*!
 * @page canopyecm
 * @section radiation Radiation
 *  Calculates radiation in each canopy layer and beneath the canopy
 *  and absorbed radiation in sunlit and shaded canopy fractions as well as the fraction
 *  of sunlit foliage area.
 * 
 *  The basic algorithms are taken from Spitters 1986 and complemented by Thornley 2002.
 *
 *  In contrast to traditional approaches, light extinction is accounted for separately
 *  layer by layer, considering changes in crown shape.
 * 
 *  Note that the proceedure doesen't account for slope effects. 
 *  A possible approach to address this might be that of Garnier and Ohmura (1968) 
 *  also applied in Lexer & Hoenninger 2001.
 */
void MicroClimateCanopyECM::CalcShortwaveRadiation()
{
    int fl_cnt( m_veg->canopy_layers_used());

    if ( cbm::flt_greater_zero( loc_rad_sw))
    {
        //photosynthetic active diffuse radiation outside the foliated part of a canopy layer (W m-2)
        double parOutDif_flm1 = loc_rad_sw * cbm::FPAR * loc_fdif;

        //photosynthetic active direct radiation outside the foliated part of a canopy layer (W m-2)
        double parOutDir_flm1 = std::max(0.0, loc_rad_sw * cbm::FPAR - parOutDif_flm1);

        //photosynthetic active diffuse radiation at the top of the foliated part of a canopy layer (W m-2)
        double parInDif_flm1  = 0.0;

        //photosynthetic active direct radiation at the top of the foliated part of a canopy layer (W m-2)
        double parInDir_flm1  = 0.0;

        double paiCum( 0.0);
        double f_areaCum( 0.0);
        double f_area_flm1( 0.0);

        double canopy_height( ph_.h_fl.sum());
        for ( int fl = fl_cnt-1;  fl >= 0;  fl--)
        {
            canopy_height -= 0.5 * ph_.h_fl[fl];

            if ( cbm::flt_greater( canopy_height, wc_.surface_ice))
            {
                //effective parameter estimation based on relative leaf area per canopy layer
                double  extEff( 0.0);
                double  albEff( 0.0);
                double  tauEff( 0.0);
                if ( cbm::flt_greater_zero( lai_fl[fl]))
                {
                    for ( PlantIterator vt = this->m_veg->begin(); vt != this->m_veg->end(); ++vt)
                    {
                        double const  lai_scale( (*vt)->lai_fl[fl] / lai_fl[fl]);
                        extEff += vt->EXT() * lai_scale;
                        albEff += vt->ALB() * lai_scale;
                        tauEff += vt->TAU() * lai_scale;
                    }
                }

                //lai and ground coverage of canopy layers
                double lai_flSum  = 0.0;
                for ( PlantIterator vt = this->m_veg->begin(); vt != this->m_veg->end(); ++vt)
                {
                    lai_flSum += (*vt)->lai_fl[fl];
                }
                f_areaCum = std::max(f_areaCum, f_area_fl[fl]);

                //plant area index within a canopy layer that is not related to foliage area
                double pai_agw( 0.0);
                for ( PlantIterator vt = this->m_veg->begin(); vt != this->m_veg->end(); ++vt)
                {
                    if ( cbm::flt_greater_zero( (*vt)->fFol_fl[fl]))
                    {
                        pai_agw += (*vt)->aboveground_wood() * QMASSAREA * (*vt)->fFol_fl[fl];
                    }
                }

                /* pai includes foliage and branch related lai
                 * foliated trees:
                 *  - branches are covered by foliage and only foliage related lai is accounted for
                 * non-foliated trees:
                 *  - only branch related lai is accounted for
                 * switch between foliated/non-foliated time periods is modeled by maximum relation.
                 */
                double pai_fl = std::max(lai_flSum, pai_agw);   //material for light extinction per hectare
                pai_fl /= std::max(0.01, f_area_fl[fl]);        //material for light extinction per ground coverage
                paiCum += (0.5 * pai_fl);

                //extinction coefficients (Spitters 1986)
                //radiation extinction coefficients for diffuse radiation
                //note: ext parameterized originally for foliated and non-foliated layer fractions pooled together
                double const extDif( cbm::flt_greater_zero( extEff) ? extEff : 0.8 * sqrt(1.0 - albEff));

                //radiation extinction coefficients for a black surface
                double extBla( 0.0);
                if ( !cbm::flt_greater_zero( extDif))
                {
                    if ( !cbm::flt_greater_zero( pai_fl))
                    {
                        extBla = 0.0;
                    }
                    else
                    {
                        extBla = 1.0;
                    }
                }
                else
                {
                    if ( cbm::flt_greater_zero( loc_sinb))
                    {
                        extBla = std::min(1.0, 0.5 * extDif / (extDif * loc_sinb));
                    }
                    else
                    {
                        extBla = 1.0;
                    }
                }

                //radiation extinction coefficients for direct radiation
                double const extDir = sqrt(1.0 - albEff) * extBla;

                //reflection coefficient (Goudriaan 1977, cit. in Spitters 1986) assuming horizontal leaf orientation
                double const refl = std::min(1.0, pai_fl * 0.5) * (1 - sqrt(1.0-albEff)) / (1 + sqrt(1.0-albEff));

                //photosynthetic active radiation decrease when passing the upper half of the foliage (Goudriaan 1982, cit. in Spitters 1986)
                double fparInDif_fl  = parInDif_flm1  * (1.0-refl) * std::exp(-extDif * pai_fl * 0.5);

                //photosynthetic active diffuse radiation within the foliated part of a canopy layer (W m-2)
                double fparInDir_fl  = parInDir_flm1  * (1.0-refl) * std::exp(-extDir * pai_fl * 0.5);

                //
                double fparOutDif_fl = parOutDif_flm1 * (1.0-refl) * std::exp(-extDif * pai_fl * 0.5);

                //photosynthetic active direct radiation within the foliated part of a canopy layer (W m-2)
                double fparOutDir_fl = parOutDir_flm1 * (1.0-refl) * std::exp(-extDir * pai_fl * 0.5);

                //actual radiation in the middle of the foliated part of the canopy layer
                double parInDif_fl( 0.0);
                double parInDir_fl( 0.0);
                if ( cbm::flt_greater_zero( f_area_fl[fl]))
                {
                    parInDif_fl = (fparInDif_fl * std::min(f_area_fl[fl], f_area_flm1)         //radiation out of foliated space of previous canopy layer
                                   + fparOutDif_fl * std::max(0.0, f_area_fl[fl]-f_area_flm1)) //radiation out of unfoliated space of previous canopy layer
                                   / f_area_fl[fl];                                            //total foliated fraction of the canopy layer
                    parInDir_fl = (fparInDir_fl * std::min(f_area_fl[fl], f_area_flm1)         //radiation out of foliated space of previous canopy layer
                                   + fparOutDir_fl * std::max(0.0, f_area_fl[fl]-f_area_flm1)) //radiation out of unfoliated space of previous canopy layer
                                   / f_area_fl[fl];                                            //total foliated fraction of the canopy layer

                    //reflected short wave radiation [W m-2]
                    loc_sw_rad_refl_fl[fl] = ((parInDif_fl + parInDir_fl) * refl / (1.0-refl)) / cbm::FPAR;
                }

                //actual radiation in the unfoliated part of the canopy layer
                double parOutDif_fl( 0.0); //[W m-2]
                double parOutDir_fl( 0.0); //[W m-2]
                if ( f_area_fl[fl] < 1.0)
                {
                    parOutDif_fl = (parOutDif_flm1 * std::min(1.0-f_area_fl[fl], 1.0-f_area_flm1) //radiation out of unfoliated space of previous canopy layer
                                    + parInDif_flm1 * std::max(0.0, f_area_flm1-f_area_fl[fl]))   //radiation out of foliated space of previous canopy layer
                                    / (1.0-f_area_fl[fl]);                                        //total unfoliated fraction of the canopy layer
                    parOutDir_fl = (parOutDir_flm1 * std::min(1.0-f_area_fl[fl], 1.0-f_area_flm1) //radiation out of unfoliated space of previous canopy layer
                                    + parInDir_flm1 * std::max(0.0, f_area_flm1-f_area_fl[fl]))   //radiation out of foliated space of previous canopy layer
                                    / (1.0-f_area_fl[fl]);                                        //total unfoliated fraction of the canopy layer
                }

                //absorped photosynthetic active radiation (Spitters 1986, consideration of transmissivity according Thornley 2002)
                double const difabs    = parInDif_fl * extDif / (1.0-tauEff);
                double const dirabs    = parInDir_fl * extDir / (1.0-tauEff);
                double const dirdirabs = (1.0-refl) * parInDir_fl * extBla * std::exp(-extDir * pai_fl * 0.5);

                //absorbed radiation in shaded and sunlit areas (Spitters 1986)
                loc_parshd_fl[fl]  = difabs + (dirabs - dirdirabs);
                loc_parsun_fl[fl]  = loc_parshd_fl[fl] + parInDir_fl * extBla * (1.0-albEff);
                loc_parshd_fl[fl] *= cbm::UMOL_IN_W;
                loc_parsun_fl[fl] *= cbm::UMOL_IN_W;

                //fraction of sunlit foliage area
                // areas below foliated and unfoliated canopy fractions are separately considered, 
                // unfoliated fractions are assumed to be fully light transparent ( ...+ 1.0 * (1.0-f_areaCum))
                if ( cbm::flt_greater_zero( lai_fl[fl]))
                {
                    loc_fsun_fl[fl] = std::exp(-extBla * paiCum) * f_areaCum + (1.0-f_areaCum);
                }
                else
                {
                    if ( fl < fl_cnt-1)
                    {
                        loc_fsun_fl[fl] = loc_fsun_fl[fl+1];
                    }
                    else
                    {
                        loc_fsun_fl[fl] = 1.0;
                    }
                }

                //full canopy layer radiation
                loc_sw_rad_fl[fl] = (((parInDif_fl + parInDir_fl) * f_area_fl[fl]
                                      + (parOutDif_fl + parOutDir_fl) * (1.0-f_area_fl[fl]))
                                     / cbm::FPAR);

                //absorbed/reflected radiation at the litter layer updated until soil or surface ice / water is reached
                loc_rad_a = (1.0-FLITALB) * loc_sw_rad_fl[fl];
                loc_sw_rad_refl_a = FLITALB * loc_sw_rad_fl[fl];
                
                //photosynthetic active radiation decrease when passing the lower half of the foliage
                parInDif_fl *= ((1.0-refl) * std::exp(-extDif * pai_fl * 0.5));
                parInDir_fl *= ((1.0-refl) * std::exp(-extDir * pai_fl * 0.5));

                //saving canopy radiation and fraction values for the next layer
                parInDif_flm1  = parInDif_fl;
                parInDir_flm1  = parInDir_fl;
                parOutDif_flm1 = parOutDif_fl;
                parOutDir_flm1 = parOutDir_fl;
                f_area_flm1    = f_area_fl[fl];

                //second half of the pai calculations
                paiCum += (0.5 * pai_fl);
            }
            else
            {
                loc_parshd_fl[fl] = 0.0;
                loc_parsun_fl[fl] = 0.0;
                loc_fsun_fl[fl] = 0.0;
                loc_sw_rad_fl[fl] = 0.0;
                loc_sw_rad_refl_fl[fl] = 0.0;
            }
            canopy_height -= 0.5 * ph_.h_fl[fl];
        }
    }
    else
    {
        loc_parshd_fl = 0.0;
        loc_parsun_fl = 0.0;
        loc_fsun_fl = 0.0;
        loc_sw_rad_fl = 0.0;
        loc_sw_rad_refl_fl = 0.0;
    }

    loc_sw_rad_out = loc_sw_rad_refl_fl.sum() + loc_sw_rad_refl_a;
}


/*! 
 * @page canopyecm
 * @section vpd Vapor Pressure Deficit
 * Calculates vapor pressure deficit (mbar, or hPa) for the ground and every foliage layer.
 */
void MicroClimateCanopyECM::CalcHumidity()
{
    /*!
     * @page canopyecm
     * VPD is calculated based on the canopy layer temperature. It is 
     * assumed that the absolute water content throughout the canopy is , 
     * constant but the saturated vapor pressure and thus the pressure  
     * deficit changes with temperature in every layer.
     */
    if ( timemode() & TMODE_SUBDAILY)
    {
        double vps( 0.0);
        ldndc::meteo::vps( climate_.temp_avg_subday(lclock_ref()), &vps);
        double const vp( vps * climate_.rel_humidity_subday( lclock_ref()) / 100.0);

        for ( size_t fl = 0; fl < se_.canopylayers(); ++fl)
        {
            ldndc::meteo::vps( loc_temp_fl[fl], &vps);
            loc_vpd_fl[fl] = cbm::bound_min( 0.0, vps - vp);
        }
    }
    else
    {
        double vps( 0.0);
        ldndc::meteo::vps( climate_.temp_avg_day(lclock_ref()), &vps);
        double const vp( vps * climate_.rel_humidity_day( lclock_ref()) / 100.0);

        for ( size_t fl = 0; fl < se_.canopylayers(); ++fl)
        {
            ldndc::meteo::vps( loc_temp_fl[fl], &vps);
            loc_vpd_fl[fl] = cbm::bound_min( 0.0, vps - vp);
        }
    }
}


/*! 
 * @page canopyecm
 * @section windspeed Wind Speed
 * Calculates wind speed decline throughout the canopy according to 
 * @cite lalic_mihailovic:2002a. However, the originally used
 * dependency on leaf area has been replaced by one of plant area
 * which also considers the impact of overall woody biomass.
 *
 * Wind speed is calculated in each canopy layer (using cosh =Hyper Cosinus):
 *
 * \f[
 *   loc\_{win}_{fl} =
 *    loc\_{win} \cdot \frac {cosh(ExtFac \frac {hCum - hBottom}{hTop})}{cosh(ExtFac \frac {1-hBottom}{hTop})}^{2.5} 
 * \f]
 *
 * with
 * <TABLE border="0.1" align = "center" cellspacing="0">
 *    <TR><TD> loc\_{win}_{fl}   </TD><TD> Foliage layer specific wind speed  </TD><TD> (m s-1) </TD></TR>
 *    <TR><TD> loc\_{win}        </TD><TD> Above-canopy wind speed            </TD><TD> (m s-1) </TD></TR>
 *    <TR><TD> hCum              </TD><TD> Cumulative height                  </TD><TD> (m)     </TD></TR>
 *    <TR><TD> hBottom           </TD><TD> Bottom height of tree crowns       </TD><TD> (m)     </TD></TR>
 * </TABLE>
 *
 * The extinction factor depends linearly on plant area index:
 * \f[
 *   extFac = \frac{ 4.0 \cdot DRAGC  \cdot pai}{ROUGH^2 KARMAN^2} 
 * \f]
 * \f[
 *   pai = lai + mWood \cdot FEXT_W \cdot exp(-0.1 \cdot lai)
 * \f]
 *
 * with
 * <TABLE border="0.1" align = "center" cellspacing="0">
 *     <TR><TD> extFac        </TD><TD> Extinction factor                       </TD><TD> (-) </TD></TR>
 *     <TR><TD> DRAGC = 0.1   </TD><TD> Drag coefficient                        </TD><TD> (-) </TD></TR>
 *     <TR><TD> ROUGH = 2.0   </TD><TD> Roughness parameter of the ground (1-2) </TD><TD> (-) </TD></TR>
 *     <TR><TD> KARMAN = 0.41 </TD><TD> Karman constant (Hogstrom 1985)         </TD><TD> (-) </TD></TR>
 * </TABLE>
 * 
 * @image html CalcWindSpeed.jpg "Dependency of windspeed on crown height"
 *
 * @note: The drag coefficient is actually a function of wind speed and leaf area
 * clumping (Cescatti and Marcolla 2004), which is not considered here.
 */
void
MicroClimateCanopyECM::CalcWindspeed()
{
    double  mWood( 0.0);
    double  hBottom (0.0);
    if ( this->m_veg->size() > 0)
    {
        PlantIterator vt = this->m_veg->begin();
        hBottom = (*vt)->height_at_canopy_start;

        for ( ; vt != this->m_veg->end(); ++vt)
        {
            MoBiLE_Plant * p = *vt;
            mWood += p->aboveground_wood();
            if ( p->height_at_canopy_start < hBottom)
            {
                hBottom = p->height_at_canopy_start;
            }
        }
    }

    //effective plant area index, considers total woody tissue as proxy for wind absorbing power without leaves
    double  lai( 0.0);
    for ( PlantIterator vt = this->m_veg->begin(); vt != this->m_veg->end(); ++vt)
    {
        lai += (*vt)->lai();
    }

    // The extinction factor depends linearly on plant area index.
    double const pai_eff( lai + mWood * FEXT_W * std::exp(-0.1 * lai));
    double const extFac( (pai_eff > 0) ? 4.0 * fdrag * pai_eff / (ROUGH * ROUGH * cbm::KARMAN * cbm::KARMAN) : 0.0);

    size_t const fl_cnt( m_veg->canopy_layers_used());
    double const hTop( ph_.h_fl.sum());
    double hCum( hTop);
    for ( int  fl = (int)(fl_cnt)-1;  fl >= 0;  fl--)
    {
        hCum -= (0.5 * ph_.h_fl[fl]);
        if ( cbm::flt_greater( hCum, hBottom))
        {
            loc_win_fl[fl] = std::max(0.1, loc_win * std::pow(cosh(extFac * (hCum - hBottom) / hTop)
                                                              / (cosh(extFac * (1.0 - hBottom / hTop))),EXPW));
        }
        else
        {
            unsigned int  flc( fl+1);
            if ( flc != fl_cnt)
            {
                loc_win_fl[fl] = std::max(0.1,loc_win_fl[fl+1]);
            }
            else
            {
                loc_win_fl[fl] = loc_win;
            }
        }
        hCum -= (0.5 * ph_.h_fl[fl]);
    }
}


/*!
 * @page canopyecm
 * @section albedo Albedo
 * Albedo is the fraction of reflected light relative to the total incoming shortwave 
 * radiation.
 * \f[
 *   albedo = \frac { \sum_0^fl(sw\_{refl}_{fl}) + sw\_{refl}_{a} }{rad_sw}
 * \f]
 * \f[
 *   sw\_{refl}_{fl} = (rdiff_{fl} + rdir_{fl}) \cdot \frac {refl}{1.0-refl}
 * \f]
 * \f[
 *   refl = (pai \cdot 0.5) \cdot (1.0 - \frac { sqrt(1.0-ALB) } { 1.0 + sqrt(1.0-ALB) };
 * \f]
 * with
 * - fl: canopy layer counter
 * - sw_refl_a: reflected shortwave radiation from soil surface
 * - sw_refl: reflection coefficient @cite spitters:1986a
 * - rdiff: incoming diffuse radiation into this layer
 * - rdir: incoming direct radiation into this layer
 * - pai: plant area index (see @ref windspeed)
 * - ALB: species-specific albedo factor
 * 
 * The reflection from the soil surface is assumed to be a fixed proportion (FLITALB)
 * of the shortwave radiation that is released below the last canopy layer. This is 
 * added to the reflected radiation of all canopy layers, which in turn are influenced
 * by plant area (that is a function of foliage and wood biomass) distribution and a 
 * species-specific albedo parameter (ALB). If more than one species has leaves in the
 * same layer, plant area  and ALB are scaled with leaf area fraction of the respective
 * species.
 */
void
MicroClimateCanopyECM::CalcAlbedo()
{
    if ( cbm::flt_greater_zero( loc_rad_sw))
    {
        mc_.albedo = (loc_sw_rad_refl_fl.sum() + loc_sw_rad_refl_a) / loc_rad_sw;
    }
}



/*! @brief ratio between aboveground woody biomass and plant area index (artificial weighting index) */
const double MicroClimateCanopyECM::QMASSAREA = 0.07;

/*! @brief exponent for wind decrease (approximated to Waldstein data, spruce forest) */
const double MicroClimateCanopyECM::EXPW = 2.5;

/*! @brief roughness parameter of the ground (1-2) */
const double MicroClimateCanopyECM::ROUGH = 2.0;

/*! @brief wind extinction impact of woody tissue */
const double MicroClimateCanopyECM::FEXT_W = 0.007;

/*! @brief albedo of the litter layer */
const double MicroClimateCanopyECM::FLITALB = 0.1;

} /*namespace ldndc*/

