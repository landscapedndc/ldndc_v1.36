/*!
 * @file
 *    MicroClimateCanopyECM was created by Ruediger Grote for mobile-0.1.0
 *    (this submodule works in subdaily and daily mode and calculates
 *    climate processes of the canopy / vegetation (above ground)).
 * 
 * @author
 *      ruediger grote
 */

#ifndef  LM_MICROCLIMATE_CANOPY_ECM_H_
#define  LM_MICROCLIMATE_CANOPY_ECM_H_

#include  "mbe_legacymodel.h"
#include  "state/mbe_state.h"

namespace ldndc {


class  LDNDC_API  MicroClimateCanopyECM  :  public  MBE_LegacyModel
{
    LMOD_EXPORT_MODULE_INFO(MicroClimateCanopyECM,"microclimate:canopyecm","Microclimate CanopyECM");
    
    /*! ratio between aboveground woody biomass and plant area index
     * (artificial weighting index)
     */
    static const double  QMASSAREA;
    /*! exponent for wind decrease (approximated to Waldstein data, spruce forest) */
    static const double  EXPW;
    /*! roughness parameter of the ground (1-2) */
    static const double  ROUGH;
    /*! wind extinction impact of woody tissue in leafless periods */
    static const double  FEXT_W;

    /*! albedo of the litter layer */
    static const double  FLITALB;
    
    /*! minimum soil layer thickness [m] */
    static const double  HLMIN;
    /*! minimum depth with constant average year temperature [m] */
    static const double  HBASE;
    /*! snow damping effect on temperature transfer from air to soil [?] */
    static const double  SDAMP;

public:
    MicroClimateCanopyECM(
                          MoBiLE_State *,
                          cbm::io_kcomm_t *,
                          timemode_e);
    
    ~MicroClimateCanopyECM();
    
    
    lerr_t  configure( ldndc::config_file_t const *);
    
    lerr_t  initialize();
    
    lerr_t  solve();
    
    lerr_t  finalize() { return  LDNDC_ERR_OK; }
    
    lerr_t  sleep() { return  LDNDC_ERR_OK; }
    lerr_t  wake() { return  LDNDC_ERR_OK; }
    
private:
    input_class_setup_t const &  se_;
    substate_watercycle_t const &  wc_;
    substate_microclimate_t &  mc_;
    substate_physiology_t const &  ph_;
    
    MoBiLE_PlantVegetation *  m_veg;
    
    input_class_climate_t const &  climate_;
    input_class_soillayers_t const &  sl_;
    substate_soilchemistry_t const &  sc_;
    
    substate_surfacebulk_t &  sbl_;
    
    solver_tridiagonal_t  tdma_solver_;
    
    /* pointer to daily or subdaily state items */
    /* radiation per canopy layer (temporarly unit-converted) [W m-2] */
    lvector_t< double >  loc_lw_rad_down_fl;
    lvector_t< double >  loc_lw_rad_up_fl;
    lvector_t< double >  loc_sw_rad_fl;
    lvector_t< double >  loc_sw_rad_refl_fl;
    double loc_sw_rad_refl_a;
    lvector_t< double >  f_area_fl;
    lvector_t< double >  lai_fl;
    lvector_t< double >  loc_temp_fl;
    lvector_t< double >  loc_fsun_fl;
    lvector_t< double >  loc_parshd_fl;
    lvector_t< double >  loc_parsun_fl;
    lvector_t< double >  loc_vpd_fl;
    lvector_t< double >  loc_win_fl;

    bool al_new;                        // if additional layers above soil occur the first time ala_new is true, else false;
    
    double *  temp_old_sl;              // old soil temperature for soil layers
    double *  temp_old_sbl;             // old soil temperature for bulk surface layers (snow or water)
    
    double *  hcap_sl;                  // volumetric heat capacity of the soil layer (J K-1 m-2)
    double *  lambda_sl;                // heat conductivity (J K-1 m-1 s-1)
    
    double *  h_extended_sl;            // extends the last soil layer for more realistic lower boundary condition of constant annual temp
    double *  som_extended_sl;
    double *  min_extended_sl;
    
    //  depth of soil layer (m)
    double *  dsl;
    // volumetric heat capacity of the soil layer (J K-1 m-2)
    double *  hcap;
    // heat conductivity (J K-1 m-1 s-1)
    double *  lambda;
    // soil temperature to start with (oC)
    double *  tempOld_sl;
    
    bool dirichletboundarycondition;
    double annual_average_temperature;
    
    double domainextension;
    bool energybalance;
    
    /*! ... */
    cbm::string_t soiltemperature_method;
    
    double accumulated_evapotranspiration_canopy;
    double accumulated_evapotranspiration_soil;
    
    /* outgoing (reflected) short wave radiation [W m-2] */
    double loc_sw_rad_out;

    /* outgoing long wave radiation [W m-2] */
    double loc_lw_rad_out;

    /* outgoing sensible heat [W m-2] */
    double loc_sensible_heat_out_top;
    double loc_sensible_heat_out_bottom;
    
    /* radiation above litter [W m-2] */
    double loc_rad_a;
    double loc_temp_a;
    double loc_rad_sw;
    double loc_rad_lw;
    double loc_temp;
    double loc_win;
    double loc_vpd;
    double loc_fdif;
    double loc_sinb;

    /*! sum of lai over all species (updated each time step) */
    double  lai_sum;
    
    // parameter aggregation
    double  fdrag;
    
    
    lerr_t  CanopyECM_step_init();
    lerr_t  CanopyECM_step_out();
        
    void  CalcTemperature();

    /*!
     * @brief
     *    Calculates temperature development from above the canopy
     *    to the deepest soil layer
     */
    lerr_t  CalcSoilTemperature_implicite(
                                 lvector_t< double > & /*soil temperatures*/,
                                 double /*average temperature above canopy*/,
                                 double &/*average temperature above litter*/);

    lerr_t  CalcSoilTemperature_implicite_extended(
                                 lvector_t< double > & /*soil temperatures*/,
                                 double /*average temperature above canopy*/,
                                 double &/*average temperature above litter*/);
    
    lerr_t  CalcSoilTemperature_explicite(
                                 lvector_t< double > & /*soil temperatures*/,
                                 double /*average temperature above litter*/);
    
    
    /*! CalcShortwaveRadiation
     * Calculates radiation in each canopy layer and beneath the canopy
     *     and absorbed radiation in sunlit and shaded canopy fractions as well as the fraction
     *     of sunlit foliage area.
     *
     *     The basic algorithms are taken from Spitters 1986 and complemented by Thornley 2002.
     *
     *     In contrast to traditional approaches, light extinction is accounted for separately
     *     layer by layer, considering changes in crown shape.
     */
    void  CalcShortwaveRadiation();
    
    
    /*! CalcHumidity
     * Calculates vapor pressure deficit (mbar bzw. hPa)
     *   \li According to temperature conditions after DVWK 1996
     *     (for below-zero temperatures according to Tetens O. (1930):
     *     ueber einige meteorologische Begriffe. Z. Geophys. 6:297-309)
     *   \li Computation of vapor pressure deficit is done according to Murray (1967).
     */
    void  CalcHumidity();
    
    /*! CalcWindspeed
     * Calculates wind speed - according to Lalic and Mihailovic 2002
     *
     *   CHANGES: LAI as influencing wind speed decrease is replaice by pai_eff
     *            which also considers the impact of woody structures
     *
     *   NOTE: The drag coefficient is actually a function of wind speed and leaf area
     *         clumping (Cescatti and Marcolla 2004), which is not considered here!!
     */
    void  CalcWindspeed();
    
    /*!
     * @brief
     *
     *
     */
    void  CalcAlbedo();

    /*!
     * @brief
     *
     *
     */
    double  foliage_layer_heat_capacity( size_t /* foliage layer*/);
    
    /*!
     * @brief
     *
     *
     */
    double  get_canopy_energy();

    double  get_surface_bulk_energy();
    
    double  get_soil_energy( lvector_t< double > &  /* soil temperature */);
    
    /*!
     * @brief
     *
     *
     */
    double get_lai_fraction( size_t /* foliage layer */,
                             size_t /* no of foliage layer */);
    
    /*!
     * @brief
     *
     *
     */
    double  get_sky_view( double const & /* lai*/);
    
    /*!
     * @brief
     *
     *
     */
    double get_lw_radiation_down( double const & /* open area fraction top */,
                                  double const & /* open area fraction */,
                                  double const & /* leaf area index */,
                                  double const & /* incoming longwave radiation */);
};
} /*namespace ldndc*/

#endif  /*  !LDNDC_MOD_MICROCLIMATE_CANOPY_ECM_H_  */

