/*!
 * @file
 * @author
 *    - David Kraus
 *
 * @date
 *    Dec, 2017
 */

#include  "microclimate/ld_longwave_radiation.h"

#include  <math/cbm_math.h>
#include  <scientific/meteo/ld_meteo.h>
#include  <constants/lconstants-conv.h>
#include  <constants/lconstants-math.h>
#include  <constants/lconstants-phys.h>
#include  <logging/cbm_logging.h>


/*!
 * @page miclibs
 *  @section longwave_radiation Incoming longwave radiation
 *  Incoming longwave radiation is given by:
 *  \f[
 *  LWR_{in} = \varepsilon \sigma T^4 \; ,
 *  \f]
 *  wherein \f$ \varepsilon, \sigma, T \f$ refer to
 *  the emissivity, the Stefan-Boltzmann constant
 *  and air temperature, respectively.
 */
double
ldndc::longwave_radiation(
                          double _clouds,
                          double _temp,
                          double _vp)
{
    /*!
     * @page miclibs
     *  The emissivity \f$ \varepsilon \f$ has two components
     *  , i.e., emissivity under clear (\f$ \varepsilon_{cl} \f$)
     *  and clouded sky (\f$ \varepsilon_{cl} \f$):
     *  \f[
     *  \varepsilon = (1 - f_{cl}) \varepsilon_{cs} + f_{cl} \varepsilon_{cl}
     *  \f]
     *  The partitioning coefficient \f$ f_{cl} \f$ refers to the
     *  cloud fracion. For \f$ \varepsilon_{cl} \f$, a constant value
     *  of 0.976 is used @cite greuell:1997a.
     */
    double const epsilon_cs( radiation_emissivity_clear_sky( _vp));
    double const epsilon_cl( 0.976);
    double const emissivity( (1.0 - _clouds) * epsilon_cs + _clouds * epsilon_cl);
    return emissivity * cbm::SIGMA * std::pow(_temp + 273.16, 4.0) * cbm::SEC_IN_DAY;
}



/*!
 * @page miclibs
 *  <b> Clear sky atmospheric emissivity </b> \n
 *  Emissivity of incoming longwave radiation under clear sky conditions
 *  is calculated based on vapour pressure \f$ vp \f$ @cite brunt:1932a :
 *  \f[
 *  \varepsilon_{cs} = B_c + B_d \sqrt{vp}
 *  \f]
 *  The parameters \f$ B_c \f$ and \f$ B_d \f$ are set to 0.53 and 0.212,
 *  respectively @cite kraalingen:1997a.
 */
double
ldndc::radiation_emissivity_clear_sky(
                                      double _vp /* vapour pressure */)
{
    /* Brunt parameters (-) */
    double const bc_brunt( 0.53);
    double const bd_brunt( 0.212);

    return bc_brunt + bd_brunt * cbm::sqrt( _vp);
}



/*!
 * @page miclibs
 *  <b> Cloudiness </b> \n
 *  Cloudiness is derived from global radiation
 *  using the Angstrom formular:
 *  \f[
 *  f_{cl} = \frac{\frac{SWR}{SWR^{\ast}} - A}{B}
 *  \f]
 *  using the Angstrom parameters \f$ A = 0.29 \f$
 *  and \f$ B = 0.52 \f$ @cite kraalingen:1997a.
 */
double
ldndc::cloud_fraction_angstrom(
                               int _doy,
                               int _nd_doy,
                               double _lat,
                               double _rad_short_day)
{
    /* A value of Angstrom formula [-] */
    double ang_a( 0.29);

    /* B value of Angstrom formula [-] */
    double ang_b( 0.52);

    /* Average atmospheric transmission coefficient [-] */
    double const solar_radiation( ldndc::meteo::daily_solar_radiation(_lat, _doy, _nd_doy)
                                  * cbm::JDCM_IN_WM * cbm::CM2_IN_M2);

    return (1.0 - cbm::bound( 0.0, _rad_short_day / solar_radiation, 1.0) - ang_a) / ang_b;
}

