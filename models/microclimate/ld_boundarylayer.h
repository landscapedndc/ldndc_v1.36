/*!
 * @file
 *
 * @author
 *    david kraus (created on: april 20, 2015)
 */

#ifndef  LD_BOUNDARYLAYER_H_
#define  LD_BOUNDARYLAYER_H_

namespace ldndc {

/*!
 * @brief
 *      returns boundary layer resistance above canopy
 */
double
resistance_aero_abovecanopy(
                            double const & /* wind speed */,
                            double const & /* canopy height */);

/*!
 * @brief
 *      returns boundary layer resistance within canopy
 */
double
resistance_aero_incanopy(
                         double const & /* wind speed */,
                         double const & /* canopy height */,
                         double const & /* lai */);

/*!
 * @brief
 *
 */
double
resistance_quasilaminar(
                        double const & /* wind speed */,
                        double const & /* canopy height */,
                        double const & /* lai */);

/*!
 * @brief
 *
 */
double
resistance_ground(
                  double const & /* wind speed */,
                  double const & /* wind speed height */,
                  double const & /* canopy height */,
                  double const & /* lai */,
                  double const & /* temperature */,
                  double const & /* diffusion coefficient */);

/*!
 * @brief
 *
 */
double
reynolds_number(
                double const & /* wind speed */,
                double const & /* wind speed meas height */,
                double const & /* canopy height */,
                double const & /* temperature */);

/*!
 * @brief
 *
 */
double
schmitt_number(
               double const & /* temperature */,
               double const & /* diffusion coefficient */);

/*!
 * @brief
 *
 */
double
air_viscosity_kinematic(
                        double const & /* temperature */);

/*!
 * @brief
 *
 */
double
friction_velocity(
                  double const & /* wind speed */,
                  double const & /* wind speed meas height */,
                  double const & /* canopy height */);


} /* namespace ldndc */

#endif  /* !LD_BOUNDARYLAYER_H_ */
