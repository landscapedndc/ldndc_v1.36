/*!
 * @file
 * @author
 *    - David Kraus
 *
 * @date
 *    Dec, 2017
 */

#ifndef  LD_LONGWAVERADIATION_H_
#define  LD_LONGWAVERADIATION_H_

#include <cbm_errors.h>

namespace ldndc {

/*!
 * @brief
 *
 */
double
cloud_fraction_angstrom(
                        int /* day of year */,
                        int /* number of days of year */,
                        double /* latitude */,
                        double /* short-wave radiation */);

/*!
 * @brief
 *
 */
double
longwave_radiation(
                   double /* cloudiness */,
                   double /* temperature */,
                   double /* vapour pressure */);

/*!
 * @brief
 *
 */
double
radiation_emissivity_clear_sky(
                               double /* vapour pressure */);

} /* end namespace ldndc */

#endif  /*  !LD_LONGWAVERADIATION_H_  */
