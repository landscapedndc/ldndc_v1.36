/*!
 * @brief
 *    ...
 *
 * @author
 *   tobias denk (created on: dec, 01, 2015)
 */


#include  "ld_isotopes.h"
#include  <math/cbm_math.h>

#ifdef  MoBiLE_Isotopes

#define  ISOTOPES_MAXINDEX  128

namespace {

    size_t  iso_strcpylen( char *  _d, char const *  _c)
    {
        char *  d = _d;
        char const *  c = _c;
        while ( *c != '\0')
        {
            *d = *c;
            ++d;
            ++c;
        }
        *d = '\0';
        return  d-_d;
    }
    char *  iso_itemkey( char * _itemkey,
            char const *  _key, size_t  _k, int _hour)
    {

        char *  c = _itemkey;

        *c = 't';
        *++c = 'd';
        *++c = '@';

        size_t  len = iso_strcpylen( ++c, _key);
        c += len;
        if ( _k != ldndc::invalid_t< size_t >::value)
        {
            ldndc_assert( _k <= 0xFFFF);

            *c = '.';
            len = cbm::stringify_t< 2 >::to_hex( ++c, _k);
            c += len;
        }

        if ( _hour != -1)
        {
            ldndc_assert( _hour <= 0xFF);

            *c = '.';
            len = cbm::stringify_t< 1 >::to_hex( ++c, _hour);
            c += len;
        }

        *c = '\0';

        return  _itemkey;
    }

    void  iso_setvalue( cbm::state_scratch_t *  _mcom,
            char const *  _key_name, double  _value)
    {
        _mcom->set( _key_name, _value);
    }
    void  iso_addvalue( cbm::state_scratch_t *  _mcom,
            char const *  _key_name, double  _value)
    {
        _mcom->addvalue( _key_name, _value);
    }
}

char const *
ldndc::Isotopes::keyname( char *  _keybuffer,
        char const *  _key_name, size_t  _k, int  _hour)
{
    char const *  key_name =
        iso_itemkey( _keybuffer, _key_name, _k, _hour);
    return  key_name;
}

void
ldndc::Isotopes::rewrite(
        double  _address_name, char const *  _key_name, size_t  _k)
{
    ldndc_assert( this->io_kcomm);
    keybuffer_t  itemkey;
    iso_setvalue( this->io_kcomm->get_scratch(),
        this->keyname( itemkey, _key_name, _k), _address_name);
}
void
ldndc::Isotopes::cumulative_write( double  _address_name,
        char const *  _key_name, size_t  _k)
{
    ldndc_assert( this->io_kcomm);
    keybuffer_t  itemkey;
    iso_addvalue( this->io_kcomm->get_scratch(),
        this->keyname( itemkey, _key_name, _k), _address_name);
}
void
ldndc::Isotopes::subdaily_write( double  _address_name,
        char const *  _key_name, size_t  _k)
{
    ldndc_assert( this->io_kcomm);
    keybuffer_t  itemkey;
    iso_addvalue( this->io_kcomm->get_scratch(),
        this->keyname( itemkey, _key_name, _k, this->hour), _address_name);
}

#else /* MoBiLE_Isotopes */

void
ldndc::Isotopes::rewrite(
        double, char const *, size_t)
{ }
void
ldndc::Isotopes::cumulative_write(
        double, char const *, size_t)
{ }
void
ldndc::Isotopes::subdaily_write(
        double, char const *, size_t)
{ }
#endif

