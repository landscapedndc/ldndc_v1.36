/*!
 * @brief
 *    access attributes in formatted messages
 *
 * @author
 *    steffen klatt (created on: mar 12, 2017)
 */

#ifndef  LDNDC_ATTRIBUTE_QUERY_H_
#define  LDNDC_ATTRIBUTE_QUERY_H_

#include  "ldndc-dllexport.h"
#include  "ldndc-config.h.inc"
#include  <json/cbm_json.h>

namespace ldndc {

template < typename _T >
struct LDNDC_API _AttributeQuery
{
};
template < >
struct LDNDC_API _AttributeQuery<char const *>
{
    static char const *  get( cbm::jquery_t const &  _jquery,
            char const *  _attribute_path, char const *  _default)
        {
            return _jquery.query_string( _attribute_path, _default);
        }
};
template < >
struct LDNDC_API _AttributeQuery<bool>
{
    static bool get( cbm::jquery_t const &  _jquery,
            char const *  _attribute_path, bool  _default)
        { return _jquery.query_bool( _attribute_path, _default); }
};
template < >
struct LDNDC_API _AttributeQuery<int>
{
    static int get( cbm::jquery_t const &  _jquery,
            char const *  _attribute_path, int  _default)
        {
            return _jquery.query_int( _attribute_path, _default);
        }
};
template < >
struct LDNDC_API _AttributeQuery<double>
{
    static double get( cbm::jquery_t const &  _jquery,
            char const *  _attribute_path, double  _default)
        {
            return _jquery.query_double( _attribute_path, _default);
        }
};
template < >
struct LDNDC_API _AttributeQuery<float>
{
    static float get( cbm::jquery_t const &  _jquery,
            char const *  _attribute_path, float  _default)
        {
            return _jquery.query_float( _attribute_path, _default);
        }
};

class LDNDC_API AttributeQuery
{
public:
    AttributeQuery( char const *  _attributes, size_t  _attributes_sz)
            : jquery( _attributes, _attributes_sz) { }

    template < typename _T >
        _T const  get( char const *  _attribute_path,
                _T  _default) const
        {
            return _AttributeQuery<_T>::get(
                this->jquery, _attribute_path, _default);
        }

private:
    cbm::jquery_t  jquery;
};

class LDNDC_API AttributeBuild
{
public:
    AttributeBuild( bool  _ignore_invalid)
        : jbuild( _ignore_invalid ? CBM_JBUILD_IGNOREINVALID : 0) { }

    template < typename _T >
        lerr_t  put( char const *  _attribute_path, _T  _value)
            { return this->jbuild.put<_T>( _attribute_path, _value); }

    char const *  serialize( size_t *  _size = NULL) const
        { return this->jbuild.serialize( _size); }

private:
    cbm::jbuild_t  jbuild;
};


} /* namespace ldndc */

#endif  /* !LDNDC_ATTRIBUTE_QUERY_H_ */

