/*!
 * @brief
 *      plant container
 *
 * @author
 *      Steffen Klatt (created on: apr 20, 2017),
 *      David Kraus
 */

#ifndef  LD_PLANTS_H_
#define  LD_PLANTS_H_

#include  <species/speciesgroups.h>

#include  <string/cbm_string.h>
#include  <memory/cbm_mempool.h>
#include  <containers/cbm_set.h>

namespace ldndc {

template < typename _Parameters >
class LDNDC_API LD_PlantSettings
{
public:
    cbm::string_t  name;
    cbm::string_t  type;
    cbm::string_t  group;

    typedef _Parameters Parameters; 
    Parameters const *  parameters;
};

template < typename _Settings >
class LDNDC_API LD_Plant
{
public:
    enum LD_PlantState { unknown, seeded, growing, harvested, masked };
public:
    typedef typename _Settings::Parameters Parameters;
    typedef _Settings Settings;

    LD_PlantState  state;
public:
    LD_Plant( Settings const *,
        size_t = 0 /*approx. properties size (if 0, use internal default) */);
    virtual ~LD_Plant();

public:
    virtual Parameters const *  parameters() const
        { return &this->m_parameters; }
    virtual Parameters const *  operator->() const
        { return this->parameters(); }

    virtual cbm::string_t const &  name() const
        { return this->m_name; }
    virtual cbm::string_t const &  type() const
        { return this->m_type; }
    virtual cbm::string_t const &  group() const
        { return this->m_group; }

private:
    cbm::string_t  m_name;
    cbm::string_t  m_type;
    cbm::string_t  m_group;

protected:
    Parameters  m_parameters;

    /*for internal use only*/
    template < typename _T >
    _T *  calloc( size_t /*memory size*/);
private:
    CBM_MemPool  mempool;
};

} /* namespace ldndc */

template < typename _Settings >
ldndc::LD_Plant< _Settings >::LD_Plant(
        _Settings const *  _plantsettings, size_t  _approx_memsize)
    : state( unknown)
{
    if ( _plantsettings)
    {
        this->m_name = _plantsettings->name;
        this->m_type = _plantsettings->type;
        this->m_group = _plantsettings->group;

        if ( _plantsettings && _plantsettings->parameters)
            { this->m_parameters = *_plantsettings->parameters; }
    }

    this->mempool =
        CBM_MemPoolCreate( CBM_MemPoolNone, _approx_memsize);
    CBM_LogDebug( "approximate mempool buffer size ", _approx_memsize);
}

template < typename _Settings >
ldndc::LD_Plant< _Settings >::~LD_Plant< _Settings >()
{
    CBM_MemPoolDestroy( this->mempool);
}

template < typename _Settings >
    template < typename _T >
    _T *
ldndc::LD_Plant< _Settings >::calloc( size_t  _memsize)
{
    return  static_cast< _T * >( CBM_CAlloc( this->mempool, _memsize, 0));
}

namespace ldndc {

#define LD_PlantMemoryReserveCount 4
#define LD_PlantsParametersDB speciesparameters::input_class_speciesparameters_srv_t
template < typename _Plant >
class LDNDC_API LD_PlantVegetation
{
public:
    LD_PlantVegetation( LD_PlantsParametersDB const *);
    virtual ~LD_PlantVegetation();

    size_t  size() const
        { return CBM_SetSize( this->m_plants); }
    size_t  size_by_group( species::species_group_e);

    bool  have_plant( char const * /*name*/);
    bool  have_plant( cbm::string_t const &  _name)
        { return this->have_plant( _name.c_str()); }
    _Plant *  get_plant( char const * /*name*/);
    _Plant *  get_plant( cbm::string_t const &  _name)
        { return this->get_plant( _name.c_str()); }
    _Plant *  operator[]( char const *  _name)
        { return this->get_plant( _name); }

protected:
    virtual _Plant *  new_plant( typename _Plant::Settings const *);
    virtual lerr_t  delete_plant( char const *);

public:
    class PIterator
    {
    public:
        PIterator() { }
        virtual ~PIterator() { }

        virtual operator _Plant *() = 0;
        virtual _Plant *  operator*() = 0;
        virtual _Plant const *  operator*() const = 0;
        virtual typename _Plant::Parameters const *  operator->() const = 0;
    };

    friend class Iterator;
    class Iterator : public PIterator
    {
    public:
        Iterator();
        Iterator( CBM_Set &);
        Iterator &  next();
        Iterator &  operator++()
            { return  this->next(); }

        operator _Plant *()
            { return static_cast< _Plant * >( this->m_it.value); }
        _Plant *  operator*()
            { return static_cast< _Plant * >( this->m_it.value); }
        _Plant const *  operator*() const
            { return static_cast< _Plant const * >( this->m_it.value); }
        typename _Plant::Parameters const *  operator->() const
            { return static_cast< _Plant const * >( this->m_it.value)->parameters(); }

        bool  operator==( Iterator const &  _rhs) const
            { return this->m_it.value == _rhs.m_it.value; }
        bool  operator!=( Iterator const &  _rhs) const
            { return !this->operator==( _rhs); }

    private:
        CBM_SetNodeIterator  m_it;
    };

    Iterator  begin()
        { return  Iterator( this->m_plants); }
    Iterator  end()
        { return  Iterator(); }

    template < typename _G1, typename _G2 = species::none, typename _G3 = species::none, typename _G4 = species::none >
    class GroupIterator : public PIterator
    {
    public:
        GroupIterator( Iterator  _it, Iterator  _itend)
            : m_it( _it), m_end( _itend)
        {
            if (( this->m_it != this->m_end)
                && ( !species::species_groups_select_t< _G1, _G2, _G3, _G4 >().is_selected(
                                (*this->m_it)->groupId())))
                { this->next(); }
        }
        GroupIterator< _G1, _G2, _G3, _G4 > &  next()
        {
            ++this->m_it;
            while (( this->m_it != this->m_end)
                && ( !species::species_groups_select_t< _G1, _G2, _G3, _G4 >().is_selected(
                                (*this->m_it)->groupId())))
                { ++this->m_it; }
            return  *this;
        }
        GroupIterator< _G1, _G2, _G3, _G4 > &  operator++()
            { return  this->next(); }

        operator _Plant *()
            { return static_cast< _Plant * >( *this->m_it); }

        _Plant *  operator*()
            { return static_cast< _Plant * >( *this->m_it); }
        _Plant const *  operator*() const
            { return static_cast< _Plant const * >( *this->m_it); }
        typename _Plant::Parameters const *  operator->() const
            { return static_cast< _Plant const * >(*this->m_it)->parameters(); }

        bool  operator==( GroupIterator< _G1, _G2, _G3, _G4 > const &  _rhs) const
            { return (this->m_it == _rhs.m_it) && (this->m_end == _rhs.m_end); }
        bool  operator!=( GroupIterator< _G1, _G2, _G3, _G4 > const &  _rhs) const
            { return !this->operator==( _rhs); }

    private:
        Iterator  m_it, m_end;
    };
    template < typename _G1 >
    GroupIterator< _G1, species::none, species::none, species::none >  groupbegin()
        { return  GroupIterator< _G1, species::none, species::none, species::none >( this->begin(), this->end()); }
    template < typename _G1 >
    GroupIterator< _G1, species::none, species::none, species::none >  groupend()
        { return  GroupIterator< _G1, species::none, species::none, species::none >( this->end(), this->end()); }
    template < typename _G1, typename _G2 >
    GroupIterator< _G1, _G2, species::none, species::none >  groupbegin()
        { return  GroupIterator< _G1, _G2, species::none, species::none >( this->begin(), this->end()); }
    template < typename _G1, typename _G2 >
    GroupIterator< _G1, _G2, species::none, species::none >  groupend()
        { return  GroupIterator< _G1, _G2, species::none, species::none >( this->end(), this->end()); }
    template < typename _G1, typename _G2, typename _G3 >
    GroupIterator< _G1, _G2, _G3, species::none >  groupbegin()
        { return  GroupIterator< _G1, _G2, _G3, species::none >( this->begin(), this->end()); }
    template < typename _G1, typename _G2, typename _G3 >
    GroupIterator< _G1, _G2, _G3, species::none >  groupend()
        { return  GroupIterator< _G1, _G2, _G3, species::none >( this->end(), this->end()); }
    template < typename _G1, typename _G2, typename _G3, typename _G4 >
    GroupIterator< _G1, _G2, _G3, _G4 >  groupbegin()
        { return  GroupIterator< _G1, _G2, _G3, _G4 >( this->begin(), this->end()); }
    template < typename _G1, typename _G2, typename _G3, typename _G4 >
    GroupIterator< _G1, _G2, _G3, _G4 >  groupend()
        { return  GroupIterator< _G1, _G2, _G3, _G4 >( this->end(), this->end()); }

private:
    CBM_Set  m_plants;
    LD_PlantsParametersDB const *  m_parameters_database;
};

} /* namespace ldndc */

template < typename _Plant >
ldndc::LD_PlantVegetation< _Plant >::LD_PlantVegetation(
                LD_PlantsParametersDB const *  _parameters_database)
{
    this->m_plants = CBM_SetCreate( CBM_MemPoolRoot,
        sizeof(_Plant), LD_PlantMemoryReserveCount, 0);
    this->m_parameters_database = _parameters_database;
}
template < typename _Plant >
ldndc::LD_PlantVegetation< _Plant >::~LD_PlantVegetation< _Plant >()
{
    Iterator  pi = this->begin();
    while ( pi )
    {
        this->delete_plant( (*pi)->cname());
        ++pi;
    }
    CBM_SetDestroy( this->m_plants);
}

template < typename _Plant >
_Plant *  ldndc::LD_PlantVegetation< _Plant >::new_plant(
        typename _Plant::Settings const *  _plantsettings)
{
    if ( this->have_plant( _plantsettings->name)) /*treat as error*/
        { return  NULL; }

    _Plant *  plant = (_Plant *)CBM_SetInsert(
        this->m_plants, _plantsettings->name.c_str(), NULL);
    if ( plant)
    {
        new ( plant) _Plant( _plantsettings);
        plant->state = _Plant::seeded;
    }
    return  plant;
}

template < typename _Plant >
lerr_t  ldndc::LD_PlantVegetation< _Plant >::delete_plant(
        char const *  _name)
{
	std::string plantname = _name;
    _Plant *  plant = this->get_plant( _name);
    if ( plant)
        { plant->~_Plant(); }
    CBM_SetRemove( this->m_plants, plantname.c_str());

    return  LDNDC_ERR_OK;
}

template < typename _Plant >
size_t  ldndc::LD_PlantVegetation< _Plant >::size_by_group( species_group_e  _group)
{
    size_t  nb_plants = 0;
    for ( Iterator  pi = this->begin(); pi != this->end(); ++pi)
    {
        if ((*pi)->groupId() == _group)
            { nb_plants += 1; }
    }
    return  nb_plants;
}

template < typename _Plant >
bool  ldndc::LD_PlantVegetation< _Plant >::have_plant(
        char const *  _name)
{
    return  this->get_plant( _name) != NULL;
}
template < typename _Plant >
_Plant *  ldndc::LD_PlantVegetation< _Plant >::get_plant(
        char const *  _name)
{
    CBM_SetNodeIterator  pi = CBM_SetFind( this->m_plants, _name);
    if ( !CBM_SetNodeIteratorIsEnd( pi))
        { return (_Plant *)pi.value; }
    return  NULL;
}


template < typename _Plant >
ldndc::LD_PlantVegetation< _Plant >::Iterator::Iterator()
    : m_it( CBM_SetNodeIteratorEnd)
{ }
template < typename _Plant >
ldndc::LD_PlantVegetation< _Plant >::Iterator::Iterator( CBM_Set &  _plants)
    : m_it( CBM_SetFindHead( _plants))
{ }
template < typename _Plant >
typename ldndc::LD_PlantVegetation< _Plant >::Iterator &
ldndc::LD_PlantVegetation< _Plant >::Iterator::next()
{
    if ( !CBM_SetNodeIteratorIsEnd( this->m_it))
        { this->m_it = CBM_SetFindNext( this->m_it); }
    return  *this;
}


#endif /* !LD_PLANTS_H_ */

