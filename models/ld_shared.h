/*!
 * @brief
 *    Convenience classes for sharing state variables
 *
 * @author
 *    steffen klatt (created on: mar 30, 2017)
 */

#ifndef  LD_SHARED_H_
#define  LD_SHARED_H_

#include  "ldndc.h"
#include  <kernel/cbm_kernel.h>
#include  <string/cbm_string.h>
#include  <utils/cbm_utils.h>
#include  <containers/cbm_vector.h>

#include  <memory>
#include  <exception>

#define  LD_PortOk               0
#define  LD_PortIsMasked         1
#define  LD_PortIsSubscribed     -21
#define  LD_PortIsNotSubscribed  -22
#define  LD_PortReceiveFailure   -23
#define  LD_PortIsPublished      -31
#define  LD_PortIsNotPublished   -32
#define  LD_PortSendFailure      -33

namespace ldndc {

struct LD_TransformNone
{
    static CBM_Transform * Transform()
        { return NULL; }
};
struct LD_TransformDotSum
{
    static CBM_Transform * Transform()
        { return CBM_TransformDotSum; }
};
struct LD_TransformSum
{
    static CBM_Transform * Transform()
        { return CBM_TransformSum; }
};



template < typename _Data >
class SharedField
{
protected:
    cbm::io_kcomm_t *  m_iokcomm;
    _Data  m_value;
    CBM_Handle  m_handle;
public:
    SharedField<_Data>()
            : m_iokcomm( NULL),
                m_value( _Data()), m_handle( CBM_None),
        prev( NULL), next( NULL)
    {
        this->attr.error = 0;
        this->attr.is_masked = 0;
    }
    struct Attributes
    {
        bool error:1;
        bool is_masked:1;
    } attr;

    char const * uri() const
        { return this->m_iokcomm->port_uri( this->m_handle); }
    char const * unit( char * _buf, size_t _buf_sz) const
        { return this->m_iokcomm->port_unit( this->m_handle, _buf, _buf_sz); }

protected:
    ~SharedField<_Data>()
        { this->m_refdel(); }
    SharedField<_Data>( SharedField<_Data> const & _rhs)
            : m_iokcomm( _rhs.m_iokcomm), m_value( _rhs.m_value),
                m_handle( _rhs.m_handle),
                    prev( NULL), next( NULL)
        {
            this->m_refadd( _rhs);
            
            this->attr.error = _rhs.attr.error;
            this->attr.is_masked = _rhs.attr.is_masked;
        }

    SharedField<_Data> & operator=( SharedField<_Data> const & _rhs)
    {
        if ( this != &_rhs)
        {
            this->m_refdel();

            this->m_iokcomm = _rhs.m_iokcomm;
            this->m_value = _rhs.m_value;
            this->m_handle = _rhs.m_handle;

            this->attr.error = _rhs.attr.error;
            this->attr.is_masked = _rhs.attr.is_masked;
            
            this->m_refadd( _rhs);
        }
        return *this;
    }

    void refdec()
        { this->m_refdec(); }
    void refinc()
        { this->m_refinc(); }
    bool refexist() const
        { return this->m_refexist(); }

private:
    void m_refdel()
    {
        /* drop from references list */
        if ( this->prev)
            { this->prev->next = this->next; }
        if ( this->next)
            { this->next->prev = this->prev; }
        this->prev = NULL;
        this->next = NULL;
    }
    void m_refadd( SharedField<_Data> const & _rhs)
    {
        /* insert into references list */
        _rhs.prev = this->prev;
        this->prev = const_cast<SharedField<_Data> *>(&_rhs);
        if ( _rhs.prev)
            { _rhs.prev->next = this->prev; }
        _rhs.next = this;
    }
    void m_refdec()
    {
        if ( this->m_refexist())
            { this->m_handle = CBM_None; }
    }
    void m_refinc()
    {
        SharedField<_Data>  * field = NULL;
        if ( this->next)
            { field = this->next; }
        else if ( this->prev)
            { field = this->prev; }

        if ( field)
        {
            this->m_handle = field->m_handle;
            this->m_iokcomm = field->m_iokcomm;
            this->m_value = field->m_value;
        }
    }
    bool m_refexist() const
    {
        SharedField<_Data>  * field = NULL;

        field = this->prev;
        while ( field)
        {
            if ( CBM_HandleOk(field->m_handle))
                { return true; }
            field = field->prev;
        }
        field = this->next;
        while ( field)
        {
            if ( CBM_HandleOk(field->m_handle))
                { return true; }
            field = field->next;
        }
        return false;
    }

    /* references list, keeping track of
     * shared publications/subscriptions */
    mutable SharedField<_Data>  * prev, * next;
};

template < typename _Data, typename _Transform = LD_TransformNone, int _Flags = 0 >
struct SubscribedField : public SharedField<_Data>
{
    SubscribedField<_Data,_Transform,_Flags>() : SharedField<_Data>() { }
    SubscribedField<_Data,_Transform,_Flags>(
        SubscribedField<_Data,_Transform,_Flags> const & _rhs)
            : SharedField<_Data>( _rhs)
    {
        if ( _rhs.is_subscribed())
            { this->subscribe( NULL, NULL, _rhs.m_iokcomm); }
    }
    SubscribedField<_Data,_Transform,_Flags> &  operator=(
        SubscribedField<_Data,_Transform,_Flags> const & _rhs)
    {
        if ( this != &_rhs)
        {
            if ( this->is_subscribed())
            {
                throw std::runtime_error(
                        "Cannot call operator= on subscribed SubscribedField" );
            }
            SharedField<_Data>::operator=( _rhs);
        }
        return *this;
    }
    ~SubscribedField<_Data,_Transform,_Flags>()
    {
        if ( this->is_subscribed())
            { this->unsubscribe(); }
    }

    bool  is_subscribed() const
        { return  CBM_HandleOk( this->m_handle); }
    operator  bool() const
        { return this->is_subscribed(); }


    int  subscribe( char const * _name, char const * _unit,
            cbm::io_kcomm_t * _iokcomm)
    {
        this->m_iokcomm = _iokcomm;
        if ( !this->refexist())
        {
            this->m_handle =
                this->m_iokcomm->subscribe_port(
                    _name, _unit, _Transform::Transform(), _Flags);
        }
        this->refinc();

        return LD_PortOk;
    }
    int  subscribe( char const * _name, char const * _unit, lid_t const & _part_id,
            cbm::io_kcomm_t * _iokcomm)
    {
        this->m_iokcomm = _iokcomm;
        if ( !this->refexist())
        {
            this->m_handle =
                this->m_iokcomm->subscribe_port(
                    _name, _unit, _part_id, _Transform::Transform(), _Flags);
        }
        this->refinc();

        return LD_PortOk;
    }
    int  subscribe( cbm::string_t const & _name, char const * _unit,
            cbm::io_kcomm_t * _iokcomm)
        { return this->subscribe( _name.c_str(), _unit, _iokcomm); }

    int  unsubscribe()
    {
        if ( !this->is_subscribed())
            { return LD_PortIsNotSubscribed; }

        this->refdec();
        if ( !this->refexist())
            { this->m_iokcomm->unsubscribe_port( this->m_handle); }
        this->m_handle = CBM_None;
        this->m_iokcomm = NULL;

        return LD_PortOk;
    }

    _Data const &  receive()
    {
// TODO  check if receive is needed (timestamp)
        size_t const sz = this->m_iokcomm->recv_port(
                this->m_handle, &this->m_value, 1);
        if ( sz==invalid_size)
        {
// sk:off            CBM_LogError( "Receive failure  ",
// sk:off                "[uri=",this->m_iokcomm->port_uri( this->m_handle),"]");
            this->attr.error = 1;
            this->m_value = invalid_t<_Data>::value;
        }
        return this->m_value;
    }

    _Data const &  receive( _Data const & _default)
    {
        size_t const sz = this->m_iokcomm->recv_port(
                this->m_handle, &this->m_value, 1);
        if ( sz==invalid_size)
            { this->m_value = _default; }
        return this->m_value;
    }
};

template < typename _Data, int _Flags = 0 >
struct PublishedField : public SharedField<_Data>
{
    PublishedField<_Data,_Flags>() : SharedField<_Data>() { }
    PublishedField<_Data,_Flags>( PublishedField<_Data,_Flags> const & _rhs)
            : SharedField<_Data>( _rhs)
    {
        if ( _rhs.is_published())
            { this->publish( NULL, NULL, _rhs.m_iokcomm); }
    }
    PublishedField<_Data,_Flags> &  operator=(
            PublishedField<_Data,_Flags> const & _rhs)
    {
        if ( this != &_rhs)
        {
            if ( this->is_published())
            {
                throw std::runtime_error(
                        "Cannot call operator= on published PublishedField" );
            }
            SharedField<_Data>::operator=( _rhs);
        }
        return *this;
    }
    ~PublishedField<_Data,_Flags>()
    {
        if ( this->is_published())
            { this->unpublish(); }
    }

    bool  is_published() const
        { return  CBM_HandleOk( this->m_handle); }
    operator  bool() const
        { return this->is_published(); }


    int  publish( char const * _name, char const * _unit,
            cbm::io_kcomm_t * _iokcomm)
    {
        if ( this->attr.is_masked)
            { return LD_PortIsMasked; }

        this->m_iokcomm = _iokcomm;
        if ( !this->refexist())
        {
            this->m_handle = this->m_iokcomm->publish_port(
                    _name, _unit, &this->m_value, 1, _Flags);
        }
        this->refinc();

        return LD_PortOk;
    }
    int  publish( cbm::string_t const & _name, char const * _unit,
            cbm::io_kcomm_t * _iokcomm)
    { return this->publish( _name.c_str(), _unit, _iokcomm); }

    int  publish( char const * _name, char const * _unit,
             lid_t const & _part_id, cbm::io_kcomm_t * _iokcomm)
    {
        if ( this->attr.is_masked)
            { return LD_PortIsMasked; }

        this->m_iokcomm = _iokcomm;
        if ( !this->refexist())
        {
            this->m_handle = this->m_iokcomm->publish_port(
                    _name, _unit, _part_id, &this->m_value, 1, _Flags);
        }
        this->refinc();

        return LD_PortOk;
    }
    int  publish( cbm::string_t const & _name, char const * _unit,
             lid_t const & _part_id, cbm::io_kcomm_t * _iokcomm)
        { return this->publish( _name.c_str(), _unit, _part_id, _iokcomm); }
    int  unpublish()
    {
        if ( !this->is_published())
            { return LD_PortIsNotPublished; }
        if ( this->attr.is_masked)
            { return LD_PortIsMasked; }

        this->refdec();
        if ( !this->refexist())
            { this->m_iokcomm->unpublish_port( this->m_handle); }
        this->m_handle = CBM_None;
        this->m_iokcomm = NULL;

        return LD_PortOk;
    }

    int  send( _Data const & _rhs)
    {
        if ( this->attr.is_masked)
            { return LD_PortIsMasked; }

// TODO  check if send is needed (timestamp)
        this->m_value = _rhs;
        size_t const  sz = this->m_iokcomm->send_port(
                this->m_handle, &this->m_value, 1);
        if ( sz==invalid_size)
        {
            CBM_LogError( "Send failure  ",
                "[uri=",this->m_iokcomm->port_uri( this->m_handle),"]");
            this->attr.error = 1;
            return LD_PortSendFailure;
        }
        return LD_PortOk;
    }
};

template < typename _Data, typename _Transform = LD_TransformNone, int _Flags = 0 >
class LDNDC_API PublishedAndSubscribedField
{
public:
    operator  bool() const
    {
        return this->m_sub.is_subscribed() && this->m_pub.is_published();
    }
    
    int  publish_and_subscribe( char const * _name,
                                char const * _unit,
                                cbm::io_kcomm_t * _iokcomm)
    {
        int rc_pub = this->m_pub.publish( _name, _unit, _iokcomm);
        if ( rc_pub != LD_PortOk){ return LD_PortIsNotPublished; }
        
        int rc_sub = this->m_sub.subscribe( _name, _unit, _iokcomm);
        if ( rc_sub != LD_PortOk){ return LD_PortIsNotSubscribed; }

        return LD_PortOk;
    }
    
    int  publish_and_subscribe( char const * _name,
                                char const * _unit,
                                lid_t const & _part_id,
                                cbm::io_kcomm_t * _iokcomm)
    {
        int rc_pub = this->m_pub.publish( _name, _unit, _part_id, _iokcomm);
        if ( rc_pub != LD_PortOk){ return LD_PortIsNotPublished; }
        
        int rc_sub = this->m_sub.subscribe( _name, _unit, _part_id, _iokcomm);
        if ( rc_sub != LD_PortOk){ return LD_PortIsNotSubscribed; }

        return LD_PortOk;
    }
    
    int  unpublish_and_unsubscribe()
    {
        int rc_unsub = this->m_sub.unsubscribe();
        int rc_unpub = this->m_pub.unpublish();
        if ( rc_unsub != LD_PortOk){ return LD_PortIsNotSubscribed; }
        if ( rc_unpub != LD_PortOk){ return LD_PortIsNotPublished; }
        return LD_PortOk;
    }
    
    _Data const &  receive()
    { return this->m_sub.receive(); }

    _Data const &  receive( _Data const & _default)
    { return this->m_sub.receive( _default); }

    int  send( _Data const & _rhs)
    { return this->m_pub.send( _rhs); }

    size_t  size() const
    { return this->m_pub.size(); }
    
private:
    PublishedField<_Data,_Flags>  m_pub;
    SubscribedField<_Data,_Transform,_Flags>  m_sub;
};









template < typename _Data >
class SharedVectorField : public SharedField<size_t>
{
protected:
    size_t & m_vfsize; /*we use base class' value member :-| */
public:
    SharedVectorField<_Data>()
        : SharedField<size_t>(), m_vfsize( this->m_value)
        { }
    SharedVectorField<_Data>( SharedVectorField<_Data> const & _rhs)
            : SharedField<size_t>( _rhs), m_vfsize( this->m_value)
    {
        this->m_value = _rhs.m_value;
    }
    SharedVectorField<_Data> &  operator=(
            SharedVectorField<_Data> const & _rhs)
    {
        if ( this != &_rhs)
        {
            SharedField<size_t>::operator=( _rhs);
            this->m_value = _rhs.m_value;
        }
        return *this;
    }

    size_t  size() const
        { return this->m_vfsize; }
};

template < typename _Data, typename _Transform = LD_TransformNone, int _Flags = 0 >
struct SubscribedVectorField : public SharedVectorField<_Data>
{
    SubscribedVectorField<_Data,_Transform,_Flags>() : SharedVectorField<_Data>() { }
    SubscribedVectorField<_Data,_Transform,_Flags>(
        SubscribedVectorField<_Data,_Transform,_Flags> const & _rhs)
            : SharedVectorField<_Data>( _rhs)
    {
        if ( _rhs.is_subscribed())
            { this->subscribe( NULL, NULL, _rhs.m_vfsize, _rhs.m_iokcomm); }
    }
    SubscribedVectorField<_Data,_Transform,_Flags> &  operator=(
            SubscribedVectorField<_Data,_Transform,_Flags> const & _rhs)
    {
        if ( this != &_rhs)
        {
            if ( this->is_subscribed())
            {
                throw std::runtime_error(
                        "Cannot call operator= on subscribed SubscribedVectorField" );
            }
            SharedVectorField<_Data>::operator=( _rhs);
        }
        return *this;
    }
    ~SubscribedVectorField<_Data,_Transform,_Flags>()
    {
        if ( this->is_subscribed())
            { this->unsubscribe(); }
    }

    bool  is_subscribed() const
        { return CBM_HandleOk( this->m_handle); }
    operator  bool() const
        { return this->is_subscribed(); }

    int  subscribe( char const * _name, char const * _unit,
            size_t _vfsize, cbm::io_kcomm_t * _iokcomm)
    {
        this->m_iokcomm = _iokcomm;
        this->m_vfsize = _vfsize;
        if ( !this->refexist())
        {
            this->m_handle =
                this->m_iokcomm->subscribe_port(
                    _name, _unit, _Transform::Transform(), _Flags);
        }
        this->refinc();

        return LD_PortOk;
    }
    int  subscribe( cbm::string_t const & _name, char const * _unit,
            size_t _vfsize, cbm::io_kcomm_t * _iokcomm)
    { return this->subscribe( _name.c_str(), _unit, _vfsize, _iokcomm); }

    int  subscribe( char const * _name, char const * _unit,
           size_t _vfsize, lid_t const & _part_id, cbm::io_kcomm_t * _iokcomm)
    {
        this->m_iokcomm = _iokcomm;
        this->m_vfsize = _vfsize;
        if ( !this->refexist())
        {
            this->m_handle =
                this->m_iokcomm->subscribe_port(
                    _name, _unit, _part_id, _Transform::Transform(), _Flags);
        }
        this->refinc();

        return LD_PortOk;
    }
    int  subscribe( cbm::string_t const & _name, char const * _unit,
            size_t _vfsize, lid_t const & _part_id, cbm::io_kcomm_t * _iokcomm)
        { return this->subscribe( _name.c_str(), _unit, _vfsize, _part_id, _iokcomm); }

    int  unsubscribe()
    {
        if ( !this->is_subscribed())
            { return LD_PortIsNotSubscribed; }

        this->refdec();
        if ( !this->refexist())
            { this->m_iokcomm->unsubscribe_port( this->m_handle); }
        this->m_handle = CBM_None;
        this->m_vfsize = 0;
        this->m_iokcomm = NULL;

        return LD_PortOk;
    }

    int  receive( _Data * _buffer)
    {
// TODO  check if receive is needed (timestamp)
        size_t const sz = this->m_iokcomm->recv_port(
                    this->m_handle, _buffer, this->m_vfsize);
        if ( sz==invalid_size)
        {
// sk:off            { CBM_LogError( "Receive failure  ",
// sk:off                "[uri=",this->m_iokcomm->port_uri( this->m_handle),"]");
            this->attr.error = 1;
            return LD_PortReceiveFailure;
        }
        return LD_PortOk;
    }
    int  receive( CBM_Vector<_Data> & _buffer)
        { return this->receive( &_buffer[0]); }
    int  receive( _Data * _buffer, _Data _default)
    {
        size_t const sz = this->m_iokcomm->recv_port(
                    this->m_handle, _buffer, this->m_vfsize);
        if ( sz==invalid_size)
            { cbm::mem_set( _buffer, this->m_vfsize, _default); }
        return LD_PortOk;
    }
    int  receive( CBM_Vector<_Data> & _buffer, _Data _default)
        { return this->receive( &_buffer[0], _default); }
    int  receive( _Data * _buffer, _Data const * _defaults)
    {
        if ( _buffer == _defaults)
            { return this->m_vfsize; }
        size_t const sz = this->m_iokcomm->recv_port(
                    this->m_handle, _buffer, this->m_vfsize);
        if ( sz==invalid_size)
            { cbm::mem_cpy( _buffer, _defaults, this->m_vfsize); }
        return LD_PortOk;
    }
    int  receive( CBM_Vector<_Data> & _buffer, _Data const *  _defaults)
        { return this->receive( &_buffer[0], _defaults); }
    int  receive( CBM_Vector<_Data> & _buffer,
                    CBM_Vector<_Data> const &  _defaults)
        { return this->receive( &_buffer[0], &_defaults[0]); }
};

template < typename _Data, int _Flags = 0 >
struct PublishedVectorField : public SharedVectorField<_Data>
{
    PublishedVectorField<_Data,_Flags>() : SharedVectorField<_Data>() { }
    PublishedVectorField<_Data,_Flags>( PublishedVectorField<_Data,_Flags> const & _rhs)
            : SharedVectorField<_Data>( _rhs)
    {
        if ( _rhs.is_published())
            { this->publish( NULL, NULL, _rhs.m_vfsize, _rhs.m_iokcomm); }
    }
    PublishedVectorField<_Data,_Flags> &  operator=(
            PublishedVectorField<_Data,_Flags> const & _rhs)
    {
        if ( this != &_rhs)
        {
            if ( this->is_published())
            {
                throw std::runtime_error(
                        "Cannot call operator= on published PublishedVectorField" );
            }
            SharedVectorField<_Data>::operator=( _rhs);
        }
        return *this;
    }
    ~PublishedVectorField<_Data,_Flags>()
    {
        if ( this->is_published())
            { this->unpublish(); }
    }

    bool  is_published() const
        { return CBM_HandleOk( this->m_handle); }
    operator  bool() const
        { return this->is_published(); }

    int  publish( char const * _name, char const * _unit,
            size_t _vfsize, cbm::io_kcomm_t * _iokcomm)
    {
        if ( this->attr.is_masked)
            { return LD_PortIsMasked; }

        this->m_iokcomm = _iokcomm;
        this->m_vfsize = _vfsize;
        if ( !this->refexist())
        {
            _Data * const  no_buffer = NULL;
            this->m_handle = this->m_iokcomm->publish_port(
                _name, _unit, no_buffer, this->m_vfsize, _Flags);
        }
        this->refinc();

        return LD_PortOk;
    }
    int  publish( cbm::string_t const & _name, char const * _unit,
                 size_t _vfsize, cbm::io_kcomm_t * _iokcomm)
    { return this->publish( _name.c_str(), _unit, _vfsize, _iokcomm); }

    int  publish( char const * _name, char const * _unit,
             size_t _vfsize, lid_t const & _part_id,
             cbm::io_kcomm_t * _iokcomm)
    {
        if ( this->attr.is_masked)
        { return LD_PortIsMasked; }

        this->m_iokcomm = _iokcomm;
        this->m_vfsize = _vfsize;
        if ( !this->refexist())
        {
            _Data * const  no_buffer = NULL;
            this->m_handle = this->m_iokcomm->publish_port(
               _name, _unit, _part_id, no_buffer, this->m_vfsize, _Flags);
        }
        this->refinc();

        return LD_PortOk;
    }
    int  publish( cbm::string_t const & _name, char const * _unit,
                 size_t _vfsize, lid_t const & _part_id, cbm::io_kcomm_t * _iokcomm)
        { return this->publish( _name.c_str(), _unit, _part_id, _vfsize, _iokcomm); }

    int  unpublish()
    {
        if ( !this->is_published())
            { return LD_PortIsNotPublished; }
        if ( this->attr.is_masked)
            { return LD_PortIsMasked; }

        this->refdec();
        if ( !this->refexist())
            { this->m_iokcomm->unpublish_port( this->m_handle); }
        this->m_handle = CBM_None;
        this->m_vfsize = 0;
        this->m_iokcomm = NULL;

        return LD_PortOk;
    }

    int  send( _Data const * _rhs)
    {
        if ( this->attr.is_masked)
            { return LD_PortIsMasked; }

// TODO  check if send is needed (timestamp)
        size_t const  sz = this->m_iokcomm->send_port(
                this->m_handle, _rhs, this->m_vfsize);
        if ( sz==invalid_size)
        {
            CBM_LogError( "Send failure  ",
                "[uri=",this->m_iokcomm->port_uri( this->m_handle),"]");
            this->attr.error = 1;
            return LD_PortSendFailure;
        }
        return LD_PortOk;
    }
    size_t  send( CBM_Vector<_Data> const & _rhs)
        { return this->send( &_rhs[0]); }
};


class LDNDC_API SubscribeHelper
{
public:
    SubscribeHelper() : iokcomm( NULL)
        { }
    SubscribeHelper( cbm::io_kcomm_t * _iokcomm)
            : iokcomm( _iokcomm)
        { }

    template < typename _SubscribedField >
    int  subscribe( _SubscribedField & _field,
            char const * _name, char const * _unit)
        { return _field.subscribe( _name, _unit, this->iokcomm); }
    template < typename _SubscribedVectorField >
    int  subscribe( _SubscribedVectorField & _vfield,
            char const * _name, char const * _unit, size_t _vfsize)
        { return _vfield.subscribe( _name, _unit, _vfsize, this->iokcomm); }
private:
    cbm::io_kcomm_t * iokcomm;
};

class LDNDC_API PublishHelper
{
public:
    PublishHelper() : iokcomm( NULL)
        { }
    PublishHelper( cbm::io_kcomm_t * _iokcomm,
        cbm::string_array_t const & _mask = cbm::string_array_t())
            : iokcomm( _iokcomm), mask( _mask)
        { }

    template < typename _PublishedField >
    int  publish( _PublishedField & _field,
            char const * _name, char const * _unit)
    {
        if ( !this->mask.contains( _name))
            { return _field.publish( _name, _unit, this->iokcomm); }
        _field.attr.is_masked = 1;
        return LD_PortIsMasked;
    }
    template < typename _PublishedVectorField >
    int  publish( _PublishedVectorField & _vfield,
            char const * _name, char const * _unit, size_t _vfsize)
    {
        if ( !this->mask.contains( _name))
            { return _vfield.publish( _name, _unit, _vfsize, this->iokcomm); }
        _vfield.attr.is_masked = 1;
        return LD_PortIsMasked;
    }
private:
    cbm::io_kcomm_t * iokcomm;
    cbm::string_array_t const  mask;
};


template < typename _Data, typename _Transform = LD_TransformNone, int _Flags = 0 >
class LDNDC_API PublishedAndSubscribedVectorField
{
public:
    operator  bool() const
        { return this->m_sub.is_subscribed()
                && this->m_pub.is_published(); }

    int  publish_and_subscribe( char const * _name,
            char const * _unit, size_t _vfsize,
                cbm::io_kcomm_t * _iokcomm)
    {
        int rc_pub = this->m_pub.publish( _name, _unit, _vfsize, _iokcomm);
        if ( rc_pub != LD_PortOk)
            { return LD_PortIsNotPublished; }
        int rc_sub = this->m_sub.subscribe( _name, _unit, _vfsize, _iokcomm);
        if ( rc_sub != LD_PortOk)
            { return LD_PortIsNotSubscribed; }

        return LD_PortOk;
    }
    
    int  publish_and_subscribe( char const * _name,
            char const * _unit, size_t _vfsize, lid_t const & _part_id,
                cbm::io_kcomm_t * _iokcomm)
    {
        int rc_pub = this->m_pub.publish( _name, _unit, _vfsize, _part_id, _iokcomm);
        if ( rc_pub != LD_PortOk)
            { return LD_PortIsNotPublished; }
        int rc_sub = this->m_sub.subscribe( _name, _unit, _vfsize, _part_id, _iokcomm);
        if ( rc_sub != LD_PortOk)
            { return LD_PortIsNotSubscribed; }

        return LD_PortOk;
    }

    int  unpublish_and_unsubscribe()
    {
        int rc_unsub = this->m_sub.unsubscribe();
        int rc_unpub = this->m_pub.unpublish();
        if ( rc_unsub != LD_PortOk)
            { return LD_PortIsNotSubscribed; }
        if ( rc_unpub != LD_PortOk)
            { return LD_PortIsNotPublished; }
        return LD_PortOk;
    }

    int  receive( _Data * _buffer)
        { return this->m_sub.receive( _buffer); }
    int  receive( CBM_Vector<_Data> & _buffer)
        { return this->m_sub.receive( _buffer); }
    int  receive( _Data * _buffer, _Data _default)
        { return this->m_sub.receive( _buffer, _default); }
    int  receive( CBM_Vector<_Data> & _buffer, _Data _default)
        { return this->m_sub.receive( _buffer, _default); }
    int  receive( _Data * _buffer, _Data const * _default)
        { return this->m_sub.receive( _buffer, _default); }
    int  receive( CBM_Vector<_Data> & _buffer, _Data const * _default)
        { return this->m_sub.receive( _buffer, _default); }
    int  receive( CBM_Vector<_Data> & _buffer, CBM_Vector<_Data> const & _default)
        { return this->m_sub.receive( _buffer, _default); }

    int  send( _Data const * _rhs)
        { return this->m_pub.send( _rhs); }
    int  send( CBM_Vector<_Data> const & _rhs)
        { return this->m_pub.send( _rhs); }

    size_t  size() const
        { return this->m_pub.size(); }

private:
    PublishedVectorField<_Data,_Flags>  m_pub;
    SubscribedVectorField<_Data,_Transform,_Flags>  m_sub;
};

} /* namespace ldndc */

#include  "ld_eventqueue.h"
namespace ldndc {

struct LDNDC_API EventHandler
{
    virtual  ~EventHandler()
        { }
    virtual int  callback( char const * /*message*/,
                    size_t /*message size*/) = 0;

    void  name( char const * _eventname)
        { this->m_name = _eventname ? _eventname : ""; }
    char const *  name() const
        { return ( this->m_name.length()==0) ? NULL : this->m_name.c_str(); }
private:
    cbm::string_t  m_name;
};
} /* namespace ldndc */

static inline
int EventHandlerFn( void const * _msg, size_t _msg_sz, void * _eventhandler)
{
    ldndc::EventHandler * eventhandler =
        static_cast< ldndc::EventHandler * >( _eventhandler);
    if ( !_eventhandler)
        { return -1; }
    return eventhandler->callback((char const *)_msg, _msg_sz);
}

namespace ldndc {

struct LD_EventHandlerQueue : public EventHandler
{
    LD_EventHandlerQueue()
            : EventHandler()
        { }
    int  callback( char const * _msg, size_t _msg_sz)
    {
        EventAttributes event_attributes( this->name(), _msg, _msg_sz);
        this->queue.push( event_attributes);
        return LD_PortOk;
    }

    struct Interface
    {
        EventAttributes const *  pop()
        {
            if ( this->m_queue->is_empty())
                { return NULL; }
            this->m_head = this->m_queue->pop();
            return &this->m_head;
        }
        void  clear()
            { this->m_queue->clear(); }
        bool  is_filled() const
            { return !this->m_queue->is_empty(); }

    protected:
        Interface() : m_queue( NULL)
            { }
        void  set_handlerdata( LD_EventHandlerQueue & _handler)
            { this->m_queue = &_handler.queue; }
    private:
        EventQueue * m_queue;
        EventAttributes m_head;
    };

    EventQueue queue;
};
template < typename _Type >
struct LD_EventHandlerCall : public EventHandler
{
    LD_EventHandlerCall<_Type>()
            : EventHandler(), obj( NULL)
        { }
    int  callback( char const * _msg, size_t _msg_sz)
    {
        EventAttributes event_attributes( this->name(), _msg, _msg_sz);
        int  rc_call = (this->obj->*fn)( &event_attributes);
        if ( rc_call)
            { return LD_PortReceiveFailure; }
        return LD_PortOk;
    }

    struct Interface
    {
        void  handler( int (_Type::*_fn)( EventAttributes const *), _Type * _obj)
        {
            this->m_handler->obj = _obj;
            this->m_handler->fn = _fn;
        }
    protected:
        Interface() : m_handler( NULL)
            { }
        void  set_handlerdata( LD_EventHandlerCall<_Type> & _handler)
            { this->m_handler = &_handler; }
    private:
        LD_EventHandlerCall<_Type> * m_handler;
    };
    _Type * obj;
    int (_Type::*fn)( EventAttributes const *);
};

template < typename _EventHandler >
struct LDNDC_API SubscribedEvent : public _EventHandler::Interface
{
private:
    CBM_Handle  m_handle;
    cbm::io_kcomm_t *  m_iokcomm;
public:
    SubscribedEvent<_EventHandler>()
                : _EventHandler::Interface(),
            m_handle( CBM_None), m_iokcomm( NULL)
        { this->set_handlerdata( this->m_eventhandler); }

    bool  is_subscribed() const
        { return CBM_HandleOk( this->m_handle); }

    int  subscribe( char const * _eventname, cbm::io_kcomm_t * _iokcomm)
    {
        if ( CBM_HandleOk(this->m_handle))
            { return LD_PortIsSubscribed; }

        this->m_iokcomm = _iokcomm;
        this->m_eventhandler.name( _eventname);

        CBM_Callback  cb_event;
        cb_event.fn = &EventHandlerFn;
        cb_event.data = &this->m_eventhandler;
        this->m_handle = this->m_iokcomm->subscribe_event( _eventname, cb_event);
        if ( !CBM_HandleOk(this->m_handle))
            { return LD_PortIsNotSubscribed; }
        return LD_PortOk;
    }
    int  unsubscribe()
    {
        if ( !this->is_subscribed())
            { return LD_PortIsNotSubscribed; }

        this->m_iokcomm->unsubscribe_event( this->m_handle);
        this->m_handle = CBM_None;
        this->m_iokcomm = NULL;

        return LD_PortOk;
    }

private:
    _EventHandler m_eventhandler;

private:
    SubscribedEvent<_EventHandler>( SubscribedEvent<_EventHandler> const &);
    SubscribedEvent<_EventHandler> &  operator=( SubscribedEvent<_EventHandler> const &);
};
struct LDNDC_API PublishedEvent
{
private:
    CBM_Handle  m_handle;
    cbm::io_kcomm_t *  m_iokcomm;
public:
    struct EventAttributes
    {
        template < typename _T >
        EventAttributes &  set( char const * _path, _T const & _value)
        {
            if ( !_path || *_path=='\0')
                { /* no op */ }
            else
                { this->m_attributes.put< _T >( _path, _value); }
            return *this;
        }
        char const *  serialize( size_t * _data_sz) const
            { return this->m_attributes.serialize( _data_sz); }
    private:
        EventAttributesCollector m_attributes;
    };

    PublishedEvent()
            : m_handle( CBM_None), m_iokcomm( NULL)
        { }

    bool  is_published() const
        { return CBM_HandleOk( this->m_handle); }

    int  publish( char const * _eventname, cbm::io_kcomm_t * _iokcomm)
    {
// TODO  masking?!
        if ( CBM_HandleOk(this->m_handle))
            { return LD_PortIsPublished; }

        this->m_iokcomm = _iokcomm;
        this->m_handle = this->m_iokcomm->publish_event( _eventname);
        if ( !CBM_HandleOk(this->m_handle))
            { return LD_PortIsNotPublished; }

        return LD_PortOk;
    }

    int  publish( char const * _eventname, lid_t const & _part_id, cbm::io_kcomm_t * _iokcomm)
    {
        if ( CBM_HandleOk(this->m_handle))
            { return LD_PortIsPublished; }

        this->m_iokcomm = _iokcomm;
        this->m_handle = _iokcomm->publish_port<char>( _eventname, CBM_NoUnit, _part_id,
                                                       NULL, CBM_DynamicSize);
        if ( !CBM_HandleOk(this->m_handle))
            { return LD_PortIsNotPublished; }

        return LD_PortOk;
    }
    
    int  unpublish()
    {
        if ( !this->is_published())
            { return LD_PortIsNotPublished; }

        this->m_iokcomm->unpublish_event( this->m_handle);
        this->m_handle = CBM_None;
        this->m_iokcomm = NULL;

        return LD_PortOk;
    }


    int  send( char const * _eventattributes, size_t _eventattributes_sz)
    {
// TODO  masking?!
        if ( !_eventattributes || _eventattributes_sz == 0)
            { return LD_PortOk; }

        size_t const sz = this->m_iokcomm->send_event(
                this->m_handle, _eventattributes, _eventattributes_sz);
        if ( sz == invalid_size)
            { return LD_PortSendFailure; }
        return LD_PortOk;
    }
    int  send( EventAttributes const & _eventattributes)
    {
        size_t  eventattributes_sz = 0;
        char const *  eventattributes = _eventattributes.serialize( &eventattributes_sz);
        if ( eventattributes && eventattributes_sz > 0)
            { return this->send( eventattributes, eventattributes_sz); }
        return LD_PortSendFailure;
    }

//    EventAttributes new_event()
//        { return EventAttributes(); }
};
} /* namespace ldndc */


#include  <containers/cbm_set.h>
namespace ldndc {

#define LD_FieldsMemoryReserveCount 4
template < typename _Fields >
class LDNDC_API SharedFields
{
public:
    SharedFields( size_t = LD_FieldsMemoryReserveCount);
    virtual ~SharedFields();

    size_t  size() const
        { return CBM_SetSize( this->m_fields); }

    bool  have_fields( char const * /*name*/);
    bool  have_fields( cbm::string_t const &  _name)
        { return this->have_fields( _name.c_str()); }
    _Fields *  get_fields( char const * /*name*/);
    _Fields *  get_fields( cbm::string_t const &  _name)
        { return this->get_fields( _name.c_str()); }
    _Fields *  operator[]( char const *  _name)
        { return this->get_fields( _name); }
    _Fields *  operator[]( cbm::string_t const &  _name)
        { return this->get_fields( _name); }

    virtual _Fields *  new_fields( char const * /*name*/ /*, _Fields &*/);
    virtual lerr_t  delete_fields( char const *);

public:
    class Iterator
    {
    public:
        Iterator();
        Iterator( CBM_Set &);
        Iterator &  next();
        Iterator &  operator++()
            { return  this->next(); }

        operator _Fields *()
            { return static_cast< _Fields * >( this->m_it.value); }
        _Fields *  operator*()
            { return static_cast< _Fields * >( this->m_it.value); }
        _Fields const *  operator*() const
            { return static_cast< _Fields const * >( this->m_it.value); }

        bool  operator==( Iterator const &  _rhs) const
            { return this->m_it.value == _rhs.m_it.value; }
        bool  operator!=( Iterator const &  _rhs) const
            { return !this->operator==( _rhs); }

    private:
        CBM_SetNodeIterator  m_it;
    };

    Iterator  begin()
        { return  Iterator( this->m_fields); }
    Iterator  end()
        { return  Iterator(); }

private:
    CBM_Set  m_fields;
};

} /* namespace ldndc */

template < typename _Fields >
ldndc::SharedFields< _Fields >::SharedFields( size_t  _memory_reserve_count)
{
    this->m_fields = CBM_SetCreate( CBM_MemPoolRoot,
        sizeof(_Fields), _memory_reserve_count, 0);
}
template < typename _Fields >
ldndc::SharedFields< _Fields >::~SharedFields< _Fields >()
{
    CBM_SetDestroy( this->m_fields);
}

template < typename _Fields >
_Fields *  ldndc::SharedFields< _Fields >::new_fields( char const *  _name)
{
    if ( this->have_fields( _name)) /*treat as error*/
        { return  NULL; }

    _Fields *  fields = (_Fields *)CBM_SetInsert(
                    this->m_fields, _name, NULL);
    if ( fields)
        { new ( fields) _Fields(); }
    return  fields;
}

template < typename _Fields >
lerr_t  ldndc::SharedFields< _Fields >::delete_fields(
        char const *  _name)
{
    _Fields *  fields = this->get_fields( _name);
    CBM_SetRemove( this->m_fields, _name);
    if ( fields)
        { fields->~_Fields(); }
    return  LDNDC_ERR_OK;
}

template < typename _Fields >
bool  ldndc::SharedFields< _Fields >::have_fields(
        char const *  _name)
{
    return  this->get_fields( _name) != NULL;
}
template < typename _Fields >
_Fields *  ldndc::SharedFields< _Fields >::get_fields(
        char const *  _name)
{
    CBM_SetNodeIterator  fi = CBM_SetFind( this->m_fields, _name);
    if ( !CBM_SetNodeIteratorIsEnd( fi))
        { return (_Fields *)fi.value; }
    return  NULL;
}


template < typename _Fields >
ldndc::SharedFields< _Fields >::Iterator::Iterator()
    : m_it( CBM_SetNodeIteratorEnd)
{ }
template < typename _Fields >
ldndc::SharedFields< _Fields >::Iterator::Iterator( CBM_Set &  _fields)
    : m_it( CBM_SetFindHead( _fields))
{ }
template < typename _Fields >
typename ldndc::SharedFields< _Fields >::Iterator &
ldndc::SharedFields< _Fields >::Iterator::next()
{
    if ( !CBM_SetNodeIteratorIsEnd( this->m_it))
        { this->m_it = CBM_SetFindNext( this->m_it); }
    return  *this;
}


#endif /* !LD_SHARED_H_ */

